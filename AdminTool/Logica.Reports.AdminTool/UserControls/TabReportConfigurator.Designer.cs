﻿using System;
namespace Logica.Reports.AdminTool.UserControls
{
	partial class TabReportConfigurator
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabReportConfigurator));
            this.pnlTop = new System.Windows.Forms.Panel();
            this.uiCbxPrintingFitToPage = new System.Windows.Forms.CheckBox();
            this.tbxHeader = new System.Windows.Forms.TextBox();
            this.lblTabHeader = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbSheets = new System.Windows.Forms.TabControl();
            this.tpPlanningData = new System.Windows.Forms.TabPage();
            this.tbPlanTables = new System.Windows.Forms.TabControl();
            this.tpPlanForm = new System.Windows.Forms.TabPage();
            this.ucmgPlan = new Logica.Reports.AdminTool.UserControls.ManageGrid();
            this.tsPlan = new System.Windows.Forms.ToolStrip();
            this.tsbAddPlan = new System.Windows.Forms.ToolStripButton();
            this.tsbDeletePlan = new System.Windows.Forms.ToolStripButton();
            this.tpFactData = new System.Windows.Forms.TabPage();
            this.tbFactTables = new System.Windows.Forms.TabControl();
            this.tpFactForm = new System.Windows.Forms.TabPage();
            this.ucmgFact = new Logica.Reports.AdminTool.UserControls.ManageGrid();
            this.tsFact = new System.Windows.Forms.ToolStrip();
            this.tsbAddFact = new System.Windows.Forms.ToolStripButton();
            this.tsDeleteTable = new System.Windows.Forms.ToolStripButton();
            this.pnlTop.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tbSheets.SuspendLayout();
            this.tpPlanningData.SuspendLayout();
            this.tbPlanTables.SuspendLayout();
            this.tpPlanForm.SuspendLayout();
            this.tsPlan.SuspendLayout();
            this.tpFactData.SuspendLayout();
            this.tbFactTables.SuspendLayout();
            this.tpFactForm.SuspendLayout();
            this.tsFact.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.uiCbxPrintingFitToPage);
            this.pnlTop.Controls.Add(this.tbxHeader);
            this.pnlTop.Controls.Add(this.lblTabHeader);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(613, 64);
            this.pnlTop.TabIndex = 0;
            // 
            // uiCbxPrintingFitToPage
            // 
            this.uiCbxPrintingFitToPage.AutoSize = true;
            this.uiCbxPrintingFitToPage.Location = new System.Drawing.Point(93, 34);
            this.uiCbxPrintingFitToPage.Name = "uiCbxPrintingFitToPage";
            this.uiCbxPrintingFitToPage.Size = new System.Drawing.Size(167, 17);
            this.uiCbxPrintingFitToPage.TabIndex = 2;
            this.uiCbxPrintingFitToPage.Text = "Fit tab to one page for printing";
            this.uiCbxPrintingFitToPage.UseVisualStyleBackColor = true;
            this.uiCbxPrintingFitToPage.CheckedChanged += new System.EventHandler(this.uiCbxPrintingFitToPage_CheckedChanged);
            // 
            // tbxHeader
            // 
            this.tbxHeader.Location = new System.Drawing.Point(93, 8);
            this.tbxHeader.Name = "tbxHeader";
            this.tbxHeader.Size = new System.Drawing.Size(472, 20);
            this.tbxHeader.TabIndex = 1;
            this.tbxHeader.Leave += new System.EventHandler(this.tbxHeader_Leave);
            // 
            // lblTabHeader
            // 
            this.lblTabHeader.AutoSize = true;
            this.lblTabHeader.Location = new System.Drawing.Point(13, 15);
            this.lblTabHeader.Name = "lblTabHeader";
            this.lblTabHeader.Size = new System.Drawing.Size(65, 13);
            this.lblTabHeader.TabIndex = 0;
            this.lblTabHeader.Text = "Tab header:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbSheets);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(613, 573);
            this.panel2.TabIndex = 1;
            // 
            // tbSheets
            // 
            this.tbSheets.Controls.Add(this.tpPlanningData);
            this.tbSheets.Controls.Add(this.tpFactData);
            this.tbSheets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSheets.Location = new System.Drawing.Point(0, 0);
            this.tbSheets.Name = "tbSheets";
            this.tbSheets.SelectedIndex = 0;
            this.tbSheets.Size = new System.Drawing.Size(613, 573);
            this.tbSheets.TabIndex = 1;
            // 
            // tpPlanningData
            // 
            this.tpPlanningData.Controls.Add(this.tbPlanTables);
            this.tpPlanningData.Controls.Add(this.tsPlan);
            this.tpPlanningData.Location = new System.Drawing.Point(4, 22);
            this.tpPlanningData.Name = "tpPlanningData";
            this.tpPlanningData.Padding = new System.Windows.Forms.Padding(3);
            this.tpPlanningData.Size = new System.Drawing.Size(605, 547);
            this.tpPlanningData.TabIndex = 0;
            this.tpPlanningData.Text = "Planning Data";
            this.tpPlanningData.UseVisualStyleBackColor = true;
            // 
            // tbPlanTables
            // 
            this.tbPlanTables.Controls.Add(this.tpPlanForm);
            this.tbPlanTables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPlanTables.Location = new System.Drawing.Point(3, 28);
            this.tbPlanTables.Name = "tbPlanTables";
            this.tbPlanTables.SelectedIndex = 0;
            this.tbPlanTables.Size = new System.Drawing.Size(599, 516);
            this.tbPlanTables.TabIndex = 3;
            // 
            // tpPlanForm
            // 
            this.tpPlanForm.Controls.Add(this.ucmgPlan);
            this.tpPlanForm.Location = new System.Drawing.Point(4, 22);
            this.tpPlanForm.Name = "tpPlanForm";
            this.tpPlanForm.Padding = new System.Windows.Forms.Padding(3);
            this.tpPlanForm.Size = new System.Drawing.Size(591, 490);
            this.tpPlanForm.TabIndex = 0;
            this.tpPlanForm.Text = "Table_1";
            this.tpPlanForm.UseVisualStyleBackColor = true;
            // 
            // ucmgPlan
            // 
            this.ucmgPlan.CurrentTable = null;
            this.ucmgPlan.CurrentTableType = Logica.Reports.ConfigXmlParser.Model.TableType.Plan;
            this.ucmgPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucmgPlan.Location = new System.Drawing.Point(3, 3);
            this.ucmgPlan.Name = "ucmgPlan";
            this.ucmgPlan.Size = new System.Drawing.Size(585, 484);
            this.ucmgPlan.TabIndex = 0;
            this.ucmgPlan.TableIdentifier = new System.Guid("9fa9dc7d-e801-4d42-b8f0-c1d0b2ef72ab");
            // 
            // tsPlan
            // 
            this.tsPlan.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAddPlan,
            this.tsbDeletePlan});
            this.tsPlan.Location = new System.Drawing.Point(3, 3);
            this.tsPlan.Name = "tsPlan";
            this.tsPlan.Size = new System.Drawing.Size(599, 25);
            this.tsPlan.TabIndex = 2;
            this.tsPlan.Text = "toolStrip1";
            // 
            // tsbAddPlan
            // 
            this.tsbAddPlan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbAddPlan.Enabled = false;
            this.tsbAddPlan.Image = ((System.Drawing.Image)(resources.GetObject("tsbAddPlan.Image")));
            this.tsbAddPlan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddPlan.Name = "tsbAddPlan";
            this.tsbAddPlan.Size = new System.Drawing.Size(65, 22);
            this.tsbAddPlan.Text = "Add Table";
            this.tsbAddPlan.Click += new System.EventHandler(this.tsbAddPlan_Click);
            // 
            // tsbDeletePlan
            // 
            this.tsbDeletePlan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDeletePlan.Enabled = false;
            this.tsbDeletePlan.Image = ((System.Drawing.Image)(resources.GetObject("tsbDeletePlan.Image")));
            this.tsbDeletePlan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDeletePlan.Name = "tsbDeletePlan";
            this.tsbDeletePlan.Size = new System.Drawing.Size(76, 22);
            this.tsbDeletePlan.Text = "Delete Table";
            this.tsbDeletePlan.Click += new System.EventHandler(this.tsbDeletePlan_Click);
            // 
            // tpFactData
            // 
            this.tpFactData.Controls.Add(this.tbFactTables);
            this.tpFactData.Controls.Add(this.tsFact);
            this.tpFactData.Location = new System.Drawing.Point(4, 22);
            this.tpFactData.Name = "tpFactData";
            this.tpFactData.Padding = new System.Windows.Forms.Padding(3);
            this.tpFactData.Size = new System.Drawing.Size(605, 547);
            this.tpFactData.TabIndex = 1;
            this.tpFactData.Text = "Fact Data";
            this.tpFactData.UseVisualStyleBackColor = true;
            // 
            // tbFactTables
            // 
            this.tbFactTables.Controls.Add(this.tpFactForm);
            this.tbFactTables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFactTables.Location = new System.Drawing.Point(3, 28);
            this.tbFactTables.Name = "tbFactTables";
            this.tbFactTables.SelectedIndex = 0;
            this.tbFactTables.Size = new System.Drawing.Size(599, 516);
            this.tbFactTables.TabIndex = 3;
            // 
            // tpFactForm
            // 
            this.tpFactForm.Controls.Add(this.ucmgFact);
            this.tpFactForm.Location = new System.Drawing.Point(4, 22);
            this.tpFactForm.Name = "tpFactForm";
            this.tpFactForm.Padding = new System.Windows.Forms.Padding(3);
            this.tpFactForm.Size = new System.Drawing.Size(591, 490);
            this.tpFactForm.TabIndex = 0;
            this.tpFactForm.Text = "Table_1";
            this.tpFactForm.UseVisualStyleBackColor = true;
            // 
            // ucmgFact
            // 
            this.ucmgFact.CurrentTable = null;
            this.ucmgFact.CurrentTableType = Logica.Reports.ConfigXmlParser.Model.TableType.Fact;
            this.ucmgFact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucmgFact.Location = new System.Drawing.Point(3, 3);
            this.ucmgFact.Name = "ucmgFact";
            this.ucmgFact.Size = new System.Drawing.Size(585, 484);
            this.ucmgFact.TabIndex = 0;
            this.ucmgFact.TableIdentifier = new System.Guid("b22281a9-6802-4900-91be-3245228862a1");
            // 
            // tsFact
            // 
            this.tsFact.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAddFact,
            this.tsDeleteTable});
            this.tsFact.Location = new System.Drawing.Point(3, 3);
            this.tsFact.Name = "tsFact";
            this.tsFact.Size = new System.Drawing.Size(599, 25);
            this.tsFact.TabIndex = 2;
            this.tsFact.Text = "toolStrip2";
            // 
            // tsbAddFact
            // 
            this.tsbAddFact.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbAddFact.Enabled = false;
            this.tsbAddFact.Image = ((System.Drawing.Image)(resources.GetObject("tsbAddFact.Image")));
            this.tsbAddFact.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddFact.Name = "tsbAddFact";
            this.tsbAddFact.Size = new System.Drawing.Size(65, 22);
            this.tsbAddFact.Text = "Add Table";
            this.tsbAddFact.Click += new System.EventHandler(this.tsbAddFact_Click);
            // 
            // tsDeleteTable
            // 
            this.tsDeleteTable.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsDeleteTable.Enabled = false;
            this.tsDeleteTable.Image = ((System.Drawing.Image)(resources.GetObject("tsDeleteTable.Image")));
            this.tsDeleteTable.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsDeleteTable.Name = "tsDeleteTable";
            this.tsDeleteTable.Size = new System.Drawing.Size(76, 22);
            this.tsDeleteTable.Text = "Delete Table";
            this.tsDeleteTable.Click += new System.EventHandler(this.tsDeleteTable_Click);
            // 
            // TabReportConfigurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlTop);
            this.Name = "TabReportConfigurator";
            this.Size = new System.Drawing.Size(613, 637);
            this.VisibleChanged += new System.EventHandler(this.TabReportConfigurator_VisibleChanged);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tbSheets.ResumeLayout(false);
            this.tpPlanningData.ResumeLayout(false);
            this.tpPlanningData.PerformLayout();
            this.tbPlanTables.ResumeLayout(false);
            this.tpPlanForm.ResumeLayout(false);
            this.tsPlan.ResumeLayout(false);
            this.tsPlan.PerformLayout();
            this.tpFactData.ResumeLayout(false);
            this.tpFactData.PerformLayout();
            this.tbFactTables.ResumeLayout(false);
            this.tpFactForm.ResumeLayout(false);
            this.tsFact.ResumeLayout(false);
            this.tsFact.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlTop;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TabControl tbSheets;
		private System.Windows.Forms.TabPage tpPlanningData;
		private System.Windows.Forms.TabPage tpFactData;
		private System.Windows.Forms.TextBox tbxHeader;
		private System.Windows.Forms.Label lblTabHeader;
		private System.Windows.Forms.ToolStrip tsPlan;
		private System.Windows.Forms.ToolStripButton tsbAddPlan;
		private System.Windows.Forms.ToolStripButton tsbDeletePlan;
		private System.Windows.Forms.ToolStrip tsFact;
		private System.Windows.Forms.ToolStripButton tsbAddFact;
		private System.Windows.Forms.TabControl tbPlanTables;
		private System.Windows.Forms.TabPage tpPlanForm;
		private ManageGrid ucmgPlan;
		private System.Windows.Forms.TabControl tbFactTables;
		private System.Windows.Forms.TabPage tpFactForm;
		private ManageGrid ucmgFact;
        private System.Windows.Forms.ToolStripButton tsDeleteTable;
        private System.Windows.Forms.CheckBox uiCbxPrintingFitToPage;

	}
}
