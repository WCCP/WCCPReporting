﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Logica.Reports.AdminTool.UserControls.Contracts
{
    interface ILayoutDesigner
    {
        /// <summary>
        /// Sets the initial configuration for a designer control.
        /// </summary>
        /// <param name="dt">The datasource table.</param>
        /// <param name="xmlLayout">The XML layout.</param>
        void Initialize(DataTable dt, string xmlLayout);
        /// <summary>
        /// Serializes the layout.
        /// </summary>
        /// <returns>Serialized layout.</returns>
        string SerializeLayout();
    }
}
