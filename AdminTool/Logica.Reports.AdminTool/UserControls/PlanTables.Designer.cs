﻿namespace Logica.Reports.AdminTool.UserControls
{
	partial class PlanTables
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvColumns = new System.Windows.Forms.ListView();
			this.propertyGrid = new System.Windows.Forms.PropertyGrid();
			this.tbxTableName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnUpdateDB = new System.Windows.Forms.Button();
			this.btnAddColumn = new System.Windows.Forms.Button();
			this.btnDeleteColumn = new System.Windows.Forms.Button();
			this.btnClearColumn = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lvColumns
			// 
			this.lvColumns.Activation = System.Windows.Forms.ItemActivation.TwoClick;
			this.lvColumns.BackColor = System.Drawing.Color.White;
			this.lvColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lvColumns.FullRowSelect = true;
			this.lvColumns.GridLines = true;
			this.lvColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.lvColumns.HideSelection = false;
			this.lvColumns.Location = new System.Drawing.Point(4, 37);
			this.lvColumns.MultiSelect = false;
			this.lvColumns.Name = "lvColumns";
			this.lvColumns.Size = new System.Drawing.Size(240, 222);
			this.lvColumns.TabIndex = 27;
			this.lvColumns.UseCompatibleStateImageBehavior = false;
			this.lvColumns.View = System.Windows.Forms.View.Tile;
			this.lvColumns.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvColumns_MouseDoubleClick);
			// 
			// propertyGrid
			// 
			this.propertyGrid.Location = new System.Drawing.Point(256, 37);
			this.propertyGrid.Name = "propertyGrid";
			this.propertyGrid.Size = new System.Drawing.Size(273, 222);
			this.propertyGrid.TabIndex = 26;
			// 
			// tbxTableName
			// 
			this.tbxTableName.Location = new System.Drawing.Point(108, 4);
			this.tbxTableName.Name = "tbxTableName";
			this.tbxTableName.Size = new System.Drawing.Size(421, 20);
			this.tbxTableName.TabIndex = 25;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 11);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 13);
			this.label2.TabIndex = 24;
			this.label2.Text = "Table name";
			// 
			// btnUpdateDB
			// 
			this.btnUpdateDB.Location = new System.Drawing.Point(6, 275);
			this.btnUpdateDB.Name = "btnUpdateDB";
			this.btnUpdateDB.Size = new System.Drawing.Size(75, 23);
			this.btnUpdateDB.TabIndex = 28;
			this.btnUpdateDB.Text = "Update DB";
			this.btnUpdateDB.UseVisualStyleBackColor = true;
			this.btnUpdateDB.Click += new System.EventHandler(this.btnUpdateDB_Click);
			// 
			// btnAddColumn
			// 
			this.btnAddColumn.Location = new System.Drawing.Point(256, 275);
			this.btnAddColumn.Name = "btnAddColumn";
			this.btnAddColumn.Size = new System.Drawing.Size(75, 23);
			this.btnAddColumn.TabIndex = 29;
			this.btnAddColumn.Text = "Add";
			this.btnAddColumn.UseVisualStyleBackColor = true;
			this.btnAddColumn.Click += new System.EventHandler(this.btnAddColumn_Click);
			// 
			// btnDeleteColumn
			// 
			this.btnDeleteColumn.Location = new System.Drawing.Point(356, 275);
			this.btnDeleteColumn.Name = "btnDeleteColumn";
			this.btnDeleteColumn.Size = new System.Drawing.Size(75, 23);
			this.btnDeleteColumn.TabIndex = 30;
			this.btnDeleteColumn.Text = "Delete";
			this.btnDeleteColumn.UseVisualStyleBackColor = true;
			this.btnDeleteColumn.Click += new System.EventHandler(this.btnDeleteColumn_Click);
			// 
			// btnClearColumn
			// 
			this.btnClearColumn.Location = new System.Drawing.Point(454, 275);
			this.btnClearColumn.Name = "btnClearColumn";
			this.btnClearColumn.Size = new System.Drawing.Size(75, 23);
			this.btnClearColumn.TabIndex = 31;
			this.btnClearColumn.Text = "Clear";
			this.btnClearColumn.UseVisualStyleBackColor = true;
			this.btnClearColumn.Click += new System.EventHandler(this.btnClearColumn_Click);
			// 
			// ucPlanTables
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnClearColumn);
			this.Controls.Add(this.btnDeleteColumn);
			this.Controls.Add(this.btnAddColumn);
			this.Controls.Add(this.btnUpdateDB);
			this.Controls.Add(this.lvColumns);
			this.Controls.Add(this.propertyGrid);
			this.Controls.Add(this.tbxTableName);
			this.Controls.Add(this.label2);
			this.Name = "ucPlanTables";
			this.Size = new System.Drawing.Size(539, 317);
			this.Load += new System.EventHandler(this.ucPlanTables_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView lvColumns;
		private System.Windows.Forms.PropertyGrid propertyGrid;
		private System.Windows.Forms.TextBox tbxTableName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnUpdateDB;
		private System.Windows.Forms.Button btnAddColumn;
		private System.Windows.Forms.Button btnDeleteColumn;
		private System.Windows.Forms.Button btnClearColumn;
	}
}
