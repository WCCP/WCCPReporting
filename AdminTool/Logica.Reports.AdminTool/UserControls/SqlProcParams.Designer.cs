﻿namespace Logica.Reports.AdminTool.UserControls
{
	partial class SqlProcParams
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SqlProcParams));
			this.imgs = new System.Windows.Forms.ImageList(this.components);
			this.lvSelectParams = new System.Windows.Forms.ListView();
			this.pgSParamsDetails = new System.Windows.Forms.PropertyGrid();
			this.tbxSelectProc = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnSelectParams = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// imgs
			// 
			this.imgs.ImageStream = ((System.Windows.Forms.ImageListStreamer) (resources.GetObject("imgs.ImageStream")));
			this.imgs.TransparentColor = System.Drawing.Color.Transparent;
			this.imgs.Images.SetKeyName(0, "refresh.jpg");
			// 
			// lvSelectParams
			// 
			this.lvSelectParams.Activation = System.Windows.Forms.ItemActivation.TwoClick;
			this.lvSelectParams.BackColor = System.Drawing.Color.White;
			this.lvSelectParams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lvSelectParams.FullRowSelect = true;
			this.lvSelectParams.GridLines = true;
			this.lvSelectParams.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.lvSelectParams.HideSelection = false;
			this.lvSelectParams.Location = new System.Drawing.Point(6, 45);
			this.lvSelectParams.MultiSelect = false;
			this.lvSelectParams.Name = "lvSelectParams";
			this.lvSelectParams.Size = new System.Drawing.Size(240, 222);
			this.lvSelectParams.TabIndex = 22;
			this.lvSelectParams.UseCompatibleStateImageBehavior = false;
			this.lvSelectParams.View = System.Windows.Forms.View.Tile;
			this.lvSelectParams.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvSelectParams_MouseClick);
			this.lvSelectParams.SelectedIndexChanged += new System.EventHandler(this.lvSelectParams_SelectedIndexChanged);
			// 
			// pgSParamsDetails
			// 
			this.pgSParamsDetails.Location = new System.Drawing.Point(263, 45);
			this.pgSParamsDetails.Name = "pgSParamsDetails";
			this.pgSParamsDetails.Size = new System.Drawing.Size(273, 222);
			this.pgSParamsDetails.TabIndex = 21;
			this.pgSParamsDetails.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgSParamsDetails_PropertyValueChanged);
			// 
			// tbxSelectProc
			// 
			this.tbxSelectProc.Location = new System.Drawing.Point(115, 9);
			this.tbxSelectProc.Name = "tbxSelectProc";
			this.tbxSelectProc.Size = new System.Drawing.Size(396, 20);
			this.tbxSelectProc.TabIndex = 20;
			this.tbxSelectProc.Leave += new System.EventHandler(this.tbxSelectProc_Leave);
			this.tbxSelectProc.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbxSelectProc_KeyUp);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(10, 14);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(85, 13);
			this.label2.TabIndex = 19;
			this.label2.Text = "Procedure name";
			// 
			// btnSelectParams
			// 
			this.btnSelectParams.ImageIndex = 0;
			this.btnSelectParams.ImageList = this.imgs;
			this.btnSelectParams.Location = new System.Drawing.Point(512, 7);
			this.btnSelectParams.Name = "btnSelectParams";
			this.btnSelectParams.Size = new System.Drawing.Size(24, 24);
			this.btnSelectParams.TabIndex = 23;
			this.btnSelectParams.UseVisualStyleBackColor = true;
			this.btnSelectParams.Click += new System.EventHandler(this.btnSelectParams_Click);
			// 
			// SqlProcParams
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnSelectParams);
			this.Controls.Add(this.lvSelectParams);
			this.Controls.Add(this.pgSParamsDetails);
			this.Controls.Add(this.tbxSelectProc);
			this.Controls.Add(this.label2);
			this.Name = "SqlProcParams";
			this.Size = new System.Drawing.Size(542, 284);
			this.VisibleChanged += new System.EventHandler(this.SqlProcParams_VisibleChanged);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnSelectParams;
		private System.Windows.Forms.ImageList imgs;
		private System.Windows.Forms.ListView lvSelectParams;
		private System.Windows.Forms.PropertyGrid pgSParamsDetails;
		private System.Windows.Forms.TextBox tbxSelectProc;
		private System.Windows.Forms.Label label2;
	}
}
