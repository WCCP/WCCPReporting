﻿namespace Logica.Reports.AdminTool.UserControls
{
	partial class ManageGrid
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageGrid));
            this.tbxHeader = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnShowInGrid = new System.Windows.Forms.Button();
            this.tbxTabOrderTable = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.xtbTables = new DevExpress.XtraTab.XtraTabControl();
            this.xtraCreateTable = new DevExpress.XtraTab.XtraTabPage();
            this.ucPlanTables1 = new Logica.Reports.AdminTool.UserControls.PlanTables();
            this.xtraSelectP = new DevExpress.XtraTab.XtraTabPage();
            this.ucSelectSqlProc = new Logica.Reports.AdminTool.UserControls.SqlProcParams();
            this.xtraModifyP = new DevExpress.XtraTab.XtraTabPage();
            this.ucModifySqlProc = new Logica.Reports.AdminTool.UserControls.SqlProcParams();
            this.xtraChart = new DevExpress.XtraTab.XtraTabPage();
            this.gbGridPosition = new System.Windows.Forms.GroupBox();
            this.cbGridPosition = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbDisplay = new System.Windows.Forms.ComboBox();
            this.gbPosition = new System.Windows.Forms.GroupBox();
            this.cbPosition = new System.Windows.Forms.ComboBox();
            this.btnChartSetup = new System.Windows.Forms.Button();
            this.gbChartSettings = new System.Windows.Forms.GroupBox();
            this.rbUseForCalculation = new System.Windows.Forms.RadioButton();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.rbOnlyChart = new System.Windows.Forms.RadioButton();
            this.rbChartAndGrid = new System.Windows.Forms.RadioButton();
            this.uiBtnGridPreview = new System.Windows.Forms.Button();
            this.uiCbxHideHeader = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.xtbTables)).BeginInit();
            this.xtbTables.SuspendLayout();
            this.xtraCreateTable.SuspendLayout();
            this.xtraSelectP.SuspendLayout();
            this.xtraModifyP.SuspendLayout();
            this.xtraChart.SuspendLayout();
            this.gbGridPosition.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbPosition.SuspendLayout();
            this.gbChartSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbxHeader
            // 
            this.tbxHeader.Location = new System.Drawing.Point(130, 13);
            this.tbxHeader.Name = "tbxHeader";
            this.tbxHeader.Size = new System.Drawing.Size(416, 20);
            this.tbxHeader.TabIndex = 11;
            this.tbxHeader.Leave += new System.EventHandler(this.tbxHeader_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Header";
            // 
            // btnShowInGrid
            // 
            this.btnShowInGrid.Location = new System.Drawing.Point(9, 97);
            this.btnShowInGrid.Name = "btnShowInGrid";
            this.btnShowInGrid.Size = new System.Drawing.Size(99, 23);
            this.btnShowInGrid.TabIndex = 17;
            this.btnShowInGrid.Text = "Setup DevX grid";
            this.btnShowInGrid.UseVisualStyleBackColor = true;
            this.btnShowInGrid.Click += new System.EventHandler(this.btnShowInGrid_Click);
            // 
            // tbxTabOrderTable
            // 
            this.tbxTabOrderTable.Location = new System.Drawing.Point(130, 47);
            this.tbxTabOrderTable.Name = "tbxTabOrderTable";
            this.tbxTabOrderTable.Size = new System.Drawing.Size(416, 20);
            this.tbxTabOrderTable.TabIndex = 23;
            this.tbxTabOrderTable.Leave += new System.EventHandler(this.tbxTabOrderTable_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Order";
            // 
            // xtbTables
            // 
            this.xtbTables.Location = new System.Drawing.Point(9, 126);
            this.xtbTables.Name = "xtbTables";
            this.xtbTables.SelectedTabPage = this.xtraCreateTable;
            this.xtbTables.Size = new System.Drawing.Size(548, 344);
            this.xtbTables.TabIndex = 24;
            this.xtbTables.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraCreateTable,
            this.xtraSelectP,
            this.xtraModifyP,
            this.xtraChart});
            // 
            // xtraCreateTable
            // 
            this.xtraCreateTable.Controls.Add(this.ucPlanTables1);
            this.xtraCreateTable.Name = "xtraCreateTable";
            this.xtraCreateTable.Size = new System.Drawing.Size(541, 315);
            this.xtraCreateTable.Text = "Create Plan Table";
            // 
            // ucPlanTables1
            // 
            this.ucPlanTables1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPlanTables1.Location = new System.Drawing.Point(0, 0);
            this.ucPlanTables1.Name = "ucPlanTables1";
            this.ucPlanTables1.Size = new System.Drawing.Size(541, 315);
            this.ucPlanTables1.TabIndex = 0;
            this.ucPlanTables1.TableColumns = ((System.Collections.Generic.List<Logica.Reports.AdminTool.Data.PropertyTable>)(resources.GetObject("ucPlanTables1.TableColumns")));
            // 
            // xtraSelectP
            // 
            this.xtraSelectP.Controls.Add(this.ucSelectSqlProc);
            this.xtraSelectP.Name = "xtraSelectP";
            this.xtraSelectP.Size = new System.Drawing.Size(541, 315);
            this.xtraSelectP.Text = "Setup select procedure";
            // 
            // ucSelectSqlProc
            // 
            this.ucSelectSqlProc.CurrentQuery = null;
            this.ucSelectSqlProc.CurrentQueryType = Logica.Reports.Common.SQLQueryType.DataSet;
            this.ucSelectSqlProc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucSelectSqlProc.IsTableQuery = false;
            this.ucSelectSqlProc.Location = new System.Drawing.Point(0, 0);
            this.ucSelectSqlProc.Name = "ucSelectSqlProc";
            this.ucSelectSqlProc.PropertyValueChanged = false;
            this.ucSelectSqlProc.Size = new System.Drawing.Size(541, 315);
            this.ucSelectSqlProc.TabIndex = 2;
            // 
            // xtraModifyP
            // 
            this.xtraModifyP.Controls.Add(this.ucModifySqlProc);
            this.xtraModifyP.Name = "xtraModifyP";
            this.xtraModifyP.Size = new System.Drawing.Size(541, 315);
            this.xtraModifyP.Text = "Setup modify procedure";
            // 
            // ucModifySqlProc
            // 
            this.ucModifySqlProc.CurrentQuery = null;
            this.ucModifySqlProc.CurrentQueryType = Logica.Reports.Common.SQLQueryType.DataSet;
            this.ucModifySqlProc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucModifySqlProc.IsTableQuery = false;
            this.ucModifySqlProc.Location = new System.Drawing.Point(0, 0);
            this.ucModifySqlProc.Name = "ucModifySqlProc";
            this.ucModifySqlProc.PropertyValueChanged = false;
            this.ucModifySqlProc.Size = new System.Drawing.Size(541, 315);
            this.ucModifySqlProc.TabIndex = 2;
            // 
            // xtraChart
            // 
            this.xtraChart.Controls.Add(this.gbGridPosition);
            this.xtraChart.Controls.Add(this.groupBox1);
            this.xtraChart.Controls.Add(this.gbPosition);
            this.xtraChart.Controls.Add(this.btnChartSetup);
            this.xtraChart.Controls.Add(this.gbChartSettings);
            this.xtraChart.Name = "xtraChart";
            this.xtraChart.Size = new System.Drawing.Size(541, 315);
            this.xtraChart.Text = "Setup chart and commons";
            // 
            // gbGridPosition
            // 
            this.gbGridPosition.Controls.Add(this.cbGridPosition);
            this.gbGridPosition.Location = new System.Drawing.Point(5, 95);
            this.gbGridPosition.Name = "gbGridPosition";
            this.gbGridPosition.Size = new System.Drawing.Size(533, 56);
            this.gbGridPosition.TabIndex = 30;
            this.gbGridPosition.TabStop = false;
            this.gbGridPosition.Text = "Grid Position";
            // 
            // cbGridPosition
            // 
            this.cbGridPosition.FormattingEnabled = true;
            this.cbGridPosition.Items.AddRange(new object[] {
            "Right",
            "Left",
            "Top",
            "Bottom"});
            this.cbGridPosition.Location = new System.Drawing.Point(16, 19);
            this.cbGridPosition.Name = "cbGridPosition";
            this.cbGridPosition.Size = new System.Drawing.Size(196, 21);
            this.cbGridPosition.TabIndex = 0;
            this.cbGridPosition.SelectedIndexChanged += new System.EventHandler(this.cbGridPosition_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbDisplay);
            this.groupBox1.Location = new System.Drawing.Point(9, 252);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 60);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Common properties";
            // 
            // cbDisplay
            // 
            this.cbDisplay.FormattingEnabled = true;
            this.cbDisplay.Items.AddRange(new object[] {
            "Everywhere",
            "FormOnly",
            "ExportDocOnly"});
            this.cbDisplay.Location = new System.Drawing.Point(16, 19);
            this.cbDisplay.Name = "cbDisplay";
            this.cbDisplay.Size = new System.Drawing.Size(196, 21);
            this.cbDisplay.TabIndex = 0;
            this.cbDisplay.SelectedIndexChanged += new System.EventHandler(this.cbDisplay_SelectedIndexChanged);
            // 
            // gbPosition
            // 
            this.gbPosition.Controls.Add(this.cbPosition);
            this.gbPosition.Location = new System.Drawing.Point(5, 157);
            this.gbPosition.Name = "gbPosition";
            this.gbPosition.Size = new System.Drawing.Size(533, 60);
            this.gbPosition.TabIndex = 28;
            this.gbPosition.TabStop = false;
            this.gbPosition.Text = "Chart Position";
            // 
            // cbPosition
            // 
            this.cbPosition.Enabled = false;
            this.cbPosition.FormattingEnabled = true;
            this.cbPosition.Items.AddRange(new object[] {
            "Right",
            "Left",
            "Bottom",
            "Top"});
            this.cbPosition.Location = new System.Drawing.Point(16, 19);
            this.cbPosition.Name = "cbPosition";
            this.cbPosition.Size = new System.Drawing.Size(196, 21);
            this.cbPosition.TabIndex = 0;
            this.cbPosition.SelectedIndexChanged += new System.EventHandler(this.cbPosition_SelectedIndexChanged);
            // 
            // btnChartSetup
            // 
            this.btnChartSetup.Enabled = false;
            this.btnChartSetup.Location = new System.Drawing.Point(439, 223);
            this.btnChartSetup.Name = "btnChartSetup";
            this.btnChartSetup.Size = new System.Drawing.Size(99, 23);
            this.btnChartSetup.TabIndex = 26;
            this.btnChartSetup.Text = "Setup Chart";
            this.btnChartSetup.UseVisualStyleBackColor = true;
            this.btnChartSetup.Click += new System.EventHandler(this.btnChartSetup_Click);
            // 
            // gbChartSettings
            // 
            this.gbChartSettings.Controls.Add(this.rbUseForCalculation);
            this.gbChartSettings.Controls.Add(this.rbNone);
            this.gbChartSettings.Controls.Add(this.rbOnlyChart);
            this.gbChartSettings.Controls.Add(this.rbChartAndGrid);
            this.gbChartSettings.Location = new System.Drawing.Point(5, 3);
            this.gbChartSettings.Name = "gbChartSettings";
            this.gbChartSettings.Size = new System.Drawing.Size(529, 86);
            this.gbChartSettings.TabIndex = 1;
            this.gbChartSettings.TabStop = false;
            // 
            // rbUseForCalculation
            // 
            this.rbUseForCalculation.AutoSize = true;
            this.rbUseForCalculation.Location = new System.Drawing.Point(224, 19);
            this.rbUseForCalculation.Name = "rbUseForCalculation";
            this.rbUseForCalculation.Size = new System.Drawing.Size(198, 17);
            this.rbUseForCalculation.TabIndex = 3;
            this.rbUseForCalculation.Tag = "Invisible";
            this.rbUseForCalculation.Text = "Init calculations(don\'t show anything)";
            this.rbUseForCalculation.UseVisualStyleBackColor = true;
            this.rbUseForCalculation.CheckedChanged += new System.EventHandler(this.rbMode_CheckedChanged);
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Checked = true;
            this.rbNone.Location = new System.Drawing.Point(16, 19);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 2;
            this.rbNone.TabStop = true;
            this.rbNone.Tag = "Grid";
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            this.rbNone.CheckedChanged += new System.EventHandler(this.rbMode_CheckedChanged);
            // 
            // rbOnlyChart
            // 
            this.rbOnlyChart.AutoSize = true;
            this.rbOnlyChart.Location = new System.Drawing.Point(16, 65);
            this.rbOnlyChart.Name = "rbOnlyChart";
            this.rbOnlyChart.Size = new System.Drawing.Size(74, 17);
            this.rbOnlyChart.TabIndex = 1;
            this.rbOnlyChart.Tag = "Chart";
            this.rbOnlyChart.Text = "Only Chart";
            this.rbOnlyChart.UseVisualStyleBackColor = true;
            this.rbOnlyChart.CheckedChanged += new System.EventHandler(this.rbMode_CheckedChanged);
            // 
            // rbChartAndGrid
            // 
            this.rbChartAndGrid.AutoSize = true;
            this.rbChartAndGrid.Location = new System.Drawing.Point(16, 42);
            this.rbChartAndGrid.Name = "rbChartAndGrid";
            this.rbChartAndGrid.Size = new System.Drawing.Size(93, 17);
            this.rbChartAndGrid.TabIndex = 0;
            this.rbChartAndGrid.Tag = "GridChart";
            this.rbChartAndGrid.Text = "Chart and Grid";
            this.rbChartAndGrid.UseVisualStyleBackColor = true;
            this.rbChartAndGrid.CheckedChanged += new System.EventHandler(this.rbMode_CheckedChanged);
            // 
            // uiBtnGridPreview
            // 
            this.uiBtnGridPreview.Location = new System.Drawing.Point(114, 97);
            this.uiBtnGridPreview.Name = "uiBtnGridPreview";
            this.uiBtnGridPreview.Size = new System.Drawing.Size(114, 23);
            this.uiBtnGridPreview.TabIndex = 25;
            this.uiBtnGridPreview.Text = "Preview DevX grid";
            this.uiBtnGridPreview.UseVisualStyleBackColor = true;
            this.uiBtnGridPreview.Click += new System.EventHandler(this.uiBtnGridPreview_Click);
            // 
            // uiCbxHideHeader
            // 
            this.uiCbxHideHeader.AutoSize = true;
            this.uiCbxHideHeader.Location = new System.Drawing.Point(130, 73);
            this.uiCbxHideHeader.Name = "uiCbxHideHeader";
            this.uiCbxHideHeader.Size = new System.Drawing.Size(87, 17);
            this.uiCbxHideHeader.TabIndex = 27;
            this.uiCbxHideHeader.Text = "Hide grid title";
            this.uiCbxHideHeader.UseVisualStyleBackColor = true;
            this.uiCbxHideHeader.CheckedChanged += new System.EventHandler(this.uiCbxHideHeader_CheckedChanged);
            // 
            // ManageGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiCbxHideHeader);
            this.Controls.Add(this.uiBtnGridPreview);
            this.Controls.Add(this.xtbTables);
            this.Controls.Add(this.tbxTabOrderTable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnShowInGrid);
            this.Controls.Add(this.tbxHeader);
            this.Controls.Add(this.label1);
            this.Name = "ManageGrid";
            this.Size = new System.Drawing.Size(570, 474);
            this.Load += new System.EventHandler(this.ManageGrid_Load);
            this.VisibleChanged += new System.EventHandler(this.ManageGrid_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.xtbTables)).EndInit();
            this.xtbTables.ResumeLayout(false);
            this.xtraCreateTable.ResumeLayout(false);
            this.xtraSelectP.ResumeLayout(false);
            this.xtraModifyP.ResumeLayout(false);
            this.xtraChart.ResumeLayout(false);
            this.gbGridPosition.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.gbPosition.ResumeLayout(false);
            this.gbChartSettings.ResumeLayout(false);
            this.gbChartSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tbxHeader;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnShowInGrid;
		private System.Windows.Forms.TextBox tbxTabOrderTable;
		private System.Windows.Forms.Label label2;
		private DevExpress.XtraTab.XtraTabControl xtbTables;
		private DevExpress.XtraTab.XtraTabPage xtraCreateTable;
		private DevExpress.XtraTab.XtraTabPage xtraSelectP;
		private DevExpress.XtraTab.XtraTabPage xtraModifyP;
		private SqlProcParams ucSelectSqlProc;
		private SqlProcParams ucModifySqlProc;
		private AdminTool.UserControls.PlanTables ucPlanTables1;
        private System.Windows.Forms.Button uiBtnGridPreview;
		private DevExpress.XtraTab.XtraTabPage xtraChart;
		private System.Windows.Forms.GroupBox gbChartSettings;
		private System.Windows.Forms.RadioButton rbOnlyChart;
		private System.Windows.Forms.RadioButton rbChartAndGrid;
        private System.Windows.Forms.RadioButton rbNone;
		private System.Windows.Forms.Button btnChartSetup;
		private System.Windows.Forms.GroupBox gbPosition;
		private System.Windows.Forms.ComboBox cbPosition;
        private System.Windows.Forms.CheckBox uiCbxHideHeader;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbDisplay;
        private System.Windows.Forms.GroupBox gbGridPosition;
        private System.Windows.Forms.ComboBox cbGridPosition;
        private System.Windows.Forms.RadioButton rbUseForCalculation;
		
		
	}
}
