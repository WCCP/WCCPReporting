﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.Customization;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Views.BandedGrid.Customization;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraVerticalGrid.Rows;
using Logica.Reports.ConfigXmlParser.GridExtension;
using System.IO;
using Logica.Reports.AdminTool.UserControls.Contracts;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;
using DevExpress.Utils;
using Logica.Reports.AdminTool.Forms;
using DevExpress.XtraEditors.Controls;
using System.Web;

namespace Logica.Reports.AdminTool.UserControls
{
    /// <summary>
    /// Provides functionality to design DXGrid layout to be saved in XML format.
    /// </summary>
    public partial class GridDesigner : UserControl, ILayoutDesigner
    {
        #region Private fields

        private ModifiedObjectType _modifiedObjectType;
        private CustomizationForm _customizationForm;
        private ColumnCustomizationListBox _columnListBox;
        private BandCustomizationListBox _bandListBox;
        private XtraTabControl _tabControl;
        private List<string> _columnList;
        private RepositoryItemLookUpEdit _groupFieldEditor;
        private RepositoryItemButtonEdit _applyHeaderStylesToAll;
        private RepositoryItemButtonEdit _applyCellStylesToAll;
        private RepositoryItemButtonEdit _applyColumnWidthToAll;


        #endregion // Private fields

        #region Properties

        /// <summary>
        /// Gets the list of columns, present in the grid.
        /// </summary>
        /// <value>The column list.</value>
        protected List<string> ColumnList
        {
            get
            {
                if (_columnList == null && uiGvMainView != null)
                {
                    _columnList = new List<string>();
                    _columnList.Add(string.Empty);
                    foreach (GridColumn col in uiGvMainView.Columns)
                    {
                        _columnList.Add(col.FieldName);
                    }
                }
                return _columnList;
            }
        }

        /// <summary>
        /// Gets the default customization object.
        /// </summary>
        /// <value>The default customization object.</value>
        protected object DefaultCustomizationObject
        {
            get
            {
                object result = null;
                int i = 0;
                switch (_modifiedObjectType)
                {
                    case ModifiedObjectType.Column:
                        GridColumn col = new DynamicColumn();
                        string fieldName = string.Format("NewField{0}", i);
                        while (uiGvMainView.Columns.ColumnByName(fieldName) != null)
                        {
                            fieldName = string.Format("NewField{0}", ++i);
                        }
                        //col.Name = "col" + fieldName;
                        col.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
                        col.FieldName = fieldName;
                        col.Caption = fieldName;
                        result = col;
                        break;
                    case ModifiedObjectType.Band:
                        GridBand band = new GroupBand();
                        string bandName = string.Format("NewBand{0}", i);
                        while (uiGvMainView.Bands[bandName] != null)
                        {
                            bandName = string.Format("NewBand{0}", ++i);
                        }
                        //band.Name = bandName;
                        band.Caption = bandName;
                        result = band;
                        break;
                }
                return result;
            }
        }

        #endregion // Properties

        #region Constructors

        public GridDesigner()
        {
            this.Dock = DockStyle.Fill;
            this.Top = 30;
            InitializeComponent();
        }

        #endregion // Constructors

        #region Event handlers

        private void tabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            _modifiedObjectType = (ModifiedObjectType)_tabControl.SelectedTabPageIndex;
            SelectCustomizationObject(DefaultCustomizationObject);
            ButtonSetVisibility(false);
        }

        private void uiGvMainView_MouseDown(object sender, MouseEventArgs e)
        {
            BandedGridHitInfo hitInfo = uiGvMainView.CalcHitInfo(e.Location);
            if (hitInfo.InColumn && hitInfo.Column != null)
            {
                SelectCustomizationObject(hitInfo.Column);
            }
            if (hitInfo.InBandPanel && hitInfo.Band != null)
            {
                SelectCustomizationObject(hitInfo.Band);
            }
        }

        private void uiTbtAdd_Click(object sender, EventArgs e)
        {
            switch (_modifiedObjectType)
            {
                case ModifiedObjectType.Column:
                    DynamicColumn column = (DynamicColumn)uiPgProperties.SelectedObject;
                    if (uiGvMainView.Columns.ColumnByFieldName(column.FieldName) != null)
                    {
                        MessageBox.Show(string.Format("A column with name '{0}' already exists.", column.FieldName), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    column.Visible = true;
                    if (column.Height > 0)
                    {
                        uiGvMainView.ColumnPanelRowHeight = column.Height;
                    }
                    uiGvMainView.Columns.Add(column);
                    if (_columnList != null)
                    {
                        _columnList.Add(column.FieldName);
                    }
                    break;
                case ModifiedObjectType.Band:
                    GroupBand band = (GroupBand)uiPgProperties.SelectedObject;
                    /*if (uiGvMainView.Bands[band.Name] != null)
                    {
                        MessageBox.Show(string.Format("A band with name '{0}' already exists.", band.Name), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }*/
                    band.Visible = false;
                    if (band.Height > 0)
                    {
                        uiGvMainView.BandPanelRowHeight = band.Height;
                    }
                    uiGvMainView.Bands.Add(band);
                    break;
            }
            SelectCustomizationObject(DefaultCustomizationObject);
        }

        private void uiTbtDelete_Click(object sender, EventArgs e)
        {
            switch (_modifiedObjectType)
            {
                case ModifiedObjectType.Column:
                    GridColumn column = (GridColumn)_customizationForm.ActiveListBox.SelectedItem;
                    uiGvMainView.Columns.Remove(column);
                    if (_columnList != null && _columnList.Contains(column.FieldName))
                    {
                        _columnList.Remove(column.FieldName);
                    }
                    break;
                case ModifiedObjectType.Band:
                    GridBand band = (GridBand)_customizationForm.ActiveListBox.SelectedItem;
                    uiGvMainView.Bands.Remove(band);
                    break;
            }
            SelectCustomizationObject(DefaultCustomizationObject);
        }

        private void uiTbtGroup_Click(object sender, EventArgs e)
        {
            GridColumn column = uiPgProperties.SelectedObject as GridColumn;
            if (column != null)
            {
                column.GroupIndex = uiGvMainView.SortInfo.GroupCount;
                column.Group();
                uiGvMainView.OptionsView.ShowGroupPanel = false;
            }
        }

        private void uiTbtEditStyleCondition_Click(object sender, EventArgs e)
        {
            ExpressionConditionsEditor editor = new ExpressionConditionsEditor();
            editor.Init(uiGvMainView);
            editor.Dock = DockStyle.Fill;
            using (Form frm = new Form())
            {
                frm.Text = "Format condition editor...";
                frm.Size = new Size(240, 440);
                frm.Controls.Add(editor);
                frm.ShowDialog();
            }
        }

        private void uiTbtShowViewEditor_Click(object sender, EventArgs e)
        {
            GridViewSettings viewSettings = new GridViewSettings(uiGvMainView);
            viewSettings.ShowDialog();
        }

        #endregion // Event handlers

        #region Helper methods

        private void ButtonSetVisibility(bool editMode)
        {
            uiTbtDelete.Visible = editMode;
            uiTbtGroup.Visible = editMode && _modifiedObjectType == ModifiedObjectType.Column;
        }

        private void SelectCustomizationObject(object customObject)
        {
            BaseRow appearanceRow;
            uiPgProperties.SelectedObject = customObject;
            if (_modifiedObjectType == ModifiedObjectType.Band)
            {
                BaseRow groupFieldRow = uiPgProperties.Rows.GetRowByFieldName("GroupColumn", true);
                if (groupFieldRow != null)
                {
                    groupFieldRow.Properties.RowEdit = _groupFieldEditor;
                }
            }
            else if (_modifiedObjectType == ModifiedObjectType.Column)
            {
                // Располагаем кнопку для применения стилей клетки ко всем заголовкам
                appearanceRow = uiPgProperties.Rows.GetRowByFieldName("AppearanceCell", true);
                if (appearanceRow != null)
                {
                    _applyCellStylesToAll.Buttons[0].Tag = ((GridColumn)customObject).AppearanceCell;
                    appearanceRow.Properties.RowEdit = _applyCellStylesToAll;
                }
            }
            
            // Располагаем кнопку для применения стилей заголовка ко всем заголовкам
            appearanceRow = uiPgProperties.Rows.GetRowByFieldName("AppearanceHeader", true);
            if (appearanceRow != null)
            {
                if (customObject is GridColumn)
                {
                    _applyHeaderStylesToAll.Buttons[0].Tag = ((GridColumn)customObject).AppearanceHeader;
                }
                if (customObject is GridBand)
                {
                    _applyHeaderStylesToAll.Buttons[0].Tag = ((GridBand)customObject).AppearanceHeader;
                }
                appearanceRow.Properties.RowEdit = _applyHeaderStylesToAll;
            }
            
            // Располагаем кнопку для применения ширины колонки
            appearanceRow = uiPgProperties.Rows.GetRowByFieldName("Width", true);
            if (appearanceRow != null && customObject is GridColumn)
            {
                _applyColumnWidthToAll.Buttons[0].Tag = ((GridColumn)customObject).Width;
                appearanceRow.Properties.RowEdit = _applyColumnWidthToAll;
            }
        }

        private void customizationListBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (((CustomCustomizationListBox)sender).SelectedIndex != -1)
            {
                SelectCustomizationObject(((CustomCustomizationListBox)sender).SelectedItem);
                ButtonSetVisibility(true);
            }
        }

        private void _applyHeaderStylesToAll_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            AppearanceObject appearance = e.Button.Tag as AppearanceObject;
            if (appearance != null)
            {
                foreach (GridColumn column in uiGvMainView.Columns)
                {
                    column.AppearanceHeader.Assign(appearance);
                    uiGvMainView.InvalidateColumnHeader(column);
                }
                foreach (GridBand band in uiGvMainView.Bands)
                {
                	ApplyHeaderStyleToBand(band, appearance);
                }
            }
        }
		private void ApplyHeaderStyleToBand (GridBand band, AppearanceObject appearance)
		{
			band.AppearanceHeader.Assign(appearance);
			uiGvMainView.InvalidateBandHeader(band);
			if (band.HasChildren)
				foreach (GridBand childBand in band.Children)
				{
					ApplyHeaderStyleToBand(childBand, appearance);
				}
		}

    	private void _applyCellStylesToAll_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            AppearanceObject appearance = e.Button.Tag as AppearanceObject;
            if (appearance != null)
            {
                foreach (GridColumn column in uiGvMainView.Columns)
                {
                    column.AppearanceCell.Assign(appearance);
                    uiGvMainView.InvalidateColumnHeader(column);
                }
            }
        }

        private void _applyColumnWidthToAll_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            int width = (int)e.Button.Tag;
            if (MessageBox.Show(string.Format("Are you shure that you want set all columns width to {0}?", width), "AdminTool", MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            foreach (GridColumn column in uiGvMainView.Columns)
            {
                column.Width = width;
            }
            uiGvMainView.Invalidate();
        }


        #endregion // Helper methods

        #region ILayoutDesigner members

        /// <summary>
        /// Initializes the specified dt.
        /// </summary>
        /// <param name="dt">The datasource.</param>
        /// <param name="xmlLayout">The XML layout.</param>
        public void Initialize(DataTable dt, string xmlLayout)
        {
            if (!string.IsNullOrEmpty(xmlLayout))
            {
                xmlLayout = HttpUtility.HtmlDecode(xmlLayout);
                uiGvMainView.ConfigureGridView(xmlLayout);
            }
            else
            {
                uiGvMainView.PopulateColumns(dt);
            }

            uiGvMainView.ShowCustomization();
            uiGvMainView.OptionsCustomization.AllowChangeColumnParent = true;
            uiGvMainView.OptionsCustomization.AllowColumnMoving = true;
            uiGvMainView.OptionsCustomization.AllowColumnResizing = true;
            uiGvMainView.OptionsCustomization.AllowBandMoving = true;
            uiGvMainView.OptionsCustomization.AllowBandResizing = true;
            uiGvMainView.OptionsCustomization.AllowFilter = false;
            uiGvMainView.OptionsCustomization.AllowGroup = false;

            _customizationForm = uiGvMainView.CustomizationForm;

            _groupFieldEditor = new RepositoryItemLookUpEdit();
            _groupFieldEditor.DataSource = ColumnList;
            _groupFieldEditor.SortColumnIndex = 0;
            _groupFieldEditor.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            _applyHeaderStylesToAll = new RepositoryItemButtonEdit();
            _applyHeaderStylesToAll.Buttons.Clear();
            _applyHeaderStylesToAll.Buttons.Add(new EditorButton(ButtonPredefines.Glyph, "Apply to all", 100, true, true, false, HorzAlignment.Center, null));
            _applyHeaderStylesToAll.ButtonClick += _applyHeaderStylesToAll_ButtonClick;
            _applyCellStylesToAll = new RepositoryItemButtonEdit();
            _applyCellStylesToAll.Buttons.Clear();
            _applyCellStylesToAll.Buttons.Add(new EditorButton(ButtonPredefines.Glyph, "Apply to all", 100, true, true, false, HorzAlignment.Center, null));
            _applyCellStylesToAll.ButtonClick += _applyCellStylesToAll_ButtonClick;
            _applyColumnWidthToAll = new RepositoryItemButtonEdit();
            _applyColumnWidthToAll.Buttons.Clear();
            _applyColumnWidthToAll.Buttons.Add(new EditorButton(ButtonPredefines.Glyph, "Apply to all", 100, true, true, false, HorzAlignment.Center, null));
            _applyColumnWidthToAll.ButtonClick += _applyColumnWidthToAll_ButtonClick;
            uiPgProperties.RepositoryItems.Add(_groupFieldEditor);
            uiPgProperties.RepositoryItems.Add(_applyHeaderStylesToAll);
            //uiPgProperties.RepositoryItems.Add(_applyColumnWidthToAll);


            _tabControl = _customizationForm.Controls[0] as XtraTabControl;
            _tabControl.Dock = DockStyle.Fill;
            if (_tabControl != null)
            {
                uiPnlListContainer.Controls.Add(_tabControl);
                _tabControl.SelectedPageChanged += new TabPageChangedEventHandler(tabControl_SelectedPageChanged);
                _columnListBox = _tabControl.TabPages[0].Controls[0] as ColumnCustomizationListBox;
                if (_columnListBox != null)
                {
                     //_columnListBox.SelectedIndexChanged += new EventHandler(customizationListBox_SelectedIndexChanged);
                     _columnListBox.MouseUp += new MouseEventHandler(customizationListBox_MouseDown);
                }
                _bandListBox = _tabControl.TabPages[1].Controls[0] as BandCustomizationListBox;
                if (_bandListBox != null)
                {
                    //_bandListBox.SelectedIndexChanged += new EventHandler(customizationListBox_SelectedIndexChanged);
                    _bandListBox.MouseUp += new MouseEventHandler(customizationListBox_MouseDown);
                }
            }

            _customizationForm.WindowState = FormWindowState.Minimized;
            SelectCustomizationObject(DefaultCustomizationObject);
        }

        /// <summary>
        /// Serializes the layout.
        /// </summary>
        /// <returns>Serialized layout.</returns>
        public string SerializeLayout()
        {
            string layoutXml;
            using (MemoryStream st = new MemoryStream())
            {
                uiGvMainView.SaveLayoutToStream(st, uiGvMainView.SerializationOptions);
                st.Position = 0;
                using (StreamReader sr = new StreamReader(st))
                {
                    layoutXml = sr.ReadToEnd();
                }
            }
            return layoutXml;
        }

        #endregion // Public methods

        private void названияКолонокВБэндыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (BandedGridColumn column in uiGvMainView.Columns)
            {
                if (column.Visible && column.OwnerBand != null)
                {
                    column.OwnerBand.Caption = column.Caption;
                    uiGvMainView.InvalidateBandHeader(column.OwnerBand);
                }
            }
        }

        private void названияБэндовВКолонкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (BandedGridColumn column in uiGvMainView.Columns)
            {
                if (column.Visible && column.OwnerBand != null && column.OwnerBand.Columns.Count == 1)
                {
                    column.Caption = column.OwnerBand.Caption;
                    uiGvMainView.InvalidateColumnHeader(column);
                }

            }
        }

        private void применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (uiPgProperties.SelectedObject is DynamicColumn)
            {
                var column = uiPgProperties.SelectedObject as DynamicColumn;
                if (column.OwnerBand != null)
                    foreach (DynamicColumn childColumn in column.OwnerBand.Columns)
                    {
                        childColumn.VerticalCaption = column.VerticalCaption;
                        childColumn.AppearanceHeader.ForeColor = column.AppearanceHeader.ForeColor;
                        uiGvMainView.InvalidateColumnHeader(childColumn);
                    }
            }
            if (uiPgProperties.SelectedObject is GroupBand)
            {
                var band = uiPgProperties.SelectedObject as GroupBand;
                if (band.ParentBand != null)
                    foreach (GroupBand childBand in band.ParentBand.Children)
                    {
                        childBand.VerticalCaption = band.VerticalCaption;
                        childBand.AppearanceHeader.ForeColor = band.AppearanceHeader.ForeColor;
                        uiGvMainView.InvalidateBandHeader(childBand);
                    }
            }
        }

        private void применитьСтильВГруппеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (uiPgProperties.SelectedObject is DynamicColumn)
            {
                var column = uiPgProperties.SelectedObject as DynamicColumn;
                if (column.OwnerBand != null)
                    foreach (DynamicColumn childColumn in column.OwnerBand.Columns)
                    {
                        childColumn.AppearanceHeader.Assign(column.AppearanceHeader);
                        uiGvMainView.InvalidateColumnHeader(childColumn);
                    }
            }
            if (uiPgProperties.SelectedObject is GroupBand)
            {
                var band = uiPgProperties.SelectedObject as GroupBand;
                if (band.ParentBand != null)
                    foreach (GroupBand childBand in band.ParentBand.Children)
                    {
                        childBand.AppearanceHeader.Assign(band.AppearanceHeader);
                        uiGvMainView.InvalidateBandHeader(childBand);
                    }
            }
        }
    }

    internal enum ModifiedObjectType
    {
        Column = 0,
        Band
    }
}
