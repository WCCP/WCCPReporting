﻿using System;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.AdminTool.Data;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.AdminTool.UserControls
{
	public partial class TabReportConfigurator : UserControl
	{
		int _reportTabIndex;
		string _reportHeader = string.Empty;

		public int ReportTabIndex
		{
			get
			{
				return _reportTabIndex;
			}
			set
			{
				_reportTabIndex = value;
			}
		}

		public string SheetHeader
		{
			get
			{
				return _reportHeader;
			}
			set
			{
				_reportHeader = value;
				tbxHeader.Text = value;
			}
		}

        public bool IsFitToPageForPrinting
        {
            get { return uiCbxPrintingFitToPage.Checked; }
            set { uiCbxPrintingFitToPage.Checked = value; }
        } 

		public TabReportConfigurator():this(0)
		{
			
		}
	
		public TabReportConfigurator(int reportTab)
		{
			InitializeComponent();
			_reportTabIndex = reportTab;
            ucmgPlan.HideUnusedTabs(TableType.Plan);
            ucmgFact.HideUnusedTabs(TableType.Fact);
		}

		public void SetupTabTable(Logica.Reports.ConfigXmlParser.Model.Table[] tables)
		{
			ReportProxyObject.PlanTableTabs = tbPlanTables;
			ReportProxyObject.FactTableTabs = tbFactTables;
			int plan_Index = 0;
			int fact_Index = 0;

			ucmgPlan.CurrentTableType = TableType.Plan;
			ucmgPlan.OnActionRised += new CallActionRefreshEventHandler(this.VerifyButtonStatuses);
			ucmgFact.CurrentTableType = TableType.Fact;
			ucmgFact.OnActionRised += new CallActionRefreshEventHandler(this.VerifyButtonStatuses);

			for (int tableIndex = 0; tableIndex < tables.Length; tableIndex++)
			{
				Logica.Reports.ConfigXmlParser.Model.Table currentTable = tables[tableIndex];
				if (currentTable.Id == Guid.Empty)
				{
					currentTable.Id = Guid.NewGuid();
				}
				if (currentTable.Type == Logica.Reports.ConfigXmlParser.Model.TableType.Plan)
				{    
					if (plan_Index == 0)
					{                        
						if (!string.IsNullOrEmpty(currentTable.Header))
						{
							tpPlanForm.Text = currentTable.Header;
						}
                        ucmgPlan.CurrentTable = currentTable;
					}
					else
					{
						ReportProxyObject.AddTableTab(currentTable.Type, currentTable, currentTable.Header, this.VerifyButtonStatuses);
					}
					plan_Index++;
				}
				else
				{
					currentTable.Type = TableType.Fact;
                    
					if (fact_Index == 0)
					{
                        if (!string.IsNullOrEmpty(currentTable.Header))
						{
							tpFactForm.Text = currentTable.Header;
						}
                        ucmgFact.CurrentTable = currentTable;
					}
					else
					{
						ReportProxyObject.AddTableTab(currentTable.Type, currentTable, currentTable.Header, this.VerifyButtonStatuses);
					}
					fact_Index++;
				}
			}
		}

		#region Events

		internal void VerifyButtonStatuses(object sender, EventArgs e)
		{
			int planCount = 0;
			int factCount = 0;
			ReportProxyObject.ChangeEnabledButtonStatuses(ref planCount, ref factCount);
			tsbAddPlan.Enabled = planCount > 0;
			tsbAddFact.Enabled = factCount > 0;
			tsbDeletePlan.Enabled = planCount > 1;
			tsDeleteTable.Enabled = factCount > 1;
		}

		private void tbxHeader_Leave(object sender, EventArgs e)
		{
			if (!tbxHeader.Text.Trim().Equals(_reportHeader) && !string.IsNullOrEmpty(tbxHeader.Text.Trim()))
			{
				_reportHeader = tbxHeader.Text.Trim();
				ReportProxyObject.SetReportSheetHeader(_reportTabIndex, _reportHeader);
			}
		}

        private void uiCbxPrintingFitToPage_CheckedChanged(object sender, EventArgs e)
        {
            ReportProxyObject.ConfigReport.Tabs[_reportTabIndex].PrintingFitToPage = uiCbxPrintingFitToPage.Checked;
        }

		#endregion

		private void tsbAddPlan_Click(object sender, EventArgs e)
		{
			AddTable(TableType.Plan);
			if (ReportProxyObject.PlanTableTabs.TabPages.Count > 1)
				tsbDeletePlan.Enabled = true;
		}

		private void AddTable(TableType tableType)
		{
			ReportProxyObject.AddSheetTable(tableType, this.VerifyButtonStatuses);
		}

		private void tsbAddFact_Click(object sender, EventArgs e)
		{
			AddTable(TableType.Fact);
			if (ReportProxyObject.FactTableTabs.TabPages.Count > 1)
				tsDeleteTable.Enabled = true;
		}

		private void DeleteTable(TableType tableType, int index, Guid identifier)
		{
			ReportProxyObject.DeleteSheetTable(tableType, index, identifier);
		}

		private void tsbDeletePlan_Click(object sender, EventArgs e)
		{
			DeleteTable(TableType.Plan, tbPlanTables.TabPages.IndexOf(tbPlanTables.SelectedTab), 
				GetTableIdentifier(tbPlanTables.SelectedTab));
			VerifyButtonStatuses(sender, e);
		}

		private Guid GetTableIdentifier(TabPage tab)
		{
			foreach (Control c in tab.Controls)
			{
				if (c.GetType() == typeof(ManageGrid))
				{
					return ((ManageGrid) c).TableIdentifier;
				}
			}
			return Guid.Empty;
		}

		private void tsDeleteTable_Click(object sender, EventArgs e)
		{
			DeleteTable(TableType.Fact, tbFactTables.TabPages.IndexOf(tbFactTables.SelectedTab),
				GetTableIdentifier(tbFactTables.SelectedTab));
			VerifyButtonStatuses(sender, e);
		}

		private void TabReportConfigurator_VisibleChanged(object sender, EventArgs e)
		{
			ReportProxyObject.PlanTableTabs = tbPlanTables;
			ReportProxyObject.FactTableTabs = tbFactTables;
		}
	}
}
