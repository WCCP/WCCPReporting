﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Logica.Reports.AdminTool.Data;
using System.ComponentModel;
using Logica.Reports.DataAccess;
using System.Collections;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.Common;

namespace Logica.Reports.AdminTool.UserControls
{
	public partial class SqlProcParams : UserControl
	{
		#region Delegates
		
		public SetProcedureNameChangedEventHandler OnRefreshProcedureName;
		
		#endregion

		#region const

		private const string STR_NAME = "Name";
		private const string STR_TYPE = "Type";
		private const string STR_DEFAULT_VALUE = "Default Value";
		
		#endregion

		#region Fields

		private PropertyTable _sqlParameterColumn;
		private Parameter _selectedProperty;
		private Query _currentQuery;
		private SQLQueryType _queryType;


		private bool _propertyValueChanged;
		private bool _isTable;
	   /// <summary>
	   /// true = TableQuery
	   /// false = PreConditionQuery
	   /// </summary>
		public bool IsTableQuery
		{
			get
			{
				return _isTable;
			}
			set
			{
				_isTable = value;
			}
		}

		public SQLQueryType CurrentQueryType
		{
			get
			{
				return _queryType;
			}
			set
			{
				_queryType = value;
			}
		}

		Guid GetTableId
		{
			get
			{
				return ((ManageGrid) ((DevExpress.XtraTab.XtraTabControl) ((DevExpress.XtraTab.XtraTabPage) this.Parent).Parent).Parent).TableIdentifier;
			}
		}

		TableType GetTableType
		{
			get
			{
				return ((ManageGrid) ((DevExpress.XtraTab.XtraTabControl) ((DevExpress.XtraTab.XtraTabPage) this.Parent).Parent).Parent).CurrentTableType;
			}
		}

		public Query CurrentQuery
		{
			get
			{
				return _currentQuery;
			}
			set
			{
				_currentQuery = value;
                if (_currentQuery != null)
                {
                    ReportProxyObject.CurrentQuery = _currentQuery;
                    tbxSelectProc.Text = _currentQuery.Name;
                }
			}
		}

		public bool PropertyValueChanged
		{
			get
			{
				return _propertyValueChanged;
			}
			set
			{
				_propertyValueChanged = value;
			}
		}

		#endregion

		#region Public Methods

		public SqlProcParams()
		{
			InitializeComponent();
		}

		public void SetupInitData()
		{
			btnSelectParams.Enabled = !string.IsNullOrEmpty(CurrentQuery.Name);
			RefreshListParams();
		}

		public void RefreshListParams()
		{
			lvSelectParams.Items.Clear();
			for (int i = 0; i < _currentQuery.Parameters.Length; i++)
			{
				ListViewItem lvi = new ListViewItem(_currentQuery.Parameters[i].Name);
				lvi.Tag = _currentQuery.Parameters[i];
				lvSelectParams.Items.Add(lvi);
			}
		}

		#endregion

		#region Events

		public void OnSetProcedureName(object sender, EventArgs e, string procedure, SQLQueryType qType)
		{
			ReportProxyObject.AddNewQuery(qType);
			tbxSelectProc.Text = procedure;
			btnSelectParams_Click(sender, e);
		}

		private void tbxSelectProc_Leave(object sender, EventArgs e)
		{
			btnSelectParams.Enabled = !string.IsNullOrEmpty(tbxSelectProc.Text.Trim());
		}

		private void btnSelectParams_Click(object sender, EventArgs e)
		{
			CurrentQuery = IsTableQuery ? ReportProxyObject.SetSheetTableProcedureName(GetTableId, _queryType, tbxSelectProc.Text.Trim(), GetTableType)
				: ReportProxyObject.SetInitProcedureName(_queryType, tbxSelectProc.Text.Trim(), CurrentQuery);

			RefreshListParams();
			if (_queryType == SQLQueryType.DataSet &&
			 OnRefreshProcedureName != null)
			{
				ReportProxyObject.CurrentBody.ChartXml = string.Empty;
				ReportProxyObject.CurrentBody.GridXml = string.Empty;
				ReportProxyObject.CurrentQuery = CurrentQuery;
				OnRefreshProcedureName(sender, e);
			}
		}

		private void lvSelectParams_MouseClick(object sender, MouseEventArgs e)
		{
			Parameter pp = (Parameter) lvSelectParams.SelectedItems[0].Tag;
			CreateColumnInfo();
			_sqlParameterColumn[STR_NAME] = pp.Name;
			_sqlParameterColumn[STR_TYPE] = pp.DataType;
			_sqlParameterColumn[STR_DEFAULT_VALUE] = Convert.ToString(pp.DefaultValue);
			this.pgSParamsDetails.SelectedObject = _sqlParameterColumn;
		}

		private void SqlProcParams_VisibleChanged(object sender, EventArgs e)
		{
			btnSelectParams.Enabled = !string.IsNullOrEmpty(tbxSelectProc.Text.Trim());
		}
		
		private void tbxSelectProc_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				tbxSelectProc_Leave(sender, null);
			}
		}

		#endregion

		#region PropertyGrid


		private void CreateColumnInfo()
		{
			_sqlParameterColumn = new PropertyTable();
			_sqlParameterColumn.Properties.Add(new PropertySpec(STR_NAME, typeof(String), null, null, "column name"));
			_sqlParameterColumn.Properties.Add(new PropertySpec(STR_TYPE, typeof(SqlDbType), null, null, SqlDbType.NVarChar));
			_sqlParameterColumn.Properties.Add(new PropertySpec(STR_DEFAULT_VALUE, typeof(String), null, null, string.Empty));

			_sqlParameterColumn[STR_NAME] = "";
			_sqlParameterColumn[STR_TYPE] = SqlDbType.VarChar;
			_sqlParameterColumn[STR_DEFAULT_VALUE] = "-1";
		}

		private void pgSParamsDetails_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			string value = Convert.ToString(e.ChangedItem.Value);
			foreach (Parameter pp in CurrentQuery.Parameters)
			{
				if (pp.Name == _selectedProperty.Name)
				{
					switch (e.ChangedItem.Label)
					{
						case "Default Value":
						if (ValidateParameterValue(pp, value))
						{
							pp.DefaultValue = value;
							_propertyValueChanged = true;
						}
						else
						{
							MessageBox.Show(string.Format("Value '{0}' does not support to parameter {1} type.", value, pp.DataType.ToString()));
						}
						break;
						case "Type":
						value = Convert.ToString(pp.DefaultValue);
						pp.DataType = (SqlDbType) e.ChangedItem.Value;
						_propertyValueChanged = true;
						if (!ValidateParameterValue(pp, value))
						{
							pp.DefaultValue = string.Empty;
							MessageBox.Show(string.Format("Value '{0}' does not support to parameter {1} type.", value, pp.DataType.ToString()));
						}
						break;
					}
				}
			}
			if (IsTableQuery)
			{
				ReportProxyObject.SetSheetTableProcedureParametersValue(GetTableId, CurrentQuery);
			}
			else
			{
				ReportProxyObject.SetInitProcedureParametersValue(CurrentQuery);
			}
		}

		private void lvSelectParams_SelectedIndexChanged(object sender, EventArgs e)
		{
			ArrayList objs = new ArrayList();
			foreach (ListViewItem item in lvSelectParams.SelectedItems)
			{
				_selectedProperty = (Parameter) item.Tag;
				objs.Add(item.Tag);
			}
			pgSParamsDetails.SelectedObjects = objs.ToArray();
		}

		internal bool ValidateParameterValue(Parameter p, string value)
		{
			bool result = false;

			switch (p.DataType)
			{
				case SqlDbType.BigInt:
				long lresult = 0;
				return Int64.TryParse(value, out lresult);
				case SqlDbType.TinyInt:
				Byte bresult = 0; 
				return Byte.TryParse(value, out bresult);
				case SqlDbType.SmallInt:
				Int16 i16result = 0;
				return Int16.TryParse(value, out i16result);
				case SqlDbType.Int:
				int iresult = 0;
				return Int32.TryParse(value, out iresult);
				case SqlDbType.UniqueIdentifier:
				try
				{
					Guid gresult = new Guid(value);
					return true;
				}
				catch
				{
					return false;
				}
				case SqlDbType.Bit:
				bool boolresult = false;
				return Boolean.TryParse(value, out boolresult);
				case SqlDbType.SmallDateTime:
				case SqlDbType.DateTime:
				DateTime dtresult = DateTime.MinValue;
				return DateTime.TryParse(value, out dtresult);
				case SqlDbType.Decimal:
				Decimal decresult = 0;
				return Decimal.TryParse(value, out decresult);
                default:
                return true;
			}	
			return result;
		}

		#endregion


	}
}

