﻿using DevExpress.Utils;

namespace Logica.Reports.AdminTool.UserControls
{
    partial class ChartDesigner
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.uiChartControl = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.uiChartControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiChartControl
            // 
            this.uiChartControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiChartControl.Location = new System.Drawing.Point(0, 0);
            this.uiChartControl.Name = "uiChartControl";
            this.uiChartControl.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel1.LineVisibility = DefaultBoolean.True;
            this.uiChartControl.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.uiChartControl.Size = new System.Drawing.Size(763, 428);
            this.uiChartControl.TabIndex = 2;
            this.uiChartControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uiChartControl_MouseUp);
            // 
            // ChartDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiChartControl);
            this.Name = "ChartDesigner";
            this.Size = new System.Drawing.Size(763, 428);
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiChartControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraCharts.ChartControl uiChartControl;

    }
}
