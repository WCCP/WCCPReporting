﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Logica.Reports.AdminTool.Data;

namespace Logica.Reports.AdminTool.UserControls
{
	public partial class PlanTables : UserControl
	{
		private const string STR_COLUMN_NAME = "Column name";
		private const string STR_COLUMN_TYPE = "Column type";
		private const string STR_KPI_KEY = "Kpi key";
		private const string STR_ADD = "Add";
		private const string STR_SAVE = "Save";
		public CallProcedureNameChangesEventHandler OnCallProcedureNameChange;
		//string _devXGridLayoutXml = string.Empty;
		PropertyTable _propertyGridColumn;
		int _selectedPropertyIndex = -1;
		List<PropertyTable> _propertyColumnList;

		public List<PropertyTable> TableColumns
		{
			get
			{
				if (_propertyColumnList == null)
					_propertyColumnList = new List<PropertyTable>();
				return _propertyColumnList;
			}
			set
			{
				_propertyColumnList = value;
			}
		}

		public PlanTables()
		{
			InitializeComponent();
			_propertyColumnList = new List<PropertyTable>();
		}

		#region Private Methods

		private void RefreshListColumns()
		{
			lvColumns.Items.Clear();
			for (int i = 0; i < this.TableColumns.Count; i++)
			{
				ListViewItem lvi = new ListViewItem((string) this.TableColumns[i][STR_COLUMN_NAME]);
				lvi.Tag = this.TableColumns[i];
				lvColumns.Items.Add(lvi);
			}
		}

		private void CreateColumnInfo()
		{
			_propertyGridColumn = new PropertyTable();
			_propertyGridColumn.Properties.Add(new PropertySpec(STR_COLUMN_NAME, typeof(String), null, "Table column name", "column name"));
			_propertyGridColumn.Properties.Add(new PropertySpec(STR_COLUMN_TYPE, typeof(SqlDbType), null, "Column sql data type", SqlDbType.VarChar));
			_propertyGridColumn.Properties.Add(new PropertySpec(STR_KPI_KEY, typeof(FieldBoolean), null, "Primary key for Kpi`s table", FieldBoolean.False));

			_propertyGridColumn[STR_COLUMN_NAME] = "column";
			_propertyGridColumn[STR_COLUMN_TYPE] = SqlDbType.VarChar;
			_propertyGridColumn[STR_KPI_KEY] = FieldBoolean.False;
			
		}

		private void CreateNewPropertyColumn()
		{
			btnAddColumn.Text = STR_ADD;
			CreateColumnInfo();
			this.propertyGrid.SelectedObject = _propertyGridColumn;
		}

		#endregion

		#region Events

		private void btnAddColumn_Click(object sender, EventArgs e)
		{
			if (btnAddColumn.Text == STR_ADD)
			{
				TableColumns.Add(_propertyGridColumn);
				propertyGrid.SelectedObjects = this.TableColumns.ToArray();
			}
			else
			{
				this.TableColumns[_selectedPropertyIndex] = _propertyGridColumn;
			}
			RefreshListColumns();
			CreateNewPropertyColumn();
		}

		private void btnClearColumn_Click(object sender, EventArgs e)
		{
			CreateNewPropertyColumn();
		}

		private void lvColumns_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			btnAddColumn.Text = STR_SAVE;
			_selectedPropertyIndex = lvColumns.SelectedItems[0].Index;
			_propertyGridColumn = (PropertyTable) lvColumns.SelectedItems[0].Tag;
			this.propertyGrid.SelectedObject = _propertyGridColumn;
		}

		private void ucPlanTables_Load(object sender, EventArgs e)
		{
			CreateNewPropertyColumn();
		}

		private void btnDeleteColumn_Click(object sender, EventArgs e)
		{
			this.TableColumns.Remove((PropertyTable) lvColumns.SelectedItems[0].Tag);
			RefreshListColumns();
			CreateNewPropertyColumn();
		}

		private void btnUpdateDB_Click(object sender, EventArgs e)
		{
			if (tbxTableName.Text.Trim() == string.Empty)
			{
				MessageBox.Show("Set table name");
				tbxTableName.Focus();
				return;
			}

			if (this.TableColumns.Count == 0)
			{
				MessageBox.Show("Add atleast one column to the table");
				btnAddColumn.Focus();
				return;
			}
			bool hasKeyColumn = false;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if ((FieldBoolean) pt[STR_KPI_KEY] == FieldBoolean.True)
				{
					hasKeyColumn = true;
					break;
				}
			}

			if (!hasKeyColumn)
			{
				MessageBox.Show("Add atleast one column in the table should be Primary Key");
				btnAddColumn.Focus();
				return;
			}

			string table_identifier = string.Format("DW_{0}", tbxTableName.Text.Trim());

			#region Create Table
			
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(string.Format("CREATE TABLE [dbo].[{0}](", table_identifier));
			string colls = string.Empty;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if (colls != string.Empty)
					colls += ",";
				if (((SqlDbType) pt[STR_COLUMN_TYPE]) == SqlDbType.VarChar ||
					((SqlDbType) pt[STR_COLUMN_TYPE]) == SqlDbType.NVarChar)
				{
					colls += string.Format(" [{0}] {1} (max) NULL ", pt[STR_COLUMN_NAME], Convert.ToString(pt[STR_COLUMN_TYPE]).ToLower());
				}
				else
				colls += string.Format("[{0}] [{1}] {2}", pt[STR_COLUMN_NAME], Convert.ToString(pt[STR_COLUMN_TYPE]).ToLower(), ((FieldBoolean) pt[STR_KPI_KEY]) == FieldBoolean.True ? " NOT NULL " : " NULL ");
			}
			sb.AppendLine(string.Format(@"{0}) ON [PRIMARY]", colls));

			try
			{
                Logica.Reports.DataAccess.DataAccessLayer.ExecuteQuery(sb.ToString());
			}
			catch (SqlException exc)
			{
				MessageBox.Show(exc.Message);
				return;
			}

			#endregion

			#region Create Select procedure

			sb = new StringBuilder();
            sb.AppendLine(string.Format(" CREATE PROCEDURE [dbo].[sp{0}_GetList](", table_identifier));
			//start params
			colls = string.Empty;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if ((FieldBoolean) pt[STR_KPI_KEY] == FieldBoolean.True)
				{
					if (colls != string.Empty)
						colls += " , ";
					if (((SqlDbType) pt[STR_COLUMN_TYPE]) == SqlDbType.VarChar ||
						((SqlDbType) pt[STR_COLUMN_TYPE]) == SqlDbType.NVarChar)
					{
						colls += string.Format(" @{0} {1} (max) ", pt[STR_COLUMN_NAME], Convert.ToString(pt[STR_COLUMN_TYPE]).ToLower());
					}
					else
						colls += string.Format(" @{0} {1} ", pt[STR_COLUMN_NAME], Convert.ToString(pt[STR_COLUMN_TYPE]).ToLower());
			
				}
			}
			sb.AppendLine(string.Format("{0}) ", colls));
			//end params
			//start body
			sb.AppendLine(@" AS
							BEGIN
							SELECT");
			string colls_arr = string.Empty;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if (colls_arr != string.Empty)
					colls_arr += ",";
				colls_arr += string.Format("{0} ", pt[STR_COLUMN_NAME]);
			}
			sb.AppendLine(string.Format("{0}  FROM [dbo].[{1}] ", colls_arr, table_identifier));

			string where_claus = " WHERE ";

			//end body
			//start where
			colls = string.Empty;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if ((FieldBoolean) pt[STR_KPI_KEY] == FieldBoolean.True)
				{
					if (colls != string.Empty)
						colls += " AND ";
					colls += string.Format(" {0} =@{0}  ", pt[STR_COLUMN_NAME]);
				}
			}

			where_claus += colls;

			sb.AppendLine(string.Format(" {0} ", where_claus));
			//end where
			sb.AppendLine(@" END
	
							SET QUOTED_IDENTIFIER OFF");
			

			try
			{
                Logica.Reports.DataAccess.DataAccessLayer.ExecuteQuery(sb.ToString());
			}
			catch (SqlException exc)
			{
				MessageBox.Show(exc.Message);
				return;
			}

			#endregion

			#region Create Modify procedure

			sb = new StringBuilder();
			sb.AppendLine(string.Format(" CREATE PROCEDURE [dbo].[sp{0}_Put](", table_identifier));
			//start params
			colls = string.Empty;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if (colls != string.Empty)
						colls += " , ";
				if (((SqlDbType) pt[STR_COLUMN_TYPE]) == SqlDbType.VarChar ||
					((SqlDbType) pt[STR_COLUMN_TYPE]) == SqlDbType.NVarChar)
				{
					colls += string.Format(" @{0} {1} (max) ", pt[STR_COLUMN_NAME], Convert.ToString(pt[STR_COLUMN_TYPE]).ToLower());
				}
				else
					colls += string.Format(" @{0} {1} ", pt[STR_COLUMN_NAME], Convert.ToString(pt[STR_COLUMN_TYPE]).ToLower());
			}
			sb.AppendLine(string.Format(" {0} )", colls));
			//end params
			//start body
			//if exists
			sb.AppendLine(string.Format(@" AS
							BEGIN
							if EXISTS (SELECT * FROM [dbo].[{0}] {1} ) ", table_identifier, where_claus));

			sb.AppendLine(string.Format(@" BEGIN
							
							UPDATE [dbo].[{0}] 
							SET	", table_identifier));

			colls = string.Empty;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if ((FieldBoolean) pt[STR_KPI_KEY] == FieldBoolean.False)
				{
					if (colls != string.Empty)
						colls += " , ";
					colls += string.Format(" {0} =@{0}  ", pt[STR_COLUMN_NAME]);
				}
			}
			sb.AppendLine(string.Format(" {0} ", colls));

			sb.AppendLine(where_claus);
			//end if exists
			//else
			sb.AppendLine(string.Format(@" END 
							
							ELSE 
						
							BEGIN 
							
							INSERT INTO [dbo].[{0}] ( {1} ) VALUES ( ", table_identifier, colls_arr));

			colls = string.Empty;
			foreach (PropertyTable pt in this.TableColumns)
			{
				if (colls != string.Empty)
					colls += " , ";
				colls += string.Format("@{0} ", pt[STR_COLUMN_NAME]);
			}
			sb.AppendLine(string.Format("{0} )", colls));
			//end else
			//end body
			sb.AppendLine(@"END
	
							END");
			try
			{
                Logica.Reports.DataAccess.DataAccessLayer.ExecuteQuery(sb.ToString());
			}
			catch (SqlException exc)
			{
				MessageBox.Show(exc.Message);
				return;
			}
			MessageBox.Show("Plan table and stored procedures  created successful");
			#endregion

				if(OnCallProcedureNameChange!= null)
                    OnCallProcedureNameChange(sender, e, string.Format(@"sp{0}_GetList", table_identifier), string.Format(@"sp{0}_Put", table_identifier));
		}

		#endregion
	}
}
