﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.AdminTool.UserControls.Contracts;
using System.IO;
using DevExpress.Utils.Menu;
using DevExpress.XtraCharts.Wizard;

namespace Logica.Reports.AdminTool.UserControls
{
    /// <summary>
    /// Provides functionality to design DXChart layout to be saved in XML format.
    /// </summary>
    public partial class ChartDesigner : UserControl, ILayoutDesigner
    {
        #region Private members

        private ChartWizard _chartWizard;
        private const string xmlDefinitionTag = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

        #endregion // Private members

        #region Constructors

        public ChartDesigner()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        #endregion // Constructors

        #region ILayoutDesigner Members

        /// <summary>
        /// Sets the initial configuration for a designer control.
        /// </summary>
        /// <param name="dt">The datasource table.</param>
        /// <param name="xmlLayout">The XML layout.</param>
        public void Initialize(DataTable dt, string xmlLayout)
        {
            if (!string.IsNullOrEmpty(xmlLayout))
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.Write(xmlDefinitionTag + xmlLayout);
                        writer.Flush();
                        stream.Position = 0;
                        uiChartControl.LoadFromStream(stream);
                        uiChartControl.DataSource = dt;
                    }
                }
            }
            else
            {
                uiChartControl.DataSource = dt;
                ChartWizardShow(null, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Serializes the layout.
        /// </summary>
        /// <returns>Serialized layout.</returns>
        public string SerializeLayout()
        {
            string xmlLayout = string.Empty;
            using (MemoryStream stream = new MemoryStream())
            {
                uiChartControl.SaveToStream(stream);
                stream.Flush();
                stream.Position = 0;
                using (StreamReader reader = new StreamReader(stream))
                {
                    xmlLayout = reader.ReadToEnd();
                }
            }
            if (xmlLayout.Trim().StartsWith(xmlDefinitionTag))
            {
                xmlLayout = xmlLayout.Remove(0, xmlDefinitionTag.Length + 2);
            }
           
            return xmlLayout;
        }

        #endregion

        #region Event handlers

        private void uiChartControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DXPopupMenu menu = new DXPopupMenu();
                menu.Items.Add(new DXMenuItem("Setup chart", ChartWizardShow));
                MenuManagerHelper.ShowMenu(menu
                    , new DevExpress.LookAndFeel.UserLookAndFeel(uiChartControl)
                    , MenuManagerHelper.Standard
                    , uiChartControl
                    , e.Location);
            }
        }

        private void ChartWizardShow(object sender, EventArgs e)
        {
            if (_chartWizard == null)
            {
                _chartWizard = new ChartWizard(uiChartControl);
            }
            _chartWizard.ShowDialog();
        }

        #endregion // Event handlers
    }
}
