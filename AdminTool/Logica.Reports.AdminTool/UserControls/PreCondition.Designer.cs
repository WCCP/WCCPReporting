﻿namespace Logica.Reports.AdminTool.UserControls
{
	partial class PreCondition
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
			this.xtraTbCPreInit = new DevExpress.XtraTab.XtraTabControl();
			this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
			this.sqlProcParamsPreInit = new Logica.Reports.AdminTool.UserControls.SqlProcParams();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.bar1 = new DevExpress.XtraBars.Bar();
			this.bBAddCondition = new DevExpress.XtraBars.BarButtonItem();
			this.bBDeleteCondition = new DevExpress.XtraBars.BarButtonItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
			this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
			((System.ComponentModel.ISupportInitialize) (this.xtraTbCPreInit)).BeginInit();
			this.xtraTbCPreInit.SuspendLayout();
			this.xtraTabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this.barManager1)).BeginInit();
			this.SuspendLayout();
			// 
			// standaloneBarDockControl2
			// 
			this.standaloneBarDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
			this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
			this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
			this.standaloneBarDockControl2.Size = new System.Drawing.Size(556, 33);
			this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
			// 
			// xtraTbCPreInit
			// 
			this.xtraTbCPreInit.Location = new System.Drawing.Point(0, 49);
			this.xtraTbCPreInit.Name = "xtraTbCPreInit";
			this.xtraTbCPreInit.SelectedTabPage = this.xtraTabPage2;
			this.xtraTbCPreInit.Size = new System.Drawing.Size(557, 313);
			this.xtraTbCPreInit.TabIndex = 3;
			this.xtraTbCPreInit.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2});
			// 
			// xtraTabPage2
			// 
			this.xtraTabPage2.Controls.Add(this.sqlProcParamsPreInit);
			this.xtraTabPage2.Name = "xtraTabPage2";
			this.xtraTabPage2.Size = new System.Drawing.Size(550, 284);
			this.xtraTabPage2.Text = "Condition 1";
			// 
			// sqlProcParamsPreInit
			// 
			this.sqlProcParamsPreInit.Dock = System.Windows.Forms.DockStyle.Fill;
			this.sqlProcParamsPreInit.Location = new System.Drawing.Point(0, 0);
			this.sqlProcParamsPreInit.Name = "sqlProcParamsPreInit";
			this.sqlProcParamsPreInit.PropertyValueChanged = false;
			this.sqlProcParamsPreInit.Size = new System.Drawing.Size(550, 284);
			this.sqlProcParamsPreInit.TabIndex = 0;
			// 
			// barManager1
			// 
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem7,
            this.barButtonItem8,
            this.bBAddCondition,
            this.bBDeleteCondition});
			this.barManager1.MaxItemId = 12;
			// 
			// bar1
			// 
			this.bar1.BarName = "PreInit";
			this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
			this.bar1.DockCol = 0;
			this.bar1.DockRow = 0;
			this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
			this.bar1.FloatLocation = new System.Drawing.Point(53, 180);
			this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bBAddCondition),
            new DevExpress.XtraBars.LinkPersistInfo(this.bBDeleteCondition)});
			this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl2;
			this.bar1.Text = "PreInit";
			// 
			// bBAddCondition
			// 
			this.bBAddCondition.Caption = "Add Condition";
			this.bBAddCondition.Id = 10;
			this.bBAddCondition.Name = "bBAddCondition";
			this.bBAddCondition.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bBAddCondition_ItemClick);
			// 
			// bBDeleteCondition
			// 
			this.bBDeleteCondition.Caption = "Delete Condition";
			this.bBDeleteCondition.Id = 11;
			this.bBDeleteCondition.Name = "bBDeleteCondition";
			this.bBDeleteCondition.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bBDeleteCondition_ItemClick);
			// 
			// barButtonItem7
			// 
			this.barButtonItem7.Caption = "Add Condition";
			this.barButtonItem7.Id = 6;
			this.barButtonItem7.Name = "barButtonItem7";
			// 
			// barButtonItem8
			// 
			this.barButtonItem8.Caption = "Delete Condition";
			this.barButtonItem8.Enabled = false;
			this.barButtonItem8.Id = 7;
			this.barButtonItem8.Name = "barButtonItem8";
			// 
			// PreCondition
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.standaloneBarDockControl2);
			this.Controls.Add(this.xtraTbCPreInit);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "PreCondition";
			this.Size = new System.Drawing.Size(556, 370);
			this.VisibleChanged += new System.EventHandler(this.PreCondition_VisibleChanged);
			((System.ComponentModel.ISupportInitialize) (this.xtraTbCPreInit)).EndInit();
			this.xtraTbCPreInit.ResumeLayout(false);
			this.xtraTabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize) (this.barManager1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
		private DevExpress.XtraBars.Bar bar1;
		private DevExpress.XtraTab.XtraTabControl xtraTbCPreInit;
		private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
		private SqlProcParams sqlProcParamsPreInit;
		private DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.BarButtonItem barButtonItem7;
		private DevExpress.XtraBars.BarButtonItem barButtonItem8;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraBars.BarButtonItem bBAddCondition;
		private DevExpress.XtraBars.BarButtonItem bBDeleteCondition;
	}
}
