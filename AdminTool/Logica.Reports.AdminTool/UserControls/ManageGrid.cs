﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Logica.Reports.AdminTool.Data;
using Logica.Reports.AdminTool.Forms;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;

namespace Logica.Reports.AdminTool.UserControls
{
    public partial class ManageGrid : UserControl
    {
        private const string MSG_SET_STORED_PROCEDURE_AND_REFRESH = "Selected the stored procedure name and press 'Refresh'.";
        SetProcedureNameEventHandler OnSetSelectProcedure;
        SetProcedureNameEventHandler OnSetModifyProcedure;

        public CallActionRefreshEventHandler OnActionRised;

        #region	 Fields

        int _tabOrder;
        string _tableHeader = string.Empty;
        string _tableChart = string.Empty;
        string _tableGrid = string.Empty;
        Guid _tableIdentifier;
        DataTable testTable = null;
        TableType _tableType;
        Table _currentTable;

        public Table CurrentTable
        {
            get { return _currentTable; }
            set { _currentTable = value; }
        }

        public TableType CurrentTableType
        {
            get
            {
                return _tableType;
            }
            set
            {
                _tableType = value;
            }
        }


        public Guid TableIdentifier
        {
            get
            {
                return _tableIdentifier;
            }
            set
            {
                _tableIdentifier = value;
            }
        }

        #endregion

        #region Public Methods

        public ManageGrid()
        {
            InitializeComponent();
            ucSelectSqlProc.CurrentQueryType = SQLQueryType.DataSet;
            ucModifySqlProc.CurrentQueryType = SQLQueryType.NonQuery;
            OnSetSelectProcedure = new SetProcedureNameEventHandler(ucSelectSqlProc.OnSetProcedureName);
            OnSetModifyProcedure = new SetProcedureNameEventHandler(ucModifySqlProc.OnSetProcedureName);
            ucModifySqlProc.OnRefreshProcedureName += new SetProcedureNameChangedEventHandler(this.OnProcedureNameRefresh);
            ucSelectSqlProc.OnRefreshProcedureName += new SetProcedureNameChangedEventHandler(this.OnProcedureNameRefresh);
            ucPlanTables1.OnCallProcedureNameChange = new CallProcedureNameChangesEventHandler(this.OnCallProcedureNameChange);
        }

        public void OnCallProcedureNameChange(object sender, EventArgs e, string procSelectName, string procModifyName)
        {
            OnSetSelectProcedure(sender, e, procSelectName, SQLQueryType.DataSet);
            OnSetModifyProcedure(sender, e, procModifyName, SQLQueryType.NonQuery);
        }

        public void HideUnusedTabs(TableType tableType)
        {
            if (tableType != TableType.Plan)
            {
                xtbTables.TabPages.Remove(xtraModifyP);
                xtbTables.TabPages.Remove(xtraCreateTable);
            }
            else
            {

                xtbTables.TabPages.Remove(xtraChart);
            }
        }

        public void SetInitData(Table currentTable)
        {
            WaitManager.StartWait();
            _tableIdentifier = currentTable.Id;
            _tabOrder = currentTable.TabIndex;
            _tableType = currentTable.Type;
            tbxHeader.Text = _tableHeader = currentTable.Header;
            uiCbxHideHeader.Checked = currentTable.HideHeader;
            cbPosition.SelectedItem = currentTable.Position.ToString();
            cbGridPosition.SelectedItem = currentTable.GridPosition.ToString();
            cbDisplay.SelectedItem = currentTable.Display.ToString();
            if (currentTable.Body != null)
            {
                _tableGrid = currentTable.Body.GridXml; //.Replace("   <", "<").Replace("  <", "<").Replace(" <", "<");
                _tableChart = currentTable.Body.ChartXml; //.Replace("   <", "<").Replace("  <", "<").Replace(" <", "<");
            }
            if (_tableType == TableType.Plan && currentTable.Queries != null)
            {
                xtbTables.TabPages.Remove(xtraCreateTable);
            }
            tbxTabOrderTable.Text = _tabOrder.ToString();
            cbPosition.SelectedText = currentTable.Position.ToString();
            cbGridPosition.SelectedText = currentTable.GridPosition.ToString();
            switch (currentTable.Mode)
            {
                case ShowMode.GridChart:
                    rbChartAndGrid.Checked = true;
                    break;
                case ShowMode.Chart:
                    rbOnlyChart.Checked = true;
                    break;
                case ShowMode.Invisible:
                    rbUseForCalculation.Checked = true;
                    break;
                default:
                    rbNone.Checked = true;
                    break;
            }
            if (currentTable.Queries != null)
            {
                SetupQueryReport(currentTable.Queries);
            }
            WaitManager.StopWait();
        }

        #endregion

        #region Events

        private void OnProcedureNameRefresh(object sender, EventArgs e)
        {
            testTable = null;
            testTable = ReportProxyObject.GetResultSetTable();
            if (OnActionRised != null)
            {
                OnActionRised(sender, e);
            }
        }

        private void tbxHeader_Leave(object sender, EventArgs e)
        {
            if (!tbxHeader.Text.Trim().Equals(_tableHeader) && !string.IsNullOrEmpty(tbxHeader.Text.Trim()))
            {
                _tableHeader = tbxHeader.Text.Trim();
                if (!string.IsNullOrEmpty(_tableHeader.Trim()))
                {
                    ReportProxyObject.SetSheetTableHeader(_tableType, _tableHeader, _tableIdentifier);
                }
            }
        }

        private void uiCbxHideHeader_CheckedChanged(object sender, EventArgs e)
        {
            ReportProxyObject.SetTableHideHeader(uiCbxHideHeader.Checked, _tableIdentifier, _tableType);
        }

        private void btnShowInGrid_Click(object sender, EventArgs e)
        {
            if (testTable == null || ucSelectSqlProc.PropertyValueChanged)
            {
                testTable = ReportProxyObject.GetResultSetTable();
            }
            using (GridConfiguration frm = new GridConfiguration(testTable, _tableGrid, ComponentDesignMode.Grid))
            {
                try
                {
                    if (frm.ShowDialog(this.FindForm()) == DialogResult.OK)
                    {
                        _tableGrid = frm.XmlConfiguration;
                        ReportProxyObject.SetTableGridXml(_tableIdentifier, _tableGrid);
                    }
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void tbxTabOrderTable_Leave(object sender, EventArgs e)
        {
            int val = -1;
            int.TryParse(tbxTabOrderTable.Text.Trim(), out val);
            if (val != -1)
            {
                ReportProxyObject.SetTableTabIndex(_tableIdentifier, val);
            }
        }

        private void uiBtnGridPreview_Click(object sender, EventArgs e)
        {
            if (testTable == null || ucSelectSqlProc.PropertyValueChanged)
            {
                testTable = ReportProxyObject.GetResultSetTable();
            }

            GridPreview form = new GridPreview(testTable, _tableGrid);
            form.Show(this);
        }

        private void btnChartSetup_Click(object sender, EventArgs e)
        {
            if (testTable == null || ucSelectSqlProc.PropertyValueChanged)
            {
                testTable = ReportProxyObject.GetResultSetTable();
            }

            using (GridConfiguration frm = new GridConfiguration(testTable, _tableChart.Replace("><", "> <"), ComponentDesignMode.Chart))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    _tableChart = frm.XmlConfiguration;
                    ReportProxyObject.SetTableChartXml(_tableIdentifier, _tableChart);
                }
            }
        }

        private void rbMode_CheckedChanged(object sender, EventArgs e)
        {
            ShowMode showMode = (ShowMode)Enum.Parse(typeof(ShowMode), (string)((RadioButton)sender).Tag);
            ReportProxyObject.SetTableShowMode(_tableIdentifier, showMode);

            btnShowInGrid.Enabled = false;
            uiBtnGridPreview.Enabled = false;
            cbGridPosition.Enabled = false;
            btnChartSetup.Enabled = false;
            cbPosition.Enabled = false;

            switch (showMode)
            {
                case ShowMode.Grid:
                    btnShowInGrid.Enabled =
                        uiBtnGridPreview.Enabled =
                        cbGridPosition.Enabled = true;
                    break;
                case ShowMode.Chart:
                    btnChartSetup.Enabled =
                        cbPosition.Enabled = true;
                    break;
                case ShowMode.GridChart:
                    btnShowInGrid.Enabled =
                        uiBtnGridPreview.Enabled =
                        cbGridPosition.Enabled =
                        btnChartSetup.Enabled =
                        cbPosition.Enabled = true;
                    break;
            }
        }

        private void ManageGrid_VisibleChanged(object sender, EventArgs e)
        {
            if (!ucSelectSqlProc.IsTableQuery)
            {
                ucModifySqlProc.IsTableQuery = true;
                ucSelectSqlProc.IsTableQuery = true;
            }
        }

        private void cbPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportProxyObject.SetTableChartPosition(_tableIdentifier, (ChartPosition)Enum.Parse(typeof(ChartPosition), (string)cbPosition.SelectedItem));
        }

        #endregion

        #region Private Methods

        private void SetupQueryReport(Logica.Reports.ConfigXmlParser.Model.Query[] queries)
        {
            for (int queryIndex = 0; queryIndex < queries.Length; queryIndex++)
            {
                Logica.Reports.ConfigXmlParser.Model.Query currentQuery = queries[queryIndex];
                if (!string.IsNullOrEmpty(currentQuery.Name))
                {
                    if (currentQuery.SqlQueryType == SQLQueryType.DataSet)
                    {
                        ucSelectSqlProc.CurrentQuery = currentQuery;
                        ucSelectSqlProc.IsTableQuery = true;
                        ucSelectSqlProc.SetupInitData();
                        if (!string.IsNullOrEmpty(currentQuery.Name))
                            OnProcedureNameRefresh(this, null);
                    }
                    else
                    {
                        ucModifySqlProc.CurrentQuery = currentQuery;
                        ucModifySqlProc.IsTableQuery = true;
                        ucModifySqlProc.SetupInitData();
                    }
                }
            }
        }

        private void cbDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportProxyObject.SetTableDisplay(_tableIdentifier, (DisplayType)Enum.Parse(typeof(DisplayType), (string)cbDisplay.SelectedItem));
        }

        private void ManageGrid_Load(object sender, EventArgs e)
        {
            if (CurrentTable != null)
            {
                SetInitData(CurrentTable);
            }
        }

        #endregion

        private void cbGridPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportProxyObject.SetTableGridPosition(_tableIdentifier, (GridPosition)Enum.Parse(typeof(GridPosition), (string)cbGridPosition.SelectedItem));
        }

    }
}
