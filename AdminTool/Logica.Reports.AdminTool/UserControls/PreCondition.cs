﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTab;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.AdminTool.Data;
using System.Data.SqlClient;

namespace Logica.Reports.AdminTool.UserControls
{
	public partial class PreCondition : UserControl
	{
		#region Fields

		private PreconditionMode _mode;

		private Init _currentInit;

		public Init CurrentInit
		{
			get
			{
				return _currentInit;
			}
			set
			{
				_currentInit = value;
			}
		}

		public PreconditionMode ConditionMode
		{
			get
			{
				return _mode;
			}
			set
			{
				_mode = value;
			}
		}

		public string SQLQueryType
		{
			get
			{
				return _mode.ToString().ToLower();
			}
		}

		#endregion

		public PreCondition()
		{
			InitializeComponent();
		}

		public void SetupPreConditions()
		{
			ReportProxyObject.InitConditionMode = ConditionMode;
			CurrentInit = ReportProxyObject.CurrentInit;
			ReportProxyObject.InitTabs = xtraTbCPreInit;
			ReportProxyObject.FillPreConditions();
			bBDeleteCondition.Enabled = CurrentInit.Queries.Length > 1;
		}

		private void bBAddCondition_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			ReportProxyObject.AddCondition(ConditionMode);
			if (ReportProxyObject.InitTabs.TabPages.Count > 1)
				bBDeleteCondition.Enabled = true;
		}

		private void bBDeleteCondition_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			ReportProxyObject.DeleteCondition();
			if (ReportProxyObject.InitTabs.TabPages.Count > 1)
				bBDeleteCondition.Enabled = false;
		}

		private void PreCondition_VisibleChanged(object sender, EventArgs e)
		{
			ReportProxyObject.CurrentInit = CurrentInit;
			ReportProxyObject.InitTabs = xtraTbCPreInit;
			ReportProxyObject.InitConditionMode = ConditionMode;
		}
	}
}
