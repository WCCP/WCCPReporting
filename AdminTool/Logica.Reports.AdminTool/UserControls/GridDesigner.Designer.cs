﻿namespace Logica.Reports.AdminTool.UserControls
{
    partial class GridDesigner
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridDesigner));
            this.uiPgProperties = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.uiPnlEditorContainer = new System.Windows.Forms.Panel();
            this.uiGrid = new DevExpress.XtraGrid.GridControl();
            this.uiGvMainView = new Logica.Reports.ConfigXmlParser.GridExtension.GroupBandedView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.uiPnlListContainer = new System.Windows.Forms.Panel();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.uiTsToolBox = new System.Windows.Forms.ToolStrip();
            this.uiTbtAdd = new System.Windows.Forms.ToolStripButton();
            this.uiTbtDelete = new System.Windows.Forms.ToolStripButton();
            this.uiTbtGroup = new System.Windows.Forms.ToolStripButton();
            this.uiTbtEditStyleCondition = new System.Windows.Forms.ToolStripButton();
            this.uiTbtShowViewEditor = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButtonUtils = new System.Windows.Forms.ToolStripDropDownButton();
            this.названияКолонокВБэндыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.названияБэндовВКолонкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.применитьСтильВГруппеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.uiPgProperties)).BeginInit();
            this.uiPnlEditorContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGvMainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.uiTsToolBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPgProperties
            // 
            this.uiPgProperties.AutoGenerateRows = true;
            this.uiPgProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPgProperties.Location = new System.Drawing.Point(0, 0);
            this.uiPgProperties.Name = "uiPgProperties";
            this.uiPgProperties.Size = new System.Drawing.Size(314, 333);
            this.uiPgProperties.TabIndex = 1;
            // 
            // uiPnlEditorContainer
            // 
            this.uiPnlEditorContainer.Controls.Add(this.uiPgProperties);
            this.uiPnlEditorContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPnlEditorContainer.Location = new System.Drawing.Point(154, 0);
            this.uiPnlEditorContainer.Name = "uiPnlEditorContainer";
            this.uiPnlEditorContainer.Size = new System.Drawing.Size(314, 333);
            this.uiPnlEditorContainer.TabIndex = 3;
            // 
            // uiGrid
            // 
            this.uiGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGrid.Location = new System.Drawing.Point(0, 0);
            this.uiGrid.MainView = this.uiGvMainView;
            this.uiGrid.Name = "uiGrid";
            this.uiGrid.Size = new System.Drawing.Size(409, 333);
            this.uiGrid.TabIndex = 0;
            this.uiGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.uiGvMainView,
            this.gridView1});
            // 
            // uiGvMainView
            // 
            this.uiGvMainView.GridControl = this.uiGrid;
            this.uiGvMainView.Name = "uiGvMainView";
            this.uiGvMainView.OptionsBehavior.AutoExpandAllGroups = true;
            this.uiGvMainView.OptionsBehavior.AutoPopulateColumns = false;
            this.uiGvMainView.OptionsBehavior.SummariesIgnoreNullValues = true;
            this.uiGvMainView.OptionsCustomization.AllowChangeBandParent = true;
            this.uiGvMainView.OptionsCustomization.AllowRowSizing = true;
            this.uiGvMainView.OptionsMenu.EnableColumnMenu = false;
            this.uiGvMainView.OptionsView.ShowFooter = true;
            this.uiGvMainView.OptionsView.ShowGroupPanel = false;
            this.uiGvMainView.OptionsView.ShowIndicator = false;
            this.uiGvMainView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uiGvMainView_MouseDown);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.uiGrid;
            this.gridView1.Name = "gridView1";
            // 
            // uiPnlListContainer
            // 
            this.uiPnlListContainer.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiPnlListContainer.Location = new System.Drawing.Point(0, 0);
            this.uiPnlListContainer.Name = "uiPnlListContainer";
            this.uiPnlListContainer.Size = new System.Drawing.Size(154, 333);
            this.uiPnlListContainer.TabIndex = 4;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 25);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.uiGrid);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.uiPnlEditorContainer);
            this.splitContainerControl1.Panel2.Controls.Add(this.uiPnlListContainer);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(883, 333);
            this.splitContainerControl1.SplitterPosition = 468;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // uiTsToolBox
            // 
            this.uiTsToolBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uiTbtAdd,
            this.uiTbtDelete,
            this.uiTbtGroup,
            this.uiTbtEditStyleCondition,
            this.uiTbtShowViewEditor,
            this.toolStripDropDownButtonUtils});
            this.uiTsToolBox.Location = new System.Drawing.Point(0, 0);
            this.uiTsToolBox.Name = "uiTsToolBox";
            this.uiTsToolBox.Size = new System.Drawing.Size(883, 25);
            this.uiTsToolBox.TabIndex = 6;
            this.uiTsToolBox.Text = "Tools";
            // 
            // uiTbtAdd
            // 
            this.uiTbtAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.uiTbtAdd.Image = ((System.Drawing.Image)(resources.GetObject("uiTbtAdd.Image")));
            this.uiTbtAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiTbtAdd.Name = "uiTbtAdd";
            this.uiTbtAdd.Size = new System.Drawing.Size(54, 22);
            this.uiTbtAdd.Text = "Создать";
            this.uiTbtAdd.Click += new System.EventHandler(this.uiTbtAdd_Click);
            // 
            // uiTbtDelete
            // 
            this.uiTbtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.uiTbtDelete.Image = ((System.Drawing.Image)(resources.GetObject("uiTbtDelete.Image")));
            this.uiTbtDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiTbtDelete.Name = "uiTbtDelete";
            this.uiTbtDelete.Size = new System.Drawing.Size(55, 22);
            this.uiTbtDelete.Text = "Удалить";
            this.uiTbtDelete.Click += new System.EventHandler(this.uiTbtDelete_Click);
            // 
            // uiTbtGroup
            // 
            this.uiTbtGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.uiTbtGroup.Image = ((System.Drawing.Image)(resources.GetObject("uiTbtGroup.Image")));
            this.uiTbtGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiTbtGroup.Name = "uiTbtGroup";
            this.uiTbtGroup.Size = new System.Drawing.Size(88, 22);
            this.uiTbtGroup.Text = "Группировать";
            this.uiTbtGroup.Click += new System.EventHandler(this.uiTbtGroup_Click);
            // 
            // uiTbtEditStyleCondition
            // 
            this.uiTbtEditStyleCondition.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.uiTbtEditStyleCondition.Image = ((System.Drawing.Image)(resources.GetObject("uiTbtEditStyleCondition.Image")));
            this.uiTbtEditStyleCondition.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiTbtEditStyleCondition.Name = "uiTbtEditStyleCondition";
            this.uiTbtEditStyleCondition.Size = new System.Drawing.Size(117, 22);
            this.uiTbtEditStyleCondition.Text = "Условия раскраски";
            this.uiTbtEditStyleCondition.Click += new System.EventHandler(this.uiTbtEditStyleCondition_Click);
            // 
            // uiTbtShowViewEditor
            // 
            this.uiTbtShowViewEditor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.uiTbtShowViewEditor.Image = ((System.Drawing.Image)(resources.GetObject("uiTbtShowViewEditor.Image")));
            this.uiTbtShowViewEditor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiTbtShowViewEditor.Name = "uiTbtShowViewEditor";
            this.uiTbtShowViewEditor.Size = new System.Drawing.Size(156, 22);
            this.uiTbtShowViewEditor.Text = "Настройки представления";
            this.uiTbtShowViewEditor.Click += new System.EventHandler(this.uiTbtShowViewEditor_Click);
            // 
            // toolStripDropDownButtonUtils
            // 
            this.toolStripDropDownButtonUtils.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButtonUtils.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.названияКолонокВБэндыToolStripMenuItem,
            this.названияБэндовВКолонкиToolStripMenuItem,
            this.применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem,
            this.применитьСтильВГруппеToolStripMenuItem});
            this.toolStripDropDownButtonUtils.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButtonUtils.Image")));
            this.toolStripDropDownButtonUtils.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButtonUtils.Name = "toolStripDropDownButtonUtils";
            this.toolStripDropDownButtonUtils.Size = new System.Drawing.Size(67, 22);
            this.toolStripDropDownButtonUtils.Text = "Утилиты";
            // 
            // названияКолонокВБэндыToolStripMenuItem
            // 
            this.названияКолонокВБэндыToolStripMenuItem.Name = "названияКолонокВБэндыToolStripMenuItem";
            this.названияКолонокВБэндыToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.названияКолонокВБэндыToolStripMenuItem.Text = "Названия колонок в бэнды";
            this.названияКолонокВБэндыToolStripMenuItem.Click += new System.EventHandler(this.названияКолонокВБэндыToolStripMenuItem_Click);
            // 
            // названияБэндовВКолонкиToolStripMenuItem
            // 
            this.названияБэндовВКолонкиToolStripMenuItem.Name = "названияБэндовВКолонкиToolStripMenuItem";
            this.названияБэндовВКолонкиToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.названияБэндовВКолонкиToolStripMenuItem.Text = "Названия бэндов в колонки";
            this.названияБэндовВКолонкиToolStripMenuItem.Click += new System.EventHandler(this.названияБэндовВКолонкиToolStripMenuItem_Click);
            // 
            // применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem
            // 
            this.применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem.Name = "применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem";
            this.применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem.Text = "Применить вертикальность в группе";
            this.применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem.Click += new System.EventHandler(this.применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem_Click);
            // 
            // применитьСтильВГруппеToolStripMenuItem
            // 
            this.применитьСтильВГруппеToolStripMenuItem.Name = "применитьСтильВГруппеToolStripMenuItem";
            this.применитьСтильВГруппеToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.применитьСтильВГруппеToolStripMenuItem.Text = "Применить стиль заголовка в группе";
            this.применитьСтильВГруппеToolStripMenuItem.Click += new System.EventHandler(this.применитьСтильВГруппеToolStripMenuItem_Click);
            // 
            // GridDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.uiTsToolBox);
            this.Name = "GridDesigner";
            this.Size = new System.Drawing.Size(883, 358);
            ((System.ComponentModel.ISupportInitialize)(this.uiPgProperties)).EndInit();
            this.uiPnlEditorContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGvMainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.uiTsToolBox.ResumeLayout(false);
            this.uiTsToolBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraVerticalGrid.PropertyGridControl uiPgProperties;
        private System.Windows.Forms.Panel uiPnlEditorContainer;
        private DevExpress.XtraGrid.GridControl uiGrid;
        private Logica.Reports.ConfigXmlParser.GridExtension.GroupBandedView uiGvMainView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel uiPnlListContainer;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.ToolStrip uiTsToolBox;
        private System.Windows.Forms.ToolStripButton uiTbtAdd;
        private System.Windows.Forms.ToolStripButton uiTbtDelete;
        private System.Windows.Forms.ToolStripButton uiTbtGroup;
        private System.Windows.Forms.ToolStripButton uiTbtEditStyleCondition;
        private System.Windows.Forms.ToolStripButton uiTbtShowViewEditor;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButtonUtils;
        private System.Windows.Forms.ToolStripMenuItem названияКолонокВБэндыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem названияБэндовВКолонкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem применитьВертикальностьКоВсемЭлементамГруппыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem применитьСтильВГруппеToolStripMenuItem;
    }
}
