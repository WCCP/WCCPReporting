﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Logica.Reports.AdminTool.Forms
{
    public partial class ReportConfiguration : Form
    {
        public ReportConfiguration()
        {
            InitializeComponent();
            spnPrintingMargin.Properties.MaxValue = int.MaxValue;
        }

        internal int PrintingMargin
        {
            get
            {
                int result;
                try
                {
                    result = Convert.ToInt32(spnPrintingMargin.EditValue);
                }
                catch (Exception)
                {
                    result = 120;
                }
                return result;
            }
            set { spnPrintingMargin.EditValue = value; }
        }
    }
}
