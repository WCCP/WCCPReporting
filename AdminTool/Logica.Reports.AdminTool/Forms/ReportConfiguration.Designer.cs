﻿namespace Logica.Reports.AdminTool.Forms
{
    partial class ReportConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPrintingMargin = new DevExpress.XtraEditors.LabelControl();
            this.spnPrintingMargin = new DevExpress.XtraEditors.SpinEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.spnPrintingMargin.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPrintingMargin
            // 
            this.lblPrintingMargin.Location = new System.Drawing.Point(12, 15);
            this.lblPrintingMargin.Name = "lblPrintingMargin";
            this.lblPrintingMargin.Size = new System.Drawing.Size(144, 13);
            this.lblPrintingMargin.TabIndex = 0;
            this.lblPrintingMargin.Text = "Printing magrins (120=~1cm):";
            // 
            // spnPrintingMargin
            // 
            this.spnPrintingMargin.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnPrintingMargin.Location = new System.Drawing.Point(162, 12);
            this.spnPrintingMargin.Name = "spnPrintingMargin";
            this.spnPrintingMargin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spnPrintingMargin.Properties.IsFloatValue = false;
            this.spnPrintingMargin.Properties.Mask.EditMask = "N00";
            this.spnPrintingMargin.Size = new System.Drawing.Size(100, 20);
            this.spnPrintingMargin.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(124, 76);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(205, 76);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            // 
            // ReportConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 111);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.spnPrintingMargin);
            this.Controls.Add(this.lblPrintingMargin);
            this.MaximumSize = new System.Drawing.Size(300, 145);
            this.MinimumSize = new System.Drawing.Size(300, 145);
            this.Name = "ReportConfiguration";
            this.Text = "ReportConfiguration";
            ((System.ComponentModel.ISupportInitialize)(this.spnPrintingMargin.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblPrintingMargin;
        private DevExpress.XtraEditors.SpinEdit spnPrintingMargin;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;

    }
}