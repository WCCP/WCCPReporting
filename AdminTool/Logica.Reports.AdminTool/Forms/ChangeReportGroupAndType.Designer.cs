﻿namespace Logica.Reports.AdminTool.Forms
{
	partial class ChangeReportGroupAndType
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbGroup = new System.Windows.Forms.GroupBox();
			this.cbGroup = new System.Windows.Forms.ComboBox();
			this.gbType = new System.Windows.Forms.GroupBox();
			this.cbType = new System.Windows.Forms.ComboBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblReportName = new System.Windows.Forms.Label();
			this.gbGroup.SuspendLayout();
			this.gbType.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbGroup
			// 
			this.gbGroup.Controls.Add(this.cbGroup);
			this.gbGroup.Location = new System.Drawing.Point(12, 32);
			this.gbGroup.Name = "gbGroup";
			this.gbGroup.Size = new System.Drawing.Size(334, 59);
			this.gbGroup.TabIndex = 0;
			this.gbGroup.TabStop = false;
			this.gbGroup.Text = "Group";
			// 
			// cbGroup
			// 
			this.cbGroup.FormattingEnabled = true;
			this.cbGroup.Location = new System.Drawing.Point(6, 25);
			this.cbGroup.Name = "cbGroup";
			this.cbGroup.Size = new System.Drawing.Size(322, 21);
			this.cbGroup.TabIndex = 0;
			// 
			// gbType
			// 
			this.gbType.Controls.Add(this.cbType);
			this.gbType.Location = new System.Drawing.Point(12, 97);
			this.gbType.Name = "gbType";
			this.gbType.Size = new System.Drawing.Size(334, 61);
			this.gbType.TabIndex = 1;
			this.gbType.TabStop = false;
			this.gbType.Text = "Type";
			// 
			// cbType
			// 
			this.cbType.FormattingEnabled = true;
			this.cbType.Location = new System.Drawing.Point(6, 19);
			this.cbType.Name = "cbType";
			this.cbType.Size = new System.Drawing.Size(322, 21);
			this.cbType.TabIndex = 0;
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(176, 170);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 2;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(271, 170);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// lblReportName
			// 
			this.lblReportName.AutoSize = true;
			this.lblReportName.Location = new System.Drawing.Point(14, 13);
			this.lblReportName.Name = "lblReportName";
			this.lblReportName.Size = new System.Drawing.Size(73, 13);
			this.lblReportName.TabIndex = 4;
			this.lblReportName.Text = "[ReportName]";
			// 
			// ChangeReportGroupAndType
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(361, 206);
			this.Controls.Add(this.lblReportName);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.gbType);
			this.Controls.Add(this.gbGroup);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ChangeReportGroupAndType";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Set Report Group and Type";
			this.Load += new System.EventHandler(this.ChangeReportGroupAndType_Load);
			this.gbGroup.ResumeLayout(false);
			this.gbType.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox gbGroup;
		private System.Windows.Forms.GroupBox gbType;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblReportName;
		private System.Windows.Forms.ComboBox cbGroup;
		private System.Windows.Forms.ComboBox cbType;
	}
}