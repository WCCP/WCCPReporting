﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Logica.Reports.AdminTool.Data;
using Logica.Reports.DataAccess;
using System.Reflection;

namespace Logica.Reports.AdminTool.Forms
{
	public partial class ReportUpload : Form
	{
		byte[] _assembly = new byte[0];
		
		string _config = string.Empty;
		string _assemblyName = string.Empty;
		string _assemblyVersion = string.Empty;
		string _assemblyHeader = string.Empty;
		string _reportHeader = string.Empty;
		public ReportUpload()
		{
			InitializeComponent();
		}

		private void btnUploadAssembly_Click(object sender, EventArgs e)
		{
			if (fdoFile.ShowDialog() == DialogResult.OK)
			{
				FileStream myStream;
				if ((myStream = (FileStream) fdoFile.OpenFile()) != null)
				{
					_assembly = AdminToolCommon.GetBytes(myStream);
					lblAssembly.Text = _assemblyName = fdoFile.SafeFileName;
					try
					{
						Assembly assem = Assembly.Load(_assembly);
						string[] res = assem.FullName.Split(',');
						_assemblyVersion = res[1].Replace("Version=", "").Trim();
					}
					catch(BadImageFormatException)
					{
						_assemblyVersion = "0.0.0.0";
					}
				}
			}
		}


		private void btnUploadConfig_Click(object sender, EventArgs e)
		{
			if (fdoFile.ShowDialog() == DialogResult.OK)
			{
                using (Stream myStream = (FileStream)fdoFile.OpenFile())
                {
                    if (myStream != null)
                    {
                        using (StreamReader sr = new StreamReader(myStream))
                        {
                            _config = sr.ReadToEnd();
                            lblConfig.Text = fdoFile.FileName;
                        }
                    }
                }
			}
		}

		private void radioButton2_CheckedChanged(object sender, EventArgs e)
		{
			ChangeConfigXmlSelection(true);
		}

		private void ChangeConfigXmlSelection(bool value)
		{
			btnUploadConfig.Enabled = value;
			btnCreateConfig.Enabled = !value;
		}

		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{
			ChangeConfigXmlSelection(false);
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(_reportHeader))
			{
				bool isLocked = (_assembly.Length > 0 && !string.IsNullOrEmpty(_config));
				if (chbReport.Checked)
				{
					if (string.IsNullOrEmpty(_config))
					{
						_config = AdminToolCommon.TXT_INIT_REPORT_CONFIG;
					}
					this.DialogResult = (AdminReportProvider.UploadAssembly(_assemblyName,
						_reportHeader, _assembly, _config, AdminToolCommon.OBJECT_GROUP_ID_REPORT, AdminToolCommon.OBJECT_TYPE_ID_REPORT, AdminToolCommon.IS_ACTIVE_REPORT, _assemblyVersion, true))
						? DialogResult.OK : DialogResult.Cancel;
				}
				else
				{
					this.DialogResult = (AdminReportProvider.UploadAssembly(_assemblyName,
                                            string.Format("{0}.{1}",_assemblyName, _assemblyVersion), _assembly, string.Empty, AdminToolCommon.OBJECT_GROUP_ID_COMMON, AdminToolCommon.OBJECT_TYPE_ID_COMMON, AdminToolCommon.IS_ACTIVE_COMMON, _assemblyVersion, false))
											? DialogResult.OK : DialogResult.Cancel;
				}
			}
			else
			{
				MessageBox.Show("Set report title");
				tbxReportHeader.Focus();
			}
		}

		private void btnCreateConfig_Click(object sender, EventArgs e)
		{
			ReportProxyObject.InitXml(AdminToolCommon.TXT_INIT_REPORT_CONFIG);
			using (SheetConfiguration frm = new SheetConfiguration())
			{
				if (frm.ShowDialog() == DialogResult.OK)
				{
					_config = ReportProxyObject.GetConfigXml;
					File.WriteAllText("c:\\sample.xml", _config);
				}
			}
		}

		private void tbxReportHeader_Leave(object sender, EventArgs e)
		{
			_reportHeader = tbxReportHeader.Text.Trim();
		}
	}
}
