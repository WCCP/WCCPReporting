﻿using Logica.Reports.ConfigXmlParser.GridExtension;
namespace Logica.Reports.AdminTool.Forms
{
    partial class GridPreview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGrid = new DevExpress.XtraGrid.GridControl();
            this.uiGvMainView = new Logica.Reports.ConfigXmlParser.GridExtension.GroupBandedView();
            ((System.ComponentModel.ISupportInitialize)(this.uiGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGvMainView)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGrid
            // 
            this.uiGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGrid.Location = new System.Drawing.Point(0, 0);
            this.uiGrid.MainView = this.uiGvMainView;
            this.uiGrid.Name = "uiGrid";
            this.uiGrid.Size = new System.Drawing.Size(655, 415);
            this.uiGrid.TabIndex = 0;
            this.uiGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.uiGvMainView});
            // 
            // uiGvMainView
            // 
            this.uiGvMainView.GridControl = this.uiGrid;
            this.uiGvMainView.Name = "uiGvMainView";
            this.uiGvMainView.OptionsBehavior.AutoExpandAllGroups = true;
            this.uiGvMainView.OptionsBehavior.AutoPopulateColumns = false;
            this.uiGvMainView.OptionsBehavior.SummariesIgnoreNullValues = true;
            this.uiGvMainView.OptionsCustomization.AllowChangeBandParent = true;
            this.uiGvMainView.OptionsCustomization.AllowRowSizing = true;
            this.uiGvMainView.OptionsView.ShowGroupPanel = false;
            this.uiGvMainView.OptionsView.ShowIndicator = false;
            // 
            // GridPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 415);
            this.Controls.Add(this.uiGrid);
            this.Name = "GridPreview";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Grid preview...";
            ((System.ComponentModel.ISupportInitialize)(this.uiGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGvMainView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl uiGrid;
        private GroupBandedView uiGvMainView;
    }
}
