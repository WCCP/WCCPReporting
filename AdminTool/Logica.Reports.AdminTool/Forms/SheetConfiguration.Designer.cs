﻿using Logica.Reports.AdminTool.UserControls;
namespace Logica.Reports.AdminTool.Forms
{
	partial class SheetConfiguration
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.xtraTbReport = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTbPInit = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTbCInit = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTbPPreInit = new DevExpress.XtraTab.XtraTabPage();
            this.preInitCondition = new Logica.Reports.AdminTool.UserControls.PreCondition();
            this.xtraTbPOnInit = new DevExpress.XtraTab.XtraTabPage();
            this.onInitCondition = new Logica.Reports.AdminTool.UserControls.PreCondition();
            this.xtraTbPSheetConfiguration = new DevExpress.XtraTab.XtraTabPage();
            this.tbTabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.bciHtml = new DevExpress.XtraBars.BarCheckItem();
            this.bciMht = new DevExpress.XtraBars.BarCheckItem();
            this.bciPdf = new DevExpress.XtraBars.BarCheckItem();
            this.bciRtf = new DevExpress.XtraBars.BarCheckItem();
            this.bciTxt = new DevExpress.XtraBars.BarCheckItem();
            this.bciXls = new DevExpress.XtraBars.BarCheckItem();
            this.bciXlsx = new DevExpress.XtraBars.BarCheckItem();
            this.bciCsv = new DevExpress.XtraBars.BarCheckItem();
            this.bciImage = new DevExpress.XtraBars.BarCheckItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonoptions = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTbReport)).BeginInit();
            this.xtraTbReport.SuspendLayout();
            this.xtraTbPInit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTbCInit)).BeginInit();
            this.xtraTbCInit.SuspendLayout();
            this.xtraTbPPreInit.SuspendLayout();
            this.xtraTbPOnInit.SuspendLayout();
            this.xtraTbPSheetConfiguration.SuspendLayout();
            this.tbTabs.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTbReport
            // 
            this.xtraTbReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTbReport.Location = new System.Drawing.Point(0, 24);
            this.xtraTbReport.Name = "xtraTbReport";
            this.xtraTbReport.SelectedTabPage = this.xtraTbPInit;
            this.xtraTbReport.Size = new System.Drawing.Size(691, 698);
            this.xtraTbReport.TabIndex = 1;
            this.xtraTbReport.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTbPInit,
            this.xtraTbPSheetConfiguration});
            this.xtraTbReport.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTbReport_SelectedPageChanged);
            // 
            // xtraTbPInit
            // 
            this.xtraTbPInit.Controls.Add(this.xtraTbCInit);
            this.xtraTbPInit.Name = "xtraTbPInit";
            this.xtraTbPInit.Size = new System.Drawing.Size(684, 669);
            this.xtraTbPInit.Text = "Report pre-conditions";
            // 
            // xtraTbCInit
            // 
            this.xtraTbCInit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTbCInit.Location = new System.Drawing.Point(0, 0);
            this.xtraTbCInit.Name = "xtraTbCInit";
            this.xtraTbCInit.SelectedTabPage = this.xtraTbPPreInit;
            this.xtraTbCInit.Size = new System.Drawing.Size(684, 669);
            this.xtraTbCInit.TabIndex = 0;
            this.xtraTbCInit.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTbPPreInit,
            this.xtraTbPOnInit});
            // 
            // xtraTbPPreInit
            // 
            this.xtraTbPPreInit.Controls.Add(this.preInitCondition);
            this.xtraTbPPreInit.Name = "xtraTbPPreInit";
            this.xtraTbPPreInit.Size = new System.Drawing.Size(677, 640);
            this.xtraTbPPreInit.Text = "PreInit";
            // 
            // preInitCondition
            // 
            this.preInitCondition.ConditionMode = Logica.Reports.AdminTool.Data.PreconditionMode.PreInit;
            this.preInitCondition.CurrentInit = null;
            this.preInitCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.preInitCondition.Location = new System.Drawing.Point(0, 0);
            this.preInitCondition.Name = "preInitCondition";
            this.preInitCondition.Size = new System.Drawing.Size(677, 640);
            this.preInitCondition.TabIndex = 0;
            // 
            // xtraTbPOnInit
            // 
            this.xtraTbPOnInit.Controls.Add(this.onInitCondition);
            this.xtraTbPOnInit.Name = "xtraTbPOnInit";
            this.xtraTbPOnInit.Size = new System.Drawing.Size(677, 640);
            this.xtraTbPOnInit.Text = "OnInit";
            // 
            // onInitCondition
            // 
            this.onInitCondition.ConditionMode = Logica.Reports.AdminTool.Data.PreconditionMode.OnInit;
            this.onInitCondition.CurrentInit = null;
            this.onInitCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.onInitCondition.Location = new System.Drawing.Point(0, 0);
            this.onInitCondition.Name = "onInitCondition";
            this.onInitCondition.Size = new System.Drawing.Size(677, 640);
            this.onInitCondition.TabIndex = 0;
            // 
            // xtraTbPSheetConfiguration
            // 
            this.xtraTbPSheetConfiguration.Controls.Add(this.tbTabs);
            this.xtraTbPSheetConfiguration.Name = "xtraTbPSheetConfiguration";
            this.xtraTbPSheetConfiguration.Size = new System.Drawing.Size(684, 669);
            this.xtraTbPSheetConfiguration.Text = "Sheet configuration";
            // 
            // tbTabs
            // 
            this.tbTabs.Controls.Add(this.tabPage1);
            this.tbTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTabs.Location = new System.Drawing.Point(0, 0);
            this.tbTabs.Name = "tbTabs";
            this.tbTabs.SelectedIndex = 0;
            this.tbTabs.Size = new System.Drawing.Size(684, 669);
            this.tbTabs.TabIndex = 0;
            this.tbTabs.SelectedIndexChanged += new System.EventHandler(this.tbTabs_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(676, 643);
            this.tabPage1.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(79, 26);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(78, 22);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barSubItem1,
            this.bciHtml,
            this.bciMht,
            this.bciPdf,
            this.bciRtf,
            this.bciTxt,
            this.bciXls,
            this.bciXlsx,
            this.bciCsv,
            this.bciImage,
            this.barButtonoptions});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 23;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonoptions, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Add Tab";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Delete Tab";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "|";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Save";
            this.barButtonItem4.Id = 3;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "|";
            this.barButtonItem5.Id = 4;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Cancel";
            this.barButtonItem6.Id = 5;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "|";
            this.barButtonItem7.Id = 11;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Export Format";
            this.barSubItem1.Id = 12;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciHtml),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciMht),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciRtf),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciTxt),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciXls),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciXlsx),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciImage)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // bciHtml
            // 
            this.bciHtml.Caption = "Web Page (*.html)";
            this.bciHtml.Id = 13;
            this.bciHtml.Name = "bciHtml";
            // 
            // bciMht
            // 
            this.bciMht.Caption = "Web Page (*.mht)";
            this.bciMht.Id = 14;
            this.bciMht.Name = "bciMht";
            // 
            // bciPdf
            // 
            this.bciPdf.Caption = "Portable Document (*.pdf)";
            this.bciPdf.Id = 15;
            this.bciPdf.Name = "bciPdf";
            // 
            // bciRtf
            // 
            this.bciRtf.Caption = "Rich Text (*.rtf)";
            this.bciRtf.Checked = true;
            this.bciRtf.Id = 16;
            this.bciRtf.Name = "bciRtf";
            // 
            // bciTxt
            // 
            this.bciTxt.Caption = "Text File (*.txt)";
            this.bciTxt.Id = 17;
            this.bciTxt.Name = "bciTxt";
            // 
            // bciXls
            // 
            this.bciXls.Caption = "Microsoft Excel 97 (*.xls)";
            this.bciXls.Checked = true;
            this.bciXls.Id = 18;
            this.bciXls.Name = "bciXls";
            // 
            // bciXlsx
            // 
            this.bciXlsx.Caption = "Microsoft Excel 2007 (*.xlsx)";
            this.bciXlsx.Checked = true;
            this.bciXlsx.Id = 19;
            this.bciXlsx.Name = "bciXlsx";
            // 
            // bciCsv
            // 
            this.bciCsv.Caption = "Comma-separated (*.csv)";
            this.bciCsv.Id = 20;
            this.bciCsv.Name = "bciCsv";
            // 
            // bciImage
            // 
            this.bciImage.Caption = "Image (*.bmp)";
            this.bciImage.Id = 21;
            this.bciImage.Name = "bciImage";
            // 
            // barButtonoptions
            // 
            this.barButtonoptions.Caption = "Options";
            this.barButtonoptions.Id = 22;
            this.barButtonoptions.Name = "barButtonoptions";
            this.barButtonoptions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonoptions_ItemClick);
            // 
            // SheetConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 722);
            this.Controls.Add(this.xtraTbReport);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "SheetConfiguration";
            this.ShowInTaskbar = false;
            this.Text = "Generate configuration file";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.xtraTbReport)).EndInit();
            this.xtraTbReport.ResumeLayout(false);
            this.xtraTbPInit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTbCInit)).EndInit();
            this.xtraTbCInit.ResumeLayout(false);
            this.xtraTbPPreInit.ResumeLayout(false);
            this.xtraTbPOnInit.ResumeLayout(false);
            this.xtraTbPSheetConfiguration.ResumeLayout(false);
            this.tbTabs.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tbTabs;
		private System.Windows.Forms.TabPage tabPage1;
		//private TabReportConfigurator ucTabReportConfigurator1;
		private DevExpress.XtraTab.XtraTabControl xtraTbReport;
		private DevExpress.XtraTab.XtraTabPage xtraTbPInit;
		private DevExpress.XtraTab.XtraTabPage xtraTbPSheetConfiguration;
		private DevExpress.XtraTab.XtraTabControl xtraTbCInit;
		private DevExpress.XtraTab.XtraTabPage xtraTbPPreInit;
		private DevExpress.XtraTab.XtraTabPage xtraTbPOnInit;
		private DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.Bar bar2;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraBars.BarButtonItem barButtonItem1;
		private DevExpress.XtraBars.BarButtonItem barButtonItem2;
		private DevExpress.XtraBars.BarButtonItem barButtonItem3;
		private DevExpress.XtraBars.BarButtonItem barButtonItem4;
		private DevExpress.XtraBars.BarButtonItem barButtonItem5;
		private DevExpress.XtraBars.BarButtonItem barButtonItem6;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
		private PreCondition preInitCondition;
		private PreCondition onInitCondition;
		private DevExpress.XtraBars.BarButtonItem barButtonItem7;
		private DevExpress.XtraBars.BarSubItem barSubItem1;
		private DevExpress.XtraBars.BarCheckItem bciHtml;
		private DevExpress.XtraBars.BarCheckItem bciMht;
		private DevExpress.XtraBars.BarCheckItem bciPdf;
		private DevExpress.XtraBars.BarCheckItem bciRtf;
		private DevExpress.XtraBars.BarCheckItem bciTxt;
		private DevExpress.XtraBars.BarCheckItem bciXls;
		private DevExpress.XtraBars.BarCheckItem bciXlsx;
		private DevExpress.XtraBars.BarCheckItem bciCsv;
		private DevExpress.XtraBars.BarCheckItem bciImage;
        private DevExpress.XtraBars.BarButtonItem barButtonoptions;


	}
}