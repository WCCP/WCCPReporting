﻿using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.AdminTool.Data;
using Logica.Reports.DataAccess;

namespace Logica.Reports.AdminTool.Forms {
    public partial class MainForm : Form {
        private const string STR_OBJECT_ID = "Object_ID";
        private const string STR_OBJECT_NAME = "ObjectName";
        private const string STR_OBJECT_VERSION = "ObjectVersion";

        private const string STR_OBJECT_LIBRARY = "ObjectLibrary";
        private const string MSG_ASSEMBLY_ADDED = "Assembly was added";
        private const string MSG_REPORT_ACTIVE = "Report status changed to Active";
        private const string MSG_ASSEMBLY_UPDATED = "Assembly was updated";
        private const string MSG_GROUP_TYPE_CHANGED = @"Group\Type was changed successful";
        private const string MSG_CONFIG_UPDATED = "Configuration file for report updated";
        private const string MSG_WANT_TO_DELETE_REPORT = "You want to delete report?";
        private const string STR_DELETE = "Delete";
        private bool _connected = false;

        public MainForm() {
            InitializeComponent();
        }

        /// <summary>
        /// Refresh TreeView list of Common and Report assemblies
        /// </summary>
        private void RefreshTree() {
            trReportList.Nodes.Clear();
            DataTable dt = AdminReportProvider.GetReportsList().Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++) {
                trReportList.Nodes.Add(Convert.ToString(dt.Rows[i][STR_OBJECT_ID]),
                    Convert.ToString(dt.Rows[i][STR_OBJECT_NAME]));
            }

            tvCommonAssemblies.Nodes.Clear();
            dt = AdminReportProvider.GetCommonAssembliesList().Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++) {
                tvCommonAssemblies.Nodes.Add(Convert.ToString(dt.Rows[i][STR_OBJECT_ID]),
                    Convert.ToString(dt.Rows[i][STR_OBJECT_LIBRARY]), Convert.ToString(dt.Rows[i][STR_OBJECT_VERSION]));
            }
        }

        /// <summary>
        /// ReSet LDB connection string
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetConnectionToLDB(object sender, EventArgs e) {
            using (ConnectionToSqlForm frm = new ConnectionToSqlForm()) {
                if (frm.ShowDialog() == DialogResult.OK) {
                    _connected = true;
                    RefreshTree();
                }
            }
            barSubItem3.Enabled = _connected;
        }

        /// <summary>
        /// Update assembly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmiUAssembly_Click(object sender, EventArgs e) {
            UpdateAssembly(trReportList);
        }

        private void UpdateAssembly(TreeView tree) {
            if (fdoFile.ShowDialog() == DialogResult.OK) {
                FileStream myStream;
                if ((myStream = (FileStream) fdoFile.OpenFile()) != null) {
                    byte[] assembly = AdminToolCommon.GetBytes(myStream);
                    string assemblyVersion = "0.0.0.0";
                    try {
                        Assembly assem = Assembly.Load(assembly);
                        string[] res = assem.FullName.Split(',');
                        assemblyVersion = res[1].Replace("Version=", "").Trim();
                    }
                    catch (BadImageFormatException) {
                    }

                    AdminReportProvider.UpdateReportAssembly(
                        Convert.ToInt32(tree.SelectedNode.Name), assembly, assemblyVersion, fdoFile.SafeFileName);
                }
                MessageBox.Show(MSG_ASSEMBLY_UPDATED);
                RefreshTree();
            }
        }

        /// <summary>
        /// Update report config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmiUConfig_Click(object sender, EventArgs e) {
            if (trReportList.SelectedNode == null)
                return;
            int report_Id = Convert.ToInt32(trReportList.SelectedNode.Name);

            string devXGridLayoutXml = AdminReportProvider.ReportConfig(report_Id);
            if (string.IsNullOrEmpty(devXGridLayoutXml)) {
                devXGridLayoutXml = AdminToolCommon.TXT_INIT_REPORT_CONFIG;
            }
            this.Cursor = Cursors.WaitCursor;
            ReportProxyObject.InitXml(devXGridLayoutXml);
            using (SheetConfiguration frm = new SheetConfiguration()) {
                this.Cursor = Cursors.Default;
                if (frm.ShowDialog() == DialogResult.OK) {
                    string config = ReportProxyObject.GetConfigXml;
                    AdminReportProvider.UpdateReportConfig(report_Id, config);
                    MessageBox.Show(MSG_CONFIG_UPDATED);
                    CreateBackup(trReportList.SelectedNode.Text, config);
                }
            }
        }

        /// <summary>
        /// Delete assembly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmiDelete_Click(object sender, EventArgs e) {
            if (MessageBox.Show(MSG_WANT_TO_DELETE_REPORT, STR_DELETE, MessageBoxButtons.OKCancel) == DialogResult.OK) {
                AdminReportProvider.DeleteReport(Convert.ToInt32(trReportList.SelectedNode.Name));
                RefreshTree();
            }
        }

        /// <summary>
        /// Create new object (Report/Common) in DW_URM_Object table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barSubItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            using (ReportUpload frm = new ReportUpload()) {
                if (frm.ShowDialog() == DialogResult.OK) {
                    MessageBox.Show(MSG_ASSEMBLY_ADDED);
                }
                RefreshTree();
            }
        }

        /// <summary>
        /// Setup new LDB connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            trReportList.Nodes.Clear();
            tvCommonAssemblies.Nodes.Clear();
            SetConnectionToLDB(sender, e);
        }

        /// <summary>
        /// Close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            Close();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e) {
            UpdateAssembly(tvCommonAssemblies);
        }

        private void tsbChangeGroupType_Click(object sender, EventArgs e) {
            if (trReportList.SelectedNode == null)
                return;
            using (ChangeReportGroupAndType frm = new ChangeReportGroupAndType()) {
                frm.ReportId = Convert.ToInt32(trReportList.SelectedNode.Name);
                frm.ReportName = trReportList.SelectedNode.Text;
                if (frm.ShowDialog() == DialogResult.OK) {
                    MessageBox.Show(MSG_GROUP_TYPE_CHANGED);
                }
                RefreshTree();
            }
        }

        private void uploadConfigToolStripMenuItem_Click(object sender, EventArgs e) {
            if (trReportList.SelectedNode == null)
                return;
            int report_Id = Convert.ToInt32(trReportList.SelectedNode.Name);
            string config = string.Empty;
            if (fdoFile.ShowDialog() == DialogResult.OK) {
                using (Stream myStream = (FileStream) fdoFile.OpenFile()) {
                    if (myStream != null) {
                        using (StreamReader sr = new StreamReader(myStream, System.Text.Encoding.Default)) {
                            config = sr.ReadToEnd();
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(config)) {
                AdminReportProvider.UpdateReportConfig(report_Id, config);
                MessageBox.Show(MSG_CONFIG_UPDATED);
            }
        }

        private void setActiveToolStripMenuItem_Click(object sender, EventArgs e) {
            if (trReportList.SelectedNode == null)
                return;
            try {
                int report_Id = Convert.ToInt32(trReportList.SelectedNode.Name);
                AdminReportProvider.SetReportActive(report_Id);
                MessageBox.Show(MSG_REPORT_ACTIVE);
            }
            catch (Exception exc) {
                MessageBox.Show(exc.Message);
            }
        }

        /// <summary>
        /// Creates a backup file for report config file and save it to the hard drive.
        /// </summary>
        /// <param name="reportName">Will be used as a name of file</param>
        /// <param name="config">XML report config</param>
        private void CreateBackup(string reportName, string config) {
            string folderName = string.Format("C:\\ConfigBackup\\{0}", DateTime.Today.ToString("ddMMyyyy"));
            string fileName = string.Format("{0}.xml", reportName);
            string fullName = string.Concat(folderName, "\\", fileName);
            if (!Directory.Exists(folderName)) {
                Directory.CreateDirectory(folderName);
            }
            if (File.Exists(fullName)) {
                File.Delete(fullName);
            }
            FileStream f = new FileStream(fullName, FileMode.CreateNew);
            using (StreamWriter writer = new StreamWriter(f)) {
                writer.Write(config);
            }
            f.Close();
        }

        private void cCommon_Opening(object sender, System.ComponentModel.CancelEventArgs e) {
            if (tvCommonAssemblies.SelectedNode == null)
                return;
            if (cCommon.Items.Count == 3) {
                cCommon.Items.Add((string.IsNullOrEmpty(tvCommonAssemblies.SelectedNode.ImageKey))
                                      ? "empty"
                                      : string.Format("Version: {0}", tvCommonAssemblies.SelectedNode.ImageKey));
            }
            else {
                cCommon.Items[3].Text = (string.IsNullOrEmpty(tvCommonAssemblies.SelectedNode.ImageKey))
                                            ? "empty"
                                            : string.Format("Version: {0}", tvCommonAssemblies.SelectedNode.ImageKey);
            }
        }

        private void trReportList_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) {
            if (e.Button == MouseButtons.Right) {
                trReportList.SelectedNode = e.Node;
            }
        }

        private void downloadConfigToolStripMenuItem_Click(object sender, EventArgs e) {
            if (trReportList.SelectedNode != null) {
                int report_Id = Convert.ToInt32(trReportList.SelectedNode.Name);
                string config = AdminReportProvider.ReportConfig(report_Id);
                saveFileDialog.FileName = trReportList.SelectedNode.Text + '.' + saveFileDialog.DefaultExt;
                if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                    FileStream f = new FileStream(saveFileDialog.FileName, FileMode.Create);
                    using (StreamWriter writer = new StreamWriter(f, Encoding.Unicode)) {
                        writer.Write(config);
                    }
                    f.Close();
                }
            }
        }

        private void tvCommonAssemblies_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) {
            if (e.Button == MouseButtons.Right) {
                tvCommonAssemblies.SelectedNode = e.Node;
            }
        }
    }
}