﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.DataAccess;

namespace Logica.Reports.AdminTool.Forms
{
	public partial class ChangeReportGroupAndType : Form
	{
		string _reportName;
		int _reportId;

		public string ReportName
		{
			get
			{
				return _reportName;
			}
			set
			{
				lblReportName.Text = _reportName = value;
			}
		}

		public int ReportId
		{
			get
			{
				return _reportId;
			}
			set
			{
				_reportId = value;
			}
		}
		public ChangeReportGroupAndType()
		{
			InitializeComponent();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			AdminReportProvider.ChangeReportGroupAndType(_reportId, (Int16) cbGroup.SelectedValue
				, (byte) cbType.SelectedValue);
			this.DialogResult = DialogResult.OK;
		}

		private void ChangeReportGroupAndType_Load(object sender, EventArgs e)
		{
			DataTable dtGroup = AdminReportProvider.GetURMGroupList().Tables[0];
			cbGroup.DataSource = dtGroup;
			cbGroup.DisplayMember = "ObjectGroupName";
			cbGroup.ValueMember = "ObjectGroup_ID";

			DataTable dtType = AdminReportProvider.GetURMTypeList().Tables[0];
			cbType.DataSource = dtType;
			cbType.DisplayMember = "ObjectTypeName";
			cbType.ValueMember = "ObjectType_ID";

			DataRow dr = AdminReportProvider.GetReportGroupAndType(_reportId).Tables[0].Rows[0];

			cbGroup.SelectedValue = (Int16) dr[0];
			cbType.SelectedValue = (byte) dr[1];
		}
	}
}
