﻿namespace Logica.Reports.AdminTool.Forms
{
	partial class ReportUpload
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.chbReport = new System.Windows.Forms.CheckBox();
			this.lblConfig = new System.Windows.Forms.Label();
			this.lblAssembly = new System.Windows.Forms.Label();
			this.btnUploadAssembly = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.btnSave = new System.Windows.Forms.Button();
			this.fdoFile = new System.Windows.Forms.OpenFileDialog();
			this.btnUploadConfig = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.tbxReportHeader = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.btnCreateConfig = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// chbReport
			// 
			this.chbReport.AutoSize = true;
			this.chbReport.Location = new System.Drawing.Point(15, 222);
			this.chbReport.Name = "chbReport";
			this.chbReport.Size = new System.Drawing.Size(110, 17);
			this.chbReport.TabIndex = 17;
			this.chbReport.Text = "Is report assembly";
			this.chbReport.UseVisualStyleBackColor = true;
			// 
			// lblConfig
			// 
			this.lblConfig.AutoSize = true;
			this.lblConfig.Location = new System.Drawing.Point(8, 124);
			this.lblConfig.Name = "lblConfig";
			this.lblConfig.Size = new System.Drawing.Size(0, 13);
			this.lblConfig.TabIndex = 16;
			// 
			// lblAssembly
			// 
			this.lblAssembly.AutoSize = true;
			this.lblAssembly.Location = new System.Drawing.Point(19, 57);
			this.lblAssembly.Name = "lblAssembly";
			this.lblAssembly.Size = new System.Drawing.Size(0, 13);
			this.lblAssembly.TabIndex = 15;
			// 
			// btnUploadAssembly
			// 
			this.btnUploadAssembly.Location = new System.Drawing.Point(311, 35);
			this.btnUploadAssembly.Name = "btnUploadAssembly";
			this.btnUploadAssembly.Size = new System.Drawing.Size(75, 23);
			this.btnUploadAssembly.TabIndex = 14;
			this.btnUploadAssembly.Text = "Load";
			this.btnUploadAssembly.UseVisualStyleBackColor = true;
			this.btnUploadAssembly.Click += new System.EventHandler(this.btnUploadAssembly_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(75, 13);
			this.label2.TabIndex = 13;
			this.label2.Text = "Report header";
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(209, 248);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 10;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// fdoFile
			// 
			this.fdoFile.FileName = "openFileDialog1";
			// 
			// btnUploadConfig
			// 
			this.btnUploadConfig.Enabled = false;
			this.btnUploadConfig.Location = new System.Drawing.Point(307, 92);
			this.btnUploadConfig.Name = "btnUploadConfig";
			this.btnUploadConfig.Size = new System.Drawing.Size(75, 23);
			this.btnUploadConfig.TabIndex = 11;
			this.btnUploadConfig.Text = "Load";
			this.btnUploadConfig.UseVisualStyleBackColor = true;
			this.btnUploadConfig.Click += new System.EventHandler(this.btnUploadConfig_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(311, 248);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 38);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(113, 13);
			this.label3.TabIndex = 18;
			this.label3.Text = "Select report assembly";
			// 
			// tbxReportHeader
			// 
			this.tbxReportHeader.Location = new System.Drawing.Point(162, 6);
			this.tbxReportHeader.Name = "tbxReportHeader";
			this.tbxReportHeader.Size = new System.Drawing.Size(234, 20);
			this.tbxReportHeader.TabIndex = 19;
			this.tbxReportHeader.Leave += new System.EventHandler(this.tbxReportHeader_Leave);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButton2);
			this.groupBox1.Controls.Add(this.radioButton1);
			this.groupBox1.Controls.Add(this.btnCreateConfig);
			this.groupBox1.Controls.Add(this.btnUploadConfig);
			this.groupBox1.Controls.Add(this.lblConfig);
			this.groupBox1.Location = new System.Drawing.Point(4, 72);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(388, 140);
			this.groupBox1.TabIndex = 20;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Configuration xml";
			// 
			// radioButton2
			// 
			this.radioButton2.AutoSize = true;
			this.radioButton2.Location = new System.Drawing.Point(11, 95);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(103, 17);
			this.radioButton2.TabIndex = 18;
			this.radioButton2.Text = "Select config file";
			this.radioButton2.UseVisualStyleBackColor = true;
			this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
			// 
			// radioButton1
			// 
			this.radioButton1.AutoSize = true;
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(11, 35);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(104, 17);
			this.radioButton1.TabIndex = 17;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Create config file";
			this.radioButton1.UseVisualStyleBackColor = true;
			this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			// 
			// btnCreateConfig
			// 
			this.btnCreateConfig.Location = new System.Drawing.Point(307, 29);
			this.btnCreateConfig.Name = "btnCreateConfig";
			this.btnCreateConfig.Size = new System.Drawing.Size(75, 23);
			this.btnCreateConfig.TabIndex = 13;
			this.btnCreateConfig.Text = "Open form";
			this.btnCreateConfig.UseVisualStyleBackColor = true;
			this.btnCreateConfig.Click += new System.EventHandler(this.btnCreateConfig_Click);
			// 
			// ReportUpload
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(404, 288);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.tbxReportHeader);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.chbReport);
			this.Controls.Add(this.lblAssembly);
			this.Controls.Add(this.btnUploadAssembly);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnCancel);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ReportUpload";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Upload new report\\common dll";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckBox chbReport;
		private System.Windows.Forms.Label lblConfig;
		private System.Windows.Forms.Label lblAssembly;
		private System.Windows.Forms.Button btnUploadAssembly;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.OpenFileDialog fdoFile;
		private System.Windows.Forms.Button btnUploadConfig;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbxReportHeader;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnCreateConfig;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton1;
	}
}