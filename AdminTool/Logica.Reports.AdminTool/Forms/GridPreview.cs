﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Logica.Reports.AdminTool.Forms
{
    public partial class GridPreview : Form
    {
        public GridPreview(DataTable dataSource, string xmlLayout)
        {
            InitializeComponent();
            uiGvMainView.ConfigureGridView(xmlLayout);
            uiGvMainView.FillData(dataSource);
        }
    }
}
