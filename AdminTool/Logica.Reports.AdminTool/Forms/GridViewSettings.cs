﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace Logica.Reports.AdminTool.Forms
{
    public partial class GridViewSettings : Form
    {
        #region Private members

        private BandedGridView _gridView;

        #endregion // Private members

        #region Constructors

        public GridViewSettings(BandedGridView gridView)
        {
            InitializeComponent();
            _gridView = gridView;
            LoadSettings();
        }

        #endregion // Constructors

        #region Event handlers

        private void uiBtnOk_Click(object sender, EventArgs e)
        {
            SaveSettings();
            Close();
        }

        private void uiBtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion // Event handlers

        #region Helper methods

        private void LoadSettings()
        {
            if (_gridView == null)
            {
                return;
            }

            uiCbxShowBands.Checked = _gridView.OptionsView.ShowBands;
            uiCbxShowColumnHeaders.Checked = _gridView.OptionsView.ShowColumnHeaders;
            uiCbxShowGroupArea.Checked = _gridView.OptionsView.ShowGroupPanel;
            uiCbxColumnsAutoWidth.Checked = _gridView.OptionsView.ColumnAutoWidth;
            uiCbxAllowBandMoving.Checked = _gridView.OptionsCustomization.AllowBandMoving;
            uiCbxShowTotals.Checked = _gridView.OptionsView.ShowFooter;
            uiCbxAutoRowHeight.Checked = _gridView.OptionsView.RowAutoHeight;
            uiCbxAllowMerge.Checked = _gridView.OptionsView.AllowCellMerge;
        }

        private void SaveSettings()
        {
            if (_gridView == null)
            {
                return;
            }

            _gridView.OptionsView.ShowBands = uiCbxShowBands.Checked;
            _gridView.OptionsView.ShowColumnHeaders = uiCbxShowColumnHeaders.Checked;
            _gridView.OptionsView.ShowGroupPanel = uiCbxShowGroupArea.Checked;
            _gridView.OptionsView.ColumnAutoWidth = uiCbxColumnsAutoWidth.Checked;
            _gridView.OptionsCustomization.AllowBandMoving = uiCbxAllowBandMoving.Checked;
            _gridView.OptionsView.ShowFooter = uiCbxShowTotals.Checked;
            _gridView.OptionsView.RowAutoHeight = uiCbxAutoRowHeight.Checked;
            _gridView.OptionsView.AllowCellMerge = uiCbxAllowMerge.Checked;
          
        }

        #endregion // Helper methods
    }
}

