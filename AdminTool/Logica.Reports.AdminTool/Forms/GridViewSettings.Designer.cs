﻿namespace Logica.Reports.AdminTool.Forms
{
    partial class GridViewSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGrpHeader = new System.Windows.Forms.GroupBox();
            this.uiCbxShowGroupArea = new System.Windows.Forms.CheckBox();
            this.uiCbxShowColumnHeaders = new System.Windows.Forms.CheckBox();
            this.uiCbxShowBands = new System.Windows.Forms.CheckBox();
            this.uiGrpTableBody = new System.Windows.Forms.GroupBox();
            this.uiCbxAutoRowHeight = new System.Windows.Forms.CheckBox();
            this.uiCbxAllowBandMoving = new System.Windows.Forms.CheckBox();
            this.uiCbxColumnsAutoWidth = new System.Windows.Forms.CheckBox();
            this.uiCbxTableFooter = new System.Windows.Forms.GroupBox();
            this.uiCbxShowTotals = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uiBtnOk = new System.Windows.Forms.Button();
            this.uiBtnCancel = new System.Windows.Forms.Button();
            this.uiCbxAllowMerge = new System.Windows.Forms.CheckBox();
            this.uiGrpHeader.SuspendLayout();
            this.uiGrpTableBody.SuspendLayout();
            this.uiCbxTableFooter.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiGrpHeader
            // 
            this.uiGrpHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGrpHeader.Controls.Add(this.uiCbxShowGroupArea);
            this.uiGrpHeader.Controls.Add(this.uiCbxShowColumnHeaders);
            this.uiGrpHeader.Controls.Add(this.uiCbxShowBands);
            this.uiGrpHeader.Location = new System.Drawing.Point(2, 3);
            this.uiGrpHeader.Name = "uiGrpHeader";
            this.uiGrpHeader.Size = new System.Drawing.Size(327, 93);
            this.uiGrpHeader.TabIndex = 0;
            this.uiGrpHeader.TabStop = false;
            this.uiGrpHeader.Text = "Настройки шапки таблицы";
            // 
            // uiCbxShowGroupArea
            // 
            this.uiCbxShowGroupArea.AutoSize = true;
            this.uiCbxShowGroupArea.Location = new System.Drawing.Point(10, 65);
            this.uiCbxShowGroupArea.Name = "uiCbxShowGroupArea";
            this.uiCbxShowGroupArea.Size = new System.Drawing.Size(186, 17);
            this.uiCbxShowGroupArea.TabIndex = 2;
            this.uiCbxShowGroupArea.Text = "Показать область группировки";
            this.uiCbxShowGroupArea.UseVisualStyleBackColor = true;
            // 
            // uiCbxShowColumnHeaders
            // 
            this.uiCbxShowColumnHeaders.AutoSize = true;
            this.uiCbxShowColumnHeaders.Location = new System.Drawing.Point(10, 42);
            this.uiCbxShowColumnHeaders.Name = "uiCbxShowColumnHeaders";
            this.uiCbxShowColumnHeaders.Size = new System.Drawing.Size(176, 17);
            this.uiCbxShowColumnHeaders.TabIndex = 1;
            this.uiCbxShowColumnHeaders.Text = "Показать заголовки колонок";
            this.uiCbxShowColumnHeaders.UseVisualStyleBackColor = true;
            // 
            // uiCbxShowBands
            // 
            this.uiCbxShowBands.AutoSize = true;
            this.uiCbxShowBands.Location = new System.Drawing.Point(10, 19);
            this.uiCbxShowBands.Name = "uiCbxShowBands";
            this.uiCbxShowBands.Size = new System.Drawing.Size(110, 17);
            this.uiCbxShowBands.TabIndex = 0;
            this.uiCbxShowBands.Text = "Показать бэнды";
            this.uiCbxShowBands.UseVisualStyleBackColor = true;
            // 
            // uiGrpTableBody
            // 
            this.uiGrpTableBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
          this.uiGrpTableBody.Controls.Add(this.uiCbxAllowMerge);
            this.uiGrpTableBody.Controls.Add(this.uiCbxAutoRowHeight);
            this.uiGrpTableBody.Controls.Add(this.uiCbxAllowBandMoving);
            this.uiGrpTableBody.Controls.Add(this.uiCbxColumnsAutoWidth);
            this.uiGrpTableBody.Location = new System.Drawing.Point(2, 102);
            this.uiGrpTableBody.Name = "uiGrpTableBody";
          this.uiGrpTableBody.Size = new System.Drawing.Size(327, 113);
            this.uiGrpTableBody.TabIndex = 1;
            this.uiGrpTableBody.TabStop = false;
            this.uiGrpTableBody.Text = "Тело таблицы";
            // 
          // uiCbxAutoRowHeight
          // 
          this.uiCbxAutoRowHeight.AutoSize = true;
          this.uiCbxAutoRowHeight.Location = new System.Drawing.Point(10, 65);
          this.uiCbxAutoRowHeight.Name = "uiCbxAutoRowHeight";
          this.uiCbxAutoRowHeight.Size = new System.Drawing.Size(177, 17);
          this.uiCbxAutoRowHeight.TabIndex = 2;
          this.uiCbxAutoRowHeight.Text = "Автоподгонка высоты строки";
          this.uiCbxAutoRowHeight.UseVisualStyleBackColor = true;
          // 
            // uiCbxAllowBandMoving
            // 
            this.uiCbxAllowBandMoving.AutoSize = true;
            this.uiCbxAllowBandMoving.Location = new System.Drawing.Point(10, 42);
            this.uiCbxAllowBandMoving.Name = "uiCbxAllowBandMoving";
            this.uiCbxAllowBandMoving.Size = new System.Drawing.Size(195, 17);
            this.uiCbxAllowBandMoving.TabIndex = 1;
            this.uiCbxAllowBandMoving.Text = "Разрешить перемещение бэндов";
            this.uiCbxAllowBandMoving.UseVisualStyleBackColor = true;
            // 
            // uiCbxColumnsAutoWidth
            // 
            this.uiCbxColumnsAutoWidth.AutoSize = true;
            this.uiCbxColumnsAutoWidth.Location = new System.Drawing.Point(10, 19);
            this.uiCbxColumnsAutoWidth.Name = "uiCbxColumnsAutoWidth";
            this.uiCbxColumnsAutoWidth.Size = new System.Drawing.Size(185, 17);
            this.uiCbxColumnsAutoWidth.TabIndex = 0;
            this.uiCbxColumnsAutoWidth.Text = "Автоподгонка ширины колонки";
            this.uiCbxColumnsAutoWidth.UseVisualStyleBackColor = true;
            // 
            // uiCbxTableFooter
            // 
            this.uiCbxTableFooter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiCbxTableFooter.Controls.Add(this.uiCbxShowTotals);
          this.uiCbxTableFooter.Location = new System.Drawing.Point(2, 221);
            this.uiCbxTableFooter.Name = "uiCbxTableFooter";
            this.uiCbxTableFooter.Size = new System.Drawing.Size(327, 97);
            this.uiCbxTableFooter.TabIndex = 2;
            this.uiCbxTableFooter.TabStop = false;
            this.uiCbxTableFooter.Text = "Футтер таблицы";
            // 
            // uiCbxShowTotals
            // 
            this.uiCbxShowTotals.AutoSize = true;
            this.uiCbxShowTotals.Location = new System.Drawing.Point(10, 31);
            this.uiCbxShowTotals.Name = "uiCbxShowTotals";
            this.uiCbxShowTotals.Size = new System.Drawing.Size(114, 17);
            this.uiCbxShowTotals.TabIndex = 1;
            this.uiCbxShowTotals.Text = "Показать тоталы";
            this.uiCbxShowTotals.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.uiBtnOk);
            this.panel1.Controls.Add(this.uiBtnCancel);
          this.panel1.Location = new System.Drawing.Point(2, 324);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(327, 31);
            this.panel1.TabIndex = 3;
            // 
            // uiBtnOk
            // 
            this.uiBtnOk.Location = new System.Drawing.Point(244, 3);
            this.uiBtnOk.Name = "uiBtnOk";
            this.uiBtnOk.Size = new System.Drawing.Size(75, 23);
            this.uiBtnOk.TabIndex = 1;
            this.uiBtnOk.Text = "OK";
            this.uiBtnOk.UseVisualStyleBackColor = true;
            this.uiBtnOk.Click += new System.EventHandler(this.uiBtnOk_Click);
            // 
            // uiBtnCancel
            // 
            this.uiBtnCancel.Location = new System.Drawing.Point(163, 3);
            this.uiBtnCancel.Name = "uiBtnCancel";
            this.uiBtnCancel.Size = new System.Drawing.Size(75, 23);
            this.uiBtnCancel.TabIndex = 0;
            this.uiBtnCancel.Text = "Cancel";
            this.uiBtnCancel.UseVisualStyleBackColor = true;
            this.uiBtnCancel.Click += new System.EventHandler(this.uiBtnCancel_Click);
            // 
          // uiCbxAllowMerge
          // 
          this.uiCbxAllowMerge.AutoSize = true;
          this.uiCbxAllowMerge.Location = new System.Drawing.Point(10, 88);
          this.uiCbxAllowMerge.Name = "uiCbxAllowMerge";
          this.uiCbxAllowMerge.Size = new System.Drawing.Size(184, 17);
          this.uiCbxAllowMerge.TabIndex = 3;
          this.uiCbxAllowMerge.Text = "Разрешить объединение ячеек";
          this.uiCbxAllowMerge.UseVisualStyleBackColor = true;
          // 
            // GridViewSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(333, 367);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uiCbxTableFooter);
            this.Controls.Add(this.uiGrpTableBody);
            this.Controls.Add(this.uiGrpHeader);
            this.Name = "GridViewSettings";
            this.Text = "Опции представления";
            this.uiGrpHeader.ResumeLayout(false);
            this.uiGrpHeader.PerformLayout();
            this.uiGrpTableBody.ResumeLayout(false);
            this.uiGrpTableBody.PerformLayout();
            this.uiCbxTableFooter.ResumeLayout(false);
            this.uiCbxTableFooter.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox uiGrpHeader;
        private System.Windows.Forms.CheckBox uiCbxShowBands;
        private System.Windows.Forms.CheckBox uiCbxShowGroupArea;
        private System.Windows.Forms.CheckBox uiCbxShowColumnHeaders;
        private System.Windows.Forms.GroupBox uiGrpTableBody;
        private System.Windows.Forms.CheckBox uiCbxColumnsAutoWidth;
        private System.Windows.Forms.GroupBox uiCbxTableFooter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button uiBtnOk;
        private System.Windows.Forms.Button uiBtnCancel;
        private System.Windows.Forms.CheckBox uiCbxShowTotals;
        private System.Windows.Forms.CheckBox uiCbxAllowBandMoving;
        private System.Windows.Forms.CheckBox uiCbxAutoRowHeight;
        private System.Windows.Forms.CheckBox uiCbxAllowMerge;
    }
}