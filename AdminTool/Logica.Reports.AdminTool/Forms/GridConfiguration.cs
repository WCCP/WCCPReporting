﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.AdminTool.UserControls.Contracts;
using Logica.Reports.AdminTool.UserControls;

namespace Logica.Reports.AdminTool.Forms
{
	public partial class GridConfiguration : Form
	{
		string _layoutXml = string.Empty;
        ILayoutDesigner _designerControl;
        
		public string XmlConfiguration
		{
			get
			{
				return _layoutXml;
			}
			set
			{
				_layoutXml = value;
			}
		}

		public GridConfiguration(DataTable dt, string xmlLayout, ComponentDesignMode designMode)
		{
			InitializeComponent();
            switch (designMode)
            { 
                case ComponentDesignMode.Grid:
                    _designerControl = new GridDesigner();
                    break;
                case ComponentDesignMode.Chart:
                    _designerControl = new ChartDesigner();
                    break;

            }
            uiPnlContent.Controls.Add((Control)_designerControl);
            _designerControl.Initialize(dt, xmlLayout);
		}

		private void toolStripButton1_Click(object sender, EventArgs e)
		{
            _layoutXml = _designerControl.SerializeLayout();
            /*if (File.Exists(@"C:\1.xml"))
            {
                File.Delete(@"C:\1.xml");
            }
            FileStream f = new FileStream(@"C:\1.xml", FileMode.CreateNew);
            using (StreamWriter writer = new StreamWriter(f))
            {
                writer.Write(_layoutXml);
            }
            f.Close();*/
            
            /*FileStream f = new FileStream(@"C:\1.xml", FileMode.Open);
            using (StreamReader reader = new StreamReader(f))
            {
                _layoutXml = reader.ReadToEnd();
            }
            f.Close();*/
			this.DialogResult = DialogResult.OK;
		}

		private void toolStripButton2_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
        }
	}

    public enum ComponentDesignMode
    { 
        Grid,
        Chart
    }
}
