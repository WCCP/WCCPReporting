﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using Logica.Reports.AdminTool.Data;

namespace Logica.Reports.AdminTool.Forms
{
	public partial class ConnectionToSqlForm : Form
	{
		public ConnectionToSqlForm()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
            cmbSQLServersName.Text = Configuration.ServerName;
            cmbLDBbases.Text = Configuration.DbName;
            if (Configuration.AutoConnect)
            {
                rbWindowsAuthentication.Checked = true;
                if (Connect())
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Cannot connect to database automatically. Please check application configuration.",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                cmbSQLServersName.DataSource = null;
                cmbLDBbases.DataSource = null;
                rbWindowsAuthentication.Checked = true;
                txtPassword.Text = string.Empty;
                txtUserName.Text = string.Empty;
                EnableOKButton(0);
            }
		}

		private void cmbSQLServersName_DropDown(object sender, EventArgs e)
		{
			if (null == cmbSQLServersName.DataSource)
			{
				this.Cursor = Cursors.WaitCursor;
				SqlDataSourceEnumerator srvs = SqlDataSourceEnumerator.Instance;
				cmbSQLServersName.SelectedIndex = -1;
				cmbSQLServersName.DataSource = SqlServerManager.Current.GetServerNames();
				cmbSQLServersName.SelectedIndex = -1;
				this.Cursor = Cursors.Default;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.Close();
			SqlServerManager.Current.SetConnectionStringToDefault();
		}

		private void cmbDataBases_DropDown(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(cmbSQLServersName.Text))
			{
				btnRefreshServerNames_Click(sender, e);
			}
		}

		private void btnTestConnection_Click(object sender, EventArgs e)
		{
			TestConection(true);
		}

		private void EnableOKButton(int correction)
		{
			btnOK.Enabled = !string.IsNullOrEmpty((string) cmbLDBbases.SelectedItem);

		}

		private void cmbSQLServersName_SelectedIndexChanged(object sender, EventArgs e)
		{
			EnableOKButton(0);
		}

		private void cmbDataBases_SelectedIndexChanged(object sender, EventArgs e)
		{
			cmbSQLServersName_SelectedIndexChanged(sender, e);
		}

		private void AuthenticationChanged(object sender, EventArgs e)
		{
			lblUserName.Enabled =
				txtUserName.Enabled =
				txtPassword.Enabled =
				lblPassword.Enabled = rbSQLAuthentication.Checked;
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
            Connect();
		}

        private bool Connect()
        {
            bool result = false;
            string dataBaseName = cmbLDBbases.Text;

            if (!string.IsNullOrEmpty(dataBaseName) && TestConection(false))
            {
                ConnectionDescription cd;
                if (rbWindowsAuthentication.Checked)
                {
                    cd = new ConnectionDescription(cmbSQLServersName.Text, dataBaseName);
                    //						result = SqlServerManager.Current.SetConnectionString(cmbSQLServersName.Text, cmbDataBases.Text);
                }
                else
                {
                    cd = new ConnectionDescription(cmbSQLServersName.Text,
                                dataBaseName,
                                txtUserName.Text,
                                txtPassword.Text);
                }
                SqlServerManager.Current.AddConnection(cd);
                result = true;
            }
            if (!result)
            {
                MessageBox.Show("The selected parameters do not fit!\nPlease test connection before",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return result;
        }

		private bool TestConection(bool showMessageBox)
		{
			if (string.IsNullOrEmpty(cmbSQLServersName.Text))
			{
				if (showMessageBox)
					MessageBox.Show("The connection can not be tested beacuse no server name has specified!",
						Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

				return false;
			}
			this.Cursor = Cursors.WaitCursor;
			string errorMsg, userId, userPassword;
			userId = rbSQLAuthentication.Checked ? txtUserName.Text : null;
			userPassword = rbSQLAuthentication.Checked ? txtPassword.Text : null;
			bool result = false;
			errorMsg = string.Empty;

            string dbName = cmbLDBbases.Text;
    		result = SqlServerManager.Current.CheckConnection(cmbSQLServersName.Text,
								dbName, userId, userPassword, out errorMsg);

			if (showMessageBox)
				if (result)
				{
					MessageBox.Show("Test conection succeeded!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnOK.Enabled = true;
				}
				else
				{
					MessageBox.Show(errorMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}

			this.Cursor = Cursors.Default;
			return result;
		}

		private void btnRefreshServerNames_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			SqlDataSourceEnumerator srvs = SqlDataSourceEnumerator.Instance;
			cmbSQLServersName.SelectedIndex = -1;
			List<string> lst = SqlServerManager.Current.GetServerNames();
			cmbSQLServersName.DataSource = lst;
			cmbSQLServersName.SelectedIndex = -1;
			this.Cursor = Cursors.Default;
		}

		private void btnRefreshDatabas_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(cmbSQLServersName.Text))
			{
				MessageBox.Show("Databases can not be obtained beacuse no server name has specified!",
					Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

				return;
			}
			this.Cursor = Cursors.WaitCursor;
			string userId, userPass;
			userId = userPass = null;

			if (rbSQLAuthentication.Checked)
			{
				userId = txtUserName.Text;
				userPass = txtPassword.Text;
			}
			List<string> databases = SqlServerManager.Current.GetSWDatabases(cmbSQLServersName.Text, userId, userPass);

			cmbLDBbases.Items.Clear();
			if (null != databases)
			{
				cmbLDBbases.Items.AddRange(databases.ToArray());
				EnableOKButton(0);
			}

			this.Cursor = Cursors.Default;
		}

		private void cmbSQLServersName_Leave(object sender, EventArgs e)
		{
			//btnRefreshDatabas_Click(sender, e);
		}

		private void cmbLDBbases_SelectedIndexChanged(object sender, EventArgs e)
		{
			int correction = cmbLDBbases.SelectedIndex;
			EnableOKButton(correction);
		}
	}
}
