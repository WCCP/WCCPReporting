﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.AdminTool.Data;
using Logica.Reports.AdminTool.UserControls;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.ConfigXmlParser;
using DevExpress.XtraTab;
using Logica.Reports.Common;

namespace Logica.Reports.AdminTool.Forms
{
	public partial class SheetConfiguration : Form
	{
		public SheetConfiguration()
		{
			InitializeComponent();
			SetupForms();
		}

		private void SetupForms()
		{
			#region	  Export

			bciHtml.Checked = ReportProxyObject.ConfigReport.ExportHtml;
			bciMht.Checked = ReportProxyObject.ConfigReport.ExportMht;
			bciPdf.Checked = ReportProxyObject.ConfigReport.ExportPdf;
			bciRtf.Checked = ReportProxyObject.ConfigReport.ExportRtf;
			bciTxt.Checked = ReportProxyObject.ConfigReport.ExportTxt;
			bciXls.Checked = ReportProxyObject.ConfigReport.ExportXls;
			bciXlsx.Checked = ReportProxyObject.ConfigReport.ExportXlsx;
			bciCsv.Checked = ReportProxyObject.ConfigReport.ExportCsv;
			bciImage.Checked = ReportProxyObject.ConfigReport.ExportImage;

			#endregion

			#region SetupPreConditions
			preInitCondition.CurrentInit = ReportProxyObject.ConfigReport.Preconditions.PreInit;
			preInitCondition.ConditionMode = PreconditionMode.PreInit;
			preInitCondition.SetupPreConditions();
			onInitCondition.CurrentInit = ReportProxyObject.ConfigReport.Preconditions.OnInit;
			onInitCondition.ConditionMode = PreconditionMode.OnInit;
			onInitCondition.SetupPreConditions();
			#endregion

			#region FillReportSheets
			ReportProxyObject.SheetTabs = tbTabs;
			ReportProxyObject.FillReportSheets();
			if (ReportProxyObject.SheetTabs.TabPages.Count > 1)
					barButtonItem2.Enabled = true;
			#endregion
		}

		private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			ReportProxyObject.AddReportSheet();
			ReportProxyObject.SheetTabs.SelectTab(ReportProxyObject.SheetTabs.TabPages.Count-1);
			if (ReportProxyObject.SheetTabs.TabPages.Count > 1)
				barButtonItem2.Enabled = true;
		}

		private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			ReportProxyObject.DeleteReportSheet();
			if (ReportProxyObject.SheetTabs.TabPages.Count == 1)
				barButtonItem2.Enabled = false;
		}

		private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			try
			{
				#region	  Export

				ReportProxyObject.ConfigReport.ExportHtml = bciHtml.Checked;
				ReportProxyObject.ConfigReport.ExportMht = bciMht.Checked;
				ReportProxyObject.ConfigReport.ExportPdf = bciPdf.Checked;
				ReportProxyObject.ConfigReport.ExportRtf = bciRtf.Checked;
				ReportProxyObject.ConfigReport.ExportTxt = bciTxt.Checked;
				ReportProxyObject.ConfigReport.ExportXls = bciXls.Checked;
				ReportProxyObject.ConfigReport.ExportXlsx = bciXlsx.Checked;
				ReportProxyObject.ConfigReport.ExportCsv = bciCsv.Checked;
				ReportProxyObject.ConfigReport.ExportImage = bciImage.Checked;

				#endregion

				this.DialogResult = DialogResult.OK;
			}
			catch (TableHeaderEmptyExceptions excTable)
			{
				MessageBox.Show(excTable.ToString());
				tbTabs.SelectedTab = tbTabs.TabPages[excTable.TabIndex];
			}
			catch (TabHeaderEmptyExceptions exc)
			{
				MessageBox.Show(exc.ToString());
				tbTabs.SelectedTab = tbTabs.TabPages[exc.TabIndex];
			}
		}

		private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}

		private void xtraTbReport_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
		{
			ReportProxyObject.SetPreConditions();
		}

		private void tbTabs_SelectedIndexChanged(object sender, EventArgs e)
		{
			ReportProxyObject.CurrentSheetIndex = ReportProxyObject.SheetTabs.TabPages.IndexOf(ReportProxyObject.SheetTabs.SelectedTab);
			((TabReportConfigurator)ReportProxyObject.SheetTabs.SelectedTab.Controls.Find("TabReportConfigurator", true)[0]).VerifyButtonStatuses(sender,e);
		}

        private void barButtonoptions_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportConfiguration cfg = new ReportConfiguration(){PrintingMargin = ReportProxyObject.ConfigReport.PrintingMargin};
            if (cfg.ShowDialog() == DialogResult.OK)
            {
                ReportProxyObject.ConfigReport.PrintingMargin = cfg.PrintingMargin;
            }
        }
	}
}
