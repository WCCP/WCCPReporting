﻿namespace Logica.Reports.AdminTool.Forms
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.trReportList = new System.Windows.Forms.TreeView();
            this.cReport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiUAssembly = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiUConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbChangeGroupType = new System.Windows.Forms.ToolStripMenuItem();
            this.setActiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.tvCommonAssemblies = new System.Windows.Forms.TreeView();
            this.cCommon = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.fdoFile = new System.Windows.Forms.OpenFileDialog();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.cReport.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.cCommon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.xtraTabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(302, 474);
            this.panel1.TabIndex = 2;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(302, 474);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.trReportList);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(296, 446);
            this.xtraTabPage1.Text = "Report Assemblies";
            // 
            // trReportList
            // 
            this.trReportList.ContextMenuStrip = this.cReport;
            this.trReportList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trReportList.Location = new System.Drawing.Point(0, 0);
            this.trReportList.MinimumSize = new System.Drawing.Size(180, 370);
            this.trReportList.Name = "trReportList";
            this.trReportList.Size = new System.Drawing.Size(296, 446);
            this.trReportList.TabIndex = 2;
            this.trReportList.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.trReportList_NodeMouseClick);
            // 
            // cReport
            // 
            this.cReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiUAssembly,
            this.toolStripSeparator2,
            this.tsmiUConfig,
            this.downloadConfigToolStripMenuItem,
            this.uploadConfigToolStripMenuItem,
            this.toolStripSeparator3,
            this.tsbChangeGroupType,
            this.setActiveToolStripMenuItem,
            this.toolStripMenuItem2,
            this.tsmiDelete});
            this.cReport.Name = "cReport";
            this.cReport.Size = new System.Drawing.Size(183, 176);
            // 
            // tsmiUAssembly
            // 
            this.tsmiUAssembly.Name = "tsmiUAssembly";
            this.tsmiUAssembly.Size = new System.Drawing.Size(182, 22);
            this.tsmiUAssembly.Text = "Update Assembly";
            this.tsmiUAssembly.Click += new System.EventHandler(this.tsmiUAssembly_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(179, 6);
            // 
            // tsmiUConfig
            // 
            this.tsmiUConfig.Name = "tsmiUConfig";
            this.tsmiUConfig.Size = new System.Drawing.Size(182, 22);
            this.tsmiUConfig.Text = "Update Config";
            this.tsmiUConfig.Click += new System.EventHandler(this.tsmiUConfig_Click);
            // 
            // downloadConfigToolStripMenuItem
            // 
            this.downloadConfigToolStripMenuItem.Name = "downloadConfigToolStripMenuItem";
            this.downloadConfigToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.downloadConfigToolStripMenuItem.Text = "Download Config";
            this.downloadConfigToolStripMenuItem.Click += new System.EventHandler(this.downloadConfigToolStripMenuItem_Click);
            // 
            // uploadConfigToolStripMenuItem
            // 
            this.uploadConfigToolStripMenuItem.Name = "uploadConfigToolStripMenuItem";
            this.uploadConfigToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.uploadConfigToolStripMenuItem.Text = "Upload Config";
            this.uploadConfigToolStripMenuItem.Click += new System.EventHandler(this.uploadConfigToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(179, 6);
            // 
            // tsbChangeGroupType
            // 
            this.tsbChangeGroupType.Name = "tsbChangeGroupType";
            this.tsbChangeGroupType.Size = new System.Drawing.Size(182, 22);
            this.tsbChangeGroupType.Text = "Change Group\\Type";
            this.tsbChangeGroupType.Click += new System.EventHandler(this.tsbChangeGroupType_Click);
            // 
            // setActiveToolStripMenuItem
            // 
            this.setActiveToolStripMenuItem.Name = "setActiveToolStripMenuItem";
            this.setActiveToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.setActiveToolStripMenuItem.Text = "Set Active";
            this.setActiveToolStripMenuItem.Click += new System.EventHandler(this.setActiveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(179, 6);
            // 
            // tsmiDelete
            // 
            this.tsmiDelete.Name = "tsmiDelete";
            this.tsmiDelete.Size = new System.Drawing.Size(182, 22);
            this.tsmiDelete.Text = "Delete";
            this.tsmiDelete.Click += new System.EventHandler(this.tsmiDelete_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.tvCommonAssemblies);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(296, 444);
            this.xtraTabPage2.Text = "Common Assemblies";
            // 
            // tvCommonAssemblies
            // 
            this.tvCommonAssemblies.ContextMenuStrip = this.cCommon;
            this.tvCommonAssemblies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvCommonAssemblies.Location = new System.Drawing.Point(0, 0);
            this.tvCommonAssemblies.MinimumSize = new System.Drawing.Size(180, 370);
            this.tvCommonAssemblies.Name = "tvCommonAssemblies";
            this.tvCommonAssemblies.Size = new System.Drawing.Size(296, 444);
            this.tvCommonAssemblies.TabIndex = 3;
            this.tvCommonAssemblies.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvCommonAssemblies_NodeMouseClick);
            // 
            // cCommon
            // 
            this.cCommon.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.toolStripMenuItem4});
            this.cCommon.Name = "cCommon";
            this.cCommon.Size = new System.Drawing.Size(167, 54);
            this.cCommon.Opening += new System.ComponentModel.CancelEventHandler(this.cCommon_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItem1.Text = "Update Assembly";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(163, 6);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItem4.Text = "Delete";
            // 
            // fdoFile
            // 
            this.fdoFile.FileName = "openFileDialog1";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.barSubItem4,
            this.barSubItem5,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 10;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "File";
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Reports";
            this.barSubItem2.Id = 1;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Upload";
            this.barButtonItem3.Id = 9;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSubItem3_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Set DB Connection";
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Close";
            this.barButtonItem2.Id = 7;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(576, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 496);
            this.barDockControlBottom.Size = new System.Drawing.Size(576, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 474);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(576, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 474);
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Upload";
            this.barSubItem3.Id = 2;
            this.barSubItem3.Name = "barSubItem3";
            this.barSubItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSubItem3_ItemClick);
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "Set LDB Connection";
            this.barSubItem4.Id = 3;
            this.barSubItem4.Name = "barSubItem4";
            this.barSubItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "-";
            this.barSubItem5.Id = 4;
            this.barSubItem5.Name = "barSubItem5";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "xml";
            this.saveFileDialog.Filter = "XML files|*.xml";
            this.saveFileDialog.Title = "Save file";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 496);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MainForm";
            this.Text = "Admin tool";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SetConnectionToLDB);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.cReport.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.cCommon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TreeView trReportList;
		private System.Windows.Forms.ContextMenuStrip cReport;
		private System.Windows.Forms.ToolStripMenuItem tsmiUAssembly;
		private System.Windows.Forms.ToolStripMenuItem tsmiUConfig;
		private System.Windows.Forms.ToolStripMenuItem tsmiDelete;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.OpenFileDialog fdoFile;
		private DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.Bar bar2;
		private DevExpress.XtraBars.BarSubItem barSubItem1;
		private DevExpress.XtraBars.BarSubItem barSubItem2;
		private DevExpress.XtraBars.BarSubItem barSubItem3;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraBars.BarSubItem barSubItem4;
		private DevExpress.XtraBars.BarSubItem barSubItem5;
		private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
		private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
		private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
		private System.Windows.Forms.TreeView tvCommonAssemblies;
		private DevExpress.XtraBars.BarButtonItem barButtonItem1;
		private System.Windows.Forms.ContextMenuStrip cCommon;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
		private DevExpress.XtraBars.BarButtonItem barButtonItem2;
		private DevExpress.XtraBars.BarButtonItem barButtonItem3;
		private System.Windows.Forms.ToolStripMenuItem tsbChangeGroupType;
        private System.Windows.Forms.ToolStripMenuItem uploadConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setActiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem downloadConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;

	}
}

