﻿using System;
using System.Windows.Forms;
using Logica.Reports.AdminTool.Forms;

namespace Logica.Reports.AdminTool {
    internal static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}