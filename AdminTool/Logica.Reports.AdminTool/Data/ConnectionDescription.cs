﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Logica.Reports.AdminTool.Data {
    internal class ConnectionDescription {
        private SqlConnectionStringBuilder _connectionString;
        private bool _LDBExists = false;

        internal ConnectionDescription(string sqlServerName, string dbName, string userId, string userPassword) {
            _connectionString = new SqlConnectionStringBuilder();
            _connectionString.IntegratedSecurity = string.IsNullOrEmpty(userId);
            _connectionString.DataSource = sqlServerName;
            _connectionString.InitialCatalog = dbName;
            if (!string.IsNullOrEmpty(userId))
                _connectionString.UserID = userId;

            if (!string.IsNullOrEmpty(userPassword))
                _connectionString.Password = userPassword;

            List<string> dbList = SqlServerManager.Current.GetDatabasesList(sqlServerName, userId, userPassword);
            if (null != dbList) {
                foreach (string db in dbList) {
                    if (db.Equals(SqlServerManager.LogicaDatabaseName, StringComparison.InvariantCultureIgnoreCase)) {
                        _LDBExists = true;
                        break;
                    }
                }
            }
        }

        internal ConnectionDescription(string sqlServerName, string dbName)
            : this(sqlServerName, dbName, null, null) {
        }

        /// <summary>
        /// return connection parameters to the SW database
        /// </summary>
        internal SqlConnectionStringBuilder SqlConnectionString {
            get { return _connectionString; }
        }

        /// <summary>
        /// return SQL server name
        /// </summary>
        internal string ServerName {
            get { return _connectionString.DataSource; }
        }

        /// <summary>
        /// return SW database name
        /// </summary>
        internal string DataBase {
            get { return _connectionString.InitialCatalog; }
        }

        internal bool LDBExista {
            get { return _LDBExists; }
        }

        public override bool Equals(object obj) {
            if (!obj.GetType().Equals(typeof (ConnectionDescription))) {
                return false;
            }
            ConnectionDescription connectionDescr = (ConnectionDescription) obj;

            if (this._connectionString.ToString().Equals(connectionDescr.SqlConnectionString.ToString(),
                StringComparison.InvariantCultureIgnoreCase))
                return true;

            if (this.ServerName.Equals(connectionDescr.ServerName, StringComparison.InvariantCultureIgnoreCase) &&
                this.DataBase.Equals(connectionDescr.DataBase, StringComparison.InvariantCultureIgnoreCase) &&
                this._connectionString.UserID.Equals(connectionDescr._connectionString.UserID,
                    StringComparison.InvariantCultureIgnoreCase) &&
                this._connectionString.Password.Equals(connectionDescr._connectionString.Password,
                    StringComparison.InvariantCultureIgnoreCase)
                ) {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return _connectionString.GetHashCode();
        }

        public ConnectionDescription Clone() {
            return new ConnectionDescription(this.ServerName, this.DataBase, _connectionString.UserID,
                _connectionString.Password);
        }
    }
}