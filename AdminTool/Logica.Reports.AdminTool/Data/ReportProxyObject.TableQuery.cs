﻿using System.Data;
using System.Data.SqlClient;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.AdminTool.Data {
    /// <summary>
    /// Describe report sheet table queries functionality 
    /// </summary>
    public sealed partial class ReportProxyObject {
        #region Fields

        private static Query _currentQuery;

        public static Query CurrentQuery {
            get { return ReportProxyObject._currentQuery; }
            set { ReportProxyObject._currentQuery = value; }
        }

        #endregion

        #region Query

        public static Query AddNewQuery(SQLQueryType queryType) {
            Query newQuery = new Query();
            newQuery.Name = string.Empty;
            newQuery.SqlQueryType = queryType;
            newQuery.Parameters = new Parameter[0];
            return newQuery;
        }

        public static void SetProcedureName(string procedureName) {
            CurrentQuery.Name = procedureName;
            DataSet ds = Logica.Reports.DataAccess.DataAccessLayer.ExecuteStoredProcedure(
                Logica.Reports.DataAccess.CommonQueries.PRC_GET_PROCEDURE_INFO,
                new SqlParameter[]
                {new SqlParameter(Logica.Reports.DataAccess.CommonQueries.STR_OBJECT_NAME, procedureName)});

            if (ds.Tables.Count > 0) {
                Parameter[] newParameters = new Parameter[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    DataRow dr = ds.Tables[0].Rows[i];
                    Parameter pp = new Parameter();
                    pp.Name = (string) dr[AdminToolCommon.STR_PARAMETER_NAME];
                    pp.DataType = GetDataType((string) dr[AdminToolCommon.STR_PARAMETER_DATA_TYPE]);

                    newParameters[i] = pp;
                }
                if (CurrentQuery.Parameters != null) {
                    foreach (Parameter pOld in CurrentQuery.Parameters) {
                        foreach (Parameter pNew in newParameters) {
                            if (pNew.Name == pOld.Name) {
                                pNew.DefaultValue = pOld.DefaultValue;
                                break;
                            }
                        }
                    }
                }
                CurrentQuery.Parameters = newParameters;
            }
        }

        public static void SetProcedureParametersValue(Query changedQueryType) {
            foreach (Parameter pOld in CurrentQuery.Parameters) {
                foreach (Parameter pNew in changedQueryType.Parameters) {
                    if (pNew.Name == pOld.Name) {
                        pOld.DefaultValue = pNew.DefaultValue;
                        break;
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private static SqlDbType GetDataType(string p) {
            switch (p) {
                case "tinyint":
                    return SqlDbType.TinyInt;
                case "smallint":
                    return SqlDbType.SmallInt;
                case "int":
                    return SqlDbType.Int;
                case "bigint":
                    return SqlDbType.BigInt;
                case "uniqueidentifier":
                    return SqlDbType.UniqueIdentifier;
                case "bit":
                    return SqlDbType.Bit;
                case "datetime":
                    return SqlDbType.DateTime;
                case "smalldatetime":
                    return SqlDbType.SmallDateTime;
                case "decimal":
                    return SqlDbType.Decimal;
                case "image":
                    return SqlDbType.Image;
                default:
                    return SqlDbType.VarChar;
            }
        }

        #endregion
    }
}