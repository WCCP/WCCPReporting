﻿using System;
using System.Configuration;

namespace Logica.Reports.AdminTool.Data {
    public sealed class Configuration {
        public static bool AutoConnect {
            get {
                string confValue = ConfigurationManager.AppSettings["AutoConnect"];
                if (!string.IsNullOrEmpty(confValue)) {
                    return Convert.ToBoolean(confValue);
                }
                return false;
            }
        }

        public static string ServerName {
            get {
                string confValue = ConfigurationManager.AppSettings["ServerName"];
                if (!string.IsNullOrEmpty(confValue)) {
                    return confValue;
                }
                return string.Empty;
            }
        }

        public static string DbName {
            get {
                string confValue = ConfigurationManager.AppSettings["DbName"];
                if (!string.IsNullOrEmpty(confValue)) {
                    return confValue;
                }
                return string.Empty;
            }
        }
    }
}