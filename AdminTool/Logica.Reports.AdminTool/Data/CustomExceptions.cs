﻿using System;

namespace Logica.Reports.AdminTool.Data {
    public class TabHeaderEmptyExceptions : Exception {
        private int _tabIndex;

        public int TabIndex {
            get { return _tabIndex; }
            set { _tabIndex = value; }
        }

        public TabHeaderEmptyExceptions(int tabId) {
            _tabIndex = tabId;
        }

        public override string ToString() {
            return "Set tab header value";
        }
    }

    public class TableHeaderEmptyExceptions : TabHeaderEmptyExceptions {
        private int _tabIndex;
        private int _tableIndex;

        public int TableIndex {
            get { return _tabIndex; }
            set { _tabIndex = value; }
        }

        public int TabIndex {
            get { return _tabIndex; }
            set { _tabIndex = value; }
        }

        public TableHeaderEmptyExceptions(int tabId, int tableId)
            : base(tabId) {
            _tableIndex = tableId;
            _tabIndex = tabId;
        }

        public override string ToString() {
            return "Set table header value";
        }
    }
}