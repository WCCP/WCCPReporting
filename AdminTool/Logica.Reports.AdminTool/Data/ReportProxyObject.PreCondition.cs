﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DevExpress.XtraTab;
using Logica.Reports.AdminTool.UserControls;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.AdminTool.Data {
    /// <summary>
    /// Describe report pre-condition functionality 
    /// </summary>
    public sealed partial class ReportProxyObject {
        #region Fields

        private static Init _curentInit;

        private static XtraTabControl _initTabs;

        private static PreconditionMode _initConditionMode;

        public static PreconditionMode InitConditionMode {
            get { return _initConditionMode; }
            set { _initConditionMode = value; }
        }

        public static Init CurrentInit {
            set { _curentInit = value; }
            get {
                _curentInit = (InitConditionMode == PreconditionMode.PreInit)
                                  ? ConfigReport.Preconditions.PreInit
                                  : ConfigReport.Preconditions.OnInit;
                return _curentInit;
            }
        }

        public static XtraTabControl InitTabs {
            get { return ReportProxyObject._initTabs; }
            set { ReportProxyObject._initTabs = value; }
        }

        public static int CurrentInitIndex {
            get { return InitTabs.TabPages.IndexOf(InitTabs.SelectedTabPage); }
        }

        private static List<SqlProcedureConditionIdentifier> _onInitProcedures =
            new List<SqlProcedureConditionIdentifier>();

        private static string _preInitTablesScript = string.Empty;

        public static List<SqlProcedureConditionIdentifier> OnInitProcedures {
            get { return _onInitProcedures; }
        }

        public static string PreInitTablesScript {
            get { return _preInitTablesScript; }
        }

        #endregion

        #region Public Methods

        public static void SetPreConditions() {
            GetPreIntScript();
            GetOnInitSqlProcedureCondition();
        }

        public static Query AddNewPreConditionQuery(SQLQueryType queryType) {
            int newIndex = 0;
            if (CurrentInit.Queries == null || CurrentInit.Queries.Length == 0) {
                CurrentInit.Queries = new Query[1];
            }
            else {
                newIndex = CurrentInit.Queries.Length;
            }
            Query newQuery =
                AddNewQuery(InitConditionMode == PreconditionMode.OnInit ? SQLQueryType.NonQuery : SQLQueryType.Scalar);
            Query[] arrQuery = new Query[newIndex + 1];
            CurrentInit.Queries.CopyTo(arrQuery, 0);
            CurrentInit.Queries = arrQuery;
            CurrentInit.Queries[newIndex] = newQuery;
            CurrentQuery = CurrentInit.Queries[newIndex];
            return CurrentQuery;
        }

        public static void AddCondition(PreconditionMode mode) {
            int newIndex = CurrentInit.Queries.Length;

            Query tempQuery = new Query();
            tempQuery.Name = string.Empty;
            tempQuery.Parameters = new Parameter[0];
            tempQuery.SqlQueryType = mode == PreconditionMode.OnInit ? SQLQueryType.NonQuery : SQLQueryType.Scalar;

            Query[] newQueries = new Query[CurrentInit.Queries.Length + 1];
            CurrentInit.Queries.CopyTo(newQueries, 0);
            CurrentInit.Queries = newQueries;
            CurrentInit.Queries[newIndex] = tempQuery;

            XtraTabPage tpConditionPage = new XtraTabPage();
            tpConditionPage.Text = string.Format("Condition {0}", InitTabs.TabPages.Count + 1);

            SqlProcParams trcCondition = new SqlProcParams();
            trcCondition.CurrentQuery = tempQuery;
            trcCondition.IsTableQuery = false;
            trcCondition.SetupInitData();
            tpConditionPage.Controls.Add(trcCondition);
            InitTabs.TabPages.Add(tpConditionPage);
        }

        public static void DeleteCondition() {
            int index = InitTabs.TabPages.IndexOf(InitTabs.SelectedTabPage);
            Query[] refreshConditions = new Query[CurrentInit.Queries.Length - 1];

            int validIndex = 0;
            for (int i = 0; i < CurrentInit.Queries.Length; i++) {
                if (i != index) {
                    refreshConditions[validIndex] = CurrentInit.Queries[validIndex];
                    validIndex++;
                }
            }
            CurrentInit.Queries = refreshConditions;
            InitTabs.TabPages.Remove(InitTabs.SelectedTabPage);
        }

        public static void FillPreConditions() {
            if (CurrentInit.Queries == null) {
                CurrentInit.Queries = new Query[0];
            }

            if (CurrentInit.Queries.Length > 0) {
                InitTabs.TabPages.Clear();
                for (int i = 0; i < CurrentInit.Queries.Length; i++) {
                    Query currentQuery = CurrentInit.Queries[i];
                    XtraTabPage tpConditionPage = new XtraTabPage();
                    tpConditionPage.Text = string.Format("Condition {0}", InitTabs.TabPages.Count + 1);
                    SqlProcParams trcCondition = new SqlProcParams();
                    trcCondition.CurrentQuery = currentQuery;
                    trcCondition.IsTableQuery = false;
                    trcCondition.SetupInitData();
                    tpConditionPage.Controls.Add(trcCondition);
                    InitTabs.TabPages.Add(tpConditionPage);
                }
            }
        }

        public static Query SetInitProcedureName(SQLQueryType queryType, string procedureName, Query updatedQuery) {
            if (InitConditionMode == PreconditionMode.OnInit) {
                CurrentInit = ConfigReport.Preconditions.OnInit;
                queryType = SQLQueryType.NonQuery;
            }
            else {
                CurrentInit = ConfigReport.Preconditions.PreInit;
                queryType = SQLQueryType.Scalar;
            }

            int index = CurrentInitIndex;
            if (CurrentInit.Queries == null || CurrentInit.Queries.Length == 0) {
                CurrentInit.Queries = new Query[1];
                CurrentInit.Queries[0] = AddNewQuery(queryType);
            }
            if (updatedQuery != null) {
                CurrentQuery = updatedQuery;
                SetProcedureName(procedureName);
            }
            else {
                foreach (Query initQuery in CurrentInit.Queries) {
                    if (initQuery.SqlQueryType == queryType && string.IsNullOrEmpty(initQuery.Name)) {
                        CurrentQuery = initQuery;
                        SetProcedureName(procedureName);
                        break;
                    }
                }
            }
            return CurrentQuery;
        }

        public static Query SetInitProcedureParametersValue(Query changedQueryType) {
            int index = CurrentInitIndex;
            foreach (Query tableQuery in CurrentInit.Queries) {
                if (tableQuery.Name == changedQueryType.Name) {
                    CurrentQuery = tableQuery;
                    SetProcedureParametersValue(changedQueryType);
                    break;
                }
            }
            return CurrentQuery;
        }

        #endregion

        #region Private Methods

        private static string GetTempTables(Parameter[] paramms, string procedureName) {
            SqlParameter[] parameters = new SqlParameter[paramms.Length];

            for (int i = 0; i < parameters.Length; i++) {
                Parameter pp = paramms[i];
                SqlParameter sp = new SqlParameter(pp.Name, DBNull.Value);
                sp.SqlDbType = pp.DataType;
                sp.Value = pp.DefaultValue;
                parameters[i] = sp;
            }

            return (string) Logica.Reports.DataAccess.DataAccessLayer.ExecuteScalarStoredProcedure(
                procedureName, parameters);
        }

        private static void GetOnInitSqlProcedureCondition() {
            _onInitProcedures.Clear();
            foreach (Query q in _configReport.Preconditions.OnInit.Queries) {
                SqlProcedureConditionIdentifier spc = new SqlProcedureConditionIdentifier();
                spc.ProcedureName = q.Name;
                int countParams = q.Parameters.Length;
                spc.Parameters = new SqlParameter[countParams];
                for (int i = 0; i < countParams; i++) {
                    SqlParameter sqlParam = new SqlParameter();
                    sqlParam.ParameterName = q.Parameters[i].Name;
                    sqlParam.SqlDbType = q.Parameters[i].DataType;
                    sqlParam.Value = string.IsNullOrEmpty(q.Parameters[i].DefaultValue)
                                         ? null
                                         : q.Parameters[i].DefaultValue;
                    spc.Parameters[i] = sqlParam;
                }
                _onInitProcedures.Add(spc);
            }
        }

        private static void GetPreIntScript() {
            StringBuilder sb = new StringBuilder();
            foreach (Query q in _configReport.Preconditions.PreInit.Queries) {
                sb.AppendLine(GetTempTables(q.Parameters, q.Name));
            }
            _preInitTablesScript = sb.ToString();
        }

        #endregion
    }
}