﻿using System;
using System.Windows.Forms;
using Logica.Reports.AdminTool.UserControls;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.AdminTool.Data {
    /// <summary>
    /// Describe report sheet functionality 
    /// </summary>
    public sealed partial class ReportProxyObject {
        #region Fields

        private static int _currentSheetIndex = 0;
        private static TabControl _sheetTabs;

        public static int CurrentSheetIndex {
            set { _currentSheetIndex = value; }
            get { return _currentSheetIndex; }
        }

        public static TabControl SheetTabs {
            get { return ReportProxyObject._sheetTabs; }
            set { ReportProxyObject._sheetTabs = value; }
        }

        #endregion

        #region ReportSheet

        internal static void FillReportSheets() {
            ReportProxyObject.SetPreConditions();
            SetupPreConditions();

            if (ReportProxyObject.ConfigReport.Tabs.Length > 0) {
                SheetTabs.TabPages.Clear();
                for (int tabIndex = 0; tabIndex < ReportProxyObject.ConfigReport.Tabs.Length; tabIndex++) {
                    Tab currentTab = ReportProxyObject.ConfigReport.Tabs[tabIndex];
                    if (currentTab.Id == Guid.Empty) {
                        currentTab.Id = Guid.NewGuid();
                    }
                    TabPage tpReportTabPage = new TabPage();

                    tpReportTabPage.Text = string.IsNullOrEmpty(currentTab.Header)
                                               ? string.Format("Tab_{0}", SheetTabs.TabPages.Count + 1)
                                               : currentTab.Header;
                    TabReportConfigurator trcTabReportConfigurator = new TabReportConfigurator(SheetTabs.TabPages.Count);
                    SheetTabs.TabPages.Add(tpReportTabPage);
                    trcTabReportConfigurator.SheetHeader = tpReportTabPage.Text;
                    trcTabReportConfigurator.IsFitToPageForPrinting = currentTab.PrintingFitToPage;
                    trcTabReportConfigurator.SetupTabTable(currentTab.Tables);
                    tpReportTabPage.Controls.Add(trcTabReportConfigurator);
                }
            }
            else {
                Tab currentTab = CreateTab(0, "Tab_0");
                if (currentTab.Id == Guid.Empty) {
                    currentTab.Id = Guid.NewGuid();
                }
                TabPage tpReportTabPage = new TabPage();

                tpReportTabPage.Text = string.IsNullOrEmpty(currentTab.Header)
                                           ? string.Format("Tab_{0}", SheetTabs.TabPages.Count + 1)
                                           : currentTab.Header;
                TabReportConfigurator trcTabReportConfigurator = new TabReportConfigurator(SheetTabs.TabPages.Count);
                SheetTabs.TabPages.Add(tpReportTabPage);
                trcTabReportConfigurator.SheetHeader = tpReportTabPage.Text;
                trcTabReportConfigurator.IsFitToPageForPrinting = currentTab.PrintingFitToPage;
                trcTabReportConfigurator.SetupTabTable(currentTab.Tables);
                tpReportTabPage.Controls.Add(trcTabReportConfigurator);
            }
            Logica.Reports.DataAccess.DataAccessLayer.CloseConnection();
            _preConditionsExists = false;
        }

        internal static void AddReportSheet() {
            int newIndex = ReportProxyObject.ConfigReport.Tabs.Length;
            string tabName = string.Format("Tab_{0}", SheetTabs.TabPages.Count + 1);
            Tab tb = CreateTab(newIndex, tabName);

            Tab[] addTab = new Tab[ReportProxyObject.ConfigReport.Tabs.Length + 1];
            ReportProxyObject.ConfigReport.Tabs.CopyTo(addTab, 0);
            ReportProxyObject.ConfigReport.Tabs = addTab;
            ReportProxyObject.ConfigReport.Tabs[newIndex] = tb;

            TabPage tpReportTabPage = new TabPage();
            tpReportTabPage.Text = tabName;

            TabReportConfigurator trcTabReportConfigurator = new TabReportConfigurator(SheetTabs.TabPages.Count);
            trcTabReportConfigurator.SheetHeader = tpReportTabPage.Text;
            trcTabReportConfigurator.IsFitToPageForPrinting = false;
            trcTabReportConfigurator.SetupTabTable(tb.Tables);
            tpReportTabPage.Controls.Add(trcTabReportConfigurator);
            SheetTabs.TabPages.Add(tpReportTabPage);
        }

        private static Tab CreateTab(int newIndex, string tabName) {
            Tab tb = new Tab();
            tb.Header = tabName;
            tb.Id = Guid.NewGuid();
            tb.Order = newIndex;
            tb.Tables = new Table[2];
            tb.Tables[0] = AddReportTable(TableType.Plan);
            tb.Tables[1] = AddReportTable(TableType.Fact);
            return tb;
        }

        internal static void DeleteReportSheet() {
            Tab[] refreshTabs = new Tab[ReportProxyObject.ConfigReport.Tabs.Length - 1];

            int validIndex = 0;
            for (int i = 0; i < ReportProxyObject.ConfigReport.Tabs.Length; i++) {
                if (i != _currentSheetIndex) {
                    refreshTabs[validIndex] = ReportProxyObject.ConfigReport.Tabs[i];
                    validIndex++;
                }
            }
            ReportProxyObject.ConfigReport.Tabs = refreshTabs;

            SheetTabs.TabPages.Remove(SheetTabs.SelectedTab);

            for (int i = 0; i < SheetTabs.TabPages.Count; i++) {
                TabPage tp = SheetTabs.TabPages[i];
                foreach (Control c in tp.Controls) {
                    if (c.GetType() == typeof (TabReportConfigurator)) {
                        ((TabReportConfigurator) c).ReportTabIndex = i;
                        break;
                    }
                }
            }
        }

        internal static void SetReportSheetHeader(int tabIndex, string tabName) {
            ConfigReport.Tabs[tabIndex].Header = tabName;
            SheetTabs.TabPages[tabIndex].Text = tabName;
        }

        internal static Table[] GetSheetTables() {
            return ConfigReport.Tabs[CurrentSheetIndex].Tables;
        }

        #endregion
    }
}