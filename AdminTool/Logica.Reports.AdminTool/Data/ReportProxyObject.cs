﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.AdminTool.Data {
    /// <summary>
    /// Describe main report management functionality 
    /// </summary>
    public sealed partial class ReportProxyObject {
        #region Fields

        private static bool _preConditionsExists = false;
        private static Report _configReport = new Report();

        public static Report ConfigReport {
            get { return _configReport; }
            set { _configReport = value; }
        }

        #endregion

        #region Public Methods

        public static void InitXml(string content) {
            CleareReportObject();
            _configReport = XmlParserManager.ConvertXmlToModel(content);
        }

        public static string GetConfigXml {
            get {
                string result = string.Empty;
                using (StringWriter configStreamWriter = new StringWriter()) {
                    XmlSerializer x = new XmlSerializer(typeof (Report));
                    x.Serialize(configStreamWriter, _configReport);
                    result = configStreamWriter.ToString();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(result);
                    XmlNodeList list1 = doc.GetElementsByTagName("grid");
                    foreach (XmlNode node in list1)
                        node.InnerXml = node.InnerText;
                    XmlNodeList list2 = doc.GetElementsByTagName("chart");
                    foreach (XmlNode node in list2)
                        node.InnerXml = node.InnerText;

                    using (StringWriter ms = new StringWriter()) {
                        XmlWriterSettings xws = new XmlWriterSettings {Indent = true};
                        XmlWriter xw = XmlWriter.Create(ms, xws);
                        doc.WriteTo(xw);
                        xw.Flush();
                        result = ms.ToString();
                    }
                    //result = doc.InnerXml;
                }
                CleareReportObject();
                return result;
            }
        }

        #endregion

        #region Private Methods

        private static void CleareReportObject() {
            _configReport = new Report();
            _onInitProcedures = new List<SqlProcedureConditionIdentifier>();
            _sheetTabs = null;
            _tablePlanTabs = null;
            _tableFactTabs = null;
            _initTabs = null;
            _preInitTablesScript = string.Empty;
            _curentInit = new Init();
            _currentQuery = new Query();
            _currentSheetIndex = 0;
        }

        #endregion

        internal static DataTable GetResultSetTable() {
            DataTable resultTable = new DataTable();
            bool executePreconditions = false;
            if (!_preConditionsExists) {
                SetupPreConditions();
                executePreconditions = true;
            }

            return resultTable;

            if (CurrentSheetIndex >= 0 && CurrentSheetIndex < ConfigReport.Tabs.Count()) {
                var queries =
                    ConfigReport.Tabs[CurrentSheetIndex].Tables.Where(t => t.Mode == ShowMode.Invisible).SelectMany(
                        t => t.Queries.Where(q => q.SqlQueryType == SQLQueryType.DataSet)).ToArray();
                foreach (var query in queries) {
                    var par = query.Parameters.Select(t => t.ConvertToSqlParameter()).ToList();
                    foreach (var t in CurrentQuery.Parameters) {
                        var found = par.Where(p => p.ParameterName == t.Name).FirstOrDefault();
                        if (found != null) {
                            int itemIndex = par.IndexOf(found);
                            par[itemIndex] = t.ConvertToSqlParameter();
                        }
                    }
                    DataAccess.DataAccessLayer.ExecuteStoredProcedure(query.Name, par.ToArray());
                }
            }

            if (CurrentTable.Mode == ShowMode.Invisible) {
                return resultTable;
            }

            SqlParameter[] paramms = new SqlParameter[CurrentQuery.Parameters.Length];

            for (int i = 0; i < CurrentQuery.Parameters.Length; i++) {
                paramms[i] = CurrentQuery.Parameters[i].ConvertToSqlParameter();
            }
            try {
                resultTable = DataAccess.DataAccessLayer.ExecuteStoredProcedure(
                    CurrentQuery.Name, paramms).Tables[0];
            }
            catch (Exception exception) {
                MessageBox.Show("Please set default value for all required parameters\n{0}", exception.Message);
                return resultTable;
            }
            if (executePreconditions) {
                DataAccess.DataAccessLayer.CloseConnection();
                _preConditionsExists = false;
            }
            return resultTable;
        }

        private static void SetupPreConditions() {
            _preConditionsExists = true;
            return;
           
            DataAccess.DataAccessLayer.OpenConnection();
            if (PreInitTablesScript != string.Empty) {
                DataAccess.DataAccessLayer.ExecuteNonQuery(PreInitTablesScript, new SqlParameter[0]);
            }
            if (OnInitProcedures.Count > 0) {
                for (int i = 0; i < OnInitProcedures.Count; i++) {
                    DataAccess.DataAccessLayer.ExecuteNonQueryStoredProcedure(OnInitProcedures[i].ProcedureName,
                        OnInitProcedures[i].Parameters);
                }
            }
        }
    }
}