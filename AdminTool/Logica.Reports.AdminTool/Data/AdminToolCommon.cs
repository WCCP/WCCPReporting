﻿using System;
using System.IO;
using Logica.Reports.Common;

namespace Logica.Reports.AdminTool.Data
{
    public delegate void SetProcedureNameChangedEventHandler(object sender, EventArgs e);
    public delegate void SetProcedureNameEventHandler(object sender, EventArgs e, string procaName, SQLQueryType qType);
    public delegate void CallProcedureNameChangesEventHandler(object sender, EventArgs e, string procSelectName, string procModifyName);
    public delegate void CallActionRefreshEventHandler(object sender, EventArgs e);

    public enum PreconditionMode {
        PreInit,
        OnInit
    }

    public enum FieldType {
        DateTime,
        Int,
        BigInt,
        Boolean,
        String,
        Decimal
    }

    public enum FieldBoolean {
        True,
        False
    }

    public enum TypeTable {
        Plan,
        Fact
    }

    public class AdminToolCommon {
        public const int IS_ACTIVE_REPORT = 0;
        public const int IS_ACTIVE_COMMON = 1;

        public const int OBJECT_GROUP_ID_REPORT = 11;
        public const int OBJECT_GROUP_ID_COMMON = 12;

        public const int OBJECT_TYPE_ID_REPORT = 1;
        public const int OBJECT_TYPE_ID_COMMON = 4;

        public const string STR_PARAMETER_NAME = "ParameterName";
        public const string STR_PARAMETER_DATA_TYPE = "ParameterDataType";

        public const string TXT_INIT_REPORT_CONFIG = @"<report exporthtml=""false"" exportmht=""false"" exportpdf=""false"" exportrtf=""true"" exporttxt=""false"" exportxls=""true"" exportxlsx=""true"" exportcsv=""false"" exportimage=""false"">
    <preconditions>
        <preinit>
            <queries/>
        </preinit>
        <oninit>
            <queries/>
        </oninit>
    </preconditions>
    <tabs>
        <tab order=""0"">
            <header></header>
            <tables>
            </tables>
       </tab>
    </tabs>
</report>";

        //57d4800d-1ab2-4302-a770-4d8de2d93c4a
        //8cfd6ecb-00bc-4634-b962-1f2f4d237423

        public static byte[] GetBytes(FileStream stream) {
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, (int) stream.Length);
            stream.Close();

            return buffer;
        }
    }
}
