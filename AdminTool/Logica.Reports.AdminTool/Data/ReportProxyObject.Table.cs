﻿using System;
using System.Windows.Forms;
using Logica.Reports.AdminTool.UserControls;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.AdminTool.Data {
    /// <summary>
    /// Describe report sheet table functionality 
    /// </summary>
    public sealed partial class ReportProxyObject {
        #region Fields

        private static Table _curentTable;
        private static TabControl _tablePlanTabs;
        private static TabControl _tableFactTabs;

        public static Body CurrentBody {
            get {
                if (CurrentTable.Body == null) {
                    CurrentTable.Body = new Body();
                }
                return CurrentTable.Body;
            }
        }

        public static Table CurrentTable {
            set { _curentTable = value; }
            get { return _curentTable; }
        }

        public static TabControl PlanTableTabs {
            get { return ReportProxyObject._tablePlanTabs; }
            set { ReportProxyObject._tablePlanTabs = value; }
        }

        public static TabControl FactTableTabs {
            get { return ReportProxyObject._tableFactTabs; }
            set { ReportProxyObject._tableFactTabs = value; }
        }

        #endregion

        #region SheetTable

        public static void AddSheetTable(TableType tabType, CallActionRefreshEventHandler action) {
            int newIndex = ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length;
            int tablePagesCount = tabType == TableType.Plan
                                      ? PlanTableTabs.TabPages.Count
                                      : FactTableTabs.TabPages.Count;
            string tabName = string.Format("Table_{0}", tablePagesCount + 1);

            Table tb = AddReportTable(tabType);
            tb.Header = tabName;
            tb.Id = Guid.NewGuid();
            Table[] addTable = new Table[ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length + 1];
            ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.CopyTo(addTable, 0);
            ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables = addTable;
            ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables[newIndex] = tb;
            AddTableTab(tabType, tb, tabName, action);
        }

        public static Table GetTableById(Guid identifier) {
            if (ReportProxyObject.ConfigReport.Tabs.Length != 0) {
                for (int i = 0; i < ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length; i++) {
                    Table seletedTable = ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables[i];
                    if (seletedTable.Id == identifier) {
                        return seletedTable;
                    }
                }
            }
            return new Table();
        }

        public static void AddTableTab(TableType tabType, Table tb, string tabName, CallActionRefreshEventHandler action) {
            TabPage tpTablePage = new TabPage();
            tpTablePage.Text = tabName;

            ManageGrid ucmg = new ManageGrid();
            ucmg.HideUnusedTabs(tb.Type);
            ucmg.OnActionRised += new CallActionRefreshEventHandler(action);
            ucmg.CurrentTable = tb;
            tpTablePage.Controls.Add(ucmg);

            if (tabType == TableType.Plan) {
                PlanTableTabs.TabPages.Add(tpTablePage);
            }
            else {
                FactTableTabs.TabPages.Add(tpTablePage);
            }
        }

        public static void SetTableTabIndex(Guid identifier, int index) {
            CurrentTable = GetTableById(identifier);
            CurrentTable.TabIndex = index;
        }

        public static void SetTableShowMode(Guid identifier, ShowMode mode) {
            CurrentTable = GetTableById(identifier);
            CurrentTable.Mode = mode;
        }

        public static void SetTableChartPosition(Guid identifier, ChartPosition position) {
            CurrentTable = GetTableById(identifier);
            CurrentTable.Position = position;
        }

        public static void SetTableGridPosition(Guid identifier, GridPosition gridPosition) {
            CurrentTable = GetTableById(identifier);
            CurrentTable.GridPosition = gridPosition;
        }

        public static void SetTableDisplay(Guid identifier, DisplayType display) {
            CurrentTable = GetTableById(identifier);
            CurrentTable.Display = display;
        }

        internal static void SetTableGridXml(Guid identifier, string tableGridXml) {
            CurrentTable = GetTableById(identifier);
            if (CurrentTable.Body == null) {
                CurrentTable.Body = new Body();
            }
            CurrentTable.Body.GridXml = tableGridXml;
        }

        internal static void SetTableChartXml(Guid identifier, string tableChartXml) {
            CurrentTable = GetTableById(identifier);
            if (CurrentTable.Body == null) {
                CurrentTable.Body = new Body();
            }
            CurrentTable.Body.ChartXml = tableChartXml;
        }

        public static void DeleteSheetTable(TableType tableType, int index, Guid identifier) {
            Table[] refreshTables = new Table[ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length - 1];

            int validIndex = 0;
            for (int i = 0; i < ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length; i++) {
                Table tbNotDeleted = ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables[i];
                if (tbNotDeleted.Id != identifier) {
                    refreshTables[validIndex] = tbNotDeleted;
                    validIndex++;
                }
            }

            ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables = refreshTables;

            if (tableType == TableType.Plan) {
                PlanTableTabs.TabPages.Remove(PlanTableTabs.SelectedTab);
            }
            else {
                FactTableTabs.TabPages.Remove(FactTableTabs.SelectedTab);
            }
        }

        public static void SetSheetTableHeader(TableType tableType, string tabName, Guid identifier) {
            CurrentTable = GetTableById(identifier);
            CurrentTable.Header = tabName;

            if (tableType == TableType.Plan) {
                PlanTableTabs.SelectedTab.Text = tabName;
            }
            else {
                FactTableTabs.SelectedTab.Text = tabName;
            }
        }

        public static void SetTableHideHeader(bool hideHeader, Guid identifier, TableType tabType) {
            CheckTableExists(identifier, tabType);
            CurrentTable.HideHeader = hideHeader;
        }

        public static Query AddNewTableQuery(SQLQueryType queryType) {
            int newIndex = 0;
            if (CurrentTable.Queries == null) {
                CurrentTable.Queries = new Query[1];
            }
            else {
                newIndex = CurrentTable.Queries.Length;
            }
            Query newQuery = AddNewQuery(queryType);
            Query[] arrQuery = new Query[newIndex + 1];
            CurrentTable.Queries.CopyTo(arrQuery, 0);
            CurrentTable.Queries = arrQuery;
            CurrentTable.Queries[newIndex] = newQuery;
            CurrentQuery = CurrentTable.Queries[newIndex];
            return CurrentQuery;
        }

        internal static void CheckTableExists(Guid identifier, TableType tabType) {
            if (ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length != 0) {
                for (int i = 0; i < ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length; i++) {
                    Table seletedTable = ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables[i];
                    if (seletedTable.Id == identifier) {
                        CurrentTable = seletedTable;
                        return;
                    }
                }
            }

            int newIndex = ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length;
            int tablePagesCount = tabType == TableType.Plan
                                      ? PlanTableTabs.TabPages.Count
                                      : FactTableTabs.TabPages.Count;
            string tabName = string.Format("Table_{0}", tablePagesCount + 1);

            Table tb = AddReportTable(tabType);
            tb.Header = tabName;

            tb.Id = identifier;
            Table[] addTable = new Table[ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length + 1];
            ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.CopyTo(addTable, 0);
            ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables = addTable;
            ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables[newIndex] = tb;
            CurrentTable = ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables[newIndex];
        }

        public static Query SetSheetTableProcedureName(Guid identifier, SQLQueryType queryType, string procedureName,
                                                       TableType tabType) {
            CheckTableExists(identifier, tabType);
            bool commited = false;
            int index = CurrentSheetIndex;
            if (CurrentTable.Queries == null || CurrentTable.Queries.Length == 0) {
                AddNewTableQuery(queryType);
                SetProcedureName(procedureName);
                return CurrentQuery;
            }
            foreach (Query tableQuery in CurrentTable.Queries) {
                if (tableQuery.SqlQueryType == queryType) {
                    CurrentQuery = tableQuery;
                    SetProcedureName(procedureName);
                    commited = true;
                    break;
                }
            }
            if (!commited) {
                AddNewTableQuery(queryType);
                SetProcedureName(procedureName);
            }
            return CurrentQuery;
        }

        internal static void ChangeEnabledButtonStatuses(ref int planTables, ref int factTables) {
            if (ReportProxyObject.ConfigReport.Tabs.Length != 0) {
                planTables = 0;
                factTables = 0;
                for (int i = 0; i < ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables.Length; i++) {
                    Table tb = ReportProxyObject.ConfigReport.Tabs[CurrentSheetIndex].Tables[i];
                    if (tb != null) {
                        if (tb.Type == TableType.Plan) {
                            if (tb.Queries != null) {
                                planTables++;
                            }
                        }
                        else {
                            if (tb.Queries != null) {
                                factTables++;
                            }
                        }
                    }
                }
            }
        }

        public static Query SetSheetTableProcedureParametersValue(Guid identifier, Query changedQueryType) {
            CurrentTable = GetTableById(identifier);
            foreach (Query tableQuery in CurrentTable.Queries) {
                if (tableQuery.Name == changedQueryType.Name) {
                    CurrentQuery = tableQuery;
                    SetProcedureParametersValue(changedQueryType);
                    break;
                }
            }
            return CurrentQuery;
        }

        #endregion

        #region Private Methods

        private static Table AddReportTable(TableType tabType) {
            Table tempTable = new Table();
            tempTable.Id = Guid.NewGuid();
            tempTable.Mode = ShowMode.Grid;
            tempTable.Position = ChartPosition.Bottom;
            tempTable.TabIndex = 0;
            tempTable.Type = tabType;
            tempTable.Body = new Logica.Reports.ConfigXmlParser.Model.Body();
            tempTable.Body.ChartXml = string.Empty;
            tempTable.Body.GridXml = string.Empty;
            return tempTable;
        }

        #endregion
    }
}