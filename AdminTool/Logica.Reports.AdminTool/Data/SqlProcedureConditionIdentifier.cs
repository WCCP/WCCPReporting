﻿using System.Data.SqlClient;

namespace Logica.Reports.AdminTool.Data {
    public struct SqlProcedureConditionIdentifier {
        private string _procedureName;
        private SqlParameter[] _parameters;

        public SqlParameter[] Parameters {
            get { return _parameters; }
            set { _parameters = value; }
        }

        public string ProcedureName {
            get { return _procedureName; }
            set { _procedureName = value; }
        }
    }
}