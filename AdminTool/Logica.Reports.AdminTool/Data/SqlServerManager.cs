﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Windows.Forms;
using Logica.Reports.DataAccess;

namespace Logica.Reports.AdminTool.Data {
    internal enum TablesColumn {
        TABLE_QUALIFIER,
        TABLE_OWNER,
        TABLE_NAME
    }

    public class SqlServerManager {
        //private static string CONNECTION_STRING_TEMPLATE = "Data Source={0};Initial Catalog={1};Integrated Security=True";
        //private static string CONNECTION_STRING_USER_PART = ";User ID={0};Password={1}";
        private static string LOGICA_DB_NAME = "LDB";
        private static string ERROR_MESG_CANNOT_GET_DARABASES = "Can not get datatbases names!";

        private static SqlConnectionStringBuilder _connectionString;
        private static string COLUMN_NAME_DB_NAME = "DATABASE_NAME";
        private List<string> _servers;
        private static SqlServerManager _current;
        private List<ConnectionDescription> _connectionDescriptions = new List<ConnectionDescription>();
        private Dictionary<string, List<string>> _databases = new Dictionary<string, List<string>>();

        private SqlServerManager() {
        }

        #region Public methods

        public static SqlServerManager Current {
            get {
                if (null == _current) {
                    _current = new SqlServerManager();
                }
                return _current;
            }
        }

        public List<string> GetServerNames() {
            _servers = new List<string>();

            SqlDataSourceEnumerator srvs = SqlDataSourceEnumerator.Instance;
            DataTable dt = srvs.GetDataSources();
            if (dt.Rows.Count > 0) {
                foreach (DataRow r in dt.Rows) {
                    string instanceName = r[dt.Columns["InstanceName"]].ToString();
                    _servers.Add(r[dt.Columns["ServerName"]].ToString()
                                 + (string.IsNullOrEmpty(instanceName) ? "" : "\\" + instanceName));
                }
            }
            return _servers;
        }

        public List<string> GetDatabases(string serverName) {
            return GetDatabases(serverName, null, null);
        }

        public List<string> GetDatabases(string serverName, string userId, string password) {
            if (_databases.ContainsKey(serverName))
                return _databases[serverName];

            List<string> databeses = new List<string>();
            string errorMessage = null;
            //try
            //{
            //    DataAccessLayer.SetConnectionString(ConnectionStringMake(serverName, "master", userId, password));
            //}
            //catch (ConnectionFailedException connectionFailedEx)
            //{
            //    errorMessage = connectionFailedEx.Message;
            //}
            //catch (Exception ex)
            //{
            //    errorMessage = ex.Message;
            //}

            if (null != errorMessage) {
                MessageBox.Show(errorMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            databeses = GetDatabasesList(serverName, userId, password);

            _databases.Add(serverName, databeses);

            if (null == databeses) {
                MessageBox.Show(ERROR_MESG_CANNOT_GET_DARABASES, Application.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return null;
            }
            return databeses;
        }

        public List<string> GetSWDatabases(string serverName, string userId, string password) {
            List<string> dbNames = GetDatabases(serverName);
            List<string> swDbNames = new List<string>();

            if (null != dbNames) {
                foreach (string dbName in dbNames) {
                    if (!dbName.Equals(LOGICA_DB_NAME, StringComparison.InvariantCultureIgnoreCase)) {
                        string connectionString = ConnectionStringMake(serverName, dbName, userId, password);
                        object res = DataAccessLayer.ExecuteScalarQuery(CommonQueries.SQL_CHECK_DB_IS_SW,
                            connectionString);
                        if (null != res) {
                            swDbNames.Add(dbName.Clone().ToString());
                        }
                    }
                }
            }
            return swDbNames;
        }

        public bool CheckConnection(string serverName, string database, out string errorMessage) {
            return CheckConnection(serverName, database, null, null, out errorMessage);
        }

        public bool CheckConnection(string serverName, string database, string userName, string password,
                                    out string errorMessage) {
            errorMessage = "";
            Exception ex;

            string connectionString;

            try {
                connectionString = ConnectionStringMake(serverName, database, userName, password);
            }
            catch (Exception connectionEx) {
                errorMessage = connectionEx.Message;
                return false;
            }

            ex = DataAccessLayer.CheckConnectionString(connectionString);
            if (ex != null) {
                errorMessage = ex.Message;
            }

            return ex == null;
        }

        internal void AddConnection(ConnectionDescription connectionDescription) {
            foreach (ConnectionDescription  item in _connectionDescriptions) {
                if (item.Equals(connectionDescription))
                    return;
            }
            _connectionDescriptions.Add(connectionDescription);
            DataAccessLayer.SetConnectionString(connectionDescription.SqlConnectionString.ConnectionString);
        }

        internal void RemoveConnection(ConnectionDescription connectionDescription) {
            foreach (ConnectionDescription  item in _connectionDescriptions) {
                if (item.Equals(connectionDescription)) {
                    _connectionDescriptions.Remove(item);
                    return;
                }
            }
        }

        internal void RemoveConnectionByValues(string srvName, string batabaseName) {
            foreach (ConnectionDescription item in _connectionDescriptions) {
                if (item.ServerName.Equals(srvName, StringComparison.InvariantCultureIgnoreCase) &&
                    item.DataBase.Equals(batabaseName, StringComparison.InvariantCultureIgnoreCase)) {
                    _connectionDescriptions.Remove(item);
                    return;
                }
            }
        }

        internal void RemoveAllConnection() {
            _connectionDescriptions.Clear();
        }

        internal List<ConnectionDescription> SqlConnections {
            get { return _connectionDescriptions; }
        }

        #region Legacy

        //public bool SetConnectionString(string serverName, string database)
        //{
        //    string errorMessage;
        //    bool result = CheckConnection(serverName, database, out errorMessage);
        //    if (result)
        //    {
        //        DataAccessLayer.SetConnectionString(ConnectionStringMake(serverName, database, null, null));
        //    }
        //    else
        //    {
        //        DataAccessLayer.SetConnectionStringToDefault();
        //    }

        //    return result;
        //}

        //public bool SetConnectionString(string serverName, string database, string userName, string password)
        //{
        //    string errorMessage;
        //    bool result = CheckConnection(serverName, database, userName, password, out errorMessage);
        //    if (result)
        //    {
        //        DataAccessLayer.SetConnectionString(ConnectionStringMake(serverName, database, userName, password));
        //    }
        //    else
        //    {
        //        DataAccessLayer.SetConnectionStringToDefault();
        //    }

        //    return result;
        //}

        #endregion

        public void SetConnectionStringToDefault() {
            DataAccessLayer.SetConnectionStringToDefault();
        }

        public SqlConnectionStringBuilder ConnectionString {
            get { return DataAccessLayer.ConnectionString; }
        }

        public List<string> GetDatabasesList(string serverName, string userId, string password) {
            List<string> dbList = new List<string>();
            try {
                string connectionString = ConnectionStringMake(serverName, "model", userId, password);
                DataSet ds = DataAccessLayer.ExecuteQuery(CommonQueries.SQL_GET_DATABASES, connectionString);
                DataTable dt = ds.Tables[0];
                foreach (DataRow r in dt.Rows) {
                    dbList.Add(r[COLUMN_NAME_DB_NAME].ToString());
                }
            }
            catch {
                return null;
            }

            return dbList;
        }

        internal DataTable GetTablesName(ConnectionDescription sqlConnection) {
            DataSet ds = DataAccessLayer.ExecuteQuery(CommonQueries.SQL_GET_TABLES,
                sqlConnection.SqlConnectionString.ToString());
            if (null != ds) {
                DataTable dt = ds.Tables[0];
                return dt;
            }
            return null;
        }

        #endregion

        public static string LogicaDatabaseName {
            get { return LOGICA_DB_NAME; }
        }

        #region Private

        private string ConnectionStringMake(string serverName, string database, string userName, string password) {
            _connectionString = new SqlConnectionStringBuilder();
            _connectionString.DataSource = serverName;
            _connectionString.InitialCatalog = database;
            _connectionString.IntegratedSecurity = string.IsNullOrEmpty(userName);

            if (!string.IsNullOrEmpty(userName))
                _connectionString.UserID = userName;
            if (!string.IsNullOrEmpty(password))
                _connectionString.Password = password;

            return _connectionString.ConnectionString;
        }

        #endregion
    }
}