﻿using System;
using System.Runtime.InteropServices;

namespace UrmMolule
{
    public static class UrmInterface
    {
        [DllImport("wccpadmin.dll", EntryPoint = "ShowAdminForm", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int RunAdmin(IntPtr handle);
    }
}
