﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UrmMolule
{
    public partial class AdminModuleCover : Form
    {
        public AdminModuleCover()
        {
            InitializeComponent();
        }

        private void AdminModuleCover_Shown(object sender, EventArgs e)
        {
            Close();
        }

        private void AdminModuleCover_Load(object sender, EventArgs e)
        {
            try
            {
                UrmInterface.RunAdmin(this.Handle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка при загрузке консоли администратора", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
