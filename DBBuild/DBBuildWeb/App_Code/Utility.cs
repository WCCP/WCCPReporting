﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using DBBuild;

public class Utility
{
    public static void SendGeneratedFile(HttpResponse httpResponse, ScriptGenerator sg, string projPrefix)
    {
        httpResponse.Clear();

        string archiveName = String.Format("archive-{0}-{1}.zip", projPrefix, DateTime.Now.ToString("yyyyMMddHHmmss"));
        httpResponse.ContentType = "application/zip";
        httpResponse.AddHeader("content-disposition", "filename=" + archiveName);
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString))
            {
                sg.SaveAsArchiveToStream(conn, projPrefix, httpResponse.OutputStream);
            }
        }
        catch (Exception ex)
        {
            httpResponse.Clear();
            httpResponse.ContentType = "text/plain";
            httpResponse.Output.Write(ex.Message);
        }

        httpResponse.End();
    }
}