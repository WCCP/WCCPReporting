﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBBuild;
using DBBuild.DataAccess;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Parameter = System.Web.UI.WebControls.Parameter;

#endregion

public partial class DeploymentRequests : Page
{
    #region Fields

    private string currentLoggingFile;

    #endregion

    #region Instance Methods

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
            return;

        if (Request.QueryString["p"] != null)
        {
            string ProjectID;

            ProjectID = Request.QueryString["p"];

            dlProjecstList.DataBind();
            dlProjecstList.Items[dlProjecstList.SelectedIndex].Selected = false;
            dlProjecstList.Items.FindByValue(ProjectID).Selected = true;

            dlDeploymentRequests.DataBind();

            if (Request.QueryString["dr"] != null)
            {
                int DRID = Int32.Parse(Request.QueryString["dr"]);

                for (int i = 0; i <= dlDeploymentRequests.DataKeys.Count - 1; i++)
                {
                    if ((int)dlDeploymentRequests.DataKeys[i].Value == DRID)
                    {
                        dlDeploymentRequests.SelectedIndex = i;
                        break;
                    }
                }
            }

        }
    }

    protected void btnDeploy_Click(object sender, EventArgs e)
    {
        if (dlProjecstList.SelectedIndex == -1 || dlDeploymentRequests.SelectedIndex == -1)
            return;
        int projectId = Convert.ToInt32(dlProjecstList.SelectedValue);
        int drId = Convert.ToInt32(dlDeploymentRequests.SelectedValue);
        int envId = Convert.ToInt32(ddEnvironment.SelectedValue);

        //todo: replace "" with DBName

        EnvironmentEntity env;
        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
        ScriptGenerator sg = new ScriptGenerator(connStr, projectId, drId, "");

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString))
        {
            env = EnvironmentEntity.GetById(conn, envId);
        }

        if (!sg.DeploymentRequest.IncludeAllFiles && dlDPRFiles.Rows.Count == 0)
            return;

        ExecuteDeployment(sg, env);

        dlDPRHistory.DataBind();
        if (dlDPRHistory.Rows.Count > 0)
            dlDPRHistory.SelectedIndex = 0;
    }

    private void ExecuteDeployment(ScriptGenerator sg, EnvironmentEntity env)
    {
        ResultScript[] res = null;
        try
        {
            res = sg.Generate();
        }
        catch (Exception ex)
        {
            tbErrorMsg.Text = ex.Message;
            ppcErrorMsgMPE.Show();
        }

        if (res == null || res.Length == 0)
        {
            return;
        }

        var logDirectory = CreateLogDirectoryStructure(sg.Project, sg.DeploymentRequest);
        string connStr2 = DataAccess.GetConnectionString(env.Server, env.DataBase);

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (SqlConnection conn = new SqlConnection(connStr2))
                {
                    conn.Open();
                    Server server = new Server(new ServerConnection(conn));
                    server.ConnectionContext.InfoMessage += ConnectionContext_InfoMessage;
                    //server.ConnectionContext.SqlConnectionObject.FireInfoMessageEventOnUserErrors = true;
                    try
                    {
                        string projectNamePrefix = string.Format("{0}_{1}", sg.Project.ProjectName,
                                                                 sg.DeploymentRequest.DRName);
                        foreach (var option in new[] { "ON", "OFF" })
                        {
                            string sqlOption = string.Format("SET PARSEONLY {0}", option, Environment.NewLine);
                            server.ConnectionContext.ExecuteNonQuery(sqlOption);

                            int scriptNum = 0;

                            foreach (ResultScript script in res.OrderBy(i => i.ExecutionOrder).ToArray())
                            {
                                currentLoggingFile = null;
                                string sqlFilePath = Path.Combine(logDirectory,
                                                                  string.Format("{0:D2}_{1}_{2}.sql", ++scriptNum,
                                                                                projectNamePrefix,
                                                                                script.CategoryName));
                                string outFile = Path.ChangeExtension(sqlFilePath, "out.txt");
                                File.WriteAllText(sqlFilePath, script.SqlScript, new UTF8Encoding(false));
                                File.WriteAllText(outFile,
                                                  string.Format("Output file for {0}{1}{2}{1}{1}", sqlFilePath,
                                                                Environment.NewLine, sqlOption), new UTF8Encoding(false));
                                currentLoggingFile = outFile;

                                server.ConnectionContext.ExecuteNonQuery(script.SqlScript);
                            }
                        }
                    }
                    finally
                    {
                        server.ConnectionContext.InfoMessage -= ConnectionContext_InfoMessage;
                    }
                }
                transactionScope.Complete();
            }
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString))
            {
                DataAccess.AddDeployment(conn, sg.DeploymentRequest.ID, env.ID, sg.Revision, true, "");
            }
        }
        catch (Exception ex)
        {
            using (
                SqlConnection conn =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString))
            {
                SqlException sex = ex as SqlException;
                if (sex == null && ex.InnerException != null && ex.InnerException is SqlException)
                {
                    sex = (SqlException)ex.InnerException;
                }

                string errors = sex != null
                                    ? string.Format(
                                        "Message: {1}{0}Server: {2}{0}Class: {3}{0}Number: {4}{0}LineNumber: {5}{0}State: {6}",
                                        Environment.NewLine,
                                        sex.Message, sex.Server, sex.Class, sex.Number, sex.LineNumber,
                                        sex.State)
                                    : ex.ToString();

                if (!string.IsNullOrEmpty(currentLoggingFile))
                    File.AppendAllText(currentLoggingFile, errors + Environment.NewLine);

                DataAccess.AddDeployment(conn, sg.DeploymentRequest.ID, env.ID, sg.Revision, false, errors);
            }
        }
    }

    protected void butAddSelected_Click(object sender, EventArgs e)
    {
        if (lbProjectFiles.SelectedIndex == -1 || dlProjecstList.SelectedIndex == -1)
            return;

        Parameter par = dsDPRFiles.InsertParameters[1]; //ProjectFileID
        foreach (ListItem item in lbProjectFiles.Items)
        {
            if (!item.Selected)
                continue;

            par.DefaultValue = item.Value;
            dsDPRFiles.Insert();

            item.Selected = false;
        }
        dlDPRFiles.DataBind();
        dlDPRHistory.DataBind();
        if (dlDPRHistory.Rows.Count > 0)
            dlDPRHistory.SelectedIndex = 0;
        lbProjectFiles.DataBind();
    }

    protected void butFilter_Click(object sender, EventArgs e)
    {
        lbProjectFiles.DataBind();
    }

    protected void dlDPRHistory_DataBound(object sender, EventArgs e)
    {
        if (dlDPRHistory.Rows.Count > 0 &&
            (dlDPRHistory.SelectedIndex < 0 || dlDPRHistory.SelectedIndex >= dlDPRHistory.Rows.Count))
        {
            dlDPRHistory.SelectedIndex = 0;
        }
        dlDeploymentError.DataBind();
        dlDeploymentRequests_SelectedIndexChanged(null, null);
    }

    protected void dlDeploymentRequests_DataBound(object sender, EventArgs e)
    {
        if (dlDeploymentRequests.Rows.Count > 0 &&
            (dlDeploymentRequests.SelectedIndex < 0 ||
             dlDeploymentRequests.SelectedIndex >= dlDeploymentRequests.Rows.Count))
        {
            dlDeploymentRequests.SelectedIndex = 0;
        }
        dlDPRFiles.DataBind();
        dlDPRHistory.DataBind();
        UpdateInfoLabel();
    }

    protected void dlDeploymentRequests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "download")
        {
            int projectId = Int32.Parse(dlProjecstList.SelectedValue);
            int drid = -1;
            string drName = e.CommandArgument.ToString().Substring(e.CommandArgument.ToString().IndexOf(",") + 1);
            string dridstr = e.CommandArgument.ToString().Substring(0, e.CommandArgument.ToString().IndexOf(","));
            int.TryParse(dridstr, out drid);
            if (drid == -1)
            {
                return;
            }
            string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
            ScriptGenerator sg = new ScriptGenerator(connStr, projectId, drid, string.Empty);
            String projPrefix = String.Format("{0}-{1}", dlProjecstList.SelectedItem.Text, drName);
            Utility.SendGeneratedFile(Response, sg, projPrefix);
        }
    }

    protected void dlDeploymentRequests_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dlDeploymentRequests.SelectedValue == null)
            return;

        int drid = (int)dlDeploymentRequests.SelectedValue;
        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            DeploymentRequest dr = DataAccess.GetDPR(conn, drid);
            lbProjectFiles.Enabled = !dr.IncludeAllFiles;
            butAddSelected.Enabled = !dr.IncludeAllFiles;
            butFilter.Enabled = !dr.IncludeAllFiles;
            tbNameFilter.Enabled = !dr.IncludeAllFiles;
        }
        UpdateInfoLabel();
    }

    protected void UpdateInfoLabel()
    {
        if (dlDeploymentRequests.Rows.Count > 0 && dlDeploymentRequests.SelectedRow != null && dlProjecstList.SelectedItem != null)
        {

            var firstOrDefault = dlDeploymentRequests.SelectedRow.Cells[2].Controls.Cast<object>().FirstOrDefault(c => c is LinkButton);
            if (firstOrDefault != null)
            {
                string text = string.Format("DR \"{0}\" from Project \"{1}\"", ((LinkButton)firstOrDefault).Text, dlProjecstList.SelectedItem.Text);
                string url = Request.Url.Query!=""? Request.Url.AbsoluteUri.Replace(Request.Url.Query, "") : Request.Url.AbsoluteUri;
                lbIdentification.NavigateUrl = string.Format("{0}?p={1}&dr={2}", url, dlProjecstList.SelectedItem.Value, dlDeploymentRequests.SelectedValue);
                lbIdentification.Text = text;
                return;
            }
        }
        lbIdentification.Text = string.Empty;
    }

    protected void dlProjecstList_DataBound(object sender, EventArgs e)
    {
        if (dlProjecstList.Items.Count > 0)
        {
            dlProjecstList.SelectedIndex = 0;
        }
    }

    protected void lbProjects_Click(object sender, EventArgs e)
    {
        string query = "";
        if (dlProjecstList.SelectedIndex != -1)
            query = "?p="+ dlProjecstList.SelectedValue.ToString();

        Response.Redirect("Default.aspx" + query);
    }

    private string CreateLogDirectoryStructure(Project project, DeploymentRequest deploymentRequest)
    {
        string logDirectory = Path.Combine(WebConfigurationManager.AppSettings["DeploymentLogDirectory"],
                                           DateTime.Now.ToString("yyyyMMdd"));
        if (!Directory.Exists(logDirectory))
            Directory.CreateDirectory(logDirectory);
        logDirectory = Path.Combine(logDirectory,
                                    string.Format("{0}_{1}_{2}", DateTime.Now.ToString("HHmmss"),
                                                  project.ProjectName, deploymentRequest.DRName));
        if (!Directory.Exists(logDirectory))
            Directory.CreateDirectory(logDirectory);
        return logDirectory;
    }

    #endregion

    #region Event Handling

    private void ConnectionContext_InfoMessage(object sender, SqlInfoMessageEventArgs e)
    {
        StringBuilder sb = new StringBuilder();
        foreach (SqlError error in e.Errors)
        {
            sb.AppendFormat("Line {0}: {1}{2}", error.LineNumber, error.Message, Environment.NewLine);
        }
        File.AppendAllText(currentLoggingFile, sb.ToString());
    }

    protected void butOK_Click(object sender, EventArgs e)
    {
        string dprName = tbProjectName.Text.Trim();
        if (dprName.Length == 0)
            return;
        int pid = Int32.Parse(dlProjecstList.SelectedValue);

        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            DataAccess.AddDPR(conn, pid, dprName, cbIncludeAllFiles.Checked);
        }
        //ppcAddDR.ShowOnPageLoad = false;
        dlDeploymentRequests.DataBind();
    }

    #endregion
}