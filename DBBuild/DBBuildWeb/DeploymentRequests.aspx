﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeploymentRequests.aspx.cs"
    Inherits="DeploymentRequests" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="StyleSheet" href="default.css" type="text/css" />
    <title>DBBuild - Deployment requests</title>
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolKit:ToolkitScriptManager ID="asm" runat="server" />
    <ajaxToolKit:ModalPopupExtender ID="ppcAddDRMPE" runat="server" TargetControlID="lbAddDPR" PopupControlID="ppcAddDR" />
    <ajaxToolKit:ModalPopupExtender ID="ppcErrorMsgMPE" runat="server" TargetControlID="hfNewDPRName" PopupControlID="ppcErrorMsg" />

    <div id="main">
        <h1>
            DB Build</h1>
        <div id="menu">
            <asp:LinkButton CssClass="menu_item" ID="lbProjects" runat="server" OnClick="lbProjects_Click">Projects</asp:LinkButton>
            <asp:HiddenField ID="hfNewDPRName" runat="server" />
        </div>
        <div id="top">
            <div id="dpr_list">
                Project:
                <asp:DropDownList ID="dlProjecstList" runat="server" DataSourceID="dsProjectList"
                    DataTextField="ProjectName" DataValueField="ProjectID" AutoPostBack="True" OnDataBound="dlProjecstList_DataBound">
                </asp:DropDownList>
                <br />
                <asp:LinkButton CssClass="menu_item" runat="server" ID="lbAddDPR">Add deployment request</asp:LinkButton> <%--OnClientClick="ppcAddDR.Show(); return false;"--%>
                <asp:GridView ID="dlDeploymentRequests" runat="server" 
                    AutoGenerateColumns="False"
                    DataKeyNames="DRID" DataSourceID="dsProjectDPRs" OnDataBound="dlDeploymentRequests_DataBound"
                    ShowHeaderWhenEmpty="True" CssClass="max_table" OnRowCommand="dlDeploymentRequests_RowCommand"
                    OnSelectedIndexChanged="dlDeploymentRequests_SelectedIndexChanged" 
                    GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderText="" SortExpression="DRName">
                            <ItemTemplate>
                                <asp:LinkButton ID="DeploymentRequestDelete" runat="server" Text='Delete' CommandName="delete" ToolTip="Delete"
                                    OnClientClick="javascript:return confirm('Are you shure that you want to delete this deployment request?')">
                                    <img src="./img/Del.png" alt="Del"/>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:LinkButton ID="DeploymentRequestDownload" runat="server" Text='Download' CommandName='download' ToolTip="Download"
                                    CommandArgument='<%# Eval("DRID") + "," + Eval("DRName") %>' >
                                    <img src="./img/Download.png" alt="Download"/>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" SortExpression="DRName">
                            <ItemTemplate>
                                <asp:LinkButton ID="DeploymentRequestLabel" runat="server" Text='<%# Bind("DRName") %>' CommandName="select"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CheckBoxField DataField="IncludeAllFiles" HeaderText="All Files" SortExpression="IncludeAllFiles" />
                        <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
                        <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" />
                    </Columns>
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="alternate" />
                </asp:GridView>
            </div>
            <div id="dpr_files">
                <asp:GridView ID="dlDPRFiles" runat="server" DataSourceID="dsDPRFiles" AutoGenerateColumns="False"
                    DataKeyNames="ProjectFileID" ShowHeaderWhenEmpty="True" 
                    CssClass="max_table" GridLines="None">
                    <Columns>
                        <asp:CommandField  HeaderText="" ShowDeleteButton="True"  ButtonType="Image" DeleteImageUrl = "./img/Del.png"/>
                        <asp:BoundField DataField="FileName" HeaderText="FileName" ReadOnly="True" SortExpression="FileName" />
                        <asp:BoundField DataField="BuildOrder" HeaderText="Order" SortExpression="BuildOrder" />
                        <asp:BoundField DataField="ObjectTypeName" HeaderText="Type" SortExpression="ObjectTypeName" />
                        <asp:BoundField DataField="ScriptCategoryName" HeaderText="Category" SortExpression="ScriptCategoryName" />
                    </Columns>
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="alternate" />
                </asp:GridView>
            </div>
        </div>
        <div id="Bottom">
            <table>
                <tr>
                    <td>
                        <%--&nbsp; Object Type
                        <asp:DropDownList ID="ddObjectTypes" runat="server" DataSourceID="dsObjectTypeLookup"
                            DataTextField="ObjectTypeName" DataValueField="ObjectTypeID" AutoPostBack="True">
                        </asp:DropDownList>--%>
                        &nbsp; Filter by name
                        <asp:TextBox ID="tbNameFilter" runat="server" MaxLength="64" 
                            AutoPostBack="True"></asp:TextBox>
                        &nbsp;
                        <asp:Button ID="butFilter" runat="server" Text="Filter" OnClick="butFilter_Click" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddEnvironment" runat="server" DataSourceID="dsEnvironments"
                            DataTextField="EnvironmentName" DataValueField="EnvironmentID" AutoPostBack="false">
                        </asp:DropDownList>
                        <asp:Button ID="btnDeploy" runat="server" Text="Deploy" OnClick="btnDeploy_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="butAddSelected" runat="server" Text="Add Selected" OnClick="butAddSelected_Click" />
                    </td>
                    <td>
                        <span>Deployment history</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdControlContainer">
                        <asp:ListBox ID="lbProjectFiles" runat="server" DataSourceID="dsRepositoryFiles"
                            DataTextField="FileName" DataValueField="ProjectFileID" Rows="15" SelectionMode="Multiple"
                            CssClass="list_rep_files" ></asp:ListBox>
                    </td>
                    <td class="tdControlContainer">
                        <asp:GridView ID="dlDPRHistory" runat="server" AutoGenerateColumns="False" DataKeyNames="DeploymentID"
                            DataSourceID="dsDPRHistory" OnDataBound="dlDPRHistory_DataBound" ShowHeaderWhenEmpty="True"
                            CssClass="max_table" GridLines="None">
                            <Columns>
                                <asp:TemplateField HeaderText="DeploymentDate" SortExpression="DeploymentDate">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="DeploymentDateLabel" runat="server" Text='<%# Bind("DeploymentDate") %>'
                                            CommandName="select"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CheckBoxField DataField="IsSuccessfull" HeaderText="IsSuccessfull" SortExpression="IsSuccessfull" />
                                <asp:BoundField DataField="SVNRevision" HeaderText="SVNRevision" SortExpression="SVNRevision" />
                                <asp:BoundField DataField="DeployedBy" HeaderText="DeployedBy" SortExpression="DeployedBy" />
                                <asp:BoundField DataField="Environment" HeaderText="Environment" ReadOnly="True"
                                    SortExpression="Environment" />
                            </Columns>
                            <SelectedRowStyle CssClass="selected" />
                            <HeaderStyle CssClass="header" />
                            <AlternatingRowStyle CssClass="alternate" />
                        </asp:GridView>
                        <asp:DataList ID="dlDeploymentError" runat="server" DataSourceID="dsDeploymentError"
                            Width="100%">
                            <ItemTemplate>
                                <asp:Label ID="ErrorsLabel" runat="server" Text='<%# Eval("Errors").ToString().Replace(Environment.NewLine, "<br />") %>' />
                                <br />
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <asp:HyperLink ID="lbIdentification" runat="server"></asp:HyperLink>
        <asp:Panel ID="ppcAddDR" runat="server" CssClass="modalPopup">
        <table>
        <tr>
            <td>Request Name:</td>
            <td>
                <asp:TextBox ID="tbProjectName" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Deployment name should contain only latin symbols" ControlToValidate="tbProjectName" ValidationExpression="[a-zA-Z0-9_ ]+"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:CheckBox ID="cbIncludeAllFiles" runat="server" Text="Include all project files" /></td>
        </tr>                
        <tr>
            <td></td>
            <td>
                <asp:Button ID="butOK" runat="server" Text="OK" Width="100px" OnClick="butOK_Click" /> <%--OnClick="butOK_Click"--%>
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="butCancel" runat="server" Text="Cancel" OnClientClick="$find('ppcAddDRMPE').hide(); return false;" CausesValidation="false" Width="100px" />
            </td>
        </tr>                
        
        </table>
        </asp:Panel>

        <asp:Panel ID="ppcErrorMsg" runat="server" CssClass="modalPopup">
            <asp:TextBox ID="tbErrorMsg" runat="server" MaxLength="1500" EnableViewState="False" Wrap = "true" Width="80%" TextMode="MultiLine" Rows="5"></asp:TextBox>
            &nbsp;
            <asp:Button ID="butErrCancel" runat="server" Text="Cancel" OnClientClick="$find('ppcErrorMsgMPE').hide(); return false;" CausesValidation="false" Width="100px" />
        </asp:Panel>

        <asp:SqlDataSource ID="dsProjectList" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetProjectsList" SelectCommandType="StoredProcedure" />
        <asp:SqlDataSource ID="dsProjectDPRs" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetProjectDPRs" SelectCommandType="StoredProcedure" InsertCommand="pAddDPR"
            InsertCommandType="StoredProcedure" DeleteCommand="pDeleteDPR" DeleteCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="dlProjecstList" Name="ProjectID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Direction="ReturnValue" Name="RETURN_VALUE" Type="Int32" />
                <asp:ControlParameter ControlID="dlDeploymentRequests" Name="DRID"
                    PropertyName="SelectedValue" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="dlProjecstList" Name="ProjectID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="hfNewDPRName" Name="DRName" PropertyName="Value"
                    Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsDPRFiles" runat="server" 
            ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetDPRFiles" SelectCommandType="StoredProcedure" 
            InsertCommand="pAddDPRFile" InsertCommandType="StoredProcedure" 
            DeleteCommand="pDeleteDeploymentFile" DeleteCommandType="StoredProcedure"
            CancelSelectOnNullParameter="False">
            <SelectParameters>
                <asp:ControlParameter ControlID="dlDeploymentRequests" Name="DRID" PropertyName="SelectedValue" Type="Int32" DefaultValue="" />
            </SelectParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="dlDeploymentRequests" Name="DRID" PropertyName="SelectedValue" Type="Int32" />
                <asp:Parameter Name="ProjectFileID" Type="Int32" />
            </InsertParameters>
            <DeleteParameters>
                <asp:ControlParameter ControlID="dlDeploymentRequests" Name="DRID" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="dlDPRFiles" Name="ProjectFileID" PropertyName="SelectedValue" Type="Int32" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsDPRHistory" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetDPRHistory" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False">
            <SelectParameters>
                <asp:ControlParameter ControlID="dlDeploymentRequests" Name="DRID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsRepositoryFiles" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetAvailableProjectFiles" SelectCommandType="StoredProcedure"
            CancelSelectOnNullParameter="False">
            <SelectParameters>
                <asp:ControlParameter ControlID="dlProjecstList" Name="ProjectID" PropertyName="SelectedValue"
                    Type="Int32" DefaultValue="" />
                <asp:ControlParameter ControlID="dlDeploymentRequests" Name="DRID" PropertyName="SelectedValue"
                    Type="Int32" DefaultValue="" />
                <asp:Parameter DefaultValue="" Name="ObjectTypeID" />
                <asp:ControlParameter ControlID="tbNameFilter" DefaultValue=" " Name="FileNamePart"
                    PropertyName="Text" Type="String" ConvertEmptyStringToNull="False" Size="64" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsObjectTypeLookup" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetObjectTypes" SelectCommandType="StoredProcedure" />
        <asp:SqlDataSource ID="dsEnvironments" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetEnvironments" SelectCommandType="StoredProcedure" />
        <asp:SqlDataSource ID="dsDeploymentError" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetDeploymentError" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False">
            <SelectParameters>
                <asp:ControlParameter ControlID="dlDPRHistory" Name="DeploymentID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
