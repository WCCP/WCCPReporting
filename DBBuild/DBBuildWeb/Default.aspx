﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="StyleSheet" href="default.css" type="text/css" />
    <title>DBBuild - Projects</title>
    <script type="text/javascript">
        function SetProjectFolder() 
        {
            tb = document.getElementById('tbProjectFolder');
            if (document.getElementById('cbIsInTrunk').checked)
                tb.disabled = true;
            else
                tb.disabled = false;
        }
        function BuildProjectPath() 
        {
            InTrunk = document.getElementById('cbIsInTrunk');
            db = document.getElementById('ddDatabases');
            folder = document.getElementById('tbProjectFolder')
            purl = document.getElementById('tbProjectURL')
            BaseURL = document.getElementById('tbPrBaseURL').value;
            DBRootPath = document.getElementById('tbPrTrunkPath').value;
            BranchesPath = document.getElementById('tbPrBranchesPath').value; 

            if (InTrunk.checked)
                ProjectURL = BaseURL + DBRootPath + '/DB/' + db.options[db.selectedIndex].text;
            else
                ProjectURL = BaseURL + BranchesPath + '/' + folder.value;
            purl.value = ProjectURL;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolKit:ToolkitScriptManager ID="asm" runat="server" />
    <ajaxToolKit:ModalPopupExtender ID="ppcAddProjectMPE" runat="server" 
        TargetControlID="lbAddProjectHidden"
        PopupControlID="ppcAddProject" 
        />

    <ajaxToolKit:ModalPopupExtender ID="ppcErrorMsgMPE" runat="server" 
        TargetControlID="lbAddProjectHidden"
        PopupControlID="ppcErrorMsg" 
        />

    <div id="main">
        <h1>DB Build</h1>
        <div id="menu">
            <asp:LinkButton CssClass="menu_item" ID="lbDPR" runat="server" OnClick="lbDPR_Click">Deployment requests</asp:LinkButton>
        </div>
        <div id="top">
            <div id="top_left">
                <asp:LinkButton ID="lbAddProjectHidden" runat="server" style="display:none;" />
                <asp:LinkButton ID="lbAddProject" runat="server" OnClick="lbAddProject_Click">Add Project</asp:LinkButton>
                <asp:DataList ID="dlProjecstList" runat="server" DataKeyField="ProjectID" DataSourceID="dsProjectList"
                    OnSelectedIndexChanged="dlProjecstList_SelectedIndexChanged" 
                    OnDeleteCommand="dlProjecstList_DeleteCommand"
                    OnItemCommand="dlProjecstList_ItemCommand"
                    Width="100%" >
                    <HeaderTemplate>Projects</HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbDeleteProject" CommandName="delete" runat="server" OnClientClick="javascript:return confirm('Are you shure that you want to delete this project?')" ToolTip="Delete"><img src="./img/Del.png" alt="Del"/></asp:LinkButton>
                        <asp:LinkButton ID="lbEditProject" CommandName="edit" runat="server" CommandArgument='<%# Eval("ProjectID") %>' ToolTip="Edit"><img src="./img/Edit.png" alt="Edit"/></asp:LinkButton>
                        <asp:LinkButton ID="lbDownloadProject" CommandName="download" runat="server" CommandArgument='<%# Eval("ProjectID") + "," + Eval("ProjectName") %>' ToolTip="Download"><img src="./img/Download.png" alt="Download"/></asp:LinkButton>
                        <asp:LinkButton ID="lbSelectProject" CommandName="select" runat="server"><%# Eval("ProjectName") %></asp:LinkButton>
                    </ItemTemplate>
                    <SelectedItemTemplate>
                        <asp:LinkButton ID="lbDeleteProject" CommandName="delete" runat="server" ToolTip="Delete"><img src="./img/Del.png" alt="Del"/></asp:LinkButton>
                        <asp:LinkButton ID="lbEditProject" CommandName="edit" runat="server" CommandArgument='<%# Eval("ProjectID") %>' ToolTip="Edit"><img src="./img/Edit.png" alt="Edit"/></asp:LinkButton>
                        <asp:LinkButton ID="lbDownloadProject" CommandName="download" runat="server" CommandArgument='<%# Eval("ProjectID") + "," + Eval("ProjectName") %>' ToolTip="Download"><img src="./img/Download.png" alt="Download"/></asp:LinkButton>
                        <asp:LinkButton ID="lbSelectProject" CommandName="select" runat="server"><%# Eval("ProjectName") %></asp:LinkButton>
                    </SelectedItemTemplate>
                    <SelectedItemStyle CssClass="sel_proj" />
                    <AlternatingItemStyle CssClass="list_alt" />
                    <HeaderStyle CssClass="list_header" />
                </asp:DataList>
            </div>
            <div id="top_right">
                <asp:DataList ID="dlProjectFiles" runat="server" DataKeyField="ProjectFileID" DataSourceID="dsProjectFiles"
                    OnDeleteCommand="dlProjectFiles_DeleteCommand" OnCancelCommand="dlProjectFiles_CancelCommand"
                    OnEditCommand="dlProjectFiles_EditCommand" OnUpdateCommand="dlProjectFiles_UpdateCommand"
                    RepeatLayout="Table"
                    RepeatDirection="Vertical"
                    >
                    <HeaderTemplate>
                        <td class="list_header"></td>
                        <td class="list_header">Object type</td>
                        <td class="list_header">Object name</td>
                        <td class="list_header" width="15%">Build order</td>
                        <td class="list_header">Category</td>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                        <td><asp:LinkButton ID="lbDeleteProjectFile" CommandName="delete" runat="server" ToolTip="Delete"><img src="./img/Del.png" alt="Delete"/></asp:LinkButton></td>
                        <td><asp:LinkButton ID="lbEdit" CommandName="edit" runat="server" ToolTip="Edit"><img src="./img/Edit.png" alt="Edit"/></asp:LinkButton></td>
                        <td><asp:Label ID="ObjectTypeNameLabel" runat="server" Text='<%# Eval("ObjectTypeName") %>' /></td>
                        <td><asp:Label ID="FileNameLabel" runat="server" Text='<%# Eval("FileName") %>' /></td>
                        <td><asp:Label ID="BuildOrderLabel" runat="server" Text='<%# Eval("BuildOrder") %>' /></td>
                        <td><asp:Label ID="ScriptCategoryLabel" runat="server" Text='<%# Eval("ScriptCategoryName") %>' /></td>
                        </tr>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <tr>
                            <td><asp:LinkButton ID="lbUpdate" CommandName="update" runat="server">Update</asp:LinkButton>&nbsp;</td>
                            <td><asp:LinkButton ID="lbCancel" CommandName="cancel" runat="server">Cancel</asp:LinkButton>&nbsp;</td>
                            <td><asp:Label ID="ObjectTypeNameLabel" runat="server" Text='<%# Eval("ObjectTypeName") %>' />&nbsp;</td>
                            <td><asp:Label ID="FileNameLabel" runat="server" Text='<%# Eval("FileName") %>' />&nbsp;</td>
                            <td><asp:TextBox ID="tbBuildOrderEdit" runat="server" Text='<%# Eval("BuildOrder") %>' /></td>
                            <td><asp:Label ID="ScriptCategoryLabel" runat="server" Text='<%# Eval("ScriptCategoryName") %>' /></td>
                        </tr>
                    </EditItemTemplate>
                    <HeaderStyle CssClass="list_header" />
                </asp:DataList>
            </div>
        </div>
        <div id="Bottom">
            <table id="BottomLayoutTable">
                <tr>
                    <td class="list_header" width="25%">Repository</td>
                    <td class="list_header" width="50%">Files</td>
                    <td width="25%"> </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton CssClass="menu_item" ID="lbRefreshRepository" runat="server" OnClick="lbRefreshRepository_Click">Refresh Repository</asp:LinkButton>
                    </td>
                    <td>
                        Filter by name
                        <asp:TextBox ID="tbNameFilter" runat="server" MaxLength="64" AutoPostBack="True" OnTextChanged="tbNameFilter_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        Parse file list
                    </td>
                </tr>
                <tr>
                <td class="tdControlContainer" style="height:250px">
                        <asp:TreeView ID="treeSVN" runat="server" CssClass="tree" ExpandDepth="0" ImageSet="Msdn" onselectednodechanged="treeSVN_SelectedNodeChanged">
                            <SelectedNodeStyle CssClass="sel_folder" />
                        </asp:TreeView>
                </td>
                <td class="tdControlContainer" style = "height:250px;">
                    <asp:ListBox ID="lbRepositoryFiles" runat="server" Rows="14" SelectionMode="Multiple"
                        CssClass="filelist" ></asp:ListBox>
                </td>
                    <td class="tdControlContainer" style = "height:250px;">
                        <asp:TextBox ID="tbDropBox" runat="server" Rows="14" TextMode="MultiLine" 
                             CssClass="filelist"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                <td rowspan="2"></td>
                <td>
                    <p>Script Category</p>
                    <asp:DropDownList ID="ddScriptCategory" runat="server" DataSourceID="dsScriptCategory"
                        DataTextField="ScriptCategoryName" DataValueField="ScriptCategoryID" AutoPostBack="false">
                    </asp:DropDownList>
                    <asp:Button ID="butAddSelected" runat="server" Text="Add Selected" OnClick="butAddSelected_Click" />
                </td>
                <td>
                        <p>&nbsp</p>
                        <asp:Button ID="butParseDropBox" runat="server" Text="Parse" OnClick="butParseDropBox_Click" />
                </td>
                </tr>
            </table>
        </div>
        <asp:Panel ID="ppcAddProject" runat="server" CssClass="modalPopup">
            <asp:TextBox ID="tbProjectID"      runat="server" MaxLength="50" EnableViewState="False" style="display:none"></asp:TextBox>
            <asp:TextBox ID="tbPrTrunkPath"    runat="server" MaxLength="50" EnableViewState="False" style="display:none"></asp:TextBox>
            <asp:TextBox ID="tbPrBaseURL"      runat="server" MaxLength="50" EnableViewState="False" style="display:none"></asp:TextBox>
            <asp:TextBox ID="tbPrBranchesPath" runat="server" MaxLength="50" EnableViewState="False" style="display:none"></asp:TextBox>
            <table class="popup">
            <tbody>
            <tr>
                <td>Project Name</td>
                <td>
                    <asp:TextBox ID="tbProjectName" runat="server" MaxLength="50" EnableViewState="False" onkeyup="BuildProjectPath();"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Project name should contain only latin symbols"
                        ControlToValidate="tbProjectName" ValidationExpression="[a-zA-Z0-9_ ]+"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>Database</td>
                <td><asp:DropDownList ID="ddDatabases" runat="server" ></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Is in trunk</td>
                <td><asp:CheckBox ID="cbIsInTrunk" runat="server" onclick="SetProjectFolder();BuildProjectPath();" /></td>
            </tr>
            <tr>
                <td>Project Folder</td>
                <td><asp:TextBox ID="tbProjectFolder" runat="server" MaxLength="50" EnableViewState="False" onkeyup="BuildProjectPath();"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblArchivedProject" runat="server">Archived</asp:Label></td>
                <td><asp:CheckBox ID="cbIsArchived" runat="server" /></td>
            </tr>
            <tr>
                <td>Project URL</td>
                <td>
                <asp:TextBox ID="tbProjectURL" runat="server" MaxLength="1024" 
                    EnableViewState="False" Enabled="False" TextMode="MultiLine" Width="466px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td></td>
                 <td>
                    <asp:Button ID="butProjectOK" runat="server" Text="OK" 
                        OnClick="butProjectOK_Click" Width="100px" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="butProjectCancel" runat="server" Text="Cancel" OnClientClick="$find('ppcAddProjectMPE').hide(); return false;"
                        CausesValidation="false" Width="100px" />
                  </td>
              </tr>
              </tbody>
              </table>
        </asp:Panel>

        <asp:Panel ID="ppcErrorMsg" runat="server">
            <asp:TextBox ID="tbErrorMsg" runat="server" MaxLength="1500" EnableViewState="False"></asp:TextBox>
        </asp:Panel>

        <asp:SqlDataSource ID="dsProjectList" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetProjectsList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsProjectFiles" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetProjectFiles" SelectCommandType="StoredProcedure" UpdateCommand="pUpdateProjectFile"
            UpdateCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="dlProjecstList" Name="ProjectID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="ProjectFileID" Type="Int32" />
                <asp:Parameter Name="BuildOrder" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsScriptCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DBBuildConnectionString %>"
            SelectCommand="pGetScriptCategories" SelectCommandType="StoredProcedure" />
    </div>
    </form>
</body>
</html>
