﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.Caching;
using DBBuild;
using System.Data.SqlClient;
//using System.
using DBBuild.DataAccess;

public partial class _Default : System.Web.UI.Page 
{
    /// <summary>
    /// Список БД из таблицы Databases
    /// </summary>
    private Dictionary<int, string> Databases
    {
        get
        {
            if (Cache["Databases"] == null)
                ReReadData();
            return (Dictionary<int, string>)Cache["Databases"];
        }
    }

    /// <summary>
    /// Хранит содержимое репозиториев всех проектов (папки с файлами)
    ///  URL репозитория - объект Dir
    /// </summary>
    private Dictionary<string, Dir> Repositories
    {
        get 
        {
            if (Cache["Repositories"] == null)
                ReReadData();
            return (Dictionary<string, Dir>)Cache["Repositories"];
        }
    }

    /// <summary>
    /// Список проектов
    /// </summary>
    private Dictionary<int, Project> Projects
    {
        get 
        {
            if (Cache["Projects"] == null)
                ReReadData();
            return (Dictionary<int, Project>)Cache["Projects"];
        }
    }

    /// <summary>
    /// Текущий выбраный проект
    /// </summary>
    private Project CurrentProject
    {
        get
        {
            if (dlProjecstList.SelectedIndex == -1)
                return null;
            int projectID = (Int32)dlProjecstList.DataKeys[dlProjecstList.SelectedIndex];
            return Projects[projectID];
        }
    }

    /// <summary>
    /// Содержимое репозитория текущего проекта
    /// </summary>
    private Dir CurrentRepository
    {
        get
        {
            int projectID = (Int32)dlProjecstList.DataKeys[dlProjecstList.SelectedIndex];
            return Repositories[Projects[projectID].ProjectURL];
        }
    }

    /// <summary>
    /// Репозиторий текущего проекта
    /// В текущей реализации репозиторий может быть только один, поэтому его можно хранить в Cache
    /// </summary>
    private Repository CurrentSVNRep
    {
        get
        {
            if (Cache["CurrentSVNRep"] == null)
                ReReadData();
            return (Repository)Cache["CurrentSVNRep"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
            return;

        dlProjecstList.DataBind();

        
        if (Request.QueryString["p"] != null)
        {
            int ProjectID = -1;
            ProjectID = Int32.Parse(Request.QueryString["p"]);
            for (int i = 0; i < dlProjecstList.DataKeys.Count; i++)
                if ((Int32)dlProjecstList.DataKeys[i] == ProjectID)
                {
                    dlProjecstList.SelectedIndex = i;
                    break;
                }
            dlProjectFiles.DataBind();
        }
        else
            if (dlProjecstList.Items.Count > 0)
            {
                dlProjecstList.SelectedIndex = 0;
                dlProjectFiles.DataBind();
            }
       
        ddScriptCategory.DataBind();
        if (ddScriptCategory.Items.Count > 0)
            ddScriptCategory.SelectedIndex = 1;

        treeSVN.DataSource = CurrentRepository;
        treeSVN.DataBind();

    }

    public void ReReadData()
    {
        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;

        using (SqlConnection conn = new SqlConnection(connStr))
        {
            Repository r = DataAccess.GetRepository(conn, 1);
            Cache.Add("CurrentSVNRep", r, null, Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0), CacheItemPriority.Normal, null);

            Dictionary<int, string> Databases = DataAccess.GetDatabasesList(conn);
            Cache.Add("Databases", Databases, null, Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0), CacheItemPriority.Normal, null);

            Dictionary<int, Project> Projects = DataAccess.GetProjectsList(conn);
            Cache.Add("Projects", Projects, null, Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0), CacheItemPriority.Normal, null);

            int projectID = (Int32)dlProjecstList.DataKeys[0];
            Project prj = Projects[projectID];

            Dictionary<string, Dir> Repositories;
            if (Cache["Repositories"] == null)
            {
                Repositories = new Dictionary<string, Dir>();
                Dir d = DBBuildHelper.ReadRepFromSVN(connStr, prj.ProjectURL);
                Repositories.Add(prj.ProjectURL, d);
                Cache.Add("Repositories", Repositories, null, Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0), CacheItemPriority.Normal, null);
            }

        }

    }

    #region Project
    protected void lbRefreshRepository_Click(object sender, EventArgs e)
    {
        if (CurrentProject == null)
            return;

        string projectURL = CurrentProject.ProjectURL;
        //заново считаем дерево из SVN
        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
        Dir d = DBBuildHelper.ReadRepFromSVN(connStr, projectURL);
        //пересохраним его в список репозиториев
        Repositories[projectURL] = d;

        treeSVN.DataSource = d;
        treeSVN.DataBind();

        lbRepositoryFiles.DataBind();

    }

    protected void butProjectOK_Click(object sender, EventArgs e)
    {
        string sProjectID = tbProjectID.Text.Trim();
        int ProjectID = (sProjectID.Length > 0) ? Int32.Parse(sProjectID) : -1;

        string ProjectName = tbProjectName.Text.Trim();
        string ProjectFolder = tbProjectFolder.Text.Trim();
        int DatabaseID = Int32.Parse(ddDatabases.SelectedItem.Value);
        int RepositoryID = 1;
        bool IsArchived = cbIsArchived.Checked;
        bool IsInTrunk = cbIsInTrunk.Checked;

        if (ProjectName.Length == 0)
            return;

        Repository r = CurrentSVNRep;

        string ProjectURL;
        if (IsInTrunk)
            ProjectURL = r.BaseURL + r.DBRootPath + "/DB/" + ddDatabases.SelectedItem;
        else
            ProjectURL = r.BaseURL + r.BranchesPath + '/' + @ProjectFolder;

        Project p = new Project(ProjectID, ProjectName, ProjectFolder, ProjectURL, DatabaseID, RepositoryID, IsInTrunk, IsArchived);

        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            if (ProjectID == -1) //создание нового
            {
                p.ProjectID = DataAccess.AddProject(conn, p);
                if (!Projects.ContainsKey(p.ProjectID))
                    Projects.Add(p.ProjectID, p);
            }
            else
            {
                DataAccess.UpdateProject(conn, p);
                Projects[ProjectID] = p;
            }
        }

        dlProjecstList.DataBind();
        for (int i = 0; i < dlProjecstList.DataKeys.Count; i++)
        {
            if((Int32)dlProjecstList.DataKeys[i] == p.ProjectID)
            {
                dlProjecstList.SelectedIndex = i;
                break;
            }

        }
        
        dlProjectFiles.DataBind();
        lbRepositoryFiles.DataBind();
        CheckRepository(ProjectURL);
    }

    protected void dlProjecstList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CurrentProject == null)
            return;
        //убедимся что репозиторий есть в кеше
        CheckRepository(CurrentProject.ProjectURL);
        dlProjectFiles.DataBind();
        
    }

    private void CheckRepository(string ProjectURL)
    {
        Dir rep;
        if (!Repositories.ContainsKey(ProjectURL))
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
            rep = DBBuildHelper.ReadRepFromSVN(connStr, ProjectURL);
            Repositories.Add(ProjectURL, rep);
        }
        else //из списка достанем репозиторий проекта
        {
            rep = Repositories[ProjectURL];
        }
        lbRepositoryFiles.DataSource = rep.Files;
        lbRepositoryFiles.DataBind();
        treeSVN.DataSource = rep;
        treeSVN.DataBind();
    }

    protected void dlProjecstList_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        if (CurrentProject == null)
            return;

        int projectID = CurrentProject.ProjectID;

        //удалим из БД
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString))
        {
            DataAccess.DeleteProject(conn, projectID);
        }

        //удалим из кеша
        Projects.Remove(projectID);

        dlProjecstList.DataBind();

        if (dlProjecstList.Items.Count > 0)
        {
            dlProjecstList.SelectedIndex = 0;
            dlProjectFiles.DataBind();
            dlProjecstList_SelectedIndexChanged(this, null);
        }
    }
    protected void dlProjecstList_ItemCommand(object source, DataListCommandEventArgs e)
    {
        int projectId = -1;
        if (e.CommandName.Equals("download"))
        {
            string projectName = e.CommandArgument.ToString().Substring(e.CommandArgument.ToString().IndexOf(",") + 1);
            string projectIdStr = e.CommandArgument.ToString().Substring(0, e.CommandArgument.ToString().IndexOf(","));
            int.TryParse(projectIdStr, out projectId);
            if (projectId == -1) { return; }

            string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
            ScriptGenerator sg = new ScriptGenerator(connStr, projectId, string.Empty);
            String projPrefix = String.Format("{0}", projectName);
            Utility.SendGeneratedFile(Response, sg, projPrefix);
        }
        else if (e.CommandName.Equals("edit"))
        {
            projectId = Int32.Parse(e.CommandArgument.ToString());
            Project p = Projects[projectId];

            if (ddDatabases.Items.Count == 0)
                foreach (KeyValuePair<int, string> item in Databases)
                    ddDatabases.Items.Add(new ListItem(item.Value, item.Key.ToString()));

            Repository r = CurrentSVNRep;
            tbPrTrunkPath.Text = r.DBRootPath;
            tbPrBaseURL.Text = r.BaseURL;
            tbPrBranchesPath.Text = r.BranchesPath;

            tbProjectID.Text = p.ProjectID.ToString();
            tbProjectName.Text = p.ProjectName;
            tbProjectFolder.Text = p.ProjectFolder;
            tbProjectFolder.Enabled = !p.IsInTrunk;
            tbProjectURL.Text = p.ProjectURL;
            lblArchivedProject.Visible = true;
            cbIsArchived.Visible = true;
            cbIsArchived.Checked = p.IsArchived;
            cbIsInTrunk.Checked = p.IsInTrunk;
            ddDatabases.Enabled = false;
            ppcAddProjectMPE.Show();

        }
    }

    protected void lbAddProject_Click(object sender, EventArgs e)
    {
        if (ddDatabases.Items.Count == 0)
            foreach (KeyValuePair<int, string> item in Databases)
                ddDatabases.Items.Add(new ListItem(item.Value, item.Key.ToString()));

        Repository r = CurrentSVNRep;
        tbPrTrunkPath.Text = r.DBRootPath;
        tbPrBaseURL.Text = r.BaseURL;
        tbPrBranchesPath.Text = r.BranchesPath;

        tbProjectID.Text = "";
        tbProjectName.Text = "";
        tbProjectFolder.Enabled = false;
        tbProjectURL.Text = "";
        tbProjectFolder.Text = "";
        lblArchivedProject.Visible = false;
        cbIsArchived.Visible = false;
        cbIsArchived.Checked = false;
        cbIsInTrunk.Checked = true;
        ddDatabases.Enabled = true;
        ppcAddProjectMPE.Show();
    }

    protected void cbIsInTrunk_CheckedChanged(object sender, EventArgs e)
    {
        tbProjectFolder.Enabled = !cbIsInTrunk.Checked;
    }
    #endregion

    #region ProjectFiles

    protected void dlProjectFiles_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        if (dlProjecstList.SelectedIndex == -1)
            return;
        int projectFileID = (Int32)dlProjectFiles.DataKeys[e.Item.ItemIndex];

        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            DataAccess.DeleteProjectFile(conn, projectFileID);
        }

        dlProjectFiles.DataBind();
        lbRepositoryFiles.DataBind();
    }
    protected void dlProjectFiles_EditCommand(object source, DataListCommandEventArgs e)
    {
        dlProjectFiles.EditItemIndex = e.Item.ItemIndex;
        dlProjectFiles.DataBind();
    }
    protected void dlProjectFiles_CancelCommand(object source, DataListCommandEventArgs e)
    {
        dlProjectFiles.EditItemIndex = -1;
        dlProjectFiles.DataBind();
    }
    protected void dlProjectFiles_UpdateCommand(object source, DataListCommandEventArgs e)
    {
        int projectFileId = Convert.ToInt32(dlProjectFiles.DataKeys[e.Item.ItemIndex]);
        string buildOrderText = ((TextBox)e.Item.FindControl("tbBuildOrderEdit")).Text.Trim();
        try
        {
            dsProjectFiles.UpdateParameters["ProjectFileID"].DefaultValue = projectFileId.ToString();
            dsProjectFiles.UpdateParameters["BuildOrder"].DefaultValue = buildOrderText;
            dsProjectFiles.Update();
        }
        catch (SqlException)
        {}

        dlProjectFiles.EditItemIndex = -1;
        dlProjectFiles.DataBind();
    }
    protected void butParseDropBox_Click(object sender, EventArgs e)
    {
        if (tbDropBox.Text == "")
            return;

        string[] rows = tbDropBox.Text.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        if (rows.Length == 0)
            return;

        if (CurrentProject == null)
            return;

        int projectID = CurrentProject.ProjectID;
        int scriptCategoryId = Convert.ToInt32(ddScriptCategory.SelectedValue);

        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;

        using (SqlConnection conn = new SqlConnection(connStr))
        {
            //из списка достанем репозиторий проекта
            Dir rep = Repositories[Projects[projectID].ProjectURL];
            Dir child = rep.FindByPath(treeSVN.SelectedNode.DataPath);
            //проверим, не вложена ли эта папка в Kit
            string KitPath = child.GetKitPath();
            string FolderName = child.DirName;//aka ObjectType

            SqlCommand cmd = DataAccess.PAddProjectFileByName;
            cmd.Connection = conn;
            cmd.Parameters[1].Value = projectID; //@ProjectID
            cmd.Parameters[3].Value = KitPath; //@KitPath
            cmd.Parameters[4].Value = FolderName; //@FolderName
            cmd.Parameters[5].Value = scriptCategoryId; //@scriptCategoryId
            conn.Open();

            string res = "";
            string fileName;
            char[] ws = new[] { ' ', '\n', '\r', '\t' };

            foreach (string name in rows)
            {
                fileName = name.Trim(ws);
                if (fileName.Length == 0)
                    continue;
                if (fileName.EndsWith(".sql", StringComparison.CurrentCultureIgnoreCase))
                    fileName = fileName.Substring(0, fileName.Length - 4);
                if (fileName.StartsWith("dbo.", StringComparison.CurrentCultureIgnoreCase))
                    fileName = fileName.Substring(4);
                cmd.Parameters[2].Value = fileName + ".sql"; //@FileName
                cmd.ExecuteNonQuery();
                if ((int)cmd.Parameters[0].Value == 1)//file name not found
                    res = res + fileName + "\r\n";
            }
            tbDropBox.Text = res;

            conn.Close();

            dlProjectFiles.DataBind();
        }
    }
    protected void butAddSelected_Click(object sender, EventArgs e)
    {
        if (lbRepositoryFiles.SelectedIndex == -1 || dlProjecstList.SelectedIndex == -1 || treeSVN.SelectedNode == null)
            return;

        int scriptCategoryId = Convert.ToInt32(ddScriptCategory.SelectedValue);

        //получим номер проекта
        int projectID = (Int32)dlProjecstList.DataKeys[dlProjecstList.SelectedIndex];
        //из списка достанем репозиторий проекта
        Dir rep = Repositories[Projects[projectID].ProjectURL];
        //найдем в репозитории ту папку, на которой кликнули
        Dir child = rep.FindByPath(treeSVN.SelectedNode.DataPath);
        //проверим, не вложена ли эта папка в Kit
        string KitPath = child.GetKitPath();
        string FolderName = child.DirName;//aka ObjectType

        string connStr = ConfigurationManager.ConnectionStrings["DBBuildConnectionString"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            SqlCommand cmd = DataAccess.PAddProjectFileByName;
            cmd.Connection = conn;
            cmd.Parameters[1].Value = projectID; //@ProjectID
            cmd.Parameters[3].Value = KitPath; //@KitPath
            cmd.Parameters[4].Value = FolderName; //@FolderName
            cmd.Parameters[5].Value = scriptCategoryId; //@ScriptCategoryId

            SqlParameter par = cmd.Parameters[2]; //@FileName

            conn.Open();
            foreach (ListItem item in lbRepositoryFiles.Items)
            {
                if (!item.Selected)
                    continue;
                par.Value = item.Value;

                cmd.ExecuteNonQuery();

                item.Selected = false;
            }
            conn.Close();
        }

        dlProjectFiles.DataBind();
        lbRepositoryFiles.DataBind();
    }
    
    #endregion



    protected void lbDPR_Click(object sender, EventArgs e)
    {
        string query = "";
        if (CurrentProject != null)
            query = "?p=" + CurrentProject.ProjectID.ToString();

        Response.Redirect("DeploymentRequests.aspx" + query);
    }

    protected void treeSVN_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (treeSVN.SelectedNode == null)
            return;

        //найдем в репозитории ту папку, на которой кликнули
        Dir child = CurrentRepository.FindByPath(treeSVN.SelectedNode.DataPath);

        //если был определен фильтр - подготовим его
        string Filter = tbNameFilter.Text.Trim().ToUpper();
        //фильтруем если фильтр определен
        if (Filter == "")
            lbRepositoryFiles.DataSource = child.Files;
        else
        {
            IEnumerable<string> query = child.Files.Where(tmp => tmp.ToUpper().Contains(Filter) );
            lbRepositoryFiles.DataSource = query;
        }
        lbRepositoryFiles.DataBind();
    }

    protected void tbNameFilter_TextChanged(object sender, EventArgs e)
    {
        treeSVN_SelectedNodeChanged(this, null);
    }

}
