﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBBuild.DataAccess
{
    public class EnvironmentEntity
    {
        private EnvironmentEntity()
        {
        }

        public int ID { get; set; }
        public string Server { get; set; }
        public string DataBase { get; set; }

        public static EnvironmentEntity GetById(SqlConnection conn, int id)
        {
            EnvironmentEntity res = null;
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pGetEnvironment";
            cmd.Parameters.AddWithValue("EnvID", id);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                res = new EnvironmentEntity {ID = (int) dr[0], Server = (string) dr[1], DataBase = (string) dr[2]};
            }
            dr.Close();
            return res;
        }
    }
}
