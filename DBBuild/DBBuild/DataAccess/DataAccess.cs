﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DBBuild.DataAccess
{
    public class DataAccess
    {
        private const string CON_STRING_TEMPLATE = @"Data Source={0};Initial Catalog={1};Integrated Security=True";
        public static string GetConnectionString(string server, string db)
        {
            return string.Format(CON_STRING_TEMPLATE, server, db);
        }
        public static string GetConnectionString(string server)
        {
            return GetConnectionString(server, "DBBuild");
        }

        public static void AddDeployment(SqlConnection conn, int drId, int envId, long revision, bool isSuccess, string errors)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "pAddDeployment";
                cmd.Parameters.AddWithValue("DRID", drId);
                cmd.Parameters.AddWithValue("EnvironmentID", envId);
                cmd.Parameters.AddWithValue("SVNRevision", revision);
                cmd.Parameters.AddWithValue("IsSuccessfull", isSuccess);
                cmd.Parameters.AddWithValue("Errors", errors);
                cmd.ExecuteNonQuery();
            }
        }

        public static void AddDPR(SqlConnection conn, int projectId, string dprName, bool includeAllFiles)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "pAddDPR";
                cmd.Parameters.AddWithValue("ProjectID", projectId);
                cmd.Parameters.AddWithValue("DRName", dprName);
                cmd.Parameters.AddWithValue("IncludeAllFiles", includeAllFiles);
                cmd.ExecuteNonQuery();
            }
        }

        public static DeploymentRequest GetDPR(SqlConnection conn, int dprID)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "pGetDPR";
                cmd.Parameters.AddWithValue("DRID", dprID);
                DeploymentRequest res = null;
                using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                {
                    if (reader.Read())
                    {
                        res = new DeploymentRequest
                                  {
                                      ID = (int) reader["DRID"],
                                      ProjectID = (int) reader["ProjectID"],
                                      DRName = (string) reader["DRName"],
                                      IncludeAllFiles = (bool) reader["IncludeAllFiles"]
                                  };
                    }
                    reader.Close();
                }
                return res;
            }
        }

        public static Repository GetRepository(SqlConnection conn, int repositoryID)
        {
            Repository res = null;

            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PGetRepository;
            cmd.Connection = conn;
            cmd.Parameters[0].Value = repositoryID;

            using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
            {
                if (reader.Read())
                {
                    res = new Repository
                              {
                                  RepositoryID   = (int)reader[0],
                                  RepositoryName = (string)reader[1],
                                  BaseURL        = (string)reader[2],
                                  CheckoutDir    = (string)reader[3],
                                  UserName       = (string)reader[4],
                                  PasswordHash   = (string)reader[5],
                                  DBRootPath     = (string)reader[6],
                                  BranchesPath   = (string)reader[7]
                              };
                }
                reader.Close();
            }

            return res;
        }


        #region Project
        public static void DeleteProject(SqlConnection conn, int projectID)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = DataAccess.PDeleteProject;
            cmd.Connection = conn;
            cmd.Parameters[0].Value = projectID; //@ProjectID

            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static int AddProject(SqlConnection conn, Project p)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = DataAccess.PAddProject;
            cmd.Connection = conn;
            cmd.Parameters[0].Value = p.ProjectName;
            cmd.Parameters[1].Value = p.ProjectFolder;
            cmd.Parameters[2].Value = p.DatabaseID;
            cmd.Parameters[3].Value = p.RepositoryID;
            cmd.Parameters[4].Value = p.IsInTrunk;
            cmd.Parameters[5].Value = p.IsArchived;

            cmd.ExecuteNonQuery();
            conn.Close();

            return (int)cmd.Parameters[6].Value;
        }

        public static void UpdateProject(SqlConnection conn, Project p)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = DataAccess.PUpdateProject;
            cmd.Connection = conn;
            cmd.Parameters[0].Value = p.ProjectID;
            cmd.Parameters[1].Value = p.ProjectName;
            cmd.Parameters[2].Value = p.ProjectFolder;
            cmd.Parameters[3].Value = p.DatabaseID;
            cmd.Parameters[4].Value = p.RepositoryID;
            cmd.Parameters[5].Value = (p.IsInTrunk == true) ? 1 : 0;
            cmd.Parameters[6].Value = (p.IsArchived == true) ? 1 : 0;

            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static Project GetProject(SqlConnection conn, int projectID)
        {
            Project res = null;

            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PGetProject;
            cmd.Connection = conn;
            cmd.Parameters[0].Value = projectID;

            using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
            {
                if (reader.Read())
                {
                    res = new Project
                              {
                                  ProjectID     = (int)   reader[0],
                                  ProjectName   = (string)reader[1],
                                  ProjectFolder = (string)reader[2],
                                  ProjectURL    = (string)reader[3],
                                  DatabaseID    = (int)   reader[4],
                                  RepositoryID  = (int)   reader[5],
                                  IsInTrunk     = (bool)  reader[6],
                                  IsArchived    = (bool)  reader[7]
                              };
                }
                reader.Close();
            }

            return res;
        }

        public static Dictionary<int, Project> GetProjectsList(SqlConnection conn)
        {
            Dictionary<int, Project> res = new Dictionary<int,Project>();

            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PGetProjectsList;
            cmd.Connection = conn;

            Project p;
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    p = new Project
                    {
                        ProjectID     = (int)   reader[0],
                        ProjectName   = (string)reader[1],
                        ProjectFolder = (string)reader[2],
                        ProjectURL    = (string)reader[3],
                        DatabaseID    = (int)   reader[4],
                        RepositoryID  = (int)   reader[5],
                        IsInTrunk     = (bool)  reader[6],
                        IsArchived    = (bool)  reader[7]
                    };
                    res.Add(p.ProjectID, p);
                }
                reader.Close();
            }

            return res;
        }
        #endregion

        public static string[] GetObjectTypes(SqlConnection conn)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PGetObjectTypes;
            cmd.Connection = conn;

            List<string> fileTypes = new List<string>();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    fileTypes.Add((string)reader[2]); //FolderName
                }
                reader.Close();
            }

            return fileTypes.ToArray();
        }

        public static DBFile[] GetProjectFiles(SqlConnection conn, int projectID)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PGetProjectFileList;
            cmd.Parameters[0].Value = projectID;
            cmd.Connection = conn;

            List<DBFile> list = new List<DBFile>();
            DBFile dbf;
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    dbf = new DBFile
                              {
                                  FileID = (int)reader[0],
                                  FileName = (string)reader[1],
                                  FileURI = (string)reader[2],
                                  ObjectTypeID = (int)reader[3],
                                  ScriptCategoryID = (ScriptCategory)reader[5]
                              };
                    if (!reader.IsDBNull(4))
                        dbf.BuildOrder = (int)reader[4];

                    list.Add(dbf);
                }
                reader.Close();
            }

            return list.ToArray();
        }

        public static void DeleteProjectFile(SqlConnection conn, int projectFileID)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PDeleteProjectFile;
            cmd.Connection = conn;
            cmd.Parameters[0].Value = projectFileID; //@ProjectFileID

            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static DBFile[] GetDPRFiles(SqlConnection conn, int dprId)
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PGetDPRFileList;
            cmd.Parameters[0].Value = dprId;
            cmd.Connection = conn;

            List<DBFile> list = new List<DBFile>();
            DBFile dbf;
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    dbf = new DBFile
                    {
                        FileID = (int)reader[0],
                        FileName = (string)reader[1],
                        FileURI = (string)reader[2],
                        ObjectTypeID = (int)reader[3],
                        ScriptCategoryID = (ScriptCategory)reader[5]
                    };
                    if (!reader.IsDBNull(4))
                        dbf.BuildOrder = (int)reader[4];

                    list.Add(dbf);
                }
                reader.Close();
            }

            return list.ToArray();
        }


        public static Dictionary<int, string> GetDatabasesList(SqlConnection conn)
        {
            Dictionary<int, string> res = new Dictionary<int, string>();

            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = PGetDatabases;
            cmd.Connection = conn;

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    res.Add((int)reader[0], (string)reader[1]);
                }
                reader.Close();
            }

            return res;
        }



        public static SqlCommand PGetRepository
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetRepository") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@RepositoryID", SqlDbType.Int);
                return cmd;
            }
        }

        public static SqlCommand PGetProject
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetProject") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                return cmd;
            }
        }

        public static SqlCommand PGetProjectByName
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetProjectByName") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectName", SqlDbType.VarChar);
                SqlParameter par = cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                par.Direction = ParameterDirection.Output;
                return cmd;
            }
        }


        public static SqlCommand PGetProjectsList
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetProjectsList") { CommandType = CommandType.StoredProcedure };
                return cmd;
            }
        }

        public static SqlCommand PGetDatabases
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetDatabases") { CommandType = CommandType.StoredProcedure };
                return cmd;
            }
        }

        public static SqlCommand PGetDRByName
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetDRByName") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectName", SqlDbType.NVarChar);
                cmd.Parameters.Add("@DRName", SqlDbType.NVarChar);
                SqlParameter par = cmd.Parameters.Add("@DRID", SqlDbType.Int);
                par.Direction = ParameterDirection.Output;
                return cmd;
            }
        }

        public static SqlCommand PGetProjectFileList
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetProjectFileList") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                return cmd;
            }
        }

        public static SqlCommand PGetDPRFileList
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetDPRFileList") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@DRID", SqlDbType.Int);
                return cmd;
            }
        }

        public static SqlCommand PGetObjectTypes
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pGetObjectTypes") { CommandType = CommandType.StoredProcedure };
                return cmd;
            }
        }

        public static SqlCommand PAddDatabase
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pAddDatabase") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@DatabaseName", SqlDbType.VarChar);
                return cmd;
            }
        }

        public static SqlCommand PAddFile
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pAddFile") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar);
                cmd.Parameters.Add("@ObjectTypeID", SqlDbType.Int);
                cmd.Parameters.Add("@KitPath", SqlDbType.VarChar);
                SqlParameter par = cmd.Parameters.Add("@FileID", SqlDbType.Int);
                par.Direction = ParameterDirection.Output;
                return cmd;
            }
        }

        public static SqlCommand PAddProject
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pAddProject") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectName", SqlDbType.NVarChar);
                cmd.Parameters.Add("@ProjectFolder", SqlDbType.VarChar);
                cmd.Parameters.Add("@DatabaseID", SqlDbType.Int);
                cmd.Parameters.Add("@RepositoryID", SqlDbType.Int);
                cmd.Parameters.Add("@IsInTrunk", SqlDbType.Bit);
                cmd.Parameters.Add("@IsArchived", SqlDbType.Bit);
                SqlParameter par = cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                par.Direction = ParameterDirection.Output;
                return cmd;
            }
        }

        public static SqlCommand PDeleteProject
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pDeleteProject") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                return cmd;
            }
        }

        public static SqlCommand PUpdateProject
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pUpdateProject") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                cmd.Parameters.Add("@ProjectName", SqlDbType.NVarChar);
                cmd.Parameters.Add("@ProjectFolder", SqlDbType.VarChar);
                cmd.Parameters.Add("@DatabaseID", SqlDbType.Int);
                cmd.Parameters.Add("@RepositoryID", SqlDbType.Int);
                cmd.Parameters.Add("@IsInTrunk", SqlDbType.Bit);
                cmd.Parameters.Add("@IsArchived", SqlDbType.Bit);
                return cmd;
            }
        }


        public static SqlCommand PAddProjectFile
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pAddProjectFile") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                cmd.Parameters.Add("@FileID", SqlDbType.Int);
                cmd.Parameters.Add("@ScriptCategoryId", SqlDbType.Int);
                SqlParameter par = cmd.Parameters.Add("@BuildOrder", SqlDbType.Int);
                par.Value = DBNull.Value;
                par = cmd.Parameters.Add("@ProjectFileID", SqlDbType.Int);
                par.Direction = ParameterDirection.Output;
                return cmd;
            }
        }

        public static SqlCommand PAddProjectFileByName
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pAddProjectFileByName") { CommandType = CommandType.StoredProcedure };
                SqlParameter par = cmd.Parameters.Add("returnVal", SqlDbType.Int);
                par.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@ProjectID", SqlDbType.Int);
                cmd.Parameters.Add("@FileName", SqlDbType.NVarChar);
                cmd.Parameters.Add("@KitPath", SqlDbType.VarChar);
                cmd.Parameters.Add("@FolderName", SqlDbType.VarChar);
                cmd.Parameters.Add("@ScriptCategoryId", SqlDbType.Int);
                par = cmd.Parameters.Add("@BuildOrder", SqlDbType.Int);
                par.Value = DBNull.Value;
                par = cmd.Parameters.Add("@ProjectFileID", SqlDbType.Int);
                par.Direction = ParameterDirection.Output;
                return cmd;
            }
        }

        public static SqlCommand PDeleteProjectFile
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pDeleteProjectFile") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@ProjectFileID", SqlDbType.Int);
                return cmd;
            }
        }

        public static SqlCommand PClearFileScan
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pClearFileScan") { CommandType = CommandType.StoredProcedure };
                return cmd;
            }
        }

        public static SqlCommand PCleanupFiles
        {
            get
            {
                SqlCommand cmd = new SqlCommand("pCleanupFiles") { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@RepositoryID", SqlDbType.Int);
                return cmd;
            }
        }
    }
}
