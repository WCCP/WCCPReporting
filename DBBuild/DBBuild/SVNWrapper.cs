﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;
using SharpSvn;
using SharpSvn.Security;

namespace DBBuild
{
    public class SVNWrapper
    {
        SvnClient client = new SvnClient();

        //private delegate void SslServerTrust(object sender, SvnSslClientCertificateEventArgs e)
        //{
        //    e.Save = true; 
        //}

        public SVNWrapper(string User, string Password)
        {
            client.Authentication.Clear();
            client.Authentication.DefaultCredentials = new System.Net.NetworkCredential(User, Password);

            client.Authentication.SslServerTrustHandlers += delegate(object sender, SvnSslServerTrustEventArgs e)
            {
                e.AcceptedFailures = e.Failures;
                e.Save = true;
            };

            client.LoadConfiguration(@"c:\temp", true);
        }


        public long GetLastChangeRevision(string URI)
        {
            SvnInfoEventArgs info;
            var target = SvnTarget.FromString(URI);
            client.GetInfo(target, out info);
            return info.LastChangeRevision;
        }

        public long GetLatestRevision(string URI)
        {
            SvnInfoEventArgs info;
            var target = SvnTarget.FromString(URI);
            client.GetInfo(target, out info);
            return info.Revision;
        }

        public bool UriExist(string URI)
        {
            var target = SvnTarget.FromString(URI);
            Collection<SvnInfoEventArgs> info;
            bool result = client.GetInfo(target, new SvnInfoArgs { ThrowOnError = false }, out info);

            return result;
        }

        public string[] GetFileList(string URI)
        {
            if (!UriExist(URI))
                return null;

            SvnListArgs args = new SvnListArgs();
            args.Depth = SvnDepth.Files;
            args.RetrieveEntries = SvnDirEntryItems.AllFieldsV15;

            args.Revision = GetLatestRevision(URI);

            Collection<SvnListEventArgs> list = new Collection<SvnListEventArgs>();

            Uri target = new System.Uri(URI);
            client.GetList(target, args, out list);

            var files = from item in list where item.Entry.NodeKind == SvnNodeKind.File select item.Path;

            return files.ToArray();
        }

        public string[] GetDirList(string URI)
        {
            if (!UriExist(URI))
                return null;

            SvnListArgs args = new SvnListArgs();
            args.RetrieveEntries = SvnDirEntryItems.AllFieldsV15;

            args.Revision = GetLatestRevision(URI);

            Collection<SvnListEventArgs> list = new Collection<SvnListEventArgs>();

            Uri target = new System.Uri(URI);
            client.GetList(target, args, out list);

            var files = from item in list where item.Entry.NodeKind == SvnNodeKind.Directory && item.Path != "" select item.Path;

            return files.ToArray();
        }

        //returns contens of directory with infinity recursion
        public Collection<SvnListEventArgs> GetDirectory(string URI)
        {
            if (!UriExist(URI))
                return null;

            SvnListArgs args = new SvnListArgs();
            args.Depth = SvnDepth.Infinity;
            args.RetrieveEntries = SvnDirEntryItems.AllFieldsV15;

            args.Revision = GetLatestRevision(URI);

            Collection<SvnListEventArgs> list = new Collection<SvnListEventArgs>();

            Uri target = new System.Uri(URI);
            client.GetList(target, args, out list);

            return list;
        }


        public string GetFile(string URI)
        {
            SvnTarget t = SvnTarget.FromString(URI);
            string res;
            using (MemoryStream ms = new MemoryStream())
            {
                //SvnWriteArgs wa = new SvnWriteArgs();
                //wa.Revision =

                client.Write(t, ms);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms); //Encoding.GetEncoding("windows-1251")
                res = sr.ReadToEnd();
                sr.Close();
            }
            return res;
        }
    }
}
