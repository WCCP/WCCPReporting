﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBBuild
{
    public class DBFile
    {
        public int FileID;
        public string FileName;
        public string KitPath;
        public int ObjectTypeID;
        public int BuildOrder = -1;
        public ScriptCategory ScriptCategoryID;
        public string FileURI;
    }
}
