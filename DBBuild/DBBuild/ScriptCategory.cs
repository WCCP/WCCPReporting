﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBBuild
{
    public enum ScriptCategory
    {
        PreScript = 1,
        Base = 2,
        PostScript = 3
    } 
}
