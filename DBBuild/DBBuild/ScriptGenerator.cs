﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using Ionic.Zip;

#endregion

namespace DBBuild
{
    public class ScriptGenerator
    {
        #region Delegates

        public delegate void MessageDelegate(string message);

        #endregion

        #region Readonly & Static Fields

        private static Regex regexUse = new Regex(
            "^\\W*USE\\W+[0-9A-Z_\\[\\]]+",
            RegexOptions.IgnoreCase
            | RegexOptions.Multiline
            | RegexOptions.CultureInvariant
            | RegexOptions.Compiled
            );

        #endregion

        #region Constructors

        public ScriptGenerator(string ConnString, int projectID, string useDBName)
        {
            connString = ConnString;
            ProjectID = projectID;
            DeploymentRequestID = null;
            DumpDirectory = null;
            DumpEncoding = new UTF8Encoding(false); // UTF-8 w/o BOM
            UseDBName = useDBName;
        }

        public ScriptGenerator(string ConnString, int projectID, int deploymentRequestID, string useDBName)
        {
            connString = ConnString;
            ProjectID = projectID;
            DeploymentRequestID = deploymentRequestID;
            DumpDirectory = null;
            DumpEncoding = new UTF8Encoding(false); // UTF-8 w/o BOM
            UseDBName = useDBName;
        }

        #endregion

        #region Instance Properties

        private string connString;

        public DeploymentRequest DeploymentRequest { get; set; }

        public int? DeploymentRequestID
        {
            get { return DeploymentRequest == null ? (int?) null : DeploymentRequest.ID; }
            set
            {
                if (value == null)
                    DeploymentRequest = null;
                else
                {
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        DeploymentRequest = DataAccess.DataAccess.GetDPR(conn, value.Value);
                    }
                }
            }
        }

        public string DumpDirectory { get; set; }
        public Encoding DumpEncoding { get; set; }
        public Project Project { get; set; }

        public int ProjectID
        {
            get { return Project.ProjectID; }
            set
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    Project = DataAccess.DataAccess.GetProject(conn, value);
                }
            }
        }

        public long Revision { get; private set; }
        public string UseDBName { get; set; }

        #endregion

        #region Instance Methods

        public ResultScript[] Generate()
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                return Generate(conn);
            }
        }

        public ResultScript[] Generate(SqlConnection conn)
        {
            DBFile[] files = GetFiles(conn);
            if (files.Length == 0)
            {
                OnMessageSent("Project is empty, no files to process");
                return null;
            }
            Repository rep = DataAccess.DataAccess.GetRepository(conn, 1);
            SVNWrapper w = new SVNWrapper(rep.UserName, rep.PasswordHash);
            Revision = w.GetLatestRevision(files[0].FileURI);

            var scriptCollections = new[]
                                        {
                                            new
                                                {
                                                    ExecutionOrder = 1,
                                                    CategoryName = "PRE",
                                                    FileSuffix = "_PRE",
                                                    Files =
                                                files.Where(i => i.ScriptCategoryID == ScriptCategory.PreScript)
                                                },
                                            new
                                                {
                                                    ExecutionOrder = 2,
                                                    CategoryName = "DDL",
                                                    FileSuffix = "_DDL",
                                                    Files =
                                                files.Where(
                                                    i =>
                                                    i.ScriptCategoryID == ScriptCategory.Base && i.ObjectTypeID != 7)
                                                }, // NOT Kit
                                            new
                                                {
                                                    ExecutionOrder = 3,
                                                    CategoryName = "DML",
                                                    FileSuffix = "_DML",
                                                    Files =
                                                files.Where(
                                                    i =>
                                                    i.ScriptCategoryID == ScriptCategory.Base && i.ObjectTypeID == 7)
                                                }, // Kit
                                            new
                                                {
                                                    ExecutionOrder = 4,
                                                    CategoryName = "POST",
                                                    FileSuffix = "_POST",
                                                    Files =
                                                files.Where(i => i.ScriptCategoryID == ScriptCategory.PostScript)
                                                }
                                        };

            List<ResultScript> resultScripts = new List<ResultScript>();
            foreach (var scriptCollection in scriptCollections)
            {
                if (scriptCollection.Files.Count() == 0)
                    continue;

                OnMessageSent("-- Start processing " + scriptCollection.CategoryName + " files. --");

                /*
                if (scriptCollection.Files.Count() == 0)
                {
                    OnMessageSent("-- Category is empty. Skipping... --");
                    continue;
                }
                */

                StringWriter stream = new StringWriter();
                stream.WriteLine("--");
                stream.WriteLine(String.Format("-- Script auto-generated by {0} on {1}",
                                               WindowsIdentity.GetCurrent().Name, DateTime.Now));
                stream.WriteLine(String.Format("-- SVN Revision {0} ", Revision));
                stream.WriteLine("--");
                stream.WriteLine();

                if (!String.IsNullOrEmpty(UseDBName))
                    stream.WriteLine(String.Format("use {0} \r\n", UseDBName));

                string dbgDir = Path.Combine(DumpDirectory ?? String.Empty, scriptCollection.CategoryName);
                if (!String.IsNullOrEmpty(DumpDirectory))
                {
                    if (!Directory.Exists(dbgDir))
                    {
                        Directory.CreateDirectory(dbgDir);
                    }
                }

                IEnumerable<DBFile> dbFiles = scriptCollection.Files.OrderBy(f => f.BuildOrder);
                foreach (DBFile dbFile in dbFiles)
                {
                    OnMessageSent(dbFile.FileName);
                    string body = AdjustScriptContent(w.GetFile(dbFile.FileURI));

                    stream.WriteLine(String.Format("--{0}\r\n", dbFile.FileName));
                    stream.Write(body);
                    stream.WriteLine();
                    stream.WriteLine("GO");
                    stream.WriteLine();

                    if (!String.IsNullOrEmpty(DumpDirectory))
                    {
                        string dbgFile = Path.Combine(dbgDir, dbFile.FileName);
                        StreamWriter sw = new StreamWriter(dbgFile, false, DumpEncoding);
                        sw.Write(body);
                        sw.Flush();
                        sw.Close();
                    }
                }

                stream.Flush();
                stream.Close();
                resultScripts.Add(new ResultScript
                                      {
                                          ExecutionOrder = scriptCollection.ExecutionOrder,
                                          CategoryName = scriptCollection.CategoryName,
                                          SqlScript = stream.ToString()
                                      });
            }
            return resultScripts.ToArray();
        }

        public void SaveAsArchiveToStream(SqlConnection conn, string projectNamePrefix, Stream s)
        {
            ResultScript[] scripts = Generate(conn) ?? new ResultScript[] {};
            using (ZipFile zip = new ZipFile())
            {
                String readmeText = String.Format("README.TXT\n\n" +
                                                  "This is a zip file that was dynamically generated at {0}\n" +
                                                  "by an ASP.NET Page running on the machine named '{1}'.\n" +
                                                  "Count of SQL scripts which included in this project: {2}\n",
                                                  DateTime.Now.ToString("G"),
                                                  Environment.MachineName,
                                                  scripts.Length
                    );
                zip.AddEntry("readme.txt", readmeText);
                int scriptNum = 0;
                foreach (ResultScript script in scripts.OrderBy(i => i.ExecutionOrder))
                {
                    string fileName = string.Format("{0:D2}_{1}_{2}.sql", ++scriptNum, projectNamePrefix,
                                                    script.CategoryName);
                    zip.AddEntry(fileName, Encoding.UTF8.GetBytes(script.SqlScript));
                }
                zip.Save(s);
            }
        }

        private string AdjustScriptContent(string sqlScript)
        {
            return regexUse.Replace(sqlScript, string.Empty);
        }

        private DBFile[] GetFiles(SqlConnection conn)
        {
            return DeploymentRequestID.HasValue
                       ? DataAccess.DataAccess.GetDPRFiles(conn, (int) DeploymentRequestID)
                       : DataAccess.DataAccess.GetProjectFiles(conn, ProjectID);
        }

        private void OnMessageSent(string message)
        {
            if (MessageSent != null)
                MessageSent(message);
        }

        #endregion

        #region Event Declarations

        public event MessageDelegate MessageSent;

        #endregion
    }
}