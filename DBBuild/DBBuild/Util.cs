﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBBuild
{
    public class Util
    {

        public static string GetPathBeforeName(string Path, string DirName)
        {
            if (Path.EndsWith(DirName))
            {
                string tmp = Path.Substring(0, Path.Length - DirName.Length);
                if (tmp.EndsWith("/"))
                    return tmp.Substring(0, tmp.Length - 1);
                else
                    return tmp;
            }
            else
                return Path;
        }

        public static string GetProperName(string UnproperName)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char ch in UnproperName.Replace(' ', '_'))
                if (Char.IsLetterOrDigit(ch) || ch == '_')
                    sb.Append(ch);
            //string projectNamePrefix = sb.ToString();
            return sb.ToString();
        }

    }
}
