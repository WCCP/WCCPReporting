﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SharpSvn;
using da = DBBuild.DataAccess;

namespace DBBuild
{
    public class DBBuildHelper
    {
        /*
        public static void RefreshMain(string ConnectionString, int RepositoryID)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                //Clear FileScan table to track all files touched by scan process
                SqlCommand cmd = da.DataAccess.PClearFileScan;
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();

                Repository rep = da.DataAccess.GetRepository(conn, RepositoryID);

                SVNWrapper w = new SVNWrapper(rep.UserName, rep.PasswordHash);

                //refresh DB objects
                //refresh list of DBs
                string[] DBNames;
                DBNames = w.GetDirList(rep.BaseURL + "/DB");

                cmd = da.DataAccess.PAddDatabase;
                cmd.Connection = conn;
                SqlParameter par = cmd.Parameters[0];
                foreach (string DBName in DBNames)
                {
                    par.Value = DBName;
                    cmd.ExecuteNonQuery();
                }

                //Get Object types
                string[] ObjectTypes = da.DataAccess.GetObjectTypes(conn);
                string[] files;

                string uri = "";

                cmd = da.DataAccess.PAddFile;
                cmd.Connection = conn;
                cmd.Parameters[0].Value = RepositoryID;
                SqlParameter par_FileURI = cmd.Parameters[1];    // @FileName
                SqlParameter par_DBName = cmd.Parameters[2];     // @DBName
                SqlParameter par_ObjectType = cmd.Parameters[3]; // @ObjectType

                foreach (string DBName in DBNames)
                {
                    par_DBName.Value = DBName;
                    Console.WriteLine(DBName);

                    foreach (string ObjectType in ObjectTypes)
                    {
                        par_ObjectType.Value = ObjectType;
                        Console.WriteLine(String.Format("  {0}",ObjectType));

                        uri = String.Format("{0}/DB/{1}/{2}", rep.BaseURL, DBName, ObjectType);

                        if (ObjectType == "Kit")
                        {
                            string[] ProjectFolders = w.GetDirList(uri);
                            if (ProjectFolders == null) continue;
                            foreach (string folder in ProjectFolders)
                            {
                                Console.WriteLine(String.Format("    {0}", folder));
                                files = w.GetFileList(uri+'/'+folder);
                                if (files == null) continue;
                                foreach (string file in files)
                                {
                                    par_FileURI.Value = uri + "/" + folder + "/" + file;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            continue;
                        }

                        files = w.GetFileList(uri);
                        if (files == null) continue;
                        foreach (string file in files)
                        {
                            par_FileURI.Value = uri + "/" +file;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                
                //remove not-existent in SVN files from DB
                cmd = da.DataAccess.PCleanupFiles;
                cmd.Connection = conn;
                cmd.Parameters[0].Value = RepositoryID;
                cmd.ExecuteNonQuery();
            }
            
        }
        */

        public static Dir ReadRepFromSVN(string ConnectionString, string RepositoryURL)
        {
            SVNWrapper w = null;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                Repository rep = da.DataAccess.GetRepository(conn, 1);

                w = new SVNWrapper(rep.UserName, rep.PasswordHash);
            }

            Collection<SvnListEventArgs> list = w.GetDirectory(RepositoryURL);

            Dir DirTree = new Dir();

            if(list == null)
                return DirTree;

            Dir current_dir = null;
            Dir parent_dir = null;
            string NewDirPath = null;
            string CurDirPath = null;

            foreach (SvnListEventArgs item in list)
            {
                if (item.Path == "") //parent folder
                {
                    DirTree.DirName = item.Name;
                    DirTree.Path = "";

                    current_dir = DirTree;
                    parent_dir = DirTree;
                    continue;
                }
                if (item.Entry.NodeKind == SvnNodeKind.Directory)
                {
                    NewDirPath = Util.GetPathBeforeName(DirTree.Path + @"/" + item.Path, item.Name);
                    CurDirPath = Util.GetPathBeforeName(current_dir.Path, current_dir.DirName);
                    if (CurDirPath == NewDirPath) //directory on the same level as current directory
                    {
                        parent_dir = current_dir.ParentDir;
                    }
                    else if (NewDirPath.Length > CurDirPath.Length) //step in
                    {
                        parent_dir = current_dir;
                    }
                    else //step out
                    {
                        parent_dir = current_dir.ParentDir.ParentDir;
                    }
                        
                    current_dir = new Dir(){DirName = item.Name};
                    current_dir.ParentDir = parent_dir;
                    current_dir.Path = NewDirPath;
                    parent_dir.Dirs.Add(current_dir);
                    continue;
                }
                if (item.Entry.NodeKind == SvnNodeKind.File)
                {
                    //Dirs[item.Path.Substring(0, item.Path.IndexOf('/'))].Files.Add(item.Name);
                    current_dir.Files.Add(item.Name);
                    continue;
                }
            }//foreach

            return DirTree;
        }

        public static void BuildProject(string ConnectionString, int ProjectID)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                Repository rep = DataAccess.DataAccess.GetRepository(conn, 0);

                SVNWrapper w = new SVNWrapper(rep.UserName, rep.PasswordHash);

                //w.
                //SqlCommand cmd = DataAccess.pClearFileScan();
                //cmd.Connection = conn;
                //cmd.ExecuteNonQuery();

            }
        }
    }
}
