﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace DBBuild
{
    public class Dir : IHierarchyData, IHierarchicalEnumerable
    {
        public string DirName;
        public string path;
        public Dir ParentDir;
        public List<Dir> Dirs = new List<Dir>();
        public List<string> Files = new List<string>();

        public override string ToString()
        {
            return DirName;
        }

        public Dir FindByPath(string PathToFind)
        {
            if (path == PathToFind)
                return this;

            Dir res;
            foreach (Dir d in Dirs)
            {
                if (d.Path == PathToFind)
                    return d;
                else
                {
                    res = d.FindByPath(PathToFind);
                    if (res != null)
                        return res;
                }
            }

            return null;
        }

        public bool IsInKit(Dir node)
        {
            if (node.ParentDir == null)
                return false;

            if (node.ParentDir.DirName == "Kit")
                return true;
            else
                return IsInKit(node.ParentDir);
        }

        public string GetKitPath()
        {
            if (path.IndexOf(@"/Kit")!=-1)
            {
                string PathInsideKit = "";
                int pos = path.IndexOf(@"/Kit/");
                if (pos != -1)
                    PathInsideKit = path.Substring(pos + 5);
                return PathInsideKit.Length > 0 ? PathInsideKit + "/" + DirName : DirName;
            }
            else
                return "";
        }

        #region IHierarchicalEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return new DirEnum(Dirs);
        }

        public IHierarchyData GetHierarchyData(object enumeratedItem)
        {
            return enumeratedItem as IHierarchyData;
        }

        //IEnumerator IEnumerable.GetEnumerator()
        //{
        //    return (IEnumerator)GetEnumerator();
        //}

        //public DirEnum GetEnumerator()
        //{
        //    return new DirEnum(Dirs);
        //}

        #endregion

        #region IHierarchyData Members

        public IHierarchicalEnumerable GetChildren()
        {
            return (IHierarchicalEnumerable)this;
        }

        public IHierarchyData GetParent()
        {
            return ParentDir;
        }

        public bool HasChildren
        {
            get
            {
                return Dirs.Count > 0;
            }
        }

        public object Item
        {
            get { return this; }
        }

        // Gets the hierarchical path of the node.
        public string Path
        {
            get 
            {
                if (path == "")
                    return DirName;
                else
                    return ParentDir.Path + @"/" + DirName; 
            }
            set { path = value; }
        }

        public string Type
        {
            get { return "Dir"; }
        }

        #endregion
    }//class

    public class DirEnum : IEnumerator<Dir>
    {
        private List<Dir> Dirs;
        private int curIndex = -1; // Enumerators are positioned before the first element until the first MoveNext() call. 
        private Dir current = null;

        public DirEnum(List<Dir> list)
        {
            Dirs = list;
        }

        void IDisposable.Dispose() { current = null; Dirs = null; }

        #region IEnumerator<Dir> Members

        Dir IEnumerator<Dir>.Current
        {
            get { return current; }
        }

        #endregion

        #region IEnumerator Members

        object IEnumerator.Current
        {
            get { return current; }
        }

        bool IEnumerator.MoveNext()
        {
            if (++curIndex >= Dirs.Count)
            {
                return false;
            }
            else
            {
                current = Dirs[curIndex];
            }
            return true;
        }

        void IEnumerator.Reset()
        {
            curIndex = -1;
        }

        #endregion
    }//DirEnum

}
