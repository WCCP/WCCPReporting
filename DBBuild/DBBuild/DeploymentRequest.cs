﻿namespace DBBuild
{
    public class DeploymentRequest
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string DRName { get; set; }
        public bool IncludeAllFiles { get; set; }
    }
}
