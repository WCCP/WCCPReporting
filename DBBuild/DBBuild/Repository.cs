﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBBuild
{
    public class Repository
    {
        public int RepositoryID;
        public string RepositoryName;
        public string BaseURL;
        public string CheckoutDir;
        public string UserName;
        public string PasswordHash;
        public string DBRootPath;
        public string BranchesPath;

        public string TrunkPath
        {
            get { return BaseURL + DBRootPath; }
            set { }
        }

    }
}
