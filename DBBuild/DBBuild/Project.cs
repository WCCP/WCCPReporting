﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBBuild
{
    public class Project
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectFolder { get; set; }
        public string ProjectURL { get; set; }
        public bool IsArchived { get; set; }
        public bool IsInTrunk { get; set; }
        public int RepositoryID { get; set; }
        public int DatabaseID { get; set; }

        public Project()
        {
        }

        public Project(int ProjectID, string ProjectName, string ProjectFolder, string ProjectURL, int DatabaseID, int RepositoryID, bool IsInTrunk, bool IsArchived)
        {
            this.ProjectID = ProjectID;
            this.ProjectName = ProjectName;
            this.ProjectFolder = ProjectFolder;
            this.ProjectURL = ProjectURL;
            this.DatabaseID = DatabaseID;
            this.RepositoryID = RepositoryID;
            this.IsInTrunk = IsInTrunk;
            this.IsArchived = IsArchived;
        }

        public override string ToString()
        {
            return ProjectName;
        }
    }
}
