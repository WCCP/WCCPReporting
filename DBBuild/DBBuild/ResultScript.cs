﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBBuild
{
    public class ResultScript
    {
        public ResultScript()
        {
            ExecutionOrder = -1;
            CategoryName = String.Empty;
            SqlScript = String.Empty;
        }

        public int ExecutionOrder { get; set; }
        public string CategoryName { get; set; }
        public string SqlScript { get; set; }
    }
}
