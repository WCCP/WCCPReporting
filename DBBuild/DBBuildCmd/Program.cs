﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBBuild;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using DBBuild.DataAccess;
using NDesk.Options;

namespace DBBuildCmd
{
    class Program
    {
        static int Main(string[] args)
        {
            Console.WriteLine("{0} v.{1}", Assembly.GetExecutingAssembly().GetName().Name, Assembly.GetExecutingAssembly().GetName().Version);

            #region Load DLL from resources

            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(
                (sender, e) =>
                {
                    //Console.WriteLine("Resolving assembly " + e.Name);
                    
                    string fileName = "";
                    if (e.Name.StartsWith("DBBuild"))
                        fileName = "DBBuild.dll";
                    if (e.Name.StartsWith("SharpSvn"))
                        fileName = "SharpSvn.dll";
                    
                    if (fileName=="")
                        return null;
                    Assembly assembly;
                    try
                    {
                        assembly = LoadDllFromResources(fileName);
                    }
                    catch
                    {
                        Console.WriteLine("Can not resolve assembly " + e.Name);
                        assembly = null;
                    }
                    return assembly;
                    
                }
            );



            #endregion


            #region command-line processing

            string serverName = "";
            string projectName = "";
            string deploymentRequestName = "";
            string useDBName = "";
            bool dumpAllFiles = false;
            bool showHelp = false;

            var p = new OptionSet () {
   	{ "s|server=",      v => serverName = v },
   	{ "p|project=",      v => projectName = v },
   	{ "dr=",      v => deploymentRequestName = v },
   	{ "db=",      v => useDBName = v },
   	{ "dump",  v => dumpAllFiles = v != null },
   	{ "h|?|help",   v => showHelp = v != null },
   };

            List<string> extra;
            try 
            {
			
                extra = p.Parse (args);
            }
            catch (OptionException e)
            {
                Console.Write("Error in arguments:");
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `DBBuildCmd -help' for more information.");
                return -1;
            }

            if (extra.Count > 0)
            {
                Console.Write(String.Format("Error in arguments, this part is not recognized: '{0}'", string.Join(" ", extra.ToArray())));
                return -1;
            }

            if (showHelp || args.Length == 0)
            {
                Console.WriteLine("To build project:");
                Console.WriteLine("  DBBuildCmd.exe -server=<DBServerName> -project=<ProjectName> [-dump] [-db=<DBName>]");
                Console.WriteLine("To build deployment request:");
                Console.WriteLine("  DBBuildCmd.exe -server=<DBServerName> -project=<ProjectName> -dr=<DeploymentRequestName> [-dump] [-db=<DBName>]");
                Console.WriteLine("Options: ");
                Console.WriteLine("    -server or -s - DBBuild DB server");
                Console.WriteLine("    -project or -p - Project name");
                Console.WriteLine("    -dr - deployment request name name");
                Console.WriteLine("    -help or -h or -? - This message");
                Console.WriteLine("    -dump - means dump all script files in separate folder");
                Console.WriteLine("    -db - adds 'use <DBName>' at the beginning of each script");
                Console.WriteLine("Example:");
                Console.WriteLine("  DBBuildCmd.exe -server=cherry -project=\"UKK\" -db=SalesWork");
                
                return -1;
            }

            if (String.IsNullOrEmpty(serverName))
            {
                Console.WriteLine("-server is requred command-line parameter");
                return -1;
            }

            #endregion

            return DoBuild(serverName, projectName, deploymentRequestName, useDBName, dumpAllFiles);
        }

        static private int DoBuild(
            string serverName,
            string projectName,
            string deploymentRequestName,
            string useDBName,
            bool dumpAllFiles)
         {


            string connStr = DataAccess.GetConnectionString(serverName);

            if (!String.IsNullOrEmpty(projectName))
            {
                if (projectName[0] == '\"')
                    projectName = projectName.Substring(1, projectName.Length - 1);
            }
            else
            {
                Console.WriteLine("-project is not specified");
                return -1;
            }
            
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(String.Format("Can't open connection {0}", connStr));
                    Console.WriteLine(ex.Message);
                    return -1;
                }

                SqlCommand cmd = DataAccess.PGetProjectByName;
                cmd.Connection = conn;
                cmd.Parameters[0].Value = projectName;
                cmd.ExecuteNonQuery();
                if (cmd.Parameters[1].Value == DBNull.Value)
                {
                    Console.WriteLine(String.Format("Project '{0}' not found", projectName));
                    return -1;
                }
                int projectID = (int)cmd.Parameters[1].Value;

                string projectNamePrefix = Util.GetProperName(projectName);

                int DRID = -1;
                if (!String.IsNullOrEmpty(deploymentRequestName))
                {
                    cmd = DataAccess.PGetDRByName;
                    cmd.Connection = conn;
                    cmd.Parameters[0].Value = projectName;
                    cmd.Parameters[1].Value = deploymentRequestName;
                    cmd.ExecuteNonQuery();
                    if (cmd.Parameters[2].Value == DBNull.Value)
                    {
                        Console.WriteLine(String.Format("Deployment Request '{0}' not found", deploymentRequestName));
                        return -1;
                    }
                    DRID = (int)cmd.Parameters[2].Value;
                    projectNamePrefix = String.Format("{0}_{1}", projectNamePrefix, Util.GetProperName(deploymentRequestName));
                }

                Encoding encoding = new UTF8Encoding(false);
                string curDir = Directory.GetCurrentDirectory();
                ScriptGenerator sg;
                if (DRID == -1) //build project
                    sg = new ScriptGenerator(connStr, projectID, useDBName);
                else //build DR
                    sg = new ScriptGenerator(connStr, projectID, DRID, useDBName);

                sg.MessageSent += Console.WriteLine;
                if (dumpAllFiles)
                {
                    curDir = Path.Combine(curDir, "files");
                    Directory.CreateDirectory(curDir);
                    sg.DumpDirectory = curDir;
                    sg.DumpEncoding = encoding;
                }

                ResultScript[] scripts;
                try
                {
                    scripts = sg.Generate(conn) ?? new ResultScript[]{};
                }
                catch (Exception ex)
                {
                    Console.WriteLine(String.Format("Error at time of script generation: \n{0}", ex.Message));
                    return -1;
                }

                int scriptNum = 0;
                foreach (ResultScript script in scripts.OrderBy(i=>i.ExecutionOrder))
                {
                    //if (script.Files)
                    StreamWriter sw = new StreamWriter(Path.Combine(curDir, string.Format("{0:D2}_{1}_{2}.sql", ++scriptNum, projectNamePrefix, script.CategoryName)));
                    sw.Write(script.SqlScript);
                    sw.Close();
                }

                return 0;
            }
        }

        private static Assembly LoadDllFromResources(string AssemblyName)
        {
            foreach (string resource in Assembly.GetExecutingAssembly().GetManifestResourceNames())
            {
                if (!resource.EndsWith(AssemblyName))
                    continue;

                using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource))
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    byte[] buf = reader.ReadBytes((int)stream.Length);
                    try
                    {
                        return Assembly.Load(buf);
                    }
                    catch (FileLoadException)
                    {
                        //non-native assemblies can't be loaded 
                        //Console.WriteLine(ex.Message);
                        string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AssemblyName);
                        File.Delete(path);
                        using (FileStream fs = File.OpenWrite(path))
                        {
                            fs.Write(buf, 0, (int)stream.Length);
                        }

                        return Assembly.LoadFile(path);
                    }
                }
            }

            throw new ApplicationException(string.Format("Can't load {0} from resources", AssemblyName));

        }

    }
}
