﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using DBBuild.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBBuild;


namespace DBBuildTest
{
    /// <summary>
    /// Summary description for HelperTest
    /// </summary>
    [TestClass]
    public class HelperTest
    {
        public HelperTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;
        public const string ConnStr = @"Data Source=SV109\SQLEXPRESS;Initial Catalog=DBBuild;Integrated Security=SSPI;";

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestReadRepFromSVN()
        {
            Dir Root = DBBuildHelper.ReadRepFromSVN(ConnStr, @"https://svnnew.softserveinc.com/svnroot-ss/ABInBev_Projects/Project/trunk/Sources/DBRoot/DB/SalesWorks");
            //string path = Root.Dirs[1].Dirs[33].GetKitPath();
            //path = Root.Dirs[1].Dirs[33].Dirs[0].GetKitPath();
        }

        [TestMethod]
        public void TestFindByPath()
        {
            Dir d = DBBuildHelper.ReadRepFromSVN(ConnStr, @"https://svnnew.softserveinc.com/svnroot-ss/ABInBev_Projects/Project/trunk/Sources/DBRoot/DB/SalesWorks");
            Dir res = d.FindByPath(@"SalesWorks/Kit/CaPS");
        }
        
        [TestMethod]
        public void TestGetFileTypes()
        {
            SqlConnection conn = new SqlConnection(ConnStr);
            string[]res = DataAccess.GetObjectTypes(conn);
        }
        
    }
}
