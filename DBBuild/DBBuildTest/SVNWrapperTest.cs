﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBBuild;
using SharpSvn;

namespace DBBuildTest
{
    /// <summary>
    /// Summary description for SVNWrapperTest
    /// </summary>
    [TestClass]
    public class SVNWrapperTest
    {
        SVNWrapper w = new SVNWrapper("osinits", "Pulemett1!");

        public SVNWrapperTest()
        {
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetFileList()
        {
            string[] res = w.GetFileList("https://svnnew.softserveinc.com/svnroot-ss/ABInBev_Projects/Project/Branches/DBBuild DB branches");

        }

        [TestMethod]
        public void GetDirList()
        {
            string[] res = w.GetDirList("https://cvs.softservecom.com/svnroot-ssl/ABInBev/Projects/Project/trunk/Sources/DB/_Scripts/DB_clean");

        }

        [TestMethod]
        public void GetDirectory()
        {
            w.GetDirectory("https://svnnew.softserveinc.com/svnroot-ss/ABInBev_Projects/Project/Branches/DBBuild DB branches");

        }

        [TestMethod]
        public void GetFile()
        {
            string res = w.GetFile("https://cvs.softservecom.com/svnroot-ssl/ABInBev/Projects/Project/trunk/Sources/DB/_Scripts/DB_clean/restore_db.sql");
        }

    }
}
