﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DBBuild;
using DBBuild.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DBBuildTest
{
    [TestClass]
    public class DataAccessTest
    {
        private TestContext m_testContext;
        public TestContext TestContext
        {
            get { return m_testContext; }
            set { m_testContext = value; }
        }

        [TestMethod]
        public void CheckProject()
        {
            SqlConnection conn = new SqlConnection(HelperTest.ConnStr);
            conn.Open();
            Project rep = DataAccess.GetProject(conn, 12);
            conn.Close();
            Assert.IsNotNull(rep);
        }

        [TestMethod]
        public void CheckRepository()
        {
            SqlConnection conn = new SqlConnection(HelperTest.ConnStr);
            conn.Open();
            Repository rep = DataAccess.GetRepository(conn, 1);
            conn.Close();
            Assert.IsNotNull(rep);
        }

        [TestMethod]
        public void CheckProjectFiles()
        {
            SqlConnection conn = new SqlConnection(HelperTest.ConnStr);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pGetProjectsList";
            SqlDataReader dr = cmd.ExecuteReader();
            int projId = -1;
            while (dr.Read())
            {
                projId = (int) dr[0];
            }
            dr.Close();
            if (projId > 0)
            {
                var f = DataAccess.GetProjectFiles(conn, projId);
            }
            conn.Close();
        }
    }
}
