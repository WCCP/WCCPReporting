﻿using System;
using System.IO;
using System.Text;

namespace SoftServe.Core.Common.Logging {
    public class SimpleLogger {
        public string LogFileName = "log";
        private string _logFileFullName;
        private readonly Encoding _encoding;
        private StreamWriter _writer;

        public SimpleLogger() : this("log") {
        }

        public SimpleLogger(string logFileName) {
            LogFileName = logFileName;
            _encoding = Encoding.GetEncoding(1251);
            CreateLogFile();
        }

        public void LogMessage(string msg) {
            _writer.WriteLine(msg);
            _writer.Flush();
        }

        public void CreateLogFile() {
            string logFileName = LogFileName + "_" + DateTime.Today.ToString("yyyyMMdd") + ".log";
            string lLogPath = Directory.GetCurrentDirectory();
            _logFileFullName = Path.Combine(lLogPath, logFileName);
            _writer = new StreamWriter(File.Open(_logFileFullName, FileMode.Append, FileAccess.Write, FileShare.Read), _encoding);
        }

        public void LogMsgHeader(string msg) {
            LogMessage(string.Format("{0} " + msg, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        }

        public void LogException(Exception exception) {
            LogMsgHeader("\t" + exception.GetType().FullName);
            LogMessage("  Data: " + exception.Data);
            LogMessage("  Message: " + exception.Message);
            LogMessage("  Source: " + exception.Source);
            LogMessage("  StackTrace: " + exception.StackTrace);
            LogMessage("\n");
            
            if (exception.InnerException == null)
                return;
            LogMessage("Inner exception:");
            LogException(exception.InnerException);
        }

        public void Close() {
            _writer.Close();
            _writer = null;
        }

    }
}
