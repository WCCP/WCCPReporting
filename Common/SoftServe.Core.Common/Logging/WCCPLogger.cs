﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using System.Linq;
using System.Text;
using BLToolkit;
using BLToolkit.Data;
using System.Diagnostics;
using System.Globalization;
using CreateConnection;
using Logica.Reports.Common;
using Logica.Reports.Common.Crypto;

namespace SoftServe.Core.Common.Logging
{
    public static class WccpLogger
    {
        private static StringBuilder _commandTxt = new StringBuilder();
        private static DbManager _localDbMg;
        private static List<int> _objectsToLog;
        private static string _userId;

        private static DbManager LocalDbMg
        {
            get { return _localDbMg; }
        }

        public static void SetConnectionConfig(string consett, string userNameSW, string userPWDSW)
        {
            try
            {
                if (_localDbMg == null)
                {
                    SqlConnectionStringBuilder conBuld = new SqlConnectionStringBuilder(consett);

                    bool isLDB = conBuld.InitialCatalog == "LDB";
                    _userId = conBuld.UserID;

                    if (isLDB)
                    {
                        ConnectionToSWDB_V35 saCon = new ConnectionToSWDB_V35();
                        conBuld.UserID = saCon.UserID;
                        conBuld.Password = saCon.UserPWD;
                    }
                    else
                    {
                        conBuld.UserID = userNameSW;
                        Cryptographer _crypto = new Cryptographer();
                        //string s = _crypto.EncryptStr(userPWDSW);
                        conBuld.Password = _crypto.DecryptStr(userPWDSW);
                    }
                    conBuld.IntegratedSecurity = false;


                    DbManager.DefaultConfiguration = string.Empty; //to reset possible previous changes
                    DbManager.AddConnectionString(conBuld.ConnectionString);
                    _localDbMg = new DbManager();
                    _localDbMg.Command.CommandTimeout = 15*60; // 15 minutes by default

                    _objectsToLog = GetLogParams(isLDB);
                }
            }
            catch
            {
                _localDbMg = null;
            }
        }

        private static List<int> GetLogParams(bool isLDB)
        {
            try
            {
                LocalDbMg.SetCommand(CommandType.Text, String.Format("SELECT ObjectId FROM dbo.LogWccpParams WHERE IsLogging{0} = 1", !isLDB?"SW":"LDB"));
                DataTable lTable = LocalDbMg.ExecuteDataTable();
                List<int> lList = lTable.AsEnumerable().Select(p => p.Field<int>("ObjectId")).ToList();
                if (lList.Contains(0))
                {
                    return lList;
                }
                lList.Clear();
                return lList;
            }
            catch
            {
                return new List<int>();
            }
        }

        public static void WccpOpentLog(ReportParameter rp)
        {
            if (LocalDbMg == null || !_objectsToLog.Contains(rp.Id))
                return;

            try
            {
                LocalDbMg.Command.CommandText =
                    String.Format(
                        @"declare @Id table (id uniqueidentifier) INSERT  INTO dbo.LogWccp (PCName , PCUserLogin, SQLUserLogin, ReportName, ReportId, StartDate, EndDate)
                                OUTPUT  inserted.LogID into @Id
                                VALUES  ( '{0}', '{1}', '{2}', '{3}', {4}, GETDATE(), NULL) select id from @Id"
                        , Environment.MachineName, Environment.UserName, _userId, rp.Name, rp.Id);
                object outputId = LocalDbMg.ExecuteScalar();
                rp.LogId = Guid.Parse(outputId.ToString());
            }
            catch
            {
            }
        }

        public static void WccpExitLog(ReportParameter rp)
        {
            if (LocalDbMg == null || !_objectsToLog.Contains(rp.Id))
                return;
            try
            {
                if (rp.LogId != Guid.Empty)
                {
                    LocalDbMg.Command.CommandText =
                        String.Format(@"UPDATE  dbo.LogWccp SET EndDate = GETDATE() WHERE LogID = '{0}'",
                            rp.LogId);
                    LocalDbMg.ExecuteNonQuery();
                }
            }
            catch
            {
            }

        }

        public static TimeParameter InitTimeParameter()
        {
            return new TimeParameter() {StartDateTime = DateTime.Now, Stopwatch = Stopwatch.StartNew()};
        }

        public static void DoReportTraceLog(ReportParameter rp, string step, string stepDetails, string layer,
            TimeParameter tp, string msg = null)
        {
            if (LocalDbMg == null || !_objectsToLog.Contains(rp.Id))
                return;

            try
            {
                bool _isTp = tp != null;

                if (_isTp)
                    tp.Stopwatch.Stop();
                LocalDbMg.Command.CommandText =
                    String.Format(
                        @"INSERT INTO LogWccpDetails (LogID, ReportID, Step, StepDetails, Layer, StartDate, EndDate, Duration, Status, Message)
                                                         VALUES ('{0}', {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9})",
                        rp.LogId.ToString(), rp.Id, step, stepDetails, layer,
                        _isTp
                            ? tp.StartDateTime.ToString(CultureInfo.InvariantCulture)
                            : DateTime.Now.ToString(CultureInfo.InvariantCulture),
                        DateTime.Now.ToString(CultureInfo.InvariantCulture),
                        _isTp ? tp.Stopwatch.ElapsedMilliseconds.ToString(CultureInfo.InvariantCulture) : 0.ToString(),
                        msg == null ? "Ok" : "Error", msg == null ? "NULL" : "'" + msg + "'");
                LocalDbMg.ExecuteNonQuery();
            }
            catch
            {
            }
        }

        public static object DoSafeDbTraceLoge(ReportParameter rp, TimeParameter tp, DbManager db,
            SQLQueryType queryType)
        {
            if (LocalDbMg == null || !_objectsToLog.Contains(rp.Id))
                return null;

            string message = null;
            object result = null;
            try
            {
                switch (queryType)
                {
                    case SQLQueryType.DataSet:
                        result = db.ExecuteDataSet();
                        break;
                    case SQLQueryType.NonQuery:
                        result = db.ExecuteNonQuery();
                        break;
                    case SQLQueryType.Scalar:
                        result = db.ExecuteScalar();
                        break;
                    case SQLQueryType.DataTable:
                        result = db.ExecuteDataTable();
                        break;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            finally
            {
                DoReportTraceLog(rp, db.Command.CommandText,
                    ParseSqlCommand(db), LayerType.DB, tp, message);
            }
            return result;
        }

        private static string ParseSqlCommand(DbManager dbm)
        {
            try
            {
                if (dbm == null)
                    return String.Empty;
                _commandTxt.Clear();

                if (dbm.Command.CommandType == CommandType.StoredProcedure)
                {
                    _commandTxt.Append("exec " + dbm.Command.CommandText);

                    for (int i = 1; i < dbm.Command.Parameters.Count; i++)
                    {
                        var p = (SqlParameter) dbm.Command.Parameters[i];
                        _commandTxt.Append(" " + p.ParameterName + "=" + p.Value + ",");
                    }

                    if (dbm.Command.Parameters.Count > 1)
                        _commandTxt.Remove(_commandTxt.Length - 1, 1);

                    return _commandTxt.ToString();
                }
                return dbm.Command.CommandText;
            }
            catch
            {
                return String.Empty;
            }
        }

    }


    public class TimeParameter
    {
        public Stopwatch Stopwatch { get; set; }
        public DateTime StartDateTime { get; set; }
    }

    public class ReportParameter
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public Guid LogId { get; set; }
    }

    public static class LayerType
    {
        public static readonly string UI = "UI";
        public static readonly string DB = "DB";
    }

    public enum SQLQueryType
    {
        DataSet,
        Scalar,
        NonQuery,
        DataTable
    }
}
