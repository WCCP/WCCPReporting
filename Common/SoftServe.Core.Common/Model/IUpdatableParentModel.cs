﻿namespace SoftServe.Core.Common.Model {
    public interface IUpdatableParentModel : IUpdatableModel {
        /// <summary>
        /// Gets the parent object.
        /// </summary>
        /// <returns></returns>
        IUpdatableParentModel GetParentModel();

        /// <summary>
        /// Sets the parent object.
        /// </summary>
        /// <param name="parentModel"></param>
        void SetParentModel(IUpdatableParentModel parentModel);
        
        /// <summary>
        /// Changes parent object state to modified.
        /// Parent objects should be IUpdatableModel
        /// </summary>
        void MakeParentModified();
    }
}