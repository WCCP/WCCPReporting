﻿namespace SoftServe.Core.Common.Model {
    public interface ISimpleModel : IBaseModel {
        int GetId();
        void SetId(int id);

        string GetName();
        void SetName(string name);
    }
}