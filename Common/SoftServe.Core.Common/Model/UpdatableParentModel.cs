using System;

namespace SoftServe.Core.Common.Model {
    [Serializable]
    public class UpdatableParentModel : UpdatableModel, IUpdatableParentModel {
        private IUpdatableParentModel _parentModel;

        #region Implementation of IUpdatableParentModel

        /// <summary>
        /// Gets the parent object.
        /// </summary>
        /// <returns></returns>
        public IUpdatableParentModel GetParentModel() {
            return _parentModel;
        }

        /// <summary>
        /// Sets the parent object.
        /// </summary>
        /// <param name="parentModel"></param>
        public void SetParentModel(IUpdatableParentModel parentModel) {
            _parentModel = parentModel;
        }

        /// <summary>
        /// Changes parent object state to modified.
        /// Parent objects should be IUpdatableModel
        /// </summary>
        public virtual void MakeParentModified() {
            if (null == _parentModel)
                return;
            _parentModel.MakeModified();
        }

        #endregion

        public override void MakeModified() {
            base.MakeModified();
            MakeParentModified();
        }
    }
}