using System;
using System.Diagnostics;

namespace SoftServe.Core.Common.Model {
    [Serializable]
    public class SimpleModel : BaseModel, ISimpleModel {
        #region Fields

        private int _id;
        private string _name;

        #endregion

        public override string ToString() {
            return _name ?? string.Empty;
        }

        #region Constructors

        public SimpleModel() : this(0, string.Empty) {
        }

        public SimpleModel(int id) : this(id, string.Empty) {
        }

        public SimpleModel(int id, string name) {
            _id = id;
            _name = name;
        }

        #endregion

        #region BaseModel implementation

        public override void ClearKey() {
            base.ClearKey();
            SetId(0);
        }

        public override object GetKey() {
            return _id;
        }

        public override IBaseModel Clone() {
            return new SimpleModel(_id, _name);
        }

        public override bool Assign(IBaseModel model) {
            SimpleModel lModel = model as SimpleModel;
            if (null == lModel)
                return false;
            
            _id = lModel.GetId();
            _name = lModel.GetName();
            return true;
        }

        public override bool Equals(object obj) {
            Debug.Assert(obj != null, "Object should not be null");
            SimpleModel lModel = obj as SimpleModel;
            if (null == lModel)
                return false;
            
            return _id.Equals(lModel.GetId()) &&
                   _name.Equals(lModel.GetName());
        }

        public override int GetHashCode() {
            return _id.GetHashCode();
        }

        #endregion

        #region Implementation of ISimpleModel

        public int GetId() {
            return _id;
        }

        public void SetId(int id) {
            _id = id;
        }

        public string GetName() {
            return _name;
        }

        public void SetName(string name) {
            _name = name;
        }

        #endregion
    }
}