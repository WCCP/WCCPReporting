﻿using System;

namespace SoftServe.Core.Common.Model {
    [Serializable]
    public class BaseModel : IBaseModel {
        private object _key;

        public BaseModel() : this(null) {
        }

        public BaseModel(object key) {
            _key = key;
        }

        #region Implementation of IBaseModel

        public virtual bool Assign(IBaseModel model) {
            _key = model.GetKey();
            return true;
        }

        public virtual IBaseModel Clone() {
            BaseModel lModel = new BaseModel(GetKey());
            return lModel;
        }

        public virtual object GetKey() {
            return _key;
        }

        public virtual void ClearKey() {
            _key = null;
        }

        #endregion
    }
}
