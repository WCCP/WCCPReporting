using System;

namespace SoftServe.Core.Common.Model {
    [Serializable]
    public class UpdatableModel : BaseModel, IUpdatableModel {
        private bool _isNew;
        private bool _isDeleted;
        private bool _isModified;

        #region Implementation of IUpdatableModel

        /// <summary>
        /// Determines whenever object is new (not saved to storage yet).
        /// </summary>
        /// <returns>True if object is new, otherwise false.</returns>
        public bool IsNew() {
            return _isNew;
        }

        /// <summary>
        /// Determines whenever object is modified.
        /// </summary>
        /// <returns>True if object is modified, otherwise false.</returns>
        public bool IsModified() {
            return _isNew || _isModified;
        }

        /// <summary>
        /// Determines whenever object is deleted.
        /// </summary>
        /// <returns>True if object is deleted, otherwise false.</returns>
        public bool IsDeleted() {
            return _isDeleted;
        }

        /// <summary>
        /// Makes object not modified.
        /// </summary>
        /// <returns>Returns True if success.</returns>
        public virtual bool Apply() {
            _isNew = false;
            _isModified = false;
            return true;
        }

        /// <summary>
        /// Cancels all edits of object.
        /// </summary>
        public virtual void Cancel() {
            _isModified = false;
            _isDeleted = false;
        }

        /// <summary>
        /// Changes object state to new.
        /// </summary>
        public virtual void MakeNew() {
            _isNew = true;
        }

        /// <summary>
        /// Changes object state to modified.
        /// </summary>
        public virtual void MakeModified() {
            _isModified = true;
        }

        /// <summary>
        /// Changes object state to deleted.
        /// </summary>
        public void Delete() {
            _isDeleted = true;
        }

        /// <summary>
        /// Changes object state to not deleted.
        /// </summary>
        public void UnDelete() {
            _isDeleted = false;
        }

        #endregion

        #region BaseModel impementation

        public override void ClearKey() {
            _isNew = true;
            base.ClearKey();
        }

        #endregion
    }
}