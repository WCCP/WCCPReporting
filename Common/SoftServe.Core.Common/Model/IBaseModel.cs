﻿namespace SoftServe.Core.Common.Model {
    public interface IBaseModel {
        bool Equals(object model);
        bool Assign(IBaseModel model);
        IBaseModel Clone();

        object GetKey();
        void ClearKey();
    }
}
