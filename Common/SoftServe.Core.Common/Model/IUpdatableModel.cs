﻿namespace SoftServe.Core.Common.Model {
    public interface IUpdatableModel : IBaseModel {
        /// <summary>
        /// Determines whenever object is new (not saved to storage yet).
        /// </summary>
        /// <returns>True if object is new, otherwise false.</returns>
        bool IsNew();

        /// <summary>
        /// Determines whenever object is modified.
        /// </summary>
        /// <returns>True if object is modified, otherwise false.</returns>
        bool IsModified();

        /// <summary>
        /// Determines whenever object is deleted.
        /// </summary>
        /// <returns>True if object is deleted, otherwise false.</returns>
        bool IsDeleted();

        /// <summary>
        /// Makes object not modified.
        /// </summary>
        /// <returns>Returns True if success.</returns>
        bool Apply();
        
        /// <summary>
        /// Cancels all edits of object.
        /// </summary>
        void Cancel();

        /// <summary>
        /// Changes object state to new.
        /// </summary>
        void MakeNew();

        /// <summary>
        /// Changes object state to modified.
        /// </summary>
        void MakeModified();

        /// <summary>
        /// Changes object state to deleted.
        /// </summary>
        void Delete();

        /// <summary>
        /// Changes object state to not deleted.
        /// </summary>
        void UnDelete();
    }
}