﻿using SoftServe.Core.Common.View;

namespace SoftServe.Core.Common.Controller {
    public interface IViewController {
        void Start();
        void Close();
        IView GetView();

        bool Validate();
        void LoadData(bool reload = false);
        void SaveData();
    }
}