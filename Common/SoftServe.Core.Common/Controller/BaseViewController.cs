﻿using System;
using SoftServe.Core.Common.View;

namespace SoftServe.Core.Common.Controller {
    public class BaseViewController : IViewController {

        private IView _view;

        public BaseViewController(IView view) {
            Initialize(view);
        }

        #region Implementation of IViewController

        public virtual void Start() {
            LoadData();
            _view.InitializeView();
            _view.RefreshData();
            _view.ShowView();
        }

        public void Close() {
            _view.CloseView();
        }

        public IView GetView() {
            return _view;
        }

        public virtual bool Validate() {
            return true;
        }

        /// <summary>
        /// Load data from storage into the model objects. Should be overriden in descendant class.
        /// </summary>
        public virtual void LoadData(bool reload = false) {
        }

        /// <summary>
        /// Save data to storage from the model objects. Should be overriden in descendant class.
        /// </summary>
        public virtual void SaveData() {
        }

        #endregion

        private void Initialize(IView view) {
            _view = view ?? CreateView();
            _view.SetController(this);
        }

        protected virtual IView CreateView() {
            throw new Exception("You have to overload CreateView method in descendant class");
        }

    }
}