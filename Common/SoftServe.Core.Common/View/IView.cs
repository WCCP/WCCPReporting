﻿using System;
using SoftServe.Core.Common.Controller;

namespace SoftServe.Core.Common.View {
    public interface IView {
        IViewController GetController();
        void SetController(IViewController viewController);
        string GetCaption();
        void SetCaption(string caption);

        void InitializeView();
        void ShowView();
        bool CloseView();
        void HideView();

        /// <summary>
        /// Complete data input and update model data with data from controls.
        /// </summary>
        void UpdateModelData();
        void RefreshData();

        Object GetDockObject(ISimpleControlView controlView);
    }
}