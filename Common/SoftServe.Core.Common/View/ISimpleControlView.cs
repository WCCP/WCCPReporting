﻿namespace SoftServe.Core.Common.View {
    public interface ISimpleControlView : IView {
        void StartTo(IView view);
        bool ShutDown();
    }
}