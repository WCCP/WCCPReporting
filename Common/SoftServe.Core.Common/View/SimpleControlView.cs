﻿using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SoftServe.Core.Common.Controller;

namespace SoftServe.Core.Common.View {
    [ToolboxItem(false)]
    public partial class SimpleControlView : XtraUserControl, ISimpleControlView {
        #region Fields

        private IViewController _controller;
        private string _viewCaption;

        #endregion

        public SimpleControlView() {
            InitializeComponent();
            Init();
        }

        private void Init() {
            //Do some init actions here
            //Dock = DockStyle.Fill;
        }

        #region Implementation of IView

        public virtual IViewController GetController() {
            return _controller;
        }

        public virtual void SetController(IViewController viewController) {
            _controller = viewController;
        }

        public string GetCaption() {
            return _viewCaption;
        }

        public void SetCaption(string caption) {
            _viewCaption = caption;
        }

        public void InitializeView() {
            Text = _viewCaption ?? string.Empty;
        }

        public virtual void ShowView() {
            Show();
        }

        public virtual bool CloseView() {
            Hide();
            Dispose(true);
            return true;
        }

        public virtual void HideView() {
            Hide();
        }

        /// <summary>
        /// Complete data input and update model data with data from controls.
        /// </summary>
        public virtual void UpdateModelData() {
        }

        public virtual void RefreshData() {
        }

        public virtual object GetDockObject(ISimpleControlView controlView) {
            return this;
        }

        #endregion

        #region Implementation of ISimpleControlView

        public virtual void StartTo(IView view) {
            Parent = (Control) view.GetDockObject(this);
            BringToFront();
            Dock = DockStyle.Fill;
        }

        public virtual bool ShutDown() {
            if (CloseView())
                return true;
            return false;
        }

        #endregion
    }
}
