﻿namespace Logica.Reports.ConfigXmlParser.GridExtension
{
    //XtraGrid use XtraXmlSerrializer so we can't use XmlEnumAttribute to set serrialized values to old.
    public enum NullZeroTransform
    {
        //Show values as is. 0->0, NULL->EmptyString
        NoTransform,
        //Show NULL as 0. 0->0, NULL->0
        NullToZero,
        //Show 0 as empty string. 0->EmptyString, NULL->EmptyString
        ZeroToNull
    }
}
