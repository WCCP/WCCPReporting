﻿using System;
using System.Collections.Generic;
using DevExpress.XtraGrid.Views.BandedGrid;
using System.ComponentModel;
using DevExpress.Utils.Serializing;

namespace Logica.Reports.ConfigXmlParser.GridExtension
{
    public class DynamicColumn : BandedGridColumn, ICloneable
    {
        private int _height;

        private readonly Dictionary<NullZeroTransform, string> nullZeroTransformMapping = new Dictionary<NullZeroTransform, string>
                                                                                              {
                                                                                                  {NullZeroTransform.NoTransform, "default"},
                                                                                                  {NullZeroTransform.NullToZero, "true"},
                                                                                                  {NullZeroTransform.ZeroToNull, "false"}
                                                                                              };

        public DynamicColumn()
            : base()
        {
            OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            OptionsColumn.ReadOnly = true;
            OptionsColumn.AllowEdit = false;
            Width = 75;
            WidthLocked = true;
            NullZeroTransformation = NullZeroTransform.NullToZero;
            EditedRows = new List<int>();
            MaximumTotal = 0m;
            RemoveFieldPrefix = false;
            ExpandColumnWithOwnerBand = false;
            DataValidation = DataValidationRule.None;
        }

        /// <summary>
        /// Gets or sets the field prefix, 
        /// by which the column will be bound to datasource fields.
        /// </summary>
        /// <value>The field prefix.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        public string FieldPrefix { get; set; }
        
        /// <summary>
        /// Gets or sets the field which will be used in update operations, 
        /// For internal use only. This field populated for dynamic fields only (fields where FieldPrefix is set).
        /// </summary>
        /// <value>The field prefix.</value>
        [Browsable(false)]
        public string UpdateField { get; set; }
        
        /// <summary>
        /// Gets or sets if field prefix should be removed in caption, 
        /// </summary>
        /// <value>The field prefix.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        [DefaultValue(false)]
        public bool RemoveFieldPrefix { get; set; }

        /// <summary>
        /// Gets or sets if field should be copied with owner band and band should have caption of column 
        /// </summary>
        /// <value>The field prefix.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        [DefaultValue(false)]
        public bool ExpandColumnWithOwnerBand { get; set; }


        [Category("Options")]
        [Browsable(true)]
        [XtraSerializableProperty]
        [DefaultValue(DataValidationRule.None)]
        public DataValidationRule DataValidation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the header caption must be drawn vertically or horizontally.
        /// </summary>
        /// <value><c>true</c> if [vertical caption]; otherwise, <c>false</c>.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        public bool VerticalCaption { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the column can contain zeroes 
        /// or should be replaced with Empty string. Used only for serrialization to keep compatibility with old reports!!!
        /// </summary>
        /// <value><c>true</c> if [allow zeroes]; otherwise, <c>false</c>.</value>
        [Browsable(false)]
        [XtraSerializableProperty]
        [DefaultValue("true")]
        public string AllowZeroes
        {
            get { return nullZeroTransformMapping[NullZeroTransformation]; }
            set
            {
                foreach (KeyValuePair<NullZeroTransform, string> pair in nullZeroTransformMapping)
                {
                    if (!pair.Value.Equals(value)) continue;
                    NullZeroTransformation = pair.Key;
                    break;
                }
            }
        }

        /// <summary>
        /// Gets or sets the null &lt;-&gt; zero transformation rules.
        /// This value used in UI.
        /// </summary>
        /// <value>The transformation enum value.</value>
        [Category("Data")]
        [Browsable(true)]
        [DefaultValue(NullZeroTransform.NullToZero)]
        public NullZeroTransform NullZeroTransformation { get; set; }

        /// <summary>
        /// Gets or sets the maximum total for the column.
        /// Used to auto-calculate default values for columns with top limit.
        /// </summary>
        /// <value>The maximum total.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        [DefaultValue(0)]
        public decimal MaximumTotal { get; set; }

        /// <summary>
        /// Contains the list of modified rows' handles.
        /// Used to auto-calculate default values for columns with top limit.
        /// </summary>
        /// <value>The edited rows.</value>
        public List<int> EditedRows { get; set; }

        /// <summary>
        /// Gets or sets the height of the column header box.
        /// </summary>
        /// <value>The height.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        public int Height
        {
            get
            {
                if (View != null)
                {
                    return View.ColumnPanelRowHeight;
                }
                else
                {
                    return _height;
                }
            }
            set
            {
                if (View != null)
                {
                    View.ColumnPanelRowHeight = value;
                }
                else
                {
                    _height = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the data associated with the current band.
        /// Overriden to hide from property grid!
        /// </summary>
        /// <value>
        /// An object containing information which is associated with the current band.
        /// </value>
        [Browsable(false)]
        public override object Tag
        {
            get { return base.Tag; }
            set { base.Tag = value; }
        }

        #region ICloneable Members

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return CloneNoBoxing();
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// Doesn't require a boxing operation.
        /// </summary>
        /// <returns></returns>
        public DynamicColumn CloneNoBoxing()
        {
            DynamicColumn newColumn = new DynamicColumn();
            newColumn.AppearanceCell.Assign(AppearanceCell);
            newColumn.AppearanceHeader.Assign(AppearanceHeader);
            newColumn.OptionsColumn.Assign(OptionsColumn);
            newColumn.OptionsFilter.Assign(OptionsFilter);
            newColumn.SummaryItem.Assign(SummaryItem);
            newColumn.FilterMode = FilterMode;
            newColumn.ShowButtonMode = ShowButtonMode;
            newColumn.VerticalCaption = VerticalCaption;
            newColumn.ToolTip = ToolTip;
            newColumn.Caption = Caption;
            newColumn.Width = Width;
            //newColumn.AllowZeroes = AllowZeroes;
            newColumn.NullZeroTransformation = NullZeroTransformation;
            newColumn.UnboundExpression = UnboundExpression;
            newColumn.UnboundType = UnboundType;
            newColumn.DisplayFormat.Assign(DisplayFormat);
            return newColumn;
        }

        #endregion // ICloneable Members
    }
}