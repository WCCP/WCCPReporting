﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace Logica.Reports.ConfigXmlParser.GridExtension
{
    /// <summary>
    /// Represents a collection of GroupBands
    /// </summary>
    public class GroupBandCollection : GridBandCollection
    {
        public GroupBandCollection(BandedGridView view) : base(view, null) { }

        /// <summary>
        /// Creates a new band with default settings.
        /// </summary>
        /// <returns>
        /// A <see cref="T:DevExpress.XtraGrid.Views.BandedGrid.GridBand"/> object that represents the created band.
        /// </returns>
        public override GridBand CreateBand()
        {
            return new GroupBand();
        }
    }
}
