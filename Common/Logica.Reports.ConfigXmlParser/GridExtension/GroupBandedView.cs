﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid;
using System.IO;
using DevExpress.Utils;
using System.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.Data;
using System.Drawing.Drawing2D;
using DevExpress.Data.Filtering.Helpers;
using DevExpress.XtraEditors.Controls;
using System.Web;
using DevExpress.Data.Filtering;

namespace Logica.Reports.ConfigXmlParser.GridExtension
{
    /// <summary>
    /// Represents a grid view, supporting,
    /// by which the grouping must be performed.
    /// </summary>
    public class GroupBandedView : BandedGridView
    {
        #region Private members

        private ExpressionEvaluator summaryEvaluator;
        private Dictionary<string, List<string>> groupValues;
        private const string GroupRowHandleColumn = "GroupRowHandle";

        #endregion // Private members

        #region Public properties

        /// <summary>
        /// Gets the options, that describe what layout data must be serialized/deserialized.
        /// </summary>
        /// <value>The serialization options.</value>
        public OptionsLayoutGrid SerializationOptions
        {
            get
            {
                OptionsLayoutGrid layoutOptions = new OptionsLayoutGrid();
                layoutOptions.Columns.StoreAllOptions = true;
                layoutOptions.Columns.StoreAppearance = true;
                layoutOptions.Columns.StoreLayout = true;
                layoutOptions.Columns.AddNewColumns = true;
                layoutOptions.Columns.RemoveOldColumns = false;
                layoutOptions.StoreAllOptions = false;
                layoutOptions.StoreAppearance = true;
                layoutOptions.StoreDataSettings = true;
                layoutOptions.StoreVisualOptions = true;

                return layoutOptions;
            }
        }

        /// <summary>
        /// Gets the grouped bands.
        /// </summary>
        /// <value>The grouped bands.</value>
        public List<GroupBand> GroupedBands
        {
            get
            {
                List<GroupBand> bands = new List<GroupBand>();
                GetAllBands(bands, Bands, true);
                return bands;
            }
        }

        public Dictionary<string, List<string>> GroupValues
        {
            get
            {
                return groupValues;
            }
        }

        #endregion // Public properties

        #region Constructors

        public GroupBandedView() : this(null) { }
        public GroupBandedView(GridControl grid) : base(grid) 
        {
            OptionsCustomization.AllowChangeBandParent = true;
            OptionsCustomization.AllowBandMoving = true;
            OptionsCustomization.AllowColumnResizing = true;
            OptionsCustomization.AllowRowSizing = true;
            OptionsCustomization.AllowGroup = true;

            OptionsBehavior.AutoPopulateColumns = false;
            OptionsBehavior.AutoExpandAllGroups = true;
            OptionsBehavior.KeepGroupExpandedOnSorting = true;
            OptionsBehavior.SummariesIgnoreNullValues = true;
            OptionsView.ShowGroupPanel = false;
            OptionsView.ShowIndicator = false;

            CustomColumnDisplayText += GroupBandedView_CustomColumnDisplayText;
            CustomDrawColumnHeader += GroupBandedView_CustomDrawColumnHeader;
            CustomDrawBandHeader += GroupBandedView_CustomDrawBandHeader;
            CustomSummaryCalculate += GroupBandedView_CustomSummaryCalculate;
            CellValueChanged += GroupBandedView_CellValueChanged;
            ValidatingEditor += GroupBandedView_ValidatingEditor;

        }

        #endregion // Constructors

        #region Public methods

        /// <summary>
        /// Configures and fills the grid.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        public void FillData(DataTable dataSource) {
            if (dataSource == null)
                return;

            DataTable newDataSource = new DataTable("GridData");
            List<GroupBand> groupedBands = GroupedBands;
            List<string> groupByFields = GetGroupByFields(dataSource, true);
            List<string> agregateFields = GetAgregateFields();
            List<string> invisibleFields = GetGroupByFields(dataSource, false);

            foreach (GroupBand groupBand in groupedBands)
                if (!string.IsNullOrEmpty(groupBand.GroupCaptionColumn) &&
                    !groupBand.CreateDynamicBands &&
                    dataSource.Columns.Contains(groupBand.GroupCaptionColumn)) {
                    string bandCaption = dataSource.DefaultView[0][groupBand.GroupCaptionColumn] as string;
                    if (!string.IsNullOrEmpty(bandCaption))
                        groupBand.Caption = bandCaption;
                }

            if (agregateFields == null || agregateFields.Count == 0) {
                BindColumns(dataSource);
                GridControl.DataSource = dataSource;
                SetupSubTotals();
                (GridControl.DefaultView as GridView).UpdateTotalSummary();
                return;
            }

            foreach (string fieldName in groupByFields)
                newDataSource.Columns.Add(fieldName, dataSource.Columns[fieldName].DataType);

            foreach (string fieldName in invisibleFields)
                newDataSource.Columns.Add(fieldName, dataSource.Columns[fieldName].DataType);

            foreach (string fieldName in agregateFields) {
                if (!dataSource.Columns.Contains(fieldName))
                    continue;

                DataColumn col = newDataSource.Columns.Add(fieldName, dataSource.Columns[fieldName].DataType);
                if (IsNumber(dataSource.Columns[fieldName].DataType))
                    col.DefaultValue = 0.0;
            }

            groupValues = new Dictionary<string, List<string>>();
            foreach (GroupBand groupBand in groupedBands) {
                GridBandCollection bandCollection = groupBand.Collection ?? Bands;
                List<string> fieldValues = GetGroupFieldValues(groupBand.GroupColumn, dataSource);
                groupValues.Add(groupBand.GroupColumn, fieldValues);
                GroupBand newBand = null;

                if (!groupBand.CreateDynamicBands) {
                    newBand = groupBand.CloneNoBoxing();
                    newBand.Visible = true;
                    bandCollection.Insert(groupBand.Index, newBand);
                }

                for (int i = 0; i < fieldValues.Count; i++) {
                    string fieldValue = fieldValues[i];
                    dataSource.DefaultView.RowFilter = string.Format("[{0}]='{1}'", groupBand.GroupColumn, fieldValue);
                    bool hasValues = dataSource.DefaultView.Count > 0;
                    string bandCaption = string.Empty;
                    object groupColumnValue = null;
                    if (hasValues) {
                        if (!string.IsNullOrEmpty(groupBand.GroupCaptionColumn))
                            bandCaption = dataSource.DefaultView[0][groupBand.GroupCaptionColumn] as string;
                        if (!string.IsNullOrEmpty(groupBand.GroupColumn))
                            groupColumnValue = dataSource.DefaultView[0][groupBand.GroupColumn];
                    }
                    dataSource.DefaultView.RowFilter = string.Empty;

                    if (!hasValues)
                        continue;

                    if (groupBand.CreateDynamicBands) {
                        newBand = groupBand.CloneNoBoxing();
                        newBand.Visible = true;
                        newBand.Caption = bandCaption;
                        newBand.ToolTip = bandCaption;
                        newBand.GroupColumnValue = groupColumnValue;
                        bandCollection.Insert(groupBand.Index, newBand);
                    }

                    foreach (GridColumn column in groupBand.Columns) {
                        DynamicColumn dynamicColumn = column as DynamicColumn;
                        if (dynamicColumn == null)
                            continue;
                        string newFieldName = string.Format("{0}_{1}", dynamicColumn.FieldName, i);

                        DynamicColumn newColumn = dynamicColumn.CloneNoBoxing();
                        newColumn.Visible = true;
                        newColumn.FieldName = newFieldName;
                        newColumn.SummaryItem.FieldName = newFieldName;
                        if (!groupBand.CreateDynamicBands)
                            newColumn.Caption = fieldValue;
                        else if (dynamicColumn.Caption == string.Empty && groupBand.Columns.Count == 1)
                            newColumn.Caption = newBand.Caption;

                        if (column.UnboundType != UnboundColumnType.Bound)
                            newColumn.UnboundExpression = newColumn.UnboundExpression.Replace("_##", string.Format("_{0}", i));
                        else {
                            Type columnType = dataSource.Columns[dynamicColumn.FieldName].DataType;
                            DataColumn col = newDataSource.Columns.Add(newFieldName, columnType);
                            //added NoTransform processing
                            if (IsNumber(columnType) && newColumn.NullZeroTransformation != NullZeroTransform.NoTransform)
                                col.DefaultValue = 0.0;
                        }
                        newBand.Columns.Add(newColumn);
                    }
                }
                groupBand.Visible = false;
            }

            foreach (GroupBand groupBand in groupedBands)
            {
                List<string> fieldValues = GetGroupFieldValues(groupBand.GroupColumn, dataSource);
                for (int i = 0; i < fieldValues.Count; i++)
                {
                    string fieldValue = fieldValues[i];
                    dataSource.DefaultView.RowFilter = string.Format("[{0}] = '{1}'", groupBand.GroupColumn, fieldValue);
                    foreach (DataRowView row in dataSource.DefaultView)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (string groupByField in groupByFields)
                        {
                            if (row[groupByField] == null)
                            {
                                continue;
                            }
                            if (sb.Length != 0)
                            {
                                sb.Append(" AND ");
                            }
                            if (row[groupByField].ToString() == string.Empty)
                            {
                                sb.AppendFormat("[{0}] IS NULL", groupByField);
                            }
                            else
                            {
                                sb.AppendFormat("[{0}] = '{1}'", groupByField, row[groupByField].ToString().Replace("'", "''"));
                            }
                        }
                        newDataSource.DefaultView.RowFilter = sb.ToString();
                        if (newDataSource.DefaultView.Count > 0)
                        {
                            foreach (DataRowView r in newDataSource.DefaultView)
                            {
                                foreach (GridColumn column in groupBand.Columns)
                                {
                                    if (column.UnboundType != UnboundColumnType.Bound)
                                    {
                                        continue;
                                    }
                                    string dynamicColumnName = string.Format("{0}_{1}", column.FieldName, i);
                                    double operand1;
                                    double operand2;
                                    if (r[dynamicColumnName] != DBNull.Value &&
                                        double.TryParse(r[dynamicColumnName].ToString(), out operand1) &&
                                        double.TryParse(row[column.FieldName].ToString(), out operand2))
                                    {
                                        r[dynamicColumnName] = operand1 + operand2;
                                        r[column.FieldName] = operand1 + operand2;
                                    }
                                    else
                                    {
                                        r[dynamicColumnName] = row[column.FieldName];
                                        r[column.FieldName] = row[column.FieldName];
                                    }
                                }
                            }
                            newDataSource.AcceptChanges();
                        }
                        else
                        {
                            DataRow r = newDataSource.NewRow();
                            foreach (string fld in groupByFields)
                            {
                                r[fld] = row[fld];
                            }
                            foreach (string fld in invisibleFields)
                            {
                                r[fld] = row[fld];
                            }
                            
                            foreach (GridColumn col in groupBand.Columns)
                            {
                                if (col.UnboundType != UnboundColumnType.Bound)
                                {
                                    continue;
                                }
                                if (row[col.FieldName] != DBNull.Value)
                                {
                                    double operand;
                                    if ((double.TryParse(row[col.FieldName].ToString(), out operand)))
                                    {
                                        r[string.Format("{0}_{1}", col.FieldName, i)] = operand;
                                    }
                                    else
                                    {
                                        r[string.Format("{0}_{1}", col.FieldName, i)] = row[col.FieldName];
                                    }
                                }
                                r[col.FieldName] = row[col.FieldName];
                            }
                            newDataSource.Rows.Add(r);
                            newDataSource.AcceptChanges();
                        }
                    }
                }
            }
            newDataSource.DefaultView.RowFilter = string.Empty;
            GridControl.DataSource = newDataSource;
            SetupSubTotals();
            (GridControl.DefaultView as GridView).UpdateTotalSummary();
        }

        /// <summary>
        /// Configures the grid view using xml layout.
        /// </summary>
        /// <param name="xmlLayout">The XML layout.</param>
        public void ConfigureGridView(string xmlLayout)
        {
            if (string.IsNullOrEmpty(xmlLayout))
            {
                return;
            }
            Columns.Clear();
            Bands.Clear();
            using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    xmlLayout = HttpUtility.HtmlDecode(xmlLayout);
                    writer.Write(xmlLayout);
                    writer.Flush();
                    stream.Position = 0;
                    RestoreLayoutFromStream(stream, SerializationOptions);
                }
            }

            OptionsView.ShowGroupPanel = false; // Прячем группировку (hotfix: должно быть прописанов конфиге).
            OptionsView.ShowIndicator = false;
        }

        /// <summary>
        /// Creates the group band collection.
        /// </summary>
        /// <returns></returns>
        protected override GridBandCollection CreateBands()
        {
            return new GroupBandCollection(this);
        }

        /// <summary>
        /// Creates the column collection.
        /// </summary>
        /// <returns></returns>

        protected override GridColumnCollection CreateColumnCollection()
        {
            return new DynamicColumnCollection(this);
        }

        #endregion // Public methods

        #region Helper methods

        /// <summary>
        /// Gets values from grouping field to show in bands.
        /// </summary>
        /// <param name="groupFieldName">Name of the grouping field.</param>
        /// <param name="dataSource">The data source.</param>
        /// <returns>Grouping field values.</returns>
        private List<string> GetGroupFieldValues(string groupFieldName, DataTable dataSource)
        { 
            List<string> groupFieldValues = new List<string>();
            if (dataSource.Columns.Contains(groupFieldName))
            {
                foreach (DataRow r in dataSource.Rows)
                {
                    string fieldValue = r[groupFieldName].ToString();
                    if (!string.IsNullOrEmpty(fieldValue) && !groupFieldValues.Contains(fieldValue))
                    {
                        groupFieldValues.Add(fieldValue);
                    }
                }
            }
            return groupFieldValues;
        }

        /// <summary>
        /// Gets the GroupBy fields.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        /// <param name="visible"></param>
        /// <returns>GroupBy fields</returns>
        private List<string> GetGroupByFields(DataTable dataSource, bool visible)
        {
            List<string> groupByFields = new List<string>();
            foreach (BandedGridColumn col in Columns)
            {
                if (((visible && col.Visible) || (!visible && !col.Visible))
                    && !GroupedBands.Contains((GroupBand)col.OwnerBand) && dataSource.Columns.Contains(col.FieldName))
                {
                    groupByFields.Add(col.FieldName);
                }
            }
            return groupByFields;
        }

        /// <summary>
        /// Gets the agregate fields, which must show under grouping band.
        /// </summary>
        /// <returns>Agregate field names</returns>
        public List<string> GetAgregateFields()
        {
            List<string> groupFields = new List<string>();
            foreach (GroupBand band in GroupedBands)
            {
                foreach (GridColumn col in band.Columns)
                {
                    if (col.UnboundType == UnboundColumnType.Bound)
                    {
                        groupFields.Add(col.FieldName);
                    }
                }
            }
            return groupFields;
        }

        /// <summary>
        /// Gets field names from datasource by specified prefix.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <param name="dataSource">The data source.</param>
        /// <returns>List of field names.</returns>
        private List<string> GetFieldsByPrefix(string prefix, DataTable dataSource)
        {
            List<string> fields = new List<string>();
            if (dataSource != null)
            {
                foreach (DataColumn column in dataSource.Columns)
                {
                    if (column.ColumnName.StartsWith(prefix))
                    {
                        fields.Add(column.ColumnName);
                    }
                }
            }
            return fields;
        }

        private void BindColumns(DataTable dataSource)
        {
            List<DynamicColumn> dynamicColumns = new List<DynamicColumn>();
            foreach (BandedGridColumn column in Columns)
            {
                DynamicColumn dynamicColumn = column as DynamicColumn;
                if (dynamicColumn != null && !string.IsNullOrEmpty(dynamicColumn.FieldPrefix) && dynamicColumn.VisibleIndex != -1)
                {
                    dynamicColumns.Add(dynamicColumn);
                }
            }
            foreach (DynamicColumn dynamicColumn in dynamicColumns)
            {
                string fieldPrefix = dynamicColumn.FieldPrefix;
                int columnIndex = dynamicColumn.ColIndex;
                int bandIndex = -1;
                List<string> fieldNames = GetFieldsByPrefix(fieldPrefix, dataSource);
                GridBand ownerBand = dynamicColumn.OwnerBand;
                if (ownerBand != null)
                {
                    ownerBand.Columns.Remove(dynamicColumn);
                    bandIndex = ownerBand.Index;
                }
                Columns.Remove(dynamicColumn);
                int fieldIndex = 0;
                foreach (string fieldName in fieldNames)
                {
                    string caption = dynamicColumn.RemoveFieldPrefix ? fieldName.Substring(fieldPrefix.Length) : fieldName;
                    DynamicColumn newColumn = dynamicColumn.CloneNoBoxing();
                    if (newColumn.SummaryItem.SummaryType != SummaryItemType.Custom)
                    {
                        newColumn.SummaryItem.FieldName = fieldName;
                    }
                    if (newColumn.SummaryItem.SummaryType != SummaryItemType.None)
                    {
                        GroupSummary.Add(newColumn.SummaryItem.SummaryType, newColumn.SummaryItem.FieldName, newColumn, newColumn.SummaryItem.DisplayFormat, newColumn.SummaryItem.Format);
                    }
                    newColumn.ToolTip = caption;
                    newColumn.Visible = true;
                    newColumn.Caption = caption;
                    newColumn.FieldName = fieldName;
                    newColumn.UpdateField = string.Format("{0}{1}", fieldPrefix, ++fieldIndex);

                    Columns.Add(newColumn);
                    if (ownerBand != null)
                    {
                        if (dynamicColumn.ExpandColumnWithOwnerBand && ownerBand is GroupBand)
                        {
                            var newBand = ((GroupBand) ownerBand).CloneNoBoxing();
                            newBand.Caption = caption;
                            newBand.Columns.Add(newColumn);
                            if (ownerBand.ParentBand == null)
                            {
                                Bands.Insert(bandIndex++, newBand);
                            }
                            else
                            {
                                ownerBand.ParentBand.Children.Insert(bandIndex++, newBand);
                            }
                        }
                        else
                        {
                            ownerBand.Columns.Insert(columnIndex++, newColumn);
                        }
                    }
                }
                if (ownerBand!=null && dynamicColumn.ExpandColumnWithOwnerBand)
                {
                    if (ownerBand.ParentBand == null)
                    {
                        Bands.Remove(ownerBand);
                    }
                    else
                    {
                        ownerBand.ParentBand.Children.Remove(ownerBand);
                    }
                }
            }
        }

        private void GetAllBands(List<GroupBand> bandList, GridBandCollection bands, bool onlyGroup)
        {
            foreach (GridBand band in bands)
            {
                GroupBand groupBand = band as GroupBand;
                if (groupBand != null && groupBand.ReallyVisible && (!onlyGroup || groupBand.IsGroup))
                {
                    bandList.Add(groupBand);
                }
                if (band.HasChildren)
                {
                    GetAllBands(bandList, band.Children, onlyGroup);
                }
            }
        }

        private void CustomDrawHeader(CustomDrawObjectEventArgs e, AppearanceObject appearance, string headerText, bool isVertical)
        {
            if (e == null || appearance == null)
            {
                return;
            }
            bool backgroudCustomDraw = false;
            DevExpress.Utils.Drawing.HeaderObjectInfoArgs info = (DevExpress.Utils.Drawing.HeaderObjectInfoArgs)e.Info;
            if (appearance.Options.UseBackColor && appearance.BackColor != Color.Empty)
            {
                Rectangle r = e.Info.Bounds;
                info.Caption = string.Empty;

                Color backcolor = Color.FromArgb(100, appearance.BackColor);
                Color backcolor2 = Color.FromArgb(100, appearance.BackColor2);

                r.Inflate(-1, -1);
                e.Painter.DrawObject(e.Info);
                int angle = GetGradientAngle(appearance.GradientMode);
                using (LinearGradientBrush brush = new LinearGradientBrush(r, backcolor, backcolor2, angle, true))
                {
                    e.Graphics.FillRectangle(brush, r);
                }
                if (!isVertical)
                {
                    e.Graphics.DrawString(headerText, appearance.Font, appearance.GetForeBrush(e.Cache), info.CaptionRect, appearance.TextOptions.GetStringFormat());
                    e.Handled = true;
                }
                backgroudCustomDraw = true;
            }

            if (isVertical)
            {
                Rectangle textRect = info.CaptionRect;
                info.Caption = string.Empty;

                if (!backgroudCustomDraw && e.Painter != null)
                {
                    e.Painter.DrawObject(info);
                }
                GraphicsState state = e.Graphics.Save();

                StringFormat sf = appearance.TextOptions.GetStringFormat();
                sf.Trimming = StringTrimming.EllipsisCharacter;
                textRect = Transform(e.Graphics, -90, textRect);
                e.Graphics.DrawString(headerText, appearance.Font, appearance.GetForeBrush(e.Cache) , textRect, sf);
                e.Graphics.Restore(state);
                
                e.Handled = true;
            }
        }

        private Rectangle Transform(Graphics g, int degree, Rectangle r)
        {
            g.RotateTransform(degree);
            float cos = (float)Math.Round(Math.Cos(degree * (Math.PI / 180)), 2);
            float sin = (float)Math.Round(Math.Sin(degree * (Math.PI / 180)), 2);
            Rectangle r1 = r;
            r1.X = (int)(r.X * cos + r.Y * sin);
            r1.Y = (int)(r.X * (-sin) + r.Y * cos);
            r1.Offset(-r.Height, 0);
            r1.Width = r.Height;
            r1.Height = r.Width;

            return r1;
        }

        private void SetupSubTotals()
        {
            GridGroupSummaryItemCollection gsiDisplaySummary = new GridGroupSummaryItemCollection(this);
            bool hasGroupIndex = false;
            bool hasGroupColumns = false;
            foreach (GridColumn column in Columns)
            {
                if (column.GroupIndex != -1)
                {
                    column.Group();
                    hasGroupIndex = true;
                }
                if (column.SummaryItem != null && column.SummaryItem.SummaryType != SummaryItemType.None)
                {
                    gsiDisplaySummary.Add(column.SummaryItem.SummaryType, column.SummaryItem.FieldName, column, column.SummaryItem.DisplayFormat, column.SummaryItem.Format);
                    hasGroupColumns = true;
                }
            }
            if (hasGroupColumns && hasGroupIndex)
            {
                OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
                GroupSummary.Assign(gsiDisplaySummary);
            }
            //OptionsView.ShowFooter = hasGroupColumns;
            SetDefaultValuesForLimitedColumns();
        }

        private bool IsNumber(Type type)
        {
            return (type == typeof(int) || type == typeof(long) || type == typeof(decimal) || type == typeof(double));           
        }

        /// <summary>
        /// Gets the angle by gradient mode.
        /// </summary>
        /// <param name="gradientMode">The gradient mode.</param>
        /// <returns>Angle value</returns>
        private int GetGradientAngle(LinearGradientMode gradientMode)
        {
            int angle = 0;
            switch (gradientMode)
            {
                case LinearGradientMode.Horizontal:
                    angle = 0;
                    break;
                case LinearGradientMode.Vertical:
                    angle = 90;
                    break;
                case LinearGradientMode.BackwardDiagonal:
                    angle = 45;
                    break;
                case LinearGradientMode.ForwardDiagonal:
                    angle = 225;
                    break;
                default:
                    break;
            }
            return angle;
        }

        private decimal CalculateColumnSummary(DataTable dataTable, string fieldName, SummaryItemType summaryType, int? rowGroupHandle)
        {
            if (summaryType == SummaryItemType.Count)
            {
                return dataTable.Rows.Count;
            }
            decimal result = 0m;
            decimal currentValue = 0m;
            foreach (DataRow r in dataTable.Rows)
            {
                if (rowGroupHandle != null && rowGroupHandle != Convert.ToInt32(r[GroupRowHandleColumn]))
                {
                    continue;
                }
                switch (summaryType)
                { 
                    case SummaryItemType.Sum:
                        result += Convert.ToDecimal(r[fieldName]);
                        break;
                    case SummaryItemType.Average:
                        result += Convert.ToDecimal(r[fieldName]) / dataTable.Rows.Count;
                        break;
                    case SummaryItemType.Max:
                        currentValue = Convert.ToDecimal(r[fieldName]);
                        if (currentValue > result)
                        {
                            result = currentValue;
                        }
                        break;
                    case SummaryItemType.Min:
                        currentValue = Convert.ToDecimal(r[fieldName]);
                        if (currentValue < result)
                        {
                            result = currentValue;
                        }
                        break;
                    default:
                        result += Convert.ToDecimal(r[fieldName]);
                        break;
                }
            }
            return result;
        }

        private void SetDefaultValuesForLimitedColumns()
        {
            foreach (GridColumn col in Columns)
            {
                DynamicColumn dynamicColumn = col as DynamicColumn;
                DataView dataSource = DataSource as DataView;
                if (dynamicColumn != null && dynamicColumn.MaximumTotal != 0m && dynamicColumn.VisibleIndex != -1 && dataSource != null)
                { 
                    bool isChanged = false;
                    foreach (DataRowView r in dataSource)
                    {
                        if (r[dynamicColumn.FieldName] != DBNull.Value && r[dynamicColumn.FieldName] != null)
                        {
                            if (Convert.ToDecimal(r[dynamicColumn.FieldName]) != 0m)
                            {
                                isChanged = true;
                                break;
                            }
                        }
                    }
                    if (!isChanged)
                    {
                        CellValueChanged -= GroupBandedView_CellValueChanged;
                        decimal averageValue = dataSource.Count == 0 ? 0 : dynamicColumn.MaximumTotal / (decimal)dataSource.Count;
                        for (int i = 0; i < dataSource.Count; i++)
                        {
                            int rowHandler = GetRowHandle(i);
                            SetRowCellValue(rowHandler, dynamicColumn, averageValue);
                        }
                        CellValueChanged += GroupBandedView_CellValueChanged;
                    }
                }
            }
        }

        #endregion // Helper methods

        #region Event handlers

        public void GroupBandedView_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex == GridControl.InvalidRowHandle) return; //means that value for filter
            DynamicColumn column = e.Column as DynamicColumn; 
            if (column != null)
            {
                if (column.NullZeroTransformation == NullZeroTransform.NoTransform) 
                    return;
                
                double cellValue;
                object objCellValue = e.Value; // GetRowCellValue не возвращает значение для полей по которым идет группировка.
                if (objCellValue == null || objCellValue == DBNull.Value)
                {
                    if (IsNumber(column.ColumnType) && column.NullZeroTransformation == NullZeroTransform.NullToZero)
                    {
                        e.DisplayText = column.DisplayFormat.GetDisplayText(decimal.Zero);
                    }
                    else
                    {
                        e.DisplayText = string.Empty;
                    }
                }
                else if (double.TryParse(objCellValue.ToString(), out cellValue) && cellValue == 0.0)
                {
                    if (column.NullZeroTransformation == NullZeroTransform.ZeroToNull)
                    {
                        e.DisplayText = string.Empty;
                    }
                }
            }
        }

        private void GroupBandedView_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            DynamicColumn column = e.Column as DynamicColumn;
            if (column != null)
            {
                CustomDrawHeader(e, e.Column.AppearanceHeader, column.GetTextCaption(), column.VerticalCaption);
            }
            //e.Info.Bounds = new Rectangle(0, 0, 100, 200);
            //e.Graphics.FillRectangle(new SolidBrush(Color.Blue), e.Bounds.X - 50, e.Bounds.Y, e.Bounds.Width + 50, e.Bounds.Height);
        }

        private void GroupBandedView_CustomDrawBandHeader(object sender, BandHeaderCustomDrawEventArgs e)
        {
            GroupBand band = e.Band as GroupBand;
            if (band != null)
            {
                CustomDrawHeader(e, e.Band.AppearanceHeader, band.GetTextCaption(), band.VerticalCaption);
            }
        }

        private void GroupBandedView_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            BandedGridColumn column = Columns[((GridSummaryItem)e.Item).FieldName];
            if (DataSource != null && column != null && column.UnboundType != UnboundColumnType.Bound && !string.IsNullOrEmpty(column.UnboundExpression))
            {
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    if (!column.OwnerBand.Visible)
                        return;

                    string summaryExpression = column.UnboundExpression;
                    var fieldNames = System.Text.RegularExpressions.Regex.Matches(summaryExpression, @"[[{].*?[}\]]");
                    foreach (System.Text.RegularExpressions.Match field in fieldNames)
                    {
                        string fld = field.Groups[0].Value;
                        string fieldName = fld.Trim('[', ']');
                        double summary = 0.0;
                        GridColumn gridColumn = Columns.ColumnByFieldName(fieldName);
                        if (gridColumn == null)
                            gridColumn = Columns.ColumnByName(fieldName);

                        if (e.IsTotalSummary)
                            summary = Convert.ToDouble(gridColumn.SummaryItem.SummaryValue);

                        if (e.IsGroupSummary) {
                            summary = Convert.ToDouble(GetGroupRowValue(e.GroupRowHandle, gridColumn));
                            GridView view = sender as GridView;
                            foreach (GridGroupSummaryItem item in view.GroupSummary) {
                                if (item.FieldName == fieldName) {
                                    int parentRowHandle = view.GetParentRowHandle(e.RowHandle);
                                    summary = Convert.ToDouble(view.GetGroupSummaryValue(parentRowHandle, item));
                                    break;
                                }
                            }
                        }

                        summaryExpression = summaryExpression.Replace(fld, summary.ToString("#.00").Replace(",", "."));
                    }
                    
                    Exception ex;

                    CriteriaOperator criteriaOperator = CriteriaOperator.Parse(summaryExpression);
                    summaryEvaluator = DataController.CreateExpressionEvaluator(criteriaOperator, true, out ex);
                    
                    if (ex != null) {
                        throw ex;
                    } 
                    
                    if (summaryEvaluator != null) {//try to use custom summary if exists
                        try
                        {
                            double expressionResult = Convert.ToDouble(summaryEvaluator.Evaluate(null));

                            e.TotalValue = double.IsNaN(expressionResult) || double.IsPositiveInfinity(expressionResult) || double.IsNegativeInfinity(expressionResult) ?
                                0 : expressionResult;
                        }
                        catch (Exception)// if exists but doesn't work ;)
                        {
                            e.TotalValue = 0;
                        }
                        
                    }
                }
            }
        }

        private void GroupBandedView_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            DynamicColumn column = e.Column as DynamicColumn;
            DataView dataSource = DataSource as DataView;
            if (dataSource != null && column != null && column.MaximumTotal != 0m)
            {
                if (!column.EditedRows.Contains(e.RowHandle))
                {
                    column.EditedRows.Add(e.RowHandle);
                }
                int notEditedRowsCount = dataSource.Count - column.EditedRows.Count;
                decimal totalByColumn = 0m;
                foreach (int rowHandle in column.EditedRows)
                {
                    totalByColumn += Convert.ToDecimal(GetRowCellValue(rowHandle, column));
                }
                decimal averageValue = decimal.Round(((column.MaximumTotal - totalByColumn) * 100m / (decimal)notEditedRowsCount)) / 100m;

                CellValueChanged -= GroupBandedView_CellValueChanged;
                for (int i = 0; i < dataSource.Count; i++)
                {
                    int rowHandler = GetRowHandle(i);
                    if (!column.EditedRows.Contains(rowHandler))
                    {
                        SetRowCellValue(rowHandler, column, averageValue);
                    }
                }
                CellValueChanged += GroupBandedView_CellValueChanged;
            }
        }

        private void GroupBandedView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            DynamicColumn column = this.FocusedColumn as DynamicColumn;
            DataView dataSource = DataSource as DataView;
            if (dataSource != null && column != null && column.MaximumTotal != 0m)
            { 
                decimal cellValue = 0m;
                try
                {
                    cellValue = Convert.ToDecimal(e.Value);
                }
                catch (InvalidCastException ex)
                {
                    e.Valid = false;
                    e.ErrorText = ex.Message;
                    return;
                }

                decimal totalByColumn = cellValue;
                for (int i = 0; i < dataSource.Count; i++)
                {
                    int rowHandle = GetRowHandle(i);
                    if (column.EditedRows.Contains(rowHandle) && this.FocusedRowHandle != rowHandle)
                    {
                        totalByColumn += Convert.ToDecimal(GetRowCellValue(rowHandle, column));
                    }
                }
                if (totalByColumn > column.MaximumTotal)
                {
                    e.Valid = false;
                    e.ErrorText = string.Format("Сумма по колонке не должна превышать {0}!", column.MaximumTotal);
                }
                else
                {
                    e.Valid = true;
                    e.ErrorText = string.Empty;
                }
            }
        }

        #endregion // Event handlers
    }
}
