﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.Utils.Serializing;
using System.ComponentModel;
using DevExpress.XtraGrid.Columns;

namespace Logica.Reports.ConfigXmlParser.GridExtension
{
    /// <summary>
    /// Represents data structure for bands, supporting grouping
    /// </summary>
    public class GroupBand : GridBand, ICloneable
    {
        private int _height;

        public GroupBand()
        {
            CreateDynamicBands = true;
        }
        /// <summary>
        /// Gets or sets the field name in datasource, 
        /// by which the grouping must be performed.
        /// </summary>
        /// <value>The group column name.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        public string GroupColumn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets field name, where the captions will be taken.
        /// </summary>
        /// <value>The group caption column.</value>
        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        public string GroupCaptionColumn
        {
            get;
            set;
        }

        /// <summary>
        /// Represents the value in the data source, which this band groups by.
        /// </summary>
        /// <value>The group column value.</value>
        [Browsable(false)]
        public object GroupColumnValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the data associated with the current band.
        /// Overriden to hide from property grid!
        /// </summary>
        /// <value>
        /// An object containing information which is associated with the current band.
        /// </value>
        [Browsable(false)]
        public override object Tag
        {
            get
            {
                return base.Tag;
            }
            set
            {
                base.Tag = value;
            }
        }

        protected override GridBandCollection CreateBandCollection()
        {
            return new GroupBandCollection(this.View);
        }

        /// <summary>
        /// Gets a value indicating whether this band has a grouping behaviour.
        /// </summary>
        /// <value><c>true</c> if this band has a grouping behaviour; otherwise, <c>false</c>.</value>
        [Browsable(false)]
        public bool IsGroup
        {
            get
            {
                return !string.IsNullOrEmpty(GroupColumn);
            }
        }

        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        public bool VerticalCaption
        {
            get;
            set;
        }

        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        [DefaultValue(true)]
        public bool CreateDynamicBands
        {
            get;
            set;
        }

        [Category("Data")]
        [Browsable(true)]
        [XtraSerializableProperty]
        public int Height
        {
            get
            {
                if (View != null)
                {
                    return View.BandPanelRowHeight;
                }
                else
                {
                    return _height;
                }
            }
            set
            {
                if (View != null)
                {
                    View.BandPanelRowHeight = value;
                }
                else
                {
                    _height = value;
                }
            }
        }


        #region ICloneable Members

        public object Clone()
        {
            return CloneNoBoxing();
        }

        public GroupBand CloneNoBoxing()
        {
            GroupBand newBand = new GroupBand();
            newBand.AppearanceHeader.Assign(AppearanceHeader);
            newBand.OptionsBand.Assign(OptionsBand);
            newBand.Width = Width;
            newBand.AutoFillDown = AutoFillDown;
            newBand.Fixed = Fixed;
            newBand.MinWidth = MinWidth;
            newBand.Visible = true;
            newBand.Caption = Caption;
            newBand.ToolTip = ToolTip;
            newBand.GroupColumn = GroupColumn;
            newBand.GroupCaptionColumn = GroupCaptionColumn;
            newBand.GroupColumnValue = GroupColumnValue;
            newBand.VerticalCaption = VerticalCaption;
            newBand.RowCount = RowCount;
            return newBand;
        }

        #endregion
    }
}