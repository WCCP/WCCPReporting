﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace Logica.Reports.ConfigXmlParser.GridExtension
{
    public class DynamicColumnCollection : BandedGridColumnCollection
    {
        public DynamicColumnCollection(ColumnView view) : base(view) { }

        protected override GridColumn  CreateColumn()
        {
            return new DynamicColumn();
        }
    }
}
