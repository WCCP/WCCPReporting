﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.ConfigXmlParser
{
    /// <summary>
    /// Transform xml to object model
    /// </summary>
    public static class XmlParserManager
    {
        /// <summary>
        /// Convert xml to object model 
        /// </summary>
        /// <param name="xml">Xml settings structure</param>
        /// <returns></returns>
        public static Report ConvertXmlToModel(string xml)
        {
            if (String.IsNullOrEmpty(xml))
                return new Report();
            XmlSerializer serializer = new XmlSerializer(typeof(Report));

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            
            //Convert inner XML text
            XmlNodeList list1 = doc.GetElementsByTagName("grid");
            foreach (XmlNode node in list1)
                node.InnerText = node.InnerXml;
            XmlNodeList list2 = doc.GetElementsByTagName("chart");
            foreach (XmlNode node in list2)
                node.InnerText = node.InnerXml;
            
            TextReader tReader = new StringReader(doc.OuterXml);
            XmlReader xmlReader = XmlReader.Create(tReader);
            
            return (Report)serializer.Deserialize(xmlReader);
        }

    }

}