﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace Logica.Reports.ConfigXmlParser.Model
{
    /// <summary>
    /// Sql parameter container
    /// </summary>
    [XmlRoot("param")]
    public class Parameter : ICloneable
    {
        private string name;
        /// <summary>
        /// Name of parameter
        /// </summary>
        [XmlElement("name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private SqlDbType dataType;
        /// <summary>
        /// Type of parameter data
        /// </summary>
        [XmlElement("datatype")]
        public SqlDbType DataType
        {
            get { return dataType; }
            set { dataType = value; }
        }

        private string defaultValue;
        /// <summary>
        /// Default parameter value
        /// </summary>
        [XmlElement("defaultvalue")]
        public string DefaultValue
        {
            get { return defaultValue; }
            set { defaultValue = value; }
        }
        /// <summary>
        /// Create standart SqlParameter object
        /// </summary>
        /// <returns>SqlParameter object</returns>
        public SqlParameter ConvertToSqlParameter()
        {
            SqlParameter sqlParam = new SqlParameter();
            sqlParam.ParameterName = Name;
            sqlParam.SqlDbType = DataType;
            if (((DataType == SqlDbType.BigInt) 
                || (DataType == SqlDbType.Int))
                && string.IsNullOrEmpty(Convert.ToString(DefaultValue)))
            {
                sqlParam.Value = DBNull.Value;
            }
            else
            {
                sqlParam.Value = DefaultValue;
            }
            return sqlParam;
                            
        }

        #region ICloneable Members
        /// <summary>
        /// Clone Parameter
        /// </summary>
        /// <returns>Cloned object</returns>
        public object Clone()
        {
            return new Parameter() { DataType = DataType, DefaultValue = DefaultValue, Name = Name };
        }

        #endregion
    }
}
