﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Data;
using Logica.Reports.Common;

namespace Logica.Reports.ConfigXmlParser.Model
{
    /// <summary>
    /// Sql query settings
    /// </summary>
    [XmlRoot("query")]
    public class Query : ICloneable
    {        
        private SQLQueryType sqlQueryType;
        /// <summary>
        /// Type of query
        /// </summary>
        [XmlElement("sqlquerytype")]
        public SQLQueryType SqlQueryType
        {
            get { return sqlQueryType; }
            set { sqlQueryType = value; }
        }
        private string name;
        /// <summary>
        /// Stored procedure name
        /// </summary>
        [XmlElement("name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private Parameter[] parameters;
        /// <summary>
        /// Parameters list
        /// </summary>
        [XmlArray("params")]
        [XmlArrayItem("param")]
        public Parameter[] Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        #region ICloneable Members
        /// <summary>
        /// Clone query
        /// </summary>
        /// <returns>Cloned object</returns>
        public object Clone()
        {
            Query newQuery = new Query() { Name = Name, SqlQueryType = SqlQueryType };
            List<Parameter> lst = new List<Parameter>();
            if (null != Parameters)
            {
                foreach (Parameter p in Parameters)
                    lst.Add((Parameter)p.Clone());
                newQuery.Parameters = lst.ToArray();
            }
            return newQuery;
        }

        #endregion
    }
}
