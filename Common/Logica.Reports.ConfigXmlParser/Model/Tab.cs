﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Logica.Reports.ConfigXmlParser.Model
{
    public class Tab : ICloneable
    {
        public Tab()
        {
            PrintingFitToPage = false;
        }

        [XmlAttribute("printingfittopage")]
        public bool PrintingFitToPage { get; set; }

        /// <summary>
        /// Tab sort order
        /// </summary>
        private int order;
        [XmlAttribute("order")]
        public int Order
        {
            get { return order; }
            set { order = value; }
        }
        /// <summary>
        /// Tab unique identifier
        /// </summary>
		private Guid id;
        [XmlElement("id")]
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        /// <summary>
        /// Caption of tab
        /// </summary>
        private string header;
        [XmlElement("header")]
        public string Header
        {
            get { return header; }
            set { header = value; }
        }
        
        /// <summary>
        /// Collection of tables
        /// </summary>
        private Table[] tables;
        [XmlArray("tables")]
        [XmlArrayItem("table")]
        public Table[] Tables
        {
            get { return tables; }
            set { tables = value; }
        }
        /// <summary>
        /// Create clone
        /// </summary>
        /// <returns>cloned object</returns>
        public object Clone()
        {//using for dinamicaly tabs
            Tab newTab = new Tab() { Header = Header, Id = Id, Order = Order };
            List<Table> lst = new List<Table>();
            foreach (Table t in this.Tables)
                lst.Add((Table)t.Clone());
            newTab.Tables = lst.ToArray();
            return newTab;
        }
    }
}
