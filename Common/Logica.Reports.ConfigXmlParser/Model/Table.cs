﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Logica.Reports.ConfigXmlParser.Model
{
    /// <summary>
    /// Different types of table internal data
    /// </summary>
    public enum TableType
    {
        Undefined,
        Plan,
        //readonly
        Fact
    }
    /// <summary>
    /// Different structure types
    /// </summary>
    public enum ShowMode
    {
        Grid,
        Chart,
        GridChart,
        /// <summary>
        /// Used for calculations and executed before any visible call
        /// </summary>
        Invisible
    }
    /// <summary>
    /// Define visibility proprties
    /// </summary>
    public enum DisplayType
    {
        Everywhere,
        FormOnly,
        ExportDocOnly
    }
    /// <summary>
    /// Define different positions of chart item
    /// </summary>
    public enum ChartPosition
    {
        Left,
        Top,
        Right,
        Bottom
    }

    /// <summary>
    /// Define different positions of chart item
    /// </summary>
    public enum GridPosition
    {
        Left,
        Top,
        Right,
        Bottom
    }

    /// <summary>
    /// Define table structure
    /// </summary>
    [XmlRoot("table")]
    public class Table : ICloneable
    {
        private TableType type;
        /// <summary>
        /// Type of table internal data
        /// </summary>
        [XmlAttribute("type")]
        public TableType Type
        {
            get { return type; }
            set { type = value; }
        }

        private ShowMode mode = ShowMode.Grid;
        /// <summary>
        /// Structure type
        /// </summary>
        [XmlAttribute("mode")]
        public ShowMode Mode
        {
            get { return mode; }
            set { mode = value; }
        }
        private DisplayType display = DisplayType.Everywhere;
        /// <summary>
        /// Define visibility properties
        /// </summary>
        [XmlAttribute("display")]
        public DisplayType Display
        {
            get { return display; }
            set { display = value; }
        }
        private ChartPosition position = ChartPosition.Bottom;

        /// <summary>
        /// Define position of chart item
        /// </summary>
        [XmlAttribute("position")]
        public ChartPosition Position
        {
            get { return position; }
            set { position = value; }
        }

        private GridPosition gridPosition = GridPosition.Bottom;
        /// <summary>
        /// Define position of grid item
        /// </summary>
        [XmlAttribute("gridposition")]
        public GridPosition GridPosition
        {
            get { return gridPosition; }
            set { gridPosition = value; }
        }

		private Guid id;
        /// <summary>
        /// Table unique identifier
        /// </summary>         
        [XmlElement("id")]
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        private bool hideHeader = false;
        /// <summary>
        /// Should show table haeder
        /// </summary>
        [XmlAttribute("hideheader")]
        public bool HideHeader
        {
            get { return hideHeader; }
            set { hideHeader = value; }
        }
        private string header;
        /// <summary>
        /// Table haeder
        /// </summary>
        [XmlElement("header")]
        public string Header
        {
            get { return header; }
            set { header = value; }
        }

        private int tabIndex;
        /// <summary>
        /// Table position in internal tab control
        /// </summary>
        [XmlElement("tabindex")]
        public int TabIndex
        {
            get { return tabIndex; }
            set { tabIndex = value; }
        }

        private Query[] queries;
        /// <summary>
        /// List of queries
        /// </summary>
        [XmlArray("queries")]
        [XmlArrayItem("query")]
        public Query[] Queries
        {
            get { return queries; }
            set { queries = value; }
        }

        private Body body;
        /// <summary>
        /// Grid and chart configuration
        /// </summary>
        [XmlElement("body")]
        public Body Body
        {
            get { return body; }
            set { body = value; }
        }
        /// <summary>
        /// Clone table
        /// </summary>
        /// <returns>cloned object</returns>
        public object Clone()
        {//using for dinamicaly tables
            Table newTable = new Table(){Body = Body,Mode = Mode,Position = Position,TabIndex = TabIndex, Id=Id, Type = Type,Header = Header, GridPosition = GridPosition, HideHeader = HideHeader};
            List<Query> lst = new List<Query>();
            if (null != Queries)
            {
                foreach (Query q in Queries)
                    lst.Add((Query)q.Clone());
                newTable.Queries = lst.ToArray();
            }
            return newTable;
        }
    }
}
