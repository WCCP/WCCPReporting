﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Logica.Reports.ConfigXmlParser.Model 
{
    /// <summary>
    /// Whole report settings container
    /// </summary>
    [XmlRoot("report")]
    public class Report 
    {
        public Report()
        {
            PrintingMargin = 120;
        }

        private Preconditions preconditions;
        /// <summary>
        /// Scripts for creating temporary table
        /// </summary>
        [XmlElement("preconditions")]
        public Preconditions Preconditions
        {
            get { return preconditions; }
            set { preconditions = value; }
        }
        private string skinName = "Caramel";//"Valentine";
        /// <summary>
        /// Style name from 
        /// </summary>
        /// <seealso cref="http://www.devexpress.com/Help/?document=winconcepts/customdocument2534.htm"/>
        [XmlAttribute("skinname")]
        public string SkinName
        {
            get { return skinName; }
            set { skinName = value; }
        }
        private bool exportHtml = false;
        /// <summary>
        /// Should export to html
        /// </summary>
        [XmlAttribute("exporthtml")]
        public bool ExportHtml
        {
            get { return exportHtml; }
            set { exportHtml = value; }
        }
        private bool exportMht = false;
        /// <summary>
        /// Should export to mht
        /// </summary>
        [XmlAttribute("exportmht")]
        public bool ExportMht
        {
            get { return exportMht; }
            set { exportMht = value; }
        }
        private bool exportPdf = true;
        /// <summary>
        /// Should export to pdf
        /// </summary>
        [XmlAttribute("exportpdf")]
        public bool ExportPdf
        {
            get { return exportPdf; }
            set { exportPdf = value; }
        }
        private bool exportRtf = true;
        /// <summary>
        /// Should export to rtf
        /// </summary>
        [XmlAttribute("exportrtf")]
        public bool ExportRtf
        {
            get { return exportRtf; }
            set { exportRtf = value; }
        }
        private bool exportTxt = false;
        /// <summary>
        /// Should export to txt
        /// </summary>
        [XmlAttribute("exporttxt")]
        public bool ExportTxt
        {
            get { return exportTxt; }
            set { exportTxt = value; }
        }
        private bool exportXls = true;
        /// <summary>
        /// Should export to xls
        /// </summary>
        [XmlAttribute("exportxls")]
        public bool ExportXls 
        {
            get { return exportXls; }
            set { exportXls = value; }
        }
        private bool exportCsv = false;
        /// <summary>
        /// Should export to csv
        /// </summary>
        [XmlAttribute("exportcsv")]
        public bool ExportCsv
        {
            get { return exportCsv; }
            set { exportCsv = value; }
        }
        private bool exportImage = false;
        /// <summary>
        /// Should export to image
        /// </summary>
        [XmlAttribute("exportimage")]
        public bool ExportImage
        {
            get { return exportImage; }
            set { exportImage = value; }
        }
        private bool exportXlsx = true;
        /// <summary>
        /// Should export to xlsx
        /// </summary>
        [XmlAttribute("exportxlsx")]
        public bool ExportXlsx
        {
            get { return exportXlsx; }
            set { exportXlsx = value; }
        }

        [XmlAttribute("printingmargin")]
        public int PrintingMargin { get; set; }

        private Tab[] tabs;
        /// <summary>
        /// Tab collection
        /// </summary>
        [XmlArray("tabs")]
        [XmlArrayItem("tab")]
        public Tab[] Tabs
        {
            get { return tabs; }
            set { tabs = value; }
        }
    }
}
