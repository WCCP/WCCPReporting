﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Logica.Reports.ConfigXmlParser.Model
{
    public class Init
    {
        private Query[] queries;
        /// <summary>
        /// Temporary table creation scripts
        /// </summary>
        [XmlArray("queries")]
        [XmlArrayItem("query")]
        public Query[] Queries
        {
            get { return queries; }
            set { queries = value; }
        }
    }
}
