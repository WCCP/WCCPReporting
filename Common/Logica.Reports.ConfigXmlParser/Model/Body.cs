﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Logica.Reports.ConfigXmlParser.Model
{
    /// <summary>
    /// Reprsent XML configuration
    /// </summary>
    [XmlRoot("body")]
    public class Body
    {
        private string gridXml;
        /// <summary>
        /// Grid configuration
        /// </summary>
        [XmlElement("grid")]
        public string GridXml
        {
            get { return gridXml; }
            set { gridXml = value; }
        }
        private string chartXml;
        /// <summary>
        /// Chart configuration
        /// </summary>
        [XmlElement("chart")]
        public string ChartXml
        {
            get { return chartXml; }
            set { chartXml = value; }
        }
    }
}
