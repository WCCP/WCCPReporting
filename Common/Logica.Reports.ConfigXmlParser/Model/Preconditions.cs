﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Logica.Reports.ConfigXmlParser.Model
{
    public class Preconditions
    {
        private Init preInit;
        /// <summary>
        /// Temporary table creation pre init scripts
        /// </summary>
        [XmlElement("preinit")]
        public Init PreInit
        {
            get { return preInit; }
            set { preInit = value; }
        }
        private Init onInit;
        /// <summary>
        /// Temporary table creation init scripts
        /// </summary>
        [XmlElement("oninit")]
        public Init OnInit
        {
            get { return onInit; }
            set { onInit = value; }
        }
    }
}
