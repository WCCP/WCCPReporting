﻿using System;

namespace Logica.Reports.DataAccess.Exceptions {
    public class SQLUserIsNotSpecified : Exception {
        private const string ErrorMessage = "Login \"{0}\" is failed. The user is not associated with a trusted SQL Server connection.";

        public SQLUserIsNotSpecified(string userName)
            : base(string.Format(ErrorMessage, userName)) {
        }
    }
}