﻿using System;

namespace Logica.Reports.DataAccess.Exceptions {
    public class ConnectionFailedException : Exception {
        private const string ErrorMessage = "Connection is failed.\nThe connection string {0} is wrong.\n{1}";

        public ConnectionFailedException(string connectionString, Exception exception)
            : base(string.Format(ErrorMessage, connectionString, exception.Message), exception) {
        }
    }
}