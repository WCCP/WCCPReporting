﻿using System;
using System.Data;
using Logica.Reports.Common;
using System.Data.SqlClient;

namespace Logica.Reports.DataAccess
{
    public class ScriptExecutor : IDisposable
    {
        SqlConnection conn = new SqlConnection(HostConfiguration.DBSqlConnection);

        public DataSet Execute(string scriptName, SqlParameter[] parameters)
        {
            return Execute(scriptName, parameters, CommandType.StoredProcedure);
        }
        public DataSet Execute(string scriptName, SqlParameter[] parameters, CommandType commandType)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(scriptName, conn);
            cmd.CommandTimeout = 3000;
            cmd.CommandType = commandType;
            if(null != parameters)
                cmd.Parameters.AddRange(parameters);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public int ExecuteNonQuery(string scriptName, SqlParameter[] parameters)
        {
            return ExecuteNonQuery(scriptName, parameters, CommandType.StoredProcedure);
        }
        public int ExecuteNonQuery(string scriptName, SqlParameter[] parameters, CommandType commandType)
        {
            SqlCommand cmd = new SqlCommand(scriptName, conn);
            cmd.CommandType = commandType;
            if (null != parameters)
                cmd.Parameters.AddRange(parameters);
            return cmd.ExecuteNonQuery();
        }

        public void Dispose()
        {
            if (null != conn)
                conn.Close();
        }
    }
}
