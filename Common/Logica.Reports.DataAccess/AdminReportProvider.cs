﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace Logica.Reports.DataAccess
{
	public class AdminReportProvider
	{
		public static void DeleteReport(int reportId)
		{
			DataAccessLayer.ExecuteNonQueryStoredProcedure
					(CommonQueries.PRC_REPORT_DELETE,
					new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId) });
		}

		public static string ReportConfig(int reportId)
		{
			return (string) DataAccessLayer.ExecuteScalarStoredProcedure
					(CommonQueries.PRC_GET_REPORT_CONFIG_BY_ID,
					new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId) });
		}

        public static bool GetUserReportWriteAccess(int reportId)
        {
            return Convert.ToBoolean(DataAccessLayer.ExecuteScalarStoredProcedure
                    (CommonQueries.PRC_GET_REPORT_WRITE_ACCESS,
                    new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId) }));
        }

        public static string SetReportActive(int reportId)
        {
            return (string)DataAccessLayer.ExecuteScalarStoredProcedure
                    (CommonQueries.PRC_SET_REPORT_ACTIVE,
                    new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId) });
        }


		public static void UpdateReportConfig(int reportId, string config)
		{
			DataAccessLayer.ExecuteNonQueryStoredProcedure
					(CommonQueries.PRC_REPORT_UPDATE_CONFIG,
					new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId),
					new SqlParameter(CommonQueries.STR_OBJECT_CONFIG, config)});
		}

		public static void UpdateReportAssembly(int reportId, byte[] assembly, string objectVersion, string assemblyName)
		{
			DataAccessLayer.ExecuteNonQueryStoredProcedure
					(CommonQueries.PRC_UPDATE_ASSEMBLY,
					new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId),
					new SqlParameter(CommonQueries.STR_OBJECT_DATA, assembly),
					new SqlParameter(CommonQueries.STR_OBJECT_LIBRARY, assemblyName),
					new SqlParameter(CommonQueries.STR_OBJECT_VERSION, objectVersion)});
		}

		public static DataSet GetReportsList()
		{
			DataSet ds = new DataSet();
			ds = DataAccessLayer.ExecuteStoredProcedure(CommonQueries.PRC_REPORTS_LIST);
			return ds;
		}

		public static DataSet GetURMGroupList()
		{
			DataSet ds = new DataSet();
			ds = DataAccessLayer.ExecuteStoredProcedure(CommonQueries.PRC_OBJECT_GROUP_GET_ALL);
			return ds;
		}

		public static DataSet GetURMTypeList()
		{
			DataSet ds = new DataSet();
			ds = DataAccessLayer.ExecuteStoredProcedure(CommonQueries.PRC_OBJECT_TYPE_GET_ALL);
			return ds;
		}


		public static DataSet GetCommonAssembliesList()
		{
			DataSet ds = new DataSet();
			ds = DataAccessLayer.ExecuteStoredProcedure(CommonQueries.PRC_COMMON_ASSEMBLIES_LIST);
			return ds;
		}


		public static DataSet GetReportGroupAndType(int reportId)
		{
			DataSet ds = new DataSet();
			ds = DataAccessLayer.ExecuteStoredProcedure
					(CommonQueries.PRC_OBJECT_URM_GET_GROUP_ADN_TYPE,
					new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId)});
			return ds;
		}

		public static void ChangeReportGroupAndType(int reportId, Int16 groupId, byte typeId)
		{
			DataAccessLayer.ExecuteNonQueryStoredProcedure
					(CommonQueries.PRC_OBJECT_URM_CHANGE_GROUP_ADN_TYPE,
					new SqlParameter[] { new SqlParameter(CommonQueries.STR_OBJECT_ID, reportId),
					new SqlParameter(CommonQueries.STR_OBJECT_GROUP_ID, groupId),
					new SqlParameter(CommonQueries.STR_OBJECT_TYPE_ID, typeId)});
		}

		public static bool UploadAssembly(string assemblyName, string assemblyHeader, byte[] assemblyData,
			string config, int objectGroupId, int objectTypeId, int isActive, string assemblyVersion, bool isAdminToolReport)
		{
			object res = DataAccessLayer.ExecuteScalarStoredProcedure(
					  CommonQueries.PRC_UPLOAD_ASSEMBLY, new SqlParameter[]{
				  new SqlParameter(CommonQueries.STR_OBJECT_LIBRARY, (string.IsNullOrEmpty(assemblyName))? DBNull.Value:(object)assemblyName),
				  new SqlParameter(CommonQueries.STR_OBJECT_NAME, assemblyHeader),
				  new SqlParameter(CommonQueries.STR_OBJECT_SHORT_NAME, assemblyHeader),
				  new SqlParameter(CommonQueries.STR_OBJECT_DATA, assemblyData),
				  new SqlParameter(CommonQueries.STR_OBJECT_CONFIG, (string.IsNullOrEmpty(config))? DBNull.Value:(object)config),
				  new SqlParameter(CommonQueries.STR_OBJECT_GROUP_ID, objectGroupId),
				  new SqlParameter(CommonQueries.STR_OBJECT_TYPE_ID, objectTypeId),
				  new SqlParameter(CommonQueries.STR_IS_ACTIVE, isActive),
				  new SqlParameter(CommonQueries.STR_OBJECT_VERSION, (string.IsNullOrEmpty(assemblyVersion))? DBNull.Value:(object)assemblyVersion),
				  new SqlParameter(CommonQueries.STR_OBJECT_TYPE_LIBRARY, CommonQueries.STR_OBJECT_TYPE_LIBRARY_VALUE),
				  new SqlParameter(CommonQueries.STR_IS_ADMIN_TOOL_REPORT, isAdminToolReport)                  
				  });

			return res != null;
		}

	}
}
