﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.DataAccess
{

	public class CommonQueries
	{
		#region Procedures
		public const string PRC_GET_ASSEMBLY_BY_ID = "[dbo].[GetAssembly]";

		public const string PRC_GET_ASSEMBLY_BY_NAME = "[dbo].[GetAssemblyByName]";

        public const string PRC_GET_REPORT_WRITE_ACCESS = "[dbo].[spDW_URM_Report_Write_Access]";        

        public const string PRC_SET_REPORT_ACTIVE = "[dbo].[spDW_URM_AdminTool_SetReportIsActive]";

		public const string PRC_UPLOAD_ASSEMBLY = "[dbo].[spDW_URM_AdminTool_AssemblyUpload]";

		public const string PRC_UPDATE_ASSEMBLY = "[dbo].[spDW_URM_AdminTool_ReportAssemblyUpdate]";

		public const string PRC_REPORT_UPDATE_CONFIG = "[dbo].[spDW_URM_AdminTool_ReportConfigUpdate]";

		public const string PRC_REPORT_DELETE = "[dbo].[spDW_URM_AdminTool_AssemblyDel]";

		public const string PRC_REPORTS_LIST = "[dbo].[spDW_URM_AdminTool_ReportGetList]";

		public const string PRC_COMMON_ASSEMBLIES_LIST = "[dbo].[spDW_URM_AdminTool_CommonAssembliesList]";

		public const string PRC_GET_PROCEDURE_INFO = "[dbo].[spDW_URM_AdminTool_ProcedureInfoGet]";

		public const string PRC_GET_REPORT_CONFIG_BY_ID = "[dbo].[spDW_URM_AdminTool_ReportConfigGet]";

		public const string PRC_REPORT_LOCK_UNLOCK = "[dbo].[spDW_URM_AdminTool_ReportActivateDeactivate]";

		public const string PRC_GET_ASSEMBLY_VERSION = "[dbo].[spDW_URM_AdminTool_AssemblyVersionGet]";

		public const string PRC_OBJECT_GROUP_GET_ALL = "[dbo].[spDW_URM_AdminTool_ObjectGroup_GetAll]";

		public const string PRC_OBJECT_TYPE_GET_ALL = "[dbo].[spDW_URM_AdminTool_ObjectType_GetAll]";

		public const string PRC_OBJECT_URM_CHANGE_GROUP_ADN_TYPE = "[dbo].[spDW_URM_AdminTool_ChangeGroupAndType]";

		public const string PRC_OBJECT_URM_GET_GROUP_ADN_TYPE = "[dbo].[spDW_URM_AdminTool_GetGroupAndType]";

		public const string PRC_FAKE = "";

		#endregion

		#region Prameter names
		public const string STR_OBJECT_TYPE_LIBRARY_VALUE = "NETDLLMODULE";

		public const string STR_OBJECT_TYPE_LIBRARY = "@ObjectTypeLibrary";

		public const string STR_IS_ACTIVE = "@IsActive";

		public const string STR_OBJECT_GROUP_ID = "@ObjectGroup_ID";

		public const string STR_OBJECT_TYPE_ID = "@ObjectType_ID";

		public const string STR_OBJECT_CONFIG = "@ObjectConfig";

		public const string STR_OBJECT_ID = "@Object_ID";

		public const string STR_OBJECT_DATA = "@ObjectData";

		public const string STR_OBJECT_VERSION = "@ObjectVersion";

		public const string STR_OBJECT_NAME = "@ObjectName";

		public const string STR_OBJECT_SHORT_NAME = "@ObjectShortName";

		public const string STR_OBJECT_LIBRARY = "@ObjectLibrary";

        public const string STR_IS_ADMIN_TOOL_REPORT = "@IsAdminToolReport";        

		public const string STR_TABLE_TYPE = "";

		#endregion

		#region Query
		public const string SQL_GET_DATABASES = "exec sp_databases";

		public const string SQL_CHECK_DB_IS_SW = @"SELECT id FROM sysobjects 
WHERE xtype = 'U' AND name = 'tblCustomers'";

		public const string SQL_GET_TABLES = "exec sp_tables  @table_type='''TABLE'''";
		public const string SQL_GET_VIEWS = "exec sp_tables  @table_type='''VIEW''', @table_owner = '{0}'";
		#endregion

		#region Messages
		public const string STR_UNABLE_TO_PERFORM_OPERATION = "Unable to perform DataAccess operation: ";
		public const string STR_UNSUPPORTED_QUERY_TYPE = "Unsupported query type.";

		#endregion
	}
}


