﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Logica.Reports.Common;
using Logica.Reports.DataAccess.Exceptions;

namespace Logica.Reports.DataAccess {
    public class DataAccessLayer {
        //private const string TEST_QUERY = "select top 1 * from sysobjects";
        private const string LDB_NAME = "LDB";
        private static SqlConnection _conn;
        private static string _connectionString = HostConfiguration.DBSqlConnection;

        #region Properties

        public static SqlConnectionStringBuilder ConnectionString {
            get { return new SqlConnectionStringBuilder(_connectionString); }
        }

        public static bool IsLDB {
            get { return ConnectionString.InitialCatalog == LDB_NAME; }
        }

        #endregion

        #region Connection methods

        public static void OpenConnection() {
            _conn = new SqlConnection(_connectionString);
        }

        public static void CloseConnection() {
            _conn.Close();
        }

        public static void SetConnectionString(string connectionString) {
            ConnectionFailedException lException;
            CheckConnectionString(connectionString, out lException);
            if (null != lException)
                throw lException;
            _connectionString = connectionString;
        }

        public static void SetConnectionStringToDefault() {
            _connectionString = HostConfiguration.DBSqlConnection;
        }
        
        public static ConnectionFailedException CheckConnectionString(string connectionString) {
            ConnectionFailedException lException;
            CheckConnectionString(connectionString, out lException);
            return lException;
        }

        private static void CheckConnectionString(string connectionString, out ConnectionFailedException exception) {
            try {
                using (SqlConnection lConn = new SqlConnection(connectionString)) {
                    lConn.Open();
                    //execute(TEST_QUERY, QueryType.NonQuery, 60, CommandType.Text, connectionString, null);
                    exception = (lConn.State == ConnectionState.Open) ? null : new ConnectionFailedException(connectionString, null);
                }
            } catch (Exception lException) {
                exception = new ConnectionFailedException(connectionString, lException);
            }
        }

        #endregion

        private static object execute(string query, SQLQueryType queryType, int timeout, CommandType cmdtype,
                                        string connectionString, params SqlParameter[] procParams) {
            
            bool lConnectionChanged = false;
            if (null == _conn)
                OpenConnection();

            Debug.Assert(_conn != null, "SqlConnection object cannot be null.");
            if (string.IsNullOrEmpty(_conn.ConnectionString)) {
                _conn.ConnectionString = connectionString;
                lConnectionChanged = true;
            }

            using (SqlCommand cmd = new SqlCommand(query, _conn)) {
                object lResult;
                try {
                    if (procParams != null) {
                        foreach (SqlParameter p in procParams) {
                            SqlParameter new_parameter = new SqlParameter(p.ParameterName, p.SqlDbType, p.Size);
                            new_parameter.Value = p.Value;
                            new_parameter.Direction = p.Direction;
                            cmd.Parameters.Add(new_parameter);
                        }
                    }

                    cmd.CommandType = cmdtype;
                    cmd.CommandTimeout = timeout;

                    if (_conn.State != ConnectionState.Open)
                        _conn.Open();

                    switch (queryType) {
                        case SQLQueryType.DataSet:
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
                                lResult = new DataSet();
                                ((DataSet) lResult).RemotingFormat = SerializationFormat.Binary;
                                da.Fill((DataSet) lResult);
                            }
                            break;
                        case SQLQueryType.NonQuery:
                            cmd.ExecuteNonQuery();
                            lResult = cmd.Parameters;
                            break;
                        case SQLQueryType.Scalar:
                            lResult = cmd.ExecuteScalar();
                            break;
                        default:
                            throw new Exception(CommonQueries.STR_UNSUPPORTED_QUERY_TYPE);
                    }
                } catch (Exception lException) {
                    // we need this because parameters should stay unmapped to any SqlCommand.
                    throw new Exception(CommonQueries.STR_UNABLE_TO_PERFORM_OPERATION + lException.Message, lException);
                } finally {
                    if (lConnectionChanged) {
                        _conn.Close();
                        _conn.ConnectionString = string.Empty;
                    }
                }
                return lResult;
            }
        }

        private static object fillDataSet(string query, int timeout, CommandType cmdtype, DataSet ds,
            string tableName, string connectionString, params SqlParameter[] procParams) {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand cmd = new SqlCommand(query, conn)) {
                    //if (Connection != null)
                    //    cmd.Transaction = Connection;
                    object result = null;

                    if (procParams != null) {
                        foreach (SqlParameter p in procParams) {
                            cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value ?? DBNull.Value));
                            if (p.Value == null)
                                p.Value = DBNull.Value;
                        }
                    }
                    cmd.CommandType = cmdtype;
                    cmd.CommandTimeout = timeout;

                    if (conn.State != ConnectionState.Open) {
                        conn.Open();
                    }

                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd)) {
                        try {
                            result = sda.Fill(ds, tableName);
                        } catch (Exception Ex) {
                            // we need this because parameters should stay unmapped to any SqlCommand.
                            throw new Exception(CommonQueries.STR_UNABLE_TO_PERFORM_OPERATION + Ex.Message, Ex);
                        } finally {
                            cmd.Parameters.Clear();
                            //if ((con.State != ConnectionState.Closed) && (!ConnectionWasOpened))
                            //    con.Close();
                        }
                        return result;
                    }
                }
            }
        }

        private static object fillDataTable(string query, int timeout, CommandType cmdtype, DataTable dt,
                                            string connectionString, params SqlParameter[] procParams) {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand cmd = new SqlCommand(query, conn)) {
                    //if (Connection != null)
                    //    cmd.Transaction = Connection;

                    object result = null;
                    if (procParams != null) {
                        foreach (SqlParameter p in procParams) {
                            cmd.Parameters.Add(new SqlParameter(p.ParameterName, (p.Value == null) ? DBNull.Value : p.Value));
                            if (p.Value == null)
                                p.Value = DBNull.Value;
                        }
                    }
                    cmd.CommandType = cmdtype;
                    cmd.CommandTimeout = timeout;

                    if (conn.State != ConnectionState.Open) {
                        conn.Open();
                    }

                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd)) {
                        try {
                            result = sda.Fill(dt);
                        } catch (Exception Ex) {
                            // we need this because parameters should stay unmapped to any SqlCommand.
                            throw new Exception(CommonQueries.STR_UNABLE_TO_PERFORM_OPERATION + Ex.Message, Ex);
                        } finally {
                            cmd.Parameters.Clear();
                            //if ((con.State != ConnectionState.Closed) && (!ConnectionWasOpened))
                            //    con.Close();
                        }
                        return result;
                    }
                }
            }
        }

        #region Stored procedures

        public static DataSet ExecuteStoredProcedure(string name, params SqlParameter[] procParams) {
            return ExecuteStoredProcedure(name, _connectionString, procParams);
        }

        public static DataSet ExecuteStoredProcedure(string name, string connectionString, params SqlParameter[] procParams) {
            return (DataSet) execute(name, SQLQueryType.DataSet, 900, CommandType.StoredProcedure, connectionString, procParams);
        }

        public static DataSet ExecuteStoredProcedure(string name, int timeout, params SqlParameter[] procParams) {
            return ExecuteStoredProcedure(name, timeout, _connectionString, procParams);
        }

        public static DataSet ExecuteStoredProcedure(string name, int timeout, string connectionString, params SqlParameter[] procParams) {
            return (DataSet) execute(name, SQLQueryType.DataSet, timeout, CommandType.StoredProcedure, connectionString, procParams);
        }

        public static DataSet ExecuteStoredProcedureInNewConnection(string name, int timeout, params SqlParameter[] procParams) {
            using (SqlConnection connection = new SqlConnection(_connectionString)) {
                using (SqlCommand cmd = new SqlCommand(name, connection)) {
                    DataSet result = null;
                    try {
                        if (procParams != null) {
                            foreach (SqlParameter p in procParams) {
                                cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.SqlDbType, p.Size) {Value = p.Value});
                            }
                        }

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = timeout;

                        connection.Open();

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
                            result = new DataSet();
                            ((DataSet) result).RemotingFormat = SerializationFormat.Binary;
                            da.Fill((DataSet) result);
                        }
                    } catch (Exception Ex) {
                        // we need this because parameters should stay unmapped to any SqlCommand.
                        throw new Exception(CommonQueries.STR_UNABLE_TO_PERFORM_OPERATION + Ex.Message, Ex);
                    } finally {
                        connection.Close();
                    }
                    return result;
                }
            }
        }

        public static object ExecuteScalarStoredProcedure(string name) {
            return ExecuteScalarStoredProcedure(name, _connectionString, null);
        }

        public static object ExecuteScalarStoredProcedure(string name, string connectionString, params SqlParameter[] procParams) {
            return execute(name, SQLQueryType.Scalar, 900, CommandType.StoredProcedure, _connectionString, procParams);
        }

        public static object ExecuteScalarStoredProcedure(string name, params SqlParameter[] procParams) {
            return ExecuteScalarStoredProcedure(name, _connectionString, procParams);
        }

        public static void ExecuteNonQueryStoredProcedure(string name, params SqlParameter[] procParams) {
            ExecuteNonQueryStoredProcedure(name, _connectionString, procParams);
        }

        public static SqlParameterCollection ExecuteNonQueryStoredProcedureWithOutput(string name, params SqlParameter[] procParams) {
            //return (SqlParameterCollection)execute(name, SQLQueryType.Scalar, 900, CommandType.StoredProcedure, _connectionString, procParams);
            using (SqlCommand cmd = new SqlCommand(name, _conn)) {
                SqlParameterCollection result = null;
                try {
                    if (procParams != null) {
                        foreach (SqlParameter p in procParams) {
                            SqlParameter new_parameter = new SqlParameter(p.ParameterName, p.SqlDbType, p.Size);
                            new_parameter.Value = p.Value;
                            new_parameter.Direction = p.Direction;
                            cmd.Parameters.Add(new_parameter);
                        }
                    }

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 900;

                    if (_conn.State != ConnectionState.Open)
                        _conn.Open();

                    //case SQLQueryType.NonQuery:
                    cmd.ExecuteNonQuery();
                    //new
                    result = cmd.Parameters;
                } catch (Exception lException) {
                    // we need this because parameters should stay unmapped to any SqlCommand.
                    throw new Exception(CommonQueries.STR_UNABLE_TO_PERFORM_OPERATION + lException.Message, lException);
                }

                return result;
            }
        }

        public static void ExecuteNonQueryStoredProcedure(string name, string connectionString, params SqlParameter[] procParams) {
            execute(name, SQLQueryType.NonQuery, 900, CommandType.StoredProcedure, connectionString, procParams);
        }

        public static void ExecuteNonQueryStoredProcedure(string name, int timeout, params SqlParameter[] procParams) {
            execute(name, SQLQueryType.NonQuery, timeout, CommandType.StoredProcedure, _connectionString, procParams);
        }

        #endregion

        #region  Query

        public static DataSet ExecuteQuery(string name) {
            return ExecuteQuery(name, _connectionString, null);
        }

        public static DataSet ExecuteQuery(string name, string connectionString) {
            DataSet lDataSet;
            using (_conn = new SqlConnection(connectionString)) {
                _conn.Open();
                lDataSet = ExecuteQuery(name, connectionString, null);
                _conn.Close();
            }
            return lDataSet;
        }

        public static DataSet ExecuteQuery(string name, params SqlParameter[] procParams) {
            return ExecuteQuery(name, _connectionString, procParams);
        }

        public static DataSet ExecuteQuery(string name, string connectionString, params SqlParameter[] procParams) {
            return (DataSet) execute(name, SQLQueryType.DataSet, 900, CommandType.Text, connectionString, procParams);
        }

        public static DataSet ExecuteQuery(string name, int timeout, params SqlParameter[] procParams) {
            return (DataSet) execute(name, SQLQueryType.DataSet, 900, CommandType.Text, _connectionString, procParams);
        }

        public static DataSet ExecuteQuery(string name, int timeout, string connectionString, params SqlParameter[] procParams) {
            return (DataSet) execute(name, SQLQueryType.DataSet, timeout, CommandType.Text, connectionString, procParams);
        }

        public static object ExecuteScalarQuery(string name) {
            return ExecuteScalarQuery(name, _connectionString);
        }

        public static object ExecuteScalarQuery(string name, string connectionString) {
            return ExecuteScalarQuery(name, connectionString, null);
        }

        public static object ExecuteScalarQuery(string name, params SqlParameter[] procParams) {
            return ExecuteScalarQuery(name, _connectionString, procParams);
        }

        public static object ExecuteScalarQuery(string name, string connectionString, params SqlParameter[] procParams) {
            return execute(name, SQLQueryType.Scalar, 900, CommandType.Text, connectionString, procParams);
        }

        public static void ExecuteNonQuery(string sql, params SqlParameter[] procParams) {
            ExecuteNonQuery(sql, _connectionString, procParams);
        }

        public static void ExecuteNonQuery(string sql, string connectionString, params SqlParameter[] procParams) {
            execute(sql, SQLQueryType.NonQuery, 900, CommandType.Text, connectionString, procParams);
        }

        #endregion
    }
}