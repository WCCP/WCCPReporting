﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Logica.Reports.Common.Crypto
{

    public class Cryptographer : IDisposable
    {
        private readonly SymmetricAlgorithm _algorithm;

        private readonly byte[] _key = new byte[] 
        {0x53, 0x23, 0x74, 0x71, 0x63, 0x10, 0x74, 0xde, 0xa4, 0xf3,
        0xfe, 0xac, 0x04, 0x21, 0x87, 0x45, 0xf6, 0xde, 0x3b, 0xd4,
        0x68, 0x34, 0x00, 0x4d, 0xb2, 0xd7, 0x21, 0xd3, 0x41, 0xaf,
        0x23, 0xd4};

        private readonly byte[] _vector = new byte[] 
        {0x87, 0x45, 0xf6, 0xde, 0x3b, 0xd4, 0x68, 0x41, 
        0x34, 0x00, 0x4d, 0xb2, 0xd7, 0x21, 0xd3, 0x23};

        public Cryptographer()
        {
            _algorithm = new RijndaelManaged {Key = _key, IV = _vector};
        }

        private byte[] Encrypt(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
                throw new ArgumentNullException("plainText");
                ICryptoTransform encryptor = _algorithm.CreateEncryptor(_algorithm.Key, _algorithm.IV);
                var msEncrypt = new MemoryStream();
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (var swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(inputString);
                        swEncrypt.Flush();
                    }
                }
            return msEncrypt.ToArray();

        }

        private string Decrypt(byte[] cipherText)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            string plaintext;
            ICryptoTransform decryptor = _algorithm.CreateDecryptor(_algorithm.Key, _algorithm.IV);
            using (var msDecrypt = new MemoryStream(cipherText))
            {
                try
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))

                            plaintext = srDecrypt.ReadToEnd();

                    }
                }
                catch (CryptographicException ex)
                {
                    throw new Exception("Incorrect encrypted password", ex);
                }
            }
            return plaintext;
        }

        public string EncryptStr(string input)
        {
            return string.IsNullOrEmpty(input) ? string.Empty : FromByte(Encrypt(input));
        }


        public string DecryptStr(string input)
        {
            return string.IsNullOrEmpty(input) ? string.Empty : Decrypt(FromString(input));
        }

        protected byte FromHex(char c1, char c2)
        {
            byte hight = (byte)(c1 - 'A');
            byte low = (byte)(c2 - 'A');
            return (byte)(hight * 16 + low);
        }

        protected string ToHex(byte byData)
        {
            string res = "";
            res += (char)(byData / 16 + 'A');
            res += (char)(byData % 16 + 'A');
            return res;
        }

        protected byte[] FromString(string strData)
        {
            byte[] res = new byte[strData.Length / 2];
            for (int i = 0; i < (strData.Length / 2); i++)
            {
                res[i] = FromHex(strData[i * 2], strData[i * 2 + 1]);
            }
            return res;
        }

        protected string FromByte(byte[] bData)
        {
            string res = "";
            for (int i = 0; i < bData.Length; i++)
                res += ToHex(bData[i]);
            return res;
        }

        public void Dispose()
        {
            if(_algorithm != null)
                _algorithm.Clear();
        }
    }
}