﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.Common
{
    public static class ScriptProvider
    {
        public static string CoverageAnalysis
        {
            get
            {
                return @"
-- =============================================
-- Author:		Developer 2
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- 
-- Changed
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Coverage analysis' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************


--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
--
--set @merchandisersIDs = '        33,        35,        36,        37,        40,        41,       128,    600007'
--set @merchandisersCnt = 0
--set @supervisorId = NULL
--set @lowerDate = '2009-10-01'
--set @upperDate = '2009-10-31'
DECLARE @currentDate AS DATETIME
--Define planed visits by day
SET @currentDate = @lowerDate

CREATE TABLE #WeekDays (date DateTime NOT NULL, [weekDay] tinyInt )

WHILE @currentDate < @upperDate
BEGIN
INSERT INTO #WeekDays (date, [weekDay])
	VALUES (@currentDate, datePart(dw, @currentDate))
	SET @currentDate = @currentDate + 1
END 


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END

-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT olr.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- Выбираем ТП с бОльшим ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
											AND olr.Status = 2 AND r.Status = 2
--		INNER JOIN dbo.tblOutlets ol ON olr.OL_id = ol.OL_id
		INNER JOIN 
		( -- Ранжируем  типы каналов внури ТП
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 , Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END

		) matl ON matl.MerchActivity_ID = 1  -- Выбираем первую попавшуюся Activity (канал) у ТП
			  AND r.Merch_Id = matl.Merch_Id 
		GROUP BY olr.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 


-- Выбираем продукцию и все связанными с ним данные
SELECT p.Product_Id
	 , p.ProductName -- СКЮ
	 , pt.ProdTypeName -- Сорт
	 , pg.ProdGroupName -- Бренд
	 , u.Unit_Name -- Тип Упаковки
	 , p.productVolume * 10 AS Packing_Capacity -- Ёмкость Упаковки
	 , CASE WHEN ptp.ProductName LIKE '%BNR%' 
	   THEN 
			CASE WHEN ptp.ProductName LIKE '% ТУ%' 
			THEN 
				'BNR ТУ' 
			ELSE  
				CASE WHEN ptp.ProductName LIKE '% InBev%' 
				THEN 
					'BNR InBev' 
				ELSE 'BNR' END 
			 END
		ELSE 'Other' 
		END  AS Packing_Type -- Вид Упаковки
	 , pc.ProductCombineName -- Наименование комбинированной продукции
	
INTO #products
FROM tblProducts p 
INNER JOIN tblProductTypes pt ON pt.ProductType_id = p.ProductType_id 
INNER JOIN tblProductGroups pg ON pt.ProdGroup_ID = pg.ProdGroup_ID 
INNER JOIN tblUnits u ON p.Unit_ID = u.Unit_ID 
INNER JOIN tblProductCombine pc ON p.HLCode = pc.HLCode 
LEFT JOIN tblProducts ptp ON p.Tare_Id = ptp.Product_Id

-- Выбираем Активные визиты и остаткию связываем с ними данные по продукции
SELECT 	olmc.OL_ID
	 , olmc.Merch_ID
--	  CASE WHEN och.OL_ID IS NULL THEN  od.OL_ID ELSE och.OL_ID END OL_ID
--	, CASE WHEN och.Merch_ID IS NULL THEN  od.Merch_ID ELSE och.Merch_ID END Merch_ID
	, CASE WHEN och.OLCard_Id IS NULL THEN  od.OLCard_Id ELSE och.OLCard_Id END OLCard_Id
	, CASE WHEN och.OLCardDate IS NULL THEN od.OLCardDate ELSE och.OLCardDate END OLCardDate
	, CASE WHEN och.Product_Id IS NULL THEN od.Product_Id ELSE och.Product_Id END Product_Id
	, och.OL_ID AS OL_IsActive
	, od.OL_ID AS OL_HasRemains
	, p.ProductName -- СКЮ
	, p.ProdTypeName -- Сорт
	, p.ProdGroupName -- Бренд
	, p.Unit_Name -- Тип Упаковки
	, p.Packing_Capacity -- Ёмкость Упаковки
	, p.Packing_Type -- Вид Упаковки
	, p.ProductCombineName -- Наименование комбинированной продукции
INTO #tmpOch
FROM 
( SELECT och.OL_ID
   , och.Merch_ID 
   , och.OLCard_Id
   , och.OLCardDate
   , od.Product_Id
	FROM tblOutletDistribution od
	INNER JOIN tblOutletCardH och ON och.OLCard_id = od.OLCard_id AND od.IsPresent > 0 
							 AND och.OLCardDate BETWEEN @lowerDate AND @upperDate
) AS od
FULL OUTER JOIN 
(SELECT och.OL_ID
   , och.Merch_ID
   , och.OLCard_Id
   , och.OLCardDate
   , sod.Product_Id
    
FROM tblOutletCardH och
	INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
								  AND och.OLCardDate BETWEEN @lowerDate AND @upperDate
	INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
	INNER JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo 
	INNER JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id 
							AND sod.Product_Id = ood.Product_Id 
							AND ood.Product_qty <> 0 
							AND sod.Product_qty <> 0
) AS och 
ON od.OlCard_id = och.OLCard_ID  AND (och.Product_Id is null or  och.Product_Id  = od.Product_Id)
INNER JOIN #products p ON p.Product_id = CASE 
											WHEN och.Product_Id IS NULL 
											THEN od.Product_Id 
											ELSE och.Product_Id 
										 END
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_ID = CASE WHEN och.OL_ID IS NULL THEN  od.OL_ID ELSE och.OL_ID END 
								AND olmc.Merch_ID = 
(
		CASE 
			WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
							)
				  ) 
			THEN -- на ТТ завязан только один ТП.
				 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
				olmc.Merch_ID 
			ELSE -- на ТТ завязано несколько ТП.
				CASE
					WHEN ( -- Проверяем ТП, привязан ли он к ТТ
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = CASE WHEN och.Merch_ID IS NULL THEN  od.Merch_ID ELSE och.Merch_ID END  -- Да, привязан
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- Выбираем ТТ из 'заказов' и ищем его канал и выбираем ТП из 'Привязаных через маршруты'. 
						 -- Выбираем первый попавшийся канад у ТП, который совершил визит
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = CASE WHEN och.Merch_ID IS NULL THEN  od.Merch_ID ELSE och.Merch_ID END
																  AND olmc3.OL_id = CASE WHEN och.OL_ID IS NULL THEN  od.OL_ID ELSE och.OL_ID END 
						)
				END
		END
)

-- Выбираем визиты, которые не были активными, тобишь не попали в пердыдущий запрос
SELECT * 
INTO #tmpOch1
FROM
(
	SELECT Merch_ID
		, OLCardDate
		, OL_ID
		, OL_IsActive
		, OL_HasRemains
		, Product_Id
		, OLCard_Id
		, ProductName -- СКЮ
		, ProdTypeName -- Сорт
		, ProdGroupName -- Бренд
		, Unit_Name -- Тип Упаковки
		, Packing_Capacity -- Ёмкость Упаковки
		, Packing_Type -- Вид Упаковки
		, ProductCombineName 
	FROM #tmpOch
	UNION
	SELECT Merch_ID
		 , OLCardDate
		 , OL_id
		 , NULL AS OL_IsActive
		 , NULL AS OL_HasRemains
		 , NULL AS Product_Id
		 , OLCard_Id
		 , NULL AS ProductName -- СКЮ
		 , NULL AS ProdGroupName -- Бренд
		 , NULL AS ProdTypeName -- Сорт
		 , NULL AS Unit_Name -- Тип Упаковки
		 , NULL AS Packing_Capacity -- Ёмкость Упаковки
		 , NULL  AS Packing_Type -- Вид Упаковки
		 , NULL AS ProductCombineName
	FROM 
	(
		SELECT DISTINCT OLCard_Id, olmc.OL_Id, olmc.Merch_id, OLCardDate 
		FROM tblOutletCardH och
		INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id 
										AND olmc.Merch_Id = 
		(
		CASE 
			WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
							)
				  ) 
			THEN -- на ТТ завязан только один ТП.
				 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
				olmc.Merch_ID 
			ELSE -- на ТТ завязано несколько ТП.
				CASE
					WHEN ( -- Проверяем ТП, привязан ли он к ТТ
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = och.Merch_Id -- Да, привязан
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- Выбираем ТТ из 'заказов' и ищем его канал и выбираем ТП из 'Привязаных через маршруты'. 
						 -- Выбираем первый попавшийся канад у ТП, который совершил визит
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = och.Merch_id
																  AND olmc3.OL_id = och.OL_id
						)
				END
		END
		)
		WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate 
		EXCEPT
		Select DISTINCT OLCard_Id, OL_Id, Merch_id, OLCardDate FROM #tmpOch
	) AS D 
)AS DD

-- Добавляем ТТ, в которые не было визитов
SELECT r.* 
INTO #Result
FROM
(
	SELECT Merch_ID
		, OLCardDate
		, OL_ID
		, OL_ID AS OL_Visited
		, OL_IsActive
		, OL_HasRemains
		, Product_Id
		, OLCard_Id
		, ProductName -- СКЮ
		, ProdTypeName -- Сорт
		, ProdGroupName -- Бренд
		, Unit_Name -- Тип Упаковки
		, Packing_Capacity -- Ёмкость Упаковки
		, Packing_Type -- Вид Упаковки
		, ProductCombineName 
	FROM #tmpOch1
	UNION
	SELECT Merch_ID
		 , OLCardDate
		 , OL_id
		 , NULL AS OL_Visited
		 , NULL AS OL_IsActive
		 , NULL AS OL_HasRemains
		 , NULL AS Product_Id
		 , NULL AS OLCard_Id
		 , NULL AS ProductName -- СКЮ
		 , NULL AS ProdGroupName -- Бренд
		 , NULL AS ProdTypeName -- Сорт
		 , NULL AS Unit_Name -- Тип Упаковки
		 , NULL AS Packing_Capacity -- Ёмкость Упаковки
		 , NULL AS Packing_Type -- Вид Упаковки
		 , NULL AS ProductCombineName
	FROM 
	(
		SELECT olr.OL_id
			 , r.Merch_Id 
			 , wd.Date AS OLCardDate
			FROM #WeekDays wd
		INNER JOIN dbo.tblRoutes r ON wd.[weekDay] = substring(r.RouteName, 1, 1) AND r.Status = 2
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id = olr.Route_id 
											AND olr.Status = 2
		INNER JOIN #OL_Merch_Chanal olmc ON olmc.Merch_Id = r.Merch_Id AND olmc.OL_Id = olr.OL_Id
		EXCEPT
		SELECT DISTINCT OL_Id, Merch_id , OLCardDate
		From #tmpOch1
	) AS t
) AS r

-- Формируем результирующую таблицу
SELECT 
	   sp.Cust_Name -- Точка синхронизации
 	 , s.Supervisor_name -- Супервизор
	 , c.City_Name	 -- Город
	 , d.District_Name -- Область
	 , NULL AS Region_Name -- Район (ТТ) TODO: join to tblCountryArea
	 , a.Area_Name  -- Район в городе (ТТ) 
	 , NULL AS Territory_Type -- Тип территории TODO: join to  tblSettlement
	 --, olmc.Merch_Id
     , m.MerchName -- ТП
	 , ms.LValue AS MerchandiserStatus -- Статус ТП
	 , och.OL_Visited -- Outlet is visited by Merchandiser 
	 , och.OL_IsActive AS OL_IsActive -- Outlet is Active
	 , och.OL_HasRemains AS OL_HasRemains --OL with remains
     , och.OL_id -- Код ТТ
 	 , ol.OL_Code -- Внешний кодТТ
	 , ols.LValue as OutletsSatus -- Статус ТТ
	 , olt.OLtype_name  --ТипТТ
	 , olst.OLSubTypeName  -- ПодТипТТ
	 , ol.OLName  --Факт имя
	 , ol.OLDeliveryAddress  -- Факт. адрес
	 , ol.OLTradingName  -- Юр. имя
	 , ol.OLAddress --Юр. адрес
	 , och.OLCardDate -- Время
	 , och.Product_Id
	 , och.ProductName -- СКЮ
	 , och.ProdGroupName -- Бренд
	 , och.ProdTypeName -- Сорт
	 , och.Unit_Name -- Тип Упаковки
	 , och.Packing_Capacity -- Ёмкость Упаковки
	 , och.Packing_Type -- Вид Упаковки
	 , och.ProductCombineName -- Наименование комбинированной продукции
	 , pf.LValue AS ProximityFactor -- Фактор близости
FROM #Result och

INNER JOIN tblMerchandisers m ON m.Merch_id = och.Merch_id
INNER JOIN tblGloballookup ms ON ms.LKey = m.Status 
							  AND ms.TableName = 'tblMerchandisers' 
							  AND ms.FieldName = 'Status'
INNER JOIN tblOutlets ol ON och.OL_id = ol.OL_id
INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
INNER JOIN tblGloballookup ols ON ols.LKey = ol.Status 
							  AND ols.TableName = 'tblOutlets' 
							  AND ols.FieldName = 'Status'
INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
						  AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS cs ON cs.Supervisor_id = s.Supervisor_id	
			AND cs.CustomerOrder = 1	-- Т.к. нет возможности точно выбрать ТС, 
										-- выбираем первую попавшуюся из принадлежащих СВ
INNER JOIN tblCustomers sp ON cs.Cust_id = sp.Cust_Id 
INNER JOIN tblAreas a ON ol.Area_id = a.Area_Id
INNER JOIN tblGloballookup pf ON 
 (pf.LKey = CASE WHEN ol.Proximity IS NULL 
    THEN -3 
    ELSE ol.Proximity
    END
 ) 
 AND pf.TableName = 'tblOutlets' 
    AND pf.FieldName = 'Proximity'
INNER JOIN tblCities c ON c.City_id = a.City_Id
INNER JOIN tblDistricts d ON d.District_id = c.District_id

DROP TABLE #OL_Merch_Chanal
DROP TABLE #tmpOch
DROP TABLE #tmpOch1
DROP TABLE #merchandisersIDs
DROP TABLE #Result
DROP TABLE #products
DROP TABLE #WeekDays
";
            }
        }

        public static string FailedVisits
        {
            get
            {
                return @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the function dbo.fnDW_Common_SplitText must be used to transform 
--		the string IDs into a table representation
-- 
-- =============================================

/**/
--DECLARE @lowerDate as datetime
--DECLARE @upperDate as datetime
--
--set @lowerDate = '2009-10-01'
--set @upperDate = '2009-10-31'
--
--
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
--
--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END

-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- Выбираем ТП с бОльшим ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- Ранжируем  типы каналов внури ТП
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 ,  Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
		) matl ON matl.MerchActivity_ID = 1 -- Выбираем первую попавшуюся Activity (канал) у ТП
			  AND r.Merch_Id = matl.Merch_Id  
	WHERE olr.Status = 2		
	GROUP BY ol.OL_id
		, ActivityType			 
) AS OL_Merch_Chanal 

-- выбираем несостоявшиеся визиты по дате
SELECT 
	  tblSupervisors.Supervisor_name
	, tblSyncStatus.MerchName
	, COUNT(*) AS FailedVisitsCount
	, tblOutlets.OL_id
	, tblOutlets.OL_Code
	, tblInaccessibilityReasons.Reason
	, tblOutlets.OLDeliveryAddress + ', ' + tblOutlets.OLName AS FactTT
	, tblOutlets.OLAddress + ', ' + tblOutlets.OLTradingName AS LawTT
	, tblOutletSubTypes.OLSubTypeName
	, tblOutletTypes.OLtype_name
	, tblOutlets.OLName
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLTradingName
	, tblOutlets.OLAddress
	, ols.LValue AS OutletsSatus
	, mers.LValue AS MerchStatus
	, tblCustomers.Cust_Name
	, tblCities.City_Name
	, och.Comments
	, och.OLCardDate
    , a.Area_Name -- Район в городе (ТТ) 
	
FROM tblOutletCardH och
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
							)
				  ) 
			THEN -- на ТТ завязан только один ТП.
				 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
				olmc.Merch_ID 
			ELSE -- на ТТ завязано несколько ТП.
				CASE
					WHEN ( -- Проверяем ТП, привязан ли он к ТТ
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = och.Merch_Id -- Да, привязан
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- Выбираем ТТ из 'заказов' и ищем его канал и выбираем ТП из 'Привязаных через маршруты'. 
						 -- Выбираем первый попавшийся канад у ТП, который совершил визит
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = och.Merch_id
																  AND olmc3.OL_id = och.OL_id
						)
				END
		END
		)
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = olmc.Merch_ID
LEFT JOIN tblInaccessibilityReasons ON tblInaccessibilityReasons.Reason_id = och.Reason_id
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id AND tblSupervisors.Status <> 9
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS CustomerSupervisors ON CustomerSupervisors.Supervisor_id = tblSupervisors.Supervisor_id	
							AND CustomerSupervisors.CustomerOrder = 1	-- Т.к. нет возможности точно выбрать ТС, 
																		-- выбираем первую попавшуюся из принадлежащих СВ
INNER JOIN tblCustomers ON tblCustomers.Cust_Id = CustomerSupervisors.Cust_id
INNER JOIN tblOutlets ON tblOutlets.OL_id = olmc.OL_id
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
INNER JOIN tblAreas a ON tblOutlets.Area_id = a.Area_Id
INNER JOIN tblCities ON tblCities.City_id = a.City_Id
INNER JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status 
				AND ols.TableName = 'tblOutlets'
				AND ols.FieldName = 'Status'
INNER JOIN tblGloballookup mers ON mers.LKey = tblSyncStatus.Status 
				AND mers.TableName = 'tblSyncStatus'
				AND mers.FieldName = 'Status'
	
GROUP BY 
	  tblSupervisors.Supervisor_name
	, tblSyncStatus.MerchName
	, tblOutlets.OL_id
	, tblOutlets.OL_Code
	, tblInaccessibilityReasons.Reason
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLAddress
	, tblOutletSubTypes.OLSubTypeName
	, tblOutletTypes.OLtype_name
	, tblOutlets.OLName
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLTradingName
	, tblOutlets.OLAddress
	, tblSyncStatus.MerchStatus
	, tblOutlets.Status
	, tblCustomers.Cust_name
	, tblCities.City_name
	, och.Comments
	, och.OLCardDate
	, ols.LValue
	, mers.LValue
	, och.Inaccessible
	, a.Area_Name	
HAVING och.OLCardDate BETWEEN @lowerDate AND @upperDate	AND och.Inaccessible = 1

DROP TABLE #OL_Merch_Chanal
DROP TABLE #merchandisersIDs


";
            }
        }

        public static string OutletBase
        {
            get
            {
                return @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Outlet base' report.
-- Input parameters:
--		Data range for the report:
--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1
/**/
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
DECLARE @currentDate as datetime
DECLARE @currentMonthStart as datetime
DECLARE @previousMonthStart as datetime
DECLARE @previousMonthEnd as datetime

SET @currentDate = CAST(ROUND(CAST(getdate() AS float), 0, 1) AS datetime)
SET @previousMonthEnd = DATEADD(dd, -DATEPART(dd, @currentDate), @currentDate)
SET @currentMonthStart = DATEADD(dd, 1, @previousMonthEnd)
SET @previousMonthStart = DATEADD(mm, -1, @currentMonthStart)
--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL

-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END


-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- Выбираем ТП с бОльшим ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- Ранжируем  типы каналов внури ТП
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 ,  Merch_ID
				 ,  ActivityType
			FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
		) matl ON matl.MerchActivity_ID = 1  -- Выбираем первую попавшуюся Activity (канал) у ТП
			  AND r.Merch_Id = matl.Merch_Id 
	WHERE olr.Status = 2
	GROUP BY ol.OL_id
		, ActivityType
) AS OL_Merch_Chanal 

-- выбираем несостоявшиеся визиты по дате
SELECT 
	 tblOutlets.OL_id
	, tblOutlets.OL_Code
	, tblOutletSubTypes.OLSubTypeName
	, tblOutletTypes.OLtype_name
	, tblOutlets.OLName
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLTradingName
	, tblOutlets.OLAddress
	, ols.LValue as OutletsSatus
	, tblDistricts.District_Name
	, tblCities.City_Name
	, tblOutlets.OLDirector
	, ISNULL(Sales.PrevBottledSales, 0) AS PreviousBottledSales
	, ISNULL(Sales.PrevKegSales, 0) AS PreviousKegSales
	, ISNULL(Sales.CurrBottledSales, 0) AS CurrentBottledSales
	, ISNULL(Sales.CurrKegSales, 0) AS CurrentKegSales
	, ISNULL(Fridges.Quantity, 0) AS FridgeNumber
	, ISNULL(PlanedVisits.VisitsCount, 0) AS PlanedVisitsCount
	, ISNULL(FactVisits.VisitsCount, 0) AS TotalVisitsCount
	, pFactor.LValue as ProximityFactor
	, tblOutlets.OLTelephone
	, tblOutlets.OLFax
	, tblOutlets.OLEmail
	, tblOutlets.OLPurchManager
	, tblOutlets.OLMarkManager
	, tblOutlets.OLMarkManagerPhone
	, tblOutlets.OLAccountant
	, tblOutlets.OLAccountantPhone
	, tblOutlets.ContractNumber
	, tblOutlets.ContractDate
	, tblOutlets.RR
	, tblOutlets.BankCode
	, tblOutlets.BankName
	, tblOutlets.BankAddress
	, tblOutlets.ZKPO
	, tblOutlets.VATN
	, tblOutlets.IPN
	, tblOutlets.OLSize
	, tblOutlets.OLWHSize
	, tblAreas.Area_Name
	, tblSupervisors.Supervisor_name
	, tblSyncStatus.MerchName
--TODO : К-во охладителей СИУ, Urban/Rural, Кол-во населения, Район ТТ.

FROM 
	(--Убираем повторения в маршрутах, для избежания избыточности
	 SELECT DISTINCT o.OL_id, 
					 r.Merch_id
     FROM tblRoutes r 
	 INNER JOIN tblOutletRoutes olr ON r.Route_id = olr.Route_id 
	 INNER JOIN tblOutlets o ON olr.OL_id = o.OL_id
	 INNER JOIN #merchandisersIDs AS IDs ON r.Merch_ID = IDs.id 
											  OR  @merchandisersCnt = 0
     WHERE olr.Status = 2
    ) AS vwMerch_OL
INNER JOIN tblOutlets ON tblOutlets.OL_id = vwMerch_OL.OL_id
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = vwMerch_OL.Merch_ID
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id 
		AND tblSupervisors.Status <> 9
INNER JOIN tblAreas ON tblOutlets.Area_id = tblAreas.Area_Id
INNER JOIN tblCities ON tblCities.City_id = tblAreas.City_Id
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
INNER JOIN tblDistricts ON tblDistricts.District_id = tblCities.District_id
INNER JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status 
							  AND ols.TableName = 'tblOutlets'
							  AND ols.FieldName = 'Status' 
LEFT JOIN -- Холодильники
	(
		SELECT tblOutletPOS.OL_ID
			 , SUM(tblOutletPOS.Quantity) AS Quantity 
		FROM tblPOSTypes 
		INNER JOIN  tblPOS ON tblPOSTypes.POSType_ID = tblPOS.POSType_ID 
		INNER JOIN  tblOutletPOS ON tblPOS.POS_ID = tblOutletPOS.POS_ID 
		WHERE (tblPOSTypes.POSType_ID NOT IN (17, 10, 16, 15, 13, 12, 14)) 
		GROUP BY tblOutletPOS.OL_ID
	) Fridges ON Fridges.OL_id = vwMerch_OL.OL_id 
LEFT JOIN -- Продажи.
   (
	   SELECT tblSalOutH.OL_ID
			, olmc.Merch_ID
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @previousMonthStart AND @previousMonthEnd 
					   AND tblProducts.Unit_ID <> 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS PrevBottledSales
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @previousMonthStart AND @previousMonthEnd 
					   AND tblProducts.Unit_ID = 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS PrevKegSales
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @currentMonthStart AND @currentDate
					   AND tblProducts.Unit_ID <> 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS CurrBottledSales
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @currentMonthStart AND @currentDate 
					   AND tblProducts.Unit_ID = 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS CurrKegSales
		FROM tblSalOutH 
		INNER JOIN tblSalOutD ON tblSalOutH.Invoice_id = tblSalOutD.Invoice_id 
		INNER JOIN tblProducts ON tblSalOutD.Product_id = tblProducts.Product_id
		INNER JOIN tblProductTypes ON tblProducts.ProductType_id = tblProductTypes.ProductType_id 
		INNER JOIN tblProductGroups ON tblProductTypes.ProdGroup_ID = tblProductGroups.ProdGroup_ID 
			AND tblProductGroups.ProdGroup_ID NOT IN (6, 31, 32, 33, 35, 36, 37) 
		INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = tblSalOutH.OL_id AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
							)
				  ) 
			THEN -- на ТТ завязан только один ТП.
				 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
				olmc.Merch_ID 
			ELSE -- на ТТ завязано несколько ТП.
				CASE
					WHEN ( -- Проверяем ТП, привязан ли он к ТТ
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = tblSalOutH.Merch_Id -- Да, привязан
								)
						 )
					THEN --  
						tblSalOutH.Merch_Id 
					ELSE -- Выбираем ТТ из заказов и ищем его канал и выбираем ТП из Привязаных через маршруты. 
						 -- Выбираем первый попавшийся канад у ТП, который совершил визит
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = tblSalOutH.Merch_id
																  AND olmc3.OL_id = tblSalOutH.OL_id
						)
				END
		END
		)
		WHERE tblSalOutH.[Date] BETWEEN @previousMonthStart AND @currentDate
			  AND tblSalOutH.Status <> 9
	    GROUP BY tblSalOutH.OL_ID
			  ,  olmc.Merch_ID
   ) Sales ON Sales.OL_ID = vwMerch_OL.OL_id 
			  AND Sales.Merch_id = vwMerch_OL.Merch_id
LEFT JOIN -- Плановое количество визитов.
	(
		SELECT OL_id
			 , Merch_id
			 , SUM(VisitsCount) AS VisitsCount
		FROM 
		(
			SELECT 
				  ol.OL_id
				, r.Merch_id
				, DATEDIFF(WEEK, @currentMonthStart, @currentDate)
				  + (CASE 
					WHEN (SUBSTRING(r.RouteName, 1, 1) % 8 >= DATEPART(dw, @currentMonthStart)
						  AND SUBSTRING(r.RouteName, 1, 1) % 8 <= DATEPART(dw, @currentDate))
					THEN 
						1
					WHEN (SUBSTRING(r.RouteName, 1, 1) % 8 < DATEPART(dw, @currentMonthStart)
						  AND SUBSTRING(r.RouteName, 1, 1) % 8 > DATEPART(dw, @currentDate))
					THEN 
						-1
					ELSE
						0
				 END) AS VisitsCount
			FROM tblRoutes r
			INNER JOIN tblOutletRoutes olr ON r.Route_id = olr.Route_id 
			INNER JOIN tblOutlets ol ON olr.OL_id = ol.OL_id
			WHERE olr.Status = 2
		) t
		GROUP BY OL_id
			   , Merch_id
	) PlanedVisits ON PlanedVisits.OL_id = vwMerch_OL.OL_id 
				  AND PlanedVisits.Merch_id = vwMerch_OL.Merch_ID
LEFT JOIN -- Фактическое количество визитов.
	(
		SELECT och.OL_id
			 , olmc.Merch_ID
			 , COUNT(DISTINCT och.OLCard_id) AS VisitsCount
		FROM tblOutletCardH och
		INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
							)
				  ) 
			THEN -- на ТТ завязан только один ТП.
				 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
				olmc.Merch_ID 
			ELSE -- на ТТ завязано несколько ТП.
				CASE
					WHEN ( -- Проверяем ТП, привязан ли он к ТТ
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = och.Merch_Id -- Да, привязан
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- Выбираем ТТ из заказов и ищем его канал и выбираем ТП из Привязаных через маршруты. 
						 -- Выбираем первый попавшийся канад у ТП, который совершил визит
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = och.Merch_id
																  AND olmc3.OL_id = och.OL_id
						)
				END
		END
		)
		WHERE
			och.OLCardDate BETWEEN @currentMonthStart AND @currentDate
		GROUP BY och.OL_id
			   , olmc.Merch_ID
	) FactVisits ON FactVisits.OL_id = vwMerch_OL.OL_id 
				AND FactVisits.Merch_id = vwMerch_OL.Merch_id
INNER JOIN tblGloballookup pFactor ON (pFactor.LKey = 
										CASE WHEN tblOutlets.Proximity IS NULL 
										THEN -3 
										ELSE tblOutlets.Proximity
										END)
							  AND pFactor.TableName = 'tblOutlets' 
							  AND pFactor.FieldName = 'Proximity'
ORDER BY tblOutlets.OL_id

DROP TABLE #merchandisersIDs
DROP TABLE #OL_Merch_Chanal";
            }
        }

        public static string RoutesByOutlets
        {
            get
            {
                return @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 25-11-2009
-- Description:	Selects all needed data for 'Routes by outlets' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1
/*
DECLARE @lowerDate as datetime
DECLARE @upperDate as datetime

set @lowerDate = '2009-08-01'
set @upperDate = '2009-10-31'


DECLARE @supervisorId AS INTEGER
DECLARE @merchandisersIDs AS NVARCHAR(4000)
DECLARE @merchandisersCnt AS INTEGER

set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
set @merchandisersCnt = 0
set @supervisorId = NULL
*/

-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END

-- Исключаем маршруты с одинаковыми названиями для одного и того же ТТ и ТП.
SELECT ROW_NUMBER() OVER (PARTITION BY tblOutletRoutes.OL_id, tblRoutes.Merch_ID, (SUBSTRING(tblRoutes.RouteName, 1, 1) % 8) ORDER BY (SELECT 1)) ROUTES_CNT
		, tblOutletRoutes.OL_id
		, tblRoutes.Merch_ID
		, tblRoutes.RouteName
		, SUBSTRING(tblRoutes.RouteName, 1, 1) % 8 AS RouteDay
INTO #Routes
FROM tblRoutes
INNER JOIN #merchandisersIDs AS IDs ON tblRoutes.Merch_ID = IDs.id 
								OR @merchandisersCnt = 0
INNER JOIN tblOutletRoutes ON tblRoutes.Route_id = tblOutletRoutes.Route_id 
							AND tblOutletRoutes.Status = 2

-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT r.OL_id 
		, r.Merch_Id
		, matl.ActivityType
		, r.RouteDay
		, ROW_NUMBER() OVER (PARTITION BY r.OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
INTO #OL_Merch_Chanal
FROM  #Routes r 
	INNER JOIN tblOutlets ol ON r.OL_id = ol.OL_id
				AND ol.Status = 2
	INNER JOIN 
	( -- Ранжируем  типы каналов внури ТП
		SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
			 ,  Merch_ID
			 ,  ActivityType
		FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
	) matl ON matl.MerchActivity_ID = 1  -- Выбираем первую попавшуюся Activity (канал) у ТП
		  AND r.Merch_Id = matl.Merch_Id 
WHERE r.ROUTES_CNT = 1

-- Сохраняем во временную таблицу визиты с правильными ТП
SELECT olmc.Merch_id
	,och.OL_id
	,och.OLCardDate	
	,och.OLCard_id
	,och.BeginTime
	,och.EndTime
	,och.Comments
INTO #Correct_Visits
FROM tblOutletCardH och
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id AND DATEPART(dw, och.OLCardDate) = olmc.RouteDay AND olmc.Merch_id = 
(
CASE 
	WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
					SELECT 1 
					FROM #OL_Merch_Chanal olmc1 
					WHERE olmc1.OL_id = olmc.OL_id 
					  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
					)
		  )
	THEN -- на ТТ завязан только один ТП.
		 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
		olmc.Merch_Id 
	ELSE -- на ТТ завязано несколько ТП.
		CASE
			WHEN ( -- Проверяем ТП, привязан ли он к ТТ
				  EXISTS(
						SELECT 1 
						FROM #OL_Merch_Chanal olmc2
						WHERE olmc2.OL_id = olmc.OL_id 
							AND olmc2.Merch_Id = och.Merch_Id
							AND olmc2.RouteDay = DATEPART(dw, och.OLCardDate) -- Да, привязан
						)
				 )
			THEN 
				och.Merch_Id 
			ELSE -- Выбираем ТТ из заказов и ищем его канал и выбираем ТП из Привязаных через маршруты. 
				 -- Выбираем первый попавшийся канад у ТП, который совершил визит
				(SELECT TOP 1 olmc3.Merch_ID
				FROM #OL_Merch_Chanal olmc3
				INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
														  AND matl.Merch_id = och.Merch_id
														  AND olmc3.OL_id = och.OL_id
														  AND olmc3.RouteDay = DATEPART(dw, och.OLCardDate)
				)
		END
END
)
WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate

-- Результирующая выборка.
SELECT VisitDates.Date
	, Routes.RouteName
	, tblOutlets.OLTradingName
	, tblOutlets.OLAddress
	, tblOutlets.OLName
	, tblOutlets.OLDeliveryAddress
	, tblSupervisors.Supervisor_name
	, tblSyncStatus.MerchName
	, tblOutletSubTypes.OLSubTypeName
	, tblOutletTypes.OLtype_name
	--, tblSyncStatus.Merch_Id
	--, tblOutlets.OLName
	--, tblOutlets.OL_id
	, OutletCards.OrdersCount
	, OutletCards.RemaindersCount
	, OutletSales.SalesCount
	, CONVERT(VARCHAR(8), OutletCards.BeginTime, 114)  AS BeginTime
	, CONVERT(VARCHAR(8), OutletCards.EndTime, 114)  AS EndTime
	, CONVERT(VARCHAR(8), OutletCards.EndTime - OutletCards.BeginTime, 114) AS Duration
	, OutletCards.Comments
FROM 
(
	-- Generating a table with dates between @lowerDate and @upparDate
	SELECT DATEADD(day, z.num, @lowerDate) AS [Date]
	FROM (
	SELECT b10.i + b9.i + b8.i + b7.i + b6.i + b5.i + b4.i + b3.i + b2.i + b1.i + b0.i num
	FROM (SELECT 0 i UNION ALL SELECT 1) b0
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 2) b1
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 4) b2
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 8) b3
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 16) b4
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 32) b5
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 64) b6
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 128) b7
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 256) b8
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 512) b9
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 1024) b10
	) z
	WHERE z.num <= DATEDIFF(day, @lowerDate, @upperDate)
) AS VisitDates
INNER JOIN #Routes AS Routes ON Routes.RouteDay = DATEPART(dw, VisitDates.[Date])
		AND Routes.ROUTES_CNT = 1
INNER JOIN tblOutlets ON tblOutlets.OL_id = Routes.OL_id
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = Routes.Merch_ID
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id AND tblSupervisors.Status <> 9
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
LEFT JOIN
(   SELECT vis.OL_id
		, vis.Merch_Id
		, vis.OLCardDate
		, latest_vis.BeginTime
		, latest_vis.EndTime
		, latest_vis.Comments
		, SUM(OutletOrders.OrdersCount)	AS OrdersCount
		, SUM(OutletOrders.RemaindersCount) AS RemaindersCount
	FROM #Correct_Visits vis
	INNER JOIN 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY OL_id, Merch_id, OLCardDate ORDER BY EndTime) LastVisit_NBR
			 , OL_id
			 , Merch_id
			 , OLCardDate
			 , BeginTime
			 , EndTime
			 , Comments
		FROM #Correct_Visits		
	)AS latest_vis ON latest_vis.OL_id = vis.OL_id
			AND latest_vis.OLCardDate = vis.OLCardDate
			AND latest_vis.Merch_id = vis.Merch_id
			AND latest_vis.LastVisit_NBR = 1
	LEFT JOIN
	(
		SELECT
			  och.OLCard_id
			, SUM(tblOutletOrderD.Product_Qty * tblProducts.ProductVolume) AS OrdersCount
			, NULL AS RemaindersCount
		FROM tblOutletCardH och
		INNER JOIN tblOutletOrderH ON tblOutletOrderH.OLCard_id = och.OLCard_id
		INNER JOIN tblOutletOrderD ON tblOutletOrderD.OrderNo = tblOutletOrderH.OrderNo
		INNER JOIN tblProducts ON tblProducts.Product_id = tblOutletOrderD.Product_id
		INNER JOIN tblProductTypes ON tblProducts.ProductType_id = tblProductTypes.ProductType_id 
		WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
		GROUP BY och.OLCard_id

		UNION ALL

		SELECT
			  och.OLCard_id
			, NULL AS OrdersCount
			, SUM(tblOutletDistribution.IsPresent * tblProducts.ProductVolume) AS RemaindersCount
		FROM tblOutletCardH och
		INNER JOIN tblOutletDistribution ON tblOutletDistribution.OlCard_id = och.OLCard_id
		INNER JOIN tblProducts ON tblProducts.Product_id = tblOutletDistribution.Product_id
		INNER JOIN tblProductTypes ON tblProducts.ProductType_id = tblProductTypes.ProductType_id 
		WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
		GROUP BY och.OLCard_id
	) AS OutletOrders ON OutletOrders.OLCard_id = vis.OLCard_id
	GROUP BY vis.OL_id
		, vis.Merch_Id
		, vis.OLCardDate
		, latest_vis.BeginTime
		, latest_vis.EndTime
		, latest_vis.Comments
) AS OutletCards ON OutletCards.OL_id = tblOutlets.OL_id
				 AND OutletCards.Merch_Id = tblSyncStatus.Merch_id
				 AND CONVERT(varchar, OutletCards.OLCardDate, 112) = CONVERT(varchar, VisitDates.Date, 112)
LEFT JOIN
(
	SELECT
		  sal.[Date]
		, olmc.Merch_Id
		, sal.OL_id
		, SUM(tblSalOutD.Product_Qty * tblProducts.ProductVolume) SalesCount
	FROM tblSalOutH sal
	INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = sal.OL_id AND DATEPART(dw, sal.[Date]) = olmc.RouteDay AND olmc.Merch_Id = 
	(
	CASE 
		WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
						SELECT 1 
						FROM #OL_Merch_Chanal olmc1 
						WHERE olmc1.OL_id = olmc.OL_id 
						  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
						)
			  )
		THEN -- на ТТ завязан только один ТП.
			 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
			olmc.Merch_Id 
		ELSE -- на ТТ завязано несколько ТП.
			CASE
				WHEN ( -- Проверяем ТП, привязан ли он к ТТ
					  EXISTS(
							SELECT 1 
							FROM #OL_Merch_Chanal olmc2
							WHERE olmc2.OL_id = olmc.OL_id 
								AND olmc2.Merch_Id = sal.Merch_Id
								AND olmc2.RouteDay = DATEPART(dw, sal.[Date]) -- Да, привязан
							)
					 )
				THEN 
					sal.Merch_Id 
				ELSE -- Выбираем ТТ из заказов и ищем его канал и выбираем ТП из Привязаных через маршруты. 
					 -- Выбираем первый попавшийся канад у ТП, который совершил визит
					(SELECT TOP 1 olmc3.Merch_ID
					FROM #OL_Merch_Chanal olmc3
					INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
															  AND matl.Merch_id = sal.Merch_id
															  AND olmc3.OL_id = sal.OL_id
															  AND olmc3.RouteDay = DATEPART(dw, sal.[Date])
					)
			END
	END
	)
	INNER JOIN tblSalOutD ON tblSalOutD.Invoice_id = sal.Invoice_id 
	INNER JOIN tblProducts ON tblProducts.Product_id = tblSalOutD.Product_id
	WHERE sal.[Date] BETWEEN @lowerDate AND @upperDate
		AND sal.Status <> 9
	GROUP BY sal.[Date]
		, olmc.Merch_Id
		, sal.OL_id
) AS OutletSales ON OutletSales.OL_id = tblOutlets.OL_id
				 AND OutletSales.Merch_ID = tblSyncStatus.Merch_id
				 AND CONVERT(varchar, OutletSales.[Date], 112) = CONVERT(varchar, VisitDates.Date, 112)

ORDER BY VisitDates.Date

DROP TABLE #merchandisersIDs
DROP TABLE #OL_Merch_Chanal
DROP TABLE #Correct_Visits
DROP TABLE #Routes
";
            }
        }

        public static string VisitsAnalysis
        {
            get
            {
                return @"
-- =============================================
-- Author:		Developer 2
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- 
-- Changed
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Coverage analysis' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************


--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER

--set @merchandisersIDs = '        33,        35,        36,        37,        40,        41,       128,    600007'
--set @merchandisersCnt = 0
--set @supervisorId = NULL
--set @lowerDate = '2009-08-01'
--set @upperDate = '2009-10-31'


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END

-- Выбираем продукцию и все связанными с ним данные
SELECT p.Product_Id
	 , p.ProductName -- СКЮ
	 , pt.ProdTypeName -- Сорт
	 , pg.ProdGroupName -- Бренд
	 , u.Unit_Name -- Тип Упаковки
	 , p.productVolume * 10 AS Packing_Capacity -- Ёмкость Упаковки
	 , CASE WHEN ptp.ProductName LIKE '%BNR%' 
	   THEN 
			CASE WHEN ptp.ProductName LIKE '% ТУ%' 
			THEN 
				'BNR ТУ' 
			ELSE  
				CASE WHEN ptp.ProductName LIKE '% InBev%' 
				THEN 
					'BNR InBev' 
				ELSE 'BNR' END 
			 END
		ELSE 'Other' 
		END  AS Packing_Type -- Вид Упаковки
	 , pc.ProductCombineName -- Наименование комбинированной продукции
	
INTO #products
FROM tblProducts p 
INNER JOIN tblProductTypes pt ON pt.ProductType_id = p.ProductType_id 
INNER JOIN tblProductGroups pg ON pt.ProdGroup_ID = pg.ProdGroup_ID 
INNER JOIN tblUnits u ON p.Unit_ID = u.Unit_ID 
INNER JOIN tblProductCombine pc ON p.HLCode = pc.HLCode 
LEFT JOIN tblProducts ptp ON p.Tare_Id = ptp.Product_Id

-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- Выбираем ТП с бОльшим ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
											AND olr.Status = 2
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- Ранжируем  типы каналов внури ТП
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 , Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END

		
		) matl ON matl.MerchActivity_ID = 1  -- Выбираем первую попавшуюся Activity (канал) у ТП
			  AND r.Merch_Id = matl.Merch_Id 
		GROUP BY ol.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 


SELECT olmc.Merch_Id 
--och.Merch_Id 
 , och.OLCardDate
 , och.OL_Id -- Visited OL
 , CASE WHEN sod.Product_Id IS NULL 
		THEN NULL
		ELSE och.OL_Id
   END AS OL_Visited_IsEffective -- The visit is effective
 , ood.Product_Id AS Product_Id
 ,och.OLCard_Id
INTO #tmpOch
FROM tblOutletCardH  AS och
	INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_ID = och.OL_ID
								AND olmc.Merch_ID = 
	(
		CASE 
			WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
							)
				  ) 
			THEN -- на ТТ завязан только один ТП.
				 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
				olmc.Merch_ID 
			ELSE -- на ТТ завязано несколько ТП.
				CASE
					WHEN ( -- Проверяем ТП, привязан ли он к ТТ
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = och.Merch_ID  -- Да, привязан
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- Выбираем ТТ из 'заказов' и ищем его канал и выбираем ТП из 'Привязаных через маршруты'. 
						 -- Выбираем первый попавшийся канад у ТП, который совершил визит
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = och.Merch_ID
																  AND olmc3.OL_id = och.OL_ID
						)
				END
		END
	)

LEFT JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
							  AND och.OLCardDate BETWEEN @lowerDate AND @upperDate
LEFT JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
LEFT JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo 
LEFT JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id 
						AND sod.Product_Id = ood.Product_Id 
						AND ood.Product_qty <> 0 
						AND sod.Product_qty <> 0
-- Выбираем визиты, которые не были активными, тобишь не попали в пердыдущий запрос
SELECT * 
INTO #tmpOch1
FROM
(
	SELECT Merch_ID
		, OLCardDate
		, OL_ID
		, OL_Visited_IsEffective
		, p.Product_Id
		, OLCard_Id
		, ProductName -- СКЮ
		, ProdTypeName -- Сорт
		, ProdGroupName -- Бренд
		, Unit_Name -- Тип Упаковки
		, Packing_Capacity -- Ёмкость Упаковки
		, Packing_Type -- Вид Упаковки
		, ProductCombineName 
	FROM #tmpOch och
	LEFT JOIN #products p ON p.Product_id = och.Product_id
	UNION
	SELECT Merch_ID
		, OLCardDate
		, OL_ID
		, NULL AS OL_Visited_IsEffective
		, NULL AS Product_Id
		, OLCard_Id
		, NULL AS ProductName -- СКЮ
		, NULL AS ProdTypeName -- Сорт
		, NULL AS ProdGroupName -- Бренд
		, NULL AS Unit_Name -- Тип Упаковки
		, NULL AS Packing_Capacity -- Ёмкость Упаковки
		, NULL AS Packing_Type -- Вид Упаковки
		, NULL AS ProductCombineName 
	FROM 
	(
		SELECT DISTINCT OLCard_Id, olmc.OL_Id, olmc.Merch_id, OLCardDate 
		FROM tblOutletCardH och
		INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_ID = och.OL_ID
									AND olmc.Merch_ID = 
		(
			CASE 
				WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
								SELECT 1 
								FROM #OL_Merch_Chanal olmc1 
								WHERE olmc1.OL_id = olmc.OL_id 
								  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
								)
					  ) 
				THEN -- на ТТ завязан только один ТП.
					 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
					olmc.Merch_ID 
				ELSE -- на ТТ завязано несколько ТП.
					CASE
						WHEN ( -- Проверяем ТП, привязан ли он к ТТ
							  EXISTS(
									SELECT 1 
									FROM #OL_Merch_Chanal olmc2
									WHERE olmc2.OL_id = olmc.OL_id 
										AND olmc2.Merch_Id = och.Merch_ID  -- Да, привязан
									)
							 )
						THEN --  
							och.Merch_Id 
						ELSE -- Выбираем ТТ из 'заказов' и ищем его канал и выбираем ТП из 'Привязаных через маршруты'. 
							 -- Выбираем первый попавшийся канад у ТП, который совершил визит
							(SELECT TOP 1 olmc3.Merch_ID
							FROM #OL_Merch_Chanal olmc3
							INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																	  AND matl.Merch_id = och.Merch_ID
																	  AND olmc3.OL_id = och.OL_ID
							)
					END
			END
		)		
		WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
		EXCEPT
		Select DISTINCT OLCard_Id, OL_Id, Merch_id, OLCardDate FROM #tmpOch
	) AS D 
)AS DD


-- Добавляем ТТ, в которые не было визитов
SELECT * 
INTO #Result
FROM
(
	SELECT Merch_ID
		, OLCardDate
		, OL_ID
		, OL_ID AS OL_Visited	
		, OL_Visited_IsEffective
		, Product_Id
		, OLCard_Id
		, ProductName -- СКЮ
		, ProdTypeName -- Сорт
		, ProdGroupName -- Бренд
		, Unit_Name -- Тип Упаковки
		, Packing_Capacity -- Ёмкость Упаковки
		, Packing_Type -- Вид Упаковки
		, ProductCombineName 
	FROM #tmpOch1
	UNION
	SELECT  Merch_ID
		, NULL AS OLCardDate
		, OL_ID
		, NULL AS OL_Visited
		, NULL AS OL_Visited_IsEffective
		, NULL AS Product_Id
		, NULL AS OLCard_Id
		, NULL AS ProductName -- СКЮ
		, NULL AS ProdTypeName -- Сорт
		, NULL AS ProdGroupName -- Бренд
		, NULL AS Unit_Name -- Тип Упаковки
		, NULL AS Packing_Capacity -- Ёмкость Упаковки
		, NULL AS Packing_Type -- Вид Упаковки
		, NULL AS ProductCombineName 
	FROM 
	(
		SELECT DISTINCT OL_Id, Merch_id  FROM #OL_Merch_Chanal
		EXCEPT
		Select DISTINCT OL_Id, Merch_id From #tmpOch1
	) AS D 
) AS DD

--Visites
SELECT sp.Cust_Name -- Точка синхронизации
 	 , s.Supervisor_name -- Супервизор
	 , c.City_Name	 -- Город
	 , d.District_Name -- Область
	 , NULL AS Region_Name -- Район (ТТ) TODO: join to tblCountryArea
	 , a.Area_Name -- Район в городе (ТТ) 
	 , NULL AS Territory_Type -- Тип территории TODO: join to  tblSettlement
	 , och.Merch_Id
     , m.MerchName -- ТП
	 , ms.LValue AS MerchandiserStatus -- Статус ТП
	 , och.OLCardDate -- Время
	 , och.OL_Visited-- Outlet is visited by Merchandiser 
	 , och.OL_Visited_IsEffective AS OL_Visited_IsEffective -- Outlet is Active
     , och.OL_id -- Код ТТ
 	 , ol.OL_Code -- Внешний кодТТ
	 , ols.LValue as OutletsSatus -- Статус ТТ
	 , olt.OLtype_name --ТипТТ
	 , olst.OLSubTypeName -- ПодТипТТ
	 , ol.OLName  --Факт имя
	 , ol.OLDeliveryAddress -- Факт. адрес
	 , ol.OLTradingName -- Юр. имя
	 , ol.OLAddress --Юр. адрес
	 , och.Product_Id
	 , och.ProductName -- СКЮ
	 , och.ProdGroupName -- Бренд
	 , och.ProdTypeName -- Сорт
	 , och.Unit_Name -- Тип Упаковки
	 , och.Packing_Capacity -- Ёмкость Упаковки
	 , och.Packing_Type -- Вид Упаковки
	 , och.ProductCombineName -- Наименование комбинированной продукции
	 , pf.LValue AS ProximityFactor -- Фактор близости
FROM #Result AS  och 
INNER JOIN tblMerchandisers m ON m.Merch_id = och.Merch_id
INNER JOIN tblGloballookup ms ON ms.LKey = m.Status 
							  AND ms.TableName = 'tblMerchandisers' 
							  AND ms.FieldName = 'Status'
INNER JOIN tblOutlets ol ON och.OL_id = ol.OL_id
INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
INNER JOIN tblGloballookup ols ON ols.LKey = ol.Status 
							  AND ols.TableName = 'tblOutlets' 
							  AND ols.FieldName = 'Status'
INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
						  AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS cs ON cs.Supervisor_id = s.Supervisor_id	
			AND cs.CustomerOrder = 1	-- Т.к. нет возможности точно выбрать ТС, 
										-- выбираем первую попавшуюся из принадлежащих СВ
INNER JOIN tblCustomers sp ON cs.Cust_id = sp.Cust_Id 
INNER JOIN tblAreas a ON ol.Area_id = a.Area_Id
INNER JOIN tblCities c ON c.City_id = a.City_Id
INNER JOIN tblDistricts d ON d.District_id = c.District_id
INNER JOIN tblGloballookup pf ON 
 (pf.LKey = CASE WHEN ol.Proximity IS NULL 
    THEN -3 
    ELSE ol.Proximity
    END
 ) 
 AND pf.TableName = 'tblOutlets' 
    AND pf.FieldName = 'Proximity'

DROP TABLE #OL_Merch_Chanal
DROP TABLE #tmpOch
DROP TABLE #tmpOch1
DROP TABLE #merchandisersIDs
DROP TABLE #products
DROP TABLE #Result
";
            }
        }

        public static string ProductsMotion
        {
            get
            {
                return @"
-- =============================================
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Product motion' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime - 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************

--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
--
--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL
--set @lowerDate = '2009-08-01'
--set @upperDate = '2009-10-30'


-- Define MErchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END

-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- Выбираем ТП с бОльшим ID
		--r.Merch_Id
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
											AND olr.Status = 2 AND r.Status = 2
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- Ранжируем  типы каналов внури ТП
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 , Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
			
		) matl ON matl.MerchActivity_ID = 1  -- Выбираем первую попавшуюся Activity (канал) у ТП
			  AND r.Merch_Id = matl.Merch_Id 
		GROUP BY ol.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 

-- Выбираем продукцию и все связанными с ним данные
SELECT p.Product_Id
	 , p.ProductName -- СКЮ
	 , pt.ProdTypeName -- Сорт
	 , pg.ProdGroupName -- Бренд
	 , u.Unit_Name -- Тип Упаковки
	 , p.productVolume * 10 AS Packing_Capacity -- Ёмкость Упаковки
	 , p.productVolume 
	 , CASE WHEN ptp.ProductName LIKE '%BNR%' 
	   THEN 
			CASE WHEN ptp.ProductName LIKE '% ТУ%' 
			THEN 
				'BNR ТУ' 
			ELSE  
				CASE WHEN ptp.ProductName LIKE '% InBev%' 
				THEN 
					'BNR InBev' 
				ELSE 'BNR' END 
			 END
		ELSE 'Other' 
		END  AS Packing_Type -- Вид Упаковки
	 , pc.ProductCombineName -- Наименование комбинированной продукции
	
INTO #products
FROM tblProducts p 
INNER JOIN tblProductTypes pt ON pt.ProductType_id = p.ProductType_id 
INNER JOIN tblProductGroups pg ON pt.ProdGroup_ID = pg.ProdGroup_ID 
INNER JOIN tblUnits u ON p.Unit_ID = u.Unit_ID 
INNER JOIN tblProductCombine pc ON p.HLCode = pc.HLCode 
LEFT JOIN tblProducts ptp ON p.Tare_Id = ptp.Product_Id

-- Отгрузки, которые не вошли в заявку (Без заякви или небыли включены в заявку)
SELECT * 
INTO #pm
FROM 
(
	SELECT soh.OL_Id
		 , soh.Merch_id
		 , soh.OrderNo

		 , NULL AS OrderedProductVolume -- Взятый заказ (Здесь не может быь данных)
		 , sod.Product_qty AS SaledOrderedProductVolume -- Отгрузка (все данные входят в отгрузку)
		 , NULL AS ToTakeOffProductVolume -- Не вывоз (Здесь не может быь данных)
		 , NULL AS RemainsProductVolume -- Остаткаи TODO:
		 , CASE -- Была ли заявка на продукцию
				WHEN soh.OrderNo = 0
				THEN -- Нет, заявки не было
					sod.Product_qty 
				ELSE -- Заяка была и эту запись должны отнести к 'Сверх заявки'
				 NULL
			END AS WithoutOrder -- Без заявки
		 , CASE -- Была ли заявка на продукцию
				WHEN soh.OrderNo > 0 
				THEN -- Заявка была
					sod.Product_qty 
				ELSE  -- Заяки не было. Эту запись отнесли у 'Без заявки'
					NULL
			END AS OverOrdered -- Сверх заявки

		  , soh.Date AS SaleDate -- Дата
		  , och.OLCardDate AS OrderDate -- Дата
		  , sod.Product_id
	FROM tblSalOutH soh 
	INNER join tblSaloutD sod ON soh.Invoice_Id = sod.Invoice_Id 
							 AND soh.Date BETWEEN @lowerDate AND @upperDate 
	LEFT JOIN tblOutletOrderD ood ON soh.OrderNo = ood.OrderNo  
								 AND sod.Product_id = ood.Product_id
	LEFT JOIN tblOutletOrderH ooh ON soh.OrderNo = ooh.OrderNo
	LEFT JOIN tblOutletCardH och ON och.OLCard_id = ooh.OLCard_id
	WHERE ood.Product_id IS NULL 

	UNION ALL

	-- Заявки и отгрузки по ним
	SELECT och.OL_id
		 , och.Merch_Id
		 , ooh.OrderNo
		 , ood.Product_qty AS OrderedProductVolume -- Взятый заказ
		  -- Отгрузка
		  -- В отгрузку не должны попасть записи, заявки по которым были в текущем отчетном месяце, а реально отгружены в следующем
		  , CASE
				WHEN 
					soh.Date BETWEEN @lowerDate AND @upperDate
				THEN -- Дата отгрузки попадает в отчетный период
					sod.Product_qty 
				ELSE -- Дата отгрузки не попадает в отчетный период
					0
			END AS SaledOrderedProductVolume -- Отгрузка
		  , CASE 
				WHEN ISNULL(sod.Product_qty, 0)  > ood.Product_qty -- проверяем, есть ли заявлнная продукция 
				THEN -- Да, продано больше, чем было в заявке
					NULL
				ELSE -- Продано меньше или столько же, сколько заявлено
					ood.Product_qty - ISNULL(sod.Product_qty, 0) 
			END  AS ToTakeOffProductVolume -- Не вывоз
		  , od.IsPresent AS RemainsProductVolume -- Остаткаи
		  , NULL AS WithoutOrder -- Без заявки
		  , CASE
				WHEN ISNULL(sod.Product_qty, 0)  > ood.Product_qty -- проверяем, есть ли превышение отгрузки продукции
				THEN -- Да, продано больше, чем было в заявке
					sod.Product_qty - ood.Product_qty
				ELSE -- Продано меньше или столько же, сколько заявлено 
					NULL 
			END AS OverOrdered -- Сверх заявки
		  , soh.Date AS SaleDate -- Дата
		  , och.OLCardDate AS OrderDate -- Дата
		  , ood.Product_id

	FROM tblOutletCardH och
	INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
 								  AND och.OLCardDate BETWEEN @lowerDate AND @upperDate 

	INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
	INNER JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo 
	LEFT JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id 
							AND sod.Product_Id = ood.Product_Id
	LEFT JOIN tblOutletDistribution od ON och.OLCard_id = od.OLCard_id 
			 						  AND ood.Product_Id = od.Product_Id

	UNION ALL

	-- Остатки, которые не были учтены в заявках	

	SELECT och.OL_id
		 , och.Merch_Id
		 , ooh.OrderNo
		 , NULL AS OrderedProductVolume -- Взятый заказ
		 , NULL AS SaledOrderedProductVolume -- Отгрузка
		 , NULL AS ToTakeOffProductVolume -- Не вывоз
		 , od.IsPresent AS RemainsProductVolume -- Остаткаи
		 , NULL AS WithoutOrder -- Без заявки
		 , NULL AS OverOrdered -- Сверх заявки
		 , NULL AS SaleDate -- Дата
		 , och.OLCardDate AS OrderDate -- Дата
		 , od.Product_id

	FROM tblOutletDistribution od
	INNER JOIN tblOutletCardH och ON och.OLCard_id = od.OLCard_id 
								AND och.OLCardDate BETWEEN @lowerDate AND @upperDate 
	LEFT JOIN tblOutletOrderH ooh ON od.OLCard_id = ooh.OLCard_id 
	LEFT JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
								 AND ood.Product_Id = od.Product_Id
	WHERE ooh.OrderNo is NULL
) AS d

SELECT sp.Cust_Name -- Точка синхронизации
	  , s.Supervisor_name -- Супервизор
	  , c.City_Name	 -- Город
	  , d.District_Name -- Область	  
	  , NULL AS Region_Name -- Район (ТТ) TODO: join to tblCountryArea
	  , a.Area_Name -- Район в городе (ТТ) 
	  , NULL AS Territory_Type -- Тип территории TODO: join to  tblSettlement
	  , m.MerchName -- ТП	  
	  , ms.LValue AS MerchandiserStatus -- Статус ТП
	  , os.SaleDate -- Дата
	  , os.OrderDate -- Дата
	  , ISNULL(os.OrderedProductVolume, 0) * p.ProductVolume AS OrderedProductVolume -- Взятый заказ 
	  , ISNULL(os.SaledOrderedProductVolume, 0) * p.ProductVolume AS SaledOrderedProductVolume -- Отгрузка 
	  , ISNULL(os.ToTakeOffProductVolume, 0) * p.ProductVolume AS ToTakeOffProductVolume -- Не вывоз 
	  , ISNULL(os.RemainsProductVolume, 0) * p.ProductVolume AS RemainsProductVolume -- Остаткаи TODO:
      , ISNULL(os.WithoutOrder, 0) * p.ProductVolume AS WithoutOrder -- Без заявки
	  , ISNULL(os.OverOrdered, 0) * p.ProductVolume AS OverOrdered -- Сверх заявки
      , os.OL_id -- Код ТТ
	  ,	ol.OL_Code -- Внешний кодТТ
	  , ols.LValue as OutletsSatus -- Статус ТТ
	  , olt.OLtype_name --ТипТТ
	  , olst.OLSubTypeName -- ПодТипТТ
	  , ol.OLName  --Факт имя
	  , ol.OLDeliveryAddress  -- Факт. адрес
	  , ol.OLTradingName -- Юр. имя
	  , ol.OLAddress  --Юр. адрес
	  , p.Product_Id
	  , p.ProductName -- СКЮ
	  , p.ProdGroupName -- Бренд	  
	  , p.ProdTypeName -- Сорт
	  , p.Unit_Name -- Тип Упаковки
	  , p.Packing_Capacity -- Ёмкость Упаковки
	  , p.Packing_Type -- Вид Упаковки
	  , p.ProductCombineName -- Наименование комбинированной продукции
	  , pf.LValue AS ProximityFactor -- Фактор близости
FROM #OL_Merch_Chanal olmc
INNER JOIN #pm AS os ON  olmc.OL_id = os.OL_id 
		AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
							)
				  ) 
			THEN -- на ТТ завязан только один ТП.
				 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
				olmc.Merch_ID 
			ELSE -- на ТТ завязано несколько ТП.
				CASE
					WHEN ( -- Проверяем ТП, привязан ли он к ТТ
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = os.Merch_Id -- Да, привязан
								)
						 )
					THEN --  
						os.Merch_Id 
					ELSE -- Выбираем ТТ из 'заказов' и ищем его канал и выбираем ТП из 'Привязаных через маршруты'. 
						 -- Выбираем первый попавшийся канад у ТП, который совершил визит
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = os.Merch_id
																  AND olmc3.OL_id = os.OL_id
						)
				END
		END
		)
INNER JOIN tblMerchandisers m ON m.Merch_id = olmc.Merch_id
INNER JOIN tblGloballookup ms ON ms.LKey = m.Status 
							  AND ms.TableName = 'tblMerchandisers' 
							  AND ms.FieldName = 'Status'
INNER JOIN tblOutlets ol ON olmc.OL_id = ol.OL_id
INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
INNER JOIN tblGloballookup ols ON ols.LKey = ol.Status 
							  AND ols.TableName = 'tblOutlets' 
							  AND ols.FieldName = 'Status'
INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
						  AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS cs ON cs.Supervisor_id = s.Supervisor_id	
			AND cs.CustomerOrder = 1	-- Т.к. нет возможности точно выбрать ТС, 
										-- выбираем первую попавшуюся из принадлежащих СВ
INNER JOIN tblCustomers sp ON cs.Cust_id = sp.Cust_Id 
INNER JOIN tblAreas a ON ol.Area_id = a.Area_Id
INNER JOIN tblCities c ON c.City_id = a.City_Id
INNER JOIN tblDistricts d ON d.District_id = c.District_id
INNER JOIN #products p ON p.Product_id = os.Product_id
INNER JOIN tblGloballookup pf ON 
 (pf.LKey = CASE WHEN ol.Proximity IS NULL 
    THEN -3 
    ELSE ol.Proximity
    END
 ) 
 AND pf.TableName = 'tblOutlets' 
    AND pf.FieldName = 'Proximity'



DROP TABLE #merchandisersIDs
DROP TABLE #OL_Merch_Chanal
DROP TABLE #products
DROP TABLE #pm

";
            }
        }

        public static string ActiveOutletsTOP10
        {
            get 
            {
                return @"
-- =============================================
-- Author:		Developer 1
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************

--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER

--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL
--set @lowerDate = '2009-10-01'
--set @upperDate = '2009-10-30'

SELECT pt.ProductCn_ID 
	 , pt.Region_ID
	 , pt.StartDate
	 , pt.EndDate 
INTO #top10
FROM [LDB].[dbo].DW_ProductCnTop pt

	WHERE (@lowerDate BETWEEN StartDate AND EndDate) OR
		  (@upperDate BETWEEN StartDate AND EndDate)


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END

-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- Выбираем ТП с бОльшим ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
											AND olr.Status = 2
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- Ранжируем  типы каналов внури ТП
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 , Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
		) matl ON matl.MerchActivity_ID = 1  -- Выбираем первую попавшуюся Activity (канал) у ТП
			  AND r.Merch_Id = matl.Merch_Id 
		GROUP BY ol.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 


SELECT OL_ID
--	, Merch_Id
     , COUNT(OL_ID) as CNT
INTO #tmpOch
FROM (
		SELECT och.OL_ID
--			 , och.Merch_Id
			 , HLCode
			, pt10.StartDate
			, pt10.EndDate
			 
--		   , ROW_NUMBER() OVER (PARTITION BY och.OL_ID,Merch_Id,HLCode  ORDER BY (SELECT 1)) Cnt
		FROM tblOutletCardH och
		INNER JOIN tblOutlets ol ON och.OL_id = ol.OL_id 
			   AND och.OLCardDate BETWEEN @lowerDate AND @upperDate 
		INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
		INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
		INNER JOIN tblProducts p ON p.Product_Id = ood.Product_Id 
		INNER JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo 
		INNER JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id 
							AND sod.Product_Id = ood.Product_Id
		INNER JOIN #top10 pt10 ON CAST(p.HLCode AS INT) = pt10.ProductCn_ID
								AND och.OLCardDate BETWEEN pt10.StartDate AND pt10.EndDate

		GROUP BY och.OL_ID
--				, och.Merch_Id 
				, HLCode
				, pt10.StartDate
				, pt10.EndDate
			
) AS d
GROUP BY OL_ID
--, Merch_Id
HAVING COUNT(*) >= (SELECT COUNT(*) FROM #top10)


SELECT sp.Cust_Name -- Точка синхронизации
 	 , s.Supervisor_name -- Супервизор
	 , c.City_Name	 -- Город
	 , d.District_Name -- Область
	 , NULL AS Region_Name -- Район (ТТ) TODO: join to tblCountryArea
	 , a.Area_Name  -- Район в городе (ТТ) 
	 , NULL AS Territory_Type -- Тип территории TODO: join to  tblSettlement
	 , olmc.Merch_Id
     , m.MerchName -- ТП
	 , ms.LValue AS MerchandiserStatus -- Статус ТП
	 , och.OL_id AS  OL_Has -- Outlet is visited by Merchandiser 
     , och.OL_id -- Код ТТ
 	 , ol.OL_Code -- Внешний кодТТ
	 , ols.LValue as OutletsSatus -- Статус ТТ
	 , olt.OLtype_name --ТипТТ
	 , olst.OLSubTypeName -- ПодТипТТ
	 , ol.OLName --Факт имя
	 , ol.OLDeliveryAddress -- Факт. адрес
	 , ol.OLTradingName -- Юр. имя
	 , ol.OLAddress --Юр. адрес
	 , pf.LValue AS ProximityFactor -- Фактор близости
FROM #OL_Merch_Chanal olmc 
INNER JOIN 	#tmpOch  AS  och  ON och.OL_id = olmc.OL_id	
					-- AND olmc.Merch_Id = 
					--(
					--CASE 
					--	WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
					--					SELECT 1 
					--					FROM #OL_Merch_Chanal olmc1 
					--					WHERE olmc1.OL_id = olmc.OL_id 
					--					  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
					--					)
					--		  ) 
					--	THEN -- на ТТ завязан только один ТП.
					--		 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
					--		olmc.Merch_ID 
					--	ELSE -- на ТТ завязано несколько ТП.
					--		CASE
					--			WHEN ( -- Проверяем ТП, привязан ли он к ТТ
					--				  EXISTS(
					--						SELECT 1 
					--						FROM #OL_Merch_Chanal olmc2
					--						WHERE olmc2.OL_id = olmc.OL_id 
					--							AND olmc2.Merch_Id = och.Merch_Id -- Да, привязан
					--						)
					--				 )
					--			THEN --  
					--				och.Merch_Id 
					--			ELSE -- Выбираем ТТ из 'заказов' и ищем его канал и выбираем ТП из 'Привязаных через маршруты'. 
					--				 -- Выбираем первый попавшийся канад у ТП, который совершил визит
					--				(SELECT TOP 1 olmc3.Merch_ID
					--				FROM #OL_Merch_Chanal olmc3
					--				INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
					--														  AND matl.Merch_id = och.Merch_id
					--														  AND olmc3.OL_id = och.OL_id
					--				)
					--		END
					--END
					--)

INNER JOIN tblMerchandisers m ON m.Merch_id = olmc.Merch_id
INNER JOIN tblGloballookup ms ON ms.LKey = m.Status 
							  AND ms.TableName = 'tblMerchandisers' 
							  AND ms.FieldName = 'Status'
INNER JOIN tblOutlets ol ON olmc.OL_id = ol.OL_id
INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
INNER JOIN tblGloballookup ols ON ols.LKey = ol.Status 
							  AND ols.TableName = 'tblOutlets' 
							  AND ols.FieldName = 'Status'
INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
						  AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS cs ON cs.Supervisor_id = s.Supervisor_id	
			AND cs.CustomerOrder = 1	-- Т.к. нет возможности точно выбрать ТС, 
										-- выбираем первую попавшуюся из принадлежащих СВ
INNER JOIN tblCustomers sp ON cs.Cust_id = sp.Cust_Id 
INNER JOIN tblAreas a ON ol.Area_id = a.Area_Id
INNER JOIN tblCities c ON c.City_id = a.City_Id
INNER JOIN tblDistricts d ON d.District_id = c.District_id
INNER JOIN tblGloballookup pf ON  (pf.LKey = CASE WHEN ol.Proximity IS NULL 
									THEN -3 
									ELSE ol.Proximity
									END
								   ) 
								AND pf.TableName = 'tblOutlets' 
								AND pf.FieldName = 'Proximity'


DROP TABLE #OL_Merch_Chanal
DROP TABLE #tmpOch
DROP TABLE #merchandisersIDs
DROP TABLE #top10
";
            }
        }

        public static string VisitCommentsAnalysis
        {
            get
            {
                return @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 30-11-2009
-- Description:	Selects all needed data for 'Visit comments analysis' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1
/**/
--DECLARE @lowerDate as datetime
--DECLARE @upperDate as datetime
--set @lowerDate = '2009-08-01'
--set @upperDate = '2009-10-30'
--
--
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
--
--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END


-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- Выбираем ТП с бОльшим ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- Ранжируем  типы каналов внури ТП
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 ,  Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END

		) matl ON matl.MerchActivity_ID = 1 -- Выбираем первую попавшуюся Activity (канал) у ТП
			  AND r.Merch_Id = matl.Merch_Id  
	WHERE
		r.Status = 2
		AND olr.Status = 2
	GROUP BY ol.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 


-- Результирующая выборка
SELECT tblSupervisors.Supervisor_name
	 , tblSyncStatus.MerchName
	 , tblOutletTypes.OLtype_name
	 , tblOutletSubTypes.OLSubTypeName
	 , tblOutlets.OL_id
	 , tblOutlets.OLTradingName
	 , tblOutlets.OLAddress
	 , tblOutlets.OLName 
	 , tblOutlets.OLDeliveryAddress
	 , mers.LValue AS MerchStatus
	 , ols.LValue AS OutletsSatus
	 , och.OLCardDate
	 , tblInaccessibilityReasons.Reason
	 , och.Comments AS NotVisitedComments
	 , tblOutletOrderH.Comments AS CommentsByVisit
	 
FROM tblOutletCardH och
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id
		AND olmc.Merch_ID = 
(
CASE 
	WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
					SELECT 1 
					FROM #OL_Merch_Chanal olmc1 
					WHERE olmc1.OL_id = olmc.OL_id 
					  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
					)
		  ) 
	THEN -- на ТТ завязан только один ТП.
		 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
		olmc.Merch_ID 
	ELSE -- на ТТ завязано несколько ТП.
		CASE
			WHEN ( -- Проверяем ТП, привязан ли он к ТТ
				  EXISTS(
						SELECT 1 
						FROM #OL_Merch_Chanal olmc2
						WHERE olmc2.OL_id = olmc.OL_id 
							AND olmc2.Merch_Id = och.Merch_Id -- Да, привязан
						)
				 )
			THEN --  
				och.Merch_Id 
			ELSE -- Выбираем ТТ из заказов и ищем его канал и выбираем ТП из Привязаных через маршруты. 
				 -- Выбираем первый попавшийся канад у ТП, который совершил визит
				(SELECT TOP 1 olmc3.Merch_ID
				FROM #OL_Merch_Chanal olmc3
				INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
														  AND matl.Merch_id = och.Merch_id
														  AND olmc3.OL_id = och.OL_id
				)
		END
END
)
INNER JOIN tblOutlets ON tblOutlets.OL_id = och.OL_id
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = olmc.Merch_ID
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id AND tblSupervisors.Status <> 9
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
LEFT JOIN tblInaccessibilityReasons ON tblInaccessibilityReasons.Reason_id = och.Reason_id
LEFT JOIN tblOutletOrderH ON tblOutletOrderH.OlCard_id = och.OLCard_id
INNER JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status 
				AND ols.TableName = 'tblOutlets'
				AND ols.FieldName = 'Status'
INNER JOIN tblGloballookup mers ON mers.LKey = tblSyncStatus.Status 
				AND mers.TableName = 'tblSyncStatus'
				AND mers.FieldName = 'Status'

GROUP BY och.Comments
		, tblOutlets.OL_id
		, tblOutlets.OLAddress
		, tblOutlets.OLTradingName
		, tblOutlets.OLDeliveryAddress
		, tblOutlets.OLName
		, tblSupervisors.Supervisor_name
		, tblSyncStatus.MerchName
		, och.OLCardDate
		, tblOutLetTypes.OLtype_name
		, tblOutletSubTypes.OLSubTypeName
		, tblOutletOrderH.Comments
		, tblInaccessibilityReasons.Reason
		, tblOutlets.Status
		, tblSyncStatus.Status
		, mers.LValue
		, ols.LValue
HAVING och.OLCardDate BETWEEN @lowerDate AND @upperDate

ORDER BY och.OLCardDate

DROP TABLE #OL_Merch_Chanal
DROP TABLE #merchandisersIDs
";
            }
        }

        public static string StockturnAnalysis
        {
            get
            {
                return @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 30-11-2009
-- Description:	Selects all needed data for 'Stockturn analysis' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the function dbo.fnDW_Common_SplitText must be used to transform 
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1
/*
DECLARE @lowerDate as datetime
DECLARE @upperDate as datetime

set @lowerDate = '2009-08-01'
set @upperDate = '2009-09-30'


DECLARE @supervisorId AS INTEGER
DECLARE @merchandisersIDs AS NVARCHAR(4000)
DECLARE @merchandisersCnt AS INTEGER

set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
set @merchandisersCnt = 0
set @supervisorId = NULL
*/
-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				INSERT #merchandisersIDs VALUES (@id) 
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END

SELECT  -- Заполняем таблицу типами КЕГ, которые должны и их ценой.
	 ExternalCode
	,Product_id
INTO #kegDebts
FROM
(
	SELECT
		 ROW_NUMBER() OVER (PARTITION BY tblDebtTypes.ExternalCode ORDER BY (SELECT 1))  ExtCode_NBR
		,tblDebtTypes.ExternalCode
		,tblProducts.Product_id

	FROM
		tblDebtTypes
		INNER JOIN tblProducts ON tblProducts.ProductType_ID = 195
			AND tblProducts.Price <> 0
			AND tblProducts.ProductName like '%' + (CASE WHEN tblDebtTypes.ExternalCode = 31 THEN '20%'
														 WHEN tblDebtTypes.ExternalCode = 32 THEN '30%'
														 WHEN tblDebtTypes.ExternalCode = 33 THEN '50%' END)

	WHERE tblDebtTypes.ExternalCode IN (31, 32, 33)
) AS kegDebts
WHERE ExtCode_NBR = 1

-- Исключаем маршруты с одинаковыми названиями для одного и того же ТТ и ТП.
SELECT ROW_NUMBER() OVER (PARTITION BY tblOutletRoutes.OL_id, tblRoutes.Merch_ID, (SUBSTRING(tblRoutes.RouteName, 1, 1) % 8) ORDER BY (SELECT 1)) ROUTES_CNT
		, tblOutletRoutes.OL_id
		, tblRoutes.Merch_ID
		, tblRoutes.RouteName
		, SUBSTRING(tblRoutes.RouteName, 1, 1) % 8 AS RouteDay
INTO #Routes
FROM tblRoutes
INNER JOIN #merchandisersIDs AS IDs ON tblRoutes.Merch_ID = IDs.id 
								OR @merchandisersCnt = 0
INNER JOIN tblOutletRoutes ON tblRoutes.Route_id = tblOutletRoutes.Route_id 
							AND tblOutletRoutes.Status = 2

-- Выбираем ТП, котрые обслуживают ТТ с привязкой к каналу
SELECT r.OL_id 
		, r.Merch_Id
		, matl.ActivityType
		, r.RouteDay
		, ROW_NUMBER() OVER (PARTITION BY r.OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- Ранжируем по к-ву ТП, принадлежащих одной ТТ 
INTO #OL_Merch_Chanal
FROM  #Routes r 
	INNER JOIN tblOutlets ol ON r.OL_id = ol.OL_id
	INNER JOIN 
	( -- Ранжируем  типы каналов внури ТП
		SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
			 ,  Merch_ID
			 ,  ActivityType
		FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
	) matl ON matl.MerchActivity_ID = 1  -- Выбираем первую попавшуюся Activity (канал) у ТП
		  AND r.Merch_Id = matl.Merch_Id 
WHERE r.ROUTES_CNT = 1

-- Generating a table with dates between @lowerDate and @upperDate
SELECT DATEADD(day, z.num, @lowerDate) AS [Date]
INTO #VisitDates
FROM (
SELECT b10.i + b9.i + b8.i + b7.i + b6.i + b5.i + b4.i + b3.i + b2.i + b1.i + b0.i num
FROM (SELECT 0 i UNION ALL SELECT 1) b0
CROSS JOIN (SELECT 0 i UNION ALL SELECT 2) b1
CROSS JOIN (SELECT 0 i UNION ALL SELECT 4) b2
CROSS JOIN (SELECT 0 i UNION ALL SELECT 8) b3
CROSS JOIN (SELECT 0 i UNION ALL SELECT 16) b4
CROSS JOIN (SELECT 0 i UNION ALL SELECT 32) b5
CROSS JOIN (SELECT 0 i UNION ALL SELECT 64) b6 
CROSS JOIN (SELECT 0 i UNION ALL SELECT 128) b7
CROSS JOIN (SELECT 0 i UNION ALL SELECT 256) b8
CROSS JOIN (SELECT 0 i UNION ALL SELECT 512) b9
CROSS JOIN (SELECT 0 i UNION ALL SELECT 1024) b10
) z
WHERE z.num <= DATEDIFF(day, @lowerDate, @upperDate)


-- Заполняем таблицу данными о продажах за заданный период +30 дней назад.
SELECT SUM(tblSalOutD.Product_Qty) AS SalesInCount
	,SUM(tblSalOutD.Product_Qty * tblProducts.Price) AS SalesInMoney
	,tblSalOutH.OL_id
	,olmc.Merch_id
	,tblSalOutH.[Date]
	,Routes.RouteName
INTO #Sales
FROM tblSalOutH
INNER JOIN tblSalOutD ON tblSalOutH.Invoice_id = tblSalOutD.Invoice_id 
INNER JOIN tblProducts ON tblSalOutD.Product_id = tblProducts.Product_id 
INNER JOIN tblProductTypes ON tblProducts.ProductType_id = tblProductTypes.ProductType_id 
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = tblSalOutH.OL_id AND olmc.Merch_ID = 
(
CASE 
	WHEN (NOT EXISTS( -- Проверяем кл-во ТП, завязвнных на ТТ
					SELECT 1 
					FROM #OL_Merch_Chanal olmc1 
					WHERE olmc1.OL_id = olmc.OL_id 
					  AND olmc1.RelatedMerch_CNT = 2 -- Существует несколько ТП для одного ТТ
					)
		  ) 
	THEN -- на ТТ завязан только один ТП.
		 -- Не исплользуем ТП, сделавшего визит, записываем визит на того ТП, который завязан на ТТ через маршуты
		olmc.Merch_ID 
	ELSE -- на ТТ завязано несколько ТП.
		CASE
			WHEN ( -- Проверяем ТП, привязан ли он к ТТ
				  EXISTS(
						SELECT 1 
						FROM #OL_Merch_Chanal olmc2
						WHERE olmc2.OL_id = olmc.OL_id 
							AND olmc2.Merch_Id = tblSalOutH.Merch_Id -- Да, привязан
						)
				 )
			THEN --  
				tblSalOutH.Merch_Id 
			ELSE -- Выбираем ТТ из заказов и ищем его канал и выбираем ТП из Привязаных через маршруты. 
				 -- Выбираем первый попавшийся канад у ТП, который совершил визит
				(SELECT TOP 1 olmc3.Merch_ID
				FROM #OL_Merch_Chanal olmc3
				INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
														  AND matl.Merch_id = tblSalOutH.Merch_id
														  AND olmc3.OL_id = tblSalOutH.OL_id
				)
		END
END
)
INNER JOIN #Routes AS Routes ON Routes.RouteDay = DATEPART(dw, tblSalOutH.[Date])
			AND Routes.Merch_id = olmc.Merch_id
			AND Routes.OL_id = tblSalOutH.OL_id
			AND Routes.ROUTES_CNT = 1
INNER JOIN #merchandisersIDs AS IDs ON IDs.id = olmc.Merch_ID OR @merchandisersCnt = 0
WHERE tblSalOutH.[Date] BETWEEN DATEADD(day, -30, @lowerDate) AND @upperDate
	  AND tblSalOutH.Status <> 9
	  --AND tblProducts.Unit_ID = 3
GROUP BY tblSalOutH.OL_ID
	  , tblSalOutH.[Date]
	  , olmc.Merch_ID
	  , Routes.RouteName

-- Заполняем таблицу данными о долгах за заданный период +30 дней назад.
SELECT tblArchivedDebts.OL_id
	,tblArchivedDebts.DebtDate AS [Date]
	,SUM(CASE WHEN tblProducts.Price = 0 THEN 0 ELSE tblArchivedDebtsDetails.Debt / tblProducts.Price END) AS DebtInCount
	,SUM(tblArchivedDebtsDetails.Debt) AS DebtInMoney
	,tblDebtTypes.DebtType_id
INTO #Debts
FROM tblArchivedDebts
INNER JOIN tblArchivedDebtsDetails ON tblArchivedDebts.Archive_id = tblArchivedDebtsDetails.Archive_id
INNER JOIN tblDebtTypes ON tblDebtTypes.DebtType_id = tblArchivedDebtsDetails.DebtType_id
INNER JOIN #kegDebts kegs ON kegs.ExternalCode = tblDebtTypes.ExternalCode
INNER JOIN tblProducts ON kegs.Product_id = tblProducts.Product_id
WHERE tblArchivedDebts.DebtDate BETWEEN DATEADD(day, -30, @lowerDate) AND @upperDate
	  AND tblArchivedDebtsDetails.Debt <> 0
GROUP BY
	 tblArchivedDebts.OL_id
	,tblArchivedDebts.DebtDate
	,tblDebtTypes.DebtType_id

-- Результирующая выборка
SELECT
	  	 VisitDates.[Date]
		,Sales.RouteName
		,Sales.SalesInCount
		,Sales.SalesInMoney
		,Debts.DebtInCount
		,Debts.DebtInMoney
		,StockturnSales.SaleAmount
		,StockturnDebts.DebtAmount
		,tblOutlets.OL_id
		,tblOutlets.OL_Code
		,tblOutletSubTypes.OLSubTypeName
		,tblOutletTypes.OLtype_name
		,tblOutlets.OLName
		,tblOutlets.OLDeliveryAddress
		,tblOutlets.OLTradingName
		,tblOutlets.OLAddress
		,ols.LValue as OutletsSatus
		,tblSyncStatus.MerchName
		,tblSupervisors.Supervisor_name
		,tblDebtTypes.DebtType_Name
FROM #VisitDates AS VisitDates
INNER JOIN
(
	SELECT SUM(Sales.SalesInCount) AS SaleAmount
		, VisitDates.[Date]
		, Sales.OL_id
		, Sales.Merch_id
	FROM #VisitDates VisitDates
	INNER JOIN #Sales AS Sales ON Sales.[Date] BETWEEN DATEADD(day, -30, VisitDates.[Date]) AND VisitDates.[Date]
	GROUP BY VisitDates.[Date]
		, Sales.OL_id
		, Sales.Merch_id
) AS StockturnSales ON StockturnSales.[Date] = VisitDates.[Date]
INNER JOIN
(
	SELECT SUM(Debts.DebtInCount) AS DebtAmount
		, VisitDates.[Date]
		, Debts.DebtType_id
		, Debts.OL_id
	FROM #VisitDates VisitDates
	INNER JOIN #Debts AS Debts ON Debts.[Date] BETWEEN DATEADD(day, -30, VisitDates.[Date]) AND VisitDates.[Date]
	GROUP BY VisitDates.[Date]
		, Debts.OL_id
		, Debts.DebtType_id		
) AS StockturnDebts ON StockturnDebts.[Date] = VisitDates.[Date]
		AND StockturnDebts.OL_id = StockturnSales.OL_id

INNER JOIN tblOutlets ON tblOutlets.OL_id = StockturnSales.OL_id
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = StockturnSales.Merch_ID
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id 
		AND tblSupervisors.Status <> 9
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID

INNER JOIN tblDebtTypes ON tblDebtTypes.DebtType_id = StockturnDebts.DebtType_id

LEFT JOIN #Sales AS Sales ON Sales.[Date] = VisitDates.[Date]
		AND Sales.OL_id = StockturnSales.OL_id
		AND Sales.Merch_id = StockturnSales.Merch_ID
LEFT JOIN #Debts AS Debts ON Debts.[Date] = VisitDates.[Date]
		AND Debts.OL_id = StockturnDebts.OL_id
		AND Debts.DebtType_id = StockturnDebts.DebtType_id


INNER JOIN tblGloballookup pFactor ON (pFactor.LKey = 
								CASE WHEN tblOutlets.Proximity IS NULL 
								THEN -3 
								ELSE tblOutlets.Proximity
								END)
				AND pFactor.TableName = 'tblOutlets' 
				AND pFactor.FieldName = 'Proximity'
INNER JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status 
				AND ols.TableName = 'tblOutlets'
				AND ols.FieldName = 'Status'
WHERE StockturnSales.SaleAmount <> 0  
	  AND StockturnDebts.DebtAmount <> 0
ORDER BY VisitDates.[Date]

DROP TABLE #OL_Merch_Chanal
DROP TABLE #kegDebts
DROP TABLE #merchandisersIDs
DROP TABLE #Sales
DROP TABLE #Debts
DROP TABLE #VisitDates
DROP TABLE #Routes
";

            }
        }
    }
}
