﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.Common
{
    public enum SQLQueryType
    {
        DataSet,
        Scalar,
        NonQuery
    }
}
