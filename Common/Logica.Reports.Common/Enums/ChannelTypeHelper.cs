namespace Logica.Reports.Common.Enums {
    public static class ChannelTypeHelper {
        public static string GetChannelName(ChannelType channelType) {
            string lResult = "";
            switch (channelType) {
                case ChannelType.Unknown:
                    lResult = "";
                    break;
                case ChannelType.OnTrade:
                    lResult = "On-Trade";
                    break;
                case ChannelType.OffTrade:
                    lResult = "Off-Trade";
                    break;
                case ChannelType.KaTrade:
                    lResult = "KA-Trade";
                    break;
                case ChannelType.OnOffTrade:
                    lResult = @"On/Off-trade";
                    break;
            }
            return lResult;
        }
    }
}