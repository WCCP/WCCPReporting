﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.Common.Enums
{
    /// <summary>
    /// Defines contract's status.
    /// </summary>
    public enum ContractStatus
    {
        Unknown = 0, //
        New = 1, // новый
        ClientAgreementRequired = 2, // на согласовании с клиентом
        InitialApproveRequired = 3, // на утверждении1 (M3,M4,M5)
        FinalApproveRequired = 4, // на утверждении2 (M5)
        SignRequired = 5, // на подписании
        InWork = 6, // в работе
        Completed = 7, // завершен
        DeclinedInBev = 8, // отклонен
        DeclinedClient = 9, // отклонен
        TerminatedInBev = 10, // расторгнут
        TerminatedClient = 11, // расторгнут
    }

    /// <summary>
    /// Defines contract's channel type.
    /// </summary>
    public enum ChannelType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        OnTrade = 1,
        /// <summary>
        /// 
        /// </summary>
        OffTrade = 2,
        /// <summary>
        /// 
        /// </summary>
        KaTrade = 3,

        OnOffTrade = 4
    }
}
