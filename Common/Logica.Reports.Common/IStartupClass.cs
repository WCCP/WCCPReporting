﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Logica.Reports.Common
{
    public interface IStartupClass
    {
        string GetVersion();
        int ShowUI(int isSkined, string reportCaption);
        void CloseUI();
    }
}
