namespace Logica.Reports.Common.WaitWindow
{
    partial class Wait
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Wait));
            this.pnClient = new System.Windows.Forms.Panel();
            this.lbElapsedTimeValue = new System.Windows.Forms.Label();
            this.lbSpentTime = new System.Windows.Forms.Label();
            this.pbWait = new System.Windows.Forms.PictureBox();
            this.lbMessage = new System.Windows.Forms.Label();
            this.pnClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWait)).BeginInit();
            this.SuspendLayout();
            // 
            // pnClient
            // 
            this.pnClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.pnClient.Controls.Add(this.lbElapsedTimeValue);
            this.pnClient.Controls.Add(this.lbSpentTime);
            this.pnClient.Controls.Add(this.pbWait);
            this.pnClient.Controls.Add(this.lbMessage);
            this.pnClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnClient.Location = new System.Drawing.Point(0, 0);
            this.pnClient.Name = "pnClient";
            this.pnClient.Size = new System.Drawing.Size(345, 59);
            this.pnClient.TabIndex = 0;
            // 
            // lbElapsedTimeValue
            // 
            this.lbElapsedTimeValue.Location = new System.Drawing.Point(292, 40);
            this.lbElapsedTimeValue.Name = "lbElapsedTimeValue";
            this.lbElapsedTimeValue.Size = new System.Drawing.Size(48, 13);
            this.lbElapsedTimeValue.TabIndex = 3;
            this.lbElapsedTimeValue.Text = "00:00:00";
            this.lbElapsedTimeValue.Visible = false;
            // 
            // lbSpentTime
            // 
            this.lbSpentTime.Location = new System.Drawing.Point(240, 40);
            this.lbSpentTime.Name = "lbSpentTime";
            this.lbSpentTime.Size = new System.Drawing.Size(48, 13);
            this.lbSpentTime.TabIndex = 2;
            this.lbSpentTime.Text = "������:";
            this.lbSpentTime.Visible = false;
            // 
            // pbWait
            // 
            this.pbWait.Image = ((System.Drawing.Image)(resources.GetObject("pbWait.Image")));
            this.pbWait.Location = new System.Drawing.Point(4, 17);
            this.pbWait.Name = "pbWait";
            this.pbWait.Size = new System.Drawing.Size(24, 24);
            this.pbWait.TabIndex = 1;
            this.pbWait.TabStop = false;
            // 
            // lbMessage
            // 
            this.lbMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.lbMessage.Location = new System.Drawing.Point(37, 18);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(308, 16);
            this.lbMessage.TabIndex = 0;
            this.lbMessage.Text = "���������, ����������.  ���� ��������� ������...";
            // 
            // Wait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 59);
            this.Controls.Add(this.pnClient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Wait";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������";
            this.pnClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWait)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnClient;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.PictureBox pbWait;
        private System.Windows.Forms.Label lbElapsedTimeValue;
        private System.Windows.Forms.Label lbSpentTime;
    }
}