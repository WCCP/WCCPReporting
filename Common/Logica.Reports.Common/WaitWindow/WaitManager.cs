﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Logica.Reports.Common.WaitWindow
{
    public static class WaitManager
    {
        static Wait wait = null;
        static public void StartWait()
        {
            if (wait == null)
            {
                wait = new Wait();
                wait.Show();
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
            };

        }
        static public void StopWait()
        {
            if (wait != null)
            {
                wait.Close();
                wait.Dispose();
                wait = null;
                Cursor.Current = Cursors.Default;
                Application.DoEvents();
            };

        }
    }
}
