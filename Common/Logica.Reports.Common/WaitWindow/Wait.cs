using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Logica.Reports.Common.WaitWindow
{
    public partial class Wait : Form
    {
        public DateTime startTime = DateTime.Now;
        public Timer timerWait = new Timer();

        public Wait()
        {
            InitializeComponent();
            
            lbMessage.Text = Resource.WaitMessage;
            lbSpentTime.Text = Resource.Gone;
            
            timerWait.Interval = 1000;
            timerWait.Tick += new EventHandler(timerWait_Tick);
            System.Threading.Thread t = new System.Threading.Thread(timerWait.Start);
            t.Start();
            
        }
        private void timerWait_Tick(object sender, EventArgs e)
        {
            lbElapsedTimeValue.Text = (DateTime.Now - startTime).ToString();
        }
    }

}