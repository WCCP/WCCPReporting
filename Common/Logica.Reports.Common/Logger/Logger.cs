#define TRACE

using System;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Logica.Reports.Common
{
	public sealed class Logger : IDisposable
	{
		#region Constants
		const string SWITCH_NAME = "WCCP";
		const string SWITCH_DESCRIPTION = "Trace switch for WCCP DB installer utility";
		const string LOG_FILE_NAME = "WCCP_LDB_Install{0}.log";
		#endregion

		#region Fields
		private static Logger _instance = new Logger();
		private TraceSwitch _trace;
		private TextWriterTraceListener _traceWriter;
		string _lastError;
		private bool _disposed = false;

		#endregion

		#region Constructors
		private Logger()
		{
			_trace = new TraceSwitch(SWITCH_NAME, SWITCH_DESCRIPTION);
			_trace.Level = TraceLevel.Verbose;
			string startupPath = Application.StartupPath;

			string logFile = Path.Combine(Application.StartupPath, string.Format(LOG_FILE_NAME, ""));
			int num = 1;
			while (File.Exists(logFile))
			{
				logFile = Path.Combine(Application.StartupPath, string.Format(LOG_FILE_NAME, num++));
			}
			StreamWriter swLogFile = new System.IO.StreamWriter(logFile, false, System.Text.Encoding.Unicode);

			_traceWriter = new TextWriterTraceListener(swLogFile);
			System.Diagnostics.Trace.Listeners.Add(_traceWriter);
			System.Diagnostics.Trace.AutoFlush = true;
		}
		#endregion

		#region Properties
		public static Logger Instance
		{
			get
			{
				return _instance;
			}
		}

		public string LastError
		{
			get
			{
				return _lastError;
			}
		}

		public TraceLevel TraceLevel
		{
			get
			{
				return _trace.Level;
			}
		}
		#endregion

		#region Public Methods

		public void Clear()
		{
			if (DeleteLogFile())
			{
				_trace = new TraceSwitch(SWITCH_NAME, SWITCH_DESCRIPTION);
			}
		}

		public void LogError(Exception e)
		{
			StringBuilder errMsg = new StringBuilder();
			errMsg.AppendFormat("{0}.", GetInnerExceptionMessages(e));

			//if (_trace.TraceVerbose && null != e.InnerException && !String.IsNullOrEmpty(e.InnerException.StackTrace))
			//{
			//    errMsg.AppendFormat(" {0}Stack trace: {1}", Environment.NewLine, e.InnerException.StackTrace);
			//}

			LogError(errMsg.ToString());
			errMsg.Length = 0;
		}

		public void LogError(string error)
		{
			Trace.WriteLineIf(_trace.TraceWarning, PrepareLogEntry(TraceEventType.Error, error));
			WriteConsoleMessage(error, _trace.TraceError);
		}

		public void LogWarning(string warning)
		{
			Trace.WriteLineIf(_trace.TraceWarning, PrepareLogEntry(TraceEventType.Warning, warning));
			WriteConsoleMessage(warning, _trace.TraceWarning);
		}

		public void LogInfo(string information)
		{
			Trace.WriteLineIf(_trace.TraceInfo, PrepareLogEntry(TraceEventType.Information, information));
			WriteConsoleMessage(information, _trace.TraceInfo);
		}

		public void LogVerbose(string message)
		{
			Trace.WriteLineIf(_trace.TraceVerbose, PrepareLogEntry(TraceEventType.Verbose, message));
			WriteConsoleMessage(message, _trace.TraceVerbose);
		}

		public void LogMessage(TraceEventType eventType, string message)
		{
			Trace.Write(PrepareLogEntry(eventType, message));
			WriteConsoleMessage(message, true);
		}

		public void LogStartActivity(string activityName, bool isVerbose)
		{
			const string EVT_MESSAGE = "{0} started ...";

			if (isVerbose)
			{
				LogVerbose(String.Empty);
				LogVerbose(String.Format(EVT_MESSAGE, activityName));
			}
			else
			{
				LogInfo(String.Empty);
				LogInfo(String.Format(EVT_MESSAGE, activityName));
			}
		}

		public void LogFinishActivity(string activityName, bool isVerbose)
		{
			const string EVT_MESSAGE = "{0} finished";

			if (isVerbose)
			{
				LogVerbose(String.Format(EVT_MESSAGE, activityName));
				LogVerbose(String.Empty);
			}
			else
			{
				LogInfo(String.Format(EVT_MESSAGE, activityName));
				LogInfo(String.Empty);
			}
		}

		#endregion

		#region Helpers

		private static bool DeleteLogFile()
		{
			bool result = false;

			foreach (TraceListener listener in Trace.Listeners)
			{
				TextWriterTraceListener textListener = listener as TextWriterTraceListener;
				if (null != textListener)
				{
					StreamWriter streamWriter = textListener.Writer as StreamWriter;
					if (null != streamWriter)
					{
						FileStream fileStream = streamWriter.BaseStream as FileStream;
						if (null != fileStream)
						{
							string fileName = fileStream.Name;
							if (!String.IsNullOrEmpty(fileName) && File.Exists(fileName))
							{
								try
								{
									textListener.Dispose();
									File.Delete(fileName);
									result = true;
								}
								catch (Exception e)
								{
									try
									{
										_instance.LogError(e);
									}
									catch { }
								}
							}
						}
					}
				}
			}

			return result;
		}

		private string PrepareLogEntry(TraceEventType type, string entry)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendFormat("{0} ", DateTime.Now.ToString("dd-MM-yy HH:mm:ss:fff"));
			builder.AppendFormat("{0}: ", type);
			builder.Append(entry);

			_lastError = builder.ToString();

			return _lastError;
		}

		private static string GetInnerExceptionMessages(Exception e)
		{
			StringBuilder errorMessage = new StringBuilder(e.Message);

			for (Exception innerException = e.InnerException;
				innerException != null;
				innerException = innerException.InnerException)
			{
				string innerMessage = innerException.Message;

				if (!String.IsNullOrEmpty(innerMessage))
				{
					errorMessage.AppendFormat(" <-- {0}", innerMessage);
				}
			}

			return errorMessage.ToString();
		}

		private void WriteConsoleMessage(string message, bool shouldWrite)
		{
			if (shouldWrite)
				Console.WriteLine(message);
		}
		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
		}

        protected void Dispose(bool disposing)
		{
			if (!this._disposed)
			{
				if (disposing && _traceWriter != null)
					_traceWriter.Dispose();
			}
			this._disposed = true;
		}
		~Logger()
		{
			Dispose(false);
		}

		#endregion
	}
}
