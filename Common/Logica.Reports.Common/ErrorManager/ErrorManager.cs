﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Resources;
using DevExpress.XtraEditors;

namespace Logica.Reports.Common
{
    public class ErrorManager
    {
        public static void ShowWarningBox(string message)
        {
            XtraMessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void ShowErrorBox(string message)
        {
            XtraMessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowExtendedMessage(Exception ex, string additionalComment)
        {
            using (ErrorForm form = new ErrorForm(ex, additionalComment))
            {
                form.ShowDialog();
            }
        }
    }
}
