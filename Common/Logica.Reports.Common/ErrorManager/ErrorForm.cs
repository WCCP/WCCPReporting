﻿using DevExpress.XtraEditors;
using System;

namespace Logica.Reports.Common
{
    partial class ErrorForm : XtraForm
    {
        public ErrorForm()
        {
            InitializeComponent();
        }

        public ErrorForm(Exception e, string comment = null)
            : this()
        {
            lMessage.Text = !string.IsNullOrEmpty(comment) ? comment + Environment.NewLine : string.Empty;

            lMessage.Text += e.Message;
            tDescription.Text = e.StackTrace;
        }
    }
}
