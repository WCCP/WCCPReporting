﻿namespace Logica.Reports.Common
{
    partial class ErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lText = new System.Windows.Forms.Label();
            this.lMessage = new System.Windows.Forms.Label();
            this.lDetails = new System.Windows.Forms.Label();
            this.tDescription = new DevExpress.XtraEditors.MemoEdit();
            this.okButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tDescription.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.lText, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.lMessage, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.lDetails, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.tDescription, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.okButton, 0, 4);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 5;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.433962F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.75472F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.169811F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.9434F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(417, 265);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // lText
            // 
            this.lText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lText.Location = new System.Drawing.Point(6, 8);
            this.lText.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.lText.MaximumSize = new System.Drawing.Size(0, 17);
            this.lText.Name = "lText";
            this.lText.Size = new System.Drawing.Size(408, 17);
            this.lText.TabIndex = 0;
            this.lText.Text = "В работе приложения произошла ошибка:";
            // 
            // lMessage
            // 
            this.lMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lMessage.Location = new System.Drawing.Point(6, 25);
            this.lMessage.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.lMessage.MaximumSize = new System.Drawing.Size(0, 50);
            this.lMessage.Name = "lMessage";
            this.lMessage.Size = new System.Drawing.Size(408, 50);
            this.lMessage.TabIndex = 21;
            this.lMessage.Text = "Error Message";
            // 
            // lDetails
            // 
            this.lDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lDetails.Location = new System.Drawing.Point(6, 80);
            this.lDetails.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.lDetails.MaximumSize = new System.Drawing.Size(0, 17);
            this.lDetails.Name = "lDetails";
            this.lDetails.Size = new System.Drawing.Size(408, 17);
            this.lDetails.TabIndex = 22;
            this.lDetails.Text = "Детали:";
            this.lDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tDescription
            // 
            this.tDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tDescription.EditValue = "Description";
            this.tDescription.Location = new System.Drawing.Point(6, 102);
            this.tDescription.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.tDescription.Name = "tDescription";
            this.tDescription.Properties.ReadOnly = true;
            this.tDescription.Size = new System.Drawing.Size(408, 129);
            this.tDescription.TabIndex = 23;
            this.tDescription.TabStop = false;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(339, 239);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "&OK";
            // 
            // ErrorForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 283);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorForm";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ошибка в работе приложения";
            this.tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tDescription.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label lText;
        private System.Windows.Forms.Label lMessage;
        private System.Windows.Forms.Label lDetails;
        private DevExpress.XtraEditors.MemoEdit tDescription;
        private System.Windows.Forms.Button okButton;
    }
}
