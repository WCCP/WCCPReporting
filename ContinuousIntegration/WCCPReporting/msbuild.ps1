﻿clear
#Set-Location "C:\Adron\CruiseControl\WCCPReporting\WorkDir\"
#exit 0


$csvFile = "msbuild.csv"
$revFile = "cfgRevisions.csv"
$conString = "Data Source=CHERRY;Initial Catalog=SalesWorks;Integrated Security=True"
$sqlSelectObjects = "SELECT ObjectLibrary, ObjectVersion, ObjectConfig FROM DW_URM_Object WHERE ObjectLibrary IN ({0})"
$sqlUpdateObject = "UPDATE DW_URM_Object SET ObjectVersion = {0}, ObjectData = {1}, ObjectConfig = {2} WHERE ObjectLibrary = '{3}'"
$sqlScriptObjectSP = "spdw_scriptURMObject"
$outPath = "ScriptedDll\"


trap [Exception]{
	Write-Output $($_.Exception.Message)
	exit 1
}

function  Get-DB-Modules {
	param([System.Data.SqlClient.SqlConnection] $connection);
	$command = $connection.CreateCommand();
	$dllList = Import-Csv $csvFile -Header "dll","cfg" | 
		ForEach-Object -Begin {
			$str = New-Object -TypeName System.Text.StringBuilder
		} -Process { 
			$tmp = $str.AppendFormat(",'{0}'", $([System.IO.Path]::GetFileName($_.dll)))
		} -End {
			$str.ToString(1, $str.Length - 1)
		}
	$command.CommandText = $sqlSelectObjects -f $dllList;
	Write-Host $command.CommandText
	$da = New-Object -TypeName System.Data.SqlClient.SqlDataAdapter -ArgumentList $command;
	$ds = New-Object -TypeName System.Data.DataSet;
	$tmp = $da.Fill($ds);

	return $ds.Tables[0];
}

function Get-File-Version {
	param ([string] $dll)
	$dllFile = [IO.Path]::GetFullPath($dll);
	Write-Host ("LoadFile '{0}'" -f $dllFile)
	$assemblyVersion = ([System.Diagnostics.FileVersionInfo]::GetVersionInfo($dllFile)).FileVersion;
	#$assemblyVersion = ([Reflection.Assembly]::LoadFile($dllFile)).GetName().Version.ToString();
	return $assemblyVersion
}

function Get-Csv-Item {
	param ([string] $dll)
	return Import-Csv $csvFile -Header "dll","cfg" | Where-Object {[System.IO.Path]::GetFileName($_.dll).ToUpper().Equals($dll.ToUpper())}
}

function Get-Update-Sql {
	param ($data)
	if ($data.IsUpdateDll)
	{
		$sql1 =  $data.VerParam
		$sql2 =  $data.DllParam
	}
	else
	{
		$sql1 =  "ObjectVersion"
		$sql2 =  "ObjectData"
	}
	if ($data.IsUpdateCfg)
	{
		$sql3 =  $data.CfgParam
	}
	else
	{
		$sql3 =  "ObjectConfig"		
	}	
	$sql = $sqlUpdateObject -f $sql1,$sql2,$sql3,$data.moduleName
	return $sql
}

function Get-LastModifiedRevision {
	param([string] $file)
	$f = [System.IO.Path]::GetFullPath($file);
	$f = [System.IO.Path]::GetDirectoryName($file)
	Push-Location
	Set-Location $f
	$result = [xml] (& svn info --xml) | ForEach-Object { $_.info.entry.commit.revision }
	Pop-Location

	return $result
}

function Get-LastUploadedRevision {
	param ([string] $file)
	if ([System.IO.File]::Exists($revFile))
	{ 
		$sr = Import-Csv -Path $revFile | Where-Object {$_.cfg -like $file} | ForEach-Object { [int] $_.rev }
		if ($sr -eq $null)
			{ return -1 }
		else
			{ return $sr }
		return -1 
	}
	else
	{
		return -1 
	}
}

function Update-ConfigRevisionsFile {
	Import-Csv $csvFile -Header "dll","cfg"	| 
		Where-Object {-not [string]::IsNullOrEmpty($_.cfg) -and -not [string]::IsNullOrEmpty($_.cfg.Trim())} |
		ForEach-Object { New-Object -TypeName PSObject -Property @{cfg = $_.cfg; rev = $(Get-LastModifiedRevision $_.cfg)} } |
		Select-Object cfg,rev |
		Export-Csv -Path $revFile -Encoding UTF8 -NoTypeInformation
}

$connection = New-Object -TypeName System.Data.SqlClient.SqlConnection -ArgumentList $conString
$connection.Open();

$cmd = Get-DB-Modules $connection | 
	ForEach-Object { 
		Write-Host $("Processing item: {0}" -f $_.ObjectLibrary)
		$csvItem = Get-Csv-Item $_.ObjectLibrary; 
		$dllVersion = Get-File-Version $csvItem.dll
		$isUpdateDll = $_.ObjectVersion -ne $dllVersion
		if ($isUpdateDll)
		{
			Write-Host ("dll check: db v.{0}, compiled v.{1}" -f $_.ObjectVersion,$dllVersion)
		}
		$isCfgExists = -not ($csvItem.cfg -eq $null -or [String]::IsNullOrEmpty($csvItem.cfg.Trim()));
		if ($isCfgExists)
		{
			$backupFile = $csvItem.cfg.Replace(".", $("_{0}." -f [DateTime]::Now.ToString("yyyyMMddHHmmssffffff")))
			Set-Content -LiteralPath $backupFile $_.ObjectConfig
			$currentConfigData = $([System.IO.File]::ReadAllText($csvItem.cfg));
			$lastCfgUploadedRevision = Get-LastUploadedRevision $csvItem.cfg;
			$lastCfgModifiedRevision = Get-LastModifiedRevision $csvItem.cfg;
			$isUpdateCfg = -not $_.ObjectConfig.Equals($currentConfigData) -or $lastCfgUploadedRevision -ne $lastCfgModifiedRevision
			Write-Host ("config check: data {0}, file rev.{1}, DB rev.{2}" -f (-not $_.ObjectConfig.Equals($currentConfigData)),$lastCfgModifiedRevision,$lastCfgUploadedRevision)
		} else {
			$isUpdateCfg = $false;
		}
		@{ Dll = $csvItem.dll; DllVer = $dllVersion; Cfg = $csvItem.cfg; IsUpdateDll = $isUpdateDll; IsUpdateCfg = $isUpdateCfg }
	} | 
	Where-Object {
		$_.IsUpdateDll -or $_.IsUpdateCfg
	} | 
	ForEach-Object {
		$paramPrefix = [System.IO.Path]::GetFileNameWithoutExtension($_.Dll).Replace(".", "")
		if  ([String]::IsNullOrEmpty($_.Cfg)) {
			$Cfg = "";
		} else {
			$Cfg = [System.IO.File]::ReadAllText($_.Cfg);
		}
		@{
			moduleName = [System.IO.Path]::GetFileName($_.Dll);
			Dll = [System.IO.File]::ReadAllBytes($_.Dll);
			DllVer = $_.DllVer;
			Cfg = $Cfg;
			IsUpdateDll = $isUpdateDll;
			IsUpdateCfg = $isUpdateCfg;
			VerParam =  "@" + $paramPrefix + "Ver";
			DllParam =  "@" + $paramPrefix + "Dll";
			CfgParam =  "@" + $paramPrefix + "Cfg"
		}
	} | 
	ForEach-Object -Begin {
		$sb = New-Object -TypeName System.Text.StringBuilder
		$updatedModules = @();
		$command = $connection.CreateCommand();
	} -Process {
		$tmp = $sb.AppendFormat("{0}; ", $(Get-Update-Sql $_))
		if ($_.IsUpdateDll)
		{
			$tmp = $command.Parameters.Add($_.VerParam, $_.DllVer)
			$tmp = $command.Parameters.Add($_.DllParam, $_.Dll)
			Write-Host $("Update dll: {0}" -f $_.moduleName)
		}
		if ($_.IsUpdateCfg)
		{
			$tmp = $command.Parameters.Add($_.CfgParam, $_.Cfg)
			Write-Host $("Update config: {0}" -f $_.moduleName)
		}
		if ($_.IsUpdateCfg -or $_.IsUpdateDll)
		{
			$updatedModules += $_.moduleName;
		}
		
	} -End { 
		$command.CommandText = $sb.ToString(); 
		$command 
	}

if (-not [string]::IsNullOrEmpty($cmd.CommandText))
{
	Write-Host "Start update execution";
	Write-Host $("query: {0}" -f $cmd.CommandText);
	$cmd.ExecuteNonQuery()
	Write-Host "End update execution"

	#Script object data to file
	$updatedModules | 
	ForEach-Object -Begin {
		$sb = New-Object -TypeName System.Text.StringBuilder;
		$connection.add_InfoMessage({foreach ($err in $_.Errors){$sb.AppendLine($err.Message)}});
	} -Process {
		$sb = New-Object -TypeName System.Text.StringBuilder;
		$command = $connection.CreateCommand();
		$command.CommandType = [System.Data.CommandType]::StoredProcedure;
		$command.CommandText = $sqlScriptObjectSP;
		$tmp = $command.Parameters.AddWithValue("ObjectLibrary", $_);
		$tmp = $command.ExecuteScalar();
		$outputFile = [String]::Format("{0}_update.sql", $([System.IO.Path]::GetFileNameWithoutExtension($_)));
		$outputFile = [System.IO.Path]::Combine($outPath, $outputFile);
	    #Force UTF-8 w/o BOM
		$enc = New-Object -TypeName System.Text.UTF8Encoding -ArgumentList $false
		[System.IO.File]::WriteAllText($outputFile, $sb.ToString(), $enc); 
		Write-Host $("Created update script for {0}" -f $_)
	}
	
	Update-ConfigRevisionsFile
} else {
	Write-Host "Nothing to update";
}

$connection.Close();

exit 0
