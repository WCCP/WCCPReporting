﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OsmSharp.Osm;
using OsmSharp.Osm.Data.SQLServer.SimpleSchema;
using OsmSharp.Osm.Data.SQLServer.SimpleSchema.Processor;
using OsmSharp.Routing;
using OsmSharp.Routing.Osm.Interpreter;
using OsmSharp.Routing.Osm.Data;
using OsmSharp.Routing.Graph.DynamicGraph.PreProcessed;
using OsmSharp.Routing.Graph.Router.Dykstra;
using OsmSharp.Routing.Graph.DynamicGraph.SimpleWeighed;
using OsmSharp.Routing.Graph.Memory;
using OsmSharp.Routing.Route;
using OsmSharp.Tools.Math.Geo;
using OsmSharp.Osm.Data.XML.Processor;
using OsmSharp.Routing.Osm.Data.Processing;
//using OsmSharp.Routing.Osm.Data.Source;
using OsmSharp.Osm.Data.Processor.Filter.Sort;

using OsmSharp.Routing.TSP;
using OsmSharp.Routing.TSP.Genetic;

using OsmSharp.Tools.Xml.Gpx;
using OsmSharp.Tools.Xml.Sources;
using OsmSharp.Tools.Xml.Gpx.v1_1;
using RouteLib;
using WcfRouterCommon;

using OsmSharp.Routing.Graph.Serialization.v1;
using OsmSharp.Routing.Graph.Router;

namespace RouteLibTest
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TwoPointsRouteTest()
        {
            //string xml = @"C:\Works\OSMSharp\src\test1.osm";
            string xml = @"C:\Works\OSMSharp\ukr4.osm";

            // keeps a memory-efficient version of the osm-tags.
            OsmTagsIndex tags_index = new OsmTagsIndex();

            // creates a routing interpreter. (used to translate osm-tags into a routable network)
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();

            // create a routing datasource, keeps all processed osm routing data.
            MemoryRouterDataSource<SimpleWeighedEdge> osm_data = new MemoryRouterDataSource<SimpleWeighedEdge>(tags_index);

            // load data into this routing datasource.
            Stream osm_xml_data = new FileInfo(xml).OpenRead();

            using (osm_xml_data)
            {
                SimpleWeighedDataGraphProcessingTarget target_data = new SimpleWeighedDataGraphProcessingTarget(osm_data, interpreter, osm_data.TagsIndex, VehicleEnum.Car);
                XmlDataProcessorSource data_processor_source = new XmlDataProcessorSource(osm_xml_data);

                // pre-process the data.
                DataProcessorFilterSort sorter = new DataProcessorFilterSort();
                sorter.RegisterSource(data_processor_source);
                target_data.RegisterSource(sorter);
                target_data.Pull();
            }

            // create the router object.
            IRouter<RouterPoint> router = new Router<SimpleWeighedEdge>(osm_data, interpreter, new DykstraRoutingLive(osm_data.TagsIndex));

            //RouterPoint point1 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3069740290806, 34.8900858359377));
            //RouterPoint point2 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.4703750610352, 34.9571762084961));
            RouterPoint point1 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3061676768026, 34.892941331166));
            RouterPoint point2 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3087971442136, 34.8701373971564));

            OsmSharpRoute route;
            route = router.Calculate(VehicleEnum.Car, point1, point2);
            double dist = route.TotalDistance;
            /*
            // calculate all travel times between each of these points.
            double[][] times = router.CalculateManyToManyWeight(VehicleEnum.Car, points, points);

            // solve the TSP.
            RouterTSP tsp_solver = new RouterTSPAEXGenetic(); // use a genetic algorithm to solve the TSP. 
            OsmSharp.Routing.Core.TSP.RouterTSPWrapper<RouterPoint, RouterTSP> tsp_router =
                new RouterTSPWrapper<RouterPoint, RouterTSP>(tsp_solver, router);
            OsmSharpRoute route = tsp_router.CalculateTSP(VehicleEnum.Car, points, true);
             */
            /*
            // save this route to gpx for example.
            route.SaveAsGpx(new FileInfo(@"C:\Works\OSMSharp\src\test1.gpx"));
            */
        }

        [TestMethod]
        public void TwoPointsSQLRouteTest()
        {
            //SQLServerSimpleSchemaSource source = new SQLServerSimpleSchemaSource(@"Server=80.91.162.90;User Id=osmtest;Password=osmtest;Database=GeoData;");
            SQLServerSimpleSchemaSource source = new SQLServerSimpleSchemaSource(@"Server=sv109\sqlexpress;Trusted_Connection=true;Database=GeoData;");

            OsmTagsIndex tags_index = new OsmTagsIndex();

            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();

            OsmSourceRouterDataSource routing_data = new OsmSourceRouterDataSource(interpreter, tags_index, source, VehicleEnum.Car);

            IRouter<RouterPoint> router = new Router<PreProcessedEdge>(routing_data, interpreter, new DykstraRoutingPreProcessed(routing_data.TagsIndex));

            OsmSharpRoute route;
            //RouterPoint point1 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3069740290806, 34.8900858359377));
            //RouterPoint point2 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.4703750610352, 34.9571762084961));

            RouterPoint point1 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3061676768026, 34.892941331166));
            RouterPoint point2 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3087971442136, 34.8701373971564));
            route = router.Calculate(VehicleEnum.Car, point1, point2);
            /*
            route = router.Calculate(VehicleEnum.Car,
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3151345115244, 34.922336457966)),
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3069180651738, 34.892870938841)));

            route = router.Calculate(VehicleEnum.Car,
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3069180651738, 34.892870938841)),
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3061676768026, 34.892941331166)));

            route = router.Calculate(VehicleEnum.Car,
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3061676768026, 34.892941331166)),
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3082939615145, 34.870571949628)));

            route = router.Calculate(VehicleEnum.Car,
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3082939615145, 34.870571949628)),
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3150034243338,34.8784106812928)));

            route = router.Calculate(VehicleEnum.Car,
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3150034243338, 34.8784106812928)),
                router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3092549484347,34.8894929841615)));
            */
        }

        [TestMethod]
        public void SQLRouteTest()
        {
            SQLServerSimpleSchemaSource source = new SQLServerSimpleSchemaSource(@"Server=sv109\sqlexpress;Trusted_Connection=true;Database=GeoData;");

            // keeps a memory-efficient version of the osm-tags.
            OsmTagsIndex tags_index = new OsmTagsIndex();

            // creates a routing interpreter. (used to translate osm-tags into a routable network)
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();

            // create routing inter
            OsmSourceRouterDataSource routing_data = new OsmSourceRouterDataSource(interpreter, tags_index, source, VehicleEnum.Car);

            // create the router object.
            IRouter<RouterPoint> router = new Router<PreProcessedEdge>(routing_data, interpreter, new DykstraRoutingPreProcessed(routing_data.TagsIndex));

            // resolve both points; find the closest routable road.
            //RouterPoint point1 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(45.11757397, 33.68344803));
            //RouterPoint point2 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(45.12220588, 33.68988037));
            RouterPoint point1 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3061676768026, 34.892941331166));
            RouterPoint point2 = router.Resolve(VehicleEnum.Car, new GeoCoordinate(50.3087971442136, 34.8701373971564));



            // calculate route.
            OsmSharpRoute route;
            route = router.Calculate(VehicleEnum.Car, point2, point1);

            //route.SaveAsGpx(new FileInfo("route.gpx"));

            //route = router.Calculate(VehicleEnum.Car, point1, point2);

            //route.SaveAsGpx(new FileInfo("route2.gpx"));
        }

        [TestMethod]
        public void BuildMerchRouteTest()
        {
            MerchRouteParams param = new MerchRouteParams { Merch_ID = 11600009, RouteDate = new DateTime(2013, 06, 10), RouteType = 0 };
            RouteResult res =
            RouteLib.RouteHelper.BuildMerchRoute(
                param,
                @"Server=cherry;Trusted_Connection=true;Database=SalesWorks_Alpha;"
                );
        }

        [TestMethod]
        public void BuildRoute()
        {
            RouteParams param = new RouteParams()
            {
                Points = new GeoPoint[11]  
                {
                    new GeoPoint(46.9548983433519,36.6531442653732), 
                    new GeoPoint(46.9546991004302,36.6529611861985), 
                    new GeoPoint(46.9546003222067,36.6530693293271), 
                    new GeoPoint(46.9547049409925,36.6531643238976), 
                    new GeoPoint(46.9546459005608,36.6529123022125), 
                    new GeoPoint(46.9571555947734,36.6553270053731), 
                    new GeoPoint(46.9550453933087,36.6532793857805), 
                    new GeoPoint(47.059406290581,36.602459414268), 
                    new GeoPoint(46.9375784665617,36.6403354269306), 
                    new GeoPoint(47.0576620293726, 36.6034028172816), 
                    new GeoPoint(47.0561180114746, 36.604190826416)
                },
                IsRound = false,
                OptimizeIt = true
            };

            RouteResult res = RouteLib.RouteHelper.BuildRoute(param);

        }

        [TestMethod]
        public void LoadOSMData()
        {
            RouteLib.RouteHelper.LoadOSMData(@"C:\Works\OSMSharp\ukraine.osm", @"C:\Works\OSMSharp\ukraine.oss");
        }


        [TestMethod]
        public void ProcessFactVisits()
        {
            RouteLib.RouteHelper.ProcessFactVisits(new DateTime(2013, 07, 04), @"Server=cherry;Trusted_Connection=true;Database=SalesWorks_Alpha;");
        }

    }
}
