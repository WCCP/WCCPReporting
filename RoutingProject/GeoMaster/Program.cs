﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NDesk.Options;
using System.Reflection;
using System.IO;
using System.ServiceModel.Configuration;
using WcfRouterCommon;
using RouteLib;
using OsmSharp.Tools.Output;
using NLog;
using NLog.Targets;
using NLog.Config;

namespace GeoMaster
{
    class Program
    {
        static int Main(string[] args)
        {
            Logger logger = LogManager.GetLogger("RouteHelper");

            LoggingConfiguration config = LogManager.Configuration;

            ColoredConsoleTarget consoleTarget = new ColoredConsoleTarget();
            consoleTarget.Layout = "${date:format=HH\\:MM\\:ss} ${message}";
            config.AddTarget("console", consoleTarget);
            LoggingRule rule1 = new LoggingRule("*", LogLevel.Trace, consoleTarget);
            config.LoggingRules.Add(rule1);

            LogManager.Configuration = config;

            Console.WriteLine("{0} v.{1}", Assembly.GetExecutingAssembly().GetName().Name, Assembly.GetExecutingAssembly().GetName().Version);

            #region Command-line parsing

            string OSMFile = "";
            string sMerchId = "";
            long Merch_Id = 0;
            string sStartDate = "";
            DateTime StartDate = DateTime.MinValue;
            string route_type = "fact";
            bool showHelp = false;
            bool buildFacts = false;
            string ServerName = "";
            string DatabaseName = "";
            string UserName = "";
            string Password = "";
            bool ua = false;
            bool ru = false;
            bool scope = false;

            var p = new OptionSet() 
            {
   	            { "osm=",           v => OSMFile = v },
   	            { "m=",             v => sMerchId = v },
   	            { "StartDate|sd=",  v => sStartDate = v },
   	            { "src=",           v => route_type = v },
   	            { "srv=",           v => ServerName = v },
   	            { "db=",            v => DatabaseName = v },
   	            { "u=",             v => UserName = v },
   	            { "p=",             v => Password = v },
   	            { "bf",             v => buildFacts = v != null },
   	            { "ua",             v => ua = v != null },
   	            { "ru",             v => ru = v != null },
                { "scope",          v => scope = v!= null},
   	            { "h|?|help",       v => showHelp = v != null },
            };

            List<string> extra;
            try
            {

                extra = p.Parse(args);

                if (sMerchId != "")
                    Merch_Id = long.Parse(sMerchId);

                if (sStartDate != "")
                    StartDate = DateTime.Parse(sStartDate);
            }
            catch (OptionException e)
            {
                Console.Write("Ошибка в аргументах:");
                Console.WriteLine(e.Message);
                Console.WriteLine("Уакжите опцию -help для получения справки.");
                return -1;
            }

            if (extra.Count > 0)
            {
                Console.Write("Ошибка в аргументах, не распознана часть: '{0}'", string.Join(" ", extra.ToArray()));
                return -1;
            }

            if (showHelp || args.Length == 0)
            {
                Console.WriteLine("Для импорта файла OSM:");
                Console.WriteLine("  GeoMaster.exe -gs <SQLServerWhereGeoDBLive> -gd <GeoDbName> -osm <PathToOSMFile>");
                Console.WriteLine();
                Console.WriteLine("Чтобы построить маршрут ТП на заданый день:");
                Console.WriteLine("  GeoMaster.exe -m <Merch_ID> -sd <Date> -src fact -srv ServerName -db DB name -ua|-ru");
                Console.WriteLine("  -src может иметь значение fact (брать маршрут из фактических визитов) или plan (брать маршрут из запланированых визитов), по умолчанию fact");
                Console.WriteLine("  -srv и -db указывают на сервер и базу, в которой искать ТП и маршруты");
                Console.WriteLine("  -ua или -ru указывают на страну, для которой будут строиться маршруты");
                Console.WriteLine();
                Console.WriteLine("Чтобы построить все недостающие фактические маршруты ТП на заданый месяц:");
                Console.WriteLine("  GeoMaster.exe -bf -sd <Date> -srv ServerName -db DB name -ua -ru");
                Console.WriteLine("  -bf означает build facts");
                Console.WriteLine("  -sd <Date> дата задается вместе с днем, но день игнорируется, важен только месяц");
                Console.WriteLine("  -srv и -db указывают на сервер и базу, в которой искать ТП и маршруты");
                Console.WriteLine("  -ua и -ru указывают на страну(-ы), для которой(-ых) будут строиться маршруты");
                Console.WriteLine("  -scope режим в котором маршруты с координатами визитов вычитываются в рамках одного обращения к серверу. Иначе для каждого маршрута в цыкле  отдельно читаются координаты визитов с сервера");

                return -1;
            }

            if (!(route_type == "plan" || route_type == "fact"))
            {
                Console.WriteLine("Параметр src должен быть равен plan или fact, по умолчанию fact");
                return -1;
            }

            if (!(ua || ru))
            {
                Console.WriteLine("Необходимо указать страну(-ы) для постройки маршрутов: -ua, -ru");
                return -1;
            }

            #endregion

            #region Build route by MerchID

            if (Merch_Id > 0 && StartDate > DateTime.MinValue && ServerName.Length > 0 && DatabaseName.Length > 0)
            {
                Console.WriteLine("Строю маршрут для Merch_Id {0} на {1} по {2} визитам", Merch_Id, sStartDate, route_type == "fact" ? "фактическим" : "плановым");

                string connectionString = string.Format("Server={0};Database={1};Trusted_Connection=true;", ServerName, DatabaseName);
                
                try
                {
                    MerchRouteParams param = new MerchRouteParams
                    {
                        Merch_ID = Merch_Id,
                        RouteDate = StartDate,
                        RouteType = (short)(route_type == "fact" ? 0 : 1),
                        IsRu = ru
                    };

                    RouteResult res = RouteLib.RouteHelper.BuildMerchRoute(param, connectionString);

                    if (res.ErrorCode < 0)
                    {
                        Console.WriteLine(res.ErrorMessage);
                    }
                    else
                    {
                        Console.WriteLine(string.Format("{0} ТТ на маршруте", res.SourcePoints.Length));

                        if (res.RejectedPoints.Length > 0)
                        {
                            Console.WriteLine(string.Format("Не привязалось к дорожной сетке - {0}", res.RejectedPoints.Length));
                        }

                        //сохраняем файлы
                        //string path = AppDomain.CurrentDomain.BaseDirectory;
                        //res.SaveSourceRouteGPX(Path.Combine(path, "_initial_route.gpx"));
                        //res.SaveOptimizedRouteGPX(Path.Combine(path, "_optimized_route.gpx"));

                        Console.WriteLine(string.Format("Исходный маршрут:        {0,8:F} м", res.SourceDistance));
                        Console.WriteLine(string.Format("Оптимизированый маршрут: {0,8:F} м", res.OptimizedDistance));

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error in RouteHelper.BuildMerchRoute");
                    Console.WriteLine(ex.Message);
                    return -1;
                }

                return 0;
            }


            #endregion

            #region BuildFacts

            if (buildFacts && StartDate > DateTime.MinValue && ServerName.Length > 0 && DatabaseName.Length > 0)
            {
                string uaName = ua ? "Украины" : "";
                string ruName = ru ? "России" : "";
                string countries = (ua && ru) ? uaName + " и " + ruName : uaName + ruName;
                Console.WriteLine("Строю фактические маршруты за {0} месяц для {1}", sStartDate, countries);

                string connectionString;
                if (UserName.Length > 0)
                    connectionString = string.Format("Server={0};Database={1};User Id={2};Password={3};", ServerName, DatabaseName, UserName, Password);
                else
                    connectionString = string.Format("Server={0};Database={1};Trusted_Connection=true;", ServerName, DatabaseName);

                try
                {
                    RouteHelper.IsInScopeMode = scope;

                    if (ua)
                    {
                        if (DateTime.Now.Day == 1)
                        {
                            RouteHelper.ProcessFactVisits(StartDate.AddDays(-1), connectionString);
                        }
                        RouteHelper.ProcessFactVisits(StartDate, connectionString);
                    }
                    if (ru)
                    {
                        object data = RouteHelper.ReadGeoData();

                        if (DateTime.Now.Day == 1)
                        {
                            RouteHelper.ProcessFactVisitsRussia(data, StartDate.AddDays(-1), connectionString, 1);
                            RouteHelper.ProcessFactVisitsRussia(data, StartDate.AddDays(-1), connectionString, 2);
                        }
                        RouteHelper.ProcessFactVisitsRussia(data, StartDate, connectionString, 1);
                        RouteHelper.ProcessFactVisitsRussia(data, StartDate, connectionString, 2);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error in RouteHelper.ProcessFactVisits");
                    Console.WriteLine(ex.Message);
                    return -1;
                }

                Console.WriteLine("Маршруты посчитаны", sStartDate);

                return 0;
            }

            #endregion

            Console.WriteLine("Неизвестная комбинация входных параметров");
            return -1;
        }
    }
}
