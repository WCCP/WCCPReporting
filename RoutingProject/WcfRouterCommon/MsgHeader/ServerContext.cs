﻿using System.ServiceModel;

namespace WcfRouterCommon.MsgHeader
{
    /// <summary>
    /// This class will act as a custom context, an extension to the OperationContext.
    /// This class holds all context information for our application.
    /// </summary>
    public class ServerContext : IExtension<OperationContext>
    {
        public string PCName;
        public string PCUserLogin;
        public string SQLUserLogin;
        public string HostName;
        public long RouteId;

        // Get the current one from the extensions that are added to OperationContext.
        public static ServerContext Current
        {
            get { return OperationContext.Current.Extensions.Find<ServerContext>(); }
        }

        #region IExtension<OperationContext> Members

        public void Attach(OperationContext owner)
        {
        }

        public void Detach(OperationContext owner)
        {
        }

        #endregion
    }
}