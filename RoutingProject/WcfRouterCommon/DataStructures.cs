﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WcfRouterCommon
{
    /// <summary>
    /// Описывает параметры, по которым будет строиться маршрут.
    /// </summary>

    [DataContract]
    public class GeoPoint
    {
        [DataMember]
        public double Lat { get; set; }
        [DataMember]
        public double Lon { get; set; }

        public GeoPoint(double Lat, double Lon)
        {
            this.Lat = Lat;
            this.Lon = Lon;
        }

    }

    [DataContract]
    public class RouteParams
    {
        
        [DataMember]
        public bool IsRound { get; set; } //возвращаться в первую точку или нет
        [DataMember]
        public bool OptimizeIt { get; set; } //вернуть оптимизированый маршрут вместе с исходным или только исходный
        [DataMember]
        public GeoPoint[] Points { get; set; }
        [DataMember]
        public bool IsRu { get; set; }

        public RouteParams() { }

        public RouteParams(GeoPoint[] Points)
        {
            this.Points = Points;
        }
    }

    [DataContract]
    public class MerchRouteParams
    {
        [DataMember]
        public long Merch_ID;
        [DataMember]
        public DateTime RouteDate;
        [DataMember]
        public short RouteType; // "fact" - 0, "plan" - 1
        [DataMember]
        public bool IsRu;

        public MerchRouteParams()
        {
        }

        public MerchRouteParams(long merchID, DateTime startDate, short routeType, bool isRu)
        {
            Merch_ID = merchID;
            RouteDate = startDate;
            RouteType = routeType;
            IsRu = isRu;
        }
    }

    [DataContract]
    public class RouteResult
    {
        [DataMember]
        public short ErrorCode { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public GeoPoint[] SourcePoints; //исходные точки
        [DataMember]
        public string[] Labels; //метки исходных точек
        [DataMember]
        public List<GeoPoint[]> SourceRoute; //сегменты исходного маршрута
        [DataMember]
        public int[] RejectedPoints; //номера точек, которые не удалось привязать к дорожной сетке
        [DataMember]
        public GeoPoint[] OptimizedRoute; //оптимизированый маршрут
        [DataMember]
        public List<GeoPoint[]> OptRouteSegments; //оптимизированый маршрут
        [DataMember]
        public int[] OptimizedOrder; //порядок посещения точек после оптимизации
        [DataMember]
        public double SourceDistance = 0.0; //суммарная дистанция исходного маршрута
        [DataMember]
        public double OptimizedDistance = 0.0; //дистанция после оптимизации

        public void SetError(short ErrorCode, string ErrorMessage)
        {
            this.ErrorCode = ErrorCode;
            this.ErrorMessage = ErrorMessage;
        }

    }

}
