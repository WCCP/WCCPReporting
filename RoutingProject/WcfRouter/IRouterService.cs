﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfRouterCommon;

namespace WcfRouter
{
    [ServiceContract]
    public interface IRouterService
    {
        [OperationContract]
        RouteResult BuildRoute(RouteParams param);

        [OperationContract]
        RouteResult BuildMerchRoute(MerchRouteParams param);
    }


}
