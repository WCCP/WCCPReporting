﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Xml.Linq;
using RouteLib;
using WcfRouterCommon;
using WcfRouterCommon.MsgHeader;

namespace WcfRouter
{
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    [ServiceBehavior(MaxItemsInObjectGraph = int.MaxValue)]
    [CustomBehavior]
    public class RouterService : IRouterService
    {
        // этот объект, V1RouterDataSource, источник для копирования
        // для каждого параллельного процесса надо получить копию от него, т.к. он не потокобезопасный
        // у V1RouterDataSource есть спец. конструктор, который по исходному V1RouterDataSource делает копию
        private static object _geoDataSrcRu = null;
        private static object _geoDataSrcUa = null;
        private static readonly Object LockObject = new Object();

        private static object GetGeoDataSrc(bool isRu)
        {
            return isRu ? _geoDataSrcRu : _geoDataSrcUa;
        }

        private void SetGeoData(bool isRu, object geoDataSrc)
        {
            if (isRu)
            {
                _geoDataSrcRu = geoDataSrc;
            }
            else
            {
                _geoDataSrcUa = geoDataSrc;
            }
        }

        private void ReadGeoData(bool isRu = false)
        {
            lock (LockObject)
            {
                if (GetGeoDataSrc(isRu) == null)
                {
                    object geoData = RouteHelper.ReadGeoData(isRu);
                    SetGeoData(isRu, geoData);
                }
            }
        }

        public RouteResult BuildRoute(RouteParams param)
        {
            object geoData = GetGeoDataSrc(param.IsRu);
            if (geoData == null)
                ReadGeoData(param.IsRu);

            geoData = GetGeoDataSrc(param.IsRu);

            Stopwatch wch = Stopwatch.StartNew();
            RouteResult res = RouteHelper.BuildRoute(param, geoData);

      #region Logger
            string connectionString =
                "Data Source=eegdadb8.ee.interbrew.net;Initial Catalog=SalesWorks;Integrated Security=False;User ID=Logger;Password=3111";

            XDocument srcPoints = new XDocument();

            if (res.SourcePoints!=null && res.SourcePoints.Length > 0)
                srcPoints.Add(new XElement("root",
                    res.SourcePoints.Select( x =>
                            new XElement("point", 
                                new XElement("lat", x.Lat.ToString(CultureInfo.InvariantCulture)),
                                new XElement("lon", x.Lon.ToString(CultureInfo.InvariantCulture))))));
                    
            XDocument rejPoints = new XDocument();


            if (res.SourceRoute != null && res.RejectedPoints.Length > 0)
                rejPoints.Add(new XElement("root",
                    res.RejectedPoints.Select(x => 
                            new XElement("point", 
                                new XElement("lat", res.SourcePoints[x].Lat.ToString(CultureInfo.InvariantCulture)),
                                new XElement("lon", res.SourcePoints[x].Lon.ToString(CultureInfo.InvariantCulture))))));

            XDocument optOrderPoints = new XDocument();

            if (res.OptimizedRoute != null)
                optOrderPoints.Add(new XElement("root", 
                    res.OptimizedOrder.Select(x => 
                            new XElement("point",
                                new XElement("lat", res.SourcePoints[x].Lat.ToString(CultureInfo.InvariantCulture)),
                                new XElement("lon", res.SourcePoints[x].Lon.ToString(CultureInfo.InvariantCulture))))));


            RouteHelper.LogOperationInfo(res, OperationContext.Current.IncomingMessageProperties["CurrentContext"]
                , wch.ElapsedMilliseconds, param, srcPoints, optOrderPoints, rejPoints, connectionString);
        #endregion
            return res;
        }

        public RouteResult BuildMerchRoute(MerchRouteParams param)
        {
            object geoData = GetGeoDataSrc(param.IsRu);
            if (geoData == null)
                ReadGeoData(param.IsRu);

            string connectionString = ConfigurationManager.ConnectionStrings["WcfRouter.Properties.Settings.ConnectionString"].ConnectionString;

            geoData = GetGeoDataSrc(param.IsRu);
            RouteResult res = RouteHelper.BuildMerchRoute(param, connectionString, geoData);

            return res;
        }

    }
}
