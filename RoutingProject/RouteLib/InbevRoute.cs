﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using OsmSharp.Routing;
using OsmSharp.Tools.Math.Geo;
using OsmSharp.Tools.Xml.Gpx;
using System.Xml;
using OsmSharp.Tools.Xml.Sources;
using System.IO;
using OsmSharp.Tools.Xml.Gpx.v1_1;
using OsmSharp.Routing.Route;
using WcfRouterCommon;

namespace RouteLib
{
    /// <summary>
    /// Хранит начальный набор точек и все необходимые промежуточные данные для построения маршрута as is, 
    /// также сохраняет новый порядок точек после оптимизации
    /// </summary>
    public class InbevRoute
    {
        private IRouter<RouterPoint> router; //роутер, который будет использоваться для построения маршрута
        private List<GeoCoordinate> coords;  //начальный набор координат

        public RouterPoint[] Points;  //привязаные к карте точки
        public int[] RejectedPoints; //номера точек, которые не удалось привязать к дорожной сетке

        public string[] Labels; //метки точек, номер ТТ
        public double[] Distances; //расстояния между парами точек
        public double[] Times;  //время между парами точек
        public double SourceDistance = 0.0; //суммарная дистанция
        public double TotalTime = 0.0; //суммарное время
        public int NotConnectedPairs;//счетчик не соединенных пар

        public GeoPoint[] OptimizedRoute; //OsmSharpRoute
        public int[] OptimizedOrder; //порядок посещения точек после оптимизации
        public double OptimizedDistance = -1.0; //длина оптимизированого маршрута
        //накапливает сегменты при построении маршрута as is
        //между некоторыми парами точек путь не может быть проложен
        //тогда начнется новый сегмент с точки, которая соединится со следующей точкой
        private List<RoutePointEntry[]> Segments = new List<RoutePointEntry[]>();


        public void SaveOptimizedRouteGPX(string FilePath)
        {
            if (OptimizedRoute == null)
                return;

            if (File.Exists(FilePath))
                File.Delete(FilePath);

            GpxDocument doc = new GpxDocument(new XmlFileSource(new FileInfo(FilePath)));
            gpxType gpx = new gpxType();
            gpx.trk = new trkType[1];

            List<wptType> wpt = new List<wptType>();
            trkType track = new trkType();
            track.trkseg = new trksegType[1];

            int i = 0;
            foreach (RouterPoint point in Points)
                wpt.Add(new wptType()
                {
                    lat = (decimal)point.Location.Latitude,
                    lon = (decimal)point.Location.Longitude,
                    name = i++.ToString()
                });

            List<trksegType> xml_segments = new List<trksegType>();
            List<wptType> xml_segment;
            xml_segment = new List<wptType>();
            foreach (GeoPoint entry in OptimizedRoute)
            {
                xml_segment.Add(new wptType()
                {
                    lat = (decimal)entry.Lat,
                    lon = (decimal)entry.Lon,
                });

            }

            trksegType track_segment = new trksegType();
            track_segment.trkpt = xml_segment.ToArray();
            xml_segments.Add(track_segment);

            track.trkseg = xml_segments.ToArray();
            gpx.trk[0] = track;
            gpx.wpt = wpt.ToArray();
            doc.Gpx = gpx;
            doc.Save();
        }

        public List<GeoPoint[]> GetSourceRouteSegments()
        {
            if (Segments == null)
                return null;

            List<GeoPoint[]> res = new List<GeoPoint[]>(Segments.Count);
            GeoPoint[] part;
            foreach (RoutePointEntry[] segment in Segments)
            {
                if (segment == null)
                {
                    res.Add(null);
                    continue;
                }

                part = new GeoPoint[segment.Length];
                for (int i = 0; i < segment.Length; i++)
                    part[i] = new GeoPoint(segment[i].Latitude, segment[i].Longitude);
                res.Add(part);
            }

            return res;
        }

        public InbevRoute(IRouter<RouterPoint> router, List<GeoCoordinate> coords, string[] Labels)
        {
            this.router = router;
            this.coords = coords;
            this.Labels = Labels;
        }

        /// <summary>
        /// Привязывает точки к карте
        /// </summary>
        /// <returns></returns>
        private void ResolveGeoCoordinates()
        {
            List<RouterPoint> points = new List<RouterPoint>();
            List<int> rejectedList = new List<int>();

            RouterPoint point = null;
            float default_delta = 0.01f;
            float delta = default_delta;
            int i = 0;
            foreach (GeoCoordinate coord in coords)
            {
                try
                {
                    point = router.Resolve(VehicleEnum.Car, delta, coord);
                }
                catch
                {
                    rejectedList.Add(i);
                    point = null;
                    continue;
                }

                while (point == null && delta <= 0.1)
                {
                    delta *= 2;
                    point = router.Resolve(VehicleEnum.Car, delta, coord);
                }

                delta = default_delta;

                if (point != null)
                    points.Add(point);
                else
                    rejectedList.Add(i);

                i++;

            }

            RejectedPoints = rejectedList.ToArray();
            Points = points.ToArray();

            if (rejectedList.Count > 0)
            {
                //нужно выкинуть из Labels метки удаленных точек
                List<string> Lbls = new List<string>();
                //реализуем merge join, чтобы обработать два массива за один проход
                i = 0;
                int j = 0;
                while (i < Labels.Length)
                {
                    if (j >= RejectedPoints.Length || i < RejectedPoints[j]) //если индекс метки меньше чем номер точки которую надо выкинуть, добавляем текущую метку к результату (или если выкинутых точек больше нет)
                    {
                        Lbls.Add(Labels[i]);
                        i++;
                    }
                    else
                    {
                        j++;
                        i++;
                    }
                }
                Labels = Lbls.ToArray();
            }
        }

        /// <summary>
        /// Строит маршрут по набору координат. 
        /// В начале привязывает координаты к карте (резолвит координаты), затем строит путь по каждой паре координат, 
        /// накапливает результат в виде набора отрезков пути
        /// </summary>
        /// <returns>Возвращает код ошибки
        ///      0 - успешно
        ///      1 - успешно, но не все точки удалось привязать к карте
        ///     -2 - не все пары координат удалось связать путем
        ///     -6 - расстояние между ТТ превышает 1000 км
        /// </returns>
        public int BuildRoute(bool isRound)
        {
            if (coords.Count > 50)  // Ticket#1041279 The real route can't have more than 40 outlests
            {                       //Ticket#1066353 — [335149] WCCP - Route Distance Report расстояние по сотруднику равно нулю. From 40 --> 50 max count TT on route
                return -7;
            }
            
            //заполняет Points, RejectedPoints
             ResolveGeoCoordinates();

             for (int i = 1; i < Points.Length; i++)
             {
                 var pointFrom = new GeoCoordinate(Points[i - 1].Location.Latitude, Points[i - 1].Location.Longitude);
                 var pointTo = new GeoCoordinate(Points[i].Location.Latitude, Points[i].Location.Longitude);
                 var distance = pointFrom.DistanceReal(pointTo).Value;
                 if (distance >= 150000)
                 {
                     return -6;
                 }
             }

            OsmSharpRoute cur_route = null;
            RouterPoint prev_point = null;
            List<double> Dist = new List<double>();
            List<double> Time = new List<double>();

            bool allPointsConnected = true;
            NotConnectedPairs = 0;

            RouterPoint[] tmpPoints;
            if (isRound) //соединим последнюю точку с первой
            {
                tmpPoints = new RouterPoint[Points.Length + 1];
                Points.CopyTo(tmpPoints, 0);
                tmpPoints[tmpPoints.Length-1] = tmpPoints[0];
            }
            else
                tmpPoints = Points;

            foreach (RouterPoint cur_point in tmpPoints)
            {
                if (prev_point == null)
                {
                    prev_point = cur_point;
                    continue;
                }

                var thread = new Thread(() =>
                {
                    cur_route = router.Calculate(VehicleEnum.Car, prev_point, cur_point);
                }, 4194304); //4Mb stack

                thread.Start();
                thread.Join();

                if (cur_route == null)
                {
                    NotConnectedPairs += 1;
                    SourceDistance += cur_point.Location.DistanceReal(prev_point.Location).Value;
                    prev_point = cur_point;
                    allPointsConnected = false;
                    Segments.Add(null);
                    continue;
                }
                else
                {
                    Segments.Add(cur_route.Entries);
                }

                Dist.Add(cur_route.TotalDistance);
                Time.Add(cur_route.TotalTime);
                SourceDistance += cur_route.TotalDistance;
                TotalTime += cur_route.TotalTime;

                prev_point = cur_point;
            }//for each Point

            Distances = Dist.ToArray();
            Times = Time.ToArray();

            if (Points.Length != coords.Count && allPointsConnected)
                return 1;

            if (allPointsConnected)
                return 0;
            else
                return -2;
        }


        /// <summary>
        /// Сохраняет маршрут в GPX файле
        /// </summary>
        /// <param name="FilePath"></param>
        public void SaveAsGPX(string FilePath)
        {
            if (File.Exists(FilePath))
                File.Delete(FilePath);

            GpxDocument doc = new GpxDocument(new XmlFileSource(new FileInfo(FilePath)));
            gpxType gpx = new gpxType();
            gpx.trk = new trkType[1];

            List<wptType> wpt = new List<wptType>();
            trkType track = new trkType();
            track.trkseg = new trksegType[1];

            int i = 0;
            foreach (RouterPoint point in Points)
                wpt.Add(new wptType()
                {
                    lat = (decimal)point.Location.Latitude,
                    lon = (decimal)point.Location.Longitude,
                    name = i++.ToString()
                });

            List<trksegType> xml_segments = new List<trksegType>();
            List<wptType> xml_segment;
            foreach (RoutePointEntry[] segment in Segments)
            {
                if (segment == null)
                    continue;

                xml_segment = new List<wptType>();
                //List<trkptType>
                foreach (RoutePointEntry entry in segment)
                {
                    xml_segment.Add(new wptType()
                    {
                        lat = (decimal)entry.Latitude,
                        lon = (decimal)entry.Longitude,
                    });
                }

                trksegType track_segment = new trksegType();
                track_segment.trkpt = xml_segment.ToArray();
                xml_segments.Add(track_segment);

            }

            track.trkseg = xml_segments.ToArray();
            gpx.trk[0] = track;
            gpx.wpt = wpt.ToArray();
            doc.Gpx = gpx;
            doc.Save();

        }

        /// <summary>
        /// Вычисляет окончание множественного числа для слова "пары"
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public string PluralPostfix(int n)
        {
            string postfix = "";
            switch (n)
            {
                case 1:
                    postfix = "у";
                    break;
                case 2:
                case 3:
                case 4:
                    postfix = "ы";
                    break;
            }

            return postfix;
        }
    }
}
