﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Xml.Linq;
using OsmSharp.Osm;
using OsmSharp.Routing;
using OsmSharp.Routing.Osm.Interpreter;
using OsmSharp.Routing.Osm.Data.Processing;
using OsmSharp.Routing.Graph;
using OsmSharp.Routing.Graph.DynamicGraph.PreProcessed;
using OsmSharp.Routing.Graph.Router.Dykstra;
using OsmSharp.Routing.Route;
using OsmSharp.Routing.Metrics.Time;


//using OsmSharp.Routing.Osm.Data.Source;
using OsmSharp.Osm.Data.XML.Processor;
using OsmSharp.Osm.Data.PBF.Raw.Processor;
using OsmSharp.Osm.Data.Core.Processor;
using OsmSharp.Osm.Data.Core.Processor.Progress;
using OsmSharp.Routing.TSP;
using OsmSharp.Routing.TSP.Genetic;

using OsmSharp.Tools.Collections;
using OsmSharp.Tools.Math.Geo;
using OsmSharp.Tools.Math.VRP.Core.Routes;
using NLog;

using OsmSharp.Routing.Graph.Serialization.v1;
using OsmSharp.Routing.Graph.Router;

using WcfRouterCommon;
using WcfRouterCommon.MsgHeader;
using BLToolkit.Data;


namespace RouteLib
{
    public class RouteHelper
    {
        private static bool BLInitDone = false;
        private static Logger logger = LogManager.GetLogger("RouteHelper");
        public static bool IsInScopeMode = false;
        private static List<VisitInfo> visitsInfo;
        
        #region Routing

        public static RouteResult DoRoute(List<GeoCoordinate> Points, List<string> Labels, bool isRound, bool optimizeIt, Router<PreProcessedEdge> router, OsmRoutingInterpreter interpreter)
        {
            //предполагаем успех
            RouteResult res = new RouteResult { ErrorCode = 0, ErrorMessage = "" };

            //если точек на маршруте нет, то и оптимизировать нечего
            if (Points.Count - (isRound ? 1 : 0) == 0)
            {
                res.SetError(-1, "На маршруте нет точек");
                logger.Debug("Error {0} {1}", res.ErrorCode, res.ErrorMessage);
                return res;
            }

            //если на маршруте всего одна точка, то возвращаем ее в качестве результата
            if (Points.Count == 1)
            {
                GeoPoint[] arr = new GeoPoint[1] { new GeoPoint(Points[0].Latitude, Points[0].Longitude) };

                res = new RouteResult()
                {
                    ErrorCode = 0,
                    ErrorMessage = "",
                    SourcePoints = arr,
                    Labels = new string[1] { Labels[0] },
                    SourceRoute = new List<GeoPoint[]>() { arr },
                    OptimizedRoute = arr,
                    OptRouteSegments = new List<GeoPoint[]>() { arr },
                    OptimizedOrder = new int[1] { 0 },
                    SourceDistance = 0.0,
                    OptimizedDistance = 0.0
                };
                logger.Trace("Одна точка на маршруте");
                return res;
            }

            //build source route
            InbevRoute myroute = new InbevRoute(router, Points, Labels.ToArray());
            int route_err = myroute.BuildRoute(isRound);

            switch (route_err)
            {
                case 1:
                    res.SetError(1, string.Format("Не все точки из маршрута удалось привязать, {0} из {1}", myroute.Points.Length, Points.Count));
                    break;
                case -2:
                    res.SetError(-2, string.Format("Не удалось построить маршрут, {0} пар{1} точек не удалось соединить", myroute.NotConnectedPairs, myroute.PluralPostfix(myroute.NotConnectedPairs)));
                    break;
                case -6:
                    res.SetError(-6, "Не удалось построить маршрут, расстояние между ТТ не может превышать 150 км");
                    break;
                case - 7:
                    res.SetError(-7, "Максимальное количество ТТ на маршруте не должно превышать 50");
                    break;
            }

            if (route_err < 0 || !optimizeIt)
            {
                CopyInbevRouteToResult(myroute, res);
                return res;
            }

            //нет смысла оптимизировать маршрут из 2 точек
            if (myroute.Points.Length > 2)
            {
                OsmSharpRoute tsp = null;
                try
                {
                    RouterTSP router_tsp = new RouterTSPAEXGenetic();

                    RouterTSPWrapper<RouterPoint, RouterTSP> tsp_solver = new RouterTSPWrapper<RouterPoint, RouterTSP>(router_tsp, router, interpreter);


                    //следующий кусок кода - это часть tsp_solver.CalculateTSP, которую я выделил только для того чтобы получить IRoute, а по нему - новый порядок посещения точек
                    //tsp = tsp_solver.CalculateTSP(VehicleEnum.Car, myroute.Points);

                    #region Ticket#1054512
                    //double[][] weights = router.CalculateManyToManyWeight(VehicleEnum.Car, myroute.Points, myroute.Points);    
                    double[][] weights = new double[][] { };

                    var thread = new Thread(() =>
                    {
                        weights = router.CalculateManyToManyWeight(VehicleEnum.Car, myroute.Points, myroute.Points);
                    }, 4194304); //4Mb stack  Ticket#1054512 

                    thread.Start();
                    thread.Join();
                    #endregion

                    GeoCoordinate[] locations = myroute.Points.Select(point => point.Location).ToArray();

                    IRoute tsp_solution = router_tsp.CalculateTSP(weights, locations, 0, isRound);

                    myroute.OptimizedOrder = tsp_solution.ToArray();

                    // calculate weight
                    double weight = tsp_solution.Edges().Aggregate(0D, (sum_weight, edge) => sum_weight + weights[edge.From][edge.To]);

                    // concatenate the route(s).
                    tsp = OSMBuildRoute(VehicleEnum.Car, myroute.Points, tsp_solution, weight, router, interpreter, out res.OptRouteSegments);

                    //конец заимствования

                    myroute.OptimizedDistance = tsp.TotalDistance;
                    myroute.OptimizedRoute = tsp.Entries.Select(point => new GeoPoint(point.Latitude, point.Longitude)).ToArray();
                }
                catch (Exception ex)
                {
                    res.SetError(-4, string.Format("Ошибка во время оптимизации: {0}", ex.Message));
                    return res;
                }

                if (tsp == null)
                {
                    res.SetError(-5, "Не могу построить оптимизированый маршрут");
                    return res;
                }
            }
            else //myroute.Points.Length > 2
            {
                res.SetError(3, string.Format("Слишком мало точек для оптимизации"));
            }

            CopyInbevRouteToResult(myroute, res);
            return res;
        }


        /// <summary>
        /// Выполняет построение маршрута по точкам из Points с метками Labels, используя геоданные GeoData
        /// </summary>
        /// <param name="Points">Точки для построения маршрута</param>
        /// <param name="Labels">Метки точек</param>
        /// <param name="GeoData">Геоданные</param>
        /// <param name="isRound">Строить замкнутый маршрут или нет (возвращается в первую точку)</param>
        /// <param name="optimizeIt">Оптимизировать маршрут или нет</param>
        /// <returns>
        ///  0 - все ОК
        ///  1 - Не все точки из маршрута удалось привязать, но ОК
        ///  3 - Слишком мало точек для оптимизации, исходный маршрут вернется в качестве оптимизированного
        /// -1 - На маршруте нет точек
        /// -2 - Не удалось построить маршрут, некоторые пары точек не удалось соединить
        /// -4 - Ошибка во время оптимизации (ошибка в OSMSharp)
        /// -5 - Не могу построить оптимизированый маршрут (что-то с картой или координатами)
        /// -7 - Не могу считать маршрут из базы.
        /// </returns>
        public static RouteResult DoRoute(List<GeoCoordinate> Points, List<string> Labels, object GeoData, bool isRound, bool optimizeIt)
        {
            // create the router object.
            IBasicRouterDataSource<PreProcessedEdge> data = (IBasicRouterDataSource<PreProcessedEdge>)GeoData;
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();
            Router<PreProcessedEdge> router = new Router<PreProcessedEdge>(data, interpreter, new DykstraRoutingPreProcessed(data.TagsIndex));

            return DoRoute(Points, Labels, isRound, optimizeIt, router, interpreter);
        }

        // Заимствовано из OsmSharp
        public static OsmSharpRoute OSMBuildRoute(VehicleEnum vehicle, RouterPoint[] points, IRoute tsp_solution, double weight, Router<PreProcessedEdge> router, OsmRoutingInterpreter interpreter, out List<GeoPoint[]> segments)
        {
            int[] solution = tsp_solution.ToArray();

            OsmSharpRoute tsp = null;
            //OsmSharpRoute route;
            OsmSharpRoute route = new OsmSharpRoute(); //Ticket#1054512
            segments = new List<GeoPoint[]>(solution.Length);
            GeoPoint[] segment;
            for (int idx = 0; idx < solution.Length - 1; idx++)
            {
                #region Ticket#1054512
                //route = router.Calculate(VehicleEnum.Car, points[solution[idx]], points[solution[idx + 1]]);
                var thread = new Thread(() =>
                {
                    route = router.Calculate(VehicleEnum.Car, points[solution[idx]], points[solution[idx + 1]]);
                }, 4194304); //4Mb stack

                thread.Start();
                thread.Join();
                #endregion
                if (tsp == null)
                { // first route = start
                    tsp = route;
                }
                else
                { // concatenate.
                    tsp = OsmSharpRoute.Concatenate(tsp, route);
                }

                if (route != null)
                {
                    segment = route.Entries.Select(point => new GeoPoint(point.Latitude, point.Longitude)).ToArray();
                    segments.Add(segment);
                }
                else
                {
                    //throw
                }
            }

            if (tsp_solution.IsRound)
            {
                // concatenate the route from the last to the first point again.
                #region Ticket#1054512
                //route = router.Calculate(VehicleEnum.Car, points[solution[solution.Length - 1]], points[solution[0]]);
                var thread = new Thread(() =>
                {
                    route = router.Calculate(VehicleEnum.Car, points[solution[solution.Length - 1]], points[solution[0]]);
                }, 4194304); //4Mb stack
                thread.Start();
                thread.Join();
                #endregion
                tsp = OsmSharpRoute.Concatenate(tsp, route);
                segments.Add(route.Entries.Select(point => new GeoPoint(point.Latitude, point.Longitude)).ToArray());
            }

            if (tsp != null)
            { // the route is valid.
                if (interpreter != null)
                { // there is an interpreter set: calculate time/distance.
                    // calculate metrics.
                    var calculator = new TimeCalculator(interpreter);
                    Dictionary<string, double> metrics = calculator.Calculate(tsp);
                    tsp.TotalDistance = metrics[TimeCalculator.DISTANCE_KEY];
                    tsp.TotalTime = metrics[TimeCalculator.TIME_KEY];
                }

                // set interal weight tag.
                tsp.Tags = new RouteTags[1];
                tsp.Tags[0] = new RouteTags();
                tsp.Tags[0].Key = "internal_weight";
                tsp.Tags[0].Value = weight.ToString(System.Globalization.CultureInfo.InvariantCulture);

                // set the correct vehicle type.
                tsp.Vehicle = vehicle;
            }

            return tsp;
        }

        #endregion

        #region BuildRoute
        //строит маршрут по набору координат
        //для одиночных вызовов, геоданные считываются из файла при каждом вызове
        public static RouteResult BuildRoute(RouteParams param)
        {
            object data = ReadGeoData(param.IsRu);
            return BuildRoute(param, data);
        }

        //строит маршрут по набору координат
        //для серийных вызовов, геоданные передаются параметром GeoData
        public static RouteResult BuildRoute(RouteParams param, object GeoDataSrc)
        {
            logger.Trace("BuildRoute call, {0} points, IsRound = {1}, OptimizeIt = {2}", param.Points.Length, param.IsRound, param.OptimizeIt);

            //предполагаем успех
            RouteResult res = new RouteResult { ErrorCode = 0, ErrorMessage = "" };

            V1RouterDataSource GeoData = new V1RouterDataSource((V1RouterDataSource)GeoDataSrc);

            //получим список координат
            List<GeoCoordinate> Points = new List<GeoCoordinate>();
            List<string> Labels = new List<string>();
            for (int i = 0; i < param.Points.Length; i++)
            {
                Points.Add(new GeoCoordinate(param.Points[i].Lat, param.Points[i].Lon));
                Labels.Add(i.ToString());
            }

            if (param.IsRound && Labels.Count > 0)
                Labels[0] = "start point";

            res = DoRoute(Points, Labels, GeoData, param.IsRound, param.OptimizeIt);

            if (res.ErrorCode != 0)
                logger.Trace("Error {0} {1}", res.ErrorCode, res.ErrorMessage);

            return res;
        }
        #endregion

        #region BuildMerchRoute
        //для одиночных вызовов, геоданные считываются из файла при каждом вызове
        public static RouteResult BuildMerchRoute(MerchRouteParams param, string ConnectionString)
        {
            object data = ReadGeoData(param.IsRu);
            // create the router object.
            IBasicRouterDataSource<PreProcessedEdge> GeoData = (IBasicRouterDataSource<PreProcessedEdge>)data;
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();
            Router<PreProcessedEdge> router = new Router<PreProcessedEdge>(GeoData, interpreter, new DykstraRoutingPreProcessed(GeoData.TagsIndex));

            return BuildMerchRoute(param, ConnectionString, router, interpreter);
        }

        //Для вызовов из сервиса, геоданные закешированые
        public static RouteResult BuildMerchRoute(MerchRouteParams param, string ConnectionString, object GeoDataSrc)
        {
            // create the router object.
            V1RouterDataSource GeoData = new V1RouterDataSource((V1RouterDataSource)GeoDataSrc);
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();
            Router<PreProcessedEdge> router = new Router<PreProcessedEdge>(GeoData, interpreter, new DykstraRoutingPreProcessed(GeoData.TagsIndex));

            return BuildMerchRoute(param, ConnectionString, router, interpreter);
        }

        //для серийных вызовов, роутер и интерпренер передаются параметрами
        public static RouteResult BuildMerchRoute(MerchRouteParams param, string ConnectionString, Router<PreProcessedEdge> router, OsmRoutingInterpreter interpreter, bool logErrors = true)
        {
            logger.Trace("BuildMerchRoute call, m={0}, d={1:D4}-{2:D2}-{3:D2}, t={4}", param.Merch_ID, param.RouteDate.Year, param.RouteDate.Month, param.RouteDate.Day, param.RouteType);

            //предполагаем успех
            RouteResult res = new RouteResult { ErrorCode = 0, ErrorMessage = "" };

            //BLToolkit init
            if (!BLInitDone)
            {
                logger.Trace("BL Init");
                DbManager.AddConnectionString(ConnectionString);
                BLInitDone = true;
            }

            //получим список координат
            List<GeoCoordinate> Points = null;
            List<string> Labels = new List<string>();
            try
            {
                if (IsInScopeMode)
                {
                    List<VisitInfo> dateMerchVisits =
                        visitsInfo.FindAll(v => (v.Merch_Id == (int) param.Merch_ID) && (v.Date == param.RouteDate))
                            .ToList();
                    Points =
                        dateMerchVisits.Select(p => new GeoCoordinate(p.Coordinate.Latitude, p.Coordinate.Longitude))
                            .ToList();
                    Labels = dateMerchVisits.Select(l => l.OL_id.ToString()).ToList();
                }
                else
                {
                    Points = GetCoords(param, Labels);
                }
            }
            catch (Exception ex)
            {
                res.SetError(-7, string.Format("Не могу считать маршрут из базы. Ошибка: {0}", ex.Message));
                logger.Debug("Error {0} {1}", res.ErrorCode, res.ErrorMessage);
                return res;
            }

            //у стартовой точки OL_Id = -1
            int idx = Labels.FindIndex(label => label == "-1");
            bool useStartPoint = idx > -1;
            if (useStartPoint)
            {
                Labels[idx] = "start point";
            }

            res = DoRoute(Points, Labels, useStartPoint, true, router, interpreter);

            try
            {
                if (res.ErrorCode < 0)
                {
                    SaveErrorToDB(param, res);
                }
                else
                {
                    if (logErrors)
                    {
                        SaveRouteToDB(param, res, useStartPoint);
                    }
                    else
                    {
                        if (res.ErrorCode != 3)
                        {
                            SaveRouteToDB(param, res, useStartPoint);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                res.SetError(-7, string.Format("Ошибка при сохранении в БД: {0}", ex.Message));
                logger.Debug("Error {0} {1}", res.ErrorCode, res.ErrorMessage);
                return res;
            }

            if (res.ErrorCode != 0)
                logger.Trace("Error {0} {1}", res.ErrorCode, res.ErrorMessage);

            return res;
        }
        
        #endregion

        #region ProcessFactVisits
        public static bool ProcessFactVisits(DateTime StartDate, string ConnectionString)
        {
            object data = ReadGeoData(false);
            // create the router object.
            IBasicRouterDataSource<PreProcessedEdge> GeoData = (IBasicRouterDataSource<PreProcessedEdge>)data;
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();
            Router<PreProcessedEdge> router = new Router<PreProcessedEdge>(GeoData, interpreter, new DykstraRoutingPreProcessed(GeoData.TagsIndex));

            ProcessFactVisits(StartDate, ConnectionString, router, interpreter);
            return true;
        }

        public static bool ProcessFactVisits(DateTime StartDate, string ConnectionString, object data)
        {
            // create the router object.
            IBasicRouterDataSource<PreProcessedEdge> GeoData = (IBasicRouterDataSource<PreProcessedEdge>)data;
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();
            Router<PreProcessedEdge> router = new Router<PreProcessedEdge>(GeoData, interpreter, new DykstraRoutingPreProcessed(GeoData.TagsIndex));

            ProcessFactVisits(StartDate, ConnectionString, router, interpreter);

            return true;
        }

        public static bool ProcessFactVisits(DateTime StartDate, string ConnectionString, Router<PreProcessedEdge> router, OsmRoutingInterpreter interpreter)
        {
            logger.Trace("ProcessFactVisits call on {0}", StartDate);
            //BLToolkit init
            if (!BLInitDone)
            {
                logger.Trace("BL Init");
                DbManager.AddConnectionString(ConnectionString);
                BLInitDone = true;
            }

            bool res = true;

            //Calciulate facts in scope mode
            if (IsInScopeMode)
            {
                return ProcessFactVisitsInScopeMode(StartDate, ConnectionString, router, interpreter, 0, true);
            }

            List<VisitInfo> visits = GetUnprocessedFactVisits(StartDate);

            logger.Trace("{0} visits to go", visits.Count);

            foreach (VisitInfo visit in visits)
            {
                var param = new MerchRouteParams(visit.Merch_Id, visit.Date, 0, false);
                BuildMerchRoute(param, ConnectionString, router, interpreter);
            }

            logger.Trace("ProcessFactVisits done");

            return res;
        }

        public static bool ProcessFactVisitsRussia(object geoData, DateTime StartDate, string ConnectionString, int pid)
        {
            // create the router object.
            IBasicRouterDataSource<PreProcessedEdge> GeoData = (IBasicRouterDataSource<PreProcessedEdge>)geoData;
            OsmRoutingInterpreter interpreter = new OsmRoutingInterpreter();
            Router<PreProcessedEdge> router = new Router<PreProcessedEdge>(GeoData, interpreter, new DykstraRoutingPreProcessed(GeoData.TagsIndex));

            ProcessFactVisitsRussia(StartDate, ConnectionString, router, interpreter, pid, true);
            return true;
        }

        public static bool ProcessFactVisitsRussia(DateTime StartDate, string ConnectionString, Router<PreProcessedEdge> router, OsmRoutingInterpreter interpreter, int pid, bool logErrors)
        {
            logger.Trace("ProcessFactVisits call for PID = {0} on {1}", pid, StartDate);
            //BLToolkit init
            if (!BLInitDone)
            {
                logger.Trace("BL Init");
                DbManager.AddConnectionString(ConnectionString);
                BLInitDone = true;
            }

            bool res = true;

            if (IsInScopeMode)
            {
                return ProcessFactVisitsInScopeMode(StartDate, ConnectionString, router, interpreter, pid, true);
            }

            List<VisitInfo> visits = GetUnprocessedFactVisits(StartDate, pid);

            logger.Trace("{0} visits to go", visits.Count);

            MerchRouteParams param;

            foreach (VisitInfo visit in visits)
            {
                param = new MerchRouteParams(visit.Merch_Id, visit.Date, 0, true);
                BuildMerchRoute(param, ConnectionString, router, interpreter, logErrors);
            }

            logger.Trace("ProcessFactVisits done");

            return res;
        }

        public static bool ProcessFactVisitsInScopeMode(DateTime StartDate, string ConnectionString, Router<PreProcessedEdge> router, OsmRoutingInterpreter interpreter, int pid, bool logErrors)
        {
            logger.Trace("ProcessFactVisits call for PID = {0} on {1}", pid, StartDate);
            //BLToolkit init
            if (!BLInitDone)
            {
                logger.Trace("BL Init");
                DbManager.AddConnectionString(ConnectionString);
                BLInitDone = true;
            }

            bool res = true;

            DataTable dt = GetUnprocessedFactVisitsData(StartDate, pid);

            List<VisitInfo> visitsDetails =
                dt.AsEnumerable()
                    .Select(
                        row =>
                            new VisitInfo()
                            {
                                Merch_Id = row.Field<int>("Merch_id"),
                                Date = row.Field<DateTime>("RouteDate"),
                                OL_id = row.Field<Int64>("Ol_id"),
                                Coordinate = new GeoCoordinate(row.Field<double>("Latitude"), row.Field<double>("Longitude"))
                            })
                    .ToList();

            visitsInfo = visitsDetails;

            DataView v = new DataView(dt);
            DataTable visitsTable = v.ToTable(true, "Merch_id", "RouteDate");

            List<VisitInfo> visits = visitsTable.AsEnumerable().Select(row => new VisitInfo()
            {
                Merch_Id = row.Field<int>("Merch_id"),
                Date = row.Field<DateTime>("RouteDate")
            }).ToList();

            logger.Trace("{0} visits to go. Operation will be performed in SCOPE MODE", visits.Count);

            MerchRouteParams param = null;
            foreach (var visit in visits)
            {
                param = new MerchRouteParams(visit.Merch_Id, visit.Date, 0, pid!=0);
                BuildMerchRoute(param ,ConnectionString, router, interpreter, logErrors);
            }

            logger.Trace("ProcessFactVisits for PID = {0} on {1}", pid, StartDate);
            logger.Trace("Count of processed Date-Merch pair's was {0}", visits.Count.ToString());
            
            return res;
        }
        #endregion

        #region Misc

        public static void CopyInbevRouteToResult(InbevRoute route, RouteResult res)
        {
            if (res.ErrorCode == -7)                            // Ticket#1041279
            {
                return;
            }
            //Копируем в res все что нужно из route
            res.SourcePoints = route.Points.Select(point => new GeoPoint(point.Location.Latitude, point.Location.Longitude)).ToArray();
            res.Labels = route.Labels;
            res.SourceRoute = route.GetSourceRouteSegments();
            res.OptimizedRoute = route.OptimizedRoute;
            res.OptimizedOrder = route.OptimizedOrder;
            res.SourceDistance = route.SourceDistance;
            res.OptimizedDistance = route.OptimizedDistance;
            res.RejectedPoints = route.RejectedPoints;

            if (res.ErrorCode == 3 && res.SourceRoute.Count > 0)//если на маршруте всего две точки, то оптимизированый маршрут совпадает с исходным
            {
                res.OptimizedRoute = res.SourceRoute[0];
                res.OptimizedOrder = new int[2] { 0, 1 };
                res.OptimizedDistance = res.SourceDistance;
                res.OptRouteSegments = new List<GeoPoint[]>(1) { res.SourceRoute[0] };
            }
        }

        public static object ReadGeoData(bool isRu = true)
        {
            string sourceFileName = "ukraine.oss";
            if (isRu)
            {
                sourceFileName = "russia.oss";
            }
            logger.Trace(string.Format("Считываю {0}", sourceFileName));

            var serializer = new V1RoutingSerializer();
            IBasicRouterDataSource<PreProcessedEdge> data = null;
            string FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, sourceFileName);
            using (var stream = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                data = serializer.Deserialize(stream, false);

            logger.Trace("Готово");

            return data;
        }

        /// <summary>
        /// Конвертирует исходный OSM файл для Украины в предварительно подготовленый для роутинга (OSS)
        /// </summary>
        /// <param name="source_file">Входной файл</param>
        /// <param name="output_file">Выходной файл</param>
        public static void LoadOSMData(string source_file, string output_file)
        {
            // create the source from the osm file.
            DataProcessorSource source;
            if (source_file.EndsWith("pbf", StringComparison.InvariantCultureIgnoreCase))
                source = new PBFDataProcessorSource(new FileInfo(source_file).OpenRead());
            else
                source = new XmlDataProcessorSource(source_file);

            var interpreter = new OsmRoutingInterpreter();
            var tagsIndex = new OsmTagsIndex(new ObjectTable<OsmTagsIndex.OsmTags>(true));
            var data = new DynamicGraphRouterDataSource<PreProcessedEdge>(tagsIndex);
            var targetData = new PreProcessedDataGraphProcessingTarget(data, interpreter, data.TagsIndex, VehicleEnum.Car);
            var progressSource = new ProgressDataProcessorSource(source);
            targetData.RegisterSource(progressSource);
            targetData.Pull();

            logger.Trace("Saving data to: " + output_file);
            var serializer = new V1RoutingSerializer();
            using (var stream = new FileStream(output_file, FileMode.Create, FileAccess.ReadWrite))
            {
                serializer.Serialize(stream, data);
                stream.Flush();
                stream.Close();
            }

            logger.Trace("Finished");
        }

        #endregion

        #region DAL

        public static long GetRoute_ID(MerchRouteParams param)
        {
            long route_ID = -1;

            string sql;
            sql = @"select top 1 Route_Id from dbo.DW_MerchOutletRoutes where StartDate = @StartDate and Merch_id = @Merch_id and IsRouteOfDay = 1";
            using (DbManager db = new DbManager())
            {
                db.SetCommand(sql,
                db.Parameter("@Merch_id", param.Merch_ID),
                db.Parameter("@StartDate", param.RouteDate));
                object id;
                id = db.ExecuteScalar();
                if (id != null)
                {
                    route_ID = (long)id;
                }
            }

            return route_ID;
        }

        public static List<GeoCoordinate> GetCoords(MerchRouteParams param, List<string> Labels)
        {
            string sql;
            string src = param.RouteType == 0 ? "fact" : "plan";
            if (src != "plan")
                sql = @"
SELECT OL_id, Latitude, Longitude
FROM
(
  SELECT r.OL_id, c.Latitude, c.Longitude,  cast(BeginTime as time) time
  FROM tblOutletCardH r
    join tblOutletCardGPS c
      on r.OlCard_id = c.OlCard_id
  WHERE 1=1
    and r.OlCardDate = @StartDate
    and Merch_id = @Merch_id
    and c.Latitude is not null
    and c.Longitude is not null

  union all

  SELECT -1 as OL_id, FN1 as Latitude, FN2 as Longitude, CAST('00:00:00' as time) time
  FROM dbo.tblMerchandisersCustomField 
  WHERE CustomKey = @Merch_id
    and FN1 is not null
    and FN2 is not null
) tmp
ORDER BY time
";
            else
                sql = @"
SELECT OL_id, Latitude, Longitude
FROM
(
  SELECT r.OL_id, c.Latitude, c.Longitude, r.RouteName, r.OL_Number
  FROM dbo.DW_MerchOutletRoutes r
    join dbo.tblOutletCoordinates c
      on r.OL_id = c.OL_ID
  WHERE 
    StartDate = @StartDate
    and Merch_id = @Merch_id
    and IsRouteOfDay = 1
    and c.Latitude is not null
    and c.Longitude is not null

  union all

  SELECT -1 as OL_id, FN1 as Latitude, FN2 as Longitude, '' RouteName, -1 OL_Number
  FROM dbo.tblMerchandisersCustomField 
  WHERE CustomKey = @Merch_id
    and FN1 is not null
    and FN2 is not null
) tmp
ORDER BY RouteName, OL_Number
";


            List<GeoCoordinate> coordinates = new List<GeoCoordinate>();
            using (DbManager db = new DbManager())
            {
                db.SetCommand(sql,
                db.Parameter("@Merch_id", param.Merch_ID),
                db.Parameter("@StartDate", param.RouteDate));
                using (IDataReader r = db.ExecuteReader())
                {
                    while (r.Read())
                    {
                        Labels.Add(r[0].ToString());
                        coordinates.Add(new GeoCoordinate((double)r[1], (double)r[2]));
                    }
                }
            }

            return coordinates;
        }

        private static List<VisitInfo> GetUnprocessedFactVisits(DateTime startDate, int pid = 0)
        {
            List<VisitInfo> visits = new List<VisitInfo>();

            using (DbManager db = new DbManager())
            {
                db.SetSpCommand("spDW_OptRoute_GetFactVisits",
                db.Parameter("@StartDate", startDate),
                db.Parameter("@PID", pid));

                using (IDataReader r = db.ExecuteReader())
                {
                    while (r.Read())
                    {
                            visits.Add(new VisitInfo((int)r[0], (DateTime)r[1]));
                    }
                }
            }

            return visits;
        }

        private static DataTable GetUnprocessedFactVisitsData(DateTime startDate, int pid = 0)
        {
            DataTable data = null;
            using (DbManager db = new DbManager())
            {
                db.Command.CommandTimeout = 60*15;
                db.SetSpCommand("spDW_GetUnprocessedFactVisitsData",
                    db.Parameter("@StartDate", startDate),
                    db.Parameter("@PID", pid));
                data = db.ExecuteDataTable();
            }
            return data;
        }

        private static void SaveErrorToDB(MerchRouteParams param, RouteResult res)
        {
            using (DbManager db = new DbManager())
            {
                long route_ID = -1;

                if (param.RouteType == 1) //plan
                {
                    route_ID = GetRoute_ID(param);
                }

                int OptRouteID = -1;
                db.SetSpCommand("spDW_OptRoute_Add",
                    db.Parameter("@Merch_id", param.Merch_ID),
                    db.Parameter("@RouteType", param.RouteType),
                    db.Parameter("@RouteDate", param.RouteDate),
                    db.Parameter("@Route_Id", route_ID),
                    db.NullParameter("@POSCount", -1, -1),
                    db.NullParameter("@DistanceOld", -1.0, -1.0),
                    db.NullParameter("@DistanceNew", -1.0, -1.0),
                    db.Parameter("@ErrorCode", res.ErrorCode),
                    db.NullParameter("@ErrorMessage", res.ErrorMessage, ""),
                    db.OutputParameter("@OptRouteID", OptRouteID)
                    ).ExecuteNonQuery();

                OptRouteID = (int)db.Parameter("@OptRouteID").Value;

                db.SetSpCommand("spDW_OptRoutePoints_Del", db.Parameter("@OptRouteID", OptRouteID));
                db.ExecuteNonQuery();
            }
        }

        private static void SaveRouteToDB(MerchRouteParams param, RouteResult res, bool useStartPoint)
        {
            //используется только для плановых маршрутов
            long route_ID = -1;
            if (param.RouteType == 1) //plan
            {
                route_ID = GetRoute_ID(param);
            }

            using (DbManager db = new DbManager())
            {
                int OptRouteID = -1;
                int POSCount = res.SourcePoints.Length - (useStartPoint ? 2 : 0);
                //в случае, если ни одна точка не разрезолвилась
                if (POSCount < 0)
                    POSCount = 0;

                db.SetSpCommand("spDW_OptRoute_Add",
                    db.Parameter("@Merch_id", param.Merch_ID),
                    db.Parameter("@RouteType", param.RouteType),
                    db.Parameter("@RouteDate", param.RouteDate),
                    db.Parameter("@Route_Id", route_ID),
                    db.NullParameter("@POSCount", POSCount, -1),
                    db.NullParameter("@DistanceOld", res.SourceDistance, -1.0),
                    db.NullParameter("@DistanceNew", res.OptimizedDistance, -1.0),
                    db.Parameter("@ErrorCode", res.ErrorCode),
                    db.NullParameter("@ErrorMessage", res.ErrorMessage, ""),
                    db.OutputParameter("@OptRouteID", OptRouteID)
                    ).ExecuteNonQuery();

                OptRouteID = (int)db.Parameter("@OptRouteID").Value;

                db.SetSpCommand("spDW_OptRoutePoints_Del", db.Parameter("@OptRouteID", OptRouteID));
                db.ExecuteNonQuery();

                db.SetSpCommand("spDW_OptRoutePoint_Add",
                    db.Parameter("@OptRouteID", OptRouteID),
                    db.Parameter("@OL_Id", DbType.Int64),
                    db.Parameter("@PositionOld", DbType.Int16),
                    db.NullParameter("@PositionNew", DbType.Int16, -1)
                    );

                bool NoOptimalOrder = (res.OptimizedOrder == null);

                for (short i = (short)(useStartPoint ? 1 : 0); i < res.SourcePoints.Length - (useStartPoint ? 1 : 0); i++)
                {
                    db.Parameter("@OL_Id").Value = long.Parse(res.Labels[i]);
                    db.Parameter("@PositionOld").Value = i;
                    db.Parameter("@PositionNew").Value = NoOptimalOrder ? 0 : res.OptimizedOrder[i];
                    db.ExecuteNonQuery();
                }
            }

        }

        #endregion

        public static void LogOperationInfo(RouteResult r, object p, long duration, RouteParams prm, XDocument sourceRoute, XDocument optRoute, XDocument rejectPoints, string conStr)
        {
            if (!BLInitDone)
            {
                logger.Trace("BL Init");
                DbManager.AddConnectionString(conStr);
                BLInitDone = true;
            }

            try
            {
                ServerContext ctx = (ServerContext) p;

                using (DbManager db = new DbManager())
                {
                    db.SetSpCommand("spDW_LogGeoService",
                        db.Parameter("@PCName", ctx.PCName),
                        db.Parameter("@PCUserLogin", ctx.PCUserLogin),
                        db.Parameter("@SQLUserLogin", ctx.SQLUserLogin),
                        db.Parameter("@HostName", ctx.HostName),
                        db.Parameter("@Duration", duration),
                        db.Parameter("@Country_id", prm.IsRu ? "2" : "1"),
                        db.Parameter("@RouteId", ctx.RouteId),
                        db.Parameter("@IsOptimized", prm.OptimizeIt),
                        db.Parameter("@IsRound", prm.IsRound),
                        db.Parameter("@PointsCnt", prm.Points.Length),
                        db.Parameter("@RejectPointCnt", r.RejectedPoints.Length),
                        db.Parameter("@SourceRoute",sourceRoute.ToString()),
                        db.Parameter("@OptRoute",optRoute.ToString()),
                        db.Parameter("@RejectPoint",rejectPoints.ToString()),
                        db.Parameter("@SrcDistance", r.SourceDistance),
                        db.Parameter("@OptDistance", r.OptimizedDistance),
                        db.Parameter("@ErrorCode", r.ErrorCode),
                        db.Parameter("@ErrorMessage", r.ErrorMessage)
                        );
                    db.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

            }
        }
    }

    internal class VisitInfo
    {
        public int Merch_Id { get; set; }
        public DateTime Date { get; set; }
        public Int64 OL_id { get; set; }
        public GeoCoordinate Coordinate { get; set; }

        public VisitInfo()
        {}
        public VisitInfo(int Merch_Id, DateTime Date)
        {
            this.Merch_Id = Merch_Id;
            this.Date = Date;
        }
    }
}
