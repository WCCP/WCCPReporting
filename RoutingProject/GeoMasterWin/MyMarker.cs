﻿
namespace GeoMasterWinApp
{
    using System.Drawing;
    using GMap.NET.WindowsForms;
    using GMap.NET.WindowsForms.Markers;
    using GMap.NET;
    using System;
    using System.Runtime.Serialization;
    using System.Drawing.Drawing2D;

    [Serializable]
    public class MyMarker : GMapMarker, ISerializable
    {
        public int Num;

        [NonSerialized]
        public Pen Pen;

        [NonSerialized]
        public GMarkerGoogle InnerMarker;

        public MyMarker(PointLatLng p, int Num)
            : base(p)
        {
            this.Num = Num;
            Pen = new Pen(Brushes.Black, 0.1F);

            // do not forget set Size of the marker
            // if so, you shall have no event on it ;}
            Size = new System.Drawing.Size(20, 14);
            Offset = new System.Drawing.Point(-Size.Width / 2, -Size.Height / 2);
        }

        public override void OnRender(Graphics g)
        {
            using (Font drawFont1 = new Font("Arial", 10, FontStyle.Bold))
            using (Pen drawPen1 = new Pen(Color.DarkGray, 2))
            using (SolidBrush drawBrush1 = new SolidBrush(Color.Black))
            {
                //g.DrawRectangle(Pen, new System.Drawing.Rectangle(LocalPosition.X, LocalPosition.Y, Size.Width, Size.Height));
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                StringFormat sf = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Far
                };
                GraphicsPath gp = new GraphicsPath();
                Rectangle r = new Rectangle(LocalPosition.X + 0, LocalPosition.Y + 7, Size.Width, Size.Height);
                gp.AddString(Num.ToString(), drawFont1.FontFamily, (int)drawFont1.Style, drawFont1.Size, r, sf);
                g.DrawPath(drawPen1, gp);
                g.FillPath(drawBrush1, gp);
            }
        }

        public override void Dispose()
        {
            if (Pen != null)
            {
                Pen.Dispose();
                Pen = null;
            }

            if (InnerMarker != null)
            {
                InnerMarker.Dispose();
                InnerMarker = null;
            }

            base.Dispose();
        }

        #region ISerializable Members

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        protected MyMarker(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override string ToString()
        {
            return Num.ToString();
        }

        #endregion
    }
}
