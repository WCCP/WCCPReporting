﻿namespace GeoMasterWinApp
{
    partial class GeoMasterWin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTools = new System.Windows.Forms.Panel();
            this.lbEdges = new System.Windows.Forms.ListBox();
            this.lblDistances = new System.Windows.Forms.Label();
            this.gbIsOpt = new System.Windows.Forms.GroupBox();
            this.rbOptNo = new System.Windows.Forms.RadioButton();
            this.rbOptYes = new System.Windows.Forms.RadioButton();
            this.butCalculate = new System.Windows.Forms.Button();
            this.gbRouteType = new System.Windows.Forms.GroupBox();
            this.rbRouteTypePlan = new System.Windows.Forms.RadioButton();
            this.rbRoutrTypeFact = new System.Windows.Forms.RadioButton();
            this.tbMerchID = new System.Windows.Forms.TextBox();
            this.lblMerchID = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.panelTools.SuspendLayout();
            this.gbIsOpt.SuspendLayout();
            this.gbRouteType.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTools
            // 
            this.panelTools.Controls.Add(this.lbEdges);
            this.panelTools.Controls.Add(this.lblDistances);
            this.panelTools.Controls.Add(this.gbIsOpt);
            this.panelTools.Controls.Add(this.butCalculate);
            this.panelTools.Controls.Add(this.gbRouteType);
            this.panelTools.Controls.Add(this.tbMerchID);
            this.panelTools.Controls.Add(this.lblMerchID);
            this.panelTools.Controls.Add(this.lblDate);
            this.panelTools.Controls.Add(this.dtpDate);
            this.panelTools.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelTools.Location = new System.Drawing.Point(598, 0);
            this.panelTools.Name = "panelTools";
            this.panelTools.Size = new System.Drawing.Size(192, 488);
            this.panelTools.TabIndex = 18;
            // 
            // lbEdges
            // 
            this.lbEdges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEdges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbEdges.FormattingEnabled = true;
            this.lbEdges.Location = new System.Drawing.Point(1, 278);
            this.lbEdges.Name = "lbEdges";
            this.lbEdges.Size = new System.Drawing.Size(179, 197);
            this.lbEdges.TabIndex = 27;
            this.lbEdges.SelectedIndexChanged += new System.EventHandler(this.lbEdges_SelectedIndexChanged);
            // 
            // lblDistances
            // 
            this.lblDistances.AutoSize = true;
            this.lblDistances.Location = new System.Drawing.Point(6, 190);
            this.lblDistances.Name = "lblDistances";
            this.lblDistances.Size = new System.Drawing.Size(73, 78);
            this.lblDistances.TabIndex = 26;
            this.lblDistances.Text = "Факт (0 TT): \r\n  исх. 0 м\r\n  опт. 0 м\r\nПлан (0 TT): \r\n  исх. 0 м\r\n  опт. 0 м";
            // 
            // gbIsOpt
            // 
            this.gbIsOpt.Controls.Add(this.rbOptNo);
            this.gbIsOpt.Controls.Add(this.rbOptYes);
            this.gbIsOpt.Location = new System.Drawing.Point(89, 119);
            this.gbIsOpt.Name = "gbIsOpt";
            this.gbIsOpt.Size = new System.Drawing.Size(91, 68);
            this.gbIsOpt.TabIndex = 25;
            this.gbIsOpt.TabStop = false;
            this.gbIsOpt.Text = "Оптимизация";
            // 
            // rbOptNo
            // 
            this.rbOptNo.AutoSize = true;
            this.rbOptNo.Location = new System.Drawing.Point(6, 42);
            this.rbOptNo.Name = "rbOptNo";
            this.rbOptNo.Size = new System.Drawing.Size(44, 17);
            this.rbOptNo.TabIndex = 8;
            this.rbOptNo.Text = "Нет";
            this.rbOptNo.UseVisualStyleBackColor = true;
            this.rbOptNo.CheckedChanged += new System.EventHandler(this.rbOptNo_CheckedChanged);
            // 
            // rbOptYes
            // 
            this.rbOptYes.AutoSize = true;
            this.rbOptYes.Checked = true;
            this.rbOptYes.Location = new System.Drawing.Point(6, 19);
            this.rbOptYes.Name = "rbOptYes";
            this.rbOptYes.Size = new System.Drawing.Size(40, 17);
            this.rbOptYes.TabIndex = 7;
            this.rbOptYes.TabStop = true;
            this.rbOptYes.Text = "Да";
            this.rbOptYes.UseVisualStyleBackColor = true;
            this.rbOptYes.CheckedChanged += new System.EventHandler(this.rbOptYes_CheckedChanged);
            // 
            // butCalculate
            // 
            this.butCalculate.Location = new System.Drawing.Point(14, 90);
            this.butCalculate.Name = "butCalculate";
            this.butCalculate.Size = new System.Drawing.Size(139, 23);
            this.butCalculate.TabIndex = 24;
            this.butCalculate.Text = "Посчитать";
            this.butCalculate.UseVisualStyleBackColor = true;
            this.butCalculate.Click += new System.EventHandler(this.butCalculate_Click);
            // 
            // gbRouteType
            // 
            this.gbRouteType.Controls.Add(this.rbRouteTypePlan);
            this.gbRouteType.Controls.Add(this.rbRoutrTypeFact);
            this.gbRouteType.Location = new System.Drawing.Point(1, 119);
            this.gbRouteType.Name = "gbRouteType";
            this.gbRouteType.Size = new System.Drawing.Size(74, 68);
            this.gbRouteType.TabIndex = 22;
            this.gbRouteType.TabStop = false;
            this.gbRouteType.Text = "Источник";
            // 
            // rbRouteTypePlan
            // 
            this.rbRouteTypePlan.AutoSize = true;
            this.rbRouteTypePlan.Location = new System.Drawing.Point(6, 42);
            this.rbRouteTypePlan.Name = "rbRouteTypePlan";
            this.rbRouteTypePlan.Size = new System.Drawing.Size(51, 17);
            this.rbRouteTypePlan.TabIndex = 8;
            this.rbRouteTypePlan.Text = "План";
            this.rbRouteTypePlan.UseVisualStyleBackColor = true;
            this.rbRouteTypePlan.CheckedChanged += new System.EventHandler(this.rbRouteTypePlan_CheckedChanged);
            // 
            // rbRoutrTypeFact
            // 
            this.rbRoutrTypeFact.AutoSize = true;
            this.rbRoutrTypeFact.Checked = true;
            this.rbRoutrTypeFact.Location = new System.Drawing.Point(6, 19);
            this.rbRoutrTypeFact.Name = "rbRoutrTypeFact";
            this.rbRoutrTypeFact.Size = new System.Drawing.Size(53, 17);
            this.rbRoutrTypeFact.TabIndex = 7;
            this.rbRoutrTypeFact.TabStop = true;
            this.rbRoutrTypeFact.Text = "Факт";
            this.rbRoutrTypeFact.UseVisualStyleBackColor = true;
            this.rbRoutrTypeFact.CheckedChanged += new System.EventHandler(this.rbRoutrTypeFact_CheckedChanged);
            // 
            // tbMerchID
            // 
            this.tbMerchID.Location = new System.Drawing.Point(14, 63);
            this.tbMerchID.MaxLength = 12;
            this.tbMerchID.Name = "tbMerchID";
            this.tbMerchID.Size = new System.Drawing.Size(139, 20);
            this.tbMerchID.TabIndex = 21;
            this.tbMerchID.Text = "10000013";
            // 
            // lblMerchID
            // 
            this.lblMerchID.AutoSize = true;
            this.lblMerchID.Location = new System.Drawing.Point(11, 47);
            this.lblMerchID.Name = "lblMerchID";
            this.lblMerchID.Size = new System.Drawing.Size(52, 13);
            this.lblMerchID.TabIndex = 20;
            this.lblMerchID.Text = "Merch_Id";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(11, 8);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 13);
            this.lblDate.TabIndex = 19;
            this.lblDate.Text = "Дата";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "yyyy-MM-dd";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(14, 24);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(139, 20);
            this.dtpDate.TabIndex = 18;
            this.dtpDate.Value = new System.DateTime(2013, 6, 10, 0, 0, 0, 0);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gmap);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(598, 488);
            this.panel1.TabIndex = 19;
            // 
            // gmap
            // 
            this.gmap.AutoSize = true;
            this.gmap.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gmap.Bearing = 0F;
            this.gmap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gmap.CanDragMap = true;
            this.gmap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gmap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(0, 0);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 20;
            this.gmap.MinZoom = 4;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(598, 488);
            this.gmap.TabIndex = 1;
            this.gmap.Zoom = 6D;
            // 
            // GeoMasterWin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(790, 488);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelTools);
            this.Name = "GeoMasterWin";
            this.Text = "GeoMaster";
            this.Load += new System.EventHandler(this.GeoMasterWin_Load);
            this.panelTools.ResumeLayout(false);
            this.panelTools.PerformLayout();
            this.gbIsOpt.ResumeLayout(false);
            this.gbIsOpt.PerformLayout();
            this.gbRouteType.ResumeLayout(false);
            this.gbRouteType.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTools;
        private System.Windows.Forms.Button butCalculate;
        private System.Windows.Forms.GroupBox gbRouteType;
        private System.Windows.Forms.RadioButton rbRouteTypePlan;
        private System.Windows.Forms.RadioButton rbRoutrTypeFact;
        private System.Windows.Forms.TextBox tbMerchID;
        private System.Windows.Forms.Label lblMerchID;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Panel panel1;
        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.GroupBox gbIsOpt;
        private System.Windows.Forms.RadioButton rbOptNo;
        private System.Windows.Forms.RadioButton rbOptYes;
        private System.Windows.Forms.Label lblDistances;
        private System.Windows.Forms.ListBox lbEdges;

    }
}

