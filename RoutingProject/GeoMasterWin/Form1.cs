﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GMap.NET.MapProviders;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

using System.Net;

using GeoMasterWinApp.WcfRouterService;
using WcfRouterCommon;

namespace GeoMasterWinApp
{
    public partial class GeoMasterWin : Form
    {
        private GMapOverlay overlayRoute = new GMapOverlay("route");
        private GMapOverlay overlaySegment = new GMapOverlay("segment");
        private GMapOverlay overlayMarkers = new GMapOverlay("markers");
        private GMapOverlay overlayNumbers = new GMapOverlay("numbers");
        Dictionary<string, GeoPoint> Points = new Dictionary<string, GeoPoint>();


        private RouteResult plan;
        private RouteResult fact;

        private int CurrentEdge = 0;

        public GeoMasterWin()
        {
            InitializeComponent();
        }

        private void GeoMasterWin_Load(object sender, EventArgs e)
        {
            // Initialize map:
            GMapProvider.WebProxy = WebRequest.DefaultWebProxy;

            gmap.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gmap.SetPositionByKeywords("Ukraine");
            //gmap.Position = new PointLatLng(-25.971684, 32.589759);

            gmap.Overlays.Add(overlayRoute);
            gmap.Overlays.Add(overlaySegment);
            gmap.Overlays.Add(overlayMarkers);
            gmap.Overlays.Add(overlayNumbers);
        }

        private void butCalculate_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try {
                long MerchId = 0;

                bool res;

                res = long.TryParse(tbMerchID.Text, out MerchId);
                if (!res)
                {
                    MessageBox.Show("Не могу считать номер ТП");
                    return;
                }

                DateTime StartDate = dtpDate.Value;

                MerchRouteParams param = new MerchRouteParams()
                    {
                        Merch_ID = MerchId,
                        RouteDate = StartDate,
                        RouteType = 0 //fact
                    };

                RouterServiceClient client = new RouterServiceClient();

                RouteResult rres = client.BuildMerchRoute(param);

                if (rres.ErrorCode < 0)
                {
                    MessageBox.Show(String.Format("Ошибка c кодом {0}\r\n{1}", rres.ErrorCode, rres.ErrorMessage));
                    //client.Close();
                    //return;
                }

                fact = rres;
                param.RouteType = 1; //plan
                rres = client.BuildMerchRoute(param);
                if (rres.ErrorCode < 0)
                {
                    MessageBox.Show(String.Format("Ошибка c кодом {0}\r\n{1}", rres.ErrorCode, rres.ErrorMessage));
                    //client.Close();
                    //return;
                }
                plan = rres;
                
                client.Close();

                ShowRoute();

                lblDistances.Text = string.Format("Факт ({0} TT): \r\n  исх. {1,8:F} м\r\n  опт. {2,8:F} м\r\nПлан ({3} TT): \r\n  исх. {4,8:F} м\r\n  опт. {5,8:F} м", 
                    fact.SourcePoints.Length, 
                    fact.SourceDistance, 
                    fact.OptimizedDistance,
                    plan.SourcePoints.Length,
                    plan.SourceDistance, 
                    plan.OptimizedDistance);

            }
            finally 
            {
                Cursor.Current = Cursors.Default;
            }
            

        }

        private void ShowRoute()
        {
            if (fact == null || plan == null)
                return;

            RouteResult res = plan;
            if (rbRoutrTypeFact.Checked)
                res = fact;

            if (res.OptimizedRoute == null)
            {
                rbOptNo.Checked = true;
                gbIsOpt.Enabled = false;
            }

            //перепакуем список GeoCoordinate в список PointLatLng для GMAP
            List<PointLatLng> route;
            string routeName;
            if (rbOptYes.Checked)
            {
                overlayRoute.Routes.Clear();
                List<string> edges = new List<string>();
                if (res.OptRouteSegments != null)
                {
                    
                    GMapRoute r;
                    for (int i = 0; i < res.OptRouteSegments.Count; i++)
                    {
                        route = res.OptRouteSegments[i].Select(point => new PointLatLng(point.Lat, point.Lon)).ToList();
                        routeName = string.Format("{0} -> {1}", i, i + 1);
                        edges.Add(routeName);
                        r = new GMapRoute(route, routeName);
                        r.Tag = routeName;
                        r.Stroke.Width = 4;
                        r.Stroke.LineJoin = LineJoin.Round;
                        r.Stroke.StartCap = LineCap.NoAnchor;
                        r.Stroke.CustomEndCap = new AdjustableArrowCap(3, 3);
                        overlayRoute.Routes.Add(r);
                    }
                }
                lbEdges.DataSource = edges;
            }
            else //not optimized
            {
                List<string> edges = new List<string>(res.SourceRoute.Count);

                overlayRoute.Routes.Clear();
                GMapRoute r;
                for (int i = 0; i < res.SourceRoute.Count; i++ )
                {
                    routeName = string.Format("{0} -> {1}", i, i + 1);
                    edges.Add(routeName);

                    if (res.SourceRoute[i] == null)
                        continue;

                    route = res.SourceRoute[i].Select(point => new PointLatLng(point.Lat, point.Lon)).ToList();
                    r = new GMapRoute(route, routeName);
                    r.Tag = routeName;
                    r.Stroke.StartCap = LineCap.NoAnchor;
                    r.Stroke.CustomEndCap = new AdjustableArrowCap(3, 3);

                    overlayRoute.Routes.Add(r);
                }
                lbEdges.DataSource = edges;

            }

            CurrentEdge = 0;

            //добавим оверлей с маркерами точек
            GMarkerGoogle marker;
            MyMarker mm;
            int order;
            for(int i = 0; i<res.SourcePoints.Length; i++)
                //each (GeoPoint point in res.SourcePoints)
            {
                //order = rbOptYes.Checked?res.OptimizedOrder[i]:i;
                order = i;

                if (Points.ContainsKey(res.Labels[i]))
                {
                    ((MyMarker)overlayNumbers.Markers.First(m => m.ToolTipText == res.Labels[i])).Num = order;
                    continue;
                }

                Points.Add(res.Labels[i], res.SourcePoints[i]);

                marker = new GMarkerGoogle(new PointLatLng(res.SourcePoints[i].Lat, res.SourcePoints[i].Lon), res.Labels[i] == "start point"?GMarkerGoogleType.red_small:GMarkerGoogleType.green_small);
                marker.Tag = res.Labels[i];
                marker.ToolTip = new GMapToolTip(marker);
                marker.ToolTipMode = MarkerTooltipMode.OnMouseOver;
                marker.ToolTipText = res.Labels[i];
                overlayMarkers.Markers.Add(marker);

                mm = new MyMarker(new PointLatLng(res.SourcePoints[i].Lat, res.SourcePoints[i].Lon), order);
                mm.ToolTip = new GMapToolTip(mm);
                mm.ToolTipMode = MarkerTooltipMode.OnMouseOver;
                mm.ToolTipText = res.Labels[i];
                overlayNumbers.Markers.Add(mm);

            }

            if (overlayRoute.Routes.Count > 0 && null != overlayRoute.Routes[0])
                gmap.ZoomAndCenterRoute(overlayRoute.Routes[0]);

            //gmap.Invalidate();
            //gmap.Refresh();
        }

        private void rbRoutrTypeFact_CheckedChanged(object sender, EventArgs e)
        {
            if (fact.OptimizedRoute == null)
            {
                rbOptNo.Checked = true;
                gbIsOpt.Enabled = false;
            }
            ShowRoute();
        }

        private void rbRouteTypePlan_CheckedChanged(object sender, EventArgs e)
        {
            if (plan.OptimizedRoute == null)
            {
                rbOptNo.Checked = true;
                gbIsOpt.Enabled = false;
            }
            ShowRoute();
        }

        private void rbOptYes_CheckedChanged(object sender, EventArgs e)
        {
            ShowRoute();
        }

        private void rbOptNo_CheckedChanged(object sender, EventArgs e)
        {
            ShowRoute();
        }

        private void lbEdges_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (fact == null || plan == null)
                return;

            RouteResult res = plan;
            if (rbRoutrTypeFact.Checked)
                res = fact;

            CurrentEdge = lbEdges.SelectedIndex;
            List<PointLatLng> route = null;

            if (rbOptYes.Checked)
            {
                if (res.OptRouteSegments != null)
                    route = res.OptRouteSegments[CurrentEdge].Select(point => new PointLatLng(point.Lat, point.Lon)).ToList(); 
            }
            else
            {
                if (res.SourceRoute[CurrentEdge] != null)
                    route = res.SourceRoute[CurrentEdge].Select(point => new PointLatLng(point.Lat, point.Lon)).ToList();
            }

            overlaySegment.Clear();

            if (route != null)
            {
                GMapRoute r;
                r = new GMapRoute(route, "segment");
                r.Tag = "segment";
                r.Stroke.StartCap = LineCap.NoAnchor;
                r.Stroke.Color = Color.Yellow;
                r.Stroke.CustomEndCap = new AdjustableArrowCap(3, 3);
                overlaySegment.Routes.Add(r);
            }
        }

    }
}
