﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteMaster
{
    public class RouteWithOutletsModel
    {
        #region Fields

        private  List<OutletInRoute> _outlets = new List<OutletInRoute>(10);
        #endregion

        #region Constructors

        public RouteWithOutletsModel()
            : this(0)
        {
        }

        public RouteWithOutletsModel(long routeId)
        {
            RouteId = routeId;
        }

        public RouteWithOutletsModel(long routeId, List<OutletInRoute> outlets)
        {
            RouteId = routeId;
            _outlets = outlets;
        }

        #endregion

        public List<OutletInRoute> Outlets
        {
            get { return _outlets; }
            set { _outlets = value; }
        }

        public string ErrorMessage { get; set; }

        public int CountTt
        {
            get { return _outlets.Count; }
        }

        public decimal Distance { get; set; }

        public decimal OptDistance { get; set; }

        public bool IsOptimized { get; set; }

        public bool IsBuilt { get; set; }

        public bool IsRound
        {
            get { return (StartPointLatitude != null && StartPointLongitude!=null); }
        }

        public long RouteId { get; set; }

        public DateTime DLM { get; set; }

        public int ChckByOlId { get; set; }

        public int ChckByOlNumber { get; set; }

        public decimal? StartPointLatitude { get; set; }

        public decimal? StartPointLongitude { get; set; }
    }
}
