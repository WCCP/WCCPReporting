﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RouteMaster
{
    public class OutletInRoute
    {
        #region Fields

        private double? _lat;
        private double? _lon;

        #endregion

        #region Constructors

        public OutletInRoute()
            : this(0, 0)
        {
        }

        public OutletInRoute(long id, int olNum)
        {
            Id = id;
            OlNumber = olNum;
        }

        #endregion

        #region Properties

        public long Id { get; set; }

        public int OlNumber { get; set; }

        public double? Lat
        {
            get { return _lat; }
            set
            {
                if (value != null)
                {
                    _lat = value;
                }
            }
        }

        public double? Lon
        {
            get { return _lon; }
            set
            {
                if (value != null)
                {
                    _lon = value;
                }
            }
        }

        public bool HasCoordinates
        {
            get { return _lat != null && _lon != null; }
        }

        public long RouteId { get; set; }

        #endregion

    }
}
