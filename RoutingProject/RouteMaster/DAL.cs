﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.Data;

namespace RouteMaster
{
    static class DAL
    {
        private static DbManager _db;
        private static string _connectionString;
        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(_connectionString);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        public static void SetDbConnectionString(string constr)
        {
            _connectionString = constr;
        }

        public static DataTable GetChangedRoutes(int pid,DateTime? date = null, int? maxRouteCount = null)
        {
            return Db.SetSpCommand(DbConstants.SP_RMT_GET_CHANGET_ROUTES,
                Db.Parameter(DbConstants.PID, pid),
                Db.Parameter(DbConstants.PARAM_DATE, date),
                Db.Parameter(DbConstants.PARAM_MAX_ROUTES_COUNT, maxRouteCount)).ExecuteDataTable();
        }

        public static void SaveChangedRoutes(SqlXml data)
        {
            Db.SetSpCommand(DbConstants.SP_RMT_SAVE_CHANGET_ROUTES,
                Db.Parameter(DbConstants.DATA, data)).ExecuteNonQuery();
        }

        public static int DeleteNonActiveRoutes(int pid)
        {
           return Db.SetSpCommand(DbConstants.SP_RMT_ROUTE_DATA_CLEAN,
               Db.Parameter(DbConstants.PID, pid)).ExecuteNonQuery();
        }

        public static void ChangedRoutesInit()
        {
            Db.SetSpCommand(DbConstants.SP_RMT_CHANGET_ROUTES_INIT).ExecuteNonQuery();
        }
    }

    internal class DbConstants
    {
        public const string SP_RMT_CHANGET_ROUTES_INIT = "spDW_RMT_RouteMaster_Init";
        public const string SP_RMT_GET_CHANGET_ROUTES = "spDW_RMT_GET_ChangedRoutes_Central";
        public const string SP_RMT_SAVE_CHANGET_ROUTES = "spDW_RMT_Save_ChangedRoutes_Central";
        public const string SP_RMT_ROUTE_DATA_CLEAN = "spDW_RMT_Clean_OptRouteData";
        public const string PARAM_DATE = "@Date";
        public const string PARAM_MAX_ROUTES_COUNT = "@MaxRowCount";
        public const string PID = "@PID";
        public const string DATA = "@Data";
    }
}
