﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Text;
using NDesk.Options;
using System.Reflection;
using System.IO;
using System.Net.Configuration;
using System.ServiceModel.Configuration;
using System.Xml;
using WcfRouterCommon;
using RouteLib;
using OsmSharp.Tools.Output;
using NLog;
using NLog.Targets;
using NLog.Config;
using OsmSharp.Tools;

namespace RouteMaster
{
    class Program
    {
        static Logger logger = LogManager.GetLogger("RouteHelper");
        static int Main(string[] args)
        {

            LoggingConfiguration config = LogManager.Configuration;

            ColoredConsoleTarget consoleTarget = new ColoredConsoleTarget();
            consoleTarget.Layout = "${date:format=HH\\:MM\\:ss} ${message}";
            config.AddTarget("console", consoleTarget);
            LoggingRule rule1 = new LoggingRule("*", LogLevel.Trace, consoleTarget);
            config.LoggingRules.Add(rule1);

            LogManager.Configuration = config;

            Console.WriteLine("{0} v.{1}", Assembly.GetExecutingAssembly().GetName().Name, Assembly.GetExecutingAssembly().GetName().Version);

            #region Command-line parsing

            List<int> PIDs = new List<int>();
            DateTime? StartDate = null;
            Int32? MaxRowCount = null;
            string sStartDate = "";
            string maxRowCount = "";
            string subFromDate = "";
            bool showHelp = false;
            string ServerName = "";
            string DatabaseName = "";
            string UserName = "";
            string Password = "";
            bool ua = false;
            bool ru = false;

            var p = new OptionSet() 
             {
                 { "StartDate|sd=",  v => sStartDate = v },
                 { "subd=",      v => subFromDate = v},
                 { "srv=",           v => ServerName = v },
                 { "maxRC=",         v => maxRowCount = v},
                 { "db=",            v => DatabaseName = v },
                 { "u=",             v => UserName = v },
                 { "p=",             v => Password = v },
                 { "ua",             v => ua = v != null },
                 { "ru",             v => ru = v != null },
                 { "h|?|help",       v => showHelp = v != null },
             };

            List<string> extra;
            try
            {

                extra = p.Parse(args);

                if (sStartDate != "")
                    StartDate = DateTime.Parse(sStartDate).AddDays(subFromDate != "" ? -Convert.ToInt32(subFromDate) : 0);
                if (maxRowCount != "")
                    MaxRowCount = Int32.Parse(maxRowCount);
                if (ua && !ru)
                    PIDs.Add(0);
                if (ru && !ua)
                {
                    PIDs.Add(1);
                    PIDs.Add(2);
                }
                if (ua && ru)
                {
                    PIDs.Add(0);
                    PIDs.Add(1);
                    PIDs.Add(2);
                }
            }
            catch (OptionException e)
            {
                Console.Write("Ошибка в аргументах:");
                Console.WriteLine(e.Message);
                Console.WriteLine("Уакжите опцию -help для получения справки.");
                return -1;
            }

            if (extra.Count > 0)
            {
                Console.Write("Ошибка в аргументах, не распознана часть: '{0}'", string.Join(" ", extra.ToArray()));
                return -1;
            }

            if (showHelp || args.Length == 0)
            {
                Console.WriteLine("Чтобы пересчитать все измененные маршруты:");
                Console.WriteLine("  RouteMaster.exe -srv ServerName -db DB name -ua -ru");
                Console.WriteLine("  -srv и -db указывают на сервер и базу, в которой искать маршруты");
                Console.WriteLine("  -ua и -ru указывают на страну(-ы), для которой(-ых) будут пересчитываться маршруты");
                Console.WriteLine();
                Console.WriteLine("Чтобы пересчитать все измененные маршруты за указанную дату:");
                Console.WriteLine("  RouteMaster.exe -sd <Date> -srv ServerName -db DB name -ua -ru");
                Console.WriteLine("  -sd <Date> дата на  которую нужно пересчитать маршруты");
                Console.WriteLine("  -subd <Дней> Отнять от указанной даты");
                Console.WriteLine("  -srv и -db указывают на сервер и базу, в которой искать маршруты");
                Console.WriteLine("  -ua и -ru указывают на страну(-ы), для которой(-ых) будут пересчитываться маршруты");
                Console.WriteLine();
                Console.WriteLine("Чтобы пересчитать измененные маршруты за указанную дату в указанном количестве:");
                Console.WriteLine("  RouteMaster.exe -sd <Date> -maxRC <Count of routes> -srv ServerName -db DB name -ua -ru");
                Console.WriteLine("  -sd <Date> дата на  которую нужно пересчитать маршруты");
                Console.WriteLine("  -subd <Дней> Отнять от указанной даты");
                Console.WriteLine("  -maxRC <Count of routes> Максимальное количество маршрутов для пересчета");
                Console.WriteLine("  -srv и -db указывают на сервер и базу, в которой искать маршруты");
                Console.WriteLine("  -ua и -ru указывают на страну(-ы), для которой(-ых) будут пересчитываться маршруты");

                return -1;
            }


            if (!(ua || ru))
            {
                Console.WriteLine("Необходимо указать страну(-ы) для постройки маршрутов: -ua, -ru");
                return -1;
            }

            #endregion

            #region BL


            string connectionString;
            if (UserName.Length > 0)
                connectionString = string.Format("Server={0};Database={1};User Id={2};Password={3};", ServerName, DatabaseName, UserName, Password);
            else
                connectionString = string.Format("Server={0};Database={1};Trusted_Connection=true;", ServerName, DatabaseName);

            DAL.SetDbConnectionString(connectionString);
            DataTable routesTable = null;

            bool isPidDone = false;

            List<RouteWithOutletsModel> lstRoutes = null;
            logger.Trace("{0}  Инициализация маршрутов для пересчета...", DateTime.Now.ToString(CultureInfo.InvariantCulture));
            DAL.ChangedRoutesInit();

            foreach (var pid in PIDs)
            {
                logger.Trace("Начался перерасчет маршрутов по PID={0}",pid);
                do
                {
                    routesTable = DAL.GetChangedRoutes(pid, StartDate, MaxRowCount);
                    lstRoutes = CreateRoutesFromTable(routesTable);
                    isPidDone = MaxRowCount != null
                        ? (lstRoutes.Count < MaxRowCount)
                        : (lstRoutes.Count == 0);
                    ProcessRoutes(lstRoutes, pid == 1 || pid == 2);
                    SaveRoutesDataToDb(lstRoutes);
                }
                while (!isPidDone);
                logger.Trace("Закончился перерасчет маршрутов по PID={0}",pid);
                logger.Trace("Удаляем неактивные маршруты.");
                int delRoutsCount = DAL.DeleteNonActiveRoutes(pid);
                logger.Trace("Удалено {0} неактивных маршрутов.", delRoutsCount);
            }

            #endregion

            logger.Trace("{0},  Перерасчет маршрутов закончен...!", DateTime.Now.ToString(CultureInfo.InvariantCulture));
            return 1;
        }

        private static List<RouteWithOutletsModel> CreateRoutesFromTable(DataTable data)
        {
            List<OutletInRoute> lstOutlet =
                data.AsEnumerable()
                    .Select(row => new OutletInRoute()
                    {
                        RouteId = Convert.ToInt64(row["Route_id"]),
                        Id = Convert.ToInt64(row["OL_id"]),
                        OlNumber = Convert.ToInt32(row["OL_Number"]),
                        Lat = DbValueConvert(row["Latitude"]),
                        Lon = DbValueConvert(row["Longitude"])
                    })
                    .ToList();

            var routesTmp = data.AsEnumerable().Select(row => new RouteWithOutletsModel()
            {
                RouteId = row.Field<long>("Route_id"),
                DLM = row.Field<DateTime>("DLM"),
                ChckByOlId = row.Field<int>("NEW_CHCKByOL_id"),
                ChckByOlNumber = row.Field<int>("NEW_CHCKByOL_Number"),
                IsBuilt = row.Field<bool>("IsBuilt"),
                IsOptimized = row.Field<bool>("IsOptimized"),
                StartPointLatitude = row.Field<decimal?>("StartPoint_Latitude"),
                StartPointLongitude = row.Field<decimal?>("StartPoint_Longitude")
            }).ToList();

            List<RouteWithOutletsModel> routes = routesTmp.GroupBy(g => g.RouteId).Select(r => r.First()).ToList()
                .Select(rr =>
                {
                    rr.Outlets = lstOutlet.FindAll(o => o.RouteId == rr.RouteId);
                    return rr;
                }).ToList();
            return routes;
        }

        private static double? DbValueConvert(object val)
        {
            if (val != DBNull.Value)
            {
                return Convert.ToDouble(val);
            }
            return null;
        }

        private static void ProcessRoutes(List<RouteWithOutletsModel> lstrouts, bool isRu)
        {
            foreach (var route in lstrouts)
            {
                if (route.Outlets.Count <= 0)
                    continue;
                if (route.IsBuilt && route.IsOptimized)
                {
                    continue;
                }
                RouteParams lRouteParams;
                List<OutletInRoute> lWithoutCoordList;
                List<OutletInRoute> lSourceOutletsList;
                RoutingProviderHelper.BuildRouteParams(route, out lRouteParams, out lSourceOutletsList,
                    out lWithoutCoordList);
                lRouteParams.OptimizeIt = !route.IsOptimized;
                lRouteParams.IsRu = isRu;
                RouteResult lRouteResult = null;
                try
                {
                    DateTime start = DateTime.Now;
                    lRouteResult = RoutingProviderHelper.BuildRoute(lRouteParams);

                    if (lRouteResult == null)
                        continue;
                    logger.Trace(
                        "Маршрут Route_id={0},\tIsRound={1},\tWasBuild={2},\tWasOptimized={3},\tDistance={4},\tOptDistance={5},\tDuration={6} sec"
                        , route.RouteId, route.IsRound, route.IsBuilt, route.IsOptimized, lRouteResult.SourceDistance,
                        lRouteResult.OptimizedDistance, DateTime.Now.Subtract(start).Seconds);
                    route.Distance = (decimal)lRouteResult.SourceDistance;
                    route.OptDistance = (decimal)lRouteResult.OptimizedDistance;
                    route.ErrorMessage = lRouteResult.ErrorMessage;
                }
                catch (Exception lException)
                {
                    bool isResult = lRouteResult != null;
                    logger.Trace(
                        string.Format("Ошибка при расчете маршрута в RouteMaster.\nМаршрут: {0} \nКод: {1} \n{2}.",
                            route.RouteId, isResult ? lRouteResult.ErrorCode.ToString() : string.Empty,
                            isResult ? lRouteResult.ErrorMessage ?? lException.Message : lException.Message));
                }
            }
        }

        private static void SaveRoutesDataToDb(List<RouteWithOutletsModel> routes)
        {
            XmlWriterSettings lSettings = new XmlWriterSettings();
            lSettings.OmitXmlDeclaration = true;
            MemoryStream lStreamRoute = new MemoryStream(1024 * 10);
            XmlWriter lWriterRoute = XmlWriter.Create(lStreamRoute, lSettings);
            lWriterRoute.WriteStartElement("rows");

            foreach (var route in routes)
            {
                lWriterRoute.WriteStartElement("row");
                lWriterRoute.WriteAttributeString("Route_id", route.RouteId.ToString(CultureInfo.InvariantCulture));
                lWriterRoute.WriteAttributeString("CHCKByOL_id", route.ChckByOlId.ToString(CultureInfo.InvariantCulture));
                lWriterRoute.WriteAttributeString("CHCKByOL_Number", route.ChckByOlNumber.ToString(CultureInfo.InvariantCulture));
                lWriterRoute.WriteAttributeString("Distance", route.Distance.ToString(CultureInfo.InvariantCulture));
                lWriterRoute.WriteAttributeString("OptDistance", route.OptDistance.ToString(CultureInfo.InvariantCulture));
                lWriterRoute.WriteAttributeString("StartPoint_Latitude", route.StartPointLatitude != null ? ((decimal)route.StartPointLatitude).ToString(CultureInfo.InvariantCulture) : String.Empty);
                lWriterRoute.WriteAttributeString("StartPoint_Longitude", route.StartPointLongitude != null ? ((decimal)route.StartPointLongitude).ToString(CultureInfo.InvariantCulture) : String.Empty);
                lWriterRoute.WriteAttributeString("Error_Message", route.ErrorMessage != null ? route.ErrorMessage.ToString(CultureInfo.InvariantCulture) : String.Empty);
                lWriterRoute.WriteAttributeString("DLM", route.DLM.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
                lWriterRoute.WriteAttributeString("UpdateOnlyDLM", route.IsBuilt && route.IsOptimized ? 1.ToString(CultureInfo.InvariantCulture) : 0.ToString(CultureInfo.InvariantCulture));
                lWriterRoute.WriteAttributeString("UpdateOnlyDistance", !route.IsBuilt && route.IsOptimized ? 1.ToString(CultureInfo.InvariantCulture) : 0.ToString(CultureInfo.InvariantCulture));
                lWriterRoute.WriteEndElement();
            }
            lWriterRoute.WriteFullEndElement();
            lWriterRoute.Flush();
            lStreamRoute.Position = 0;
            SqlXml lXmlDataRoute = new SqlXml(lStreamRoute);

            DAL.SaveChangedRoutes(lXmlDataRoute);
        }
    }
}
