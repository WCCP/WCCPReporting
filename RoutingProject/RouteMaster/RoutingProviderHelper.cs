﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using NLog.Internal;
using WcfRouterCommon;
using RouteLib;

namespace RouteMaster
{
    public static class RoutingProviderHelper
    {
        private static object _geodataRu;
        private static object _geodataUa;
        private static object _geodata;
        private static string _lastMsg = String.Empty;
        private static int _lastErrorCode = 0;

        
        public static int LastErrorCode
        {
            get { return _lastErrorCode; }
        }

        public static string LastErrorMsg
        {
            get { return _lastMsg; }
        }

        public static string GetLastErrorMsg()
        {
            return _lastMsg;
        }

        static RoutingProviderHelper()
        {
           
        }

        public static RouteResult BuildRoute(RouteParams param)
        {
            _lastMsg = String.Empty;
            _lastErrorCode = 0;
            RouteResult lRouteResult = null;
            if (param.IsRu && _geodataRu == null)
            {
                _geodataRu = RouteHelper.ReadGeoData(param.IsRu);
                _geodata = _geodataRu;
                _geodataUa = null;
            }
            else if (!param.IsRu && _geodataUa == null)
            {
                _geodataUa = RouteHelper.ReadGeoData(param.IsRu);
                _geodata = _geodataUa;
                _geodataRu = null;
            }

            try
            {
                lRouteResult = RouteHelper.BuildRoute(param, _geodata);

                if (lRouteResult == null)
                    return null;

                _lastErrorCode = lRouteResult.ErrorCode;
                _lastMsg = String.Empty;
                if (lRouteResult.ErrorCode < 0)
                    _lastMsg = String.Format("Оптимизация завершена с ошибкой.\nКод: {0}\n{1}.", lRouteResult.ErrorCode,
                        lRouteResult.ErrorMessage);
            }
            catch (TypeInitializationException lException)
            {
                string str = lException.InnerException.Message;
                _lastMsg = lException.Message;
                _lastErrorCode = -1;
                throw;
            }
            return lRouteResult;
        }

        public static void BuildRouteParams(RouteWithOutletsModel route, out RouteParams routeParams,
                                             out List<OutletInRoute> sourceOutlets, out List<OutletInRoute> withoutCoordList)
        {
            routeParams = new RouteParams();
            routeParams.IsRound = false;
            int lCount = route.Outlets.Count;
            if (route.IsRound)
            {
                lCount++;
                routeParams.IsRound = true;
            }

            List<GeoPoint> lPointList = new List<GeoPoint>(lCount);
            if (route.IsRound)
            {
                GeoPoint lGeoPoint = new GeoPoint((double) route.StartPointLatitude, (double) route.StartPointLongitude);
                lPointList.Add(lGeoPoint);
            }

            sourceOutlets = new List<OutletInRoute>(route.Outlets.Count);
            withoutCoordList = new List<OutletInRoute>();
            for (int lI = 0; lI < route.Outlets.Count; lI++)
            {
                OutletInRoute lOutletInRoute = route.Outlets[lI];
                if (!lOutletInRoute.HasCoordinates)
                {
                    withoutCoordList.Add(lOutletInRoute);
                    continue;
                }
                GeoPoint lGeoPoint = new GeoPoint((double)lOutletInRoute.Lat,(double)lOutletInRoute.Lon);
                lPointList.Add(lGeoPoint);
                sourceOutlets.Add(lOutletInRoute);
            }
            routeParams.Points = lPointList.ToArray();
        }
    }
}
