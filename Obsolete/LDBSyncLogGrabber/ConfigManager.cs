﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace LDBSyncLogGrabber
{
    /// <summary>
    /// Get data from config file
    /// </summary>
    public static class ConfigManager
    {
        /// <summary>
        /// 
        /// </summary>
        public static string ResultFileName
        {
            get
            {
                return ConfigurationManager.AppSettings["ResultFileName"];
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static bool RebuildCommonLog
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["RebuildCommonLog"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static bool FTPOnLocalMachine
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["FTPOnLocalMachine"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static bool NewFileEveryDay
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["NewFileEveryDay"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string FTPURL
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPServer"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FTPUser
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPUser"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FTPPWD
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPPassword"];
            }
        }
    }
}
