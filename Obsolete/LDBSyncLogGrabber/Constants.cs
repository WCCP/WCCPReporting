﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDBSyncLogGrabber
{
    internal static class Constants
    {
        internal const string USER_ID = "Information:UserID: ";
        internal const string EXCEPTION = " Exception message: ";
        internal const string SYNC_VER = "Information:Sync Version: ";
    }
}
