﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDBSyncLogGrabber.Interfaces
{
    interface IFileManager
    {
        List<string> GetFilesList();
        string DownloadFileContent(string fileName, ref DownloadProcessor.LogRowInfo lri);
    }
}
