﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using LDBSyncLogGrabber.Interfaces;
using LDBSyncLogGrabber.Utils;

namespace LDBSyncLogGrabber
{
    internal class FtpManager : IFileManager
    {
        //private const string USER_ID = "Information:UserID: ";
        //private const string EXCEPTION = " Exception message: ";
        //private const string SYNC_VER = "Information:Sync Version: ";
        public List<string> GetFilesList()
        {
            List<string> result = new List<string>();

            try
            {
                FtpWebRequest request = GetFtpWebRequest(string.Empty);
                if (null != request)
                {
                    request.Method = WebRequestMethods.Ftp.ListDirectory;
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream responseStream = response.GetResponseStream();
                    StreamReader sr = new StreamReader(responseStream, Encoding.Default);
                    while (!sr.EndOfStream)
                    {
                        result.Add(sr.ReadLine());
                    }
                    responseStream.Close();
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public void DeleteFile(string fileName)
        {
            try
            {
                FtpWebRequest request = GetFtpWebRequest(fileName);
                if (null != request)
                {
                    request.Method = WebRequestMethods.Ftp.DeleteFile;
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream responseStream = response.GetResponseStream();
                    responseStream.Close();
                }
            }
            catch {}

        }
        
        public string DownloadFileContent(string fileName, ref DownloadProcessor.LogRowInfo lri)
        {
            string result = string.Empty;

            try
            {
                FtpWebRequest request = GetFtpWebRequest(fileName);
                if (null != request)
                {
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream responseStream = response.GetResponseStream();
                    StreamReader sr = new StreamReader(responseStream, Encoding.Default);
                    result = CustomUtils.GetCustomLogInfo(sr, ref lri);
                    sr.Close();
                    responseStream.Close();
                }
            }
            catch
            {
                result = string.Empty;
            }
            lri.Content = result;
            return result;
        }

        public bool DownloadFile(string fileName, string directory)
        {
            bool result = true;
            
            try
            {
                FtpWebRequest request = GetFtpWebRequest(fileName);
                if (null != request)
                {
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream responseStream = response.GetResponseStream();
                    FileStream fs = new FileStream(string.Format("{0}\\{1}", directory, fileName), FileMode.OpenOrCreate);
                    byte[] buffer = new byte[2047];
                    int read = 0;
                    do
                    {
                        read = responseStream.Read(buffer, 0, buffer.Length);
                        fs.Write(buffer, 0, read);
                    } while (read > 0);
                    responseStream.Close();
                    fs.Flush();
                    fs.Close();
                }
            }
            catch
            {
                result = false;
            }
            
            return result;
        }

        private FtpWebRequest GetFtpWebRequest(string ftpFileName)
        {
            string URI = ConfigManager.FTPURL;
            if (!string.IsNullOrEmpty(ftpFileName))
                URI += "/" + ftpFileName;
            FtpWebRequest request = FtpWebRequest.Create(URI) as FtpWebRequest;
            try
            {
                request.Credentials = new NetworkCredential(ConfigManager.FTPUser, ConfigManager.FTPPWD);
                request.UsePassive = false;
                request.UseBinary = true;
                request.KeepAlive = false;

            }
            catch
            {
                request = null;
            }

            return request;
        }
    }
}
