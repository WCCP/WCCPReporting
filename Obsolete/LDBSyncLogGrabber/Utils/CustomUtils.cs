﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LDBSyncLogGrabber.Utils
{
    internal static class CustomUtils
    {
        internal static string GetCustomLogInfo(StreamReader stream, ref DownloadProcessor.LogRowInfo lri)
        {
            string result = string.Empty;
            try
            {
                bool needUserID = true;
                bool needException = true;
                bool needSyncVersion = true;
                while (!stream.EndOfStream)
                {
                    string tmp = string.Empty;
                    tmp = stream.ReadLine();
                    if (needUserID && tmp.StartsWith(Constants.USER_ID))
                    {
                        lri.UserID = tmp.Substring(Constants.USER_ID.Length);
                        needUserID = false;
                    }
                    if (needException && tmp.StartsWith(Constants.EXCEPTION))
                    {
                        lri.Exception = tmp.Substring(Constants.EXCEPTION.Length);
                        needException = false;
                    }
                    if (needSyncVersion && tmp.StartsWith(Constants.SYNC_VER))
                    {
                        lri.SyncVersion = tmp.Substring(Constants.SYNC_VER.Length);
                        needSyncVersion = false;
                    }
                    result += tmp + " | ";
                }
            }
            catch { }
            return result;
        }
    }
}
