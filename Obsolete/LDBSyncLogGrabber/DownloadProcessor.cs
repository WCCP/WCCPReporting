﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using LDBSyncLogGrabber.Interfaces;

namespace LDBSyncLogGrabber
{
    internal class DownloadProcessor
    {
        private Dictionary<string, FileStream> streamPool = new Dictionary<string, FileStream>();
        private IFileManager fileManager = null;
        internal class LogRowInfo
        {
            private string delimeter = ":::";
            public string ID { get; set; }
            public string Name { get; set; }
            public string CompName { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string Status { get; set; }
            public string Content { get; set; }
            public string UserID { get; set; }
            public string SyncVersion { get; set; }
            public string Exception { get; set; }
            public LogRowInfo()
            {
                ID = string.Empty;
                Name = string.Empty;
                CompName = string.Empty;
                Date = string.Empty;
                Time = string.Empty;
                Status = string.Empty;
                Content = string.Empty;
                UserID = string.Empty;
                SyncVersion = string.Empty;
                Exception = string.Empty;
            }
            public string FormStringRecord()
            {
                string formatedDate = string.Format("{0}-{1}-{2}", Date.Substring(0, 4), Date.Substring(4, 2), Date.Substring(6, 2));
                string formatedTime = string.Format("{0}:{1}:{2}", Time.Substring(0, 2), Time.Substring(2, 2), Time.Substring(4, 2));
                return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};\n", Status, SyncVersion, UserID, ID, Name, formatedDate, formatedTime, CompName, Exception.Replace(";", ","), Content.Replace(";", ","));
            }
        }


        private const string C_STATUS = @"Sync_Status";
        private const string C_ID = @"Cust_ID";
        private const string C_NAME = @"Cust_Name"; 
        private const string C_DATE = @"Date";
        private const string C_TIME = @"Time"; 
        private const string C_COMP_NAME = @"Comp_Name";
        private const string C_SYNC_LOG = @"Sync_Log";
        private const string C_SYNC_USERID = @"UserID";
        private const string C_SYNC_VER = @"Sync Version";
        private const string C_EXCEPTION = @"Exceprion message";
        public DownloadProcessor()
        {
        }

        private string FormLogFileHeader()
        {
            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};\n", C_STATUS, C_SYNC_VER, C_SYNC_USERID, C_ID, C_NAME, C_DATE, C_TIME, C_COMP_NAME, C_EXCEPTION, C_SYNC_LOG);
        }
        private bool WriteInfo(LogRowInfo lri)
        {
            bool result = false;
            FileStream sw = GetStream(lri.Date);
            if (null != sw)
            {
                byte[] row = Encoding.Default.GetBytes(lri.FormStringRecord());
                sw.Write(row, 0, row.Length);
                result = true;
            }
            return result;
        }

        private FileStream GetStream(string date)
        {
            FileStream result = null;
            string directoty = Directory.GetCurrentDirectory();
            string fileName = ConfigManager.ResultFileName;
            if (ConfigManager.NewFileEveryDay)
                fileName = string.Format("{0}_{1}", date, fileName);

            if (streamPool.ContainsKey(fileName))
                result = streamPool[fileName];
            else
            {
                try
                {
                    string fullPath = string.Format(@"{0}\{1}", directoty, fileName);
                    bool needWriteHeader = !File.Exists(fullPath);
                    FileStream sw = null;
                    if (ConfigManager.RebuildCommonLog)
                    {
                        sw = new FileStream(fullPath, FileMode.Create);
                        needWriteHeader = true;
                    }
                    else
                        sw = new FileStream(fullPath, FileMode.OpenOrCreate);
                    sw.Seek(0, SeekOrigin.End);
                    if (needWriteHeader)
                    {
                        byte[] row = Encoding.Default.GetBytes(FormLogFileHeader());
                        sw.Write(row, 0, row.Length);
                    }
                    streamPool.Add(fileName, sw);
                    result = sw;
                }
                catch { }
            }
            return result;
        }

        private void CloseStreams()
        {
            foreach (string key in streamPool.Keys)
            {
                FileStream sw = streamPool[key];
                if (null != sw)
                {
                    try
                    {
                        sw.Flush();
                        sw.Close();
                    }
                    catch { }
                }
            }
        }

        public void Run()
        {
            if (!EnsureFileManager())
                return;

            List<string> files = fileManager.GetFilesList();
            if (null != files && files.Count > 0)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    string fileName = files[i];
                    LogRowInfo lri = GetLogRowInfo(fileName);
                    if (null != lri)
                    {
                        fileManager.DownloadFileContent(fileName, ref lri);
                        if (WriteInfo(lri))
                        {
                            //ftpManager.DeleteFile(fileName);
                        }
                    }
                }
                CloseStreams();
            }
        }

        private bool EnsureFileManager()
        {
            if (null == fileManager)
            {
                if (ConfigManager.FTPOnLocalMachine)
                    fileManager = new FileManager();
                else
                    fileManager = new FtpManager();
            }
            return (null !=fileManager);
        }

        private LogRowInfo GetLogRowInfo(string fileName)
        {
            LogRowInfo result = null;
            if (fileName.ToLower().EndsWith(".ldb"))
            {
                fileName = fileName.Substring(0, fileName.Length-4);

                string[] parts = fileName.TrimEnd().Split(new char[] {'_'});
                if (parts.Length == 5)
                {
                    result = new LogRowInfo();
                    result.ID = parts[0];
                    result.Name = parts[1];
                    result.CompName = parts[2];
                    result.Date = parts[3].Substring(0, 8);
                    result.Time = parts[3].Substring(8, 6);
                    result.Status = parts[4];
                }
                else if (parts.Length > 5)
                {
                    result = new LogRowInfo();
                    result.ID = parts[0];
                    result.Name = parts[1];
                    result.CompName = parts[2];
                    for (int i = 3; i < parts.Length - 2; i++)
                    {
                        result.CompName += string.Format("_{0}", parts[i]);
                    }
                    result.Date = parts[parts.Length - 2].Substring(0, 8);
                    result.Time = parts[parts.Length - 2].Substring(8, 6);
                    result.Status = parts[parts.Length - 1];
                }
            }
            return result;
        }
    }
}
