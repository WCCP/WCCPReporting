﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDBSyncLogGrabber
{
    class Program
    {
        static void Main(string[] args)
        {
            DownloadProcessor dp = new DownloadProcessor();
            dp.Run();
        }
    }
}
