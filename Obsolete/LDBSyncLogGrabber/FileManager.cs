﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LDBSyncLogGrabber.Interfaces;
using System.IO;
using LDBSyncLogGrabber.Utils;

namespace LDBSyncLogGrabber
{
    internal class FileManager : IFileManager
    {
        #region IFileManager Members

        public List<string> GetFilesList()
        {
            List<string> result = new List<string>();

            try
            {
                string workDirectory = ConfigManager.FTPURL;
                if (!workDirectory.EndsWith(@"\"))
                    workDirectory += @"\";
                string[] list = Directory.GetFiles(workDirectory, "*.ldb", SearchOption.TopDirectoryOnly);

                result.AddRange(
                    (from row in list
                    select row.TrimStart(workDirectory.ToCharArray())
                    ).AsQueryable<string>().ToList<string>()
                    );
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public string DownloadFileContent(string fileName, ref DownloadProcessor.LogRowInfo lri)
        {
            string res = string.Empty;
            string workDirectory = ConfigManager.FTPURL;
            if (!workDirectory.EndsWith(@"\"))
                workDirectory += @"\";
            string fullPath = workDirectory+fileName;
            if (File.Exists(fullPath))
            {
                try
                {
                    StreamReader sr = new StreamReader(fullPath, Encoding.Default);
                    res = CustomUtils.GetCustomLogInfo(sr, ref lri);
                    sr.Close();
                }
                catch { }
            }
            lri.Content = res;
            return res;
        }

        #endregion
    }
}
