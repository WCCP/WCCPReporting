﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.DataAccess;

namespace Logica.Reports
{
    public partial class CEDSettingsForm : XtraForm
    {
        #region Fields

        private bool hasWriteAccess;
        private SheetParamCollection parameterCollection;

        #endregion

        #region Constructors

        public CEDSettingsForm(bool hasWriteAccess, Guid tabId)
        {
            InitializeComponent();
            //Date
            lbDateTime.Text = Resource.Date;
            uiCbxDate.DateTime = DateTime.Now;
            //Chanel type
            lbChannelType.Text = Resource.ChannelType;
            DataSet ds2 = DataAccessLayer.ExecuteStoredProcedure(Resource.spChanelTypeList, new SqlParameter[] {});
            foreach (DataRow dr in ds2.Tables[0].Rows)
            {
                if (null == dr[Resource.spChanelTypeList_ChanelTypeID] || String.IsNullOrEmpty(dr[Resource.spChanelTypeList_ChanelTypeID].ToString()))
                {
                    continue;
                }
                uiCbxChannelType.Properties.Items.Add(new ComboBoxItem(dr[Resource.spChanelTypeList_ChanelTypeID], dr[Resource.spChanelTypeList_ChanelTypeName].ToString()));
            }
            uiCbxChannelType.SelectedIndex = 1;
            // StaffTree
            cbChannelType_SelectedIndexChanged(null, null);
            //Equipment
            lbEquipmentClass.Text = Resource.EquipmentClass;
            DataSet ds3 = DataAccessLayer.ExecuteStoredProcedure(Resource.spEquipmentClassList, new SqlParameter[] {});
            foreach (DataRow dr in ds3.Tables[0].Rows)
            {
                uiCbxEquipmentClass.Properties.Items.Add(new ComboBoxItem(dr[Resource.spEquipmentClassList_EquipmentClassID], dr[Resource.spEquipmentClassList_ClassName].ToString()));
            }
            uiCbxEquipmentClass.SelectedIndex = 0;
            HasWriteAccess = hasWriteAccess;
            TabId = tabId;
            uiRgPlanFact.Visible = hasWriteAccess;
        }

        #endregion

        #region Instance Properties

        public bool HasWriteAccess
        {
            get { return hasWriteAccess; }
            set { hasWriteAccess = value; }
        }

        /// <summary>
        ///   Gets a value indicating whether need to byild a fact report.
        /// </summary>
        /// <value><c>true</c> if this instance is fact; otherwise, <c>false</c>.</value>
        public bool IsFact
        {
            get
            {
                if (!hasWriteAccess)
                {
                    return true;
                }

                return uiRgPlanFact.SelectedIndex == 1;
            }
        }

        public SheetParamCollection ParameterCollection
        {
            get { return parameterCollection; }
            set { parameterCollection = value; }
        }

        public Guid TabId { get; set; }

        #endregion

        #region Instance Methods

        public new bool Validate()
        {
            bool isValid = false;
            if (IsFact)
            {
                if (uiCbxChannelType.SelectedItem == null || string.IsNullOrEmpty(uiCbxChannelType.SelectedItem.ToString()))
                {
                    MessageBox.Show(Resource.IncorrectChannel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (uiTlStaffLevel.Selection == null || 0 == uiTlStaffLevel.Selection.Count || string.IsNullOrEmpty(uiTlStaffLevel.Selection[0].ToString()))
                {
                    MessageBox.Show(Resource.IncorrectStaffLevel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (uiCbxEquipmentClass.SelectedItem == null || string.IsNullOrEmpty(uiCbxEquipmentClass.SelectedItem.ToString()))
                {
                    MessageBox.Show(Resource.IncorrectEquipmentClass, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    isValid = true;
                }
            }
            else
            {
                if (HasWriteAccess && uiRgPlanFact.SelectedIndex == -1)
                {
                    MessageBox.Show(Resource.PlanOrFactIsNotChosen, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (uiCbxEquipmentClass.SelectedItem == null || string.IsNullOrEmpty(uiCbxEquipmentClass.SelectedItem.ToString()))
                {
                    MessageBox.Show(Resource.IncorrectEquipmentClass, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    isValid = true;
                }
            }
            return isValid;
        }

        private void AddChildNodes(TreeListNode parent, DataTable table)
        {
            foreach (DataRow dr in table.Rows)
            {
                if ((null == parent && dr[Resource.spStaffTree_ParentID].Equals(0)) || (null != parent && dr[Resource.spStaffTree_ParentID].Equals(((IList<object>) parent.Tag)[0])))
                {
                    TreeListNode node = uiTlStaffLevel.AppendNode(new[] {dr[Resource.spStaffTree_Name]}, parent);
                    node.Tag = new List<object> {dr[Resource.spStaffTree_ItemID], dr[Resource.spStaffTree_StaffLevelID], dr[Resource.spStaffTree_ID]};
                    AddChildNodes(node, table);
                }
            }
        }

        private void BuildTree(object id)
        {
            uiTlStaffLevel.ClearNodes();
            DataAccessLayer.OpenConnection();
            DataSet ds1 = DataAccessLayer.ExecuteStoredProcedure(Resource.spStaffTree,
                                                                 null != id ? new[] {new SqlParameter(Resource.spStaffTree_Param_ChanelTypeID, id)} : new SqlParameter[] {});
            DataAccessLayer.CloseConnection();
            AddChildNodes(null, ds1.Tables[0]);
        }

        #endregion

        #region Event Handling

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }

            parameterCollection = new SheetParamCollection {TabId = TabId, TableDataType = IsFact ? TableType.Fact : TableType.Plan};
            parameterCollection.Add(new SheetParam
                                         {
                                             SqlParamName = Resource.SQL_Date,
                                             DisplayParamName = Resource.DSP_Date,
                                             DisplayValue = uiCbxDate.DateTime.Date.ToShortDateString(),
                                             Value = uiCbxDate.DateTime.Date
                                         });
            parameterCollection.Add(new SheetParam
                                         {
                                             SqlParamName = Resource.SQL_ChanelTypeID,
                                             DisplayParamName = IsFact ? Resource.DSP_ChanelTypeID : null,
                                             DisplayValue = ((ComboBoxItem) uiCbxChannelType.SelectedItem).Text,
                                             Value = ((ComboBoxItem) uiCbxChannelType.SelectedItem).Id
                                         });
            parameterCollection.Add(new SheetParam {SqlParamName = Resource.SQL_StaffLevelID, Value = ((IList<object>) uiTlStaffLevel.Selection[0].Tag)[1]});
            parameterCollection.Add(new SheetParam {SqlParamName = Resource.SQL_ID, Value = ((IList<object>) uiTlStaffLevel.Selection[0].Tag)[2]});
            parameterCollection.Add(new SheetParam
                                         {
                                             SqlParamName = Resource.SQL_EquipmentClassID,
                                             Value = ((ComboBoxItem) uiCbxEquipmentClass.SelectedItem).Id,
                                             DisplayParamName = Resource.DSP_EquipmentClassID,
                                             DisplayValue = ((ComboBoxItem) uiCbxEquipmentClass.SelectedItem).Text
                                         });
            parameterCollection.Add(new SheetParam {SqlParamName = Resource.SQL_Debug, Value = 0});
            DialogResult = DialogResult.OK;
        }

        private void cbChannelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (uiCbxChannelType.SelectedItem != null && uiCbxChannelType.Enabled)
            {
                BuildTree(((ComboBoxItem) uiCbxChannelType.SelectedItem).Id);
            }
            else
            {
                BuildTree(null);
            }
        }

        private void staffLevelTree_SelectionChanged(object sender, EventArgs e)
        {
            if (null != uiTlStaffLevel.Selection[0] && null != uiTlStaffLevel.Selection[0].Tag)
            {
                Cursor = Cursors.WaitCursor;
                Cursor = Cursors.Default;
            }
        }

        private void uiRgPlanFact_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool enabled = uiRgPlanFact.SelectedIndex != 0;
            //uiTlStaffLevel.Enabled = enabled;
            uiCbxChannelType.Enabled = enabled;
            cbChannelType_SelectedIndexChanged(null, null);
        }

        #endregion
    }
}