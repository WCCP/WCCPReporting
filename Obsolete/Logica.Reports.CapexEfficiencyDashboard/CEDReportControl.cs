﻿using System.Collections.Generic;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports;
using Logica.Reports.Common;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        private static readonly int REPORT_ID = WCCPAPI.GetReportId();
        readonly CEDSettingsForm setForm;
        public WccpUIControl() : base(REPORT_ID)
        {
            InitializeComponent();
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            setForm = new CEDSettingsForm(HasWriteAccess, Report.Tabs[0].Id);
            ShowSettingsForm();
        }

        private void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            ShowSettingsForm();
        }

        private void ShowSettingsForm()
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                UpdateSheets(new List<SheetParamCollection> { setForm.ParameterCollection });
            }
        }
    }
}
