using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace WccpReporting
{
    public static class WCCPAPI
    {
        [DllImport("wccpui.dll", EntryPoint = "GetConnectionString", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern string GetConnectionString();

        [DllImport("wccpui.dll", EntryPoint = "GetApplicationPath", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern string GetApplicationPath();

        [DllImport("wccpui.dll", EntryPoint = "SearchTab", CharSet = CharSet.None, CallingConvention = CallingConvention.StdCall)]
        public static extern int SearchTab(string TabCaption);

        [DllImport("wccpui.dll", EntryPoint = "CreateTab", CharSet = CharSet.None, CallingConvention = CallingConvention.StdCall)]
        public static extern int CreateTab(string TabCaption);

        [DllImport("wccpui.dll", EntryPoint = "CreateTabEx", CharSet = CharSet.None, CallingConvention = CallingConvention.StdCall)]
        public static extern int CreateTabEx(string TabCaption);

        [DllImport("wccpui.dll", EntryPoint = "GetDesktopReportScript", CallingConvention = CallingConvention.StdCall)]
        public static extern void GetDesktopReportScript(string fbFileName, int step, [MarshalAs(UnmanagedType.BStr)] ref String Result, ref int IsError);

        [DllImport("wccpui.dll", EntryPoint = "CheckVersionModule", CharSet = CharSet.None, CallingConvention = CallingConvention.StdCall)]
        public static extern void CheckVersionModule(string modulename, string modulefilename, string moduletype, int major, int minor, int release, int build);

    }

}
