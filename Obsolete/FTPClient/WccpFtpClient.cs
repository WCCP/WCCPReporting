using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Data;
using System.Threading;

namespace WccpReporting
{
    public class WccpUI
    {
        private string FTPServer= String.Empty;
        private DataSet dsUpdate= null;
        private DataTable tblModules= null;
        private DataColumn colID= null;
        private DataColumn colMODULENAME = null;
        private DataColumn colMODULEFILENAME = null;
        private DataColumn colMODULETYPE = null;
        private DataColumn colMAJOR = null;
        private DataColumn colMINOR = null;
        private DataColumn colRELEASE = null;
        private DataColumn colBUILD = null;

        private void InitWccpFtpClient()
        {
            dsUpdate = new DataSet();
            tblModules = new DataTable();
            colID = new DataColumn();
            colMODULENAME = new DataColumn();
            colMODULEFILENAME = new DataColumn();
            colMODULETYPE = new DataColumn();
            colMAJOR = new DataColumn();
            colMINOR = new DataColumn();
            colRELEASE = new DataColumn();
            colBUILD = new DataColumn();

            tblModules.TableName = "tblModules";
            colID.AllowDBNull = false;
            colID.ColumnName = "ID";
            colID.DataType = typeof(int);
            colMODULENAME.ColumnName = "MODULENAME";
            colMODULENAME.MaxLength = 254;
            colMODULEFILENAME.ColumnName = "MODULEFILENAME";
            colMODULEFILENAME.MaxLength = 254;
            colMODULETYPE.ColumnName = "MODULETYPE";
            colMODULETYPE.MaxLength = 32;
            colMAJOR.ColumnName = "MAJOR";
            colMAJOR.DataType = typeof(int);
            colMINOR.ColumnName = "MINOR";
            colMINOR.DataType = typeof(int);
            colRELEASE.ColumnName = "RELEASE";
            colRELEASE.DataType = typeof(int);
            colBUILD.ColumnName = "BUILD";
            colBUILD.DataType = typeof(int);
            tblModules.Columns.AddRange(new System.Data.DataColumn[] {
            colID,
            colMODULENAME,
            colMODULEFILENAME,
            colMODULETYPE,
            colMAJOR,
            colMINOR,
            colRELEASE,
            colBUILD});
            // 
            dsUpdate.Tables.AddRange(new System.Data.DataTable[] {
            tblModules});
        }

        public int WccpUpdate(string FtpServer, bool isAutUpdate, string autUserName, string autPassword, string autDomain, string oldversionFileList, string oldversionMainModule, bool IsCheckedModule)
        {
            WCCPConst.root = WCCPAPI.GetApplicationPath();
            using (UpdatingForm fmUpdatingForm = new UpdatingForm())
            {
                fmUpdatingForm.FtpServer = FtpServer;
                fmUpdatingForm.IsAutUpdate = isAutUpdate;
                fmUpdatingForm.AutUserName = autUserName;
                fmUpdatingForm.AutPassword = autPassword;
                fmUpdatingForm.AutDomain = autDomain;
                fmUpdatingForm.OldversionFileList = oldversionFileList;
                fmUpdatingForm.OldversionMainModule = oldversionMainModule;
                fmUpdatingForm.IsCheckedModule = IsCheckedModule;
                if (fmUpdatingForm.ShowDialog() == DialogResult.OK)
                {
                    if (fmUpdatingForm.IsUpdateEXE)
                      return 2;
                    else
                      return 1;
                }
                else
                  return 0;
            };
        }

        public int WccpUpdateStart(string FtpServer, bool isAutUpdate, string autUserName, string autPassword, string autDomain, string oldVersion)
        {
            int Result = 0;
            DataView dv=null;
            WCCPConst.root = WCCPAPI.GetApplicationPath();
            try
            {
                Uri serverUriWccpupdatever = new Uri(FtpServer + "/wccpupdatever.xml");
                if (Uri.CheckSchemeName(serverUriWccpupdatever.Scheme))
                {
                    using (WebClient request = new WebClient())
                    {
                        if (!isAutUpdate)
                        {
                            if (autDomain == "NONE")
                            {
                                request.Credentials = new NetworkCredential(autUserName, autPassword);
                            }
                            else
                            {
                                request.Credentials = new NetworkCredential(autUserName, autPassword, autDomain);
                            };
                        };

                        try
                        {
                            request.DownloadFile(serverUriWccpupdatever, "wccpupdatever.xml");
                            InitWccpFtpClient();
                            try
                            {
                                dsUpdate.Tables["tblModules"].Clear();
                                dsUpdate.Tables["tblModules"].ReadXml(WCCPConst.root + "wccpupdatever.xml");

                                dv = new DataView(dsUpdate.Tables["tblModules"]);
                                dv.RowFilter = "MODULEFILENAME = 'wccpreporting.exe'";

                                string version = dv[0]["MAJOR"] + "." + dv[0]["MINOR"] + "." + dv[0]["RELEASE"] + "." + dv[0]["BUILD"];

                                if (String.CompareOrdinal(oldVersion, version) < 0)
                                {
                                    Uri updateFile = new Uri(FtpServer + "/" + dv[0]["MODULEFILENAME"]);
                                    if (Uri.CheckSchemeName(updateFile.Scheme))
                                    {
                                        request.DownloadFile(updateFile, dv[0]["MODULEFILENAME"] + ".tmp");
                                        Result = 1;
                                    };
                                };
                            }
                            finally
                            {
                                if (dsUpdate != null)
                                {
                                    dsUpdate.Tables["tblModules"].Clear();
                                    dsUpdate.Dispose();
                                };

                                if (dv != null)
                                    dv.Dispose();
                            };
                        }
                        catch (WebException exw)
                        {
                            MessageBox.Show(exw.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    };

                    return Result;
                }
                else
                {
                    MessageBox.Show(WCCPConst.msgErrorURI, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return Result;
                };
            }
            catch (WebException exw)
            {
                MessageBox.Show(exw.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return Result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return Result;
            };

         }

    }
}
