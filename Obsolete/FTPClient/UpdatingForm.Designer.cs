namespace WccpReporting
{
    partial class UpdatingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbFtpServerName = new System.Windows.Forms.Label();
            this.btnFinishLoad = new System.Windows.Forms.Button();
            this.dsUpdate = new System.Data.DataSet();
            this.tblModules = new System.Data.DataTable();
            this.colID = new System.Data.DataColumn();
            this.colMODULENAME = new System.Data.DataColumn();
            this.colMODULEFILENAME = new System.Data.DataColumn();
            this.colMODULETYPE = new System.Data.DataColumn();
            this.colMAJOR = new System.Data.DataColumn();
            this.colMINOR = new System.Data.DataColumn();
            this.colRELEASE = new System.Data.DataColumn();
            this.colBUILD = new System.Data.DataColumn();
            this.tblSuccUpdate = new System.Data.DataTable();
            this.colSuccMODULEFILENAME = new System.Data.DataColumn();
            this.colSuccMODULEFILENAME_ORIG = new System.Data.DataColumn();
            this.tblOldModules = new System.Data.DataTable();
            this.colOldModuleFileName = new System.Data.DataColumn();
            this.colOldVersion = new System.Data.DataColumn();
            this.pbStatusUpdate = new System.Windows.Forms.ProgressBar();
            this.lbStatus = new System.Windows.Forms.TextBox();
            this.lbStatusDownLoad = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dsUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblModules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSuccUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblOldModules)).BeginInit();
            this.SuspendLayout();
            // 
            // lbFtpServerName
            // 
            this.lbFtpServerName.AutoSize = true;
            this.lbFtpServerName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbFtpServerName.Location = new System.Drawing.Point(12, 9);
            this.lbFtpServerName.Name = "lbFtpServerName";
            this.lbFtpServerName.Size = new System.Drawing.Size(143, 13);
            this.lbFtpServerName.TabIndex = 0;
            this.lbFtpServerName.Text = "���������� ������� � ";
            // 
            // btnFinishLoad
            // 
            this.btnFinishLoad.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnFinishLoad.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFinishLoad.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFinishLoad.Location = new System.Drawing.Point(197, 158);
            this.btnFinishLoad.Name = "btnFinishLoad";
            this.btnFinishLoad.Size = new System.Drawing.Size(75, 23);
            this.btnFinishLoad.TabIndex = 3;
            this.btnFinishLoad.Text = "�������";
            this.btnFinishLoad.UseVisualStyleBackColor = true;
            // 
            // dsUpdate
            // 
            this.dsUpdate.DataSetName = "UpdateDataSet";
            this.dsUpdate.Tables.AddRange(new System.Data.DataTable[] {
            this.tblModules,
            this.tblSuccUpdate,
            this.tblOldModules});
            // 
            // tblModules
            // 
            this.tblModules.Columns.AddRange(new System.Data.DataColumn[] {
            this.colID,
            this.colMODULENAME,
            this.colMODULEFILENAME,
            this.colMODULETYPE,
            this.colMAJOR,
            this.colMINOR,
            this.colRELEASE,
            this.colBUILD});
            this.tblModules.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ID"}, false)});
            this.tblModules.TableName = "tblModules";
            // 
            // colID
            // 
            this.colID.AllowDBNull = false;
            this.colID.ColumnName = "ID";
            this.colID.DataType = typeof(int);
            // 
            // colMODULENAME
            // 
            this.colMODULENAME.ColumnName = "MODULENAME";
            this.colMODULENAME.MaxLength = 254;
            // 
            // colMODULEFILENAME
            // 
            this.colMODULEFILENAME.ColumnName = "MODULEFILENAME";
            this.colMODULEFILENAME.MaxLength = 254;
            // 
            // colMODULETYPE
            // 
            this.colMODULETYPE.ColumnName = "MODULETYPE";
            this.colMODULETYPE.MaxLength = 32;
            // 
            // colMAJOR
            // 
            this.colMAJOR.ColumnName = "MAJOR";
            this.colMAJOR.DataType = typeof(int);
            // 
            // colMINOR
            // 
            this.colMINOR.ColumnName = "MINOR";
            this.colMINOR.DataType = typeof(int);
            // 
            // colRELEASE
            // 
            this.colRELEASE.ColumnName = "RELEASE";
            this.colRELEASE.DataType = typeof(int);
            // 
            // colBUILD
            // 
            this.colBUILD.ColumnName = "BUILD";
            this.colBUILD.DataType = typeof(int);
            // 
            // tblSuccUpdate
            // 
            this.tblSuccUpdate.Columns.AddRange(new System.Data.DataColumn[] {
            this.colSuccMODULEFILENAME,
            this.colSuccMODULEFILENAME_ORIG});
            this.tblSuccUpdate.TableName = "tblSuccUpdate";
            // 
            // colSuccMODULEFILENAME
            // 
            this.colSuccMODULEFILENAME.ColumnName = "MODULEFILENAME_TMP";
            this.colSuccMODULEFILENAME.MaxLength = 254;
            // 
            // colSuccMODULEFILENAME_ORIG
            // 
            this.colSuccMODULEFILENAME_ORIG.ColumnName = "MODULEFILENAME_ORIG";
            this.colSuccMODULEFILENAME_ORIG.MaxLength = 254;
            // 
            // tblOldModules
            // 
            this.tblOldModules.Columns.AddRange(new System.Data.DataColumn[] {
            this.colOldModuleFileName,
            this.colOldVersion});
            this.tblOldModules.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "MODULEFILENAME"}, false)});
            this.tblOldModules.TableName = "tblOldModules";
            // 
            // colOldModuleFileName
            // 
            this.colOldModuleFileName.ColumnName = "MODULEFILENAME";
            this.colOldModuleFileName.MaxLength = 254;
            // 
            // colOldVersion
            // 
            this.colOldVersion.ColumnName = "VERSION";
            this.colOldVersion.MaxLength = 254;
            // 
            // pbStatusUpdate
            // 
            this.pbStatusUpdate.Location = new System.Drawing.Point(14, 109);
            this.pbStatusUpdate.Maximum = 0;
            this.pbStatusUpdate.Name = "pbStatusUpdate";
            this.pbStatusUpdate.Size = new System.Drawing.Size(443, 23);
            this.pbStatusUpdate.Step = 1;
            this.pbStatusUpdate.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbStatusUpdate.TabIndex = 2;
            // 
            // lbStatus
            // 
            this.lbStatus.BackColor = System.Drawing.SystemColors.Window;
            this.lbStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbStatus.ForeColor = System.Drawing.Color.Maroon;
            this.lbStatus.Location = new System.Drawing.Point(14, 59);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.ReadOnly = true;
            this.lbStatus.Size = new System.Drawing.Size(443, 21);
            this.lbStatus.TabIndex = 1;
            this.lbStatus.TabStop = false;
            // 
            // lbStatusDownLoad
            // 
            this.lbStatusDownLoad.AutoSize = true;
            this.lbStatusDownLoad.Location = new System.Drawing.Point(14, 83);
            this.lbStatusDownLoad.Name = "lbStatusDownLoad";
            this.lbStatusDownLoad.Size = new System.Drawing.Size(22, 13);
            this.lbStatusDownLoad.TabIndex = 4;
            this.lbStatusDownLoad.Text = "OK";
            // 
            // UpdatingForm
            // 
            this.AcceptButton = this.btnFinishLoad;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 190);
            this.ControlBox = false;
            this.Controls.Add(this.lbStatusDownLoad);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.pbStatusUpdate);
            this.Controls.Add(this.btnFinishLoad);
            this.Controls.Add(this.lbFtpServerName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdatingForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������� ���������";
            this.Load += new System.EventHandler(this.UpdatingForm_Load);
            this.Shown += new System.EventHandler(this.UpdatingForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dsUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblModules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSuccUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblOldModules)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbFtpServerName;
        private System.Windows.Forms.Button btnFinishLoad;
        private System.Data.DataSet dsUpdate;
        private System.Data.DataTable tblModules;
        private System.Data.DataColumn colID;
        private System.Data.DataColumn colMODULENAME;
        private System.Data.DataColumn colMODULEFILENAME;
        private System.Data.DataColumn colMODULETYPE;
        private System.Data.DataColumn colMAJOR;
        private System.Data.DataColumn colMINOR;
        private System.Data.DataColumn colRELEASE;
        private System.Data.DataColumn colBUILD;
        private System.Data.DataTable tblSuccUpdate;
        private System.Data.DataColumn colSuccMODULEFILENAME;
        private System.Data.DataColumn colSuccMODULEFILENAME_ORIG;
        private System.Windows.Forms.ProgressBar pbStatusUpdate;
        private System.Data.DataTable tblOldModules;
        private System.Data.DataColumn colOldModuleFileName;
        private System.Data.DataColumn colOldVersion;
        private System.Windows.Forms.TextBox lbStatus;
        private System.Windows.Forms.Label lbStatusDownLoad;
    }
}