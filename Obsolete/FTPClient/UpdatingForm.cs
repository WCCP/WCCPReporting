using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Collections;

namespace WccpReporting
{
    public partial class UpdatingForm : Form
    {
        private string _FtpServer;
        public string FtpServer
        {
            get { return _FtpServer; }
            set { _FtpServer = value; lbFtpServerName.Text = lbFtpServerName.Text + value; }
        }
        private bool _IsUpdateEXE;
        public bool IsUpdateEXE
        {
            get { return _IsUpdateEXE; }
            set { _IsUpdateEXE = value; }
        }
        private string _oldversionMainModule;
        public string OldversionMainModule
        {
            get { return _oldversionMainModule; }
            set { _oldversionMainModule = value; }
        }
        private string _oldversionFileList;
        public string OldversionFileList
        {
            get { return _oldversionFileList; }
            set { _oldversionFileList = value;

               dsUpdate.Tables["tblOldModules"].Clear();
               string oldversionFileList = value;
               string fileName = String.Empty;
               string fileVersion = String.Empty;
               
               while (true)
               {
                   try
                   {
                       if (oldversionFileList.IndexOf("|") < 0)
                       {
                           break;
                       }
                       else
                       {
                           fileName = oldversionFileList.Substring(0, oldversionFileList.IndexOf(";"));
                           fileVersion = oldversionFileList.Substring(oldversionFileList.IndexOf(";") + 1, (oldversionFileList.IndexOf("|") - 1) - oldversionFileList.IndexOf(";"));
                           oldversionFileList = oldversionFileList.Remove(0, oldversionFileList.IndexOf("|")+1);
                           DataRow newRow = dsUpdate.Tables["tblOldModules"].NewRow();
                           newRow["MODULEFILENAME"] = fileName;
                           newRow["VERSION"] = fileVersion;
                           dsUpdate.Tables["tblOldModules"].Rows.Add(newRow);
                       };
                   }
                   catch (Exception ex)
                   {
                       MessageBox.Show(ex.Message);
                       dsUpdate.Tables["tblOldModules"].Clear();
                       break;
                   };
               };
            }
        }
        private bool _isAutUpdate;
        public bool IsAutUpdate
        {
            get { return _isAutUpdate; }
            set { _isAutUpdate = value; }
        }
        private string _autUserName;
        public string AutUserName
        {
            get { return _autUserName; }
            set { _autUserName = value; }
        }
        private string _autPassword;
        public string AutPassword
        {
            get { return _autPassword; }
            set { _autPassword = value; }
        }
        private string _autDomain;
        public string AutDomain
        {
            get { return _autDomain; }
            set { _autDomain = value; }
        }

        private bool _IsCheckedModule;
        public bool IsCheckedModule
        {
            get { return _IsCheckedModule; }
            set { _IsCheckedModule = value; }
        }

        public UpdatingForm()
        {
            InitializeComponent();
        }

        private void UpdatingForm_Load(object sender, EventArgs e)
        {
            IsUpdateEXE = false;
            lbStatus.Text = String.Empty;
            lbStatusDownLoad.Text = String.Empty;
            btnFinishLoad.Visible = false;
        }

        //���������� � �������
        public void WCCPUpdating()
        {
            DataView dvModules = null;
            DataView dvOldModules = new DataView(dsUpdate.Tables["tblOldModules"]);
            try
            {
                Uri serverUriWccpupdatever = new Uri(FtpServer + "/wccpupdatever.xml");
                if (Uri.CheckSchemeName(serverUriWccpupdatever.Scheme))
                {
                    using (WebClient request = new WebClient())
                    {
                        if (!IsAutUpdate)
                        {
                            if (AutDomain == "NONE")
                            {
                                request.Credentials = new NetworkCredential(AutUserName, AutPassword);
                            }
                            else
                            {
                                request.Credentials = new NetworkCredential(AutUserName, AutPassword, AutDomain);
                            };
                        };
                        lbStatus.Text = "�������� ������� ����� ������ �������...";
                        lbStatus.Update();
                        try
                        {
                            request.DownloadFile(serverUriWccpupdatever, "wccpupdatever.xml");

                            dsUpdate.Tables["tblModules"].Clear();
                            dsUpdate.Tables["tblSuccUpdate"].Clear();
                            dsUpdate.Tables["tblModules"].ReadXml(WCCPConst.root + "wccpupdatever.xml");

                            //��� ������
                            if (!IsCheckedModule)
                            {
                                dvModules = new DataView(dsUpdate.Tables["tblModules"]);
                            }
                            else
                            {
                                DataView dvtblOldModulesFilter = new DataView(dsUpdate.Tables["tblOldModules"]);

                                ArrayList arRowToDel = new ArrayList();

                                foreach (DataRow row in dsUpdate.Tables["tblModules"].Rows)
                                {
                                    if (Convert.ToString(row["MODULEFILENAME"]).ToLower() != "wccpreporting.exe")
                                    {
                                        dvtblOldModulesFilter.RowFilter = "MODULEFILENAME = '" + Convert.ToString(row["MODULEFILENAME"] + "'");
                                        if (dvtblOldModulesFilter.Count == 0)
                                        {
                                            arRowToDel.Add(row);
                                        };
                                    };
                                };

                                foreach (Object item in arRowToDel)
                                {
                                    dsUpdate.Tables["tblModules"].Rows.Remove((DataRow)item);
                                };

                                dvModules = new DataView(dsUpdate.Tables["tblModules"]);
                            };
                            //

                            //��������� ������� ����� ������ ��� ��. ������
                            dvModules.RowFilter = "MODULEFILENAME = 'wccpreporting.exe'";
                            if (dvModules.Count > 0)
                            {
                                string versionMainModule = dvModules[0]["MAJOR"] + "." + dvModules[0]["MINOR"] + "." + dvModules[0]["RELEASE"] + "." + dvModules[0]["BUILD"];
                                if (String.CompareOrdinal(OldversionMainModule, versionMainModule) < 0)
                                {
                                    Uri updateMainModule = new Uri(FtpServer + "/" + dvModules[0]["MODULEFILENAME"]);
                                    if (Uri.CheckSchemeName(updateMainModule.Scheme))
                                    {
                                        lbStatus.Text = "��������� " + dvModules[0]["MODULEFILENAME"];
                                        lbStatus.Update();

                                        request.DownloadFile(updateMainModule, dvModules[0]["MODULEFILENAME"] + ".tmp");
                                        IsUpdateEXE = true;
                                    };
                                };
                            };
                            dvModules.RowFilter = String.Empty;

                            if (!IsUpdateEXE)
                            {
                                try
                                {
                                    int indexUpdate = 0;
                                    if (dvModules.Count > 0)
                                        pbStatusUpdate.Maximum = dvModules.Count - 1;
                                    else
                                        pbStatusUpdate.Maximum = dvModules.Count;

                                    dvModules.RowFilter = "MODULEFILENAME <> 'wccpreporting.exe'";

                                    foreach (DataRowView row in dvModules)
                                    {
                                        string version_old = String.Empty;
                                        string version = row["MAJOR"] + "." + row["MINOR"] + "." + row["RELEASE"] + "." + row["BUILD"];

                                        dvOldModules.RowFilter = "MODULEFILENAME = '" + row["MODULEFILENAME"] + "'";
                                        if (dvOldModules.Count > 0)
                                        {
                                            version_old = Convert.ToString(dvOldModules[0]["VERSION"]);
                                        };

                                        if (String.CompareOrdinal(version_old, version) < 0 || !File.Exists(WCCPConst.root + row["MODULEFILENAME"]))
                                        {
                                            WCCPAPI.CheckVersionModule(Convert.ToString(row["MODULENAME"]), Convert.ToString(row["MODULEFILENAME"]), Convert.ToString(row["MODULETYPE"]),  
                                               Convert.ToInt32(row["MAJOR"]), Convert.ToInt32(row["MINOR"]), Convert.ToInt32(row["RELEASE"]), Convert.ToInt32(row["BUILD"]));
                                                
                                            Uri updateFile = new Uri(FtpServer + "/" + row["MODULEFILENAME"]);
                                            if (Uri.CheckSchemeName(updateFile.Scheme))
                                            {
                                                lbStatus.Text = "��������� " + row["MODULEFILENAME"];
                                                lbStatus.Update();
                                                try
                                                {
                                                    request.DownloadFile(updateFile, row["MODULEFILENAME"] + ".tmp");

                                                    lbStatusDownLoad.Text = "�������� " + row["MODULEFILENAME"];
                                                    lbStatusDownLoad.Update();

                                                    if (File.Exists(WCCPConst.root + row["MODULEFILENAME"] + ".tmp"))
                                                    {
                                                        DataRow newRow = dsUpdate.Tables["tblSuccUpdate"].NewRow();
                                                        newRow["MODULEFILENAME_TMP"] = row["MODULEFILENAME"] + ".tmp";
                                                        newRow["MODULEFILENAME_ORIG"] = row["MODULEFILENAME"];
                                                        dsUpdate.Tables[1].Rows.Add(newRow);
                                                    };
                                                }
                                                catch
                                                {
                                                    lbStatusDownLoad.Text = "�� ������ " + row["MODULEFILENAME"];
                                                    lbStatusDownLoad.Update();
                                                    Application.DoEvents();
                                                };
                                            };
                                        };

                                        ++indexUpdate;
                                        pbStatusUpdate.Value = indexUpdate;
                                        pbStatusUpdate.Update();
                                    };
                                    //

                                    foreach (DataRow row in dsUpdate.Tables["tblSuccUpdate"].Rows)
                                    {
                                        lbStatus.Text = "��������� " + row["MODULEFILENAME_ORIG"];
                                        lbStatus.Update();

                                        if (File.Exists(WCCPConst.root + row["MODULEFILENAME_TMP"]))
                                        {
                                            if (String.CompareOrdinal("wccpreporting.exe", Convert.ToString(row["MODULEFILENAME_ORIG"])) != 0)
                                            {
                                                //�����������
                                                File.Copy(WCCPConst.root + row["MODULEFILENAME_TMP"], WCCPConst.root + row["MODULEFILENAME_ORIG"], true);
                                                //������� tmp ����
                                                File.Delete(WCCPConst.root + row["MODULEFILENAME_TMP"]);
                                            };

                                        };
                                    };

                                    lbStatus.Text = "���������� ������ ���������";
                                    lbStatus.Update();
                                }
                                finally
                                {
                                    if (File.Exists(WCCPConst.root + "wccpupdatever.xml"))
                                    {
                                        File.Delete(WCCPConst.root + "wccpupdatever.xml");
                                    };
                                    dsUpdate.Tables[0].Clear();
                                    dsUpdate.Tables[1].Clear();
                                    dsUpdate.Tables[2].Clear();
                                    lbStatusDownLoad.Text = String.Empty;
                                    lbStatusDownLoad.Update();
                                    btnFinishLoad.Visible = true;
                                };
                            }
                            else
                            {
                                if (File.Exists(WCCPConst.root + "wccpupdatever.xml"))
                                {
                                    File.Delete(WCCPConst.root + "wccpupdatever.xml");
                                };
                                dsUpdate.Tables[0].Clear();
                                dsUpdate.Tables[1].Clear();
                                dsUpdate.Tables[2].Clear();
                                lbStatus.Text = "��������� ������� ������. ���������� ���������� ���������� ����� �����������.";
                                lbStatus.Update();
                                lbStatusDownLoad.Text = String.Empty;
                                lbStatusDownLoad.Update();
                                btnFinishLoad.Visible = true;
                            };
                        }
                        catch (WebException exw)
                        {
                            MessageBox.Show(exw.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            lbStatusDownLoad.Text = String.Empty;
                            lbStatusDownLoad.Update();
                            btnFinishLoad.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            lbStatusDownLoad.Text = String.Empty;
                            lbStatusDownLoad.Update();
                            btnFinishLoad.Visible = true;
                        };
                    };
                }
                else
                {
                    MessageBox.Show(WCCPConst.msgErrorURI, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lbStatusDownLoad.Text = String.Empty;
                    lbStatusDownLoad.Update();
                    btnFinishLoad.Visible = true;
                };
            }
            catch (WebException exw)
            {
                MessageBox.Show(exw.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lbStatusDownLoad.Text = String.Empty;
                lbStatusDownLoad.Update();
                btnFinishLoad.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lbStatusDownLoad.Text = String.Empty;
                lbStatusDownLoad.Update();
                btnFinishLoad.Visible = true;
            };

        }

        private void UpdatingForm_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            WCCPUpdating();
        }
    }
}