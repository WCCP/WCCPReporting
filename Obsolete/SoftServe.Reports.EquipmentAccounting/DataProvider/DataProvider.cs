﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.DataAccess;
using SoftServe.Reports.EquipmentAccounting.Utility;

namespace SoftServe.Reports.EquipmentAccounting
{
    internal static class DataProvider
    {
        private const string INDEFINITE_PROJECT = "Неопределенный";
        private const string EMPTY_PROJECT = "    ";

        public static bool IsLDB
        {
            get { return DataAccessLayer.IsLDB; }
        }

        /// <summary>
        /// Gets the territory tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of territory division</param>
        /// <param name="levelTo">The lowest level of territory division</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetTerritoryTree(int levelFrom, int levelTo)
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(IsLDB ? Constants.SP_TERRITORY_TREE_LDB : Constants.SP_TERRITORY_TREE, new[] { 
                new SqlParameter(Constants.SP_TERRITORY_TREE_LevelFrom_PARAM, levelFrom),
                new SqlParameter(Constants.SP_TERRITORY_TREE_LevelTo_PARAM, levelTo)
                });
            
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the distributors tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of distributors hierarchy</param>
        /// <param name="levelTo">The lowest level of distributors hierarchy</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetDistributorsTree(out int distrLevel)
        {
            DataTable lDataTable = null;
            DataSet dataSet = IsLDB ? DataAccessLayer.ExecuteStoredProcedure(Constants.SP_DISTRIBUTORS_TREE_LDB)
                : DataAccessLayer.ExecuteStoredProcedure(Constants.SP_DISTRIBUTORS_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                distrLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                distrLevel = 4;
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the staffing tree actual for the date chosen
        /// </summary>
        /// <param name="date">The date of cheching</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetStaffingTree(DateTime date)
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(IsLDB ? Constants.SP_STAFFING_TREE_LDB : Constants.SP_STAFFING_TREE,
                new[] { new SqlParameter(Constants.SP_STAFFING_TREE_Date_PARAM, date) });

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            DataView lStaffingView = lDataTable.DefaultView;
            lStaffingView.RowFilter = "Level > 0";
            lDataTable = lStaffingView.ToTable();

            return lDataTable;
        }
        
        /// <summary>
        /// Gets the tree of warehouses interlaced with territory
        /// </summary>
        /// <returns>The list of warehouses</returns>
        public static DataTable GetWarehouseTree(out int warehouseLevel)
        {
            DataTable lDataTableDB = null;

            DataSet dataSet = IsLDB ? DataAccessLayer.ExecuteStoredProcedure(Constants.SP_WAREHOUSES_TREE_LDB)
                : DataAccessLayer.ExecuteStoredProcedure(Constants.SP_WAREHOUSES_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTableDB = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                warehouseLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                warehouseLevel = 7;
            }

            DataTable lDataTable = new DataTable();

            lDataTable.Columns.Add("FieldID");
            lDataTable.Columns.Add("FieldParentID");
            lDataTable.Columns.Add("FieldName");
            lDataTable.Columns.Add("Level");
            lDataTable.Columns.Add("FIeldCode");
            lDataTable.Columns.Add("wFieldID");
            lDataTable.Columns.Add("DSM_ID");

            foreach (DataRow row in lDataTableDB.Rows)
            {
                lDataTable.Rows.Add(row["FieldID"], row["FieldParentID"], row["FieldName"], row["Level"], row["FIeldCode"], row["wFieldID"], row["DSM_ID"]);
            }

            DataView lDataViewWarehouses = lDataTable.DefaultView;
            lDataViewWarehouses.RowFilter = string.Format("Level > 2");
            lDataTable = lDataViewWarehouses.ToTable();

            return lDataTable;
        }

        /// <summary>
        /// Gets the customers tree interlaced with territory
        /// </summary>
        /// <returns>Tree table structure</returns>
        public static DataTable GetPosesTree(out int posLevelLevel)
        {
            DataTable dataTableDB = null;
            DataSet dataSet = IsLDB ? DataAccessLayer.ExecuteStoredProcedure(Constants.SP_CUSTOMERS_TREE_LDB) 
                : DataAccessLayer.ExecuteStoredProcedure(Constants.SP_CUSTOMERS_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTableDB = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                posLevelLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                posLevelLevel = 7;
            }

            DataTable lDataTable = new DataTable();

            lDataTable.Columns.Add("GeographyID");
            lDataTable.Columns.Add("ParentID");
            lDataTable.Columns.Add("GeographyCode");
            lDataTable.Columns.Add("GeographyName");
            lDataTable.Columns.Add("Level");
            lDataTable.Columns.Add("GeographyOldId");

            foreach (DataRow row in dataTableDB.Rows)
            {
                lDataTable.Rows.Add(row["GeographyID"], row["ParentID"], row["GeographyCode"], row["GeographyName"], row["Level"], row["GeographyOldId"]);
            }

            DataView lDataViewCustomers = lDataTable.DefaultView;
            lDataViewCustomers.RowFilter = string.Format("Level > 2");
            lDataTable = lDataViewCustomers.ToTable();

            return lDataTable;
        }

        /// <summary>
        /// Gets the models tree
        /// </summary>
        /// <returns>The models tree</returns>
        public static DataTable GetModelsTree()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_MODELS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            //lDataTable.Rows.Add(1, "Охладитель", 0, 1, 1);
            //lDataTable.Rows.Add(2, "Кран", 0, 1, 2);

            //lDataTable.Rows.Add(3, "Охладитель 3", 1, 2, 3);
            //lDataTable.Rows.Add(4, "Охладитель 4", 1, 2, 4);
            //lDataTable.Rows.Add(5, "Охладитель 5", 1, 2, 5);

            //lDataTable.Rows.Add(6, "Кран 6", 2, 2, 6);
            //lDataTable.Rows.Add(7, "Кран 7", 2, 2, 7);

            return lDataTable;
        }

        /// <summary>
        /// Gets the brands list
        /// </summary>
        /// <returns>The brands list</returns>
        public static DataTable GetBrandList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_BRANDS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            //lDataTable.Rows.Add(1, "Бренд 1");
            //lDataTable.Rows.Add(2, "Бренд 2");
            //lDataTable.Rows.Add(3, "Бренд 3");
            //lDataTable.Rows.Add(4, "Бренд 4");
            //lDataTable.Rows.Add(5, "Бренд 5");
            //lDataTable.Rows.Add(6, "Бренд 6");

            return lDataTable;
        }

        /// <summary>
        /// Gets the list of statuses
        /// </summary>
        /// <returns>The list of statuses</returns>
        public static DataTable GetStatusesList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_STATUSES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the list of equipment technical conditions
        /// </summary>
        /// <returns>The list of equipment technical conditions</returns>
        public static DataTable GetTechnicalConditionsList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_TECHNICAL_CONDITONS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets list of distributors
        /// </summary>
        /// <returns>The list of distributors</returns>
        public static DataTable GetDistrTypesList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_DISTR_TYPES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets list of manufacturers
        /// </summary>
        /// <returns>The list of manufacturers</returns>
        public static DataTable GetManufacturersList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_MANUFACTURERS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            //lDataTable.Rows.Add(1, "Производитель 1");
            //lDataTable.Rows.Add(2, "Производитель 2");
            //lDataTable.Rows.Add(3, "Производитель 3");
            //lDataTable.Rows.Add(4, "Производитель 4");
            //lDataTable.Rows.Add(5, "Производитель 5");
            //lDataTable.Rows.Add(6, "Производитель 6");

            return lDataTable;
        }

        /// <summary>
        /// Gets list of projects
        /// </summary>
        /// <returns>The list of projects</returns>
        public static DataTable GetProjectsList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_PROJECT_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            bool lIsEmptyPojectPresent = false;
            foreach (DataRow row in lDataTable.Rows)
            {
                if (row[0] == DBNull.Value)
                {
                    row[0] = INDEFINITE_PROJECT;
                    lIsEmptyPojectPresent = true;

                    break;
                }
            }

            foreach (DataRow row in lDataTable.Rows)
            {
                if (string.IsNullOrEmpty(row.Field<string>("Project")))
                {
                    row[0] = EMPTY_PROJECT;
                   break;
                }
            }

            if (!lIsEmptyPojectPresent)
            {
                lDataTable.Rows.Add(INDEFINITE_PROJECT);
            }

            lDataTable.Columns.Add("Id", typeof(int));

            for (int i = 0; i < lDataTable.Rows.Count; i++)
            {
                lDataTable.Rows[i]["Id"] = i;
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the main report data.
        /// </summary>
        /// <returns>The main report data.</returns>
        public static DataTable GetMainReportData(ReportInfo settings)
        {
            DataTable lDataTable = null;

            object lStuffIds = DBNull.Value;
            if (settings.StaffingsIds.Count > 0)
            {
                lStuffIds = XmlHelper.GetXmlDescription(settings.StaffingsIds);
            }

            object lPosIds = DBNull.Value;
            if (settings.PosIds.Count > 0)
            {
                lPosIds = XmlHelper.GetXmlDescription(settings.PosIds);
            }

            object lAgreementStatusIds = DBNull.Value;
            if (settings.ContractStatusIds.Count > 0)
            {
                lAgreementStatusIds = XmlHelper.GetXmlDescription(settings.ContractStatusIds);
            }

            object lWarehousesIds = DBNull.Value;
            if (settings.WarehousesIds.Count > 0)
            {
                lWarehousesIds = XmlHelper.GetXmlDescription(settings.WarehousesIds);
            }
            
            string lProjects = string.Join(":", settings.Projects.ToArray());
            lProjects = lProjects.Replace(INDEFINITE_PROJECT, "-1");//indefinite project was selected
            lProjects = lProjects.Replace(EMPTY_PROJECT, "");//empty project was selected

            object lProductionDatesFrom = DBNull.Value;
            if (settings.ProductionDates.From.Date != DateTime.MinValue)
            {
                lProductionDatesFrom = settings.ProductionDates.From.Date;
            }

            object lProductionDatesTo = DBNull.Value;
            if (settings.ProductionDates.To.Date != DateTime.MinValue)
            {
                lProductionDatesTo = settings.ProductionDates.To.Date;
            }

            object lRepairDatesFrom = DBNull.Value;
            if (settings.RepairDates.From.Date != DateTime.MinValue)
            {
                lRepairDatesFrom = settings.RepairDates.From.Date;
            }

            object lRepairDatesTo = DBNull.Value;
            if (settings.RepairDates.To.Date != DateTime.MinValue)
            {
                lRepairDatesTo = settings.RepairDates.To.Date;
            }

            object lInvoiceDatesFrom = DBNull.Value;
            if (settings.InvoiceDates.From != DateTime.MinValue)
            {
                lInvoiceDatesFrom = settings.InvoiceDates.From;
            }

            object lInvoiceDatesTo = DBNull.Value;
            if (settings.InvoiceDates.To != DateTime.MinValue)
            {
                lInvoiceDatesTo = settings.InvoiceDates.To;
            }

            object lReservationDatesFrom = DBNull.Value;
            if (settings.ReservationDates.From != DateTime.MinValue)
            {
                lReservationDatesFrom = settings.ReservationDates.From;
            }

            object lReservationDatesTo = DBNull.Value;
            if (settings.ReservationDates.To != DateTime.MinValue)
            {
                lReservationDatesTo = settings.ReservationDates.To;
            }

            object lContractDatesFrom = DBNull.Value;
            if (settings.ContractDates.From != DateTime.MinValue)
            {
                lContractDatesFrom = settings.ContractDates.From;
            }

            object lContractDatesTo = DBNull.Value;
            if (settings.ContractDates.To != DateTime.MinValue)
            {
                lContractDatesTo = settings.ContractDates.To;
            }

            object lInstalationDatesFrom = DBNull.Value;
            if (settings.InstalationDates.From != DateTime.MinValue)
            {
                lInstalationDatesFrom = settings.InstalationDates.From;
            }

            object lInstalationDatesTo = DBNull.Value;
            if (settings.InstalationDates.To != DateTime.MinValue)
            {
                lInstalationDatesTo = settings.InstalationDates.To;
            }

            object lReturningDatesFrom = DBNull.Value;
            if (settings.ReturningDates.From != DateTime.MinValue)
            {
                lReturningDatesFrom = settings.ReturningDates.From;
            }

            object lReturningDatesTo = DBNull.Value;
            if (settings.ReturningDates.To != DateTime.MinValue)
            {
                lReturningDatesTo = settings.ReturningDates.To;
            }

            object lReportDatesFrom = DBNull.Value;
            if (settings.ReportDates.From != DateTime.MinValue)
            {
                lReportDatesFrom = settings.ReportDates.From;
            }

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(IsLDB ? Constants.SP_GET_REPORT_DATA_LDB : Constants.SP_GET_REPORT_DATA, new[] { 
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateStart_PARAM, lReportDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateEnd_PARAM, settings.ReportDates.To),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_JustForDateEnd_PARAM, !settings.DoShowTransitions),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Staff_PARAM, lStuffIds),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Customer_PARAM, lPosIds),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Warehouse_PARAM, lWarehousesIds),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Model_PARAM, XmlHelper.GetXmlDescription(settings.ModelsIds)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Brand_PARAM, XmlHelper.GetXmlDescription(settings.BrandsIds)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Manufacturer_PARAM, XmlHelper.GetXmlDescription(settings.ManufacturersIds)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_TechCondition_PARAM, XmlHelper.GetXmlDescription(settings.TechConditionIds)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ContractStatus_PARAM, lAgreementStatusIds),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_OrderStatus_PARAM, XmlHelper.GetXmlDescription(settings.OrderStatusIds)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateProductionFrom_PARAM, lProductionDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateProductionTo_PARAM, lProductionDatesTo),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_RepairDateFrom_PARAM, lRepairDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_RepairDateTo_PARAM, lRepairDatesTo),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateInvoiceFrom_PARAM, lInvoiceDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateInvoiceTo_PARAM, lInvoiceDatesTo),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ReservationDateFrom_PARAM, lReservationDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ReservationDateTo_PARAM, lReservationDatesTo),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ContractDateFrom_PARAM, lContractDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ContractDateTo_PARAM, lContractDatesTo),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_InstalationDateFrom_PARAM, lInstalationDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_InstalationDateTo_PARAM, lInstalationDatesTo),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ReturnSystemDateFrom_PARAM, lReturningDatesFrom),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ReturnSystemDateTo_PARAM, lReturningDatesTo),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Project_PARAM, lProjects)
                });

            if (dataSet != null)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }
    }
}
