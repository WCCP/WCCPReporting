﻿namespace SoftServe.Reports.EquipmentAccounting.Utility
{
    public enum EquipmentCapability
    {
        Incapable = 0,
        Capable = 1
    }
}
