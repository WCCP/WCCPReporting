﻿using System.Collections.Generic;
using Logica.Reports.BaseReportControl.CommonFunctionality;

namespace SoftServe.Reports.EquipmentAccounting
{
    /// <summary>
    /// Describes the report settings
    /// </summary>
    public class ReportInfo
    {
        public DateInterval ReportDates { get; set; }
        
        public string RegionIds { get; set; }
        public List<string> StaffingsIds { get; set; }

        public List<string> PosIds { get; set; }
        public List<string> WarehousesIds { get; set; }
        
        public List<string> TechConditionIds { get; set; }
        public List<string> ModelsIds { get; set; }
        public List<string> ManufacturersIds { get; set; }
        public List<string> BrandsIds { get; set; }

        public DateInterval ProductionDates { get; set; }
        public DateInterval InvoiceDates { get; set; }
        public DateInterval RepairDates { get; set; }

        public DateInterval ReservationDates { get; set; }
        public List<string> Projects { get; set; }

        public DateInterval ContractDates { get; set; }

        public List<string> ContractStatusIds { get; set; }
        public List<string> OrderStatusIds { get; set; }

        public DateInterval InstalationDates { get; set; }
        public DateInterval ReturningDates { get; set; }
        public bool DoShowTransitions { get; set; }
    }
}
