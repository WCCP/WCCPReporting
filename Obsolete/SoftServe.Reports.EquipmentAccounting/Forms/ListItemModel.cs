﻿namespace SoftServe.Reports.EquipmentAccounting.Forms
{
    class ListItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
