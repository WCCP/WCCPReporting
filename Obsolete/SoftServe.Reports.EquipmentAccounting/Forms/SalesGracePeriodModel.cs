﻿using BLToolkit.EditableObjects;

namespace SoftServe.Reports.EquipmentAccounting.Forms
{
    public abstract class SalesGracePeriodModel : EditableObject<SalesGracePeriodModel>
    {
        public abstract int Country_Id { get; set; }
        public abstract int SaleGracePeriod { get; set; }
    }
}
