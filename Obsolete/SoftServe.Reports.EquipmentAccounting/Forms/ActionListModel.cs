﻿using BLToolkit.EditableObjects;

namespace SoftServe.Reports.EquipmentAccounting.Forms
{
    public abstract class ActionListModel : EditableObject <ActionListModel>
    {
        public abstract int Country_ID { get; set; }
        public abstract int M3_ID { get; set; }
        public abstract string Item_ID { get; set; }
    }
}
