﻿using Logica.Reports.BaseReportControl.CommonControls;

namespace SoftServe.Reports.EquipmentAccounting
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.generateReportButton = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cShowTransitions = new DevExpress.XtraEditors.CheckEdit();
            this.cbDistrType = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.lDistrType = new DevExpress.XtraEditors.LabelControl();
            this.lDistr = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lWarehouses = new DevExpress.XtraEditors.LabelControl();
            this.lBrand = new DevExpress.XtraEditors.LabelControl();
            this.lManufacturer = new DevExpress.XtraEditors.LabelControl();
            this.lProjects = new DevExpress.XtraEditors.LabelControl();
            this.lCapability = new DevExpress.XtraEditors.LabelControl();
            this.tTerritory = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colTerritory = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cluDistr = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrFieldParentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbPos = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.tWarehouseLocation = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tPosLocation = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPosLocGeographyID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPosLocGeographyOldId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cluWarehouses = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.userControl21View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExternalCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareParentId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareM3Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tModels = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.treeListColumn5 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cbTechnicalState = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.cbBrand = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.cbManufacturer = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.cbProjects = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.tReservationDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.tRepairDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.tProductionDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.tIncomeDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.lTecnichalState = new DevExpress.XtraEditors.LabelControl();
            this.lActStatus = new DevExpress.XtraEditors.LabelControl();
            this.lAgreementStatus = new DevExpress.XtraEditors.LabelControl();
            this.cbActStatus = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.cbAgreementStatus = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.tReturningDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.tInstalationDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.tAgreementAssignment = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.component1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tReportDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.cPos = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.cWareHouses = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.tStaffing = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colPersName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPersParendId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPersStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPersStaffIdReal = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cStaffing = new DevExpress.XtraEditors.CheckEdit();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cEquipmentInCapable = new DevExpress.XtraEditors.CheckEdit();
            this.cEquipmentCapable = new DevExpress.XtraEditors.CheckEdit();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.cModels = new DevExpress.XtraEditors.CheckEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cShowTransitions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluDistr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tWarehouseLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPosLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluWarehouses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tModels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTechnicalState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBrand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbManufacturer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProjects.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAgreementStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cWareHouses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffing.Properties)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cEquipmentInCapable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEquipmentCapable.Properties)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cModels.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // generateReportButton
            // 
            this.generateReportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.generateReportButton.Image = global::SoftServe.Reports.EquipmentAccounting.Properties.Resources.check;
            this.generateReportButton.Location = new System.Drawing.Point(112, 27);
            this.generateReportButton.Name = "generateReportButton";
            this.generateReportButton.Size = new System.Drawing.Size(146, 23);
            this.generateReportButton.TabIndex = 0;
            this.generateReportButton.Text = "Сгенерировать отчет";
            this.generateReportButton.Click += new System.EventHandler(this.generateReportButton_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Image = global::SoftServe.Reports.EquipmentAccounting.Properties.Resources.close;
            this.simpleButton2.Location = new System.Drawing.Point(271, 27);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(86, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Отмена";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Appearance.Options.UseFont = true;
            this.lblUpd2.Location = new System.Drawing.Point(77, 442);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(0, 13);
            this.lblUpd2.TabIndex = 22;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(616, 10);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(218, 13);
            this.labelControl9.TabIndex = 23;
            this.labelControl9.Text = "Уровень детализации складских ордеров:";
            // 
            // cShowTransitions
            // 
            this.cShowTransitions.EditValue = true;
            this.cShowTransitions.Location = new System.Drawing.Point(840, 7);
            this.cShowTransitions.Name = "cShowTransitions";
            this.cShowTransitions.Properties.Caption = "Перемещения за период";
            this.cShowTransitions.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cShowTransitions.Size = new System.Drawing.Size(161, 19);
            this.cShowTransitions.TabIndex = 24;
            this.cShowTransitions.CheckedChanged += new System.EventHandler(this.cShowTransitions_CheckedChanged);
            // 
            // cbDistrType
            // 
            this.cbDistrType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDistrType.Delimiter = ",";
            this.cbDistrType.Location = new System.Drawing.Point(235, 28);
            this.cbDistrType.Name = "cbDistrType";
            this.cbDistrType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDistrType.Properties.DisplayMember = "SchemaName";
            this.cbDistrType.Properties.DropDownRows = 15;
            this.cbDistrType.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbDistrType.Properties.ShowButtons = false;
            this.cbDistrType.Properties.ValueMember = "SchemaID";
            this.cbDistrType.Size = new System.Drawing.Size(223, 20);
            this.cbDistrType.TabIndex = 28;
            this.cbDistrType.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbDistrType_Closed);
            // 
            // lDistrType
            // 
            this.lDistrType.Location = new System.Drawing.Point(235, 3);
            this.lDistrType.Name = "lDistrType";
            this.lDistrType.Size = new System.Drawing.Size(109, 13);
            this.lDistrType.TabIndex = 29;
            this.lDistrType.Text = "Тип дистрибьютора :";
            // 
            // lDistr
            // 
            this.lDistr.Location = new System.Drawing.Point(235, 48);
            this.lDistr.Name = "lDistr";
            this.lDistr.Size = new System.Drawing.Size(80, 13);
            this.lDistr.TabIndex = 30;
            this.lDistr.Text = "Дистрибьютор:";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(235, 83);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(17, 13);
            this.labelControl15.TabIndex = 34;
            this.labelControl15.Text = "ТС:";
            // 
            // lWarehouses
            // 
            this.lWarehouses.Location = new System.Drawing.Point(464, 146);
            this.lWarehouses.Name = "lWarehouses";
            this.lWarehouses.Size = new System.Drawing.Size(36, 13);
            this.lWarehouses.TabIndex = 36;
            this.lWarehouses.Text = "Склад:";
            // 
            // lBrand
            // 
            this.lBrand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lBrand.Location = new System.Drawing.Point(55, 28);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(34, 13);
            this.lBrand.TabIndex = 42;
            this.lBrand.Text = "Брэнд:";
            // 
            // lManufacturer
            // 
            this.lManufacturer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lManufacturer.Location = new System.Drawing.Point(6, 3);
            this.lManufacturer.Name = "lManufacturer";
            this.lManufacturer.Size = new System.Drawing.Size(83, 13);
            this.lManufacturer.TabIndex = 44;
            this.lManufacturer.Text = "Производитель:";
            // 
            // lProjects
            // 
            this.lProjects.Location = new System.Drawing.Point(87, 121);
            this.lProjects.Name = "lProjects";
            this.lProjects.Size = new System.Drawing.Size(41, 13);
            this.lProjects.TabIndex = 54;
            this.lProjects.Text = "Проект:";
            // 
            // lCapability
            // 
            this.lCapability.Location = new System.Drawing.Point(4, 4);
            this.lCapability.Name = "lCapability";
            this.lCapability.Size = new System.Drawing.Size(143, 13);
            this.lCapability.TabIndex = 56;
            this.lCapability.Text = "Возможность эксплуатации:";
            // 
            // tTerritory
            // 
            this.tTerritory.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colTerritory});
            this.tTerritory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tTerritory.KeyFieldName = "GeographyID";
            this.tTerritory.Location = new System.Drawing.Point(2, 22);
            this.tTerritory.Name = "tTerritory";
            this.tTerritory.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.tTerritory.OptionsView.ShowCheckBoxes = true;
            this.tTerritory.OptionsView.ShowColumns = false;
            this.tTerritory.OptionsView.ShowHorzLines = false;
            this.tTerritory.OptionsView.ShowIndicator = false;
            this.tTerritory.OptionsView.ShowVertLines = false;
            this.tTerritory.Size = new System.Drawing.Size(215, 183);
            this.tTerritory.TabIndex = 77;
            this.tTerritory.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tTerritory_AfterNodeChecked);
            // 
            // colTerritory
            // 
            this.colTerritory.Caption = "Территория";
            this.colTerritory.CustomizationCaption = "Территория";
            this.colTerritory.FieldName = "GeographyName";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsColumn.AllowEdit = false;
            this.colTerritory.OptionsColumn.ReadOnly = true;
            this.colTerritory.Visible = true;
            this.colTerritory.VisibleIndex = 0;
            // 
            // cluDistr
            // 
            this.cluDistr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cluDistr.Delimiter = ", ";
            this.cluDistr.EditValue = "";
            this.cluDistr.Location = new System.Drawing.Point(235, 63);
            this.cluDistr.Name = "cluDistr";
            this.cluDistr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cluDistr.Properties.DisplayMember = "FieldName";
            this.cluDistr.Properties.NullText = "";
            this.cluDistr.Properties.ValueMember = "FIeldCode";
            this.cluDistr.Properties.View = this.gridView1;
            this.cluDistr.Size = new System.Drawing.Size(223, 20);
            this.cluDistr.TabIndex = 100;
            this.cluDistr.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbDistr_Closed);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.colDistrFieldParentID});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Код";
            this.gridColumn4.CustomizationCaption = "Код";
            this.gridColumn4.FieldName = "FIeldCode";
            this.gridColumn4.MaxWidth = 100;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowShowHide = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Название дистрибьютора";
            this.gridColumn5.CustomizationCaption = "Название дистрибьютора";
            this.gridColumn5.FieldName = "FieldName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowShowHide = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Id";
            this.gridColumn6.FieldName = "FieldID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowShowHide = false;
            this.gridColumn6.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Level";
            this.gridColumn7.FieldName = "Level";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowShowHide = false;
            this.gridColumn7.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDistrFieldParentID
            // 
            this.colDistrFieldParentID.Caption = "ParentId";
            this.colDistrFieldParentID.FieldName = "FieldParentID";
            this.colDistrFieldParentID.Name = "colDistrFieldParentID";
            this.colDistrFieldParentID.OptionsColumn.AllowShowHide = false;
            this.colDistrFieldParentID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cbPos
            // 
            this.cbPos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPos.Delimiter = ",";
            this.cbPos.Location = new System.Drawing.Point(235, 98);
            this.cbPos.Name = "cbPos";
            this.cbPos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPos.Properties.DisplayMember = "FieldName";
            this.cbPos.Properties.DropDownRows = 15;
            this.cbPos.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbPos.Properties.ShowButtons = false;
            this.cbPos.Properties.ValueMember = "FieldCode";
            this.cbPos.Size = new System.Drawing.Size(223, 20);
            this.cbPos.TabIndex = 99;
            // 
            // tWarehouseLocation
            // 
            this.tWarehouseLocation.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn3});
            this.tWarehouseLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tWarehouseLocation.KeyFieldName = "FieldID";
            this.tWarehouseLocation.Location = new System.Drawing.Point(2, 22);
            this.tWarehouseLocation.Name = "tWarehouseLocation";
            this.tWarehouseLocation.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.tWarehouseLocation.OptionsView.ShowCheckBoxes = true;
            this.tWarehouseLocation.OptionsView.ShowColumns = false;
            this.tWarehouseLocation.OptionsView.ShowHorzLines = false;
            this.tWarehouseLocation.OptionsView.ShowIndicator = false;
            this.tWarehouseLocation.OptionsView.ShowVertLines = false;
            this.tWarehouseLocation.ParentFieldName = "FieldParentID";
            this.tWarehouseLocation.Size = new System.Drawing.Size(238, 119);
            this.tWarehouseLocation.TabIndex = 84;
            this.tWarehouseLocation.AfterEditorStateChanged += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.StateChanged(this.cWareHouses_CheckedChanged);
            this.tWarehouseLocation.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tWarehouseLocation_AfterCheckNode);
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Локация склада";
            this.treeListColumn3.CustomizationCaption = "Локация склада";
            this.treeListColumn3.FieldName = "FieldName";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.OptionsColumn.AllowEdit = false;
            this.treeListColumn3.OptionsColumn.ReadOnly = true;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 0;
            // 
            // tPosLocation
            // 
            this.tPosLocation.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.colPosLocGeographyID,
            this.colPosLocGeographyOldId});
            this.tPosLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tPosLocation.KeyFieldName = "GeographyID";
            this.tPosLocation.Location = new System.Drawing.Point(2, 22);
            this.tPosLocation.Name = "tPosLocation";
            this.tPosLocation.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.tPosLocation.OptionsView.ShowCheckBoxes = true;
            this.tPosLocation.OptionsView.ShowColumns = false;
            this.tPosLocation.OptionsView.ShowHorzLines = false;
            this.tPosLocation.OptionsView.ShowIndicator = false;
            this.tPosLocation.OptionsView.ShowVertLines = false;
            this.tPosLocation.Size = new System.Drawing.Size(228, 159);
            this.tPosLocation.TabIndex = 79;
            this.tPosLocation.AfterEditorStateChanged += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.StateChanged(this.cPos_CheckedChanged);
            this.tPosLocation.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tPosLocation_AfterCheckNode);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Дислокация";
            this.treeListColumn1.CustomizationCaption = "Дислокация";
            this.treeListColumn1.FieldName = "GeographyName";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // colPosLocGeographyID
            // 
            this.colPosLocGeographyID.Caption = "GeographyID";
            this.colPosLocGeographyID.FieldName = "GeographyID";
            this.colPosLocGeographyID.Name = "colPosLocGeographyID";
            this.colPosLocGeographyID.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPosLocGeographyID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colPosLocGeographyOldId
            // 
            this.colPosLocGeographyOldId.Caption = "GeographyOldId";
            this.colPosLocGeographyOldId.FieldName = "GeographyOldId";
            this.colPosLocGeographyOldId.Name = "colPosLocGeographyOldId";
            this.colPosLocGeographyOldId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPosLocGeographyOldId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cluWarehouses
            // 
            this.cluWarehouses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cluWarehouses.Delimiter = ", ";
            this.cluWarehouses.EditValue = "";
            this.cluWarehouses.Location = new System.Drawing.Point(464, 161);
            this.cluWarehouses.Name = "cluWarehouses";
            this.cluWarehouses.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cluWarehouses.Properties.DisplayMember = "FieldName";
            this.cluWarehouses.Properties.NullText = "";
            this.cluWarehouses.Properties.ValueMember = "FieldID";
            this.cluWarehouses.Properties.View = this.userControl21View;
            this.cluWarehouses.Size = new System.Drawing.Size(236, 20);
            this.cluWarehouses.TabIndex = 82;
            this.cluWarehouses.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cluWarehouses_Closed);
            // 
            // userControl21View
            // 
            this.userControl21View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExternalCode,
            this.colWarehouse,
            this.colWareId,
            this.colWareLevel,
            this.colWareParentId,
            this.colWareM3Id});
            this.userControl21View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.userControl21View.Name = "userControl21View";
            this.userControl21View.OptionsFilter.AllowFilterEditor = false;
            this.userControl21View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.userControl21View.OptionsView.ShowGroupPanel = false;
            // 
            // colExternalCode
            // 
            this.colExternalCode.Caption = "Код";
            this.colExternalCode.CustomizationCaption = "Код";
            this.colExternalCode.FieldName = "FIeldCode";
            this.colExternalCode.MaxWidth = 100;
            this.colExternalCode.Name = "colExternalCode";
            this.colExternalCode.OptionsColumn.AllowEdit = false;
            this.colExternalCode.OptionsColumn.AllowShowHide = false;
            this.colExternalCode.OptionsColumn.ReadOnly = true;
            this.colExternalCode.Visible = true;
            this.colExternalCode.VisibleIndex = 0;
            // 
            // colWarehouse
            // 
            this.colWarehouse.Caption = "Название склада";
            this.colWarehouse.CustomizationCaption = "Название склада";
            this.colWarehouse.FieldName = "FieldName";
            this.colWarehouse.Name = "colWarehouse";
            this.colWarehouse.OptionsColumn.AllowEdit = false;
            this.colWarehouse.OptionsColumn.AllowShowHide = false;
            this.colWarehouse.OptionsColumn.ReadOnly = true;
            this.colWarehouse.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colWarehouse.Visible = true;
            this.colWarehouse.VisibleIndex = 1;
            // 
            // colWareId
            // 
            this.colWareId.Caption = "Id";
            this.colWareId.FieldName = "FieldID";
            this.colWareId.Name = "colWareId";
            this.colWareId.OptionsColumn.AllowShowHide = false;
            this.colWareId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWareLevel
            // 
            this.colWareLevel.Caption = "Level";
            this.colWareLevel.FieldName = "Level";
            this.colWareLevel.Name = "colWareLevel";
            this.colWareLevel.OptionsColumn.AllowShowHide = false;
            this.colWareLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWareParentId
            // 
            this.colWareParentId.Caption = "ParentId";
            this.colWareParentId.FieldName = "FieldParentID";
            this.colWareParentId.Name = "colWareParentId";
            this.colWareParentId.OptionsColumn.AllowShowHide = false;
            this.colWareParentId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWareM3Id
            // 
            this.colWareM3Id.Caption = "WareM3Id";
            this.colWareM3Id.FieldName = "DSM_ID";
            this.colWareM3Id.Name = "colWareM3Id";
            this.colWareM3Id.OptionsColumn.AllowShowHide = false;
            this.colWareM3Id.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // tModels
            // 
            this.tModels.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn5});
            this.tModels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tModels.KeyFieldName = "FieldID";
            this.tModels.Location = new System.Drawing.Point(2, 22);
            this.tModels.Name = "tModels";
            this.tModels.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.tModels.OptionsView.ShowCheckBoxes = true;
            this.tModels.OptionsView.ShowColumns = false;
            this.tModels.OptionsView.ShowHorzLines = false;
            this.tModels.OptionsView.ShowIndicator = false;
            this.tModels.OptionsView.ShowVertLines = false;
            this.tModels.RootValue = null;
            this.tModels.Size = new System.Drawing.Size(229, 114);
            this.tModels.TabIndex = 97;
            // 
            // treeListColumn5
            // 
            this.treeListColumn5.Caption = "Модель";
            this.treeListColumn5.CustomizationCaption = "Модель";
            this.treeListColumn5.FieldName = "FieldName";
            this.treeListColumn5.Name = "treeListColumn5";
            this.treeListColumn5.OptionsColumn.AllowEdit = false;
            this.treeListColumn5.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn5.OptionsColumn.ReadOnly = true;
            this.treeListColumn5.Visible = true;
            this.treeListColumn5.VisibleIndex = 0;
            // 
            // cbTechnicalState
            // 
            this.cbTechnicalState.Delimiter = ",";
            this.cbTechnicalState.Location = new System.Drawing.Point(4, 113);
            this.cbTechnicalState.Name = "cbTechnicalState";
            this.cbTechnicalState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTechnicalState.Properties.DisplayMember = "TechnicalCondition";
            this.cbTechnicalState.Properties.DropDownRows = 15;
            this.cbTechnicalState.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbTechnicalState.Properties.ShowButtons = false;
            this.cbTechnicalState.Properties.ValueMember = "TCID";
            this.cbTechnicalState.Size = new System.Drawing.Size(199, 20);
            this.cbTechnicalState.TabIndex = 98;
            // 
            // cbBrand
            // 
            this.cbBrand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBrand.Delimiter = ",";
            this.cbBrand.Location = new System.Drawing.Point(95, 28);
            this.cbBrand.Name = "cbBrand";
            this.cbBrand.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbBrand.Properties.DisplayMember = "POSBrand_Name";
            this.cbBrand.Properties.DropDownRows = 15;
            this.cbBrand.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbBrand.Properties.ShowButtons = false;
            this.cbBrand.Properties.ValueMember = "POSBrand_ID";
            this.cbBrand.Size = new System.Drawing.Size(135, 20);
            this.cbBrand.TabIndex = 97;
            // 
            // cbManufacturer
            // 
            this.cbManufacturer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbManufacturer.Delimiter = ",";
            this.cbManufacturer.Location = new System.Drawing.Point(95, 3);
            this.cbManufacturer.Name = "cbManufacturer";
            this.cbManufacturer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbManufacturer.Properties.DisplayMember = "ManufacturerName";
            this.cbManufacturer.Properties.DropDownRows = 15;
            this.cbManufacturer.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbManufacturer.Properties.ShowButtons = false;
            this.cbManufacturer.Properties.ValueMember = "ManufacturerId";
            this.cbManufacturer.Size = new System.Drawing.Size(135, 20);
            this.cbManufacturer.TabIndex = 96;
            // 
            // cbProjects
            // 
            this.cbProjects.Delimiter = ",";
            this.cbProjects.EditValue = "";
            this.cbProjects.Location = new System.Drawing.Point(147, 114);
            this.cbProjects.Name = "cbProjects";
            this.cbProjects.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbProjects.Properties.DisplayMember = "Project";
            this.cbProjects.Properties.DropDownRows = 15;
            this.cbProjects.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbProjects.Properties.ShowButtons = false;
            this.cbProjects.Properties.ValueMember = "Id";
            this.cbProjects.Size = new System.Drawing.Size(177, 20);
            this.cbProjects.TabIndex = 86;
            // 
            // tReservationDates
            // 
            this.tReservationDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tReservationDates.BackColor = System.Drawing.Color.Transparent;
            this.tReservationDates.Description = "Дата резервирования:";
            this.tReservationDates.From = new System.DateTime(((long)(0)));
            this.tReservationDates.Location = new System.Drawing.Point(-9, 83);
            this.tReservationDates.Name = "tReservationDates";
            this.tReservationDates.Size = new System.Drawing.Size(336, 25);
            this.tReservationDates.TabIndex = 84;
            this.tReservationDates.To = new System.DateTime(((long)(0)));
            // 
            // tRepairDates
            // 
            this.tRepairDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tRepairDates.BackColor = System.Drawing.Color.Transparent;
            this.tRepairDates.Description = "Дата восстановления:";
            this.tRepairDates.From = new System.DateTime(((long)(0)));
            this.tRepairDates.Location = new System.Drawing.Point(-9, 54);
            this.tRepairDates.Name = "tRepairDates";
            this.tRepairDates.Size = new System.Drawing.Size(336, 25);
            this.tRepairDates.TabIndex = 83;
            this.tRepairDates.To = new System.DateTime(((long)(0)));
            // 
            // tProductionDates
            // 
            this.tProductionDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tProductionDates.BackColor = System.Drawing.Color.Transparent;
            this.tProductionDates.Description = "Дата выпуска:";
            this.tProductionDates.From = new System.DateTime(((long)(0)));
            this.tProductionDates.Location = new System.Drawing.Point(-9, 2);
            this.tProductionDates.Name = "tProductionDates";
            this.tProductionDates.Size = new System.Drawing.Size(336, 25);
            this.tProductionDates.TabIndex = 81;
            this.tProductionDates.To = new System.DateTime(((long)(0)));
            // 
            // tIncomeDates
            // 
            this.tIncomeDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tIncomeDates.BackColor = System.Drawing.Color.Transparent;
            this.tIncomeDates.Description = "Дата прихода:";
            this.tIncomeDates.From = new System.DateTime(((long)(0)));
            this.tIncomeDates.Location = new System.Drawing.Point(-9, 28);
            this.tIncomeDates.Name = "tIncomeDates";
            this.tIncomeDates.Size = new System.Drawing.Size(336, 25);
            this.tIncomeDates.TabIndex = 82;
            this.tIncomeDates.To = new System.DateTime(((long)(0)));
            // 
            // lTecnichalState
            // 
            this.lTecnichalState.Location = new System.Drawing.Point(7, 97);
            this.lTecnichalState.Name = "lTecnichalState";
            this.lTecnichalState.Size = new System.Drawing.Size(124, 13);
            this.lTecnichalState.TabIndex = 75;
            this.lTecnichalState.Text = "Техническое состояние:";
            // 
            // lActStatus
            // 
            this.lActStatus.Location = new System.Drawing.Point(82, 85);
            this.lActStatus.Name = "lActStatus";
            this.lActStatus.Size = new System.Drawing.Size(67, 13);
            this.lActStatus.TabIndex = 97;
            this.lActStatus.Text = "Статус акта:";
            // 
            // lAgreementStatus
            // 
            this.lAgreementStatus.Location = new System.Drawing.Point(63, 59);
            this.lAgreementStatus.Name = "lAgreementStatus";
            this.lAgreementStatus.Size = new System.Drawing.Size(91, 13);
            this.lAgreementStatus.TabIndex = 98;
            this.lAgreementStatus.Text = "Статус договора:";
            // 
            // cbActStatus
            // 
            this.cbActStatus.Delimiter = ",";
            this.cbActStatus.Location = new System.Drawing.Point(170, 82);
            this.cbActStatus.Name = "cbActStatus";
            this.cbActStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbActStatus.Properties.DisplayMember = "StatusName";
            this.cbActStatus.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbActStatus.Properties.ShowButtons = false;
            this.cbActStatus.Properties.ValueMember = "Status";
            this.cbActStatus.Size = new System.Drawing.Size(189, 20);
            this.cbActStatus.TabIndex = 87;
            // 
            // cbAgreementStatus
            // 
            this.cbAgreementStatus.Delimiter = ",";
            this.cbAgreementStatus.Location = new System.Drawing.Point(170, 56);
            this.cbAgreementStatus.Name = "cbAgreementStatus";
            this.cbAgreementStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbAgreementStatus.Properties.DisplayMember = "StatusName";
            this.cbAgreementStatus.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbAgreementStatus.Properties.ShowButtons = false;
            this.cbAgreementStatus.Properties.ValueMember = "Status";
            this.cbAgreementStatus.Size = new System.Drawing.Size(189, 20);
            this.cbAgreementStatus.TabIndex = 86;
            // 
            // tReturningDates
            // 
            this.tReturningDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tReturningDates.BackColor = System.Drawing.Color.Transparent;
            this.tReturningDates.Description = "Дата возврата в системе:";
            this.tReturningDates.From = new System.DateTime(((long)(0)));
            this.tReturningDates.Location = new System.Drawing.Point(2, 136);
            this.tReturningDates.Name = "tReturningDates";
            this.tReturningDates.Size = new System.Drawing.Size(360, 25);
            this.tReturningDates.TabIndex = 80;
            this.tReturningDates.To = new System.DateTime(((long)(0)));
            // 
            // tInstalationDates
            // 
            this.tInstalationDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tInstalationDates.BackColor = System.Drawing.Color.Transparent;
            this.tInstalationDates.Description = "Дата установки в системе:";
            this.tInstalationDates.From = new System.DateTime(((long)(0)));
            this.tInstalationDates.Location = new System.Drawing.Point(2, 108);
            this.tInstalationDates.Name = "tInstalationDates";
            this.tInstalationDates.Size = new System.Drawing.Size(360, 25);
            this.tInstalationDates.TabIndex = 81;
            this.tInstalationDates.To = new System.DateTime(((long)(0)));
            // 
            // tAgreementAssignment
            // 
            this.tAgreementAssignment.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tAgreementAssignment.BackColor = System.Drawing.Color.Transparent;
            this.tAgreementAssignment.Description = "Дата заключения договора:";
            this.tAgreementAssignment.From = new System.DateTime(((long)(0)));
            this.tAgreementAssignment.Location = new System.Drawing.Point(2, 27);
            this.tAgreementAssignment.Name = "tAgreementAssignment";
            this.tAgreementAssignment.Size = new System.Drawing.Size(360, 25);
            this.tAgreementAssignment.TabIndex = 79;
            this.tAgreementAssignment.To = new System.DateTime(((long)(0)));
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // component1View
            // 
            this.component1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.component1View.Name = "component1View";
            this.component1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.component1View.OptionsView.ShowGroupPanel = false;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Дислокация";
            this.treeListColumn2.CustomizationCaption = "Дислокация";
            this.treeListColumn2.FieldName = "GeographyName";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // tReportDates
            // 
            this.tReportDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tReportDates.BackColor = System.Drawing.Color.Transparent;
            this.tReportDates.Description = "Период времени:";
            this.tReportDates.From = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.tReportDates.Location = new System.Drawing.Point(-73, 1);
            this.tReportDates.Name = "tReportDates";
            this.tReportDates.Size = new System.Drawing.Size(558, 26);
            this.tReportDates.TabIndex = 77;
            this.tReportDates.To = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.07217F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 229F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.92783F));
            this.tableLayoutPanel2.Controls.Add(this.groupControl6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupControl7, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.cluWarehouses, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.lWarehouses, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.lDistrType, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.cbDistrType, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lDistr, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.cluDistr, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelControl15, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.cbPos, 1, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 22);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(703, 183);
            this.tableLayoutPanel2.TabIndex = 78;
            // 
            // groupControl6
            // 
            this.groupControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl6.Controls.Add(this.cPos);
            this.groupControl6.Controls.Add(this.tPosLocation);
            this.groupControl6.Location = new System.Drawing.Point(0, 0);
            this.groupControl6.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl6.Name = "groupControl6";
            this.tableLayoutPanel2.SetRowSpan(this.groupControl6, 8);
            this.groupControl6.Size = new System.Drawing.Size(232, 183);
            this.groupControl6.TabIndex = 82;
            this.groupControl6.Text = "Дислокация ТС";
            // 
            // cPos
            // 
            this.cPos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cPos.EditValue = true;
            this.cPos.Location = new System.Drawing.Point(186, 2);
            this.cPos.Name = "cPos";
            this.cPos.Properties.AllowGrayed = true;
            this.cPos.Properties.Caption = "Все";
            this.cPos.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cPos.Size = new System.Drawing.Size(43, 19);
            this.cPos.TabIndex = 82;
            this.cPos.TabStop = false;
            // 
            // groupControl7
            // 
            this.groupControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl7.Controls.Add(this.cWareHouses);
            this.groupControl7.Controls.Add(this.tWarehouseLocation);
            this.groupControl7.Location = new System.Drawing.Point(461, 0);
            this.groupControl7.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl7.Name = "groupControl7";
            this.tableLayoutPanel2.SetRowSpan(this.groupControl7, 6);
            this.groupControl7.Size = new System.Drawing.Size(242, 143);
            this.groupControl7.TabIndex = 83;
            this.groupControl7.Text = "Дислокация склада";
            // 
            // cWareHouses
            // 
            this.cWareHouses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cWareHouses.EditValue = true;
            this.cWareHouses.Location = new System.Drawing.Point(196, 1);
            this.cWareHouses.Name = "cWareHouses";
            this.cWareHouses.Properties.AllowGrayed = true;
            this.cWareHouses.Properties.Caption = "Все";
            this.cWareHouses.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cWareHouses.Size = new System.Drawing.Size(43, 19);
            this.cWareHouses.TabIndex = 81;
            this.cWareHouses.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.tableLayoutPanel2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(454, 0);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(707, 207);
            this.groupControl2.TabIndex = 80;
            this.groupControl2.Text = "По территории";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 30);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 230F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1161, 437);
            this.tableLayoutPanel3.TabIndex = 81;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.94005F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.33015F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.81668F));
            this.tableLayoutPanel4.Controls.Add(this.groupControl3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupControl2, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupControl4, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1161, 207);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.tTerritory);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(219, 207);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "Территория:";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.tStaffing);
            this.groupControl4.Controls.Add(this.cStaffing);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(219, 0);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(235, 207);
            this.groupControl4.TabIndex = 1;
            this.groupControl4.Text = "Персонал:";
            // 
            // tStaffing
            // 
            this.tStaffing.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colPersName,
            this.colPersParendId,
            this.colPersStaffId,
            this.colPersStaffIdReal});
            this.tStaffing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tStaffing.KeyFieldName = "wStaffID";
            this.tStaffing.Location = new System.Drawing.Point(2, 22);
            this.tStaffing.Name = "tStaffing";
            this.tStaffing.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.tStaffing.OptionsView.ShowCheckBoxes = true;
            this.tStaffing.OptionsView.ShowColumns = false;
            this.tStaffing.OptionsView.ShowHorzLines = false;
            this.tStaffing.OptionsView.ShowIndicator = false;
            this.tStaffing.OptionsView.ShowVertLines = false;
            this.tStaffing.ParentFieldName = "wParentID";
            this.tStaffing.Size = new System.Drawing.Size(231, 183);
            this.tStaffing.TabIndex = 82;
            this.tStaffing.AfterEditorStateChanged += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.StateChanged(this.cStaffing_CheckedChanged);
            this.tStaffing.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tStaffing_AfterNodeChecked);
            // 
            // colPersName
            // 
            this.colPersName.Caption = "Персонал";
            this.colPersName.CustomizationCaption = "Территория";
            this.colPersName.FieldName = "Name";
            this.colPersName.Name = "colPersName";
            this.colPersName.OptionsColumn.AllowEdit = false;
            this.colPersName.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersName.OptionsColumn.ReadOnly = true;
            this.colPersName.Visible = true;
            this.colPersName.VisibleIndex = 0;
            // 
            // colPersParendId
            // 
            this.colPersParendId.Caption = "wParentID";
            this.colPersParendId.FieldName = "wParentID";
            this.colPersParendId.Name = "colPersParendId";
            this.colPersParendId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersParendId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colPersStaffId
            // 
            this.colPersStaffId.Caption = "wStaffID";
            this.colPersStaffId.FieldName = "wStaffID";
            this.colPersStaffId.Name = "colPersStaffId";
            this.colPersStaffId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersStaffId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colPersStaffIdReal
            // 
            this.colPersStaffIdReal.Caption = "StaffID";
            this.colPersStaffIdReal.FieldName = "StaffID";
            this.colPersStaffIdReal.Name = "colPersStaffIdReal";
            this.colPersStaffIdReal.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersStaffIdReal.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cStaffing
            // 
            this.cStaffing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cStaffing.EditValue = true;
            this.cStaffing.Location = new System.Drawing.Point(187, 3);
            this.cStaffing.Name = "cStaffing";
            this.cStaffing.Properties.AllowGrayed = true;
            this.cStaffing.Properties.Caption = "Все";
            this.cStaffing.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cStaffing.Size = new System.Drawing.Size(43, 19);
            this.cStaffing.TabIndex = 84;
            this.cStaffing.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 368F));
            this.tableLayoutPanel5.Controls.Add(this.groupControl5, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.groupControl1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 207);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1161, 230);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // groupControl5
            // 
            this.groupControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl5.Controls.Add(this.tableLayoutPanel1);
            this.groupControl5.Location = new System.Drawing.Point(3, 3);
            this.groupControl5.Name = "groupControl5";
            this.tableLayoutPanel5.SetRowSpan(this.groupControl5, 2);
            this.groupControl5.Size = new System.Drawing.Size(787, 224);
            this.groupControl5.TabIndex = 82;
            this.groupControl5.Text = "Оборудование";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 212F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 332F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 22);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(783, 200);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cEquipmentInCapable);
            this.panel2.Controls.Add(this.cEquipmentCapable);
            this.panel2.Controls.Add(this.cbTechnicalState);
            this.panel2.Controls.Add(this.lCapability);
            this.panel2.Controls.Add(this.lTecnichalState);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(206, 138);
            this.panel2.TabIndex = 0;
            // 
            // cEquipmentInCapable
            // 
            this.cEquipmentInCapable.EditValue = true;
            this.cEquipmentInCapable.Location = new System.Drawing.Point(5, 50);
            this.cEquipmentInCapable.Name = "cEquipmentInCapable";
            this.cEquipmentInCapable.Properties.Caption = "Непригодное к эксплуатации";
            this.cEquipmentInCapable.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cEquipmentInCapable.Size = new System.Drawing.Size(198, 19);
            this.cEquipmentInCapable.TabIndex = 100;
            this.cEquipmentInCapable.CheckedChanged += new System.EventHandler(this.cEquipmentCapables_CheckedChanged);
            // 
            // cEquipmentCapable
            // 
            this.cEquipmentCapable.EditValue = true;
            this.cEquipmentCapable.Location = new System.Drawing.Point(5, 32);
            this.cEquipmentCapable.Name = "cEquipmentCapable";
            this.cEquipmentCapable.Properties.Caption = "Пригодное к эксплуатации";
            this.cEquipmentCapable.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cEquipmentCapable.Size = new System.Drawing.Size(198, 19);
            this.cEquipmentCapable.TabIndex = 99;
            this.cEquipmentCapable.CheckedChanged += new System.EventHandler(this.cEquipmentCapables_CheckedChanged);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.lManufacturer, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.cbBrand, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.lBrand, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.cbManufacturer, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(215, 147);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(233, 50);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.groupControl8, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(215, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(233, 138);
            this.tableLayoutPanel7.TabIndex = 2;
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.tModels);
            this.groupControl8.Controls.Add(this.cModels);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl8.Location = new System.Drawing.Point(0, 0);
            this.groupControl8.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(233, 138);
            this.groupControl8.TabIndex = 83;
            this.groupControl8.Text = "Модель оборудования";
            // 
            // cModels
            // 
            this.cModels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cModels.EditValue = true;
            this.cModels.Location = new System.Drawing.Point(188, 2);
            this.cModels.Name = "cModels";
            this.cModels.Properties.AllowGrayed = true;
            this.cModels.Properties.Caption = "Все";
            this.cModels.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cModels.Size = new System.Drawing.Size(43, 19);
            this.cModels.TabIndex = 80;
            this.cModels.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(454, 147);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(326, 50);
            this.panel3.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lProjects);
            this.panel4.Controls.Add(this.cbProjects);
            this.panel4.Controls.Add(this.tRepairDates);
            this.panel4.Controls.Add(this.tReservationDates);
            this.panel4.Controls.Add(this.tIncomeDates);
            this.panel4.Controls.Add(this.tProductionDates);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(454, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(326, 138);
            this.panel4.TabIndex = 4;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lActStatus);
            this.groupControl1.Controls.Add(this.tReturningDates);
            this.groupControl1.Controls.Add(this.lAgreementStatus);
            this.groupControl1.Controls.Add(this.tAgreementAssignment);
            this.groupControl1.Controls.Add(this.cbActStatus);
            this.groupControl1.Controls.Add(this.tInstalationDates);
            this.groupControl1.Controls.Add(this.cbAgreementStatus);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(793, 3);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(368, 166);
            this.groupControl1.TabIndex = 82;
            this.groupControl1.Text = "Документация";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.generateReportButton);
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(793, 169);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(368, 61);
            this.panel1.TabIndex = 82;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 472);
            this.Controls.Add(this.cShowTransitions);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tReportDates);
            this.Controls.Add(this.lblUpd2);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 480);
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры к отчету \"Учет оборудования\"";
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cShowTransitions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluDistr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tWarehouseLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPosLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluWarehouses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tModels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTechnicalState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBrand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbManufacturer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProjects.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAgreementStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cWareHouses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffing.Properties)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cEquipmentInCapable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEquipmentCapable.Properties)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cModels.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.SimpleButton generateReportButton;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.CheckEdit cShowTransitions;
        private ComboboxExtended cbDistrType;
        private DevExpress.XtraEditors.LabelControl lDistrType;
        private DevExpress.XtraEditors.LabelControl lDistr;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lWarehouses;
        private DevExpress.XtraEditors.LabelControl lBrand;
        private DevExpress.XtraEditors.LabelControl lManufacturer;
        private DevExpress.XtraEditors.LabelControl lProjects;
        private DevExpress.XtraEditors.LabelControl lCapability;
        private DevExpress.XtraEditors.LabelControl lTecnichalState;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.Grid.GridView component1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking cluWarehouses;
        private DevExpress.XtraGrid.Views.Grid.GridView userControl21View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouse;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWareId;
        private DevExpress.XtraGrid.Columns.GridColumn colWareLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWareParentId;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tTerritory;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTerritory;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tPosLocation;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tWarehouseLocation;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tReportDates;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tAgreementAssignment;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tReturningDates;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tInstalationDates;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tRepairDates;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tProductionDates;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tIncomeDates;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tReservationDates;
        private DevExpress.XtraEditors.LabelControl lActStatus;
        private DevExpress.XtraEditors.LabelControl lAgreementStatus;
        private ComboboxExtended cbActStatus;
        private ComboboxExtended cbAgreementStatus;
        private ComboboxExtended cbProjects;
        private ComboboxExtended cbManufacturer;
        private ComboboxExtended cbBrand;
        private ComboboxExtended cbTechnicalState;
        private ComboboxExtended cbPos;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tModels;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn5;
        private GridLookUpWithMultipleChecking cluDistr;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrFieldParentID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPosLocGeographyID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPosLocGeographyOldId;
        private DevExpress.XtraEditors.CheckEdit cStaffing;
        private DevExpress.XtraEditors.CheckEdit cModels;
        private DevExpress.XtraEditors.CheckEdit cPos;
        private DevExpress.XtraEditors.CheckEdit cWareHouses;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private TreeListIncremental tStaffing;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersParendId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersStaffId;
        private DevExpress.XtraEditors.CheckEdit cEquipmentInCapable;
        private DevExpress.XtraEditors.CheckEdit cEquipmentCapable;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersStaffIdReal;
        private DevExpress.XtraGrid.Columns.GridColumn colWareM3Id;
	}
}