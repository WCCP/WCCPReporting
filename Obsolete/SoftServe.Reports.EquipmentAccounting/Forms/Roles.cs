﻿namespace SoftServe.Reports.EquipmentAccounting.Forms
{
    /// <summary>
    /// Describes MM role set
    /// </summary>
    enum Roles
    {
        SkuM5,
        SkuM3,
        DistrPushM5,
        DistrPushM3,
        FocusSkuM5,
        FocusSkuM3,
        FocusGoalM5,
        FocusGoalM3,
        KpiRatingM5,
        KpiRatingM3,
        GeneralM5,
        GeneralM3
    }
}
