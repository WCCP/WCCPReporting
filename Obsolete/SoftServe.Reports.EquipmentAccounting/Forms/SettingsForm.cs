﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.CommonControls;
using SoftServe.Reports.EquipmentAccounting.Utility;

namespace SoftServe.Reports.EquipmentAccounting
{
    public partial class SettingsForm : XtraForm
    {
        #region Constants

        private const char STRING_DELIMITER = ',';

        private const string DumbFilter = "-17";

        private const string FilterByLevel = "Level = {0}";
        private const string FilterByLevelLess = "Level < {0}";
        private const string AndFilterByColumn = " and {0} in ({1})";
        private const string FilterByColumn = "{0} in ({1})";

        private const string Level = "Level";

        private const string PosParentIdField = "FieldParentID";
        private const string DistrWorkSchema = "SchemaOfWork";

        private const string IsEquipnentCapable = "isPossible";

        private int _staffM3Level = 4;

        #endregion

        #region Private Fields

        private int WarehousesLevel;

        private int DistributorsLevel;
        private int PosLevel;

        private int PosLocationLevel;


        private DataTable distributors;
        private DataTable posLocations;
        private DataTable warehouses;
        private DataTable projects;

        private bool ShouldClearWarehousesSelection = false;
        private bool ShouldClearPosesSelection = false;

        #endregion

        /// <summary>
        /// Creates the new instance od <c>SettingsForm</c>/>
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();

            InitDataSources();
            SetDateIntervals();
            SetInitialSettings();

            tReportDates.EndDateChanged += tReportDates_EndDateChanged;
        }

        /// <summary>
        /// Performs gathering of settings info
        /// </summary>
        /// <returns>Current setting info</returns>
        internal ReportInfo GetSettingsData()
        {
            ReportInfo settings = new ReportInfo();

            settings.ReportDates = tReportDates.Interval;

            settings.RegionIds = tTerritory.GetCheckedIdsValues();
            settings.StaffingsIds = DataProvider.IsLDB ? tStaffing.GetCheckedColumnValuesChildren(colPersStaffIdReal.FieldName, Level, 6) : tStaffing.GetCheckedColumnValuesAll(colPersStaffIdReal.FieldName);

            settings.PosIds = cbPos.GetComboBoxCheckedValues();

            settings.WarehousesIds = cluWarehouses.SelectedValues("wFieldID");

            settings.TechConditionIds = cbTechnicalState.GetComboBoxCheckedValues();
            settings.ModelsIds = tModels.GetCheckedColumnValues("NaturalID", "Level", 1);
            settings.ManufacturersIds = cbManufacturer.GetComboBoxCheckedValues();
            settings.BrandsIds = cbBrand.GetComboBoxCheckedValues();

            settings.Projects = GetProjectsNames(cbProjects.GetComboBoxCheckedValues());

            settings.ProductionDates = tProductionDates.Interval;
            settings.InvoiceDates = tIncomeDates.Interval;
            settings.RepairDates = tRepairDates.Interval;

            settings.ReservationDates = tReservationDates.Interval;
            
            settings.ContractDates = tAgreementAssignment.Interval;

            settings.ContractStatusIds = cbAgreementStatus.GetComboBoxCheckedValues();
            settings.OrderStatusIds = cbActStatus.GetComboBoxCheckedValues();

            settings.InstalationDates = tInstalationDates.Interval;
            settings.ReturningDates = tReturningDates.Interval;

            settings.DoShowTransitions = cShowTransitions.Checked;

            return settings;
        }
        
        /// <summary>
        /// Assigns datasources to contols
        /// </summary>
        private void InitDataSources()
        {
            tStaffing.LoadDataSource(DataProvider.GetStaffingTree(DateTime.Now));

            distributors = DataProvider.GetDistributorsTree(out DistributorsLevel);
            PosLevel = DistributorsLevel + 1;
            posLocations = DataProvider.GetPosesTree(out PosLocationLevel);
            warehouses = DataProvider.GetWarehouseTree(out WarehousesLevel);

            tTerritory.LoadDataSource(DataProvider.GetTerritoryTree(2, 3));

            cbDistrType.LoadDataSource(DataProvider.GetDistrTypesList());

            DataView lDataViewWarehouses = warehouses.DefaultView;
            lDataViewWarehouses.RowFilter = string.Format(FilterByLevelLess, WarehousesLevel);
            tWarehouseLocation.LoadDataSource(lDataViewWarehouses.ToTable());

            cluWarehouses.LoadDataSource(warehouses.Copy());

            cluDistr.LoadDataSource(distributors.Copy());
            cbPos.LoadDataSource(distributors.Copy());

            tPosLocation.LoadDataSource(posLocations.Copy());

            cbActStatus.LoadDataSource(DataProvider.GetStatusesList());
            cbAgreementStatus.LoadDataSource(DataProvider.GetStatusesList());

            cbTechnicalState.LoadDataSource(DataProvider.GetTechnicalConditionsList());

            cbBrand.LoadDataSource(DataProvider.GetBrandList());

            tModels.LoadDataSource(DataProvider.GetModelsTree());
            HideModels();

            cbManufacturer.LoadDataSource(DataProvider.GetManufacturersList());

            projects = DataProvider.GetProjectsList();
            cbProjects.LoadDataSource(projects);
        }

        /// <summary>
        /// Sets initial settings
        /// </summary>
        private void SetInitialSettings()
        {
            tTerritory.CheckNodes(CheckState.Checked);
            FilterTreesByRegion();

            tStaffing.CheckNodes(CheckState.Checked);
            tStaffing.AssignCheckEdit(cStaffing);

            cbDistrType.SelectAll();

            tPosLocation.CheckNodes(CheckState.Checked);
            tPosLocation.AssignCheckEdit(cPos);

            tWarehouseLocation.CheckNodes(CheckState.Checked);
            tWarehouseLocation.AssignCheckEdit(cWareHouses);

            FilterDistrGeneral();
            cluDistr.SelectAll();

            FilterPosListGeneral();
            cbPos.SelectAll();

            FilterWarehousesListGeneral();
            cluWarehouses.SelectAll();

            tModels.CheckNodes(CheckState.Checked);
            tModels.AssignCheckEdit(cModels);

            cbTechnicalState.SelectAll();
            cbBrand.SelectAll();
            cbManufacturer.SelectAll();
            cbProjects.SelectAll();

            cbActStatus.SelectAll();

            cShowTransitions.Checked = false;
        }

        /// <summary>
        /// Sets default date intervals
        /// </summary>
        private void SetDateIntervals()
        {
            DateTime lCurrentMonthBeginning = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            tReportDates.dTo.Properties.AllowNullInput = DefaultBoolean.False;

            tReportDates.To = DateTime.Now;
            tReportDates.From = lCurrentMonthBeginning;

            tAgreementAssignment.SetMaxValue(DateTime.MaxValue);
            tReservationDates.SetMaxValue(DateTime.MaxValue);
        }

        /// <summary>
        /// Handles report end date changes
        /// </summary>
        private void ReloadDateDependentData()
        {
            tStaffing.LoadDataSource(DataProvider.GetStaffingTree(tReportDates.To));

            FilterStaffingTree();
        }

        /// <summary>
        /// Validates selected data
        /// </summary>
        /// <param name="reportInfo">Settings to be checked</param>
        /// <param name="message">Warning message</param>
        /// <returns><c>true</c> if settings are valid, <c>false</c> otherwise</returns>
        private bool AreSettingsValid(ReportInfo reportInfo, out string message)
        {
            message = string.Empty;

            message += reportInfo.ReportDates.IsValid || !cShowTransitions.Checked
                           ? string.Empty
                           : "\t- Период времени: отсутствует дата С;\r\n";
            message += reportInfo.WarehousesIds.Count == 0 ? "\t- не выбрано ни одного склада;\r\n" : string.Empty;

            message += reportInfo.TechConditionIds.Count > 0
                           ? string.Empty
                           : "\t- не выбрано ни одного технического состояния;\r\n";

            message += reportInfo.ModelsIds.Count > 0
                           ? string.Empty
                           : "\t- не выбрано ни одной модели оборудования;\r\n";
            message += reportInfo.ManufacturersIds.Count > 0
                           ? string.Empty
                           : "\t- не выбрано ни одного производителя;\r\n";
            message += reportInfo.BrandsIds.Count > 0 ? string.Empty : "\t- не выбрано ни одного бренда;\r\n";

            message += reportInfo.Projects.Count > 0 ? string.Empty : "\t- не выбрано ни одного проекта;\r\n";

            message += reportInfo.OrderStatusIds.Count > 0 ? string.Empty : "\t- не выбрано ни одного статуса акта;\r\n";

            message += reportInfo.ProductionDates.IsValid
                           ? string.Empty
                           : "\t- Дата выпуска: некорректно введен период;\r\n";
            message += reportInfo.InvoiceDates.IsValid
                           ? string.Empty
                           : "\t- Дата прихода: некорректно введен период;\r\n";
            message += reportInfo.RepairDates.IsValid
                           ? string.Empty
                           : "\t- Дата восстановления: некорректно введен период;\r\n";
            message += reportInfo.ReservationDates.IsValid
                           ? string.Empty
                           : "\t- Дата резервирования на проекте: некорректно введен период;\r\n";
            message += reportInfo.ContractDates.IsValid
                           ? string.Empty
                           : "\t- Дата заключения договора: некорректно введен период;\r\n";
            message += reportInfo.InstalationDates.IsValid
                           ? string.Empty
                           : "\t- Дата установки в системе: некорректно введен период;\r\n";
            message += reportInfo.ReturningDates.IsValid
                           ? string.Empty
                           : "\t- Дата возврати в системе: некорректно введен период;\r\n";
            
            return message.Length == 0;
        }

        /// <summary>
        /// Performs filtering of Staffing, POS and Warehouses trees by checked regions
        /// </summary>
        private void FilterTreesByRegion()
        {
            List<string> lFilter = tTerritory.GetCheckedNodesIds();

            if (cPos.CheckState == CheckState.Indeterminate && lFilter.Count == 0)
            {
                cPos.CheckState = CheckState.Unchecked;
            }

            if (cWareHouses.CheckState == CheckState.Indeterminate && lFilter.Count == 0)
            {
                cWareHouses.CheckState = CheckState.Unchecked;
            }

            if (lFilter.Count == 0)
            {
                lFilter.Add("-5");
            }

            FilterStaffingTree();

            tPosLocation.FilterTree(lFilter);
            HidePosLevel();

            tWarehouseLocation.FilterTree(lFilter);
        }

        #region Filtering

        /// <summary>
        /// Performs stuffing filtering - by regions
        /// </summary>
        private void FilterStaffingTree()
        {
            List<string> lTerritoryFilter = tTerritory.GetCheckedNodesIds();

            List<string> lStaffingFilter = new List<string>();

            foreach (TreeListNode node in tStaffing.Nodes)
            {
                if (lTerritoryFilter.Contains(node[colPersParendId.FieldName].ToString()))
                {
                    lStaffingFilter.Add(node[colPersStaffId.FieldName].ToString());
                }
            }

            if (lStaffingFilter.Count == 0)
            {
                lStaffingFilter.Add(DumbFilter);
            }

            List<string> values = tStaffing.GetCheckedColumnValuesAll(tStaffing.KeyFieldName);
            tStaffing.FilterTree(lStaffingFilter);

            switch (cStaffing.CheckState)
            {
                case CheckState.Checked:
                    tStaffing.CheckNodes(CheckState.Checked);
                    break;
                case CheckState.Unchecked:
                    tStaffing.CheckNodes(CheckState.Unchecked);
                    break;
                case CheckState.Indeterminate:
                    if (lTerritoryFilter.Count == 0)
                    {
                        cStaffing.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        tStaffing.CheckNodes(values, CheckState.Checked);
                    }
                    break;
            }
        }

        /// <summary>
        /// Performs filtering of distributors by territory and distr types
        /// </summary>
        private void FilterDistrGeneral()
        {
            string lFilter = string.Format(FilterByLevel, DistributorsLevel);

            //by region
            string regionFilter = tTerritory.GetCheckedIdsValues();

            if (!string.IsNullOrEmpty(regionFilter)) //there are selected regions
            {
                lFilter += string.Format(AndFilterByColumn, colDistrFieldParentID.FieldName, regionFilter);
            }
            else
            {
                lFilter += string.Format(AndFilterByColumn, colDistrFieldParentID.FieldName, DumbFilter);
            }

            //by distr type
            string distrTypesFilter = cbDistrType.GetComboBoxCheckedValuesList();

            if (!string.IsNullOrEmpty(distrTypesFilter)) //there are selected types
            {
                lFilter += string.Format(AndFilterByColumn, DistrWorkSchema, distrTypesFilter);
            }

            List<string> values = cluDistr.SelectedIds();
            //apply filter
            //cluDistr.FilterList(lFilter);

            cluDistr.ClearSelection();

            DataView lDataView = distributors.DefaultView;
            lDataView.RowFilter = lFilter;
            lDataView.Sort = cluDistr.Properties.DisplayMember;
            cluDistr.Properties.DataSource = lDataView.ToTable(true, cluDistr.Properties.ValueMember,
                                                                  cluDistr.Properties.DisplayMember, DistrWorkSchema);

            cluDistr.SetComboBoxCheckedValues(values);
        }

        /// <summary>
        /// Perfoms filtering of warehouses by territory
        /// </summary>
        private void FilterWarehousesListGeneral()
        {
            string lFilter = string.Format(FilterByLevel, WarehousesLevel);

            //by warehouse location
            string lWarehouseLocationFilter = tWarehouseLocation.GetCheckedColumnValuesAllList(colWareId.FieldName);

            if (!string.IsNullOrEmpty(lWarehouseLocationFilter)) //there is warehouses location selected
            {
                lFilter += string.Format(AndFilterByColumn, colWareParentId.FieldName, lWarehouseLocationFilter);
            }
            else //there is no territory selected
            {
                lWarehouseLocationFilter = tWarehouseLocation.GetCheckedColumnValuesAllList(colWareId.FieldName,
                                                                                            CheckState.Unchecked);

                if (!string.IsNullOrEmpty(lWarehouseLocationFilter))
                {
                    lFilter += string.Format(AndFilterByColumn, colWareParentId.FieldName, lWarehouseLocationFilter);
                }
                else // for параноїків
                {
                    lFilter += string.Format(AndFilterByColumn, colWareParentId.FieldName, DumbFilter);
                }
            }

            //by stuffing M3
            string lStuffM3Filter = string.Join(",",
                                                tStaffing.GetCheckedAndIndeterminateColumnValues(
                                                    colPersStaffIdReal.FieldName, Level, _staffM3Level).ToArray());

            if (!string.IsNullOrEmpty(lStuffM3Filter)) //there is stuff selected
            {
                lFilter += string.Format(" and ({0} is null or {1})", colWareM3Id.FieldName,
                                         string.Format(FilterByColumn, colWareM3Id.FieldName, lStuffM3Filter));
            }
            else
            {
                lFilter += string.Format(" and ({0} is null)", colWareM3Id.FieldName);
            }

            List<string> values = cluWarehouses.SelectedIds();
            //apply filter
            cluWarehouses.FilterList(lFilter);

            if (!ShouldClearWarehousesSelection)
            {
                cluWarehouses.SetComboBoxCheckedValues(values);
            }
            else
            {
                ShouldClearWarehousesSelection = false;
            }
        }

        /// <summary>
        /// Performs filtering of Pos by distributors (so as by territory)
        /// </summary>
        private void FilterPosListGeneral()
        {
            string lFilter = string.Format(FilterByLevel, PosLevel);

            //by distr
            string lDistrCheckedFilter = cluDistr.SelectedIdsString();
            if (!string.IsNullOrEmpty(lDistrCheckedFilter)) //there are distr selected
            {
                lFilter += string.Format(AndFilterByColumn, PosParentIdField, GetDistrStringDescription(lDistrCheckedFilter));
            }
            else //there are no distr selected
            {
                lDistrCheckedFilter = cluDistr.AllIdsString();
                if (!string.IsNullOrEmpty(lDistrCheckedFilter))
                {
                    lFilter += string.Format(AndFilterByColumn, PosParentIdField, GetDistrStringDescription(lDistrCheckedFilter));
                }
                else // for параноїків
                {
                    lFilter += string.Format(AndFilterByColumn, PosParentIdField, DumbFilter);
                }
            }

            string lPosLocationFilter =
                tPosLocation.GetInvisibleColumnValuesChildrenList(colPosLocGeographyOldId.FieldName, Level,
                                                                  PosLocationLevel);
            if (!string.IsNullOrEmpty(lPosLocationFilter)) //there are Poses selected
            {
                lFilter += string.Format(AndFilterByColumn, cbPos.Properties.ValueMember, lPosLocationFilter);
            }
            else //check whether there is the territory selected
            {
                List<string> lPosTerritoryFilter = tPosLocation.GetCheckedColumnValues(tPosLocation.KeyFieldName, Level,
                                                                                       PosLocationLevel - 1);

                if (lPosTerritoryFilter.Count > 0)
                {
                    lFilter += string.Format(AndFilterByColumn, cbPos.Properties.ValueMember, DumbFilter);
                }
            }

            List<string> values = cbPos.GetComboBoxCheckedValues();
            //apply filter
            cbPos.FilterList(lFilter);

            if (!ShouldClearPosesSelection)
            {
                cbPos.SetComboBoxCheckedValues(values);
            }
            else
            {
                ShouldClearPosesSelection = false;
            }
        }

        /// <summary>
        /// Hides POS level of POS location tree
        /// </summary>
        private void HidePosLevel()
        {
            tPosLocation.HideLevel(Level, PosLocationLevel);
        }

        /// <summary>
        /// Hides empty models 
        /// </summary>
        private void HideModels()
        {
            List<string> lMolels = tModels.GetNodesWithoutChildren(tModels.KeyFieldName, Level, 0);

            tModels.BeginUpdate();
            foreach (string nodeId in lMolels)
            {
                TreeListNode lNode = tModels.GetNodeByKeyField(nodeId);
                tModels.SetChildrensVisibility(lNode, false);
            }
            tModels.EndUpdate();
        }

        private string GetDistrStringDescription(string filterCode)
        {
            DataView lDataView = distributors.DefaultView;
            lDataView.RowFilter = string.Format(FilterByLevel, DistributorsLevel) + string.Format(AndFilterByColumn, cluDistr.Properties.ValueMember, filterCode);// "FIeldCode in (" + filterCode + ") and Level = 4";

            List<string> lFilterIds = new List<string>();

            foreach (DataRow row in lDataView.ToTable().Rows)
            {
                lFilterIds.Add(row["FieldID"].ToString());
            }

            return string.Join(",", lFilterIds.ToArray());
        }

        private List<string> GetProjectsNames(List<string> projectIds)
        {
            List<string> lProjects = new List<string>();

            foreach (string projectId in projectIds)
            {
                lProjects.Add(projects.Rows[int.Parse(projectId)].Field<string>("Project"));
            }

            return lProjects;
        }

        #endregion

        #region Event Handling

        /// <summary>
        /// Handles AfterCheckNode event of tTerritory: performs territory dependent filtering
        /// </summary>
        /// <param name="sender">tTerritory</param>
        /// <param name="e">NodeEventArgs</param>
        private void tTerritory_AfterNodeChecked(object sender, EventArgs args)
        {
            FilterTreesByRegion();

            FilterDistrGeneral();

            FilterPosListGeneral();
            HidePosLevel();

            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Handles Closed event of cbDistr: performs dist dependent filtering
        /// </summary>
        /// <param name="sender">cbDistr</param>
        /// <param name="e">ClosedEventArgs</param>
        private void cbDistr_Closed(object sender, ClosedEventArgs e)
        {
            lDistr.Focus();
            FilterPosListGeneral();
        }

        /// <summary>
        /// Handles Closed event of cbDistrType: perfoms dist type dependent filtering
        /// </summary>
        /// <param name="sender">cbDistrType</param>
        /// <param name="e">ClosedEventArgs</param>
        private void cbDistrType_Closed(object sender, ClosedEventArgs e)
        {
            FilterDistrGeneral();

            FilterPosListGeneral();
        }

        /// <summary>
        /// Filters warehouses by location
        /// </summary>
        private void tWarehouseLocation_AfterCheckNode(object sender, EventArgs e)
        {
            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Filters POSs by locations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tPosLocation_AfterCheckNode(object sender, EventArgs e)
        {
            FilterPosListGeneral();
        }

        /// <summary>
        /// Handles EndDateChanged event of tReportDates
        /// </summary>
        /// <param name="sender">tReportDates</param>
        /// <param name="args">PeriodChangedArgs</param>
        private void tReportDates_EndDateChanged(object sender, PeriodChangedArgs args)
        {
            ReloadDateDependentData();
        }

        /// <summary>
        /// Handles Click event of generateReportButton
        /// </summary>
        /// <param name="sender">generateReportButton</param>
        /// <param name="e">EventArgs</param>
        private void generateReportButton_Click(object sender, EventArgs e)
        {
            ReportInfo settings = GetSettingsData();
            string message;

            if (AreSettingsValid(settings, out message))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Не все параметры введены:\n\r\n" + message, "Предупреждение", MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Selects/deselects all Pos locations
        /// </summary>
        private void cPos_CheckedChanged(object sender, EventArgs e)
        {
            if (cPos.CheckState == CheckState.Unchecked)
            {
                ShouldClearPosesSelection = true;
            }

            HidePosLevel();
            FilterPosListGeneral();
        }

        /// <summary>
        /// Selects/deselects all warehouses locations
        /// </summary>
        private void cWareHouses_CheckedChanged(object sender, EventArgs e)
        {
            if (cWareHouses.CheckState == CheckState.Unchecked)
            {
                ShouldClearWarehousesSelection = true;
            }

            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Forces selected list redrawing
        /// </summary>
        private void cluWarehouses_Closed(object sender, ClosedEventArgs e)
        {
            lWarehouses.Focus();
        }

        /// <summary>
        /// Performs equipment state filtering
        /// </summary>
        private void cEquipmentCapables_CheckedChanged(object sender, EventArgs e)
        {
            string lFilter = string.Empty;

            if (!cEquipmentCapable.Checked && !cEquipmentInCapable.Checked)
            {
                lFilter = string.Format(FilterByColumn, IsEquipnentCapable, DumbFilter);

                cbTechnicalState.FilterList(lFilter);
            }
            else
            {
                List<string> lCheckedValues = cbTechnicalState.GetComboBoxCheckedValues();
                List<string> lAllValues =
                    new List<string>(cbTechnicalState.GetComboBoxValuesList().Split(STRING_DELIMITER));

                if (cEquipmentCapable.Checked && cEquipmentInCapable.Checked)
                {
                    lFilter = string.Empty;
                }
                else
                {
                    lFilter = string.Format(FilterByColumn, IsEquipnentCapable,
                                            cEquipmentCapable.Checked
                                                ? (int)EquipmentCapability.Capable
                                                : (int)EquipmentCapability.Incapable);
                }

                cbTechnicalState.FilterList(lFilter);

                for (int i = 0; i < cbTechnicalState.Properties.Items.Count; i++)
                {
                    string lValue = cbTechnicalState.Properties.Items[i].Value.ToString();

                    if (lAllValues.Contains(lValue))
                    {
                        cbTechnicalState.Properties.Items[i].CheckState = lCheckedValues.Contains(lValue)
                                                                              ? CheckState.Checked
                                                                              : CheckState.Unchecked;
                    }
                    else
                    {
                        cbTechnicalState.Properties.Items[i].CheckState = CheckState.Checked;
                    }
                }

            }
        }

        /// <summary>
        /// Performs warehouse filtering 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void tStaffing_AfterNodeChecked(object sender, EventArgs args)
        {
            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Performs warehouse filtering 
        /// </summary>
        private void cStaffing_CheckedChanged(object sender, EventArgs e)
        {
            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Coordinates dependency between tReportDates start date accessibility and cShowTransitions state
        /// </summary>
        private void cShowTransitions_CheckedChanged(object sender, EventArgs e)
        {
            tReportDates.dFrom.Enabled = cShowTransitions.Checked;
            tReportDates.dFrom.DateTime = cShowTransitions.Checked ? new DateTime(tReportDates.dTo.DateTime.Year, tReportDates.dTo.DateTime.Month, 1) : DateTime.MinValue;
        }
        #endregion
    }
}
