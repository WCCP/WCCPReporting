﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.EquipmentAccounting;
using SoftServe.Reports.EquipmentAccounting.Tabs;
using System;
using System.Data;
using System.Windows.Forms;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        /// <summary>
        /// Report's settings form 
        /// </summary>
        readonly SettingsForm settingsForm = new SettingsForm();

        private MainTab _mainTab;

        /// <summary>
        /// Initializes the new instance of <see cref="WccpUIControl"/> class
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            Localizer.Active = new RussianEditorsLocalizer();

            SetExportType(ExportToType.Pdf, false);
            SetExportType(ExportToType.Rtf, false);

            MenuButtonsRendering += WccpUIControl_MenuButtonsRendering;
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            CustomExport += OnCustomExport;
            RefreshClick += WccpUIControl_RefreshClick;
        }

        public int ReportInit()
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                LoadReport();
                return 0;
            }

            return 1;
        }

        /// <summary>
        /// Drives export activity
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="selectedPage">Page to be exported</param>
        /// <param name="type">Export type</param>
        /// <returns>Composite link to be exported</returns>
        private void OnCustomExport(object sender, CustomExportEventArgs customExportEventArgs)
        {
            if (!WasReportLoaded)
            {
                customExportEventArgs.Handled = true;
                return;
            }

            string lFilePath = SelectFilePath(customExportEventArgs.ExportType);

            if (!string.IsNullOrEmpty(lFilePath) && customExportEventArgs.ExportPage != null)
            {
                MainTab tab = customExportEventArgs.ExportPage as MainTab;

                try
                {
                    WaitManager.StartWait();
                    tab.ExportToXls(lFilePath, customExportEventArgs.ExportType);
                }
                catch (OutOfMemoryException)
                {
                    XtraMessageBox.Show("Не хватает памяти: слишком много пользователей или данных",
                                        "Ошибка при выгрузке данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception)
                {
                    XtraMessageBox.Show("При выгрузке данных случилась ошибка", "Ошибка при выгрузке данных",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    WaitManager.StopWait();
                }
            }

            customExportEventArgs.Handled = true;
        }

        /// <summary>
        /// Handles Menu button visibilyty/accessibility
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="eventArgs">Event args</param>
        private void WccpUIControl_MenuButtonsRendering(object sender, MenuButtonsRenderingEventArgs eventArgs)
        {
            eventArgs.ShowExportAllBtn = false;
            eventArgs.ShowPrintAllBtn = false;
            eventArgs.ShowPrintBtn = false;
            eventArgs.ShowExportBtn = true;
        }

        /// <summary>
        /// Handles Settings Form button click
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="selectedPage">Active page</param>
        private void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                LoadReport();
            }
        }

        /// <summary>
        /// Hanldes Refresh button click
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="selectedPage">Page to be refreshed</param>
        private void WccpUIControl_RefreshClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (WasReportLoaded)
            {
                LoadReport();
            }
        }

        /// <summary>
        /// Loads data from DB and displays it
        /// </summary>
        private void LoadReport()
        {
            tabManager.TabPages.Clear();

            WaitManager.StartWait();
            try
            {
                ReportInfo settings = settingsForm.GetSettingsData();
                DataTable sourceData = DataProvider.GetMainReportData(settings);

                _mainTab = new MainTab(settings, sourceData);
                tabManager.TabPages.Add(_mainTab);
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        /// <summary>
        /// Returns <c>true</c> if report has already been loaded
        /// </summary>
        private bool WasReportLoaded
        {
            get { return _mainTab.MainControl != null; }
        }
    }
}
