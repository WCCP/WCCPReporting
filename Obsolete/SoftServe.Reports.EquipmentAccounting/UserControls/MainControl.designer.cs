﻿namespace SoftServe.Reports.EquipmentAccounting.UserControls
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAgreementNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqSWAgreementNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreementDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreementEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreementStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActReturnNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActReturnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInstalationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActReturningDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSystemReturningDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqSystemDocumentNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqSystemReturnDocNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBelongsToRoute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPocCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocJurName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocActualAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubtype = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodeInn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerritoryType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPopulation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTown = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCityDistrict = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colМ4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colМ3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3ResponsibleForTO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3Location = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffingChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseChanel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoorsQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentKind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstInstalationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManufacterer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConstructionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncomeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegenerationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjectReservationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvenNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBrand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpluatationAbility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTechnicalState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastStatusChangeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInitPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_CurrentTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_CurrentRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_DistrSAPCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_CurrentDistributo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_WorkSchema = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_CurrentCustLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_CurrentCustomer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_CurrentWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRO_CurrentWarehouseLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSenderTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionPopulation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSenderRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistributor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiverDistr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrSAPCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrSAPCodeReciever = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkSchema = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkSchemeReciever = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrBranchTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrBranchTerritoryReciever = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrBranch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrBranchReciever = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareHouseCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareHouseCodeRec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareHouseNameRec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareHouseLocationRec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetirementDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastConfirmationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInventDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rCheck,
            this.rTextEdit});
            this.gridControl.Size = new System.Drawing.Size(900, 500);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 45;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAgreementNumber,
            this.colUniqSWAgreementNumber,
            this.colAgreementDate,
            this.colAgreementEndDate,
            this.colAgreementStatus,
            this.colActNumber,
            this.colActReturnNumber,
            this.colActStatus,
            this.colActReturnStatus,
            this.colActDate,
            this.colInstalationDate,
            this.colActReturningDate,
            this.colSystemReturningDate,
            this.colUniqSystemDocumentNumber,
            this.colUniqSystemReturnDocNum,
            this.colBelongsToRoute,
            this.colPocCode,
            this.colPocJurName,
            this.colActualName,
            this.colPocActualAdress,
            this.colPocChannel,
            this.colPocType,
            this.colSubtype,
            this.colCodeInn,
            this.colPocStatus,
            this.colTerritoryType,
            this.colPopulation,
            this.colRegion,
            this.colDistrict,
            this.colTown,
            this.colCityDistrict,
            this.colМ4,
            this.colМ3,
            this.colM3ResponsibleForTO,
            this.colMol,
            this.colM3Location,
            this.colM2,
            this.colM1,
            this.colStaffingChannel,
            this.colWarehouseChanel,
            this.colDoorsQuantity,
            this.colEquipmentKind,
            this.colEquipmentType,
            this.colFirstInstalationDate,
            this.colEquipmentModel,
            this.colManufacterer,
            this.colConstructionDate,
            this.colIncomeDate,
            this.colRegenerationDate,
            this.colProjectReservationDate,
            this.colProject,
            this.colComments,
            this.colInvenNumber,
            this.colSerialNumber,
            this.colBrand,
            this.colExpluatationAbility,
            this.colTechnicalState,
            this.colLastStatusChangeDate,
            this.colRentalPrice,
            this.colInitPrice,
            this.colTRO_CurrentTerritory,
            this.colTRO_CurrentRegion,
            this.colTRO_DistrSAPCode,
            this.colTRO_CurrentDistributo,
            this.colTRO_WorkSchema,
            this.colTRO_CurrentCustLocation,
            this.colTRO_CurrentCustomer,
            this.colTRO_CurrentWarehouse,
            this.colTRO_CurrentWarehouseLocation,
            this.colTerritory,
            this.colSenderTerritory,
            this.colRegionPopulation,
            this.colSenderRegion,
            this.colDistributor,
            this.colReceiverDistr,
            this.colDistrSAPCode,
            this.colDistrSAPCodeReciever,
            this.colWorkSchema,
            this.colWorkSchemeReciever,
            this.colDistrBranchTerritory,
            this.colDistrBranchTerritoryReciever,
            this.colDistrBranch,
            this.colDistrBranchReciever,
            this.colWareHouseCode,
            this.colWareHouseName,
            this.colWareHouseCodeRec,
            this.colWareHouseNameRec,
            this.colWarehouseLocation,
            this.colWareHouseLocationRec,
            this.colRetirementDate,
            this.colLastConfirmationDate,
            this.colLastInventDate});
            this.gridView.GridControl = this.gridControl;
            this.gridView.GroupFormat = "{0}: {1}";
            this.gridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "UpliftPct", null, "{0:N2}", "2"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftDal", null, "{0:N3}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftMACO", null, "{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "AvgMACO", null, "{0:N3}", "7"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SalesDal_Promo", null, "{0:N3}")});
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsFilter.ColumnFilterPopupRowCount = 30;
            this.gridView.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Дата с  _  - , Дата по  _ ";
            this.gridView.ShowCustomizationForm += new System.EventHandler(this.gridView_ShowCustomizationForm);
            // 
            // colAgreementNumber
            // 
            this.colAgreementNumber.Caption = "Номер договора";
            this.colAgreementNumber.FieldName = "OlContractNo";
            this.colAgreementNumber.Name = "colAgreementNumber";
            this.colAgreementNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAgreementNumber.Visible = true;
            this.colAgreementNumber.VisibleIndex = 0;
            this.colAgreementNumber.Width = 150;
            // 
            // colUniqSWAgreementNumber
            // 
            this.colUniqSWAgreementNumber.Caption = "Уникальный номер договора в SW";
            this.colUniqSWAgreementNumber.FieldName = "OlContractId";
            this.colUniqSWAgreementNumber.Name = "colUniqSWAgreementNumber";
            this.colUniqSWAgreementNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUniqSWAgreementNumber.Visible = true;
            this.colUniqSWAgreementNumber.VisibleIndex = 1;
            this.colUniqSWAgreementNumber.Width = 150;
            // 
            // colAgreementDate
            // 
            this.colAgreementDate.Caption = "Дата заключения договора";
            this.colAgreementDate.FieldName = "StartDate";
            this.colAgreementDate.Name = "colAgreementDate";
            this.colAgreementDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAgreementDate.Visible = true;
            this.colAgreementDate.VisibleIndex = 2;
            this.colAgreementDate.Width = 100;
            // 
            // colAgreementEndDate
            // 
            this.colAgreementEndDate.Caption = "Дата прекращения срока действия договора";
            this.colAgreementEndDate.FieldName = "EndDate";
            this.colAgreementEndDate.Name = "colAgreementEndDate";
            this.colAgreementEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAgreementEndDate.Visible = true;
            this.colAgreementEndDate.VisibleIndex = 3;
            this.colAgreementEndDate.Width = 125;
            // 
            // colAgreementStatus
            // 
            this.colAgreementStatus.Caption = "Статус договора";
            this.colAgreementStatus.FieldName = "ContractStatus";
            this.colAgreementStatus.Name = "colAgreementStatus";
            this.colAgreementStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAgreementStatus.Visible = true;
            this.colAgreementStatus.VisibleIndex = 4;
            this.colAgreementStatus.Width = 100;
            // 
            // colActNumber
            // 
            this.colActNumber.Caption = "№ акта";
            this.colActNumber.FieldName = "ScanWOrderNo";
            this.colActNumber.Name = "colActNumber";
            this.colActNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActNumber.Visible = true;
            this.colActNumber.VisibleIndex = 5;
            this.colActNumber.Width = 100;
            // 
            // colActReturnNumber
            // 
            this.colActReturnNumber.Caption = "№ акта возврата";
            this.colActReturnNumber.FieldName = "Ret_ScanWOrderNo";
            this.colActReturnNumber.Name = "colActReturnNumber";
            this.colActReturnNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActReturnNumber.Visible = true;
            this.colActReturnNumber.VisibleIndex = 6;
            this.colActReturnNumber.Width = 100;
            // 
            // colActStatus
            // 
            this.colActStatus.Caption = "Статус акта";
            this.colActStatus.FieldName = "WOStatus";
            this.colActStatus.Name = "colActStatus";
            this.colActStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActStatus.Visible = true;
            this.colActStatus.VisibleIndex = 7;
            this.colActStatus.Width = 100;
            // 
            // colActReturnStatus
            // 
            this.colActReturnStatus.Caption = "Статус акта возврата";
            this.colActReturnStatus.FieldName = "Ret_WOStatus";
            this.colActReturnStatus.Name = "colActReturnStatus";
            this.colActReturnStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActReturnStatus.Visible = true;
            this.colActReturnStatus.VisibleIndex = 8;
            this.colActReturnStatus.Width = 100;
            // 
            // colActDate
            // 
            this.colActDate.Caption = "Дата акта";
            this.colActDate.FieldName = "FactDateSetup";
            this.colActDate.Name = "colActDate";
            this.colActDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActDate.Visible = true;
            this.colActDate.VisibleIndex = 9;
            this.colActDate.Width = 100;
            // 
            // colInstalationDate
            // 
            this.colInstalationDate.Caption = "Дата установки в системе";
            this.colInstalationDate.FieldName = "SystemDateSetup";
            this.colInstalationDate.Name = "colInstalationDate";
            this.colInstalationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInstalationDate.Visible = true;
            this.colInstalationDate.VisibleIndex = 10;
            this.colInstalationDate.Width = 100;
            // 
            // colActReturningDate
            // 
            this.colActReturningDate.Caption = "Дата акта возврата";
            this.colActReturningDate.FieldName = "FactDateReturn";
            this.colActReturningDate.Name = "colActReturningDate";
            this.colActReturningDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActReturningDate.Visible = true;
            this.colActReturningDate.VisibleIndex = 11;
            this.colActReturningDate.Width = 100;
            // 
            // colSystemReturningDate
            // 
            this.colSystemReturningDate.Caption = "Дата возврата в системе";
            this.colSystemReturningDate.FieldName = "SystemDateReturn";
            this.colSystemReturningDate.Name = "colSystemReturningDate";
            this.colSystemReturningDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSystemReturningDate.Visible = true;
            this.colSystemReturningDate.VisibleIndex = 12;
            this.colSystemReturningDate.Width = 100;
            // 
            // colUniqSystemDocumentNumber
            // 
            this.colUniqSystemDocumentNumber.Caption = "Уникальный номер системного документа";
            this.colUniqSystemDocumentNumber.FieldName = "WOrderNo";
            this.colUniqSystemDocumentNumber.Name = "colUniqSystemDocumentNumber";
            this.colUniqSystemDocumentNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUniqSystemDocumentNumber.Visible = true;
            this.colUniqSystemDocumentNumber.VisibleIndex = 13;
            this.colUniqSystemDocumentNumber.Width = 150;
            // 
            // colUniqSystemReturnDocNum
            // 
            this.colUniqSystemReturnDocNum.Caption = "Уникальный номер системного документа возврата";
            this.colUniqSystemReturnDocNum.FieldName = "Ret_WOrderNo";
            this.colUniqSystemReturnDocNum.Name = "colUniqSystemReturnDocNum";
            this.colUniqSystemReturnDocNum.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUniqSystemReturnDocNum.Visible = true;
            this.colUniqSystemReturnDocNum.VisibleIndex = 14;
            this.colUniqSystemReturnDocNum.Width = 150;
            // 
            // colBelongsToRoute
            // 
            this.colBelongsToRoute.Caption = "Принадлежность к маршрутам";
            this.colBelongsToRoute.ColumnEdit = this.rCheck;
            this.colBelongsToRoute.FieldName = "OnRoute";
            this.colBelongsToRoute.Name = "colBelongsToRoute";
            this.colBelongsToRoute.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBelongsToRoute.Visible = true;
            this.colBelongsToRoute.VisibleIndex = 15;
            this.colBelongsToRoute.Width = 120;
            // 
            // rCheck
            // 
            this.rCheck.AutoHeight = false;
            this.rCheck.DisplayValueChecked = "Да";
            this.rCheck.DisplayValueGrayed = " ";
            this.rCheck.DisplayValueUnchecked = "Нет";
            this.rCheck.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rCheck.Name = "rCheck";
            // 
            // colPocCode
            // 
            this.colPocCode.Caption = "Код ТТ";
            this.colPocCode.ColumnEdit = this.rTextEdit;
            this.colPocCode.FieldName = "OL_ID";
            this.colPocCode.Name = "colPocCode";
            this.colPocCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocCode.Visible = true;
            this.colPocCode.VisibleIndex = 16;
            // 
            // colPocJurName
            // 
            this.colPocJurName.Caption = "Юр. имя ТТ";
            this.colPocJurName.FieldName = "OLName";
            this.colPocJurName.Name = "colPocJurName";
            this.colPocJurName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocJurName.Visible = true;
            this.colPocJurName.VisibleIndex = 17;
            this.colPocJurName.Width = 100;
            // 
            // colActualName
            // 
            this.colActualName.Caption = "Факт. имя ТТ";
            this.colActualName.FieldName = "OLTradingName";
            this.colActualName.Name = "colActualName";
            this.colActualName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActualName.Visible = true;
            this.colActualName.VisibleIndex = 18;
            this.colActualName.Width = 100;
            // 
            // colPocActualAdress
            // 
            this.colPocActualAdress.Caption = "Факт. адрес ТТ";
            this.colPocActualAdress.FieldName = "OLDeliveryAddress";
            this.colPocActualAdress.Name = "colPocActualAdress";
            this.colPocActualAdress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocActualAdress.Visible = true;
            this.colPocActualAdress.VisibleIndex = 19;
            this.colPocActualAdress.Width = 150;
            // 
            // colPocChannel
            // 
            this.colPocChannel.Caption = "Канал ТТ";
            this.colPocChannel.FieldName = "OLGroup_name";
            this.colPocChannel.Name = "colPocChannel";
            this.colPocChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocChannel.Visible = true;
            this.colPocChannel.VisibleIndex = 20;
            // 
            // colPocType
            // 
            this.colPocType.Caption = "Тип ТТ";
            this.colPocType.FieldName = "OLtype_name";
            this.colPocType.Name = "colPocType";
            this.colPocType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocType.Visible = true;
            this.colPocType.VisibleIndex = 21;
            // 
            // colSubtype
            // 
            this.colSubtype.Caption = "Подтип ТТ";
            this.colSubtype.FieldName = "OLSubTypeName";
            this.colSubtype.Name = "colSubtype";
            this.colSubtype.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSubtype.Visible = true;
            this.colSubtype.VisibleIndex = 22;
            // 
            // colCodeInn
            // 
            this.colCodeInn.Caption = "Код ОКПО/ИНН";
            this.colCodeInn.FieldName = "ZKPOIPN";
            this.colCodeInn.Name = "colCodeInn";
            this.colCodeInn.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCodeInn.Visible = true;
            this.colCodeInn.VisibleIndex = 23;
            this.colCodeInn.Width = 100;
            // 
            // colPocStatus
            // 
            this.colPocStatus.Caption = "Статус ТТ";
            this.colPocStatus.FieldName = "OLStatus";
            this.colPocStatus.Name = "colPocStatus";
            this.colPocStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocStatus.Visible = true;
            this.colPocStatus.VisibleIndex = 24;
            // 
            // colTerritoryType
            // 
            this.colTerritoryType.Caption = "Тип территории";
            this.colTerritoryType.FieldName = "OLSettlement";
            this.colTerritoryType.Name = "colTerritoryType";
            this.colTerritoryType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTerritoryType.Visible = true;
            this.colTerritoryType.VisibleIndex = 25;
            this.colTerritoryType.Width = 100;
            // 
            // colPopulation
            // 
            this.colPopulation.Caption = "Население";
            this.colPopulation.FieldName = "OLPopulation";
            this.colPopulation.Name = "colPopulation";
            this.colPopulation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPopulation.Visible = true;
            this.colPopulation.VisibleIndex = 26;
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Область";
            this.colRegion.FieldName = "OLDistrict_name";
            this.colRegion.Name = "colRegion";
            this.colRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 27;
            this.colRegion.Width = 100;
            // 
            // colDistrict
            // 
            this.colDistrict.Caption = "Район";
            this.colDistrict.FieldName = "OLCntArea_name";
            this.colDistrict.Name = "colDistrict";
            this.colDistrict.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrict.Visible = true;
            this.colDistrict.VisibleIndex = 28;
            this.colDistrict.Width = 100;
            // 
            // colTown
            // 
            this.colTown.Caption = "Город";
            this.colTown.FieldName = "OLCity_name";
            this.colTown.Name = "colTown";
            this.colTown.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTown.Visible = true;
            this.colTown.VisibleIndex = 29;
            this.colTown.Width = 100;
            // 
            // colCityDistrict
            // 
            this.colCityDistrict.Caption = "Район в городе";
            this.colCityDistrict.FieldName = "OLCityArea_name";
            this.colCityDistrict.Name = "colCityDistrict";
            this.colCityDistrict.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCityDistrict.Visible = true;
            this.colCityDistrict.VisibleIndex = 30;
            this.colCityDistrict.Width = 100;
            // 
            // colМ4
            // 
            this.colМ4.Caption = "М4";
            this.colМ4.FieldName = "RM_name";
            this.colМ4.Name = "colМ4";
            this.colМ4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colМ4.Visible = true;
            this.colМ4.VisibleIndex = 31;
            this.colМ4.Width = 100;
            // 
            // colМ3
            // 
            this.colМ3.Caption = "М3";
            this.colМ3.FieldName = "DSM_Name";
            this.colМ3.Name = "colМ3";
            this.colМ3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colМ3.Visible = true;
            this.colМ3.VisibleIndex = 32;
            this.colМ3.Width = 100;
            // 
            // colM3ResponsibleForTO
            // 
            this.colM3ResponsibleForTO.Caption = "М3, отв. за ТО";
            this.colM3ResponsibleForTO.FieldName = "ResponsibleM3";
            this.colM3ResponsibleForTO.Name = "colM3ResponsibleForTO";
            this.colM3ResponsibleForTO.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3ResponsibleForTO.Visible = true;
            this.colM3ResponsibleForTO.VisibleIndex = 33;
            this.colM3ResponsibleForTO.Width = 100;
            // 
            // colMol
            // 
            this.colMol.Caption = "МОЛ";
            this.colMol.FieldName = "MOL_ID";
            this.colMol.Name = "colMol";
            this.colMol.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colMol.Visible = true;
            this.colMol.VisibleIndex = 34;
            this.colMol.Width = 100;
            // 
            // colM3Location
            // 
            this.colM3Location.Caption = "Ключевой город";
            this.colM3Location.Name = "colM3Location";
            this.colM3Location.OptionsColumn.ShowInCustomizationForm = false;
            this.colM3Location.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colM2
            // 
            this.colM2.Caption = "M2";
            this.colM2.FieldName = "Supervisor_name";
            this.colM2.Name = "colM2";
            this.colM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2.Visible = true;
            this.colM2.VisibleIndex = 35;
            this.colM2.Width = 100;
            // 
            // colM1
            // 
            this.colM1.Caption = "M1";
            this.colM1.FieldName = "MerchName";
            this.colM1.Name = "colM1";
            this.colM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1.Visible = true;
            this.colM1.VisibleIndex = 36;
            this.colM1.Width = 100;
            // 
            // colStaffingChannel
            // 
            this.colStaffingChannel.Caption = "Канал персонала";
            this.colStaffingChannel.FieldName = "Merch_ChanelType";
            this.colStaffingChannel.Name = "colStaffingChannel";
            this.colStaffingChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colStaffingChannel.Visible = true;
            this.colStaffingChannel.VisibleIndex = 37;
            // 
            // colWarehouseChanel
            // 
            this.colWarehouseChanel.Caption = "Канал Склада/ТТ";
            this.colWarehouseChanel.FieldName = "OLGroup_name2";
            this.colWarehouseChanel.Name = "colWarehouseChanel";
            this.colWarehouseChanel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWarehouseChanel.Visible = true;
            this.colWarehouseChanel.VisibleIndex = 38;
            // 
            // colDoorsQuantity
            // 
            this.colDoorsQuantity.Caption = "Количество дверей";
            this.colDoorsQuantity.FieldName = "DoorsQty";
            this.colDoorsQuantity.Name = "colDoorsQuantity";
            this.colDoorsQuantity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDoorsQuantity.Visible = true;
            this.colDoorsQuantity.VisibleIndex = 39;
            this.colDoorsQuantity.Width = 100;
            // 
            // colEquipmentKind
            // 
            this.colEquipmentKind.Caption = "Вид оборудования ";
            this.colEquipmentKind.FieldName = "POSGroup_Name";
            this.colEquipmentKind.Name = "colEquipmentKind";
            this.colEquipmentKind.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEquipmentKind.Visible = true;
            this.colEquipmentKind.VisibleIndex = 40;
            this.colEquipmentKind.Width = 100;
            // 
            // colEquipmentType
            // 
            this.colEquipmentType.Caption = "Тип оборудования";
            this.colEquipmentType.FieldName = "POSType";
            this.colEquipmentType.Name = "colEquipmentType";
            this.colEquipmentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEquipmentType.Visible = true;
            this.colEquipmentType.VisibleIndex = 41;
            this.colEquipmentType.Width = 100;
            // 
            // colFirstInstalationDate
            // 
            this.colFirstInstalationDate.Caption = "Дата первой установки";
            this.colFirstInstalationDate.FieldName = "FirstInstallDate";
            this.colFirstInstalationDate.Name = "colFirstInstalationDate";
            this.colFirstInstalationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colFirstInstalationDate.Visible = true;
            this.colFirstInstalationDate.VisibleIndex = 42;
            this.colFirstInstalationDate.Width = 100;
            // 
            // colEquipmentModel
            // 
            this.colEquipmentModel.Caption = "Модель оборудования";
            this.colEquipmentModel.FieldName = "POSType_Name";
            this.colEquipmentModel.Name = "colEquipmentModel";
            this.colEquipmentModel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEquipmentModel.Visible = true;
            this.colEquipmentModel.VisibleIndex = 43;
            this.colEquipmentModel.Width = 100;
            // 
            // colManufacterer
            // 
            this.colManufacterer.Caption = "Производитель";
            this.colManufacterer.FieldName = "Manufacturer";
            this.colManufacterer.Name = "colManufacterer";
            this.colManufacterer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colManufacterer.Visible = true;
            this.colManufacterer.VisibleIndex = 44;
            this.colManufacterer.Width = 100;
            // 
            // colConstructionDate
            // 
            this.colConstructionDate.Caption = "Дата выпуска";
            this.colConstructionDate.FieldName = "YearProduction";
            this.colConstructionDate.Name = "colConstructionDate";
            this.colConstructionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colConstructionDate.Visible = true;
            this.colConstructionDate.VisibleIndex = 45;
            this.colConstructionDate.Width = 100;
            // 
            // colIncomeDate
            // 
            this.colIncomeDate.Caption = "Дата прихода";
            this.colIncomeDate.FieldName = "ArrivalDate";
            this.colIncomeDate.Name = "colIncomeDate";
            this.colIncomeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colIncomeDate.Visible = true;
            this.colIncomeDate.VisibleIndex = 46;
            this.colIncomeDate.Width = 100;
            // 
            // colRegenerationDate
            // 
            this.colRegenerationDate.Caption = "Дата восстановления";
            this.colRegenerationDate.FieldName = "RecoveryDate";
            this.colRegenerationDate.Name = "colRegenerationDate";
            this.colRegenerationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegenerationDate.Visible = true;
            this.colRegenerationDate.VisibleIndex = 47;
            this.colRegenerationDate.Width = 125;
            // 
            // colProjectReservationDate
            // 
            this.colProjectReservationDate.Caption = "Дата резервирования под проект";
            this.colProjectReservationDate.FieldName = "ReservationDate";
            this.colProjectReservationDate.Name = "colProjectReservationDate";
            this.colProjectReservationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colProjectReservationDate.Visible = true;
            this.colProjectReservationDate.VisibleIndex = 48;
            this.colProjectReservationDate.Width = 125;
            // 
            // colProject
            // 
            this.colProject.Caption = "Проект";
            this.colProject.FieldName = "Project";
            this.colProject.Name = "colProject";
            this.colProject.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colProject.Visible = true;
            this.colProject.VisibleIndex = 49;
            // 
            // colComments
            // 
            this.colComments.Caption = "Комментарии";
            this.colComments.FieldName = "Comment";
            this.colComments.Name = "colComments";
            this.colComments.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 50;
            this.colComments.Width = 100;
            // 
            // colInvenNumber
            // 
            this.colInvenNumber.Caption = "Инвентарный номер";
            this.colInvenNumber.FieldName = "Invent_No";
            this.colInvenNumber.Name = "colInvenNumber";
            this.colInvenNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInvenNumber.Visible = true;
            this.colInvenNumber.VisibleIndex = 51;
            this.colInvenNumber.Width = 100;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.Caption = "Серийный номер";
            this.colSerialNumber.FieldName = "Serial_No";
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 52;
            this.colSerialNumber.Width = 100;
            // 
            // colBrand
            // 
            this.colBrand.Caption = "Бренд";
            this.colBrand.FieldName = "POSBrand_Name";
            this.colBrand.Name = "colBrand";
            this.colBrand.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBrand.Visible = true;
            this.colBrand.VisibleIndex = 53;
            // 
            // colExpluatationAbility
            // 
            this.colExpluatationAbility.Caption = "Возможность эксплуатации";
            this.colExpluatationAbility.FieldName = "CanUse";
            this.colExpluatationAbility.Name = "colExpluatationAbility";
            this.colExpluatationAbility.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colExpluatationAbility.Visible = true;
            this.colExpluatationAbility.VisibleIndex = 54;
            this.colExpluatationAbility.Width = 100;
            // 
            // colTechnicalState
            // 
            this.colTechnicalState.Caption = "Техническое состояние";
            this.colTechnicalState.FieldName = "TechnicalCondition";
            this.colTechnicalState.Name = "colTechnicalState";
            this.colTechnicalState.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTechnicalState.Visible = true;
            this.colTechnicalState.VisibleIndex = 55;
            this.colTechnicalState.Width = 100;
            // 
            // colLastStatusChangeDate
            // 
            this.colLastStatusChangeDate.Caption = "Дата последнего изменения статуса";
            this.colLastStatusChangeDate.FieldName = "LastDateOfTechStatus";
            this.colLastStatusChangeDate.Name = "colLastStatusChangeDate";
            this.colLastStatusChangeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLastStatusChangeDate.Visible = true;
            this.colLastStatusChangeDate.VisibleIndex = 56;
            this.colLastStatusChangeDate.Width = 125;
            // 
            // colRentalPrice
            // 
            this.colRentalPrice.Caption = "Стоимость залоговая";
            this.colRentalPrice.DisplayFormat.FormatString = "N2";
            this.colRentalPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRentalPrice.FieldName = "CollateralValue";
            this.colRentalPrice.Name = "colRentalPrice";
            this.colRentalPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRentalPrice.Visible = true;
            this.colRentalPrice.VisibleIndex = 57;
            this.colRentalPrice.Width = 125;
            // 
            // colInitPrice
            // 
            this.colInitPrice.Caption = "Стоимость первоначальная";
            this.colInitPrice.DisplayFormat.FormatString = "N2";
            this.colInitPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colInitPrice.FieldName = "InitialCost";
            this.colInitPrice.Name = "colInitPrice";
            this.colInitPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInitPrice.Visible = true;
            this.colInitPrice.VisibleIndex = 58;
            this.colInitPrice.Width = 125;
            // 
            // colTRO_CurrentTerritory
            // 
            this.colTRO_CurrentTerritory.Caption = "Территория текущей локации ТРО";
            this.colTRO_CurrentTerritory.FieldName = "TRO_CurrentTerritory";
            this.colTRO_CurrentTerritory.Name = "colTRO_CurrentTerritory";
            this.colTRO_CurrentTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_CurrentTerritory.Visible = true;
            this.colTRO_CurrentTerritory.VisibleIndex = 59;
            this.colTRO_CurrentTerritory.Width = 100;
            // 
            // colTRO_CurrentRegion
            // 
            this.colTRO_CurrentRegion.Caption = "Регион текущей локации ТРО";
            this.colTRO_CurrentRegion.FieldName = "TRO_CurrentRegion";
            this.colTRO_CurrentRegion.Name = "colTRO_CurrentRegion";
            this.colTRO_CurrentRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_CurrentRegion.Visible = true;
            this.colTRO_CurrentRegion.VisibleIndex = 60;
            this.colTRO_CurrentRegion.Width = 100;
            // 
            // colTRO_DistrSAPCode
            // 
            this.colTRO_DistrSAPCode.Caption = "Код дистрибьютора текущей локации ТРО";
            this.colTRO_DistrSAPCode.FieldName = "TRO_DistrSAPCode";
            this.colTRO_DistrSAPCode.Name = "colTRO_DistrSAPCode";
            this.colTRO_DistrSAPCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_DistrSAPCode.Visible = true;
            this.colTRO_DistrSAPCode.VisibleIndex = 61;
            this.colTRO_DistrSAPCode.Width = 125;
            // 
            // colTRO_CurrentDistributo
            // 
            this.colTRO_CurrentDistributo.Caption = "Дистрибьютор текущей локации ТРО";
            this.colTRO_CurrentDistributo.FieldName = "TRO_CurrentDistributor";
            this.colTRO_CurrentDistributo.Name = "colTRO_CurrentDistributo";
            this.colTRO_CurrentDistributo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_CurrentDistributo.Visible = true;
            this.colTRO_CurrentDistributo.VisibleIndex = 62;
            this.colTRO_CurrentDistributo.Width = 100;
            // 
            // colTRO_WorkSchema
            // 
            this.colTRO_WorkSchema.Caption = "Схема работы текущей локации ТРО";
            this.colTRO_WorkSchema.FieldName = "TRO_WorkSchema";
            this.colTRO_WorkSchema.Name = "colTRO_WorkSchema";
            this.colTRO_WorkSchema.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_WorkSchema.Visible = true;
            this.colTRO_WorkSchema.VisibleIndex = 63;
            this.colTRO_WorkSchema.Width = 125;
            // 
            // colTRO_CurrentCustLocation
            // 
            this.colTRO_CurrentCustLocation.Caption = "Территория ТС текущей локации ТРО";
            this.colTRO_CurrentCustLocation.FieldName = "TRO_CurrentCustLocation";
            this.colTRO_CurrentCustLocation.Name = "colTRO_CurrentCustLocation";
            this.colTRO_CurrentCustLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_CurrentCustLocation.Visible = true;
            this.colTRO_CurrentCustLocation.VisibleIndex = 64;
            this.colTRO_CurrentCustLocation.Width = 100;
            // 
            // colTRO_CurrentCustomer
            // 
            this.colTRO_CurrentCustomer.Caption = "ТС текущей локации ТРО";
            this.colTRO_CurrentCustomer.FieldName = "TRO_CurrentCustomer";
            this.colTRO_CurrentCustomer.Name = "colTRO_CurrentCustomer";
            this.colTRO_CurrentCustomer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_CurrentCustomer.Visible = true;
            this.colTRO_CurrentCustomer.VisibleIndex = 65;
            this.colTRO_CurrentCustomer.Width = 100;
            // 
            // colTRO_CurrentWarehouse
            // 
            this.colTRO_CurrentWarehouse.Caption = "Склад текущей локации ТРО";
            this.colTRO_CurrentWarehouse.FieldName = "TRO_CurrentWarehouse";
            this.colTRO_CurrentWarehouse.Name = "colTRO_CurrentWarehouse";
            this.colTRO_CurrentWarehouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_CurrentWarehouse.Visible = true;
            this.colTRO_CurrentWarehouse.VisibleIndex = 66;
            this.colTRO_CurrentWarehouse.Width = 100;
            // 
            // colTRO_CurrentWarehouseLocation
            // 
            this.colTRO_CurrentWarehouseLocation.Caption = "Место локации склада текущей локации ТРО";
            this.colTRO_CurrentWarehouseLocation.FieldName = "TRO_CurrentWarehouseLocation";
            this.colTRO_CurrentWarehouseLocation.Name = "colTRO_CurrentWarehouseLocation";
            this.colTRO_CurrentWarehouseLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTRO_CurrentWarehouseLocation.Visible = true;
            this.colTRO_CurrentWarehouseLocation.VisibleIndex = 67;
            this.colTRO_CurrentWarehouseLocation.Width = 125;
            // 
            // colTerritory
            // 
            this.colTerritory.Caption = "Территория отправителя";
            this.colTerritory.FieldName = "OlTerritory_Name";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTerritory.Visible = true;
            this.colTerritory.VisibleIndex = 68;
            this.colTerritory.Width = 100;
            // 
            // colSenderTerritory
            // 
            this.colSenderTerritory.Caption = "Территория получателя";
            this.colSenderTerritory.FieldName = "Territory_Name_To";
            this.colSenderTerritory.Name = "colSenderTerritory";
            this.colSenderTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSenderTerritory.Visible = true;
            this.colSenderTerritory.VisibleIndex = 69;
            this.colSenderTerritory.Width = 100;
            // 
            // colRegionPopulation
            // 
            this.colRegionPopulation.Caption = "Регион отправителя";
            this.colRegionPopulation.FieldName = "OLRegion_name";
            this.colRegionPopulation.Name = "colRegionPopulation";
            this.colRegionPopulation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegionPopulation.Visible = true;
            this.colRegionPopulation.VisibleIndex = 70;
            this.colRegionPopulation.Width = 100;
            // 
            // colSenderRegion
            // 
            this.colSenderRegion.Caption = "Регион получателя";
            this.colSenderRegion.FieldName = "OLRegion_name_To";
            this.colSenderRegion.Name = "colSenderRegion";
            this.colSenderRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSenderRegion.Visible = true;
            this.colSenderRegion.VisibleIndex = 71;
            this.colSenderRegion.Width = 100;
            // 
            // colDistributor
            // 
            this.colDistributor.Caption = "Дистрибьютор отправитель";
            this.colDistributor.FieldName = "Distr_name";
            this.colDistributor.Name = "colDistributor";
            this.colDistributor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistributor.Visible = true;
            this.colDistributor.VisibleIndex = 72;
            this.colDistributor.Width = 100;
            // 
            // colReceiverDistr
            // 
            this.colReceiverDistr.Caption = "Дистрибьютор получатель";
            this.colReceiverDistr.FieldName = "Distr_name_To";
            this.colReceiverDistr.Name = "colReceiverDistr";
            this.colReceiverDistr.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colReceiverDistr.Visible = true;
            this.colReceiverDistr.VisibleIndex = 73;
            this.colReceiverDistr.Width = 100;
            // 
            // colDistrSAPCode
            // 
            this.colDistrSAPCode.Caption = "Код дистрибьютора отправителя SAP";
            this.colDistrSAPCode.FieldName = "Distr_SAP";
            this.colDistrSAPCode.Name = "colDistrSAPCode";
            this.colDistrSAPCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrSAPCode.Visible = true;
            this.colDistrSAPCode.VisibleIndex = 74;
            this.colDistrSAPCode.Width = 125;
            // 
            // colDistrSAPCodeReciever
            // 
            this.colDistrSAPCodeReciever.Caption = "Код дистрибьютора получателя SAP";
            this.colDistrSAPCodeReciever.FieldName = "Distr_SAP_To";
            this.colDistrSAPCodeReciever.Name = "colDistrSAPCodeReciever";
            this.colDistrSAPCodeReciever.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrSAPCodeReciever.Visible = true;
            this.colDistrSAPCodeReciever.VisibleIndex = 75;
            this.colDistrSAPCodeReciever.Width = 125;
            // 
            // colWorkSchema
            // 
            this.colWorkSchema.Caption = "Схема работы отправителя";
            this.colWorkSchema.FieldName = "WorkSchema";
            this.colWorkSchema.Name = "colWorkSchema";
            this.colWorkSchema.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWorkSchema.Visible = true;
            this.colWorkSchema.VisibleIndex = 76;
            this.colWorkSchema.Width = 90;
            // 
            // colWorkSchemeReciever
            // 
            this.colWorkSchemeReciever.Caption = "Схема работы получателя";
            this.colWorkSchemeReciever.FieldName = "WorkSchema_To";
            this.colWorkSchemeReciever.Name = "colWorkSchemeReciever";
            this.colWorkSchemeReciever.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWorkSchemeReciever.Visible = true;
            this.colWorkSchemeReciever.VisibleIndex = 77;
            this.colWorkSchemeReciever.Width = 90;
            // 
            // colDistrBranchTerritory
            // 
            this.colDistrBranchTerritory.Caption = "Территория филиала дистрибьютора отправителя";
            this.colDistrBranchTerritory.FieldName = "BranchTerritory";
            this.colDistrBranchTerritory.Name = "colDistrBranchTerritory";
            this.colDistrBranchTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrBranchTerritory.Visible = true;
            this.colDistrBranchTerritory.VisibleIndex = 78;
            this.colDistrBranchTerritory.Width = 130;
            // 
            // colDistrBranchTerritoryReciever
            // 
            this.colDistrBranchTerritoryReciever.Caption = "Территория филиала дистрибьютора получателя";
            this.colDistrBranchTerritoryReciever.FieldName = "BranchTerritory_To";
            this.colDistrBranchTerritoryReciever.Name = "colDistrBranchTerritoryReciever";
            this.colDistrBranchTerritoryReciever.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrBranchTerritoryReciever.Visible = true;
            this.colDistrBranchTerritoryReciever.VisibleIndex = 79;
            this.colDistrBranchTerritoryReciever.Width = 130;
            // 
            // colDistrBranch
            // 
            this.colDistrBranch.Caption = "Филиал дистрибьютора отправителя";
            this.colDistrBranch.FieldName = "Cust_name";
            this.colDistrBranch.Name = "colDistrBranch";
            this.colDistrBranch.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrBranch.Visible = true;
            this.colDistrBranch.VisibleIndex = 80;
            this.colDistrBranch.Width = 125;
            // 
            // colDistrBranchReciever
            // 
            this.colDistrBranchReciever.Caption = "Филиал дистрибьютора получателя";
            this.colDistrBranchReciever.FieldName = "Cust_Name_To";
            this.colDistrBranchReciever.Name = "colDistrBranchReciever";
            this.colDistrBranchReciever.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrBranchReciever.Visible = true;
            this.colDistrBranchReciever.VisibleIndex = 81;
            this.colDistrBranchReciever.Width = 125;
            // 
            // colWareHouseCode
            // 
            this.colWareHouseCode.Caption = "Код cклада отправителя";
            this.colWareHouseCode.FieldName = "WH_CodeFrom";
            this.colWareHouseCode.Name = "colWareHouseCode";
            this.colWareHouseCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWareHouseCode.Visible = true;
            this.colWareHouseCode.VisibleIndex = 82;
            this.colWareHouseCode.Width = 100;
            // 
            // colWareHouseName
            // 
            this.colWareHouseName.Caption = "Название склада отправителя";
            this.colWareHouseName.FieldName = "WarehouseName";
            this.colWareHouseName.Name = "colWareHouseName";
            this.colWareHouseName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWareHouseName.Visible = true;
            this.colWareHouseName.VisibleIndex = 83;
            this.colWareHouseName.Width = 100;
            // 
            // colWareHouseCodeRec
            // 
            this.colWareHouseCodeRec.Caption = "Код cклада получателя";
            this.colWareHouseCodeRec.FieldName = "WH_CodeTo";
            this.colWareHouseCodeRec.Name = "colWareHouseCodeRec";
            this.colWareHouseCodeRec.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWareHouseCodeRec.Visible = true;
            this.colWareHouseCodeRec.VisibleIndex = 84;
            this.colWareHouseCodeRec.Width = 100;
            // 
            // colWareHouseNameRec
            // 
            this.colWareHouseNameRec.Caption = "Название склада получателя";
            this.colWareHouseNameRec.FieldName = "WarehouseNameTo";
            this.colWareHouseNameRec.Name = "colWareHouseNameRec";
            this.colWareHouseNameRec.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWareHouseNameRec.Visible = true;
            this.colWareHouseNameRec.VisibleIndex = 85;
            this.colWareHouseNameRec.Width = 100;
            // 
            // colWarehouseLocation
            // 
            this.colWarehouseLocation.Caption = "Место локации склада отправителя";
            this.colWarehouseLocation.FieldName = "WarehouseTerritory";
            this.colWarehouseLocation.Name = "colWarehouseLocation";
            this.colWarehouseLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWarehouseLocation.Visible = true;
            this.colWarehouseLocation.VisibleIndex = 86;
            this.colWarehouseLocation.Width = 100;
            // 
            // colWareHouseLocationRec
            // 
            this.colWareHouseLocationRec.Caption = "Место локации склада получателя";
            this.colWareHouseLocationRec.FieldName = "WarehouseTerritoryTo";
            this.colWareHouseLocationRec.Name = "colWareHouseLocationRec";
            this.colWareHouseLocationRec.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWareHouseLocationRec.Visible = true;
            this.colWareHouseLocationRec.VisibleIndex = 87;
            this.colWareHouseLocationRec.Width = 100;
            // 
            // colRetirementDate
            // 
            this.colRetirementDate.Caption = "Дата списания";
            this.colRetirementDate.FieldName = "DisposalDate";
            this.colRetirementDate.Name = "colRetirementDate";
            this.colRetirementDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRetirementDate.Visible = true;
            this.colRetirementDate.VisibleIndex = 88;
            this.colRetirementDate.Width = 100;
            // 
            // colLastConfirmationDate
            // 
            this.colLastConfirmationDate.Caption = "Последняя дата подтверждения";
            this.colLastConfirmationDate.FieldName = "LastDateOfConfirmation";
            this.colLastConfirmationDate.Name = "colLastConfirmationDate";
            this.colLastConfirmationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLastConfirmationDate.Visible = true;
            this.colLastConfirmationDate.VisibleIndex = 89;
            this.colLastConfirmationDate.Width = 125;
            // 
            // colLastInventDate
            // 
            this.colLastInventDate.Caption = "Последняя дата инвентаризации";
            this.colLastInventDate.FieldName = "LastDateOfInventory";
            this.colLastInventDate.Name = "colLastInventDate";
            this.colLastInventDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLastInventDate.Visible = true;
            this.colLastInventDate.VisibleIndex = 90;
            this.colLastInventDate.Width = 125;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(900, 500);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gridControl;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreementNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqSWAgreementNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreementDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreementEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreementStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInstalationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActReturningDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSystemReturningDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqSystemDocumentNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPocCode;
        private DevExpress.XtraGrid.Columns.GridColumn colPocJurName;
        private DevExpress.XtraGrid.Columns.GridColumn colActualName;
        private DevExpress.XtraGrid.Columns.GridColumn colPocActualAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colPocChannel;
        private DevExpress.XtraGrid.Columns.GridColumn colPocType;
        private DevExpress.XtraGrid.Columns.GridColumn colSubtype;
        private DevExpress.XtraGrid.Columns.GridColumn colCodeInn;
        private DevExpress.XtraGrid.Columns.GridColumn colPocStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colTerritoryType;
        private DevExpress.XtraGrid.Columns.GridColumn colPopulation;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict;
        private DevExpress.XtraGrid.Columns.GridColumn colTown;
        private DevExpress.XtraGrid.Columns.GridColumn colCityDistrict;
        private DevExpress.XtraGrid.Columns.GridColumn colМ4;
        private DevExpress.XtraGrid.Columns.GridColumn colМ3;
        private DevExpress.XtraGrid.Columns.GridColumn colM3Location;
        private DevExpress.XtraGrid.Columns.GridColumn colM2;
        private DevExpress.XtraGrid.Columns.GridColumn colM1;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffingChannel;
        private DevExpress.XtraGrid.Columns.GridColumn colDoorsQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentKind;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentType;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstInstalationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentModel;
        private DevExpress.XtraGrid.Columns.GridColumn colManufacterer;
        private DevExpress.XtraGrid.Columns.GridColumn colConstructionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncomeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRegenerationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProjectReservationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProject;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraGrid.Columns.GridColumn colInvenNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colBrand;
        private DevExpress.XtraGrid.Columns.GridColumn colExpluatationAbility;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colInitPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionPopulation;
        private DevExpress.XtraGrid.Columns.GridColumn colDistributor;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrSAPCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkSchema;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrBranchTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrBranch;
        private DevExpress.XtraGrid.Columns.GridColumn colWareHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colRetirementDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastConfirmationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInventDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTechnicalState;
        private DevExpress.XtraGrid.Columns.GridColumn colActReturnNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActReturnStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqSystemReturnDocNum;
        private DevExpress.XtraGrid.Columns.GridColumn colBelongsToRoute;
        private DevExpress.XtraGrid.Columns.GridColumn colMol;
        private DevExpress.XtraGrid.Columns.GridColumn colLastStatusChangeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSenderTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colSenderRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiverDistr;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrSAPCodeReciever;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkSchemeReciever;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrBranchTerritoryReciever;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrBranchReciever;
        private DevExpress.XtraGrid.Columns.GridColumn colWareHouseNameRec;
        private DevExpress.XtraGrid.Columns.GridColumn colWareHouseLocationRec;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rCheck;
        private DevExpress.XtraGrid.Columns.GridColumn colM3ResponsibleForTO;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseChanel;
        private DevExpress.XtraGrid.Columns.GridColumn colWareHouseCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWareHouseCodeRec;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_CurrentTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_CurrentRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_DistrSAPCode;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_CurrentDistributo;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_WorkSchema;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_CurrentCustLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_CurrentCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_CurrentWarehouse;
        private DevExpress.XtraGrid.Columns.GridColumn colTRO_CurrentWarehouseLocation;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextEdit;

    }
}
