﻿using System.Windows.Forms;
using DevExpress.XtraBars;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraTab;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    //public partial class WccpUIControl : Form
    {
        private SettingsForm settingsForm;

        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            settingsForm = new SettingsForm(Report);
            
            SettingsFormClick += WccpUIControl_SettingsFormClick;
        }

        public int ReportInit()
        {
            if ((settingsForm.FormDisabled && settingsForm.ValidateData()) || settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
                return 0;
            }

            return 1;
        }
        
        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
            }
        }

        private void UpdateAllSheets()
        {
            UpdateSheets(settingsForm.ListSheetParams);
            RefreshControls();
        }

        void RefreshControls()
        {
            btnPrintAll.Visibility = BarItemVisibility.Never;
            btnExportAllTo.Visibility = BarItemVisibility.Never;
            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    (tab as BaseTab).AllowMultiSelect = true;
                    (tab as BaseTab).EnableAppearenceFocusedCell = false;
                }

                if (tab.Controls.Count == 2)
                {
                    Control grid = tab.Controls[0];
                    Control label = tab.Controls[1];

                    grid.Dock = DockStyle.Fill;
                    label.Dock = DockStyle.Fill;

                    tab.Controls.Remove(label);
                    tab.Controls.Remove(grid);

                    SplitContainer sc = new SplitContainer();
                    sc.Dock = DockStyle.Fill;
                    sc.Orientation = Orientation.Horizontal;
                    sc.Panel1MinSize = 19;
                    sc.SplitterDistance = 19;
                    sc.SizeChanged += (x, y) => { sc.SplitterDistance = 19; };
                    sc.IsSplitterFixed = true;
                    sc.SplitterWidth = 1;

                    sc.Panel1.Controls.Add(label);
                    sc.Panel2.Controls.Add(grid);

                    tab.Controls.Add(sc);
                }
            }
        }
    }
}
