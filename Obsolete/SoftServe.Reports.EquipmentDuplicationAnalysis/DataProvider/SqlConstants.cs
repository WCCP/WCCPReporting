﻿namespace WccpReporting.DataProvider
{
    public class SqlConstants
    {
        // SP names
        public const string ProcGetRegion = "spDW_GetUserAccesibleRegions";
        public const string ProcReport = "";

        // SP Params
        public const string ParamRegions = "@sRegions";

        // Field names
        public const string FldRegionName = "NAME";
        public const string FldRegionId = "ID";
    }
}
