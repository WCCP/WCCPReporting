﻿using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;

namespace WccpReporting.DataProvider
{
    class DataAccessProvider
    {
        /// <summary>
        /// Gets regions
        /// </summary>
        /// <returns>Table with regions</returns>
        internal static DataTable GetRegions()
        {
            DataTable res = null;
            DataAccessLayer.OpenConnection();
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.ProcGetRegion, new SqlParameter[] { });

            if (null != ds && ds.Tables.Count > 0)
            {
                res = ds.Tables[0];
            }
            DataAccessLayer.CloseConnection();

            return res;
        }
    }
}
