﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpEquipmentDuplicationAnalysis.dll")]
    public class WccpUI : IStartupClass
    {
        #region Implementation of IStartupClass

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                _reportControl = new WccpUIControl(reportId);
                _reportCaption = Resources.ReportName;

                return _reportControl.ReportInit();
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        public void CloseUI() { }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}
