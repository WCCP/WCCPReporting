﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraEditors.Controls;
using Softserve.Reports.InitiativeMonitoring;
using Softserv.Reports.InitiativeMonitoring.Contracts;
using System.Linq;
using System.Text.RegularExpressions;

namespace Softserv.Reports.InitiativeMonitoring.UserControls
{
    [ToolboxItem(false)]
    public partial class PivotControl : UserControl
    {
        private const string GRAND_TOTAL = @"Grand Total";
        private IDictionary<string, string> fieldName2Alias = new Dictionary<string, string>();
        public DataTable SourceData
        {
            set
            {
                pivotGridControl1.DataSource = value;
                //pivotGridControl1.BestFit();
            }
        }

        internal PivotGridControl Component
        {
            get { return pivotGridControl1; }
        }

        public PivotControl()
        {
            InitializeComponent();
            pivotGridControl1.Appearance.HeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            pivotGridControl1.Appearance.HeaderArea.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            pivotGridControl1.Appearance.HeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            
            ///*
            //pivotGridControl1.OptionsView.ShowColumnHeaders = false
            //pivotGridControl1.OptionsView.ShowGrandTotalHeaderIfNoColumnFields = false;
            pivotGridControl1.OptionsView.ShowGrandTotalHeaderIfNoRowFields = false;
            pivotGridControl1.OptionsView.ShowCustomTotalsForSingleValues = true;
            pivotGridControl1.OptionsView.ShowGrandTotalsForSingleValues = true;
            pivotGridControl1.OptionsView.ShowTotalsForSingleValues = true;
            //*/
            pivotGridControl1.OptionsBehavior.HorizontalScrolling = PivotGridScrolling.Control;

            pivotGridControl1.CustomCellDisplayText += new PivotCellDisplayTextEventHandler(pivotGridControl1_CustomCellDisplayText);

            pivotGridControl1.SizeChanged += new EventHandler(pivotGridControl1_SizeChanged);
        }

        void pivotGridControl1_SizeChanged(object sender, EventArgs e)
        {
            xtraScrollableControl1.AutoScrollMinSize = pivotGridControl1.Size;
        }

        void pivotGridControl1_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            if (!(string.IsNullOrEmpty(e.DataField.UnboundExpression)))
            {
                try
                {
                    string format = "0.00";
                    if (e.DataField.CellFormat.FormatString.Contains("%"))
                        format = "0.00%";
                    else if (e.DataField.CellFormat.FormatString.Contains("{0:N0}"))
                        format = "0";
                    FormulaInterpretator fi = new FormulaInterpretator(e.DataField.UnboundExpression, pivotGridControl1, e, fieldName2Alias);
                    decimal? d = fi.CalculateValue();
                    e.DisplayText = (d.HasValue) ? d.Value.ToString(format) : "";
                }
                catch
                {
                }
            }
        }

        private void pivotGridControl1_CustomExportCell(object sender, CustomExportCellEventArgs e)
        {
            if (!(string.IsNullOrEmpty(e.DataField.UnboundExpression)))
            {
                ((DevExpress.XtraPrinting.ITextBrick)e.Brick).TextValue = e.Text;
            }
        }

        internal void SetupFieldsInfo(IDictionary<ColumnPlaceType, ListBoxItemCollection> fieldsInfo)
        {
            ListBoxItemCollection items = fieldsInfo[ColumnPlaceType.filter];
            SetupReportFieldsInfo(items, PivotArea.FilterArea);
            
            items = fieldsInfo[ColumnPlaceType.row];
            SetupReportFieldsInfo(items, PivotArea.RowArea);

            items = fieldsInfo[ColumnPlaceType.column];
            SetupReportFieldsInfo(items, PivotArea.DataArea);

            items = fieldsInfo[ColumnPlaceType.group];
            SetupReportFieldsInfo(items, PivotArea.ColumnArea);
        }

        private void SetupReportFieldsInfo(DevExpress.XtraEditors.Controls.ListBoxItemCollection fields, PivotArea pivotArea)
        {
            if (null == fields)
                return;

            for (int i = 0; i < fields.Count; i++)
            {
                FieldItem fi = fields[i] as FieldItem;
                if (null != fi)
                {
                    GridBuilder.AddField(pivotGridControl1, fi, pivotArea, i);
                    if (fieldName2Alias.ContainsKey(fi.ColumnName))
                    {
                        // duplicate column ????
                        string ss = string.Empty;
                    }
                    else
                        fieldName2Alias.Add(fi.ColumnName, fi.ColumnAlias);
                }
            }
        }
        public class FormulaInterpretator
        {
            public static string PLUS = "+";
            public static string MINUS = "-";
            public static string MULTIPLICATION = "*";
            public static string DIVISION = "/";

            private static string IF_OPERATOR = "IF";
            private static string DISTINCT_OPERATOR = "DISTINCT";

            private static string EXPRESSION_PATTERN = @"([\[a-z;а-ь;.;і;ї;';,; ;%;\;0-9\]]*)([*;/;-;+])([\[a-z;а-ь;.;і;ї;';,; ;%;\;0-9\]]*)";

            private string Formula { get; set; }
            private PivotGridControl PivotGridControl { get; set; }
            private PivotCellDisplayTextEventArgs Event { get; set; }
            public bool IsCalculated { get; set; }
            IDictionary<string, string> fieldName2Alias = null;
            public FormulaInterpretator(string formula, PivotGridControl pivotGridControl, PivotCellDisplayTextEventArgs e, IDictionary<string, string> fieldName2AliasMapping)
            {
                IsCalculated = false;
                Formula = formula;
                PivotGridControl = pivotGridControl;
                Event = e;
                fieldName2Alias = fieldName2AliasMapping;
            }
            public decimal? CalculateValue()
            {
                decimal? res = null;

                List<Operant> lstOperants = new List<Operant>();
                Check_IfOperator();
                ProcessCustomOperators();
                CheckBrackets();
                ProcessPlusMinusBlocks(true, lstOperants);
                ProcessPlusMinusBlocks(false, lstOperants);

                GetOperants(ref lstOperants, Formula);

                res = Calculate(lstOperants);

                return res;
            }

            private decimal? Calculate(List<Operant> lstOperants)
            {
                decimal? res = null;

                if (lstOperants.Count > 0)
                {
                    res = lstOperants[0].GetDataValue(PivotGridControl, Event, fieldName2Alias);
                    for (int i = 0; i < lstOperants.Count - 2; i++)
                    {
                        res = Calc(res, lstOperants[i + 2].GetDataValue(PivotGridControl, Event, fieldName2Alias), lstOperants[i + 1].Value);
                    }
                }

                return res;
            }

            private void GetOperants(ref List<Operant> lstOperants, string expression)
            {
                if (null == lstOperants)
                    lstOperants = new List<Operant>();
                try
                {
                    string[] fPriorityChunc = Regex.Split(expression, EXPRESSION_PATTERN, RegexOptions.IgnoreCase); //expression.Split(delims, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < fPriorityChunc.Length; i++)
                    {
                        string chunc = fPriorityChunc[i];
                        chunc = chunc.TrimStart(new char[] { ' ', '[' });// Replace(" ", "");
                        chunc = chunc.TrimEnd(new char[] { ' ', ']' });
                        //chunc = chunc.Replace("[", "");
                        //chunc = chunc.Replace("]", "");
                        if (!String.IsNullOrEmpty(chunc))
                            lstOperants.Add(new Operant(chunc));
                    }
                }
                catch { }
            }

            private void Check_IfOperator()
            {
                if (Formula.StartsWith(IF_OPERATOR))
                {
                    int startIdx = Formula.IndexOf("(");
                    int endIdx = Formula.IndexOf(")");
                    if (-1 < startIdx && -1 < endIdx)
                    {
                        string fieldName = Formula.Substring(startIdx + 1, endIdx -(startIdx + 1));
                        string[] fParts = Formula.Substring(endIdx + 1).Split(new char[] {':'}, StringSplitOptions.RemoveEmptyEntries);
                        if (2 == fParts.Count())
                        {
                            if (IsFieldInRowArea(fieldName))
                                Formula = fParts[0];
                            else
                                Formula = fParts[1];
                        }
                    }
                }
            }

            private void ProcessCustomOperators()
            {
                if (Formula.Contains(DISTINCT_OPERATOR))
                {
                    int startIdx = Formula.IndexOf(DISTINCT_OPERATOR + "(");
                    int operatorLength = DISTINCT_OPERATOR.Length;
                    int endIdx = Formula.IndexOf(")", startIdx);
                    if (-1 < startIdx && -1 < endIdx)
                    {
                        string expression = Formula.Substring(startIdx + operatorLength + 1, endIdx - (startIdx + operatorLength + 1));
                        List<Operant> operants = null;
                        GetOperants(ref operants, expression);
                        if (operants.Count > 0)
                        {
                            PivotDrillDownDataSource ds = Event.CreateDrillDownDataSource();
                            List<long> list = new List<long>();
                            for (int i = 0; i < ds.RowCount; i++)
                            {
                                List<Operant> tmp = new List<Operant>();
                                PivotDrillDownDataRow row = ds[i];
                                for (int y = 0; y < operants.Count; y++)
                                {
                                    Operant operant = operants[y];
                                    if (operant.Type == Operant.OperantType.FieldName)
                                    {
                                        if (null != row[operant.Value])
                                            tmp.Add(new Operant(row[operant.Value].ToString()));
                                        else
                                            tmp.Add(new Operant(""));
                                    }
                                    else
                                    {
                                        tmp.Add(operant);
                                    }
                                }
                                decimal? tmpRes = Calculate(tmp);
                                if (tmpRes.HasValue && tmpRes.Value != 0)
                                    list.Add((long)tmpRes.Value);
                            }
                            int res = list.GroupBy(x => x).Count();
                            Formula = Formula.Substring(0, startIdx) + res.ToString() + Formula.Substring(endIdx + 1);
                            /*
                            if (Formula.Length > 0)
                                Formula = String.Format("{0}{1}", res, Formula);
                            else
                                Formula = res.ToString();
                             */
                        }
                    }
                    ProcessCustomOperators();
                }
            }

            private bool IsFieldInRowArea(string fieldName)
            {
                return PivotGridControl.GetFieldsByArea(PivotArea.RowArea).Where(x => x.FieldName.Equals(fieldName)).Count() > 0;
            }

            private void CheckBrackets()
            {
                if (Formula.Contains("("))
                {
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sbBrackedContent = new StringBuilder();
                    bool firstBracketFound = false;
                    bool isFieldContent = false;
                    for (int i = 0; i < Formula.Length; i++)
                    {
                        char character = Formula[i];
                        if (character.Equals('(')) // start bracket content
                        {
                            if (isFieldContent)
                            {
                                if (firstBracketFound)
                                    sbBrackedContent.Append(character);
                                else
                                    sb.Append(character);
                            }
                            else
                                firstBracketFound = true;
                        }
                        else if (character.Equals(')')) // end bracket content
                        {
                            if (isFieldContent)
                            {
                                if (firstBracketFound)
                                    sbBrackedContent.Append(character);
                                else
                                    sb.Append(character);
                            }
                            else
                            {
                                firstBracketFound = false;
                                FormulaInterpretator fi = new FormulaInterpretator(sbBrackedContent.ToString(), PivotGridControl, Event, fieldName2Alias);
                                sbBrackedContent = new StringBuilder();
                                decimal? val = fi.CalculateValue();
                                sb.Append(val.HasValue ? val : 0);
                            }
                        }
                        else if (character.Equals('[')) // start field content
                        {
                            isFieldContent = true;
                            if (firstBracketFound)
                                sbBrackedContent.Append(character);
                            else
                                sb.Append(character);
                        }
                        else if (character.Equals(']')) // end field content
                        {
                            isFieldContent = false;
                            if (firstBracketFound)
                                sbBrackedContent.Append(character);
                            else
                                sb.Append(character);
                        }
                        else
                        {
                            if (firstBracketFound)
                                sbBrackedContent.Append(character);
                            else
                                sb.Append(character);
                        }
                    }
                    Formula = sb.ToString();
                }
            }

            private decimal? Calc(decimal? par1, decimal? par2, string operant)
            {
                decimal? res = null;
                try
                {
                    switch (operant)
                    {
                        case "+":
                            res = par1 + par2;
                            break;
                        case "-":
                            res = par1 - par2;
                            break;
                        case "*":
                            res = par1 * par2;
                            break;
                        case "/":
                            res = par1 / par2;
                            break;
                    }
                }
                catch { }

                return res;
            }
            private void ProcessPlusMinusBlocks(bool isPlus, List<Operant> lstOperants)
            {
                string[] fPriorityChunc = Formula.Split(new char[] { isPlus ? '+' : '-' });
                if (fPriorityChunc.Length > 1)
                {
                    for (int i = 0; i < fPriorityChunc.Length; i++)
                    {
                        string chunc = fPriorityChunc[i];
                        if (Operant.GetOperantType(chunc) == Operant.OperantType.Expression)
                        {
                            FormulaInterpretator fi = new FormulaInterpretator(chunc, PivotGridControl, Event, fieldName2Alias);
                            decimal? calculatedValue = fi.CalculateValue();
                            lstOperants.Add(new Operant((calculatedValue == null) ? "0" : calculatedValue.ToString()));
                        }
                        else
                            lstOperants.Add(new Operant(chunc));
                        lstOperants.Add(new Operant(isPlus ? "+" : "-"));
                    }

                    if (lstOperants.Count > 0)
                        lstOperants.RemoveAt(lstOperants.Count - 1);

                    Formula = string.Empty;
                }
            }
        }

        public class Operant
        {
            public enum OperantType
            {
                FieldName = 0,
                Operator,
                Expression,
                Value

            };

            public OperantType Type { get; private set; }
            public string Value { get; private set; }
            public Operant(string operant)
            {
                Type = GetOperantType(operant);
                Value = operant;
                Value = Value.TrimStart(new char[] { ' ', '[' });
                Value = Value.TrimEnd(new char[] { ' ', ']' });
            }
            public static OperantType GetOperantType(string operant)
            {
                OperantType res;
                decimal tmp;
                if (operant.Length == 1 && (operant.Equals(FormulaInterpretator.PLUS) || operant.Equals(FormulaInterpretator.MINUS) || operant.Equals(FormulaInterpretator.MULTIPLICATION) || operant.Equals(FormulaInterpretator.DIVISION)))
                    res = OperantType.Operator;
                else if (operant.Contains(FormulaInterpretator.PLUS) || operant.Contains(FormulaInterpretator.MINUS) || operant.Contains(FormulaInterpretator.MULTIPLICATION) || operant.Contains(FormulaInterpretator.DIVISION))
                    res = OperantType.Expression;
                else if (Decimal.TryParse(operant, out tmp))
                    res = OperantType.Value;
                else
                    res = OperantType.FieldName;

                return res;
            }

            internal static PivotGridField GetPivotGridField(PivotGridControl pivotGridControl, string fieldName, IDictionary<string, string> fieldName2Alias)
            {
                PivotGridField field = pivotGridControl.Fields[fieldName];
                if (null == field && fieldName2Alias.ContainsKey(fieldName))
                    field = pivotGridControl.Fields[fieldName2Alias[fieldName]];
                return field;
            }
            internal decimal? GetDataValue(PivotGridControl pivotGridControl, PivotCellDisplayTextEventArgs e, IDictionary<string, string> fieldName2Alias)
            {
                decimal? res = null;
                switch (Type)
                {
                    case OperantType.Value:
                        decimal tmp;
                        if (Decimal.TryParse(Value, out tmp))
                            res = tmp;
                        break;
                    case OperantType.FieldName:
                        PivotGridField field = Operant.GetPivotGridField(pivotGridControl, Value, fieldName2Alias);
                        if (null != field)
                        {
                            PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                            List<decimal> list = new List<decimal>();
                            for (int i = 0; i < ds.RowCount; i++)
                            {
                                PivotDrillDownDataRow row = ds[i];
                                if (null != row[field])
                                    list.Add((decimal)row[field]);
                            }
                            res = list.Sum();
                        }
                        break;
                }
                return res;
            }
        }
    }
}
