﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraEditors;
using Logica.Reports.DataAccess;
using DevExpress.XtraEditors.Controls;
//using Logica.Reports.ConfigXmlParser.Model;
using System.Data.SqlClient;
using Logica.Reports.ConfigXmlParser.Model;
using Softserv.Reports.InitiativeMonitoring;
using Softserv.Reports.InitiativeMonitoring.Constants;
using Softserv.Reports.InitiativeMonitoring.DataProvider;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraTreeList.Nodes;
using Softserv.Reports.InitiativeMonitoring.Contracts;
using Logica.Reports.Common.WaitWindow;

namespace Softserve.Reports.InitiativeMonitoring
{
    public partial class SettingsForm : XtraForm
    {
        private const string DEFAULT_INITIATIVE_TYPE = @"Контракты";
        private const string INDICATOR_NAME = "Indicator_Name";
        private const string INDICATOR_VALUE = "Indicator_Value";
        private ReportSettings reportSettings = new ReportSettings();
        private IDictionary<int, FieldItem> profileColumnsInfo = new Dictionary<int, FieldItem>();

        List<FieldItem> inVisibleFields = new List<FieldItem>(); // used for calculated fields
        List<FieldItem> inVisibleIndicators = new List<FieldItem>(); // used for calculated indicators
        List<FieldItem> groupFields = new List<FieldItem>();
        FieldItem indicatorName = null;
        FieldItem indicatorValue = null;

        BaseReportUserControl bruc = null;
        bool initializing = true;

        public SettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            bruc = parent;

            DataAccessProvider.OpenConnection();
            //txtReportName.Text = Resource.ReportHeader; //Softserve.Reports.InitiativeMonitoring.
            dateFrom.DateTime = DateTime.Now.AddDays(-(DateTime.Now.Day - 1));
            dateTo.DateTime = DateTime.Now;
            rbDate.SelectedIndex = 0;

            //Init Tab1
            InitCmbInitiativeTypes();
            InitCmbPersonalChanels();

            BuildRegionList();
            
            //Init Tab2
            InitProfiles();

            RefreshButtonsState();

            initializing = false;

            ReInitInitiativeList();

            chkDeclineTTVUplift_CheckedChanged(chkDeclineTTVUplift, new EventArgs());
            chkUpliftContract_CheckedChanged(chkUpliftContract, new EventArgs());

            //ExpandRegionList();
        }

        internal ReportSettings ReportParameters
        {
            get
            {
                return reportSettings;
            }
        }
        #region TAB1
        private void InitCmbInitiativeTypes()
        {
            DataTable initiativeTypes = DataAccessProvider.GetInitiativeTypes();
            FillSimpleComboBox(initiativeTypes, SQLConstants.ACTION_TYPE_ID, SQLConstants.ACTION_TYPE_NAME, cmbInitiativeType);
            for (int i = 0; i < cmbInitiativeType.Properties.Items.Count; i++ )
            {
                CustomSelectItem item = cmbInitiativeType.Properties.Items[i] as CustomSelectItem;
                if (item != null && item.Text.Equals(DEFAULT_INITIATIVE_TYPE))
                {
                    cmbInitiativeType.SelectedItem = cmbInitiativeType.Properties.Items[i];
                    break;
                }
            }
            //initialReportSettings.InitiativeType = cbChannelType.Text;
        }

        private void InitCmbPersonalChanels()
        {
            DataTable chanelTypes = DataAccessProvider.GetChanelTypeList();
            FillSimpleComboBox(chanelTypes, SQLConstants.CHANELTYPE_ID, SQLConstants.CHANELTYPE, cmbChanelType);
            //initialReportSettings.ChanelType = cbChannelType.Text;
        }

        private void InitProfiles()
        {
            DataTable data = DataAccessProvider.GetReportProfileList(bruc.ReportId);
            foreach (DataRow dr in data.Rows)
            {
                ProfileItem pi = new ProfileItem(
                                   ConvertEx.ToInt(dr[SQLConstants.PROFILE_ID]),
                                   dr[SQLConstants.PROFILE_NAME].ToString(),
                                   ConvertEx.ToBool(dr[SQLConstants.IS_SYSTEM])
                                   );

                lstProfiles.Items.Add(pi);

                cmbProfiles.Properties.Items.Add(pi);

            }
            if (lstProfiles.Items.Count > 1)
            {
                lstProfiles.SelectedIndex = 0;
            }
            if (cmbProfiles.Properties.Items.Count > 0)
                cmbProfiles.SelectedIndex = 0;
        }

        private void FillSimpleComboBox(DataTable dataTable, string fieldID, string fieldValue, ComboBoxEdit cmb)
        {
            foreach (DataRow dr in dataTable.Rows)
            {
                if (null == dr[fieldID] || String.IsNullOrEmpty(dr[fieldID].ToString()))
                    continue;
                cmb.Properties.Items.Add(new CustomSelectItem(ConvertEx.ToInt(dr[fieldID]), dr[fieldValue].ToString()));
            }
            if (cmb.Properties.Items.Count > 0)
                cmb.SelectedIndex = 0;
            cmb.Refresh();
        }

        #endregion TAB1

        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                return new List<SheetParamCollection>();
            }
            /*
            get 
            {
                List<SheetParamCollection> list = new List<SheetParamCollection>();
                // Fill params for M2
                SheetParamCollection paramCollection = new SheetParamCollection();
                
                ComboBoxItem cbi = (ComboBoxItem)(cbM2.Properties.Items[cbM2.SelectedIndex]);

                paramCollection.MainHeader = String.Format(Resource.MainHeader, cbi.Text);
                paramCollection.TabId = Guid.NewGuid();
                paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.M2_PARAM, Value = cbi.Id, DisplayParamName = Resource.M2, DisplayValue = cbi.Text });
                paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.DATE_PARAM, Value = dateEdit.DateTime, DisplayParamName = Resource.ReportDay, DisplayValue = dateEdit.DateTime.ToShortDateString() });
                
                list.Add(paramCollection);

                // Fill params for M1
                foreach (CheckedListBoxItem clbi in cbM1.Properties.Items)
                {
                    if (clbi.CheckState == CheckState.Checked)
                    {
                        list.Add(FillParamCol(Guid.NewGuid(), clbi));
                    }
                }
                return list;
            }*/
        }
        /// <summary>
        /// Fill parameters info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private SheetParamCollection FillParamCol(Guid id)
        {
            SheetParamCollection paramCollection = new SheetParamCollection();
            /*
            paramCollection.MainHeader = String.Format(Resource.MainHeader, merch.Description);
            paramCollection.TabId = id;

            paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.M1_PARAM, Value = merch.Value, DisplayParamName = Resource.M1, DisplayValue = merch.Description });
            paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.DATE_PARAM, Value = dateEdit.DateTime, DisplayParamName = Resource.ReportDay, DisplayValue = dateEdit.DateTime.ToShortDateString() });
            //paramCollection.Add(new SheetParam() { SqlParamName = "@Supervisor_Id", Value = null });
            */
            return paramCollection;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (ValidateSettings(true)) // validate params
                DialogResult = DialogResult.OK;
        }

        private bool ValidateSettings(bool showMessage)
        {
            bool res = true;
            string message = string.Empty;
            reportSettings.initiativeTypeID = ((CustomSelectItem)cmbInitiativeType.SelectedItem).ID;
            reportSettings.channelTypeID = ((CustomSelectItem)cmbChanelType.SelectedItem).ID;
            reportSettings.isOnlyTerritory = chkOnlySelectedRegion.Checked;
            if (rbDate.SelectedIndex == 1 && dateFrom.DateTime > dateTo.DateTime)
            {
                res = false;
                message = string.Format("{0}{1}{2}", message, Resource.Error_Date, Environment.NewLine);
            }
            else if (rbDate.SelectedIndex == 1)
            {
                reportSettings.dateFrom = dateFrom.DateTime;
                reportSettings.dateTo = dateTo.DateTime;

            }
            else
            {
                reportSettings.dateFrom = null;
                reportSettings.dateTo = null;
            }

            reportSettings.VinDal = (rbVinDal.SelectedIndex == 0);
            reportSettings.UpliftGain = (int)spUpliftIncrease.Value;

            reportSettings.M4Id_List = reportSettings.M3Id_List = reportSettings.M2Id_List = string.Empty;
            if (!HasTreeCheckedNode(treeStaff.Nodes))
            {
                res = false;
                message = string.Format("{0}{1}{2}", message, Resource.Error_Region, Environment.NewLine);
            }
            else
            {
                GetStaffInfo(treeStaff.Nodes);
            }

            if (chkUseOneInitiative.Checked)
            {
                CustomSelectItem csi = lstInitiative.SelectedItem as CustomSelectItem;
                if (null == csi)
                {
                    res = false;
                    message = string.Format("{0}{1}{2}", message, Resource.Error_Initiative, Environment.NewLine);
                }
                else
                {
                    reportSettings.initiativeID = csi.ID;
                    // gather all rest info
                    reportSettings.TargetVasCondition = chkVSale.Checked;
                    if (chkDeclineTTVUplift.Checked)
                    {
                        reportSettings.VminHall = (int)speTTVUpliftLow.Value;
                        reportSettings.VmaxHall = (int)speTTVUpliftHight.Value;
                    }

                    if (chkUpliftContract.Checked)
                    {
                        reportSettings.BasketWidth = (int)speUpliftTTVW.Value;
                        reportSettings.BasketVol = (int)speUpliftTTVH.Value;
                        reportSettings.EquatePromoTT = chkEquatePromoTT.Checked;
                    }
                }
            }
            else
            {
                reportSettings.initiativeID = null;
                reportSettings.KPI_List = reportSettings.SKU_List = string.Empty;
            }

            if (!GatherIndicators())
            {
                res = false;
                message = string.Format("{0}{1}{2}", message, Resource.Error_Indicators, Environment.NewLine);
            }

            reportSettings.ReportName = ((ProfileItem)cmbProfiles.SelectedItem).Text;

            if (!res && showMessage)
                XtraMessageBox.Show(this, message, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            return res;
        }

        private bool GatherIndicators()
        {
            bool result = false;
            
            if (lstIndicators.CheckedItems.Count > 0)
            {
                reportSettings.indicatorsIDs = string.Empty;
                for (int i = 0; i < lstIndicators.CheckedItems.Count; i++)
                {
                    reportSettings.indicatorsIDs = string.Format("{0}{1},", reportSettings.indicatorsIDs, (lstIndicators.CheckedItems[i] as FieldItem).ColumnID);
                }

                reportSettings.indicatorsIDs = string.Format("({0})", reportSettings.indicatorsIDs.TrimEnd(new char[] { ',' }));

                result = true;
            }

            return result;
        }

        private string GetOneInitiativeCheckedItems(TreeListNodes nodes)
        {
            string res = string.Empty;
            if (null != nodes)
            {
                foreach (TreeListNode node in nodes)
                {
                    if (node.Checked && (node.Nodes == null || !node.HasChildren))
                    {
                        DataTable data = ((DataTable)node.TreeList.DataSource);
                        DataRow row = data.Rows[node.Id];
                        if (row != null)
                            res += string.Format("{0},", row[SQLConstants.ITEM_ID]);
                    }
                    else
                        res += GetOneInitiativeCheckedItems(node.Nodes);
                }
            }

            if (nodes == nodes.TreeList.Nodes && !string.IsNullOrEmpty(res))
            {
                res = string.Format("({0})", res.TrimEnd(new char[] { ',' }));
            }

            return res;
        }

        private void GetStaffInfo(TreeListNodes nodes)
        {
            if (null != nodes)
            {
                foreach (TreeListNode node in nodes)
                {
                    if (node.Checked)
                    {
                        DataTable data = ((DataTable)node.TreeList.DataSource);
                        DataRow row = data.Rows[node.Id];
                        if (row != null)
                        {
                            int staffLevel = ConvertEx.ToInt(row[SQLConstants.STAFFLEVEL_ID]);
                            switch (staffLevel)
                            {
                                case 5: // country
                                    reportSettings.M4Id_List = reportSettings.M3Id_List = reportSettings.M2Id_List = string.Empty;
                                    break;
                                case 4: // M4
                                    reportSettings.M4Id_List += string.Format("{0},", row[SQLConstants.ITEM_ID]);
                                    break;
                                case 3: // M3
                                    reportSettings.M3Id_List += string.Format("{0},", row[SQLConstants.ITEM_ID]);
                                    break;
                                case 2: // M2
                                    reportSettings.M2Id_List += string.Format("{0},", row[SQLConstants.ITEM_ID]);
                                    break;
                            }
                        }
                    }
                    else
                        GetStaffInfo(node.Nodes);
                }
            }

            if (nodes == treeStaff.Nodes)
            {
                if (!String.IsNullOrEmpty(reportSettings.M4Id_List))
                    reportSettings.M4Id_List = string.Format("({0})", reportSettings.M4Id_List.TrimEnd(new char[] { ',' }));
                if (!String.IsNullOrEmpty(reportSettings.M3Id_List))
                    reportSettings.M3Id_List = string.Format("({0})", reportSettings.M3Id_List.TrimEnd(new char[] { ',' }));
                if (!String.IsNullOrEmpty(reportSettings.M2Id_List))
                    reportSettings.M2Id_List = string.Format("({0})", reportSettings.M2Id_List.TrimEnd(new char[] { ',' }));
            }
        }

        private bool HasTreeCheckedNode(TreeListNodes nodes)
        {
            bool res = false;
            if (null != nodes)
            {
                foreach (TreeListNode node in nodes)
                {
                    res = node.Checked;
                    if (!res)
                    {
                        res = HasTreeCheckedNode(node.Nodes);
                    }
                    if (res)
                        break;
                }
            }
            return res;
        }

        #region Column moving opertions
        private void MoveField(ListBoxControl lstSource, ListBoxControl lstDestination)
        {
            int i = 0;
            // unselect selected items
            int destSelectedItems = lstDestination.SelectedIndices.Count;
            for (i = 0; i < destSelectedItems; i++)
            {
                lstDestination.SetSelected(lstDestination.SelectedIndices[0], false);
            }
            int selectedItems = lstSource.SelectedItems.Count;
            for (i = 0; i < selectedItems; i++)
            {
                // Old code for simple selection
                /*
                object fieldToMove = lstSource.Items[lstSource.SelectedIndex];
                lstSource.Items.RemoveAt(lstSource.SelectedIndex);
                lstDestination.SelectedIndex = lstDestination.Items.Add(fieldToMove);
                */
                object fieldToMove = lstSource.SelectedItems[0];
                lstSource.Items.Remove(fieldToMove);
                int newIndex = lstDestination.Items.Add(fieldToMove);
                lstDestination.SetSelected(newIndex, true);
            }
            if (lstSource.Items.Count>0)
                lstSource.SelectedIndex = 0;
            RefreshButtonsState();
        }

        private void btnToFilter_Click(object sender, EventArgs e)
        {
            MoveField(lstAllFields, lstFilterFields);
        }

        private void btnFromFilter_Click(object sender, EventArgs e)
        {
            MoveField(lstFilterFields, lstAllFields);
        }

        private void btnToRow_Click(object sender, EventArgs e)
        {
            MoveField(lstAllFields, lstRowFields);
        }

        private void btnFromRow_Click(object sender, EventArgs e)
        {
            MoveField(lstRowFields, lstAllFields);
        }

        private void btnToColumn_Click(object sender, EventArgs e)
        {
            MoveField(lstAllFields, lstColumnFields);
        }

        private void btnFromColumn_Click(object sender, EventArgs e)
        {
            MoveField(lstColumnFields, lstAllFields);
        }
        #endregion Column moving opertions

        private void listPresetsFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReInitColumnsInfo();
            int idx = lstProfiles.SelectedIndex;
            if (-1 != idx)
                cmbProfiles.SelectedIndex = idx;
        }

        private void ReInitColumnsInfo()
        {
            inVisibleFields.Clear();
            
            profileColumnsInfo.Clear();

            FillIndicators();

            DataTable data = DataAccessProvider.GetReportColumnList(bruc.ReportId);

            List<FieldItem> filterArea = InitColumnsInfoByArea(ColumnPlaceType.filter);
            List<FieldItem> rowArea = InitColumnsInfoByArea(ColumnPlaceType.row);
            List<FieldItem> columnArea = InitColumnsInfoByArea(ColumnPlaceType.column);
            groupFields = InitColumnsInfoByArea(ColumnPlaceType.group);
            
            for (int i = 0; i < groupFields.Count; i++)
            {
                FieldItem fi = groupFields[i];
                CheckIfIndicatorsInfoItem(fi, ColumnPlaceType.group);
            }

            FillColumnsDataArea(lstFilterFields, filterArea);
            FillColumnsDataArea(lstRowFields, rowArea);
            FillColumnsDataArea(lstColumnFields, columnArea);

            lstAllFields.Items.Clear();
            // filling all rest columns
            foreach (DataRow dr in data.Rows)
            {
                if (!profileColumnsInfo.Keys.Contains(ConvertEx.ToInt(dr[SQLConstants.COLUMN_ID])))
                {
                    FieldItem item = GetFieldItem(dr);
                    if (item.IsVisible)
                        lstAllFields.Items.Add(item);
                    else
                        inVisibleFields.Add(item);
                }
            }

            if (lstAllFields.Items.Count > 0)
                lstAllFields.SelectedIndex = 0;
        }

        private FieldItem GetFieldItem(DataRow dr)
        {
            FieldItem item = new FieldItem()
            {
                ColumnID = ConvertEx.ToInt(dr[SQLConstants.COLUMN_ID]),
                ColumnName = dr[SQLConstants.COLUMN_NAME].ToString(),
                ColumnAlias = dr[SQLConstants.COLUMN_ALIAS].ToString(),
                ColumnWidth = ConvertEx.ToInt(dr[SQLConstants.COLUMN_WIDTH]),
                StaffLevelID = ConvertEx.ToInt(dr[SQLConstants.STAFFLEVEL_ID]),
                DisplayFormat = dr[SQLConstants.DISPLAY_FORMAT].ToString(),
                IsCalculated = ConvertEx.ToBool(dr[SQLConstants.IS_CALCULATED]),
                ColumnFormula = dr[SQLConstants.COLUMN_FORMULA].ToString(),
                DisplayType = ConvertEx.ToInt(dr[SQLConstants.DISPLAY_TYPE]),
                IsVisible = ConvertEx.ToBool(dr[SQLConstants.IS_VISIBLE]),
                IsForOneInitiative = ConvertEx.ToBool(dr[SQLConstants.IS_ONE_INITIATIVE]),
                IsIndicator = ConvertEx.ToBool(dr[SQLConstants.IS_INDICATOR])
            };
               
            try
            {
                object obj = dr[SQLConstants.COLUMN_TYPE];
                item.ColumnType = ConvertEx.ToInt(obj);
            }
            catch{}

            //if (item.IsIndicator)
                //item.ColumnName = item.ColumnAlias;
            return item;
        }

        private void FillColumnsDataArea(ListBoxControl lst, List<FieldItem> dataArea)
        {
            lst.Items.Clear();
            if (dataArea.Count > 0)
            {
                lst.Items.AddRange(dataArea.ToArray());
                lst.SelectedIndex = 0;
            }
        }

        private void FillIndicators()
        {
            inVisibleIndicators.Clear();
            lstIndicators.Items.Clear();
            DataTable data = DataAccessProvider.GetReportProfileColumnList(((ProfileItem)lstProfiles.Items[lstProfiles.SelectedIndex]).ID, null, true);
            if (null == data)
                return;

            foreach (DataRow dr in data.Rows)
            {
                FieldItem item = GetFieldItem(dr);
                item.ProfileID = ConvertEx.ToInt(dr[SQLConstants.PROFILE_ID]);
                //profileColumnsInfo.Add(item.ColumnID, item);
                if (item.IsVisible)
                {
                    int curItemID = lstIndicators.Items.Add(item);
                    CheckedListBoxItem clbi = lstIndicators.Items[curItemID];
                    clbi.CheckState = (item.ColumnType > 0) ? CheckState.Checked : CheckState.Unchecked;
                    // temp
                    //inVisibleFields.Add(item);
                }
                else
                    inVisibleFields.Add(item);
            }
             //*/
        }

        private List<FieldItem> InitColumnsInfoByArea(ColumnPlaceType columnPlaceType)
        {
            List<FieldItem> res = new List<FieldItem>();
            DataTable data = DataAccessProvider.GetReportProfileColumnList(((ProfileItem)lstProfiles.Items[lstProfiles.SelectedIndex]).ID, columnPlaceType, false);
            foreach (DataRow dr in data.Rows)
            {
                FieldItem item = GetFieldItem(dr);
                item.ProfileID = ConvertEx.ToInt(dr[SQLConstants.PROFILE_ID]);
                profileColumnsInfo.Add(item.ColumnID, item);
                if (item.IsVisible)
                    res.Add(item);
                else
                {
                    if (!CheckIfIndicatorsInfoItem(item, columnPlaceType))
                        inVisibleFields.Add(item);
                }
            }
            return res;
        }

        private bool CheckIfIndicatorsInfoItem(FieldItem item, ColumnPlaceType columnPlaceType)
        {
            bool res = false;
            if (item.ColumnName.Equals(INDICATOR_NAME))
            {
                indicatorName = item;
                res = true;
            }
            else if (item.ColumnName.Equals(INDICATOR_VALUE))
            {
                indicatorValue = item;
                res = true;
            } 

            if ( res)
                item.IsVisible = true;

            return res;
        }

        private void SetButtonEnabled(SimpleButton btn, object sender)
        {
            bool enabled = false;
            ListBoxControl lbc = sender as ListBoxControl;
            if (null != lbc)
                enabled = (lbc.SelectedItems != null && lbc.SelectedItems.Count > 0);
            btn.Enabled = enabled;
        }

        private void lstFilterFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetButtonEnabled(btnFromFilter, sender);
            RefreshButtonsState();
        }

        private void lstRowFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetButtonEnabled(btnFromRow, sender);
            RefreshButtonsState();
        }

        private void lstColumnFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetButtonEnabled(btnFromColumn, sender);
            RefreshButtonsState();
        }

        private void lstAllFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetButtonEnabled(btnToFilter, sender);
            SetButtonEnabled(btnToRow, sender);
            SetButtonEnabled(btnToColumn, sender);
            RefreshButtonsState();
        }

        private void lstFields_MouseClick(object sender, MouseEventArgs e)
        {
            RefreshButtonsState();
        }

        private void RefreshButtonsState()
        {
            // move from buttons
            SetButtonEnabled(btnFromFilter, lstFilterFields);
            SetButtonEnabled(btnFromRow, lstRowFields);
            SetButtonEnabled(btnFromColumn, lstColumnFields);

            // move to buttons
            SetButtonEnabled(btnToFilter, lstAllFields);
            SetButtonEnabled(btnToRow, lstAllFields);
            SetButtonEnabled(btnToColumn, lstAllFields);

            SetUpDownButtonsState();
        }

        private void SetUpDownButtonsState()
        {
            SetUpDownButtonState(lstRowFields, btnRowUp, btnRowDown);
            SetUpDownButtonState(lstColumnFields, btnColumnUp, btnColumnDown);
        }

        private void SetUpDownButtonState(ListBoxControl lbc, SimpleButton btnUp, SimpleButton btnDown)
        {
            btnUp.Enabled = true;
            btnDown.Enabled = true;

            if (lbc.Items.Count < 2)
            {
                btnUp.Enabled = false;
                btnDown.Enabled = false;
            }
            else if (lbc.SelectedIndex == 0)
                btnUp.Enabled = false;
            else if (lbc.SelectedIndex == lbc.Items.Count - 1)
                btnDown.Enabled = false;
        }

        private void btnRowUp_Click(object sender, EventArgs e)
        {
            ChangeFieldOrder(lstRowFields, true);
        }

        private void btnRowDown_Click(object sender, EventArgs e)
        {
            ChangeFieldOrder(lstRowFields, false);
        }

        private void btnColumnUp_Click(object sender, EventArgs e)
        {
            ChangeFieldOrder(lstColumnFields, true);
        }

        private void btnColumnDown_Click(object sender, EventArgs e)
        {
            ChangeFieldOrder(lstColumnFields, false);
        }

        private void ChangeFieldOrder(ListBoxControl lbc, bool up)
        {
            int step = (up) ? -1 : 1;
            int currentIndex = lbc.SelectedIndex;
            int nextIndex = currentIndex + step;
            object tmp = lbc.Items[currentIndex];
            lbc.Items[currentIndex] = lbc.Items[nextIndex];
            lbc.Items[nextIndex] = tmp;

            lbc.SelectedIndex = nextIndex;

            SetUpDownButtonsState();
        }

        private void ExpandRegionList()
        {
            treeStaff.ExpandAll();
            treeStaff.CollapseAll();
            if (null != treeStaff.Nodes && null != treeStaff.Nodes.FirstNode)
            {
                treeStaff.Nodes.FirstNode.Expanded = true;
                treeStaff.Nodes.FirstNode.CheckState = CheckState.Checked;
                tree_AfterCheckNode(treeStaff, new DevExpress.XtraTreeList.NodeEventArgs(treeStaff.Nodes.FirstNode));
            }
        }

        private void BuildRegionList()
        {
            treeStaff.ClearNodes();

            DataTable data = DataAccessProvider.GetTerritoryPersonnelTree(((CustomSelectItem)cmbChanelType.SelectedItem).ID);
            treeStaff.DataSource = data;
            treeStaff.Enabled = false;
            treeStaff.Enabled = true;
            ExpandRegionList();
            //RegionListAddChildNodes(data, null);
        }

        private void RegionListAddChildNodes(DataTable data, TreeListNode parentNode)
        {
            if (null != data)
            {
                int parentID = 0;
                if (null != parentNode)
                {
                    StaffInfoItem parentItem = parentNode.Tag as StaffInfoItem;
                    if (null != parentItem)
                        parentID = parentItem.ID;
                }

                foreach (DataRow dr in data.Rows)
                {
                    int currentParentID = ConvertEx.ToInt(dr[SQLConstants.PARENT_ID]);
                    if ((parentID == 0 && null == parentNode) || (null != parentNode && parentID == currentParentID))
                    {
                        StaffInfoItem sii = new StaffInfoItem()
                                                {
                                                    ID = ConvertEx.ToInt(dr[SQLConstants.ID]),
                                                    ItemID = ConvertEx.ToInt(dr[SQLConstants.ITEM_ID]),
                                                    Name = dr[SQLConstants.NAME].ToString(),
                                                    ParentID = currentParentID,
                                                    StaffLevelID = ConvertEx.ToInt(dr[SQLConstants.STAFFLEVEL_ID])
                                                };
                        TreeListNode childNode = treeStaff.AppendNode(new object[] { dr[SQLConstants.NAME] }, parentNode, sii);
                        RegionListAddChildNodes(data, childNode);
                    }
                }
            }
        }

        private void ReInitInitiativeList()
        {
            if (initializing)
                return;
            WaitManager.StartWait();
            System.Diagnostics.Debug.Write("ReInitInitiativeList: " + Environment.TickCount);

            lstInitiative.Items.Clear();
            DateTime? dateStart = null;
            DateTime? dateEnd = null;
            if (rbDate.SelectedIndex == 1)
            {
                dateStart = dateFrom.DateTime;
                dateEnd = dateTo.DateTime;
            }
            ValidateSettings(false);
            string M2TmpList = reportSettings.M2Id_List;
            if (!HasTreeCheckedNode(treeStaff.Nodes))
                M2TmpList = "(-1)";
            DataTable data = DataAccessProvider.GetInitiativeList(((CustomSelectItem)cmbInitiativeType.SelectedItem).ID, ((CustomSelectItem)cmbChanelType.SelectedItem).ID, dateStart, dateEnd,
                reportSettings.isOnlyTerritory, reportSettings.M4Id_List, reportSettings.M3Id_List, M2TmpList);

            initializing = true;
            foreach (DataRow dr in data.Rows)
                lstInitiative.Items.Add(new CustomSelectItem(ConvertEx.ToInt(dr[SQLConstants.INITIATIVE_ID]), dr[SQLConstants.INITIATIVE_NAME].ToString()));
            initializing = false; ;

            if (lstInitiative.Items.Count > 0)
                lstInitiative.SelectedIndex = 0;
            lstInitiative.Refresh();

            InitOneInitiativeControls();

            btnYes.Enabled = true;
            if (chkUseOneInitiative.Checked && lstInitiative.Items.Count == 0)
                btnYes.Enabled = false;

            System.Diagnostics.Debug.WriteLine(" - " + Environment.TickCount);
            WaitManager.StopWait();
        }

        private void cmbInitiativeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReInitInitiativeList();
        }

        private void rbDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool enabled = (rbDate.SelectedIndex == 1);
            dateFrom.Enabled = enabled;
            dateTo.Enabled = enabled;

            ReInitInitiativeList();
        }

        private void date_TextChanged(object sender, EventArgs e)
        {
            ReInitInitiativeList();
        }

        private void chkUseOneInitiative_EditValueChanged(object sender, EventArgs e)
        {
            bool enabled = (chkUseOneInitiative.Checked == true);
            lstInitiative.Enabled = enabled;
            if (!enabled || (enabled && lstInitiative.ItemCount > 0))
                btnYes.Enabled = true;
            else
                btnYes.Enabled = false;

            //this.Invalidate(true);
            lstIndicators.Refresh();
            if (!chkUseOneInitiative.Checked)
                lblWarning.Visible = true;
            else
                lblWarning.Visible = false;
        }
        
        internal IDictionary<ColumnPlaceType, ListBoxItemCollection> GetFieldsInfo()
        {
            IDictionary<ColumnPlaceType, ListBoxItemCollection> fieldsInfo = new Dictionary<ColumnPlaceType, ListBoxItemCollection>(); 
            
            fieldsInfo.Add(ColumnPlaceType.filter, GetFieldsInfo(ColumnPlaceType.filter));
            fieldsInfo.Add(ColumnPlaceType.row, GetFieldsInfo(ColumnPlaceType.row));
            fieldsInfo.Add(ColumnPlaceType.column, GetFieldsInfo(ColumnPlaceType.column));
            fieldsInfo.Add(ColumnPlaceType.group, GetFieldsInfo(ColumnPlaceType.group));

            return fieldsInfo;
        }

        private void CopyListBoxItemCollection(ListBoxControl source, ref ListBoxItemCollection destination)
        {
            if (null == destination)
                destination = new ListBoxItemCollection();
            //ListBoxItemCollection result = new ListBoxItemCollection();
            if (null != source)
            {
                for (int i = 0; i < source.Items.Count; i++)
                {
                    FieldItem fi = source.Items[i] as FieldItem;
                    if (null != fi)
                    {
                        if (chkUseOneInitiative.Checked)
                            destination.Add(fi);
                        else if (!fi.IsForOneInitiative)
                            destination.Add(fi);
                    }
                }
            }
            //return destination;
        }

        private void CopyListBoxItemCollection(CheckedListBoxControl source, ref ListBoxItemCollection destination)
        {
            if (null == destination)
                destination = new ListBoxItemCollection();
            //ListBoxItemCollection result = new ListBoxItemCollection();
            if (null != source)
            {
                for (int i = 0; i < source.CheckedItems.Count; i++)
                {
                    FieldItem fi = source.CheckedItems[i] as FieldItem;
                    if (null != fi)
                    {
                        if (chkUseOneInitiative.Checked)
                            destination.Add(fi);
                        else if (!fi.IsForOneInitiative)
                            destination.Add(fi);
                    }
                }
            }
            //return destination;
        }

        internal ListBoxItemCollection GetFieldsInfo(ColumnPlaceType columnPlaceType)
        {
            ListBoxItemCollection result = null;
            switch (columnPlaceType)
            {
                case ColumnPlaceType.filter:
                    CopyListBoxItemCollection(lstFilterFields, ref result);
                    //result.AddRange(inVisibleFields.ToArray());
                    break;
                case ColumnPlaceType.row:
                    CopyListBoxItemCollection(lstRowFields, ref result);
                    break;
                case ColumnPlaceType.column:
                    CopyListBoxItemCollection(lstColumnFields, ref result);
                    result.AddRange(inVisibleFields.ToArray());
                    CopyListBoxItemCollection(lstIndicators, ref result);
                    if (null != indicatorValue)
                        result.Add(indicatorValue);
                    break;
                case ColumnPlaceType.group:
                    CopyListBoxItemCollection((ListBoxControl)null, ref result);
                    result.AddRange(groupFields.ToArray());
                    if (null != indicatorName)
                        result.Add(indicatorName);
                    break;
            }

            CheckFieldVisibility(ref result);
            return result;
        }

        private void CheckFieldVisibility(ref ListBoxItemCollection fieldsItemCollection)
        {
            if (!chkUseOneInitiative.Checked)
            {
                for (int i = 0; i < fieldsItemCollection.Count; i++)
                {
                    FieldItem fi = fieldsItemCollection[i] as FieldItem;
                    if (fi != null && fi.IsForOneInitiative)
                        fi.IsVisible = false;
                }
            }
        }

        private void cmbProfiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIndex = cmbProfiles.SelectedIndex;
            if (-1 != selectedIndex)
                lstProfiles.SelectedIndex = selectedIndex;
        }

        private void speUpliftTTVW_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void chkUpliftContract_CheckedChanged(object sender, EventArgs e)
        {
            bool enabled = (((CheckEdit)sender).Checked);
            speUpliftTTVH.Enabled = enabled;
            speUpliftTTVW.Enabled = enabled;
            chkEquatePromoTT.Enabled = enabled;
        }

        private void chkDeclineTTVUplift_CheckedChanged(object sender, EventArgs e)
        {
            bool enabled = (((CheckEdit)sender).Checked);
            speTTVUpliftLow.Enabled = enabled;
            speTTVUpliftHight.Enabled = enabled;
        }

        private void chkUseOneInitiative_CheckedChanged(object sender, EventArgs e)
        {
            bool enabled = (((CheckEdit)sender).Checked);

            lstInitiative.Enabled = enabled;
            chkVSale.Enabled = enabled;
            chkDeclineTTVUplift.Enabled = enabled;
            speTTVUpliftLow.Enabled = enabled;
            speTTVUpliftHight.Enabled = enabled;
            chkUpliftContract.Enabled = enabled;
            speUpliftTTVW.Enabled = enabled;
            speUpliftTTVH.Enabled = enabled;

            InitOneInitiativeControls();
        }

        private void InitOneInitiativeControls()
        {
            if (initializing)
                return;

            if (!chkUseOneInitiative.Checked)
                return;
        }

        private void chkOnlySelectedRegion_CheckedChanged(object sender, EventArgs e)
        {
            ReInitInitiativeList();
        }

        private void cmbChanelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildRegionList();
            ReInitInitiativeList();
        }

        private void treeStaff_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {

        }

        private void lstActivity_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitOneInitiativeControls();
        }

        private void tree_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            if (e.Node.CheckState == CheckState.Checked)
            {
                CheckChildNodes(e.Node, true);
                if (null != e.Node.ParentNode)
                    CheckParentNodes(e.Node.ParentNode);
            }
            else if (e.Node.CheckState == CheckState.Unchecked)
            {
                CheckChildNodes(e.Node, false);
                UncheckParentNodes(e.Node);
            }

            if (sender == treeStaff)
            {
                ReInitInitiativeList();
            }
        }
        private void CheckChildNodes(TreeListNodes treeListNodes, bool checkedState)
        {
            if (null != treeListNodes)
            {
                foreach (TreeListNode treeListNode in treeListNodes)
                {
                    treeListNode.Checked = checkedState;
                    CheckChildNodes(treeListNode, checkedState);
                }
            }
        }

        private void CheckChildNodes(TreeListNode treeListNode, bool checkedState)
        {
            if (null != treeListNode && treeListNode.HasChildren)
            {
                foreach (TreeListNode childNode in treeListNode.Nodes)
                {
                    childNode.Checked = checkedState;
                    CheckChildNodes(childNode, checkedState);
                }
            }
        }
        private void CheckParentNodes(TreeListNode treeListNode)
        {
            if (null != treeListNode)
            {
                bool allChildChecked = true;
                foreach (TreeListNode childNode in treeListNode.Nodes)
                {
                    if (!childNode.Checked)
                    {
                        allChildChecked = false;
                        break;
                    }
                }

                if (allChildChecked)
                {
                    treeListNode.Checked = true;
                    if (null != treeListNode.ParentNode)
                        CheckParentNodes(treeListNode.ParentNode);
                }
            }
        }
        private void UncheckParentNodes(TreeListNode treeListNode)
        {
            if (null != treeListNode && null != treeListNode.ParentNode)
            {
                treeListNode.ParentNode.Checked = false;
                UncheckParentNodes(treeListNode.ParentNode);
            }
        }

        private void lstFields_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            Brush backBrush = new SolidBrush(Color.Gray);

            // declare field representing the text of the item being drawn

            if (!chkUseOneInitiative.Checked)
            {
                FieldItem fi = null;
                ListBoxControl lbc = sender as ListBoxControl;
                if (null != lbc)
                    fi = lbc.GetItem(e.Index) as FieldItem;
                else
                {
                    CheckedListBoxControl clbc = sender as CheckedListBoxControl;
                    if (null != clbc)
                        fi = (clbc.GetItem(e.Index) as CheckedListBoxItem).Value as FieldItem;
                }

                if (null != fi && fi.IsForOneInitiative)
                {
                    if ((e.State & DrawItemState.Selected) == 0)
                    {
                        e.Appearance.BackColor = Color.Gray;
                    }
                    //((BaseListBoxControl)sender).Invalidate();
                    return;

                    string itemText = " " + ((BaseListBoxControl)sender).GetItemText(e.Index);

                    if ((e.State & DrawItemState.Selected) != 0)
                    {
                        e.Cache.FillRectangle(e.Appearance.BackColor, e.Bounds);
                        e.Cache.DrawString(itemText, e.Appearance.Font, new SolidBrush(Color.White),
                          e.Bounds, e.Appearance.GetStringFormat());
                    }
                    else
                    {
                        e.Cache.DrawString(itemText, e.Appearance.Font, new SolidBrush(Color.Gray),
                          e.Bounds, e.Appearance.GetStringFormat());
                    }
                    e.Handled = true;
                }
            }
            //else
            {
                //((BaseListBoxControl)sender).Refresh();
                //Application.DoEvents();
                //this.Invalidate();
            }
        }

        private void tcOptions_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (/*tcOptions.SelectedTabPage == xtraTabPage1 && */!chkUseOneInitiative.Checked)
                lblWarning.Visible = true;
            else
                lblWarning.Visible = false;
        }
    }
}
