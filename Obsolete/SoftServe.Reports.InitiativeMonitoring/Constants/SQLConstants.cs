﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softserv.Reports.InitiativeMonitoring.Constants
{
    internal static class SQLConstants
    {
        // SP Names
        internal const string SP_DW_CHANNEL_LIST = @"spDW_ChannelList";
        internal const string SP_DW_RPTC_GET_SKU = @"spDW_RPTC_GetSKU";
        internal const string SP_DW_RPTC_GET_SKU_TREE = @"spDW_RPTC_GetSKUTree";
        internal const string SP_DW_MI_GET_INITIATIVE_TYPE_LIST = @"spDW_MI_GetInitiativeTypeList";
        internal const string SP_DW_GET_REPORT_PROFILE_LIST = @"spDW_GetReportProfileList";
        internal const string SP_DW_GET_REPORT_COLUMN_LIST = @"spDW_GetReportColumnList";
        internal const string SP_DW_GET_REPORT_PROFILE_COLUMN_LIST = @"spDW_GetReportProfileColumnList";
        internal const string SP_DW_MI_GET_INITIATIVE_LIST = @"spDW_MI_GetInitiativeList";
        internal const string SP_DW_MI_GET_TERRITORY_PERSONNEL_TREE = @"spDW_MI_GetTerritoryPersonnelTree";
        internal const string SP_DW_MI_GET_INITIATIVE_SKU_TREE = @"spDW_MI_GetInitiativeSKUTree";
        internal const string SP_DW_MI_GET_INITIATIVE_KPI_TREE = @"spDW_MI_GetInitiativeKPITree";

        internal const string SP_DW_MI_REPORT_MAIN = @"spDW_MI_ReportMain";

        // SP Params
        //internal const string PAR_DATE_START = @"@Date";
        internal const string PAR_REPORT_ID = @"@report_Id";
        internal const string PAR_PROFILE_ID = @"@profile_Id";
        internal const string PAR_COLUMN_TYPE = @"@column_type";
        internal const string PAR_IS_INDICATOR = @"@isIndicator";

        internal const string PAR_INITIATIVE_TYPE_ID = @"@initiativeType_Id";
        internal const string PAR_CHANNEL_TYPE_ID = @"@channelType_Id";
        internal const string PAR_DATE_FROM = @"@dateFrom";
        internal const string PAR_DATE_TO = @"@dateTo";

        internal const string PAR_INITIATIVE_ID = @"@initiative_Id";

        internal const string PAR_INITIATIVETYPE_ID = @"@initiativeType_Id";
        internal const string PAR_CHANNELTYPE_ID = @"@channelType_Id";
        internal const string PAR_IS_ONLY_TERRITORY = @"@isOnlyTerritory";
        internal const string PAR_M4ID_LIST = @"@M4Id_List";
        internal const string PAR_M3ID_LIST = @"@M3Id_List";
        internal const string PAR_M2ID_LIST = @"@M2Id_List";
        internal const string PAR_SKU_LIST = @"@SKU_List";
        internal const string PAR_KPI_LIST = @"@KPI_List";

        internal const string PAR_INDICATOR_ID_LIST = @"indicatorId_List";
        // bit = 1             -- =1 V считается в дал, =0 - V считается в гл  
        internal const string PAR_VIN_DAL = @"VinDal";
        //int = null      -- не брать ТТ для uplift% с приростом > или < заданного значения  
        internal const string PAR_UPLIFT_GAIN = @"upliftGain";
        //bit = 0 -- считать целевой V продаж план/факт условием  
        internal const string PAR_TARGET_VAS_CONDITION = @"targetVasCondition";
        // не брать ТТ вне заданного коридора V для uplift-а:  
        //int = null        -- нижний предел V в %  
        internal const string PAR_VMIN_HALL = @"VminHall";
        //int = null        -- верхний предел V в %  
        internal const string PAR_VMAX_HALL = @"VmaxHall";
        // отфильтровать ТТ для uplift-а по корзинам М3:  
        //int = null     -- ширина корзины <= @basketWidth % от max V в ТТ  
        internal const string PAR_BASKET_WIDTH = @"basketWidth";
        //int = null       -- емкость корзины <= @basketWidth % от кол-ва ТТ в группе  
        internal const string PAR_BASKET_VOL = @"basketVol";
        //bit = null   -- равнять емкость промо корзин по контрольным  
        internal const string PAR_EQUATE_PROMO_TT = @"equatePromoTT";

        // Field Names
        internal const string CHANELTYPE_ID = @"ChanelType_id";
        internal const string CHANELTYPE = @"ChanelType";

        internal const string ACTION_TYPE_ID = @"ActionType_ID";
        internal const string ACTION_TYPE_NAME = @"ActionType_Name";

        internal const string INITIATIVE_ID = @"Initiative_Id";
        internal const string INITIATIVE_NAME = @"Initiative_Name";

        internal const string PROFILE_ID = @"Profile_Id";
        internal const string PROFILE_NAME = @"Profile_Name";
        internal const string IS_SYSTEM = @"isSystem";

        internal const string COLUMN_ID = @"Column_Id";
        internal const string COLUMN_NAME = @"Column_Name";
        internal const string COLUMN_ALIAS = @"Column_Alias";
        internal const string COLUMN_WIDTH = @"Column_Width";
        internal const string COLUMN_TYPE = @"Column_Type";
        internal const string STAFFLEVEL_ID = @"StaffLevel_ID";
        internal const string DISPLAY_FORMAT = @"Display_Format";
        internal const string IS_CALCULATED = @"isCalculated";
        internal const string COLUMN_FORMULA = @"Column_Formula";
        internal const string DISPLAY_TYPE = @"Display_Type";
        internal const string IS_VISIBLE = @"isVisible";
        internal const string IS_ONE_INITIATIVE = @"isOneInitiative";
        internal const string IS_INDICATOR = @"isIndicator";

        internal const string ACTION_ID = @"Action_ID";
        internal const string ACTION_NAME = @"Action_Name";

        internal const string ID = @"Id";
        internal const string PARENT_ID = @"Parent_Id";
        internal const string ITEM_ID = @"Item_Id";
        internal const string NAME = @"Name";
    }
}
