﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softserv.Reports.InitiativeMonitoring.Constants
{
    internal class ReportSettings
    {
        public int initiativeTypeID { get; set; }
        public int channelTypeID { get; set; }
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }
        public bool isOnlyTerritory { get; set; }
        public string M4Id_List { get; set; }
        public string M3Id_List { get; set; }
        public string M2Id_List { get; set; }
        public string indicatorsIDs { get; set; }
        public int? initiativeID { get; set; }

        #region Obsolete properties
        public string SKU_List { get; set; }
        public string KPI_List { get; set; }
        #endregion

        /// <summary>
        /// =1 V считается в дал, =0 - V считается в гл
        /// </summary>
        public bool VinDal { get; set; }

        /// <summary>
        /// = null      -- не брать ТТ для uplift% с приростом > или < заданного значения
        /// </summary>
        public int UpliftGain { get; set; }
        /// <summary>
        /// bit = 0 -- считать целевой V продаж план/факт условием
        /// </summary>
        public bool TargetVasCondition { get; set; }

        ///
        /// не брать ТТ вне заданного коридора V для uplift-а:
        ///
        ///<summary>    
        /// нижний предел V в %
        /// </summary>
        public int? VminHall { get; set; }
        /// <summary>
        /// верхний предел V в %
        /// </summary>
        public int? VmaxHall { get; set; }

        ///отфильтровать ТТ для uplift-а по корзинам М3:

        /// <summary>
        /// ширина корзины <= @basketWidth % от max V в ТТ
        /// </summary>
        public int? BasketWidth { get; set; }
        /// <summary>
        /// емкость корзины <= @basketWidth % от кол-ва ТТ в группе
        /// </summary>
        public int? BasketVol { get; set; }
        /// <summary>
        /// равнять емкость промо корзин по контрольным
        /// </summary>
        public bool? EquatePromoTT { get; set; }

        /// <summary>
        /// Report Name
        /// </summary>
        public string ReportName { get; set; }
    }
}
