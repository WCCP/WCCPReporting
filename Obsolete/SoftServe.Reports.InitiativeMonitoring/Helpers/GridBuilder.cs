﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraPivotGrid;
using Softserv.Reports.InitiativeMonitoring.Contracts;
using DevExpress.Data;

namespace Softserv.Reports.InitiativeMonitoring
{
    internal static class GridBuilder
    {
        internal static PivotGridField AddField(PivotGridControl pivotGridControl, FieldItem fi, PivotArea pivotArea, int orderIndex)
        {
            PivotGridField pgf = null;
            try
            {
                pivotGridControl.BeginInit();
                pgf = pivotGridControl.Fields.Add();
                pgf.Appearance.Header.Options.UseTextOptions = true;
                pgf.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                pgf.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                pgf.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                
                pgf.Appearance.Value.Options.UseTextOptions = true;
                pgf.Appearance.Value.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                
                pgf.Area = pivotArea;
                //*
                if (pivotArea == PivotArea.DataArea)
                {
                    //*
                    pgf.Appearance.Cell.Options.UseTextOptions = true;
                    //pgf.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    pgf.Appearance.Cell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    pgf.Appearance.Cell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    //*/
                    /*
                    pgf.Appearance.CellTotal.Options.UseTextOptions = true;
                    pgf.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    pgf.Appearance.CellTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    pgf.Appearance.CellTotal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    //*/
                    /*
                    pgf.Appearance.CellGrandTotal.Options.UseTextOptions = true;
                    pgf.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    pgf.Appearance.CellGrandTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    pgf.Appearance.CellGrandTotal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    //*/
                    pgf.Appearance.Value.Options.UseTextOptions = true;
                    pgf.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    pgf.Appearance.Value.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    pgf.Appearance.Value.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                }
                //*/
                pgf.AreaIndex = orderIndex;
                pgf.Caption = fi.ColumnAlias;
                pgf.ColumnValueLineCount = 3;
                //pgf.Appearance.
                //pgf.RowValueLineCount = 2;
                pgf.Width = pgf.MinWidth = fi.ColumnWidth;
                pgf.Name = fi.ColumnName + "_" + orderIndex.ToString();
                pgf.Options.ShowGrandTotal = false;
                if (fi.IsCalculated && !string.IsNullOrEmpty(fi.ColumnFormula))
                {
                    pgf.UnboundExpression = fi.ColumnFormula;
                    pgf.UnboundType = UnboundColumnType.Bound;//GetUnboundType(fi.DisplayType);
                }
                else
                    pgf.FieldName = fi.ColumnName;

                if (!string.IsNullOrEmpty(fi.DisplayFormat))
                {
                    pgf.CellFormat.FormatType = (DevExpress.Utils.FormatType)fi.DisplayType;
                    pgf.CellFormat.FormatString = fi.DisplayFormat;
                }
                pgf.Visible = fi.IsVisible;
                pivotGridControl.EndInit();
            }
            catch { }

            return pgf;
        }
        private static UnboundColumnType GetUnboundType(int mask)
        {
            UnboundColumnType res = UnboundColumnType.Object;
            switch (mask)
            { 
                case 1:
                    res = UnboundColumnType.Decimal;
                    break;
                case 2:
                    res = UnboundColumnType.DateTime;
                    break;
            }
            return res;
        }
    }
}
