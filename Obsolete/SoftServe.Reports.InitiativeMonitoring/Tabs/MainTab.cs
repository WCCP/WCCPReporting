﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using Softserv.Reports.InitiativeMonitoring.UserControls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraEditors;
using Softserv.Reports.InitiativeMonitoring.Contracts;
using DevExpress.XtraEditors.Controls;

namespace Softserv.Reports.InitiativeMonitoring.Tabs
{
    public class MainTab : XtraTabPage, IPrint
    {
        
        #region Private Properties
        private BaseReportUserControl ReportUserControl { get; set; }
        #endregion Private Properties

        #region Members
        private PivotControl pivotControl = null;
        #endregion

        private GridControl headerGrid = null;
        private GridView headerGridView = null;

        public MainTab(BaseReportUserControl bruc)
        {
            ReportUserControl = bruc;
            SetupInfo();
        }

        #region IPrint Members

        public CompositeLink PrepareCompositeLink()
        {
            System.Windows.Forms.MessageBox.Show("Sorry. Not implemented yet");
            return new CompositeLink();
        }

        #endregion

        internal void SetSourceData(System.Data.DataTable sourceData)
        {
            //SetupInfo();
            pivotControl.SourceData = sourceData;
        }

        private void SetupInfo()
        {
            //TabOperationHelper.CreateSettingSection(this, SheetParams, ref headerGrid, ref headerGridView);
            //Controls[0].Dock = System.Windows.Forms.DockStyle.Top;
            //Controls[0].Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top;
            pivotControl = new PivotControl();
            Controls.Add(pivotControl);
            pivotControl.Dock = System.Windows.Forms.DockStyle.Fill;
            //pivotControl.Top = headerGrid.Bottom;
        }
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            if (null != headerGridView)
                headerGridView.BestFitColumns();
        }

        internal CompositeLink PrepareExport(object sender, XtraTabPage selectedPage, ExportToType type)
        {
            List<IPrintable> components = new List<IPrintable>();
            if (null != headerGrid)
                components.Add(headerGrid);
            components.Add(pivotControl.Component);
            return TabOperationHelper.PrepareCompositeLink(type, "", components, true);
        }

        internal void SetupFieldsInfo(IDictionary<ColumnPlaceType, ListBoxItemCollection> fieldsInfo)
        {
            pivotControl.SetupFieldsInfo(fieldsInfo);
        }
    }
}
