﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Softserv.Reports.InitiativeMonitoring {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Softserv.Reports.InitiativeMonitoring.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Все.
        /// </summary>
        internal static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка выбора параметров.
        /// </summary>
        internal static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неверная дата. Дата начала не может быть позже даты окончания..
        /// </summary>
        internal static string Error_Date {
            get {
                return ResourceManager.GetString("Error_Date", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите Индикатор(ы).
        /// </summary>
        internal static string Error_Indicators {
            get {
                return ResourceManager.GetString("Error_Indicators", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите Инициативу.
        /// </summary>
        internal static string Error_Initiative {
            get {
                return ResourceManager.GetString("Error_Initiative", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите Условие.
        /// </summary>
        internal static string Error_KPI {
            get {
                return ResourceManager.GetString("Error_KPI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите хотя бы один регион.
        /// </summary>
        internal static string Error_Region {
            get {
                return ResourceManager.GetString("Error_Region", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите SKU.
        /// </summary>
        internal static string Error_SKU {
            get {
                return ResourceManager.GetString("Error_SKU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Отчет.
        /// </summary>
        internal static string MainHeader {
            get {
                return ResourceManager.GetString("MainHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initiative Monitoring.
        /// </summary>
        internal static string Report {
            get {
                return ResourceManager.GetString("Report", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Мониторинг Инициатив.
        /// </summary>
        internal static string ReportHeader {
            get {
                return ResourceManager.GetString("ReportHeader", resourceCulture);
            }
        }
    }
}
