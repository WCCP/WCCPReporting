﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softserv.Reports.InitiativeMonitoring.Contracts
{
    internal enum ColumnPlaceType
    {
        undefined = 0,
        filter,
        row,
        column,
        group
    }
    internal class StaffInfoItem
    {
        public int ID { get; set; }
        public int ParentID { get; set; }
        public int ItemID { get; set; }
        public string Name { get; set; }
        public int StaffLevelID { get; set; }
        public StaffInfoItem()
        {
            ItemID = 0;
            Name = string.Empty;
        }
    }
    internal class ProfileItem
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public bool IsSytem { get; set; }
        public ProfileItem(int id, string text, bool isSystem)
        {
            ID = id;
            Text = text;
            IsSytem = isSystem;
        }
        public ProfileItem()
            : this(0, "Все", true)
        {

        }
        public override string ToString()
        {
            return Text;
        }
    }
    internal class FieldItem
    {
        public int ProfileID { get; set; }
        public int ColumnID { get; set; }
        public string ColumnName { get; set; }
        public string ColumnAlias { get; set; }
        public int ColumnWidth { get; set; }
        public int StaffLevelID { get; set; }
        public string DisplayFormat { get; set; }
        public bool IsCalculated { get; set; }
        public string ColumnFormula { get; set; }
        public int DisplayType { get; set; }
        public int ColumnType { get; set; }
        public bool IsVisible { get; set; }
        public bool IsForOneInitiative { get; set; }
        public bool IsIndicator { get; set; }
        public FieldItem()
        {
            ProfileID = 0;
        }
        public override string ToString()
        {
            return ColumnAlias;
        }
    }
    internal class CustomSelectItem
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public CustomSelectItem(int id, string text)
        {
            ID = id;
            Text = text;
        }
        public CustomSelectItem()
            : this(0, "Все")
        {

        }
        public override string ToString()
        {
            return Text;
        }
    }
}
