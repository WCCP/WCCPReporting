﻿namespace Softserve.Reports.InitiativeMonitoring
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.tcOptions = new DevExpress.XtraTab.XtraTabControl();
            this.pgAction = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.lstIndicators = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.spUpliftIncrease = new DevExpress.XtraEditors.SpinEdit();
            this.rbVinDal = new DevExpress.XtraEditors.RadioGroup();
            this.chkSumIn1000 = new DevExpress.XtraEditors.CheckEdit();
            this.cmbProfiles = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.groupOnlyOneInitiative = new DevExpress.XtraEditors.GroupControl();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.chkEquatePromoTT = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.speUpliftTTVH = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.speUpliftTTVW = new DevExpress.XtraEditors.SpinEdit();
            this.speTTVUpliftHight = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.speTTVUpliftLow = new DevExpress.XtraEditors.SpinEdit();
            this.chkUpliftContract = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeclineTTVUplift = new DevExpress.XtraEditors.CheckEdit();
            this.chkVSale = new DevExpress.XtraEditors.CheckEdit();
            this.chkUseOneInitiative = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.lstInitiative = new DevExpress.XtraEditors.ListBoxControl();
            this.groupData1 = new DevExpress.XtraEditors.GroupControl();
            this.dateTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbInitiativeType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.rbDate = new DevExpress.XtraEditors.RadioGroup();
            this.cmbChanelType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateFrom = new DevExpress.XtraEditors.DateEdit();
            this.lbDate = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.chkOnlySelectedRegion = new DevExpress.XtraEditors.CheckEdit();
            this.treeStaff = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.btnColumnDown = new DevExpress.XtraEditors.SimpleButton();
            this.btnColumnUp = new DevExpress.XtraEditors.SimpleButton();
            this.btnRowDown = new DevExpress.XtraEditors.SimpleButton();
            this.btnRowUp = new DevExpress.XtraEditors.SimpleButton();
            this.btnFromColumn = new DevExpress.XtraEditors.SimpleButton();
            this.btnToColumn = new DevExpress.XtraEditors.SimpleButton();
            this.btnFromRow = new DevExpress.XtraEditors.SimpleButton();
            this.btnToRow = new DevExpress.XtraEditors.SimpleButton();
            this.btnFromFilter = new DevExpress.XtraEditors.SimpleButton();
            this.btnToFilter = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.lstColumnFields = new DevExpress.XtraEditors.ListBoxControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.lstRowFields = new DevExpress.XtraEditors.ListBoxControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.lstFilterFields = new DevExpress.XtraEditors.ListBoxControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.lstAllFields = new DevExpress.XtraEditors.ListBoxControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lstProfiles = new DevExpress.XtraEditors.ListBoxControl();
            this.lblWarning = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).BeginInit();
            this.tcOptions.SuspendLayout();
            this.pgAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstIndicators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spUpliftIncrease.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbVinDal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSumIn1000.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProfiles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOnlyOneInitiative)).BeginInit();
            this.groupOnlyOneInitiative.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEquatePromoTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speUpliftTTVH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speUpliftTTVW.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTTVUpliftHight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTTVUpliftLow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUpliftContract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeclineTTVUplift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVSale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseOneInitiative.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstInitiative)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).BeginInit();
            this.groupData1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInitiativeType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbChanelType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlySelectedRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaff)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstColumnFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstRowFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstFilterFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstAllFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstProfiles)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHelp
            // 
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(307, 600);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 10;
            this.btnHelp.Text = "&Справка";
            this.btnHelp.Visible = false;
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(557, 600);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(90, 25);
            this.btnNo.TabIndex = 12;
            this.btnNo.Text = "&Отмена";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(400, 600);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(151, 25);
            this.btnYes.TabIndex = 11;
            this.btnYes.Text = "&Сгенерировать отчет";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // tcOptions
            // 
            this.tcOptions.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.tcOptions.Appearance.Options.UseBackColor = true;
            this.tcOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcOptions.Location = new System.Drawing.Point(0, 0);
            this.tcOptions.Name = "tcOptions";
            this.tcOptions.SelectedTabPage = this.pgAction;
            this.tcOptions.Size = new System.Drawing.Size(654, 594);
            this.tcOptions.TabIndex = 13;
            this.tcOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgAction,
            this.xtraTabPage1});
            this.tcOptions.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tcOptions_SelectedPageChanged);
            // 
            // pgAction
            // 
            this.pgAction.Controls.Add(this.groupControl11);
            this.pgAction.Controls.Add(this.groupControl10);
            this.pgAction.Controls.Add(this.groupOnlyOneInitiative);
            this.pgAction.Controls.Add(this.groupData1);
            this.pgAction.Controls.Add(this.groupControl2);
            this.pgAction.Name = "pgAction";
            this.pgAction.Size = new System.Drawing.Size(647, 565);
            this.pgAction.Text = "Общие параметры";
            // 
            // groupControl11
            // 
            this.groupControl11.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl11.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl11.Appearance.Options.UseBackColor = true;
            this.groupControl11.Controls.Add(this.lstIndicators);
            this.groupControl11.Location = new System.Drawing.Point(341, 152);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(303, 154);
            this.groupControl11.TabIndex = 3;
            this.groupControl11.Text = "Список показателей отчета";
            // 
            // lstIndicators
            // 
            this.lstIndicators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstIndicators.Location = new System.Drawing.Point(2, 22);
            this.lstIndicators.Name = "lstIndicators";
            this.lstIndicators.Size = new System.Drawing.Size(299, 130);
            this.lstIndicators.TabIndex = 0;
            this.lstIndicators.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.lstFields_DrawItem);
            // 
            // groupControl10
            // 
            this.groupControl10.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl10.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl10.Appearance.Options.UseBackColor = true;
            this.groupControl10.Controls.Add(this.spUpliftIncrease);
            this.groupControl10.Controls.Add(this.rbVinDal);
            this.groupControl10.Controls.Add(this.chkSumIn1000);
            this.groupControl10.Controls.Add(this.cmbProfiles);
            this.groupControl10.Controls.Add(this.labelControl8);
            this.groupControl10.Controls.Add(this.labelControl7);
            this.groupControl10.Location = new System.Drawing.Point(341, 3);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(303, 143);
            this.groupControl10.TabIndex = 1;
            this.groupControl10.Text = "Параметры полей отчета";
            // 
            // spUpliftIncrease
            // 
            this.spUpliftIncrease.EditValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spUpliftIncrease.Location = new System.Drawing.Point(237, 118);
            this.spUpliftIncrease.Name = "spUpliftIncrease";
            this.spUpliftIncrease.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spUpliftIncrease.Properties.DisplayFormat.FormatString = "{0:N0}%";
            this.spUpliftIncrease.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spUpliftIncrease.Properties.EditFormat.FormatString = "{0:N0} %";
            this.spUpliftIncrease.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spUpliftIncrease.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spUpliftIncrease.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spUpliftIncrease.Size = new System.Drawing.Size(61, 20);
            this.spUpliftIncrease.TabIndex = 5;
            // 
            // rbVinDal
            // 
            this.rbVinDal.EditValue = 0;
            this.rbVinDal.Location = new System.Drawing.Point(3, 85);
            this.rbVinDal.Name = "rbVinDal";
            this.rbVinDal.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "V считается в дал"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "V считается в гл")});
            this.rbVinDal.Size = new System.Drawing.Size(295, 27);
            this.rbVinDal.TabIndex = 3;
            // 
            // chkSumIn1000
            // 
            this.chkSumIn1000.EditValue = true;
            this.chkSumIn1000.Location = new System.Drawing.Point(2, 62);
            this.chkSumIn1000.Name = "chkSumIn1000";
            this.chkSumIn1000.Properties.Caption = "Выводить суммы в тысячах";
            this.chkSumIn1000.Size = new System.Drawing.Size(171, 19);
            this.chkSumIn1000.TabIndex = 2;
            // 
            // cmbProfiles
            // 
            this.cmbProfiles.Location = new System.Drawing.Point(3, 38);
            this.cmbProfiles.Name = "cmbProfiles";
            this.cmbProfiles.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbProfiles.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbProfiles.Size = new System.Drawing.Size(295, 20);
            this.cmbProfiles.TabIndex = 1;
            this.cmbProfiles.SelectedIndexChanged += new System.EventHandler(this.cmbProfiles_SelectedIndexChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(6, 23);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(223, 13);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Сохраненная комбинация колонок отчета *";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(6, 121);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(228, 13);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "Не брать ТТ для uplift% с приростом > или <";
            // 
            // groupOnlyOneInitiative
            // 
            this.groupOnlyOneInitiative.Appearance.BackColor = System.Drawing.Color.White;
            this.groupOnlyOneInitiative.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupOnlyOneInitiative.Appearance.Options.UseBackColor = true;
            this.groupOnlyOneInitiative.Controls.Add(this.groupControl9);
            this.groupOnlyOneInitiative.Controls.Add(this.chkUseOneInitiative);
            this.groupOnlyOneInitiative.Controls.Add(this.groupControl8);
            this.groupOnlyOneInitiative.Location = new System.Drawing.Point(3, 309);
            this.groupOnlyOneInitiative.Name = "groupOnlyOneInitiative";
            this.groupOnlyOneInitiative.Size = new System.Drawing.Size(640, 256);
            this.groupOnlyOneInitiative.TabIndex = 4;
            this.groupOnlyOneInitiative.Text = " Параметры для отчета по одной инициативе ";
            // 
            // groupControl9
            // 
            this.groupControl9.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl9.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl9.Appearance.Options.UseBackColor = true;
            this.groupControl9.Controls.Add(this.chkEquatePromoTT);
            this.groupControl9.Controls.Add(this.labelControl11);
            this.groupControl9.Controls.Add(this.labelControl10);
            this.groupControl9.Controls.Add(this.speUpliftTTVH);
            this.groupControl9.Controls.Add(this.labelControl4);
            this.groupControl9.Controls.Add(this.labelControl5);
            this.groupControl9.Controls.Add(this.speUpliftTTVW);
            this.groupControl9.Controls.Add(this.speTTVUpliftHight);
            this.groupControl9.Controls.Add(this.labelControl9);
            this.groupControl9.Controls.Add(this.labelControl6);
            this.groupControl9.Controls.Add(this.speTTVUpliftLow);
            this.groupControl9.Controls.Add(this.chkUpliftContract);
            this.groupControl9.Controls.Add(this.chkDeclineTTVUplift);
            this.groupControl9.Controls.Add(this.chkVSale);
            this.groupControl9.Location = new System.Drawing.Point(306, 25);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(332, 227);
            this.groupControl9.TabIndex = 2;
            this.groupControl9.Text = "Параметры расчетных полей:";
            // 
            // chkEquatePromoTT
            // 
            this.chkEquatePromoTT.Location = new System.Drawing.Point(28, 194);
            this.chkEquatePromoTT.Name = "chkEquatePromoTT";
            this.chkEquatePromoTT.Properties.Caption = "равнять емкость промо корзин по контрольным";
            this.chkEquatePromoTT.Size = new System.Drawing.Size(307, 19);
            this.chkEquatePromoTT.TabIndex = 13;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(200, 175);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(129, 13);
            this.labelControl11.TabIndex = 12;
            this.labelControl11.Text = "от кол-ва V в ТТ в группе";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(200, 152);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(68, 13);
            this.labelControl10.TabIndex = 9;
            this.labelControl10.Text = "от max V в ТТ";
            // 
            // speUpliftTTVH
            // 
            this.speUpliftTTVH.EditValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.speUpliftTTVH.Location = new System.Drawing.Point(137, 172);
            this.speUpliftTTVH.Name = "speUpliftTTVH";
            this.speUpliftTTVH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.speUpliftTTVH.Properties.DisplayFormat.FormatString = "{0:N0} %";
            this.speUpliftTTVH.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speUpliftTTVH.Properties.EditFormat.FormatString = "{0:N0} %";
            this.speUpliftTTVH.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speUpliftTTVH.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.speUpliftTTVH.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speUpliftTTVH.Size = new System.Drawing.Size(57, 20);
            this.speUpliftTTVH.TabIndex = 11;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(30, 175);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(106, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Емкость корзины <=";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(30, 152);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(105, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Ширина корзины <=";
            // 
            // speUpliftTTVW
            // 
            this.speUpliftTTVW.EditValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.speUpliftTTVW.Location = new System.Drawing.Point(137, 149);
            this.speUpliftTTVW.Name = "speUpliftTTVW";
            this.speUpliftTTVW.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.speUpliftTTVW.Properties.DisplayFormat.FormatString = "{0:N0} %";
            this.speUpliftTTVW.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speUpliftTTVW.Properties.EditFormat.FormatString = "{0:N0} %";
            this.speUpliftTTVW.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speUpliftTTVW.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.speUpliftTTVW.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speUpliftTTVW.Size = new System.Drawing.Size(57, 20);
            this.speUpliftTTVW.TabIndex = 8;
            this.speUpliftTTVW.EditValueChanged += new System.EventHandler(this.speUpliftTTVW_EditValueChanged);
            // 
            // speTTVUpliftHight
            // 
            this.speTTVUpliftHight.EditValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.speTTVUpliftHight.Location = new System.Drawing.Point(124, 100);
            this.speTTVUpliftHight.Name = "speTTVUpliftHight";
            this.speTTVUpliftHight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.speTTVUpliftHight.Properties.DisplayFormat.FormatString = "{0:N0} %";
            this.speTTVUpliftHight.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speTTVUpliftHight.Properties.EditFormat.FormatString = "{0:N0} %";
            this.speTTVUpliftHight.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speTTVUpliftHight.Size = new System.Drawing.Size(95, 20);
            this.speTTVUpliftHight.TabIndex = 5;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(30, 103);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(91, 13);
            this.labelControl9.TabIndex = 4;
            this.labelControl9.Text = "Верхний предел V";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(30, 80);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(88, 13);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "Нижний предел V";
            // 
            // speTTVUpliftLow
            // 
            this.speTTVUpliftLow.EditValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.speTTVUpliftLow.Location = new System.Drawing.Point(124, 77);
            this.speTTVUpliftLow.Name = "speTTVUpliftLow";
            this.speTTVUpliftLow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.speTTVUpliftLow.Properties.DisplayFormat.FormatString = "{0:N0} %";
            this.speTTVUpliftLow.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speTTVUpliftLow.Properties.EditFormat.FormatString = "{0:N0} %";
            this.speTTVUpliftLow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.speTTVUpliftLow.Size = new System.Drawing.Size(95, 20);
            this.speTTVUpliftLow.TabIndex = 3;
            // 
            // chkUpliftContract
            // 
            this.chkUpliftContract.Location = new System.Drawing.Point(8, 127);
            this.chkUpliftContract.Name = "chkUpliftContract";
            this.chkUpliftContract.Properties.Caption = "Отфильтровать ТТ для uplift-а по корзинам М3";
            this.chkUpliftContract.Size = new System.Drawing.Size(307, 19);
            this.chkUpliftContract.TabIndex = 6;
            this.chkUpliftContract.CheckedChanged += new System.EventHandler(this.chkUpliftContract_CheckedChanged);
            // 
            // chkDeclineTTVUplift
            // 
            this.chkDeclineTTVUplift.Location = new System.Drawing.Point(8, 52);
            this.chkDeclineTTVUplift.Name = "chkDeclineTTVUplift";
            this.chkDeclineTTVUplift.Properties.Caption = "Не брать ТТ вне заданного коридора V для uplift-а";
            this.chkDeclineTTVUplift.Size = new System.Drawing.Size(307, 19);
            this.chkDeclineTTVUplift.TabIndex = 1;
            this.chkDeclineTTVUplift.CheckedChanged += new System.EventHandler(this.chkDeclineTTVUplift_CheckedChanged);
            // 
            // chkVSale
            // 
            this.chkVSale.Location = new System.Drawing.Point(8, 26);
            this.chkVSale.Name = "chkVSale";
            this.chkVSale.Properties.Caption = "Считать целевой V продаж план/факт с условием";
            this.chkVSale.Size = new System.Drawing.Size(314, 19);
            this.chkVSale.TabIndex = 0;
            // 
            // chkUseOneInitiative
            // 
            this.chkUseOneInitiative.EditValue = true;
            this.chkUseOneInitiative.Location = new System.Drawing.Point(5, 25);
            this.chkUseOneInitiative.Name = "chkUseOneInitiative";
            this.chkUseOneInitiative.Properties.Caption = "Выбрать данные по одной инициативе";
            this.chkUseOneInitiative.Size = new System.Drawing.Size(282, 19);
            this.chkUseOneInitiative.TabIndex = 0;
            this.chkUseOneInitiative.EditValueChanged += new System.EventHandler(this.chkUseOneInitiative_EditValueChanged);
            this.chkUseOneInitiative.CheckedChanged += new System.EventHandler(this.chkUseOneInitiative_CheckedChanged);
            // 
            // groupControl8
            // 
            this.groupControl8.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl8.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl8.Appearance.Options.UseBackColor = true;
            this.groupControl8.Controls.Add(this.lstInitiative);
            this.groupControl8.Location = new System.Drawing.Point(2, 46);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(302, 205);
            this.groupControl8.TabIndex = 1;
            this.groupControl8.Text = "Инициативы";
            // 
            // lstInitiative
            // 
            this.lstInitiative.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstInitiative.HorizontalScrollbar = true;
            this.lstInitiative.Location = new System.Drawing.Point(2, 22);
            this.lstInitiative.Name = "lstInitiative";
            this.lstInitiative.Size = new System.Drawing.Size(298, 181);
            this.lstInitiative.TabIndex = 0;
            this.lstInitiative.SelectedIndexChanged += new System.EventHandler(this.lstActivity_SelectedIndexChanged);
            // 
            // groupData1
            // 
            this.groupData1.Controls.Add(this.dateTo);
            this.groupData1.Controls.Add(this.labelControl3);
            this.groupData1.Controls.Add(this.cmbInitiativeType);
            this.groupData1.Controls.Add(this.rbDate);
            this.groupData1.Controls.Add(this.cmbChanelType);
            this.groupData1.Controls.Add(this.labelControl2);
            this.groupData1.Controls.Add(this.labelControl1);
            this.groupData1.Controls.Add(this.dateFrom);
            this.groupData1.Controls.Add(this.lbDate);
            this.groupData1.Location = new System.Drawing.Point(3, 3);
            this.groupData1.Name = "groupData1";
            this.groupData1.Size = new System.Drawing.Size(332, 143);
            this.groupData1.TabIndex = 0;
            this.groupData1.Text = "Инициативы:";
            // 
            // dateTo
            // 
            this.dateTo.EditValue = null;
            this.dateTo.Location = new System.Drawing.Point(169, 113);
            this.dateTo.Name = "dateTo";
            this.dateTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateTo.Properties.ShowToday = false;
            this.dateTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateTo.Size = new System.Drawing.Size(100, 20);
            this.dateTo.TabIndex = 8;
            this.dateTo.TextChanged += new System.EventHandler(this.date_TextChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(150, 116);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(15, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "по ";
            // 
            // cmbInitiativeType
            // 
            this.cmbInitiativeType.Location = new System.Drawing.Point(8, 38);
            this.cmbInitiativeType.Name = "cmbInitiativeType";
            this.cmbInitiativeType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbInitiativeType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbInitiativeType.Size = new System.Drawing.Size(169, 20);
            this.cmbInitiativeType.TabIndex = 1;
            this.cmbInitiativeType.SelectedIndexChanged += new System.EventHandler(this.cmbInitiativeType_SelectedIndexChanged);
            // 
            // rbDate
            // 
            this.rbDate.Location = new System.Drawing.Point(8, 61);
            this.rbDate.Name = "rbDate";
            this.rbDate.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Выбрать текущие инициативы"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Выбрать активные инициативы в период")});
            this.rbDate.Size = new System.Drawing.Size(319, 48);
            this.rbDate.TabIndex = 4;
            this.rbDate.SelectedIndexChanged += new System.EventHandler(this.rbDate_SelectedIndexChanged);
            // 
            // cmbChanelType
            // 
            this.cmbChanelType.Location = new System.Drawing.Point(196, 38);
            this.cmbChanelType.Name = "cmbChanelType";
            this.cmbChanelType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbChanelType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbChanelType.Size = new System.Drawing.Size(126, 20);
            this.cmbChanelType.TabIndex = 3;
            this.cmbChanelType.SelectedIndexChanged += new System.EventHandler(this.cmbChanelType_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(196, 23);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(96, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Канал персонала *";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(8, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(92, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Тип инициативы *";
            // 
            // dateFrom
            // 
            this.dateFrom.EditValue = null;
            this.dateFrom.Location = new System.Drawing.Point(42, 113);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFrom.Properties.ShowToday = false;
            this.dateFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateFrom.Size = new System.Drawing.Size(100, 20);
            this.dateFrom.TabIndex = 6;
            this.dateFrom.TextChanged += new System.EventHandler(this.date_TextChanged);
            // 
            // lbDate
            // 
            this.lbDate.Location = new System.Drawing.Point(28, 116);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(8, 13);
            this.lbDate.TabIndex = 5;
            this.lbDate.Text = "c ";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl2.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl2.Appearance.Options.UseBackColor = true;
            this.groupControl2.Controls.Add(this.chkOnlySelectedRegion);
            this.groupControl2.Controls.Add(this.treeStaff);
            this.groupControl2.Location = new System.Drawing.Point(3, 152);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(332, 154);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Территория и персонал *";
            // 
            // chkOnlySelectedRegion
            // 
            this.chkOnlySelectedRegion.Location = new System.Drawing.Point(6, 134);
            this.chkOnlySelectedRegion.Name = "chkOnlySelectedRegion";
            this.chkOnlySelectedRegion.Properties.Caption = "Проходящие только по выбраной территории";
            this.chkOnlySelectedRegion.Size = new System.Drawing.Size(270, 19);
            this.chkOnlySelectedRegion.TabIndex = 9;
            this.chkOnlySelectedRegion.CheckedChanged += new System.EventHandler(this.chkOnlySelectedRegion_CheckedChanged);
            // 
            // treeStaff
            // 
            this.treeStaff.BestFitVisibleOnly = true;
            this.treeStaff.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeStaff.KeyFieldName = "Id";
            this.treeStaff.Location = new System.Drawing.Point(2, 22);
            this.treeStaff.Name = "treeStaff";
            this.treeStaff.OptionsBehavior.Editable = false;
            this.treeStaff.OptionsMenu.EnableColumnMenu = false;
            this.treeStaff.OptionsMenu.EnableFooterMenu = false;
            this.treeStaff.OptionsView.ShowCheckBoxes = true;
            this.treeStaff.OptionsView.ShowColumns = false;
            this.treeStaff.OptionsView.ShowIndicator = false;
            this.treeStaff.ParentFieldName = "Parent_Id";
            this.treeStaff.Size = new System.Drawing.Size(328, 111);
            this.treeStaff.TabIndex = 6;
            this.treeStaff.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterCheckNode);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "Name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsColumn.AllowSort = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 91;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.btnColumnDown);
            this.xtraTabPage1.Controls.Add(this.btnColumnUp);
            this.xtraTabPage1.Controls.Add(this.btnRowDown);
            this.xtraTabPage1.Controls.Add(this.btnRowUp);
            this.xtraTabPage1.Controls.Add(this.btnFromColumn);
            this.xtraTabPage1.Controls.Add(this.btnToColumn);
            this.xtraTabPage1.Controls.Add(this.btnFromRow);
            this.xtraTabPage1.Controls.Add(this.btnToRow);
            this.xtraTabPage1.Controls.Add(this.btnFromFilter);
            this.xtraTabPage1.Controls.Add(this.btnToFilter);
            this.xtraTabPage1.Controls.Add(this.groupControl7);
            this.xtraTabPage1.Controls.Add(this.groupControl6);
            this.xtraTabPage1.Controls.Add(this.groupControl5);
            this.xtraTabPage1.Controls.Add(this.groupControl4);
            this.xtraTabPage1.Controls.Add(this.groupControl3);
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(647, 565);
            this.xtraTabPage1.Text = "Колонки отчета";
            // 
            // btnColumnDown
            // 
            this.btnColumnDown.Location = new System.Drawing.Point(547, 460);
            this.btnColumnDown.Name = "btnColumnDown";
            this.btnColumnDown.Size = new System.Drawing.Size(53, 23);
            this.btnColumnDown.TabIndex = 15;
            this.btnColumnDown.Text = "Вниз";
            this.btnColumnDown.Click += new System.EventHandler(this.btnColumnDown_Click);
            // 
            // btnColumnUp
            // 
            this.btnColumnUp.Location = new System.Drawing.Point(547, 431);
            this.btnColumnUp.Name = "btnColumnUp";
            this.btnColumnUp.Size = new System.Drawing.Size(53, 23);
            this.btnColumnUp.TabIndex = 14;
            this.btnColumnUp.Text = "Вверх";
            this.btnColumnUp.Click += new System.EventHandler(this.btnColumnUp_Click);
            // 
            // btnRowDown
            // 
            this.btnRowDown.Location = new System.Drawing.Point(547, 343);
            this.btnRowDown.Name = "btnRowDown";
            this.btnRowDown.Size = new System.Drawing.Size(53, 23);
            this.btnRowDown.TabIndex = 10;
            this.btnRowDown.Text = "Вниз";
            this.btnRowDown.Click += new System.EventHandler(this.btnRowDown_Click);
            // 
            // btnRowUp
            // 
            this.btnRowUp.Location = new System.Drawing.Point(547, 314);
            this.btnRowUp.Name = "btnRowUp";
            this.btnRowUp.Size = new System.Drawing.Size(53, 23);
            this.btnRowUp.TabIndex = 9;
            this.btnRowUp.Text = "Вверх";
            this.btnRowUp.Click += new System.EventHandler(this.btnRowUp_Click);
            // 
            // btnFromColumn
            // 
            this.btnFromColumn.Location = new System.Drawing.Point(240, 460);
            this.btnFromColumn.Name = "btnFromColumn";
            this.btnFromColumn.Size = new System.Drawing.Size(53, 23);
            this.btnFromColumn.TabIndex = 12;
            this.btnFromColumn.Text = "<";
            this.btnFromColumn.Click += new System.EventHandler(this.btnFromColumn_Click);
            // 
            // btnToColumn
            // 
            this.btnToColumn.Location = new System.Drawing.Point(240, 431);
            this.btnToColumn.Name = "btnToColumn";
            this.btnToColumn.Size = new System.Drawing.Size(53, 23);
            this.btnToColumn.TabIndex = 11;
            this.btnToColumn.Text = ">";
            this.btnToColumn.Click += new System.EventHandler(this.btnToColumn_Click);
            // 
            // btnFromRow
            // 
            this.btnFromRow.Location = new System.Drawing.Point(240, 343);
            this.btnFromRow.Name = "btnFromRow";
            this.btnFromRow.Size = new System.Drawing.Size(53, 23);
            this.btnFromRow.TabIndex = 7;
            this.btnFromRow.Text = "<";
            this.btnFromRow.Click += new System.EventHandler(this.btnFromRow_Click);
            // 
            // btnToRow
            // 
            this.btnToRow.Location = new System.Drawing.Point(240, 314);
            this.btnToRow.Name = "btnToRow";
            this.btnToRow.Size = new System.Drawing.Size(53, 23);
            this.btnToRow.TabIndex = 6;
            this.btnToRow.Text = ">";
            this.btnToRow.Click += new System.EventHandler(this.btnToRow_Click);
            // 
            // btnFromFilter
            // 
            this.btnFromFilter.Location = new System.Drawing.Point(238, 224);
            this.btnFromFilter.Name = "btnFromFilter";
            this.btnFromFilter.Size = new System.Drawing.Size(53, 23);
            this.btnFromFilter.TabIndex = 4;
            this.btnFromFilter.Text = "<";
            this.btnFromFilter.Click += new System.EventHandler(this.btnFromFilter_Click);
            // 
            // btnToFilter
            // 
            this.btnToFilter.Location = new System.Drawing.Point(238, 195);
            this.btnToFilter.Name = "btnToFilter";
            this.btnToFilter.Size = new System.Drawing.Size(53, 23);
            this.btnToFilter.TabIndex = 3;
            this.btnToFilter.Text = ">";
            this.btnToFilter.Click += new System.EventHandler(this.btnToFilter_Click);
            // 
            // groupControl7
            // 
            this.groupControl7.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl7.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl7.Appearance.Options.UseBackColor = true;
            this.groupControl7.Controls.Add(this.lstColumnFields);
            this.groupControl7.Location = new System.Drawing.Point(310, 394);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(220, 110);
            this.groupControl7.TabIndex = 13;
            this.groupControl7.Text = "Список колонок в таблице отчета:";
            // 
            // lstColumnFields
            // 
            this.lstColumnFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstColumnFields.Location = new System.Drawing.Point(2, 22);
            this.lstColumnFields.Name = "lstColumnFields";
            this.lstColumnFields.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstColumnFields.Size = new System.Drawing.Size(216, 86);
            this.lstColumnFields.TabIndex = 0;
            this.lstColumnFields.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.lstFields_DrawItem);
            this.lstColumnFields.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstFields_MouseClick);
            this.lstColumnFields.SelectedIndexChanged += new System.EventHandler(this.lstColumnFields_SelectedIndexChanged);
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl6.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl6.Appearance.Options.UseBackColor = true;
            this.groupControl6.Controls.Add(this.lstRowFields);
            this.groupControl6.Location = new System.Drawing.Point(310, 275);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(220, 110);
            this.groupControl6.TabIndex = 8;
            this.groupControl6.Text = "Список строк в таблице отчета:";
            // 
            // lstRowFields
            // 
            this.lstRowFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstRowFields.Location = new System.Drawing.Point(2, 22);
            this.lstRowFields.Name = "lstRowFields";
            this.lstRowFields.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstRowFields.Size = new System.Drawing.Size(216, 86);
            this.lstRowFields.TabIndex = 0;
            this.lstRowFields.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.lstFields_DrawItem);
            this.lstRowFields.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstFields_MouseClick);
            this.lstRowFields.SelectedIndexChanged += new System.EventHandler(this.lstRowFields_SelectedIndexChanged);
            // 
            // groupControl5
            // 
            this.groupControl5.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl5.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl5.Appearance.Options.UseBackColor = true;
            this.groupControl5.Controls.Add(this.lstFilterFields);
            this.groupControl5.Location = new System.Drawing.Point(310, 154);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(220, 110);
            this.groupControl5.TabIndex = 5;
            this.groupControl5.Text = "Список полей в области фильтра:";
            // 
            // lstFilterFields
            // 
            this.lstFilterFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstFilterFields.Location = new System.Drawing.Point(2, 22);
            this.lstFilterFields.Name = "lstFilterFields";
            this.lstFilterFields.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstFilterFields.Size = new System.Drawing.Size(216, 86);
            this.lstFilterFields.TabIndex = 0;
            this.lstFilterFields.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.lstFields_DrawItem);
            this.lstFilterFields.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstFields_MouseClick);
            this.lstFilterFields.SelectedIndexChanged += new System.EventHandler(this.lstFilterFields_SelectedIndexChanged);
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl4.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl4.Appearance.Options.UseBackColor = true;
            this.groupControl4.Controls.Add(this.lstAllFields);
            this.groupControl4.Location = new System.Drawing.Point(5, 154);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(220, 350);
            this.groupControl4.TabIndex = 2;
            this.groupControl4.Text = "Список доступных колонок для отчета:";
            // 
            // lstAllFields
            // 
            this.lstAllFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstAllFields.Location = new System.Drawing.Point(2, 22);
            this.lstAllFields.Name = "lstAllFields";
            this.lstAllFields.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstAllFields.Size = new System.Drawing.Size(216, 326);
            this.lstAllFields.TabIndex = 0;
            this.lstAllFields.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstFields_MouseClick);
            this.lstAllFields.SelectedIndexChanged += new System.EventHandler(this.lstAllFields_SelectedIndexChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl3.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl3.Appearance.Options.UseBackColor = true;
            this.groupControl3.Controls.Add(this.memoEdit1);
            this.groupControl3.Location = new System.Drawing.Point(310, 3);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(316, 145);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "Коментарий:";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.EditValue = resources.GetString("memoEdit1.EditValue");
            this.memoEdit1.Location = new System.Drawing.Point(2, 22);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit1.Size = new System.Drawing.Size(312, 121);
            this.memoEdit1.TabIndex = 1;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Controls.Add(this.lstProfiles);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(301, 145);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Сохраненные комбинации колонок:";
            // 
            // lstProfiles
            // 
            this.lstProfiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstProfiles.Location = new System.Drawing.Point(2, 22);
            this.lstProfiles.Name = "lstProfiles";
            this.lstProfiles.Size = new System.Drawing.Size(297, 121);
            this.lstProfiles.TabIndex = 1;
            this.lstProfiles.SelectedIndexChanged += new System.EventHandler(this.listPresetsFields_SelectedIndexChanged);
            // 
            // lblWarning
            // 
            this.lblWarning.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Appearance.Options.UseForeColor = true;
            this.lblWarning.Location = new System.Drawing.Point(332, 4);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(315, 13);
            this.lblWarning.TabIndex = 2;
            this.lblWarning.Text = "При текущих параметрах отчета есть недоступные колонки!";
            this.lblWarning.Visible = false;
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(654, 632);
            this.Controls.Add(this.tcOptions);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(662, 659);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(662, 659);
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры отчёта - Мониторинг инициатив";
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).EndInit();
            this.tcOptions.ResumeLayout(false);
            this.pgAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstIndicators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spUpliftIncrease.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbVinDal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSumIn1000.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProfiles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOnlyOneInitiative)).EndInit();
            this.groupOnlyOneInitiative.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEquatePromoTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speUpliftTTVH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speUpliftTTVW.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTTVUpliftHight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTTVUpliftLow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUpliftContract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeclineTTVUplift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVSale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseOneInitiative.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstInitiative)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).EndInit();
            this.groupData1.ResumeLayout(false);
            this.groupData1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInitiativeType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbChanelType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlySelectedRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaff)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstColumnFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstRowFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstFilterFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstAllFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstProfiles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraTab.XtraTabControl tcOptions;
        private DevExpress.XtraTab.XtraTabPage pgAction;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.GroupControl groupData1;
        private DevExpress.XtraEditors.DateEdit dateFrom;
        private DevExpress.XtraEditors.LabelControl lbDate;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.ListBoxControl lstColumnFields;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.ListBoxControl lstRowFields;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.ListBoxControl lstFilterFields;
        private DevExpress.XtraEditors.ListBoxControl lstAllFields;
        private DevExpress.XtraEditors.ListBoxControl lstProfiles;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SimpleButton btnToFilter;
        private DevExpress.XtraEditors.SimpleButton btnFromColumn;
        private DevExpress.XtraEditors.SimpleButton btnToColumn;
        private DevExpress.XtraEditors.SimpleButton btnFromRow;
        private DevExpress.XtraEditors.SimpleButton btnToRow;
        private DevExpress.XtraEditors.SimpleButton btnFromFilter;
        private DevExpress.XtraEditors.SimpleButton btnRowDown;
        private DevExpress.XtraEditors.SimpleButton btnRowUp;
        private DevExpress.XtraEditors.SimpleButton btnColumnDown;
        private DevExpress.XtraEditors.SimpleButton btnColumnUp;
        private DevExpress.XtraEditors.RadioGroup rbDate;
        private DevExpress.XtraEditors.ComboBoxEdit cmbChanelType;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbInitiativeType;
        private DevExpress.XtraEditors.DateEdit dateTo;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.CheckEdit chkVSale;
        private DevExpress.XtraEditors.CheckEdit chkUpliftContract;
        private DevExpress.XtraEditors.CheckEdit chkDeclineTTVUplift;
        private DevExpress.XtraEditors.CheckEdit chkUseOneInitiative;
        private DevExpress.XtraEditors.ListBoxControl lstInitiative;
        private DevExpress.XtraTreeList.TreeList treeStaff;
        private DevExpress.XtraEditors.SpinEdit speTTVUpliftLow;
        private DevExpress.XtraEditors.GroupControl groupOnlyOneInitiative;
        private DevExpress.XtraEditors.SpinEdit spUpliftIncrease;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.RadioGroup rbVinDal;
        private DevExpress.XtraEditors.CheckEdit chkSumIn1000;
        private DevExpress.XtraEditors.ComboBoxEdit cmbProfiles;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SpinEdit speTTVUpliftHight;
        private DevExpress.XtraEditors.SpinEdit speUpliftTTVH;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SpinEdit speUpliftTTVW;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.CheckEdit chkOnlySelectedRegion;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.LabelControl lblWarning;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private DevExpress.XtraEditors.CheckedListBoxControl lstIndicators;
        private DevExpress.XtraEditors.CheckEdit chkEquatePromoTT;
    }
}