﻿using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.DataAccess;
using Softserv.Reports.InitiativeMonitoring.Constants;
using System.Data;
using System.Data.SqlClient;
using Softserv.Reports.InitiativeMonitoring.Contracts;

namespace Softserv.Reports.InitiativeMonitoring.DataProvider
{
    internal static class DataAccessProvider
    {
        private static String BuildInStatement(int[] values)
        {
            String res = String.Empty;

            for (int i = 0; i < values.Length; i++)
            {
                res += values[i].ToString();
                res += ",";
            }
            res = res.TrimEnd(",".ToCharArray());

            if (!String.IsNullOrEmpty(res))
                res = String.Format("({0})", res);

            return res;
        }

        private static String BuildInStatement(String value)
        {
            String res = String.Empty;

            if (!String.IsNullOrEmpty(value))
                res = String.Format("({0})", value);

            return res;
        }

        private static SqlParameter[] GetInStatementAsParameters(String paramName, SqlDbType paramType, int[] values)
        {
            SqlParameter[] spParams = new SqlParameter[1];
            SqlParameter param = new SqlParameter()
            {
                ParameterName = paramName,
                SqlValue = BuildInStatement(values),
                SqlDbType = paramType
            };
            spParams[0] = param;
            return spParams;
        }

        private static SqlParameter[] BuildFullParameters(/*ReportSettings reportSettings,*/ bool needStep)
        {
            int count = 0;
            SqlParameter sqlParam = null;
            SqlParameter[] sqlParams = null;
/*
            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.DateTime,
                Value = reportSettings.DateStartPeriod1.ToString("yyyy-MM-dd"),
                ParameterName = SQLConstants.PERIOD1_BEGIN
            };
            sqlParams[count++] = sqlParam;
*/
            return sqlParams;
        }

        internal static void OpenConnection()
        {
            DataAccessLayer.OpenConnection();
        }

        internal static void CloseConnection()
        {
            DataAccessLayer.CloseConnection();
        }
        /*
        internal static DataTable GetRegions()
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_REGIONS, new SqlParameter[] { });

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];

            return res;
        }
        */
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static DataTable GetSKUs()
        {
            return GetDataTable(SQLConstants.SP_DW_RPTC_GET_SKU);
        }

        /// <summary>
        /// exec spDW_RPTC_GetSKU
        /// Викликаєтсья без параметрів
        /// </summary>
        /// <returns>Повертає ID та назви всіх SKU</returns>
        internal static DataTable GetSKUsTreeInfo()
        {
            return GetDataTable(SQLConstants.SP_DW_RPTC_GET_SKU_TREE);
        }

        internal static DataTable GetChanelTypeList()
        {
            return GetDataTable(SQLConstants.SP_DW_CHANNEL_LIST);
        }

        internal static DataTable GetInitiativeTypes()
        {
            return GetDataTable(SQLConstants.SP_DW_MI_GET_INITIATIVE_TYPE_LIST);
        }

        internal static DataTable GetReportProfileList(int reportID)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            SqlParameter param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_REPORT_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = reportID
            };
            parameters[0] = param;
            return GetDataTable(SQLConstants.SP_DW_GET_REPORT_PROFILE_LIST, parameters);
        }

        internal static DataTable GetTerritoryPersonnelTree(int chanellID)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            SqlParameter param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_CHANNEL_TYPE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = chanellID
            };
            parameters[0] = param;
            return GetDataTable(SQLConstants.SP_DW_MI_GET_TERRITORY_PERSONNEL_TREE, parameters);
        }

        internal static DataTable GetInitiativeKPITree(int initiativeID)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            SqlParameter param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_INITIATIVE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = initiativeID
            };
            parameters[0] = param;
            return GetDataTable(SQLConstants.SP_DW_MI_GET_INITIATIVE_KPI_TREE, parameters);
        }

        internal static DataTable GetInitiativeSKUTree(int initiativeID)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            SqlParameter param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_INITIATIVE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = initiativeID
            };
            parameters[0] = param;
            return GetDataTable(SQLConstants.SP_DW_MI_GET_INITIATIVE_SKU_TREE, parameters);
        }

        internal static DataTable GetReportColumnList(int reportID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter
            {
                ParameterName = SQLConstants.PAR_REPORT_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = reportID
            });
            parameters.Add(new SqlParameter
            {
                ParameterName = SQLConstants.PAR_IS_INDICATOR,
                SqlDbType = SqlDbType.Bit,
                SqlValue = 0
            });

            return GetDataTable(SQLConstants.SP_DW_GET_REPORT_COLUMN_LIST, parameters.ToArray());
        }

        internal static DataTable GetReportProfileColumnList(int profileID, ColumnPlaceType? columnPlaceType, bool? isIndicator)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add( new SqlParameter
            {
                ParameterName = SQLConstants.PAR_PROFILE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = profileID
            });

            if (null != columnPlaceType)
            {
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_COLUMN_TYPE,
                    SqlDbType = SqlDbType.Int,
                    SqlValue = columnPlaceType
                });
            }
            if (null != isIndicator)
            {
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_IS_INDICATOR,
                    SqlDbType = SqlDbType.Bit,
                    SqlValue = isIndicator
                });
            }
            return GetDataTable(SQLConstants.SP_DW_GET_REPORT_PROFILE_COLUMN_LIST, parameters.ToArray());
        }

        private static DataTable GetDataTable(string spName)
        {
            return GetDataTable(spName, new SqlParameter[] { });
        }

        private static DataTable GetDataTable(string spName, SqlParameter[] sqlParams)
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(spName, sqlParams);

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];

            return res;
        }

        internal static DataTable GetInitiativeList(int initiativeTypeID, int channelTypeID, DateTime? dateStart, DateTime? dateEnd, bool isOnlyTerritory, string M4IdList, string M3IdList, string M2IdList)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            SqlParameter param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_INITIATIVE_TYPE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = initiativeTypeID
            };
            parameters.Add(param);
            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_CHANNEL_TYPE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = channelTypeID
            };
            parameters.Add(param);
            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_DATE_FROM,
                SqlDbType = SqlDbType.DateTime,
                SqlValue = dateStart
            };
            parameters.Add(param);
            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_DATE_TO,
                SqlDbType = SqlDbType.DateTime,
                SqlValue = dateEnd
            };
            parameters.Add(param);
            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_IS_ONLY_TERRITORY,
                SqlDbType = SqlDbType.Bit,
                SqlValue = isOnlyTerritory
            };
            parameters.Add(param);
            //if (!String.IsNullOrEmpty(M4IdList))
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_M4ID_LIST,
                    SqlDbType = SqlDbType.VarChar,
                    SqlValue = M4IdList
                };
                parameters.Add(param);
            }
            //if (!String.IsNullOrEmpty(M3IdList))
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_M3ID_LIST,
                    SqlDbType = SqlDbType.VarChar,
                    SqlValue = M3IdList
                };
                parameters.Add(param);
            }
            //if (!String.IsNullOrEmpty(M2IdList))
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_M2ID_LIST,
                    SqlDbType = SqlDbType.VarChar,
                    SqlValue = M2IdList
                };
                parameters.Add(param);
            }
            return GetDataTable(SQLConstants.SP_DW_MI_GET_INITIATIVE_LIST, parameters.ToArray());
        }

        internal static DataTable GetReportData(ReportSettings rs)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            SqlParameter param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_INITIATIVE_TYPE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = rs.initiativeTypeID
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_CHANNEL_TYPE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = rs.channelTypeID
            };
            parameters.Add(param);

            if (null != rs.dateFrom)
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_DATE_FROM,
                    SqlDbType = SqlDbType.DateTime,
                    SqlValue = rs.dateFrom
                };
                parameters.Add(param);
            }
            if (null != rs.dateTo)
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_DATE_TO,
                    SqlDbType = SqlDbType.DateTime,
                    SqlValue = rs.dateTo
                };
                parameters.Add(param);
            }

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_IS_ONLY_TERRITORY,
                SqlDbType = SqlDbType.Bit,
                SqlValue = rs.isOnlyTerritory
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_M4ID_LIST,
                SqlDbType = SqlDbType.VarChar,
                SqlValue = rs.M4Id_List
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_M3ID_LIST,
                SqlDbType = SqlDbType.VarChar,
                SqlValue = rs.M3Id_List
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_M2ID_LIST,
                SqlDbType = SqlDbType.VarChar,
                SqlValue = rs.M2Id_List
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_VIN_DAL,
                SqlDbType = SqlDbType.Bit,
                SqlValue = rs.VinDal
            };
            parameters.Add(param);

            parameters.Add(new SqlParameter
            {
                ParameterName = SQLConstants.PAR_UPLIFT_GAIN,
                SqlDbType = SqlDbType.Int,
                SqlValue = rs.UpliftGain
            });

            if (null != rs.initiativeID)
            {
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_INITIATIVE_ID,
                    SqlDbType = SqlDbType.Int,
                    SqlValue = rs.initiativeID
                });

                //bit = 0 -- считать целевой V продаж план/факт условием  
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_TARGET_VAS_CONDITION,
                    SqlDbType = SqlDbType.Bit,
                    SqlValue = rs.TargetVasCondition
                });

                // не брать ТТ вне заданного коридора V для uplift-а:  
                //int = null        -- нижний предел V в %  
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_VMIN_HALL,
                    SqlDbType = SqlDbType.Int,
                    SqlValue = rs.VminHall
                });
                //int = null        -- верхний предел V в %  
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_VMAX_HALL,
                    SqlDbType = SqlDbType.Int,
                    SqlValue = rs.VmaxHall
                });
                // отфильтровать ТТ для uplift-а по корзинам М3:  
                //int = null     -- ширина корзины <= @basketWidth % от max V в ТТ  
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_BASKET_WIDTH,
                    SqlDbType = SqlDbType.Int,
                    SqlValue = rs.BasketWidth
                });
                //int = null       -- емкость корзины <= @basketWidth % от кол-ва ТТ в группе  
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_BASKET_VOL,
                    SqlDbType = SqlDbType.Int,
                    SqlValue = rs.BasketVol
                });
                //bit = null   -- равнять емкость промо корзин по контрольным  
                parameters.Add(new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_EQUATE_PROMO_TT,
                    SqlDbType = SqlDbType.Bit,
                    SqlValue = rs.EquatePromoTT
                });
            }

            parameters.Add(new SqlParameter
            {
                ParameterName = SQLConstants.PAR_INDICATOR_ID_LIST,
                SqlDbType = SqlDbType.VarChar,
                SqlValue = rs.indicatorsIDs
            });

            return GetDataTable(SQLConstants.SP_DW_MI_REPORT_MAIN, parameters.ToArray());
        }
        
        internal static DataTable GetReportData(int initiativeTypeID, int channelTypeID, DateTime? dateFrom, DateTime? dateTo, bool isOnlyTerritory,
            string M4Id_List, string M3Id_List, string M2Id_List,
            int? initiativeID, string SKU_List, string KPI_List)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            SqlParameter param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_INITIATIVE_TYPE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = initiativeTypeID
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_CHANNEL_TYPE_ID,
                SqlDbType = SqlDbType.Int,
                SqlValue = channelTypeID
            };
            parameters.Add(param);

            if (null != dateFrom)
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_DATE_FROM,
                    SqlDbType = SqlDbType.DateTime,
                    SqlValue = dateFrom
                };
                parameters.Add(param);
            }
            if (null != dateTo)
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_DATE_TO,
                    SqlDbType = SqlDbType.DateTime,
                    SqlValue = dateTo
                };
                parameters.Add(param);
            }

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_IS_ONLY_TERRITORY,
                SqlDbType = SqlDbType.Bit,
                SqlValue = isOnlyTerritory
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_M4ID_LIST,
                SqlDbType = SqlDbType.VarChar,
                SqlValue = M4Id_List
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_M3ID_LIST,
                SqlDbType = SqlDbType.VarChar,
                SqlValue = M3Id_List
            };
            parameters.Add(param);

            param = new SqlParameter
            {
                ParameterName = SQLConstants.PAR_M2ID_LIST,
                SqlDbType = SqlDbType.VarChar,
                SqlValue = M2Id_List
            };
            parameters.Add(param);

            if (null != initiativeID)
            {
                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_INITIATIVE_ID,
                    SqlDbType = SqlDbType.Int,
                    SqlValue = initiativeID
                };
                parameters.Add(param);

                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_SKU_LIST,
                    SqlDbType = SqlDbType.VarChar,
                    SqlValue = SKU_List
                };
                parameters.Add(param);

                param = new SqlParameter
                {
                    ParameterName = SQLConstants.PAR_KPI_LIST,
                    SqlDbType = SqlDbType.VarChar,
                    SqlValue = KPI_List
                };
                parameters.Add(param);
            }

            return GetDataTable(SQLConstants.SP_DW_MI_REPORT_MAIN, parameters.ToArray());
        }
    }
}
