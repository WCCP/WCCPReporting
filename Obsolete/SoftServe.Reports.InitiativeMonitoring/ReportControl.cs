﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Softserve.Reports.InitiativeMonitoring;
using Logica.Reports.Common;
using Softserv.Reports.InitiativeMonitoring.Tabs;
using Softserv.Reports.InitiativeMonitoring.Constants;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using DevExpress.XtraPrintingLinks;
using Softserv.Reports.InitiativeMonitoring.DataProvider;
using DevExpress.XtraEditors.Controls;
using Softserv.Reports.InitiativeMonitoring.Contracts;
using Logica.Reports.Common.WaitWindow;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        SettingsForm settingsForm = null;
        public WccpUIControl()
            : base(WCCPAPI.GetReportId())
        {
            InitializeComponent();
            SetExportType(ExportToType.Pdf, true);
            SetExportType(ExportToType.Rtf, false);
            this.MenuButtonsRendering += new MenuButtonsRenderingHandler(WccpUIControl_MenuButtonsRendering);

            settingsForm = new SettingsForm(this);
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                ShowReport();
            }
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
            this.PrepareExport += new PrepareExportMenuClickHandler(WccpUIControl_PrepareExport);
            this.RefreshClick += new MenuClickHandler(WccpUIControl_RefreshClick);
        }

        void WccpUIControl_MenuButtonsRendering(object sender, BaseReportUserControl.MenuButtonsRenderingEventArgs eventArgs)
        {
            eventArgs.ShowExportAllBtn = false;
            eventArgs.ShowPrintAllBtn = false;
        }

        void WccpUIControl_RefreshClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            ShowReport();
        }

        CompositeLink WccpUIControl_PrepareExport(object sender, DevExpress.XtraTab.XtraTabPage selectedPage, ExportToType type)
        {
            CompositeLink result = null;
            MainTab tab = selectedPage as MainTab;
            if (null != tab)
                result = tab.PrepareExport(sender, selectedPage, type);
            return result;
        }

        void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
                ShowReport();
        }
        
        private void ShowReport()
        {
            WaitManager.StartWait();
            while (tabManager.TabPages.Count > 0)
                tabManager.TabPages.RemoveAt(0);
            DataAccessLayer.OpenConnection();
            {
                DataTable sourceData = null;
                ReportSettings rs = settingsForm.ReportParameters;
                
                // old code
                /*
                sourceData = DataAccessProvider.GetReportData(rs.initiativeTypeID, rs.channelTypeID, rs.dateFrom, rs.dateTo,
                    rs.isOnlyTerritory, rs.M4Id_List, rs.M3Id_List, rs.M2Id_List,
                    rs.initiativeID, rs.SKU_List, rs.KPI_List);
                */
                sourceData = DataAccessProvider.GetReportData(rs);

                MainTab tab = new MainTab(this);

                IDictionary<ColumnPlaceType, ListBoxItemCollection> fieldsInfo = settingsForm.GetFieldsInfo();

                tab.SetupFieldsInfo(fieldsInfo);
                tab.Text = rs.ReportName;
                tabManager.TabPages.Add(tab);
                tab.SetSourceData(sourceData);
            }
            DataAccessLayer.CloseConnection();
            WaitManager.StopWait();
        }
    }
}
