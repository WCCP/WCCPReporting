﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraEditors;
using Logica.Reports.DataAccess;
using DevExpress.XtraEditors.Controls;
//using Logica.Reports.ConfigXmlParser.Model;
using System.Data.SqlClient;
using Logica.Reports.ConfigXmlParser.Model;
using Softserv.Reports.ORM1M2PriceActivity;
using Softserv.Reports.ORM1M2PriceActivity.Constants;

namespace Softserve.Reports.ORM1M2PriceActivity
{
    public partial class SettingsForm : XtraForm
    {
        DataTable dtMerch = null;
        DataTable dtSuper = null;
        DataTable dataTableDate = null;
        BaseReportUserControl bruc;
        public SettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            bruc = parent;
            dtMerch = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_MERCHANDISERS_GETLIST, new SqlParameter[] { new SqlParameter(SQLConstants.REPORT_ID, 100/*bruc.ReportId*/) }).Tables[0];
            dtSuper = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_SUPERVISORS_GETLIST, new SqlParameter[] { new SqlParameter(SQLConstants.REPORT_ID, 100/*bruc.ReportId*/) }).Tables[0];
            FillM2();
            //FillM1(null);
            txtReportName.Text = Resource.ReportHeader; //Softserve.Reports.ORM1M2PriceActivity.
            dateEdit.DateTime = DateTime.Now;
        }

        private void FillM2()
        {
            cbM2.Properties.Items.Clear();
            //cbM2.Properties.Items.Add(Resource.All);
            foreach (DataRow dr in dtSuper.Rows)
            {
                cbM2.Properties.Items.Add(new ComboBoxItem() { Id = dr[SQLConstants.F_SUPERVISOR_ID], Text = dr[SQLConstants.F_SUPERVISOR_NAME].ToString() });
            }
            if (cbM2.Properties.Items.Count > 0)
                cbM2.SelectedIndex = 0;
            else
                FillM1(null);
        }

        private void FillM1(object m2)
        {
            cbM1.Properties.Items.Clear();
            foreach (DataRow dr in dtMerch.Rows)
            {
                if (null == m2 || m2.Equals(dr[SQLConstants.F_SUPERVISOR_ID]))
                    cbM1.Properties.Items.Add(dr[SQLConstants.F_MERCH_ID], dr[SQLConstants.F_MERCHNAME].ToString(), CheckState.Checked, true);
            }
        }

        private void cbM2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbM2.SelectedItem is ComboBoxItem)
                FillM1((cbM2.SelectedItem as ComboBoxItem).Id);
            else
                FillM1(null);
        }

        DateTime dtStart, dtEnd;
        public List<SheetParamCollection> SheetParamsList
        {
            get 
            {
                List<SheetParamCollection> list = new List<SheetParamCollection>();
                // Fill params for M2
                SheetParamCollection paramCollection = new SheetParamCollection();
                
                ComboBoxItem cbi = (ComboBoxItem)(cbM2.Properties.Items[cbM2.SelectedIndex]);

                paramCollection.MainHeader = String.Format(Resource.MainHeader, cbi.Text);
                paramCollection.TabId = Guid.NewGuid();
                paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.M2_PARAM, Value = cbi.Id, DisplayParamName = Resource.M2, DisplayValue = cbi.Text });
                paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.DATE_PARAM, Value = dateEdit.DateTime, DisplayParamName = Resource.ReportDay, DisplayValue = dateEdit.DateTime.ToShortDateString() });
                
                list.Add(paramCollection);

                // Fill params for M1
                foreach (CheckedListBoxItem clbi in cbM1.Properties.Items)
                {
                    if (clbi.CheckState == CheckState.Checked)
                    {
                        list.Add(FillParamCol(Guid.NewGuid(), clbi));
                    }
                }
                return list;
            }
        }
        private SheetParamCollection FillParamCol(Guid id, CheckedListBoxItem merch)
        {
            SheetParamCollection paramCollection = new SheetParamCollection();
            paramCollection.MainHeader = String.Format(Resource.MainHeader, merch.Description);
            paramCollection.TabId = id;

            paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.M1_PARAM, Value = merch.Value, DisplayParamName = Resource.M1, DisplayValue = merch.Description });
            paramCollection.Add(new SheetParam() { SqlParamName = SQLConstants.DATE_PARAM, Value = dateEdit.DateTime, DisplayParamName = Resource.ReportDay, DisplayValue = dateEdit.DateTime.ToShortDateString() });
            //paramCollection.Add(new SheetParam() { SqlParamName = "@Supervisor_Id", Value = null });

            return paramCollection;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            bool selected = false;

            if (cbM2.SelectedIndex == -1)
            {
                XtraMessageBox.Show(Resource.Error_M2);
                return;
            }

            foreach (CheckedListBoxItem clbi in cbM1.Properties.Items)
            {
                if (clbi.CheckState == CheckState.Checked)
                {
                    selected = true;
                    break;
                }
            }
            if (!selected)
                XtraMessageBox.Show(Resource.Error_M1);
            else
                DialogResult = DialogResult.OK;
        }
    }

    internal class ComboBoxItem
    {
        object id;
        public object Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
