﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using Softserv.Reports.ORM1M2PriceActivity.UserControls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraEditors;

namespace Softserv.Reports.ORM1M2PriceActivity.Tabs
{
    public class M21Tab : XtraTabPage, IPrint
    {
        public enum TabType
        {
            M1 = 1,
            M2
        }
        
        #region Private Properties
        private BaseReportUserControl ReportUserControl { get; set; }
        private TabType ViewType { get; set; }
        private SheetParamCollection SheetParams { get; set; }
        #endregion Private Properties

        #region Members
        private PivotControl pivotControl = null;
        #endregion

        private GridControl headerGrid = null;
        private GridView headerGridView = null;
        //private SplitContainerControl 

        public M21Tab(BaseReportUserControl bruc, TabType tabType, SheetParamCollection sheetParams)
        {
            SheetParams = sheetParams;
            ReportUserControl = bruc;
            ViewType = tabType;
            this.SizeChanged += new EventHandler(M21Tab_SizeChanged);
        }

        #region IPrint Members

        public CompositeLink PrepareCompositeLink()
        {
            System.Windows.Forms.MessageBox.Show("Sorry. Not implemented yet");
            return new CompositeLink();
        }

        #endregion

        internal void SetSourceData(System.Data.DataTable sourceData)
        {
            SetupInfo();
            pivotControl.SourceData = sourceData;
        }

        private void SetupInfo()
        {
            TabOperationHelper.CreateSettingSection(this, SheetParams, ref headerGrid, ref headerGridView);
            Controls[0].Dock = System.Windows.Forms.DockStyle.Top;
            Controls[0].Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top;
            pivotControl = new PivotControl(ViewType == TabType.M2);
            Controls.Add(pivotControl);
            pivotControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            pivotControl.Top = headerGrid.Bottom;
            //pivotControl.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom;
            if (null != headerGridView)
                headerGridView.BestFitColumns();
        }
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            if (null != headerGridView)
                headerGridView.BestFitColumns();
            base.OnPaint(e);
            if (null != headerGridView)
                headerGridView.BestFitColumns();
        }

        void M21Tab_SizeChanged(object sender, EventArgs e)
        {
            if (null != headerGridView)
                headerGridView.BestFitColumns();
        }

        internal CompositeLink PrepareExport(object sender, XtraTabPage selectedPage, ExportToType type)
        {
            CompositeLink result = null;
            List<IPrintable> components = new List<IPrintable>();
            if (null != headerGrid)
            {
                headerGridView.BestFitColumns();
                components.Add(headerGrid);
            }
            components.Add(pivotControl.Component);
            pivotControl.SetExportNames(true);
            result = TabOperationHelper.PrepareCompositeLink(type, "", components, true);
            pivotControl.SetExportNames(false);
            return result;
        }
    }
}
