﻿namespace Softserv.Reports.ORM1M2PriceActivity.UserControls
{
    partial class PivotControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField8 = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.Appearance.Header.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.pivotGridField4.Appearance.Header.Options.UseTextOptions = true;
            this.pivotGridField4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridField4.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridField4.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField4.AreaIndex = 0;
            this.pivotGridField4.Caption = "Активность";
            this.pivotGridField4.ColumnValueLineCount = 5;
            this.pivotGridField4.FieldName = "Action_Name";
            this.pivotGridField4.Name = "pivotGridField4";
            this.pivotGridField4.Options.ShowGrandTotal = false;
            this.pivotGridField4.Options.ShowInCustomizationForm = false;
            this.pivotGridField4.Options.ShowTotals = false;
            this.pivotGridField4.RowValueLineCount = 5;
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Appearance.ColumnHeaderArea.Options.UseTextOptions = true;
            this.pivotGridControl1.Appearance.ColumnHeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.Appearance.ColumnHeaderArea.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridControl1.Appearance.ColumnHeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridControl1.Appearance.DataHeaderArea.Options.UseTextOptions = true;
            this.pivotGridControl1.Appearance.DataHeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.Appearance.DataHeaderArea.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridControl1.Appearance.DataHeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridControl1.Appearance.FieldHeader.Options.UseTextOptions = true;
            this.pivotGridControl1.Appearance.FieldHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.Appearance.FieldHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridControl1.Appearance.HeaderArea.Options.UseTextOptions = true;
            this.pivotGridControl1.Appearance.HeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.Appearance.HeaderArea.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridControl1.Appearance.HeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridControl1.AppearancePrint.FieldHeader.Options.UseTextOptions = true;
            this.pivotGridControl1.AppearancePrint.FieldHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridControl1.AppearancePrint.FieldHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridControl1.AppearancePrint.FieldHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField1,
            this.pivotGridField2,
            this.pivotGridField3,
            this.pivotGridField4,
            this.pivotGridField5,
            this.pivotGridField6,
            this.pivotGridField7,
            this.pivotGridField8});
            pivotGridGroup1.Fields.Add(this.pivotGridField4);
            pivotGridGroup1.Hierarchy = null;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.AllowFilter = false;
            this.pivotGridControl1.OptionsDataField.ColumnValueLineCount = 5;
            this.pivotGridControl1.OptionsDataField.RowValueLineCount = 5;
            this.pivotGridControl1.OptionsPrint.UsePrintAppearance = true;
            this.pivotGridControl1.OptionsView.ShowColumnGrandTotals = false;
            this.pivotGridControl1.OptionsView.ShowColumnHeaders = false;
            this.pivotGridControl1.OptionsView.ShowColumnTotals = false;
            this.pivotGridControl1.OptionsView.ShowDataHeaders = false;
            this.pivotGridControl1.OptionsView.ShowFilterHeaders = false;
            this.pivotGridControl1.OptionsView.ShowGrandTotalHeaderIfNoColumnFields = false;
            this.pivotGridControl1.OptionsView.ShowGrandTotalHeaderIfNoRowFields = false;
            this.pivotGridControl1.Size = new System.Drawing.Size(1134, 669);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.CustomExportFieldValue += new System.EventHandler<DevExpress.XtraPivotGrid.CustomExportFieldValueEventArgs>(this.pivotGridControl1_CustomExportFieldValue);
            this.pivotGridControl1.CustomCellDisplayText += new DevExpress.XtraPivotGrid.PivotCellDisplayTextEventHandler(this.pivotGridControl1_CustomCellDisplayText);
            this.pivotGridControl1.CustomDrawFieldValue += new DevExpress.XtraPivotGrid.PivotCustomDrawFieldValueEventHandler(this.pivotGridControl1_CustomDrawFieldValue);
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Appearance.Header.Options.UseTextOptions = true;
            this.pivotGridField1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridField1.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "Код ТТ";
            this.pivotGridField1.ColumnValueLineCount = 5;
            this.pivotGridField1.FieldName = "OL_ID";
            this.pivotGridField1.MinWidth = 90;
            this.pivotGridField1.Name = "pivotGridField1";
            this.pivotGridField1.Options.ShowGrandTotal = false;
            this.pivotGridField1.Width = 90;
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Appearance.Header.Options.UseTextOptions = true;
            this.pivotGridField2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridField2.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField2.AreaIndex = 1;
            this.pivotGridField2.Caption = "Факт имя";
            this.pivotGridField2.ColumnValueLineCount = 5;
            this.pivotGridField2.FieldName = "OLTradingName";
            this.pivotGridField2.MinWidth = 100;
            this.pivotGridField2.Name = "pivotGridField2";
            this.pivotGridField2.Width = 250;
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.Appearance.Header.Options.UseTextOptions = true;
            this.pivotGridField3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridField3.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField3.AreaIndex = 2;
            this.pivotGridField3.Caption = "Факт адрес";
            this.pivotGridField3.ColumnValueLineCount = 5;
            this.pivotGridField3.FieldName = "OLAddress";
            this.pivotGridField3.MinWidth = 100;
            this.pivotGridField3.Name = "pivotGridField3";
            this.pivotGridField3.Width = 250;
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.Appearance.Header.Options.UseTextOptions = true;
            this.pivotGridField5.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridField5.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridField5.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridField5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField5.AreaIndex = 0;
            this.pivotGridField5.Caption = "Нужно подключить";
            this.pivotGridField5.ColumnValueLineCount = 5;
            this.pivotGridField5.FieldName = "NeedToAttachCount";
            this.pivotGridField5.MinWidth = 45;
            this.pivotGridField5.Name = "pivotGridField5";
            this.pivotGridField5.RowValueLineCount = 5;
            this.pivotGridField5.Width = 135;
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.Appearance.Header.Options.UseTextOptions = true;
            this.pivotGridField6.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridField6.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridField6.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridField6.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField6.AreaIndex = 1;
            this.pivotGridField6.Caption = "Участники акции";
            this.pivotGridField6.ColumnValueLineCount = 5;
            this.pivotGridField6.FieldName = "ParticipantCount";
            this.pivotGridField6.MinWidth = 45;
            this.pivotGridField6.Name = "pivotGridField6";
            this.pivotGridField6.RowValueLineCount = 5;
            this.pivotGridField6.Width = 135;
            // 
            // pivotGridField7
            // 
            this.pivotGridField7.Appearance.Header.Options.UseTextOptions = true;
            this.pivotGridField7.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridField7.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridField7.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridField7.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField7.AreaIndex = 2;
            this.pivotGridField7.Caption = "ТТ с нарушениями";
            this.pivotGridField7.ColumnValueLineCount = 5;
            this.pivotGridField7.FieldName = "ViolationCount";
            this.pivotGridField7.MinWidth = 45;
            this.pivotGridField7.Name = "pivotGridField7";
            this.pivotGridField7.RowValueLineCount = 5;
            this.pivotGridField7.Width = 135;
            // 
            // pivotGridField8
            // 
            this.pivotGridField8.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField8.AreaIndex = 3;
            this.pivotGridField8.ColumnValueLineCount = 5;
            this.pivotGridField8.FieldName = ".";
            this.pivotGridField8.MinWidth = 1;
            this.pivotGridField8.Name = "pivotGridField8";
            this.pivotGridField8.RowValueLineCount = 5;
            this.pivotGridField8.Width = 1;
            // 
            // PivotControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pivotGridControl1);
            this.Name = "PivotControl";
            this.Size = new System.Drawing.Size(1134, 669);
            this.Resize += new System.EventHandler(this.PivotControl_Resize);
            this.SizeChanged += new System.EventHandler(this.PivotControl_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField8;
    }
}
