﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;

namespace Softserv.Reports.ORM1M2PriceActivity.UserControls
{
    public partial class PivotControl : UserControl
    {
        private const string GRAND_TOTAL = @"Grand Total";
        private const string ACTION_NAME = @"Action_Name";
        private const string NEED_TO_ATTACH_COUNT = @"NeedToAttachCount";
        private const string PARTICIPANT_COUNT = @"ParticipantCount";
        private const string VIOLATION_COUNT = @"ViolationCount";
        private int YShift = 0;
        private class ExportInfo
        {
            public bool ActionNameExported = false;
            public bool NeedToAttachExported = false;
            public bool ParticipantExported = false;
            public bool ViolationExported = false;
            public void Reset()
            {
                ActionNameExported = false;
                NeedToAttachExported = false;
                ParticipantExported = false;
                ViolationExported = false;
            }
        }

        private ExportInfo exportInfo = new ExportInfo();
        public DataTable SourceData
        {
            set
            {
                pivotGridControl1.DataSource = value;
            }
        }

        internal PivotGridControl Component
        {
            get 
            {
                exportInfo.Reset();
                return pivotGridControl1; 
            }
        }

        public PivotControl(bool isM2View)
        {
            InitializeComponent();

            if (isM2View)
            {
                pivotGridField1.Visible = false;
                pivotGridField2.Caption = "ТП";
                pivotGridField2.FieldName = "MerchName";
                pivotGridField3.Visible = false;
            }
            //pivotGridField8.Visible = false;
            //return;
            pivotGridControl1.AllowDrop = false;
            pivotGridControl1.OptionsCustomization.AllowDrag = false;
            pivotGridControl1.OptionsCustomization.AllowEdit = false;
            pivotGridControl1.OptionsCustomization.AllowFilter = false;
            pivotGridControl1.OptionsCustomization.AllowHideFields = DevExpress.XtraPivotGrid.AllowHideFieldsType.Never;
            pivotGridControl1.OptionsCustomization.AllowSort = false;
            pivotGridControl1.OptionsCustomization.AllowSortBySummary = false;
            pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Simple;
            pivotGridControl1.OptionsView.ShowGrandTotalHeaderIfNoColumnFields = false;
            pivotGridControl1.OptionsView.ShowGrandTotalHeaderIfNoRowFields = false;

            pivotGridControl1.OptionsFilterPopup.AllowContextMenu = false;
            pivotGridControl1.OptionsMenu.EnableFieldValueMenu = false;
            pivotGridControl1.OptionsMenu.EnableHeaderAreaMenu = false;
            pivotGridControl1.OptionsMenu.EnableHeaderMenu = false;
            
            //pivotGridControl1.Appearance.HeaderArea.HAlignment // read-only
            /*
            pivotGridControl1.Appearance.HeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            pivotGridControl1.Appearance.HeaderArea.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            pivotGridControl1.Appearance.HeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            */
            //pivotGridControl1.OptionsPrint.
        }

        private void pivotGridControl1_CustomDrawFieldValue(object sender, DevExpress.XtraPivotGrid.PivotCustomDrawFieldValueEventArgs e)
        {
            e.Handled = false;
            if (e.Field != null && (e.Field.FieldName.Equals(ACTION_NAME) || e.Field.FieldName.Equals(NEED_TO_ATTACH_COUNT) || e.Field.FieldName.Equals(PARTICIPANT_COUNT) || e.Field.FieldName.Equals(VIOLATION_COUNT) || e.DisplayText.Equals(GRAND_TOTAL)))
            {
                int fWidth = 45;
                int fHeight = 55;

                Rectangle r = e.Info.CaptionRect;

                e.Info.HeaderPosition = DevExpress.Utils.Drawing.HeaderPositionKind.Center;

                StringFormat sf = new StringFormat();
                sf.Trimming = StringTrimming.Word;//EllipsisCharacter;

                e.Field.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                if (e.Field.FieldName.Equals(ACTION_NAME) || e.Field.FieldName.Equals(GRAND_TOTAL))
                {
                    e.Field.MinWidth = e.Field.Width = fWidth * 3;
                    r.Height = fHeight;
                    r.Width = fWidth * 3;
                    r.Y -= 3;
                    YShift = r.Y + r.Height;
                }
                else
                {
                    fHeight = 88;
                    r.Height = fHeight;
                    r.Width = e.Field.MinWidth = e.Field.Width = fWidth;
                    sf.FormatFlags |= StringFormatFlags.DirectionVertical | StringFormatFlags.DirectionRightToLeft;
                    r.Y = YShift;
                }
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;


                System.Drawing.Drawing2D.GraphicsState state = e.Graphics.Save();

                r.X -= 8;
                e.Graphics.DrawString(e.Info.Caption, e.Appearance.Font, e.Appearance.GetForeBrush(e.GraphicsCache), r, sf);
                e.Graphics.DrawRectangle(e.Appearance.GetForePen(e.GraphicsCache), r);
                e.Graphics.Restore(state);
                e.Handled = true;
            }
            else if (e.DisplayText.Equals(GRAND_TOTAL))
            {
                int fWidth = 45;
                int fHeight = 55;

                Rectangle r = e.Info.CaptionRect;
                if (r.X < 20)
                    return;
                e.Info.HeaderPosition = DevExpress.Utils.Drawing.HeaderPositionKind.Center;

                StringFormat sf = new StringFormat();
                sf.Trimming = StringTrimming.Word;//EllipsisCharacter;

                r.Height = fHeight;
                r.Width = fWidth * 3;
                r.Y -= 3;
                YShift = r.Y + r.Height;

                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;


                System.Drawing.Drawing2D.GraphicsState state = e.Graphics.Save();

                r.X -= 8;
                e.Graphics.DrawString(e.Info.Caption, e.Appearance.Font, e.Appearance.GetForeBrush(e.GraphicsCache), r, sf);
                e.Graphics.DrawRectangle(e.Appearance.GetForePen(e.GraphicsCache), r);
                e.Graphics.Restore(state);
                e.Handled = true;
            }
        }

        private void pivotGridControl1_CustomExportFieldValue(object sender, CustomExportFieldValueEventArgs e)
        {
            if (e.Brick == null)
                return;
            if (e.Field != null && (/*e.Field.FieldName.Equals(ACTION_NAME) || */e.Field.FieldName.Equals(NEED_TO_ATTACH_COUNT) || e.Field.FieldName.Equals(PARTICIPANT_COUNT) || e.Field.FieldName.Equals(VIOLATION_COUNT) || e.Field.FieldName.Equals(".")))
            {
                /*
                if (e.Field.FieldName.Equals(ACTION_NAME))
                {
                    return;
                    if (exportInfo.ActionNameExported)
                        return;
                    exportInfo.ActionNameExported = true;
                }
                else if (e.Field.FieldName.Equals(NEED_TO_ATTACH_COUNT))
                {
                    if (exportInfo.NeedToAttachExported)
                        return;
                    exportInfo.NeedToAttachExported = true;
                } 
                else if (e.Field.FieldName.Equals(PARTICIPANT_COUNT))
                {
                    if (exportInfo.ParticipantExported)
                        return;
                    exportInfo.ParticipantExported = true;
                }
                else if (e.Field.FieldName.Equals(VIOLATION_COUNT))
                {
                    if (exportInfo.ViolationExported)
                        return;
                    exportInfo.ViolationExported = true;
                }
                */
                StringFormatFlags sff = StringFormatFlags.DirectionVertical | StringFormatFlags.DirectionRightToLeft | StringFormatFlags.DisplayFormatControl;
                if (e.Field.FieldName.Equals(ACTION_NAME))
                {
                    sff = StringFormatFlags.DisplayFormatControl;// (StringFormatFlags)0;
                    return;
                }

                //e.Field.Appearance.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;

                StringFormat sf = new StringFormat();
                sf.Trimming = StringTrimming.Word;

                //e.Brick.TextValueFormatString
                //e.Field.Caption = GetColumnText(e.Field);

                //sf.FormatFlags |= StringFormatFlags.DirectionVertical;// | StringFormatFlags.DirectionRightToLeft; 
                
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;

                e.Brick.Style.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(sf);
            }
            else if (e.Text.Equals(GRAND_TOTAL))
            {
                if (e.Brick.Rect.X < 20)
                    return;
                //e.Brick.Style.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                DevExpress.XtraPrinting.BrickStringFormat bsf = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center, StringAlignment.Center,
                    (StringFormatFlags)0,
                    System.Drawing.Text.HotkeyPrefix.None,
                    StringTrimming.Word);
                e.Brick.Style.StringFormat = bsf;
            }
            else if (e.Field.FieldName.Equals(ACTION_NAME))
            {
                //e.Appearance.H
                StringFormat sf = new StringFormat();
                sf.Trimming = StringTrimming.Word;

                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                //sf.FormatFlags |= StringFormatFlags.DirectionVertical;// | StringFormatFlags.DirectionRightToLeft; 
                //e.Brick.TextValueFormatString.
                e.Brick.Style.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(sf);
            }
            else
                e.Brick.Style = DevExpress.XtraPrinting.BrickStyle.CreateDefault();
        }

        private string GetColumnText(PivotGridField pivotGridField)
        {
            string res = string.Empty;
            string tmp = string.Empty;
            if (pivotGridField.FieldName.Equals(NEED_TO_ATTACH_COUNT))
            {
                tmp = "Подключить";
            }
            else if (pivotGridField.FieldName.Equals(PARTICIPANT_COUNT))
            {
                tmp = "Участники";
            }
            else if (pivotGridField.FieldName.Equals(VIOLATION_COUNT))
            {
                tmp = "С нарушениями";
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tmp.Length; i++)
            {
                sb.Append(tmp.Substring(i, 1));
                sb.Append("\n");
            }
            res = sb.ToString();
            return res;
        }

        private void pivotGridControl1_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            if (e.DataField != null && (e.DataField.FieldName.Equals(NEED_TO_ATTACH_COUNT) || e.DataField.FieldName.Equals(PARTICIPANT_COUNT) || e.DataField.FieldName.Equals(VIOLATION_COUNT))) //  || e.DataField.FieldName.Equals(GRAND_TOTAL))
            {
                if (e.DisplayText.Equals("0"))
                {
                    e.DisplayText = string.Empty;
                }
            }
        }

        private void PivotControl_SizeChanged(object sender, EventArgs e)
        {
            string s = string.Empty;
            int newHeight = this.Parent.Size.Height - 39;
            if (newHeight < 1)
                newHeight = 1;
            this.Size = new Size(this.Parent.Size.Width, newHeight);
        }

        private void PivotControl_Resize(object sender, EventArgs e)
        {
            string s = string.Empty;
        }

        internal void SetExportNames(bool useExportNames)
        {
            if (!useExportNames)
            {
                this.pivotGridField5.Caption = "Нужно подключить";
                this.pivotGridField6.Caption = "Участники акции";
                this.pivotGridField7.Caption = "ТТ с нарушениями";
            }
            else
            {
                this.pivotGridField5.Width = 45;
                this.pivotGridField6.Width = 45;
                this.pivotGridField7.Width = 45;

                StringBuilder sb = new StringBuilder();
                sb.Append("Под-");
                sb.Append("\n");
                sb.Append("клю-");
                sb.Append("\n");
                sb.Append("чить");
                this.pivotGridField5.Caption = sb.ToString();

                sb = new StringBuilder();
                sb.Append("Участ-");
                sb.Append("\n");
                sb.Append("ники");
                this.pivotGridField6.Caption = sb.ToString();

                sb = new StringBuilder();
                sb.Append("С");
                sb.Append("\n");
                sb.Append("нару-");
                sb.Append("\n");
                sb.Append("шени-");
                sb.Append("\n");
                sb.Append("ями");
                this.pivotGridField7.Caption = sb.ToString();
            }
        }
    }
}
