﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softserv.Reports.ORM1M2PriceActivity.Constants
{
    internal static class SQLConstants
    {
        // SP Names
        internal const string SP_DW_MERCHANDISERS_GETLIST = @"spDW_MerchandisersGetList";
        internal const string SP_DW_SUPERVISORS_GETLIST = @"spDW_SupervisorsGetList";

        internal const string SP_DW_M1_OPERATIONAL_PRICING_ACTIVITY = @"spDW_M1_OperationalPricingActivity";
        internal const string SP_DW_M2_OPERATIONAL_PRICING_ACTIVITY = @"spDW_M2_OperationalPricingActivity";

        // SP Params
        internal const string DATE_PARAM = @"@Date";
        internal const string M2_PARAM = @"@supervisorId";
        internal const string M1_PARAM = @"@Merch_ID";
        internal const string REPORT_ID = @"ReportId";

        // Field Names
        internal const string F_SUPERVISOR_ID = @"Supervisor_id";
        internal const string F_SUPERVISOR_NAME = @"Supervisor_name";
        internal const string F_MERCH_ID = @"Merch_id";
        internal const string F_MERCHNAME = @"MerchName";
    }
}
