﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Softserve.Reports.ORM1M2PriceActivity;
using Logica.Reports.Common;
using Softserv.Reports.ORM1M2PriceActivity.Tabs;
using Softserv.Reports.ORM1M2PriceActivity.Constants;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        SettingsForm setForm;
        public WccpUIControl()
            : base(WCCPAPI.GetReportId())
        {
            InitializeComponent();
            SetExportType(ExportToType.Pdf, true);
            SetExportType(ExportToType.Rtf, false);
            setForm = new SettingsForm(this);
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                ShowReport(setForm.SheetParamsList);
            }
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
            this.PrepareExport += new PrepareExportMenuClickHandler(WccpUIControl_PrepareExport);
            this.RefreshClick += new MenuClickHandler(WccpUIControl_RefreshClick);
        }

        void WccpUIControl_RefreshClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            ShowReport(setForm.SheetParamsList);
        }

        CompositeLink WccpUIControl_PrepareExport(object sender, DevExpress.XtraTab.XtraTabPage selectedPage, ExportToType type)
        {
            CompositeLink result = null;
            M21Tab tab = selectedPage as M21Tab;
            if (null != tab)
                result = tab.PrepareExport(sender, selectedPage, type);
            return result;
        }

        void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
                ShowReport(setForm.SheetParamsList);
        }
        
        private void ShowReport(List<SheetParamCollection> list)
        {
            while (tabManager.TabPages.Count > 0)
                tabManager.TabPages.RemoveAt(0);
            DataAccessLayer.OpenConnection();
            for (int i = 0; i < list.Count; i++)
            {
                SheetParamCollection sheetParams = list[i];
                bool isM2 = sheetParams.Exists(par => par.SqlParamName.Equals(SQLConstants.M2_PARAM));

                string execSP = string.Empty;
                SqlParameter[] sqlParams = new SqlParameter[2];
                SqlParameter sqlParam = new SqlParameter();

                SheetParam sheetParam = sheetParams.Find(par => par.SqlParamName.Equals(SQLConstants.DATE_PARAM));
                if (null != sheetParam)
                {
                    sqlParam.SqlDbType = SqlDbType.DateTime;
                    sqlParam.Value = sheetParam.Value;
                    sqlParam.ParameterName = sheetParam.SqlParamName;
                }
                sqlParams[0] = sqlParam;

                sqlParam = new SqlParameter();
                string tabName = string.Empty;
                if (isM2)
                {
                    execSP = SQLConstants.SP_DW_M2_OPERATIONAL_PRICING_ACTIVITY;
                    sheetParam = sheetParams.Find(par => par.SqlParamName.Equals(SQLConstants.M2_PARAM));
                    tabName = string.Format("M2: {0}", sheetParam.DisplayValue.ToString());
                }
                else
                {
                    execSP = SQLConstants.SP_DW_M1_OPERATIONAL_PRICING_ACTIVITY;
                    sheetParam = sheetParams.Find(par => par.SqlParamName.Equals(SQLConstants.M1_PARAM));
                    tabName = string.Format("M1: {0}", sheetParam.DisplayValue.ToString());
                }
                sqlParam.Value = sheetParam.Value;
                sqlParam.SqlDbType = SqlDbType.Int;
                sqlParam.ParameterName = sheetParam.SqlParamName;
                sqlParams[1] = sqlParam;

                DataTable sourceData = null;
                
                sourceData = DataAccessLayer.ExecuteStoredProcedure(execSP, sqlParams).Tables[0];

                M21Tab tab = new M21Tab(this, (isM2) ? M21Tab.TabType.M2 : M21Tab.TabType.M1, sheetParams);
                tab.Text = tabName;
                tabManager.TabPages.Add(tab);
                tab.SetSourceData(sourceData);
            }
            DataAccessLayer.CloseConnection();
        }
    }
}
