﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using WccpReporting.CAPEXInFieldReport;
using DevExpress.XtraTab;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        private SettingsForm settingsForm;

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl()
            : base(Constants.REPORT_ID)
        {
            InitializeComponent();

            settingsForm = new SettingsForm(this.Report);

            if ((settingsForm.FormDisabled && settingsForm.ValidateData()) || settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
            }
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
        }

        /// <summary>
        /// WCCPs the UI control_ settings form click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
            }
        }

        /// <summary>
        /// Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            UpdateSheets(settingsForm.ListSheetParams);
            RefreshControls();
        }

        /// <summary>
        /// Refreshes the controls.
        /// </summary>
        void RefreshControls()
        {
            foreach (XtraTabPage tab in this.tabManager.TabPages)
            {
                if (tab is BaseTab) 
                {
                    (tab as BaseTab).AllowMultiSelect = true;
                    (tab as BaseTab).EnableAppearenceFocusedCell = false;
                }

                if (tab.Controls.Count == 2)
                {
                    Control grid = tab.Controls[0];
                    Control label = tab.Controls[1];

                    grid.Dock = DockStyle.Fill;
                    label.Dock = DockStyle.Fill;

                    tab.Controls.Remove(label);
                    tab.Controls.Remove(grid);

                    SplitContainer sc = new SplitContainer();
                    sc.Dock = DockStyle.Fill;
                    sc.Orientation = Orientation.Horizontal;
                    sc.Panel1MinSize = 19;
                    sc.SplitterDistance = 19;
                    sc.SizeChanged += (x, y) => { sc.SplitterDistance = 19; };
                    sc.IsSplitterFixed = true;
                    sc.SplitterWidth = 1;

                    sc.Panel1.Controls.Add(label);
                    sc.Panel2.Controls.Add(grid);

                    tab.Controls.Add(sc);
                }
            }
        }
    }
}
