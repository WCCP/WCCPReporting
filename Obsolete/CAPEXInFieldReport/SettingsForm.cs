using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using WccpReporting.CAPEXInFieldReport.DataProvider;
using WccpReporting.POCMappingProgress.DataProvider;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.Model;

namespace WccpReporting.CAPEXInFieldReport
{
    public partial class SettingsForm : DevExpress.XtraEditors.XtraForm
    {
        public bool FormDisabled { get; set; }
        private List<SheetParamCollection> listSheetParams = new List<SheetParamCollection>();
        private Report report;
        private string selectedRegion;
        private string selectedRegionName;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsForm"/> class.
        /// </summary>
        /// <param name="report">The report.</param>
        public SettingsForm(Report report)
        {
            this.report = report;

            InitializeComponent();
            BuildSettings();
        }

        /// <summary>
        /// Gets the list sheet params.
        /// </summary>
        /// <value>The list sheet params.</value>
        public List<SheetParamCollection> ListSheetParams
        {
            get { BuildSheetSettings(); return listSheetParams; }
        }

        /// <summary>
        /// Validates the data.
        /// </summary>
        /// <returns>True if data is valid otherwise false.</returns>
        public bool ValidateData()
        {
            selectedRegionName = chkBoxRegion.Text;
            selectedRegion = chkBoxRegion.EditValue.ToString();

            return !string.IsNullOrEmpty(selectedRegion);
        }

        private void BuildSettings()
        {
            this.Text = Resources.ParamsFormCaption;
            this.labelRegion.Text = Resources.Region;
            this.btnYes.Text = Resources.Start;
            this.btnCancel.Text = Resources.Cancel;

            // Select data for Region
            this.chkBoxRegion.Properties.DataSource = DataAccessProvider.GetRegions();
            this.chkBoxRegion.Properties.DisplayMember = SqlConstants.FldRegionName;
            this.chkBoxRegion.Properties.ValueMember = SqlConstants.FldRegionId;
            
            CheckedListBoxItemCollection items = this.chkBoxRegion.Properties.GetItems();
            foreach (CheckedListBoxItem item in items)
            {
                item.CheckState = CheckState.Checked;
            }
            if (items.Count == 1)
            {
                items[0].Enabled = false;
                FormDisabled = true;
            }

            SetBtnEnabling();
        }

        /// <summary>
        /// Builds the sheet settings.
        /// </summary>
        /// <param name="report">The report.</param>
        private void BuildSheetSettings()
        {
            if (report != null)
            {
                listSheetParams.Clear();

                foreach (Tab tab in report.Tabs)
                {
                    listSheetParams.Add(GetParamCollection(tab.Id));
                }
            }
        }

        /// <summary>
        /// Gets the param collection.
        /// </summary>
        /// <param name="tabId">The tab id.</param>
        /// <returns></returns>
        private SheetParamCollection GetParamCollection(Guid tabId)
        {
            SheetParamCollection parameterCollection = new SheetParamCollection();

            parameterCollection.TabId = tabId;
            parameterCollection.TableDataType = TableType.Fact;

            parameterCollection.Add(new SheetParam()
            {
                SqlParamName = SqlConstants.ParamRegions
                ,
                DisplayParamName = Resources.Region
                ,
                DisplayValue = this.selectedRegionName
                ,
                Value = selectedRegion
            });
            return parameterCollection;
        }

        private void SetBtnEnabling() 
        {
            this.btnYes.Enabled = this.chkBoxRegion.EditValue != null && !string.IsNullOrEmpty(this.chkBoxRegion.EditValue.ToString());
        }

        private void btnYes_Click(Object sender, EventArgs e)
        {
            if (ValidateData())
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void chkBoxRegion_EditValueChanged(object sender, EventArgs e)
        {
            this.SetBtnEnabling();
        }
    }
}