namespace WccpReporting.CAPEXInFieldReport
{
    partial class SettingsForm : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.chkBoxRegion = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.labelRegion = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // chkBoxRegion
            // 
            this.chkBoxRegion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBoxRegion.Location = new System.Drawing.Point(12, 35);
            this.chkBoxRegion.Name = "chkBoxRegion";
            this.chkBoxRegion.Size = new System.Drawing.Size(256, 20);
            this.chkBoxRegion.TabIndex = 0;
            this.chkBoxRegion.EditValueChanged += new System.EventHandler(this.chkBoxRegion_EditValueChanged);
            // 
            // labelRegion
            // 
            this.labelRegion.Location = new System.Drawing.Point(12, 12);
            this.labelRegion.Name = "labelRegion";
            this.labelRegion.Size = new System.Drawing.Size(35, 13);
            this.labelRegion.TabIndex = 3;
            this.labelRegion.Text = "������";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.ImageList = this.imCollection;
            this.btnCancel.Location = new System.Drawing.Point(178, 78);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 25);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "������";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "check_24.png");
            this.imCollection.Images.SetKeyName(1, "close_24.png");
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 0;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(82, 78);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(90, 25);
            this.btnYes.TabIndex = 17;
            this.btnYes.Text = "���������";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 115);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.labelRegion);
            this.Controls.Add(this.chkBoxRegion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������";
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelRegion;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkBoxRegion;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.Utils.ImageCollection imCollection;
    }
}