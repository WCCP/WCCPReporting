﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.POCMappingProgress.DataProvider
{
    public class SqlConstants
    {
        // SP names
        public const string ProcGetRegion = "spDW_GetUserAccesibleRegions";
        public const string ProcReport = "spDW_CAPEXInFieldsReport";

        // SP Params
        public const string ParamRegions = "@sRegions";

        // Field names
        public const string FldRegionName = "NAME";
        public const string FldRegionId = "ID";
    }
}
