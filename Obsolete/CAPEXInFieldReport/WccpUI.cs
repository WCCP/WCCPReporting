﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.Common;
using WccpReporting.CAPEXInFieldReport;

namespace WccpReporting
{
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int isSkined, string reportCaption)
        {
            try
            {
                return WCCPAPI.CreateTabEx(Resources.Dislocation) > 0 ? 1 : 0;
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            };
            return 0;
        }

        public void CloseUI()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
