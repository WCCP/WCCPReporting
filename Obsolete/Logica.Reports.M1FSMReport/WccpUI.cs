﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.M1FSMReport;
using Logica.Reports.Common;

namespace WccpReporting
{
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int isSkined, string reportCaption)
        {
            try
            {
                return WCCPAPI.CreateTabEx(Resource.FSMReport) > 0 ? 1 : 0;
                //if (0 == WCCPAPI.SearchTab(Resource.SVReport))
                //    return WCCPAPI.CreateTab(Resource.SVReport) > 0 ? 1 : 0;
                //else
                //    MessageBox.Show(Resource.ReportExists, Resource.Information, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            };
            return 0;
        }

        public void CloseUI()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
