﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.M1FSMReport;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        M1FSMSettingsForm setForm;
        public WccpUIControl() : base(100)
        {
            InitializeComponent();
            setForm = new M1FSMSettingsForm(this);
            if (setForm.ShowDialog() == DialogResult.OK)
                UpdateSheets(setForm.SheetParamsList);
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
        }

        void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
                UpdateSheets(setForm.SheetParamsList);
        }
    }
}
