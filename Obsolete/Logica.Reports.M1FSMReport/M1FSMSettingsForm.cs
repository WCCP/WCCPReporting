﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraEditors;
using Logica.Reports.DataAccess;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.ConfigXmlParser.Model;
using System.Data.SqlClient;

namespace Logica.Reports.M1FSMReport
{
    
    public partial class M1FSMSettingsForm : XtraForm
    {
        DataTable dtMerch;
        DataTable dtSuper;
        DataTable dataTableDate;
        BaseReportUserControl bruc;
        public M1FSMSettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            bruc = parent;
            dtMerch = DataAccessLayer.ExecuteStoredProcedure("spDW_MerchandisersGetList", new SqlParameter[] { new SqlParameter("@ReportId", bruc.ReportId) }).Tables[0];
            dtSuper = DataAccessLayer.ExecuteStoredProcedure("spDW_SupervisorsGetList", new SqlParameter[] { new SqlParameter("@ReportId", bruc.ReportId) }).Tables[0];
            FillM2();
            FillM1(null);
            txtReportName.Text = Resource.FSMReportHeader;
            dateEdit.DateTime = DateTime.Now;
        }

        private void FillM2()
        {
            cbM2.Properties.Items.Clear();
            cbM2.Properties.Items.Add(Resource.All);
            foreach (DataRow dr in dtSuper.Rows)
                cbM2.Properties.Items.Add(new ComboBoxItem() { Id = dr["Supervisor_id"], Text = dr["Supervisor_name"].ToString() });
        }
        private void FillM1(object m2)
        {
            cbM1.Properties.Items.Clear();
            foreach (DataRow dr in dtMerch.Rows)
                if (null == m2 || m2.Equals(dr["Supervisor_ID"]))
                    cbM1.Properties.Items.Add(dr["Merch_id"],dr["MerchName"].ToString(),CheckState.Unchecked, true);
        }

        private void cbM2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbM2.SelectedItem is ComboBoxItem)
                FillM1((cbM2.SelectedItem as ComboBoxItem).Id);
            else
                FillM1(null);
        }

        DateTime dtStart, dtEnd;
        public List<SheetParamCollection> SheetParamsList
        {
            get 
            {
                if (rbType.SelectedIndex == 0)
                {
                    dtStart = new DateTime(dateEdit.DateTime.Year, dateEdit.DateTime.Month, 1);
                    dtEnd = dtStart.AddMonths(1).AddMilliseconds(-1);
                    //dataTableDate = DataAccessLayer.ExecuteStoredProcedure("spDW_FSM_WorkingDays", new SqlParameter[] { new SqlParameter("@Date", dtStart) }).Tables[0];
                }
                else
                {
                    dataTableDate = DataAccessLayer.ExecuteStoredProcedure("spDW_WorkingDays", new SqlParameter[] { new SqlParameter("@Date", dateEdit.DateTime) }).Tables[0];
                }

                List<SheetParamCollection> list = new List<SheetParamCollection>();
                foreach (CheckedListBoxItem clbi in cbM1.Properties.Items)
                {
                    if (clbi.CheckState != CheckState.Checked)
                        continue;
                    Dictionary<Guid, Guid> cloneList = bruc.CloneTabs(clbi.Description);
                    list.Add(FillParamCol(cloneList[new Guid("aa805b7d-c86d-490f-98fd-3f017f9f2d7b")], clbi));//M1_A
                    list.Add(FillParamCol(cloneList[new Guid("ba15f54b-857b-44e7-93d6-f76eaf4c181a")], clbi));//M1_B
                }
                return list;
            }
        }
        private SheetParamCollection FillParamCol(Guid id, CheckedListBoxItem merch)
        {
            SheetParamCollection paramCollection = new SheetParamCollection();
            paramCollection.MainHeader = Resource.MainHeader;
            paramCollection.TabId = id;

            paramCollection.Add(new SheetParam() { SqlParamName = "@Merch_ID", Value = merch.Value, DisplayParamName = Resource.M1, DisplayValue = merch.Description });
            paramCollection.Add(new SheetParam() { SqlParamName = "@Date", Value = dateEdit.DateTime });

            if (rbType.SelectedIndex == 1)
            {
                paramCollection.TableDataType = TableType.Fact;
                paramCollection.Add(new SheetParam() { SqlParamName = "@isEvening", Value = rbFactType.SelectedIndex, DisplayParamName = Resource.Period, DisplayValue = rbFactType.SelectedIndex == 0 ? Resource.Morning : Resource.Evening });
                paramCollection.Add(new SheetParam() { SqlParamName = "@Supervisor_Id", Value = null });
                paramCollection.Add(new SheetParam() { DisplayParamName = Resource.ReportDay, DisplayValue = Convert.ToDateTime(dataTableDate.Rows[0]["CurrentDate"]).ToShortDateString() });
                paramCollection.Add(new SheetParam() { DisplayParamName = Resource.ReportMounth, DisplayValue = Convert.ToDateTime(dataTableDate.Rows[0]["CurrentDate"]).ToString("MM yyyy") });
                paramCollection.Add(new SheetParam() { DisplayParamName = Resource.WorkDays, DisplayValue = dataTableDate.Rows[0]["WorkDaysInMonth"] });
                paramCollection.Add(new SheetParam() { DisplayParamName = Resource.WorkDay, DisplayValue = dataTableDate.Rows[0]["WorkDay"] });
            }
            else
            {
                paramCollection.TableDataType = TableType.Plan;
                paramCollection.Add(new SheetParam() { SqlParamName = "@StartDate", Value = dtStart, DisplayParamName = Resource.ReportMounth, DisplayValue = dtStart.ToString("MMMM yyyy") });
                paramCollection.Add(new SheetParam() { SqlParamName = "@EndDate", Value = dtEnd });
            }
                
            return paramCollection;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            bool selected = false;

            foreach (CheckedListBoxItem clbi in cbM1.Properties.Items)
            {
                if (clbi.CheckState == CheckState.Checked)
                {
                    selected = true;
                    break;
                }
            }
            if (!selected)
                XtraMessageBox.Show(Resource.Error_M1);
            else if (rbType.SelectedIndex == 0 && dateEdit.DateTime < new DateTime(DateTime.Now.Year,DateTime.Now.Month,1))
                XtraMessageBox.Show(Resource.Error_Date);
            else
                DialogResult = DialogResult.OK;
        }

        private void rbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbFactType.Enabled = (rbType.SelectedIndex == 1);
            lbDate.Visible = dateEdit.Visible = true;
        }

    }
    internal class ComboBoxItem
    {
        object id;
        public object Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
