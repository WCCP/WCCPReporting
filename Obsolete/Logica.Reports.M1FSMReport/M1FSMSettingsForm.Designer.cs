﻿namespace Logica.Reports.M1FSMReport
{
    partial class M1FSMSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(M1FSMSettingsForm));
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.tcOptions = new DevExpress.XtraTab.XtraTabControl();
            this.pgAction = new DevExpress.XtraTab.XtraTabPage();
            this.OptionsPanel = new DevExpress.XtraEditors.PanelControl();
            this.groupData1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.lbDate = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.rbType = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cbM1 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cbM2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblM1 = new DevExpress.XtraEditors.LabelControl();
            this.lblM2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.rbFactType = new DevExpress.XtraEditors.RadioGroup();
            this.lblReport = new DevExpress.XtraEditors.LabelControl();
            this.txtReportName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).BeginInit();
            this.tcOptions.SuspendLayout();
            this.pgAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).BeginInit();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).BeginInit();
            this.groupData1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbFactType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHelp
            // 
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(285, 453);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 10;
            this.btnHelp.Text = "&Справка";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(204, 453);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 12;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(123, 453);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 11;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // tcOptions
            // 
            this.tcOptions.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.tcOptions.Appearance.Options.UseBackColor = true;
            this.tcOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcOptions.Location = new System.Drawing.Point(0, 0);
            this.tcOptions.Name = "tcOptions";
            this.tcOptions.SelectedTabPage = this.pgAction;
            this.tcOptions.Size = new System.Drawing.Size(365, 447);
            this.tcOptions.TabIndex = 13;
            this.tcOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgAction});
            // 
            // pgAction
            // 
            this.pgAction.Controls.Add(this.OptionsPanel);
            this.pgAction.Name = "pgAction";
            this.pgAction.Size = new System.Drawing.Size(358, 418);
            this.pgAction.Text = "Действие";
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.OptionsPanel.Appearance.Options.UseBackColor = true;
            this.OptionsPanel.Controls.Add(this.groupData1);
            this.OptionsPanel.Controls.Add(this.groupControl3);
            this.OptionsPanel.Controls.Add(this.groupControl2);
            this.OptionsPanel.Controls.Add(this.groupControl1);
            this.OptionsPanel.Controls.Add(this.lblReport);
            this.OptionsPanel.Controls.Add(this.txtReportName);
            this.OptionsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionsPanel.Location = new System.Drawing.Point(0, 0);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(358, 418);
            this.OptionsPanel.TabIndex = 0;
            // 
            // groupData1
            // 
            this.groupData1.Controls.Add(this.dateEdit);
            this.groupData1.Controls.Add(this.lbDate);
            this.groupData1.Location = new System.Drawing.Point(11, 50);
            this.groupData1.Name = "groupData1";
            this.groupData1.Size = new System.Drawing.Size(339, 64);
            this.groupData1.TabIndex = 13;
            this.groupData1.Text = "Период";
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = null;
            this.dateEdit.Location = new System.Drawing.Point(62, 35);
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit.Size = new System.Drawing.Size(100, 20);
            this.dateEdit.TabIndex = 5;
            // 
            // lbDate
            // 
            this.lbDate.Location = new System.Drawing.Point(19, 38);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(26, 13);
            this.lbDate.TabIndex = 4;
            this.lbDate.Text = "Дата";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.rbType);
            this.groupControl3.Location = new System.Drawing.Point(11, 120);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(339, 84);
            this.groupControl3.TabIndex = 12;
            this.groupControl3.Text = "Тип отчета:";
            // 
            // rbType
            // 
            this.rbType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbType.Location = new System.Drawing.Point(2, 22);
            this.rbType.Name = "rbType";
            this.rbType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Ввод планов"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Получение фактов")});
            this.rbType.Size = new System.Drawing.Size(335, 60);
            this.rbType.TabIndex = 10;
            this.rbType.SelectedIndexChanged += new System.EventHandler(this.rbType_SelectedIndexChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl2.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl2.Appearance.Options.UseBackColor = true;
            this.groupControl2.Controls.Add(this.cbM1);
            this.groupControl2.Controls.Add(this.cbM2);
            this.groupControl2.Controls.Add(this.lblM1);
            this.groupControl2.Controls.Add(this.lblM2);
            this.groupControl2.Location = new System.Drawing.Point(11, 296);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(339, 116);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Пользователь:";
            // 
            // cbM1
            // 
            this.cbM1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.cbM1.Location = new System.Drawing.Point(9, 90);
            this.cbM1.Name = "cbM1";
            this.cbM1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbM1.Size = new System.Drawing.Size(327, 20);
            this.cbM1.TabIndex = 15;
            // 
            // cbM2
            // 
            this.cbM2.Location = new System.Drawing.Point(9, 49);
            this.cbM2.Name = "cbM2";
            this.cbM2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbM2.Size = new System.Drawing.Size(327, 20);
            this.cbM2.TabIndex = 14;
            this.cbM2.SelectedIndexChanged += new System.EventHandler(this.cbM2_SelectedIndexChanged);
            // 
            // lblM1
            // 
            this.lblM1.Location = new System.Drawing.Point(9, 71);
            this.lblM1.Name = "lblM1";
            this.lblM1.Size = new System.Drawing.Size(18, 13);
            this.lblM1.TabIndex = 8;
            this.lblM1.Text = "М1:";
            // 
            // lblM2
            // 
            this.lblM2.Location = new System.Drawing.Point(10, 30);
            this.lblM2.Name = "lblM2";
            this.lblM2.Size = new System.Drawing.Size(18, 13);
            this.lblM2.TabIndex = 9;
            this.lblM2.Text = "М2:";
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Controls.Add(this.rbFactType);
            this.groupControl1.Location = new System.Drawing.Point(11, 210);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(339, 81);
            this.groupControl1.TabIndex = 10;
            this.groupControl1.Text = "Факты на:";
            // 
            // rbFactType
            // 
            this.rbFactType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbFactType.Enabled = false;
            this.rbFactType.Location = new System.Drawing.Point(2, 22);
            this.rbFactType.Name = "rbFactType";
            this.rbFactType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Утро"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Вечер")});
            this.rbFactType.Size = new System.Drawing.Size(335, 57);
            this.rbFactType.TabIndex = 10;
            // 
            // lblReport
            // 
            this.lblReport.Location = new System.Drawing.Point(8, 3);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(36, 13);
            this.lblReport.TabIndex = 2;
            this.lblReport.Text = "Отчет:";
            // 
            // txtReportName
            // 
            this.txtReportName.EditValue = "";
            this.txtReportName.Location = new System.Drawing.Point(11, 21);
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtReportName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtReportName.Properties.Appearance.Options.UseBackColor = true;
            this.txtReportName.Properties.Appearance.Options.UseFont = true;
            this.txtReportName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.txtReportName.Properties.ReadOnly = true;
            this.txtReportName.Size = new System.Drawing.Size(341, 22);
            this.txtReportName.TabIndex = 0;
            this.txtReportName.TabStop = false;
            // 
            // M1FSMSettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(365, 485);
            this.Controls.Add(this.tcOptions);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "M1FSMSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отчёт";
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).EndInit();
            this.tcOptions.ResumeLayout(false);
            this.pgAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).EndInit();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).EndInit();
            this.groupData1.ResumeLayout(false);
            this.groupData1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbFactType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraTab.XtraTabControl tcOptions;
        private DevExpress.XtraTab.XtraTabPage pgAction;
        private DevExpress.XtraEditors.PanelControl OptionsPanel;
        private DevExpress.XtraEditors.TextEdit txtReportName;
        private DevExpress.XtraEditors.LabelControl lblReport;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.RadioGroup rbType;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl lblM1;
        private DevExpress.XtraEditors.LabelControl lblM2;
        private DevExpress.XtraEditors.RadioGroup rbFactType;
        private DevExpress.XtraEditors.ComboBoxEdit cbM2;
        private DevExpress.XtraEditors.GroupControl groupData1;
        private DevExpress.XtraEditors.LabelControl lbDate;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cbM1;
        private DevExpress.XtraEditors.DateEdit dateEdit;
    }
}