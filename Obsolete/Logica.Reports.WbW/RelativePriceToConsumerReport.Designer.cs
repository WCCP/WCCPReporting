namespace WccpReporting
{
    partial class RelativePriceToConsumerReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RelativePriceToConsumerReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // RelativePriceToConsumerReportScrollableControl
            // 
            this.RelativePriceToConsumerReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RelativePriceToConsumerReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.RelativePriceToConsumerReportScrollableControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.RelativePriceToConsumerReportScrollableControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.RelativePriceToConsumerReportScrollableControl.Name = "RelativePriceToConsumerReportScrollableControl";
            this.RelativePriceToConsumerReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.RelativePriceToConsumerReportScrollableControl.TabIndex = 0;
            // 
            // RelativePriceToConsumerReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RelativePriceToConsumerReportScrollableControl);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "RelativePriceToConsumerReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl RelativePriceToConsumerReportScrollableControl;


    }
}
