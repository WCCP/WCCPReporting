using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.OleDb;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraCharts;

namespace WccpReporting
{
    public partial class ContractsContext : DevExpress.XtraEditors.XtraUserControl
    {
        public ContractsContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(string channel, int WaveId, string WaveName, int prevWave_ID, string prevWaveName)
        {
            try
            {
                DataTable dataDataViewTable1Context = new DataTable();
                DataTable dataDataViewTable2Context = new DataTable();

                using (OleDbCommand cmdDW_TomasResearch_WbW_Contracts_by_segment = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_Contracts_by_segment_ONTRADE" : "DW_TomasResearch_WbW_Contracts_by_segment"), WccpUIControl.Conn))
                {
                    cmdDW_TomasResearch_WbW_Contracts_by_segment.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_Contracts_by_segment.Parameters.AddWithValue("@Wave_ID", WaveId);
                    cmdDW_TomasResearch_WbW_Contracts_by_segment.Parameters.AddWithValue("@prevWave_ID", prevWave_ID);
                    cmdDW_TomasResearch_WbW_Contracts_by_segment.CommandType = CommandType.StoredProcedure;

                    using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Contracts_by_segment))
                    {
                        da.Fill(dataDataViewTable1Context);
                    };
                };

                using (OleDbCommand cmdDW_TomasResearch_WbW_Contract_deployment = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_Contract_deployment_ONTRADE" : "DW_TomasResearch_WbW_Contract_deployment"), WccpUIControl.Conn))
                {
                    cmdDW_TomasResearch_WbW_Contract_deployment.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_Contract_deployment.Parameters.AddWithValue("@Wave_ID", WaveId);
                    cmdDW_TomasResearch_WbW_Contract_deployment.Parameters.AddWithValue("@prevWave_ID", prevWave_ID);
                    cmdDW_TomasResearch_WbW_Contract_deployment.CommandType = CommandType.StoredProcedure;

                    using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Contract_deployment))
                    {
                        da.Fill(dataDataViewTable2Context);
                    };
                };

                gvTable1Context.Columns.Clear();
                gvTable1Context.BeginUpdate();
                gbMSShift.Children.Clear();
                bvTable2Context.BeginUpdate();

                try
                {
                    //������ �������
                    foreach (DataColumn col in dataDataViewTable1Context.Columns)
                    {
                        GridColumn newCol = gvTable1Context.Columns.AddField(col.ColumnName);
                        newCol.Caption = col.Caption;
                        newCol.AppearanceCell.Options.UseFont = true;
                        newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        newCol.AppearanceCell.Options.UseTextOptions = true;
                        newCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                        newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Default;
                        newCol.AppearanceHeader.Options.UseTextOptions = true;
                        newCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                        newCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                        newCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;

                        if (col.ColumnName == "Wave")
                        {
                            newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
                            newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Bold);
                            newCol.Width = 105;
                        }
                        else
                        {
                            if (col.ColumnName == "Description")
                                newCol.Width = 125;
                            else
                                newCol.Width = 55;
                        };

                        if (col.ColumnName != "isTotal" && col.ColumnName != "isChart")
                            newCol.Visible = true;
                        else
                            newCol.Visible = false;

                        if (col.ColumnName != "Wave" && col.ColumnName != "Description")
                        {
                            newCol.OptionsColumn.ShowCaption = true;
                        }
                        else
                        {
                            newCol.AppearanceHeader.BackColor = Color.White;
                            newCol.AppearanceHeader.BorderColor = Color.White;
                            newCol.OptionsColumn.ShowCaption = false;
                        };
                    };

                    //������ �������
                    foreach (DataColumn col in dataDataViewTable2Context.Columns)
                    {
                        if (col.ColumnName != "BRAND" && col.ColumnName != "SEGMENT" && col.ColumnName != "POCS" && col.ColumnName != "POCS_Percent"
                            && col.ColumnName != "AVR_DAL" && col.ColumnName != "AVR_VAR" && col.ColumnName != "MS_SHIFT")
                        {
                            GridBand newBand = new GridBand();
                            newBand.Caption = col.Caption;
                            newBand.AppearanceHeader.Options.UseTextOptions = true;
                            newBand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            newBand.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                            newBand.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;

                            BandedGridColumn newCol = bvTable2Context.Columns.AddField(col.ColumnName);
                            newCol.Caption = " ";
                            newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                            newCol.Visible = true;
                            newCol.Width = 105;
                            newCol.OwnerBand = newBand;
                            gbMSShift.Children.Add(newBand);
                        };
                    };

                }
                finally
                {
                    gvTable1Context.EndUpdate();
                    bvTable2Context.EndUpdate();
                };

                chartContext.Series[0].Name = prevWaveName;
                chartContext.Series[1].Name = WaveName;

                dataDataViewTable1Context.DefaultView.RowFilter = "isChart = 1 AND Wave = '" + prevWaveName + "'";
                if (dataDataViewTable1Context.DefaultView.Count > 0)
                {
                    foreach (DataColumn seriesCol in dataDataViewTable1Context.Columns)
                    {
                        if (seriesCol.ColumnName != "Wave" && seriesCol.ColumnName != "Description" && seriesCol.ColumnName != "isChart" && seriesCol.ColumnName != "isTotal")
                        {
                            chartContext.Series[0].Points.Add(new SeriesPoint(seriesCol.ColumnName, new double[] { Convert.ToDouble(dataDataViewTable1Context.DefaultView[0][seriesCol.ColumnName]) }));
                        };
                    };
                };

                dataDataViewTable1Context.DefaultView.RowFilter = "isChart = 1 AND Wave = '" + WaveName + "'";
                if (dataDataViewTable1Context.DefaultView.Count > 0)
                {
                    foreach (DataColumn seriesCol in dataDataViewTable1Context.Columns)
                    {
                        if (seriesCol.ColumnName != "Wave" && seriesCol.ColumnName != "Description" && seriesCol.ColumnName != "isChart" && seriesCol.ColumnName != "isTotal")
                        {
                            chartContext.Series[1].Points.Add(new SeriesPoint(seriesCol.ColumnName, new double[] { Convert.ToDouble(dataDataViewTable1Context.DefaultView[0][seriesCol.ColumnName]) }));
                        };
                    };
                };

                dataDataViewTable1Context.DefaultView.RowFilter = String.Empty;

                int table1H = 0, table2H = 0, delta = 110;
                int table1W = 0, table2W = 0;

                if (dataDataViewTable1Context.Rows.Count > 0)
                {
                    gcTable1Context.DataSource = dataDataViewTable1Context.DefaultView;
                    table1H = ((gvTable1Context.RowHeight * dataDataViewTable1Context.DefaultView.Count) + ((gvTable1Context.RowSeparatorHeight * 2) * dataDataViewTable1Context.DefaultView.Count)) + gvTable1Context.ColumnPanelRowHeight + 30;
                    foreach (GridColumn colGridView in gvTable1Context.Columns)
                    {
                        if (colGridView.Caption != "isTotal")
                            table1W = table1W + colGridView.Width;
                    };
                };

                if (dataDataViewTable2Context.Rows.Count > 0)
                {
                    gcTable2Context.DataSource = dataDataViewTable2Context.DefaultView;
                    table2H = ((bvTable2Context.RowHeight * dataDataViewTable2Context.DefaultView.Count) + ((bvTable2Context.RowSeparatorHeight * 2) * dataDataViewTable2Context.DefaultView.Count)) + (bvTable2Context.BandPanelRowHeight * 3) + 50;
                    foreach (GridColumn colGridView in bvTable2Context.Columns)
                    {
                        table2W = table2W + colGridView.Width;
                    };
                    table2W = table1W + chartContext.Width;
                };

                //��������� ������ � ������ ���������
                table1LayoutControlItem.ControlMinSize = new Size(table1W, table1H);
                table1LayoutControlItem.ControlMaxSize = new Size(table1W, table1H);
                table2LayoutControlItem.ControlMinSize = new Size(table2W, table2H);
                table2LayoutControlItem.ControlMaxSize = new Size(table2W, table2H);

                this.Height = table1H + table2H + 50;
                this.Width = (table1W > table2W) ? (table1W+30) : (table2W+30);
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //������� ��� �������������, totals � ������ ����
        private void gvTable1Context_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                string isTotal = View.GetRowCellDisplayText(e.RowHandle, View.Columns["isTotal"]);
                if (e.Column.FieldName != "Wave" && e.Column.FieldName != "Description" && e.Column.FieldName != "isTotal" && e.Column.FieldName != "isChart")
                {
                    if (e.CellValue != DBNull.Value)
                    {
                        if (Convert.ToDouble(e.CellValue) < 0)
                        {
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (Convert.ToDouble(e.CellValue) == 0)
                        {
                            e.Appearance.ForeColor = Color.Silver;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        };
                    };
                };
                if (isTotal == "1")
                {
                    if (e.Column.Caption != "Wave")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                    };
                    e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                };
            }

        }

        private void bvTable2Context_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                if (e.Column.Caption != "BRAND" && e.Column.Caption != "SEGMENT")
                {
                    if (e.CellValue != DBNull.Value)
                    {
                        if (Convert.ToDouble(e.CellValue) < 0)
                        {
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (Convert.ToDouble(e.CellValue) == 0)
                        {
                            e.Appearance.ForeColor = Color.Silver;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        };
                    };
                };
            }

        }

    }
}
