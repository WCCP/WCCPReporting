namespace WccpReporting
{
    partial class ContractsReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContractsReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // ContractsReportScrollableControl
            // 
            this.ContractsReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContractsReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.ContractsReportScrollableControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ContractsReportScrollableControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ContractsReportScrollableControl.Name = "ContractsReportScrollableControl";
            this.ContractsReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.ContractsReportScrollableControl.TabIndex = 0;
            // 
            // ContractsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ContractsReportScrollableControl);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ContractsReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl ContractsReportScrollableControl;

    }
}
