using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Data.OleDb;
using DevExpress.XtraTab;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraDataLayout;
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid;
using DevExpress.XtraCharts;
using System.Diagnostics;
using Logica.Reports.Common.WaitWindow;


namespace WccpReporting
{
    public partial class WccpUIControl : DevExpress.XtraEditors.XtraUserControl
    {
        private bool IsPrepare = false;
        private static OleDbConnection conn;
        public static OleDbConnection Conn
        {
            get { return conn; }
            set { conn = value; }
        }
        private enum EditType { etNone, etAdd, etEdit };
        private EditType curEditType = EditType.etNone;

        private string _ReportTitle;
        public string ReportTitle
        {
            get { return _ReportTitle; }
            set { _ReportTitle = value; lbReportTitle.Text = value; }
        }

        private bool _IsSkin;
        public bool IsSkin
        {
            get { return _IsSkin; }
            set { _IsSkin = value; }
        }

        private int _RegionId;
        public int RegionId
        {
            get { return _RegionId; }
            set { _RegionId = value; }
        }
        private string _RegionName;
        public string RegionName
        {
            get { return _RegionName; }
            set { _RegionName = value; edRegionText.Text = value; }
        }

        private string _ChanelName;
        public string ChanelName
        {
            get { return _ChanelName; }
            set { _ChanelName = value; edChanelText.Text = value; }
        }

        private int _WaveId;
        //Текущая волна
        public int WaveId
        {
            get { return _WaveId; }
            set { _WaveId = value; }
        }

        private string _WaveName;
        public string WaveName
        {
            get { return _WaveName; }
            set { _WaveName = value; edWave3Text.Text = value; }
        }

        private int _Wave1Id;
        //Предпредыдущая волна
        public int Wave1Id
        {
            get { return _Wave1Id; }
            set { _Wave1Id = value; }
        }

        private string _Wave1Name;
        public string Wave1Name
        {
            get { return _Wave1Name; }
            set { _Wave1Name = value; edWave1Text.Text = value; }
        }

        private int _Wave2Id;
        //Предыдущая волна
        public int Wave2Id
        {
            get { return _Wave2Id; }
            set { _Wave2Id = value; }
        }

        private string _Wave2Name;
        public string Wave2Name
        {
            get { return _Wave2Name; }
            set { _Wave2Name = value; edWave2Text.Text = value; }
        }

        public WccpUIControl()
        {
            InitializeComponent();
            Disposed += new EventHandler(ReportControl_Disposed);
        }

        void ReportControl_Disposed(object sender, EventArgs e)
        {
            try
            {
              Conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCloseConnection + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //Определить поведение элементов управления
        private void ActionControlUpdate()
        {
            if (reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.Count > 0)
            {
                btnEditCompetPair.Enabled = true;
                btnDelCompetPair.Enabled = true;
                btnDelAllCompetPair.Enabled = true;
            }
            else
            {
                btnEditCompetPair.Enabled = false;
                btnDelCompetPair.Enabled = false;
                btnDelAllCompetPair.Enabled = false;
            };
        }

        private bool CleanDirForReportFiles(string exlFile)
        {
            try
            {
                if (exlFile != String.Empty)
                {
                    if (File.Exists(exlFile))
                    {
                        File.Delete(exlFile);
                        return true;
                    }
                    else
                        return true;
                }
                else
                    return false;
            }
            catch
            {
                MessageBox.Show("Закройте файл " + exlFile + " и повторите попытку", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);                
                return false;
            };
        }

        //Отобразить список отчетов
        private void ShowReportList(string Channel)
        {
            using (OleDbCommand cmdWbW_ListReports = new OleDbCommand("DW_TomasResearch_WbW_ListReports", conn))
            {
                cmdWbW_ListReports.CommandTimeout = WCCPConst.CommandTimeout;
                cmdWbW_ListReports.CommandType = CommandType.StoredProcedure;
                cmdWbW_ListReports.Parameters.AddWithValue("@Channel", Channel);
                using (OleDbDataAdapter daWbW_ListReports = new OleDbDataAdapter(cmdWbW_ListReports))
                {
                    try
                    {
                      reportDataSet.tblReportList.Rows.Clear();
                      daWbW_ListReports.Fill(reportDataSet, "tblReportList");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorCreateListReports + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            };
        }

        //Создать списки СКЮ для Конкурентные пары
        private void CreateSKUListCompetPair()
        {
            using (OleDbCommand cmdDW_TomasResearch_ListSKU = new OleDbCommand("DW_TomasResearch_ListSKU", conn))
            {
                cmdDW_TomasResearch_ListSKU.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_ListSKU.Parameters.AddWithValue("@Region_ID", DBNull.Value);
                cmdDW_TomasResearch_ListSKU.Parameters.AddWithValue("@Channel", ChanelName);
                cmdDW_TomasResearch_ListSKU.CommandType = CommandType.StoredProcedure;
                using (OleDbDataAdapter dacmdDW_TomasResearch_ListSKU = new OleDbDataAdapter(cmdDW_TomasResearch_ListSKU))
                {
                    try
                    {
                        reportDataSet.tblDW_TomasResearch_ListSKU.Rows.Clear();
                        dacmdDW_TomasResearch_ListSKU.Fill(reportDataSet, "tblDW_TomasResearch_ListSKU");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorCreateSKUListCompetPair + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            };
        }

        //Сохранить скрипт в файл (тест)
        private void SaveScriptToFile(string scriptFile, string sql)
        {
            //if (File.Exists(scriptFile))
            //{
            //  File.Delete(scriptFile);
            //};
            FileStream fs = new FileStream(scriptFile, FileMode.Create, FileAccess.Write, FileShare.Write);
            fs.Close();
            StreamWriter sw = new StreamWriter(scriptFile, true, Encoding.Default);
            sw.Write(sql);
            sw.Close();
        }

        //Создать предварительные наборы данных
        private bool Prepare()
        {
            string sql = String.Empty;

            string tblTmpDW_TomasResearch_SN_FILTERED =
                   "CREATE TABLE #DW_TomasResearch_SN_FILTERED "
                 + "( "
	             + "[Wave_ID] [int] NOT NULL, "
	             + "[OL_ID] [bigint] NOT NULL, "
	             + "[Channel] [varchar](20), "
	             + "[SName] [sysname], "
	             + "[KPI] [varchar](128), "
	             + "[SKU] [varchar](50), "
	             + "[SUBKPI] [varchar](50), "
	             + "[AggrType] [int], "
	             + "[S_Channel] [varchar](20), "
	             + "[Producer_ID] [tinyint] NULL, "
	             + "[FValue] [float] NULL, "
	             + "[Region_ID] [int] NULL, "
                 + "PRIMARY KEY CLUSTERED  "
                 + "( "
	             + "[Wave_ID] ASC, "
	             + "[OL_ID] ASC, "
	             + "[Channel] ASC, "
	             + "[SName] ASC "
                 + ") "
                 + ") ";

            string tblTmpDW_TomasResearch_WbW_CompetPairSKU =
                   "CREATE TABLE #DW_TomasResearch_WbW_CompetPairSKU "
                 + "( "
                 + "[CompetPair_ID] [int] IDENTITY(1,1) NOT NULL, "
                 + "[SKU_ID1] [int] NOT NULL, "
                 + "[SKU_ID2] [int] NOT NULL, "
                 + "PRIMARY KEY CLUSTERED  "
                 + "( "
                 + "[CompetPair_ID] ASC "
                 + "), "
                 + "UNIQUE NONCLUSTERED "
                 + "( "
                 + "[SKU_ID1] ASC, "
                 + "[SKU_ID2] ASC "
                 + ") "
                 + ") ";

            string tbltmpDW_TomasResearch_WbW_RPTC_Limits =
                   "CREATE TABLE #DW_TomasResearch_WbW_RPTC_Limits "
                 + "( "
                 + "[RPTC_Limit_ID] [int] IDENTITY(1,1) NOT NULL, "
                 + "[ChanelType_ID] [int] NULL, " 
                 + "[SKU_ID] [int] NULL, "
                 + "[LimitType_ID] [int] NOT NULL, "
                 + "[LimitName] [varchar](20) COLLATE Cyrillic_General_CI_AS NOT NULL, "
                 + "[Limit1] [float] NOT NULL, "
                 + "[Limit2] [float] NOT NULL, "
                 + "[SortOrder] [int] NOT NULL "
                 + ")";

            string tbltmpDW_TomasResearch_WbW_SKU =
                  "CREATE TABLE #DW_TomasResearch_WbW_SKU "
                + "( "
                + "[ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[SKU_ID] [int] NOT NULL, "
                + "[SKU] [varchar](50) COLLATE Cyrillic_General_CI_AS NOT NULL, "
                + "[isChecked] [bit] NULL, "
                + "[SortOrder] [int] NULL, "
                + "[Producer_ID] [tinyint] NULL "
                + ")";

            string tbltmpDW_TomasResearch_WbW_PTC_Limits =
                  "CREATE TABLE #DW_TomasResearch_WbW_PTC_Limits "
                + "( "
                + "[PTC_Limit_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[ChanelType_ID] [int] NULL, "
                + "[SKU_ID] [int] NULL, "
                + "[LimitType_ID] [int] NOT NULL, "
                + "[LimitName] [varchar](20) COLLATE Cyrillic_General_CI_AS NOT NULL, "
                + "[Limit1] [float] NOT NULL, "
                + "[Limit2] [float] NOT NULL, "
                + "[SortOrder] [int] NOT NULL "
                + ")";

            string tbltmpMain_Add_Sku =
                  "CREATE TABLE #main_add_sku "
                + "( "
                + "[ID] int IDENTITY(1,1) NOT NULL, "
                + "[Wave_ID] int, "
                + "[Region_ID] int, "
                + "[SKU_ID] int, "
                + "[SKU_NAME] varchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL, "
                + "[Type_ID] tinyint "
                + ")";

            string tbltmpDW_TomasResearch_WbW_PTCShift_Limits =
                   "CREATE TABLE #DW_TomasResearch_WbW_PTCShift_Limits "
                 + "( "
                 + "[PTCShift_Limit_ID] [int] IDENTITY(1,1) NOT NULL, "
                 + "[ChanelType_ID] [int] NULL, " 
                 + "[SKU_ID] [int] NULL, "
                 + "[LimitType_ID] [int] NOT NULL, "
                 + "[LimitName] [varchar](20) COLLATE Cyrillic_General_CI_AS NOT NULL, "
                 + "[Limit1] [float] NOT NULL, "
                 + "[Limit2] [float] NOT NULL, "
                 + "[SortOrder] [int] NOT NULL "
                 + ")";

            sql = tblTmpDW_TomasResearch_SN_FILTERED + " " + tblTmpDW_TomasResearch_WbW_CompetPairSKU
                + " " + tbltmpDW_TomasResearch_WbW_RPTC_Limits + " " + tbltmpDW_TomasResearch_WbW_SKU
                + " " + tbltmpDW_TomasResearch_WbW_PTC_Limits + " " + tbltmpMain_Add_Sku + " " + tbltmpDW_TomasResearch_WbW_PTCShift_Limits + " ";

            try
            {
               using (OleDbCommand cmdCreateTmpTable = new OleDbCommand(sql, Conn))
               {
                 cmdCreateTmpTable.CommandTimeout = WCCPConst.CommandTimeout;
                 cmdCreateTmpTable.ExecuteNonQuery();
                 using (OleDbCommand cmdDW_TomasResearch_WbW_Prepare = new OleDbCommand("DW_TomasResearch_WbW_Prepare", Conn))
                 {
                   cmdDW_TomasResearch_WbW_Prepare.CommandTimeout = WCCPConst.CommandTimeout;
                   cmdDW_TomasResearch_WbW_Prepare.Parameters.AddWithValue("@Wave_ID", WaveId);
                   cmdDW_TomasResearch_WbW_Prepare.Parameters.AddWithValue("@Region_ID", RegionId);
                   cmdDW_TomasResearch_WbW_Prepare.Parameters.AddWithValue("@Channel", ChanelName);
                   cmdDW_TomasResearch_WbW_Prepare.CommandType = CommandType.StoredProcedure;
                   cmdDW_TomasResearch_WbW_Prepare.ExecuteNonQuery();
                   return true;
                 };
               };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorPrepare + "\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            };
        }

        //Создать набор конкурентных пар по умолчанию
        private void CreateDefaultListCompetPair()
        {
            using (OleDbCommand cmdDefaultListCompetPair = new OleDbCommand("DW_TomasResearch_WbW_DefaultCompetPair", conn))
            {
               cmdDefaultListCompetPair.CommandTimeout = WCCPConst.CommandTimeout;
               cmdDefaultListCompetPair.CommandType = CommandType.StoredProcedure;
               cmdDefaultListCompetPair.Parameters.AddWithValue("@Wave_ID", WaveId);
               cmdDefaultListCompetPair.Parameters.AddWithValue("@Region_ID", RegionId);
               cmdDefaultListCompetPair.Parameters.AddWithValue("@Channel", ChanelName);                

               using (OleDbCommand cmdDW_TomasResearch_WbW_ListCompetPair = new OleDbCommand("DW_TomasResearch_WbW_ListCompetPair", conn))
               {
                 using (OleDbDataAdapter daDefaultListCompetPair = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_ListCompetPair))
                 {
                  try
                  {
                    reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.Clear();
                    cmdDefaultListCompetPair.ExecuteNonQuery();
                    daDefaultListCompetPair.Fill(reportDataSet, "tblDW_TomasResearch_WbW_ListCompetPair");
                  }
                  catch (Exception ex)
                  {
                    MessageBox.Show(WCCPConst.msgErrorCreateDefaultListCompetPair + "\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  };
                 };

                };
                 
            };
        }

        //Заблокировать/Разблокировать кнопки редактирования конкурентных пар
        private void LockCompetPairBtnEdit(bool Value)
        {
            btnAddCompetPair.Enabled = Value;
            btnEditCompetPair.Enabled = Value;
            btnDelCompetPair.Enabled = Value;
            btnDelAllCompetPair.Enabled = Value;
            btnDefaultCompetPair.Enabled = Value;
        }

        //Заблокировать панель редактирования конкурентных пар
        private void LockCompetPairPanelEdit()
        {
            pnEditCompetPair.Enabled = false;
            btnSaveCompetPair.Enabled = false;
            btnCancelCompetPair.Enabled = false;
            //
            luSKUName1.EditValue = null;
            luSKUName2.EditValue = null;
            //
            gvCompetPair.Focus();
        }

        //Разблокировать панель редактирования конкурентных пар
        private void UnLockCompetPairPanelEdit()
        {
            pnEditCompetPair.Enabled = true;
            btnSaveCompetPair.Enabled = true;
            btnCancelCompetPair.Enabled = true;
        }

        private void ReportControl_Load(object sender, EventArgs e)
        {
            ReportTitle = ReportOptions.ReportTitle;
            tcReportOptions.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            //
            RegionId=ReportOptions.RegionId;
            RegionName = ReportOptions.RegionName;
            ChanelName = ReportOptions.ChanelName;
            WaveId = ReportOptions.WaveId;
            WaveName = ReportOptions.WaveName;
            //
            //Запретить опции для отчетов
            foreach (DevExpress.XtraNavBar.NavBarItemLink itemLink in navBarOptionsGroup.ItemLinks)
            {
                itemLink.Item.Enabled = false;
            };
            //
            WaitManager.StartWait();
            //
            try
            {
                Conn = DM.Connect(WCCPConst.ConnectionString);
                Conn.Open();
                //Получить волны
                using (OleDbCommand cmdWavePrevs = new OleDbCommand("DW_TomasResearch_ListWaves", conn))
                {
                    cmdWavePrevs.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdWavePrevs.CommandType = CommandType.StoredProcedure;
                    cmdWavePrevs.Parameters.AddWithValue("@forWbW", 1);
                    cmdWavePrevs.Parameters.AddWithValue("@Wave_ID", WaveId);
                    using (OleDbDataAdapter daWavePrevs = new OleDbDataAdapter(cmdWavePrevs))
                    {
                        try
                        {
                            daWavePrevs.Fill(reportDataSet, "tblWavePrevs");
                            if (reportDataSet.tblWavePrevs.Rows.Count >= 2)
                            {
                                Wave1Id = Convert.ToInt32(reportDataSet.tblWavePrevs.Rows[0]["Wave_Id"]);
                                Wave1Name = Convert.ToString(reportDataSet.tblWavePrevs.Rows[0]["WaveName"]);
                                Wave2Id = Convert.ToInt32(reportDataSet.tblWavePrevs.Rows[1]["Wave_Id"]);
                                Wave2Name = Convert.ToString(reportDataSet.tblWavePrevs.Rows[1]["WaveName"]);
                            };
                        }
                        catch (Exception ex)
                        {
                            WaitManager.StopWait();
                            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        };
                    };
                };
                //Создать список отчетов
                ShowReportList(ChanelName);
                //Создать предварительные наборы данных
                IsPrepare = Prepare();
                //
                CreateSKUListCompetPair();            //Создать списки СКЮ для Конкурентные пары
                CreateDefaultListCompetPair();        //Создать набор конкурентных пар по умолчанию
                CreateDefaultSKUList();               //Создать списки СКЮ для PTC лимитов
                CreateDefaultRelativePTCLimitsList(); //Создать список RelativePTCLimits по умолчанию
                CreateDefaultSKUPTCLimits();          //Создать список PTC лимитов по умолчанию для всех СКЮ
                ShowSKUPTCLimit(reportDataSet.tblDW_TomasResearch_WbW_SKU[bsSKUPTCLimit.Position].SKU_ID);
                CreateDefaultPTCShiftLimits();
                ShowListPTCShiftLimit();
                CreateDefaultOsnDopSKU();             //Создать список по умолчанию для осн. доб. СКЮ
                //для не разнесенных скю
                ShowListOsnDopSKU(3, reportDataSet.tblSKUAll);
                //для основных скю
                ShowListOsnDopSKU(1, reportDataSet.tblOsnSKU);
                //для доп. скю
                ShowListOsnDopSKU(2, reportDataSet.tblDopSKU);
                //
                LockCompetPairPanelEdit();
                ActionControlUpdate();
            }
            finally
            {
                WaitManager.StopWait();
            };
        }

        //Отчет в Excel
        private void tbtnReportToExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
                
                string exlFile= String.Empty;
                Boolean IsSucces = false;
                ReportOptions.ReportLongTitle = ReportOptions.ReportLongTitle.Replace(" ", "_");
                ReportOptions.ReportLongTitle = ReportOptions.ReportLongTitle.Replace("-", "_");
                ReportOptions.ReportLongTitle = ReportOptions.ReportLongTitle.Replace(",", String.Empty);
                ReportOptions.ReportLongTitle = ReportOptions.ReportLongTitle.Replace(".", String.Empty);
                if (ReportOptions.ReportLongTitle.Length > 254)
                {
                    ReportOptions.ReportLongTitle = ReportOptions.ReportLongTitle.Remove(1, 254);
                };
                
                try
                {
                    if (tcReport.SelectedTabPageIndex > 0)
                    {
                        string newTabCaption = (tcReport.TabPages[tcReport.SelectedTabPageIndex].Text.Replace("/", "_"));
                        newTabCaption = newTabCaption.Replace("\\", "_");
                        newTabCaption = newTabCaption.Replace("+", "_");
                        newTabCaption = newTabCaption.Replace("&", "_");
                        newTabCaption = newTabCaption.Replace("~", "_");
                        newTabCaption = newTabCaption.Replace("!", "_");
                        newTabCaption = newTabCaption.Replace("@", "_");
                        newTabCaption = newTabCaption.Replace("#", "_");
                        newTabCaption = newTabCaption.Replace("$", "_");
                        newTabCaption = newTabCaption.Replace("*", "_");
                        newTabCaption = newTabCaption.Replace(" ", "_");
                        newTabCaption = newTabCaption.Replace("-", "_");
                        newTabCaption = newTabCaption.Replace(",", String.Empty);
                        newTabCaption = newTabCaption.Replace(".", String.Empty);

                        exlFile = ReportOptions.ReportLongTitle + "_" + newTabCaption + ".xls";
                        if (exlFile.Length > 254)
                        {
                            exlFile = exlFile.Remove(1, 254);
                        };
                        IsSucces = CleanDirForReportFiles(exlFile);
                        if (IsSucces)
                        {
                            CompositeLink composLink = new CompositeLink(new PrintingSystem());
                            foreach (Control reportControl in tcReport.TabPages[tcReport.SelectedTabPageIndex].Controls)
                            {
                                if (reportControl is XtraUserControl)
                                {
                                    foreach (Control reportScrollControl in ((XtraUserControl)reportControl).Controls)
                                    {
                                        if (reportScrollControl is XtraScrollableControl)
                                        {
                                            foreach (Control reportContext in ((XtraScrollableControl)reportScrollControl).Controls)
                                            {
                                                if (reportContext is XtraUserControl)
                                                {
                                                    foreach (Control reportLayoutControl in ((XtraUserControl)reportContext).Controls)
                                                    {
                                                        if (reportLayoutControl is DataLayoutControl)
                                                        {
                                                            foreach (Control dt in ((DataLayoutControl)reportLayoutControl).Controls)
                                                            {
                                                                if (dt is GridControl)
                                                                {
                                                                    PrintableComponentLink newPCLink = new PrintableComponentLink();
                                                                    newPCLink.Component = ((GridControl)dt);
                                                                    composLink.Links.Add(newPCLink);
                                                                };
                                                            };
                                                            foreach (Control dt in ((DataLayoutControl)reportLayoutControl).Controls)
                                                            {
                                                                if (dt is ChartControl)
                                                                {
                                                                    PrintableComponentLink newPCLink = new PrintableComponentLink();
                                                                    newPCLink.Component = ((ChartControl)dt);
                                                                    composLink.Links.Add(newPCLink);
                                                                };
                                                            };
                                                        };
                                                    };
                                                };
                                            };
                                        };
                                    };
                                };
                            };
                            composLink.CreateDocument();
                            composLink.PrintingSystem.ExportToXls(exlFile);
                        }

                        if (IsSucces && exlFile.Length > 0)
                        {
                            Process proc = new Process();
                            proc.EnableRaisingEvents = false;
                            proc.StartInfo.FileName = "excel";
                            proc.StartInfo.Arguments = exlFile;
                            proc.Start();
                        };
                    }
                    else
                    {
                        MessageBox.Show(WCCPConst.msgWarningNotReportChecked, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    };

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Не могу открыть " + exlFile + "\nПопробуйте открыть этот файл через Excel" + "\n(" + ex.Message + ")", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                };
        }

        //Обновить отчет
        private void tbtnReportRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using(InputReportDataForm fmInputReportDataForm = new InputReportDataForm()){
                    fmInputReportDataForm.ReportName = ReportTitle;
                    if (fmInputReportDataForm.ShowDialog() == DialogResult.OK)
                    {
                        ReportOptions.RegionId = fmInputReportDataForm.RegionId;
                        ReportOptions.RegionName = fmInputReportDataForm.RegionName;
                        ReportOptions.ChanelName = fmInputReportDataForm.ChanelName;
                        ReportOptions.WaveId = fmInputReportDataForm.WaveId;
                        ReportOptions.WaveName = fmInputReportDataForm.WaveName;
                        //
                        RegionId = ReportOptions.RegionId;
                        RegionName = ReportOptions.RegionName;
                        ChanelName = ReportOptions.ChanelName;
                        WaveId = ReportOptions.WaveId;
                        WaveName = ReportOptions.WaveName;
                        //
                        WaitManager.StartWait();
                        try
                        {
                            Conn.Close();
                            Conn.Open();
                            //
                            //Получить волны
                            using (OleDbCommand cmdWavePrevs = new OleDbCommand("DW_TomasResearch_ListWaves", Conn))
                            {
                                cmdWavePrevs.CommandTimeout = WCCPConst.CommandTimeout;
                                cmdWavePrevs.CommandType = CommandType.StoredProcedure;
                                cmdWavePrevs.Parameters.AddWithValue("@forWbW", 1);
                                cmdWavePrevs.Parameters.AddWithValue("@Wave_ID", WaveId);
                                using (OleDbDataAdapter daWavePrevs = new OleDbDataAdapter(cmdWavePrevs))
                                {
                                    try
                                    {
                                        reportDataSet.tblWavePrevs.Rows.Clear();
                                        daWavePrevs.Fill(reportDataSet, "tblWavePrevs");
                                        if (reportDataSet.tblWavePrevs.Rows.Count >= 2)
                                        {
                                            Wave1Id = Convert.ToInt32(reportDataSet.tblWavePrevs.Rows[0]["Wave_Id"]);
                                            Wave1Name = Convert.ToString(reportDataSet.tblWavePrevs.Rows[0]["WaveName"]);
                                            Wave2Id = Convert.ToInt32(reportDataSet.tblWavePrevs.Rows[1]["Wave_Id"]);
                                            Wave2Name = Convert.ToString(reportDataSet.tblWavePrevs.Rows[1]["WaveName"]);
                                        };
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    };
                                };
                            };
                            ShowReportList(ChanelName);
                            //Создать предварительные наборы данных
                            IsPrepare = Prepare();
                            //
                            CreateSKUListCompetPair();            //Создать списки СКЮ для Конкурентные пары
                            CreateDefaultListCompetPair();        //Создать набор конкурентных пар по умолчанию
                            CreateDefaultSKUList();               //Создать списки СКЮ для PTC лимитов
                            CreateDefaultRelativePTCLimitsList(); //Создать список RelativePTCLimits по умолчанию
                            CreateDefaultSKUPTCLimits();          //Создать список PTC лимитов по умолчанию для всех СКЮ
                            ShowSKUPTCLimit(reportDataSet.tblDW_TomasResearch_WbW_SKU[bsSKUPTCLimit.Position].SKU_ID);
                            CreateDefaultPTCShiftLimits();
                            ShowListPTCShiftLimit();
                            CreateDefaultOsnDopSKU();             //Создать список по умолчанию для осн. доб. СКЮ
                            //для не разнесенных скю
                            ShowListOsnDopSKU(3, reportDataSet.tblSKUAll);
                            //для основных скю
                            ShowListOsnDopSKU(1, reportDataSet.tblOsnSKU);
                            //для доп. скю
                            ShowListOsnDopSKU(2, reportDataSet.tblDopSKU);
                            //
                            LockCompetPairPanelEdit();
                            //
                            ActionControlUpdate();
                        }
                        finally
                        {
                            WaitManager.StopWait();
                        };
                    };
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //Отобразить панель опций исходя из выбора меню
        private void navBarMenu_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tcReportOptions.SelectedTabPageIndex = Convert.ToInt32(e.Link.Item.Tag);

            switch (tcReportOptions.SelectedTabPageIndex)
            {
                case 0:  gvReportList.Focus();          //Список отчетов
                         break;
            };
        }

        //Конкурентные пары - Добавить
        private void btnAddCompetPair_Click(object sender, EventArgs e)
        {
            UnLockCompetPairPanelEdit();
            //
            LockCompetPairBtnEdit(false);
            //
            luSKUName1.Focus();
            //
            curEditType = EditType.etAdd;
        }

        //Конкурентные пары - Изменить
        private void btnEditCompetPair_Click(object sender, EventArgs e)
        {
            UnLockCompetPairPanelEdit();
            //
            LockCompetPairBtnEdit(false);
            //
            luSKUName1.EditValue = reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].SKU_ID1;
            luSKUName2.EditValue = reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].SKU_ID2;
            //
            luSKUName1.Focus();
            //
            curEditType = EditType.etEdit;
        }

        //Конкурентные пары - Удалить
        private void btnDelCompetPair_Click(object sender, EventArgs e)
        {
           DialogResult dresult= MessageBox.Show(WCCPConst.msgQuestionDeleteItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
           if (dresult == DialogResult.Yes)
           {
               using (OleDbCommand cmdDelCompetPair = new OleDbCommand("DW_TomasResearch_WbW_DelCompetPair", conn))
               {
                   cmdDelCompetPair.CommandTimeout = WCCPConst.CommandTimeout;
                   cmdDelCompetPair.Parameters.AddWithValue("@CompetPair_ID", reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].CompetPair_ID);
                   cmdDelCompetPair.CommandType = CommandType.StoredProcedure;
                   try
                   {
                       cmdDelCompetPair.ExecuteNonQuery();
                       reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].Delete();
                       reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.AcceptChanges();

                       curEditType = EditType.etNone;
                       LockCompetPairPanelEdit();
                       LockCompetPairBtnEdit(true);
                       ActionControlUpdate();
                   }
                   catch (Exception ex)
                   {
                       MessageBox.Show(WCCPConst.msgErrorAddRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   };
               };
           };
        }

        //Конкурентные пары - Удалить все
        private void btnDelAllCompetPair_Click(object sender, EventArgs e)
        {
            DialogResult dresult = MessageBox.Show(WCCPConst.msgQuestionDeleteAllItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dresult == DialogResult.Yes)
            {
                using (OleDbCommand cmdDelCompetPair = new OleDbCommand("DW_TomasResearch_WbW_DelCompetPair", conn))
                {
                    cmdDelCompetPair.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDelCompetPair.Parameters.AddWithValue("@CompetPair_ID", DBNull.Value);
                    cmdDelCompetPair.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        cmdDelCompetPair.ExecuteNonQuery();
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.Clear();

                        curEditType = EditType.etNone;
                        LockCompetPairPanelEdit();
                        LockCompetPairBtnEdit(true);
                        ActionControlUpdate();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorAddRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            };
        }

        //Конкурентные пары - По умолчанию
        private void btnDefaultCompetPair_Click(object sender, EventArgs e)
        {
            DialogResult dresult = MessageBox.Show(WCCPConst.msgQuestionDefaultItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dresult == DialogResult.Yes)
            {
                CreateDefaultListCompetPair();
                ActionControlUpdate();
            };
        }

        //Конкурентные пары - Сохранить
        private void btnSaveCompetPair_Click(object sender, EventArgs e)
        {
            if (luSKUName1.EditValue == null)
            {
                MessageBox.Show(WCCPConst.msgErrorSKU1IsNull, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                luSKUName1.Focus();
                return;
            };

            if (luSKUName2.EditValue == null)
            {
                MessageBox.Show(WCCPConst.msgErrorSKU2IsNull, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                luSKUName2.Focus();
                return;
            };

            if (curEditType == EditType.etAdd)
            {
                using (OleDbCommand cmdAddCompetPair = new OleDbCommand("DW_TomasResearch_WbW_PutCompetPair", conn))
                {
                    cmdAddCompetPair.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdAddCompetPair.Parameters.AddWithValue("@CompetPair_ID", DBNull.Value);
                    cmdAddCompetPair.Parameters.AddWithValue("@SKU_ID1", Convert.ToInt32(luSKUName1.EditValue));
                    cmdAddCompetPair.Parameters.AddWithValue("@SKU_ID2", Convert.ToInt32(luSKUName2.EditValue));
                    OleDbParameter outInserted_ID = new OleDbParameter("@Inserted_ID", OleDbType.Integer);
                    outInserted_ID.Direction = ParameterDirection.Output;
                    cmdAddCompetPair.Parameters.Add(outInserted_ID);
                    cmdAddCompetPair.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        cmdAddCompetPair.ExecuteNonQuery();

                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.AddtblDW_TomasResearch_WbW_ListCompetPairRow(Convert.ToInt32(cmdAddCompetPair.Parameters["@Inserted_ID"].Value),
                            Convert.ToInt32(luSKUName1.EditValue), luSKUName1.Text, Convert.ToInt32(luSKUName2.EditValue), luSKUName2.Text);
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.AcceptChanges();

                        curEditType = EditType.etNone;
                        LockCompetPairPanelEdit();
                        LockCompetPairBtnEdit(true);
                        ActionControlUpdate();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorAddRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            };
            //
            if (curEditType == EditType.etEdit)
            {
                using (OleDbCommand cmdAddCompetPair = new OleDbCommand("DW_TomasResearch_WbW_PutCompetPair", conn))
                {
                    cmdAddCompetPair.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdAddCompetPair.Parameters.AddWithValue("@CompetPair_ID", reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].CompetPair_ID);
                    cmdAddCompetPair.Parameters.AddWithValue("@SKU_ID1", Convert.ToInt32(luSKUName1.EditValue));
                    cmdAddCompetPair.Parameters.AddWithValue("@SKU_ID2", Convert.ToInt32(luSKUName2.EditValue));
                    OleDbParameter outInserted_ID = new OleDbParameter("@Inserted_ID", OleDbType.Integer);
                    outInserted_ID.Direction = ParameterDirection.Output;
                    cmdAddCompetPair.Parameters.Add(outInserted_ID);
                    cmdAddCompetPair.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        cmdAddCompetPair.ExecuteNonQuery();

                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].BeginEdit();
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].SKU_ID1 = Convert.ToInt32(luSKUName1.EditValue);
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].SKU_NAME1 = luSKUName1.Text;
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].SKU_ID2 = Convert.ToInt32(luSKUName2.EditValue);
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].SKU_NAME2 = luSKUName2.Text;
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair[bsCompetPair.Position].EndEdit();
                        reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.AcceptChanges();

                        curEditType = EditType.etNone;
                        LockCompetPairPanelEdit();
                        LockCompetPairBtnEdit(true);
                        ActionControlUpdate();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            };
        }

        //Конкурентные пары - Отмена
        private void btnCancelCompetPair_Click(object sender, EventArgs e)
        {
            LockCompetPairPanelEdit();
            LockCompetPairBtnEdit(true);
            ActionControlUpdate();
        }

        //Получить по умолчанию список Relative PTC лимитов
        private void btnDefaultRelativePTCLimits_Click(object sender, EventArgs e)
        {
          DialogResult dresult = MessageBox.Show(WCCPConst.msgQuestionDefaultItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (dresult == DialogResult.Yes)
          {
             CreateDefaultRelativePTCLimitsList();
          };
        }

        private void CreateDefaultRelativePTCLimitsList()
        {
            using (OleDbCommand cmdRelativeRPTLimits = new OleDbCommand("DW_TomasResearch_WbW_ListRPTC_Limits", Conn))
            {
                cmdRelativeRPTLimits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdRelativeRPTLimits.Parameters.AddWithValue("@LimitType_ID", 1);
                cmdRelativeRPTLimits.Parameters.AddWithValue("@ChanelType_ID", ((ChanelName == "On-trade") ? 1 : 2));
                cmdRelativeRPTLimits.CommandType = CommandType.StoredProcedure;
                try
                {
                  using (OleDbCommand cmdDW_TomasResearch_WbW_DefaultRPTC_Limits = new OleDbCommand("DW_TomasResearch_WbW_DefaultRPTC_Limits", Conn))
                  {
                      cmdDW_TomasResearch_WbW_DefaultRPTC_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                      cmdDW_TomasResearch_WbW_DefaultRPTC_Limits.Parameters.AddWithValue("@Wave_ID", WaveId);
                      cmdDW_TomasResearch_WbW_DefaultRPTC_Limits.Parameters.AddWithValue("@Region_ID", RegionId);
                      cmdDW_TomasResearch_WbW_DefaultRPTC_Limits.Parameters.AddWithValue("@Channel", ChanelName);
                      cmdDW_TomasResearch_WbW_DefaultRPTC_Limits.CommandType = CommandType.StoredProcedure;

                      cmdDW_TomasResearch_WbW_DefaultRPTC_Limits.ExecuteNonQuery();

                      using (OleDbDataAdapter daRelativeRPTLimits = new OleDbDataAdapter(cmdRelativeRPTLimits))
                      {
                        reportDataSet.tblDW_TomasResearch_WbW_RPTC_Limits.Clear();
                        daRelativeRPTLimits.Fill(reportDataSet.tblDW_TomasResearch_WbW_RPTC_Limits);
                      };
                  };
                }
                catch (Exception ex)
                {
                  MessageBox.Show(WCCPConst.msgErrorCreateRelativeRPTLimits + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Изменить запись для Relative PTC лимитов
        private void gvRelativePTCLimits_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            reportDataSet.tblDW_TomasResearch_WbW_RPTC_Limits.AcceptChanges();
            using(OleDbCommand cmdDW_TomasResearch_WbW_EditRPTC_Limits = new OleDbCommand("DW_TomasResearch_WbW_EditRPTC_Limits", Conn))
            {
                cmdDW_TomasResearch_WbW_EditRPTC_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_EditRPTC_Limits.Parameters.AddWithValue("@RPTC_Limit_ID", reportDataSet.tblDW_TomasResearch_WbW_RPTC_Limits[bsRelativeRPTLimits.Position].RPTC_Limit_ID);
                cmdDW_TomasResearch_WbW_EditRPTC_Limits.Parameters.AddWithValue("@LimitName", reportDataSet.tblDW_TomasResearch_WbW_RPTC_Limits[bsRelativeRPTLimits.Position].LimitName);
                cmdDW_TomasResearch_WbW_EditRPTC_Limits.Parameters.AddWithValue("@Limit1", reportDataSet.tblDW_TomasResearch_WbW_RPTC_Limits[bsRelativeRPTLimits.Position].Limit1);
                cmdDW_TomasResearch_WbW_EditRPTC_Limits.Parameters.AddWithValue("@Limit2", reportDataSet.tblDW_TomasResearch_WbW_RPTC_Limits[bsRelativeRPTLimits.Position].Limit2);
                OleDbParameter outInserted_ID = new OleDbParameter("@Inserted_ID", OleDbType.Integer);
                outInserted_ID.Direction = ParameterDirection.Output;
                cmdDW_TomasResearch_WbW_EditRPTC_Limits.Parameters.Add(outInserted_ID);
                cmdDW_TomasResearch_WbW_EditRPTC_Limits.CommandType = CommandType.StoredProcedure;

                try
                {
                    cmdDW_TomasResearch_WbW_EditRPTC_Limits.ExecuteNonQuery();
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Получить по умолчанию список СКЮ
        private void btnDefaultSKU_Click(object sender, EventArgs e)
        {
            DialogResult dresult = MessageBox.Show(WCCPConst.msgQuestionDefaultItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dresult == DialogResult.Yes)
            {
                CreateDefaultSKUList();
            };
        }

        //Создать по умолчанию список СКЮ
        private void CreateDefaultSKUList()
        {
            using (OleDbCommand cmdListSKU = new OleDbCommand("DW_TomasResearch_WbW_ListSKU", Conn))
            {
                cmdListSKU.CommandTimeout = WCCPConst.CommandTimeout;
                cmdListSKU.CommandType = CommandType.StoredProcedure;
                try
                {
                    using (OleDbCommand cmdDW_TomasResearch_WbW_DefaultSKU = new OleDbCommand("DW_TomasResearch_WbW_DefaultSKU", conn))
                    {
                        cmdDW_TomasResearch_WbW_DefaultSKU.CommandTimeout = WCCPConst.CommandTimeout;
                        cmdDW_TomasResearch_WbW_DefaultSKU.Parameters.AddWithValue("@Region_ID", RegionId);
                        cmdDW_TomasResearch_WbW_DefaultSKU.Parameters.AddWithValue("@Channel", ChanelName);
                        cmdDW_TomasResearch_WbW_DefaultSKU.CommandType = CommandType.StoredProcedure;

                        cmdDW_TomasResearch_WbW_DefaultSKU.ExecuteNonQuery();

                        using (OleDbDataAdapter daListSKU = new OleDbDataAdapter(cmdListSKU))
                        {
                            reportDataSet.tblDW_TomasResearch_WbW_SKU.Clear();
                            daListSKU.Fill(reportDataSet.tblDW_TomasResearch_WbW_SKU);
                        };
                    };
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorCreateSKUList + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };            
        }

        //Изменить запись для списка СКЮ
        private void gvSKU_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            reportDataSet.tblDW_TomasResearch_WbW_SKU.AcceptChanges();
            using (OleDbCommand cmdDW_TomasResearch_WbW_PutSKU = new OleDbCommand("DW_TomasResearch_WbW_PutSKU", Conn))
            {
                cmdDW_TomasResearch_WbW_PutSKU.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_PutSKU.Parameters.AddWithValue("@ID", reportDataSet.tblDW_TomasResearch_WbW_SKU[bsSKUList.Position].ID);
                cmdDW_TomasResearch_WbW_PutSKU.Parameters.AddWithValue("@isChecked", reportDataSet.tblDW_TomasResearch_WbW_SKU[bsSKUList.Position].isChecked);
                cmdDW_TomasResearch_WbW_PutSKU.CommandType = CommandType.StoredProcedure;

                try
                {
                    cmdDW_TomasResearch_WbW_PutSKU.ExecuteNonQuery();
                    
                }
                catch (Exception ex)
                {
                   MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Создать список PTC лимитов по умолчанию для всех СКЮ
        private void CreateDefaultSKUPTCLimits()
        {
            using (OleDbCommand cmdDW_TomasResearch_WbW_DefaultPTC_Limits = new OleDbCommand("DW_TomasResearch_WbW_DefaultPTC_Limits", Conn))
            {
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@Wave_ID", WaveId);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@Region_ID", RegionId);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@Channel", ChanelName);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@SKU_ID", DBNull.Value);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.CommandType = CommandType.StoredProcedure;
                try
                {
                  cmdDW_TomasResearch_WbW_DefaultPTC_Limits.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorCreateDefaultSKUPTCLimit + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Отобразить список лимитов для конкретного СКЮ
        private void ShowSKUPTCLimit(int SKU_Id)
        {
            using (OleDbCommand cmdDW_TomasResearch_WbW_ListPTC_Limits = new OleDbCommand("DW_TomasResearch_WbW_ListPTC_Limits", Conn))
            {
                cmdDW_TomasResearch_WbW_ListPTC_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_ListPTC_Limits.Parameters.AddWithValue("@SKU_ID", SKU_Id);
                cmdDW_TomasResearch_WbW_ListPTC_Limits.Parameters.AddWithValue("@ChanelType_ID", ((ChanelName == "On-trade") ? 1 : 2));
                cmdDW_TomasResearch_WbW_ListPTC_Limits.CommandType = CommandType.StoredProcedure;

                try
                {
                    using (OleDbDataAdapter daPTC_Limits = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_ListPTC_Limits))
                    {
                      reportDataSet.tblDW_TomasResearch_WbW_PTC_Limits.Clear();
                      daPTC_Limits.Fill(reportDataSet.tblDW_TomasResearch_WbW_PTC_Limits);
                    };
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorCreateSKUPTCLimit + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };            
        }

        //Отобразить список лимитов для конкретного СКЮ
        private void bsSKUPTCLimit_PositionChanged(object sender, EventArgs e)
        {
            ShowSKUPTCLimit(reportDataSet.tblDW_TomasResearch_WbW_SKU[bsSKUPTCLimit.Position].SKU_ID);
        }

        //Отобразить список РТС лимитов по умолчанию для СКЮ
        private void btnDefaultSKUPTCLimit_Click(object sender, EventArgs e)
        {
            DialogResult dresult = MessageBox.Show(WCCPConst.msgQuestionDefaultItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dresult == DialogResult.Yes)
            {
                if (reportDataSet.tblDW_TomasResearch_WbW_SKU.Rows.Count > 0)
                {
                  CreateDefaultSKUPTCLimit(reportDataSet.tblDW_TomasResearch_WbW_SKU[bsSKUPTCLimit.Position].SKU_ID);
                  ShowSKUPTCLimit(reportDataSet.tblDW_TomasResearch_WbW_SKU[bsSKUPTCLimit.Position].SKU_ID);
                }
                else
                {
                  MessageBox.Show(WCCPConst.msgWarningCreateSKUPTCLimit, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                };
            };
        }

        //Создать список PTC лимитов по умолчанию для СКЮ
        private void CreateDefaultSKUPTCLimit(int SKU_Id)
        {
            using (OleDbCommand cmdDW_TomasResearch_WbW_DefaultPTC_Limits = new OleDbCommand("DW_TomasResearch_WbW_DefaultPTC_Limits", Conn))
            {
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@Wave_ID", WaveId);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@Region_ID", RegionId);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@Channel", ChanelName);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.Parameters.AddWithValue("@SKU_ID", SKU_Id);
                cmdDW_TomasResearch_WbW_DefaultPTC_Limits.CommandType = CommandType.StoredProcedure;
                
                try
                {
                    cmdDW_TomasResearch_WbW_DefaultPTC_Limits.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorCreateDefaultSKUPTCLimit + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Изменить запись для PTC лимитов конкретного СКЮ
        private void gvPTCLimit_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            reportDataSet.tblDW_TomasResearch_WbW_PTC_Limits.AcceptChanges();
            using (OleDbCommand cmdDW_TomasResearch_WbW_EditPTC_Limits = new OleDbCommand("DW_TomasResearch_WbW_EditPTC_Limits", conn))
            {
                cmdDW_TomasResearch_WbW_EditPTC_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_EditPTC_Limits.Parameters.AddWithValue("@PTC_Limit_ID", reportDataSet.tblDW_TomasResearch_WbW_PTC_Limits[bsPTCLimit.Position].PTC_Limit_ID);
                cmdDW_TomasResearch_WbW_EditPTC_Limits.Parameters.AddWithValue("@LimitName", reportDataSet.tblDW_TomasResearch_WbW_PTC_Limits[bsPTCLimit.Position].LimitName);
                cmdDW_TomasResearch_WbW_EditPTC_Limits.Parameters.AddWithValue("@Limit1", reportDataSet.tblDW_TomasResearch_WbW_PTC_Limits[bsPTCLimit.Position].Limit1);
                cmdDW_TomasResearch_WbW_EditPTC_Limits.Parameters.AddWithValue("@Limit2", reportDataSet.tblDW_TomasResearch_WbW_PTC_Limits[bsPTCLimit.Position].Limit2);
                cmdDW_TomasResearch_WbW_EditPTC_Limits.CommandType = CommandType.StoredProcedure;

                try
                {
                    cmdDW_TomasResearch_WbW_EditPTC_Limits.ExecuteNonQuery();                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Создать список основных и дополнительных СКЮ по умолчанию
        private void CreateDefaultOsnDopSKU()
        {
            using (OleDbCommand cmdDW_TomasResearch_WbW_DefaultOsnDopSKU = new OleDbCommand("DW_TomasResearch_WbW_DefaultOsnDopSKU", conn))
            {
                cmdDW_TomasResearch_WbW_DefaultOsnDopSKU.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_DefaultOsnDopSKU.Parameters.AddWithValue("@Wave_ID", WaveId);
                cmdDW_TomasResearch_WbW_DefaultOsnDopSKU.Parameters.AddWithValue("@Region_ID", RegionId);
                cmdDW_TomasResearch_WbW_DefaultOsnDopSKU.Parameters.AddWithValue("@Channel", ChanelName);
                cmdDW_TomasResearch_WbW_DefaultOsnDopSKU.CommandType = CommandType.StoredProcedure;
                try
                {
                    cmdDW_TomasResearch_WbW_DefaultOsnDopSKU.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorCreateDefaultOsnDopSKU + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Отобразить список основных и дополнительных СКЮ
        private void ShowListOsnDopSKU(int Type_ID, System.Data.DataTable dt)
        {
            using (OleDbCommand cmdDW_TomasResearch_WbW_ListOsnDopSKU = new OleDbCommand("DW_TomasResearch_WbW_ListOsnDopSKU", Conn))
            {
                cmdDW_TomasResearch_WbW_ListOsnDopSKU.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_ListOsnDopSKU.Parameters.AddWithValue("@Type_ID", Type_ID);
                cmdDW_TomasResearch_WbW_ListOsnDopSKU.CommandType = CommandType.StoredProcedure;
                try
                {
                    using (OleDbDataAdapter daListOsnDopSKU = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_ListOsnDopSKU))
                    {
                        dt.Clear();
                        daListOsnDopSKU.Fill(dt);
                    };
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorShowListOsnDopSKU + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Отобразить список основных и дополнительных СКЮ по умолчанию
        private void btnDefaultBasicAndAddSKU_Click(object sender, EventArgs e)
        {
            DialogResult dresult = MessageBox.Show(WCCPConst.msgQuestionDefaultItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dresult == DialogResult.Yes)
            {
                CreateDefaultOsnDopSKU();
                //для не разнесенных скю
                ShowListOsnDopSKU(3, reportDataSet.tblSKUAll);
                //для основных скю
                ShowListOsnDopSKU(1, reportDataSet.tblOsnSKU);
                //для доп. скю
                ShowListOsnDopSKU(2, reportDataSet.tblDopSKU);
            };
        }

        //Отобразить список PTC Shift лимитов по умолчанию
        private void btnDefaultPTCShiftLimits_Click(object sender, EventArgs e)
        {
            DialogResult dresult = MessageBox.Show(WCCPConst.msgQuestionDefaultItem, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dresult == DialogResult.Yes)
            {
                CreateDefaultPTCShiftLimits();
                ShowListPTCShiftLimit();
            };
            gvPTCShiftLimits.Focus();
        }

        //Создать список PTC Shift лимитов по умолчанию
        private void CreateDefaultPTCShiftLimits()
        {
            using (OleDbCommand cmdDW_TomasResearch_WbW_DefaultPTCShift_Limits = new OleDbCommand("DW_TomasResearch_WbW_DefaultPTCShift_Limits", Conn))
            {
                cmdDW_TomasResearch_WbW_DefaultPTCShift_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_DefaultPTCShift_Limits.Parameters.AddWithValue("@Wave_ID", WaveId);
                cmdDW_TomasResearch_WbW_DefaultPTCShift_Limits.Parameters.AddWithValue("@Region_ID", RegionId);
                cmdDW_TomasResearch_WbW_DefaultPTCShift_Limits.Parameters.AddWithValue("@Channel", ChanelName);
                cmdDW_TomasResearch_WbW_DefaultPTCShift_Limits.CommandType = CommandType.StoredProcedure;
                try
                {
                    cmdDW_TomasResearch_WbW_DefaultPTCShift_Limits.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorCreateDefaultPTCShiftLimit + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Отобразить список PTC Shift лимитов
        private void ShowListPTCShiftLimit()
        {
            using (OleDbCommand cmdDW_TomasResearch_WbW_ListPTCShift_Limits = new OleDbCommand("DW_TomasResearch_WbW_ListPTCShift_Limits", Conn))
            {
                cmdDW_TomasResearch_WbW_ListPTCShift_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_ListPTCShift_Limits.Parameters.AddWithValue("@LimitType_ID", 3);
                cmdDW_TomasResearch_WbW_ListPTCShift_Limits.Parameters.AddWithValue("@ChanelType_ID", ((ChanelName == "On-trade") ? 1 : 2));
                cmdDW_TomasResearch_WbW_ListPTCShift_Limits.CommandType = CommandType.StoredProcedure;
                try
                {
                    using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_ListPTCShift_Limits))
                   {
                       reportDataSet.tblDW_TomasResearch_WbW_PTCShift_Limits.Clear();
                       da.Fill(reportDataSet.tblDW_TomasResearch_WbW_PTCShift_Limits);
                   };
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorShowListPTCShiftLimit + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Изменить запись для PTC Shift лимитов 
        private void gvPTCShiftLimits_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            reportDataSet.tblDW_TomasResearch_WbW_PTCShift_Limits.AcceptChanges();
            using (OleDbCommand cmdDW_TomasResearch_WbW_EditPTCShift_Limits = new OleDbCommand("DW_TomasResearch_WbW_EditPTCShift_Limits", Conn))
            {
                cmdDW_TomasResearch_WbW_EditPTCShift_Limits.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_WbW_EditPTCShift_Limits.Parameters.AddWithValue("@PTCShift_Limit_ID", reportDataSet.tblDW_TomasResearch_WbW_PTCShift_Limits[bsPTCShiftLimit.Position].PTCShift_Limit_ID);
                cmdDW_TomasResearch_WbW_EditPTCShift_Limits.Parameters.AddWithValue("@LimitName", reportDataSet.tblDW_TomasResearch_WbW_PTCShift_Limits[bsPTCShiftLimit.Position].LimitName);
                cmdDW_TomasResearch_WbW_EditPTCShift_Limits.Parameters.AddWithValue("@Limit1", reportDataSet.tblDW_TomasResearch_WbW_PTCShift_Limits[bsPTCShiftLimit.Position].Limit1);
                cmdDW_TomasResearch_WbW_EditPTCShift_Limits.Parameters.AddWithValue("@Limit2", reportDataSet.tblDW_TomasResearch_WbW_PTCShift_Limits[bsPTCShiftLimit.Position].Limit2);
                cmdDW_TomasResearch_WbW_EditPTCShift_Limits.CommandType = CommandType.StoredProcedure;

                try
                {
                    cmdDW_TomasResearch_WbW_EditPTCShift_Limits.ExecuteNonQuery();
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };            
        }

        //Добавить в основные СКЮ
        private void btnInsertToBasicSKU_Click(object sender, EventArgs e)
        {
            if (reportDataSet.tblSKUAll.Rows.Count > 0)
            {
                using (OleDbCommand cmdDW_TomasResearch_WbW_PutOsnDopSKU = new OleDbCommand("DW_TomasResearch_WbW_PutOsnDopSKU", Conn))
                {
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@SKU_ID", reportDataSet.tblSKUAll[bsSKUAll.Position].SKU_ID);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@Type_ID", 1);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        cmdDW_TomasResearch_WbW_PutOsnDopSKU.ExecuteNonQuery();

                        reportDataSet.tblOsnSKU.AddtblOsnSKURow(reportDataSet.tblSKUAll[bsSKUAll.Position].SKU_ID, reportDataSet.tblSKUAll[bsSKUAll.Position].SKU_NAME);
                        reportDataSet.tblOsnSKU.AcceptChanges();

                        reportDataSet.tblSKUAll.RemovetblSKUAllRow(reportDataSet.tblSKUAll[bsSKUAll.Position]);
                        reportDataSet.tblSKUAll.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            }
            else
            {
                MessageBox.Show(WCCPConst.msgWarningNotItem, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            };
        }

        //Удалить из основных СКЮ
        private void btnRemoveFromBasicSKU_Click(object sender, EventArgs e)
        {
            if (reportDataSet.tblOsnSKU.Rows.Count > 0)
            {
                using (OleDbCommand cmdDW_TomasResearch_WbW_PutOsnDopSKU = new OleDbCommand("DW_TomasResearch_WbW_PutOsnDopSKU", Conn))
                {
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@SKU_ID", reportDataSet.tblOsnSKU[bsOsnSKU.Position].SKU_ID);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@Type_ID", 3);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        cmdDW_TomasResearch_WbW_PutOsnDopSKU.ExecuteNonQuery();

                        reportDataSet.tblSKUAll.AddtblSKUAllRow(reportDataSet.tblOsnSKU[bsOsnSKU.Position].SKU_ID, reportDataSet.tblOsnSKU[bsOsnSKU.Position].SKU_NAME);
                        reportDataSet.tblSKUAll.AcceptChanges();

                        reportDataSet.tblOsnSKU.RemovetblOsnSKURow(reportDataSet.tblOsnSKU[bsOsnSKU.Position]);
                        reportDataSet.tblOsnSKU.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            }
            else
            {
                MessageBox.Show(WCCPConst.msgWarningNotItem, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            };
        }

        //Добавить в дополнительные СКЮ
        private void btnInsertToAdvancedSKU_Click(object sender, EventArgs e)
        {
            if (reportDataSet.tblSKUAll.Rows.Count > 0)
            {
                using (OleDbCommand cmdDW_TomasResearch_WbW_PutOsnDopSKU = new OleDbCommand("DW_TomasResearch_WbW_PutOsnDopSKU", Conn))
                {
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@SKU_ID", reportDataSet.tblSKUAll[bsSKUAll.Position].SKU_ID);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@Type_ID", 2);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        cmdDW_TomasResearch_WbW_PutOsnDopSKU.ExecuteNonQuery();

                        reportDataSet.tblDopSKU.AddtblDopSKURow(reportDataSet.tblSKUAll[bsSKUAll.Position].SKU_ID, reportDataSet.tblSKUAll[bsSKUAll.Position].SKU_NAME);
                        reportDataSet.tblDopSKU.AcceptChanges();

                        reportDataSet.tblSKUAll.RemovetblSKUAllRow(reportDataSet.tblSKUAll[bsSKUAll.Position]);
                        reportDataSet.tblSKUAll.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            }
            else
            {
                MessageBox.Show(WCCPConst.msgWarningNotItem, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            };
        }

        //Удалить из дополнительных СКЮ
        private void btnRemoveFromAdvancedSKU_Click(object sender, EventArgs e)
        {
            if (reportDataSet.tblDopSKU.Rows.Count > 0)
            {
                using (OleDbCommand cmdDW_TomasResearch_WbW_PutOsnDopSKU = new OleDbCommand("DW_TomasResearch_WbW_PutOsnDopSKU", Conn))
                {
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@SKU_ID", reportDataSet.tblDopSKU[bsDopSKU.Position].SKU_ID);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.Parameters.AddWithValue("@Type_ID", 3);
                    cmdDW_TomasResearch_WbW_PutOsnDopSKU.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        cmdDW_TomasResearch_WbW_PutOsnDopSKU.ExecuteNonQuery();

                        reportDataSet.tblSKUAll.AddtblSKUAllRow(reportDataSet.tblDopSKU[bsDopSKU.Position].SKU_ID, reportDataSet.tblDopSKU[bsDopSKU.Position].SKU_NAME);
                        reportDataSet.tblSKUAll.AcceptChanges();

                        reportDataSet.tblDopSKU.RemovetblDopSKURow(reportDataSet.tblDopSKU[bsDopSKU.Position]);
                        reportDataSet.tblDopSKU.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(WCCPConst.msgErrorEditRecord + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    };
                };
            }
            else
            {
                MessageBox.Show(WCCPConst.msgWarningNotItem, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            };
        }

        //Создать отчеты
        private void btnCreateReports_Click(object sender, EventArgs e)
        {
            int countReport = 0;

            for (int i = tcReport.TabPages.Count-1; i > 0; --i)
            {
              tcReport.TabPages[i].Dispose();
            };

            foreach (DataRow rowReport in reportDataSet.tblReportList.Rows)
            {
                if (Convert.ToBoolean(rowReport["Report_Checked"]) == true)
                {
                    ++countReport;
                }
            };

            WaitManager.StartWait();
            try
            {
                foreach (DataRow rowReport in reportDataSet.tblReportList.Rows)
                {
                    if (Convert.ToBoolean(rowReport["Report_Checked"]) == true)
                    {
                        switch (Convert.ToInt32(rowReport["Report_Id"]))
                        {
                            //PTC Relative
                            case 1:
                            case 14:
                                {
                                    int heightControl = 640;
                                    int y = 1;

                                    try
                                    {
                                        XtraTabPage newRelativePriceToConsumerTabReport = new XtraTabPage();
                                        
                                        newRelativePriceToConsumerTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                        RelativePriceToConsumerReport reportRelativePriceToConsumer = new RelativePriceToConsumerReport();
                                        reportRelativePriceToConsumer.Dock = DockStyle.Fill;
                                        if (reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.Rows.Count > 0)
                                        {
                                          foreach (DataRow rowCompetPair in reportDataSet.tblDW_TomasResearch_WbW_ListCompetPair.Rows)
                                          {
                                            RelativePriceToConsumerContext relativePriceToConsumerReportContext = new RelativePriceToConsumerContext();
                                            relativePriceToConsumerReportContext.ShowData(WaveId, "PTC (UAH/SKU)", Convert.ToString(rowCompetPair["SKU_NAME1"]), Convert.ToString(rowCompetPair["SKU_NAME2"]), ChanelName);
                                            relativePriceToConsumerReportContext.Location = new System.Drawing.Point(1, y);
                                            reportRelativePriceToConsumer.RelativePriceToConsumerReportScrollableControl.Controls.Add(relativePriceToConsumerReportContext);
                                            y = y + (heightControl + 25);
                                          };
                                            
                                        }
                                         else
                                         {
                                           System.Windows.Forms.Label lbMessage = new System.Windows.Forms.Label();
                                           lbMessage.Text = "Нет данных для построения отчета";
                                           lbMessage.Font = new System.Drawing.Font("Tahoma", 9, FontStyle.Bold);
                                           lbMessage.TextAlign = ContentAlignment.MiddleCenter;
                                           lbMessage.Dock = DockStyle.Fill;
                                           reportRelativePriceToConsumer.RelativePriceToConsumerReportScrollableControl.Controls.Add(lbMessage);
                                         };
                                         
                                         newRelativePriceToConsumerTabReport.Controls.Add(reportRelativePriceToConsumer);
                                         tcReport.TabPages.Add(newRelativePriceToConsumerTabReport);
                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //PTC/PTC Shift 
                            case 2:
                            case 15:
                                {
                                    int heightControl = 370;
                                    int y = 1;

                                    try
                                    {
                                        XtraTabPage newPTCTabReport = new XtraTabPage();
                                        newPTCTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                        PTCReport reportPTC = new PTCReport();
                                        reportPTC.Dock = DockStyle.Fill;

                                        if (reportDataSet.tblDW_TomasResearch_WbW_SKU.Rows.Count > 0)
                                        {
                                            foreach (DataRow rowSKU in reportDataSet.tblDW_TomasResearch_WbW_SKU.Rows)
                                            {
                                                if (Convert.ToBoolean(rowSKU["isChecked"]))
                                                {
                                                    PTCContext PTCReportContext = new PTCContext();
                                                    PTCReportContext.ShowData(WaveId, WaveName, Wave1Id, Wave1Name, Wave2Id, Wave2Name, "PTC (UAH/SKU)", Convert.ToString(rowSKU["SKU"]), ChanelName);
                                                    PTCReportContext.Location = new System.Drawing.Point(1, y);
                                                    reportPTC.PTCReportScrollableControl.Controls.Add(PTCReportContext);
                                                    y = y + (heightControl + 25);
                                                };
                                            };
                                        }
                                        else
                                        {
                                            System.Windows.Forms.Label lbMessage = new System.Windows.Forms.Label();
                                            lbMessage.Text = "Нет данных для построения отчета";
                                            lbMessage.Font = new System.Drawing.Font("Tahoma", 9, FontStyle.Bold);
                                            lbMessage.TextAlign = ContentAlignment.MiddleCenter;
                                            lbMessage.Dock = DockStyle.Fill;
                                            reportPTC.PTCReportScrollableControl.Controls.Add(lbMessage);
                                        };
                                        newPTCTabReport.Controls.Add(reportPTC);
                                        tcReport.TabPages.Add(newPTCTabReport);
                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //Distribution
                            case  4:
                            case 17:
                                {
                                    try
                                    {
                                        XtraTabPage newDistributionTabReport = new XtraTabPage();
                                        newDistributionTabReport.Text = Convert.ToString("Distribution");
                                        DistributionReport reportDistribution = new DistributionReport();
                                        reportDistribution.Dock = DockStyle.Fill;
                                        DistributionContext DistributionReportContext = new DistributionContext();
                                        DistributionReportContext.Dock = DockStyle.Fill;
                                        DistributionReportContext.ShowData(ChanelName, WaveId, WaveName, Wave2Id, Wave2Name, Wave1Id, Wave1Name);
                                        reportDistribution.DistributionReportScrollableControl.Controls.Add(DistributionReportContext);
                                        newDistributionTabReport.Controls.Add(reportDistribution);
                                        tcReport.TabPages.Add(newDistributionTabReport);


                                        XtraTabPage newEvolutionTabReport = new XtraTabPage();
                                        newEvolutionTabReport.Text = Convert.ToString("Evolution");
                                        EvolutionReport reportEvolution = new EvolutionReport();
                                        reportEvolution.Dock = DockStyle.Fill;
                                        EvolutionContext EvolutionReportContext = new EvolutionContext();
                                        EvolutionReportContext.Dock = DockStyle.Fill;
                                        EvolutionReportContext.ShowData(ChanelName, WaveId, WaveName, Wave2Id, Wave2Name, Wave1Id, Wave1Name);
                                        reportEvolution.EvolutionReportScrollableControl.Controls.Add(EvolutionReportContext);
                                        newEvolutionTabReport.Controls.Add(reportEvolution);
                                        tcReport.TabPages.Add(newEvolutionTabReport);

                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };

                                    break;
                                };
                            //SKU Relations
                            case 5:
                            case 18:
                                {
                                    try
                                    {
                                        XtraTabPage newSKURelationsTabReport = new XtraTabPage();
                                        newSKURelationsTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                        SKURelationsReport reportSKURelations = new SKURelationsReport();
                                        reportSKURelations.Dock = DockStyle.Fill;
                                        SKURelationsContext SKURelationsReportContext = new SKURelationsContext();
                                        SKURelationsReportContext.ShowData(ChanelName, WaveId, Convert.ToInt32(sedSKUInBevCount.Value));
                                        SKURelationsReportContext.Dock = DockStyle.Fill;
                                        reportSKURelations.SKURelationsReportScrollableControl.Controls.Add(SKURelationsReportContext);
                                        newSKURelationsTabReport.Controls.Add(reportSKURelations);
                                        tcReport.TabPages.Add(newSKURelationsTabReport);
                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //Mkt Overview
                            case 6:
                            case 19:
                                {
                                    int y = 1;

                                    try
                                    {
                                        XtraTabPage newMktOverviewTabReport = new XtraTabPage();
                                        newMktOverviewTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                        MktOverviewReport reportMktOverview = new MktOverviewReport();
                                        reportMktOverview.Dock = DockStyle.Fill;
                                        MktOverviewContext MktOverviewReportContext = new MktOverviewContext();
                                        MktOverviewReportContext.ShowData(ChanelName, WaveId, WaveName, Wave2Id, Wave2Name, Wave1Id, Wave1Name);
                                        MktOverviewReportContext.Location = new System.Drawing.Point(1, y);
                                        reportMktOverview.MktOverviewReportScrollableControl.Controls.Add(MktOverviewReportContext);
                                        newMktOverviewTabReport.Controls.Add(reportMktOverview);
                                        tcReport.TabPages.Add(newMktOverviewTabReport);
                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //Margin Shift
                            case 7:
                            case 20:
                                {   
                                    int heightControl = 135;
                                    int y = 1;

                                    XtraTabPage newMarginShiftTabReport = new XtraTabPage();
                                    newMarginShiftTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                    MarginShiftReport reportMarginShift = new MarginShiftReport();
                                    reportMarginShift.Dock = DockStyle.Fill;
                                    DataSet ds = new DataSet();

                                    try
                                    {
                                        using (OleDbCommand cmdDW_TomasResearch_WbW_Margin_Shift = new OleDbCommand((ChanelName == "On-trade" ? "DW_TomasResearch_WbW_Margin_Shift_ONTRADE" : "DW_TomasResearch_WbW_Margin_Shift"), WccpUIControl.Conn))
                                        {
                                          cmdDW_TomasResearch_WbW_Margin_Shift.Parameters.AddWithValue("@Wave_ID", WaveId);
                                          cmdDW_TomasResearch_WbW_Margin_Shift.Parameters.AddWithValue("@prevWave_ID", Wave2Id);
                                          cmdDW_TomasResearch_WbW_Margin_Shift.Parameters.AddWithValue("@prevprevWave_ID", Wave1Id);
                                          cmdDW_TomasResearch_WbW_Margin_Shift.CommandType = CommandType.StoredProcedure;

                                          try
                                          {
                                              using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Margin_Shift))
                                              {
                                                  da.Fill(ds);
                                                  if (ds.Tables.Count > 0)
                                                  {
                                                      foreach (DataTable dt in ds.Tables)
                                                      {
                                                          MarginShiftContext MarginShiftReportContext = new MarginShiftContext();
                                                          MarginShiftReportContext.ShowData(dt);
                                                          MarginShiftReportContext.Height = heightControl;
                                                          MarginShiftReportContext.Location = new System.Drawing.Point(1, y);
                                                          reportMarginShift.MarginShiftReportScrollableControl.Controls.Add(MarginShiftReportContext);
                                                          y = y + (heightControl + 2);
                                                      };
                                                  }
                                                  else
                                                  {
                                                      System.Windows.Forms.Label lbMessage = new System.Windows.Forms.Label();
                                                      lbMessage.Text = "Нет данных для построения отчета";
                                                      lbMessage.Font = new System.Drawing.Font("Tahoma", 9, FontStyle.Bold);
                                                      lbMessage.TextAlign = ContentAlignment.MiddleCenter;
                                                      lbMessage.Dock = DockStyle.Fill;
                                                      reportMarginShift.MarginShiftReportScrollableControl.Controls.Add(lbMessage);
                                                  };
                                              };

                                              newMarginShiftTabReport.Controls.Add(reportMarginShift);
                                              tcReport.TabPages.Add(newMarginShiftTabReport);
                                          }
                                          catch (Exception ex)
                                          {
                                              MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                          };
                                           
                                        };

                                    }
                                    finally
                                    {
                                        ds.Dispose();
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //Mkt Overview (by segments)
                            case 28:
                            case 29:
                                {
                                    int tableIndex = 0;
                                    DataSet ds = new DataSet();
                                    try
                                    {
                                        using (OleDbCommand cmdDW_TomasResearch_WbW_Mkt_overview_by_segment = new OleDbCommand((ChanelName == "On-trade" ? "DW_TomasResearch_WbW_Mkt_overview_by_segment_ONTRADE" : "DW_TomasResearch_WbW_Mkt_overview_by_segment"), WccpUIControl.Conn))
                                        {
                                            cmdDW_TomasResearch_WbW_Mkt_overview_by_segment.Parameters.AddWithValue("@Wave_ID", WaveId);
                                            cmdDW_TomasResearch_WbW_Mkt_overview_by_segment.Parameters.AddWithValue("@prevWave_ID", Wave2Id);
                                            cmdDW_TomasResearch_WbW_Mkt_overview_by_segment.CommandType = CommandType.StoredProcedure;

                                            try
                                            {
                                                using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Mkt_overview_by_segment))
                                                {
                                                    da.Fill(ds);
                                                    if (ds.Tables.Count > 0)
                                                    {
                                                        foreach (DataTable dt in ds.Tables)
                                                        {
                                                            XtraTabPage newMktOverviewBySegmentsTabReport = new XtraTabPage();

                                                            newMktOverviewBySegmentsTabReport.Text = "Лист " + Convert.ToString(tableIndex);

                                                            if (tableIndex == 0)
                                                            {
                                                                newMktOverviewBySegmentsTabReport.Text = "Market owerview by segements " + WaveName;
                                                            };
                                                            if (tableIndex == 1)
                                                            {
                                                                newMktOverviewBySegmentsTabReport.Text = "Market owerview by segements " + Wave2Name;
                                                            };
                                                            if (tableIndex == 2)
                                                            {
                                                                newMktOverviewBySegmentsTabReport.Text = "Market owerview by segements " + Wave2Name + "-" + WaveName;
                                                            };

                                                            MktOverviewBySegmentsReport reportMktOverviewBySegments = new MktOverviewBySegmentsReport();
                                                            reportMktOverviewBySegments.Dock = DockStyle.Fill;

                                                            MktOverviewBySegmentsContext MktOverviewBySegmentsReportContext = new MktOverviewBySegmentsContext();
                                                            MktOverviewBySegmentsReportContext.Dock = DockStyle.Fill;
                                                            MktOverviewBySegmentsReportContext.ShowData(dt);
                                                            reportMktOverviewBySegments.MktOverviewBySegmentsReportScrollableControl.Controls.Add(MktOverviewBySegmentsReportContext);

                                                            newMktOverviewBySegmentsTabReport.Controls.Add(reportMktOverviewBySegments);
                                                            tcReport.TabPages.Add(newMktOverviewBySegmentsTabReport);

                                                            ++tableIndex;
                                                        };
                                                    }
                                                    else
                                                    {
                                                        XtraTabPage newMktOverviewBySegmentsTabReport = new XtraTabPage();
                                                        MktOverviewBySegmentsReport reportMktOverviewBySegments = new MktOverviewBySegmentsReport();
                                                        System.Windows.Forms.Label lbMessage = new System.Windows.Forms.Label();
                                                        lbMessage.Text = "Нет данных для построения отчета";
                                                        lbMessage.Font = new System.Drawing.Font("Tahoma", 9, FontStyle.Bold);
                                                        lbMessage.TextAlign = ContentAlignment.MiddleCenter;
                                                        lbMessage.Dock = DockStyle.Fill;
                                                        reportMktOverviewBySegments.MktOverviewBySegmentsReportScrollableControl.Controls.Add(lbMessage);
                                                        newMktOverviewBySegmentsTabReport.Controls.Add(reportMktOverviewBySegments);
                                                        tcReport.TabPages.Add(newMktOverviewBySegmentsTabReport);
                                                    };
                                                };

                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            };

                                        };

                                    }
                                    finally
                                    {
                                        ds.Dispose();
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //Brands_Entrance
                            case 13:
                            case 26:
                                {
                                    int heightControl = 165;
                                    int y = 1;

                                    XtraTabPage newBrandsEntranceTabReport = new XtraTabPage();
                                    newBrandsEntranceTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                    BrandsEntranceReport reportBrandsEntrance = new BrandsEntranceReport();
                                    reportBrandsEntrance.Dock = DockStyle.Fill;

                                    try
                                    {
                                        if (reportDataSet.tblDW_TomasResearch_WbW_SKU.Rows.Count > 0)
                                        {
                                            foreach (DataRow rowSKU in reportDataSet.tblDW_TomasResearch_WbW_SKU.Rows)
                                            {
                                                if (Convert.ToBoolean(rowSKU["isChecked"]))
                                                {
                                                    BrandsEntranceContext BrandsEntranceReportContext = new BrandsEntranceContext();
                                                    BrandsEntranceReportContext.ShowData(WaveId, Wave2Id, Convert.ToString(rowSKU["SKU"]), ChanelName);
                                                    BrandsEntranceReportContext.Location = new System.Drawing.Point(1, y);
                                                    reportBrandsEntrance.BrandsEntranceReportScrollableControl.Controls.Add(BrandsEntranceReportContext);
                                                    y = y + (heightControl + 2);
                                                };
                                            };

                                            newBrandsEntranceTabReport.Controls.Add(reportBrandsEntrance);
                                            tcReport.TabPages.Add(newBrandsEntranceTabReport);
                                        }
                                        else
                                        {
                                            System.Windows.Forms.Label lbMessage = new System.Windows.Forms.Label();
                                            lbMessage.Text = "Нет данных для построения отчета";
                                            lbMessage.Font = new System.Drawing.Font("Tahoma", 9, FontStyle.Bold);
                                            lbMessage.TextAlign = ContentAlignment.MiddleCenter;
                                            lbMessage.Dock = DockStyle.Fill;
                                            reportBrandsEntrance.BrandsEntranceReportScrollableControl.Controls.Add(lbMessage);
                                            newBrandsEntranceTabReport.Controls.Add(reportBrandsEntrance);
                                            tcReport.TabPages.Add(newBrandsEntranceTabReport);
                                        };
                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //CoolersAnalysis
                            case 9:
                            case 22:
                                {
                                    int y = 1;

                                    try
                                    {
                                        XtraTabPage newCoolersAnalysisTabReport = new XtraTabPage();
                                        newCoolersAnalysisTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                        CoolersAnalysisReport reportCoolersAnalysis = new CoolersAnalysisReport();
                                        reportCoolersAnalysis.Dock = DockStyle.Fill;
                                        CoolersAnalysisContext CoolersAnalysisReportContext = new CoolersAnalysisContext();
                                        CoolersAnalysisReportContext.ShowData(ChanelName, WaveId, Wave2Id);
                                        CoolersAnalysisReportContext.Location = new System.Drawing.Point(1, y);
                                        reportCoolersAnalysis.CoolersAnalysisReportScrollableControl.Controls.Add(CoolersAnalysisReportContext);
                                        newCoolersAnalysisTabReport.Controls.Add(reportCoolersAnalysis);
                                        tcReport.TabPages.Add(newCoolersAnalysisTabReport);
                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };
                            //Contracts
                            case 11:
                            case 24:
                                {
                                    int y = 1;

                                    try
                                    {
                                        XtraTabPage newContractsTabReport = new XtraTabPage();
                                        newContractsTabReport.Text = Convert.ToString(rowReport["Report_Name"]);
                                        ContractsReport reportContracts = new ContractsReport();
                                        reportContracts.Dock = DockStyle.Fill;
                                        ContractsContext ContractsReportContext = new ContractsContext();
                                        ContractsReportContext.ShowData(ChanelName, WaveId, WaveName, Wave2Id, Wave2Name);
                                        ContractsReportContext.Location = new System.Drawing.Point(1, y);
                                        reportContracts.ContractsReportScrollableControl.Controls.Add(ContractsReportContext);
                                        newContractsTabReport.Controls.Add(reportContracts);
                                        tcReport.TabPages.Add(newContractsTabReport);
                                    }
                                    finally
                                    {
                                        WaitManager.StopWait();
                                    };
                                    break;
                                };


                        };

                    };
                };
            }
            finally
            {
                WaitManager.StopWait();
            };

            //Если есть созданные отчеты, то переходим на первый созданный
            if (tcReport.TabPages.Count > 1)
            {
              tcReport.SelectedTabPageIndex = 1;
            }
            else
            {
                MessageBox.Show(WCCPConst.msgWarningNotReportChecked, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            };
        }

        //Выбрать отчет
        private void gvReportList_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column.FieldName == "Report_Checked")
            {
                reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Checked"] = !Convert.ToBoolean(reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Checked"]);
                reportDataSet.tblReportList.AcceptChanges();
                EnableOptionList(Convert.ToInt32(reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Id"]), Convert.ToBoolean(reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Checked"]));
            };
        }

        //Управление доступностью опций
        private void EnableOptionList(int ReportId, bool RptChacked)
        {
            using (OleDbCommand cmdDW_TomasResearch_ListOption = new OleDbCommand("DW_TomasResearch_ListOption", conn))
            {
                cmdDW_TomasResearch_ListOption.CommandTimeout = WCCPConst.CommandTimeout;
                cmdDW_TomasResearch_ListOption.Parameters.AddWithValue("@Report_ID", ReportId);
                cmdDW_TomasResearch_ListOption.CommandType = CommandType.StoredProcedure;

                try
                {
                    OleDbDataReader dr = cmdDW_TomasResearch_ListOption.ExecuteReader();
                    while (dr.Read())
                    {
                        //Если отчет выбран
                        if (RptChacked)
                        {
                            foreach (DevExpress.XtraNavBar.NavBarItemLink itemLink in navBarOptionsGroup.ItemLinks)
                            {
                                if (Convert.ToInt32(itemLink.Item.Tag) == Convert.ToInt32(dr["SortOrder"]))
                                {
                                    itemLink.Item.Enabled = Convert.ToBoolean(dr["isActive"]);
                                }
                            };
                        }
                        else
                        {
                            foreach (DevExpress.XtraNavBar.NavBarItemLink itemLink in navBarOptionsGroup.ItemLinks)
                            {
                                if (Convert.ToInt32(itemLink.Item.Tag) == Convert.ToInt32(dr["SortOrder"]))
                                {
                                    itemLink.Item.Enabled = false; 
                                }
                            };
                        };
                    };
                }
                catch (Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorExecProc + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        //Выбрать отчет
        private void gvReportList_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Space)
            {
                if (gvReportList.FocusedColumn.FieldName == "Report_Checked")
                {
                    reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Checked"] = !Convert.ToBoolean(reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Checked"]);
                    reportDataSet.tblReportList.AcceptChanges();
                    EnableOptionList(Convert.ToInt32(reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Id"]), Convert.ToBoolean(reportDataSet.tblReportList.Rows[bsReportList.Position]["Report_Checked"]));
                };
                e.Handled = true;
            };
        }

        //Выбрать все отчеты
        private void chkedSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataRow rowReport in reportDataSet.tblReportList.Rows)
            {
                rowReport["Report_Checked"] = !chkedSelectAll.Checked;
                reportDataSet.tblReportList.AcceptChanges();
                EnableOptionList(Convert.ToInt32(rowReport["Report_Id"]), Convert.ToBoolean(rowReport["Report_Checked"]));
            };
        }

    }
}
