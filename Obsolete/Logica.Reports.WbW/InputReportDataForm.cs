using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using Logica.Reports.WbW.Properties;

namespace WccpReporting
{
    public partial class InputReportDataForm : WccpReporting.CustomInputReportDataForm
    {        
        private string _reportName;
        public string ReportName
        {
            get { return _reportName; }
            set { _reportName = value;
                  edReportName.Text = value;
                }
        }

        private int _RegionId;
        public int RegionId
        {
            get { return _RegionId; }
            set { _RegionId = value; }
        }
        private string _RegionName;
        public string RegionName
        {
            get { return _RegionName; }
            set { _RegionName = value; }
        }

        private string _ChanelName;
        public string ChanelName
        {
            get { return _ChanelName; }
            set { _ChanelName = value; }
        }

        private int _WaveId;
        public int WaveId
        {
            get { return _WaveId; }
            set { _WaveId = value; }
        }

        private string _WaveName;
        public string WaveName
        {
            get { return _WaveName; }
            set { _WaveName = value; }
        }

        public InputReportDataForm()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            Control ErrRaiseControl= null;

            try
            {
                if (luRegion.EditValue == null)
                {
                    ErrRaiseControl = luRegion;
                    throw new Exception(WCCPConst.msgErrorRegionIsNull);
                };

                if (cboxChanel.EditValue == null)
                {
                    ErrRaiseControl = cboxChanel;
                    throw new Exception(WCCPConst.msgErrorChanelIsNull);
                };

                if (luWave.EditValue == null)
                {
                    ErrRaiseControl = luWave;
                    throw new Exception(WCCPConst.msgErrorWaveIsNull);
                };

                RegionId = (int)luRegion.EditValue;
                RegionName = luRegion.Text;
                ChanelName = cboxChanel.Text;
                WaveId = (int)luWave.EditValue;
                WaveName = luWave.Text;

                ReportOptions.RegionId = RegionId;
                ReportOptions.RegionName = RegionName;
                ReportOptions.ChanelName = ChanelName;
                ReportOptions.WaveId = WaveId;
                ReportOptions.WaveName = WaveName;

                Settings.Default.RegionId = RegionId;
                Settings.Default.RegionName = RegionName;
                Settings.Default.ChanelName = ChanelName;
                Settings.Default.WaveId = WaveId;
                Settings.Default.WaveName = WaveName;
                Settings.Default.Save();
                
                DialogResult = DialogResult.OK;                
            }
            catch (Exception ex)
            {
                ErrRaiseControl.Focus();
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(WCCPConst.msgNoHelp, "����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void InputProgressReportDataForm_Shown(object sender, EventArgs e)
        {
            
            if (Settings.Default.RegionId > 0)
            {                
                luRegion.EditValue = Settings.Default.RegionId;
                cboxChanel.EditValue = Settings.Default.ChanelName;
                luWave.EditValue = Settings.Default.WaveId;
            };
             
           luRegion.Focus();                        
        }

        //��������� �����
        private void InputProgressReportDataForm_Load(object sender, EventArgs e)
        {
            RegionId = -1;
            RegionName = String.Empty;
            ChanelName = String.Empty;
            WaveId = -1;
            WaveName = String.Empty;

            try
            {
                string cmdText=   "SELECT Region_Id, Region_Name "
                                + "FROM tblRegions "
                                + "WHERE (Region_Id > 0) "
                                + "ORDER BY Region_Name";

                using(OleDbCommand cmdRegion = new OleDbCommand(cmdText, DM.Connect(WCCPConst.ConnectionString))){
                    cmdRegion.CommandTimeout = WCCPConst.CommandTimeout;
                    using (OleDbDataAdapter daRegion = new OleDbDataAdapter(cmdRegion))
                    {
                        daRegion.Fill(dsInputReportDataForm, "tblRegions");
                    };
                };

                using (OleDbCommand cmdWave = new OleDbCommand("DW_TomasResearch_ListWaves", DM.Connect(WCCPConst.ConnectionString)))
                {
                    cmdWave.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdWave.CommandType = CommandType.StoredProcedure;
                    cmdWave.Parameters.AddWithValue("@forWbW", 1);
                    cmdWave.Parameters.AddWithValue("@Wave_ID", null);
                    using (OleDbDataAdapter daWave = new OleDbDataAdapter(cmdWave))
                    {
                        daWave.Fill(dsInputReportDataForm, "tblWave");
                    };
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateListData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };

        }

    }
}

