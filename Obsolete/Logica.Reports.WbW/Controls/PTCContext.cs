using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;
using System.Data.OleDb;
using DevExpress.XtraCharts;

namespace WccpReporting
{
    public partial class PTCContext : DevExpress.XtraEditors.XtraUserControl
    {
        public PTCContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(int WaveId, string WaveName, int Wave1Id, string Wave1Name, int Wave2Id, string Wave2Name, string KPI, string SKU, string channel)
        {
            lbSKUName.Text = SKU;
            try
            {
                DataTable dataDataView1 = new DataTable();
                DataTable dataDataView2 = null;
                DataTable dataDataView3 = new DataTable(); 

                //������ �������
                GridBand newWaveTable1Band = new GridBand();
                GridBand newPTCunitTable1Band = new GridBand();
                GridBand newPOCsTable1Band = new GridBand();
                GridBand newMarketShareTable1Band = new GridBand();
                GridBand newSKUTable1Band = new GridBand();
                GridBand newInBevTable1Band = new GridBand();
                GridBand newSKUVolumeTable1Band = new GridBand();
                GridBand newInBevVolumeTable1Band = new GridBand();
                GridBand newVolumeTable1Band = new GridBand();
                //

                advbvTable1Context.Bands.Clear();
                advbvTable1Context.Columns.Clear();
                advbvTable1Context.BeginUpdate();

                //
                {
                    newWaveTable1Band.Caption = "Wave of " + WaveName;
                    newWaveTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newWaveTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newWaveTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newWaveTable1Band.Visible = true;
                }

                {
                    newPTCunitTable1Band.Caption = "PTC ($/unit)";
                    newPTCunitTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPTCunitTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPTCunitTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPTCunitTable1Band.Visible = true;
                }

                {
                    newPOCsTable1Band.Caption = "# POCs";
                    newPOCsTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPOCsTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPOCsTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPOCsTable1Band.Visible = true;
                }
                newWaveTable1Band.Children.Add(newPTCunitTable1Band);
                newWaveTable1Band.Children.Add(newPOCsTable1Band);
                {
                    newMarketShareTable1Band.Caption = "Market share (%)";
                    newMarketShareTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newMarketShareTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newMarketShareTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newMarketShareTable1Band.Visible = true;
                }
                {
                    newSKUTable1Band.Caption = SKU;
                    newSKUTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newSKUTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newSKUTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newSKUTable1Band.Visible = true;
                }
                {
                    newInBevTable1Band.Caption = "InBev";
                    newInBevTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newInBevTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newInBevTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newInBevTable1Band.Visible = true;
                }
                newMarketShareTable1Band.Children.Add(newSKUTable1Band);
                newMarketShareTable1Band.Children.Add(newInBevTable1Band);
                {
                    newVolumeTable1Band.Caption = "Volume (dal/wk)";
                    newVolumeTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newVolumeTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newVolumeTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newVolumeTable1Band.Visible = true;
                }
                {
                    newSKUVolumeTable1Band.Caption = SKU;
                    newSKUVolumeTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newSKUVolumeTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newSKUVolumeTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newSKUVolumeTable1Band.Visible = true;
                }
                {
                    newInBevVolumeTable1Band.Caption = "InBev";
                    newInBevVolumeTable1Band.AppearanceHeader.Options.UseTextOptions = true;
                    newInBevVolumeTable1Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newInBevVolumeTable1Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newInBevVolumeTable1Band.Visible = true;
                }
                newVolumeTable1Band.Children.Add(newSKUVolumeTable1Band);
                newVolumeTable1Band.Children.Add(newInBevVolumeTable1Band);
                //
                newWaveTable1Band.Children.Add(newMarketShareTable1Band);
                newWaveTable1Band.Children.Add(newVolumeTable1Band);
                //
                //������� ��� ������ �������
                //
                BandedGridColumn newPTCunitTable1BandCol = advbvTable1Context.Columns.AddField("PTC ($/unit)");
                newPTCunitTable1BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPTCunitTable1BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPTCunitTable1BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPTCunitTable1BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPTCunitTable1BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPTCunitTable1BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPTCunitTable1BandCol.Visible = true;
                newPTCunitTable1BandCol.OwnerBand = newPTCunitTable1Band;
                
                BandedGridColumn newPOCsTable1BandCol = advbvTable1Context.Columns.AddField("#POCs");
                newPOCsTable1BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPOCsTable1BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPOCsTable1BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPOCsTable1BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPOCsTable1BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPOCsTable1BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPOCsTable1BandCol.Visible = true;
                newPOCsTable1BandCol.OwnerBand = newPOCsTable1Band;
                //
                BandedGridColumn newSKUTable1BandCol = advbvTable1Context.Columns.AddField("MS_" + SKU);
                newSKUTable1BandCol.AppearanceCell.Options.UseTextOptions = true;
                newSKUTable1BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newSKUTable1BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newSKUTable1BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newSKUTable1BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newSKUTable1BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newSKUTable1BandCol.Visible = true;
                newSKUTable1BandCol.OwnerBand = newSKUTable1Band;

                BandedGridColumn newInBevTable1BandCol = advbvTable1Context.Columns.AddField("MS_Inbev");
                newInBevTable1BandCol.AppearanceCell.Options.UseTextOptions = true;
                newInBevTable1BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newInBevTable1BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newInBevTable1BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newInBevTable1BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newInBevTable1BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newInBevTable1BandCol.Visible = true;
                newInBevTable1BandCol.OwnerBand = newInBevTable1Band;
                //
                BandedGridColumn newSKUVolumeTable1BandCol = advbvTable1Context.Columns.AddField("VOL_" + SKU);
                newSKUVolumeTable1BandCol.AppearanceCell.Options.UseTextOptions = true;
                newSKUVolumeTable1BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newSKUVolumeTable1BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newSKUVolumeTable1BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newSKUVolumeTable1BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newSKUVolumeTable1BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newSKUVolumeTable1BandCol.Visible = true;
                newSKUVolumeTable1BandCol.OwnerBand = newSKUVolumeTable1Band;

                BandedGridColumn newInBevVolumeTable1BandCol = advbvTable1Context.Columns.AddField("VOL_Inbev");
                newInBevVolumeTable1BandCol.AppearanceCell.Options.UseTextOptions = true;
                newInBevVolumeTable1BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newInBevVolumeTable1BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newInBevVolumeTable1BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newInBevVolumeTable1BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newInBevVolumeTable1BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newInBevVolumeTable1BandCol.Visible = true;
                newInBevVolumeTable1BandCol.OwnerBand = newInBevVolumeTable1Band;
                
                //
                advbvTable1Context.Bands.Add(newWaveTable1Band);

                //������ �������

                GridBand newPTCunitTable2Band = new GridBand();
                GridBand newPOCsTable2Band = new GridBand();
                GridBand newPercentPOCsTable2Band = new GridBand();

                GridBand newPOCsWave1Table2Band = new GridBand();
                GridBand newPOCsWave2Table2Band = new GridBand();

                GridBand newPercentPOCsWave1Table2Band = new GridBand();
                GridBand newPercentPOCsWave2Table2Band = new GridBand();
                GridBand newPercentPOCsWaveTable2Band = new GridBand();

                advbvTable2Context.Bands.Clear();
                advbvTable2Context.Columns.Clear();
                advbvTable2Context.BeginUpdate();
                //
                {
                    newPTCunitTable2Band.Caption = "PTC ($/unit)";
                    newPTCunitTable2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPTCunitTable2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPTCunitTable2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPTCunitTable2Band.Visible = true;
                }
                advbvTable2Context.Bands.Add(newPTCunitTable2Band);
                //
                {
                    newPOCsTable2Band.Caption = "# POCs";
                    newPOCsTable2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPOCsTable2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPOCsTable2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPOCsTable2Band.Visible = true;
                }

                {
                    newPOCsWave1Table2Band.Caption = "N/A";
                    newPOCsWave1Table2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPOCsWave1Table2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPOCsWave1Table2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPOCsWave1Table2Band.Visible = true;
                }

                {
                    newPOCsWave2Table2Band.Caption = Wave2Name;
                    newPOCsWave2Table2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPOCsWave2Table2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPOCsWave2Table2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPOCsWave2Table2Band.Visible = true;
                }
                newPOCsTable2Band.Children.Add(newPOCsWave1Table2Band);
                newPOCsTable2Band.Children.Add(newPOCsWave2Table2Band);
                advbvTable2Context.Bands.Add(newPOCsTable2Band);
                //
                {
                    newPercentPOCsTable2Band.Caption = "% POCs";
                    newPercentPOCsTable2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPercentPOCsTable2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPercentPOCsTable2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPercentPOCsTable2Band.Visible = true;
                }

                {
                    newPercentPOCsWave1Table2Band.Caption = "N/A";
                    newPercentPOCsWave1Table2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPercentPOCsWave1Table2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPercentPOCsWave1Table2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPercentPOCsWave1Table2Band.Visible = true;
                }

                {
                    newPercentPOCsWave2Table2Band.Caption = Wave2Name;
                    newPercentPOCsWave2Table2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPercentPOCsWave2Table2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPercentPOCsWave2Table2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPercentPOCsWave2Table2Band.Visible = true;
                }

                {
                    newPercentPOCsWaveTable2Band.Caption = WaveName;
                    newPercentPOCsWaveTable2Band.AppearanceHeader.Options.UseTextOptions = true;
                    newPercentPOCsWaveTable2Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    newPercentPOCsWaveTable2Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    newPercentPOCsWaveTable2Band.Visible = true;
                }

                newPercentPOCsTable2Band.Children.Add(newPercentPOCsWave1Table2Band);
                newPercentPOCsTable2Band.Children.Add(newPercentPOCsWave2Table2Band);
                newPercentPOCsTable2Band.Children.Add(newPercentPOCsWaveTable2Band);

                advbvTable2Context.Bands.Add(newPercentPOCsTable2Band);
                
                //
                //������� ��� ������ �������
                //
                BandedGridColumn newPTCunitTable2BandCol = advbvTable2Context.Columns.AddField("PTC ($/unit)");
                newPTCunitTable2BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPTCunitTable2BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPTCunitTable2BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPTCunitTable2BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPTCunitTable2BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPTCunitTable2BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPTCunitTable2BandCol.Visible = true;
                newPTCunitTable2BandCol.OwnerBand = newPTCunitTable2Band;
                //
                BandedGridColumn newPOCsWave1Table2BandCol = advbvTable2Context.Columns.AddField("#POCs|" + Wave1Name);
                newPOCsWave1Table2BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPOCsWave1Table2BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPOCsWave1Table2BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPOCsWave1Table2BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPOCsWave1Table2BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPOCsWave1Table2BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPOCsWave1Table2BandCol.Visible = true;
                newPOCsWave1Table2BandCol.OwnerBand = newPOCsWave1Table2Band;
                //
                BandedGridColumn newPOCsWave2Table2BandCol = advbvTable2Context.Columns.AddField("#POCs|" + Wave2Name);
                newPOCsWave2Table2BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPOCsWave2Table2BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPOCsWave2Table2BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPOCsWave2Table2BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPOCsWave2Table2BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPOCsWave2Table2BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPOCsWave2Table2BandCol.Visible = true;
                newPOCsWave2Table2BandCol.OwnerBand = newPOCsWave2Table2Band;
                //
                BandedGridColumn newPercentPOCsWave1Table2BandCol = advbvTable2Context.Columns.AddField("% POCs|" + Wave1Name);
                newPercentPOCsWave1Table2BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPercentPOCsWave1Table2BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPercentPOCsWave1Table2BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPercentPOCsWave1Table2BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPercentPOCsWave1Table2BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPercentPOCsWave1Table2BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPercentPOCsWave1Table2BandCol.Visible = true;
                newPercentPOCsWave1Table2BandCol.OwnerBand = newPercentPOCsWave1Table2Band;
                //
                BandedGridColumn newPercentPOCsWave2Table2BandCol = advbvTable2Context.Columns.AddField("% POCs|" + Wave2Name);
                newPercentPOCsWave2Table2BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPercentPOCsWave2Table2BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPercentPOCsWave2Table2BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPercentPOCsWave2Table2BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPercentPOCsWave2Table2BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPercentPOCsWave2Table2BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPercentPOCsWave2Table2BandCol.Visible = true;
                newPercentPOCsWave2Table2BandCol.OwnerBand = newPercentPOCsWave2Table2Band;
                //
                BandedGridColumn newPercentPOCsWaveTable2BandCol = advbvTable2Context.Columns.AddField("% POCs|" + WaveName);
                newPercentPOCsWaveTable2BandCol.AppearanceCell.Options.UseTextOptions = true;
                newPercentPOCsWaveTable2BandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPercentPOCsWaveTable2BandCol.AppearanceHeader.Options.UseTextOptions = true;
                newPercentPOCsWaveTable2BandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newPercentPOCsWaveTable2BandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newPercentPOCsWaveTable2BandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                newPercentPOCsWaveTable2BandCol.Visible = true;
                newPercentPOCsWaveTable2BandCol.OwnerBand = newPercentPOCsWaveTable2Band;
                

                try
                {
                    using (OleDbCommand cmdDW_TomasResearch_WbW_PTC = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_PTC_ONTRADE" : "DW_TomasResearch_WbW_PTC"), WccpUIControl.Conn))
                    {
                        cmdDW_TomasResearch_WbW_PTC.CommandTimeout = WCCPConst.CommandTimeout;
                        cmdDW_TomasResearch_WbW_PTC.Parameters.AddWithValue("@Wave_ID", WaveId);
                        cmdDW_TomasResearch_WbW_PTC.Parameters.AddWithValue("@prevWave_ID", Wave2Id);
                        cmdDW_TomasResearch_WbW_PTC.Parameters.AddWithValue("@prevprevWave_ID", Wave1Id);
                        cmdDW_TomasResearch_WbW_PTC.Parameters.AddWithValue("@KPI", "PTC (UAH/SKU)");
                        cmdDW_TomasResearch_WbW_PTC.Parameters.AddWithValue("@SKU1", SKU);
                        cmdDW_TomasResearch_WbW_PTC.Parameters.AddWithValue("@Channel", channel);
                        cmdDW_TomasResearch_WbW_PTC.CommandType = CommandType.StoredProcedure;

                        using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_PTC))
                        {
                            da.Fill(dataDataView1);
                            dataDataView2 = dataDataView1.Copy();
                        };
                    };

                    using (OleDbCommand cmdDW_TomasResearch_WbW_PTC_Shift = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_PTC_Shift_ONTRADE" : "DW_TomasResearch_WbW_PTC_Shift"), WccpUIControl.Conn))
                    {
                        cmdDW_TomasResearch_WbW_PTC_Shift.CommandTimeout = WCCPConst.CommandTimeout;
                        cmdDW_TomasResearch_WbW_PTC_Shift.Parameters.AddWithValue("@Wave_ID", WaveId);
                        cmdDW_TomasResearch_WbW_PTC_Shift.Parameters.AddWithValue("@prevWave_ID", Wave2Id);
                        cmdDW_TomasResearch_WbW_PTC_Shift.Parameters.AddWithValue("@SKU1", SKU);
                        cmdDW_TomasResearch_WbW_PTC_Shift.Parameters.AddWithValue("@Channel", channel);
                        cmdDW_TomasResearch_WbW_PTC_Shift.CommandType = CommandType.StoredProcedure;

                        using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_PTC_Shift))
                        {
                            da.Fill(dataDataView3);
                        };
                    };


                }
                finally
                {
                    advbvTable1Context.EndUpdate();
                    advbvTable2Context.EndUpdate();
                };
                
                chartContext.Series[0].Name = Wave2Name;
                chartContext.Series[1].Name = WaveName;
                
                if (dataDataView2.DefaultView.Count > 0)
                {
                    foreach (DataRow seriesRow in dataDataView2.Rows)
                    {
                        chartContext.Series[0].Points.Add(new SeriesPoint(Convert.ToDouble(seriesRow["PTC ($/unit)"]), new double[] { Convert.ToDouble(seriesRow["% POCs|" + Wave2Name]) }));
                        chartContext.Series[1].Points.Add(new SeriesPoint(Convert.ToDouble(seriesRow["PTC ($/unit)"]), new double[] { Convert.ToDouble(seriesRow["% POCs|" + WaveName]) }));
                    };
                };
                
                if (dataDataView1 != null)
                {
                    gcTable1Context.DataSource = dataDataView1.DefaultView;
                    gcTable2Context.DataSource = dataDataView2.DefaultView;
                };

                if (dataDataView3.Rows.Count > 0)
                {
                    gcTable3Context.DataSource = dataDataView3.DefaultView;
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }
    }
}
