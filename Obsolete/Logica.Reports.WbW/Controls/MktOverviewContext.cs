using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.OleDb;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraCharts;

namespace WccpReporting
{
    public partial class MktOverviewContext : DevExpress.XtraEditors.XtraUserControl
    {
        public MktOverviewContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(string channel, int WaveId, string WaveName, int prevWave_ID, string prevWaveName, int prevprevWave_ID, string prevprevWaveName)
        {
            try
            {
                DataTable dataDataView = new DataTable();

                using (OleDbCommand cmdDW_TomasResearch_WbW_Market_Overview = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_Market_Overview_ONTRADE" : "DW_TomasResearch_WbW_Market_Overview"), WccpUIControl.Conn))
                {
                    cmdDW_TomasResearch_WbW_Market_Overview.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_Market_Overview.Parameters.AddWithValue("@Wave_ID", WaveId);
                    cmdDW_TomasResearch_WbW_Market_Overview.Parameters.AddWithValue("@prevWave_ID", prevWave_ID);
                    cmdDW_TomasResearch_WbW_Market_Overview.Parameters.AddWithValue("@prevprevWave_ID", prevprevWave_ID);
                    cmdDW_TomasResearch_WbW_Market_Overview.CommandType = CommandType.StoredProcedure;

                    using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Market_Overview))
                    {
                        da.Fill(dataDataView);
                    };
                };
                gvTableContext.Columns.Clear();
                gvTableContext.BeginUpdate();
                try
                {
                    foreach (DataColumn col in dataDataView.Columns)
                    {
                        GridColumn newCol = gvTableContext.Columns.AddField(col.ColumnName);
                        newCol.AppearanceCell.Options.UseFont = true;
                        newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        newCol.AppearanceCell.Options.UseTextOptions = true;
                        newCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                        newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Default;
                        newCol.AppearanceHeader.Options.UseTextOptions = true;
                        newCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                        newCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                        newCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                        newCol.Caption = col.Caption;

                        if (col.ColumnName == "KPI" || col.ColumnName == "Brewers")
                        {
                            newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
                            newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Bold);
                            newCol.Width = 105;
                        }
                        else
                        {
                            if (col.ColumnName == "SKU")
                            {
                                newCol.Width = 125;
                            }
                            else
                              newCol.Width = 100;
                        };

                        if (col.ColumnName != "GROUP" && col.ColumnName != "isTotal" && col.ColumnName != "isBrand")
                            newCol.Visible = true;
                        else
                        {
                            newCol.Visible = false;
                        };

                        if (col.ColumnName != "KPI" && col.ColumnName != "Brewers" && col.ColumnName != "SKU")
                        {
                            newCol.OptionsColumn.ShowCaption = true;
                        }
                        else
                        {
                            newCol.AppearanceHeader.BackColor = Color.White;
                            newCol.AppearanceHeader.BorderColor = Color.White;
                            newCol.OptionsColumn.ShowCaption = false;
                        };
                    };
                }
                finally
                {
                    gvTableContext.EndUpdate();
                };

                Chart1Context.Series[0].Name = prevprevWaveName;
                Chart1Context.Series[1].Name = prevWaveName;
                Chart1Context.Series[2].Name = WaveName;

                Chart2Context.Series[0].Name = prevprevWaveName;
                Chart2Context.Series[1].Name = prevWaveName;
                Chart2Context.Series[2].Name = WaveName;

                //Numeric Distribution (%)
                dataDataView.DefaultView.RowFilter = "GROUP = 2 AND isBrand = 1";
                if (dataDataView.DefaultView.Count > 0)
                {
                    foreach (DataRowView seriesRow in dataDataView.DefaultView)
                    {
                        Chart1Context.Series[0].Points.Add(new SeriesPoint(Convert.ToString(seriesRow["SKU"]), new double[] { Convert.ToDouble(seriesRow[prevprevWaveName]) }));
                        Chart1Context.Series[1].Points.Add(new SeriesPoint(Convert.ToString(seriesRow["SKU"]), new double[] { Convert.ToDouble(seriesRow[prevWaveName])}));
                        Chart1Context.Series[2].Points.Add(new SeriesPoint(Convert.ToString(seriesRow["SKU"]), new double[] { Convert.ToDouble(seriesRow[WaveName])}));
                    };
                };

                //Market Share (%)
                dataDataView.DefaultView.RowFilter = "GROUP = 3 AND isBrand = 1";
                if (dataDataView.DefaultView.Count > 0)
                {
                    foreach (DataRowView seriesRow in dataDataView.DefaultView)
                    {
                        Chart2Context.Series[0].Points.Add(new SeriesPoint(Convert.ToString(seriesRow["SKU"]), new double[] { Convert.ToDouble(seriesRow[prevprevWaveName]) }));
                        Chart2Context.Series[1].Points.Add(new SeriesPoint(Convert.ToString(seriesRow["SKU"]), new double[] { Convert.ToDouble(seriesRow[prevWaveName]) }));
                        Chart2Context.Series[2].Points.Add(new SeriesPoint(Convert.ToString(seriesRow["SKU"]), new double[] { Convert.ToDouble(seriesRow[WaveName]) }));
                    };
                };

                dataDataView.DefaultView.RowFilter = String.Empty;
                if (dataDataView.Rows.Count > 0)
                {
                    gcTableContext.DataSource = dataDataView.DefaultView;
                    gvTableContext.Columns[0].Width = 150;

                    //��������� ������ ���������
                    int ChartH= 685;
                    int GridH = ((gvTableContext.RowHeight * dataDataView.DefaultView.Count) + ((gvTableContext.RowSeparatorHeight*2) * dataDataView.DefaultView.Count)) + gvTableContext.ColumnPanelRowHeight;
                    int delta = 130;

                    this.Height = GridH + ChartH + delta;
                    gcTableContext.Height = GridH;
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //������� ��� �������������, totals � ������ ����
        private void gvTableContext_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                string isTotal = View.GetRowCellDisplayText(e.RowHandle, View.Columns["isTotal"]);
                if (e.Column.Caption != "KPI" && e.Column.Caption != "Brewers" && e.Column.Caption != "SKU" && e.Column.Caption != "GROUP" && e.Column.Caption != "isTotal" && e.Column.Caption != "isBrand")
                {
                    if (e.CellValue != DBNull.Value)
                    {
                        if (Convert.ToDouble(e.CellValue) < 0)
                        {
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (Convert.ToDouble(e.CellValue) == 0)
                        {
                            e.Appearance.ForeColor = Color.Silver;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        };
                    };
                };
                if (isTotal == "1")
                {
                    if (e.Column.Caption != "KPI" && e.Column.Caption != "Brewers")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                    };
                    e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                };

            }

        }
    }
}
