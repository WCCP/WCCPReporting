namespace WccpReporting
{
    partial class ContractsContext
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gcTable1Context = new DevExpress.XtraGrid.GridControl();
            this.gvTable1Context = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcTable2Context = new DevExpress.XtraGrid.GridControl();
            this.bvTable2Context = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbBrand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbSegment = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbPOCs = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbPOCsPercent = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbAVR_Dal = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbAVR_Var = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbMSShift = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbInBev = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chartContext = new DevExpress.XtraCharts.ChartControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.table1LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.table2LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.chartLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable1Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTable1Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable2Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bvTable2Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table1LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table2LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartLayoutControlItem)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gcTable1Context);
            this.dataLayoutControl1.Controls.Add(this.gcTable2Context);
            this.dataLayoutControl1.Controls.Add(this.chartContext);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1200, 371);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gcTable1Context
            // 
            this.gcTable1Context.Location = new System.Drawing.Point(7, 7);
            this.gcTable1Context.MainView = this.gvTable1Context;
            this.gcTable1Context.Name = "gcTable1Context";
            this.gcTable1Context.Size = new System.Drawing.Size(389, 229);
            this.gcTable1Context.TabIndex = 0;
            this.gcTable1Context.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTable1Context});
            // 
            // gvTable1Context
            // 
            this.gvTable1Context.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gvTable1Context.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvTable1Context.ColumnPanelRowHeight = 35;
            this.gvTable1Context.GridControl = this.gcTable1Context;
            this.gvTable1Context.Name = "gvTable1Context";
            this.gvTable1Context.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvTable1Context.OptionsBehavior.Editable = false;
            this.gvTable1Context.OptionsCustomization.AllowColumnMoving = false;
            this.gvTable1Context.OptionsCustomization.AllowGroup = false;
            this.gvTable1Context.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvTable1Context.OptionsCustomization.AllowSort = false;
            this.gvTable1Context.OptionsPrint.AutoWidth = false;
            this.gvTable1Context.OptionsSelection.InvertSelection = true;
            this.gvTable1Context.OptionsView.AllowCellMerge = true;
            this.gvTable1Context.OptionsView.ColumnAutoWidth = false;
            this.gvTable1Context.OptionsView.ShowGroupPanel = false;
            this.gvTable1Context.OptionsView.ShowIndicator = false;
            this.gvTable1Context.RowHeight = 18;
            this.gvTable1Context.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvTable1Context_RowCellStyle);
            // 
            // gcTable2Context
            // 
            this.gcTable2Context.Location = new System.Drawing.Point(7, 246);
            this.gcTable2Context.MainView = this.bvTable2Context;
            this.gcTable2Context.Name = "gcTable2Context";
            this.gcTable2Context.Size = new System.Drawing.Size(1186, 118);
            this.gcTable2Context.TabIndex = 1;
            this.gcTable2Context.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bvTable2Context});
            // 
            // bvTable2Context
            // 
            this.bvTable2Context.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bvTable2Context.Appearance.BandPanel.Options.UseFont = true;
            this.bvTable2Context.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bvTable2Context.Appearance.HeaderPanel.Options.UseFont = true;
            this.bvTable2Context.BandPanelRowHeight = 18;
            this.bvTable2Context.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand1,
            this.gridBand3,
            this.gbMSShift});
            this.bvTable2Context.ColumnPanelRowHeight = 35;
            this.bvTable2Context.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7});
            this.bvTable2Context.GridControl = this.gcTable2Context;
            this.bvTable2Context.Name = "bvTable2Context";
            this.bvTable2Context.OptionsBehavior.AllowIncrementalSearch = true;
            this.bvTable2Context.OptionsBehavior.Editable = false;
            this.bvTable2Context.OptionsCustomization.AllowBandMoving = false;
            this.bvTable2Context.OptionsCustomization.AllowGroup = false;
            this.bvTable2Context.OptionsCustomization.AllowQuickHideColumns = false;
            this.bvTable2Context.OptionsCustomization.AllowSort = false;
            this.bvTable2Context.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.bvTable2Context.OptionsPrint.AutoWidth = false;
            this.bvTable2Context.OptionsPrint.PrintHeader = false;
            this.bvTable2Context.OptionsSelection.InvertSelection = true;
            this.bvTable2Context.OptionsView.AllowCellMerge = true;
            this.bvTable2Context.OptionsView.ColumnAutoWidth = false;
            this.bvTable2Context.OptionsView.ShowColumnHeaders = false;
            this.bvTable2Context.OptionsView.ShowGroupPanel = false;
            this.bvTable2Context.OptionsView.ShowIndicator = false;
            this.bvTable2Context.RowHeight = 18;
            this.bvTable2Context.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.bvTable2Context_RowCellStyle);
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gridBand2.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand2.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4});
            this.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 230;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gridBand4.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbBrand,
            this.gbSegment});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 230;
            // 
            // gbBrand
            // 
            this.gbBrand.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gbBrand.AppearanceHeader.Options.UseBackColor = true;
            this.gbBrand.Columns.Add(this.bandedGridColumn1);
            this.gbBrand.Name = "gbBrand";
            this.gbBrand.Width = 75;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bandedGridColumn1.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn1.Caption = "BRAND";
            this.bandedGridColumn1.FieldName = "BRAND";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            // 
            // gbSegment
            // 
            this.gbSegment.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gbSegment.AppearanceHeader.Options.UseBackColor = true;
            this.gbSegment.Columns.Add(this.bandedGridColumn2);
            this.gbSegment.Name = "gbSegment";
            this.gbSegment.Width = 155;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "SEGMENT";
            this.bandedGridColumn2.FieldName = "SEGMENT";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 155;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gridBand1.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand7});
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 150;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gridBand7.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand7.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbPOCs,
            this.gbPOCsPercent});
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.Width = 150;
            // 
            // gbPOCs
            // 
            this.gbPOCs.Caption = "# POCs";
            this.gbPOCs.Columns.Add(this.bandedGridColumn3);
            this.gbPOCs.Name = "gbPOCs";
            this.gbPOCs.Width = 75;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "POCS";
            this.bandedGridColumn3.FieldName = "POCS";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridColumn3.Visible = true;
            // 
            // gbPOCsPercent
            // 
            this.gbPOCsPercent.Caption = "% POCs";
            this.gbPOCsPercent.Columns.Add(this.bandedGridColumn4);
            this.gbPOCsPercent.Name = "gbPOCsPercent";
            this.gbPOCsPercent.Width = 75;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "POCS_Percent";
            this.bandedGridColumn4.FieldName = "POCS_Percent";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridColumn4.Visible = true;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gridBand3.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand3.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand10});
            this.gridBand3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 150;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "Average vol.";
            this.gridBand10.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbAVR_Dal,
            this.gbAVR_Var});
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.Width = 150;
            // 
            // gbAVR_Dal
            // 
            this.gbAVR_Dal.Caption = "dal/wk";
            this.gbAVR_Dal.Columns.Add(this.bandedGridColumn5);
            this.gbAVR_Dal.Name = "gbAVR_Dal";
            this.gbAVR_Dal.Width = 75;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "AVR_DAL";
            this.bandedGridColumn5.FieldName = "AVR_DAL";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridColumn5.Visible = true;
            // 
            // gbAVR_Var
            // 
            this.gbAVR_Var.Caption = "Var.";
            this.gbAVR_Var.Columns.Add(this.bandedGridColumn6);
            this.gbAVR_Var.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gbAVR_Var.Name = "gbAVR_Var";
            this.gbAVR_Var.Width = 75;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "AVR_VAR";
            this.bandedGridColumn6.FieldName = "AVR_VAR";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridColumn6.Visible = true;
            // 
            // gbMSShift
            // 
            this.gbMSShift.Caption = "MS Shift (pp)";
            this.gbMSShift.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbInBev});
            this.gbMSShift.Name = "gbMSShift";
            this.gbMSShift.Width = 106;
            // 
            // gbInBev
            // 
            this.gbInBev.Caption = "InBev";
            this.gbInBev.Columns.Add(this.bandedGridColumn7);
            this.gbInBev.Name = "gbInBev";
            this.gbInBev.Width = 106;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "MS_SHIFT";
            this.bandedGridColumn7.FieldName = "MS_SHIFT";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 106;
            // 
            // chartContext
            // 
            this.chartContext.BorderOptions.Color = System.Drawing.Color.Black;
            this.chartContext.BorderOptions.Thickness = 2;
            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chartContext.Diagram = xyDiagram1;
            this.chartContext.Location = new System.Drawing.Point(406, 7);
            this.chartContext.Name = "chartContext";
            sideBySideBarSeriesLabel1.LineVisible = true;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.Name = "Series 1";
            sideBySideBarSeriesLabel2.LineVisible = true;
            series2.Label = sideBySideBarSeriesLabel2;
            series2.Name = "Series 2";
            this.chartContext.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            sideBySideBarSeriesLabel3.LineVisible = true;
            this.chartContext.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chartContext.Size = new System.Drawing.Size(787, 229);
            this.chartContext.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BorderColor = System.Drawing.Color.Maroon;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.table1LayoutControlItem,
            this.table2LayoutControlItem,
            this.chartLayoutControlItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1200, 371);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // table1LayoutControlItem
            // 
            this.table1LayoutControlItem.Control = this.gcTable1Context;
            this.table1LayoutControlItem.CustomizationFormText = "layoutControlItem1";
            this.table1LayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.table1LayoutControlItem.MinSize = new System.Drawing.Size(110, 30);
            this.table1LayoutControlItem.Name = "table1LayoutControlItem";
            this.table1LayoutControlItem.Size = new System.Drawing.Size(399, 239);
            this.table1LayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.table1LayoutControlItem.Text = "table1LayoutControlItem";
            this.table1LayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.table1LayoutControlItem.TextToControlDistance = 0;
            this.table1LayoutControlItem.TextVisible = false;
            // 
            // table2LayoutControlItem
            // 
            this.table2LayoutControlItem.Control = this.gcTable2Context;
            this.table2LayoutControlItem.CustomizationFormText = "layoutControlItem2";
            this.table2LayoutControlItem.Location = new System.Drawing.Point(0, 239);
            this.table2LayoutControlItem.MinSize = new System.Drawing.Size(110, 30);
            this.table2LayoutControlItem.Name = "table2LayoutControlItem";
            this.table2LayoutControlItem.Size = new System.Drawing.Size(1196, 128);
            this.table2LayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.table2LayoutControlItem.Text = "table2LayoutControlItem";
            this.table2LayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.table2LayoutControlItem.TextToControlDistance = 0;
            this.table2LayoutControlItem.TextVisible = false;
            // 
            // chartLayoutControlItem
            // 
            this.chartLayoutControlItem.Control = this.chartContext;
            this.chartLayoutControlItem.CustomizationFormText = "chartLayoutControlItem";
            this.chartLayoutControlItem.Location = new System.Drawing.Point(399, 0);
            this.chartLayoutControlItem.MaxSize = new System.Drawing.Size(797, 0);
            this.chartLayoutControlItem.MinSize = new System.Drawing.Size(797, 30);
            this.chartLayoutControlItem.Name = "chartLayoutControlItem";
            this.chartLayoutControlItem.Size = new System.Drawing.Size(797, 239);
            this.chartLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.chartLayoutControlItem.Text = "chartLayoutControlItem";
            this.chartLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.chartLayoutControlItem.TextToControlDistance = 0;
            this.chartLayoutControlItem.TextVisible = false;
            // 
            // ContractsContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "ContractsContext";
            this.Size = new System.Drawing.Size(1200, 371);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTable1Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTable1Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable2Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bvTable2Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table1LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table2LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartLayoutControlItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraGrid.GridControl gcTable1Context;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTable1Context;
        private DevExpress.XtraGrid.GridControl gcTable2Context;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bvTable2Context;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbBrand;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbSegment;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbPOCs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbPOCsPercent;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbAVR_Dal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbAVR_Var;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbMSShift;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbInBev;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem table1LayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem table2LayoutControlItem;
        private DevExpress.XtraCharts.ChartControl chartContext;
        private DevExpress.XtraLayout.LayoutControlItem chartLayoutControlItem;
    }
}
