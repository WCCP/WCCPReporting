using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.OleDb;
using DevExpress.XtraGrid.Columns;

namespace WccpReporting
{
    public partial class BrandsEntranceContext : DevExpress.XtraEditors.XtraUserControl
    {
        public BrandsEntranceContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(int WaveId, int Wave2Id, string SKU, string channel)
        {
            try
            {
                DataTable dataDataView = new DataTable();

                gvTableContext.BeginUpdate();

                try
                {
                    using (OleDbCommand cmdDW_TomasResearch_WbW_Brands_Entrance = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_Brands_Entrance_ONTRADE" : "DW_TomasResearch_WbW_Brands_Entrance"), WccpUIControl.Conn))
                    {
                        cmdDW_TomasResearch_WbW_Brands_Entrance.CommandTimeout = WCCPConst.CommandTimeout;
                        cmdDW_TomasResearch_WbW_Brands_Entrance.Parameters.AddWithValue("@Wave_ID", WaveId);
                        cmdDW_TomasResearch_WbW_Brands_Entrance.Parameters.AddWithValue("@prevWave_ID", Wave2Id);
                        cmdDW_TomasResearch_WbW_Brands_Entrance.Parameters.AddWithValue("@SKU", SKU);
                        cmdDW_TomasResearch_WbW_Brands_Entrance.CommandType = CommandType.StoredProcedure;

                        using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Brands_Entrance))
                        {
                            da.Fill(dataDataView);
                        };
                    };
                }
                finally
                {
                    gvTableContext.EndUpdate();
                };

                int tableW = 0;

                if (dataDataView.Rows.Count > 0)
                {
                    gcTableContext.DataSource = dataDataView.DefaultView;
                    gvTableContext.BestFitColumns();

                    foreach (GridColumn colGridView in gvTableContext.Columns)
                    {
                        tableW = tableW + colGridView.Width;
                    };
                    tableW = tableW + 5;
                };

                //��������� ������ � ������ ���������
                tableLayoutControlItem.ControlMinSize = new Size(tableW, gcTableContext.Height);
                tableLayoutControlItem.ControlMaxSize = new Size(tableW, gcTableContext.Height);

                this.Width = (tableW + 50);

            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };

        }
    }
}
