namespace WccpReporting
{
    partial class DistributionReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DistributionReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // DistributionReportScrollableControl
            // 
            this.DistributionReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DistributionReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.DistributionReportScrollableControl.Name = "DistributionReportScrollableControl";
            this.DistributionReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.DistributionReportScrollableControl.TabIndex = 0;
            // 
            // DistributionReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DistributionReportScrollableControl);
            this.Name = "DistributionReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl DistributionReportScrollableControl;

    }
}
