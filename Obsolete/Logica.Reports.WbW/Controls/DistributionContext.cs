using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using System.Data.OleDb;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace WccpReporting
{
    public partial class DistributionContext : DevExpress.XtraEditors.XtraUserControl
    {
        public DistributionContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(string channel, int WaveId, string WaveName, int prevWave_ID, string prevWaveName, int prevprevWave_ID, string prevprevWaveName)
        {
            try
            {
                DataTable dataDataView1 = new DataTable();

                using (OleDbCommand cmdDW_TomasResearch_WbW_Distribution = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_Distribution_ONTRADE" : "DW_TomasResearch_WbW_Distribution"), WccpUIControl.Conn))
                {
                    cmdDW_TomasResearch_WbW_Distribution.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_Distribution.Parameters.AddWithValue("@Wave_ID", WaveId);
                    cmdDW_TomasResearch_WbW_Distribution.Parameters.AddWithValue("@prevWave_ID", prevWave_ID);
                    cmdDW_TomasResearch_WbW_Distribution.CommandType = CommandType.StoredProcedure;

                    using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Distribution))
                    {
                        da.Fill(dataDataView1);
                    };
                };

                bvTable1Context.Bands.Clear();
                bvTable1Context.Columns.Clear();
                bvTable1Context.BeginUpdate();

                try
                {
                    //������ ������� - Distribution
                    GridBand newNameBand = new GridBand();
                    GridBand newInbevBrandsBand = new GridBand();
                    GridBand newTotalBrandsBand = new GridBand();
                    GridBand newPOCsBand = new GridBand();
                    GridBand newPercentPOCsBand = new GridBand();
                    GridBand newNonInbevVolBand = new GridBand();
                    GridBand newPercNonInbevVolBand = new GridBand();
                    GridBand newCompetMSBand = new GridBand();
                    GridBand newTotalVolBand = new GridBand();
                    GridBand newPercentTotalVolBand = new GridBand();
                    GridBand newInbevPTRBand = new GridBand();
                    GridBand newInbevPTCBand = new GridBand();
                    //
                    BandedGridColumn newWaveBandCol = null;
                    BandedGridColumn newWave2BandCol = null;
                    //
                    newNameBand.Caption = "Number of InBev brands per POC";
                    newNameBand.AppearanceHeader.Options.UseBackColor = true;
                    newNameBand.AppearanceHeader.BackColor = Color.White;
                    newNameBand.AutoFillDown = true;
                    newNameBand.Fixed = FixedStyle.Left;
                    newNameBand.Visible = true;
                    //
                    newInbevBrandsBand.Caption = "# InBev brands";
                    //
                    newTotalBrandsBand.Caption = "Total # brands";
                    //
                    newPOCsBand.Caption = "# POCs";
                    //
                    newPercentPOCsBand.Caption = "% POCs";
                    //
                    newNonInbevVolBand.Caption = "Non InBev volume (dal/POC/wk)";
                    //
                    newPercNonInbevVolBand.Caption = "% non InBev volume";
                    //
                    newCompetMSBand.Caption = "Competitors market share (%)";
                    //
                    newTotalVolBand.Caption = "Total volume (dal/wk)";
                    //
                    newPercentTotalVolBand.Caption = "% total volume";
                    //
                    newInbevPTRBand.Caption = "InBev PTR ($/dal)";
                    //
                    newInbevPTCBand.Caption = "InBev PTC ($/dal)";
                    //
                    //������� ��� ������ �������
                    //
                    BandedGridColumn newNameBandCol = bvTable1Context.Columns.AddField("Name");
                    newNameBandCol.Caption = "Name";
                    newNameBandCol.AppearanceHeader.Options.UseBackColor = true;
                    newNameBandCol.AppearanceHeader.BackColor = Color.White;
                    newNameBandCol.AppearanceCell.Options.UseFont = true;
                    newNameBandCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Bold);
                    newNameBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
                    newNameBandCol.OptionsColumn.ShowCaption = false;

                    newNameBandCol.Visible = true;
                    newNameBandCol.OwnerBand = newNameBand;
                    bvTable1Context.Bands.Add(newNameBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("InbevBrands2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newInbevBrandsBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("InbevBrands1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newInbevBrandsBand;
                    bvTable1Context.Bands.Add(newInbevBrandsBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("TotalBrands2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newTotalBrandsBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("TotalBrands1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newTotalBrandsBand;
                    bvTable1Context.Bands.Add(newTotalBrandsBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("POCs2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newPOCsBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("POCs1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newPOCsBand;
                    bvTable1Context.Bands.Add(newPOCsBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("PercentPOCs2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newPercentPOCsBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("PercentPOCs1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newPercentPOCsBand;
                    bvTable1Context.Bands.Add(newPercentPOCsBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("NonInbevVol2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newNonInbevVolBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("NonInbevVol1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newNonInbevVolBand;
                    bvTable1Context.Bands.Add(newNonInbevVolBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("PercNonInbevVol2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newPercNonInbevVolBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("PercNonInbevVol1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newPercNonInbevVolBand;
                    bvTable1Context.Bands.Add(newPercNonInbevVolBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("CompetMS2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newCompetMSBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("CompetMS1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newCompetMSBand;
                    bvTable1Context.Bands.Add(newCompetMSBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("TotalVol2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newTotalVolBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("TotalVol1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newTotalVolBand;
                    bvTable1Context.Bands.Add(newTotalVolBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("PercentTotalVol2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newPercentTotalVolBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("PercentTotalVol1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newPercentTotalVolBand;
                    bvTable1Context.Bands.Add(newPercentTotalVolBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("InbevPTR2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newInbevPTRBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("InbevPTR1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newInbevPTRBand;
                    bvTable1Context.Bands.Add(newInbevPTRBand);
                    //
                    newWave2BandCol = bvTable1Context.Columns.AddField("InbevPTC2");
                    newWave2BandCol.Caption = prevWaveName;
                    newWave2BandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWave2BandCol.Visible = true;
                    newWave2BandCol.OwnerBand = newInbevPTCBand;
                    newWaveBandCol = bvTable1Context.Columns.AddField("InbevPTC1");
                    newWaveBandCol.Caption = WaveName;
                    newWaveBandCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                    newWaveBandCol.Visible = true;
                    newWaveBandCol.OwnerBand = newInbevPTCBand;
                    bvTable1Context.Bands.Add(newInbevPTCBand);

                }
                finally
                {
                    bvTable1Context.EndUpdate();
                };

                if (dataDataView1.Rows.Count > 0)
                {
                    gcTable1Context.DataSource = dataDataView1.DefaultView;
                    bvTable1Context.BestFitColumns();
                };

            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //������� ��� �������������, totals � ������ ����
        private void bvTable1Context_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                if (e.Column.Caption != "Name")
                {
                    if (e.CellValue != DBNull.Value)
                    {
                        if (Convert.ToDouble(e.CellValue) < 0)
                        {
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (Convert.ToDouble(e.CellValue) == 0)
                        {
                            e.Appearance.ForeColor = Color.Silver;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        };
                    };
                };
            }
        }

    }
}
