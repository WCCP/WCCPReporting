namespace WccpReporting
{
    partial class DistributionContext
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gcTable1Context = new DevExpress.XtraGrid.GridControl();
            this.bvTable1Context = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridDataBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.table1LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable1Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bvTable1Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table1LayoutControlItem)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gcTable1Context);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(634, 375);
            this.dataLayoutControl1.TabIndex = 0;
            // 
            // gcTable1Context
            // 
            this.gcTable1Context.Location = new System.Drawing.Point(7, 7);
            this.gcTable1Context.MainView = this.bvTable1Context;
            this.gcTable1Context.Name = "gcTable1Context";
            this.gcTable1Context.Size = new System.Drawing.Size(620, 361);
            this.gcTable1Context.TabIndex = 0;
            this.gcTable1Context.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bvTable1Context});
            // 
            // bvTable1Context
            // 
            this.bvTable1Context.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.bvTable1Context.Appearance.BandPanel.Options.UseFont = true;
            this.bvTable1Context.Appearance.BandPanel.Options.UseTextOptions = true;
            this.bvTable1Context.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.bvTable1Context.Appearance.BandPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bvTable1Context.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bvTable1Context.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.bvTable1Context.Appearance.HeaderPanel.Options.UseFont = true;
            this.bvTable1Context.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bvTable1Context.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.bvTable1Context.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bvTable1Context.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bvTable1Context.BandPanelRowHeight = 35;
            this.bvTable1Context.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridDataBand});
            this.bvTable1Context.ColumnPanelRowHeight = 18;
            this.bvTable1Context.GridControl = this.gcTable1Context;
            this.bvTable1Context.Name = "bvTable1Context";
            this.bvTable1Context.OptionsBehavior.AllowIncrementalSearch = true;
            this.bvTable1Context.OptionsBehavior.Editable = false;
            this.bvTable1Context.OptionsCustomization.AllowBandMoving = false;
            this.bvTable1Context.OptionsCustomization.AllowColumnMoving = false;
            this.bvTable1Context.OptionsCustomization.AllowGroup = false;
            this.bvTable1Context.OptionsCustomization.AllowQuickHideColumns = false;
            this.bvTable1Context.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.bvTable1Context.OptionsPrint.AutoWidth = false;
            this.bvTable1Context.OptionsSelection.InvertSelection = true;
            this.bvTable1Context.OptionsView.AllowCellMerge = true;
            this.bvTable1Context.OptionsView.ColumnAutoWidth = false;
            this.bvTable1Context.OptionsView.ShowGroupPanel = false;
            this.bvTable1Context.OptionsView.ShowIndicator = false;
            this.bvTable1Context.RowHeight = 18;
            this.bvTable1Context.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.bvTable1Context_RowCellStyle);
            // 
            // gridDataBand
            // 
            this.gridDataBand.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridDataBand.AppearanceHeader.Options.UseFont = true;
            this.gridDataBand.Caption = "������";
            this.gridDataBand.MinWidth = 20;
            this.gridDataBand.Name = "gridDataBand";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BorderColor = System.Drawing.Color.Maroon;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.table1LayoutControlItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(634, 375);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // table1LayoutControlItem
            // 
            this.table1LayoutControlItem.Control = this.gcTable1Context;
            this.table1LayoutControlItem.CustomizationFormText = "layoutControlItem2";
            this.table1LayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.table1LayoutControlItem.MinSize = new System.Drawing.Size(110, 30);
            this.table1LayoutControlItem.Name = "table1LayoutControlItem";
            this.table1LayoutControlItem.Size = new System.Drawing.Size(630, 371);
            this.table1LayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.table1LayoutControlItem.Text = "table1LayoutControlItem";
            this.table1LayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.table1LayoutControlItem.TextToControlDistance = 0;
            this.table1LayoutControlItem.TextVisible = false;
            // 
            // DistributionContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "DistributionContext";
            this.Size = new System.Drawing.Size(634, 375);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTable1Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bvTable1Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table1LayoutControlItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gcTable1Context;
        private DevExpress.XtraLayout.LayoutControlItem table1LayoutControlItem;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bvTable1Context;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridDataBand;
    }
}
