namespace WccpReporting
{
    partial class MarginShiftContext
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gcTableContext = new DevExpress.XtraGrid.GridControl();
            this.advbvTableContext = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gvTableContext = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTableContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTableContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTableContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dataLayoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.dataLayoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dataLayoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.dataLayoutControl1.Controls.Add(this.gcTableContext);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1120, 135);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gcTableContext
            // 
            this.gcTableContext.Location = new System.Drawing.Point(2, 2);
            this.gcTableContext.MainView = this.advbvTableContext;
            this.gcTableContext.Name = "gcTableContext";
            this.gcTableContext.Size = new System.Drawing.Size(1116, 131);
            this.gcTableContext.TabIndex = 4;
            this.gcTableContext.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advbvTableContext,
            this.gvTableContext});
            // 
            // advbvTableContext
            // 
            this.advbvTableContext.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.advbvTableContext.Appearance.BandPanel.Options.UseFont = true;
            this.advbvTableContext.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.advbvTableContext.Appearance.HeaderPanel.Options.UseFont = true;
            this.advbvTableContext.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.advbvTableContext.GridControl = this.gcTableContext;
            this.advbvTableContext.Name = "advbvTableContext";
            this.advbvTableContext.OptionsBehavior.Editable = false;
            this.advbvTableContext.OptionsCustomization.AllowBandMoving = false;
            this.advbvTableContext.OptionsCustomization.AllowColumnMoving = false;
            this.advbvTableContext.OptionsCustomization.AllowGroup = false;
            this.advbvTableContext.OptionsCustomization.AllowQuickHideColumns = false;
            this.advbvTableContext.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.advbvTableContext.OptionsPrint.AutoWidth = false;
            this.advbvTableContext.OptionsView.ShowColumnHeaders = false;
            this.advbvTableContext.OptionsView.ShowGroupPanel = false;
            this.advbvTableContext.OptionsView.ShowIndicator = false;
            this.advbvTableContext.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.advbvTableContext_RowCellStyle);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "������";
            this.gridBand1.Name = "gridBand1";
            // 
            // gvTableContext
            // 
            this.gvTableContext.GridControl = this.gcTableContext;
            this.gvTableContext.Name = "gvTableContext";
            this.gvTableContext.OptionsView.ColumnAutoWidth = false;
            this.gvTableContext.OptionsView.ShowGroupPanel = false;
            this.gvTableContext.OptionsView.ShowIndicator = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BorderColor = System.Drawing.Color.Maroon;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1120, 135);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gcTableContext;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 131);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(110, 131);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(1116, 131);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // MarginShiftContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "MarginShiftContext";
            this.Size = new System.Drawing.Size(1120, 135);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTableContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTableContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTableContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gcTableContext;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTableContext;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advbvTableContext;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;


    }
}
