using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace WccpReporting
{
    public partial class MarginShiftContext : DevExpress.XtraEditors.XtraUserControl
    {
        public MarginShiftContext()
        {
            InitializeComponent();
        }

        //�������� ��������� ��� �����
        private string GetCaptionOwnerBand(string AValue)
        {
            string result = String.Empty;

            if (AValue.IndexOf("|") >= 0)
            {
                result = AValue.Substring(0, AValue.IndexOf("|"));
            }

            return result;
        }

        //�������� ��������� ��� �������
        private string GetCaptionChildBand(string AValue)
        {
            string result = String.Empty;

            if (AValue.IndexOf("|") >= 0)
            {
                result = AValue.Substring(AValue.IndexOf("|") + 1, AValue.Length - (AValue.IndexOf("|") + 1));
            };

            return result;
        }

        //���������� ������ ������
        public void ShowData(DataTable dt)
        {
            try
            {
                advbvTableContext.Bands.Clear();
                advbvTableContext.Columns.Clear();
                advbvTableContext.BeginUpdate();

                string saveCaptionBand = String.Empty;
                string captionOwnerBand = String.Empty;
                string captionChildBand = String.Empty;

                GridBand childBand = null;
                GridBand ownerBand = null;

                try
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        captionOwnerBand = GetCaptionOwnerBand(col.Caption);
                        captionChildBand = GetCaptionChildBand(col.Caption);

                        if (saveCaptionBand != captionOwnerBand)
                        {
                            saveCaptionBand = captionOwnerBand;
                            
                            if (ownerBand != null)
                            {
                              advbvTableContext.Bands.Add(ownerBand);
                            };

                            ownerBand = new GridBand();
                            ownerBand.Caption = captionOwnerBand;
                            ownerBand.AppearanceHeader.Options.UseTextOptions = true;
                            ownerBand.AppearanceHeader.Options.UseBackColor = true;
                            ownerBand.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;

                            if (col.Ordinal == 0)
                            {
                                ownerBand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                                ownerBand.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                            }
                            else
                            {
                                ownerBand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            };

                            if (captionOwnerBand.Trim().Length == 0)
                            {
                                ownerBand.AppearanceHeader.BackColor = Color.White;
                            };

                            childBand = new GridBand();
                            childBand.Caption = captionChildBand;
                            childBand.RowCount = 2;
                            childBand.AutoFillDown = true;
                            childBand.AppearanceHeader.Options.UseBackColor = true;
                            childBand.AppearanceHeader.Options.UseTextOptions = true;
                            childBand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            childBand.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                            childBand.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;

                            if (captionChildBand.Trim().Length == 0)
                            {
                                childBand.AppearanceHeader.BackColor = Color.White;
                            };

                            BandedGridColumn newBandCol = advbvTableContext.Columns.AddField(col.Caption);
                            newBandCol.Caption = " ";
                            newBandCol.Visible = true;

                            if (col.Ordinal == 0)
                            {
                                newBandCol.AppearanceCell.Options.UseFont = true;
                                newBandCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Bold);
                            };

                            newBandCol.OwnerBand = childBand;
                            ownerBand.Children.Add(childBand);
                        }
                        else
                        {
                            childBand = new GridBand();
                            childBand.Caption = captionChildBand;
                            childBand.RowCount = 2;
                            childBand.AutoFillDown = true;
                            childBand.AppearanceHeader.Options.UseTextOptions = true;
                            childBand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            childBand.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                            childBand.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;

                            BandedGridColumn newBandCol = advbvTableContext.Columns.AddField(col.Caption);
                            newBandCol.Caption = " ";
                            newBandCol.Visible = true;
                            newBandCol.OwnerBand = childBand;
                            ownerBand.Children.Add(childBand);
                        };
                    };
                    if (ownerBand != null)
                    {
                        advbvTableContext.Bands.Add(ownerBand);
                    };
                }
                finally
                {
                  advbvTableContext.EndUpdate();
                };

                if (dt != null)
                {
                    gcTableContext.DataSource = dt.DefaultView;
                    foreach (GridBand band in advbvTableContext.Bands)
                    {
                        if (band.Index == 0)
                        {
                            advbvTableContext.Bands[0].Width = 200;
                        }
                        else
                        {
                            if (band.HasChildren)
                            {
                              band.Width = (band.Children.Count*100);
                            };
                        };
                    };
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void advbvTableContext_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                Double value = 0;
                if (Double.TryParse(Convert.ToString(e.CellValue), out value))
                {
                        if (value < 0)
                        {
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (value == 0)
                        {
                            e.Appearance.ForeColor = Color.Silver;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        };
                };
            };
        }
    }
}
