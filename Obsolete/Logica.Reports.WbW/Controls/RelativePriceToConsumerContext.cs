using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraCharts;

namespace WccpReporting
{
    public partial class RelativePriceToConsumerContext : DevExpress.XtraEditors.XtraUserControl
    {
        public RelativePriceToConsumerContext()
        {
            InitializeComponent();
        }
        
        //���������� ������ ������
        public void ShowData(int WaveId, string KPI, string SKU1, string SKU2, string channel)
        {
            try
            {
                DataTable dataDataView = new DataTable();

                bvContext.Bands.Clear();
                bvContext.Columns.Clear();
                bvContext.BeginUpdate();
                
                GridBand newBand1 = new GridBand();
                newBand1.Caption = SKU1 + " x " + SKU2;
                newBand1.AppearanceHeader.Options.UseTextOptions = true;
                newBand1.AppearanceHeader.Options.UseBackColor = true;
                newBand1.AppearanceHeader.Options.UseBorderColor = true;
                newBand1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newBand1.AppearanceHeader.BackColor = Color.White;
                newBand1.AppearanceHeader.BorderColor = Color.White;
                newBand1.AutoFillDown = true;
                newBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                newBand1.Visible = true;
                bvContext.Bands.Add(newBand1);
                
                GridBand newBand2 = new GridBand();
                newBand2.Caption = "Relative PTC: PTC " + SKU1 + " - PTC " + SKU2;
                newBand2.AppearanceHeader.Options.UseTextOptions = true;
                newBand2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                newBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                newBand2.AutoFillDown = true;
                newBand2.Visible = true;
                bvContext.Bands.Add(newBand2);
                
                try
                {
                    using (OleDbCommand cmdDW_TomasResearch_WbW_PTC_Relative = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_PTC_Relative_ONTRADE" : "DW_TomasResearch_WbW_PTC_Relative"), WccpUIControl.Conn))
                    {
                        cmdDW_TomasResearch_WbW_PTC_Relative.CommandTimeout = WCCPConst.CommandTimeout;
                        cmdDW_TomasResearch_WbW_PTC_Relative.Parameters.AddWithValue("@Wave_ID", WaveId);
                        cmdDW_TomasResearch_WbW_PTC_Relative.Parameters.AddWithValue("@KPI", KPI);
                        cmdDW_TomasResearch_WbW_PTC_Relative.Parameters.AddWithValue("@SKU1", SKU1);
                        cmdDW_TomasResearch_WbW_PTC_Relative.Parameters.AddWithValue("@SKU2", SKU2);
                        cmdDW_TomasResearch_WbW_PTC_Relative.Parameters.AddWithValue("@Channel", channel);
                        cmdDW_TomasResearch_WbW_PTC_Relative.CommandType = CommandType.StoredProcedure;

                        using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_PTC_Relative))
                        {
                            da.Fill(dataDataView);
                        };
                    };
                    //
                    foreach (DataColumn col in dataDataView.Columns)
                    {
                        if (col.ColumnName != "ROW" && col.ColumnName != "IsChart")
                        {
                          BandedGridColumn newBandCol = bvContext.Columns.AddField(col.ColumnName);
                          newBandCol.FieldName = col.ColumnName;
                          newBandCol.AppearanceCell.Options.UseTextOptions = true;
                          newBandCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                          newBandCol.AppearanceHeader.Options.UseTextOptions = true;
                          newBandCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                          newBandCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                          newBandCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
                          newBandCol.Visible = true;

                          if (col.ColumnName != "Name")
                          {
                              newBandCol.Caption = col.Caption;
                              newBandCol.OwnerBand = newBand2;
                          }
                          else
                          {
                              newBandCol.AppearanceHeader.BackColor = Color.White;
                              newBandCol.AppearanceHeader.BorderColor = Color.White;
                              newBandCol.OptionsColumn.ShowCaption = false;
                              newBandCol.OwnerBand = newBand1;
                          };
                        };
                    };
                }
                finally
                {
                    bvContext.EndUpdate();
                };

                chartContext.Series[0].Name = String.Empty;
                chartContext.Series[1].Name = String.Empty;
                chartContext.Series[2].Name = String.Empty;
                chartContext.Series[3].Name = String.Empty;

                dataDataView.DefaultView.RowFilter = "IsChart = 1 AND ROW = 1";
                if (dataDataView.DefaultView.Count > 0)
                {
                    chartContext.Series[0].Name = Convert.ToString(dataDataView.DefaultView[0]["Name"]);
                    chartContext.Series[1].Name = "Avrg" + Convert.ToString(dataDataView.DefaultView[0]["Name"]);

                    foreach (DataColumn seriesCol in dataDataView.Columns)
                    {
                        if (seriesCol.ColumnName != "ROW" && seriesCol.ColumnName != "IsChart" && seriesCol.ColumnName != "Name" && seriesCol.ColumnName != "AVG")
                        {
                            chartContext.Series[0].Points.Add(new SeriesPoint(seriesCol.ColumnName, new double[] { Convert.ToDouble(dataDataView.DefaultView[0][seriesCol.ColumnName]) }));
                            chartContext.Series[1].Points.Add(new SeriesPoint(seriesCol.ColumnName, new double[] { Convert.ToDouble(dataDataView.DefaultView[0]["AVG"]) }));
                        };
                    };
                };
                //
                dataDataView.DefaultView.RowFilter = "IsChart = 1 AND ROW = 3";
                if (dataDataView.DefaultView.Count > 0)
                {
                    chartContext.Series[2].Name = Convert.ToString(dataDataView.DefaultView[0]["Name"]);
                    chartContext.Series[3].Name = "Avrg" + Convert.ToString(dataDataView.DefaultView[0]["Name"]);

                    foreach (DataColumn seriesCol in dataDataView.Columns)
                    {
                        if (seriesCol.ColumnName != "ROW" && seriesCol.ColumnName != "IsChart" && seriesCol.ColumnName != "Name" && seriesCol.ColumnName != "AVG")
                        {
                            chartContext.Series[2].Points.Add(new SeriesPoint(seriesCol.ColumnName, new double[] { Convert.ToDouble(dataDataView.DefaultView[0][seriesCol.ColumnName]) }));
                            chartContext.Series[3].Points.Add(new SeriesPoint(seriesCol.ColumnName, new double[] { Convert.ToDouble(dataDataView.DefaultView[0]["AVG"]) }));
                        };
                    };
                };

                if (dataDataView != null)
                {
                    dataDataView.DefaultView.RowFilter = String.Empty;
                    gcContext.DataSource = dataDataView.DefaultView;
                    bvContext.BestFitColumns();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };

        }

    }
}

