namespace WccpReporting
{
    partial class EvolutionReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EvolutionReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // EvolutionReportScrollableControl
            // 
            this.EvolutionReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EvolutionReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.EvolutionReportScrollableControl.Name = "EvolutionReportScrollableControl";
            this.EvolutionReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.EvolutionReportScrollableControl.TabIndex = 0;
            // 
            // EvolutionReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.EvolutionReportScrollableControl);
            this.Name = "EvolutionReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl EvolutionReportScrollableControl;

    }
}
