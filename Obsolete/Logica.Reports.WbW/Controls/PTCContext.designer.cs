namespace WccpReporting
{
    partial class PTCContext
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView2 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.dlcPTCContext = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gcTable3Context = new DevExpress.XtraGrid.GridControl();
            this.advbvTable3Context = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gBandLimitName = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgColLimitName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gBandPOCs1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgColPOCs1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gBandD_VOL = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgColD_VOL = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gBandD_MS = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgColD_MS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gcTable2Context = new DevExpress.XtraGrid.GridControl();
            this.advbvTable2Context = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gcTable1Context = new DevExpress.XtraGrid.GridControl();
            this.advbvTable1Context = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.chartContext = new DevExpress.XtraCharts.ChartControl();
            this.lbSKUName = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dlcPTCContext)).BeginInit();
            this.dlcPTCContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable3Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTable3Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable2Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTable2Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable1Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTable1Context)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // dlcPTCContext
            // 
            this.dlcPTCContext.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dlcPTCContext.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.dlcPTCContext.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dlcPTCContext.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.dlcPTCContext.Controls.Add(this.gcTable3Context);
            this.dlcPTCContext.Controls.Add(this.gcTable2Context);
            this.dlcPTCContext.Controls.Add(this.gcTable1Context);
            this.dlcPTCContext.Controls.Add(this.chartContext);
            this.dlcPTCContext.Controls.Add(this.lbSKUName);
            this.dlcPTCContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcPTCContext.Location = new System.Drawing.Point(0, 0);
            this.dlcPTCContext.Name = "dlcPTCContext";
            this.dlcPTCContext.Root = this.layoutControlGroup1;
            this.dlcPTCContext.Size = new System.Drawing.Size(1900, 370);
            this.dlcPTCContext.TabIndex = 0;
            this.dlcPTCContext.Text = "�������� ��� PTC ������";
            // 
            // gcTable3Context
            // 
            this.gcTable3Context.Location = new System.Drawing.Point(1032, 28);
            this.gcTable3Context.MainView = this.advbvTable3Context;
            this.gcTable3Context.Name = "gcTable3Context";
            this.gcTable3Context.Size = new System.Drawing.Size(324, 332);
            this.gcTable3Context.TabIndex = 1;
            this.gcTable3Context.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advbvTable3Context});
            // 
            // advbvTable3Context
            // 
            this.advbvTable3Context.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.advbvTable3Context.Appearance.BandPanel.Options.UseFont = true;
            this.advbvTable3Context.Appearance.BandPanel.Options.UseTextOptions = true;
            this.advbvTable3Context.Appearance.BandPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.advbvTable3Context.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.advbvTable3Context.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gBandLimitName,
            this.gBandPOCs1,
            this.gBandD_VOL,
            this.gBandD_MS});
            this.advbvTable3Context.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bgColLimitName,
            this.bgColPOCs1,
            this.bgColD_VOL,
            this.bgColD_MS});
            this.advbvTable3Context.GridControl = this.gcTable3Context;
            this.advbvTable3Context.Name = "advbvTable3Context";
            this.advbvTable3Context.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.advbvTable3Context.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.advbvTable3Context.OptionsBehavior.AllowIncrementalSearch = true;
            this.advbvTable3Context.OptionsBehavior.Editable = false;
            this.advbvTable3Context.OptionsCustomization.AllowBandMoving = false;
            this.advbvTable3Context.OptionsCustomization.AllowColumnMoving = false;
            this.advbvTable3Context.OptionsCustomization.AllowGroup = false;
            this.advbvTable3Context.OptionsCustomization.AllowQuickHideColumns = false;
            this.advbvTable3Context.OptionsPrint.AutoWidth = false;
            this.advbvTable3Context.OptionsView.ColumnAutoWidth = true;
            this.advbvTable3Context.OptionsView.ShowColumnHeaders = false;
            this.advbvTable3Context.OptionsView.ShowGroupPanel = false;
            this.advbvTable3Context.OptionsView.ShowIndicator = false;
            // 
            // gBandLimitName
            // 
            this.gBandLimitName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gBandLimitName.AppearanceHeader.Options.UseFont = true;
            this.gBandLimitName.AppearanceHeader.Options.UseTextOptions = true;
            this.gBandLimitName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gBandLimitName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gBandLimitName.Caption = "PTC shift ($/unit)";
            this.gBandLimitName.Columns.Add(this.bgColLimitName);
            this.gBandLimitName.Name = "gBandLimitName";
            this.gBandLimitName.OptionsBand.FixedWidth = true;
            this.gBandLimitName.RowCount = 2;
            this.gBandLimitName.Width = 140;
            // 
            // bgColLimitName
            // 
            this.bgColLimitName.FieldName = "LimitName";
            this.bgColLimitName.Name = "bgColLimitName";
            this.bgColLimitName.Visible = true;
            this.bgColLimitName.Width = 140;
            // 
            // gBandPOCs1
            // 
            this.gBandPOCs1.Caption = "% POCs";
            this.gBandPOCs1.Columns.Add(this.bgColPOCs1);
            this.gBandPOCs1.Name = "gBandPOCs1";
            this.gBandPOCs1.Width = 82;
            // 
            // bgColPOCs1
            // 
            this.bgColPOCs1.FieldName = "POCs1";
            this.bgColPOCs1.Name = "bgColPOCs1";
            this.bgColPOCs1.Visible = true;
            this.bgColPOCs1.Width = 82;
            // 
            // gBandD_VOL
            // 
            this.gBandD_VOL.Caption = "D Vol. (%)";
            this.gBandD_VOL.Columns.Add(this.bgColD_VOL);
            this.gBandD_VOL.Name = "gBandD_VOL";
            this.gBandD_VOL.Width = 82;
            // 
            // bgColD_VOL
            // 
            this.bgColD_VOL.FieldName = "D_VOL";
            this.bgColD_VOL.Name = "bgColD_VOL";
            this.bgColD_VOL.Visible = true;
            this.bgColD_VOL.Width = 82;
            // 
            // gBandD_MS
            // 
            this.gBandD_MS.Caption = "D MS (p.p)";
            this.gBandD_MS.Columns.Add(this.bgColD_MS);
            this.gBandD_MS.Name = "gBandD_MS";
            this.gBandD_MS.Width = 84;
            // 
            // bgColD_MS
            // 
            this.bgColD_MS.FieldName = "D_MS";
            this.bgColD_MS.Name = "bgColD_MS";
            this.bgColD_MS.Visible = true;
            this.bgColD_MS.Width = 84;
            // 
            // gcTable2Context
            // 
            this.gcTable2Context.Location = new System.Drawing.Point(1361, 28);
            this.gcTable2Context.MainView = this.advbvTable2Context;
            this.gcTable2Context.Name = "gcTable2Context";
            this.gcTable2Context.Size = new System.Drawing.Size(529, 332);
            this.gcTable2Context.TabIndex = 2;
            this.gcTable2Context.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advbvTable2Context});
            // 
            // advbvTable2Context
            // 
            this.advbvTable2Context.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.advbvTable2Context.Appearance.BandPanel.Options.UseFont = true;
            this.advbvTable2Context.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2});
            this.advbvTable2Context.GridControl = this.gcTable2Context;
            this.advbvTable2Context.Name = "advbvTable2Context";
            this.advbvTable2Context.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.advbvTable2Context.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.advbvTable2Context.OptionsBehavior.Editable = false;
            this.advbvTable2Context.OptionsCustomization.AllowBandMoving = false;
            this.advbvTable2Context.OptionsCustomization.AllowColumnMoving = false;
            this.advbvTable2Context.OptionsCustomization.AllowGroup = false;
            this.advbvTable2Context.OptionsCustomization.AllowQuickHideColumns = false;
            this.advbvTable2Context.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.advbvTable2Context.OptionsPrint.AutoWidth = false;
            this.advbvTable2Context.OptionsView.ColumnAutoWidth = true;
            this.advbvTable2Context.OptionsView.ShowColumnHeaders = false;
            this.advbvTable2Context.OptionsView.ShowGroupPanel = false;
            this.advbvTable2Context.OptionsView.ShowIndicator = false;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.Caption = "������";
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            // 
            // gcTable1Context
            // 
            this.gcTable1Context.Location = new System.Drawing.Point(514, 28);
            this.gcTable1Context.MainView = this.advbvTable1Context;
            this.gcTable1Context.Name = "gcTable1Context";
            this.gcTable1Context.Size = new System.Drawing.Size(513, 332);
            this.gcTable1Context.TabIndex = 0;
            this.gcTable1Context.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advbvTable1Context});
            // 
            // advbvTable1Context
            // 
            this.advbvTable1Context.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.advbvTable1Context.Appearance.BandPanel.Options.UseFont = true;
            this.advbvTable1Context.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.advbvTable1Context.GridControl = this.gcTable1Context;
            this.advbvTable1Context.Name = "advbvTable1Context";
            this.advbvTable1Context.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.advbvTable1Context.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.advbvTable1Context.OptionsBehavior.AllowIncrementalSearch = true;
            this.advbvTable1Context.OptionsBehavior.Editable = false;
            this.advbvTable1Context.OptionsCustomization.AllowBandMoving = false;
            this.advbvTable1Context.OptionsCustomization.AllowColumnMoving = false;
            this.advbvTable1Context.OptionsCustomization.AllowGroup = false;
            this.advbvTable1Context.OptionsCustomization.AllowQuickHideColumns = false;
            this.advbvTable1Context.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.advbvTable1Context.OptionsPrint.AutoWidth = false;
            this.advbvTable1Context.OptionsView.ColumnAutoWidth = true;
            this.advbvTable1Context.OptionsView.ShowColumnHeaders = false;
            this.advbvTable1Context.OptionsView.ShowGroupPanel = false;
            this.advbvTable1Context.OptionsView.ShowIndicator = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.Caption = "������";
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            // 
            // chartContext
            // 
            this.chartContext.BorderOptions.Color = System.Drawing.Color.Black;
            this.chartContext.BorderOptions.Thickness = 2;
            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Title.Alignment = System.Drawing.StringAlignment.Far;
            xyDiagram1.AxisX.Title.Text = "PTC ($/unit)";
            xyDiagram1.AxisX.Title.Visible = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Title.Alignment = System.Drawing.StringAlignment.Far;
            xyDiagram1.AxisY.Title.Text = "% POCs";
            xyDiagram1.AxisY.Title.Visible = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chartContext.Diagram = xyDiagram1;
            this.chartContext.Location = new System.Drawing.Point(10, 28);
            this.chartContext.Name = "chartContext";
            sideBySideBarSeriesLabel1.LineVisible = true;
            sideBySideBarSeriesLabel1.Visible = false;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.Name = "Series1";
            sideBySideBarSeriesView1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            sideBySideBarSeriesView1.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            series1.View = sideBySideBarSeriesView1;
            sideBySideBarSeriesLabel2.LineVisible = true;
            sideBySideBarSeriesLabel2.Visible = false;
            series2.Label = sideBySideBarSeriesLabel2;
            series2.Name = "Series2";
            sideBySideBarSeriesView2.Color = System.Drawing.Color.Gray;
            sideBySideBarSeriesView2.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            series2.View = sideBySideBarSeriesView2;
            this.chartContext.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            sideBySideBarSeriesLabel3.LineVisible = true;
            this.chartContext.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chartContext.Size = new System.Drawing.Size(499, 332);
            this.chartContext.TabIndex = 3;
            // 
            // lbSKUName
            // 
            this.lbSKUName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbSKUName.Appearance.Options.UseFont = true;
            this.lbSKUName.Location = new System.Drawing.Point(10, 10);
            this.lbSKUName.Name = "lbSKUName";
            this.lbSKUName.Size = new System.Drawing.Size(64, 13);
            this.lbSKUName.TabIndex = 0;
            this.lbSKUName.Text = "lbSKUName";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BorderColor = System.Drawing.Color.Maroon;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1900, 370);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lbSKUName;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1890, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gcTable1Context;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(504, 23);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(523, 337);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gcTable2Context;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(1351, 23);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(539, 337);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.chartContext;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(504, 337);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(504, 337);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(504, 337);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gcTable3Context;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(1027, 23);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(324, 337);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // PTCContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dlcPTCContext);
            this.Name = "PTCContext";
            this.Size = new System.Drawing.Size(1900, 370);
            ((System.ComponentModel.ISupportInitialize)(this.dlcPTCContext)).EndInit();
            this.dlcPTCContext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTable3Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTable3Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable2Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTable2Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTable1Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advbvTable1Context)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dlcPTCContext;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl lbSKUName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraCharts.ChartControl chartContext;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gcTable2Context;
        private DevExpress.XtraGrid.GridControl gcTable1Context;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advbvTable1Context;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advbvTable2Context;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.GridControl gcTable3Context;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advbvTable3Context;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gBandLimitName;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gBandPOCs1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gBandD_VOL;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gBandD_MS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgColLimitName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgColPOCs1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgColD_VOL;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgColD_MS;
    }
}
