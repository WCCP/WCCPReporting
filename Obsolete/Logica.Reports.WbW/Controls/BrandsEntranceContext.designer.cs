namespace WccpReporting
{
    partial class BrandsEntranceContext
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gcTableContext = new DevExpress.XtraGrid.GridControl();
            this.gvTableContext = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tableLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTableContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTableContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableLayoutControlItem)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dataLayoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.dataLayoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dataLayoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.dataLayoutControl1.Controls.Add(this.gcTableContext);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(640, 165);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gcTableContext
            // 
            this.gcTableContext.Location = new System.Drawing.Point(7, 7);
            this.gcTableContext.MainView = this.gvTableContext;
            this.gcTableContext.Name = "gcTableContext";
            this.gcTableContext.Size = new System.Drawing.Size(626, 151);
            this.gcTableContext.TabIndex = 4;
            this.gcTableContext.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTableContext});
            // 
            // gvTableContext
            // 
            this.gvTableContext.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gvTableContext.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvTableContext.ColumnPanelRowHeight = 35;
            this.gvTableContext.GridControl = this.gcTableContext;
            this.gvTableContext.Name = "gvTableContext";
            this.gvTableContext.OptionsBehavior.Editable = false;
            this.gvTableContext.OptionsCustomization.AllowColumnMoving = false;
            this.gvTableContext.OptionsCustomization.AllowGroup = false;
            this.gvTableContext.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvTableContext.OptionsCustomization.AllowSort = false;
            this.gvTableContext.OptionsPrint.AutoWidth = false;
            this.gvTableContext.OptionsView.ColumnAutoWidth = false;
            this.gvTableContext.OptionsView.ShowGroupPanel = false;
            this.gvTableContext.OptionsView.ShowIndicator = false;
            this.gvTableContext.RowHeight = 18;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BorderColor = System.Drawing.Color.Maroon;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tableLayoutControlItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(640, 165);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tableLayoutControlItem
            // 
            this.tableLayoutControlItem.Control = this.gcTableContext;
            this.tableLayoutControlItem.CustomizationFormText = "layoutControlItem1";
            this.tableLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutControlItem.Name = "tableLayoutControlItem";
            this.tableLayoutControlItem.Size = new System.Drawing.Size(636, 161);
            this.tableLayoutControlItem.Text = "tableLayoutControlItem";
            this.tableLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Left;
            this.tableLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.tableLayoutControlItem.TextToControlDistance = 0;
            this.tableLayoutControlItem.TextVisible = false;
            // 
            // BrandsEntranceContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "BrandsEntranceContext";
            this.Size = new System.Drawing.Size(640, 165);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTableContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTableContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableLayoutControlItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gcTableContext;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTableContext;
        private DevExpress.XtraLayout.LayoutControlItem tableLayoutControlItem;
    }
}
