using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace WccpReporting
{
    class DM
    {
        private static OleDbConnection _Connection;
        public static OleDbConnection Connection
        {
            get { return _Connection; }
        }
        public static OleDbConnection Connect(string AConString)
        {
            try
            {
                if (AConString.Length == 0)
                {
                    throw new Exception("������ ����������� �� ����������");
                };

                _Connection = new OleDbConnection(AConString);
                return Connection;
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorConnect + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            };
        }
    }
}
