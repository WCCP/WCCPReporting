namespace WccpReporting
{
    partial class Wait
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientDataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ProgressBar = new DevExpress.XtraEditors.ProgressBarControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.pbWait = new System.Windows.Forms.PictureBox();
            this.pnClient = new DevExpress.XtraEditors.PanelControl();
            this.lbMessage = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcProgressBarItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.clientDataLayoutControl)).BeginInit();
            this.clientDataLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnClient)).BeginInit();
            this.pnClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcProgressBarItem)).BeginInit();
            this.SuspendLayout();
            // 
            // clientDataLayoutControl
            // 
            this.clientDataLayoutControl.Controls.Add(this.ProgressBar);
            this.clientDataLayoutControl.Controls.Add(this.progressBarControl1);
            this.clientDataLayoutControl.Controls.Add(this.pbWait);
            this.clientDataLayoutControl.Controls.Add(this.pnClient);
            this.clientDataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientDataLayoutControl.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.clientDataLayoutControl.Location = new System.Drawing.Point(0, 0);
            this.clientDataLayoutControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.clientDataLayoutControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.clientDataLayoutControl.Name = "clientDataLayoutControl";
            this.clientDataLayoutControl.Root = this.layoutControlGroup1;
            this.clientDataLayoutControl.Size = new System.Drawing.Size(447, 66);
            this.clientDataLayoutControl.TabIndex = 0;
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(2, 44);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.ProgressBar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ProgressBar.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ProgressBar.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.ProgressBar.Properties.ShowTitle = true;
            this.ProgressBar.Properties.Step = 1;
            this.ProgressBar.Size = new System.Drawing.Size(443, 18);
            this.ProgressBar.StyleController = this.clientDataLayoutControl;
            this.ProgressBar.TabIndex = 1;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(105, 48);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(320, 18);
            this.progressBarControl1.StyleController = this.clientDataLayoutControl;
            this.progressBarControl1.TabIndex = 4;
            // 
            // pbWait
            // 
            this.pbWait.Image = global::WccpReporting.Properties.Resources.wait_32;
            this.pbWait.Location = new System.Drawing.Point(2, 2);
            this.pbWait.Name = "pbWait";
            this.pbWait.Size = new System.Drawing.Size(42, 42);
            this.pbWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWait.TabIndex = 2;
            this.pbWait.TabStop = false;
            // 
            // pnClient
            // 
            this.pnClient.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnClient.Controls.Add(this.lbMessage);
            this.pnClient.Location = new System.Drawing.Point(44, 2);
            this.pnClient.Name = "pnClient";
            this.pnClient.Size = new System.Drawing.Size(401, 42);
            this.pnClient.TabIndex = 0;
            // 
            // lbMessage
            // 
            this.lbMessage.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbMessage.Appearance.Options.UseFont = true;
            this.lbMessage.Appearance.Options.UseTextOptions = true;
            this.lbMessage.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbMessage.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbMessage.Location = new System.Drawing.Point(43, 15);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(314, 13);
            this.lbMessage.TabIndex = 0;
            this.lbMessage.Text = "���������, ����������.  ���� ��������� ������...";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.progressBarControl1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(429, 29);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.lcProgressBarItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(447, 66);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pnClient;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(42, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(401, 42);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.pbWait;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(42, 42);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(42, 42);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(42, 42);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // lcProgressBarItem
            // 
            this.lcProgressBarItem.Control = this.ProgressBar;
            this.lcProgressBarItem.CustomizationFormText = "layoutControlItem4";
            this.lcProgressBarItem.Location = new System.Drawing.Point(0, 42);
            this.lcProgressBarItem.Name = "lcProgressBarItem";
            this.lcProgressBarItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcProgressBarItem.Size = new System.Drawing.Size(443, 20);
            this.lcProgressBarItem.Text = "lcProgressBarItem";
            this.lcProgressBarItem.TextSize = new System.Drawing.Size(0, 0);
            this.lcProgressBarItem.TextToControlDistance = 0;
            this.lcProgressBarItem.TextVisible = false;
            // 
            // Wait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 66);
            this.ControlBox = false;
            this.Controls.Add(this.clientDataLayoutControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Wait";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������";
            ((System.ComponentModel.ISupportInitialize)(this.clientDataLayoutControl)).EndInit();
            this.clientDataLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnClient)).EndInit();
            this.pnClient.ResumeLayout(false);
            this.pnClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcProgressBarItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl clientDataLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl pnClient;
        private DevExpress.XtraEditors.LabelControl lbMessage;
        private System.Windows.Forms.PictureBox pbWait;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.ProgressBarControl ProgressBar;
        private DevExpress.XtraLayout.LayoutControlItem lcProgressBarItem;

    }
}