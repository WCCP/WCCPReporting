using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace WccpReporting
{
    public partial class MktOverviewBySegmentsContext : DevExpress.XtraEditors.XtraUserControl
    {
        public MktOverviewBySegmentsContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(DataTable dt)
        {
            try
            {
                gvTableContext.Columns.Clear();
                gvTableContext.BeginUpdate();

                GridColumn newCol = null;

                try
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        newCol = gvTableContext.Columns.AddField(col.Caption);
                        newCol.Caption = col.Caption;

                        if (col.Caption == "Chapter")
                        {
                            newCol.AppearanceCell.Options.UseTextOptions = true;
                            newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Bold);
                            newCol.AppearanceHeader.BackColor = Color.White;
                            newCol.AppearanceHeader.BorderColor = Color.White;
                            newCol.OptionsColumn.ShowCaption = false;
                        };

                        if (col.Caption == "Description")
                        {
                            newCol.AppearanceCell.Options.UseTextOptions = true;
                            newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                            newCol.AppearanceHeader.BackColor = Color.White;
                            newCol.AppearanceHeader.BorderColor = Color.White;
                            newCol.OptionsColumn.ShowCaption = false;
                        };

                        if (col.Caption == "Total")
                        {
                            newCol.AppearanceCell.Options.UseTextOptions = true;
                            newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                            newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (col.Caption != "Chapter")
                        {
                            newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                        }
                        else
                        {
                            newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
                        };

                        if (col.ColumnName != "GROUP" && col.ColumnName != "isTotal" && col.ColumnName != "isBrand")
                            newCol.Visible = true;
                        else
                        {
                            newCol.Visible = false;
                        };
                    };
                }
                finally
                {
                    gvTableContext.EndUpdate();
                };

                if (dt != null)
                {
                    gcTableContext.DataSource = dt.DefaultView;
                    gvTableContext.BestFitColumns();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //������� ��� �������������, totals � ������ ����
        private void gvTableContext_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                string isTotal = View.GetRowCellDisplayText(e.RowHandle, View.Columns["isTotal"]);
                if (e.Column.Caption != "Chapter" && e.Column.Caption != "Description" && e.Column.Caption != "GROUP" && e.Column.Caption != "isTotal" && e.Column.Caption != "isBrand")
                {
                    if (e.CellValue != DBNull.Value)
                    {

                        if (Convert.ToDouble(e.CellValue) < 0)
                        {
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (Convert.ToDouble(e.CellValue) == 0)
                        {
                            e.Appearance.ForeColor = Color.Silver;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        };
                    };
                };
                if (isTotal == "1")
                {
                    if (e.Column.Caption != "Chapter")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                    };
                    e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                };
            };
        }

    }
}
