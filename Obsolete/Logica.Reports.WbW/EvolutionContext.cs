using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using System.Data.OleDb;

namespace WccpReporting
{
    public partial class EvolutionContext : DevExpress.XtraEditors.XtraUserControl
    {
        public EvolutionContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(string channel, int WaveId, string WaveName, int prevWave_ID, string prevWaveName, int prevprevWave_ID, string prevprevWaveName)
        {
            try
            {
                DataTable dataDataView2 = new DataTable();

                using (OleDbCommand cmdDW_TomasResearch_WbW_Evolution = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_Evolution_ONTRADE" : "DW_TomasResearch_WbW_Evolution"), WccpUIControl.Conn))
                {
                    cmdDW_TomasResearch_WbW_Evolution.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_Evolution.Parameters.AddWithValue("@Wave_ID", WaveId);
                    cmdDW_TomasResearch_WbW_Evolution.Parameters.AddWithValue("@prevWave_ID", prevWave_ID);
                    cmdDW_TomasResearch_WbW_Evolution.Parameters.AddWithValue("@prevprevWave_ID", prevprevWave_ID);
                    cmdDW_TomasResearch_WbW_Evolution.CommandType = CommandType.StoredProcedure;

                    using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_Evolution))
                    {
                        da.Fill(dataDataView2);
                    };
                };

                gvTable2Context.Columns.Clear();
                gvTable2Context.BeginUpdate();

                try
                {
                    //������ ������� - Evolution of brand availability
                    
                    foreach (DataColumn col in dataDataView2.Columns)
                    {
                        GridColumn newCol = gvTable2Context.Columns.AddField(col.ColumnName);
                        newCol.AppearanceCell.Options.UseFont = true;
                        newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        newCol.AppearanceCell.Options.UseTextOptions = true;
                        newCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                        newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Default;
                        newCol.AppearanceHeader.Options.UseTextOptions = true;
                        newCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                        newCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                        newCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                        newCol.Caption = col.Caption;

                        if (col.ColumnName == "SUBKPI")
                        {
                            newCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
                            newCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Default;
                            newCol.AppearanceCell.Font = new Font("Tahoma", 8, FontStyle.Bold);
                            newCol.Fixed = FixedStyle.Left;
                            newCol.Width = 150;
                        }
                        else
                        {
                            if (col.ColumnName == "WaveName")
                            {
                                newCol.Fixed = FixedStyle.Left;
                                newCol.Width = 125;
                            }
                            else
                                newCol.Width = 65;
                        };

                        newCol.Visible = true;

                        if (col.ColumnName != "SUBKPI")
                        {
                            newCol.OptionsColumn.ShowCaption = true;
                        }
                        else
                        {
                            newCol.AppearanceHeader.BackColor = Color.White;
                            newCol.AppearanceHeader.BorderColor = Color.White;
                            newCol.OptionsColumn.ShowCaption = false;
                        };
                    };
                }
                finally
                {
                    gvTable2Context.EndUpdate();
                };

                if (dataDataView2.Rows.Count > 0)
                {
                    gcTable2Context.DataSource = dataDataView2.DefaultView;
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //������� ��� �������������, totals � ������ ����
        private void gvTable2Context_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                if (e.Column.Caption != "WaveName" && e.Column.Caption != "SUBKPI")
                {
                    if (e.CellValue != DBNull.Value)
                    {
                        if (Convert.ToDouble(e.CellValue) < 0)
                        {
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        };

                        if (Convert.ToDouble(e.CellValue) == 0)
                        {
                            e.Appearance.ForeColor = Color.Silver;
                            e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Regular);
                        };
                    };
                };
            }

        }


    }
}
