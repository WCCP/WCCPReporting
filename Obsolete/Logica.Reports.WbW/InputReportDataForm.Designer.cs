namespace WccpReporting
{
    partial class InputReportDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbRegion = new System.Windows.Forms.Label();
            this.dsInputReportDataForm = new WccpReporting.ReportDataSet();
            this.lbChanel = new System.Windows.Forms.Label();
            this.cboxChanel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.luRegion = new DevExpress.XtraEditors.LookUpEdit();
            this.bsRegions = new System.Windows.Forms.BindingSource(this.components);
            this.lbWave = new DevExpress.XtraEditors.LabelControl();
            this.luWave = new DevExpress.XtraEditors.LookUpEdit();
            this.bsWave = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).BeginInit();
            this.PageControlOptions.SuspendLayout();
            this.OptionsTabSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).BeginInit();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).BeginInit();
            this.grpOptionsReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).BeginInit();
            this.PanelClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsInputReportDataForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxChanel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luWave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWave)).BeginInit();
            this.SuspendLayout();
            // 
            // OptionsTabSheet
            // 
            this.OptionsTabSheet.ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
            // 
            // grpOptionsReport
            // 
            this.grpOptionsReport.Controls.Add(this.luWave);
            this.grpOptionsReport.Controls.Add(this.lbWave);
            this.grpOptionsReport.Controls.Add(this.luRegion);
            this.grpOptionsReport.Controls.Add(this.cboxChanel);
            this.grpOptionsReport.Controls.Add(this.lbChanel);
            this.grpOptionsReport.Controls.Add(this.lbRegion);
            this.grpOptionsReport.TabIndex = 0;
            // 
            // edReportName
            // 
            this.edReportName.EditValue = "";
            this.edReportName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.edReportName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edReportName.Properties.Appearance.Options.UseBackColor = true;
            this.edReportName.Properties.Appearance.Options.UseFont = true;
            // 
            // btnHelp
            // 
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnYes
            // 
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // lbRegion
            // 
            this.lbRegion.AutoSize = true;
            this.lbRegion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbRegion.Location = new System.Drawing.Point(8, 19);
            this.lbRegion.Name = "lbRegion";
            this.lbRegion.Size = new System.Drawing.Size(46, 13);
            this.lbRegion.TabIndex = 0;
            this.lbRegion.Text = "&������:";
            // 
            // dsInputReportDataForm
            // 
            this.dsInputReportDataForm.DataSetName = "ReportDataSet";
            this.dsInputReportDataForm.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lbChanel
            // 
            this.lbChanel.AutoSize = true;
            this.lbChanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbChanel.Location = new System.Drawing.Point(8, 64);
            this.lbChanel.Name = "lbChanel";
            this.lbChanel.Size = new System.Drawing.Size(42, 13);
            this.lbChanel.TabIndex = 2;
            this.lbChanel.Text = "&�����:";
            // 
            // cboxChanel
            // 
            this.cboxChanel.Location = new System.Drawing.Point(8, 80);
            this.cboxChanel.Name = "cboxChanel";
            this.cboxChanel.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cboxChanel.Properties.Appearance.Options.UseBackColor = true;
            this.cboxChanel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboxChanel.Properties.Items.AddRange(new object[] {
            "Off-trade",
            "On-trade"});
            this.cboxChanel.Properties.NullText = "�� ������";
            this.cboxChanel.Properties.ShowPopupShadow = false;
            this.cboxChanel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboxChanel.Size = new System.Drawing.Size(325, 22);
            this.cboxChanel.TabIndex = 3;
            // 
            // luRegion
            // 
            this.luRegion.Location = new System.Drawing.Point(8, 36);
            this.luRegion.Name = "luRegion";
            this.luRegion.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.luRegion.Properties.Appearance.Options.UseBackColor = true;
            this.luRegion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luRegion.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Region_Name", "������")});
            this.luRegion.Properties.DataSource = this.bsRegions;
            this.luRegion.Properties.DisplayMember = "Region_Name";
            this.luRegion.Properties.DropDownRows = 10;
            this.luRegion.Properties.NullText = "�� ������";
            this.luRegion.Properties.ShowPopupShadow = false;
            this.luRegion.Properties.ValueMember = "Region_Id";
            this.luRegion.Size = new System.Drawing.Size(325, 22);
            this.luRegion.TabIndex = 1;
            // 
            // bsRegions
            // 
            this.bsRegions.DataMember = "tblRegions";
            this.bsRegions.DataSource = this.dsInputReportDataForm;
            // 
            // lbWave
            // 
            this.lbWave.Location = new System.Drawing.Point(8, 112);
            this.lbWave.Name = "lbWave";
            this.lbWave.Size = new System.Drawing.Size(34, 13);
            this.lbWave.TabIndex = 4;
            this.lbWave.Text = "&�����:";
            // 
            // luWave
            // 
            this.luWave.Location = new System.Drawing.Point(8, 128);
            this.luWave.Name = "luWave";
            this.luWave.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.luWave.Properties.Appearance.Options.UseBackColor = true;
            this.luWave.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luWave.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WaveName", "�����")});
            this.luWave.Properties.DataSource = this.bsWave;
            this.luWave.Properties.DisplayMember = "WaveName";
            this.luWave.Properties.NullText = "�� ������";
            this.luWave.Properties.ShowPopupShadow = false;
            this.luWave.Properties.ValueMember = "Wave_Id";
            this.luWave.Size = new System.Drawing.Size(325, 22);
            this.luWave.TabIndex = 5;
            // 
            // bsWave
            // 
            this.bsWave.DataMember = "tblWave";
            this.bsWave.DataSource = this.dsInputReportDataForm;
            // 
            // InputReportDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(369, 450);
            this.Name = "InputReportDataForm";
            this.Load += new System.EventHandler(this.InputProgressReportDataForm_Load);
            this.Shown += new System.EventHandler(this.InputProgressReportDataForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).EndInit();
            this.PageControlOptions.ResumeLayout(false);
            this.OptionsTabSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).EndInit();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).EndInit();
            this.grpOptionsReport.ResumeLayout(false);
            this.grpOptionsReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).EndInit();
            this.PanelClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsInputReportDataForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxChanel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luWave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWave)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbRegion;
        private ReportDataSet dsInputReportDataForm;
        private System.Windows.Forms.Label lbChanel;
        private DevExpress.XtraEditors.ComboBoxEdit cboxChanel;
        private DevExpress.XtraEditors.LookUpEdit luRegion;
        private System.Windows.Forms.BindingSource bsRegions;
        private DevExpress.XtraEditors.LookUpEdit luWave;
        private DevExpress.XtraEditors.LabelControl lbWave;
        private System.Windows.Forms.BindingSource bsWave;
    }
}
