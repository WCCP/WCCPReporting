namespace WccpReporting
{
    partial class BrandsEntranceReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrandsEntranceReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // BrandsEntranceReportScrollableControl
            // 
            this.BrandsEntranceReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrandsEntranceReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.BrandsEntranceReportScrollableControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.BrandsEntranceReportScrollableControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BrandsEntranceReportScrollableControl.Name = "BrandsEntranceReportScrollableControl";
            this.BrandsEntranceReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.BrandsEntranceReportScrollableControl.TabIndex = 0;
            // 
            // BrandsEntranceReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BrandsEntranceReportScrollableControl);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "BrandsEntranceReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl BrandsEntranceReportScrollableControl;

    }
}
