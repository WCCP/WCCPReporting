namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.pnReportSheet = new DevExpress.XtraEditors.PanelControl();
            this.tcReport = new DevExpress.XtraTab.XtraTabControl();
            this.tpOptions = new DevExpress.XtraTab.XtraTabPage();
            this.pnMain = new DevExpress.XtraEditors.PanelControl();
            this.pnClient = new DevExpress.XtraEditors.PanelControl();
            this.tcReportOptions = new DevExpress.XtraTab.XtraTabControl();
            this.tabOptionsReportLists = new DevExpress.XtraTab.XtraTabPage();
            this.dlcReportList = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.chkedSelectAll = new DevExpress.XtraEditors.CheckEdit();
            this.bmReport = new DevExpress.XtraBars.BarManager(this.components);
            this.barMenu = new DevExpress.XtraBars.Bar();
            this.tbtnReportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.tbtnReportRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bdocReport = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bdcReport = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imgNormal = new DevExpress.Utils.ImageCollection(this.components);
            this.imgLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcReportList = new DevExpress.XtraGrid.GridControl();
            this.bsReportList = new System.Windows.Forms.BindingSource(this.components);
            this.reportDataSet = new WccpReporting.ReportDataSet();
            this.gvReportList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReport_Checked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colReport_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgReportList = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciGridReportList = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabOptionsCompetPair = new DevExpress.XtraTab.XtraTabPage();
            this.dlcCompetPara = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancelCompetPair = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveCompetPair = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnDefaultCompetPair = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelAllCompetPair = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelCompetPair = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditCompetPair = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddCompetPair = new DevExpress.XtraEditors.SimpleButton();
            this.pnEditCompetPair = new DevExpress.XtraEditors.PanelControl();
            this.luSKUName2 = new DevExpress.XtraEditors.LookUpEdit();
            this.bsSKU2 = new System.Windows.Forms.BindingSource(this.components);
            this.luSKUName1 = new DevExpress.XtraEditors.LookUpEdit();
            this.bsSKU1 = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gcCompetPair = new DevExpress.XtraGrid.GridControl();
            this.bsCompetPair = new System.Windows.Forms.BindingSource(this.components);
            this.gvCompetPair = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolSKUName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSKUName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnCompetPairCaption = new DevExpress.XtraEditors.PanelControl();
            this.lbCompetPairCaption = new System.Windows.Forms.Label();
            this.lcgCompetPara = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabOptionsSKU = new DevExpress.XtraTab.XtraTabPage();
            this.dlcSKU = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.btnDefaultSKU = new DevExpress.XtraEditors.SimpleButton();
            this.gcSKU = new DevExpress.XtraGrid.GridControl();
            this.bsSKUList = new System.Windows.Forms.BindingSource(this.components);
            this.gvSKU = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolSKUListIsChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSKUListSKU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbHeaderSKU = new DevExpress.XtraEditors.LabelControl();
            this.lcgSKU = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciSKU = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciGridSKU = new DevExpress.XtraLayout.LayoutControlItem();
            this.espSKU = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciBtnSKU = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabOptionsRelativePTCLimits = new DevExpress.XtraTab.XtraTabPage();
            this.dlcRelativePTCLimit = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.btnDefaultRelativePTCLimits = new DevExpress.XtraEditors.SimpleButton();
            this.gcRelativePTCLimits = new DevExpress.XtraGrid.GridControl();
            this.bsRelativeRPTLimits = new System.Windows.Forms.BindingSource(this.components);
            this.gvRelativePTCLimits = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolLimitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolMinRelativePTCLimit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolMaxRelativePTCLimit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnRelativePTCLimits = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.lcgRelativePTCLimit = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabOptionsPTCLimits = new DevExpress.XtraTab.XtraTabPage();
            this.dlcPTCLimit = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.btnDefaultSKUPTCLimit = new DevExpress.XtraEditors.SimpleButton();
            this.gcPTCLimit = new DevExpress.XtraGrid.GridControl();
            this.bsPTCLimit = new System.Windows.Forms.BindingSource(this.components);
            this.gvPTCLimit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolPTCLimitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolMinPTCLimit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolMaxPTCLimit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKUPTCLimit = new DevExpress.XtraGrid.GridControl();
            this.bsSKUPTCLimit = new System.Windows.Forms.BindingSource(this.components);
            this.gvSKUPTCLimit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolSKUPTCLimit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnPTCLimits = new DevExpress.XtraEditors.PanelControl();
            this.lbPTCLimits = new System.Windows.Forms.Label();
            this.lcgPTCLimit = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabOptionsPTCShiftLimits = new DevExpress.XtraTab.XtraTabPage();
            this.dlcPTCShiftLimitsOptions = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.btnDefaultPTCShiftLimits = new DevExpress.XtraEditors.SimpleButton();
            this.gcPTCShiftLimits = new DevExpress.XtraGrid.GridControl();
            this.bsPTCShiftLimit = new System.Windows.Forms.BindingSource(this.components);
            this.gvPTCShiftLimits = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolPTCShiftName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabOptionsGeneralAndEdvancedSKU = new DevExpress.XtraTab.XtraTabPage();
            this.dlcGeneralAndEdvanced = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcDopSKU = new DevExpress.XtraGrid.GridControl();
            this.bsDopSKU = new System.Windows.Forms.BindingSource(this.components);
            this.gvDopSKU = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcOsnSKU = new DevExpress.XtraGrid.GridControl();
            this.bsOsnSKU = new System.Windows.Forms.BindingSource(this.components);
            this.gvOsnSKU = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKUAll = new DevExpress.XtraGrid.GridControl();
            this.bsSKUAll = new System.Windows.Forms.BindingSource(this.components);
            this.gvSKUAll = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnInsertToAdvancedSKU = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemoveFromAdvancedSKU = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemoveFromBasicSKU = new DevExpress.XtraEditors.SimpleButton();
            this.btnInsertToBasicSKU = new DevExpress.XtraEditors.SimpleButton();
            this.btnDefaultBasicAndAddSKU = new DevExpress.XtraEditors.SimpleButton();
            this.sedSKUInBevCount = new DevExpress.XtraEditors.SpinEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.pnBasicAndAdvancedSKU = new DevExpress.XtraEditors.PanelControl();
            this.lbBasicAndAdvancedSKU = new System.Windows.Forms.Label();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgGeneralAndEdvanced = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.pnLeft = new DevExpress.XtraEditors.PanelControl();
            this.navBarMenu = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarReportsGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarReportListsItem = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarOptionsGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarCompetParaItem = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarSKUItem = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarRelativePTCLimitsItem = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarPTCLimitsItem = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarPTCShiftLimitsItem = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGeneralAndEdvancedSKUItem = new DevExpress.XtraNavBar.NavBarItem();
            this.pnCreateReports = new DevExpress.XtraEditors.PanelControl();
            this.btnCreateReports = new DevExpress.XtraEditors.SimpleButton();
            this.pnReportHeader = new DevExpress.XtraEditors.PanelControl();
            this.edWave3Text = new DevExpress.XtraEditors.TextEdit();
            this.lbWave3 = new DevExpress.XtraEditors.LabelControl();
            this.edWave2Text = new DevExpress.XtraEditors.TextEdit();
            this.lbWave2 = new DevExpress.XtraEditors.LabelControl();
            this.edWave1Text = new DevExpress.XtraEditors.TextEdit();
            this.lbWave1 = new DevExpress.XtraEditors.LabelControl();
            this.edChanelText = new DevExpress.XtraEditors.TextEdit();
            this.lbChanel = new DevExpress.XtraEditors.LabelControl();
            this.edRegionText = new DevExpress.XtraEditors.TextEdit();
            this.lbRegion = new DevExpress.XtraEditors.LabelControl();
            this.lbReportTitle = new System.Windows.Forms.Label();
            this.pnReportToolBar = new DevExpress.XtraEditors.PanelControl();
            this.pmCompetPara = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pmAddItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pmEditItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.pmDelItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pmDelAllItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.pmDefaultItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imgBtn = new DevExpress.Utils.ImageCollection(this.components);
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).BeginInit();
            this.pnReportSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcReport)).BeginInit();
            this.tcReport.SuspendLayout();
            this.tpOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).BeginInit();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnClient)).BeginInit();
            this.pnClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcReportOptions)).BeginInit();
            this.tcReportOptions.SuspendLayout();
            this.tabOptionsReportLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcReportList)).BeginInit();
            this.dlcReportList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkedSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcReportList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsReportList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReportList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgReportList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGridReportList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            this.tabOptionsCompetPair.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcCompetPara)).BeginInit();
            this.dlcCompetPara.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnEditCompetPair)).BeginInit();
            this.pnEditCompetPair.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luSKUName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKU2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luSKUName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKU1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcCompetPair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsCompetPair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCompetPair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCompetPairCaption)).BeginInit();
            this.pnCompetPairCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCompetPara)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.tabOptionsSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcSKU)).BeginInit();
            this.dlcSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKUList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGridSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.espSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBtnSKU)).BeginInit();
            this.tabOptionsRelativePTCLimits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcRelativePTCLimit)).BeginInit();
            this.dlcRelativePTCLimit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRelativePTCLimits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRelativeRPTLimits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRelativePTCLimits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnRelativePTCLimits)).BeginInit();
            this.pnRelativePTCLimits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRelativePTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.tabOptionsPTCLimits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcPTCLimit)).BeginInit();
            this.dlcPTCLimit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSKUPTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKUPTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSKUPTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnPTCLimits)).BeginInit();
            this.pnPTCLimits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcgPTCLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.tabOptionsPTCShiftLimits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcPTCShiftLimitsOptions)).BeginInit();
            this.dlcPTCShiftLimitsOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPTCShiftLimits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPTCShiftLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPTCShiftLimits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            this.tabOptionsGeneralAndEdvancedSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcGeneralAndEdvanced)).BeginInit();
            this.dlcGeneralAndEdvanced.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDopSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDopSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDopSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcOsnSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOsnSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOsnSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSKUAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKUAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSKUAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sedSKUInBevCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBasicAndAdvancedSKU)).BeginInit();
            this.pnBasicAndAdvancedSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGeneralAndEdvanced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeft)).BeginInit();
            this.pnLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCreateReports)).BeginInit();
            this.pnCreateReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).BeginInit();
            this.pnReportHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edWave3Text.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edWave2Text.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edWave1Text.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edChanelText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edRegionText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).BeginInit();
            this.pnReportToolBar.SuspendLayout();
            this.pmCompetPara.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnReportSheet
            // 
            this.pnReportSheet.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnReportSheet.Controls.Add(this.tcReport);
            this.pnReportSheet.Controls.Add(this.pnReportHeader);
            this.pnReportSheet.Controls.Add(this.pnReportToolBar);
            this.pnReportSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnReportSheet.Location = new System.Drawing.Point(0, 0);
            this.pnReportSheet.Name = "pnReportSheet";
            this.pnReportSheet.Size = new System.Drawing.Size(885, 480);
            this.pnReportSheet.TabIndex = 0;
            // 
            // tcReport
            // 
            this.tcReport.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tcReport.AppearancePage.Header.Options.UseFont = true;
            this.tcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReport.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tcReport.Location = new System.Drawing.Point(0, 128);
            this.tcReport.Name = "tcReport";
            this.tcReport.SelectedTabPage = this.tpOptions;
            this.tcReport.Size = new System.Drawing.Size(885, 352);
            this.tcReport.TabIndex = 2;
            this.tcReport.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpOptions});
            // 
            // tpOptions
            // 
            this.tpOptions.Controls.Add(this.pnMain);
            this.tpOptions.Name = "tpOptions";
            this.tpOptions.Size = new System.Drawing.Size(881, 326);
            this.tpOptions.Tag = "1";
            this.tpOptions.Text = "�����";
            // 
            // pnMain
            // 
            this.pnMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnMain.Controls.Add(this.pnClient);
            this.pnMain.Controls.Add(this.splitterControl1);
            this.pnMain.Controls.Add(this.pnLeft);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMain.Location = new System.Drawing.Point(0, 0);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(881, 326);
            this.pnMain.TabIndex = 0;
            // 
            // pnClient
            // 
            this.pnClient.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnClient.Controls.Add(this.tcReportOptions);
            this.pnClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnClient.Location = new System.Drawing.Point(164, 0);
            this.pnClient.Name = "pnClient";
            this.pnClient.Size = new System.Drawing.Size(717, 326);
            this.pnClient.TabIndex = 1;
            // 
            // tcReportOptions
            // 
            this.tcReportOptions.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tcReportOptions.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tcReportOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReportOptions.Location = new System.Drawing.Point(0, 0);
            this.tcReportOptions.Name = "tcReportOptions";
            this.tcReportOptions.SelectedTabPage = this.tabOptionsReportLists;
            this.tcReportOptions.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tcReportOptions.Size = new System.Drawing.Size(717, 326);
            this.tcReportOptions.TabIndex = 0;
            this.tcReportOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabOptionsReportLists,
            this.tabOptionsCompetPair,
            this.tabOptionsSKU,
            this.tabOptionsRelativePTCLimits,
            this.tabOptionsPTCLimits,
            this.tabOptionsPTCShiftLimits,
            this.tabOptionsGeneralAndEdvancedSKU});
            // 
            // tabOptionsReportLists
            // 
            this.tabOptionsReportLists.Controls.Add(this.dlcReportList);
            this.tabOptionsReportLists.Name = "tabOptionsReportLists";
            this.tabOptionsReportLists.Size = new System.Drawing.Size(717, 304);
            this.tabOptionsReportLists.Tag = "0";
            this.tabOptionsReportLists.Text = "������ �������";
            // 
            // dlcReportList
            // 
            this.dlcReportList.Controls.Add(this.chkedSelectAll);
            this.dlcReportList.Controls.Add(this.gcReportList);
            this.dlcReportList.DataSource = this.bsReportList;
            this.dlcReportList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcReportList.Location = new System.Drawing.Point(0, 0);
            this.dlcReportList.Name = "dlcReportList";
            this.dlcReportList.Root = this.lcgReportList;
            this.dlcReportList.Size = new System.Drawing.Size(717, 304);
            this.dlcReportList.TabIndex = 0;
            this.dlcReportList.Text = "dblcReportList";
            // 
            // chkedSelectAll
            // 
            this.chkedSelectAll.Location = new System.Drawing.Point(7, 7);
            this.chkedSelectAll.MenuManager = this.bmReport;
            this.chkedSelectAll.Name = "chkedSelectAll";
            this.chkedSelectAll.Properties.Caption = "������� &���";
            this.chkedSelectAll.Size = new System.Drawing.Size(703, 18);
            this.chkedSelectAll.StyleController = this.dlcReportList;
            this.chkedSelectAll.TabIndex = 6;
            this.chkedSelectAll.Click += new System.EventHandler(this.chkedSelectAll_Click);
            // 
            // bmReport
            // 
            this.bmReport.AllowCustomization = false;
            this.bmReport.AllowMoveBarOnToolbar = false;
            this.bmReport.AllowQuickCustomization = false;
            this.bmReport.AllowShowToolbarsPopup = false;
            this.bmReport.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMenu});
            this.bmReport.Controller = this.bdcReport;
            this.bmReport.DockControls.Add(this.barDockControlTop);
            this.bmReport.DockControls.Add(this.barDockControlBottom);
            this.bmReport.DockControls.Add(this.barDockControlLeft);
            this.bmReport.DockControls.Add(this.barDockControlRight);
            this.bmReport.DockControls.Add(this.bdocReport);
            this.bmReport.Form = this;
            this.bmReport.Images = this.imgNormal;
            this.bmReport.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.tbtnReportToExcel,
            this.tbtnReportRefresh});
            this.bmReport.LargeImages = this.imgLarge;
            this.bmReport.MainMenu = this.barMenu;
            this.bmReport.MaxItemId = 3;
            this.bmReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            // 
            // barMenu
            // 
            this.barMenu.BarName = "menuReport";
            this.barMenu.DockCol = 0;
            this.barMenu.DockRow = 0;
            this.barMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barMenu.FloatLocation = new System.Drawing.Point(217, 125);
            this.barMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this.tbtnReportToExcel, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this.tbtnReportRefresh, true)});
            this.barMenu.OptionsBar.AllowQuickCustomization = false;
            this.barMenu.StandaloneBarDockControl = this.bdocReport;
            this.barMenu.Text = "menuReport";
            // 
            // tbtnReportToExcel
            // 
            this.tbtnReportToExcel.Caption = "����� � Excel";
            this.tbtnReportToExcel.Hint = "����� � Excel";
            this.tbtnReportToExcel.Id = 0;
            this.tbtnReportToExcel.LargeImageIndex = 1;
            this.tbtnReportToExcel.Name = "tbtnReportToExcel";
            this.tbtnReportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportToExcel_ItemClick);
            // 
            // tbtnReportRefresh
            // 
            this.tbtnReportRefresh.Caption = "�������� �����";
            this.tbtnReportRefresh.Hint = "�������� �����";
            this.tbtnReportRefresh.Id = 2;
            this.tbtnReportRefresh.LargeImageIndex = 0;
            this.tbtnReportRefresh.Name = "tbtnReportRefresh";
            this.tbtnReportRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportRefresh_ItemClick);
            // 
            // bdocReport
            // 
            this.bdocReport.AutoSize = true;
            this.bdocReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bdocReport.Location = new System.Drawing.Point(3, 3);
            this.bdocReport.Name = "bdocReport";
            this.bdocReport.Size = new System.Drawing.Size(879, 34);
            this.bdocReport.Text = "standaloneBarDockControl1";
            // 
            // bdcReport
            // 
            this.bdcReport.PaintStyleName = "WindowsXP";
            this.bdcReport.PropertiesBar.AllowLinkLighting = false;
            this.bdcReport.PropertiesBar.LargeIcons = true;
            this.bdcReport.PropertiesBar.ScaleIcons = false;
            // 
            // imgNormal
            // 
            this.imgNormal.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgNormal.ImageStream")));
            this.imgNormal.Images.SetKeyName(0, "arrow_next_16.png");
            this.imgNormal.Images.SetKeyName(1, "arrow_back_16.png");
            // 
            // imgLarge
            // 
            this.imgLarge.ImageSize = new System.Drawing.Size(24, 24);
            this.imgLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgLarge.ImageStream")));
            this.imgLarge.Images.SetKeyName(0, "briefcase_ok_24.png");
            this.imgLarge.Images.SetKeyName(1, "Excel_32.bmp");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gcReportList
            // 
            this.gcReportList.DataSource = this.bsReportList;
            this.gcReportList.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcReportList.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcReportList.EmbeddedNavigator.Buttons.CancelEdit.Hint = "������ ���������";
            this.gcReportList.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcReportList.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcReportList.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcReportList.EmbeddedNavigator.Buttons.EndEdit.Hint = "��������� ���������";
            this.gcReportList.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcReportList.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcReportList.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcReportList.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcReportList.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcReportList.EmbeddedNavigator.Buttons.Prev.Hint = "����������";
            this.gcReportList.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcReportList.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcReportList.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcReportList.Location = new System.Drawing.Point(7, 35);
            this.gcReportList.MainView = this.gvReportList;
            this.gcReportList.MenuManager = this.bmReport;
            this.gcReportList.Name = "gcReportList";
            this.gcReportList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gcReportList.Size = new System.Drawing.Size(703, 262);
            this.gcReportList.TabIndex = 5;
            this.gcReportList.UseEmbeddedNavigator = true;
            this.gcReportList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReportList});
            // 
            // bsReportList
            // 
            this.bsReportList.DataMember = "tblReportList";
            this.bsReportList.DataSource = this.reportDataSet;
            // 
            // reportDataSet
            // 
            this.reportDataSet.DataSetName = "ReportDataSet";
            this.reportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gvReportList
            // 
            this.gvReportList.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gvReportList.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvReportList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReport_Checked,
            this.colReport_Name});
            this.gvReportList.GridControl = this.gcReportList;
            this.gvReportList.Name = "gvReportList";
            this.gvReportList.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvReportList.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvReportList.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvReportList.OptionsBehavior.CacheValuesOnRowUpdating = DevExpress.Data.CacheRowValuesMode.Disabled;
            this.gvReportList.OptionsCustomization.AllowColumnMoving = false;
            this.gvReportList.OptionsCustomization.AllowFilter = false;
            this.gvReportList.OptionsCustomization.AllowGroup = false;
            this.gvReportList.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvReportList.OptionsView.ShowGroupPanel = false;
            this.gvReportList.OptionsView.ShowIndicator = false;
            this.gvReportList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvReportList_KeyPress);
            this.gvReportList.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvReportList_RowCellClick);
            // 
            // colReport_Checked
            // 
            this.colReport_Checked.AppearanceHeader.Options.UseTextOptions = true;
            this.colReport_Checked.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReport_Checked.Caption = "�����";
            this.colReport_Checked.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReport_Checked.FieldName = "Report_Checked";
            this.colReport_Checked.Name = "colReport_Checked";
            this.colReport_Checked.OptionsColumn.AllowEdit = false;
            this.colReport_Checked.OptionsColumn.FixedWidth = true;
            this.colReport_Checked.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.colReport_Checked.Visible = true;
            this.colReport_Checked.VisibleIndex = 0;
            this.colReport_Checked.Width = 73;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colReport_Name
            // 
            this.colReport_Name.Caption = "������������ ������";
            this.colReport_Name.FieldName = "Report_Name";
            this.colReport_Name.Name = "colReport_Name";
            this.colReport_Name.OptionsColumn.AllowEdit = false;
            this.colReport_Name.Visible = true;
            this.colReport_Name.VisibleIndex = 1;
            this.colReport_Name.Width = 500;
            // 
            // lcgReportList
            // 
            this.lcgReportList.CustomizationFormText = "layoutControlGroup3";
            this.lcgReportList.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciGridReportList,
            this.layoutControlItem28});
            this.lcgReportList.Location = new System.Drawing.Point(0, 0);
            this.lcgReportList.Name = "lcgReportList";
            this.lcgReportList.OptionsItemText.TextToControlDistance = 5;
            this.lcgReportList.Size = new System.Drawing.Size(717, 304);
            this.lcgReportList.Text = "lcgReportList";
            this.lcgReportList.TextVisible = false;
            // 
            // lciGridReportList
            // 
            this.lciGridReportList.Control = this.gcReportList;
            this.lciGridReportList.CustomizationFormText = "layoutControlItem29";
            this.lciGridReportList.Location = new System.Drawing.Point(0, 28);
            this.lciGridReportList.Name = "lciGridReportList";
            this.lciGridReportList.Size = new System.Drawing.Size(713, 272);
            this.lciGridReportList.Text = "lciGridReportList";
            this.lciGridReportList.TextSize = new System.Drawing.Size(0, 0);
            this.lciGridReportList.TextToControlDistance = 0;
            this.lciGridReportList.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.chkedSelectAll;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(713, 28);
            this.layoutControlItem28.Text = "layoutControlItem28";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextToControlDistance = 0;
            this.layoutControlItem28.TextVisible = false;
            // 
            // tabOptionsCompetPair
            // 
            this.tabOptionsCompetPair.Controls.Add(this.dlcCompetPara);
            this.tabOptionsCompetPair.Name = "tabOptionsCompetPair";
            this.tabOptionsCompetPair.Size = new System.Drawing.Size(717, 304);
            this.tabOptionsCompetPair.Tag = "1";
            this.tabOptionsCompetPair.Text = "������������ ����";
            // 
            // dlcCompetPara
            // 
            this.dlcCompetPara.AllowCustomizationMenu = false;
            this.dlcCompetPara.Controls.Add(this.panelControl5);
            this.dlcCompetPara.Controls.Add(this.panelControl4);
            this.dlcCompetPara.Controls.Add(this.pnEditCompetPair);
            this.dlcCompetPara.Controls.Add(this.panelControl2);
            this.dlcCompetPara.Controls.Add(this.pnCompetPairCaption);
            this.dlcCompetPara.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcCompetPara.Location = new System.Drawing.Point(0, 0);
            this.dlcCompetPara.Name = "dlcCompetPara";
            this.dlcCompetPara.Root = this.lcgCompetPara;
            this.dlcCompetPara.Size = new System.Drawing.Size(717, 304);
            this.dlcCompetPara.TabIndex = 0;
            this.dlcCompetPara.Text = "������������ ����";
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.btnCancelCompetPair);
            this.panelControl5.Controls.Add(this.btnSaveCompetPair);
            this.panelControl5.Location = new System.Drawing.Point(470, 264);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(245, 38);
            this.panelControl5.TabIndex = 8;
            // 
            // btnCancelCompetPair
            // 
            this.btnCancelCompetPair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelCompetPair.Location = new System.Drawing.Point(166, 8);
            this.btnCancelCompetPair.Name = "btnCancelCompetPair";
            this.btnCancelCompetPair.Size = new System.Drawing.Size(75, 23);
            this.btnCancelCompetPair.TabIndex = 1;
            this.btnCancelCompetPair.Text = "������";
            this.btnCancelCompetPair.Click += new System.EventHandler(this.btnCancelCompetPair_Click);
            // 
            // btnSaveCompetPair
            // 
            this.btnSaveCompetPair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveCompetPair.Location = new System.Drawing.Point(85, 8);
            this.btnSaveCompetPair.Name = "btnSaveCompetPair";
            this.btnSaveCompetPair.Size = new System.Drawing.Size(75, 23);
            this.btnSaveCompetPair.TabIndex = 0;
            this.btnSaveCompetPair.Text = "&���������";
            this.btnSaveCompetPair.Click += new System.EventHandler(this.btnSaveCompetPair_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.btnDefaultCompetPair);
            this.panelControl4.Controls.Add(this.btnDelAllCompetPair);
            this.panelControl4.Controls.Add(this.btnDelCompetPair);
            this.panelControl4.Controls.Add(this.btnEditCompetPair);
            this.panelControl4.Controls.Add(this.btnAddCompetPair);
            this.panelControl4.Location = new System.Drawing.Point(2, 264);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(468, 38);
            this.panelControl4.TabIndex = 7;
            // 
            // btnDefaultCompetPair
            // 
            this.btnDefaultCompetPair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDefaultCompetPair.Location = new System.Drawing.Point(381, 8);
            this.btnDefaultCompetPair.Name = "btnDefaultCompetPair";
            this.btnDefaultCompetPair.Size = new System.Drawing.Size(85, 23);
            this.btnDefaultCompetPair.TabIndex = 4;
            this.btnDefaultCompetPair.Text = "&�� ���������";
            this.btnDefaultCompetPair.Click += new System.EventHandler(this.btnDefaultCompetPair_Click);
            // 
            // btnDelAllCompetPair
            // 
            this.btnDelAllCompetPair.Location = new System.Drawing.Point(228, 8);
            this.btnDelAllCompetPair.Name = "btnDelAllCompetPair";
            this.btnDelAllCompetPair.Size = new System.Drawing.Size(75, 23);
            this.btnDelAllCompetPair.TabIndex = 3;
            this.btnDelAllCompetPair.Text = "������� &���";
            this.btnDelAllCompetPair.Click += new System.EventHandler(this.btnDelAllCompetPair_Click);
            // 
            // btnDelCompetPair
            // 
            this.btnDelCompetPair.Location = new System.Drawing.Point(153, 8);
            this.btnDelCompetPair.Name = "btnDelCompetPair";
            this.btnDelCompetPair.Size = new System.Drawing.Size(75, 23);
            this.btnDelCompetPair.TabIndex = 2;
            this.btnDelCompetPair.Text = "&�������";
            this.btnDelCompetPair.Click += new System.EventHandler(this.btnDelCompetPair_Click);
            // 
            // btnEditCompetPair
            // 
            this.btnEditCompetPair.Location = new System.Drawing.Point(78, 8);
            this.btnEditCompetPair.Name = "btnEditCompetPair";
            this.btnEditCompetPair.Size = new System.Drawing.Size(75, 23);
            this.btnEditCompetPair.TabIndex = 1;
            this.btnEditCompetPair.Text = "&��������";
            this.btnEditCompetPair.Click += new System.EventHandler(this.btnEditCompetPair_Click);
            // 
            // btnAddCompetPair
            // 
            this.btnAddCompetPair.Location = new System.Drawing.Point(3, 8);
            this.btnAddCompetPair.Name = "btnAddCompetPair";
            this.btnAddCompetPair.Size = new System.Drawing.Size(75, 23);
            this.btnAddCompetPair.TabIndex = 0;
            this.btnAddCompetPair.Text = "&��������";
            this.btnAddCompetPair.Click += new System.EventHandler(this.btnAddCompetPair_Click);
            // 
            // pnEditCompetPair
            // 
            this.pnEditCompetPair.Controls.Add(this.luSKUName2);
            this.pnEditCompetPair.Controls.Add(this.luSKUName1);
            this.pnEditCompetPair.Controls.Add(this.label3);
            this.pnEditCompetPair.Controls.Add(this.label2);
            this.pnEditCompetPair.Location = new System.Drawing.Point(470, 32);
            this.pnEditCompetPair.Name = "pnEditCompetPair";
            this.pnEditCompetPair.Size = new System.Drawing.Size(245, 232);
            this.pnEditCompetPair.TabIndex = 6;
            // 
            // luSKUName2
            // 
            this.luSKUName2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.luSKUName2.Location = new System.Drawing.Point(16, 76);
            this.luSKUName2.MenuManager = this.bmReport;
            this.luSKUName2.Name = "luSKUName2";
            this.luSKUName2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luSKUName2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SKU_ID", 10, "#"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SKU", "��� 2")});
            this.luSKUName2.Properties.DataSource = this.bsSKU2;
            this.luSKUName2.Properties.DisplayMember = "SKU";
            this.luSKUName2.Properties.DropDownRows = 20;
            this.luSKUName2.Properties.NullText = "�� �����";
            this.luSKUName2.Properties.ValueMember = "SKU_ID";
            this.luSKUName2.Size = new System.Drawing.Size(213, 22);
            this.luSKUName2.TabIndex = 4;
            // 
            // bsSKU2
            // 
            this.bsSKU2.DataMember = "tblDW_TomasResearch_ListSKU";
            this.bsSKU2.DataSource = this.reportDataSet;
            // 
            // luSKUName1
            // 
            this.luSKUName1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.luSKUName1.Location = new System.Drawing.Point(16, 31);
            this.luSKUName1.MenuManager = this.bmReport;
            this.luSKUName1.Name = "luSKUName1";
            this.luSKUName1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luSKUName1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SKU_ID", 10, "#"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SKU", "��� 1")});
            this.luSKUName1.Properties.DataSource = this.bsSKU1;
            this.luSKUName1.Properties.DisplayMember = "SKU";
            this.luSKUName1.Properties.DropDownRows = 20;
            this.luSKUName1.Properties.NullText = "�� �����";
            this.luSKUName1.Properties.ValueMember = "SKU_ID";
            this.luSKUName1.Size = new System.Drawing.Size(213, 22);
            this.luSKUName1.TabIndex = 3;
            // 
            // bsSKU1
            // 
            this.bsSKU1.DataMember = "tblDW_TomasResearch_ListSKU";
            this.bsSKU1.DataSource = this.reportDataSet;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "���&2:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "���&1:";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gcCompetPair);
            this.panelControl2.Location = new System.Drawing.Point(2, 32);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(468, 232);
            this.panelControl2.TabIndex = 5;
            // 
            // gcCompetPair
            // 
            this.gcCompetPair.DataSource = this.bsCompetPair;
            this.gcCompetPair.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcCompetPair.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcCompetPair.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcCompetPair.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcCompetPair.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcCompetPair.EmbeddedNavigator.Buttons.Prev.Hint = "�� ����������";
            this.gcCompetPair.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcCompetPair.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcCompetPair.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcCompetPair.Location = new System.Drawing.Point(3, 3);
            this.gcCompetPair.MainView = this.gvCompetPair;
            this.gcCompetPair.MenuManager = this.bmReport;
            this.gcCompetPair.Name = "gcCompetPair";
            this.gcCompetPair.Size = new System.Drawing.Size(462, 226);
            this.gcCompetPair.TabIndex = 0;
            this.gcCompetPair.UseEmbeddedNavigator = true;
            this.gcCompetPair.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCompetPair});
            // 
            // bsCompetPair
            // 
            this.bsCompetPair.DataMember = "tblDW_TomasResearch_WbW_ListCompetPair";
            this.bsCompetPair.DataSource = this.reportDataSet;
            // 
            // gvCompetPair
            // 
            this.gvCompetPair.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolSKUName1,
            this.gcolSKUName2});
            this.gvCompetPair.GridControl = this.gcCompetPair;
            this.gvCompetPair.Name = "gvCompetPair";
            this.gvCompetPair.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvCompetPair.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvCompetPair.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvCompetPair.OptionsBehavior.Editable = false;
            this.gvCompetPair.OptionsCustomization.AllowColumnMoving = false;
            this.gvCompetPair.OptionsCustomization.AllowGroup = false;
            this.gvCompetPair.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvCompetPair.OptionsView.ColumnAutoWidth = false;
            this.gvCompetPair.OptionsView.ShowGroupPanel = false;
            this.gvCompetPair.OptionsView.ShowIndicator = false;
            // 
            // gcolSKUName1
            // 
            this.gcolSKUName1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gcolSKUName1.AppearanceHeader.Options.UseFont = true;
            this.gcolSKUName1.Caption = "��� 1";
            this.gcolSKUName1.FieldName = "SKU_NAME1";
            this.gcolSKUName1.Name = "gcolSKUName1";
            this.gcolSKUName1.Visible = true;
            this.gcolSKUName1.VisibleIndex = 0;
            this.gcolSKUName1.Width = 150;
            // 
            // gcolSKUName2
            // 
            this.gcolSKUName2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gcolSKUName2.AppearanceHeader.Options.UseFont = true;
            this.gcolSKUName2.Caption = "��� 2";
            this.gcolSKUName2.FieldName = "SKU_NAME2";
            this.gcolSKUName2.Name = "gcolSKUName2";
            this.gcolSKUName2.Visible = true;
            this.gcolSKUName2.VisibleIndex = 1;
            this.gcolSKUName2.Width = 150;
            // 
            // pnCompetPairCaption
            // 
            this.pnCompetPairCaption.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCompetPairCaption.Controls.Add(this.lbCompetPairCaption);
            this.pnCompetPairCaption.Location = new System.Drawing.Point(7, 7);
            this.pnCompetPairCaption.Name = "pnCompetPairCaption";
            this.pnCompetPairCaption.Size = new System.Drawing.Size(703, 20);
            this.pnCompetPairCaption.TabIndex = 4;
            // 
            // lbCompetPairCaption
            // 
            this.lbCompetPairCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbCompetPairCaption.AutoSize = true;
            this.lbCompetPairCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCompetPairCaption.Location = new System.Drawing.Point(287, 4);
            this.lbCompetPairCaption.Name = "lbCompetPairCaption";
            this.lbCompetPairCaption.Size = new System.Drawing.Size(128, 13);
            this.lbCompetPairCaption.TabIndex = 0;
            this.lbCompetPairCaption.Text = "������������ ����";
            // 
            // lcgCompetPara
            // 
            this.lcgCompetPara.CustomizationFormText = "lcgCompetPara";
            this.lcgCompetPara.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.lcgCompetPara.Location = new System.Drawing.Point(0, 0);
            this.lcgCompetPara.Name = "lcgCompetPara";
            this.lcgCompetPara.OptionsItemText.TextToControlDistance = 5;
            this.lcgCompetPara.Size = new System.Drawing.Size(717, 304);
            this.lcgCompetPara.Text = "������������ ����";
            this.lcgCompetPara.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pnCompetPairCaption;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(110, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(713, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panelControl2;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(468, 232);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.pnEditCompetPair;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(468, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem3.Size = new System.Drawing.Size(245, 232);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.panelControl4;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 262);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 38);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(110, 38);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem4.Size = new System.Drawing.Size(468, 38);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.panelControl5;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(468, 262);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 38);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(110, 38);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem5.Size = new System.Drawing.Size(245, 38);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // tabOptionsSKU
            // 
            this.tabOptionsSKU.Controls.Add(this.dlcSKU);
            this.tabOptionsSKU.Name = "tabOptionsSKU";
            this.tabOptionsSKU.Size = new System.Drawing.Size(717, 304);
            this.tabOptionsSKU.Tag = "5";
            this.tabOptionsSKU.Text = "���";
            // 
            // dlcSKU
            // 
            this.dlcSKU.Controls.Add(this.btnDefaultSKU);
            this.dlcSKU.Controls.Add(this.gcSKU);
            this.dlcSKU.Controls.Add(this.lbHeaderSKU);
            this.dlcSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcSKU.Location = new System.Drawing.Point(0, 0);
            this.dlcSKU.Name = "dlcSKU";
            this.dlcSKU.Root = this.lcgSKU;
            this.dlcSKU.Size = new System.Drawing.Size(717, 304);
            this.dlcSKU.TabIndex = 0;
            this.dlcSKU.Text = "���";
            // 
            // btnDefaultSKU
            // 
            this.btnDefaultSKU.Location = new System.Drawing.Point(625, 274);
            this.btnDefaultSKU.Name = "btnDefaultSKU";
            this.btnDefaultSKU.Size = new System.Drawing.Size(85, 23);
            this.btnDefaultSKU.StyleController = this.dlcSKU;
            this.btnDefaultSKU.TabIndex = 6;
            this.btnDefaultSKU.Text = "&�� ���������";
            this.btnDefaultSKU.Click += new System.EventHandler(this.btnDefaultSKU_Click);
            // 
            // gcSKU
            // 
            this.gcSKU.DataSource = this.bsSKUList;
            this.gcSKU.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcSKU.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSKU.EmbeddedNavigator.Buttons.CancelEdit.Hint = "������ ���������";
            this.gcSKU.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcSKU.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSKU.EmbeddedNavigator.Buttons.EndEdit.Hint = "��������� ���������";
            this.gcSKU.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcSKU.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcSKU.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcSKU.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcSKU.EmbeddedNavigator.Buttons.Prev.Hint = "����������";
            this.gcSKU.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcSKU.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcSKU.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSKU.Location = new System.Drawing.Point(7, 30);
            this.gcSKU.MainView = this.gvSKU;
            this.gcSKU.MenuManager = this.bmReport;
            this.gcSKU.Name = "gcSKU";
            this.gcSKU.Size = new System.Drawing.Size(703, 234);
            this.gcSKU.TabIndex = 5;
            this.gcSKU.UseEmbeddedNavigator = true;
            this.gcSKU.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSKU});
            // 
            // bsSKUList
            // 
            this.bsSKUList.DataMember = "tblDW_TomasResearch_WbW_SKU";
            this.bsSKUList.DataSource = this.reportDataSet;
            // 
            // gvSKU
            // 
            this.gvSKU.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolSKUListIsChecked,
            this.gcolSKUListSKU});
            this.gvSKU.GridControl = this.gcSKU;
            this.gvSKU.Name = "gvSKU";
            this.gvSKU.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvSKU.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvSKU.OptionsView.ShowGroupPanel = false;
            this.gvSKU.OptionsView.ShowIndicator = false;
            this.gvSKU.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvSKU_RowUpdated);
            // 
            // gcolSKUListIsChecked
            // 
            this.gcolSKUListIsChecked.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcolSKUListIsChecked.AppearanceHeader.Options.UseFont = true;
            this.gcolSKUListIsChecked.AppearanceHeader.Options.UseTextOptions = true;
            this.gcolSKUListIsChecked.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcolSKUListIsChecked.Caption = "�����";
            this.gcolSKUListIsChecked.FieldName = "isChecked";
            this.gcolSKUListIsChecked.Name = "gcolSKUListIsChecked";
            this.gcolSKUListIsChecked.OptionsColumn.FixedWidth = true;
            this.gcolSKUListIsChecked.Visible = true;
            this.gcolSKUListIsChecked.VisibleIndex = 0;
            this.gcolSKUListIsChecked.Width = 80;
            // 
            // gcolSKUListSKU
            // 
            this.gcolSKUListSKU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcolSKUListSKU.AppearanceHeader.Options.UseFont = true;
            this.gcolSKUListSKU.Caption = "���";
            this.gcolSKUListSKU.FieldName = "SKU";
            this.gcolSKUListSKU.Name = "gcolSKUListSKU";
            this.gcolSKUListSKU.OptionsColumn.AllowEdit = false;
            this.gcolSKUListSKU.Visible = true;
            this.gcolSKUListSKU.VisibleIndex = 1;
            this.gcolSKUListSKU.Width = 528;
            // 
            // lbHeaderSKU
            // 
            this.lbHeaderSKU.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbHeaderSKU.Appearance.Options.UseFont = true;
            this.lbHeaderSKU.Location = new System.Drawing.Point(345, 7);
            this.lbHeaderSKU.Name = "lbHeaderSKU";
            this.lbHeaderSKU.Size = new System.Drawing.Size(26, 13);
            this.lbHeaderSKU.StyleController = this.dlcSKU;
            this.lbHeaderSKU.TabIndex = 4;
            this.lbHeaderSKU.Text = "���";
            // 
            // lcgSKU
            // 
            this.lcgSKU.CustomizationFormText = "layoutControlGroup6";
            this.lcgSKU.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciSKU,
            this.lciGridSKU,
            this.espSKU,
            this.lciBtnSKU});
            this.lcgSKU.Location = new System.Drawing.Point(0, 0);
            this.lcgSKU.Name = "lcgSKU";
            this.lcgSKU.OptionsItemText.TextToControlDistance = 5;
            this.lcgSKU.Size = new System.Drawing.Size(717, 304);
            this.lcgSKU.Text = "lcgSKU";
            this.lcgSKU.TextVisible = false;
            // 
            // lciSKU
            // 
            this.lciSKU.Control = this.lbHeaderSKU;
            this.lciSKU.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lciSKU.CustomizationFormText = "lciSKU";
            this.lciSKU.Location = new System.Drawing.Point(0, 0);
            this.lciSKU.Name = "lciSKU";
            this.lciSKU.Size = new System.Drawing.Size(713, 23);
            this.lciSKU.Text = "lciSKU";
            this.lciSKU.TextSize = new System.Drawing.Size(0, 0);
            this.lciSKU.TextToControlDistance = 0;
            this.lciSKU.TextVisible = false;
            // 
            // lciGridSKU
            // 
            this.lciGridSKU.Control = this.gcSKU;
            this.lciGridSKU.CustomizationFormText = "lciGridSKU";
            this.lciGridSKU.Location = new System.Drawing.Point(0, 23);
            this.lciGridSKU.Name = "lciGridSKU";
            this.lciGridSKU.Size = new System.Drawing.Size(713, 244);
            this.lciGridSKU.Text = "lciGridSKU";
            this.lciGridSKU.TextSize = new System.Drawing.Size(0, 0);
            this.lciGridSKU.TextToControlDistance = 0;
            this.lciGridSKU.TextVisible = false;
            // 
            // espSKU
            // 
            this.espSKU.CustomizationFormText = "espSKU";
            this.espSKU.Location = new System.Drawing.Point(0, 267);
            this.espSKU.Name = "espSKU";
            this.espSKU.Size = new System.Drawing.Size(618, 33);
            this.espSKU.Text = "espSKU";
            this.espSKU.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciBtnSKU
            // 
            this.lciBtnSKU.Control = this.btnDefaultSKU;
            this.lciBtnSKU.CustomizationFormText = "lciBtnSKU";
            this.lciBtnSKU.Location = new System.Drawing.Point(618, 267);
            this.lciBtnSKU.MaxSize = new System.Drawing.Size(95, 33);
            this.lciBtnSKU.MinSize = new System.Drawing.Size(95, 33);
            this.lciBtnSKU.Name = "lciBtnSKU";
            this.lciBtnSKU.Size = new System.Drawing.Size(95, 33);
            this.lciBtnSKU.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciBtnSKU.Text = "lciBtnSKU";
            this.lciBtnSKU.TextSize = new System.Drawing.Size(0, 0);
            this.lciBtnSKU.TextToControlDistance = 0;
            this.lciBtnSKU.TextVisible = false;
            // 
            // tabOptionsRelativePTCLimits
            // 
            this.tabOptionsRelativePTCLimits.Controls.Add(this.dlcRelativePTCLimit);
            this.tabOptionsRelativePTCLimits.Name = "tabOptionsRelativePTCLimits";
            this.tabOptionsRelativePTCLimits.Size = new System.Drawing.Size(717, 304);
            this.tabOptionsRelativePTCLimits.Tag = "2";
            this.tabOptionsRelativePTCLimits.Text = "Relative ��� ������";
            // 
            // dlcRelativePTCLimit
            // 
            this.dlcRelativePTCLimit.Controls.Add(this.btnDefaultRelativePTCLimits);
            this.dlcRelativePTCLimit.Controls.Add(this.gcRelativePTCLimits);
            this.dlcRelativePTCLimit.Controls.Add(this.pnRelativePTCLimits);
            this.dlcRelativePTCLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcRelativePTCLimit.Location = new System.Drawing.Point(0, 0);
            this.dlcRelativePTCLimit.Name = "dlcRelativePTCLimit";
            this.dlcRelativePTCLimit.Root = this.lcgRelativePTCLimit;
            this.dlcRelativePTCLimit.Size = new System.Drawing.Size(717, 304);
            this.dlcRelativePTCLimit.TabIndex = 0;
            this.dlcRelativePTCLimit.Text = "Relative ��� ������";
            // 
            // btnDefaultRelativePTCLimits
            // 
            this.btnDefaultRelativePTCLimits.Location = new System.Drawing.Point(625, 274);
            this.btnDefaultRelativePTCLimits.Name = "btnDefaultRelativePTCLimits";
            this.btnDefaultRelativePTCLimits.Size = new System.Drawing.Size(85, 23);
            this.btnDefaultRelativePTCLimits.StyleController = this.dlcRelativePTCLimit;
            this.btnDefaultRelativePTCLimits.TabIndex = 6;
            this.btnDefaultRelativePTCLimits.Text = "&�� ���������";
            this.btnDefaultRelativePTCLimits.Click += new System.EventHandler(this.btnDefaultRelativePTCLimits_Click);
            // 
            // gcRelativePTCLimits
            // 
            this.gcRelativePTCLimits.DataSource = this.bsRelativeRPTLimits;
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.CancelEdit.Hint = "������ ���������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Edit.Hint = "��������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.EndEdit.Hint = "��������� ���������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Prev.Hint = "����������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcRelativePTCLimits.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcRelativePTCLimits.Location = new System.Drawing.Point(7, 37);
            this.gcRelativePTCLimits.MainView = this.gvRelativePTCLimits;
            this.gcRelativePTCLimits.MenuManager = this.bmReport;
            this.gcRelativePTCLimits.Name = "gcRelativePTCLimits";
            this.gcRelativePTCLimits.Size = new System.Drawing.Size(703, 227);
            this.gcRelativePTCLimits.TabIndex = 5;
            this.gcRelativePTCLimits.UseEmbeddedNavigator = true;
            this.gcRelativePTCLimits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRelativePTCLimits});
            // 
            // bsRelativeRPTLimits
            // 
            this.bsRelativeRPTLimits.DataMember = "tblDW_TomasResearch_WbW_RPTC_Limits";
            this.bsRelativeRPTLimits.DataSource = this.reportDataSet;
            // 
            // gvRelativePTCLimits
            // 
            this.gvRelativePTCLimits.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gvRelativePTCLimits.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvRelativePTCLimits.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolLimitName,
            this.gcolMinRelativePTCLimit,
            this.gcolMaxRelativePTCLimit});
            this.gvRelativePTCLimits.GridControl = this.gcRelativePTCLimits;
            this.gvRelativePTCLimits.Name = "gvRelativePTCLimits";
            this.gvRelativePTCLimits.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvRelativePTCLimits.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvRelativePTCLimits.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvRelativePTCLimits.OptionsView.ColumnAutoWidth = false;
            this.gvRelativePTCLimits.OptionsView.ShowGroupPanel = false;
            this.gvRelativePTCLimits.OptionsView.ShowIndicator = false;
            this.gvRelativePTCLimits.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvRelativePTCLimits_RowUpdated);
            // 
            // gcolLimitName
            // 
            this.gcolLimitName.Caption = "������������";
            this.gcolLimitName.FieldName = "LimitName";
            this.gcolLimitName.Name = "gcolLimitName";
            this.gcolLimitName.Visible = true;
            this.gcolLimitName.VisibleIndex = 0;
            this.gcolLimitName.Width = 110;
            // 
            // gcolMinRelativePTCLimit
            // 
            this.gcolMinRelativePTCLimit.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcolMinRelativePTCLimit.AppearanceHeader.Options.UseFont = true;
            this.gcolMinRelativePTCLimit.Caption = "���. ��������";
            this.gcolMinRelativePTCLimit.FieldName = "Limit1";
            this.gcolMinRelativePTCLimit.Name = "gcolMinRelativePTCLimit";
            this.gcolMinRelativePTCLimit.Visible = true;
            this.gcolMinRelativePTCLimit.VisibleIndex = 1;
            this.gcolMinRelativePTCLimit.Width = 105;
            // 
            // gcolMaxRelativePTCLimit
            // 
            this.gcolMaxRelativePTCLimit.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcolMaxRelativePTCLimit.AppearanceHeader.Options.UseFont = true;
            this.gcolMaxRelativePTCLimit.Caption = "����. ��������";
            this.gcolMaxRelativePTCLimit.FieldName = "Limit2";
            this.gcolMaxRelativePTCLimit.Name = "gcolMaxRelativePTCLimit";
            this.gcolMaxRelativePTCLimit.Visible = true;
            this.gcolMaxRelativePTCLimit.VisibleIndex = 2;
            this.gcolMaxRelativePTCLimit.Width = 110;
            // 
            // pnRelativePTCLimits
            // 
            this.pnRelativePTCLimits.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnRelativePTCLimits.Controls.Add(this.label1);
            this.pnRelativePTCLimits.Location = new System.Drawing.Point(7, 7);
            this.pnRelativePTCLimits.Name = "pnRelativePTCLimits";
            this.pnRelativePTCLimits.Size = new System.Drawing.Size(703, 20);
            this.pnRelativePTCLimits.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(289, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Relative PTC ������";
            // 
            // lcgRelativePTCLimit
            // 
            this.lcgRelativePTCLimit.CustomizationFormText = "lcgRelativePTCLimit";
            this.lcgRelativePTCLimit.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.lcgRelativePTCLimit.Location = new System.Drawing.Point(0, 0);
            this.lcgRelativePTCLimit.Name = "lcgRelativePTCLimit";
            this.lcgRelativePTCLimit.OptionsItemText.TextToControlDistance = 5;
            this.lcgRelativePTCLimit.Size = new System.Drawing.Size(717, 304);
            this.lcgRelativePTCLimit.Text = "lcgRelativePTCLimit";
            this.lcgRelativePTCLimit.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.pnRelativePTCLimits;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(110, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(713, 30);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 267);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 33);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 33);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(618, 33);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gcRelativePTCLimits;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(713, 237);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnDefaultRelativePTCLimits;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(618, 267);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(95, 33);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // tabOptionsPTCLimits
            // 
            this.tabOptionsPTCLimits.Controls.Add(this.dlcPTCLimit);
            this.tabOptionsPTCLimits.Name = "tabOptionsPTCLimits";
            this.tabOptionsPTCLimits.Size = new System.Drawing.Size(717, 304);
            this.tabOptionsPTCLimits.Tag = "3";
            this.tabOptionsPTCLimits.Text = "��� ������";
            // 
            // dlcPTCLimit
            // 
            this.dlcPTCLimit.Controls.Add(this.btnDefaultSKUPTCLimit);
            this.dlcPTCLimit.Controls.Add(this.gcPTCLimit);
            this.dlcPTCLimit.Controls.Add(this.gcSKUPTCLimit);
            this.dlcPTCLimit.Controls.Add(this.pnPTCLimits);
            this.dlcPTCLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcPTCLimit.Location = new System.Drawing.Point(0, 0);
            this.dlcPTCLimit.Name = "dlcPTCLimit";
            this.dlcPTCLimit.Root = this.lcgPTCLimit;
            this.dlcPTCLimit.Size = new System.Drawing.Size(717, 304);
            this.dlcPTCLimit.TabIndex = 0;
            this.dlcPTCLimit.Text = "dataLayoutControl1";
            // 
            // btnDefaultSKUPTCLimit
            // 
            this.btnDefaultSKUPTCLimit.Location = new System.Drawing.Point(625, 274);
            this.btnDefaultSKUPTCLimit.Name = "btnDefaultSKUPTCLimit";
            this.btnDefaultSKUPTCLimit.Size = new System.Drawing.Size(85, 23);
            this.btnDefaultSKUPTCLimit.StyleController = this.dlcPTCLimit;
            this.btnDefaultSKUPTCLimit.TabIndex = 7;
            this.btnDefaultSKUPTCLimit.Text = "&�� ���������";
            this.btnDefaultSKUPTCLimit.Click += new System.EventHandler(this.btnDefaultSKUPTCLimit_Click);
            // 
            // gcPTCLimit
            // 
            this.gcPTCLimit.DataSource = this.bsPTCLimit;
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcPTCLimit.EmbeddedNavigator.Buttons.CancelEdit.Hint = "������ ���������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Edit.Hint = "��������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcPTCLimit.EmbeddedNavigator.Buttons.EndEdit.Hint = "��������� ���������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Prev.Hint = "����������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcPTCLimit.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcPTCLimit.Location = new System.Drawing.Point(327, 37);
            this.gcPTCLimit.MainView = this.gvPTCLimit;
            this.gcPTCLimit.MenuManager = this.bmReport;
            this.gcPTCLimit.Name = "gcPTCLimit";
            this.gcPTCLimit.Size = new System.Drawing.Size(383, 227);
            this.gcPTCLimit.TabIndex = 6;
            this.gcPTCLimit.UseEmbeddedNavigator = true;
            this.gcPTCLimit.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPTCLimit});
            // 
            // bsPTCLimit
            // 
            this.bsPTCLimit.DataMember = "tblDW_TomasResearch_WbW_PTC_Limits";
            this.bsPTCLimit.DataSource = this.reportDataSet;
            // 
            // gvPTCLimit
            // 
            this.gvPTCLimit.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gvPTCLimit.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvPTCLimit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolPTCLimitName,
            this.gcolMinPTCLimit,
            this.gcolMaxPTCLimit});
            this.gvPTCLimit.GridControl = this.gcPTCLimit;
            this.gvPTCLimit.Name = "gvPTCLimit";
            this.gvPTCLimit.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPTCLimit.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPTCLimit.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvPTCLimit.OptionsView.ColumnAutoWidth = false;
            this.gvPTCLimit.OptionsView.ShowGroupPanel = false;
            this.gvPTCLimit.OptionsView.ShowIndicator = false;
            this.gvPTCLimit.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvPTCLimit_RowUpdated);
            // 
            // gcolPTCLimitName
            // 
            this.gcolPTCLimitName.Caption = "������������";
            this.gcolPTCLimitName.FieldName = "LimitName";
            this.gcolPTCLimitName.Name = "gcolPTCLimitName";
            this.gcolPTCLimitName.Visible = true;
            this.gcolPTCLimitName.VisibleIndex = 0;
            this.gcolPTCLimitName.Width = 110;
            // 
            // gcolMinPTCLimit
            // 
            this.gcolMinPTCLimit.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcolMinPTCLimit.AppearanceHeader.Options.UseFont = true;
            this.gcolMinPTCLimit.Caption = "���. ��������";
            this.gcolMinPTCLimit.FieldName = "Limit1";
            this.gcolMinPTCLimit.Name = "gcolMinPTCLimit";
            this.gcolMinPTCLimit.Visible = true;
            this.gcolMinPTCLimit.VisibleIndex = 1;
            this.gcolMinPTCLimit.Width = 105;
            // 
            // gcolMaxPTCLimit
            // 
            this.gcolMaxPTCLimit.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcolMaxPTCLimit.AppearanceHeader.Options.UseFont = true;
            this.gcolMaxPTCLimit.Caption = "����. ��������";
            this.gcolMaxPTCLimit.FieldName = "Limit2";
            this.gcolMaxPTCLimit.Name = "gcolMaxPTCLimit";
            this.gcolMaxPTCLimit.Visible = true;
            this.gcolMaxPTCLimit.VisibleIndex = 2;
            this.gcolMaxPTCLimit.Width = 110;
            // 
            // gcSKUPTCLimit
            // 
            this.gcSKUPTCLimit.DataSource = this.bsSKUPTCLimit;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Prev.Hint = "����������";
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcSKUPTCLimit.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSKUPTCLimit.Location = new System.Drawing.Point(7, 37);
            this.gcSKUPTCLimit.MainView = this.gvSKUPTCLimit;
            this.gcSKUPTCLimit.MenuManager = this.bmReport;
            this.gcSKUPTCLimit.Name = "gcSKUPTCLimit";
            this.gcSKUPTCLimit.Size = new System.Drawing.Size(310, 227);
            this.gcSKUPTCLimit.TabIndex = 5;
            this.gcSKUPTCLimit.UseEmbeddedNavigator = true;
            this.gcSKUPTCLimit.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSKUPTCLimit});
            // 
            // bsSKUPTCLimit
            // 
            this.bsSKUPTCLimit.DataMember = "tblDW_TomasResearch_WbW_SKU";
            this.bsSKUPTCLimit.DataSource = this.reportDataSet;
            this.bsSKUPTCLimit.Filter = "isChecked=1";
            this.bsSKUPTCLimit.PositionChanged += new System.EventHandler(this.bsSKUPTCLimit_PositionChanged);
            // 
            // gvSKUPTCLimit
            // 
            this.gvSKUPTCLimit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolSKUPTCLimit});
            this.gvSKUPTCLimit.GridControl = this.gcSKUPTCLimit;
            this.gvSKUPTCLimit.Name = "gvSKUPTCLimit";
            this.gvSKUPTCLimit.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvSKUPTCLimit.OptionsBehavior.Editable = false;
            this.gvSKUPTCLimit.OptionsSelection.InvertSelection = true;
            this.gvSKUPTCLimit.OptionsView.ShowGroupPanel = false;
            this.gvSKUPTCLimit.OptionsView.ShowIndicator = false;
            // 
            // gcolSKUPTCLimit
            // 
            this.gcolSKUPTCLimit.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcolSKUPTCLimit.AppearanceHeader.Options.UseFont = true;
            this.gcolSKUPTCLimit.Caption = "���";
            this.gcolSKUPTCLimit.FieldName = "SKU";
            this.gcolSKUPTCLimit.Name = "gcolSKUPTCLimit";
            this.gcolSKUPTCLimit.Visible = true;
            this.gcolSKUPTCLimit.VisibleIndex = 0;
            // 
            // pnPTCLimits
            // 
            this.pnPTCLimits.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnPTCLimits.Controls.Add(this.lbPTCLimits);
            this.pnPTCLimits.Location = new System.Drawing.Point(7, 7);
            this.pnPTCLimits.Name = "pnPTCLimits";
            this.pnPTCLimits.Size = new System.Drawing.Size(703, 20);
            this.pnPTCLimits.TabIndex = 4;
            // 
            // lbPTCLimits
            // 
            this.lbPTCLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbPTCLimits.AutoSize = true;
            this.lbPTCLimits.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPTCLimits.Location = new System.Drawing.Point(314, 4);
            this.lbPTCLimits.Name = "lbPTCLimits";
            this.lbPTCLimits.Size = new System.Drawing.Size(75, 13);
            this.lbPTCLimits.TabIndex = 0;
            this.lbPTCLimits.Text = "PTC ������";
            // 
            // lcgPTCLimit
            // 
            this.lcgPTCLimit.CustomizationFormText = "lcgPTCLimit";
            this.lcgPTCLimit.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.emptySpaceItem4,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.lcgPTCLimit.Location = new System.Drawing.Point(0, 0);
            this.lcgPTCLimit.Name = "lcgPTCLimit";
            this.lcgPTCLimit.OptionsItemText.TextToControlDistance = 5;
            this.lcgPTCLimit.Size = new System.Drawing.Size(717, 304);
            this.lcgPTCLimit.Text = "lcgPTCLimit";
            this.lcgPTCLimit.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.pnPTCLimits;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(208, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(713, 30);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.gcSKUPTCLimit;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(320, 237);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 267);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(618, 33);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.gcPTCLimit;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(320, 30);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(393, 237);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnDefaultSKUPTCLimit;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(618, 267);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(95, 33);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // tabOptionsPTCShiftLimits
            // 
            this.tabOptionsPTCShiftLimits.Controls.Add(this.dlcPTCShiftLimitsOptions);
            this.tabOptionsPTCShiftLimits.Name = "tabOptionsPTCShiftLimits";
            this.tabOptionsPTCShiftLimits.Size = new System.Drawing.Size(717, 304);
            this.tabOptionsPTCShiftLimits.Tag = "4";
            this.tabOptionsPTCShiftLimits.Text = "PTCShiftLimits";
            // 
            // dlcPTCShiftLimitsOptions
            // 
            this.dlcPTCShiftLimitsOptions.Controls.Add(this.btnDefaultPTCShiftLimits);
            this.dlcPTCShiftLimitsOptions.Controls.Add(this.gcPTCShiftLimits);
            this.dlcPTCShiftLimitsOptions.Controls.Add(this.panelControl1);
            this.dlcPTCShiftLimitsOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcPTCShiftLimitsOptions.Location = new System.Drawing.Point(0, 0);
            this.dlcPTCShiftLimitsOptions.Name = "dlcPTCShiftLimitsOptions";
            this.dlcPTCShiftLimitsOptions.Root = this.layoutControlGroup1;
            this.dlcPTCShiftLimitsOptions.Size = new System.Drawing.Size(717, 304);
            this.dlcPTCShiftLimitsOptions.TabIndex = 0;
            // 
            // btnDefaultPTCShiftLimits
            // 
            this.btnDefaultPTCShiftLimits.Location = new System.Drawing.Point(625, 274);
            this.btnDefaultPTCShiftLimits.Name = "btnDefaultPTCShiftLimits";
            this.btnDefaultPTCShiftLimits.Size = new System.Drawing.Size(85, 23);
            this.btnDefaultPTCShiftLimits.StyleController = this.dlcPTCShiftLimitsOptions;
            this.btnDefaultPTCShiftLimits.TabIndex = 6;
            this.btnDefaultPTCShiftLimits.Text = "&�� ���������";
            this.btnDefaultPTCShiftLimits.Click += new System.EventHandler(this.btnDefaultPTCShiftLimits_Click);
            // 
            // gcPTCShiftLimits
            // 
            this.gcPTCShiftLimits.DataSource = this.bsPTCShiftLimit;
            this.gcPTCShiftLimits.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcPTCShiftLimits.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcPTCShiftLimits.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcPTCShiftLimits.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcPTCShiftLimits.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcPTCShiftLimits.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcPTCShiftLimits.Location = new System.Drawing.Point(7, 37);
            this.gcPTCShiftLimits.MainView = this.gvPTCShiftLimits;
            this.gcPTCShiftLimits.MenuManager = this.bmReport;
            this.gcPTCShiftLimits.Name = "gcPTCShiftLimits";
            this.gcPTCShiftLimits.Size = new System.Drawing.Size(703, 227);
            this.gcPTCShiftLimits.TabIndex = 5;
            this.gcPTCShiftLimits.UseEmbeddedNavigator = true;
            this.gcPTCShiftLimits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPTCShiftLimits});
            // 
            // bsPTCShiftLimit
            // 
            this.bsPTCShiftLimit.DataMember = "tblDW_TomasResearch_WbW_PTCShift_Limits";
            this.bsPTCShiftLimit.DataSource = this.reportDataSet;
            // 
            // gvPTCShiftLimits
            // 
            this.gvPTCShiftLimits.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gvPTCShiftLimits.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvPTCShiftLimits.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolPTCShiftName,
            this.gridColumn1,
            this.gridColumn2});
            this.gvPTCShiftLimits.GridControl = this.gcPTCShiftLimits;
            this.gvPTCShiftLimits.Name = "gvPTCShiftLimits";
            this.gvPTCShiftLimits.OptionsView.ColumnAutoWidth = false;
            this.gvPTCShiftLimits.OptionsView.ShowGroupPanel = false;
            this.gvPTCShiftLimits.OptionsView.ShowIndicator = false;
            this.gvPTCShiftLimits.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvPTCShiftLimits_RowUpdated);
            // 
            // gcolPTCShiftName
            // 
            this.gcolPTCShiftName.Caption = "������������";
            this.gcolPTCShiftName.FieldName = "LimitName";
            this.gcolPTCShiftName.Name = "gcolPTCShiftName";
            this.gcolPTCShiftName.Visible = true;
            this.gcolPTCShiftName.VisibleIndex = 0;
            this.gcolPTCShiftName.Width = 110;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "���. ��������";
            this.gridColumn1.FieldName = "Limit1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 105;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.Caption = "����. ��������";
            this.gridColumn2.FieldName = "Limit2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 110;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(7, 7);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(703, 20);
            this.panelControl1.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(304, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(94, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "PTCShift ������";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.emptySpaceItem3,
            this.layoutControlItem26,
            this.layoutControlItem27});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(717, 304);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.panelControl1;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(214, 30);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(713, 30);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 267);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(618, 33);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.gcPTCShiftLimits;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(713, 237);
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.btnDefaultPTCShiftLimits;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(618, 267);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(95, 33);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "layoutControlItem27";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextToControlDistance = 0;
            this.layoutControlItem27.TextVisible = false;
            // 
            // tabOptionsGeneralAndEdvancedSKU
            // 
            this.tabOptionsGeneralAndEdvancedSKU.Controls.Add(this.dlcGeneralAndEdvanced);
            this.tabOptionsGeneralAndEdvancedSKU.Name = "tabOptionsGeneralAndEdvancedSKU";
            this.tabOptionsGeneralAndEdvancedSKU.Size = new System.Drawing.Size(717, 304);
            this.tabOptionsGeneralAndEdvancedSKU.Tag = "6";
            this.tabOptionsGeneralAndEdvancedSKU.Text = "�������� � �������������� ���";
            // 
            // dlcGeneralAndEdvanced
            // 
            this.dlcGeneralAndEdvanced.Controls.Add(this.groupControl2);
            this.dlcGeneralAndEdvanced.Controls.Add(this.groupControl1);
            this.dlcGeneralAndEdvanced.Controls.Add(this.gcSKUAll);
            this.dlcGeneralAndEdvanced.Controls.Add(this.btnInsertToAdvancedSKU);
            this.dlcGeneralAndEdvanced.Controls.Add(this.btnRemoveFromAdvancedSKU);
            this.dlcGeneralAndEdvanced.Controls.Add(this.btnRemoveFromBasicSKU);
            this.dlcGeneralAndEdvanced.Controls.Add(this.btnInsertToBasicSKU);
            this.dlcGeneralAndEdvanced.Controls.Add(this.btnDefaultBasicAndAddSKU);
            this.dlcGeneralAndEdvanced.Controls.Add(this.sedSKUInBevCount);
            this.dlcGeneralAndEdvanced.Controls.Add(this.label4);
            this.dlcGeneralAndEdvanced.Controls.Add(this.pnBasicAndAdvancedSKU);
            this.dlcGeneralAndEdvanced.Controls.Add(this.gridControl1);
            this.dlcGeneralAndEdvanced.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlcGeneralAndEdvanced.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14});
            this.dlcGeneralAndEdvanced.Location = new System.Drawing.Point(0, 0);
            this.dlcGeneralAndEdvanced.Name = "dlcGeneralAndEdvanced";
            this.dlcGeneralAndEdvanced.Root = this.lcgGeneralAndEdvanced;
            this.dlcGeneralAndEdvanced.Size = new System.Drawing.Size(717, 304);
            this.dlcGeneralAndEdvanced.TabIndex = 0;
            this.dlcGeneralAndEdvanced.Text = "dataLayoutControl1";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gcDopSKU);
            this.groupControl2.Location = new System.Drawing.Point(368, 149);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(347, 120);
            this.groupControl2.TabIndex = 15;
            this.groupControl2.Text = "&�������������� ���:";
            // 
            // gcDopSKU
            // 
            this.gcDopSKU.DataSource = this.bsDopSKU;
            this.gcDopSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDopSKU.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcDopSKU.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcDopSKU.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcDopSKU.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcDopSKU.EmbeddedNavigator.Buttons.Prev.Hint = "����������";
            this.gcDopSKU.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcDopSKU.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcDopSKU.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcDopSKU.Location = new System.Drawing.Point(3, 19);
            this.gcDopSKU.MainView = this.gvDopSKU;
            this.gcDopSKU.MenuManager = this.bmReport;
            this.gcDopSKU.Name = "gcDopSKU";
            this.gcDopSKU.Size = new System.Drawing.Size(341, 98);
            this.gcDopSKU.TabIndex = 0;
            this.gcDopSKU.UseEmbeddedNavigator = true;
            this.gcDopSKU.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDopSKU});
            // 
            // bsDopSKU
            // 
            this.bsDopSKU.DataMember = "tblDopSKU";
            this.bsDopSKU.DataSource = this.reportDataSet;
            // 
            // gvDopSKU
            // 
            this.gvDopSKU.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5});
            this.gvDopSKU.GridControl = this.gcDopSKU;
            this.gvDopSKU.Name = "gvDopSKU";
            this.gvDopSKU.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvDopSKU.OptionsBehavior.Editable = false;
            this.gvDopSKU.OptionsCustomization.AllowColumnMoving = false;
            this.gvDopSKU.OptionsCustomization.AllowGroup = false;
            this.gvDopSKU.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvDopSKU.OptionsNavigation.AutoFocusNewRow = true;
            this.gvDopSKU.OptionsSelection.InvertSelection = true;
            this.gvDopSKU.OptionsView.ShowGroupPanel = false;
            this.gvDopSKU.OptionsView.ShowIndicator = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.Caption = "���";
            this.gridColumn5.FieldName = "SKU_NAME";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gcOsnSKU);
            this.groupControl1.Location = new System.Drawing.Point(368, 32);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(347, 117);
            this.groupControl1.TabIndex = 14;
            this.groupControl1.Text = "&�������� ���:";
            // 
            // gcOsnSKU
            // 
            this.gcOsnSKU.DataSource = this.bsOsnSKU;
            this.gcOsnSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.First.Hint = "�� ������";
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Last.Hint = "�� ���������";
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Next.Hint = "���������";
            this.gcOsnSKU.EmbeddedNavigator.Buttons.NextPage.Hint = "��������� ��������";
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Prev.Hint = "����������";
            this.gcOsnSKU.EmbeddedNavigator.Buttons.PrevPage.Hint = "���������� ��������";
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcOsnSKU.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcOsnSKU.Location = new System.Drawing.Point(3, 19);
            this.gcOsnSKU.MainView = this.gvOsnSKU;
            this.gcOsnSKU.MenuManager = this.bmReport;
            this.gcOsnSKU.Name = "gcOsnSKU";
            this.gcOsnSKU.Size = new System.Drawing.Size(341, 95);
            this.gcOsnSKU.TabIndex = 0;
            this.gcOsnSKU.UseEmbeddedNavigator = true;
            this.gcOsnSKU.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOsnSKU});
            // 
            // bsOsnSKU
            // 
            this.bsOsnSKU.DataMember = "tblOsnSKU";
            this.bsOsnSKU.DataSource = this.reportDataSet;
            // 
            // gvOsnSKU
            // 
            this.gvOsnSKU.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4});
            this.gvOsnSKU.GridControl = this.gcOsnSKU;
            this.gvOsnSKU.Name = "gvOsnSKU";
            this.gvOsnSKU.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvOsnSKU.OptionsBehavior.Editable = false;
            this.gvOsnSKU.OptionsCustomization.AllowColumnMoving = false;
            this.gvOsnSKU.OptionsCustomization.AllowGroup = false;
            this.gvOsnSKU.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvOsnSKU.OptionsNavigation.AutoFocusNewRow = true;
            this.gvOsnSKU.OptionsSelection.InvertSelection = true;
            this.gvOsnSKU.OptionsView.ShowGroupPanel = false;
            this.gvOsnSKU.OptionsView.ShowIndicator = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.Caption = "���";
            this.gridColumn4.FieldName = "SKU_NAME";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gcSKUAll
            // 
            this.gcSKUAll.DataSource = this.bsSKUAll;
            this.gcSKUAll.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcSKUAll.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSKUAll.Location = new System.Drawing.Point(2, 32);
            this.gcSKUAll.MainView = this.gvSKUAll;
            this.gcSKUAll.MenuManager = this.bmReport;
            this.gcSKUAll.Name = "gcSKUAll";
            this.gcSKUAll.Size = new System.Drawing.Size(274, 237);
            this.gcSKUAll.TabIndex = 13;
            this.gcSKUAll.UseEmbeddedNavigator = true;
            this.gcSKUAll.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSKUAll});
            // 
            // bsSKUAll
            // 
            this.bsSKUAll.DataMember = "tblSKUAll";
            this.bsSKUAll.DataSource = this.reportDataSet;
            // 
            // gvSKUAll
            // 
            this.gvSKUAll.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6});
            this.gvSKUAll.GridControl = this.gcSKUAll;
            this.gvSKUAll.Name = "gvSKUAll";
            this.gvSKUAll.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvSKUAll.OptionsBehavior.Editable = false;
            this.gvSKUAll.OptionsCustomization.AllowColumnMoving = false;
            this.gvSKUAll.OptionsCustomization.AllowGroup = false;
            this.gvSKUAll.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvSKUAll.OptionsNavigation.AutoFocusNewRow = true;
            this.gvSKUAll.OptionsSelection.InvertSelection = true;
            this.gvSKUAll.OptionsView.ShowGroupPanel = false;
            this.gvSKUAll.OptionsView.ShowIndicator = false;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.Caption = "���";
            this.gridColumn6.FieldName = "SKU_NAME";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            // 
            // btnInsertToAdvancedSKU
            // 
            this.btnInsertToAdvancedSKU.ImageIndex = 0;
            this.btnInsertToAdvancedSKU.ImageList = this.imgNormal;
            this.btnInsertToAdvancedSKU.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnInsertToAdvancedSKU.Location = new System.Drawing.Point(276, 225);
            this.btnInsertToAdvancedSKU.Name = "btnInsertToAdvancedSKU";
            this.btnInsertToAdvancedSKU.Size = new System.Drawing.Size(92, 22);
            this.btnInsertToAdvancedSKU.StyleController = this.dlcGeneralAndEdvanced;
            this.btnInsertToAdvancedSKU.TabIndex = 12;
            this.btnInsertToAdvancedSKU.ToolTip = "�������� � ��������������";
            this.btnInsertToAdvancedSKU.Click += new System.EventHandler(this.btnInsertToAdvancedSKU_Click);
            // 
            // btnRemoveFromAdvancedSKU
            // 
            this.btnRemoveFromAdvancedSKU.ImageIndex = 1;
            this.btnRemoveFromAdvancedSKU.ImageList = this.imgNormal;
            this.btnRemoveFromAdvancedSKU.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveFromAdvancedSKU.Location = new System.Drawing.Point(276, 247);
            this.btnRemoveFromAdvancedSKU.Name = "btnRemoveFromAdvancedSKU";
            this.btnRemoveFromAdvancedSKU.Size = new System.Drawing.Size(92, 22);
            this.btnRemoveFromAdvancedSKU.StyleController = this.dlcGeneralAndEdvanced;
            this.btnRemoveFromAdvancedSKU.TabIndex = 11;
            this.btnRemoveFromAdvancedSKU.ToolTip = "������� �� ��������������";
            this.btnRemoveFromAdvancedSKU.Click += new System.EventHandler(this.btnRemoveFromAdvancedSKU_Click);
            // 
            // btnRemoveFromBasicSKU
            // 
            this.btnRemoveFromBasicSKU.ImageIndex = 1;
            this.btnRemoveFromBasicSKU.ImageList = this.imgNormal;
            this.btnRemoveFromBasicSKU.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveFromBasicSKU.Location = new System.Drawing.Point(276, 54);
            this.btnRemoveFromBasicSKU.Name = "btnRemoveFromBasicSKU";
            this.btnRemoveFromBasicSKU.Size = new System.Drawing.Size(92, 22);
            this.btnRemoveFromBasicSKU.StyleController = this.dlcGeneralAndEdvanced;
            this.btnRemoveFromBasicSKU.TabIndex = 10;
            this.btnRemoveFromBasicSKU.ToolTip = "������� �� ��������";
            this.btnRemoveFromBasicSKU.Click += new System.EventHandler(this.btnRemoveFromBasicSKU_Click);
            // 
            // btnInsertToBasicSKU
            // 
            this.btnInsertToBasicSKU.ImageIndex = 0;
            this.btnInsertToBasicSKU.ImageList = this.imgNormal;
            this.btnInsertToBasicSKU.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnInsertToBasicSKU.Location = new System.Drawing.Point(276, 32);
            this.btnInsertToBasicSKU.Name = "btnInsertToBasicSKU";
            this.btnInsertToBasicSKU.Size = new System.Drawing.Size(92, 22);
            this.btnInsertToBasicSKU.StyleController = this.dlcGeneralAndEdvanced;
            this.btnInsertToBasicSKU.TabIndex = 9;
            this.btnInsertToBasicSKU.ToolTip = "�������� � ��������";
            this.btnInsertToBasicSKU.Click += new System.EventHandler(this.btnInsertToBasicSKU_Click);
            // 
            // btnDefaultBasicAndAddSKU
            // 
            this.btnDefaultBasicAndAddSKU.Location = new System.Drawing.Point(625, 274);
            this.btnDefaultBasicAndAddSKU.Name = "btnDefaultBasicAndAddSKU";
            this.btnDefaultBasicAndAddSKU.Size = new System.Drawing.Size(85, 23);
            this.btnDefaultBasicAndAddSKU.StyleController = this.dlcGeneralAndEdvanced;
            this.btnDefaultBasicAndAddSKU.TabIndex = 8;
            this.btnDefaultBasicAndAddSKU.Text = "&�� ���������";
            this.btnDefaultBasicAndAddSKU.Click += new System.EventHandler(this.btnDefaultBasicAndAddSKU_Click);
            // 
            // sedSKUInBevCount
            // 
            this.sedSKUInBevCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sedSKUInBevCount.Location = new System.Drawing.Point(152, 274);
            this.sedSKUInBevCount.MenuManager = this.bmReport;
            this.sedSKUInBevCount.Name = "sedSKUInBevCount";
            this.sedSKUInBevCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.sedSKUInBevCount.Properties.DisplayFormat.FormatString = "n0";
            this.sedSKUInBevCount.Properties.IsFloatValue = false;
            this.sedSKUInBevCount.Properties.Mask.EditMask = "n0";
            this.sedSKUInBevCount.Properties.MaxLength = 3;
            this.sedSKUInBevCount.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.sedSKUInBevCount.Properties.NullText = "0";
            this.sedSKUInBevCount.Size = new System.Drawing.Size(50, 22);
            this.sedSKUInBevCount.StyleController = this.dlcGeneralAndEdvanced;
            this.sedSKUInBevCount.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(7, 274);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 23);
            this.label4.TabIndex = 6;
            this.label4.Text = "&���������� ��� �����:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnBasicAndAdvancedSKU
            // 
            this.pnBasicAndAdvancedSKU.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBasicAndAdvancedSKU.Controls.Add(this.lbBasicAndAdvancedSKU);
            this.pnBasicAndAdvancedSKU.Location = new System.Drawing.Point(7, 7);
            this.pnBasicAndAdvancedSKU.Name = "pnBasicAndAdvancedSKU";
            this.pnBasicAndAdvancedSKU.Size = new System.Drawing.Size(703, 20);
            this.pnBasicAndAdvancedSKU.TabIndex = 4;
            // 
            // lbBasicAndAdvancedSKU
            // 
            this.lbBasicAndAdvancedSKU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbBasicAndAdvancedSKU.AutoSize = true;
            this.lbBasicAndAdvancedSKU.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbBasicAndAdvancedSKU.Location = new System.Drawing.Point(247, 4);
            this.lbBasicAndAdvancedSKU.Name = "lbBasicAndAdvancedSKU";
            this.lbBasicAndAdvancedSKU.Size = new System.Drawing.Size(209, 13);
            this.lbBasicAndAdvancedSKU.TabIndex = 0;
            this.lbBasicAndAdvancedSKU.Text = "�������� � �������������� ���";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(111, 37);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.bmReport;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(187, 232);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.gridControl1;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(301, 242);
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // lcgGeneralAndEdvanced
            // 
            this.lcgGeneralAndEdvanced.CustomizationFormText = "lcgGeneralAndEdvanced";
            this.lcgGeneralAndEdvanced.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.emptySpaceItem5,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.emptySpaceItem7,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24});
            this.lcgGeneralAndEdvanced.Location = new System.Drawing.Point(0, 0);
            this.lcgGeneralAndEdvanced.Name = "lcgGeneralAndEdvanced";
            this.lcgGeneralAndEdvanced.OptionsItemText.TextToControlDistance = 5;
            this.lcgGeneralAndEdvanced.Size = new System.Drawing.Size(717, 304);
            this.lcgGeneralAndEdvanced.Text = "lcgGeneralAndEdvanced";
            this.lcgGeneralAndEdvanced.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.pnBasicAndAdvancedSKU;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(214, 30);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(713, 30);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(205, 267);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(413, 33);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.label4;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 267);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(145, 0);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(145, 30);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(145, 33);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.sedSKUInBevCount;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(145, 267);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(60, 33);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(60, 33);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(60, 33);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btnDefaultBasicAndAddSKU;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(618, 267);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(95, 33);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(95, 33);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(274, 74);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(92, 149);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btnInsertToBasicSKU;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(274, 30);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem18.Size = new System.Drawing.Size(92, 22);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnRemoveFromBasicSKU;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(274, 52);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem19.Size = new System.Drawing.Size(92, 22);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.btnRemoveFromAdvancedSKU;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(274, 245);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem20.Size = new System.Drawing.Size(92, 22);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.btnInsertToAdvancedSKU;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(274, 223);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem21.Size = new System.Drawing.Size(92, 22);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.gcSKUAll;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem22.Size = new System.Drawing.Size(274, 237);
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.groupControl1;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(366, 30);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem23.Size = new System.Drawing.Size(347, 117);
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.groupControl2;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(366, 147);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem24.Size = new System.Drawing.Size(347, 120);
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(160, 0);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(4, 326);
            this.splitterControl1.TabIndex = 1;
            this.splitterControl1.TabStop = false;
            // 
            // pnLeft
            // 
            this.pnLeft.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnLeft.Controls.Add(this.navBarMenu);
            this.pnLeft.Controls.Add(this.pnCreateReports);
            this.pnLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLeft.Location = new System.Drawing.Point(0, 0);
            this.pnLeft.Name = "pnLeft";
            this.pnLeft.Size = new System.Drawing.Size(160, 326);
            this.pnLeft.TabIndex = 0;
            // 
            // navBarMenu
            // 
            this.navBarMenu.ActiveGroup = this.navBarReportsGroup;
            this.navBarMenu.AllowSelectedLink = true;
            this.navBarMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.navBarMenu.ContentButtonHint = null;
            this.navBarMenu.DragDropFlags = DevExpress.XtraNavBar.NavBarDragDrop.None;
            this.navBarMenu.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarReportsGroup,
            this.navBarOptionsGroup});
            this.navBarMenu.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarReportListsItem,
            this.navBarCompetParaItem,
            this.navBarRelativePTCLimitsItem,
            this.navBarSKUItem,
            this.navBarPTCLimitsItem,
            this.navBarGeneralAndEdvancedSKUItem,
            this.navBarPTCShiftLimitsItem});
            this.navBarMenu.Location = new System.Drawing.Point(0, 8);
            this.navBarMenu.Name = "navBarMenu";
            this.navBarMenu.OptionsNavPane.ExpandedWidth = 164;
            this.navBarMenu.Size = new System.Drawing.Size(168, 283);
            this.navBarMenu.TabIndex = 0;
            this.navBarMenu.TabStop = true;
            this.navBarMenu.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarMenu_LinkClicked);
            // 
            // navBarReportsGroup
            // 
            this.navBarReportsGroup.Caption = "������";
            this.navBarReportsGroup.Expanded = true;
            this.navBarReportsGroup.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText;
            this.navBarReportsGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarReportListsItem)});
            this.navBarReportsGroup.Name = "navBarReportsGroup";
            // 
            // navBarReportListsItem
            // 
            this.navBarReportListsItem.Caption = "������ �������";
            this.navBarReportListsItem.Hint = "������ �������";
            this.navBarReportListsItem.Name = "navBarReportListsItem";
            this.navBarReportListsItem.Tag = "0";
            // 
            // navBarOptionsGroup
            // 
            this.navBarOptionsGroup.Caption = "�����";
            this.navBarOptionsGroup.Expanded = true;
            this.navBarOptionsGroup.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText;
            this.navBarOptionsGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarCompetParaItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarSKUItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarRelativePTCLimitsItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarPTCLimitsItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarPTCShiftLimitsItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarGeneralAndEdvancedSKUItem)});
            this.navBarOptionsGroup.Name = "navBarOptionsGroup";
            // 
            // navBarCompetParaItem
            // 
            this.navBarCompetParaItem.Caption = "������������ ����";
            this.navBarCompetParaItem.Hint = "������������ ����";
            this.navBarCompetParaItem.Name = "navBarCompetParaItem";
            this.navBarCompetParaItem.Tag = "1";
            // 
            // navBarSKUItem
            // 
            this.navBarSKUItem.Caption = "������ ���";
            this.navBarSKUItem.Hint = "������ ���";
            this.navBarSKUItem.Name = "navBarSKUItem";
            this.navBarSKUItem.Tag = "2";
            // 
            // navBarRelativePTCLimitsItem
            // 
            this.navBarRelativePTCLimitsItem.Caption = "Relative ��� ������";
            this.navBarRelativePTCLimitsItem.Hint = "Relative ��� ������";
            this.navBarRelativePTCLimitsItem.Name = "navBarRelativePTCLimitsItem";
            this.navBarRelativePTCLimitsItem.Tag = "3";
            // 
            // navBarPTCLimitsItem
            // 
            this.navBarPTCLimitsItem.Caption = "��� ������";
            this.navBarPTCLimitsItem.Hint = "��� ������";
            this.navBarPTCLimitsItem.Name = "navBarPTCLimitsItem";
            this.navBarPTCLimitsItem.Tag = "4";
            // 
            // navBarPTCShiftLimitsItem
            // 
            this.navBarPTCShiftLimitsItem.Caption = "���Shift ������";
            this.navBarPTCShiftLimitsItem.Hint = "���Shift ������";
            this.navBarPTCShiftLimitsItem.Name = "navBarPTCShiftLimitsItem";
            this.navBarPTCShiftLimitsItem.Tag = "5";
            // 
            // navBarGeneralAndEdvancedSKUItem
            // 
            this.navBarGeneralAndEdvancedSKUItem.Caption = "�������� � ���. ���";
            this.navBarGeneralAndEdvancedSKUItem.Hint = "�������� � �������������� ���";
            this.navBarGeneralAndEdvancedSKUItem.Name = "navBarGeneralAndEdvancedSKUItem";
            this.navBarGeneralAndEdvancedSKUItem.Tag = "6";
            // 
            // pnCreateReports
            // 
            this.pnCreateReports.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCreateReports.Controls.Add(this.btnCreateReports);
            this.pnCreateReports.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnCreateReports.Location = new System.Drawing.Point(0, 294);
            this.pnCreateReports.Name = "pnCreateReports";
            this.pnCreateReports.Size = new System.Drawing.Size(160, 32);
            this.pnCreateReports.TabIndex = 1;
            // 
            // btnCreateReports
            // 
            this.btnCreateReports.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCreateReports.Appearance.Options.UseFont = true;
            this.btnCreateReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCreateReports.Image = global::Logica.Reports.WbW.Properties.Resources.check_favorites_24;
            this.btnCreateReports.Location = new System.Drawing.Point(0, 0);
            this.btnCreateReports.Name = "btnCreateReports";
            this.btnCreateReports.Size = new System.Drawing.Size(160, 32);
            this.btnCreateReports.TabIndex = 1;
            this.btnCreateReports.Text = "&������� ������";
            this.btnCreateReports.Click += new System.EventHandler(this.btnCreateReports_Click);
            // 
            // pnReportHeader
            // 
            this.pnReportHeader.Controls.Add(this.edWave3Text);
            this.pnReportHeader.Controls.Add(this.lbWave3);
            this.pnReportHeader.Controls.Add(this.edWave2Text);
            this.pnReportHeader.Controls.Add(this.lbWave2);
            this.pnReportHeader.Controls.Add(this.edWave1Text);
            this.pnReportHeader.Controls.Add(this.lbWave1);
            this.pnReportHeader.Controls.Add(this.edChanelText);
            this.pnReportHeader.Controls.Add(this.lbChanel);
            this.pnReportHeader.Controls.Add(this.edRegionText);
            this.pnReportHeader.Controls.Add(this.lbRegion);
            this.pnReportHeader.Controls.Add(this.lbReportTitle);
            this.pnReportHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportHeader.Location = new System.Drawing.Point(0, 40);
            this.pnReportHeader.Name = "pnReportHeader";
            this.pnReportHeader.Size = new System.Drawing.Size(885, 88);
            this.pnReportHeader.TabIndex = 1;
            // 
            // edWave3Text
            // 
            this.edWave3Text.Location = new System.Drawing.Point(475, 64);
            this.edWave3Text.MenuManager = this.bmReport;
            this.edWave3Text.Name = "edWave3Text";
            this.edWave3Text.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edWave3Text.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edWave3Text.Properties.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.edWave3Text.Properties.Appearance.Options.UseBackColor = true;
            this.edWave3Text.Properties.Appearance.Options.UseFont = true;
            this.edWave3Text.Properties.Appearance.Options.UseForeColor = true;
            this.edWave3Text.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edWave3Text.Properties.ReadOnly = true;
            this.edWave3Text.Size = new System.Drawing.Size(150, 20);
            this.edWave3Text.TabIndex = 10;
            this.edWave3Text.TabStop = false;
            // 
            // lbWave3
            // 
            this.lbWave3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbWave3.Appearance.Options.UseFont = true;
            this.lbWave3.Location = new System.Drawing.Point(421, 68);
            this.lbWave3.Name = "lbWave3";
            this.lbWave3.Size = new System.Drawing.Size(48, 13);
            this.lbWave3.TabIndex = 9;
            this.lbWave3.Text = "����� 3:";
            // 
            // edWave2Text
            // 
            this.edWave2Text.Location = new System.Drawing.Point(266, 64);
            this.edWave2Text.MenuManager = this.bmReport;
            this.edWave2Text.Name = "edWave2Text";
            this.edWave2Text.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edWave2Text.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edWave2Text.Properties.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.edWave2Text.Properties.Appearance.Options.UseBackColor = true;
            this.edWave2Text.Properties.Appearance.Options.UseFont = true;
            this.edWave2Text.Properties.Appearance.Options.UseForeColor = true;
            this.edWave2Text.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edWave2Text.Properties.ReadOnly = true;
            this.edWave2Text.Size = new System.Drawing.Size(150, 20);
            this.edWave2Text.TabIndex = 8;
            this.edWave2Text.TabStop = false;
            // 
            // lbWave2
            // 
            this.lbWave2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbWave2.Appearance.Options.UseFont = true;
            this.lbWave2.Location = new System.Drawing.Point(212, 68);
            this.lbWave2.Name = "lbWave2";
            this.lbWave2.Size = new System.Drawing.Size(48, 13);
            this.lbWave2.TabIndex = 7;
            this.lbWave2.Text = "����� 2:";
            // 
            // edWave1Text
            // 
            this.edWave1Text.Location = new System.Drawing.Point(57, 64);
            this.edWave1Text.MenuManager = this.bmReport;
            this.edWave1Text.Name = "edWave1Text";
            this.edWave1Text.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edWave1Text.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edWave1Text.Properties.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.edWave1Text.Properties.Appearance.Options.UseBackColor = true;
            this.edWave1Text.Properties.Appearance.Options.UseFont = true;
            this.edWave1Text.Properties.Appearance.Options.UseForeColor = true;
            this.edWave1Text.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edWave1Text.Properties.ReadOnly = true;
            this.edWave1Text.Size = new System.Drawing.Size(150, 20);
            this.edWave1Text.TabIndex = 6;
            this.edWave1Text.TabStop = false;
            // 
            // lbWave1
            // 
            this.lbWave1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbWave1.Appearance.Options.UseFont = true;
            this.lbWave1.Location = new System.Drawing.Point(4, 68);
            this.lbWave1.Name = "lbWave1";
            this.lbWave1.Size = new System.Drawing.Size(48, 13);
            this.lbWave1.TabIndex = 5;
            this.lbWave1.Text = "����� 1:";
            // 
            // edChanelText
            // 
            this.edChanelText.Location = new System.Drawing.Point(266, 31);
            this.edChanelText.MenuManager = this.bmReport;
            this.edChanelText.Name = "edChanelText";
            this.edChanelText.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edChanelText.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edChanelText.Properties.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.edChanelText.Properties.Appearance.Options.UseBackColor = true;
            this.edChanelText.Properties.Appearance.Options.UseFont = true;
            this.edChanelText.Properties.Appearance.Options.UseForeColor = true;
            this.edChanelText.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edChanelText.Properties.ReadOnly = true;
            this.edChanelText.Size = new System.Drawing.Size(150, 20);
            this.edChanelText.TabIndex = 4;
            this.edChanelText.TabStop = false;
            // 
            // lbChanel
            // 
            this.lbChanel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbChanel.Appearance.Options.UseFont = true;
            this.lbChanel.Location = new System.Drawing.Point(222, 35);
            this.lbChanel.Name = "lbChanel";
            this.lbChanel.Size = new System.Drawing.Size(38, 13);
            this.lbChanel.TabIndex = 3;
            this.lbChanel.Text = "�����:";
            // 
            // edRegionText
            // 
            this.edRegionText.Location = new System.Drawing.Point(57, 31);
            this.edRegionText.MenuManager = this.bmReport;
            this.edRegionText.Name = "edRegionText";
            this.edRegionText.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edRegionText.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edRegionText.Properties.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.edRegionText.Properties.Appearance.Options.UseBackColor = true;
            this.edRegionText.Properties.Appearance.Options.UseFont = true;
            this.edRegionText.Properties.Appearance.Options.UseForeColor = true;
            this.edRegionText.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edRegionText.Properties.ReadOnly = true;
            this.edRegionText.Size = new System.Drawing.Size(150, 20);
            this.edRegionText.TabIndex = 2;
            this.edRegionText.TabStop = false;
            // 
            // lbRegion
            // 
            this.lbRegion.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbRegion.Appearance.Options.UseFont = true;
            this.lbRegion.Location = new System.Drawing.Point(9, 35);
            this.lbRegion.Name = "lbRegion";
            this.lbRegion.Size = new System.Drawing.Size(43, 13);
            this.lbRegion.TabIndex = 1;
            this.lbRegion.Text = "������:";
            // 
            // lbReportTitle
            // 
            this.lbReportTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbReportTitle.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbReportTitle.Location = new System.Drawing.Point(3, 3);
            this.lbReportTitle.Name = "lbReportTitle";
            this.lbReportTitle.Size = new System.Drawing.Size(879, 18);
            this.lbReportTitle.TabIndex = 0;
            this.lbReportTitle.Text = "Title";
            this.lbReportTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnReportToolBar
            // 
            this.pnReportToolBar.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pnReportToolBar.Controls.Add(this.bdocReport);
            this.pnReportToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportToolBar.Location = new System.Drawing.Point(0, 0);
            this.pnReportToolBar.Name = "pnReportToolBar";
            this.pnReportToolBar.Size = new System.Drawing.Size(885, 40);
            this.pnReportToolBar.TabIndex = 0;
            // 
            // pmCompetPara
            // 
            this.pmCompetPara.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pmAddItem,
            this.pmEditItem,
            this.toolStripSeparator1,
            this.pmDelItem,
            this.pmDelAllItem,
            this.toolStripSeparator2,
            this.pmDefaultItem});
            this.pmCompetPara.Name = "pmCompetPara";
            this.pmCompetPara.ShowImageMargin = false;
            this.pmCompetPara.Size = new System.Drawing.Size(193, 126);
            // 
            // pmAddItem
            // 
            this.pmAddItem.Name = "pmAddItem";
            this.pmAddItem.ShortcutKeys = System.Windows.Forms.Keys.Insert;
            this.pmAddItem.Size = new System.Drawing.Size(192, 22);
            this.pmAddItem.Text = "&��������";
            // 
            // pmEditItem
            // 
            this.pmEditItem.Name = "pmEditItem";
            this.pmEditItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.pmEditItem.Size = new System.Drawing.Size(192, 22);
            this.pmEditItem.Text = "&��������";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(189, 6);
            // 
            // pmDelItem
            // 
            this.pmDelItem.Name = "pmDelItem";
            this.pmDelItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.pmDelItem.Size = new System.Drawing.Size(192, 22);
            this.pmDelItem.Text = "&�������...";
            // 
            // pmDelAllItem
            // 
            this.pmDelAllItem.Name = "pmDelAllItem";
            this.pmDelAllItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.D)));
            this.pmDelAllItem.Size = new System.Drawing.Size(192, 22);
            this.pmDelAllItem.Text = "&������� ���..";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(189, 6);
            // 
            // pmDefaultItem
            // 
            this.pmDefaultItem.Name = "pmDefaultItem";
            this.pmDefaultItem.Size = new System.Drawing.Size(192, 22);
            this.pmDefaultItem.Text = "�� ���������";
            // 
            // imgBtn
            // 
            this.imgBtn.ImageSize = new System.Drawing.Size(24, 24);
            this.imgBtn.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgBtn.ImageStream")));
            this.imgBtn.Images.SetKeyName(0, "arrow_next_24.png");
            this.imgBtn.Images.SetKeyName(1, "arrow_back_24.png");
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(516, 301);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(84, 23);
            this.simpleButton8.TabIndex = 6;
            this.simpleButton8.Text = "�� ���������";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 294);
            this.emptySpaceItem2.Name = "emptySpaceItem1";
            this.emptySpaceItem2.Size = new System.Drawing.Size(509, 33);
            this.emptySpaceItem2.Text = "emptySpaceItem1";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // WccpUIControl
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnReportSheet);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(885, 480);
            this.Load += new System.EventHandler(this.ReportControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).EndInit();
            this.pnReportSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcReport)).EndInit();
            this.tcReport.ResumeLayout(false);
            this.tpOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).EndInit();
            this.pnMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnClient)).EndInit();
            this.pnClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcReportOptions)).EndInit();
            this.tcReportOptions.ResumeLayout(false);
            this.tabOptionsReportLists.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcReportList)).EndInit();
            this.dlcReportList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkedSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcReportList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsReportList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReportList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgReportList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGridReportList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            this.tabOptionsCompetPair.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcCompetPara)).EndInit();
            this.dlcCompetPara.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnEditCompetPair)).EndInit();
            this.pnEditCompetPair.ResumeLayout(false);
            this.pnEditCompetPair.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luSKUName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKU2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luSKUName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKU1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcCompetPair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsCompetPair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCompetPair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCompetPairCaption)).EndInit();
            this.pnCompetPairCaption.ResumeLayout(false);
            this.pnCompetPairCaption.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCompetPara)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.tabOptionsSKU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcSKU)).EndInit();
            this.dlcSKU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKUList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGridSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.espSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBtnSKU)).EndInit();
            this.tabOptionsRelativePTCLimits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcRelativePTCLimit)).EndInit();
            this.dlcRelativePTCLimit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcRelativePTCLimits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRelativeRPTLimits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRelativePTCLimits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnRelativePTCLimits)).EndInit();
            this.pnRelativePTCLimits.ResumeLayout(false);
            this.pnRelativePTCLimits.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRelativePTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.tabOptionsPTCLimits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcPTCLimit)).EndInit();
            this.dlcPTCLimit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcPTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSKUPTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKUPTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSKUPTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnPTCLimits)).EndInit();
            this.pnPTCLimits.ResumeLayout(false);
            this.pnPTCLimits.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcgPTCLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.tabOptionsPTCShiftLimits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcPTCShiftLimitsOptions)).EndInit();
            this.dlcPTCShiftLimitsOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcPTCShiftLimits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPTCShiftLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPTCShiftLimits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            this.tabOptionsGeneralAndEdvancedSKU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcGeneralAndEdvanced)).EndInit();
            this.dlcGeneralAndEdvanced.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDopSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDopSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDopSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcOsnSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOsnSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOsnSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSKUAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSKUAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSKUAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sedSKUInBevCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBasicAndAdvancedSKU)).EndInit();
            this.pnBasicAndAdvancedSKU.ResumeLayout(false);
            this.pnBasicAndAdvancedSKU.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgGeneralAndEdvanced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeft)).EndInit();
            this.pnLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCreateReports)).EndInit();
            this.pnCreateReports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).EndInit();
            this.pnReportHeader.ResumeLayout(false);
            this.pnReportHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edWave3Text.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edWave2Text.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edWave1Text.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edChanelText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edRegionText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).EndInit();
            this.pnReportToolBar.ResumeLayout(false);
            this.pnReportToolBar.PerformLayout();
            this.pmCompetPara.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnReportSheet;
        private DevExpress.XtraEditors.PanelControl pnReportToolBar;
        private DevExpress.XtraBars.BarManager bmReport;
        private DevExpress.XtraBars.Bar barMenu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.StandaloneBarDockControl bdocReport;
        private DevExpress.XtraBars.BarAndDockingController bdcReport;
        private DevExpress.XtraBars.BarButtonItem tbtnReportToExcel;
        private DevExpress.Utils.ImageCollection imgNormal;
        private DevExpress.Utils.ImageCollection imgLarge;
        private DevExpress.XtraEditors.PanelControl pnReportHeader;
        private System.Windows.Forms.Label lbReportTitle;
        private DevExpress.XtraEditors.TextEdit edRegionText;
        private DevExpress.XtraEditors.LabelControl lbRegion;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem tbtnReportRefresh;
        private DevExpress.XtraTab.XtraTabControl tcReport;
        private DevExpress.XtraTab.XtraTabPage tpOptions;
        private DevExpress.XtraEditors.TextEdit edChanelText;
        private DevExpress.XtraEditors.LabelControl lbChanel;
        private DevExpress.XtraEditors.TextEdit edWave2Text;
        private DevExpress.XtraEditors.LabelControl lbWave2;
        private DevExpress.XtraEditors.TextEdit edWave1Text;
        private DevExpress.XtraEditors.LabelControl lbWave1;
        private DevExpress.XtraEditors.TextEdit edWave3Text;
        private DevExpress.XtraEditors.LabelControl lbWave3;
        private DevExpress.XtraEditors.PanelControl pnMain;
        private DevExpress.XtraEditors.PanelControl pnClient;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.PanelControl pnLeft;
        private DevExpress.XtraNavBar.NavBarControl navBarMenu;
        private DevExpress.XtraNavBar.NavBarGroup navBarReportsGroup;
        private DevExpress.XtraNavBar.NavBarItem navBarReportListsItem;
        private DevExpress.XtraNavBar.NavBarGroup navBarOptionsGroup;
        private DevExpress.XtraNavBar.NavBarItem navBarCompetParaItem;
        private DevExpress.XtraNavBar.NavBarItem navBarRelativePTCLimitsItem;
        private DevExpress.XtraNavBar.NavBarItem navBarSKUItem;
        private DevExpress.XtraNavBar.NavBarItem navBarPTCLimitsItem;
        private DevExpress.XtraNavBar.NavBarItem navBarGeneralAndEdvancedSKUItem;
        private DevExpress.XtraEditors.PanelControl pnCreateReports;
        private DevExpress.XtraEditors.SimpleButton btnCreateReports;
        private DevExpress.XtraTab.XtraTabControl tcReportOptions;
        private DevExpress.XtraTab.XtraTabPage tabOptionsReportLists;
        private DevExpress.XtraTab.XtraTabPage tabOptionsCompetPair;
        private DevExpress.XtraTab.XtraTabPage tabOptionsRelativePTCLimits;
        private DevExpress.XtraTab.XtraTabPage tabOptionsSKU;
        private DevExpress.XtraTab.XtraTabPage tabOptionsPTCLimits;
        private DevExpress.XtraTab.XtraTabPage tabOptionsGeneralAndEdvancedSKU;
        private System.Windows.Forms.ContextMenuStrip pmCompetPara;
        private System.Windows.Forms.ToolStripMenuItem pmAddItem;
        private System.Windows.Forms.ToolStripMenuItem pmEditItem;
        private System.Windows.Forms.ToolStripMenuItem pmDelItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem pmDelAllItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem pmDefaultItem;
        private DevExpress.Utils.ImageCollection imgBtn;
        private System.Windows.Forms.BindingSource bsReportList;
        private ReportDataSet reportDataSet;
        private DevExpress.XtraDataLayout.DataLayoutControl dlcReportList;
        private DevExpress.XtraGrid.GridControl gcReportList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvReportList;
        private DevExpress.XtraGrid.Columns.GridColumn colReport_Checked;
        private DevExpress.XtraGrid.Columns.GridColumn colReport_Name;
        private DevExpress.XtraLayout.LayoutControlGroup lcgReportList;
        private DevExpress.XtraLayout.LayoutControlItem lciGridReportList;
        private DevExpress.XtraDataLayout.DataLayoutControl dlcSKU;
        private DevExpress.XtraLayout.LayoutControlGroup lcgSKU;
        private DevExpress.XtraEditors.LabelControl lbHeaderSKU;
        private DevExpress.XtraLayout.LayoutControlItem lciSKU;
        private DevExpress.XtraGrid.GridControl gcSKU;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSKU;
        private DevExpress.XtraLayout.LayoutControlItem lciGridSKU;
        private DevExpress.XtraEditors.SimpleButton btnDefaultSKU;
        private DevExpress.XtraLayout.EmptySpaceItem espSKU;
        private DevExpress.XtraLayout.LayoutControlItem lciBtnSKU;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraDataLayout.DataLayoutControl dlcCompetPara;
        private DevExpress.XtraLayout.LayoutControlGroup lcgCompetPara;
        private DevExpress.XtraDataLayout.DataLayoutControl dlcRelativePTCLimit;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRelativePTCLimit;
        private DevExpress.XtraDataLayout.DataLayoutControl dlcPTCLimit;
        private DevExpress.XtraLayout.LayoutControlGroup lcgPTCLimit;
        private DevExpress.XtraDataLayout.DataLayoutControl dlcGeneralAndEdvanced;
        private DevExpress.XtraLayout.LayoutControlGroup lcgGeneralAndEdvanced;
        private DevExpress.XtraEditors.PanelControl pnCompetPairCaption;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.Label lbCompetPairCaption;
        private DevExpress.XtraEditors.PanelControl pnEditCompetPair;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnDefaultCompetPair;
        private DevExpress.XtraEditors.SimpleButton btnDelAllCompetPair;
        private DevExpress.XtraEditors.SimpleButton btnDelCompetPair;
        private DevExpress.XtraEditors.SimpleButton btnEditCompetPair;
        private DevExpress.XtraEditors.SimpleButton btnAddCompetPair;
        private DevExpress.XtraEditors.SimpleButton btnCancelCompetPair;
        private DevExpress.XtraEditors.SimpleButton btnSaveCompetPair;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit luSKUName2;
        private DevExpress.XtraEditors.LookUpEdit luSKUName1;
        private DevExpress.XtraGrid.GridControl gcCompetPair;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCompetPair;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSKUName1;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSKUName2;
        private System.Windows.Forms.BindingSource bsSKU1;
        private System.Windows.Forms.BindingSource bsSKU2;
        private System.Windows.Forms.BindingSource bsCompetPair;
        private DevExpress.XtraEditors.PanelControl pnRelativePTCLimits;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.GridControl gcRelativePTCLimits;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRelativePTCLimits;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton btnDefaultRelativePTCLimits;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn gcolMinRelativePTCLimit;
        private DevExpress.XtraGrid.Columns.GridColumn gcolMaxRelativePTCLimit;
        private System.Windows.Forms.BindingSource bsRelativeRPTLimits;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSKUListIsChecked;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSKUListSKU;
        private System.Windows.Forms.BindingSource bsSKUList;
        private DevExpress.XtraEditors.PanelControl pnPTCLimits;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.Label lbPTCLimits;
        private DevExpress.XtraGrid.GridControl gcSKUPTCLimit;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSKUPTCLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.GridControl gcPTCLimit;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPTCLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.SimpleButton btnDefaultSKUPTCLimit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSKUPTCLimit;
        private DevExpress.XtraGrid.Columns.GridColumn gcolMinPTCLimit;
        private DevExpress.XtraGrid.Columns.GridColumn gcolMaxPTCLimit;
        private DevExpress.XtraEditors.PanelControl pnBasicAndAdvancedSKU;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.Label lbBasicAndAdvancedSKU;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.SpinEdit sedSKUInBevCount;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.SimpleButton btnDefaultBasicAndAddSKU;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.SimpleButton btnInsertToAdvancedSKU;
        private DevExpress.XtraEditors.SimpleButton btnRemoveFromAdvancedSKU;
        private DevExpress.XtraEditors.SimpleButton btnRemoveFromBasicSKU;
        private DevExpress.XtraEditors.SimpleButton btnInsertToBasicSKU;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraGrid.GridControl gcSKUAll;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSKUAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraGrid.GridControl gcDopSKU;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDopSKU;
        private DevExpress.XtraGrid.GridControl gcOsnSKU;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOsnSKU;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource bsSKUPTCLimit;
        private System.Windows.Forms.BindingSource bsSKUAll;
        private DevExpress.XtraTab.XtraTabPage tabOptionsPTCShiftLimits;
        private DevExpress.XtraNavBar.NavBarItem navBarPTCShiftLimitsItem;
        private DevExpress.XtraDataLayout.DataLayoutControl dlcPTCShiftLimitsOptions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.GridControl gcPTCShiftLimits;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPTCShiftLimits;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton btnDefaultPTCShiftLimits;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private System.Windows.Forms.BindingSource bsPTCShiftLimit;
        private System.Windows.Forms.BindingSource bsOsnSKU;
        private System.Windows.Forms.BindingSource bsDopSKU;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource bsPTCLimit;
        private DevExpress.XtraGrid.Columns.GridColumn gcolLimitName;
        private DevExpress.XtraGrid.Columns.GridColumn gcolPTCLimitName;
        private DevExpress.XtraGrid.Columns.GridColumn gcolPTCShiftName;
        private DevExpress.XtraEditors.CheckEdit chkedSelectAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;

    }
}
