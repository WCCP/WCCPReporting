namespace WccpReporting
{
    partial class CoolersAnalysisReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CoolersAnalysisReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // CoolersAnalysisReportScrollableControl
            // 
            this.CoolersAnalysisReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CoolersAnalysisReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.CoolersAnalysisReportScrollableControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.CoolersAnalysisReportScrollableControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.CoolersAnalysisReportScrollableControl.Name = "CoolersAnalysisReportScrollableControl";
            this.CoolersAnalysisReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.CoolersAnalysisReportScrollableControl.TabIndex = 0;
            // 
            // CoolersAnalysisReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CoolersAnalysisReportScrollableControl);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "CoolersAnalysisReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl CoolersAnalysisReportScrollableControl;
    }
}
