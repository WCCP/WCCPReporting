using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WccpReporting
{
    public partial class Wait : Form
    {
        static private Form _Wait= null;

        public Wait()
        {
            InitializeComponent();
        }

        static public void StartWait()
        {
            if (_Wait == null)
            {
                _Wait = new Wait();
                _Wait.Text = "���������";
                ((Wait)_Wait).lcProgressBarItem.ContentVisible = false;
                _Wait.Show();
                Application.DoEvents();
                Cursor.Current = Cursors.WaitCursor;
            };
        }

        static public void StartWait(string captionText, bool IsProgress, int maxValue)
        {
            if (_Wait == null)
            {
                _Wait = new Wait();
                _Wait.Text = captionText;
                ((Wait)_Wait).ProgressBar.Visible = IsProgress;
                ((Wait)_Wait).ProgressBar.Properties.Maximum = maxValue;
                ((Wait)_Wait).ProgressBar.Properties.Minimum = 0;
                ((Wait)_Wait).ProgressBar.Properties.Step = 1;
                if (maxValue == 1)
                {
                    ((Wait)_Wait).ProgressBar.Position = maxValue;
                };
                _Wait.Show();                
                Application.DoEvents();
                Cursor.Current = Cursors.WaitCursor;
            };
        }

        static public void StepWait()
        {
            ((Wait)_Wait).ProgressBar.Position = ((Wait)_Wait).ProgressBar.Position + 1;
            ((Wait)_Wait).ProgressBar.Update();
        }

        static public void StopWait()
        {
            if (_Wait != null)
            {
                _Wait.Close();
                _Wait.Dispose();
                _Wait = null;
                Cursor.Current = Cursors.Default;
                Application.DoEvents();
            };
        }
    }
}