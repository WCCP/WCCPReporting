using System;
using System.Windows.Forms;
using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpWbW.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        private int tab = 0;

        //���������� ������ ������
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                string winAuth = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog={0};Data Source={1};Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;Tag with column collation when possible=False;";
                string serverAuth = "Provider=SQLOLEDB.1;Password={0};Persist Security Info=True;User ID={1};Initial Catalog={2};Data Source={3};Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;Tag with column collation when possible=False";

                SqlConnectionStringBuilder con = new SqlConnectionStringBuilder(HostConfiguration.DBSqlConnection);

                string lConnectionString;

                if (con.IntegratedSecurity)
                {
                    lConnectionString = string.Format(winAuth, con.InitialCatalog, con.DataSource);
                }
                else
                {
                    lConnectionString = string.Format(serverAuth, con.Password, con.UserID, con.InitialCatalog,
                                                      con.DataSource);
                }

                WCCPConst.ConnectionString = lConnectionString;
                ReportOptions.ReportTitle = reportCaption;

                using (InputReportDataForm fmInputReportDataForm = new InputReportDataForm())
                {
                    fmInputReportDataForm.ReportName = reportCaption;

                    if (fmInputReportDataForm.ShowDialog() == DialogResult.OK)
                    {
                        _reportCaption = fmInputReportDataForm.ReportName + ", " + fmInputReportDataForm.RegionName + ", " +
                            fmInputReportDataForm.ChanelName + ", " + fmInputReportDataForm.WaveName;

                        fmInputReportDataForm.ReportName = _reportCaption;


                        ReportOptions.ReportLongTitle = _reportCaption;

                        _reportControl = new WccpUIControl();

                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);

                return 1;
            }
        }

        //����� ������ ������
        public void CloseUI()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool AllowClose()
        {
            return true;
        }

    }
}
