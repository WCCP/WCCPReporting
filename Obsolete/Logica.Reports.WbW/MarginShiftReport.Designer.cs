namespace WccpReporting
{
    partial class MarginShiftReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MarginShiftReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // MarginShiftReportScrollableControl
            // 
            this.MarginShiftReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MarginShiftReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.MarginShiftReportScrollableControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.MarginShiftReportScrollableControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MarginShiftReportScrollableControl.Name = "MarginShiftReportScrollableControl";
            this.MarginShiftReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.MarginShiftReportScrollableControl.TabIndex = 0;
            // 
            // MarginShiftReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MarginShiftReportScrollableControl);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "MarginShiftReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl MarginShiftReportScrollableControl;

    }
}
