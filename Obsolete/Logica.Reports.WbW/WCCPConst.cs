using System;

namespace WccpReporting
{
    static class ReportOptions
    {        
        static public string ReportTitle = String.Empty;
        static public string ReportLongTitle = String.Empty;
        static public int RegionId       = -1;
        static public string RegionName  = String.Empty;
        static public string ChanelName  = String.Empty;
        static public int WaveId         = -1;
        static public string WaveName    = String.Empty;
    };

    static class WCCPConst
    {
        public static string root {
            //get { return WCCPAPI.GetApplicationPath(); }
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }
        static public bool   IsSkin = false;
        static public string ConnectionString;
        static public int    CommandTimeout = 300;
        
        static public string msgNoHelp              = "� ������ ������ ��������� ������� �����������";
        static public string msgErrorCloseConnection = "�� ���� ������� ����������";
        static public string msgErrorCreateWaveList = "�� ���� ������� ������ ����";
        static public string msgErrorCreateSupervisorList = "�� ���� ������� ������ �������������";
        static public string msgErrorSaveOptions    = "�� ���� ��������� �����";
        static public string msgErrorSupervisorIsNull = "����������� ������ ���� �����";
        static public string msgErrorPrepare = "�� ���� ������������ ��������������� ����� ������";
        static public string msgErrorCreateListData = "�� ���� ������������ ������ ������";
        static public string msgErrorCreateReportData = "�� ���� ������������ ������ ��� ������";
        static public string msgErrorConnect = "�� ���� ������������ � ���� ������";
        static public string msgErrorCreateDefaultListCompetPair = "�� ���� ������� ������ ������������ ��� �� ���������";
        static public string msgErrorCreateSKUListCompetPair = "�� ���� ������� ������ ��� ��� ������������ ���";
        static public string msgErrorCreateRelativeRPTLimits = "�� ���� ������� ������ Relative PTC �������";
        static public string msgErrorCreateSKUList = "�� ���� ������� ������ ���";
        static public string msgErrorCreateDefaultSKUPTCLimit = "�� ���� ������� ������ PTC ������� ��� ������ ���";
        static public string msgErrorCreateSKUPTCLimit = "�� ���� ������� ������ PTC ������� ��� ���";
        static public string msgErrorCreateListReports = "�� ���� ������� ������ �������";
        static public string msgErrorCreateDefaultOsnDopSKU = "�� ���� ������� ������ �� ��������� ��� �������� � �������������� ���";
        static public string msgErrorShowListOsnDopSKU = "�� ���� ���������� ������ �������� � �������������� ���";
        static public string msgErrorCreateDefaultPTCShiftLimit = "�� ���� ������� ������ PTC Shift ������� �� ���������";
        static public string msgErrorShowListPTCShiftLimit = "�� ���� ���������� ������ PTC Shift �������";
        static public string msgErrorExecProc = "�� ���� ��������� ���������";
        static public string msgErrorAddRecord = "�� ���� ������� ������";
        static public string msgErrorEditRecord = "�� ���� �������� ������";
        static public string msgErrorRegionIsNull = "������ ������ ���� �����";
        static public string msgErrorChanelIsNull = "����� ������ ���� �����";
        static public string msgErrorWaveIsNull = "����� ������ ���� ������";
        static public string msgErrorSKU1IsNull = "��� 1 ������ ���� �����";
        static public string msgErrorSKU2IsNull = "��� 2 ������ ���� �����";

        static public string msgWarningCreateSKUPTCLimit = "�� ���� ���������� ������ ������� ��� ���. ������ ��� �� �����.";
        static public string msgWarningNotItem = "��� ������� ��� ���������";
        static public string msgWarningNotReportChecked = "�� ������ �����";
        //

        //
        static public string msgQuestionDeleteItem  = "������� ������ ����� �������. \n������� ������� ������ ?";
        static public string msgQuestionDeleteAllItem = "������ ����� �������. \n������� ������ ?";
        static public string msgQuestionDefaultItem = "���������� ����� ������� ����� ������ � ����� ������ ����� ������� �� ���������. \n������� ����� ������� �� ��������� ?";
    }

}
