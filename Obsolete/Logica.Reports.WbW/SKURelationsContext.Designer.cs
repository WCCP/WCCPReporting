namespace WccpReporting
{
    partial class SKURelationsContext
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gcTableContext = new DevExpress.XtraGrid.GridControl();
            this.gvTableContext = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcSKURelations_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKURelations_MS_Inbev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKURelations_MS_Brand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKURelations_PTR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKURelations_PTC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKURelations_POCS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKURelations_Volume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSKURelations_isBasic = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTableContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTableContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dataLayoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.dataLayoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dataLayoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.dataLayoutControl1.Controls.Add(this.gcTableContext);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.dataLayoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(640, 375);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gcTableContext
            // 
            this.gcTableContext.Location = new System.Drawing.Point(7, 7);
            this.gcTableContext.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gcTableContext.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcTableContext.MainView = this.gvTableContext;
            this.gcTableContext.Name = "gcTableContext";
            this.gcTableContext.Size = new System.Drawing.Size(626, 361);
            this.gcTableContext.TabIndex = 4;
            this.gcTableContext.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTableContext});
            // 
            // gvTableContext
            // 
            this.gvTableContext.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gvTableContext.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvTableContext.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcSKURelations_Name,
            this.gcSKURelations_MS_Inbev,
            this.gcSKURelations_MS_Brand,
            this.gcSKURelations_PTR,
            this.gcSKURelations_PTC,
            this.gcSKURelations_POCS,
            this.gcSKURelations_Volume,
            this.gcSKURelations_isBasic});
            this.gvTableContext.GridControl = this.gcTableContext;
            this.gvTableContext.Name = "gvTableContext";
            this.gvTableContext.OptionsBehavior.AllowIncrementalSearch = true;
            this.gvTableContext.OptionsBehavior.Editable = false;
            this.gvTableContext.OptionsCustomization.AllowColumnMoving = false;
            this.gvTableContext.OptionsCustomization.AllowGroup = false;
            this.gvTableContext.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvTableContext.OptionsPrint.AutoWidth = false;
            this.gvTableContext.OptionsView.ShowGroupPanel = false;
            this.gvTableContext.OptionsView.ShowIndicator = false;
            this.gvTableContext.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gvTableContext_RowStyle);
            // 
            // gcSKURelations_Name
            // 
            this.gcSKURelations_Name.AppearanceHeader.BackColor = System.Drawing.Color.White;
            this.gcSKURelations_Name.AppearanceHeader.Options.UseBackColor = true;
            this.gcSKURelations_Name.Caption = " ";
            this.gcSKURelations_Name.FieldName = "Name";
            this.gcSKURelations_Name.Name = "gcSKURelations_Name";
            this.gcSKURelations_Name.Visible = true;
            this.gcSKURelations_Name.VisibleIndex = 0;
            // 
            // gcSKURelations_MS_Inbev
            // 
            this.gcSKURelations_MS_Inbev.Caption = "MS InBev";
            this.gcSKURelations_MS_Inbev.FieldName = "MS_Inbev";
            this.gcSKURelations_MS_Inbev.Name = "gcSKURelations_MS_Inbev";
            this.gcSKURelations_MS_Inbev.Visible = true;
            this.gcSKURelations_MS_Inbev.VisibleIndex = 1;
            // 
            // gcSKURelations_MS_Brand
            // 
            this.gcSKURelations_MS_Brand.Caption = "MS brand";
            this.gcSKURelations_MS_Brand.FieldName = "MS_Brand";
            this.gcSKURelations_MS_Brand.Name = "gcSKURelations_MS_Brand";
            this.gcSKURelations_MS_Brand.Visible = true;
            this.gcSKURelations_MS_Brand.VisibleIndex = 2;
            // 
            // gcSKURelations_PTR
            // 
            this.gcSKURelations_PTR.Caption = "PTR ($/unit)";
            this.gcSKURelations_PTR.FieldName = "PTR";
            this.gcSKURelations_PTR.Name = "gcSKURelations_PTR";
            this.gcSKURelations_PTR.Visible = true;
            this.gcSKURelations_PTR.VisibleIndex = 3;
            // 
            // gcSKURelations_PTC
            // 
            this.gcSKURelations_PTC.Caption = "PTC ($/unit)";
            this.gcSKURelations_PTC.FieldName = "PTC";
            this.gcSKURelations_PTC.Name = "gcSKURelations_PTC";
            this.gcSKURelations_PTC.Visible = true;
            this.gcSKURelations_PTC.VisibleIndex = 4;
            // 
            // gcSKURelations_POCS
            // 
            this.gcSKURelations_POCS.Caption = "# POCs";
            this.gcSKURelations_POCS.FieldName = "POCS";
            this.gcSKURelations_POCS.Name = "gcSKURelations_POCS";
            this.gcSKURelations_POCS.Visible = true;
            this.gcSKURelations_POCS.VisibleIndex = 5;
            // 
            // gcSKURelations_Volume
            // 
            this.gcSKURelations_Volume.Caption = "Volume (dal/wk)";
            this.gcSKURelations_Volume.FieldName = "Volume";
            this.gcSKURelations_Volume.Name = "gcSKURelations_Volume";
            this.gcSKURelations_Volume.Visible = true;
            this.gcSKURelations_Volume.VisibleIndex = 6;
            // 
            // gcSKURelations_isBasic
            // 
            this.gcSKURelations_isBasic.Caption = "isBasic";
            this.gcSKURelations_isBasic.FieldName = "isBasic";
            this.gcSKURelations_isBasic.Name = "gcSKURelations_isBasic";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(640, 375);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gcTableContext;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(636, 371);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // SKURelationsContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "SKURelationsContext";
            this.Size = new System.Drawing.Size(640, 375);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTableContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTableContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gcTableContext;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTableContext;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_Name;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_MS_Inbev;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_MS_Brand;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_PTR;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_PTC;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_POCS;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_Volume;
        private DevExpress.XtraGrid.Columns.GridColumn gcSKURelations_isBasic;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;

    }
}
