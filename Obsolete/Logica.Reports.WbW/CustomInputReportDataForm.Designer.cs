namespace WccpReporting
{
    partial class CustomInputReportDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PageControlOptions = new DevExpress.XtraTab.XtraTabControl();
            this.OptionsTabSheet = new DevExpress.XtraTab.XtraTabPage();
            this.OptionsPanel = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.grpOptionsReport = new DevExpress.XtraEditors.GroupControl();
            this.edReportName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).BeginInit();
            this.PanelClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).BeginInit();
            this.PageControlOptions.SuspendLayout();
            this.OptionsTabSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).BeginInit();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelClient
            // 
            this.PanelClient.Controls.Add(this.PageControlOptions);
            // 
            // btnYes
            // 
            this.btnYes.Image = global::Logica.Reports.WbW.Properties.Resources.ok;
            // 
            // PageControlOptions
            // 
            this.PageControlOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageControlOptions.Location = new System.Drawing.Point(0, 0);
            this.PageControlOptions.Name = "PageControlOptions";
            this.PageControlOptions.SelectedTabPage = this.OptionsTabSheet;
            this.PageControlOptions.Size = new System.Drawing.Size(369, 419);
            this.PageControlOptions.TabIndex = 0;
            this.PageControlOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.OptionsTabSheet});
            // 
            // OptionsTabSheet
            // 
            this.OptionsTabSheet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.OptionsTabSheet.Controls.Add(this.OptionsPanel);
            this.OptionsTabSheet.Name = "OptionsTabSheet";
            this.OptionsTabSheet.Size = new System.Drawing.Size(365, 395);
            this.OptionsTabSheet.Text = "��������";
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Controls.Add(this.label1);
            this.OptionsPanel.Controls.Add(this.grpOptionsReport);
            this.OptionsPanel.Controls.Add(this.edReportName);
            this.OptionsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionsPanel.Location = new System.Drawing.Point(0, 0);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(365, 395);
            this.OptionsPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "�����:";
            // 
            // grpOptionsReport
            // 
            this.grpOptionsReport.Location = new System.Drawing.Point(8, 121);
            this.grpOptionsReport.Name = "grpOptionsReport";
            this.grpOptionsReport.Size = new System.Drawing.Size(345, 255);
            this.grpOptionsReport.TabIndex = 1;
            this.grpOptionsReport.Text = " ����� ������: ";
            // 
            // edReportName
            // 
            this.edReportName.Location = new System.Drawing.Point(8, 20);
            this.edReportName.Name = "edReportName";
            this.edReportName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.edReportName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edReportName.Properties.Appearance.Options.UseBackColor = true;
            this.edReportName.Properties.Appearance.Options.UseFont = true;
            this.edReportName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.edReportName.Properties.ReadOnly = true;
            this.edReportName.Size = new System.Drawing.Size(344, 22);
            this.edReportName.TabIndex = 0;
            this.edReportName.TabStop = false;
            // 
            // CustomInputReportDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(369, 450);
            this.Name = "CustomInputReportDataForm";
            this.Text = "�����";
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).EndInit();
            this.PanelClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).EndInit();
            this.PageControlOptions.ResumeLayout(false);
            this.OptionsTabSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).EndInit();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraTab.XtraTabControl PageControlOptions;
        public DevExpress.XtraTab.XtraTabPage OptionsTabSheet;
        public DevExpress.XtraEditors.PanelControl OptionsPanel;
        public DevExpress.XtraEditors.GroupControl grpOptionsReport;
        private System.Windows.Forms.Label label1;
        protected DevExpress.XtraEditors.TextEdit edReportName;
    }
}
