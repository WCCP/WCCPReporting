namespace WccpReporting
{
    partial class MktOverviewReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MktOverviewReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // MktOverviewReportScrollableControl
            // 
            this.MktOverviewReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MktOverviewReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.MktOverviewReportScrollableControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.MktOverviewReportScrollableControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MktOverviewReportScrollableControl.Name = "MktOverviewReportScrollableControl";
            this.MktOverviewReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.MktOverviewReportScrollableControl.TabIndex = 0;
            // 
            // MktOverviewReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MktOverviewReportScrollableControl);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "MktOverviewReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl MktOverviewReportScrollableControl;

    }
}
