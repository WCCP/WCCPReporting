namespace WccpReporting
{
    partial class MktOverviewBySegmentsReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MktOverviewBySegmentsReportScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // MktOverviewBySegmentsReportScrollableControl
            // 
            this.MktOverviewBySegmentsReportScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MktOverviewBySegmentsReportScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.MktOverviewBySegmentsReportScrollableControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.MktOverviewBySegmentsReportScrollableControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MktOverviewBySegmentsReportScrollableControl.Name = "MktOverviewBySegmentsReportScrollableControl";
            this.MktOverviewBySegmentsReportScrollableControl.Size = new System.Drawing.Size(640, 480);
            this.MktOverviewBySegmentsReportScrollableControl.TabIndex = 0;
            // 
            // MktOverviewBySegmentsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MktOverviewBySegmentsReportScrollableControl);
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "MktOverviewBySegmentsReport";
            this.Size = new System.Drawing.Size(640, 480);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.XtraScrollableControl MktOverviewBySegmentsReportScrollableControl;

    }
}
