using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.OleDb;
using DevExpress.XtraGrid.Views.Grid;

namespace WccpReporting
{
    public partial class SKURelationsContext : DevExpress.XtraEditors.XtraUserControl
    {
        public SKURelationsContext()
        {
            InitializeComponent();
        }

        //���������� ������ ������
        public void ShowData(string channel, int WaveId, int NumBrands)
        {
            try
            {
                DataTable dataDataView = new DataTable();

                using (OleDbCommand cmdDW_TomasResearch_WbW_SKU_Relations = new OleDbCommand((channel == "On-trade" ? "DW_TomasResearch_WbW_SKU_Relations_ONTRADE" : "DW_TomasResearch_WbW_SKU_Relations"), WccpUIControl.Conn))
                {
                    cmdDW_TomasResearch_WbW_SKU_Relations.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdDW_TomasResearch_WbW_SKU_Relations.Parameters.AddWithValue("@Wave_ID", WaveId);
                    cmdDW_TomasResearch_WbW_SKU_Relations.Parameters.AddWithValue("@Brands", NumBrands);
                    cmdDW_TomasResearch_WbW_SKU_Relations.CommandType = CommandType.StoredProcedure;

                    using (OleDbDataAdapter da = new OleDbDataAdapter(cmdDW_TomasResearch_WbW_SKU_Relations))
                    {
                        da.Fill(dataDataView);
                    };
                };

                if (dataDataView.Rows.Count > 0)
                {
                    gcTableContext.DataSource = dataDataView.DefaultView;
                    gvTableContext.BestFitColumns();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateReportData + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        //������� ��� ������� ���
        private void gvTableContext_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                string isBasic = View.GetRowCellDisplayText(e.RowHandle, View.Columns["isBasic"]);
                if (isBasic == "1")
                {
                    e.Appearance.BackColor = Color.Silver;
                    e.Appearance.Font = new Font("Tahoma", 8, FontStyle.Bold);
                }
            }
        }
    }
}
