﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.M2FSMReport;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        M2FSMSettingsForm setForm;
        public WccpUIControl() : base(113)
        {
            InitializeComponent();
            setForm = new M2FSMSettingsForm(this);
            if (setForm.ShowDialog() == DialogResult.OK)
                UpdateSheets(setForm.SheetParamsList);
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
        }

        void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
                UpdateSheets(setForm.SheetParamsList);
        }
    }
}
