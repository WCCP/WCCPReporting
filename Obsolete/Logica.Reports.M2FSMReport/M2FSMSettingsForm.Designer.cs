﻿namespace Logica.Reports.M2FSMReport
{
    partial class M2FSMSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(M2FSMSettingsForm));
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.tcOptions = new DevExpress.XtraTab.XtraTabControl();
            this.pgAction = new DevExpress.XtraTab.XtraTabPage();
            this.OptionsPanel = new DevExpress.XtraEditors.PanelControl();
            this.cbM2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblM2 = new DevExpress.XtraEditors.LabelControl();
            this.clbReports = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.lbDate = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.rbType = new DevExpress.XtraEditors.RadioGroup();
            this.gcInterval = new DevExpress.XtraEditors.GroupControl();
            this.rbFactType = new DevExpress.XtraEditors.RadioGroup();
            this.lblReport = new DevExpress.XtraEditors.LabelControl();
            this.txtReportName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).BeginInit();
            this.tcOptions.SuspendLayout();
            this.pgAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).BeginInit();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcInterval)).BeginInit();
            this.gcInterval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbFactType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHelp
            // 
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(285, 420);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 10;
            this.btnHelp.Text = "&Справка";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(204, 420);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 12;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(123, 420);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 11;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // tcOptions
            // 
            this.tcOptions.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.tcOptions.Appearance.Options.UseBackColor = true;
            this.tcOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcOptions.Location = new System.Drawing.Point(0, 0);
            this.tcOptions.Name = "tcOptions";
            this.tcOptions.SelectedTabPage = this.pgAction;
            this.tcOptions.Size = new System.Drawing.Size(365, 419);
            this.tcOptions.TabIndex = 13;
            this.tcOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgAction});
            // 
            // pgAction
            // 
            this.pgAction.Controls.Add(this.OptionsPanel);
            this.pgAction.Name = "pgAction";
            this.pgAction.Size = new System.Drawing.Size(358, 390);
            this.pgAction.Text = "Действие";
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.OptionsPanel.Appearance.Options.UseBackColor = true;
            this.OptionsPanel.Controls.Add(this.cbM2);
            this.OptionsPanel.Controls.Add(this.lblM2);
            this.OptionsPanel.Controls.Add(this.clbReports);
            this.OptionsPanel.Controls.Add(this.dateEdit);
            this.OptionsPanel.Controls.Add(this.lbDate);
            this.OptionsPanel.Controls.Add(this.groupControl3);
            this.OptionsPanel.Controls.Add(this.gcInterval);
            this.OptionsPanel.Controls.Add(this.lblReport);
            this.OptionsPanel.Controls.Add(this.txtReportName);
            this.OptionsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionsPanel.Location = new System.Drawing.Point(0, 0);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(358, 390);
            this.OptionsPanel.TabIndex = 0;
            // 
            // cbM2
            // 
            this.cbM2.Location = new System.Drawing.Point(56, 70);
            this.cbM2.Name = "cbM2";
            this.cbM2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbM2.Size = new System.Drawing.Size(291, 20);
            this.cbM2.TabIndex = 17;
            // 
            // lblM2
            // 
            this.lblM2.Location = new System.Drawing.Point(12, 73);
            this.lblM2.Name = "lblM2";
            this.lblM2.Size = new System.Drawing.Size(18, 13);
            this.lblM2.TabIndex = 16;
            this.lblM2.Text = "М2:";
            // 
            // clbReports
            // 
            this.clbReports.CheckOnClick = true;
            this.clbReports.Location = new System.Drawing.Point(10, 151);
            this.clbReports.Name = "clbReports";
            this.clbReports.Size = new System.Drawing.Size(339, 177);
            this.clbReports.TabIndex = 15;
            this.clbReports.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.clbReports_ItemCheck);
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = null;
            this.dateEdit.Location = new System.Drawing.Point(56, 45);
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit.Size = new System.Drawing.Size(100, 20);
            this.dateEdit.TabIndex = 14;
            // 
            // lbDate
            // 
            this.lbDate.Location = new System.Drawing.Point(12, 48);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(26, 13);
            this.lbDate.TabIndex = 13;
            this.lbDate.Text = "Дата";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.rbType);
            this.groupControl3.Location = new System.Drawing.Point(10, 97);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(339, 48);
            this.groupControl3.TabIndex = 12;
            this.groupControl3.Text = "Тип отчета:";
            // 
            // rbType
            // 
            this.rbType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbType.Location = new System.Drawing.Point(2, 22);
            this.rbType.Name = "rbType";
            this.rbType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Ввод планов"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Получение фактов")});
            this.rbType.Size = new System.Drawing.Size(335, 24);
            this.rbType.TabIndex = 10;
            this.rbType.SelectedIndexChanged += new System.EventHandler(this.rbType_SelectedIndexChanged);
            // 
            // gcInterval
            // 
            this.gcInterval.Appearance.BackColor = System.Drawing.Color.White;
            this.gcInterval.Appearance.BackColor2 = System.Drawing.Color.White;
            this.gcInterval.Appearance.Options.UseBackColor = true;
            this.gcInterval.Controls.Add(this.rbFactType);
            this.gcInterval.Enabled = false;
            this.gcInterval.Location = new System.Drawing.Point(10, 334);
            this.gcInterval.Name = "gcInterval";
            this.gcInterval.Size = new System.Drawing.Size(339, 49);
            this.gcInterval.TabIndex = 10;
            this.gcInterval.Text = "Данные на:";
            // 
            // rbFactType
            // 
            this.rbFactType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbFactType.Location = new System.Drawing.Point(2, 22);
            this.rbFactType.Name = "rbFactType";
            this.rbFactType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Утро"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Вечер")});
            this.rbFactType.Size = new System.Drawing.Size(335, 25);
            this.rbFactType.TabIndex = 10;
            // 
            // lblReport
            // 
            this.lblReport.Location = new System.Drawing.Point(8, 3);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(36, 13);
            this.lblReport.TabIndex = 2;
            this.lblReport.Text = "Отчет:";
            // 
            // txtReportName
            // 
            this.txtReportName.EditValue = "";
            this.txtReportName.Enabled = false;
            this.txtReportName.Location = new System.Drawing.Point(11, 21);
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtReportName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtReportName.Properties.Appearance.Options.UseBackColor = true;
            this.txtReportName.Properties.Appearance.Options.UseFont = true;
            this.txtReportName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.txtReportName.Properties.ReadOnly = true;
            this.txtReportName.Size = new System.Drawing.Size(341, 22);
            this.txtReportName.TabIndex = 0;
            this.txtReportName.TabStop = false;
            // 
            // M2FSMSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 446);
            this.Controls.Add(this.tcOptions);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "M2FSMSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отчёт";
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).EndInit();
            this.tcOptions.ResumeLayout(false);
            this.pgAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).EndInit();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcInterval)).EndInit();
            this.gcInterval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbFactType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraTab.XtraTabControl tcOptions;
        private DevExpress.XtraTab.XtraTabPage pgAction;
        private DevExpress.XtraEditors.PanelControl OptionsPanel;
        private DevExpress.XtraEditors.TextEdit txtReportName;
        private DevExpress.XtraEditors.LabelControl lblReport;
        private DevExpress.XtraEditors.GroupControl gcInterval;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.RadioGroup rbType;
        private DevExpress.XtraEditors.RadioGroup rbFactType;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.LabelControl lbDate;
        private DevExpress.XtraEditors.CheckedListBoxControl clbReports;
        private DevExpress.XtraEditors.ComboBoxEdit cbM2;
        private DevExpress.XtraEditors.LabelControl lblM2;
    }
}