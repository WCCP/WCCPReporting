﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraEditors;
using Logica.Reports.DataAccess;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.ConfigXmlParser.Model;
using System.Data.SqlClient;

namespace Logica.Reports.M2FSMReport
{
    public partial class M2FSMSettingsForm : XtraForm
    {
        private List<Guid> lstIsEvening = new List<Guid>()
        {
            new Guid("4925b6c6-ca67-4817-a8b9-c4b1ce12b9b3"),
            new Guid("08faf224-4d43-4234-a127-0636f4683f58"),
            new Guid("6bfee8c4-b033-49a0-aebe-ffa9249b3b89"),
            new Guid("66f0b2de-a5f4-47b7-ba54-b81ad9880b93")
        };
        DataTable dtSuper;
        DataTable dataTableDate;

        BaseReportUserControl bruc;
        public M2FSMSettingsForm(BaseReportUserControl parent)
        {

            InitializeComponent();
            dateEdit.DateTime = DateTime.Now;
            bruc = parent;
            txtReportName.Text = Resource.FSMReportHeader;
            dtSuper = DataAccessLayer.ExecuteStoredProcedure("spDW_SupervisorsGetList", new SqlParameter[] { new SqlParameter("@ReportId", parent.ReportId) }).Tables[0];
            FillM2();
            BuildReportList(TableType.Plan);

        }
        private void BuildReportList(TableType type)
        {
            clbReports.Items.Clear();
            foreach (Tab tab in bruc.Report.Tabs)
            {
                foreach(Table tbl in tab.Tables)
                {
                    if (tbl.Type == type)
                    {
                        clbReports.Items.Add(tab.Id, tab.Header);
                        break;
                    }
                }
            }
        }
        private void FillM2()
        {
            cbM2.Properties.Items.Clear();
            //cbM2.Properties.Items.Add(Resource.All);
            foreach (DataRow dr in dtSuper.Rows)
                cbM2.Properties.Items.Add(new ComboBoxItem() { Id = dr["Supervisor_id"], Text = dr["Supervisor_name"].ToString() });
            cbM2.SelectedIndex = 0;
        }

        public List<SheetParamCollection> SheetParamsList
        {
            get 
            {
                dataTableDate = DataAccessLayer.ExecuteStoredProcedure("spDW_WorkingDays", new SqlParameter[] { new SqlParameter("@Date", dateEdit.DateTime) }).Tables[0];

                List<SheetParamCollection> lst = new List<SheetParamCollection>();
                foreach (CheckedListBoxItem item in clbReports.CheckedItems)
                {
                    SheetParamCollection pars = new SheetParamCollection();
                    pars.MainHeader = Resource.MainHeader;// "Параметры";
                    pars.TabId = (Guid)item.Value;
                    pars.TableDataType = rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact;
                    pars.Add(new SheetParam() { DisplayParamName = Resource.M2, SqlParamName = "@Supervisor_id", Value = ((ComboBoxItem)cbM2.SelectedItem).Id, DisplayValue = ((ComboBoxItem)cbM2.SelectedItem).Text });
                    pars.Add(new SheetParam() { DisplayParamName = Resource.ReportDay, DisplayValue = Convert.ToDateTime(dataTableDate.Rows[0]["CurrentDate"]).ToShortDateString() });
                    pars.Add(new SheetParam() { DisplayParamName = Resource.ReportMounth, DisplayValue = Convert.ToDateTime(dataTableDate.Rows[0]["CurrentDate"]).ToString("MM yyyy") });
                    pars.Add(new SheetParam() { DisplayParamName = Resource.WorkDays, DisplayValue = dataTableDate.Rows[0]["WorkDaysInMonth"] });
                    pars.Add(new SheetParam() { DisplayParamName = Resource.WorkDay, DisplayValue = dataTableDate.Rows[0]["WorkDay"] });

                    pars.Add(new SheetParam() { DisplayParamName = "Дата:", SqlParamName = "@Date", Value = dateEdit.DateTime.Date, DisplayValue = dateEdit.DateTime.ToShortDateString() });
                    if(lstIsEvening.Contains(pars.TabId))
                        pars.Add(new SheetParam() { SqlParamName = "@isEvening", Value = rbFactType.SelectedIndex });
                    lst.Add(pars);
                }
                return lst;
            }
        }

        private void clbReports_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            foreach (CheckedListBoxItem item in clbReports.CheckedItems)
            {
                Guid selID = (Guid)item.Value;
                if (lstIsEvening.Contains(selID))
                {
                    gcInterval.Enabled = true;
                    return;
                }
            }
            gcInterval.Enabled = false;
        }

        private void rbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildReportList(rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact);
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
    internal class ComboBoxItem
    {
        object id;
        public object Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public override string ToString()
        {
            return Text;
        }
    }

}
