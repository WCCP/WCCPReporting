﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Services;
using Logica.LDBSync.WebServiceLogic.Interaction.Operations;
using Logica.LDBSync.WebServiceLogic.Interaction.Request;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebService
{
    /// <summary>
    /// Summary description for UpdateService
    /// </summary>
    [WebService(Namespace = "http://ABInBev.org/LDBSyncWebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class UpdateService : System.Web.Services.WebService
    {
        [WebMethod]
        public GetModuleResponse GetModule(GetModuleRequest request)
        {
            GetModule handler = new GetModule();
            return handler.Execute(() => handler.Processor(request));
        }

        [WebMethod]
        public GetModuleResponse GetModuleWithPath(GetModuleRequest request, string pathToDirectory)
        {
            GetModule handler = new GetModule();
            return handler.Execute(() => handler.Processor(request, pathToDirectory));
        }

        [WebMethod]
        public FileVersionResponse GetModuleVersion(GetModuleRequest request)
        {
            GetModuleVersion handler = new GetModuleVersion();
            return handler.Execute(() => handler.Processor(request));
        }

        [WebMethod]
        public FileVersionResponse GetModuleVersionWithPath(GetModuleRequest request, string pathToDirectory)
        {
            GetModuleVersion handler = new GetModuleVersion();
            return handler.Execute(() => handler.Processor(request, pathToDirectory));
        }

        [WebMethod]
        public FileHashResponse GetFileHash(GetModuleRequest request)
        {
            GetFileHash handler = new GetFileHash();
            return handler.Execute(() => handler.Processor(request));
        }

        [WebMethod]
        public FileListResponse GetApplicationFileList()
        {
            GetApplicationFileList handler = new GetApplicationFileList();
            return handler.Execute(() => handler.Processor());
        }

        [WebMethod]
        public FileListResponse GetUpdaterFileList()
        {
            GetUpdaterFileList handler = new GetUpdaterFileList();
            return handler.Execute(() => handler.Processor());
        }

        [WebMethod]
        public FileListResponse GetHostBaseFileList()
        {
            GetUpdaterFileList handler = new GetUpdaterFileList();
            return handler.Execute(() => handler.HostFileProcessor());
        }

    }
}
