﻿using System.ComponentModel;
using System.Web.Services;
using Logica.LDBSync.WebServiceLogic.Interaction.Operations;
using Logica.LDBSync.WebServiceLogic.Interaction.Request;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebService
{
    /// <summary>
    /// Summary description for LoggingService
    /// </summary>
    [WebService(Namespace = "http://ABInBev.org/LDBSyncWebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class LoggingService : System.Web.Services.WebService
    {
        [WebMethod]
        public LogLDBSyncStatusResponse LogLDBSyncStatus(LogLDBSyncStatusRequest request)
        {
            string ip = Context.Request.UserHostAddress; 

            LogLDBSyncStatus logLDBSyncStatus = new LogLDBSyncStatus();
            return logLDBSyncStatus.Execute(() => logLDBSyncStatus.Processor(request, ip));
        }
    }
}