﻿using System.Data;
using System.Data.SqlClient;

namespace Logica.LDBSync.WebServiceLogic {
    internal static class DataLayer {
        private const int DEFAULT_TIMEOUT = 900;

        public static DataSet ExecuteStoredProcedure(string procedureName, SqlParameter[] parameters,
                                                     int timeout = DEFAULT_TIMEOUT) {
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.SalesWorksConnection)) {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = timeout;
                command.Parameters.AddRange(parameters);
                DataSet result = new DataSet();
                new SqlDataAdapter(command).Fill(result);
                return result;
            }
        }

        public static object ExecuteScalarStoredProcedure(string procedureName, SqlParameter[] parameters,
                                                          int timeout = DEFAULT_TIMEOUT) {
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.SalesWorksConnection)) {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = timeout;
                command.Parameters.AddRange(parameters);
                return command.ExecuteScalar();
            }
        }

        public static void ExecuteNonQueryStoredProcedure(string procedureName, SqlParameter[] parameters,
                                                          int timeout = DEFAULT_TIMEOUT) {
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.SalesWorksConnection)) {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = timeout;
                command.Parameters.AddRange(parameters);
                command.ExecuteNonQuery();
            }
        }
    }
}