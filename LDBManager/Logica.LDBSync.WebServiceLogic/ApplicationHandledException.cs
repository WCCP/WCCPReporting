﻿using System;
using System.Runtime.Serialization;

namespace Logica.LDBSync.WebServiceLogic {
    public class ApplicationHandledException : ApplicationException {
        public ApplicationHandledException() {
        }

        public ApplicationHandledException(int code, string message) : base(message) {
            Code = code;
        }

        public ApplicationHandledException(int code, string message, Exception innerException)
            : base(message, innerException) {
            Code = code;
        }

        protected ApplicationHandledException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }

        public int Code { get; set; }
    }
}