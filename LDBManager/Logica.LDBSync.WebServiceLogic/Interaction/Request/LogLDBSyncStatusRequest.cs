﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Request {
    [Serializable]
    public class LogLDBSyncStatusRequest : RequestBase {
        public string MachineName { get; set; }
        public int SyncStatus { get; set; }
        public string ErrorMessage { get; set; }
        public string LDBVersion { get; set; }
    }
}