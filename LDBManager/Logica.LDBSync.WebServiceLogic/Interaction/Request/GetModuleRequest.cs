﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Request {
    [Serializable]
    public class GetModuleRequest : RequestBase {
        public string ModuleName { get; set; }
    }
}