﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Response {
    /// <summary>
    /// Store information about file name and version
    /// If file is executable, FileVersion is a version of file
    /// if file is not executable, FileVersion is a hash of file
    /// </summary>
    public struct UpdateFileInfo {
        public string FileName;
        public string FileVersion;
    }

    [Serializable]
    public class FileListResponse : ResponseBase {
        public UpdateFileInfo[] FileList { get; set; }
    }
}