﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Response {
    [Serializable]
    public class FileVersionResponse : ResponseBase {
        public string FileVersion { get; set; }
    }
}