﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Response {
    [Serializable]
    public class ResponseBase {
        public ResponseBase() {
            ErrorCode = 0;
            ErrorMessage = null;
        }

        public int ErrorCode { get; set; }
        public String ErrorMessage { get; set; }
    }
}