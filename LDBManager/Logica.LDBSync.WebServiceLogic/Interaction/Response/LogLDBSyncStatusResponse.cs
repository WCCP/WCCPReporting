﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Response {
    [Serializable]
    public class LogLDBSyncStatusResponse : ResponseBase {
        public int RecordID { get; set; }
    }
}