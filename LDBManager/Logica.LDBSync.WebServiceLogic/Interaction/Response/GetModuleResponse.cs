﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Response {
    [Serializable]
    public class GetModuleResponse : ResponseBase {
        public byte[] ModuleContent { get; set; }
    }
}