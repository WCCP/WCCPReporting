﻿using System;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Response {
    [Serializable]
    public class FileHashResponse : ResponseBase {
        public int FileHash { get; set; }
    }
}