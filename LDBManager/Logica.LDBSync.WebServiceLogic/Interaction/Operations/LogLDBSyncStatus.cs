﻿using System.Data.SqlClient;
using Logica.LDBSync.WebServiceLogic.Interaction.Request;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;
using Logica.LDBSync.WebServiceLogic.Properties;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Operations {
    public class LogLDBSyncStatus : OperationBase {
        public LogLDBSyncStatusResponse Processor(LogLDBSyncStatusRequest request, string ip) {
            DataLayer.ExecuteScalarStoredProcedure(Resources.SQL_ReplLog_Insert,
                new[] {
                    new SqlParameter(Resources.SQL_ReplLog_Insert_PRM_1, request.MachineName),
                    new SqlParameter(Resources.SQL_ReplLog_Insert_PRM_2, request.SyncStatus),
                    new SqlParameter(Resources.SQL_ReplLog_Insert_PRM_3, request.ErrorMessage),
                    new SqlParameter(Resources.SQL_ReplLog_Insert_PRM_4, ip),
                    new SqlParameter(Resources.SQL_ReplLog_Insert_PRM_5, request.LDBVersion)
                });
            return new LogLDBSyncStatusResponse();
        }
    }
}