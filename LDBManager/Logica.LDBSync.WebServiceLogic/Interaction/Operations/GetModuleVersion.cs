﻿using System.Configuration;
using System.Diagnostics;
using System.IO;
using Logica.LDBSync.WebServiceLogic.Interaction.Request;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Operations
{
    public class GetModuleVersion : OperationBase
    {
        public FileVersionResponse Processor(GetModuleRequest request)
        {
            return Processor(request, Properties.Settings.Default.UpdaterServiceFiles);
        }

        public FileVersionResponse Processor(GetModuleRequest request, string pathToDirectory)
        {
            string path = Path.Combine(pathToDirectory, request.ModuleName);

            if (File.Exists(path))
            {
                FileVersionResponse result;
                if (request.ModuleName.EndsWith(".exe.config"))
                {
                    var configVersion = GetConfigVersion(path);
                    result = new FileVersionResponse {FileVersion = configVersion};
                }
                else
                {
                    FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(path);
                    result = new FileVersionResponse {FileVersion = fvi.FileVersion};
                }
                return result;
            }

            throw new ApplicationHandledException(100, string.Format("File {0} not found", request.ModuleName));
        }


        private static string GetConfigVersion(string path)
        {
            var configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = path;

            var config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
            string configVersion = config.AppSettings.Settings["ConfigVersion"].Value;
            return configVersion;
        }
    }
}