﻿using System.IO;
using Logica.LDBSync.WebServiceLogic.Interaction.Request;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Operations
{
    public class GetModule : OperationBase
    {
        public GetModuleResponse Processor(GetModuleRequest request)
        {
            return Processor(request, Properties.Settings.Default.UpdaterServiceFiles);
        }

        public GetModuleResponse Processor(GetModuleRequest request, string pathToDirectory)
        {
            string path = Path.Combine(pathToDirectory, request.ModuleName);

            if (File.Exists(path))
            {
                return new GetModuleResponse { ModuleContent = File.ReadAllBytes(path) };
            }

            throw new ApplicationHandledException(100, string.Format("File {0} not found", request.ModuleName));
        }
    }
}