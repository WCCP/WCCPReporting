﻿using System.IO;
using Logica.LDBSync.WebServiceLogic.Interaction.Request;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Operations {
    public class GetFileHash : OperationBase {
        public FileHashResponse Processor(GetModuleRequest request) {
            string path = Path.Combine(Properties.Settings.Default.UpdaterServiceFiles, request.ModuleName);

            if (File.Exists(path)) {
                return new FileHashResponse() {FileHash = File.ReadAllBytes(path).GetHashCode()};
            }

            throw new ApplicationHandledException(100, string.Format("File {0} not found", request.ModuleName));
        }
    }
}