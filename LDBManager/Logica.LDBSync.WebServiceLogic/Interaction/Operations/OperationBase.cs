﻿using System;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Operations {
    public class OperationBase {
        public T Execute<T>(Func<T> processor)
            where T : ResponseBase, new() {
            try {
                return processor();
            }
            catch (ApplicationHandledException exception) {
                return new T {ErrorCode = exception.Code, ErrorMessage = exception.Message};
            }
            catch (Exception exception) {
                return new T {ErrorCode = -1, ErrorMessage = "Unknown error"};
            }
        }
    }
}