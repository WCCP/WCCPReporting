﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web.Configuration;
using Logica.LDBSync.WebServiceLogic.Interaction.Request;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Operations
{
    public class GetUpdaterFileList : OperationBase
    {
        public static int GetArrayHashCode(byte[] array)
        {
            unchecked
            {
                var result = 0;
                foreach (byte b in array)
                    result = (result * 31) ^ b;
                return result;
            }
        }

        public FileListResponse Processor()
        {
            // метод такой же как в GetApplicationFileList, копи-паст кода. Надо бы вынести в общего предка, но нет времени
            //return GetResponse("UpdaterNameTemplate");
            string currentDir = Properties.Settings.Default.UpdaterServiceFiles;

            string moduleNameTemplate = WebConfigurationManager.AppSettings["UpdaterNameTemplate"];
            if (string.IsNullOrEmpty(moduleNameTemplate))
            {
                throw new ApplicationHandledException(101,
                    "UpdaterNameTemplate key was not specified in web service config");
            }

            List<UpdateFileInfo> modules = new List<UpdateFileInfo>(5);

            int moduleIndex = 1;
            string moduleName = string.Empty;
            string moduleNameKey;
            string moduleVersion;
            string modulePath;
            string ext;

            while (true)
            {
                moduleNameKey = string.Format(moduleNameTemplate, moduleIndex);
                moduleName = WebConfigurationManager.AppSettings[moduleNameKey];

                if (string.IsNullOrEmpty(moduleName))
                    break;

                modulePath = Path.Combine(currentDir, moduleName);

                ext = Path.GetExtension(modulePath).ToUpper();
                try
                {
                    if (ext == ".EXE" || ext == ".DLL") //executable file
                        moduleVersion = FileVersionInfo.GetVersionInfo(modulePath).FileVersion;
                    else //not executable file
                        moduleVersion = GetArrayHashCode(File.ReadAllBytes(modulePath)).ToString();
                }
                catch (System.Exception ex)
                {
                    throw new ApplicationHandledException(100,
                        string.Format("Can't get file {0} version : {1} ", moduleName, ex.Message));
                }

                modules.Add(new UpdateFileInfo() { FileName = moduleName, FileVersion = moduleVersion });

                moduleIndex += 1;
            }

            return new FileListResponse() { FileList = modules.ToArray() };
        }


        public FileListResponse HostFileProcessor()
        {
            string lUpdatePath = WebConfigurationManager.AppSettings["HostUpdatePath"];
            GetModuleVersion lVersionProcessor = new GetModuleVersion();

            List<string> lBaseModules = new List<string>();

            lBaseModules.AddRange(WebConfigurationManager.AppSettings["BaseModules"].Split(new[] { ';', '\r', '\n', ' ' }, StringSplitOptions.RemoveEmptyEntries));
            lBaseModules.AddRange(WebConfigurationManager.AppSettings["DevExModules"].Split(new[] { ';', '\r', '\n', ' ' }, StringSplitOptions.RemoveEmptyEntries));

            List<UpdateFileInfo> modules = new List<UpdateFileInfo>();

            foreach (var module in lBaseModules)
            {
                FileVersionResponse lFileVersionResponse = lVersionProcessor.Processor(new GetModuleRequest { ModuleName = module }, lUpdatePath);
                modules.Add(new UpdateFileInfo { FileName = module, FileVersion = lFileVersionResponse.FileVersion });
            }
            
            return new FileListResponse { FileList = modules.ToArray() };
        }
    }
}