﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web.Configuration;
using Logica.LDBSync.WebServiceLogic.Interaction.Response;

namespace Logica.LDBSync.WebServiceLogic.Interaction.Operations {
    public class GetApplicationFileList : OperationBase {
        public static int GetArrayHashCode(byte[] array) {
            unchecked {
                int result = 0;
                foreach (byte b in array)
                    result = (result * 31) ^ b;
                return result;
            }
        }

        public FileListResponse Processor() {
            string currentDir = Properties.Settings.Default.UpdaterServiceFiles;

            string moduleNameTemplate = WebConfigurationManager.AppSettings["ModuleNameTemplate"];
            if (string.IsNullOrEmpty(moduleNameTemplate)) {
                throw new ApplicationHandledException(101,
                    "ModuleNameTemplate key was not specified in web service config");
            }

            List<UpdateFileInfo> modules = new List<UpdateFileInfo>(5);

            int moduleIndex = 1;
            string moduleName = string.Empty;
            string moduleNameKey;
            string moduleVersion;
            string modulePath;
            string ext;

            while (true) {
                moduleNameKey = string.Format(moduleNameTemplate, moduleIndex);
                moduleName = WebConfigurationManager.AppSettings[moduleNameKey];

                if (string.IsNullOrEmpty(moduleName))
                    break;

                modulePath = Path.Combine(currentDir, moduleName);

                ext = Path.GetExtension(modulePath).ToUpper();
                try {
                    if (ext == ".EXE" || ext == ".DLL") //executable file
                        moduleVersion = FileVersionInfo.GetVersionInfo(modulePath).FileVersion;
                    else //not executable file
                        moduleVersion = GetArrayHashCode(File.ReadAllBytes(modulePath)).ToString();
                }
                catch (System.Exception ex) {
                    throw new ApplicationHandledException(100,
                        string.Format("Can't get file {0} version : {1} ", moduleName, ex.Message));
                }

                modules.Add(new UpdateFileInfo() {FileName = moduleName, FileVersion = moduleVersion});

                moduleIndex += 1;
            }

            return new FileListResponse() {FileList = modules.ToArray()};
        }
    }
}