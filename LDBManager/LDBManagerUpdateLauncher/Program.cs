﻿//#define _DEBUG
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace LDBManagerUpdateLauncher
{
    class Program
    {
        internal static bool updateOnly = false;

        static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler
              (
                (sender, e) =>
                {
                    if (e.IsTerminating)
                    {
                        object o = e.ExceptionObject;
                        string message = "Unknown error";
                        if (o != null)
                            message = o.ToString();
                        LogManager.WriteMessage(message, EventLogEntryType.Error);
                    }
                }
              );

            LogManager.WriteMessage(string.Format("LDBManagerUpdateLauncher version {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()));
            LogManager.WriteMessage("Starting update LDBSync....");
#if _DEBUG
            foreach(string arg in args)
                Console.Out.WriteLine(arg);
            Console.ReadLine();
#endif
            //parse command line
            foreach (string s in args)
            {
                switch (s.ToUpper())
                {
                    case "-UPDATEONLY": // update Logica.LDBSync.exe and exit
                        updateOnly = true;
                        break;
                }
            }

            using (Mutex mutex = new Mutex(false, "Global\\LDBSyncApp"))  // prevents to start more than one instance
            {
                bool locked = false;
                try
                {
                    locked = mutex.WaitOne(100, false);
                }
                catch (AbandonedMutexException)
                {
                    locked = true;
                }

                if (locked)
                {
                    // run update procedure
                    UpdateHelper helper = new UpdateHelper();
                    helper.RunUpdate();

                    // launch LDBSync
                    if (updateOnly)
                        return 0;

                    LogManager.WriteMessage("Running LDB Sync");
                    mutex.ReleaseMutex();

                    RunLDBManager(args);
                }
                else
                    LogManager.WriteMessage("LDBSync has been already started and can't be updated", EventLogEntryType.Error);
            }

            return 0;
        }

        private static void RunLDBManager(string[] args)
        {
            string ldbSyncFilePath = string.Format("{0}\\{1}", Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), ConfigManager.ModuleToRun);
            ProcessStartInfo psi = new ProcessStartInfo(ldbSyncFilePath);
            foreach (string arg in args)
                psi.Arguments += string.Format("{0} ", arg);
            psi.Arguments += string.Format("{0} {1}", "-CHKUPDT", "0");

            try
            {
                Process process = Process.Start(psi);
            }
            catch (Exception e)
            {
                LogManager.WriteMessage("Can't start process: " + e.Message, EventLogEntryType.Error);
            }
        }

    }

    /// <summary>
    /// Store information about module
    /// ModuleName = file name
    /// ModulePath = full name of the file, include path
    /// </summary>
    struct ModuleInfo
    {
        public string ModuleName;
        public string ModulePath;
    }
}
