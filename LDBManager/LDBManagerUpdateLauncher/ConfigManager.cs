﻿using System;
using System.Configuration;

namespace LDBManagerUpdateLauncher
{
    /// <summary>
    /// Get data from config file
    /// </summary>
    public static class ConfigManager
    {
        public static bool UpdateThruWS
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["UpdateThruWS"]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FTPURL
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPServer"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FTPUser
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPUser"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FTPPWD
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPPassword"];
            }
        }
        
        public static string ModuleToRun
        {
            get
            {
                return ConfigurationManager.AppSettings["ModuleToRun"];
            }
        }

    }
}
