﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Text;

namespace LDBManagerUpdateLauncher
{
    /// <summary>
    /// Write trace information into Event Viewer
    /// </summary>
    public static class LogManager
    {
        static EventLogEntryType? tp;
        static string sourceName = "LDBManagerUpdateLauncher";
        public static string LastAppicationException { get; set; }
    
        /// <summary>
        /// Get log level (filter for messages) 
        /// </summary>
        static LogManager()
        {
            try
            {
                string logLevel = ConfigurationManager.AppSettings["LogLevel"];
                tp = null;
                if (String.IsNullOrEmpty(logLevel))
                    return;
                string[] levs = logLevel.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string lev in levs)
                {
                    EventLogEntryType newType = (EventLogEntryType)Enum.Parse(typeof(EventLogEntryType), lev, true);
                    if (!tp.HasValue)
                        tp = newType;
                    else
                        tp |= newType;
                }
            }
            catch
            {
                tp = EventLogEntryType.Error | EventLogEntryType.Warning | EventLogEntryType.Information | EventLogEntryType.FailureAudit | EventLogEntryType.SuccessAudit;
            }
        
        }
        /// <summary>
        /// Write message into EventViewer
        /// </summary>
        /// <param name="message">message text</param>
        public static void WriteMessage(string message)
        {
            WriteMessage(message, EventLogEntryType.Information);
        }
        /// <summary>
        /// Write message into EventViewer
        /// </summary>
        /// <param name="message">message text</param>
        /// <param name="type">error type</param>
        public static void WriteMessage(string message, EventLogEntryType type)
        {
            WriteMessage(message, null, type);
        }
        /// <summary>
        /// Write message into EventViewer
        /// </summary>
        /// <param name="message">message text</param>
        /// <param name="type">error type</param>
        /// <param name="ex">exception</param>
        public static void WriteMessage(string message, Exception ex, EventLogEntryType type)
        {
            Console.WriteLine(message);

            try
            {
                if ((tp & type) == 0)
                    return;
                if (ex != null)
                    message += "\r\n" + FormatExceptionMessage(ex);
                EventLog elog = new EventLog();
                elog.Source = sourceName;
                elog.WriteEntry(message, type);
                elog.Close();

                if(type == EventLogEntryType.Error)
                {
                    LastAppicationException = message;
                }
            }
            catch { }
        }
        /// <summary>
        /// Convert exception to text
        /// </summary>
        /// <param name="ex">exception to convert</param>
        /// <returns>exception in text view</returns>
        public static string FormatExceptionMessage(Exception ex)
        {
            StringBuilder result = new StringBuilder();
            while (ex != null)
            {
                result.Append(string.Format("Exception type: {0}\r\n Exception message: {1}\r\n\r\n", ex.GetType().Name, ex.Message));
                result.Append(string.Format("\r\n{0}\r\n\r\n", ex.StackTrace));
                ex = ex.InnerException;
            }
            return result.ToString();
        }
    }
}

