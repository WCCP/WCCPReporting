﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using LDBManagerUpdateLauncher.UpdaterServiceReference;

namespace LDBManagerUpdateLauncher
{
    internal class UpdateHelper
    {
        private const string fileSuffix = ".tmp";
        private string localDirectoryPath = null;
        UpdateServiceSoapClient service = new UpdateServiceSoapClient();
        
        public UpdateHelper()
        {
            localDirectoryPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        public void RunUpdate()
        {
            List<ModuleInfo> appList = CheckApplicationUpdates();

            if (appList == null)
            {
                LogManager.WriteMessage("Error on application update");
                return;
            }

            if (appList.Count == 0)
            {
                LogManager.WriteMessage("Application is up to date");
                return;
            }

            foreach(ModuleInfo module in appList)
            {
                LogManager.WriteMessage(string.Format("Updating {0}", module.ModuleName));
                if (UpdateModule(module.ModuleName))
                    LogManager.WriteMessage(string.Format("Module {0} updated", module.ModuleName));
                //System.Threading.Thread.Sleep(10);
            }
        }

        private List<ModuleInfo> CheckApplicationUpdates()
        {
            FileListResponse response = service.GetApplicationFileList();
            if (response.ErrorCode != 0)
            {
                LogManager.WriteMessage(String.Format("Can't get file list from update service: \n{0}", response.ErrorMessage));
                //throw new ApplicationException(response.ErrorMessage);
                return null;
            }

            return CheckUpdates(response.FileList);
        }


        public int GetArrayHashCode(byte[] array)
        {
            unchecked
            {
                var result = 0;
                foreach (byte b in array)
                    result = (result * 31) ^ b;
                return result;
            }
        }

        private List<ModuleInfo> CheckUpdates(UpdateFileInfo[] files)
        {
            string local_version;
            string ext;
            string filePath;

            List<ModuleInfo> modulesToUpdate = new List<ModuleInfo>(3);

            foreach (UpdateFileInfo file in files)
            {
                filePath = Path.Combine(localDirectoryPath, file.FileName);
                ext = Path.GetExtension(filePath).ToUpper();
                try
                {
                    if (ext == ".EXE" || ext == ".DLL") //executable file
                        local_version = FileVersionInfo.GetVersionInfo(filePath).FileVersion;
                    else //not executable file
                        local_version = GetArrayHashCode(File.ReadAllBytes(filePath)).ToString();
                }
                catch (Exception ex)
                {
                    LogManager.WriteMessage(String.Format("Can't get file {0} version : {1} ", file.FileName, ex.Message));
                    //throw new ApplicationException();
                    local_version = "";
                }

                if (local_version != file.FileVersion)
                    modulesToUpdate.Add(new ModuleInfo() { ModuleName = file.FileName, ModulePath = filePath });
            }

            return modulesToUpdate;

        }

        private bool UpdateModule(string moduleName)
        {
            bool res = false;

            string tmpFile = moduleName + fileSuffix;

            string tmpDir = Path.Combine(localDirectoryPath, "Temp");
            string origDir = Path.Combine(localDirectoryPath, "Original");

            try
            {
                Directory.CreateDirectory(tmpDir);
                Directory.CreateDirectory(origDir);
            }
            catch (Exception ex)
            {
                LogManager.WriteMessage(String.Format("Can't create Temp and Orig folders: {0}", ex.Message));
                return false;
            }

            string fileName = Path.Combine(localDirectoryPath, moduleName);
            //copy current file to Orig folder
            try
            {
                if (File.Exists(fileName))
                    File.Copy(fileName, Path.Combine(origDir, moduleName), true);
            }
            catch (Exception ex)
            {
                LogManager.WriteMessage(String.Format("Can't copy file to Orig folder: {0}", ex.Message));
            }

            if (FtpManager.DownloadFile(moduleName, tmpDir))
            {
                try
                {
                    File.Copy(Path.Combine(tmpDir, moduleName), fileName, true);
                    res = true;
                }
                catch (Exception ex)
                {
                    LogManager.WriteMessage(String.Format("Can't replace file: {0}", ex.Message));
                }
            }

            return res;
        }
    }
}
