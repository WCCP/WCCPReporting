﻿using System;
using System.IO;
using System.Net;
using LDBManagerUpdateLauncher.UpdaterServiceReference;

namespace LDBManagerUpdateLauncher
{
    internal static class FtpManager
    {
        public static bool DownloadFile(string fileName, string directory)
        {
            bool result = true;
            string fullFileName = Path.Combine(directory, fileName);
            
            try
            {
                if (ConfigManager.UpdateThruWS)
                {
                    UpdateServiceSoapClient service = new UpdateServiceSoapClient();
                    var response = service.GetModule(new GetModuleRequest {ModuleName = fileName});
                    if (response.ErrorCode != 0)
                    {
                        throw new ApplicationException(response.ErrorMessage);
                    }
                    using (MemoryStream responseStream = new MemoryStream(response.ModuleContent))
                    {
                        SaveRecievedStream(responseStream, fullFileName);
                    }
                }
                else
                {
                    var request = WebRequest.Create(ConfigManager.FTPURL + "/" + fileName);
                    request.Credentials = new NetworkCredential(ConfigManager.FTPUser, ConfigManager.FTPPWD);
                    if (request is FtpWebRequest)
                    {
                        ((FtpWebRequest) request).UsePassive = false;
                        ((FtpWebRequest) request).UseBinary = true;
                        ((FtpWebRequest) request).KeepAlive = false;
                        request.Method = WebRequestMethods.Ftp.DownloadFile;
                    }
                    else if (request is HttpWebRequest)
                    {
                        request.Method = WebRequestMethods.Http.Get;
                    }

                    var response = request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    SaveRecievedStream(responseStream, fullFileName);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteMessage(ex.Message, System.Diagnostics.EventLogEntryType.Error);
                result = false;
            }
            
            return result;
        }

        private static void SaveRecievedStream(Stream responseStream, string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
            FileStream fs = new FileStream(fileName, FileMode.CreateNew);
            byte[] buffer = new byte[2047];

            int read = responseStream.Read(buffer, 0, buffer.Length);
            while (read > 0)
            {
                fs.Write(buffer, 0, read);
                read = responseStream.Read(buffer, 0, buffer.Length);
            }
            responseStream.Close();
            fs.Flush();
            fs.Close();
        }
    }
}
