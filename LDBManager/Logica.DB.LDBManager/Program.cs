﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Logica.LDBManager {
    internal static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args) {
            LogManager.WriteMessage("LDBManager started");
            Unpacker.CheckDependensyExistence();

            List<string> lDB = new List<string>();
            List<string> flds = new List<string>();
            List<string> pks = new List<string>();

            if (null != args) {
                CommandLineArgsProseccor.ProcessArgs(args);
            }
            Unpacker.GetEmbededSQLs();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DBComparer());
        }
    }
}