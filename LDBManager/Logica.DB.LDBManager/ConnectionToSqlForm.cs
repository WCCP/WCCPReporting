﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using Logica.DB.LDBProcessor;

namespace Logica.LDBManager
{
	public partial class ConnectionToSqlForm : Form
	{
		private List<string> prohibitedSrvNames = new List<string>(new string[]{"(local)",".", "localhost"});
		public ConnectionToSqlForm()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			CleanUpFormCntrols();
		}
		private void cmbSQLServersName_DropDown(object sender, EventArgs e)
		{
			if (null == cmbSQLServersName.DataSource)
			{
				this.Cursor = Cursors.WaitCursor;
				SqlDataSourceEnumerator srvs = SqlDataSourceEnumerator.Instance;
				cmbSQLServersName.SelectedIndex = -1;
				cmbSQLServersName.DataSource = SqlServerManager.Current.GetServerNames();
				cmbSQLServersName.SelectedIndex = -1;
				this.Cursor = Cursors.Default;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void cmbDataBases_DropDown(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(cmbSQLServersName.Text))
			{
				btnRefreshServerNames_Click(sender, e);
			}
		}

		private void btnTestConnection_Click(object sender, EventArgs e)
		{
			TestConection(true);
		}

		private void EnableOKButton(int correction)
		{
			btnOK.Enabled = !string.IsNullOrEmpty(cmbSQLServersName.Text) && (cklSWDatabases.CheckedItems.Count + correction) > 0;

		}

		private void cmbSQLServersName_SelectedIndexChanged(object sender, EventArgs e)
		{
			EnableOKButton(0);
		}

		private void cmbDataBases_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void AuthenticationChanged(object sender, EventArgs e)
		{
			lblUserName.Enabled =
				txtUserName.Enabled =
				txtPassword.Enabled =
				lblPassword.Enabled = rbSQLAuthentication.Checked;
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			bool result = false;

			if (cklSWDatabases.CheckedItems.Count > 0 && TestConection(false))
			{

				foreach (object item in cklSWDatabases.CheckedItems)
				{
					if (rbWindowsAuthentication.Checked)
					{
						SqlServerManager.Current.AddConnection(cmbSQLServersName.Text, item.ToString());

						//						result = SqlServerManager.Current.SetConnectionString(cmbSQLServersName.Text, cmbDataBases.Text);
					}
					else
					{
						SqlServerManager.Current.AddConnection(cmbSQLServersName.Text,
									item.ToString(),
									txtUserName.Text,
									txtPassword.Text);
					}
				}
				result = true;
			}
			if (!result)
			{
				MessageBox.Show("The selected parameters do not fit!\nPlease test connection before",
					Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			this.Close();
		}

		private bool TestConection(bool showMessageBox)
		{
			if (string.IsNullOrEmpty(cmbSQLServersName.Text))
			{
				if (showMessageBox)
					MessageBox.Show("The connection can not be tested beacuse no server name has specified!",
						Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

				return false;
			}
			this.Cursor = Cursors.WaitCursor;
			string errorMsg, userId, userPassword;
			userId = rbSQLAuthentication.Checked ? txtUserName.Text : null;
			userPassword = rbSQLAuthentication.Checked ? txtPassword.Text : null;
			bool result = false;
			errorMsg = string.Empty;

			foreach (object item in cklSWDatabases.CheckedItems)
			{
				string dbName = item.ToString();
				result = SqlServerManager.Current.CheckConnection(cmbSQLServersName.Text,
								dbName, userId, userPassword, out errorMsg);
				if (!result)
					break;
			}

			if (showMessageBox)
				if (result)
				{
					MessageBox.Show("Test conection succeeded!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show(errorMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}

			this.Cursor = Cursors.Default;
			return result;
		}

		private void btnRefreshServerNames_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			SqlDataSourceEnumerator srvs = SqlDataSourceEnumerator.Instance;
			cmbSQLServersName.SelectedIndex = -1;
			List<string> lst = SqlServerManager.Current.GetServerNames();
			cmbSQLServersName.DataSource = lst;
			cmbSQLServersName.SelectedIndex = -1;
			this.Cursor = Cursors.Default;
		}

		private void btnRefreshDatabas_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(cmbSQLServersName.Text))
			{
				MessageBox.Show("Databases can not be obtained beacuse no server name has specified!",
					Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

				return;
			}
			this.Cursor = Cursors.WaitCursor;
			string userId, userPass;
			userId = userPass = null;

			if (rbSQLAuthentication.Checked)
			{
				userId = txtUserName.Text;
				userPass = txtPassword.Text;
			}
			List<string> databases = SqlServerManager.Current.GetSWDatabases(cmbSQLServersName.Text, userId, userPass);

			cklSWDatabases.Items.Clear();
			if (null != databases)
			{
				cklSWDatabases.Items.AddRange(databases.ToArray());
				EnableOKButton(0);
			}

			//cklSWDatabases.CheckedItems

			for (int i = 0; i < cklSWDatabases.Items.Count; i++)
			{
				cklSWDatabases.SetItemChecked(i, true);
			}

			this.Cursor = Cursors.Default;
		}

		private void cmbSQLServersName_Leave(object sender, EventArgs e)
		{
			string srvname = cmbSQLServersName.Text.Trim();
			if (string.IsNullOrEmpty(srvname))
			{
				CleanUpFormCntrols();
				return;
			}
			btnRefreshDatabas_Click(sender, e);
		}

		private void cklSWDatabases_ItemCheck(object sender, ItemCheckEventArgs e)
		{
			int correction= (e.NewValue == CheckState.Unchecked) ? -1 : 1;
			EnableOKButton(correction);
		}
		private void CleanUpFormCntrols()
		{
			cmbSQLServersName.DataSource = null;
			cmbSQLServersName.Text = "";
			cklSWDatabases.Items.Clear();
			rbWindowsAuthentication.Checked = true;
			txtPassword.Text = "";
			txtUserName.Text = "";
			EnableOKButton(0);
		}
	}
}
