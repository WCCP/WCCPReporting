﻿namespace Logica.LDBManager
{
	partial class DBComparer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBComparer));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SQLServerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SWDataBaseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectDataBase = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnChangeConnection = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnOpenScriptFileFolder = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFolderName = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnOpenLogFileFolder = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLDBLogFolder = new System.Windows.Forms.TextBox();
            this.btnOpenLDBFolder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLDBFolder = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlPathToSQLscripts = new System.Windows.Forms.Panel();
            this.btnRecreateViews = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlPathToSQLscripts.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.btnChangeConnection);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(420, 139);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connected to:";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(337, 10);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(73, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SQLServerName,
            this.SWDataBaseName,
            this.SelectDataBase});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(9, 39);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(399, 90);
            this.dataGridView1.TabIndex = 2;
            // 
            // SQLServerName
            // 
            this.SQLServerName.DataPropertyName = "ServerName";
            this.SQLServerName.Frozen = true;
            this.SQLServerName.HeaderText = "Server name";
            this.SQLServerName.Name = "SQLServerName";
            this.SQLServerName.ReadOnly = true;
            this.SQLServerName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SQLServerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SWDataBaseName
            // 
            this.SWDataBaseName.DataPropertyName = "Database";
            this.SWDataBaseName.Frozen = true;
            this.SWDataBaseName.HeaderText = "SW Database name";
            this.SWDataBaseName.Name = "SWDataBaseName";
            this.SWDataBaseName.ReadOnly = true;
            this.SWDataBaseName.Width = 150;
            // 
            // SelectDataBase
            // 
            this.SelectDataBase.DataPropertyName = "LDBExists";
            this.SelectDataBase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectDataBase.Frozen = true;
            this.SelectDataBase.HeaderText = "LDB exists";
            this.SelectDataBase.Name = "SelectDataBase";
            this.SelectDataBase.ReadOnly = true;
            this.SelectDataBase.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // btnChangeConnection
            // 
            this.btnChangeConnection.Location = new System.Drawing.Point(260, 10);
            this.btnChangeConnection.Name = "btnChangeConnection";
            this.btnChangeConnection.Size = new System.Drawing.Size(67, 23);
            this.btnChangeConnection.TabIndex = 6;
            this.btnChangeConnection.Text = "Add";
            this.btnChangeConnection.UseVisualStyleBackColor = true;
            this.btnChangeConnection.Click += new System.EventHandler(this.btnChangeConnection_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnOpenScriptFileFolder);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtFolderName);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(420, 70);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Path to SQL scripts:";
            // 
            // btnOpenScriptFileFolder
            // 
            this.btnOpenScriptFileFolder.Location = new System.Drawing.Point(387, 32);
            this.btnOpenScriptFileFolder.Margin = new System.Windows.Forms.Padding(0);
            this.btnOpenScriptFileFolder.Name = "btnOpenScriptFileFolder";
            this.btnOpenScriptFileFolder.Size = new System.Drawing.Size(24, 24);
            this.btnOpenScriptFileFolder.TabIndex = 2;
            this.btnOpenScriptFileFolder.Text = "...";
            this.btnOpenScriptFileFolder.UseVisualStyleBackColor = true;
            this.btnOpenScriptFileFolder.Click += new System.EventHandler(this.btnOpenScriptFileFolder_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Source folder:";
            // 
            // txtFolderName
            // 
            this.txtFolderName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFolderName.Location = new System.Drawing.Point(19, 34);
            this.txtFolderName.Name = "txtFolderName";
            this.txtFolderName.ReadOnly = true;
            this.txtFolderName.Size = new System.Drawing.Size(365, 20);
            this.txtFolderName.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(255, 10);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(336, 10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnOpenLogFileFolder);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.txtLDBLogFolder);
            this.groupBox3.Controls.Add(this.btnOpenLDBFolder);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtLDBFolder);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 139);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(420, 102);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Path LDB files:";
            // 
            // btnOpenLogFileFolder
            // 
            this.btnOpenLogFileFolder.Location = new System.Drawing.Point(387, 72);
            this.btnOpenLogFileFolder.Margin = new System.Windows.Forms.Padding(0);
            this.btnOpenLogFileFolder.Name = "btnOpenLogFileFolder";
            this.btnOpenLogFileFolder.Size = new System.Drawing.Size(24, 24);
            this.btnOpenLogFileFolder.TabIndex = 5;
            this.btnOpenLogFileFolder.Text = "...";
            this.btnOpenLogFileFolder.UseVisualStyleBackColor = true;
            this.btnOpenLogFileFolder.Click += new System.EventHandler(this.btnOpenLogFileFolder_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Log folder:";
            // 
            // txtLDBLogFolder
            // 
            this.txtLDBLogFolder.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDBLogFolder.Location = new System.Drawing.Point(19, 74);
            this.txtLDBLogFolder.Name = "txtLDBLogFolder";
            this.txtLDBLogFolder.Size = new System.Drawing.Size(365, 20);
            this.txtLDBLogFolder.TabIndex = 3;
            // 
            // btnOpenLDBFolder
            // 
            this.btnOpenLDBFolder.Location = new System.Drawing.Point(387, 31);
            this.btnOpenLDBFolder.Margin = new System.Windows.Forms.Padding(0);
            this.btnOpenLDBFolder.Name = "btnOpenLDBFolder";
            this.btnOpenLDBFolder.Size = new System.Drawing.Size(24, 24);
            this.btnOpenLDBFolder.TabIndex = 2;
            this.btnOpenLDBFolder.Text = "...";
            this.btnOpenLDBFolder.UseVisualStyleBackColor = true;
            this.btnOpenLDBFolder.Click += new System.EventHandler(this.btnOpenLDBFolder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Database folder:";
            // 
            // txtLDBFolder
            // 
            this.txtLDBFolder.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDBFolder.Location = new System.Drawing.Point(19, 33);
            this.txtLDBFolder.Name = "txtLDBFolder";
            this.txtLDBFolder.Size = new System.Drawing.Size(366, 20);
            this.txtLDBFolder.TabIndex = 0;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(6, 7);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(42, 13);
            this.lblState.TabIndex = 4;
            this.lblState.Text = "lblState";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRecreateViews);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 241);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 42);
            this.panel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.lblState);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 283);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(420, 28);
            this.panel2.TabIndex = 6;
            // 
            // pnlPathToSQLscripts
            // 
            this.pnlPathToSQLscripts.Controls.Add(this.groupBox2);
            this.pnlPathToSQLscripts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPathToSQLscripts.Location = new System.Drawing.Point(0, 311);
            this.pnlPathToSQLscripts.Name = "pnlPathToSQLscripts";
            this.pnlPathToSQLscripts.Size = new System.Drawing.Size(420, 70);
            this.pnlPathToSQLscripts.TabIndex = 7;
            // 
            // btnRecreateViews
            // 
            this.btnRecreateViews.Enabled = false;
            this.btnRecreateViews.Location = new System.Drawing.Point(9, 10);
            this.btnRecreateViews.Name = "btnRecreateViews";
            this.btnRecreateViews.Size = new System.Drawing.Size(115, 23);
            this.btnRecreateViews.TabIndex = 4;
            this.btnRecreateViews.Text = "Recreate Views";
            this.btnRecreateViews.UseVisualStyleBackColor = true;
            this.btnRecreateViews.Click += new System.EventHandler(this.btnRecreateViews_Click);
            // 
            // DBComparer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 381);
            this.Controls.Add(this.pnlPathToSQLscripts);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DBComparer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DBComparer";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlPathToSQLscripts.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnChangeConnection;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFolderName;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SQLServerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SWDataBaseName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectDataBase;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnOpenScriptFileFolder;
        private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button btnOpenLDBFolder;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtLDBFolder;
		private System.Windows.Forms.Button btnOpenLogFileFolder;
		private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLDBLogFolder;
		private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlPathToSQLscripts;
        private System.Windows.Forms.Button btnRecreateViews;

	}
}