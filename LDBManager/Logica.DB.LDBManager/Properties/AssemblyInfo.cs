﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LDBManager")]
[assembly: AssemblyDescription("Create LDB or recreate views in LDB.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logica, SoftServe Inc.")]
[assembly: AssemblyProduct("LDBManager")]
[assembly: AssemblyCopyright("Copyright (c) 2009 Logica, 2010 - 2014 SoftServe Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("377af832-8f98-4102-85f4-78e2e41009ae")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.3.7")]
[assembly: AssemblyFileVersion("1.0.3.7")]
