﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Logica.DB.LDBProcessor;
using Logica.LDBManager.DataSets;
using Logica.LDBManager.Syncronization;

namespace Logica.LDBManager {
    public partial class DBComparer : Form {
        private ConnectionToSqlForm connectionForm = new ConnectionToSqlForm();
        private SWDatabases.SQLServerNamesDataTable _swDatatable = new SWDatabases.SQLServerNamesDataTable();

        private static string LBD_INSTALLED_SUCCESSFULLY = "{0} has been installed successfully!";
        private static string LDB_INSTALLATRION_FAILED = "{0} installation is failed!\nLook at the log file to find a problem.";
        private static string OPEN_FOLDER_DIALOG_DESCRIPTION = "Select a folder with SQL scripts to create a LDB.";
        private static string OPEN_LDB_FOLDER_DIALOG_DESCRIPTION = "Select a folder where a LDB will be created.";
        private static string LDB_EXISTS = "The LDB has already exists!\nDo you realy want to recreate the LDB and subscription?";
        private static string SYNC_EXISTS = "Программа синхронизации уже была установлена ранее!\nВы хотите переустановить ее?";
        private static string LDB_DOESNOT_EXIST = "You can not process the LDB further\nbecause it doesn't exist.";
        private const string LDB_CREATED_SUCCESSFULLY = "Create LDB completed successfully.";
        private const string RECREATE_VIEWS_SUCCESSFULLY = "Recreate Views on LDB completed successfully.";
        private const string RECREATE_VIEWS_FAILED = "Recreate Views failed.";

        private enum FileValidationEnum {
            NoWarning,
            FileExists,
            DirectoryNotExist
        }

        public DBComparer() {
            InitializeComponent();
            LDBProccessor.Instance.OnEntityProcess += Current_OnEntityProcess;
            SqlServerManager.Current.SetSkipingSqlOjects(CommandLineArgsProseccor.EliminationFileSkip);
            int lBottomPanelHeight = pnlPathToSQLscripts.Height;
            Height -= pnlPathToSQLscripts.Height;
            pnlPathToSQLscripts.Visible = false;
            lblState.Text = string.Empty;
            if (!string.IsNullOrEmpty(CommandLineArgsProseccor.ScriptFolder)) {
                Height += lBottomPanelHeight;
                pnlPathToSQLscripts.Height = lBottomPanelHeight;
                pnlPathToSQLscripts.Visible = true;

                txtFolderName.Text = CommandLineArgsProseccor.ScriptFolder;
                ConnectionDescription lConnectionDescription = CommandLineArgsProseccor.GetArgsConnection();
                if (null != lConnectionDescription)
                    SqlServerManager.Current.AddConnection(lConnectionDescription);

                RefreshDBList();
            }
        }

        private void Current_OnEntityProcess(object sender, LDBProcessEventArgs args) {
            lblState.Text = args.ProcessMessage;
            Invalidate();
            Application.DoEvents();
        }

        /// <summary>
        /// Open the connection definition form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangeConnection_Click(object sender, EventArgs e) {
            connectionForm.ShowDialog(this);
            RefreshDBList();
        }

        /// <summary>
        /// Delete a connection to a SW
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e) {
            foreach (DataGridViewRow dgwr in dataGridView1.SelectedRows) {
                DataRow dr = ((DataRowView) dgwr.DataBoundItem).Row;
                SqlServerManager.Current.RemoveConnectionByValues(dr[0].ToString(), dr[1].ToString());
                _swDatatable.Rows.Remove(dr);
            }
            EnableButtons();
        }

        /// <summary>
        /// Start a process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e) {
            SuspendLayout();
            Cursor = Cursors.WaitCursor;

            lblState.Text = string.Empty;
            bool result;

            bool? LDBExists = LDBProccessor.Instance.CheckLDBExistance();
            if (!LDBExists.HasValue) {
                Cursor = Cursors.Default;
                lblState.Text = string.Empty;
                MessageBox.Show(string.Format(LDB_INSTALLATRION_FAILED, SqlServerManager.LogicaDatabaseName), Application.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            result = LDBExists.Value;

            if (CommandLineArgsProseccor.CreateLDB) {
                if (result
                    && MessageBox.Show(LDB_EXISTS, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) {
                    Cursor = Cursors.Default;
                    lblState.Text = string.Empty;
                    if (MessageBox.Show(SYNC_EXISTS, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        OpenSyncForm();
                    return;
                }

                result = LDBProccessor.Instance.CreateLBDFiles(txtLDBFolder.Text, txtLDBLogFolder.Text);
                if (!result) {
                    MessageBox.Show(string.Format(LDB_INSTALLATRION_FAILED, SqlServerManager.LogicaDatabaseName), Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor = Cursors.Default;
                    lblState.Text = string.Empty;
                    return;
                }

                result = LDBProccessor.Instance.CreatePredefinedSQLs(Unpacker.GetEmbededSQLs(),
                    SqlServerManager.Current.SqlConnections[0].ConnectionToLDB);
                if (!result) {
                    MessageBox.Show(string.Format(LDB_INSTALLATRION_FAILED, SqlServerManager.LogicaDatabaseName), Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (MessageBox.Show("Would you like to keep on proccessing", Application.ProductName, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.No) {
                        Cursor = Cursors.Default;
                        lblState.Text = string.Empty;
                        return;
                    }
                }

                result = LDBProccessor.Instance.CreateReferencingView(SqlServerManager.Current.SqlConnections);
                if (!result) {
                    MessageBox.Show(string.Format(LDB_INSTALLATRION_FAILED, SqlServerManager.LogicaDatabaseName), Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lblState.Text = string.Empty;
                    Cursor = Cursors.Default;
                    return;
                }
            } else {
                if (!result) {
                    MessageBox.Show(LDB_DOESNOT_EXIST, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    lblState.Text = string.Empty;
                    Cursor = Cursors.Default;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(txtFolderName.Text)) {
                result = LDBProccessor.Instance.CreateLDBEntitiesProcess(SqlServerManager.Current.SqlConnections, txtFolderName.Text);

                if (!result) {
                    MessageBox.Show(string.Format(LDB_INSTALLATRION_FAILED, SqlServerManager.LogicaDatabaseName),
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lblState.Text = string.Empty;
                    Cursor = Cursors.Default;
                    return;
                }
            }

            Cursor = Cursors.Default;
            lblState.Text = LDB_CREATED_SUCCESSFULLY;
            MessageBox.Show(string.Format(LBD_INSTALLED_SUCCESSFULLY, SqlServerManager.LogicaDatabaseName), Application.ProductName,
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            OpenSyncForm();
        }

        /// <summary>
        /// Open form for customizing sync process
        /// </summary>
        private void OpenSyncForm() {
            SyncMain lSyncMain = new SyncMain(SqlServerManager.Current.SqlConnections);
            lSyncMain.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e) {
            Dispose();
        }

        private void OpenLDBFolderClick(object sender, EventArgs e, TextBox txtBox, string folderDesc) {
            LBDDataFileType fldType;

            if (((Button) sender).Name == btnOpenLDBFolder.Name) {
                fldType = LBDDataFileType.ROWS;
            } else if (((Button) sender).Name == btnOpenLogFileFolder.Name) {
                fldType = LBDDataFileType.LOG;
            } else {
                fldType = LBDDataFileType.UNDEFINED;
            }


            using (FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog()) {
                string startFolder = Application.StartupPath;
                if (!string.IsNullOrEmpty(txtBox.Text))
                    startFolder = txtBox.Text;

                folderBrowserDialog1.SelectedPath = startFolder;
                folderBrowserDialog1.Description = folderDesc;

                DialogResult res = folderBrowserDialog1.ShowDialog();
                if (res == DialogResult.OK) {
                    txtBox.Text = fldType == LBDDataFileType.UNDEFINED
                        ? folderBrowserDialog1.SelectedPath : SqlServerManager.Current.GetLDBFileName(folderBrowserDialog1.SelectedPath, fldType);
                }
            }
        }

        private void btnOpenLDBFolder_Click(object sender, EventArgs e) {
            OpenLDBFolderClick(sender, e, txtLDBFolder, OPEN_LDB_FOLDER_DIALOG_DESCRIPTION);
        }

        private void btnOpenLogFileFolder_Click(object sender, EventArgs e) {
            OpenLDBFolderClick(sender, e, txtLDBLogFolder, OPEN_LDB_FOLDER_DIALOG_DESCRIPTION);
        }

        private void SetLDBDataFilesPathes() {
            try {
                Dictionary<LBDDataFileType, string> result;

                txtLDBFolder.ReadOnly = txtLDBLogFolder.ReadOnly = SqlServerManager.Current.SqlConnections.HasLDB;
                if (SqlServerManager.Current.SqlConnections.HasLDB) {
                    result = SqlServerManager.Current.GetLDBFilesPathes();
                } else {
                    result = SqlServerManager.Current.GetSalesWorksDataFilesPath();
                    if (result.Count == 0) {
                        result = SqlServerManager.Current.GetDefaultSLQDataFilesPathes();
                    }
                }
                if (result.Count > 0) {
                    if (result.ContainsKey(LBDDataFileType.LOG))
                        txtLDBLogFolder.Text = result[LBDDataFileType.LOG];

                    if (result.ContainsKey(LBDDataFileType.ROWS))
                        txtLDBFolder.Text = result[LBDDataFileType.ROWS];
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EnableButtons() {
            btnStart.Enabled = _swDatatable.Rows.Count > 0;
            btnRecreateViews.Enabled = btnStart.Enabled;
        }

        private void btnOpenScriptFileFolder_Click(object sender, EventArgs e) {
            OpenLDBFolderClick(sender, e, txtFolderName, OPEN_FOLDER_DIALOG_DESCRIPTION);
        }

        private void RefreshDBList() {
            SuspendLayout();
            List<ConnectionDescription> lConnections = SqlServerManager.Current.SqlConnections;

            _swDatatable.Rows.Clear();

            foreach (ConnectionDescription lConnection in lConnections) {
                DataRow lRow = _swDatatable.NewRow();
                _swDatatable.Rows.Add(lConnection.ServerName, lConnection.DataBase, lConnection.LDBExists);
            }

            btnChangeConnection.Enabled = btnDelete.Enabled = CommandLineArgsProseccor.CreateLDB;

            if (lConnections.Count > 0 && CommandLineArgsProseccor.CreateLDB) {
                SetLDBDataFilesPathes();
            } else {
                txtLDBLogFolder.Text = txtLDBFolder.Text = string.Empty;
                txtLDBLogFolder.ReadOnly = txtLDBFolder.ReadOnly = true;
                btnOpenLogFileFolder.Enabled = btnOpenLDBFolder.Enabled = false;
            }

            dataGridView1.DataSource = _swDatatable;
            EnableButtons();
            ResumeLayout();
        }

        private void btnRecreateViews_Click(object sender, EventArgs e) {
            SuspendLayout();
            Cursor = Cursors.WaitCursor;
            lblState.Text = string.Empty;

            try {
                bool lResult = LDBProccessor.Instance.CreateReferencingView(SqlServerManager.Current.SqlConnections);
                if (!lResult) {
                    MessageBox.Show(string.Format(LDB_INSTALLATRION_FAILED, SqlServerManager.LogicaDatabaseName), Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lblState.Text = RECREATE_VIEWS_FAILED;
                    Cursor = Cursors.Default;
                    return;
                }
            } finally {
                Cursor = Cursors.Default;
                lblState.Text = RECREATE_VIEWS_SUCCESSFULLY;
            }
        }
    }
}