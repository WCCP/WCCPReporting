
exec sp_addmergepullsubscription_agent
	 @publisher = N'{PublisherName}'
	, @publisher_db = N'SalesWorks'
	, @publication = N'DW'
	, @distributor = N'{DistributorName}'
	, @distributor_security_mode = 0
	, @distributor_login = N'WebSync'
	, @distributor_password = N''
	, @publisher_security_mode = 0
	, @publisher_login = N'WebSync'
	, @publisher_password = N''
	, @use_interactive_resolver = N'False'
	, @use_web_sync = 1
	, @internet_url = N'{InternetUrl}'
	, @internet_login = N'db4_sync_webuser'
	, @internet_password = null
	, @internet_security_mode = 0
	, @internet_timeout = 900
