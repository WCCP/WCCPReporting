-- Добавление подписки слиянием по запросу

/****** Начало: сценарий для запуска на стороне подписчика: ******/
IF Object_ID('dbo.sysmergesubscriptions') IS NOT NULL
BEGIN
	IF EXISTS(
		SELECT 1
		FROM  sysmergesubscriptions
		WHERE (subscriber_server = '{PublisherName}')
	)
	EXEC sp_dropmergepullsubscription
		@publisher = N'{PublisherName}', 
		@publisher_db = N'SalesWorks', 
		@publication = N'DW'
END

exec sp_addmergepullsubscription 
	  @publisher = N'{PublisherName}'
	, @publication = N'DW'
	, @publisher_db = N'SalesWorks'
	, @subscriber_type = N'Local'
	, @subscription_priority = 0
	, @description = N'LDB WebSync'
/****** Конец: сценарий для запуска на стороне подписчика: ******/


