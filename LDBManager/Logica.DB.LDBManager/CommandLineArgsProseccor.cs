﻿using System;
using System.Collections.Generic;
using System.Text;
using Logica.DB.LDBProcessor;

namespace Logica.LDBManager
{
	internal static class CommandLineArgsProseccor
	{
		const string scriptsFolderArg = "/f";
		const string useLDBCreationArg = "/s";
		const string SQLServerNameArg = "/q";
		const string SQLUserNameArg = "/u";
		const string SQLUserPasswordArg = "/p";
		const string SQLSWDBNameArg = "/d";
		const string eliminationFileSkipArg = "/l";


		private static string _scriptFolder;
		/// <summary>
		/// Returns a folder with SQL scripts
		/// </summary>
		public static string ScriptFolder
		{
			get { return _scriptFolder; }
		}

		private static bool _createLDB = true;
		public static bool CreateLDB
		{
			get { return _createLDB; }
		}
		
		private static string _SQLServerName;
		public static string SQLServerName
		{
			get { return _SQLServerName; }
		}

		private static string _SQLSWDBName;

		public static string SQLSWDBName
		{
			get { return _SQLSWDBName; }
		}
		
		private static string _SQLUserName;
		public static string SQLUserName
		{
			get { return _SQLUserName; }
		}
		
		private static string _SQLUserPassword;
		public static string SQLUserPassword
		{
			get { return _SQLUserPassword; }
		}

		private static bool _eliminationFileSkip = false;
		public static bool EliminationFileSkip
		{
			get { return _eliminationFileSkip; }
		}

		/// <summary>
		/// Reveal args come from the command line
		/// </summary>
		/// <param name="args"></param>
		internal static void ProcessArgs(string[] args)  
		{
			//Get folder with LDB scripts
			_scriptFolder = GetCommandLineArgument(args, scriptsFolderArg);
			
			//Skip LDB creation
			string result = GetCommandLineArgument(args, useLDBCreationArg);
			if (null != result)
			{
				_createLDB = false;
			}

			//Get SQL Server name
			_SQLServerName = GetCommandLineArgument(args, SQLServerNameArg);

			//Get SQL SW DB name
			_SQLSWDBName = GetCommandLineArgument(args, SQLSWDBNameArg);

			//Get user name to connect to SQL Server
			_SQLUserName = GetCommandLineArgument(args, SQLUserNameArg);

			//Get user password to connect to SQL Server
			_SQLUserPassword = GetCommandLineArgument(args, SQLUserPasswordArg);
			
			//Skip exception list from the elimination.xml file
			result = GetCommandLineArgument(args, eliminationFileSkipArg);
			if (result != null)
				_eliminationFileSkip = true;
		}

		/// <summary>
		/// returns SQL connection came form the command line
		/// </summary>
		/// <returns></returns>
		public static ConnectionDescription GetArgsConnection()
		{
			if (!string.IsNullOrEmpty(_SQLServerName))
			{
				return new ConnectionDescription(_SQLServerName, string.IsNullOrEmpty(_SQLSWDBName) ? SqlServerManager.LogicaDatabaseName : _SQLSWDBName, _SQLUserName, _SQLUserPassword);
			}
			return null;

		}

		/// <summary>
		/// Return a particular value caome from args
		/// </summary>
		/// <param name="args">list of argument values</param>
		/// <param name="argPreposition">a key to be retrieved</param>
		/// <returns>Particular value</returns>
		private static string GetCommandLineArgument(string[] args, string argPreposition)
		{
			foreach (string arg in args)
			{
				if (arg.Substring(0, 2).Equals(argPreposition, StringComparison.InvariantCultureIgnoreCase))
				{
					return arg.Substring(2);
				}
			}
			return null;
		}
	}
}
