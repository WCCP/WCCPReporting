﻿namespace Logica.LDBManager.Syncronization
{
    partial class SyncMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreate = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbDeny = new System.Windows.Forms.CheckBox();
            this.cbAllow = new System.Windows.Forms.CheckBox();
            this.cbError = new System.Windows.Forms.CheckBox();
            this.cbWarn = new System.Windows.Forms.CheckBox();
            this.cbInfo = new System.Windows.Forms.CheckBox();
            this.cbDesktop = new System.Windows.Forms.CheckBox();
            this.cbStartup = new System.Windows.Forms.CheckBox();
            this.cbRun = new System.Windows.Forms.CheckBox();
            this.btnSkip = new System.Windows.Forms.Button();
            this.cbCreateSchedule = new System.Windows.Forms.CheckBox();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(124, 172);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 16;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbDeny);
            this.groupBox3.Controls.Add(this.cbAllow);
            this.groupBox3.Controls.Add(this.cbError);
            this.groupBox3.Controls.Add(this.cbWarn);
            this.groupBox3.Controls.Add(this.cbInfo);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(268, 78);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Уровень логирования";
            // 
            // cbDeny
            // 
            this.cbDeny.AutoSize = true;
            this.cbDeny.Location = new System.Drawing.Point(140, 36);
            this.cbDeny.Name = "cbDeny";
            this.cbDeny.Size = new System.Drawing.Size(99, 17);
            this.cbDeny.TabIndex = 15;
            this.cbDeny.Tag = "FailureAudit";
            this.cbDeny.Text = "Аудит отказов";
            this.cbDeny.UseVisualStyleBackColor = true;
            // 
            // cbAllow
            // 
            this.cbAllow.AutoSize = true;
            this.cbAllow.Location = new System.Drawing.Point(140, 19);
            this.cbAllow.Name = "cbAllow";
            this.cbAllow.Size = new System.Drawing.Size(98, 17);
            this.cbAllow.TabIndex = 14;
            this.cbAllow.Tag = "SuccessAudit";
            this.cbAllow.Text = "Аудит успехов";
            this.cbAllow.UseVisualStyleBackColor = true;
            // 
            // cbError
            // 
            this.cbError.AutoSize = true;
            this.cbError.Checked = true;
            this.cbError.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbError.Location = new System.Drawing.Point(14, 54);
            this.cbError.Name = "cbError";
            this.cbError.Size = new System.Drawing.Size(66, 17);
            this.cbError.TabIndex = 13;
            this.cbError.Tag = "Error";
            this.cbError.Text = "Ошибки";
            this.cbError.UseVisualStyleBackColor = true;
            // 
            // cbWarn
            // 
            this.cbWarn.AutoSize = true;
            this.cbWarn.Checked = true;
            this.cbWarn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWarn.Location = new System.Drawing.Point(14, 37);
            this.cbWarn.Name = "cbWarn";
            this.cbWarn.Size = new System.Drawing.Size(113, 17);
            this.cbWarn.TabIndex = 12;
            this.cbWarn.Tag = "Warning";
            this.cbWarn.Text = "Предупреждения";
            this.cbWarn.UseVisualStyleBackColor = true;
            // 
            // cbInfo
            // 
            this.cbInfo.AutoSize = true;
            this.cbInfo.Checked = true;
            this.cbInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbInfo.Location = new System.Drawing.Point(14, 20);
            this.cbInfo.Name = "cbInfo";
            this.cbInfo.Size = new System.Drawing.Size(96, 17);
            this.cbInfo.TabIndex = 11;
            this.cbInfo.Tag = "Information";
            this.cbInfo.Text = "Уведомления";
            this.cbInfo.UseVisualStyleBackColor = true;
            // 
            // cbDesktop
            // 
            this.cbDesktop.AutoSize = true;
            this.cbDesktop.Checked = true;
            this.cbDesktop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDesktop.Location = new System.Drawing.Point(26, 97);
            this.cbDesktop.Name = "cbDesktop";
            this.cbDesktop.Size = new System.Drawing.Size(196, 17);
            this.cbDesktop.TabIndex = 17;
            this.cbDesktop.Text = "Создать ярлык на рабочем столе";
            this.cbDesktop.UseVisualStyleBackColor = true;
            // 
            // cbStartup
            // 
            this.cbStartup.AutoSize = true;
            this.cbStartup.Location = new System.Drawing.Point(26, 114);
            this.cbStartup.Name = "cbStartup";
            this.cbStartup.Size = new System.Drawing.Size(148, 17);
            this.cbStartup.TabIndex = 17;
            this.cbStartup.Text = "Создать ярлык в Пуск\'е";
            this.cbStartup.UseVisualStyleBackColor = true;
            // 
            // cbRun
            // 
            this.cbRun.AutoSize = true;
            this.cbRun.Checked = true;
            this.cbRun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRun.Location = new System.Drawing.Point(26, 131);
            this.cbRun.Name = "cbRun";
            this.cbRun.Size = new System.Drawing.Size(250, 17);
            this.cbRun.TabIndex = 17;
            this.cbRun.Text = "Запустить первоначальную синхронизацию ";
            this.cbRun.UseVisualStyleBackColor = true;
            // 
            // btnSkip
            // 
            this.btnSkip.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSkip.Location = new System.Drawing.Point(205, 172);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(75, 23);
            this.btnSkip.TabIndex = 18;
            this.btnSkip.Text = "Пропустить";
            this.btnSkip.UseVisualStyleBackColor = true;
            // 
            // cbCreateSchedule
            // 
            this.cbCreateSchedule.AutoSize = true;
            this.cbCreateSchedule.Checked = true;
            this.cbCreateSchedule.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCreateSchedule.Location = new System.Drawing.Point(26, 149);
            this.cbCreateSchedule.Name = "cbCreateSchedule";
            this.cbCreateSchedule.Size = new System.Drawing.Size(131, 17);
            this.cbCreateSchedule.TabIndex = 19;
            this.cbCreateSchedule.Text = "Создать расписание";
            this.cbCreateSchedule.UseVisualStyleBackColor = true;
            // 
            // SyncMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnSkip;
            this.ClientSize = new System.Drawing.Size(294, 205);
            this.Controls.Add(this.cbCreateSchedule);
            this.Controls.Add(this.btnSkip);
            this.Controls.Add(this.cbRun);
            this.Controls.Add(this.cbStartup);
            this.Controls.Add(this.cbDesktop);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCreate);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SyncMain";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Планировщик синхронизации";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cbDeny;
        private System.Windows.Forms.CheckBox cbAllow;
        private System.Windows.Forms.CheckBox cbError;
        private System.Windows.Forms.CheckBox cbWarn;
        private System.Windows.Forms.CheckBox cbInfo;
        private System.Windows.Forms.CheckBox cbDesktop;
        private System.Windows.Forms.CheckBox cbStartup;
        private System.Windows.Forms.CheckBox cbRun;
        private System.Windows.Forms.Button btnSkip;
        private System.Windows.Forms.CheckBox cbCreateSchedule;
    }
}