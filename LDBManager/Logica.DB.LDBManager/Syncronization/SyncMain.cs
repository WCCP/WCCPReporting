﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Resources;
using System.Reflection;
using Logica.DB.LDBProcessor;

namespace Logica.LDBManager.Syncronization
{
    public partial class SyncMain : Form
    {
        
        string pathToFile = String.Empty;
        CheckBox[] listLogLevel;
        ConnectionsCollection connections;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connections">Connection data</param>
        public SyncMain(ConnectionsCollection connections)
        {
            InitializeComponent();
            this.connections = connections;
        }
        /// <summary>
        /// Unpack executive files, set config , set default values
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //create executed files 
            string pathToExecuteFiles = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Logica\";
            try
            {
                //Create directory
                if (Directory.Exists(pathToExecuteFiles))
                {
                    Directory.Delete(pathToExecuteFiles, true);
                }
                Directory.CreateDirectory(pathToExecuteFiles);
                if (!Directory.Exists(pathToExecuteFiles))
                    MessageBox.Show("Directory " + pathToExecuteFiles + "does not exist", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogManager.WriteMessage(string.Format("Directory {0} created", pathToExecuteFiles));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogManager.WriteMessage(ex.Message, ex.InnerException, System.Diagnostics.EventLogEntryType.Error);
            }
            try
            {
                pathToFile = SyncModuleManager.Unpack(pathToExecuteFiles);
                //Add connection to config file
                AddConnectionSectionToCF();
            }
            catch (Exception eUnpack)
            {
                string unpackError = string.Format("Не могу распаковать файлы синхронизатора в {0} - {1}", pathToExecuteFiles, eUnpack.Message);
                LogManager.WriteMessage(unpackError, eUnpack, System.Diagnostics.EventLogEntryType.Error);
                MessageBox.Show(unpackError, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Dispose();
            }
            try
            {
                SyncModuleManager.SetupCertificate(pathToExecuteFiles);
            }
            catch(Exception eSetupCertificate)
            {
                string setupCertificateError = string.Format("Не могу установить сертификат. Установите сертификат вручную");
                LogManager.WriteMessage(setupCertificateError, eSetupCertificate, System.Diagnostics.EventLogEntryType.Warning);
                MessageBox.Show(setupCertificateError, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            //set forms controls values
            listLogLevel = new CheckBox[] { cbInfo, cbWarn, cbError, cbAllow, cbDeny };
        }
        
        //Create shortcuts, add records to config file, run first sync process, open ScheduleSetup form
        private void btnCreate_Click(object sender, EventArgs e)
        {
            //Add record into config file
            string level = "";
            foreach (CheckBox cb in listLogLevel)
                if (cb.Checked)
                    level += cb.Tag.ToString() + "|";
            AddAppSectionToCF("LogLevel", level.Length > 0 ? level.TrimEnd('|') : "");
            if (cbDesktop.Checked)
            {
                try
                {
                    SyncModuleManager.CreateDesktopShortcut(pathToFile);
                }
                catch (Exception eCreateDesktopShortcut)
                {
                    string setupCreateDesktopShortcut = string.Format("Не создать ярлык на рабочем столе");
                    LogManager.WriteMessage(setupCreateDesktopShortcut, eCreateDesktopShortcut, System.Diagnostics.EventLogEntryType.Warning);
                    MessageBox.Show(setupCreateDesktopShortcut, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (cbStartup.Checked)
            {
                try
                {
                    SyncModuleManager.CreateSturtupShortcut(pathToFile);
                }
                catch (Exception eCreateSturtupShortcut)
                {
                    string setupCreateSturtupShortcut = string.Format("Не создать ярлык в меню Пуск");
                    LogManager.WriteMessage(setupCreateSturtupShortcut, eCreateSturtupShortcut, System.Diagnostics.EventLogEntryType.Warning);
                    MessageBox.Show(setupCreateSturtupShortcut, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (cbCreateSchedule.Checked)
            {
                ScheduleSetup ss = new ScheduleSetup(pathToFile);
                ss.ShowDialog();
            }
            if (cbRun.Checked)
                System.Diagnostics.Process.Start(pathToFile);
            DialogResult = DialogResult.OK;
        }
        /// <summary>
        /// Add section to config file
        /// </summary>
        /// <param name="sectionName">Name of section</param>
        /// <param name="sectionValue">Value of section</param>
        private void AddAppSectionToCF(string sectionName, string sectionValue)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(pathToFile);
            config.AppSettings.Settings.Add(sectionName, String.IsNullOrEmpty(sectionValue) ? "" : sectionValue);
            config.Save(ConfigurationSaveMode.Modified);
        }
        /// <summary>
        /// Add connection string section to config file
        /// </summary>
        private void AddConnectionSectionToCF()
        {
            SqlConnectionStringBuilder sectionValue = connections[0].ConnectionToLDB.SqlConnectionString;
            AddAppSectionToCF("DataSource", sectionValue.DataSource);
            AddAppSectionToCF("InitialCatalog", sectionValue.InitialCatalog);
            AddAppSectionToCF("IntegratedSecurity", sectionValue.IntegratedSecurity.ToString());
            AddAppSectionToCF("Password", sectionValue.Password);
            AddAppSectionToCF("UserID", sectionValue.UserID);
            for (int i = 0; i < connections.Count; i++)
                AddAppSectionToCF("SWInitialCatalog" + i.ToString(), connections[i].SqlConnectionString.InitialCatalog);

            //Configuration config = ConfigurationManager.OpenExeConfiguration(pathToExecuteFiles + SyncModule.strLogica_LDBSync_Main);
            ////HARDCODED because of Logica.LDBSync.Main.exe waits this name
            //ConnectionStringSettings css = new ConnectionStringSettings("SQLExpress", sectionValue.ToString() ,"System.Data.SqlClient");
            //config.ConnectionStrings.ConnectionStrings.Add(css);
            //config.Save(ConfigurationSaveMode.Modified);
        }
    }
}
