﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Resources;
using System.Reflection;
using System.Collections;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;

namespace Logica.LDBManager.Syncronization
{
    /// <summary>
    /// Unpack files from resources
    /// </summary>
    public static class SyncModuleManager
    {
        /// <summary>
        /// Unpack sync program files(dll, exe, config)
        /// </summary>
        /// <param name="folderName">Path to unpack</param>
        /// <returns></returns>
        public static string Unpack(string folderName)
        {
            string mainPath = String.Empty;
            ResourceManager res = new ResourceManager(SyncModule.ResourceManager.BaseName, Assembly.GetExecutingAssembly());
            using (ResourceSet resSet = res.GetResourceSet(CultureInfo.InvariantCulture, true, false))
            {
                if (null != resSet)
                {
                    IDictionaryEnumerator collection = resSet.GetEnumerator();
                    collection.Reset();

                    while (collection.MoveNext())
                    {
                        if (collection.Value.GetType() != typeof(Byte[]))
                            continue;

                        object key = collection.Key;
                        object value = collection.Value;
                        if (null != key && null != value)
                        {
                            string fileName = res.GetString(string.Format("str{0}", collection.Key));
                            if (null != fileName)
                            {
                                if (fileName.EndsWith(".exe"))
                                    mainPath = Path.Combine(folderName, fileName);
                                fileName = Path.Combine(folderName, fileName);
                                
                                if (File.Exists(fileName))
                                    File.Delete(fileName);
                                WriteFile(fileName, (byte[])collection.Value);
                            }
                        }
                    }
                }
            }
            res.ReleaseAllResources();
            return mainPath;
        }
        /// <summary>
        /// Save byte array as file on disk
        /// </summary>
        /// <param name="fileName">Full path to file</param>
        /// <param name="fileContent">Content of file</param>
        private static void WriteFile(string fileName, byte[] fileContent)
        {
            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
            fs.Write(fileContent, 0, fileContent.Length);
            fs.Flush();
            fs.Close();
            fs.Dispose();
        }

        /// <summary>
        /// CreateShortcut
        /// </summary>
        /// <param name="path">Sync program path</param>
        /// <param name="destination">Target folder</param>
        public static void CreateShortcut(string path, string destination)
        {
            Type shell = Type.GetTypeFromProgID("WScript.Shell");

            object obj = Activator.CreateInstance(shell);
            List<object> pars = new List<object>();
            pars.Add(destination + @"\LDB Sync Main.lnk");
            object shortcut = shell.InvokeMember("CreateShortcut", BindingFlags.InvokeMethod, null, obj, pars.ToArray());
            
            pars.Clear();
            pars.Add(path);
            shortcut.GetType().InvokeMember("TargetPath", BindingFlags.SetProperty, null, shortcut, pars.ToArray());

            pars.Clear();
            pars.Add(@"LDBSync.Main.exe");
            shortcut.GetType().InvokeMember("Description", BindingFlags.SetProperty, null, shortcut, pars.ToArray());

            pars.Clear();
            shortcut.GetType().InvokeMember("Save", BindingFlags.InvokeMethod, null, shortcut, pars.ToArray());
        }
        /// <summary>
        /// Create Shortcut on desktop
        /// </summary>
        /// <param name="path">Sync program path</param>
        public static void CreateDesktopShortcut(string path)
        {
            CreateShortcut(path, Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
        }
        /// <summary>
        /// Create Shortcut in Startup 
        /// </summary>
        /// <param name="path">Sync program path</param>
        public static void CreateSturtupShortcut(string path)
        {
            CreateShortcut(path, Environment.GetFolderPath(Environment.SpecialFolder.Programs));
        }
        /// <summary>
        /// Setup if nessesary new certificates
        /// </summary>
        /// <param name="folderName">path to new certificates</param>
        public static void SetupCertificate(string folderName)
        {
            string certPath = Path.Combine(folderName, SyncModule.strCert);
            X509Certificate2 cert = new X509Certificate2(certPath, "Cthnbabrfn");
            X509Store store = new X509Store(StoreName.AuthRoot, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadWrite);
            store.Add(cert);
            store.Close();
            try{File.Delete(certPath);}catch { }
         }
    }
}
