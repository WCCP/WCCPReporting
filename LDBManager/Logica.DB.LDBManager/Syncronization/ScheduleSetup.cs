﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Resources;
using System.Reflection;

namespace Logica.LDBManager.Syncronization
{
    public partial class ScheduleSetup : Form
    {
        string pathToFile = String.Empty;
        CheckBox[] listDays;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pathToFile">Path to sync module</param>
        public ScheduleSetup(string pathToFile)
        {
            InitializeComponent();
            this.pathToFile = pathToFile;
            
        }
        /// <summary>
        /// Set default parameters values
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //create executed files 
            //set forms controls values
            listDays = new CheckBox[] { cbSun, cbMon, cbTue, cdWen, cbThu, cbFri, cbSut };
            cmbType.SelectedIndex = 2;
            dtStart.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0);// DateTime.Now.Date.AddHours(2);
            txtUser.Text = Environment.UserDomainName + @"\" + Environment.UserName;
        }
        /// <summary>
        /// Change friquency type
        /// </summary>
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            foreach (CheckBox cb in listDays)
                cb.Visible = false;
            lbPrefix.Visible = lbPostfix.Visible = numCount.Visible = true;
            if (cmbType.SelectedIndex == 0)
            {
                lbPrefix.Text = "Каждую";
                lbPostfix.Text = "минуту";
                grSched.Text = "Расписание по минутам";
                numCount.Maximum = 1439;
            }
            else if (cmbType.SelectedIndex == 1)
            {
                lbPrefix.Text = "Каждый";
                lbPostfix.Text = "час";
                grSched.Text = "Расписание по часам";
                numCount.Maximum = 23;
            }
            else if (cmbType.SelectedIndex == 2)
            {
                lbPrefix.Text = "Каждый";
                lbPostfix.Text = "день";
                grSched.Text = "Расписание по дням";
                numCount.Maximum = 365;
            }
            else if (cmbType.SelectedIndex == 3)
            {
                lbPrefix.Text = "Каждую";
                lbPostfix.Text = "неделю";
                grSched.Text = "Расписание по неделям";
                foreach (CheckBox cb in listDays)
                    cb.Visible = true;
                numCount.Maximum = 52;
            }
            else if (cmbType.SelectedIndex == 4)
            {
                lbPrefix.Text = "Каждый";
                lbPostfix.Text = "месяц";
                grSched.Text = "Расписание по месяцам";
                numCount.Maximum = 12;
            }
            else
            {
                lbPrefix.Visible = lbPostfix.Visible = numCount.Visible = false;
            }
        }
        /// <summary>
        /// Start process of creation task in Task Manager
        /// </summary>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (!Validate())
                return;
            //
            ScheduledTaskManager sm = new ScheduledTaskManager(SyncModule.TaskName, pathToFile, GetFriquency(), txtPassword.Text, txtUser.Text);
            sm.AddFriquencyCoefficient((int)numCount.Value);
            //set day of week
            if (cmbType.SelectedIndex == 3)
            {
                String value = "";
                foreach (CheckBox cb in listDays)
                    if (cb.Checked)
                        value += cb.Tag.ToString() + " ";
                if (String.IsNullOrEmpty(value))
                    value = ScheduledTaskManager.DayType.MON.ToString();
                sm.AddRunDay(value);
            }
            //
            sm.AddStartDateTime(dtStart.Value);
            if (sm.RunCreationProcess() == 1)
            {
                MessageBox.Show(SyncModule.ErrorNotCreatedSch, SyncModule.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult = DialogResult.OK;
                MessageBox.Show(SyncModule.Successfully, SyncModule.Information, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// Return selected FriquencyType 
        /// </summary>
        /// <returns></returns>
        private ScheduledTaskManager.FriquencyType GetFriquency()
        {
            if (cmbType.SelectedIndex == 0)
                return ScheduledTaskManager.FriquencyType.MINUTE;
            else if (cmbType.SelectedIndex == 1)
                return ScheduledTaskManager.FriquencyType.HOURLY;
            else if (cmbType.SelectedIndex == 2)
                return ScheduledTaskManager.FriquencyType.DAILY;
            else if (cmbType.SelectedIndex == 3)
                return ScheduledTaskManager.FriquencyType.WEEKLY;
            else 
                return ScheduledTaskManager.FriquencyType.MONTHLY;
        }
        /// <summary>
        /// Validate input data
        /// </summary>
        /// <returns>If data is valid</returns>
        private new bool Validate()
        {
            bool valid = true;
            if (String.IsNullOrEmpty(txtUser.Text))
            {
                MessageBox.Show(String.Format(SyncModule.ErrorEmptyField, lbUser.Text), SyncModule.Warning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                valid = false;
            }
            else if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show(String.Format(SyncModule.ErrorEmptyField, lbPassword.Text), SyncModule.Warning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //if (MessageBox.Show(String.Format("Вы уверены что паролем являеться пустое значение?", SyncModule.Warning, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)) == DialogResult.No)
                    valid = false;
            }
            return valid;
        }
    }
}
