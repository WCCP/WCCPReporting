﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Logica.LDBManager.Syncronization
{
    /// <summary>
    /// Create task in task manager
    /// </summary>
    public class ScheduledTaskManager
    {
        #region Enums
        /// Type of Friquency run
        public enum FriquencyType
        {
             MINUTE, HOURLY, DAILY, WEEKLY, MONTHLY, ONCE, ONSTART, ONLOGON, ONIDLE
        }
        // Week Days 
        public enum DayType
        {
             MON, TUE, WED, THU, FRI, SAT, SUN
        }
        // Month
        public enum MonthType
        {
             JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC
        }
        #endregion

        /// <summary>
        /// Argument for task creating
        /// </summary>
        private StringBuilder arguments = new StringBuilder(@"/create ");
        public StringBuilder Arguments
        {
            get { return arguments; }
            set { arguments = value; }
        }
        private string taskName = "";
        private FriquencyType friquency;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="taskName">Task Name</param>
        /// <param name="programPath">Path to execute programm</param>
        /// <param name="friquency">Run friquency</param>
        /// <param name="taskPassword">Run task user password</param>
        /// <param name="taskUser">Run task user name</param>
        public ScheduledTaskManager(string taskName, string programPath, FriquencyType friquency, string taskPassword, string taskUser)
        {
            this.friquency =friquency;
            this.taskName = taskName;
            AddArg(@"/TN", taskName);
            AddArg(@"/TR", "\\\"" + programPath + "\\\" -SCHD");
            AddArg(@"/SC", friquency.ToString());
            AddArg(@"/RU", taskUser);
            AddArg(@"/RP", taskPassword);
        }
        /// <summary>
        /// Start creation task process
        /// </summary>
        /// <returns>ExitCode from schtasks</returns>
        public int RunCreationProcess()
        {
            ProcessStartInfo psiDel = new ProcessStartInfo();
            psiDel.Arguments = String.Format("/Delete /TN \"{0}\" /F", taskName);
            psiDel.FileName = @"schtasks";
            psiDel.CreateNoWindow = true;
            psiDel.WindowStyle = ProcessWindowStyle.Hidden;
            Process procDel = Process.Start(psiDel);
            while (!procDel.HasExited)
                System.Threading.Thread.Sleep(500);

            ProcessStartInfo psiCreate = new ProcessStartInfo();
            psiCreate.Arguments = Arguments.ToString();
            psiCreate.FileName = @"schtasks";
            psiCreate.CreateNoWindow = true;
            psiCreate.WindowStyle = ProcessWindowStyle.Hidden;
            Process proc = Process.Start(psiCreate);
            while (!proc.HasExited)
                System.Threading.Thread.Sleep(500);
            return proc.ExitCode;
        }

        #region Set properties
        /// <summary>
        /// Add system
        /// </summary>
        /// <param name="pc">system(PC) name</param>
        public void AddSystem(string pc)
        {
            AddArg(@"/S", pc);
        }
        /// <summary>
        /// Add application user
        /// </summary>
        /// <param name="user">User name</param>
        public void AddApplicationUser(string user)
        {
            AddArg(@"/U", user);
        }
        /// <summary>
        /// Add application user password
        /// </summary>
        /// <param name="user">User password</param>
        public void AddApplicationPassword(string password)
        {
            AddArg(@"/P", password);
        }
        /// <summary>
        /// Set friquency of task run
        /// </summary>
        /// <param name="friquencyCoof">Type of friquency</param>
        public void AddFriquencyCoefficient(int friquencyCoof)
        {
            if (friquency == FriquencyType.MINUTE)
                RarseCoof(ref friquencyCoof, 1, 1439);
            if (friquency == FriquencyType.HOURLY)
                RarseCoof(ref friquencyCoof, 1, 23);
            if (friquency == FriquencyType.DAILY)
                RarseCoof(ref friquencyCoof, 1, 365);
            if (friquency == FriquencyType.WEEKLY)
                RarseCoof(ref friquencyCoof, 1, 52);
            if (friquency == FriquencyType.MONTHLY)
                RarseCoof(ref friquencyCoof, 1, 12);

            AddArg(@"/MO", friquencyCoof.ToString());
        }
        /// <summary>
        /// Day, then task should be run
        /// </summary>
        /// <param name="days">day name</param>
        public void AddRunDay(string days)
        {
            AddArg(@"/D", days);
        }
        /// <summary>
        /// Day, then task should be run
        /// </summary>
        /// <param name="day">day name</param>
        public void AddRunDay(DayType day)
        {
            AddArg(@"/D", day.ToString());
        }
        /// <summary>
        /// Month, then task should be run
        /// </summary>
        /// <param name="month">Name of month</param>
        public void AddRunMonth(MonthType month)
        {
            AddArg(@"/M", month.ToString());
        }
        /// <summary>
        /// Set the date of first start
        /// </summary>
        /// <param name="date">Date</param>
        public void AddStartDateTime(DateTime date)
        {
            AddArg(@"/SD", date.ToString("dd/MM/yyyy").Replace('.', '/'));
            AddArg(@"/ST", date.ToString("HH:mm:ss"));
        }
        /// <summary>
        /// Set last run date
        /// </summary>
        /// <param name="date">Date</param>
        public void AddEndDateTime(DateTime date)
        {
            AddArg(@"/ED", date.ToString("dd/mm/yyyy"));
        }
        /// <summary>
        /// Add recort to Argument collection
        /// </summary>
        /// <param name="argKey">Key</param>
        /// <param name="argValue">Value</param>
        private void AddArg(string argKey, string argValue)
        {
            if (!String.IsNullOrEmpty(argValue) && !String.IsNullOrEmpty(argKey))
            {
                Arguments.Append(argKey);
                Arguments.Append(" ");
                Arguments.Append("\"" + argValue+ "\"");
                Arguments.Append(" ");
            }
        }
        /// <summary>
        /// Set correct value of param
        /// </summary>
        /// <param name="value">param</param>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        private void RarseCoof(ref int value, int min, int max)
        {
            if (value > max)
                value = max;
            else if (value < min)
                value = min;
        }
        #endregion
    }
}
