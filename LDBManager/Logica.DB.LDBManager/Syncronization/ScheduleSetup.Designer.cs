﻿namespace Logica.LDBManager.Syncronization
{
    partial class ScheduleSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.lbPassword = new System.Windows.Forms.Label();
            this.lbUser = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.grSched = new System.Windows.Forms.GroupBox();
            this.cbSun = new System.Windows.Forms.CheckBox();
            this.cbSut = new System.Windows.Forms.CheckBox();
            this.cbFri = new System.Windows.Forms.CheckBox();
            this.cbThu = new System.Windows.Forms.CheckBox();
            this.cdWen = new System.Windows.Forms.CheckBox();
            this.cbTue = new System.Windows.Forms.CheckBox();
            this.cbMon = new System.Windows.Forms.CheckBox();
            this.lbPostfix = new System.Windows.Forms.Label();
            this.numCount = new System.Windows.Forms.NumericUpDown();
            this.lbPrefix = new System.Windows.Forms.Label();
            this.dtStart = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnSkip = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grSched.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCount)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.txtUser);
            this.groupBox1.Controls.Add(this.lbPassword);
            this.groupBox1.Controls.Add(this.lbUser);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 84);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Данные доступа";
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.Khaki;
            this.txtPassword.Location = new System.Drawing.Point(70, 47);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(190, 20);
            this.txtPassword.TabIndex = 0;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtUser
            // 
            this.txtUser.BackColor = System.Drawing.Color.Khaki;
            this.txtUser.Location = new System.Drawing.Point(70, 20);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(190, 20);
            this.txtUser.TabIndex = 20;
            this.txtUser.TabStop = false;
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Location = new System.Drawing.Point(11, 47);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(48, 13);
            this.lbPassword.TabIndex = 1;
            this.lbPassword.Text = "Пароль:";
            // 
            // lbUser
            // 
            this.lbUser.AutoSize = true;
            this.lbUser.Location = new System.Drawing.Point(11, 20);
            this.lbUser.Name = "lbUser";
            this.lbUser.Size = new System.Drawing.Size(32, 13);
            this.lbUser.TabIndex = 0;
            this.lbUser.Text = "Имя:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.grSched);
            this.groupBox2.Controls.Add(this.dtStart);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cmbType);
            this.groupBox2.Location = new System.Drawing.Point(13, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 198);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Настройка расписания запуска синхронизации";
            // 
            // grSched
            // 
            this.grSched.Controls.Add(this.cbSun);
            this.grSched.Controls.Add(this.cbSut);
            this.grSched.Controls.Add(this.cbFri);
            this.grSched.Controls.Add(this.cbThu);
            this.grSched.Controls.Add(this.cdWen);
            this.grSched.Controls.Add(this.cbTue);
            this.grSched.Controls.Add(this.cbMon);
            this.grSched.Controls.Add(this.lbPostfix);
            this.grSched.Controls.Add(this.numCount);
            this.grSched.Controls.Add(this.lbPrefix);
            this.grSched.Location = new System.Drawing.Point(10, 64);
            this.grSched.Name = "grSched";
            this.grSched.Size = new System.Drawing.Size(249, 126);
            this.grSched.TabIndex = 14;
            this.grSched.TabStop = false;
            this.grSched.Text = "Расписание";
            // 
            // cbSun
            // 
            this.cbSun.AutoSize = true;
            this.cbSun.Location = new System.Drawing.Point(135, 85);
            this.cbSun.Name = "cbSun";
            this.cbSun.Size = new System.Drawing.Size(93, 17);
            this.cbSun.TabIndex = 10;
            this.cbSun.Tag = "SUN";
            this.cbSun.Text = "Воскресенье";
            this.cbSun.UseVisualStyleBackColor = true;
            // 
            // cbSut
            // 
            this.cbSut.AutoSize = true;
            this.cbSut.Location = new System.Drawing.Point(135, 68);
            this.cbSut.Name = "cbSut";
            this.cbSut.Size = new System.Drawing.Size(67, 17);
            this.cbSut.TabIndex = 9;
            this.cbSut.Tag = "SAT";
            this.cbSut.Text = "Суббота";
            this.cbSut.UseVisualStyleBackColor = true;
            // 
            // cbFri
            // 
            this.cbFri.AutoSize = true;
            this.cbFri.Location = new System.Drawing.Point(135, 51);
            this.cbFri.Name = "cbFri";
            this.cbFri.Size = new System.Drawing.Size(69, 17);
            this.cbFri.TabIndex = 8;
            this.cbFri.Tag = "FRI";
            this.cbFri.Text = "Пятница";
            this.cbFri.UseVisualStyleBackColor = true;
            // 
            // cbThu
            // 
            this.cbThu.AutoSize = true;
            this.cbThu.Location = new System.Drawing.Point(9, 102);
            this.cbThu.Name = "cbThu";
            this.cbThu.Size = new System.Drawing.Size(68, 17);
            this.cbThu.TabIndex = 7;
            this.cbThu.Tag = "THU";
            this.cbThu.Text = "Четверг";
            this.cbThu.UseVisualStyleBackColor = true;
            // 
            // cdWen
            // 
            this.cdWen.AutoSize = true;
            this.cdWen.Location = new System.Drawing.Point(9, 85);
            this.cdWen.Name = "cdWen";
            this.cdWen.Size = new System.Drawing.Size(57, 17);
            this.cdWen.TabIndex = 6;
            this.cdWen.Tag = "WED";
            this.cdWen.Text = "Среда";
            this.cdWen.UseVisualStyleBackColor = true;
            // 
            // cbTue
            // 
            this.cbTue.AutoSize = true;
            this.cbTue.Location = new System.Drawing.Point(9, 68);
            this.cbTue.Name = "cbTue";
            this.cbTue.Size = new System.Drawing.Size(68, 17);
            this.cbTue.TabIndex = 5;
            this.cbTue.Tag = "TUE";
            this.cbTue.Text = "Вторник";
            this.cbTue.UseVisualStyleBackColor = true;
            // 
            // cbMon
            // 
            this.cbMon.AutoSize = true;
            this.cbMon.Location = new System.Drawing.Point(9, 51);
            this.cbMon.Name = "cbMon";
            this.cbMon.Size = new System.Drawing.Size(94, 17);
            this.cbMon.TabIndex = 4;
            this.cbMon.Tag = "MON";
            this.cbMon.Text = "Понедельник";
            this.cbMon.UseVisualStyleBackColor = true;
            // 
            // lbPostfix
            // 
            this.lbPostfix.AutoSize = true;
            this.lbPostfix.Location = new System.Drawing.Point(113, 24);
            this.lbPostfix.Name = "lbPostfix";
            this.lbPostfix.Size = new System.Drawing.Size(37, 13);
            this.lbPostfix.TabIndex = 14;
            this.lbPostfix.Text = "postfix";
            // 
            // numCount
            // 
            this.numCount.Location = new System.Drawing.Point(59, 22);
            this.numCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCount.Name = "numCount";
            this.numCount.Size = new System.Drawing.Size(48, 20);
            this.numCount.TabIndex = 3;
            this.numCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbPrefix
            // 
            this.lbPrefix.AutoSize = true;
            this.lbPrefix.Location = new System.Drawing.Point(6, 24);
            this.lbPrefix.Name = "lbPrefix";
            this.lbPrefix.Size = new System.Drawing.Size(32, 13);
            this.lbPrefix.TabIndex = 12;
            this.lbPrefix.Text = "prefix";
            // 
            // dtStart
            // 
            this.dtStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtStart.Location = new System.Drawing.Point(137, 37);
            this.dtStart.Name = "dtStart";
            this.dtStart.ShowUpDown = true;
            this.dtStart.Size = new System.Drawing.Size(122, 20);
            this.dtStart.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(134, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Время начала:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Назначить задание:";
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Ежеминутно ",
            "Ежечасно",
            "Ежедневно",
            "Еженедельно",
            "Ежемесячно"});
            this.cmbType.Location = new System.Drawing.Point(10, 37);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(107, 21);
            this.cmbType.TabIndex = 1;
            this.cmbType.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(124, 311);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 16;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnSkip
            // 
            this.btnSkip.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSkip.Location = new System.Drawing.Point(205, 311);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(75, 23);
            this.btnSkip.TabIndex = 18;
            this.btnSkip.Text = "Пропустить";
            this.btnSkip.UseVisualStyleBackColor = true;
            // 
            // ScheduleSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnSkip;
            this.ClientSize = new System.Drawing.Size(294, 341);
            this.Controls.Add(this.btnSkip);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScheduleSetup";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Планировщик синхронизации";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grSched.ResumeLayout(false);
            this.grSched.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.Label lbUser;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grSched;
        private System.Windows.Forms.CheckBox cbSun;
        private System.Windows.Forms.CheckBox cbSut;
        private System.Windows.Forms.CheckBox cbFri;
        private System.Windows.Forms.CheckBox cbThu;
        private System.Windows.Forms.CheckBox cdWen;
        private System.Windows.Forms.CheckBox cbTue;
        private System.Windows.Forms.CheckBox cbMon;
        private System.Windows.Forms.Label lbPostfix;
        private System.Windows.Forms.NumericUpDown numCount;
        private System.Windows.Forms.Label lbPrefix;
        private System.Windows.Forms.DateTimePicker dtStart;
        private System.Windows.Forms.Button btnSkip;
    }
}