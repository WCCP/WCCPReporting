﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;

namespace Logica.LDBManager {
    internal static class Unpacker {
        private const string DEFAULT_PUBLISHER_NAME = "EEGDADB8";
        private const string DEFAULT_DISTRIBUTOR_NAME = "EEGDADB8";
        private const string DEFAULT_INTERNET_URL = @"https://swes.ab-inbev.com/LDBSync/replisapi.dll";

        /// <summary>
        /// Check existnce of necessary assamblies
        /// </summary>
        public static void CheckDependensyExistence() {
            FileInfo fi = new FileInfo(Application.ExecutablePath);
            string currentPath = fi.Directory.FullName;

            ResourceManager res = new ResourceManager(Files.ResourceManager.BaseName, Assembly.GetExecutingAssembly());

            using (ResourceSet lResourceSet = res.GetResourceSet(CultureInfo.InvariantCulture, true, false)) {
                if (null != lResourceSet) {
                    IDictionaryEnumerator lDictionary = lResourceSet.GetEnumerator();
                    lDictionary.Reset();

                    while (lDictionary.MoveNext()) {
                        if (lDictionary.Key.ToString().Substring(0, 3) != "str")
                            continue;
                        object lKey = lDictionary.Key;
                        object lValue = lDictionary.Value;
                        if (null == lKey || null == lValue)
                            continue;

                        string lFileName = lDictionary.Value.ToString();
                        lValue = res.GetObject(lKey.ToString().Substring(3));
                        lFileName = Path.Combine(currentPath, lFileName);
                        
                        if (!File.Exists(lFileName)) {
                            if (lValue.GetType().Equals(typeof (byte[])))
                                WriteFile(lFileName, (byte[]) lValue);
                            else if (lValue.GetType().Equals(typeof (string)))
                                WriteFile(lFileName, (string) lValue);
                        }
                        //TODO: Implement file versions comparison and overwrite DLL with new version
//                        else {
//                            if (lValue.GetType().Equals(typeof (byte[]))) {
//                                
//                            }
//                        }
                    }
                }
            }
            res.ReleaseAllResources();
        }

        /// <summary>
        /// Write a file with string content form the resource
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="fileContent">File content</param>
        private static void WriteFile(string fileName, string fileContent) {
            TextWriter fs = new StreamWriter(fileName);
            fs.Write(fileContent);
            fs.Flush();
            fs.Close();
            fs.Dispose();
        }

        /// <summary>
        /// Write a file with binary content form the resource
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="fileContent">File content</param>
        private static void WriteFile(string fileName, byte[] fileContent) {
            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
            fs.Write(fileContent, 0, fileContent.Length);
            fs.Flush();
            fs.Close();
            fs.Dispose();
        }

        /// <summary>
        /// Returns embeded sql script 
        /// </summary>
        /// <returns>List of scripts</returns>
        public static SortedList<int, KeyValuePair<string, string>> GetEmbededSQLs() {
            //Dictionary<string, string> sqls = new Dictionary<string, string>();
            SortedList<int, KeyValuePair<string, string>> sqlSripts =
                new SortedList<int, KeyValuePair<string, string>>();
            ResourceManager rm = new ResourceManager(SQLFiles.ResourceManager.BaseName, Assembly.GetExecutingAssembly());

            using (ResourceSet resSet = rm.GetResourceSet(CultureInfo.InvariantCulture, true, false)) {
                if (null != resSet) {
                    IDictionaryEnumerator collection = resSet.GetEnumerator();
                    collection.Reset();

                    while (collection.MoveNext()) {
                        string key = collection.Key.ToString();

                        if (!key.Substring(0, 3).Equals("str", StringComparison.InvariantCultureIgnoreCase))
                            continue;
                        string sqlValue = rm.GetString(key.Substring(3));
                        string publisherName = !string.IsNullOrEmpty(Properties.Settings.Default.PublisherName)
                                                   ? Properties.Settings.Default.PublisherName
                                                   : DEFAULT_PUBLISHER_NAME;
                        string distributorName = !string.IsNullOrEmpty(Properties.Settings.Default.DistributorName)
                                                     ? Properties.Settings.Default.DistributorName
                                                     : DEFAULT_DISTRIBUTOR_NAME;
                        string internetURL = !string.IsNullOrEmpty(Properties.Settings.Default.InternetURL)
                                                 ? Properties.Settings.Default.InternetURL
                                                 : DEFAULT_INTERNET_URL;
                        sqlValue =
                            sqlValue.Replace("{PublisherName}", publisherName).Replace("{DistributorName}",
                                distributorName).Replace("{InternetUrl}", internetURL);
                        KeyValuePair<string, string> scr = new KeyValuePair<string, string>(sqlValue, key.Substring(3));
                        sqlSripts.Add(Convert.ToInt32(collection.Value), scr);
                    }
                }
            }
            rm.ReleaseAllResources();
            return sqlSripts;
        }
    }
}