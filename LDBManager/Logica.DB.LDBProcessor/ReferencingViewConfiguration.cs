﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using NLog;

namespace Logica.DB.LDBProcessor {
    /// <summary>
    /// Read configuration for creation of referencing views in LDB
    /// </summary>
    internal class ReferencingViewConfiguration {
        private static SqlObjectExceptions _sqlObjectsExceptions;
        private static QueryToLDBViews _queryToLDBViews;
        private static DirectSQLViewList _directSQLViewList;
        private readonly Logger _logger = LoggerHelper.Logger;

        private const string XML_FILE_NAME = "elimination.xml";
        private const string X_PATH_PATTERN0 = "//{0}";
        private static readonly ReferencingViewConfiguration _instance = new ReferencingViewConfiguration();

        private ReferencingViewConfiguration() {
            XmlDocument lXmlDoc = new XmlDocument();
            XmlNode lXmlNode;

            FileInfo lFileInfo = new FileInfo(Application.ExecutablePath);
            if (File.Exists(Path.Combine(lFileInfo.DirectoryName, XML_FILE_NAME)))
                try {
                    lXmlDoc.Load(Path.Combine(lFileInfo.DirectoryName, XML_FILE_NAME));

                    XmlSerializer lXmlSerializer;
                    lXmlNode =
                        lXmlDoc.SelectSingleNode(string.Format(X_PATH_PATTERN0,
                            LDBConfigurationXmlNodes.ArrayOfSqlObjectException));
                    if (null != lXmlNode) {
                        lXmlSerializer = new XmlSerializer(typeof (SqlObjectExceptions));
                        using (StringReader lReader = new StringReader(lXmlNode.OuterXml)) {
                            _sqlObjectsExceptions = (SqlObjectExceptions) lXmlSerializer.Deserialize(lReader);
                        }
                    }

                    lXmlNode =
                        lXmlDoc.SelectSingleNode(string.Format(X_PATH_PATTERN0,
                            LDBConfigurationXmlNodes.ArrayOfQueryToLDBView));
                    if (null != lXmlNode) {
                        lXmlSerializer = new XmlSerializer(typeof (QueryToLDBViews));
                        using (StringReader lReader = new StringReader(lXmlNode.OuterXml)) {
                            _queryToLDBViews = (QueryToLDBViews) lXmlSerializer.Deserialize(lReader);
                        }
                    }

                    lXmlNode =
                        lXmlDoc.SelectSingleNode(string.Format(X_PATH_PATTERN0,
                            LDBConfigurationXmlNodes.ArrayOfDirectSQL));
                    if (null != lXmlNode) {
                        XmlRootAttribute lXmlRootAttribute = new XmlRootAttribute();
                        lXmlRootAttribute.ElementName = LDBConfigurationXmlNodes.ArrayOfDirectSQL.ToString();
                        lXmlRootAttribute.IsNullable = true;
                        lXmlSerializer = new XmlSerializer(typeof (DirectSQLViewList), lXmlRootAttribute);
                        using (StringReader lReader = new StringReader(lXmlNode.OuterXml)) {
                            _directSQLViewList = (DirectSQLViewList) lXmlSerializer.Deserialize(lReader);
                        }
                    }
                }
                catch (Exception lException) {
                    _logger.ErrorException("Exception: ", lException);
                }
        }

        public DirectSQLViewList DirectSQLViews {
            get { return _directSQLViewList; }
        }

        /// <summary>
        /// List of tables not to be created in LDB
        /// </summary>
        public SqlObjectExceptions SqlObjectsExceptions {
            get { return _sqlObjectsExceptions; }
        }

        /// <summary>
        /// List of tables with specific query for creation in LDB
        /// </summary>
        public QueryToLDBViews QueryToLDBViews {
            get { return _queryToLDBViews; }
        }

        internal static ReferencingViewConfiguration Instance {
            get { return _instance; }
        }

        #region Nested type: LDBConfigurationXmlNodes

        /// <summary>
        /// Nodes in the elimination.xml file
        /// </summary>
        private enum LDBConfigurationXmlNodes {
            /// <summary>
            /// Describes collection of table names not to be created in LDB
            /// </summary>
            ArrayOfSqlObjectException,

            /// <summary>
            /// Describes collection of table names with specific definition in LDB view
            /// </summary>
            ArrayOfQueryToLDBView,

            /// <summary>
            /// Describes collection of direct SQL statements for creating views in LDB
            /// </summary>
            ArrayOfDirectSQL
        }

        #endregion
    }
}