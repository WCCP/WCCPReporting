﻿using System;
using System.Collections.Generic;

namespace Logica.DB.LDBProcessor {
    internal class SqlServerTables : Dictionary<string, SqlTableDescription> {
        private readonly SqlScriptsSubFolders _entityType;

        internal SqlScriptsSubFolders EntityType {
            get { return _entityType; }
        }

        internal SqlServerTables()
            : this(SqlScriptsSubFolders.Tables) {
        }

        internal SqlServerTables(SqlScriptsSubFolders entityType) {
            if (entityType != SqlScriptsSubFolders.Tables && entityType != SqlScriptsSubFolders.Views) {
                throw new Exception(string.Format("Value {0} is unacceptable", entityType));
            }
            _entityType = entityType;
        }

        internal string DBName {
            get {
                foreach (SqlTableDescription lSqlTableDescription in Values) {
                    return lSqlTableDescription.DBName;
                }
                return "";
            }
        }

        internal List<string> TableSchemas {
            get {
                List<string> lTableSchemas = new List<string>();
                foreach (SqlTableDescription lSqlTableDescription in Values) {
                    if (!lTableSchemas.Contains(lSqlTableDescription.Schema))
                        lTableSchemas.Add(lSqlTableDescription.Schema);
                }
                return lTableSchemas;
            }
        }
    }
}