﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Security;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Logica.DB.LDBProcessor
{
    internal enum TablesColumn
    {
        TABLE_QUALIFIER,
        TABLE_OWNER,
        TABLE_SCHEMA,
        TABLE_NAME,
        COLUMN_NAME,
        COLUMN_ID,
        SYSTEM_TYPE_ID,
        IS_PK
    }

    public class SqlServerManager
    {
        //private static string CONNECTION_STRING_TEMPLATE = "Data Source={0};Initial Catalog={1};Integrated Security=True";
        //private static string CONNECTION_STRING_USER_PART = ";User ID={0};Password={1}";
        private const string SALESWORKS_ENTERPRISE_REG_KEY = @"SOFTWARE\SoftServe\SalesWorks Enterprise";
        private const string SALESWORKS_ENTERPRISE_REG_KEY_PATH_VALUE = "Path";

        private const string LOGICA_DB_NAME = "LDB";
        private const string ERROR_MESG_CANNOT_GET_DATABASES = "Can not get datatbases names!";
        private static bool _skipSqlObjects;

        private static SqlConnectionStringBuilder _connectionString;
        private const string COLUMN_NAME_DB_NAME = "DATABASE_NAME";
        private static readonly SqlServerManager _current = new SqlServerManager();
        private readonly ConnectionsCollection _connectionDescriptions = new ConnectionsCollection();
        private readonly Dictionary<string, List<string>> _databases = new Dictionary<string, List<string>>();
        private List<string> _servers;

        private readonly Dictionary<ConnectionDescription, SqlServerTables> _tables =
            new Dictionary<ConnectionDescription, SqlServerTables>();

        private readonly SqlObjectExceptions _sqlObjectExceptions;

        private SqlServerManager()
        {
            _sqlObjectExceptions = ReferencingViewConfiguration.Instance.SqlObjectsExceptions;
        }

        #region Public methods

        ///<summary>
        /// Collection of SQL Server connections.
        ///</summary>
        public ConnectionsCollection SqlConnections
        {
            get { return _connectionDescriptions; }
        }
        
        public List<string> GetServerNames()
        {
            _servers = new List<string>();

            SqlDataSourceEnumerator lSrvs = SqlDataSourceEnumerator.Instance;
            DataTable lDataTable = lSrvs.GetDataSources();
            if (lDataTable.Rows.Count > 0)
            {
                foreach (DataRow lRow in lDataTable.Rows)
                {
                    string lInstanceName = lRow[lDataTable.Columns["InstanceName"]].ToString();
                    _servers.Add(lRow[lDataTable.Columns["ServerName"]]
                                 + (string.IsNullOrEmpty(lInstanceName) ? "" : "\\" + lInstanceName));
                }
            }
            return _servers;
        }

        public List<string> GetDatabases(string serverName)
        {
            return GetDatabases(serverName, null, null);
        }

        public List<string> GetDatabases(string serverName, string userId, string password)
        {
            if (_databases.ContainsKey(serverName))
                return _databases[serverName];
            
            List<string> lDatabases = GetDatabasesList(serverName, userId, password);

            _databases.Add(serverName, lDatabases);

            if (null == lDatabases)
            {
                MessageBox.Show(ERROR_MESG_CANNOT_GET_DATABASES, Application.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return null;
            }
            return lDatabases;
        }

        public List<string> GetSWDatabases(string serverName, string userId, string password)
        {
            List<string> lDBNames = GetDatabases(serverName, userId, password);

            List<string> lSWDbNames = new List<string>();

            if (null != lDBNames)
            {
                foreach (string lDBName in lDBNames)
                {
                    if (!IsLDB(lDBName))
                    {
                        string lConnectionString = ConnectionStringMake(serverName, lDBName, userId, password);

                        if (DataProvider.IsSWdb(lConnectionString))
                        {
                            lSWDbNames.Add(lDBName.Clone().ToString());
                        }
                    }
                }
            }
            return lSWDbNames;
        }

        private static bool IsLDB(string lDBName)
        {
            return lDBName.Equals(LOGICA_DB_NAME, StringComparison.InvariantCultureIgnoreCase);
        }

        public bool CheckConnection(string serverName, string database, out string errorMessage)
        {
            return CheckConnection(serverName, database, null, null, out errorMessage);
        }

        public bool CheckConnection(string serverName, string database, string userName, string password,
                                    out string errorMessage)
        {
            errorMessage = "";
            string lConnectionString;

            try
            {
                lConnectionString = ConnectionStringMake(serverName, database, userName, password);
            }
            catch (Exception lException)
            {
                errorMessage = lException.Message;
                return false;
            }

            Exception lConnectionFailedException = DataProvider.CheckConnectionString(lConnectionString);
            if (lConnectionFailedException != null)
                errorMessage = lConnectionFailedException.Message;

            return lConnectionFailedException == null;
        }

        public void AddConnection(ConnectionDescription connectionDescription)
        {
            foreach (ConnectionDescription lItem in _connectionDescriptions)
            {
                if (lItem.Equals(connectionDescription))
                    return;
            }
            _connectionDescriptions.Add(connectionDescription);
        }

        public void AddConnection(string serverName, string datatbsaeName)
        {
            AddConnection(new ConnectionDescription(serverName, datatbsaeName));
        }

        public void AddConnection(string serverName, string datatbsaeName, string userName, string userPassword)
        {
            AddConnection(new ConnectionDescription(serverName, datatbsaeName, userName, userPassword));
        }

        internal void RemoveConnection(ConnectionDescription connectionDescription)
        {
            foreach (ConnectionDescription item in _connectionDescriptions)
            {
                if (item.Equals(connectionDescription))
                {
                    _connectionDescriptions.Remove(item);
                    return;
                }
            }
        }

        public void RemoveConnectionByValues(string srvName, string batabaseName)
        {
            foreach (ConnectionDescription lItem in _connectionDescriptions)
            {
                if (lItem.ServerName.Equals(srvName, StringComparison.InvariantCultureIgnoreCase) &&
                    lItem.DataBase.Equals(batabaseName, StringComparison.InvariantCultureIgnoreCase))
                {
                    _connectionDescriptions.Remove(lItem);
                    return;
                }
            }
        }

        internal void RemoveAllConnection()
        {
            _connectionDescriptions.Clear();
        }
        
        private List<string> GetDatabasesList(string serverName, string userId, string password)
        {
            List<string> dbList = new List<string>();
            try
            {
                string connectionString = ConnectionStringMake(serverName, "", userId, password);
                DataSet ds = DataProvider.GetDatabases(connectionString);

                DataTable dt = ds.Tables[0];
                foreach (DataRow r in dt.Rows)
                {
                    string dbName = r[COLUMN_NAME_DB_NAME].ToString();
                    if (!dbName.Equals("model") && !dbName.Equals("master") && !dbName.Equals("msdb")
                        && !dbName.Equals("tempdb"))
                        dbList.Add(dbName);
                }
            }
            catch
            {
                return null;
            }

            return dbList;
        }

        internal string DeleteReferencedViews(ConnectionDescription LDBConnection, SqlServerTablesCollection srvTables,
                                              SqlServerTablesCollection views)
        {
            string errorMessage = null;
            List<SqlServerTables> SWDBObjects = new List<SqlServerTables>(srvTables);
            SWDBObjects.AddRange(views);
            try
            {
                SqlServerTables LDBViews = new SqlServerTables(SqlScriptsSubFolders.Views);
                LDBViews = Current.GetViewsNameList(LDBConnection);
                foreach (SqlTableDescription tblDescription in LDBViews.Values)
                {
                    bool allowDrop = false;
                    foreach (SqlServerTables item in SWDBObjects)
                    {
                        if (item.ContainsValue(tblDescription))
                        {
                            allowDrop = true;
                        }
                    }
                    if (!allowDrop)
                    {
                        continue;
                    }
                    LoggerHelper.Logger.Info(string.Format("Delete {0}.", tblDescription.ShortAlias));
                    try
                    {
                        DataProvider.DropViewIfExists(LDBConnection, tblDescription.ShortAlias);
                    }
                    catch
                    {
                        LoggerHelper.Logger.Info(string.Format("The {0} was not deleted.", tblDescription.FullAlias));
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return errorMessage;
        }



        internal SqlServerTables GetViewsNameList(ConnectionDescription sqlConnection)
        {
            List<string> lSchemaList = DataProvider.GetDbSchemas(sqlConnection.SqlConnectionString.ConnectionString);

            if (0 == lSchemaList.Count)
            {
                lSchemaList.Add("dbo");
            }

            SqlServerTables lViews = new SqlServerTables(SqlScriptsSubFolders.Views);

            foreach (string lSchema in lSchemaList)
            {
                DataTable lViewsTable = DataProvider.GetViewsName(sqlConnection.SqlConnectionString.ToString(), lSchema);
                DataTable lFieldsTable = DataProvider.GetViewFieldsDescription(sqlConnection);
                foreach (DataRow lDataRow in lViewsTable.Rows)
                {
                    string lTableName = lDataRow[TablesColumn.TABLE_NAME.ToString()].ToString();
                    if (!_skipSqlObjects && IsExceptionObject(lTableName, lSchema))
                        continue;

                    Dictionary<string, SqlFieldDescription> lFields = GetFieldsDescription(lFieldsTable, lTableName, lSchema);
                    SqlTableDescription lTableDescription = new SqlTableDescription(
                        lDataRow[TablesColumn.TABLE_QUALIFIER.ToString()].ToString(),
                        lDataRow[TablesColumn.TABLE_OWNER.ToString()].ToString(),
                        lTableName, lFields);
                    lViews.Add(lTableDescription.ShortAlias.ToLower(), lTableDescription);
                }
            }
            return lViews;
        }


        internal SqlServerTables GetTablesNameList(ConnectionDescription sqlConnection)
        {
            if (!_tables.ContainsKey(sqlConnection))
            {
                SqlServerTables lTables = new SqlServerTables();
                DataTable lTablesName = DataProvider.GetTablesName(sqlConnection.SqlConnectionString.ToString());
                DataTable lTableFields = DataProvider.GetFieldsDescription(sqlConnection);

                if (null == lTablesName)
                    return null;

                foreach (DataRow lDataRow in lTablesName.Rows)
                {
                    string lTableName = lDataRow[TablesColumn.TABLE_NAME.ToString()].ToString();
                    string lSchema = lDataRow[TablesColumn.TABLE_OWNER.ToString()].ToString();
                    if (!_skipSqlObjects && IsExceptionObject(lTableName, lSchema))
                        continue;

                    Dictionary<string, SqlFieldDescription> lFields = GetFieldsDescription(lTableFields, lTableName, lSchema);

                    SqlTableDescription lTableDescription = new SqlTableDescription(
                        lDataRow[TablesColumn.TABLE_QUALIFIER.ToString()].ToString(),
                        lSchema, lTableName, lFields);
                    lTables.Add(lTableDescription.ShortAlias.ToLower(), lTableDescription);
                }
                _tables.Add(sqlConnection, lTables);
            }
            return _tables[sqlConnection];
        }

        private bool IsExceptionObject(string tableName, string schema)
        {
            string lEntityName = schema + "." + tableName;
            if (_sqlObjectExceptions.Contains(schema, tableName))
                return true;

            foreach (SqlObjectException lObject in _sqlObjectExceptions)
            {
                if (!lObject.ObjectName.Contains("*") && !lObject.ObjectName.Contains("?"))
                    continue;
                string lPattern = WildcardToRegex(lObject.ObjectName);
                if (Regex.IsMatch(lEntityName, lPattern, RegexOptions.IgnoreCase))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns regex pattern string from wildcard string (with * and ?)
        /// </summary>
        /// <param name="pattern">string with wildcard chars * and ?</param>
        /// <returns></returns>
        public static string WildcardToRegex(string pattern)
        {
            return "^" + Regex.Escape(pattern).Replace("\\*", ".*").Replace("\\?", ".") + "$";
        }

        public Dictionary<LBDDataFileType, string> GetLDBFilesPathes()
        {
            if (_connectionDescriptions.Count == 0)
                return null;

            Dictionary<LBDDataFileType, string> result = new Dictionary<LBDDataFileType, string>();
            DataTable dt = DataProvider.GetLDBFilesPathes(_connectionDescriptions[0].ConnectionToLDB);

            foreach (LBDDataFileType item in Enum.GetValues(typeof(LBDDataFileType)))
            {
                DataRow[] rows =
                    dt.Select(string.Format("[{0}] = {1}", LDBDataFilesPathesFields.type.ToString(), (int)item));
                if (rows.Length > 0)
                {
                    result.Add(item, (rows[0][LDBDataFilesPathesFields.physical_name.ToString()]).ToString());
                }
            }

            return result;
        }

        public Dictionary<LBDDataFileType, string> GetDefaultSLQDataFilesPathes()
        {
            if (_connectionDescriptions.Count == 0)
                throw new IndexOutOfRangeException("The collection of connection is empty!");

            Dictionary<LBDDataFileType, string> result = new Dictionary<LBDDataFileType, string>();
            DataSet ds = DataProvider.GetDefaultDataFilesPathes(_connectionDescriptions[0]);
            if (null != ds)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string ldbFileName =
                        GetLDBFileName((dt.Rows[0][SQLDefaultDataFilesPathesFields.DefaultFile.ToString()]).ToString(),
                            LBDDataFileType.ROWS);
                    result.Add(LBDDataFileType.ROWS, ldbFileName);
                    ldbFileName =
                        GetLDBFileName((dt.Rows[0][SQLDefaultDataFilesPathesFields.DefaultLog.ToString()]).ToString(),
                            LBDDataFileType.LOG);
                    result.Add(LBDDataFileType.LOG, ldbFileName);
                }
            }
            return result;
        }

        public Dictionary<LBDDataFileType, string> GetSalesWorksDataFilesPath()
        {
            Dictionary<LBDDataFileType, string> result = new Dictionary<LBDDataFileType, string>();
            try
            {
                RegistryKey regKey = Registry.LocalMachine;

                regKey = regKey.OpenSubKey(SALESWORKS_ENTERPRISE_REG_KEY, false);

                if (null == regKey)
                {
                    return result;
                }

                object pathValue = regKey.GetValue(SALESWORKS_ENTERPRISE_REG_KEY_PATH_VALUE, string.Empty,
                    RegistryValueOptions.None);

                if (null != pathValue)
                {
                    string ldbFileName = GetLDBFileName(pathValue.ToString(), LBDDataFileType.ROWS);
                    result.Add(LBDDataFileType.ROWS, ldbFileName);
                    ldbFileName = GetLDBFileName(pathValue.ToString(), LBDDataFileType.LOG);
                    result.Add(LBDDataFileType.LOG, ldbFileName);
                }
            }
            catch (Exception ex)
            {
                if (typeof(UnauthorizedAccessException).Equals(ex.GetType())
                    || typeof(SecurityException).Equals(ex.GetType()))
                {
                    throw new Exception(string.Format("You don't have permissions to open [{0}]!",
                        "HKEY_LOCAL_MACHINE" + SALESWORKS_ENTERPRISE_REG_KEY));
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }

        public void SetSkipingSqlOjects(bool skipSqlObjects)
        {
            _skipSqlObjects = skipSqlObjects;
        }

        #endregion

        #region Static methods

        public static string LogicaDatabaseName
        {
            get { return LOGICA_DB_NAME; }
        }

        public static SqlServerManager Current
        {
            get { return _current; }
        }

        #endregion

        #region Private

        private string ConnectionStringMake(string serverName, string database, string userName, string password)
        {
            ConnectionDescription con = _connectionDescriptions.Find(serverName, database, userName, password);
            if (null == con)
            {
                con = new ConnectionDescription(serverName, database, userName, password);
                //			_connectionDescriptions.Add(con);
            }
            _connectionString = con.SqlConnectionString;
            return _connectionString.ConnectionString;
        }

        public string GetLDBFileName(string filePath, LBDDataFileType fileType)
        {
            string fileNameTeplate = LOGICA_DB_NAME + (fileType == LBDDataFileType.LOG ? "{0}_log.ldf" : "{0}.mdf");
            string fileName = string.Format(fileNameTeplate, "");
            int cnt = 0;
            while (File.Exists(Path.Combine(filePath, fileName)))
            {
                fileName = string.Format(fileNameTeplate, ++cnt);
            }
            return Path.Combine(filePath, fileName);
        }

        private static Dictionary<string, SqlFieldDescription> GetFieldsDescription(DataTable dt, string tableName, string schema)
        {
            Dictionary<string, SqlFieldDescription> lFields = new Dictionary<string, SqlFieldDescription>();

            DataRow[] lRows = dt.Select(string.Format("[{0}] = '{1}' AND [{2}] = '{3}'",
                TablesColumn.TABLE_SCHEMA.ToString(), schema,
                TablesColumn.TABLE_NAME.ToString(), tableName));

            foreach (DataRow lRow in lRows)
            {
                string lFieldName = lRow[TablesColumn.COLUMN_NAME.ToString()].ToString();
                lFields.Add(lFieldName, new SqlFieldDescription(
                    (byte)lRow[TablesColumn.SYSTEM_TYPE_ID.ToString()],
                    Convert.ToBoolean(lRow[TablesColumn.IS_PK.ToString()])));
            }
            return lFields;
        }

        #endregion
    }
}