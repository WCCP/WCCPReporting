﻿using System;

namespace Logica.DB.LDBProcessor {
    ///<summary>
    /// Describes current process type.
    ///</summary>
    public enum LDBProcessLBDEntityEnum {
        ///<summary>
        /// Create database process type.
        ///</summary>
        CreateLDB,

        ///<summary>
        /// Create subscription type.
        ///</summary>
        CreateSubcription,

        ///<summary>
        /// Create scripts type.
        ///</summary>
        CreateScripts,

        ///<summary>
        /// Check DB equivalence type
        ///</summary>
        DatabaseEquivalence
    }

    ///<summary>
    /// 
    ///</summary>
    ///<param name="sender"></param>
    ///<param name="args"></param>
    public delegate void LDBProcessEventHandler(object sender, LDBProcessEventArgs args);

    ///<summary>
    /// Event args for processing.
    ///</summary>
    public class LDBProcessEventArgs : EventArgs {
        private const string MESSAGE_TEMPLATE = "{0}: {1} - {2}";

        private readonly string _ldbEntityType = SqlScriptsSubFolders.Tables.ToString();
        private readonly LDBProcessLBDEntityEnum _processType;

        private string _entityName = string.Empty;

        ///<summary>
        /// Creates new instance of LDBProcessEventArgs
        ///</summary>
        ///<param name="ldbEntityType">Entity type</param>
        ///<param name="processType">process type</param>
        public LDBProcessEventArgs(string ldbEntityType, LDBProcessLBDEntityEnum processType) {
            _ldbEntityType = ldbEntityType;
            _processType = processType;
        }

        ///<summary>
        /// Entity type.
        ///</summary>
        public string LDBEntityType {
            get { return _ldbEntityType; }
        }

        ///<summary>
        /// Name of entity.
        ///</summary>
        public string EntityName {
            set { _entityName = value; }
        }

        ///<summary>
        /// Process type.
        ///</summary>
        public LDBProcessLBDEntityEnum ProcessType {
            get { return _processType; }
        }

        ///<summary>
        /// returns message
        ///</summary>
        public string ProcessMessage {
            get { return string.Format(MESSAGE_TEMPLATE, _processType, _ldbEntityType, _entityName); }
        }
    }
}