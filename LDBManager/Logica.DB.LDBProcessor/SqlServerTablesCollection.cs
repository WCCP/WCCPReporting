﻿using System.Collections.Generic;

namespace Logica.DB.LDBProcessor {
    internal class SqlServerTablesCollection : List<SqlServerTables> {
        public List<string> DBNameList {
            get {
                List<string> lResult = new List<string>();
                foreach (SqlServerTables lSqlServerTables in this) {
                    lResult.Add(lSqlServerTables.DBName);
                }
                return lResult;
            }
        }
    }
}