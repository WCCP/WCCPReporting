﻿using System.Collections.Generic;

namespace Logica.DB.LDBProcessor {
    /// <summary>
    /// Connection descriptions collection
    /// </summary>
    public class ConnectionsCollection : List<ConnectionDescription> {
        /// <summary>
        /// Signals whether the LDB exists on an SQL Server instans where connections collection from
        /// </summary>
        public bool HasLDB {
            get { return Count > 0 && this[0].LDBExists; }
        }

        public ConnectionDescription Find(ConnectionDescription connection) {
            foreach (ConnectionDescription conn in this) {
                if (conn.Equals(connection)) {
                    return conn;
                }
            }
            return null;
        }

        public ConnectionDescription Find(string serverName, string database, string userName, string password) {
            foreach (ConnectionDescription conn in this)
                if (conn.Equals(serverName, database, userName, password))
                    return conn;
            return null;
        }
    }
}