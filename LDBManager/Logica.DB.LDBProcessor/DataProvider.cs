﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using BLToolkit.Data;

namespace Logica.DB.LDBProcessor
{
    public static class DataProvider
    {
        private static List<DbManager> _dbManagers = new List<DbManager>();

        private static string SQL_GET_DATABASES = "sp_databases";
        private static string SQL_CHECK_DB_IS_SW = @"SELECT id FROM sysobjects WHERE xtype = 'U' AND name = 'tblCustomers'";
        private static string SQL_GET_VIEWS = "exec sp_tables  @table_type='''VIEW''', @table_owner = '{0}'";
        private static string SQL_GET_TABLES = "exec sp_tables  @table_type='''TABLE'''";
        private static string SQL_CHECK_OBJECT_EXISTANCE = "select 1 from dbo.sysobjects where id = object_id(N'{0}')";

        public static Exception CheckConnectionString(string connectionString)
        {
            Exception lException;
            CheckConnectionString(connectionString, out lException);
            return lException;
        }

        private static void CheckConnectionString(string connectionString, out Exception exception)
        {
            try
            {
                using (SqlConnection lConn = new SqlConnection(connectionString))
                {
                    lConn.Open();
                    exception = (lConn.State == ConnectionState.Open) ? null : new Exception(connectionString, null);
                }
            }
            catch (Exception lException)
            {
                exception = new Exception(connectionString, lException);
            }
        }

        private static DbManager GetDbManager(string connectionString)
        {
            DbManager lDb = _dbManagers.FirstOrDefault(m => m.Connection.ConnectionString == connectionString);

            if (lDb == null)
            {
                DbManager.AddConnectionString(connectionString);

                lDb = new DbManager();
                lDb.Command.CommandTimeout = 20 * 600;

                _dbManagers.Add(lDb);
            }

            return lDb;
        }

        private static DbManager GetDbManager(ConnectionDescription connection)
        {
            return GetDbManager(connection.SqlConnectionString.ToString());
        }

        public static bool IsSWdb(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            object res = lDb.SetCommand(SQL_CHECK_DB_IS_SW).ExecuteScalar();

            return res != null;
        }

        public static DataSet GetDatabases(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            DataSet res = lDb.SetSpCommand(SQL_GET_DATABASES).ExecuteDataSet();

            return res;
        }

        public static DataTable GetViewsName(string connectionString, string owner)
        {
            DbManager lDb = GetDbManager(connectionString);
            string lScript = String.Format(SQL_GET_VIEWS, owner);

            DataTable res = lDb.SetCommand(lScript).ExecuteDataTable();

            return res;
        }

        public static DataTable GetTablesName(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            DataTable res = lDb.SetCommand(SQL_GET_TABLES).ExecuteDataTable();

            return res;
        }

        public static List<string> GetDbSchemas(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);

            DataTable schemasTable = lDb.SetCommand(CreateLDBScripts.GetDBSchemas).ExecuteDataTable();

            List<string> schemas = (from DataRow row in schemasTable.Rows select (string)row[0]).ToList();

            return schemas;
        }

        public static string CreateLDB(string connectionString, Dictionary<LBDDataFileType, string> LDBFiles)
        {
            DbManager lDb = GetDbManager(connectionString);
            string result = String.Empty;

            try
            {
                lDb.SetCommand(CreateLDBScripts.CretaeLDBScript(LDBFiles[LBDDataFileType.ROWS], LDBFiles[LBDDataFileType.LOG])).ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        public static bool CheckLDBExistance(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            object result = lDb.SetCommand(CreateLDBScripts.CheckLDBExistence).ExecuteScalar();

            return result != null;
        }

        public static DBObjectTypes GetDBObjectType(string objectName, string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            DBObjectTypes lResult = DBObjectTypes.Undefined;

            string lGetObjectType = String.Format(CreateLDBScripts.GetDBObjectType, objectName);
            object lDBObjectType = lDb.SetCommand(lGetObjectType).ExecuteScalar();

            if (lDBObjectType != null)
            {
                lResult = (DBObjectTypes)Enum.Parse(typeof(DBObjectTypes), lDBObjectType.ToString());
            }

            return lResult;
        }

        public static string UnsubscribeReplication(ConnectionDescription connectToLDB, string ldbName)
        {
            if (!connectToLDB.DataBase.Equals(ldbName, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Exception("Connection MUST be to LDB!!!!!");
            }
            string result = String.Empty;

            try
            {
                DbManager lDb = GetDbManager(connectToLDB.SqlConnectionString.ToString());
                lDb.SetCommand(CreateLDBScripts.GetUnsubscribeRepliaction).ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        public static bool DropLDB(ConnectionDescription sqlConnection, string ldbName, out string errorMessage)
        {
            errorMessage = String.Empty;

            try
            {
                ClearConnections();

                if (sqlConnection.LDBExists && !sqlConnection.DataBase.Equals(ldbName, StringComparison.InvariantCultureIgnoreCase))
                {
                    DbManager lDb = GetDbManager(sqlConnection);
                    lDb.SetCommand(CreateLDBScripts.GetDropLDBScript).ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return false;
            }

            return true;
        }

        private static void ClearConnections()
        {
            foreach (var manager in _dbManagers)
            {
                manager.Connection.Close();
                manager.Close();
            }

            SqlConnection.ClearAllPools();

            _dbManagers.Clear();
        }

        public static DataTable GetFieldsDescription(ConnectionDescription sqlConnection)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            DataTable ds = lDb.SetCommand(CreateLDBScripts.GetFiledsDescription).ExecuteDataTable();

            return ds;
        }

        public static DataTable GetViewFieldsDescription(ConnectionDescription sqlConnection)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            DataTable ds = lDb.SetCommand(CreateLDBScripts.GetViewFiledsDescription).ExecuteDataTable();

            return ds;
        }

        public static DataSet GetDefaultDataFilesPathes(ConnectionDescription sqlConnection)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            DataSet ds = lDb.SetCommand(CreateLDBScripts.DefaultDataFilesPathes).ExecuteDataSet();

            return ds;
        }

        public static void DropViewIfExists(ConnectionDescription sqlConnection, string viewName)
        {
            string checkExistance = String.Format(CreateLDBScripts.DropViewIfExistsScript, viewName);

            DbManager lDb = GetDbManager(sqlConnection);
            lDb.SetCommand(checkExistance).ExecuteNonQuery();
        }

        public static bool CheckViewExistance(ConnectionDescription connectionString, string name)
        {
            DbManager lDb = GetDbManager(connectionString);
            object result = lDb.SetCommand(string.Format(SQL_CHECK_OBJECT_EXISTANCE, name)).ExecuteScalar();

            return result != null;
        }

        public static void CreateViewDumbIfNotExists(ConnectionDescription sqlConnection, string viewName)
        {
            string checkExistance = String.Format(CreateLDBScripts.IfNotExistsViewScript, viewName);

            DbManager lDb = GetDbManager(sqlConnection);
            lDb.SetCommand(checkExistance).ExecuteNonQuery();
        }

        public static bool SchemaExists(ConnectionDescription sqlConnection, string schemaName)
        {
            DbManager lDb = GetDbManager(sqlConnection);

            string lSerSql = String.Format(CreateLDBScripts.IfExistsSchemaScript, schemaName);
            int res = lDb.SetCommand(lSerSql).ExecuteScalar<int>();

            return res != 0;
        }

        public static void CreateSchema(ConnectionDescription sqlConnection, string schemaName)
        {
            DbManager lDb = GetDbManager(sqlConnection);

            string lSerSql = String.Format(CreateLDBScripts.CreateSchemaScript, schemaName);
            lDb.SetCommand(lSerSql).ExecuteNonQuery();
        }

        public static DataTable GetLDBFilesPathes(ConnectionDescription sqlConnection)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            DataTable ds = lDb.SetCommand(CreateLDBScripts.LDBFilesPathes).ExecuteDataTable();

            return ds;
        }

        public static void ExecuteNonQuery(ConnectionDescription sqlConnection, string script)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            lDb.SetCommand(script).ExecuteNonQuery();
        }

        public static void DropObject(ConnectionDescription sqlConnection, SqlScriptsSubFolders objectType, string objectName)
        {
            string lDropScript = GetLDBDScript(objectType);

            if (!string.IsNullOrEmpty(lDropScript))
            {
                ExecuteNonQuery(sqlConnection, string.Format(lDropScript, objectName));
            }
        }

        public static string GetLDBDScript(SqlScriptsSubFolders dbObjectType)
        {
            string script = String.Empty;
            switch (dbObjectType)
            {
                case SqlScriptsSubFolders.Tables:
                    throw new NotImplementedException(SqlScriptsSubFolders.Tables.ToString());
                case SqlScriptsSubFolders.Indexes:
                    throw new NotImplementedException(SqlScriptsSubFolders.Tables.ToString());
                case SqlScriptsSubFolders.Keys:
                    throw new NotImplementedException(SqlScriptsSubFolders.Tables.ToString());
                case SqlScriptsSubFolders.SP:
                    return CreateLDBScripts.DropSPIfExistsScript;
                case SqlScriptsSubFolders.UDF:
                    return CreateLDBScripts.DropUDFIfExistsScript;
                case SqlScriptsSubFolders.Views:
                    return CreateLDBScripts.DropViewIfExistsScript;
            }
            return script;
        }

        public static void AlterView(ConnectionDescription sqlConnection, string schemaName, string name, string directQuery)
        {
            DbManager lDb = GetDbManager(sqlConnection);

            string lSerSql = string.Format(ViewQueryBuilder.ALTER_VIEW_SCRIPT, schemaName, name) + directQuery;
            lDb.SetCommand(lSerSql).ExecuteNonQuery();
        }

        public static int CheckOverlapping(ConnectionDescription connectionString, string script)
        {
            DbManager lDb = GetDbManager(connectionString);
            int result = lDb.SetCommand(script).ExecuteScalar<int>();

            return result;
        }


    }
}
