﻿namespace Logica.DB.LDBProcessor {
    internal static class CreateLDBScripts {
        #region SQLScripts

        private static string CREATAE_LDB_SCRIPT =
            @"CREATE DATABASE LDB ON PRIMARY 
( NAME = N'LDB', FILENAME = N'{0}' , SIZE = 204800KB , FILEGROWTH = 51200KB )
 LOG ON 
( NAME = N'LDB_log', FILENAME = N'{1}' , SIZE = 1024KB , FILEGROWTH = 10%)
COLLATE Cyrillic_General_CI_AS";

        private static string IF_EXISTS_SCHEMA_SCRIPT =
            @"if exists (select * from sys.schemas where name = N'{0}')
  select 1
else
  select 0";

        private static string CREATE_SCHEMA_SCRIPT = "create schema [{0}] authorization [dbo]";

        private static string IF_NOT_EXISTS_VIEW_SCRIPT =
            @"if not exists (select * from dbo.sysobjects where id = object_id(N'{0}') and OBJECTPROPERTY(id, N'IsView') = 1)
exec('create view {0} as select 1 as F1')";

        private static string DROP_VIEW_SCRIPT =
            @"if exists (select * from dbo.sysobjects where id = object_id(N'{0}') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view {0}";

        private static string DROP_SP_SCRIPT =
            @"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[{0}]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[{0}]";

        private static string DROP_UDF_SCRIPT =
            @"if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[{0}]') AND (OBJECTPROPERTY(id, N'IsTableFunction') = 1 OR OBJECTPROPERTY(id, N'IsScalarFunction') = 1))
drop function [dbo].[{0}]";

        private static string DROP_LDB_SCRIPT = @"DROP DATABASE LDB";

        private static string UNSUBSCRUBE_REPLICATION =
            @"IF Object_ID('dbo.sysmergesubscriptions') IS NOT NULL
BEGIN
	IF EXISTS(
		SELECT 1
		FROM  sysmergesubscriptions
		WHERE (subscriber_server = 'EEGDADB8')
	)
	EXEC sp_dropmergepullsubscription
		@publisher = N'EEGDADB8', 
		@publisher_db = N'SalesWorks', 
		@publication = N'DW'
END";

        private static string GET_DEFAULT_DATA_FILES =
            @"
declare @RegPathParams sysname
declare @Arg sysname
declare @Param sysname
declare @MasterPath nvarchar(512)
declare @LogPath nvarchar(512)
declare @n int

select @n=0
select @RegPathParams=N'Software\Microsoft\MSSQLServer\MSSQLServer'+'\Parameters'
select @Param='dummy'
while(not @Param is null)
begin
	select @Param=null
	select @Arg='SqlArg'+convert(nvarchar,@n)

	exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE', @RegPathParams, @Arg, @Param OUTPUT

	if(@Param like '-d%')
	begin
		select @Param=substring(@Param, 3, 255)
		select @MasterPath=substring(@Param, 1, len(@Param) - charindex('\', reverse(@Param)))
	end
	else if(@Param like '-l%')
	begin
		select @Param=substring(@Param, 3, 255)
		select @LogPath=substring(@Param, 1, len(@Param) - charindex('\', reverse(@Param)))
	end
	select @n=@n+1
end

declare @SmoDefaultFile nvarchar(512)
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'DefaultData', @SmoDefaultFile OUTPUT

declare @SmoDefaultLog nvarchar(512)
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'DefaultLog', @SmoDefaultLog OUTPUT
			
SELECT
ISNULL(@SmoDefaultFile,@MasterPath) AS [DefaultFile],
ISNULL(@SmoDefaultLog,@LogPath) AS [DefaultLog]";

        private static string GET_DTATABASE_FILE_PATHES = @"SELECT physical_name, type FROM sys.database_files";

        private static string GET_DB_SCHEMAS = @"SELECT distinct TABLE_SCHEMA FROM INFORMATION_SCHEMA.TABLES";

        private static string GET_DB_OBJECT_TYPE = @"SELECT type FROM sys.objects WHERE object_id = object_id('{0}')";

        private static string GET_VIEW_FIELDS_DESCRIPTION = @"
SELECT
  schema_name(so.schema_id) as TABLE_SCHEMA,
  so.name as TABLE_NAME,
  sc.name as COLUMN_NAME,
  sc.COLUMN_ID,
  sc.SYSTEM_TYPE_ID,
  0 as IS_PK
FROM sys.columns sc
  join sys.objects so on sc.object_id = so.object_id
WHERE so.type = 'V'
ORDER BY so.name";

        private static string GET_FIELDS_DESCRIPTION = @"
SELECT
  schema_name(so.schema_id) as TABLE_SCHEMA,
  so.name as TABLE_NAME,
  sc.name as COLUMN_NAME,
  sc.COLUMN_ID,
  sc.SYSTEM_TYPE_ID,
  case when kcu.COLUMN_NAME is null then 0 else 1 end as IS_PK
FROM sys.objects so
  join sys.columns sc on sc.object_id = so.object_id
  left join sys.objects pk on pk.parent_object_id = so.object_id
    and pk.type = 'PK'
  left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu on kcu.TABLE_SCHEMA = schema_name(so.schema_id)
    and kcu.TABLE_NAME = so.name and kcu.COLUMN_NAME = sc.name and pk.name = kcu.CONSTRAINT_NAME
WHERE so.type = 'U'
ORDER BY so.name, sc.COLUMN_ID";
/*----------------- alternative variant
SELECT
  t.TABLE_SCHEMA, t.TABLE_NAME, c.COLUMN_NAME,
  sc.COLUMN_ID, sc.SYSTEM_TYPE_ID,
  case when c.COLUMN_NAME = isnull(kcu.COLUMN_NAME, '') then 1 else 0 end as IS_PK
FROM INFORMATION_SCHEMA.TABLES t
  join INFORMATION_SCHEMA.COLUMNS c on c.TABLE_SCHEMA = t.TABLE_SCHEMA
    and c.TABLE_NAME = t.TABLE_NAME
  join sys.columns sc on sc.name = c.COLUMN_NAME
    and sc.object_id = object_id('[' + t.TABLE_SCHEMA + '].[' + t.TABLE_NAME + ']')
  left join INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc on tc.TABLE_SCHEMA = t.TABLE_SCHEMA
    and tc.TABLE_NAME = t.TABLE_NAME and tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
  left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu on kcu.TABLE_SCHEMA = t.TABLE_SCHEMA
    and kcu.TABLE_NAME = t.TABLE_NAME and kcu.COLUMN_NAME = c.COLUMN_NAME
    and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME
WHERE t.TABLE_TYPE = 'BASE TABLE'
ORDER BY t.TABLE_SCHEMA,
  t.TABLE_NAME, sc.column_id
*/
        #endregion

        internal static string CretaeLDBScript(string pathToLDBDataFile, string pathToLDBLogFile) {
            return string.Format(CREATAE_LDB_SCRIPT, pathToLDBDataFile, pathToLDBLogFile);
        }

        internal static string CheckLDBExistence {
            get {
                return
                    string.Format(
                        @"SELECT
dtb.name AS [DatabaseName2]
FROM
master.sys.databases AS dtb
WHERE
(dtb.name=N'{0}')",
                        SqlServerManager.LogicaDatabaseName);
            }
        }

        internal static string DefaultDataFilesPathes {
            get { return GET_DEFAULT_DATA_FILES; }
        }

        internal static string LDBFilesPathes {
            get { return GET_DTATABASE_FILE_PATHES; }
        }

        internal static string GetDBSchemas {
            get { return GET_DB_SCHEMAS; }
        }

        internal static string IfExistsSchemaScript {
            get { return IF_EXISTS_SCHEMA_SCRIPT; }
        }

        internal static string CreateSchemaScript {
            get { return CREATE_SCHEMA_SCRIPT; }
        }

        internal static string IfNotExistsViewScript {
            get { return IF_NOT_EXISTS_VIEW_SCRIPT; }
        }

        internal static string DropViewIfExistsScript {
            get { return DROP_VIEW_SCRIPT; }
        }

        internal static string DropSPIfExistsScript {
            get { return DROP_SP_SCRIPT; }
        }

        internal static string DropUDFIfExistsScript {
            get { return DROP_UDF_SCRIPT; }
        }

        internal static string GetDBObjectType {
            get { return GET_DB_OBJECT_TYPE; }
        }

        internal static string GetDropLDBScript {
            get { return DROP_LDB_SCRIPT; }
        }

        internal static string GetUnsubscribeRepliaction {
            get { return UNSUBSCRUBE_REPLICATION; }
        }

        internal static string GetFiledsDescription {
            get { return GET_FIELDS_DESCRIPTION; }
        }

        internal static string GetViewFiledsDescription {
            get { return GET_VIEW_FIELDS_DESCRIPTION; }
        }
    }

    /// <summary>
    /// Specify type of SQL Database files
    /// </summary>
    public enum LBDDataFileType {
        UNDEFINED = -1,
        ROWS = 0,
        LOG = 1
    }

    /// <summary>
    /// LDB Process types
    /// </summary>
    public enum LDBProcessType {
        Replication,
        Create
    }

    internal enum LDBDataFilesPathesFields {
        physical_name,
        type
    }

    internal enum SQLDefaultDataFilesPathesFields {
        DefaultFile,
        DefaultLog
    }

    /// <summary>
    /// Subfolders name
    /// </summary>
    public enum SqlScriptsSubFolders {
        Tables = 1,
        Views = 2,
        Keys = 3,
        Indexes = 4,
        SP = 6,
        UDF = 5
    }

    public enum DBObjectTypes {
        V,  // Views
        U,  // Tables
        TF,  // Table-valued User defined functions
        IF, // Table-valued User defined functions
        FN,  // Scalar User defined functions
        P,  // Stored procedures
        Undefined  // Undefined
    }
}