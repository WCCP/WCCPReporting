﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using NLog;

namespace Logica.DB.LDBProcessor
{
    ///<summary>
    /// Creates LDB database and appropriate views in it based on SW_DDB databases.
    ///</summary>
    public class LDBProccessor
    {
        private static readonly LDBProccessor _insnance = new LDBProccessor();
        private Logger _logger = LoggerHelper.Logger;
        private const string ERROR_MESSAGE_INSTALLATION_BROKEN = "Installation was broken on {0} creation";

        private LDBProccessor()
        {
        }

        /// <summary>
        /// Returns instance of LDBProcessor.
        /// </summary>
        public static LDBProccessor Instance
        {
            get { return _insnance; }
        }

        private static string DropViewIfExistsScript
        {
            get { return CreateLDBScripts.DropViewIfExistsScript; }
        }
        
        public event LDBProcessEventHandler OnEntityProcess;

        /// <summary>
        /// Creates LDB files 
        /// </summary>
        /// <param name="ldbDataFileName">Database data file </param>
        /// <param name="ldbDataLogFileName">Database log file name</param>
        /// <returns></returns>
        public bool CreateLBDFiles(string ldbDataFileName, string ldbDataLogFileName)
        {
            if (SqlServerManager.Current.SqlConnections.Count == 0)
            {
                _logger.Warn("LDB creation was failed! There are no defined connections.");
                return false;
            }
            ConnectionDescription lConnection = SqlServerManager.Current.SqlConnections[0];

            LDBProcessEventArgs lArgs = new LDBProcessEventArgs("Database", LDBProcessLBDEntityEnum.CreateLDB);
            string lErrorMsg = string.Empty;
            Dictionary<LBDDataFileType, string> LDBFiles = new Dictionary<LBDDataFileType, string>();
            LDBFiles.Add(LBDDataFileType.ROWS, ldbDataFileName);
            LDBFiles.Add(LBDDataFileType.LOG, ldbDataLogFileName);
            _logger.Info("Check LDB existance.");
            lArgs.EntityName = "Check LDB existance.";

            if (null != OnEntityProcess)
                OnEntityProcess(null, lArgs);

            bool result = DataProvider.CheckLDBExistance(lConnection.SqlConnectionString.ToString());
            if (result)
            {
                if (!lConnection.LDBExists)
                {
                    _logger.Error("Cannot drop LDB. It doesn't exist!\n   " + lErrorMsg);
                    return false;
                }

                ConnectionDescription connectionToLDB = lConnection.ConnectionToLDB;

                lArgs.EntityName = "Unsubscribe replication";
                if (null != OnEntityProcess)
                    OnEntityProcess(null, lArgs);

                lErrorMsg = DataProvider.UnsubscribeReplication(connectionToLDB, SqlServerManager.LogicaDatabaseName);
                if (!string.IsNullOrEmpty(lErrorMsg.Trim()))
                    _logger.Error(lErrorMsg);

                lArgs.EntityName = "Drop LDB";
                if (null != OnEntityProcess)
                    OnEntityProcess(null, lArgs);

                result = !DataProvider.DropLDB(lConnection, SqlServerManager.LogicaDatabaseName, out lErrorMsg);

                if (!string.IsNullOrEmpty(lErrorMsg))
                {
                    _logger.Error("LDB creation was failed!\n   " + lErrorMsg);
                    return false;
                }
            }

            if (!result)
            {
                _logger.Info("LDB doesn't exist. Try to create LDB. ");
                lArgs.EntityName = "Create LDB";
                if (null != OnEntityProcess)
                    OnEntityProcess(null, lArgs);

                _logger.Info(string.Format("LDB files. \"{0}\", \"{1}\"", LDBFiles[LBDDataFileType.ROWS], LDBFiles[LBDDataFileType.LOG]));
                lArgs.EntityName = "Create LDB";
                if (null != OnEntityProcess)
                    OnEntityProcess(null, lArgs);

                lErrorMsg = DataProvider.CreateLDB(lConnection.SqlConnectionString.ToString(), LDBFiles);
                if (!string.IsNullOrEmpty(lErrorMsg))
                    _logger.Error("LDB creation was failed!\n" + lErrorMsg);
                else
                    _logger.Info("LDB was created successfully!\n" + lErrorMsg);
            }
            else
            {
                LDBFiles = SqlServerManager.Current.GetLDBFilesPathes();
            }
            return string.IsNullOrEmpty(lErrorMsg);
        }

        /// <summary>
        /// Perform SQL sripts embeded into LDB manager 
        /// </summary>
        /// <param name="scripts">SQL scrips collection</param>
        /// <param name="connectToLDB">COnnection to SQL Server</param>
        /// <returns>If the script is performed successfully it returns true</returns>
        public bool CreatePredefinedSQLs(SortedList<int, KeyValuePair<string, string>> scripts,
                                         ConnectionDescription connectToLDB)
        {
            if (
                !connectToLDB.DataBase.Equals(SqlServerManager.LogicaDatabaseName,
                    StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Exception("Connection MUST be to LDB!!!!!");
            }

            _logger.Info("Creation predefined SQL scripts");
            LDBProcessEventArgs lArgs = new LDBProcessEventArgs("Database", LDBProcessLBDEntityEnum.CreateSubcription);

            lArgs.EntityName = "Predefined SQL scripts";

            if (null != OnEntityProcess)
                OnEntityProcess(null, lArgs);

            for (int i = 1; i <= scripts.Count; i++)
            {
                KeyValuePair<string, string> sqlScript = scripts[i];
                try
                {
                    DataProvider.ExecuteNonQuery(connectToLDB, sqlScript.Key);
                }
                catch (Exception lEx)
                {
                    _logger.Error(string.Format("Script \"{0}\". Execution failed!", sqlScript.Value));
                    _logger.ErrorException("Exception: ", lEx);
                    return false;
                }
            }
            _logger.Info("Predefined scripts were created successfully.");
            return true;
        }

        /// <summary>
        /// Clean referencing views up in a LDB
        /// </summary>
        /// <param name="SWSqlConnections">Connection to a LDB</param>
        /// <returns>If the cleaning up is successfull it returns true</returns>
        public bool CleanUpReferencingViews(List<ConnectionDescription> SWSqlConnections)
        {
            if (SWSqlConnections.Count == 0)
            {
                _logger.Error("There is no connection described.");
                return false;
            }

            // Delete referensinig Views 
            _logger.Info("Deleting referencing Views.");

            SqlServerTablesCollection views, srvTables;
            ConnectionDescription LDBSqlConnection = SWSqlConnections[0].ConnectionToLDB;

            if (!CheckSDDBCoincidence(SWSqlConnections, out srvTables, out views))
                return false;

            string lErrorMsg = SqlServerManager.Current.DeleteReferencedViews(LDBSqlConnection, srvTables, views);
            if (!string.IsNullOrEmpty(lErrorMsg))
            {
                _logger.Warn("Thew views deletion was failed!\n" + lErrorMsg);
                return false;
            }
            _logger.Info("The views were deleted successfully!\n  " + lErrorMsg);
            return true;
        }

        /// <summary>
        /// Create referencing views in LDB
        /// </summary>
        /// <param name="SWSqlConnections">Connection description to LDB</param>
        /// <returns>If the cleaning up is successfull it returns true</returns>
        public bool CreateReferencingView(List<ConnectionDescription> SWSqlConnections)
        {
            return CreateReferencingView(SWSqlConnections, LDBProcessType.Create);
        }

        /// <summary>
        /// Create referencing views in LDB
        /// </summary>
        /// <param name="swSqlConnections">Connection description to LDB</param>
        /// <param name="processType"></param>
        /// <returns></returns>
        public bool CreateReferencingView(List<ConnectionDescription> swSqlConnections, LDBProcessType processType)
        {
            if (swSqlConnections.Count == 0)
            {
                _logger.Error("There is no connection described.");
                return false;
            }

            SqlServerTablesCollection lViews, lSrvTables;
            ConnectionDescription lLDBSqlConnection = swSqlConnections[0].ConnectionToLDB;

            if (!CheckSDDBCoincidence(swSqlConnections, out lSrvTables, out lViews))
                return false;
            //check and create schemas
            if (!CheckAndCreateSchemas(lSrvTables, lViews, lLDBSqlConnection))
                return false;

            if (!CreateReferencingViews(lSrvTables, lLDBSqlConnection, SqlScriptsSubFolders.Tables, processType))
                return false;

            return CreateReferencingViews(lViews, lLDBSqlConnection, SqlScriptsSubFolders.Views, processType);
        }

        private bool CheckAndCreateSchemas(SqlServerTablesCollection srvTables, SqlServerTablesCollection views,
                                           ConnectionDescription ldbSqlConnection)
        {
            List<string> lSchemaList = new List<string>();

            if (srvTables.Count > 0)
                lSchemaList.AddRange(srvTables[0].TableSchemas);
            if (views.Count > 0)
                foreach (string lViewSchema in views[0].TableSchemas)
                    if (!lSchemaList.Contains(lViewSchema))
                        lSchemaList.Add(lViewSchema);

            foreach (string lSchema in lSchemaList)
            {
                if (lSchema == "dbo")
                    continue;

                if (!DataProvider.SchemaExists(ldbSqlConnection, lSchema))
                {
                    try
                    {
                        DataProvider.CreateSchema(ldbSqlConnection, lSchema);
                        _logger.Info(string.Format("The schema {0} was created successfully.", lSchema));
                    }
                    catch (Exception lException)
                    {
                        _logger.ErrorException("Exception: ", lException);
                        return false;
                    }
                }
            }
            return true;
        }

        public string CheckSqlFileStructure(string folderName)
        {
            DirectoryInfo lDirectoryInfo = new DirectoryInfo(folderName);
            DirectoryInfo[] lFolders = lDirectoryInfo.GetDirectories();
            string lResult = string.Empty;

            foreach (string lItem in Enum.GetNames(typeof(SqlScriptsSubFolders)))
            {
                bool lFound = false;
                foreach (DirectoryInfo lDirInfo in lFolders)
                {
                    if (lDirInfo.Name.Equals(lItem, StringComparison.InvariantCultureIgnoreCase))
                    {
                        lFound = true;
                        break;
                    }
                }
                if (!lFound)
                    lResult = lItem;
            }
            return lResult;
        }

        public bool? CheckLDBExistance()
        {
            try
            {
                _logger.Info("Check LDB existence.");
                bool lResult = DataProvider.CheckLDBExistance(SqlServerManager.Current.SqlConnections[0].SqlConnectionString.ToString());
                _logger.Info(lResult ? "LDB exists" : "LDB doesn't exist");
                return lResult;
            }
            catch (Exception lException)
            {
                _logger.ErrorException("Exception: ", lException);
                return null;
            }
        }

        public bool CreateLDBEntitiesProcess(List<ConnectionDescription> swSqlConnections, string pathToScripts)
        {
            string lAbsentFolder = CheckSqlFileStructure(pathToScripts);
            if (!string.IsNullOrEmpty(lAbsentFolder))
            {
                _logger.Error(string.Format("The sql subfolder \"{0}\" was not found!", lAbsentFolder));
                return false;
            }

            ConnectionDescription lLDBSqlConnection = swSqlConnections[0];

            _logger.Info(string.Format("Start updating databse \"{0}\" on server \"{1}\"",
                lLDBSqlConnection.DataBase, lLDBSqlConnection.ServerName));

            foreach (int lItemInt in Enum.GetValues(typeof(SqlScriptsSubFolders)))
            {
                SqlScriptsSubFolders lItem = (SqlScriptsSubFolders)lItemInt;
                _logger.Info(string.Format("Execute the scrips in {0}", lItem));
                DirectoryInfo lDirInfo = new DirectoryInfo(Path.Combine(pathToScripts, lItem.ToString()));
                FileInfo[] lFiles = lDirInfo.GetFiles("*.sql");

                if (0 == lFiles.Length)
                    continue;

                if (lItem == SqlScriptsSubFolders.Keys || lItem == SqlScriptsSubFolders.Indexes
                    || lItem == SqlScriptsSubFolders.Tables)
                {
                    _logger.Warn(lItem + " is not implemented");
                    continue;
                }
                int lCount = 0;
                while (true)
                {
                    lCount++;
                    List<FileInfo> lRepeate;
                    if (!CreateLDBObjecProcess(lFiles, lLDBSqlConnection, lItem, out lRepeate))
                    {
                        _logger.Error(string.Format(ERROR_MESSAGE_INSTALLATION_BROKEN, lItem.ToString()));
                        return false;
                    }
                    if (lRepeate.Count == 0)
                        break;
                    lFiles = lRepeate.ToArray();
                    if (lCount == 20)
                    {
                        foreach (FileInfo lFileInfo in lFiles)
                            _logger.Error(string.Format("Cannot execute the content of \"{0}\" file!", lFileInfo.FullName));
                        return false;
                    }
                }
            }
            return true;
        }

        private bool CreateLDBObjecProcess(FileInfo[] files, ConnectionDescription ldbSqlConnection,
                                           SqlScriptsSubFolders dbObjectType, out List<FileInfo> repeateFiles)
        {
            repeateFiles = new List<FileInfo>();
            Dictionary<string, SqlTableDescription> lTables = SqlServerManager.Current.GetTablesNameList(ldbSqlConnection);

            LDBProcessEventArgs lArgs = new LDBProcessEventArgs(dbObjectType.ToString(), LDBProcessLBDEntityEnum.CreateScripts);

            foreach (FileInfo lFileInfo in files)
            {
                string lTableName = lFileInfo.Name.Substring(0, lFileInfo.Name.Length - lFileInfo.Extension.Length);

                lArgs.EntityName = lTableName;
                if (null != OnEntityProcess)
                    OnEntityProcess(null, lArgs);

                if (!lTables.ContainsKey(lTableName))
                {
                    string lContent = GetFileContent(lFileInfo);
                    try
                    {
                        _logger.Info(string.Format("Create {1} {0}.", lFileInfo.Name, GetDBObjectName(dbObjectType).ToLower()));
                        DBObjectTypes lObjectTp = DataProvider.GetDBObjectType(lTableName, ldbSqlConnection.SqlConnectionString.ToString());
                        if (lObjectTp != DBObjectTypes.Undefined)
                        {
                            DataProvider.DropObject(ldbSqlConnection, dbObjectType, lTableName);
                        }
                        DataProvider.ExecuteNonQuery(ldbSqlConnection, lContent);
                        _logger.Info(string.Format("{1} {0} was created.", lFileInfo.Name, GetDBObjectName(dbObjectType)));
                    }
                    catch (Exception lException)
                    {
                        if (null != lException.InnerException && lException.InnerException is SqlException)
                        {
                            SqlException lSqlException = lException.InnerException as SqlException;
                            if (lSqlException.Number == 208)
                            {
                                repeateFiles.Add(lFileInfo);
                                continue;
                            }
                        }
                        _logger.ErrorException("Exception: ", lException);
                        return false;
                    }
                }
                else
                {
                    SqlTableDescription lCurrentTable = lTables[lTableName];
                    _logger.Warn(string.Format("The {1} \"{0}\" has already been in the database.",
                        lCurrentTable.FullAlias, GetDBObjectName(dbObjectType).ToLower()));
                }
            }
            return true;
        }

        private static string GetFileContent(FileInfo fileInfo)
        {
            using (StreamReader lReader = fileInfo.OpenText())
            {
                return lReader.ReadToEnd();
            }
        }

        private bool CreateReferencingViews(SqlServerTablesCollection srvTables, ConnectionDescription ldbSqlConnection,
                                            SqlScriptsSubFolders entityType, LDBProcessType processType)
        {
            _logger.Info(string.Format("Create views inherited from {0}.", entityType.ToString().ToLower()));
            LDBProcessEventArgs lArgs = new LDBProcessEventArgs(entityType.ToString(), LDBProcessLBDEntityEnum.CreateScripts);

            foreach (SqlTableDescription lTblDescription in srvTables[0].Values)
            {
                string lTableName = lTblDescription.Name;

                lArgs.EntityName = lTableName;
                if (null != OnEntityProcess)
                    OnEntityProcess(null, lArgs);

                string lTableNameWithSchema = lTblDescription.ShortAlias;
               
                QueryToLDBView lLDBViewQuery =
                    ReferencingViewConfiguration.Instance.QueryToLDBViews.Find(lTblDescription.Schema, lTableName);
                DirectSQLView lDirectSQLView =
                    ReferencingViewConfiguration.Instance.DirectSQLViews.Find(lTblDescription.Schema, lTableName);
                // QueryToLDBView has higher priority than DirectSQLView
                if (null != lLDBViewQuery)
                    lDirectSQLView = null;

                if (null != lDirectSQLView)
                {
                    try
                    {
                        DataProvider.CreateViewDumbIfNotExists(ldbSqlConnection, lTableNameWithSchema);
                    }
                    catch (Exception lException)
                    {
                        _logger.ErrorException("Exception: ", lException);
                        return false;
                    }

                    

                    DirectQuery lDirectQuery = lDirectSQLView.QueryList.Find(srvTables.Count);

                    if (null != lDirectQuery)
                    {
                         _logger.Info(string.Format("Create {0} view.", lTableNameWithSchema));
                        try
                        {
                            DataProvider.AlterView(ldbSqlConnection, lTblDescription.Schema, lTblDescription.Name, lDirectQuery.Query);
                            _logger.Info(string.Format("The view {0} was cretaed successfully with direct SQL script.", lTableNameWithSchema));
                            continue;
                        }
                        catch (Exception lEx)
                        {
                            _logger.ErrorException("Exception: ", lEx);
                            return false;
                        }
                    }
                } // if (null != lDirectSQLView)

                ViewQueryBuilder lViewQueryBuilder = new ViewQueryBuilder(srvTables.DBNameList, lTblDescription, lLDBViewQuery);

                string lSerSql = srvTables[0].EntityType != SqlScriptsSubFolders.Tables
                              ? ""
                              : lViewQueryBuilder.GetCheckOwerlappingScript();

                int lOwerlapped = 0;

                if (srvTables[0].EntityType == SqlScriptsSubFolders.Tables && srvTables.DBNameList.Count > 1 &&
                    !string.IsNullOrEmpty(lSerSql))
                    lOwerlapped = DataProvider.CheckOverlapping(ldbSqlConnection, lSerSql);
                   
                lSerSql = lViewQueryBuilder.GetViewScript(lOwerlapped > 0);
                
                _logger.Info(string.Format("Check {0} existance in {1}.", lTableNameWithSchema, entityType));

                try
                {
                    if (processType == LDBProcessType.Create)
                    {
                        DBObjectTypes lDbObjectType = DataProvider.GetDBObjectType(lTblDescription.ShortAlias, ldbSqlConnection.SqlConnectionString.ToString());

                        if (lDbObjectType == DBObjectTypes.V)
                            DataProvider.DropViewIfExists(ldbSqlConnection, lTableNameWithSchema);
                        else
                        {
                            if (lDbObjectType != DBObjectTypes.Undefined)
                            {
                                _logger.Info(string.Format("Skip the SQL server {1} \"{0}\".", lTableNameWithSchema,
                                    GetDBObjectName(entityType).ToLower()));
                                continue;
                            }
                        }
                    }
                    else
                    {
                        object lIsViewExists = DataProvider.CheckViewExistance(ldbSqlConnection, lTableNameWithSchema);
                       
                        if (null != lIsViewExists)
                        {
                            _logger.Info(string.Format("Skip the SQL server object \"{0}\".", lTableNameWithSchema));
                            continue;
                        }
                    }
                }
                catch (Exception lEx)
                {
                    _logger.ErrorException("Exception: ", lEx);
                    return false;
                }

                try
                {
                    DataProvider.CreateViewDumbIfNotExists(ldbSqlConnection, lTableNameWithSchema);
                }
                catch (Exception lEx)
                {
                    _logger.ErrorException("Exception", lEx);
                    return false;
                }

                _logger.Info(string.Format("Create {0} view.", lTableNameWithSchema));
                for (int lInd = 0; lInd < 2; lInd++)
                {
                    try
                    {
                        DataProvider.ExecuteNonQuery(ldbSqlConnection, lSerSql);
                        _logger.Info(string.Format("The view {0} was cretaed successfully with {1}.",
                            lTableNameWithSchema, lInd == 0 ? "UNION" : "UNION ALL"));
                        break;
                    }
                    catch (Exception lEx)
                    {
                        _logger.ErrorException("Exception: ", lEx);
                        if (null != lEx.InnerException && lEx.InnerException is SqlException)
                        {
                            SqlException lSqlException = (SqlException)lEx.InnerException;
                            if (lInd == 0 && lSqlException.Number == 421)
                            {
                                _logger.Info(string.Format("Try to use UNION ALL to create the view {0}", lTableNameWithSchema));
                                lSerSql = lSerSql.Replace("UNION \r\n", "UNION ALL\r\n");
                                continue;
                            }
                        }
                        return false;
                    }
                }
            }
            _logger.Info(string.Format("Referencing views inherited from {0} have been created.", entityType.ToString().ToLower()));
            return true;
        }

        private static ConnectionDescription GetConnectionToLDB(ConnectionDescription swSqlConnection)
        {
            return swSqlConnection.ConnectionToLDB;
        }

        private static string GetDBObjectName(SqlScriptsSubFolders item)
        {
            switch (item)
            {
                case SqlScriptsSubFolders.Indexes:
                    return "Indexe";
                case SqlScriptsSubFolders.Keys:
                    return "Key";
                case SqlScriptsSubFolders.SP:
                    return "Storage procedure";
                case SqlScriptsSubFolders.Tables:
                    return "Table";
                case SqlScriptsSubFolders.UDF:
                    return "UDF";
                case SqlScriptsSubFolders.Views:
                    return "View";
            }
            return string.Empty;
        }



        

        /// <summary>
        /// Check SW databases for coincidence
        /// </summary>
        /// <param name="swSqlConnections">Connection to SW databases</param>
        /// <param name="srvTables">List of tables SqlServerTables</param>
        /// <param name="views">List of views</param>
        /// <returns></returns>
        private bool CheckSDDBCoincidence(List<ConnectionDescription> swSqlConnections,
                                          out SqlServerTablesCollection srvTables, out SqlServerTablesCollection views)
        {
            string databaseNames = string.Empty;
            for (int i = 0; i < swSqlConnections.Count; i++)
            {
                ConnectionDescription currentConnection = swSqlConnections[i];
                databaseNames += currentConnection.DataBase + ", ";
            }
            if (!string.IsNullOrEmpty(databaseNames))
                databaseNames = databaseNames.Substring(0, databaseNames.Length - 2);
            _logger.Info("Start checking database for equivalence");

            LDBProcessEventArgs lArgs = new LDBProcessEventArgs(SqlScriptsSubFolders.Tables.ToString(),
                LDBProcessLBDEntityEnum.DatabaseEquivalence);

            srvTables = new SqlServerTablesCollection();
            views = new SqlServerTablesCollection();

            bool result = true;
            int prevTblCnt = -1;
            int prevViewCnt = -1;
            for (int i = 0; i < swSqlConnections.Count; i++)
            {
                ConnectionDescription currentConnection = swSqlConnections[i];
                SqlServerTables tables = SqlServerManager.Current.GetTablesNameList(currentConnection);
                srvTables.Add(tables);
                views.Add(SqlServerManager.Current.GetViewsNameList(currentConnection));
                result = result && (prevTblCnt == -1 ? true : prevTblCnt == tables.Count);
                result = result && (prevViewCnt == -1 ? true : prevViewCnt == views[i].Count);

                prevTblCnt = tables.Count;
            }

            if (!result)
            {
                _logger.Error(string.Format("There were compared {0} databases ({1}) Counts of tables/views are not equal!",
                    swSqlConnections.Count, databaseNames));
                return false;
            }

            if (srvTables.Count > 1)
            {
                SqlServerTables templateTables = srvTables[0];
                SqlServerTables templateViews = views[0];

                foreach (string tableNameKey in templateTables.Keys)
                {
                    string tableName = templateTables[tableNameKey].ShortAlias;
                    lArgs.EntityName = tableName;

                    if (null != OnEntityProcess)
                        OnEntityProcess(null, lArgs);

                    for (int i = 1; i < srvTables.Count; i++)
                    {
                        SqlServerTables curSrv = srvTables[i];
                        if (!curSrv.ContainsKey(tableNameKey))
                        {
                            _logger.Error(string.Format("Can not find the table \"{0}\" in the database {1}",
                                tableName, swSqlConnections[i].DataBase));
                            return false;
                        }
                    }
                }

                lArgs = new LDBProcessEventArgs(SqlScriptsSubFolders.Views.ToString(),
                    LDBProcessLBDEntityEnum.DatabaseEquivalence);

                foreach (string viewNameKey in templateViews.Keys)
                {
                    for (int i = 1; i < views.Count; i++)
                    {
                        string viewName = templateViews[viewNameKey].ShortAlias;
                        lArgs.EntityName = viewName;

                        if (null != OnEntityProcess)
                            OnEntityProcess(null, lArgs);

                        SqlServerTables curSrv = views[i];
                        if (!curSrv.ContainsKey(viewNameKey))
                        {
                            _logger.Error(string.Format("Can not find the view \"{0}\" in the database {1}",
                                viewName, swSqlConnections[i].DataBase));
                            return false;
                        }
                    }
                }
            }
            _logger.Info(string.Format("Checking is finished. The databases ({0}) are equivalent ", databaseNames));
            return true;
        }
    }
}