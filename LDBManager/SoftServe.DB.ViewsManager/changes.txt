Last versions:
---------------
LDBManager.exe              1.0.3.6
Logica.DB.LDBProcessor.dll  1.0.1.8
Logica.Reports.Common.dll   1.0.2.8059
Logica.Reports.DataAccess.dll   1.0.3.9742


Changes history:
======================
2014-02-22 - ��������� ��� ��� �������� ��������� ��� SW_DDB - �� ����������� ����� ������ ��� ���������
             ����� � ��������.

2013-10-28 - ��������� ��������� ����� �������� � ������ <ArrayOfSqlObjectException>. ��������:
             <SqlObjectException SqlObjectName="dbo.rtms*" />
			 
2011-11-07 - ��������� ��������� �������� PKFields � �������������� ������� elimination.xml.
             �������� ����� �������� ����� ������ ���� ���������� ����� ��� ������ SW_DDB,
             ��� ��� ���������� (��. ������ ��� tblPOSContracts, tblPOSContractDetails).
2011-09-13 - ��������� ��������� ���� � ��������� �������� � �������������� ������� elimination.xml.
             ������ � SWEntityName ������ ���� ������ �����, ��������,
             SWEntityName="dbo.tblActivityTypes"

2011-08-08 - ������������ ������ QueryToLDBView � elimination.xml ��� ��������� ������� ��������.
             ��� ������ �������� ������� ��� tblDistributors.
           - ��������� ������ ArrayOfDirectSQL ��� ��������� ������ ���������������� view,
             ��� ������� ������ �������� ������ � ������ QueryToLDBView. ������ �������������:
            <ArrayOfDirectSQL>
              <DirectSQLView SWEntityName="dbo.tblActivityTypes">
                <QueryList>
                  <DirectQuery SWddbCount="1">
                    <Query> select * from SW_DDB.dbo.tblActivityTypes </Query>
                  </DirectQuery>
                  <DirectQuery SWddbCount="2">
                    <Query> select * from SW_DDB.dbo.tblActivityTypes </Query>
                  </DirectQuery>
                  <DirectQuery SWddbCount="3">
                    <Query> select * from SW_DDB.dbo.tblActivityTypes </Query>
                  </DirectQuery>
                </QueryList>
              </DirectSQLView>
            </ArrayOfDirectSQL>    


2011-07-28 - ��������� ���: ������ ��� �������� ������������ ��� SW_DDB,
             ���� � ������ ����� ������ ����� �������� � ������ ��������� ����;
           - ��� �������� view ������������ ������ ����� ������ * � �������� - ��� ������ ��������
             ������� ������� ����� � SW_DDB �����. ����� ���� � elimination.xml � ������
             ArrayOfQueryToLDBView ������ t.* ���� ������ {0}, ������� ����� ��������� �� ������
             ����� �� �������, � ����� ��� ������� ������ ���� t;
           - ��������� ������ "Recreate Views" ��� ������� �� ������� ���������� ������������ view
             � LDB ����;

2011-07-18 - ��������� ���, ��� �������� LDB ��� ����� � ����� SW_DDB

2011-06-15 - ��������� � elimination.xml Query ��� view tblPOSContracts � tblPOSContractDetails
             ��� ����, ��� �� �� ��������� ��� ���� ��� � �� (c ����� Cust_ID).
