﻿namespace Logica.LDBManager
{
    partial class ConnectionToSqlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.rbSQLAuthentication = new System.Windows.Forms.RadioButton();
            this.rbWindowsAuthentication = new System.Windows.Forms.RadioButton();
            this.btnRefreshServerNames = new System.Windows.Forms.Button();
            this.cmbSQLServersName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cDatabases = new System.Windows.Forms.ListBox();
            this.btnRefreshDatabases = new System.Windows.Forms.Button();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lblPassword);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.lblUserName);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.rbSQLAuthentication);
            this.groupBox1.Controls.Add(this.rbWindowsAuthentication);
            this.groupBox1.Location = new System.Drawing.Point(4, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 138);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Log on to the server";
            this.groupBox1.UseCompatibleTextRendering = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Firebrick;
            this.label3.Location = new System.Drawing.Point(7, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(299, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "User should have Administrator right on SQL Server";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(21, 95);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 5;
            this.lblPassword.Text = "Password:";
            // 
            // txtPassword
            // 
            this.txtPassword.Enabled = false;
            this.txtPassword.Location = new System.Drawing.Point(81, 92);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(255, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(16, 73);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(61, 13);
            this.lblUserName.TabIndex = 3;
            this.lblUserName.Text = "User name:";
            // 
            // txtUserName
            // 
            this.txtUserName.Enabled = false;
            this.txtUserName.Location = new System.Drawing.Point(81, 66);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(255, 20);
            this.txtUserName.TabIndex = 2;
            // 
            // rbSQLAuthentication
            // 
            this.rbSQLAuthentication.AutoSize = true;
            this.rbSQLAuthentication.Location = new System.Drawing.Point(10, 42);
            this.rbSQLAuthentication.Name = "rbSQLAuthentication";
            this.rbSQLAuthentication.Size = new System.Drawing.Size(173, 17);
            this.rbSQLAuthentication.TabIndex = 1;
            this.rbSQLAuthentication.TabStop = true;
            this.rbSQLAuthentication.Text = "Use SQL Server Authentication";
            this.rbSQLAuthentication.UseVisualStyleBackColor = true;
            this.rbSQLAuthentication.CheckedChanged += new System.EventHandler(this.AuthenticationChanged);
            // 
            // rbWindowsAuthentication
            // 
            this.rbWindowsAuthentication.AutoSize = true;
            this.rbWindowsAuthentication.Location = new System.Drawing.Point(10, 19);
            this.rbWindowsAuthentication.Name = "rbWindowsAuthentication";
            this.rbWindowsAuthentication.Size = new System.Drawing.Size(162, 17);
            this.rbWindowsAuthentication.TabIndex = 0;
            this.rbWindowsAuthentication.TabStop = true;
            this.rbWindowsAuthentication.Text = "Use Windows Authentication";
            this.rbWindowsAuthentication.UseVisualStyleBackColor = true;
            this.rbWindowsAuthentication.CheckedChanged += new System.EventHandler(this.AuthenticationChanged);
            // 
            // btnRefreshServerNames
            // 
            this.btnRefreshServerNames.Location = new System.Drawing.Point(286, 11);
            this.btnRefreshServerNames.Name = "btnRefreshServerNames";
            this.btnRefreshServerNames.Size = new System.Drawing.Size(60, 27);
            this.btnRefreshServerNames.TabIndex = 6;
            this.btnRefreshServerNames.Text = "Refresh";
            this.btnRefreshServerNames.UseVisualStyleBackColor = true;
            this.btnRefreshServerNames.Click += new System.EventHandler(this.btnRefreshServerNames_Click);
            // 
            // cmbSQLServersName
            // 
            this.cmbSQLServersName.FormattingEnabled = true;
            this.cmbSQLServersName.Location = new System.Drawing.Point(85, 15);
            this.cmbSQLServersName.Name = "cmbSQLServersName";
            this.cmbSQLServersName.Size = new System.Drawing.Size(195, 21);
            this.cmbSQLServersName.TabIndex = 5;
            this.cmbSQLServersName.DropDown += new System.EventHandler(this.cmbSQLServersName_DropDown);
            this.cmbSQLServersName.SelectedIndexChanged += new System.EventHandler(this.cmbSQLServersName_SelectedIndexChanged);
            this.cmbSQLServersName.TextChanged += new System.EventHandler(this.cmbSQLServersName_SelectedIndexChanged);
            this.cmbSQLServersName.Leave += new System.EventHandler(this.cmbSQLServersName_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Server name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cDatabases);
            this.groupBox2.Location = new System.Drawing.Point(4, 220);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(342, 129);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Accessible databases";
            // 
            // cDatabases
            // 
            this.cDatabases.FormattingEnabled = true;
            this.cDatabases.Location = new System.Drawing.Point(6, 16);
            this.cDatabases.Name = "cDatabases";
            this.cDatabases.Size = new System.Drawing.Size(330, 108);
            this.cDatabases.TabIndex = 16;
            // 
            // btnRefreshDatabases
            // 
            this.btnRefreshDatabases.Location = new System.Drawing.Point(178, 192);
            this.btnRefreshDatabases.Name = "btnRefreshDatabases";
            this.btnRefreshDatabases.Size = new System.Drawing.Size(168, 27);
            this.btnRefreshDatabases.TabIndex = 14;
            this.btnRefreshDatabases.Text = "Refresh Databases List";
            this.btnRefreshDatabases.UseVisualStyleBackColor = true;
            this.btnRefreshDatabases.Click += new System.EventHandler(this.btnRefreshDatabas_Click);
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Location = new System.Drawing.Point(7, 362);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(115, 27);
            this.btnTestConnection.TabIndex = 9;
            this.btnTestConnection.Text = "Test connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Enabled = false;
            this.btnOK.Location = new System.Drawing.Point(190, 362);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 27);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button3.Location = new System.Drawing.Point(271, 362);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 27);
            this.button3.TabIndex = 11;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ConnectionToSqlForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.button3;
            this.ClientSize = new System.Drawing.Size(351, 397);
            this.ControlBox = false;
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnRefreshDatabases);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnTestConnection);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRefreshServerNames);
            this.Controls.Add(this.cmbSQLServersName);
            this.Controls.Add(this.groupBox1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ConnectionToSqlForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Connection";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnRefreshServerNames;
		private System.Windows.Forms.ComboBox cmbSQLServersName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RadioButton rbWindowsAuthentication;
		private System.Windows.Forms.RadioButton rbSQLAuthentication;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label lblUserName;
		private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btnTestConnection;
		private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnRefreshDatabases;
		private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox cDatabases;

    }
}

