﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SoftServe.DB.ViewsManager.DBManager;

namespace Logica.LDBManager
{
    public partial class ConnectionToSqlForm : Form
    {
        public ConnectionToSqlForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            CleanUpFormCntrols();
        }
        private void cmbSQLServersName_DropDown(object sender, EventArgs e)
        {
            if (null == cmbSQLServersName.DataSource)
            {
                this.Cursor = Cursors.WaitCursor;
                cmbSQLServersName.SelectedIndex = -1;
                cmbSQLServersName.DataSource = SqlServerManager.Current.GetServerNames();
                cmbSQLServersName.SelectedIndex = -1;
                this.Cursor = Cursors.Default;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool result = false;

            if (cDatabases.Items.Count > 0 && TestConection(false))
            {
                SqlServerManager.Current.SqlConnections.Clear();

                foreach (object item in cDatabases.Items)
                {
                    if (rbWindowsAuthentication.Checked)
                    {
                        SqlServerManager.Current.AddConnection(cmbSQLServersName.Text, item.ToString());
                    }
                    else
                    {
                        SqlServerManager.Current.AddConnection(cmbSQLServersName.Text,
                                    item.ToString(),
                                    txtUserName.Text,
                                    txtPassword.Text);
                    }
                }

                result = true;
            }
            if (!result)
            {
                MessageBox.Show("The selected parameters are inappropriate!\nPlease test connection before",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            TestConection(true);
        }

        private void btnRefreshServerNames_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            cmbSQLServersName.SelectedIndex = -1;
            List<string> lst = SqlServerManager.Current.GetServerNames();
            cmbSQLServersName.DataSource = lst;
            cmbSQLServersName.SelectedIndex = -1;
            this.Cursor = Cursors.Default;
        }

        private void btnRefreshDatabas_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbSQLServersName.Text))
            {
                MessageBox.Show("Databases can not be obtained because no server name is specified!",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }
            this.Cursor = Cursors.WaitCursor;
            string userId, userPass;
            userId = userPass = null;

            if (rbSQLAuthentication.Checked)
            {
                userId = txtUserName.Text;
                userPass = txtPassword.Text;
            }
            List<string> databases = SqlServerManager.Current.GetDatabases(cmbSQLServersName.Text, userId, userPass);

            cDatabases.Items.Clear();
            if (null != databases)
            {
                cDatabases.Items.AddRange(databases.ToArray());
                EnableOKButton();
            }

            this.Cursor = Cursors.Default;
        }

        private void EnableOKButton()
        {
            btnOK.Enabled = !string.IsNullOrEmpty(cmbSQLServersName.Text);
        }

        private void cmbSQLServersName_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableOKButton();
        }

        private void cmbSQLServersName_Leave(object sender, EventArgs e)
        {
            string srvname = cmbSQLServersName.Text.Trim();
            if (string.IsNullOrEmpty(srvname))
            {
                CleanUpFormCntrols();
            }
        }

        private void AuthenticationChanged(object sender, EventArgs e)
        {
            lblUserName.Enabled =
                txtUserName.Enabled =
                txtPassword.Enabled =
                lblPassword.Enabled = rbSQLAuthentication.Checked;
        }

        private bool TestConection(bool showMessageBox)
        {
            if (string.IsNullOrEmpty(cmbSQLServersName.Text))
            {
                if (showMessageBox)
                    MessageBox.Show("The connection can not be tested because no server name has been specified!",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            this.Cursor = Cursors.WaitCursor;
            string errorMsg, userId, userPassword;
            userId = rbSQLAuthentication.Checked ? txtUserName.Text : null;
            userPassword = rbSQLAuthentication.Checked ? txtPassword.Text : null;
            bool result = false;
            errorMsg = string.Empty;

            foreach (object item in cDatabases.Items)
            {
                string dbName = item.ToString();
                result = SqlServerManager.Current.CheckConnection(cmbSQLServersName.Text,
                                dbName, userId, userPassword, out errorMsg);
                if (!result)
                    break;
            }

            if (showMessageBox)
                if (result)
                {
                    MessageBox.Show("Test connection succeeded!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(errorMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            this.Cursor = Cursors.Default;
            return result;
        }

        private void CleanUpFormCntrols()
        {
            cmbSQLServersName.DataSource = null;
            cmbSQLServersName.Text = "";
            cDatabases.Items.Clear();
            rbWindowsAuthentication.Checked = true;
            txtPassword.Text = "";
            txtUserName.Text = "";
            EnableOKButton();
        }
    }
}