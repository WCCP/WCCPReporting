﻿using System;
using System.Windows.Forms;

namespace Logica.LDBManager
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            LogManager.WriteMessage("ViewsManager started");
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DBComparer());
        }
    }
}