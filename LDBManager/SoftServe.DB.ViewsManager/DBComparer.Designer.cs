﻿namespace Logica.LDBManager
{
	partial class DBComparer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBComparer));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lServerName = new System.Windows.Forms.Label();
            this.btnChangeConnection = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lDestinationDatabase = new System.Windows.Forms.Label();
            this.lSourceDatabase = new System.Windows.Forms.Label();
            this.cDestinationDatabase = new System.Windows.Forms.ComboBox();
            this.cSourceDatabase = new System.Windows.Forms.ComboBox();
            this.lblState = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lServerName);
            this.groupBox1.Controls.Add(this.btnChangeConnection);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 42);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connected to:";
            // 
            // lServerName
            // 
            this.lServerName.AutoSize = true;
            this.lServerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lServerName.Location = new System.Drawing.Point(26, 18);
            this.lServerName.Name = "lServerName";
            this.lServerName.Size = new System.Drawing.Size(103, 13);
            this.lServerName.TabIndex = 5;
            this.lServerName.Text = "Is not connected";
            // 
            // btnChangeConnection
            // 
            this.btnChangeConnection.Location = new System.Drawing.Point(151, 13);
            this.btnChangeConnection.Name = "btnChangeConnection";
            this.btnChangeConnection.Size = new System.Drawing.Size(156, 23);
            this.btnChangeConnection.TabIndex = 6;
            this.btnChangeConnection.Text = "Change Connection";
            this.btnChangeConnection.UseVisualStyleBackColor = true;
            this.btnChangeConnection.Click += new System.EventHandler(this.btnChangeConnection_Click);
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(151, 6);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(232, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lDestinationDatabase);
            this.groupBox3.Controls.Add(this.lSourceDatabase);
            this.groupBox3.Controls.Add(this.cDestinationDatabase);
            this.groupBox3.Controls.Add(this.cSourceDatabase);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(316, 79);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Target Databases:";
            // 
            // lDestinationDatabase
            // 
            this.lDestinationDatabase.AutoSize = true;
            this.lDestinationDatabase.Location = new System.Drawing.Point(17, 49);
            this.lDestinationDatabase.Name = "lDestinationDatabase";
            this.lDestinationDatabase.Size = new System.Drawing.Size(112, 13);
            this.lDestinationDatabase.TabIndex = 6;
            this.lDestinationDatabase.Text = "Destination Database:";
            // 
            // lSourceDatabase
            // 
            this.lSourceDatabase.AutoSize = true;
            this.lSourceDatabase.Location = new System.Drawing.Point(36, 22);
            this.lSourceDatabase.Name = "lSourceDatabase";
            this.lSourceDatabase.Size = new System.Drawing.Size(93, 13);
            this.lSourceDatabase.TabIndex = 5;
            this.lSourceDatabase.Text = "Source Database:";
            // 
            // cDestinationDatabase
            // 
            this.cDestinationDatabase.DisplayMember = "DataBase";
            this.cDestinationDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cDestinationDatabase.FormattingEnabled = true;
            this.cDestinationDatabase.Location = new System.Drawing.Point(151, 46);
            this.cDestinationDatabase.Name = "cDestinationDatabase";
            this.cDestinationDatabase.Size = new System.Drawing.Size(156, 21);
            this.cDestinationDatabase.TabIndex = 1;
            this.cDestinationDatabase.ValueMember = "DataBase";
            this.cDestinationDatabase.SelectedIndexChanged += new System.EventHandler(this.cDestinationDatabase_SelectedIndexChanged);
            // 
            // cSourceDatabase
            // 
            this.cSourceDatabase.DisplayMember = "DataBase";
            this.cSourceDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cSourceDatabase.FormattingEnabled = true;
            this.cSourceDatabase.Location = new System.Drawing.Point(151, 19);
            this.cSourceDatabase.Name = "cSourceDatabase";
            this.cSourceDatabase.Size = new System.Drawing.Size(156, 21);
            this.cSourceDatabase.TabIndex = 0;
            this.cSourceDatabase.ValueMember = "DataBase";
            this.cSourceDatabase.SelectedIndexChanged += new System.EventHandler(this.cSourceDatabase_SelectedIndexChanged);
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(6, 7);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(42, 13);
            this.lblState.TabIndex = 4;
            this.lblState.Text = "lblState";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 121);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(316, 36);
            this.panel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.lblState);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 157);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(316, 28);
            this.panel2.TabIndex = 6;
            // 
            // DBComparer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 188);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DBComparer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DBComparer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnChangeConnection;
        private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lDestinationDatabase;
        private System.Windows.Forms.Label lSourceDatabase;
        private System.Windows.Forms.ComboBox cDestinationDatabase;
        private System.Windows.Forms.ComboBox cSourceDatabase;
        private System.Windows.Forms.Label lServerName;

	}
}