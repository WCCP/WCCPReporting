﻿using System.Collections.Generic;

namespace SoftServe.DB.ViewsManager.DBManager
{
    internal class SqlTableDescription {
        private const string FULL_TABLE_NAME_TEMPLATE = "[{0}].[{1}].[{2}]";
        private const string SHORT_TABLE_NAME_TEMPLATE = "[{0}].[{1}]";
        private readonly Dictionary<string, SqlFieldDescription> _fields = new Dictionary<string, SqlFieldDescription>();
        private readonly List<string> _pkFields = new List<string>();
        private readonly string _tableName;
        private readonly string _tableSchema;
        private readonly string _dbName;

        internal SqlTableDescription(string dbName, string schema, string name,
                                     Dictionary<string, SqlFieldDescription> fields) {
            _dbName = dbName;
            _tableName = name;
            _tableSchema = schema;
            _fields = fields;
            if (null != fields) {
                _pkFields = new List<string>();

                foreach (string lFldName in fields.Keys)
                    if (fields[lFldName].IsPK)
                        _pkFields.Add(lFldName);
            }
        }

        public string DBName {
            get { return _dbName; }
        }

        public string Schema {
            get { return _tableSchema; }
        }

        public string Name {
            get { return _tableName; }
        }

        public List<string> PKFields {
            get { return _pkFields; }
        }

        public Dictionary<string, SqlFieldDescription> Fields {
            get { return _fields; }
        }

        /// <summary>
        /// FullAlias
        /// </summary>
        public string FullAlias {
            get { return string.Format(FULL_TABLE_NAME_TEMPLATE, _dbName, _tableSchema, _tableName); }
        }

        /// <summary>
        /// ShortAlias
        /// </summary>
        public string ShortAlias {
            get { return string.Format(SHORT_TABLE_NAME_TEMPLATE, _tableSchema, _tableName); }
        }
    }
}