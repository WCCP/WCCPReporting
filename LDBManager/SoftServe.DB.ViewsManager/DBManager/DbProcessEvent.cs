﻿using System;

namespace SoftServe.DB.ViewsManager.DBManager
{
    ///<summary>
    /// 
    ///</summary>
    ///<param name="sender"></param>
    ///<param name="args"></param>
    public delegate void DbProcessEventHandler(object sender, DbProcessEventArgs args);

    ///<summary>
    /// Event args for processing.
    ///</summary>
    public class DbProcessEventArgs : EventArgs
    {
        private const string MESSAGE_TEMPLATE = "{0}: {1} - {2}";

        private readonly string _dbEntityType = SqlScriptsSubFolders.Tables.ToString();
        private string _entityName = string.Empty;

        ///<summary>
        /// Creates new instance of LDBProcessEventArgs
        ///</summary>
        ///<param name="dbEntityType">Entity type</param>
        ///<param name="processType">process type</param>
        public DbProcessEventArgs(string dbEntityType)
        {
            _dbEntityType = dbEntityType;
        }

        ///<summary>
        /// Entity type.
        ///</summary>
        public string DbEntityType
        {
            get { return _dbEntityType; }
        }

        ///<summary>
        /// Name of entity.
        ///</summary>
        public string EntityName
        {
            set { _entityName = value; }
        }

        ///<summary>
        /// returns message
        ///</summary>
        public string ProcessMessage
        {
            get { return string.Format(MESSAGE_TEMPLATE, "CreateScripts", _dbEntityType, _entityName); }
        }
    }
}