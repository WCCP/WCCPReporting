﻿using System.Collections.Generic;
using System.Text;

namespace SoftServe.DB.ViewsManager.DBManager
{
    internal class ViewQueryBuilder {
        private const string TABLE_TEMPTATE = "[{0}].{1}";
        private const string CHECK_OVERLAPPING_SCRIPT0 = @"IF (EXISTS({0})) SELECT 1 ELSE SELECT 0";
        private const string CHECK_OVERLAPPING_SCRIPT1 = "SELECT * FROM {0} AS {1}";
        private const string CHECK_OVERLAPPING_SCRIPT2 = "INNER JOIN {0} AS {1} ON ";
        private const string CHECK_OVERLAPPING_SCRIPT3 = "	{0}.[{1}] = {2}.[{1}] AND ";
        private const string ALIAS = "a{0}";
        private const string UNION_TEMPLATE = "UNION {0}";
        public static string ALTER_VIEW_SCRIPT = @"ALTER VIEW [{0}].[{1}] AS
";

        private const string VIEW_SCRIPT_TEMPLATE =
            @" SELECT *
FROM (
SELECT * 
, Row_Number() OVER (PARTITION BY {0}  ORDER BY (SELECT 1)) AS Counter
FROM
	(
		{1}
	) als
) t
WHERE Counter = 1";

        //private static string SELECT_TABLE_TEMPLATE = "SELECT * FROM {0}{1}";
        private const string SELECT_TABLE_TEMPLATE = "SELECT {0} FROM [{1}].{2} as t";
        private static readonly List<int> MemoTypes = new List<int> {34, 35, 99};
        private readonly List<string> _databases;
        private readonly SqlTableDescription _tableDescription;
//        private readonly Dictionary<string, SqlFieldDescription> _fieldlsList;
        private readonly List<string> _pkFieldsList;
        private readonly QueryToDBView _queryToDbView;
//        private readonly string _tableName;
        private bool? _hasMemoFields;
        private readonly bool _isPKinQueryToLDBView;

        public ViewQueryBuilder(List<string> databases, SqlTableDescription tableDescription)
            : this(databases, tableDescription, null) {
        }

        public ViewQueryBuilder(List<string> databases, SqlTableDescription tableDescription,
                                QueryToDBView queryToDbView) {
            _databases = databases;
            _tableDescription = tableDescription;
            _pkFieldsList = tableDescription.PKFields;
            if (null != queryToDbView) {
                _queryToDbView = queryToDbView;
                List<string> lPKFieldsList = QueryToDBView.GetPKFieldsList(_queryToDbView.PKFields);
                if (null != lPKFieldsList) {
                    _pkFieldsList = lPKFieldsList;
                    _isPKinQueryToLDBView = true;
                }
            }
        }

        private bool HasMemoFields {
            get {
                if (!_hasMemoFields.HasValue) {
                    _hasMemoFields = false;
                    if (null != _tableDescription.Fields)
                        foreach (SqlFieldDescription lFldType in _tableDescription.Fields.Values) {
                            if (MemoTypes.Contains(lFldType.FieldType)) {
                                _hasMemoFields = true;
                                break;
                            }
                        }
                }
                return _hasMemoFields.Value;
            }
        }

        /// <summary>
        /// Returs a script to check matched by PK rows
        /// </summary>
        /// <returns>T-SQL Select</returns>
        public string GetCheckOwerlappingScript() {
            if (null == _pkFieldsList || _pkFieldsList.Count == 0)
                return string.Empty;

            if (_isPKinQueryToLDBView)
                return GetViewCheckOverlappingScript();

            return GetStandardCheckOverlappingScript();
        }

        private string GetStandardCheckOverlappingScript() {
            const int firstAliasIndex = 0;

            StringBuilder lResult =
                new StringBuilder(string.Format(CHECK_OVERLAPPING_SCRIPT1, GetTableFullName(firstAliasIndex),
                    string.Format(ALIAS, firstAliasIndex)));

            for (int lIndex = firstAliasIndex + 1; lIndex < _databases.Count; lIndex++) {
                string lAliasCur = string.Format(ALIAS, lIndex);
                lResult.AppendLine("\n" + string.Format(CHECK_OVERLAPPING_SCRIPT2, GetTableFullName(lIndex), lAliasCur));
                lResult.AppendLine(ComposeJoinConditions(firstAliasIndex, lIndex));
            }

            return string.Format(CHECK_OVERLAPPING_SCRIPT0, lResult);
        }

        private string GetViewCheckOverlappingScript() {
            const int firstAliasIndex = 0;

            string lSelectScript = "(" + string.Format(_queryToDbView.QueryTemplate, "t.*", "{0}") + ")";
            StringBuilder lResult = new StringBuilder(string.Format(CHECK_OVERLAPPING_SCRIPT1,
                string.Format(lSelectScript, _databases[firstAliasIndex]), string.Format(ALIAS, firstAliasIndex)));

            for (int lIndex = firstAliasIndex + 1; lIndex < _databases.Count; lIndex++) {
                string lAliasCur = string.Format(ALIAS, lIndex);
                lResult.AppendLine("\n" + string.Format(CHECK_OVERLAPPING_SCRIPT2,
                    string.Format(lSelectScript, _databases[lIndex]), lAliasCur));
                lResult.AppendLine(ComposeJoinConditions(firstAliasIndex, lIndex));
            }

            return string.Format(CHECK_OVERLAPPING_SCRIPT0, lResult);
        }

        public string GetViewScript(bool useOverlapping) {
            string lResult;
            string lFields = string.Empty;

            // build fields list for select statement
            foreach (string lFld in _tableDescription.Fields.Keys)
                lFields += string.Format(", t.[{0}]", lFld);
            lFields = lFields.Substring(2);

            string lUnionScript =
                ComposeViewScript(
                    string.Format((null != _queryToDbView) ? _queryToDbView.QueryTemplate : SELECT_TABLE_TEMPLATE,
                        lFields, "{0}", _tableDescription.ShortAlias));

            if (null == _pkFieldsList)
                useOverlapping = false;

            if (useOverlapping) {
                string lPkFields = string.Join(", ", _pkFieldsList.ToArray());

                lResult = string.Format(VIEW_SCRIPT_TEMPLATE, /*lFields,*/ lPkFields, lUnionScript);
            }
            else
                lResult = lUnionScript;

            if (_queryToDbView != null)
                lResult = string.Format(_queryToDbView.Query, lResult);

            return string.Format(ALTER_VIEW_SCRIPT, _tableDescription.Schema, _tableDescription.Name) + lResult;
        }

        private string GetTableFullName(int databasePosition) {
            return string.Format(TABLE_TEMPTATE, _databases[databasePosition], _tableDescription.ShortAlias);
        }

        private string ComposeJoinConditions(int databasePosition1, int databasePosition2) {
            string lAliasPrev = string.Format(ALIAS, databasePosition1);
            string lAliasCur = string.Format(ALIAS, databasePosition2);
            string lResult = string.Empty;

            foreach (string lPkField in _pkFieldsList) {
                lResult += string.Format(CHECK_OVERLAPPING_SCRIPT3, lAliasCur, lPkField, lAliasPrev);
            }
            return lResult.Length > 0 ? lResult.Substring(0, lResult.Length - 4) : string.Empty;
        }

        private string ComposeViewScript(string template) {
            StringBuilder lResult = new StringBuilder();

            for (int lIndex = 0; lIndex < _databases.Count; lIndex++) {
                lResult.AppendLine(string.Format(template, _databases[lIndex]));
                if ((_databases.Count - 1) != lIndex) {
                    lResult.AppendLine(string.Format(UNION_TEMPLATE, HasMemoFields ? "ALL" : ""));
                }
            }

            return lResult.ToString();
        }
    }
}