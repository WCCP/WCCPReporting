﻿using System.Collections.Generic;

namespace SoftServe.DB.ViewsManager.DBManager
{
    /// <summary>
    /// Connection descriptions collection
    /// </summary>
    public class ConnectionsCollection : List<ConnectionDescription> {
        public ConnectionDescription Find(ConnectionDescription connection) {
            foreach (ConnectionDescription conn in this) {
                if (conn.Equals(connection)) {
                    return conn;
                }
            }
            return null;
        }

        public ConnectionDescription Find(string serverName, string database, string userName, string password) {
            foreach (ConnectionDescription conn in this)
                if (conn.Equals(serverName, database, userName, password))
                    return conn;
            return null;
        }
    }
}