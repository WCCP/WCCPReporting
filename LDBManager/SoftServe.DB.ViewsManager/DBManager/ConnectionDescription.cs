﻿using System;
using System.Data.SqlClient;

namespace SoftServe.DB.ViewsManager.DBManager
{
    /// <summary>
    /// All neccessary to establish a connection to an MS SQL Server
    /// </summary>
    public class ConnectionDescription {
        private SqlConnectionStringBuilder _connectionString;
        
        public static SqlConnectionStringBuilder GetConnectionString(string sqlServerName, string dbName, string userId,
                                                                     string userPassword) {
            SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
            connectionString.IntegratedSecurity = true;
            connectionString.DataSource = sqlServerName;
            connectionString.InitialCatalog = dbName;
            if (!string.IsNullOrEmpty(userId)) {
                connectionString.UserID = userId;
                connectionString.IntegratedSecurity = false;
            }

            if (!string.IsNullOrEmpty(userPassword))
                connectionString.Password = userPassword;

            return connectionString;
        }

        public ConnectionDescription(string sqlServerName, string dbName, string userId, string userPassword) {
            _connectionString = GetConnectionString(sqlServerName, dbName, userId, userPassword);
        }

        public ConnectionDescription(string sqlServerName, string dbName)
            : this(sqlServerName, dbName, null, null) {
        }

        /// <summary>
        /// return connection parameters to the SW database
        /// </summary>
        public SqlConnectionStringBuilder SqlConnectionString {
            get { return _connectionString; }
        }

        /// <summary>
        /// return SQL server name
        /// </summary>
        public string ServerName {
            get { return _connectionString.DataSource; }
        }

        /// <summary>
        /// return SW database name
        /// </summary>
        public string DataBase {
            get { return _connectionString.InitialCatalog; }
        }
        
        public override bool Equals(object obj) {
            if (!obj.GetType().Equals(typeof (ConnectionDescription))) {
                return false;
            }
            ConnectionDescription connectionDescr = (ConnectionDescription) obj;

            if (this._connectionString.ToString().Equals(connectionDescr.SqlConnectionString.ToString(),
                StringComparison.InvariantCultureIgnoreCase))
                return true;

            if (this.ServerName.Equals(connectionDescr.ServerName, StringComparison.InvariantCultureIgnoreCase) &&
                this.DataBase.Equals(connectionDescr.DataBase, StringComparison.InvariantCultureIgnoreCase) &&
                this._connectionString.UserID.Equals(connectionDescr._connectionString.UserID,
                    StringComparison.InvariantCultureIgnoreCase) &&
                this._connectionString.Password.Equals(connectionDescr._connectionString.Password,
                    StringComparison.InvariantCultureIgnoreCase)
                ) {
                return true;
            }

            return false;
        }

        public bool Equals(string serverName, string database, string userName, string password) {
            string currentUserID = this._connectionString.UserID;
            string currentPassword = this._connectionString.Password;

            if (this.ServerName.Equals(serverName, StringComparison.InvariantCultureIgnoreCase) &&
                this.DataBase.Equals(database, StringComparison.InvariantCultureIgnoreCase) &&
                ((string.IsNullOrEmpty(currentUserID) && string.IsNullOrEmpty(userName))
                 || currentUserID.Equals(userName, StringComparison.InvariantCultureIgnoreCase)) &&
                ((string.IsNullOrEmpty(currentPassword) && string.IsNullOrEmpty(password))
                 || currentPassword.Equals(password, StringComparison.InvariantCultureIgnoreCase))
                ) {
                return true;
            }

            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public ConnectionDescription Clone() {
            return new ConnectionDescription(this.ServerName, this.DataBase, _connectionString.UserID,
                _connectionString.Password);
        }

        /// <summary>
        /// Clone ConnectionDescription 
        /// </summary>
        /// <param name="databaseName">Database name for the new ConnectionDescription</param>
        /// <returns></returns>
        public ConnectionDescription Clone(string databaseName) {
            return new ConnectionDescription(this.ServerName, databaseName, _connectionString.UserID,
                _connectionString.Password);
        }
    }
}