﻿namespace SoftServe.DB.ViewsManager.DBManager
{
    internal class SqlFieldDescription {
        public SqlFieldDescription(byte fieldType, bool isPK) {
            _fieldType = fieldType;
            _isPK = isPK;
        }

        private byte _fieldType;
        private bool _isPK;

        public byte FieldType {
            get { return _fieldType; }
        }

        public bool IsPK {
            get { return _isPK; }
        }
    }
}