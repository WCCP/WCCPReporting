﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using BLToolkit.Data;

namespace SoftServe.DB.ViewsManager.DBManager
{
    public static class DataProvider
    {
        private static List<DbManager> _dbManagers = new List<DbManager>();

        private static string SQL_GET_DATABASES = "sp_databases";
        private static string SQL_CHECK_DB_IS_SW = @"SELECT id FROM sysobjects WHERE xtype = 'U' AND name = 'tblCustomers'";
        private static string SQL_GET_VIEWS = "exec sp_tables  @table_type='''VIEW''', @table_owner = '{0}'";
        private static string SQL_GET_TABLES = "exec sp_tables  @table_type='''TABLE'''";
        private static string SQL_CHECK_OBJECT_EXISTANCE = "select 1 from dbo.sysobjects where id = object_id(N'{0}')";

        public static Exception CheckConnectionString(string connectionString)
        {
            Exception lException;
            CheckConnectionString(connectionString, out lException);
            return lException;
        }

        private static void CheckConnectionString(string connectionString, out Exception exception)
        {
            try
            {
                using (SqlConnection lConn = new SqlConnection(connectionString))
                {
                    lConn.Open();
                    exception = (lConn.State == ConnectionState.Open) ? null : new Exception(connectionString, null);
                }
            }
            catch (Exception lException)
            {
                exception = new Exception(connectionString, lException);
            }
        }

        private static DbManager GetDbManager(string connectionString)
        {
            DbManager lDb = _dbManagers.FirstOrDefault(m => m.Connection.ConnectionString == connectionString);

            if (lDb == null)
            {
                DbManager.AddConnectionString(connectionString);

                lDb = new DbManager();
                lDb.Command.CommandTimeout = 20 * 600;

                _dbManagers.Add(lDb);
            }

            return lDb;
        }

        private static DbManager GetDbManager(ConnectionDescription connection)
        {
            return GetDbManager(connection.SqlConnectionString.ToString());
        }

        public static DataSet GetDatabases(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            DataSet res = lDb.SetSpCommand(SQL_GET_DATABASES).ExecuteDataSet();

            return res;
        }

        public static DataTable GetViewsName(string connectionString, string owner)
        {
            DbManager lDb = GetDbManager(connectionString);
            string lScript = String.Format(SQL_GET_VIEWS, owner);

            DataTable res = lDb.SetCommand(lScript).ExecuteDataTable();

            return res;
        }

        public static DataTable GetTablesName(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            DataTable res = lDb.SetCommand(SQL_GET_TABLES).ExecuteDataTable();

            return res;
        }

        public static List<string> GetDbSchemas(string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);

            DataTable schemasTable = lDb.SetCommand(CreateLDBScripts.GetDBSchemas).ExecuteDataTable();

            List<string> schemas = (from DataRow row in schemasTable.Rows select (string)row[0]).ToList();

            return schemas;
        }

        public static DBObjectTypes GetDBObjectType(string objectName, string connectionString)
        {
            DbManager lDb = GetDbManager(connectionString);
            DBObjectTypes lResult = DBObjectTypes.Undefined;

            string lGetObjectType = String.Format(CreateLDBScripts.GetDBObjectType, objectName);
            object lDBObjectType = lDb.SetCommand(lGetObjectType).ExecuteScalar();

            if (lDBObjectType != null)
            {
                lResult = (DBObjectTypes)Enum.Parse(typeof(DBObjectTypes), lDBObjectType.ToString());
            }

            return lResult;
        }

        public static DataTable GetFieldsDescription(ConnectionDescription sqlConnection)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            DataTable ds = lDb.SetCommand(CreateLDBScripts.GetFiledsDescription).ExecuteDataTable();

            return ds;
        }

        public static DataTable GetViewFieldsDescription(ConnectionDescription sqlConnection)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            DataTable ds = lDb.SetCommand(CreateLDBScripts.GetViewFiledsDescription).ExecuteDataTable();

            return ds;
        }

        public static void DropViewIfExists(ConnectionDescription sqlConnection, string viewName)
        {
            string checkExistance = String.Format(CreateLDBScripts.DropViewIfExistsScript, viewName);

            DbManager lDb = GetDbManager(sqlConnection);
            lDb.SetCommand(checkExistance).ExecuteNonQuery();
        }

        public static bool CheckViewExistance(ConnectionDescription connectionString, string name)
        {
            DbManager lDb = GetDbManager(connectionString);
            object result = lDb.SetCommand(string.Format(SQL_CHECK_OBJECT_EXISTANCE, name)).ExecuteScalar();

            return result != null;
        }

        public static void CreateViewDumbIfNotExists(ConnectionDescription sqlConnection, string viewName)
        {
            string checkExistance = String.Format(CreateLDBScripts.IfNotExistsViewScript, viewName);

            DbManager lDb = GetDbManager(sqlConnection);
            lDb.SetCommand(checkExistance).ExecuteNonQuery();
        }

        public static bool SchemaExists(ConnectionDescription sqlConnection, string schemaName)
        {
            DbManager lDb = GetDbManager(sqlConnection);

            string lSerSql = String.Format(CreateLDBScripts.IfExistsSchemaScript, schemaName);
            int res = lDb.SetCommand(lSerSql).ExecuteScalar<int>();

            return res != 0;
        }

        public static void CreateSchema(ConnectionDescription sqlConnection, string schemaName)
        {
            DbManager lDb = GetDbManager(sqlConnection);

            string lSerSql = String.Format(CreateLDBScripts.CreateSchemaScript, schemaName);
            lDb.SetCommand(lSerSql).ExecuteNonQuery();
        }

        public static void ExecuteNonQuery(ConnectionDescription sqlConnection, string script)
        {
            DbManager lDb = GetDbManager(sqlConnection);
            lDb.SetCommand(script).ExecuteNonQuery();
        }

        public static void AlterView(ConnectionDescription sqlConnection, string schemaName, string name, string directQuery)
        {
            DbManager lDb = GetDbManager(sqlConnection);

            string lSerSql = string.Format(ViewQueryBuilder.ALTER_VIEW_SCRIPT, schemaName, name) + directQuery;
            lDb.SetCommand(lSerSql).ExecuteNonQuery();
        }

        public static int CheckOverlapping(ConnectionDescription connectionString, string script)
        {
            DbManager lDb = GetDbManager(connectionString);
            int result = lDb.SetCommand(script).ExecuteScalar<int>();

            return result;
        }
    }
}
