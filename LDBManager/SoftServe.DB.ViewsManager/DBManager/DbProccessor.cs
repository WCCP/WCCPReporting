﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using NLog;

namespace SoftServe.DB.ViewsManager.DBManager
{
    ///<summary>
    /// Creates LDB database and appropriate views in it based on SW_DDB databases.
    ///</summary>
    public class DbProccessor
    {
        private static readonly DbProccessor Insnance = new DbProccessor();
        private Logger _logger = LoggerHelper.Logger;
        
        private DbProccessor()
        {
        }

        public static DbProccessor Current
        {
            get { return Insnance; }
        }

        public event DbProcessEventHandler OnEntityProcess;

        /// <summary>
        /// Create referencing views in LDB
        /// </summary>
        /// <param name="SWSqlConnections">Connection description to LDB</param>
        /// <param name="destinationConnection"></param>
        /// <returns>If the cleaning up is successfull it returns true</returns>
        public bool CreateReferencingView(ConnectionDescription sourceConnection, ConnectionDescription destinationConnection)
        {
            return CreateReferencingView(sourceConnection, destinationConnection, DbProcessType.Create);
        }

        /// <summary>
        /// Create referencing views in LDB
        /// </summary>
        /// <param name="swSqlConnections">Connection description to LDB</param>
        /// <param name="destinationConnection"></param>
        /// <param name="processType"></param>
        /// <returns></returns>
        public bool CreateReferencingView(ConnectionDescription sourceConnection, ConnectionDescription destinationConnection, DbProcessType processType)
        {
            SqlServerTablesCollection lViews, lSrvTables;
            
            if (!GetObjectsForReferencing(sourceConnection, out lSrvTables, out lViews))
                return false;
            //check and create schemas
            if (!CheckAndCreateSchemas(lSrvTables, lViews, destinationConnection))
                return false;

            if (!CreateReferencingViews(lSrvTables, destinationConnection, SqlScriptsSubFolders.Tables, processType))
                return false;

            return CreateReferencingViews(lViews, destinationConnection, SqlScriptsSubFolders.Views, processType);
        }

        private bool CheckAndCreateSchemas(SqlServerTablesCollection srvTables, SqlServerTablesCollection views,
                                           ConnectionDescription ldbSqlConnection)
        {
            List<string> lSchemaList = new List<string>();

            if (srvTables.Count > 0)
                lSchemaList.AddRange(srvTables[0].TableSchemas);
            if (views.Count > 0)
                foreach (string lViewSchema in views[0].TableSchemas)
                    if (!lSchemaList.Contains(lViewSchema))
                        lSchemaList.Add(lViewSchema);

            foreach (string lSchema in lSchemaList)
            {
                if (lSchema == "dbo")
                    continue;

                if (!DataProvider.SchemaExists(ldbSqlConnection, lSchema))
                {
                    try
                    {
                        DataProvider.CreateSchema(ldbSqlConnection, lSchema);
                        _logger.Info(string.Format("The schema {0} was created successfully.", lSchema));
                    }
                    catch (Exception lException)
                    {
                        _logger.ErrorException("Exception: ", lException);
                        return false;
                    }
                }
            }
            return true;
        }

        private bool CreateReferencingViews(SqlServerTablesCollection srvTables, ConnectionDescription ldbSqlConnection,
                                            SqlScriptsSubFolders entityType, DbProcessType processType)
        {
            _logger.Info(string.Format("Create views inherited from {0}.", entityType.ToString().ToLower()));
            DbProcessEventArgs lArgs = new DbProcessEventArgs(entityType.ToString());

            foreach (SqlTableDescription lTblDescription in srvTables[0].Values)
            {
                string lTableName = lTblDescription.Name;

                lArgs.EntityName = lTableName;
                if (null != OnEntityProcess)
                    OnEntityProcess(null, lArgs);

                string lTableNameWithSchema = lTblDescription.ShortAlias;
                string lSerSql;

                QueryToDBView lDbViewQuery = ReferencingViewConfiguration.Instance.QueryToDBViews.Find(lTblDescription.Schema, lTableName);
                DirectSQLView lDirectSQLView = ReferencingViewConfiguration.Instance.DirectSQLViews.Find(lTblDescription.Schema, lTableName);
                // QueryToLDBView has higher priority than DirectSQLView
                if (null != lDbViewQuery)
                    lDirectSQLView = null;

                if (null != lDirectSQLView)
                {
                    try
                    {
                        DataProvider.CreateViewDumbIfNotExists(ldbSqlConnection, lTableNameWithSchema);
                    }
                    catch (Exception lException)
                    {
                        _logger.Error(lException);
                        return false;
                    }

                    DirectQuery lDirectQuery = lDirectSQLView.QueryList.Find(srvTables.Count);
                    if (null != lDirectQuery)
                    {
                        _logger.Info(string.Format("Create {0} view.", lTableNameWithSchema));
                        try
                        {
                            DataProvider.AlterView(ldbSqlConnection, lTblDescription.Schema, lTblDescription.Name, lDirectQuery.Query);
                            _logger.Info(string.Format("The view {0} was cretaed successfully with direct SQL script.", lTableNameWithSchema));
                            continue;
                        }
                        catch (Exception lEx)
                        {
                            _logger.Error(lEx);
                            return false;
                        }
                    }
                } // if (null != lDirectSQLView)

                ViewQueryBuilder lViewQueryBuilder = new ViewQueryBuilder(srvTables.DBNameList, lTblDescription, lDbViewQuery);

                lSerSql = srvTables[0].EntityType != SqlScriptsSubFolders.Tables
                              ? ""
                              : lViewQueryBuilder.GetCheckOwerlappingScript();

                int lOwerlapped = 0;

                if (srvTables[0].EntityType == SqlScriptsSubFolders.Tables && srvTables.DBNameList.Count > 1 &&
                    !string.IsNullOrEmpty(lSerSql))
                    lOwerlapped = DataProvider.CheckOverlapping(ldbSqlConnection, lSerSql);

                lSerSql = lViewQueryBuilder.GetViewScript(lOwerlapped > 0);
                
                _logger.Info(string.Format("Check {0} existence in {1}.", lTableNameWithSchema, entityType));

                try
                {
                    if (processType == DbProcessType.Create)
                    {
                        DBObjectTypes lDbObjectType = DataProvider.GetDBObjectType(lTblDescription.ShortAlias, ldbSqlConnection.SqlConnectionString.ToString());
                        
                        if (lDbObjectType == DBObjectTypes.V)
                        {
                            DataProvider.DropViewIfExists(ldbSqlConnection, lTableNameWithSchema);
                        }
                        else
                        {
                            if (lDbObjectType != DBObjectTypes.Undefined)
                            {
                                _logger.Info(string.Format("Skip the SQL server {1} \"{0}\".", lTableNameWithSchema,
                                    GetDBObjectName(entityType).ToLower()));
                                continue;
                            }
                        }
                    }
                    else
                    {
                        object lIsViewExists = DataProvider.CheckViewExistance(ldbSqlConnection, lTableNameWithSchema);

                        if (null != lIsViewExists)
                        {
                            _logger.Info(string.Format("Skip the SQL server object \"{0}\".", lTableNameWithSchema));
                            continue;
                        }
                    }
                }
                catch (Exception lEx)
                {
                    _logger.Error(lEx);
                    return false;
                }

                try
                {
                    DataProvider.CreateViewDumbIfNotExists(ldbSqlConnection, lTableNameWithSchema);
                }
                catch (Exception lEx)
                {
                    _logger.ErrorException("Exception", lEx);
                    return false;
                }

                _logger.Info(string.Format("Create {0} view.", lTableNameWithSchema));
                for (int lInd = 0; lInd < 2; lInd++)
                {
                    try
                    {
                        DataProvider.ExecuteNonQuery(ldbSqlConnection, lSerSql);
                        _logger.Info(string.Format("The view {0} was cretaed successfully with {1}.",
                            lTableNameWithSchema, lInd == 0 ? "UNION" : "UNION ALL"));
                        break;
                    }
                    catch (Exception lEx)
                    {
                        _logger.Error(lEx);
                        if (null != lEx.InnerException && lEx.InnerException is SqlException)
                        {
                            SqlException lSqlException = (SqlException)lEx.InnerException;
                            if (lInd == 0 && lSqlException.Number == 421)
                            {
                                _logger.Info(string.Format(
                                    "Try to use UNION ALL to create the view {0}", lTableNameWithSchema));
                                lSerSql = lSerSql.Replace("UNION \r\n", "UNION ALL\r\n");
                                continue;
                            }
                        }
                        return false;
                    }
                }
            }
            _logger.Info(string.Format("Referencing views inherited from {0} have been created.", entityType.ToString().ToLower()));
            
            return true;
        }

        private static string GetDBObjectName(SqlScriptsSubFolders item)
        {
            switch (item)
            {
                case SqlScriptsSubFolders.Indexes:
                    return "Indexe";
                case SqlScriptsSubFolders.Keys:
                    return "Key";
                case SqlScriptsSubFolders.SP:
                    return "Storage procedure";
                case SqlScriptsSubFolders.Tables:
                    return "Table";
                case SqlScriptsSubFolders.UDF:
                    return "UDF";
                case SqlScriptsSubFolders.Views:
                    return "View";
            }
            return string.Empty;
        }

        /// <summary>
        /// Check SW databases for coincidence
        /// </summary>
        /// <param name="swSqlConnections">Connection to SW databases</param>
        /// <param name="tables">List of tables SqlServerTables</param>
        /// <param name="views">List of views</param>
        /// <returns></returns>
        private bool GetObjectsForReferencing(ConnectionDescription sourceConnection,
                                          out SqlServerTablesCollection tables, out SqlServerTablesCollection views)
        {
            _logger.Info("Start getting tables and views from source database");
            
            tables = new SqlServerTablesCollection();
            views = new SqlServerTablesCollection();

            tables.Add(SqlServerManager.Current.GetTablesNameList(sourceConnection));
            views.Add(SqlServerManager.Current.GetViewsNameList(sourceConnection));

            return true;
        }
    }
}