﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using NLog;

namespace SoftServe.DB.ViewsManager.DBManager
{
    /// <summary>
    /// Read configuration for creation of referencing views in LDB
    /// </summary>
    internal class ReferencingViewConfiguration {
        private static SqlObjectExceptions _sqlObjectsExceptions;
        private static QueryToDBViews _queryToDbViews;
        private static DirectSQLViewList _directSQLViewList;
        private static SqlObjectFirstOrderList _firstOrderList;
        private readonly Logger _logger = LoggerHelper.Logger;

        private const string XML_FILE_NAME = "elimination.xml";
        private const string X_PATH_PATTERN0 = "//{0}";
        private static readonly ReferencingViewConfiguration Configurator = new ReferencingViewConfiguration();

        private ReferencingViewConfiguration() {
            XmlDocument lXmlDoc = new XmlDocument();
            XmlNode lXmlNode;

            FileInfo lFileInfo = new FileInfo(Application.ExecutablePath);
            if (File.Exists(Path.Combine(lFileInfo.DirectoryName, XML_FILE_NAME)))
                try {
                    lXmlDoc.Load(Path.Combine(lFileInfo.DirectoryName, XML_FILE_NAME));

                    XmlSerializer lXmlSerializer;
                    lXmlNode =
                        lXmlDoc.SelectSingleNode(string.Format(X_PATH_PATTERN0,
                            DBConfigurationXmlNodes.ArrayOfSqlObjectException));
                    if (null != lXmlNode) {
                        lXmlSerializer = new XmlSerializer(typeof (SqlObjectExceptions));
                        using (StringReader lReader = new StringReader(lXmlNode.OuterXml)) {
                            _sqlObjectsExceptions = (SqlObjectExceptions) lXmlSerializer.Deserialize(lReader);
                        }
                    }

                    lXmlNode =
                        lXmlDoc.SelectSingleNode(string.Format(X_PATH_PATTERN0,
                            DBConfigurationXmlNodes.ArrayOfQueryToDBView));
                    if (null != lXmlNode) {
                        lXmlSerializer = new XmlSerializer(typeof (QueryToDBViews));
                        using (StringReader lReader = new StringReader(lXmlNode.OuterXml)) {
                            _queryToDbViews = (QueryToDBViews) lXmlSerializer.Deserialize(lReader);
                        }
                    }

                    lXmlNode =
                        lXmlDoc.SelectSingleNode(string.Format(X_PATH_PATTERN0,
                            DBConfigurationXmlNodes.ArrayOfDirectSQL));
                    if (null != lXmlNode) {
                        XmlRootAttribute lXmlRootAttribute = new XmlRootAttribute();
                        lXmlRootAttribute.ElementName = DBConfigurationXmlNodes.ArrayOfDirectSQL.ToString();
                        lXmlRootAttribute.IsNullable = true;
                        lXmlSerializer = new XmlSerializer(typeof (DirectSQLViewList), lXmlRootAttribute);
                        using (StringReader lReader = new StringReader(lXmlNode.OuterXml)) {
                            _directSQLViewList = (DirectSQLViewList) lXmlSerializer.Deserialize(lReader);
                        }
                    }

                    lXmlNode =
                        lXmlDoc.SelectSingleNode(string.Format(X_PATH_PATTERN0,
                            DBConfigurationXmlNodes.ArrayOfFirstOrderObjects));
                    if (null != lXmlNode)
                    {
                        XmlRootAttribute lXmlRootAttribute = new XmlRootAttribute();
                        lXmlRootAttribute.ElementName = DBConfigurationXmlNodes.ArrayOfFirstOrderObjects.ToString();
                        lXmlRootAttribute.IsNullable = true;
                        lXmlSerializer = new XmlSerializer(typeof(SqlObjectFirstOrderList), lXmlRootAttribute);
                        using (StringReader lReader = new StringReader(lXmlNode.OuterXml))
                        {
                            _firstOrderList = (SqlObjectFirstOrderList)lXmlSerializer.Deserialize(lReader);
                        }
                    }
                }
                catch (Exception lException) {
                    _logger.Error(lException);
                    return;
                }
        }

        public DirectSQLViewList DirectSQLViews {
            get { return _directSQLViewList; }
        }

        /// <summary>
        /// List of tables not to be created in LDB
        /// </summary>
        public SqlObjectExceptions SqlObjectsExceptions {
            get { return _sqlObjectsExceptions; }
        }

        /// <summary>
        /// List of tables to be created
        /// </summary>
        public SqlObjectFirstOrderList SqlObjectFirstOrderList
        {
            get { return _firstOrderList; }
        }

        /// <summary>
        /// List of tables with specific query for creation in LDB
        /// </summary>
        public QueryToDBViews QueryToDBViews {
            get { return _queryToDbViews; }
        }

        internal static ReferencingViewConfiguration Instance {
            get { return Configurator; }
        }

        #region Nested type: LDBConfigurationXmlNodes

        /// <summary>
        /// Nodes in the elimination.xml file
        /// </summary>
        private enum DBConfigurationXmlNodes {
            /// <summary>
            /// Describes collection of table names not to be created in LDB
            /// </summary>
            ArrayOfSqlObjectException,

            /// <summary>
            /// Describes collection of table names with specific definition in LDB view
            /// </summary>
            ArrayOfQueryToDBView,

            /// <summary>
            /// Describes collection of direct SQL statements for creating views in LDB
            /// </summary>
            ArrayOfDirectSQL,

            /// <summary>
            /// Describes collection of objects that are to be created regardless to other conditions - has the hghtest precedence
            /// </summary>
            ArrayOfFirstOrderObjects
        }

        #endregion
    }
}