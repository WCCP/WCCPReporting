﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SoftServe.DB.ViewsManager.DBManager;

namespace Logica.LDBManager
{
    public partial class DBComparer : Form
    {
        private ConnectionToSqlForm connectionForm = new ConnectionToSqlForm();
        private const string RECREATE_VIEWS_SUCCESSFULLY = "Recreate Views on LDB completed successfully.";
        private const string RECREATE_VIEWS_FAILED = "Recreate Views failed.";

        public DBComparer()
        {
            InitializeComponent();
            DbProccessor.Current.OnEntityProcess += Current_OnEntityProcess;
            SqlServerManager.Current.SetSkipingSqlOjects(false);
            lblState.Text = string.Empty;
        }

        private void Current_OnEntityProcess(object sender, DbProcessEventArgs args)
        {
            lblState.Text = args.ProcessMessage;
            Invalidate();
            Application.DoEvents();
        }

        /// <summary>
        /// Open the connection definition form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangeConnection_Click(object sender, EventArgs e)
        {
            connectionForm.ShowDialog(this);
            RefreshDBList();
        }

        /// <summary>
        /// Start a process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            SuspendLayout();
            Cursor = Cursors.WaitCursor;
            lblState.Text = string.Empty;

            try
            {
                ConnectionDescription lSourceDB = (cSourceDatabase.SelectedItem as ConnectionDescription).Clone();
                ConnectionDescription lDestinationDB = (cDestinationDatabase.SelectedItem as ConnectionDescription).Clone();

                bool lResult = DbProccessor.Current.CreateReferencingView(lSourceDB, lDestinationDB);
                if (!lResult)
                {
                    MessageBox.Show(string.Format(RECREATE_VIEWS_FAILED), Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lblState.Text = RECREATE_VIEWS_FAILED;
                    Cursor = Cursors.Default;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
                lblState.Text = RECREATE_VIEWS_SUCCESSFULLY;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void EnableButtons()
        {
            btnStart.Enabled = !string.IsNullOrEmpty(cDestinationDatabase.Text) && !string.IsNullOrEmpty(cSourceDatabase.Text) && cDestinationDatabase.Text != cSourceDatabase.Text;
        }

        private void RefreshDBList()
        {
            SuspendLayout();
            List<ConnectionDescription> lConnections = SqlServerManager.Current.SqlConnections;

            if(lConnections.Count == 0)
                return;
          
            lServerName.Text = lConnections[0].ServerName;

            cDestinationDatabase.Items.Clear();
            cSourceDatabase.Items.Clear();

            cDestinationDatabase.SelectedIndex = -1;
            cSourceDatabase.SelectedIndex = -1;
            cDestinationDatabase.Text = "";
            cSourceDatabase.Text = "";

            foreach (ConnectionDescription lConnection in lConnections)
            {
                cDestinationDatabase.Items.Add(lConnection);
                cSourceDatabase.Items.Add(lConnection);
            }

            EnableButtons();
            ResumeLayout();
        }

        private void cSourceDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons();
        }

        private void cDestinationDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons();
        }
    }
}