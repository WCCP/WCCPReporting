﻿namespace Logica.LDBSync.Main
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.cbDetailed = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnRunWithReInit = new System.Windows.Forms.Button();
            this.BGW = new System.ComponentModel.BackgroundWorker();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageLog = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbTextBox = new System.Windows.Forms.RichTextBox();
            this.tabPageOption = new System.Windows.Forms.TabPage();
            this.tbGenPerBatch = new System.Windows.Forms.TextBox();
            this.lblGenerationPerBatch = new System.Windows.Forms.Label();
            this.agentPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.cbUseReplMerg = new System.Windows.Forms.CheckBox();
            this.tabControl.SuspendLayout();
            this.tabPageLog.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPageOption.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(662, 441);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Закрыть";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Location = new System.Drawing.Point(457, 441);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 2;
            this.btnRun.Text = "Старт";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // cbDetailed
            // 
            this.cbDetailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbDetailed.AutoSize = true;
            this.cbDetailed.Checked = true;
            this.cbDetailed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDetailed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDetailed.Location = new System.Drawing.Point(6, 441);
            this.cbDetailed.Name = "cbDetailed";
            this.cbDetailed.Size = new System.Drawing.Size(269, 17);
            this.cbDetailed.TabIndex = 3;
            this.cbDetailed.Text = "Показывать детальный процесс синхронизации";
            this.cbDetailed.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(6, 412);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(731, 23);
            this.progressBar1.TabIndex = 4;
            // 
            // btnRunWithReInit
            // 
            this.btnRunWithReInit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunWithReInit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRunWithReInit.Location = new System.Drawing.Point(538, 441);
            this.btnRunWithReInit.Name = "btnRunWithReInit";
            this.btnRunWithReInit.Size = new System.Drawing.Size(118, 23);
            this.btnRunWithReInit.TabIndex = 5;
            this.btnRunWithReInit.Text = "Полный рестарт";
            this.btnRunWithReInit.UseVisualStyleBackColor = true;
            this.btnRunWithReInit.Click += new System.EventHandler(this.btnRunWithReInit_Click);
            // 
            // BGW
            // 
            this.BGW.WorkerReportsProgress = true;
            this.BGW.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_DoWork);
            this.BGW.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BGW_ProgressChanged);
            this.BGW.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGW_RunWorkerCompleted);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl.Controls.Add(this.tabPageLog);
            this.tabControl.Controls.Add(this.tabPageOption);
            this.tabControl.Location = new System.Drawing.Point(2, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(739, 403);
            this.tabControl.TabIndex = 6;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPageLog
            // 
            this.tabPageLog.Controls.Add(this.panel1);
            this.tabPageLog.Location = new System.Drawing.Point(4, 25);
            this.tabPageLog.Name = "tabPageLog";
            this.tabPageLog.Size = new System.Drawing.Size(731, 374);
            this.tabPageLog.TabIndex = 0;
            this.tabPageLog.Text = "Log";
            this.tabPageLog.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.rbTextBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(1);
            this.panel1.Size = new System.Drawing.Size(731, 374);
            this.panel1.TabIndex = 2;
            // 
            // rbTextBox
            // 
            this.rbTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.rbTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rbTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbTextBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbTextBox.Location = new System.Drawing.Point(1, 1);
            this.rbTextBox.Name = "rbTextBox";
            this.rbTextBox.ReadOnly = true;
            this.rbTextBox.Size = new System.Drawing.Size(729, 372);
            this.rbTextBox.TabIndex = 2;
            this.rbTextBox.Text = "";
            // 
            // tabPageOption
            // 
            this.tabPageOption.Controls.Add(this.cbUseReplMerg);
            this.tabPageOption.Controls.Add(this.tbGenPerBatch);
            this.tabPageOption.Controls.Add(this.lblGenerationPerBatch);
            this.tabPageOption.Controls.Add(this.agentPropertyGrid);
            this.tabPageOption.Location = new System.Drawing.Point(4, 25);
            this.tabPageOption.Name = "tabPageOption";
            this.tabPageOption.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOption.Size = new System.Drawing.Size(731, 374);
            this.tabPageOption.TabIndex = 1;
            this.tabPageOption.Text = "Options";
            this.tabPageOption.UseVisualStyleBackColor = true;
            // 
            // tbGenPerBatch
            // 
            this.tbGenPerBatch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGenPerBatch.Location = new System.Drawing.Point(126, 6);
            this.tbGenPerBatch.Name = "tbGenPerBatch";
            this.tbGenPerBatch.Size = new System.Drawing.Size(100, 20);
            this.tbGenPerBatch.TabIndex = 2;
            this.tbGenPerBatch.Text = "10";
            // 
            // lblGenerationPerBatch
            // 
            this.lblGenerationPerBatch.AutoSize = true;
            this.lblGenerationPerBatch.Location = new System.Drawing.Point(7, 7);
            this.lblGenerationPerBatch.Name = "lblGenerationPerBatch";
            this.lblGenerationPerBatch.Size = new System.Drawing.Size(112, 13);
            this.lblGenerationPerBatch.TabIndex = 1;
            this.lblGenerationPerBatch.Text = "Generations per batch";
            // 
            // agentPropertyGrid
            // 
            this.agentPropertyGrid.Location = new System.Drawing.Point(382, 6);
            this.agentPropertyGrid.Name = "agentPropertyGrid";
            this.agentPropertyGrid.Size = new System.Drawing.Size(343, 362);
            this.agentPropertyGrid.TabIndex = 0;
            this.agentPropertyGrid.Visible = false;
            // 
            // cbUseReplMerg
            // 
            this.cbUseReplMerg.AutoSize = true;
            this.cbUseReplMerg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUseReplMerg.Location = new System.Drawing.Point(10, 32);
            this.cbUseReplMerg.Name = "cbUseReplMerg";
            this.cbUseReplMerg.Size = new System.Drawing.Size(105, 17);
            this.cbUseReplMerg.TabIndex = 3;
            this.cbUseReplMerg.Text = "Use replmerg.exe";
            this.cbUseReplMerg.UseVisualStyleBackColor = true;
            // 
            // Report
            // 
            this.AcceptButton = this.btnRun;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(750, 476);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnRunWithReInit);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.cbDetailed);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Синхронизатор";
            this.tabControl.ResumeLayout(false);
            this.tabPageLog.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabPageOption.ResumeLayout(false);
            this.tabPageOption.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.CheckBox cbDetailed;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnRunWithReInit;
        private System.ComponentModel.BackgroundWorker BGW;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageLog;
        private System.Windows.Forms.TabPage tabPageOption;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox rbTextBox;
        private System.Windows.Forms.PropertyGrid agentPropertyGrid;
        private System.Windows.Forms.TextBox tbGenPerBatch;
        private System.Windows.Forms.Label lblGenerationPerBatch;
        private System.Windows.Forms.CheckBox cbUseReplMerg;
    }
}