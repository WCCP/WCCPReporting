﻿//#define _LOCAL_
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using Logica.LDBSync.Main.LoggingServiceReference;
using Logica.Password.Generator;
using Microsoft.SqlServer.Replication;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace Logica.LDBSync.Main
{
    /// <summary>
    /// Replication mechanizm
    /// </summary>
    public partial class Report : Form
    {
        private string headerText = string.Empty;
        private string LDBVersionTemplate = " | LDB Version: {0} - {1}";
        private string LDBVersion = "Unknown";
        private string LastSuccessSyncTime = "00:00:00 00/00/00";
        private bool isFirstStart = true;

        string connectionString;

        //private string lastException = null;
        private string logFileName = string.Empty;
        private FileStream logFile = null;
        //private string customerID = "0";
        //private string customerName = "Неизвестен";

        private InterProcessLock sync_lock = new InterProcessLock("Global\\LDBSync", TimeSpan.Zero); //this lock acquired only for syncronization process

        private FileSystemWatcher watcher; //we use this watcher to monitor log file of currently active LDBSync instance
        private int StartLogLine; //amount of lines in TextBox at time when we start watcher

        private Process CmdProcess = new Process();

        /// <summary>
        /// Show if application started avtomatically by scheduler or manually 
        /// </summary>
        private bool guiMode = false;

        /// <summary>
        /// Constructor
        /// </summary>
        public Report(bool guiMode)
        {
            this.guiMode = guiMode;
            
            if (!guiMode) //we don't need initialize GUI if application runs in background mode
                return;

            InitializeComponent();
            this.Text += " " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            //move all error-prone and time-consuming tasks out of constructor
            BackgroundWorker bw = new BackgroundWorker();
            bw.WorkerSupportsCancellation = false;
            bw.WorkerReportsProgress = false;
            bw.DoWork += new DoWorkEventHandler(InitThread_DoWork);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(InitThread_Completed);
            bw.RunWorkerAsync();
            WriteMessage("Получаю версию LDB ...");

            //InitializeComponentInfo();

            //Customize buttons
            if (!IsUserAdministrator())
            {
                btnRunWithReInit.Visible = false;
                int buttonSpace = btnClose.Location.X - btnRunWithReInit.Location.X - btnRunWithReInit.Width;
                int newPosition = btnClose.Location.X - (btnRun.Width + buttonSpace); ;
                btnRun.Left = newPosition;
            }
        }

        private void InitThread_DoWork(object sender, DoWorkEventArgs e)
        {
            connectionString = ConfigManager.ConnectionString.ConnectionString;
            LDBVersion = GetLDBVersion();
            LastSuccessSyncTime = GetLastSuccessSyncTime();
        }

        private void InitThread_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                WriteMessage(String.Format("Error: {0}", e.Error.Message));
            }
            else
            {
                string DBUser = GetDBUser();

                Text = string.Format("Синхронизатор {0} | UserID: {1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DBUser);
                headerText = Text;

                Text += string.Format(LDBVersionTemplate, LDBVersion, LastSuccessSyncTime);
                rbTextBox.Clear();
            }
        }

        /*
         * заменен вызовом InitThread
        private bool InitializeComponentInfo()
        {
            bool result = true;
            string DBUser = "";
            try
            {
                DBUser = GetDBUser();

                Text = string.Format("Синхронизатор version {0} | UserID: {1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DBUser);
                headerText = Text;

                try
                {
                    LDBVersion = GetLDBVersion();
                    LastSuccessSyncTime = GetLastSuccessSyncTime();
                }
                catch (Exception ex)
                {
                    WriteMessage(String.Format("Не могу обратиться к LDB : {0}", ex.Message));
                }

                Text += string.Format(LDBVersionTemplate, LDBVersion, LastSuccessSyncTime);

                //if (LDBVersion != "Unknown")
                //    EnsureCustomerInfo();

            }
            catch (Exception ex)
            {
                WriteMessage("Ошибка при получении версии LDB", ex, EventLogEntryType.Error);
                result = false;
            }

            return result;
        }
        */

        private void CreateLogFile()
        {
            try
            {
                foreach (string file in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.log"))
                    File.Delete(file);
            }
            catch(Exception e)
            {
                WriteMessage("Ошибка удаления файлов *.log", e, EventLogEntryType.Error);
            }

            try
            {
                LogManager.LastAppicationException = null;
                logFileName = string.Format("{0}_{1}.log", Environment.MachineName, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                logFileName = String.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, logFileName);
                logFile = new FileStream(logFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
            }
            catch (Exception e)
            {
                WriteMessage("Ошибка создания файла с логом",e, EventLogEntryType.Error);
            }
        }

        private void SendStatusToWS(bool IsSyncOK)
        {
            string errorMessage = null;
            
            if(!IsSyncOK)  
                errorMessage = LogManager.LastAppicationException;

            try
            {
                if (ConfigManager.NeedWSLogging)
                {
                    LoggingServiceSoapClient client = new LoggingServiceSoapClient();
                    var result = client.LogLDBSyncStatus
                        (
                            new LogLDBSyncStatusRequest
                            {
                                ErrorMessage = errorMessage,
                                MachineName = Environment.MachineName,
                                SyncStatus = IsSyncOK?0:1,
                                LDBVersion = this.LDBVersion
                            }
                        );
                    WriteMessage(string.Format("Статус запроса логирования на Web сервис: {0}",
                                               result.ErrorCode == 0 ? "OK" : result.ErrorMessage));
                }
            }
            catch (Exception e)
            {
                WriteMessage("Ошибка отправки статусного сообщения на веб-сервис", e, EventLogEntryType.Error);
            }

        }

        private void CloseLogFile(SyncReturnType resultExec)
        {

            try
            {
                if (null != logFile)
                {
                    logFile.Flush();
                    FlushFileBuffers(logFile.SafeFileHandle.DangerousGetHandle());
                    logFile.Close();

                    if (ConfigManager.NeedUploadToFTP)
                    {
                        String result = UploadFile(ConfigManager.FTPURL, ConfigManager.FTPUser, ConfigManager.FTPPWD, logFileName);
                        WriteMessage(string.Format("Статус отправки лог-файла: {0}", result));
                    }
                }
            }
            catch (Exception e)
            {
                WriteMessage("Ошибка отправки файла с логом на FTP", e, EventLogEntryType.Error);
            }

        }

        public bool IsUserAdministrator()
        {
            bool isAdmin = true;
            try
            {
                //get the currently logged in user
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException)
            {
                isAdmin = false;
            }
            catch (Exception)
            {
                isAdmin = false;
            }
            return isAdmin;
        }

        private string GetDBUser()
        {
            string DBUser = Environment.UserName;
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connectionString);
            if (!sb.IntegratedSecurity)
                DBUser = sb.UserID;
            return DBUser;
        }

        /*

private void EnsureCustomerInfo()
{
using (SqlConnection conn = new SqlConnection(connectionString))
{
    conn.Open();
    SqlCommand cmd = new SqlCommand(@"select top 1 Cust_Id, Cust_NAME from tblCustomers order by Cust_Id", conn);
    using (SqlDataReader reader = cmd.ExecuteReader())
    {
        if (reader.HasRows)
        {
            reader.Read();
            customerID = reader[0].ToString();
            customerName = reader[1].ToString();
        }
    }
    conn.Close();
}
customerName = customerName.Trim(new char[2] { ' ', '\"' });
char[] invalidFNChars = Path.GetInvalidFileNameChars();
customerName = customerName.Trim(invalidFNChars);
foreach (char ch in invalidFNChars)
{
    customerName = customerName.Replace(ch, ' ');
}
}
 */

        private string GetLDBVersion()
        {
            string version = "";

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"select top 1 ParamValue from DW_Params where ParamName='Sync_Version'", conn);
                object res = cmd.ExecuteScalar();
                if (res != null)
                    version = res.ToString();
                conn.Close();
            }

            return version;
        }

        private string GetLastSuccessSyncTime()
        {
            string time = "";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"select top 1 ParamValue from DW_Params where ParamName='Sync_LastSuccesTime'", conn);
                object res = cmd.ExecuteScalar();
                if (res != null)
                    time = res.ToString();
                conn.Close();
            }

            return time;
        }

        private void SetLastSuccessSyncTime()
        {
            try
            {
                LastSuccessSyncTime = DateTime.Now.ToString("HH:mm:ss dd/MM/yy");
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string command = string.Format(@"delete from DW_Params where ParamName='Sync_LastSuccesTime'; insert into DW_Params (ParamName, ParamValue) values('Sync_LastSuccesTime', '{0}')", LastSuccessSyncTime);
                    SqlCommand cmd = new SqlCommand(command, conn);
                    object res = cmd.ExecuteScalar();
                    conn.Close();
                }

            }
            catch (Exception ex)
            {
                WriteMessage(String.Format("Не могу установить LastSuccessSyncTime : {0}", ex.Message));
            }
        }

        private bool ReinitMergeSubscription()
        {
            string publisher = ConfigurationManager.AppSettings["PublisherName"];
            string publisher_db = ConfigurationManager.AppSettings["PublicationDB"];
            string publication = ConfigurationManager.AppSettings["PublicationName"];

            bool result = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_reinitmergepullsubscription",conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 300 };
                cmd.Parameters.AddWithValue("@publisher", publisher);
                cmd.Parameters.AddWithValue("@publisher_db", publisher_db);
                cmd.Parameters.AddWithValue("@publication", publication);
                cmd.Parameters.AddWithValue("@upload_first", "false");
                SqlParameter ret = new SqlParameter("@Return_Value", SqlDbType.Int) {Direction = ParameterDirection.ReturnValue};
                cmd.Parameters.Add(ret);
                object res = cmd.ExecuteNonQuery();
                result = (int)ret.Value == 0;
                conn.Close();
            }

            return result;
        }


        #region File watching

        private void WatchLogFile()
        {
            WriteMessage("Процесс синхронизации запущен в другом процессе LDBSync");
            WriteMessage("Вывожу лог активного процесса");

            btnRun.Enabled = false;
            btnRunWithReInit.Enabled = false;
            cbDetailed.Enabled = false;
            rbTextBox.ForeColor = SystemColors.ControlDarkDark;

            StartLogLine = rbTextBox.Lines.Length;

            string logfile = "";
            foreach (string file in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.log"))
                logfile = file;
            if (logfile == "")
            {
                WriteMessage("Файл лога не найден");
                return;
            }

            watcher = new FileSystemWatcher();
            watcher.Path = AppDomain.CurrentDomain.BaseDirectory;
            watcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.LastWrite;
            watcher.Filter = Path.GetFileName(logfile);

            watcher.Changed += new FileSystemEventHandler(OnLogFileChanged);
            watcher.EnableRaisingEvents = true;
        }

        private void OnLogFileChanged(object source, FileSystemEventArgs e)
        {
            
            MethodInvoker updateText = new MethodInvoker(() =>
            {
                using (StreamReader sr = new StreamReader(new FileStream(e.FullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite), System.Text.Encoding.UTF8))
                {
                    int i = 0;
                    foreach (string s in rbTextBox.Lines)
                    {
                        if (i++ < StartLogLine) //skipping all rows which were in a TextBox when we start file monitoring
                            continue;

                        sr.ReadLine(); //skipping part of the file we've already shown
                    }
                    rbTextBox.AppendText(sr.ReadToEnd());
                }

                Thread.Sleep(100);

                //there is no event like OnFileClose
                //so we are trying to open file for writing to catch a moment when file is available 
                bool FileWasClosed = false;
                try
                {
                    using (FileStream file = new FileStream(e.FullPath, FileMode.Append, FileAccess.Write))
                    {
                        file.Close();
                        FileWasClosed = true;
                    }
                }
                catch (IOException)
                {
                    FileWasClosed = false;
                    //WriteMessage(ex.Message);
                }

                if (FileWasClosed) //enable GUI back
                {
                    btnRun.Enabled = true;
                    btnRunWithReInit.Enabled = true;
                    cbDetailed.Enabled = true;
                    rbTextBox.ForeColor = SystemColors.ControlText;

                    watcher.Changed -= OnLogFileChanged;
                    watcher.EnableRaisingEvents = false;
                }

            });

            if (InvokeRequired)
                Invoke(updateText);
            else
                updateText();
        }
        #endregion

        /// <summary>
        /// Start sync
        /// </summary>
        private void btnRun_Click(object sender, EventArgs e)
        {
            StartSyncProcess(false);
        }

        /// <summary>
        /// Start sync with reinitialization
        /// </summary>
        private void btnRunWithReInit_Click(object sender, EventArgs e)
        {
            StartSyncProcess(true);
        }

        private void StartSyncProcess(bool isReInit)
        {
            if (!sync_lock.IsAcquired)
                sync_lock.Acquire();

            if (sync_lock.IsAcquired)
            {
                //WriteMessage("Lock acquired");

                btnClose.Enabled = false;
                btnRun.Enabled = false;
                btnRunWithReInit.Enabled = false;
                cbDetailed.Enabled = false;
                progressBar1.Value = progressBar1.Minimum;

                if (cbUseReplMerg.Checked)
                    StartReplMerg(isReInit);
                else
                    BGW.RunWorkerAsync(isReInit); //asyncronous StartProcess()

                //sync_lock.Release();
            }
            else
            {
                WatchLogFile();
            }
        }

        /// <summary>
        /// Close form
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        #region Sync
        /// <summary>
        /// Sync return type
        /// </summary>
        public enum SyncReturnType
        {
            OK = 0,
            Failed,
            NeedRepeat
        }

        // repeat logic is not implemented
        //int repeatTimes = 0;

        /// <summary>
        /// Add new Certificate for browser
        /// </summary>
        private void SetupCertificate()
        {
            try
            {
                string folderName = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string[] files = Directory.GetFiles(folderName, "*.crt");

                if (files.Length == 0)
                    return;

                X509Store store = new X509Store(StoreName.AuthRoot, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadWrite);

                string certPath;
                X509Certificate2 cert;
                foreach (string file in files)
                {
                    certPath = Path.Combine(folderName, file);
                    //X509Certificate2 cert = new X509Certificate2(certPath, "Cthnbabrfn");
                    cert = new X509Certificate2(certPath);
                    store.Add(cert);
                    WriteMessage("Сертификат '" + file + "' установлен корректно", System.Diagnostics.EventLogEntryType.Information);
                    try { File.Delete(certPath); }
                    catch { }
                }
                store.Close();
            }
            catch
            {
                WriteMessage("Ошибка при установке сертификатов", System.Diagnostics.EventLogEntryType.Warning);
            }
        }

        // callback used to validate the certificate in an SSL conversation
        private static bool ValidateRemoteCertificate(
            object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors policyErrors)
        {
            X509Store store = new X509Store(StoreName.AuthRoot, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadWrite);
            X509Certificate2 cert = new X509Certificate2(certificate);

            X509Certificate2Collection foundCerts = store.Certificates.Find(X509FindType.FindByThumbprint, cert.Thumbprint, false);
            if (foundCerts.Count == 0) //no certificate found
            {
                store.Add(cert);
                store.Close();
            }

            return true;
        }

        /// <summary>
        /// This method calls InternetURL and auto-accepts SSL certificate
        /// </summary>
        /// <param name="uri">URI to check</param>
        /// <param name="InternetLogin">login name for request</param>
        /// <param name="InternetPassword">password for request</param>
        public static void AutoAcceptCertificate(string uri, string InternetLogin, string InternetPassword)
        {
            if (uri == null || uri.Trim() == "")
                return;

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);

            if (!String.IsNullOrEmpty(InternetLogin))
                webRequest.Credentials = new NetworkCredential(InternetLogin, InternetPassword);

            webRequest.KeepAlive = false;
            // allows for validation of SSL conversations
            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (Exception)
            {

            }
            finally
            {
                if (response != null)
                    response.Close();
            }

            ServicePointManager.ServerCertificateValidationCallback -= ValidateRemoteCertificate;

        }

        private string GetConnInfoWithoutPassword(SqlConnectionStringBuilder sb)
        {
            string connInfo = string.Empty;

            if (null != sb)
            {
                if (sb.IntegratedSecurity)
                    connInfo = sb.ToString();
                else
                    connInfo = string.Format("Data Source={0};Initial Catalog={1};Integrated Security=False;UserID={2};Password=********", sb.DataSource, sb.InitialCatalog, sb.UserID);
            }

            return connInfo;
        }

        /// <summary>
        /// Main sync process
        /// </summary>
        /// <returns>process result</returns>
        private SyncReturnType SyncEngine(bool isReInit)
        {
            if (string.IsNullOrEmpty(connectionString))
                connectionString = ConfigManager.ConnectionString.ConnectionString;

            SyncReturnType res = SyncReturnType.OK;
            string key = "созвездие";
            try
            {   //Get connection string
                    //bool isDef = true;
                    SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connectionString);

                    if (null != sb)
                    {
                        WriteMessage(" - Данные о параметрах соединения получены корректно: " + GetConnInfoWithoutPassword(sb));
                    }
                    else
                    {
                        WriteMessage(" - Секция с параметрами подключения к SQL серверу в конфигурационном файле не задана или задана неверно (выполнение прервано)", System.Diagnostics.EventLogEntryType.Error);
                        return SyncReturnType.Failed;
                    }

                    string subscriberDbName = sb.InitialCatalog;

                    ServerConnection conn;
                    try
                    {
                        SqlConnection sc = new SqlConnection(sb.ToString());
                        conn = new ServerConnection(sc);
                        conn.Connect();
                    }
                    catch (Exception ex)
                    {
                        WriteMessage(" - Невозможно создать Subscriber ServerConnection (выполнение прервано)"
                            + GetConnInfoWithoutPassword(sb), ex, EventLogEntryType.Error);
                        return SyncReturnType.Failed;
                    }
                    WriteMessage(" - Cоединение успешно создано");

                    /*
                       //Get sync settings from DB
                        ReplicationSettings settings = GetReplicationSync(sb.ConnectionString);
                        WriteMessage(" - Данные из базы получены успешно");

                        if (settings == null)
                        {//Set default settings
                            WriteMessage(" - Процедура spDW_Repl_SyncGet отсутствует в базе данных. Используются параметры по-умолчанию", System.Diagnostics.EventLogEntryType.Warning);
                            settings = new ReplicationSettings();//set default settings
                            res = SyncReturnType.NeedRepeat;
                        }
                        else
                        {
                            isDef = false;
                            WriteMessage(" - Настройки Подписчика вернулись из базы корректно");
                        }
                     */

                    ReplicationSettings settings = new ReplicationSettings();//get settings from config file

                    CryptoManager crypt = new CryptoManager(CryptoManager.SymmProvEnum.Rijndael);

                    if (settings.PublisherSecurityMode == SecurityMode.Standard)
                    {
                        settings.PublisherPassword = crypt.Decrypting(settings.PublisherPassword, key); ;
                        settings.DistributorPassword = crypt.Decrypting(settings.DistributorPassword, key);

                        if (null == settings.PublisherPassword)
                        {
                            WriteMessage(" - Не указан пароль Publisher", System.Diagnostics.EventLogEntryType.Error);
                            return SyncReturnType.Failed;
                        }

                        if (null == settings.DistributorPassword)
                        {
                            WriteMessage(" - Не указан пароль Distributor", System.Diagnostics.EventLogEntryType.Error);
                            return SyncReturnType.Failed;
                        }
                    }

                    settings.InternetPassword = crypt.Decrypting(settings.InternetPassword, key);

                    if (null == settings.InternetPassword)
                    {
                        WriteMessage(" - Не указан пароль Internet Password", System.Diagnostics.EventLogEntryType.Error);
                        return SyncReturnType.Failed;
                    }

                    // Define the pull subscription.
                    MergePullSubscription subscription = new MergePullSubscription();
                    MergeSynchronizationAgent agent;

                    subscription.DatabaseName = subscriberDbName;
                    subscription.ConnectionContext = conn;
                    subscription.PublisherName = settings.PublisherName;
                    subscription.PublicationDBName = settings.PublicationDB;
                    subscription.PublicationName = settings.PublicationName;
                    subscription.DistributorName = settings.DistributorName;
                    subscription.InternetTimeout = settings.InternetTimeout;

            if (!subscription.IsExistingObject)
            {//Create subscription
                try
                {
                    if (!guiMode)
                        throw null;

                    WriteMessage(" - Не найден подписчик с заданными параметрами: \r\n" + ConStr(settings, subscriberDbName, conn), System.Diagnostics.EventLogEntryType.Warning);
                    subscription.Create();
                    WriteMessage(" - Новый подписчик создан успешно");
                }
                catch(Exception ex)
                {
                    WriteMessage(" - Ошибка создания подписчика", ex, EventLogEntryType.Error);
                }
            }
            else
            {
                WriteMessage(" - Подписчик настроен");
            }
            //Load properties from subscriber
            if (subscription.LoadProperties())
            {
                WriteMessage(" - Связь с Подписчиком установлена");
            }
            else
            {
                WriteMessage(" - Невозможно установить связь с существующим или создать новый Подписчик. Возможно неверные параметры \r\n"
                     + ConStr(settings, subscriberDbName, conn) + "(выполнение прервано)", System.Diagnostics.EventLogEntryType.Error);
                conn.Disconnect();
                return SyncReturnType.Failed;
            }

            //Set host name
            if (subscription.HostName != conn.WorkstationId)
                subscription.HostName = conn.WorkstationId;

            //Run reinitialization
            if (isReInit)
            {
                try
                {
                    WriteMessage(" - Запуск повторной инициализации подписки");
                    subscription.Reinitialize(false);
                    WriteMessage(" - Повторная инициализация подписки выполнена");
                }
                catch (Exception ex)
                {
                    WriteMessage(" - Невозможно выполнить повторную инициализацию подписки", ex, System.Diagnostics.EventLogEntryType.Error);
                    return SyncReturnType.Failed;
                }
            }

            // Get the agent for the subscription.
            try
            {
                agent = subscription.SynchronizationAgent;
            }
            catch (Exception ex)
            {
                WriteMessage(" - Невозможно загрузить агента из настроек Подписчика (выполнение прервано)", ex, System.Diagnostics.EventLogEntryType.Error);
                return SyncReturnType.Failed;
            }

            // set publisher creditionals
            try
            {
                agent.InternetTimeout = settings.InternetTimeout;
                agent.QueryTimeout = settings.InternetTimeout;
            }
            catch (Exception)
            {
                WriteMessage(" - Не могу установить InternetTimeout и QueryTimeout");
            }

            /*
            if (false)//(!isDef)
            {
                agent.PublisherSecurityMode = settings.PublisherSecurityMode;
                if (agent.PublisherSecurityMode == SecurityMode.Standard)
                    agent.PublisherLogin = settings.PublisherLogin;

                // set distributor creditionals
                agent.DistributorSecurityMode = settings.DistibutorSecurityMode;
                if (agent.DistributorSecurityMode == SecurityMode.Standard)
                    agent.DistributorLogin = settings.DistributorLogin;

                // set WebSync params
                agent.InternetUrl = settings.InternetURL;
                agent.InternetLogin = settings.InternetLogin;
                agent.InternetTimeout = settings.InternetTimeout;
                // Enable agent output to the console.
                agent.OutputVerboseLevel = 1;
                agent.Output = "";
            }

             */

            //Rewrite password in any case from DB or from default settings

            if (agent.PublisherSecurityMode == SecurityMode.Standard)
            {
                agent.PublisherPassword = settings.PublisherPassword;
                agent.DistributorPassword = settings.DistributorPassword;
            }
            agent.InternetPassword = settings.InternetPassword;

            AutoAcceptCertificate(subscription.InternetUrl, subscription.InternetLogin, agent.InternetPassword);

            WriteMessage(" - Агент настроен");
            //Проверяем, будет ли применяться новый снепшот
            //if (agent.IsSnapshotRequired()) //увы, не работает
            //{
            //    WriteMessage(" - Будет применяться новый снепшот");
            //    //!
            //    //DownloadSnapshotFile(subscription.HostName);

            //}

            //WriteMessage(String.Format("subscription.InternetTimeout {0}", subscription.InternetTimeout));
            //WriteMessage(String.Format("agent.InternetTimeout {0}", agent.InternetTimeout));
            //WriteMessage(String.Format("agent.QueryTimeout {0}", agent.QueryTimeout));
            
            // Synchronously start the Merge Agent for the subscription.
            try
            {
                agent.Status += new AgentCore.StatusEventHandler(agent_ComStatus);
                WriteMessage(" - Синхронизация началась. ");
                agent.Synchronize();
                WriteMessage(" - Синхронизация закончилась успешно!", EventLogEntryType.Warning);

            }
            catch (Exception ex)
            {
                WriteMessage(" - Синхронизация завершилась с ошибками!", ex, EventLogEntryType.Error);
                conn.Disconnect();
                return SyncReturnType.Failed;
            }
        }
        catch (Exception ex)
        {
            WriteMessage(" - Внутренняя ошибка", ex, EventLogEntryType.Error);
            return SyncReturnType.Failed;
        }
            return res;
        }

        /// <summary>
        /// Run whole sync process with pre and post event
        /// Please don't do unprotected GUI access in this method, it is called from asyncronous thread
        /// </summary>
        /// <returns></returns>
        public bool StartProcess(bool isReInit)
        {
            CreateLogFile();
            WriteMessage(string.Format("Репликация запущена {0} в {1:u} на {2}", Environment.UserName, DateTime.Now, Environment.MachineName));

            if (string.IsNullOrEmpty(connectionString))
                connectionString = ConfigManager.ConnectionString.ConnectionString;

            ExecutePreScript();

            bool result = false;

            SyncReturnType res = SyncEngine(isReInit);

            if (res == SyncReturnType.Failed && (!guiMode) && isFirstStart)
            {
                isFirstStart = false;
                WriteMessage(" - Trying to re-run on first failure", EventLogEntryType.Warning);
                System.Threading.Thread.Sleep(5 * 60 * 1000);//wait for 5 minutes
                res = SyncEngine(isReInit);
            }

            if (res == SyncReturnType.NeedRepeat)
            {
                WriteMessage(" - Repeating", EventLogEntryType.Warning);
                res = SyncEngine(false);
            }

            result = (res == SyncReturnType.OK);

            if (result)
            {
                SetLastSuccessSyncTime();
                LDBVersion = GetLDBVersion();
                ExecutePostScript();
            }

            SendStatusToWS(result);
            CloseLogFile(res);

            return result;
        }

        private string GetFileContent(string key)
        {
            string result = string.Empty;
            string fileName = ConfigManager.GetConfigValue(key);
            if (!String.IsNullOrEmpty(fileName))
            {
                string currentDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string fullPath = string.Format("{0}\\{1}", currentDir, fileName);
                if (File.Exists(fullPath))
                {
                    try
                    {
                        FileStream ds = File.OpenRead(fullPath);
                        byte[] content = new byte[ds.Length];
                        ds.Read(content, 0, (int)ds.Length);
                        ds.Close();
                        Encoding enc = Encoding.Unicode;
                        result = enc.GetString(content);
                    }
                    catch { }
                }
            }
            return result;
        }

        private void ExecScript(string scriptName, string script)
        {
            if (!String.IsNullOrEmpty(script))
            {
                try
                {
                    WriteMessage(string.Format("Выполняю скрипт {0}", scriptName));

                    Server server = new Server(new ServerConnection(ConfigManager.ConnectionString.DataSource));
                    server.ConnectionContext.StatementTimeout = 600;
                    server.ConnectionContext.ExecuteNonQuery(script);

                    //WriteMessage("Успешное исполнение скрипта", EventLogEntryType.Information);
                }
                catch (Exception e)
                {
                    WriteMessage("Ошибка исполнения скрипта", e, EventLogEntryType.Error);
                }
            }
        }

        private void ExecutePostScript()
        {
            string script = GetFileContent(ConfigManager.POST_REPLICATION_SCRIPT);
            if (!String.IsNullOrEmpty(script))
                ExecScript(ConfigManager.POST_REPLICATION_SCRIPT, script);
        }

        private void ExecutePreScript()
        {
            string script = GetFileContent(ConfigManager.PRE_REPLICATION_SCRIPT);
            if (!String.IsNullOrEmpty(script))
                ExecScript(ConfigManager.PRE_REPLICATION_SCRIPT, script);
        }

        /// <summary>
        /// Get message from replication agent about prosess workflow
        /// </summary>
        void agent_ComStatus(object sender, StatusEventArgs e)
        {
            switch (e.MessageStatus)
            {
                case MessageStatus.Retry:
                    WriteMessage(" - " + e.Message, EventLogEntryType.Warning);
                    break;
                case MessageStatus.Fail:
                    WriteMessage(" - " + e.Message, EventLogEntryType.Error);
                    break;
                case MessageStatus.InProgress:
                    WriteMessage(e.PercentCompleted, " - " + e.Message);
                    break;
                default:
                    WriteMessage(e.PercentCompleted, " - " + e.Message);
                    break;
            }                
        }
        #endregion

        #region Messaging

        public void WriteMessage(string message)
        {
            WriteMessage(message, EventLogEntryType.Information);
        }

        public void WriteMessage(string message, EventLogEntryType type)
        {
            WriteMessage(message, null, type);
        }

        public void WriteMessage(int PercentCompleted, string message)
        {
            WriteMessage(PercentCompleted, message, null, EventLogEntryType.Information);
        }

        public void WriteMessage(string message, Exception ex, EventLogEntryType type)
        {
            WriteMessage(0, message, ex, type);
        }

        public void WriteMessage(int PercentCompleted, string message, Exception ex, EventLogEntryType type)
        {

            if (null != ex)
            {
                StringBuilder sb = new StringBuilder(); //message text
                sb.AppendLine(message);
                sb.AppendLine(ex.Message);

                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    sb.AppendLine(ex.Message);
                }
                message = sb.ToString();
            }

            LogManager.WriteMessage(message, type); //event log
            WriteLogToFile(String.Format("{0:yyyy'-'MM'-'dd HH':'mm':'ss} {1}\r\n", DateTime.Now, message)); //file

            if (!guiMode) //если не в гуи моде - мы сделали все что надо, иначе надо обновить ГУИ
                return;

            if (guiMode && BGW.IsBusy) //если мы в GUI режиме и параллельная задача работает - вызываем ReportProgress, чтобы правильно обратиться к элементам GUI
                BGW.ReportProgress(PercentCompleted, message);
            else //иначе вызываем событие напрямую
                BGW_ProgressChanged(this, new ProgressChangedEventArgs(PercentCompleted, message));
        }

        private void WriteLogToFile(string message)
        {
            if (null != logFile)
            {
                try
                {
                    byte[] msg = Encoding.UTF8.GetBytes(message);
                    logFile.Write(msg, 0, msg.Length);
                    logFile.Flush();
                    FlushFileBuffers(logFile.SafeFileHandle.DangerousGetHandle()); //workaround for bug in FileStream.Flush
                }
                catch { }
            }
        }

        static string UploadFile(string ftpURL, string userName, string password, string filePath)
        {
            string result = "Error";
            try
            {
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(ftpURL + "/" + Path.GetFileName(filePath));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(userName, password);
                request.UsePassive = false;
                request.UseBinary = true;
                request.KeepAlive = false;

#if _LOCAL_
                WebProxy webProxy = new WebProxy("proxy.softservecom.com", 8080);
                webProxy.Credentials = CredentialCache.DefaultNetworkCredentials;
                request.Proxy = webProxy;
#endif
                FileStream stream = File.OpenRead(filePath);
                byte[] buffer = new byte[stream.Length];

                stream.Read(buffer, 0, buffer.Length);
                stream.Close();

                Stream reqStream = request.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                result = response.StatusDescription;
                response.Close();
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Create string for output
        /// </summary>
        private string ConStr(ReplicationSettings settings, string subscriberDbName, ServerConnection conn)
        {
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(conn.ConnectionString);

            return "subscriberDbName: " + subscriberDbName + " \r\n" +
                    "PublisherName: " + settings.PublisherName + " \r\n" +
                    "PublicationDBName: " + settings.PublicationDB + " \r\n" +
                    "PublicationName: " + settings.PublicationName + " \r\n" +
                    "DistributorName: " + settings.DistributorName + " \r\n" +
                    "ConnectionContext: " + GetConnInfoWithoutPassword(sb) + " \r\n";
        }
        #endregion

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32", SetLastError = true)]
        private static extern bool FlushFileBuffers(IntPtr handle);

        public void ScrollToBottom(System.Windows.Forms.TextBoxBase tb)
        {
            uint WM_VSCROLL = 277;
            uint SB_BOTTOM = 7;
            SendMessage(tb.Handle, WM_VSCROLL, new IntPtr(SB_BOTTOM), new IntPtr(0));
        }

        private void BGW_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (progressBar1.Value != e.ProgressPercentage)
            {
                progressBar1.Value = e.ProgressPercentage;
                progressBar1.Refresh();
            }

            if (!cbDetailed.Checked)
                rbTextBox.AppendText(". ");
            else
            {
                rbTextBox.AppendText(e.UserState.ToString() + "\r\n");
                ScrollToBottom(rbTextBox);
            }
            rbTextBox.Refresh();
        }

        private void BGW_DoWork(object sender, DoWorkEventArgs e)
        {
            bool isReInit = (bool)e.Argument;
            bool res = StartProcess(isReInit);
            
            e.Result = res;
        }

        private void BGW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null) //произошло неперехваченое исключение в теле BGW
            {
                WriteMessage(e.Error.Message);
            }
            else //no error
            {
                if (e.Cancelled)
                {
                    WriteMessage("Thread was canceled");
                }
                else
                {
                    OnSyncEnd((bool)e.Result);
                }
            }

            sync_lock.Release();
            //WriteMessage("Lock released");
        }

        //all actions which should be done at the end of sync, no matter how sync was runned should be placed there
        private void OnSyncEnd(bool SyncResult)
        {
            btnClose.Enabled = true;
            btnRun.Enabled = true;
            btnRunWithReInit.Enabled = true;
            cbDetailed.Enabled = true;
            progressBar1.Value = progressBar1.Maximum;

            try
            {
                if (SyncResult)
                {
                    LDBVersion = GetLDBVersion();
                    SetLastSuccessSyncTime();
                }
            }
            catch (Exception ex)
            {
                WriteMessage(String.Format("Не могу обратиться к LDB : {0}", ex.Message));
            }

            this.Text = headerText + string.Format(LDBVersionTemplate, LDBVersion, LastSuccessSyncTime);

            WriteMessage(string.Format("Sync Version: {0} - Result: {1}", LDBVersion, SyncResult ? "OK" : "Failed"));
        }

/*
        public ReplicationSettings GetReplicationSync(string ConnectionString)
        {
            ReplicationSettings rs = null;

            try
            {
                using (SqlConnection cnn = new SqlConnection(ConnectionString))
                {
                    cnn.Open();
                    using (SqlCommand cmd = new SqlCommand("spDW_Repl_SyncGet", cnn))
                    {
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {
                            while (rd.Read())
                            {
                                rs = new ReplicationSettings();
                                rs.ReplSyncID = Convert.ToInt32(rd["Repl_Sync_ID"]);
                                rs.LastUpdatedTime = rd["StartDate"].ToString();
                                rs.PublicationDB = rd["PublicationDB"].ToString();
                                rs.PublicationName = rd["PublicationName"].ToString();
                                rs.PublisherName = rd["PublisherName"].ToString();
                                rs.PublisherSecurityMode = (SecurityMode)Enum.Parse(typeof(SecurityMode), rd["PublisherSecurityMode"].ToString());
                                rs.PublisherLogin = rd["PublisherLogin"] == null ? "" : rd["PublisherLogin"].ToString();
                                rs.PublisherPassword = rd["PublisherPassword"] == null ? "" : rd["PublisherPassword"].ToString();
                                rs.DistributorName = rd["DistributorName"].ToString();
                                rs.DistibutorSecurityMode = (SecurityMode)Enum.Parse(typeof(SecurityMode), rd["DistibutorSecurityMode"].ToString());
                                rs.DistributorLogin = rd["DistributorLogin"] == null ? "" : rd["DistributorLogin"].ToString();
                                rs.DistributorPassword = rd["DistributorPassword"] == null ? "" : rd["DistributorPassword"].ToString();
                                rs.InternetURL = rd["InternetURL"] == null ? "" : rd["InternetURL"].ToString();
                                string tmp = ConfigManager.GetConfigValue(ConfigManager.INTERNET_SYNC_URL);

                                if (!String.IsNullOrEmpty(tmp))
                                    rs.InternetURL = tmp;

                                rs.InternetLogin = rd["InternetLogin"] == null ? "" : rd["InternetLogin"].ToString();
                                rs.InternetPassword = rd["InternetPassword"] == null ? "" : rd["InternetPassword"].ToString();
                                rs.Rowguid = (Guid)rd["rowguid"];
                            }
                        }
                    }
                    cnn.Close();
                }
            }
            catch (Exception ex)
            {
                WriteMessage(string.Format("Exception {0}", ex.Message));
                return null;
            }
            return rs;
        }
*/
        private void rbTextBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            if (tabControl.SelectedIndex == 1)
            {
                ServerConnection conn = new ServerConnection(@"sv109\sqlexpress");
                conn.Connect();
                MergePullSubscription subs = new MergePullSubscription("LDB", "vm64437sql", "SalesWorks", "DW", conn);
                subs.LoadProperties();
                MergeSynchronizationAgent agent = subs.SynchronizationAgent;

                ReplicationSettings settings = GetReplSettingsFromAgent(agent);
                agentPropertyGrid.SelectedObject = settings;

            }
             */
        }

        private ReplicationSettings GetReplSettingsFromAgent(MergeSynchronizationAgent agent)
        {
            return new ReplicationSettings()
            {
                AltSnapshotFolder = agent.AltSnapshotFolder,
                DistibutorSecurityMode = agent.DistributorSecurityMode,
                DistributorLogin = agent.DistributorLogin,
                DistributorName = agent.Distributor,
                //DistributorPassword
                InternetLogin = agent.InternetLogin,
                //InternetPassword
                InternetURL = agent.InternetUrl,
                LastUpdatedTime = agent.LastUpdatedTime,
                OutputVerboseLevel = agent.OutputVerboseLevel,
                PublicationDB = agent.PublisherDatabase,
                PublicationName = agent.Publication,
                PublisherLogin = agent.PublisherLogin,
                PublisherName = agent.Publisher,
                //PublisherPassword
                PublisherSecurityMode = agent.PublisherSecurityMode
                //UploadGenerationsPerBatch = agent.UploadGenerationsPerBatch; //only in sql server 2008
            };
        }

        private void StartReplMerg(bool isReInit)
        {
            if (isReInit)
            {
                try
                {
                    WriteMessage("Помечаю подписку для реинициализации");
                    ReinitMergeSubscription();
                    WriteMessage("ОК");
                }
                catch (Exception ex)
                {
                    WriteMessage(String.Format("Не могу реинициализировать подписку : {0}", ex.Message));
                    return;
                }
            }

            string uri = ConfigurationManager.AppSettings["InternetUrl"];
            
            AutoAcceptCertificate(uri, "", "");

            //некрасивая логика для поиска пути к replmerg.exe
            //путь к утилитам можно взять из реестра HKEY_LOCAL_MACHINE\SOFTWARE\Microsft\Microsoft SQL Server(version number)\VerSpecificRootDir
            //версию можно запросить у сервера
            //но можеть быть установлено несколько клиентов, в некоторых не будет компонент репликации, все равно придется искать
            string[] ProgramFiles = {Environment.GetEnvironmentVariable("ProgramFiles"), Environment.GetEnvironmentVariable("ProgramFiles(x86)")};
            string[] versions = {"100", "90"};

            string ReplMergPath = null;
            bool found = false;

            foreach (string location in ProgramFiles)
            {
                foreach (string version in versions)
                {
                    ReplMergPath = String.Format(@"{0}\Microsoft SQL Server\{1}\COM\replmerg.exe", location, version);
                    if (File.Exists(ReplMergPath))
                    {
                        found = true;
                        break;
                    }
                }

                if (found)
                    break;
            }

            if (!found)
            {
                WriteMessage("Не могу найти replmerg.exe");
                return;
            }

            int DownloadGenerationsPerBatch = 10;
            if (!int.TryParse(tbGenPerBatch.Text, out DownloadGenerationsPerBatch))
            {
                MessageBox.Show("Не могу распознать число в поле Generations per batch");
                return;
            }

            // Redirect the output stream of the child process.
            CmdProcess.StartInfo.CreateNoWindow = true;
            CmdProcess.StartInfo.UseShellExecute = false; //should be false to redirection
            CmdProcess.StartInfo.RedirectStandardOutput = true;
            CmdProcess.OutputDataReceived += new DataReceivedEventHandler(CmdOutputHandler);
            CmdProcess.EnableRaisingEvents = true; //should be true for Exited
            CmdProcess.Exited += new EventHandler(CmdExitedEvent);

            CmdProcess.StartInfo.FileName = ReplMergPath;
            CmdProcess.StartInfo.Arguments = String.Format(
                 " -Publication DW -SubscriptionType 1 -Distributor {0} -DistributorSecurityMode 0 -DistributorLogin WebSync -DistributorPassword Cby[hjybpfwbz123 " +
                 " -Publisher {1} -PublisherDB SalesWorks -PublisherSecurityMode 0 -PublisherLogin WebSync -PublisherPassword Cby[hjybpfwbz123 -Subscriber {2} " +
                 " -SubscriberDB LDB -SubscriberSecurityMode 1 -InternetLogin DB4_Sync_WebUser -InternetPassword Gjkyfz_pfkj4rf -InternetTimeout 600 -HistoryVerboseLevel 1 " +
                 " -OutputVerboseLevel 1 -DownloadGenerationsPerBatch {3} ",
                 ConfigManager.GetConfigValue("DistributorName"),
                 ConfigManager.GetConfigValue("PublisherName"),
                 Environment.MachineName,
                 DownloadGenerationsPerBatch
                 );

            btnClose.Enabled = false;
            btnRun.Enabled = false;
            btnRunWithReInit.Enabled = false;
            cbDetailed.Enabled = false;

            CmdProcess.Start();
            CmdProcess.BeginOutputReadLine();

            //string output = p.StandardOutput.ReadToEnd();
            //p.WaitForExit();

            tabControl.SelectedIndex = 0;
        }

        //called when background process running replmerg.exe exits
        private void CmdExitedEvent(object sender, EventArgs e)
        {
            MethodInvoker ExitActions = new MethodInvoker(() =>
            {
                bool result = CmdProcess.ExitCode == 0;
                OnSyncEnd(result);
                SendStatusToWS(result);

                //! to send sync status
            });

            if (InvokeRequired)
                Invoke(ExitActions);
            else
                ExitActions();

            CmdProcess.OutputDataReceived -= CmdOutputHandler;
            CmdProcess.Exited -= CmdExitedEvent;
        }

        private void CmdOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                MethodInvoker updateText = new MethodInvoker(() =>
               {
                   WriteMessage(outLine.Data);
               });

                if (InvokeRequired)
                    Invoke(updateText);
                else
                    updateText();

            }
        }

    }
}
