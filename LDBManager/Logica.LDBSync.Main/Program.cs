﻿//#define _DEBUG
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Security.AccessControl;
using System.Security.Principal;
using Logica.LDBSync.Main.UpdaterServiceReference;

namespace Logica.LDBSync.Main
{
    /// <summary>
    /// Store information about module
    /// ModuleName = file name
    /// ModulePath = full name of the file, include path
    /// </summary>
    struct ModuleInfo
    {
        public string ModuleName;
        public string ModulePath;
    }

    class Program
    {
        internal static bool guiMode = true;
        internal static bool updateOnly = false;
        internal static bool skipUpdate = false;
        internal static string currentDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        internal static UpdateServiceSoapClient service = new UpdateServiceSoapClient();

        [STAThreadAttribute]
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(currentDomain_AssemblyResolve);
            //*
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(
                (sender, e) =>
                {
                    if (e.IsTerminating)
                    {
                        object o = e.ExceptionObject;
                        string message = "Unknown error";
                        if (o != null)
                            message = o.ToString();
                        LogManager.WriteMessage("AssemblyResolve error");
                        LogManager.WriteMessage(message, EventLogEntryType.Error);
                    }
                }
              );
            //*/
#if _DEBUG
            foreach (string arg in args)
                System.Windows.Forms.MessageBox.Show(arg);
            
#endif
            //parse command line
            foreach (string s in args)
            {
                switch (s.ToUpper())
                {
                    case "-SCHD": //scheduled run or silent mode, programm doesn't show dialog window
                        guiMode = false;
                        break;
                    case "-CHKUPDT": //skip update
                        skipUpdate = true;
                        break;
                    case "-UPDATEONLY": // update LDBManagerUpdateLauncher.exe and exit
                        updateOnly = true;
                        break;
                }
            }

            if (skipUpdate && updateOnly)
                updateOnly = false;

            //check if one instance is already running
            InterProcessLock lck = new InterProcessLock("Global\\LDBSyncApp", TimeSpan.Zero);
            lck.Acquire();

            if (skipUpdate)
                LogManager.WriteMessage("Update has been disabled");

            if (lck.IsAcquired && !skipUpdate) //it is first instance and update hasn't been disabled, let's try to update
            {
                
                try //try to update updater
                {
                    List<ModuleInfo> updaterList = CheckUpdaterUpdates();
                    UpdateLauncher(updaterList); //if launcher was not updated, we continue execution
                }
                catch (Exception ex)
                {
                    LogManager.WriteMessage(String.Format("Can't update updater: {0}", ex.Message));
                }

                if (updateOnly) //update updater and exit
                {
                    lck.Release();
                    return;
                }

                try //check if we have any app updates
                {
                    List<ModuleInfo> appList = CheckApplicationUpdates();

                    if (appList.Count > 0) //if there is some changes
                    {
                        if (RunLauncher(args)) //start updater and exit
                        {
                            lck.Release();
                            return;
                        }
                        else
                            LogManager.WriteMessage("Can't start updater");
                    }
                    else
                        LogManager.WriteMessage("Application is up to date");
                }
                catch (Exception ex)
                {
                    LogManager.WriteMessage(String.Format("Can't check application updates: {0}", ex.Message));
                }

            }

            if (!lck.IsAcquired)
                LogManager.WriteMessage("LDBSync запущен, обновление не выполнится", EventLogEntryType.Warning);

            //check if application executed in the same session
            InterProcessLock local_lck = new InterProcessLock("Local\\MyLDBSyncApp", TimeSpan.Zero);
            local_lck.Acquire();
            
            if (!local_lck.IsAcquired)
            {
                LogManager.WriteMessage("LDBSync уже запущен в этой сессии", EventLogEntryType.Warning);
                if (guiMode)
                    System.Windows.Forms.MessageBox.Show("LDBSync уже запущен в этой сессии");
                return;
            }
            
            Report rep = new Report(guiMode);

            if (guiMode)
                rep.ShowDialog();
            else
            {//start process without opening window
                rep.StartProcess(false);//isReInit == false
                rep.Close();
                rep.Dispose();
            }

            lck.Release();
            local_lck.Release();
        }

        static System.Reflection.Assembly currentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {

            string asmName = args.Name.Substring(0, args.Name.IndexOf(','));
            LogManager.WriteMessage(String.Format("{0} - try to load", args.Name), EventLogEntryType.Information); 

            System.Reflection.Assembly asm = System.Reflection.Assembly.Load(asmName);

            LogManager.WriteMessage(String.Format("{0} - loaded", asm.FullName), EventLogEntryType.Information); 

            return asm;
        }


        private static List<ModuleInfo> CheckUpdaterUpdates()
        {
            FileListResponse response = service.GetUpdaterFileList();
            if (response.ErrorCode != 0)
                {
                    LogManager.WriteMessage(String.Format("Can't get file list from update service: \n{0}", response.ErrorMessage), EventLogEntryType.Error);
                    throw new ApplicationException(response.ErrorMessage);
                }

            return CheckUpdates(response.FileList);
        }

        private static List<ModuleInfo> CheckApplicationUpdates()
        {
            FileListResponse response = service.GetApplicationFileList();
            if (response.ErrorCode != 0)
            {
                LogManager.WriteMessage(String.Format("Can't get file list from update service: \n{0}", response.ErrorMessage), EventLogEntryType.Error);
                throw new ApplicationException(response.ErrorMessage);
            }

            return CheckUpdates(response.FileList);
        }

        public static int GetArrayHashCode(byte[] array)
        {
            unchecked
            {
                var result = 0;
                foreach (byte b in array)
                    result = (result * 31) ^ b;
                return result;
            }
        }

        private static List<ModuleInfo> CheckUpdates(UpdateFileInfo[] files)
        {
            string local_version;
            string ext;
            string filePath;

            List<ModuleInfo> modulesToUpdate = new List<ModuleInfo>(3);

            foreach (UpdateFileInfo file in files)
            {
                filePath = Path.Combine(currentDir, file.FileName);
                ext = Path.GetExtension(filePath).ToUpper();
                try
                {
                    if (ext == ".EXE" || ext == ".DLL") //executable file
                        local_version = FileVersionInfo.GetVersionInfo(filePath).FileVersion;
                    else //not executable file
                        local_version = GetArrayHashCode(File.ReadAllBytes(filePath)).ToString();
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Can't get file {0} version : {1} ", file.FileName, ex.Message));
                }

                if (local_version != file.FileVersion)
                    modulesToUpdate.Add(new ModuleInfo() { ModuleName = file.FileName, ModulePath = filePath });
            }

            return modulesToUpdate;

        }

        private static bool UpdateLauncher(List<ModuleInfo> modules)
        {
            if (modules == null)
                return true;

            if (modules.Count == 0)
            {
                LogManager.WriteMessage("Updater is up to date");
                return true;
            }

            bool downloadOK = true;

            foreach (ModuleInfo module in modules)
            {
                var response = service.GetModule(new GetModuleRequest { ModuleName = module.ModuleName });
                if (response.ErrorCode != 0)
                {
                    downloadOK = false;
                    LogManager.WriteMessage(String.Format("Can't update launcher file ({0}): {1}", module.ModuleName, response.ErrorMessage), EventLogEntryType.Warning);
                    break;
                }

                using (MemoryStream responseStream = new MemoryStream(response.ModuleContent))
                {
                    SaveRecievedStream(responseStream, module.ModulePath);
                }

            }

            return downloadOK;
        }

        private static bool RunLauncher(string[] args)
        {
            bool result = true;
            string launcherFilePath = Path.Combine(currentDir, "LDBManagerUpdateLauncher.exe");

            try
            {
                ProcessStartInfo psi = new ProcessStartInfo(launcherFilePath);
                foreach (string arg in args)
                    psi.Arguments += string.Format("{0} ", arg);

                Process process = Process.Start(psi);
            }
            catch (Exception e)
            {
                LogManager.WriteMessage(String.Format("Can't start launcher: \n{0}", e.Message), EventLogEntryType.Error);
                result = false;
            }
            return result;
        }


        public static bool DownloadSnapshotFile(string HostName)
        {
            //cформируем адрес для загрузки снепшота
            string URL = ConfigManager.GetConfigValue("InternetUrl");
            if (URL.Length < 13)
                return false;
            URL = URL.Substring(0, URL.Length - 13); //replisapi.dll
            URL = URL + "snapshot.cab?s=" + HostName;

            //определим, куда будем сохранять

            return true;
        }

        private static bool DownloadFile(string ftpURL, string userName, string password, string filePath)
        {
            bool result = true;
            string fullFileName = filePath.Trim(new char[] { '\"' });
            string fileName = Path.GetFileName(fullFileName);

            try
            {
                if (ConfigManager.UpdateThruWS)
                {
                    var response = service.GetModule(new GetModuleRequest { ModuleName = fileName });
                    if (response.ErrorCode != 0)
                    {
                        throw new ApplicationException(response.ErrorMessage);
                    }
                    using (MemoryStream responseStream = new MemoryStream(response.ModuleContent))
                    {
                        SaveRecievedStream(responseStream, fullFileName);
                    }
                }
                else
                {
                    var request = WebRequest.Create(ftpURL + "/" + fileName);
                    request.Credentials = new NetworkCredential(userName, password);
                    if (request is FtpWebRequest)
                    {
                        ((FtpWebRequest) request).UsePassive = false;
                        ((FtpWebRequest) request).UseBinary = true;
                        ((FtpWebRequest) request).KeepAlive = false;
                        request.Method = WebRequestMethods.Ftp.DownloadFile;
                    }
                    else if (request is HttpWebRequest)
                    {
                        request.Method = WebRequestMethods.Http.Get;
                    }
                    var response = request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    SaveRecievedStream(responseStream, fullFileName);
                }
            }
            catch (Exception e)
            {
                LogManager.WriteMessage("Error at time of file downloading: ", e, EventLogEntryType.Error);
                result = false;
            }

            return result;
        }

        private static void SaveRecievedStream(Stream responseStream, string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);

            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
            byte[] buffer = new byte[2047];
            int read = responseStream.Read(buffer, 0, buffer.Length);
            while (read > 0)
            {
                fs.Write(buffer, 0, read);
                read = responseStream.Read(buffer, 0, buffer.Length);
            }
            responseStream.Close();
            fs.Flush();
            fs.Close();
        }
    }
}
