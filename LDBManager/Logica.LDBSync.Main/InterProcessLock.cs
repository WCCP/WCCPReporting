﻿using System;
using System.Text;
using System.Threading;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Logica.LDBSync.Main
{
    public class InterProcessLock : IDisposable
    {
        private Mutex mutex = null;
        public bool IsAcquired { get; private set; }
        private string name;
        private TimeSpan timeout;

        public InterProcessLock(string name, TimeSpan timeout)
        {
            IsAcquired = false;
            this.name = name;
            this.timeout = timeout;
        }

        public bool Acquire()
        {
            if (IsAcquired)
                return true;

            if (mutex == null)
            {
                var security = new MutexSecurity();
                security.AddAccessRule(new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow));

                bool created;
                mutex = new Mutex(false, name, out created, security);
            }

            try
            {
                IsAcquired = mutex.WaitOne(timeout);
            }
            catch (AbandonedMutexException)
            {
                // Log the fact the mutex was abandoned in another process, it will still get aquired
                IsAcquired = true;
            }

            return IsAcquired;
        }

        public void Release()
        {
            if (IsAcquired)
            {
                mutex.ReleaseMutex();
                IsAcquired = false;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (IsAcquired)
            {
                mutex.ReleaseMutex();
                IsAcquired = false;
            }
        }

        #endregion
    }
}
