﻿using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using Logica.LDBSync.Main;

namespace Logica.Password.Generator
{
    /// <summary>
    /// SymmCrypto is a wrapper of System.Security.Cryptography.SymmetricAlgorithm classes
    /// and simplifies the interface. It supports customized SymmetricAlgorithm as well.
    /// </summary>
    public class CryptoManager
    {
        /// <summary>
        /// Type of decrypting
        /// </summary>
        public enum SymmProvEnum : int
        {
            DES, RC2, Rijndael
        }

        private SymmetricAlgorithm mobjCryptoService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="NetSelected">Type of decrypting</param>
        public CryptoManager(SymmProvEnum NetSelected)
        {
            switch (NetSelected)
            {
                case SymmProvEnum.DES:
                    mobjCryptoService = new DESCryptoServiceProvider();
                    break;
                case SymmProvEnum.RC2:
                    mobjCryptoService = new RC2CryptoServiceProvider();
                    break;
                case SymmProvEnum.Rijndael:
                    mobjCryptoService = new RijndaelManaged();
                    break;
            }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ServiceProvider">Custom algorithm</param>
        public CryptoManager(SymmetricAlgorithm ServiceProvider)
        {
            mobjCryptoService = ServiceProvider;
        }
        /// <summary>
        /// Process key for decrypting
        /// </summary>
        /// <param name="Key">key</param>
        /// <returns>processed value</returns>
        private byte[] GetLegalKey(string Key)
        {
            string sTemp;
            if (mobjCryptoService.LegalKeySizes.Length > 0)
            {
                int lessSize = 0, moreSize = mobjCryptoService.LegalKeySizes[0].MinSize;
                while (Key.Length * 8 > moreSize)
                {
                    lessSize = moreSize;
                    moreSize += mobjCryptoService.LegalKeySizes[0].SkipSize;
                }
                sTemp = Key.PadRight(moreSize / 8, ' ');
            }
            else
                sTemp = Key;
            return ASCIIEncoding.ASCII.GetBytes(sTemp);
        }
        /// <summary>
        /// Encript string 
        /// </summary>
        /// <param name="Source">String to convert</param>
        /// <param name="Key">Key</param>
        /// <returns>Converted value</returns>
        public string Encrypting(string Source, string Key)
        {
            try
            {
                byte[] bytIn = System.Text.ASCIIEncoding.ASCII.GetBytes(Source);

                System.IO.MemoryStream ms = new System.IO.MemoryStream();

                byte[] bytKey = GetLegalKey(Key);
                mobjCryptoService.Key = bytKey;
                mobjCryptoService.IV = bytKey;
                ICryptoTransform encrypto = mobjCryptoService.CreateEncryptor();
                CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Write);
                cs.Write(bytIn, 0, bytIn.Length);
                cs.FlushFinalBlock();

                byte[] bytOut = ms.GetBuffer();
                int i = 0;
                for (i = 0; i < bytOut.Length; i++)
                    if (bytOut[i] == 0)
                        break;
                return System.Convert.ToBase64String(bytOut, 0, i);
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// Decrypting string
        /// </summary>
        /// <param name="Source">String to decrypting</param>
        /// <param name="Key">Key</param>
        /// <returns>Decrypted value</returns>
        public string Decrypting(string Source, string Key)
        {
            try
            {
                byte[] bytIn = System.Convert.FromBase64String(Source);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(bytIn, 0, bytIn.Length);
                byte[] bytKey = GetLegalKey(Key);
                mobjCryptoService.Key = bytKey;
                mobjCryptoService.IV = bytKey;

                ICryptoTransform encrypto = mobjCryptoService.CreateDecryptor();
                CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Read);
                System.IO.StreamReader sr = new System.IO.StreamReader(cs);
                return sr.ReadToEnd();
            }
            catch (Exception)
            {                
                return null;
            }
        }
    }
}
