﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Logica.LDBSync.Main
{
    /// <summary>
    /// Show error message window
    /// </summary>
    public partial class ErrorForm : Form
    {
        /// <summary>
        /// Text of message
        /// </summary>
        public string Message
        {
            get { return rtbInfo.Text; }
            set { rtbInfo.Text = value; }
        }
        /// <summary>
        /// Details of message
        /// </summary>
        public string Details
        {
            get { return rtbDetails.Text; }
            set { rtbDetails.Text = value; }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ErrorForm()
        {
            InitializeComponent();
            this.Size = new Size(this.Size.Width,129);
        }
        /// <summary>
        /// Hide details
        /// </summary>
        private void btnDetails2_Click(object sender, EventArgs e)
        {
            this.Size = new Size(this.Size.Width, 129);
            btnDetails1.Visible = true;
            btnDetails2.Visible = false;
            rtbDetails.Visible = false;
        }
        /// <summary>
        /// Show details
        /// </summary>
        private void btnDetails1_Click(object sender, EventArgs e)
        {
            this.Size = new Size(this.Size.Width, 410);
            btnDetails2.Visible = true;
            rtbDetails.Visible = true;
            btnDetails1.Visible = false;
        }

    }
}
