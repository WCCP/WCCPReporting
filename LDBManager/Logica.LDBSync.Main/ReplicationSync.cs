﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SqlServer.Replication;
using System.Configuration;
using System.ComponentModel;

namespace Logica.LDBSync.Main
{
    /*
    CategoryAttribute  	This attribute places your property in the appropriate category in a node on the property grid.
    DescriptionAttribute 	This attribute places a description of your property at the bottom of the property grid
    BrowsableAttribute 	This is used to determine whether or not the property is shown or hidden in the property grid
    ReadOnlyAttribute 	Use this attribute to make your property read only inside the property grid
    DefaultValueAttribute 	Specifies the default value of the property shown in the property grid
    DefaultPropertyAttribute If placed above a property, this property gets the focus when the property grid is first launched. Unlike the other attributes, this attribute goes above the class. 
      */

    /// <summary>
    /// Params container, subset of MergeSynchronizationAgent properties
    /// </summary>
    public class ReplicationSettings
    {
        int replSyncID = -1;
        Guid rowguid;
        //Publisher
        string publicationDB;
        string publicationName;
        string publisherName;
        SecurityMode publisherSecurityMode = SecurityMode.Standard;
        string publisherLogin;
        string publisherPassword;
        //Distributor
        string distributorName;
        SecurityMode distibutorSecurityMode = SecurityMode.Standard;
        string distributorLogin;
        string distributorPassword;
        //Web syncronization
        string internetURL;
        string internetLogin;
        string internetPassword;
        int internetTimeout = -1;
        //Misc
        string altSnapshotFolder;
        string lastUpdatedTime;
        int outputVerboseLevel;
        short uploadGenerationsPerBatch;

        //Repl_Sync_ID
        [BrowsableAttribute(false)]
        public int ReplSyncID
        {
            get { return replSyncID; }
            set { replSyncID = value; }
        }

        //rowguid
        [BrowsableAttribute(false)]
        public Guid Rowguid
        {
            get { return rowguid; }
            set { rowguid = value; }
        }

        //PublicationDB
        [CategoryAttribute("Publication"), DescriptionAttribute("Name of the publication database")]
        public string PublicationDB
        {
            get 
            {
                if (String.IsNullOrEmpty(publicationDB))
                    publicationDB = ConfigurationManager.AppSettings["PublicationDB"];
                return publicationDB; 
            }
            set { publicationDB = value; }
        }

        //PublicationName
        [CategoryAttribute("Publication"), DescriptionAttribute("Name of the publication")]
        public string PublicationName
        {
            get 
            {
                if(String.IsNullOrEmpty(publicationName))
                    publicationName = ConfigurationManager.AppSettings["PublicationName"];
                return publicationName; 
            }
            set { publicationName = value; }
        }

        //PublisherName
        [CategoryAttribute("Publisher"), DescriptionAttribute("Name of the instance of Microsoft SQL Server that is the Publisher for the subscription")]
        public string PublisherName
        {
            get 
            {
                if(String.IsNullOrEmpty(publisherName))
                    publisherName = ConfigurationManager.AppSettings["PublisherName"];
                return publisherName; 
            }
            set { publisherName = value; }
        }

        //PublisherSecurityMode
        [CategoryAttribute("Publisher"), DescriptionAttribute("Gets or sets the security mode that is used when connecting to the Publisher.")]
        public SecurityMode PublisherSecurityMode
        {
            get { return publisherSecurityMode; }
            set { publisherSecurityMode = value; }
        }

        //PublisherLogin
        [CategoryAttribute("Publisher"), DescriptionAttribute("Gets or sets the login name that is used when connecting to the Publisher by using SQL Server Authentication.")]
        public string PublisherLogin
        {
            get { return publisherLogin; }
            set { publisherLogin = value; }
        }
        
        //PublisherPassword
        [CategoryAttribute("Publisher"), DescriptionAttribute("Sets the password that is used when connecting to the Publisher by using SQL Server Authentication.")]
        public string PublisherPassword
        {
            get
            {
                if (String.IsNullOrEmpty(publisherPassword))
                    publisherPassword = ConfigurationManager.AppSettings["PublisherPassword"];
                return publisherPassword;
            }
            set { publisherPassword = value; }
        }
        
        //DistributorName
        [CategoryAttribute("Distributor"), DescriptionAttribute("Gets or sets the name of the instance of Microsoft SQL Server that is acting as the Distributor for the subscription.")]
        public string DistributorName
        {
            get 
            {
                if(String.IsNullOrEmpty(distributorName))
                    distributorName = ConfigurationManager.AppSettings["DistributorName"];
                return distributorName; 
            }
            set { distributorName = value; }
        }
        
        //DistibutorSecurityMode
        [CategoryAttribute("Distributor"), DescriptionAttribute("Gets or sets the security mode used when connecting to the Distributor.")]
        public SecurityMode DistibutorSecurityMode
        {
            get { return distibutorSecurityMode; }
            set { distibutorSecurityMode = value; }
        }
        
        //DistributorLogin
        [CategoryAttribute("Distributor"), DescriptionAttribute("Gets or sets the security mode used when connecting to the Distributor.")]
        public string DistributorLogin
        {
            get { return distributorLogin; }
            set { distributorLogin = value; }
        }
        
        //DistributorPassword
        [CategoryAttribute("Distributor"), DescriptionAttribute("Sets the password that is used when connecting to the Distributor using SQL Server Authentication.")]
        public string DistributorPassword
        {
            get
            {
                if (String.IsNullOrEmpty(distributorPassword))
                    distributorPassword = ConfigurationManager.AppSettings["DistributorPassword"];
                return distributorPassword;
            }
            set { distributorPassword = value; }
        }
        
        //InternetURL
        [CategoryAttribute("Internet"), DescriptionAttribute("Gets or sets the URL of the Web service that is configured for Web synchronization.")]
        public string InternetURL
        {
            get { return internetURL; }
            set { internetURL = value; }
        }
        
        //InternetLogin
        [CategoryAttribute("Internet"), DescriptionAttribute("Gets or sets the login name that is used with Web synchronization when connecting to the Publisher by using Internet authentication.")]
        public string InternetLogin
        {
            get { return internetLogin; }
            set { internetLogin = value; }
        }
        
        //InternetPassword
        [CategoryAttribute("Internet"), DescriptionAttribute("Sets the password for the InternetLogin property that is used with Web synchronization when connecting to the Publisher by using Internet authentication.")]
        public string InternetPassword
        {
            get
            {
                if (String.IsNullOrEmpty(internetPassword))
                    internetPassword = ConfigurationManager.AppSettings["InternetPassword"];
                return internetPassword;
            }
            set { internetPassword = value; }
        }

        //InternetTimeout
        [CategoryAttribute("InternetTimeout"), DescriptionAttribute("InternetTimeout")]
        public int InternetTimeout
        {
            get
            {
                if (internetTimeout == -1)
                    internetTimeout = int.Parse(ConfigurationManager.AppSettings["InternetTimeout"]);//todo: add error handling here
                return internetTimeout;
            }
            set { internetTimeout = value; }
        }

        //AltSnapshotFolder
        [CategoryAttribute("Misc"), DescriptionAttribute("Gets or sets the alternate snapshot folder for the subscription.")]
        public string AltSnapshotFolder
        {
            get { return altSnapshotFolder; }
            set { altSnapshotFolder = value; }
        }

        //StartDate
        [CategoryAttribute("Misc"), ReadOnlyAttribute(true), DescriptionAttribute("Gets the timestamp of the last time that replication agent synchronized the subscription.")]
        public string LastUpdatedTime
        {
            get { return lastUpdatedTime; }
            set { lastUpdatedTime = value; }
        }

        //OutputVerboseLevel
        [CategoryAttribute("Misc"), DescriptionAttribute("Gets or sets the level of detail of information that is written to the agent output file.")]
        public int OutputVerboseLevel
        {
            get { return outputVerboseLevel; }
            set { outputVerboseLevel = value; }
        }

        //UploadGenerationsPerBatch
        [CategoryAttribute("Misc"), DescriptionAttribute("Gets or sets the number of generations to be processed in a single batch while uploading changes from the Subscriber to the Publisher. A generation is defined as a logical group of changes per article.")]
        public short UploadGenerationsPerBatch
        {
            get { return uploadGenerationsPerBatch; }
            set { uploadGenerationsPerBatch = value; }
        }


        /// <summary>
        /// Create string for output
        /// </summary>
        /*
        private string AggStr()
        {
            return "PublisherSecurityMode " + PublisherSecurityMode + " \r\n" +
                    "PublisherLogin " + PublisherLogin + " \r\n" +
                //"PublisherPassword " + PublisherPassword + " \r\n" +
                    "DistibutorSecurityMode " + DistibutorSecurityMode + " \r\n" +
                    "DistributorLogin " + DistributorLogin + " \r\n" +
                //"DistributorPassword " + DistributorPassword + " \r\n" +
                    "InternetURL " + InternetURL + " \r\n" +
                    "InternetLogin " + InternetLogin + " \r\n";
            //"InternetPassword " + InternetPassword + " \r\n";
        }
        */
    }
}
