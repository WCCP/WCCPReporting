﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
//using Logica.DB.LDBProcessor;

namespace Logica.LDBSync.Main
{
    /// <summary>
    /// Get data from config file
    /// </summary>
    public static class ConfigManager
    {
        public static string PRE_REPLICATION_SCRIPT = "PreReplicationScript";
        public static string POST_REPLICATION_SCRIPT = "PostReplicationScript";
        public static string INTERNET_SYNC_URL = "InternetUrl";
        static SqlConnectionStringBuilder sb;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        public static string GetConfigValue(string configKey)
        {
            string result = string.Empty;
            try
            {
                result = ConfigurationManager.AppSettings[configKey];
            }
            catch { }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public static bool NeedUploadToFTP
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["UploadToFTP"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string FTPURL
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPServer"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FTPUser
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPUser"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FTPPWD
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPPassword"];
            }
        }
        /// <summary>
        /// Indicate if programm should use WS to place log files for update
        /// </summary>
        public static bool NeedWSLogging
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["NeedWSLogging"]);
            }
        }
        /// <summary>
        /// Indicate if programm should use WS for updates
        /// </summary>
        public static bool UpdateThruWS
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["UpdateThruWS"]);
            }
        }

        /// <summary>
        /// Connection string to LDB 
        /// </summary>
        /// 
        public static SqlConnectionStringBuilder ConnectionString
        {
            get
            {
                try
                {
                    if (null == sb)
                    {
                        sb = new SqlConnectionStringBuilder();
                        sb.DataSource = ConfigurationManager.AppSettings["DataSource"];
                        sb.InitialCatalog = ConfigurationManager.AppSettings["InitialCatalog"];
                        sb.IntegratedSecurity = Boolean.Parse(ConfigurationManager.AppSettings["IntegratedSecurity"]);

                        if (sb.IntegratedSecurity)
                        {
                                using (SqlConnection conn = new SqlConnection(sb.ConnectionString))
                                {
                                    try
                                    {
                                        conn.Open();
                                    }
                                    catch
                                    {
                                        sb.IntegratedSecurity = false;
                                    }
                                    conn.Close();
                                }
                        }

                        if (!sb.IntegratedSecurity)
                        {
                            bool connectionInfoProcessed = false;
                            try
                            {
                                CreateConnection.ConnectionToSWDB_V35 connInfo_v35 = new CreateConnection.ConnectionToSWDB_V35();
                                sb.Password = connInfo_v35.UserPWD;
                                sb.UserID = connInfo_v35.UserID;
                                connectionInfoProcessed = true;
                            }
                            catch { }

                            if (!connectionInfoProcessed)
                            {
                                try
                                {
                                    CreateConnection.ConnectionToSWDB connInfo = new CreateConnection.ConnectionToSWDB();
                                    sb.Password = connInfo.UserPWD;
                                    sb.UserID = connInfo.UserID;
                                    connectionInfoProcessed = true;
                                }
                                catch { }
                            }

                            if (!connectionInfoProcessed)
                                sb.IntegratedSecurity = true;
                        }
                    }
                    return sb;
                }
                catch
                {
                    return null;
                }
            }
        }
        /*
                private string GetDecryptPassword(string encryptPassword)
                {
                    CCheckerClass key = new CCheckerClass();
                    key.Decrypt(ref encryptPassword, (uint)encryptPassword.Length);
                    return encryptPassword.Split('\0')[0];
                }
        */
        /// <summary>
        /// COnnection to LDB string
        /// </summary>
        public static SqlConnection LDBConnection
        {
            get
            {
                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
                sb.InitialCatalog = ConnectionString.InitialCatalog;
                sb.UserID = ConnectionString.UserID;
                sb.Password = ConnectionString.Password;

                return new SqlConnection(sb.ConnectionString);
                //return new ConnectionDescription(ConnectionString.DataSource, ConnectionString.InitialCatalog, ConnectionString.UserID, ConnectionString.Password);
            }
        }
        /// <summary>
        /// COnnection to SW DB string
        /// </summary>
        public static List<SqlConnection> SWConnections
        {
            get
            {
                List<SqlConnection> resList = new List<SqlConnection>();
                SqlConnectionStringBuilder sb;
                for (int i = 0; i < 100; i++)
                {
                    string str = ConfigurationManager.AppSettings["SWInitialCatalog" + i.ToString()];
                    if (!String.IsNullOrEmpty(str))
                    {
                        sb = new SqlConnectionStringBuilder();
                        sb.DataSource = ConnectionString.DataSource;
                        sb.InitialCatalog = str;
                        sb.UserID = ConnectionString.UserID;
                        sb.Password = ConnectionString.Password;
                        resList.Add(new SqlConnection(sb.ConnectionString));
                        //resList.Add(new ConnectionDescription(ConnectionString.DataSource, str, ConnectionString.UserID, ConnectionString.Password));
                    }
                }
                return resList;
            }
        }

    }
}
