﻿using System;
using System.Data.SqlClient;
using System.Xml;
using KeyChecker;
using Microsoft.Win32;

namespace CreateConnection
{
    public class ConnectionToSWDB
    {
        private const string APP_REGISTRY_KEY = @"SOFTWARE\SoftServe\SalesWorks Enterprise";
        private string _InstallPath;
        private DatabaseInfos _dbInfo;
        private SqlConnection _connection;

        public ConnectionToSWDB()
        {
            LoadXmlSettings(PathToConfigFile);
            CreateConnection();
        }

        public string UserID
        {
            get
            {
                return _dbInfo.UserName;
            }
        }

        public string UserPWD
        {
            get
            {
                return GetDecryptPassword(_dbInfo.Password);
            }
        }

        private string InstallPath
        {
            get
            {
                if (_InstallPath == null)
                {
                    RegistryKey rk = Registry.LocalMachine.OpenSubKey(APP_REGISTRY_KEY, false);
                    if (rk == null) throw new Exception();
                    object objRegValue = rk.GetValue("Path");
                    if (objRegValue == null) throw new Exception();
                    _InstallPath = objRegValue.ToString();
                    if (_InstallPath[_InstallPath.Length - 1] != '\\') _InstallPath += '\\';
                }
                return _InstallPath;
            }
        }

        private string PathToConfigFile
        {
            get
            {
                return InstallPath + "config.xml";
            }
        }

        public SqlConnection Connection
        {
            get { return _connection; }
        }

        private void LoadXmlSettings(string fileName)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);
            XmlNodeList nodeList = xmlDocument.SelectNodes("/config/DB");
            if (nodeList != null && nodeList.Count > 0)
            {
                XmlNode xmlNode = nodeList[0];
                _dbInfo = new DatabaseInfos();
                LoadDBSettings(xmlNode);
            }
        }
        private void LoadDBSettings(XmlNode xmlNode)
        {
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                if (childNode.Name == "DisplayName")
                    _dbInfo.DisplayName = childNode.InnerText;
                if (childNode.Name == "ServerName")
                    _dbInfo.ServerName = childNode.InnerText;
                if (childNode.Name == "DatabaseName")
                    _dbInfo.DatabaseName = childNode.InnerText;
                if (childNode.Name == "UserName")
                    _dbInfo.UserName = childNode.InnerText;
                if (childNode.Name == "Password")
                    _dbInfo.Password = childNode.InnerText;
                if (childNode.Name == "DSN_CUS2SW")
                    _dbInfo.CUS2SW = childNode.InnerText;
                if (childNode.Name == "DSN_SW2CUS")
                    _dbInfo.SW2CUS = childNode.InnerText;
            }
        }
        private string GetDecryptPassword(string encryptPassword)
        {
            CCheckerClass key = new CCheckerClass();
            key.Decrypt(ref encryptPassword, (uint)encryptPassword.Length);
            return encryptPassword.Split('\0')[0];            
        }

        private void CreateConnection()
        {
            SqlConnectionStringBuilder _conSB = new SqlConnectionStringBuilder();
            _conSB.DataSource = _dbInfo.ServerName;
            _conSB.InitialCatalog = _dbInfo.DatabaseName;
            _conSB.UserID = _dbInfo.UserName;
            _conSB.Password = GetDecryptPassword(_dbInfo.Password);
            _conSB.Pooling = false;                     
            _connection = new SqlConnection(_conSB.ConnectionString);
        }
    }
    public class DatabaseInfos
    {
        public string ServerName;
        public string DatabaseName;
        public string DisplayName;
        public string HTML;
        public string RestoreFileName;
        public string BackupFileName;
        public string CUS2PS;
        public string PS2CUS;
        public string CUS2SW;
        public string SW2CUS;
        public string UserName;
        public string Password;
        public string RemoteServerName;
        public string XInternal;
    }
}
