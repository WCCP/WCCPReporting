﻿namespace Logica.LDBSync.Main
{
    partial class ErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDetails1 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.rtbInfo = new System.Windows.Forms.RichTextBox();
            this.rtbDetails = new System.Windows.Forms.RichTextBox();
            this.btnDetails2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDetails1
            // 
            this.btnDetails1.Location = new System.Drawing.Point(302, 64);
            this.btnDetails1.Name = "btnDetails1";
            this.btnDetails1.Size = new System.Drawing.Size(75, 23);
            this.btnDetails1.TabIndex = 0;
            this.btnDetails1.Text = "Детали";
            this.btnDetails1.UseVisualStyleBackColor = true;
            this.btnDetails1.Click += new System.EventHandler(this.btnDetails1_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(403, 64);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Закрыть";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // rtbInfo
            // 
            this.rtbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbInfo.Location = new System.Drawing.Point(88, 23);
            this.rtbInfo.Name = "rtbInfo";
            this.rtbInfo.ReadOnly = true;
            this.rtbInfo.Size = new System.Drawing.Size(390, 26);
            this.rtbInfo.TabIndex = 2;
            this.rtbInfo.Text = "";
            // 
            // rtbDetails
            // 
            this.rtbDetails.Location = new System.Drawing.Point(12, 97);
            this.rtbDetails.Name = "rtbDetails";
            this.rtbDetails.ReadOnly = true;
            this.rtbDetails.Size = new System.Drawing.Size(465, 270);
            this.rtbDetails.TabIndex = 3;
            this.rtbDetails.Text = "";
            this.rtbDetails.Visible = false;
            // 
            // btnDetails2
            // 
            this.btnDetails2.Location = new System.Drawing.Point(302, 64);
            this.btnDetails2.Name = "btnDetails2";
            this.btnDetails2.Size = new System.Drawing.Size(75, 23);
            this.btnDetails2.TabIndex = 4;
            this.btnDetails2.Text = "Детали";
            this.btnDetails2.UseVisualStyleBackColor = true;
            this.btnDetails2.Visible = false;
            this.btnDetails2.Click += new System.EventHandler(this.btnDetails2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Logica.LDBSync.Main.Properties.Resources.error2;
            this.pictureBox1.Location = new System.Drawing.Point(26, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 37);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // ErrorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(489, 378);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnDetails2);
            this.Controls.Add(this.rtbDetails);
            this.Controls.Add(this.rtbInfo);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDetails1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ошибка";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDetails1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.RichTextBox rtbInfo;
        private System.Windows.Forms.RichTextBox rtbDetails;
        private System.Windows.Forms.Button btnDetails2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}