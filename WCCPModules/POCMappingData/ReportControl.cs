﻿using System;
using System.Data;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.XtraTab;
using DevExpress.XtraBars;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.BaseReportControl;
using WccpReporting.POCMappingData;
using WccpReporting.POCMappingData.UserControls;
using WccpReporting.POCMappingData.Core;
using WccpReporting.POCMappingData.DataProvider;

namespace WccpReporting {
    public partial class WccpUIControl : DevExpress.XtraEditors.XtraUserControl {
        readonly DataReport _dataReport = new DataReport();
        readonly DataReportRU _dataReportRU = new DataReportRU();
        private int _reportId;
        readonly SettingsForm _setForm = new SettingsForm();
        SettingsForm.SettingsData _lastReportData;

        public WccpUIControl(int reportId) {
            InitializeComponent();
            _reportId = reportId;
        }

        public int ReportInit()
        {
            return CallSettingsForm();
        }

        private int CallSettingsForm()
        {
            if (_setForm.ShowDialog() == DialogResult.OK)
            {
                ShowReports();
                return 0;
            }

            return 1;
        }

        internal virtual void ShowReports()
        {
            WaitManager.StartWait();

            try {
                while (tabManager.TabPages.Count > 0)
                    tabManager.TabPages.RemoveAt(0);

                if (_setForm.SelectedData.Reports == SettingsForm.SettingsData.ReportType.pivot ||
                    _setForm.SelectedData.Reports == SettingsForm.SettingsData.ReportType.both)
                {
                    DataTable lDataTable = DataAccessProvider.GetMappingData(_setForm.SelectedData.SelectedWave,
                        _setForm.SelectedData.SelectedStaffM4, _setForm.SelectedData.SelectedChanel.ToString()); int columnCount1 = lDataTable.Columns.Count;
                    if (_setForm.SelectedData.CountryId == CommonConstants.UkraineId)
                    {
                        _dataReport.SetSource(lDataTable);
                        _dataReport.ShowReport(_setForm);

                        XtraTabPage lPage = new XtraTabPage();
                        lPage.Controls.Add(_dataReport as Control);
                        lPage.Tag = _dataReport;
                        lPage.Text = CommonConstants.WccpUIControl.Data;
                        lPage.Dock = DockStyle.Fill;
                        _dataReport.Dock = DockStyle.Fill;
                        tabManager.TabPages.Add(lPage);
                        btnExportToExcelXlsx.Visibility = BarItemVisibility.Never;
                        btnExportAllToExcelXlsx.Visibility = BarItemVisibility.Never;
                    }
                    else
                    {
                        _dataReportRU.SetSource(lDataTable);
                        _dataReportRU.ShowReport(_setForm);

                        XtraTabPage lPage = new XtraTabPage();
                        lPage.Controls.Add(_dataReportRU as Control);
                        lPage.Tag = _dataReportRU;
                        lPage.Text = CommonConstants.WccpUIControl.Data;
                        lPage.Dock = DockStyle.Fill;
                        _dataReportRU.Dock = DockStyle.Fill;
                        tabManager.TabPages.Add(lPage);
                        btnExportToExcelXlsx.Visibility = BarItemVisibility.Always;
                        btnExportAllToExcelXlsx.Visibility = BarItemVisibility.Always;
                    }
                }

                if (_setForm.SelectedData.Reports == SettingsForm.SettingsData.ReportType.flat ||
                    _setForm.SelectedData.Reports == SettingsForm.SettingsData.ReportType.both)
                {
                    DataTable lDataTableFlat = DataAccessProvider.GetMappingDataFlat(_setForm.SelectedData.SelectedWave,
                        _setForm.SelectedData.SelectedStaffM4, _setForm.SelectedData.SelectedChanel.ToString());
                    IFlatReport lFlatReport;

                    if (_setForm.SelectedData.SelectedChanel == CommonConstants.ChnlOnUAId
                        && _setForm.SelectedData.CountryId == CommonConstants.UkraineId)
                        lFlatReport = new FlatDataReport();
                    else if (_setForm.SelectedData.SelectedChanel == CommonConstants.ChnlOffUAId
                        && _setForm.SelectedData.CountryId == CommonConstants.UkraineId)
                        lFlatReport = new FlatDataReportOff();
                    else if (_setForm.SelectedData.SelectedChanel == CommonConstants.ChnlOnRUId
                        && _setForm.SelectedData.CountryId == CommonConstants.RusiiaId)
                        lFlatReport = new FlatDataReportRU();
                    else
                        lFlatReport = new FlatDataReportOffRU();
                    lFlatReport.SetSource(lDataTableFlat);
                    lFlatReport.ShowReport();

                    XtraTabPage lPage = new XtraTabPage();
                    lPage.Controls.Add(lFlatReport as Control);
                    lPage.Tag = lFlatReport;
                    lPage.Text = CommonConstants.WccpUIControl.FlatData;
                    lPage.Dock = DockStyle.Fill;
                    (lFlatReport as Control).Dock = DockStyle.Fill;
                    tabManager.TabPages.Add(lPage);
                }
            }
            finally {
                WaitManager.StopWait();
            }
            
            _lastReportData = _setForm.SelectedData;
            btnRefreshCats.Enabled = _setForm.LastWaveSelected && DataAccessProvider.UserHasWriteAccess(_reportId);
        }

        #region Menu Event

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e) {
            CallSettingsForm();
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e) {
            if (null != tabManager.SelectedTabPage)
                tabManager.TabPages.Remove(tabManager.SelectedTabPage);
        }

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e) {
            if (tabManager.SelectedTabPage != null) {
                IReport lReport = tabManager.SelectedTabPage.Tag as IReport;
                if (lReport != null)
                    lReport.ShowPrintPreview();
            }
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e) {
            if (_setForm.ValidateData(true))
                ShowReports();
        }

        private void btnRefreshCats_ItemClick(object sender, ItemClickEventArgs e)
        {
            //if (new RefreshCathegoriesForm().ShowDialog() == DialogResult.OK)
            //    DataAccessProvider.UpdateCath(_lastReportData.SelectedWave, _lastReportData.SelectedStaffM4, _lastReportData.SelectedChanel.ToString());
        }

        #endregion

        #region Export engine

        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e) {
            Export(ExportToType.Xls);
        }

        private void btnExportToExcelXlsx_ItemClick(object sender, ItemClickEventArgs e)
        {
            Export(ExportToType.Xlsx);
        }

        private void Export(ExportToType type) {
            if (null == tabManager.SelectedTabPage)
                return;

            SaveFileDialog lSaveFileDlg = new SaveFileDialog();

            lSaveFileDlg.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            lSaveFileDlg.FileName = CommonConstants.WccpUIControl.POCMappingData + tabManager.SelectedTabPage.Text;
            lSaveFileDlg.Filter = string.Format("(*.{0})|*.{0}",type.ToString().ToLower()) ;
            lSaveFileDlg.FilterIndex = 1;
            lSaveFileDlg.OverwritePrompt = true;
            lSaveFileDlg.RestoreDirectory = true;

            if (lSaveFileDlg.ShowDialog() != DialogResult.Cancel)
                ExportEngine(tabManager.SelectedTabPage, lSaveFileDlg.FileName, type);
        }
        
        private void btnExportAll_ItemClick(object sender, ItemClickEventArgs e) {
            ExportToType type = (ExportToType)e.Item.Tag;
            if (tabManager.TabPages.Count < 2) {
                Export((ExportToType)e.Item.Tag);
                return;
            }

            DialogResult lDialogResult = MessageBox.Show(CommonConstants.WccpUIControl.ExportAllTabs
                + " (" + tabManager.TabPages.Count + " " + CommonConstants.WccpUIControl.Things + ")",
                CommonConstants.WccpUIControl.Info, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult.Yes == lDialogResult) {
                FolderBrowserDialog lFolderDlg = new FolderBrowserDialog();
                lFolderDlg.Description = CommonConstants.WccpUIControl.ChooseDirectory;
                string lPathStr = System.Reflection.Assembly.GetExecutingAssembly().Location;
                lPathStr = lPathStr.Substring(0, lPathStr.LastIndexOf(@"\") + 1);
                lFolderDlg.SelectedPath = lPathStr;

                if (lFolderDlg.ShowDialog() != DialogResult.Cancel) {
                    foreach (XtraTabPage page in tabManager.TabPages) {
                        string lFName = string.Format("{0}\\" + CommonConstants.WccpUIControl.POCMappingData + "{1}.{2}",
                            lFolderDlg.SelectedPath, page.Text, type.ToString().ToLower());
                        ExportEngine(page, lFName, type);
                    }
                }
            }
        }

        private void ExportEngine(XtraTabPage page, string fileName, ExportToType type) {
            IReport lReport = page.Tag as IReport;
            if (lReport == null)
                return;
            try {
                switch (type)
                {
                    case ExportToType.Xls: lReport.ExportToXls(fileName); break;
                    case ExportToType.Xlsx: lReport.ExportToXlsx(fileName); break;
                    default:return;
                }
                
            }
            catch (Exception lException) {
                MessageBox.Show(CommonConstants.WccpUIControl.ExportError + fileName, CommonConstants.WccpUIControl.Error);
            }
        }

        #endregion
    }
}
