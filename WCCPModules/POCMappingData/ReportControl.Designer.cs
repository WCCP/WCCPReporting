﻿using Logica.Reports.BaseReportControl;
namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.barMenuManager = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportAllToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.btnSettings = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefreshCats = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.imCollection = new DevExpress.Utils.ImageCollection();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu();
            this.tabManager = new DevExpress.XtraTab.XtraTabControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager();
            this.btnExportToExcelXlsx = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToExcelXlsx = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabManager)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // barMenuManager
            // 
            this.barMenuManager.AllowCustomization = false;
            this.barMenuManager.AllowMoveBarOnToolbar = false;
            this.barMenuManager.AllowQuickCustomization = false;
            this.barMenuManager.AllowShowToolbarsPopup = false;
            this.barMenuManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barMenuManager.Controller = this.barAndDockingController1;
            this.barMenuManager.DockControls.Add(this.barDockControlTop);
            this.barMenuManager.DockControls.Add(this.barDockControlBottom);
            this.barMenuManager.DockControls.Add(this.barDockControlLeft);
            this.barMenuManager.DockControls.Add(this.barDockControlRight);
            this.barMenuManager.DockControls.Add(this.standaloneBarDockControl1);
            this.barMenuManager.Form = this;
            this.barMenuManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSettings,
            this.btnClose,
            this.btnPrint,
            this.btnRefresh,
            this.btnExportTo,
            this.btnExportToExcel,
            this.btnExportAllTo,
            this.btnExportAllToExcel,
            this.barButtonItem1,
            this.btnRefreshCats,
            this.btnExportToExcelXlsx,
            this.btnExportAllToExcelXlsx});
            this.barMenuManager.LargeImages = this.imCollection;
            this.barMenuManager.MaxItemId = 33;
            this.barMenuManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            // 
            // bar1
            // 
            this.bar1.BarName = "menuBar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(74, 165);
            this.bar1.FloatSize = new System.Drawing.Size(48, 32);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportAllTo, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTo, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSettings, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefreshCats)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "menuBar";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Обновить";
            this.btnRefresh.Id = 6;
            this.btnRefresh.LargeImageIndex = 5;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnExportAllTo
            // 
            this.btnExportAllTo.Caption = "Экспортировать все в...";
            this.btnExportAllTo.Id = 19;
            this.btnExportAllTo.LargeImageIndex = 14;
            this.btnExportAllTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToExcelXlsx)});
            this.btnExportAllTo.Name = "btnExportAllTo";
            // 
            // btnExportAllToExcel
            // 
            this.btnExportAllToExcel.Caption = "Xls";
            this.btnExportAllToExcel.Id = 20;
            this.btnExportAllToExcel.LargeImageIndex = 3;
            this.btnExportAllToExcel.Name = "btnExportAllToExcel";
            this.btnExportAllToExcel.Tag = Logica.Reports.BaseReportControl.ExportToType.Xls;
            this.btnExportAllToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportTo
            // 
            this.btnExportTo.Caption = "Экспортировать в ...";
            this.btnExportTo.Id = 10;
            this.btnExportTo.LargeImageIndex = 15;
            this.btnExportTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportToExcel, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToExcelXlsx)});
            this.btnExportTo.Name = "btnExportTo";
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Caption = "Xls";
            this.btnExportToExcel.Id = 11;
            this.btnExportToExcel.LargeImageIndex = 3;
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Tag = Logica.Reports.BaseReportControl.ExportToType.Xls;
            this.btnExportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "Печать";
            this.btnPrint.Id = 5;
            this.btnPrint.LargeImageIndex = 4;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
            // 
            // btnSettings
            // 
            this.btnSettings.Caption = "Параметры отчета";
            this.btnSettings.Id = 3;
            this.btnSettings.LargeImageIndex = 0;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSettings_ItemClick);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "Закрыть лист";
            this.btnClose.Id = 4;
            this.btnClose.LargeImageIndex = 2;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // btnRefreshCats
            // 
            this.btnRefreshCats.Caption = "Обновить категории ТТ";
            this.btnRefreshCats.Enabled = false;
            this.btnRefreshCats.Id = 30;
            this.btnRefreshCats.Name = "btnRefreshCats";
            this.btnRefreshCats.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnRefreshCats.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefreshCats_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(640, 32);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.barAndDockingController1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(640, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 480);
            this.barDockControlBottom.Size = new System.Drawing.Size(640, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 480);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(640, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 480);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 29;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "briefcase_ok_24.png");
            this.imCollection.Images.SetKeyName(1, "briefcase_prev_24.png");
            this.imCollection.Images.SetKeyName(2, "del_24.png");
            this.imCollection.Images.SetKeyName(3, "Excel_42.bmp");
            this.imCollection.Images.SetKeyName(4, "print_24.png");
            this.imCollection.Images.SetKeyName(5, "refresh_24.png");
            this.imCollection.Images.SetKeyName(6, "ExcelAll_42.bmp");
            this.imCollection.Images.SetKeyName(7, "html.PNG");
            this.imCollection.Images.SetKeyName(8, "mhtml.PNG");
            this.imCollection.Images.SetKeyName(9, "notepad.PNG");
            this.imCollection.Images.SetKeyName(10, "pdf.PNG");
            this.imCollection.Images.SetKeyName(11, "rft.PNG");
            this.imCollection.Images.SetKeyName(12, "csv.PNG");
            this.imCollection.Images.SetKeyName(13, "bmp.PNG");
            this.imCollection.Images.SetKeyName(14, "programs_refresh_24.png");
            this.imCollection.Images.SetKeyName(15, "image_refresh_24.png");
            this.imCollection.Images.SetKeyName(16, "Xlsx.png");
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.popupMenu1.Manager = this.barMenuManager;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // tabManager
            // 
            this.tabManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabManager.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tabManager.Images = this.imCollection;
            this.tabManager.Location = new System.Drawing.Point(0, 32);
            this.tabManager.Name = "tabManager";
            this.tabManager.Size = new System.Drawing.Size(640, 448);
            this.tabManager.TabIndex = 5;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = "toolStripMenuItem2";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.Controller = this.barAndDockingController1;
            this.xtraTabbedMdiManager1.Images = this.imCollection;
            this.xtraTabbedMdiManager1.MdiParent = null;
            // 
            // btnExportToExcelXlsx
            // 
            this.btnExportToExcelXlsx.Caption = "Xlsx";
            this.btnExportToExcelXlsx.Id = 31;
            this.btnExportToExcelXlsx.LargeImageIndex = 16;
            this.btnExportToExcelXlsx.Name = "btnExportToExcelXlsx";
            this.btnExportToExcelXlsx.Tag = Logica.Reports.BaseReportControl.ExportToType.Xlsx;
            this.btnExportToExcelXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportToExcelXlsx_ItemClick);
            // 
            // btnExportAllToExcelXlsx
            // 
            this.btnExportAllToExcelXlsx.Caption = "Xlsx";
            this.btnExportAllToExcelXlsx.Id = 32;
            this.btnExportAllToExcelXlsx.LargeImageIndex = 16;
            this.btnExportAllToExcelXlsx.Name = "btnExportAllToExcelXlsx";
            this.btnExportAllToExcelXlsx.Tag = Logica.Reports.BaseReportControl.ExportToType.Xlsx; 
            this.btnExportAllToExcelXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabManager);
            this.Controls.Add(this.standaloneBarDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(640, 480);
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabManager)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barMenuManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem btnSettings;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraTab.XtraTabControl tabManager;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarSubItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem btnExportToExcel;
        private DevExpress.XtraBars.BarSubItem btnExportAllTo;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToExcel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.BarButtonItem btnRefreshCats;
        private DevExpress.XtraBars.BarButtonItem btnExportToExcelXlsx;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToExcelXlsx;

    }
}
