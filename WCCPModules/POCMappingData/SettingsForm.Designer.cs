﻿namespace WccpReporting.POCMappingData
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.chkBoxStaffM4 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.LStaffM4 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpWave = new DevExpress.XtraEditors.LookUpEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.chkReports = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.LookUpChannels = new DevExpress.XtraEditors.LookUpEdit();
            this.lChannelTT = new DevExpress.XtraEditors.LabelControl();
            this.LWave = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxStaffM4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpChannels.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // chkBoxStaffM4
            // 
            this.chkBoxStaffM4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBoxStaffM4.Location = new System.Drawing.Point(111, 43);
            this.chkBoxStaffM4.Name = "chkBoxStaffM4";
            this.chkBoxStaffM4.Properties.DisplayMember = "Name";
            this.chkBoxStaffM4.Properties.SelectAllItemCaption = "(Все)";
            this.chkBoxStaffM4.Properties.ValueMember = "StaffId";
            this.chkBoxStaffM4.Size = new System.Drawing.Size(227, 20);
            this.chkBoxStaffM4.TabIndex = 0;
            this.chkBoxStaffM4.EditValueChanged += new System.EventHandler(this.editor_EditValueChanged);
            // 
            // LStaffM4
            // 
            this.LStaffM4.Location = new System.Drawing.Point(34, 46);
            this.LStaffM4.Name = "LStaffM4";
            this.LStaffM4.Size = new System.Drawing.Size(69, 13);
            this.LStaffM4.TabIndex = 3;
            this.LStaffM4.Text = "Персонал М4:";
            // 
            // lookUpWave
            // 
            this.lookUpWave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpWave.Location = new System.Drawing.Point(111, 15);
            this.lookUpWave.Name = "lookUpWave";
            this.lookUpWave.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpWave.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WaveName", 100)});
            this.lookUpWave.Properties.DisplayMember = "WaveName";
            this.lookUpWave.Properties.NullText = "";
            this.lookUpWave.Properties.PopupFormMinSize = new System.Drawing.Size(20, 50);
            this.lookUpWave.Properties.ShowFooter = false;
            this.lookUpWave.Properties.ShowHeader = false;
            this.lookUpWave.Properties.ValueMember = "Wave_ID";
            this.lookUpWave.Size = new System.Drawing.Size(227, 20);
            this.lookUpWave.TabIndex = 13;
            this.lookUpWave.EditValueChanged += new System.EventHandler(this.editor_EditValueChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.ImageList = this.imCollection;
            this.btnCancel.Location = new System.Drawing.Point(266, 163);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 25);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Нет";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "check_24.png");
            this.imCollection.Images.SetKeyName(1, "close_24.png");
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 0;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(185, 163);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 17;
            this.btnYes.Text = "Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // chkReports
            // 
            this.chkReports.Location = new System.Drawing.Point(111, 100);
            this.chkReports.Name = "chkReports";
            this.chkReports.Size = new System.Drawing.Size(227, 52);
            this.chkReports.TabIndex = 19;
            // 
            // LookUpChannels
            // 
            this.LookUpChannels.Location = new System.Drawing.Point(111, 71);
            this.LookUpChannels.Name = "LookUpChannels";
            this.LookUpChannels.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpChannels.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "Name")});
            this.LookUpChannels.Properties.DisplayMember = "ChanelType";
            this.LookUpChannels.Properties.NullText = "";
            this.LookUpChannels.Properties.PopupSizeable = false;
            this.LookUpChannels.Properties.ShowFooter = false;
            this.LookUpChannels.Properties.ShowHeader = false;
            this.LookUpChannels.Properties.ValueMember = "ChanelType_id";
            this.LookUpChannels.Size = new System.Drawing.Size(227, 20);
            this.LookUpChannels.TabIndex = 21;
            this.LookUpChannels.EditValueChanged += new System.EventHandler(this.editor_EditValueChanged);
            // 
            // lChannelTT
            // 
            this.lChannelTT.Location = new System.Drawing.Point(11, 74);
            this.lChannelTT.Name = "lChannelTT";
            this.lChannelTT.Size = new System.Drawing.Size(92, 13);
            this.lChannelTT.TabIndex = 22;
            this.lChannelTT.Text = "Канал анкеты/ТТ:";
            // 
            // LWave
            // 
            this.LWave.Location = new System.Drawing.Point(69, 18);
            this.LWave.Name = "LWave";
            this.LWave.Size = new System.Drawing.Size(34, 13);
            this.LWave.TabIndex = 23;
            this.LWave.Text = "Волна:";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 198);
            this.Controls.Add(this.LWave);
            this.Controls.Add(this.lChannelTT);
            this.Controls.Add(this.LookUpChannels);
            this.Controls.Add(this.chkReports);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.lookUpWave);
            this.Controls.Add(this.LStaffM4);
            this.Controls.Add(this.chkBoxStaffM4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры";
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxStaffM4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpChannels.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl LStaffM4;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkBoxChanel;
        private DevExpress.XtraEditors.LookUpEdit lookUpWave;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkBoxStaffM4;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.CheckedListBoxControl chkReports;
        private DevExpress.XtraEditors.LookUpEdit LookUpChannels;
        private DevExpress.XtraEditors.LabelControl lChannelTT;
        private DevExpress.XtraEditors.LabelControl LWave;
    }
}