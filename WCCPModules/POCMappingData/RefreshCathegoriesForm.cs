using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WccpReporting.POCMappingData.DataProvider;
using DevExpress.XtraEditors.Controls;
using WccpReporting.POCMappingData.Properties;

namespace WccpReporting.POCMappingData
{
    public partial class RefreshCathegoriesForm : DevExpress.XtraEditors.XtraForm
    {
        public RefreshCathegoriesForm()
        {
            InitializeComponent();
            lblQuestionPart1.Text = Resources.WnatToUpdatePOCCathPart1;
            lblQuestionPart2.Text = Resources.WnatToUpdatePOCCathPart2;
        }
    }
}