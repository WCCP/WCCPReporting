﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WccpReporting.POCMappingData.DataProvider;
using DevExpress.XtraEditors.Controls;

namespace WccpReporting.POCMappingData
{
    public partial class SettingsForm : DevExpress.XtraEditors.XtraForm
    {
        private const string pivotReport = "Сводный отчет";
        private const string flatReport = "Поточечный сводный отчет";
        private SettingsData selectedData;
        const int countyear = 10;

        public SettingsForm()
        {
            InitializeComponent();
            BuildSettings();
        }

        public SettingsData SelectedData { get { return selectedData; } }

        public bool LastWaveSelected
        {
            get
            {
                DataTable dt = lookUpWave.Properties.DataSource as DataTable;
                if (dt == null) return false;

                DateTime maxDate = dt.Rows.Cast<DataRow>().Max(x => x.Field<DateTime?>(SqlConstants.FldWaveStartDate).HasValue ? x.Field<DateTime?>(SqlConstants.FldWaveStartDate).Value : DateTime.MinValue);
                DateTime? selectedDate = (DateTime?)lookUpWave.Properties.GetDataSourceValue(SqlConstants.FldWaveStartDate, lookUpWave.ItemIndex);

                return selectedDate.HasValue && selectedDate.Value == maxDate;
            }
        }


        public bool ValidateData()
        {
            return ValidateData(false);
        }

        public bool ValidateData(bool showMessage)
        {
            Boolean isValid = true;
            string errorText = string.Empty;

            if (lookUpWave.ItemIndex > -1)
            {
                selectedData.SelectedWave = (int)lookUpWave.Properties.GetDataSourceValue(lookUpWave.Properties.ValueMember, lookUpWave.ItemIndex);
                selectedData.CountryId = int.Parse((string)lookUpWave.Properties.GetDataSourceValue(SqlConstants.FldCountryId, lookUpWave.ItemIndex));
            }
            else
            {
                errorText = string.Format(CommonConstants.SettingsForm.WaveIsEmpty, errorText, Environment.NewLine);
                isValid = false;
            }

            if (chkBoxStaffM4.EditValue != null && !string.IsNullOrEmpty(chkBoxStaffM4.EditValue.ToString()))
            {
                selectedData.SelectedStaffM4 = chkBoxStaffM4.EditValue.ToString();
            }
            else
            {
                errorText = string.Format(CommonConstants.SettingsForm.RegionIsEmpty, errorText, Environment.NewLine);
                isValid = false;
            }

            if (LookUpChannels.EditValue != null && !string.IsNullOrEmpty(LookUpChannels.EditValue.ToString()))
            {
                selectedData.SelectedChanel = (int)LookUpChannels.EditValue;
            }
            else
            {
                errorText = string.Format(CommonConstants.SettingsForm.ChanelIsEmpty, errorText, Environment.NewLine);
                isValid = false;
            }

            if (chkReports.CheckedItems.Count == 0)
            {
                errorText = string.Format(CommonConstants.SettingsForm.ReportIsNotSelected, errorText, Environment.NewLine);
                isValid = false;
            }
            else
            {
                if (chkReports.CheckedItems.Count == 2)
                    selectedData.Reports = SettingsData.ReportType.both;
                else if (chkReports.CheckedItems[0].ToString().Equals(pivotReport))
                    selectedData.Reports = SettingsData.ReportType.pivot;
                else
                    selectedData.Reports = SettingsData.ReportType.flat;
            }


            if (!string.IsNullOrEmpty(errorText) && showMessage)
            {
                MessageBox.Show(errorText, CommonConstants.SettingsForm.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            return isValid;
        }

        private void BuildSettings()
        {
            // Select data for Waves
            DataTable tblWaves = DataAccessProvider.GetWaves();
            lookUpWave.Properties.DataSource = tblWaves;

            if (tblWaves != null && tblWaves.Rows.Count > 0)
            {
                lookUpWave.EditValue = tblWaves.Rows[tblWaves.Rows.Count - 1][lookUpWave.Properties.ValueMember];
            }

            // Select data for StaffM4
            chkBoxStaffM4.Properties.DataSource = DataAccessProvider.GetStaffM4();
            CheckedListBoxItemCollection lStaffM4 = chkBoxStaffM4.Properties.GetItems();

            foreach (CheckedListBoxItem item in lStaffM4)
            {
                item.CheckState = CheckState.Checked;
            }
            if (lStaffM4.Count == 1)
            {
                chkBoxStaffM4.Properties.ReadOnly = true;
            }

            // Select data for Chanels
            DataTable channels = DataAccessProvider.GetChannels();
            LookUpChannels.Properties.DataSource = channels;
            LookUpChannels.Properties.DropDownRows = channels.Rows.Count;

            chkReports.Items.Add(pivotReport, true);
            chkReports.Items.Add(flatReport, true);

            SetBtnEnabling();
        }

        private void SetBtnEnabling()
        {
            btnYes.Enabled = ValidateData();
        }

        private void btnYes_Click(Object sender, EventArgs e)
        {
            if (ValidateData())
                DialogResult = DialogResult.OK;
        }

        private void editor_EditValueChanged(object sender, EventArgs e)
        {
            SetBtnEnabling();
        }

        public struct SettingsData
        {
            public enum ReportType
            {
                pivot = 1,
                flat,
                both
            }
            public int SelectedWave { get; set; }
            public int CountryId { get; set; }
            public int SelectedChanel { get; set; }
            public string SelectedStaffM4 { get; set; }
            public ReportType Reports { get; set; }
        }
    }
}