﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.POCMappingData.Core
{
    interface IReport
    {
        void ShowPrintPreview();
        void ExportToXls(string fileName);
        void ExportToXlsx(string fileName);
    }
}
