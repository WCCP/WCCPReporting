﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WccpReporting.POCMappingData.Core
{
    interface IFlatReport
    {
        void SetSource(DataTable source);
        void ShowReport();
    }
}
