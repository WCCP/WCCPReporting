using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Data.PivotGrid;
using DevExpress.XtraEditors.Controls;

namespace WccpReporting
{
    public partial class CustomSummaryType : DevExpress.XtraEditors.XtraForm
    {
        public PivotSummaryType SelectedSummaryType
        {
            get 
            {
                return (PivotSummaryType)rgAggList.Properties.Items[rgAggList.SelectedIndex].Value; 
            }
        }
        public CustomSummaryType()
            : this(PivotSummaryType.Count)
        { }
        public CustomSummaryType(PivotSummaryType tp)
        {
            InitializeComponent();
            int selInd = 0;
            foreach(PivotSummaryType type in Enum.GetValues(typeof(PivotSummaryType)))
            {
                RadioGroupItem ri = new RadioGroupItem(type, type.ToString());
                rgAggList.Properties.Items.Add(ri);
                if(type == tp)
                    selInd = rgAggList.Properties.Items.IndexOf(ri);
            }
            rgAggList.SelectedIndex = selInd;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}