﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace WccpReporting.POCMappingData.DataProvider
{
    public class SqlConstants
    {
        // SP names
        public const string ProcMappingDataCalc = "DW_POC_Mapping_Data_Report";
        public const string ProcMappingDataCalcFlat = "DW_POC_Mapping_Flat_Data_calc";
        public const string ProcChanelTypeList = "spDW_ChannelList";
        public const string ProcReportWriteAccess = "spDW_URM_Report_Write_Access";
        public const string ProcUpdateCath = "DW_POC_Mapping_Update_Cath";
        public const string ProcGetStaffM4 = "sp_StaffM4";
        public const string ProcGetWaves = "sp_POCMappingWave";

        // SP Params
        public const string ParamWaveId = "wave_ID";
        public const string ParamRegion = "Region";
        public const string ParamChanel = "Channel_Type";
        public const string ParamObjectId = "Object_ID";
        public const string ParamCaseChannelType = "CaseParam";
        public const string ParamStaff = "Staff_M4";

        // tables
        public const string TblWaves = "DW_POCMapping_Wave";

        // SP fieds
        public const string FldValue = "value";
        public const string FldWaveName = "WaveName";
        public const string FldWaveStartDate = "StartDate";
        public const string FldChanelName = "ChanelType";
        public const string FldChanelId = "ChanelType_id";
        public const string FldCountryId = "Country_ID";


        public const string FldInBevSales = "InBev Sales";
        public const string FldObolonSales = "Obolon Sales";
        public const string FldBBHSales = "BBH Sales";
        public const string FldPPBSales = "PPB Sales";
        public const string FldSabMillerSales = "SabMiller Sales";
        public const string FldOtherForeignSales = "OtherForeign Sales";
        public const string FldOtherNativeSales = "OtherNative Sales";
        public const string FldOtherSales = "Other Sales";

        public const string FldPosSmall = "POS Inbev Small";
        public const string FldPosMedium = "POS Inbev Medium";
        public const string FldPosLarge = "POS Inbev Large";
        public const string FldPosBar = "POS Inbev Bar";


        public const string FldEksklyuziv = "Exclusivity";
        public const string FldEksklyuzivBeer = "ExclusivityBeer";
        public const string FldBotOtherForeign = "BotOtherForeign";
        public const string FldBotOtherNative = "BotOtherNative";

        // other
        public const string BrandInBev = "InBev";
        public const string BrandObolon = "Obolon";
        public const string BrandBBH = "BBH";
        public const string BrandPPB = "PPB";
        public const string BrandSabmiller = "Sabmiller";
        public const string BrandOther = "Other";
        public const string PartOfPremium = "Доля Премиум+";

        //Russia
        public const string BrandInBevRu = "InBev";
        public const string BrandBaltika = "Baltika";
        public const string BrandEfes = "Efes";
        public const string BrandHeineken = "Heineken";
        public const string BrandMPK = "MPK";
        public const string BrandOtherRU = "Прочее";
        public const string BrandOtherImport = "OtherImport";
        public const string PartOfOtherLocal = "OtherLocal";
        public const string BrandSeveralContract = "SeveralContract";
        public const string BrandNoContract = "NoContract";
        
    }
}
