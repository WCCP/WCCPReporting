﻿using System;
using System.Collections.Generic;
using System.Data;
using BLToolkit.Data;
using Logica.Reports.DataAccess;

namespace WccpReporting.POCMappingData.DataProvider
{
    class DataAccessProvider
    {
        #region BLToolkit
        private static DbManager _db;

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = string.Empty; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        internal static DataTable GetMappingDataFlat(int waveId, string staff, string channel)
        {
            return Db.SetSpCommand(SqlConstants.ProcMappingDataCalcFlat,
                Db.Parameter(SqlConstants.ParamWaveId, waveId),
                Db.Parameter(SqlConstants.ParamStaff, "(" + staff + ")"),
                Db.Parameter(SqlConstants.ParamChanel, channel)
                ).ExecuteDataTable();
        }

        internal static DataTable GetMappingData(int waveId, string staff, string channel)
        {
            return Db.SetSpCommand(SqlConstants.ProcMappingDataCalc,
                Db.Parameter(SqlConstants.ParamWaveId, waveId),
                Db.Parameter(SqlConstants.ParamStaff, "(" + staff + ")"),
                Db.Parameter(SqlConstants.ParamChanel, channel)
                ).ExecuteDataTable();
        }
        /// <summary>
        /// Gets active waves
        /// </summary>
        /// <returns>Table with active waves</returns>
        internal static DataTable GetWaves()
        {
            return Db.SetSpCommand(SqlConstants.ProcGetWaves).ExecuteDataTable();
        }
        /// <summary>
        /// Gets chanels
        /// </summary>
        /// <returns>Table with chanels</returns>
        internal static DataTable GetChannels()
        {
            return Db.SetSpCommand(SqlConstants.ProcChanelTypeList,
                Db.Parameter(SqlConstants.ParamCaseChannelType, 1)
                ).ExecuteDataTable();
        }
        /// <summary>
        /// Gets staff
        /// </summary>
        /// <returns></returns>
        internal static DataTable GetStaffM4()
        {
            return Db.SetSpCommand(SqlConstants.ProcGetStaffM4).ExecuteDataTable();
        }
        /// <summary>
        /// Cheks whether users the have write access.
        /// </summary>
        /// <returns></returns>
        internal static bool UserHasWriteAccess(int reportId)
        {
            if (DataAccessLayer.IsLDB)
                return true;

            Object res = Db.SetSpCommand(SqlConstants.ProcReportWriteAccess,
                Db.Parameter(SqlConstants.ParamObjectId, reportId)).ExecuteScalar();

            return res != null && res is int && (int)res == 1;
        }
        /// <summary>
        /// Updates categories of POCs.
        /// </summary>
        /// <returns></returns>
        internal static void UpdateCath(int waveId, string staff, string channel)
        {
            Db.SetSpCommand(SqlConstants.ProcUpdateCath,
                Db.Parameter(SqlConstants.ParamWaveId, waveId),
                Db.Parameter(SqlConstants.ParamStaff, "(" + staff + ")"),
                Db.Parameter(SqlConstants.ParamChanel, channel)
                ).ExecuteDataSet();
        }
        #endregion
    }
}
