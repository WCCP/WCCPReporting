namespace WccpReporting.POCMappingData
{
    partial class RefreshCathegoriesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefreshCathegoriesForm));
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.lblQuestionPart1 = new System.Windows.Forms.Label();
            this.lblQuestionPart2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.ImageList = this.imCollection;
            this.btnCancel.Location = new System.Drawing.Point(351, 76);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 25);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "������";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "check_24.png");
            this.imCollection.Images.SetKeyName(1, "close_24.png");
            // 
            // btnYes
            // 
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnYes.ImageIndex = 0;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(270, 76);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 17;
            this.btnYes.Text = "��";
            // 
            // lblQuestionPart1
            // 
            this.lblQuestionPart1.AutoSize = true;
            this.lblQuestionPart1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblQuestionPart1.Location = new System.Drawing.Point(13, 25);
            this.lblQuestionPart1.Name = "lblQuestionPart1";
            this.lblQuestionPart1.Size = new System.Drawing.Size(29, 14);
            this.lblQuestionPart1.TabIndex = 19;
            this.lblQuestionPart1.Text = "test";
            // 
            // lblQuestionPart2
            // 
            this.lblQuestionPart2.AutoSize = true;
            this.lblQuestionPart2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblQuestionPart2.Location = new System.Drawing.Point(13, 39);
            this.lblQuestionPart2.Name = "lblQuestionPart2";
            this.lblQuestionPart2.Size = new System.Drawing.Size(29, 14);
            this.lblQuestionPart2.TabIndex = 20;
            this.lblQuestionPart2.Text = "test";
            // 
            // RefreshCathegoriesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 113);
            this.Controls.Add(this.lblQuestionPart2);
            this.Controls.Add(this.lblQuestionPart1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "RefreshCathegoriesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������";
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckedComboBoxEdit chkBoxChanel;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.Utils.ImageCollection imCollection;
        private System.Windows.Forms.Label lblQuestionPart1;
        private System.Windows.Forms.Label lblQuestionPart2;
    }
}