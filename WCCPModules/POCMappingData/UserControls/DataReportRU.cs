﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using Logica.Reports.Common;
using WccpReporting.POCMappingData.Core;
using WccpReporting.POCMappingData.DataProvider;

namespace WccpReporting.POCMappingData.UserControls
{
    public partial class DataReportRU : UserControl, IReport
    {
        #region Fields

        private DataTable _dataTable;

        private readonly IDictionary<string, int> _brandOrder = new Dictionary<string, int> {
            {SqlConstants.BrandInBevRu, 7},
            {SqlConstants.BrandBaltika, 8},
            {SqlConstants.BrandEfes, 9},
            {SqlConstants.BrandHeineken, 10},
            {SqlConstants.BrandMPK, 11},
            {SqlConstants.BrandOtherRU, 16}/*,
            {SqlConstants.BrandOtherImport, 16},
            {SqlConstants.PartOfOtherLocal, 17},
            {SqlConstants.BrandSeveralContract, 18},
            {SqlConstants.BrandNoContract, 19}*/
        };

        private bool _customSummaryDisabled;
        private bool _summarySetted;

        #endregion
        public DataReportRU()
        {
            InitializeComponent();
        }

        public void ShowReport(SettingsForm setForm)
        {
            _customSummaryDisabled = true;
            try
            {
                pivotGridControl.DataSource = _dataTable;

                bool lIsOnChannel = setForm.SelectedData.SelectedChanel == CommonConstants.ChnlOnRUId;
                

                pivotGridField5.Visible = !lIsOnChannel;
                pivotGridFieldEquipmDoorsCount.Visible = !lIsOnChannel;

                pivotGridFieldEquipmTapCount.Visible = lIsOnChannel;
                pivotGridFieldCountPosCoctailInAssort.Visible = lIsOnChannel;
                pivotGridFieldCountPosCraftBeerInAssort.Visible = lIsOnChannel;
                pivotGridFieldCountPosVodkaInAssort.Visible = lIsOnChannel;
                pivotGridFieldCountPosWineInAssort.Visible = lIsOnChannel;
                pivotGridFieldOtherAlcololInAssort.Visible = lIsOnChannel;
                pivotGridFieldBoardGamesAvalibility.Visible = lIsOnChannel;
                pivotGridFieldcountBud.Visible = lIsOnChannel;

                //pivotGridField
            }
            catch (Exception lException)
            {
                ErrorManager.ShowErrorBox(lException.Message);
            }
            finally
            {
                _customSummaryDisabled = false;
            }
        }

        private void pivotGridControl_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            if (e.DataField == pivotGridFieldInbev)
            {
                int lCount = 0;
                PivotDrillDownDataSource lDs = e.CreateDrillDownDataSource();

                foreach (PivotDrillDownDataRow lRow in lDs)
                {
                    lCount += (int)AsDouble(lRow[SqlConstants.FldPosSmall]);
                    lCount += (int)AsDouble(lRow[SqlConstants.FldPosMedium]);
                    lCount += (int)AsDouble(lRow[SqlConstants.FldPosLarge]);
                    lCount += (int)AsDouble(lRow[SqlConstants.FldPosBar]);                    
                }
                e.CustomValue = lCount;
            }
            else if (e.DataField == pivotGridFieldCount)
            {
                IList<string> lTempList = new List<string>();

                foreach (PivotDrillDownDataRow lRow in e.CreateDrillDownDataSource())
                {
                    if (lRow[pivotGridFieldPocId] != null &&
                        !string.IsNullOrEmpty(lRow[pivotGridFieldPocId].ToString()) &&
                        !lTempList.Contains(lRow[pivotGridFieldPocId].ToString()))
                    {
                        lTempList.Add(lRow[pivotGridFieldPocId].ToString());
                    }
                }

                e.CustomValue = lTempList.Count;
            }
        }

        private double AsDouble(object num)
        {
            try
            {
                if (num == null)
                    return 0;
                if (num is int || num is long || num is decimal || num is double)
                    return Convert.ToDouble(num);
                if (num.ToString() != string.Empty)
                    return double.Parse(num.ToString(), CultureInfo.InvariantCulture);
            }
            catch
            {
            }

            return 0;
        }

        private void pivotGridControl_FieldAreaChanging(object sender, PivotAreaChangingEventArgs e)
        {
            _summarySetted = false;
        }

        private void pivotGridControl_FieldAreaChanged(object sender, PivotFieldEventArgs e)
        {
            if (_customSummaryDisabled || _summarySetted || e.Field.Area != PivotArea.DataArea)
                return;
            CustomSummaryType lSummaryType = new CustomSummaryType();
            if (lSummaryType.ShowDialog() != DialogResult.OK)
                return;
            e.Field.SummaryType = lSummaryType.SelectedSummaryType;
            _summarySetted = true;
        }

        private void pivotGridControl_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e)
        {
            if (e.Field != pivotGridFieldBrand)
                return;
            e.Result = _brandOrder[(string)e.Value1].CompareTo(_brandOrder[(string)e.Value2]);
            e.Handled = true;
        }

        #region IReport Members

        public void ShowPrintPreview()
        {
            pivotGridControl.ShowPrintPreview();
        }

        public void ExportToXls(string fileName)
        {
            pivotGridControl.ExportToXls(fileName);
        }

        public void ExportToXlsx(string fileName)
        {
            pivotGridControl.ExportToXlsx(fileName);
        }
        #endregion

        internal void SetSource(DataTable source)
        {
            _dataTable = source;
        }
    }
}
