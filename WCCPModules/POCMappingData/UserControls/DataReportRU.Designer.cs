﻿namespace WccpReporting.POCMappingData.UserControls
{
    partial class DataReportRU
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotGridFieldCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldInbev = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldBrand = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMarketShareDraft = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMarketShareFasofki = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSalesDraft = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSalesFasofki = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldRouteOwner = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSync = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDistributor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSubType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldStaffChannel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPocChanel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldNetworkName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldUrbanRural = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldLocation = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldStorageSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPhone = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDirector = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDeliveryAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTradingName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldNameIsCloseTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPocId = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldPremium = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIdEdrpou = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldABI = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldBaltica = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldEfes = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldHeineken = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMPK = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOthers = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldConcurrents = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldContracts = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldEquipmDoorsCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldEquipmTapCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCountPosCraftBeerInAssort = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCountPosWineInAssort = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCountPosCoctailInAssort = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCountPosVodkaInAssort = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOtherAlcololInAssort = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldBoardGamesAvalibility = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldcountBud = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "Кол-во ТТ";
            this.pivotGridFieldCount.EmptyCellText = "0";
            this.pivotGridFieldCount.FieldName = "M1";
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldCount.Width = 300;
            // 
            // pivotGridFieldInbev
            // 
            this.pivotGridFieldInbev.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldInbev.AreaIndex = 9;
            this.pivotGridFieldInbev.Caption = "Инбев всего";
            this.pivotGridFieldInbev.EmptyCellText = "0";
            this.pivotGridFieldInbev.Name = "pivotGridFieldInbev";
            this.pivotGridFieldInbev.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldInbev.Visible = false;
            // 
            // pivotGridFieldBrand
            // 
            this.pivotGridFieldBrand.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldBrand.AreaIndex = 0;
            this.pivotGridFieldBrand.Caption = "Brand";
            this.pivotGridFieldBrand.EmptyCellText = "0";
            this.pivotGridFieldBrand.FieldName = "Brand";
            this.pivotGridFieldBrand.Name = "pivotGridFieldBrand";
            this.pivotGridFieldBrand.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField5.AreaIndex = 6;
            this.pivotGridField5.Caption = "Количество SKU";
            this.pivotGridField5.CellFormat.FormatString = "{0:N2}";
            this.pivotGridField5.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField5.EmptyCellText = "0";
            this.pivotGridField5.FieldName = "CountSKU";
            this.pivotGridField5.Name = "pivotGridField5";
            this.pivotGridField5.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridField5.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridField5.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldMarketShareDraft
            // 
            this.pivotGridFieldMarketShareDraft.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldMarketShareDraft.AreaIndex = 3;
            this.pivotGridFieldMarketShareDraft.Caption = "Market Share, драфт";
            this.pivotGridFieldMarketShareDraft.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldMarketShareDraft.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldMarketShareDraft.EmptyCellText = "0";
            this.pivotGridFieldMarketShareDraft.FieldName = "MarketShareDraft";
            this.pivotGridFieldMarketShareDraft.Name = "pivotGridFieldMarketShareDraft";
            this.pivotGridFieldMarketShareDraft.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridFieldMarketShareDraft.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldMarketShareDraft.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldMarketShareFasofki
            // 
            this.pivotGridFieldMarketShareFasofki.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldMarketShareFasofki.AreaIndex = 4;
            this.pivotGridFieldMarketShareFasofki.Caption = "Market Share, фасовка";
            this.pivotGridFieldMarketShareFasofki.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldMarketShareFasofki.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldMarketShareFasofki.EmptyCellText = "0";
            this.pivotGridFieldMarketShareFasofki.FieldName = "MarketShareFasofki";
            this.pivotGridFieldMarketShareFasofki.Name = "pivotGridFieldMarketShareFasofki";
            this.pivotGridFieldMarketShareFasofki.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridFieldMarketShareFasofki.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldMarketShareFasofki.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldSalesDraft
            // 
            this.pivotGridFieldSalesDraft.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSalesDraft.AreaIndex = 1;
            this.pivotGridFieldSalesDraft.Caption = "Sales Volume, драфт";
            this.pivotGridFieldSalesDraft.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldSalesDraft.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSalesDraft.EmptyCellText = "0";
            this.pivotGridFieldSalesDraft.FieldName = "SalesDraft";
            this.pivotGridFieldSalesDraft.Name = "pivotGridFieldSalesDraft";
            this.pivotGridFieldSalesDraft.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridFieldSalesDraft.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldSalesDraft.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldSalesFasofki
            // 
            this.pivotGridFieldSalesFasofki.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSalesFasofki.AreaIndex = 2;
            this.pivotGridFieldSalesFasofki.Caption = "Sales Volume, фасовка";
            this.pivotGridFieldSalesFasofki.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldSalesFasofki.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSalesFasofki.EmptyCellText = "0";
            this.pivotGridFieldSalesFasofki.FieldName = "SalesFasofki";
            this.pivotGridFieldSalesFasofki.Name = "pivotGridFieldSalesFasofki";
            this.pivotGridFieldSalesFasofki.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridFieldSalesFasofki.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldSalesFasofki.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldRouteOwner
            // 
            this.pivotGridFieldRouteOwner.AreaIndex = 18;
            this.pivotGridFieldRouteOwner.Caption = "Маршрут/Владелец";
            this.pivotGridFieldRouteOwner.FieldName = "OlOwner";
            this.pivotGridFieldRouteOwner.Name = "pivotGridFieldRouteOwner";
            // 
            // pivotGridFieldSync
            // 
            this.pivotGridFieldSync.AreaIndex = 1;
            this.pivotGridFieldSync.Caption = "ТС";
            this.pivotGridFieldSync.EmptyCellText = "0";
            this.pivotGridFieldSync.FieldName = "Cust_NAME";
            this.pivotGridFieldSync.Name = "pivotGridFieldSync";
            // 
            // pivotGridFieldDistributor
            // 
            this.pivotGridFieldDistributor.AreaIndex = 13;
            this.pivotGridFieldDistributor.Caption = "Дистрибьютор";
            this.pivotGridFieldDistributor.EmptyCellText = "0";
            this.pivotGridFieldDistributor.FieldName = "DISTR_NAME";
            this.pivotGridFieldDistributor.Name = "pivotGridFieldDistributor";
            // 
            // pivotGridFieldSubType
            // 
            this.pivotGridFieldSubType.AreaIndex = 10;
            this.pivotGridFieldSubType.Caption = "Подтип ТТ";
            this.pivotGridFieldSubType.FieldName = "OLSubTypeName";
            this.pivotGridFieldSubType.Name = "pivotGridFieldSubType";
            // 
            // pivotGridFieldType
            // 
            this.pivotGridFieldType.AreaIndex = 9;
            this.pivotGridFieldType.Caption = "Тип ТТ";
            this.pivotGridFieldType.EmptyCellText = "0";
            this.pivotGridFieldType.FieldName = "OLtype_name";
            this.pivotGridFieldType.Name = "pivotGridFieldType";
            // 
            // pivotGridFieldStaffChannel
            // 
            this.pivotGridFieldStaffChannel.AreaIndex = 12;
            this.pivotGridFieldStaffChannel.Caption = "Канал персонала М3";
            this.pivotGridFieldStaffChannel.EmptyCellText = "0";
            this.pivotGridFieldStaffChannel.FieldName = "Channel_Type";
            this.pivotGridFieldStaffChannel.Name = "pivotGridFieldStaffChannel";
            // 
            // pivotGridFieldPocChanel
            // 
            this.pivotGridFieldPocChanel.AreaIndex = 11;
            this.pivotGridFieldPocChanel.Caption = "Канал ТТ";
            this.pivotGridFieldPocChanel.EmptyCellText = "0";
            this.pivotGridFieldPocChanel.FieldName = "OlChannel";
            this.pivotGridFieldPocChanel.Name = "pivotGridFieldPocChanel";
            // 
            // pivotGridFieldNetworkName
            // 
            this.pivotGridFieldNetworkName.AreaIndex = 15;
            this.pivotGridFieldNetworkName.Caption = "Принадлежность к сети";
            this.pivotGridFieldNetworkName.EmptyCellText = "0";
            this.pivotGridFieldNetworkName.FieldName = "OlNetwork";
            this.pivotGridFieldNetworkName.Name = "pivotGridFieldNetworkName";
            // 
            // pivotGridFieldUrbanRural
            // 
            this.pivotGridFieldUrbanRural.AreaIndex = 16;
            this.pivotGridFieldUrbanRural.Caption = "Urban/Rural";
            this.pivotGridFieldUrbanRural.FieldName = "Settlement";
            this.pivotGridFieldUrbanRural.Name = "pivotGridFieldUrbanRural";
            // 
            // pivotGridFieldLocation
            // 
            this.pivotGridFieldLocation.AreaIndex = 14;
            this.pivotGridFieldLocation.Caption = "Населенный пункт";
            this.pivotGridFieldLocation.EmptyCellText = "0";
            this.pivotGridFieldLocation.FieldName = "City_name";
            this.pivotGridFieldLocation.Name = "pivotGridFieldLocation";
            // 
            // pivotGridFieldStorageSize
            // 
            this.pivotGridFieldStorageSize.AreaIndex = 17;
            this.pivotGridFieldStorageSize.Caption = "Размер склада";
            this.pivotGridFieldStorageSize.FieldName = "OLWHSize";
            this.pivotGridFieldStorageSize.Name = "pivotGridFieldStorageSize";
            // 
            // pivotGridFieldPhone
            // 
            this.pivotGridFieldPhone.AreaIndex = 8;
            this.pivotGridFieldPhone.Caption = "Телефон";
            this.pivotGridFieldPhone.EmptyCellText = "0";
            this.pivotGridFieldPhone.FieldName = "OLTelephone";
            this.pivotGridFieldPhone.Name = "pivotGridFieldPhone";
            // 
            // pivotGridFieldDirector
            // 
            this.pivotGridFieldDirector.AreaIndex = 7;
            this.pivotGridFieldDirector.Caption = "Контактное лицо";
            this.pivotGridFieldDirector.EmptyCellText = "0";
            this.pivotGridFieldDirector.FieldName = "OLDirector";
            this.pivotGridFieldDirector.Name = "pivotGridFieldDirector";
            // 
            // pivotGridFieldDeliveryAddress
            // 
            this.pivotGridFieldDeliveryAddress.AreaIndex = 5;
            this.pivotGridFieldDeliveryAddress.Caption = "Факт. Адрес ТТ";
            this.pivotGridFieldDeliveryAddress.EmptyCellText = "0";
            this.pivotGridFieldDeliveryAddress.FieldName = "OLDeliveryAddress";
            this.pivotGridFieldDeliveryAddress.Name = "pivotGridFieldDeliveryAddress";
            // 
            // pivotGridFieldAddress
            // 
            this.pivotGridFieldAddress.AreaIndex = 6;
            this.pivotGridFieldAddress.Caption = "Юр. Адрес ТТ";
            this.pivotGridFieldAddress.EmptyCellText = "0";
            this.pivotGridFieldAddress.FieldName = "OLAddress";
            this.pivotGridFieldAddress.Name = "pivotGridFieldAddress";
            // 
            // pivotGridFieldTradingName
            // 
            this.pivotGridFieldTradingName.AreaIndex = 4;
            this.pivotGridFieldTradingName.Caption = "Факт. Имя ТТ";
            this.pivotGridFieldTradingName.EmptyCellText = "0";
            this.pivotGridFieldTradingName.FieldName = "OLTradingName";
            this.pivotGridFieldTradingName.Name = "pivotGridFieldTradingName";
            // 
            // pivotGridFieldNameIsCloseTT
            // 
            this.pivotGridFieldNameIsCloseTT.AreaIndex = 19;
            this.pivotGridFieldNameIsCloseTT.Caption = "ТТ закрыта, не существует";
            this.pivotGridFieldNameIsCloseTT.FieldName = "isCloseTT";
            this.pivotGridFieldNameIsCloseTT.Name = "pivotGridFieldNameIsCloseTT";
            // 
            // pivotGridFieldName
            // 
            this.pivotGridFieldName.AreaIndex = 3;
            this.pivotGridFieldName.Caption = "Юр. Имя ТТ";
            this.pivotGridFieldName.EmptyCellText = "0";
            this.pivotGridFieldName.FieldName = "OLName";
            this.pivotGridFieldName.Name = "pivotGridFieldName";
            // 
            // pivotGridFieldPocId
            // 
            this.pivotGridFieldPocId.AreaIndex = 2;
            this.pivotGridFieldPocId.Caption = "Код ТТ";
            this.pivotGridFieldPocId.EmptyCellText = "0";
            this.pivotGridFieldPocId.FieldName = "Ol_id";
            this.pivotGridFieldPocId.Name = "pivotGridFieldPocId";
            // 
            // pivotGridFieldM1
            // 
            this.pivotGridFieldM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM1.AreaIndex = 3;
            this.pivotGridFieldM1.Caption = "M1";
            this.pivotGridFieldM1.EmptyCellText = "0";
            this.pivotGridFieldM1.FieldName = "M1";
            this.pivotGridFieldM1.Name = "pivotGridFieldM1";
            // 
            // pivotGridFieldM2
            // 
            this.pivotGridFieldM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM2.AreaIndex = 2;
            this.pivotGridFieldM2.Caption = "M2";
            this.pivotGridFieldM2.EmptyCellText = "0";
            this.pivotGridFieldM2.FieldName = "M2";
            this.pivotGridFieldM2.Name = "pivotGridFieldM2";
            // 
            // pivotGridFieldM3
            // 
            this.pivotGridFieldM3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM3.AreaIndex = 1;
            this.pivotGridFieldM3.Caption = "M3";
            this.pivotGridFieldM3.EmptyCellText = "0";
            this.pivotGridFieldM3.FieldName = "M3";
            this.pivotGridFieldM3.Name = "pivotGridFieldM3";
            // 
            // pivotGridFieldM4
            // 
            this.pivotGridFieldM4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM4.AreaIndex = 0;
            this.pivotGridFieldM4.Caption = "M4";
            this.pivotGridFieldM4.EmptyCellText = "0";
            this.pivotGridFieldM4.FieldName = "M4";
            this.pivotGridFieldM4.Name = "pivotGridFieldM4";
            // 
            // pivotGridFieldRegion
            // 
            this.pivotGridFieldRegion.AreaIndex = 0;
            this.pivotGridFieldRegion.Caption = "Регион";
            this.pivotGridFieldRegion.EmptyCellText = "0";
            this.pivotGridFieldRegion.FieldName = "Region_name";
            this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldRegion,
            this.pivotGridFieldM4,
            this.pivotGridFieldM3,
            this.pivotGridFieldM2,
            this.pivotGridFieldM1,
            this.pivotGridFieldPocId,
            this.pivotGridFieldName,
            this.pivotGridFieldNameIsCloseTT,
            this.pivotGridFieldTradingName,
            this.pivotGridFieldAddress,
            this.pivotGridFieldDeliveryAddress,
            this.pivotGridFieldDirector,
            this.pivotGridFieldPhone,
            this.pivotGridFieldStorageSize,
            this.pivotGridFieldLocation,
            this.pivotGridFieldUrbanRural,
            this.pivotGridFieldNetworkName,
            this.pivotGridFieldPocChanel,
            this.pivotGridFieldStaffChannel,
            this.pivotGridFieldType,
            this.pivotGridFieldSubType,
            this.pivotGridFieldDistributor,
            this.pivotGridFieldSync,
            this.pivotGridFieldRouteOwner,
            this.pivotGridFieldSalesFasofki,
            this.pivotGridFieldSalesDraft,
            this.pivotGridFieldMarketShareFasofki,
            this.pivotGridFieldMarketShareDraft,
            this.pivotGridField5,
            this.pivotGridFieldPremium,
            this.pivotGridFieldIdEdrpou,
            this.pivotGridFieldBrand,
            this.pivotGridFieldInbev,
            this.pivotGridFieldCount,
            this.pivotGridFieldABI,
            this.pivotGridFieldBaltica,
            this.pivotGridFieldEfes,
            this.pivotGridFieldHeineken,
            this.pivotGridFieldMPK,
            this.pivotGridFieldOthers,
            this.pivotGridFieldConcurrents,
            this.pivotGridFieldContracts,
            this.pivotGridFieldEquipmDoorsCount,
            this.pivotGridFieldEquipmTapCount,
            this.pivotGridFieldCountPosCraftBeerInAssort,
            this.pivotGridFieldCountPosWineInAssort,
            this.pivotGridFieldCountPosCoctailInAssort,
            this.pivotGridFieldCountPosVodkaInAssort,
            this.pivotGridFieldOtherAlcololInAssort,
            this.pivotGridFieldBoardGamesAvalibility,
            this.pivotGridFieldcountBud});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.RowArea;
            this.pivotGridControl.OptionsDataField.AreaIndex = 4;
            this.pivotGridControl.OptionsDataField.RowHeaderWidth = 170;
            this.pivotGridControl.Size = new System.Drawing.Size(1415, 799);
            this.pivotGridControl.TabIndex = 1;
            this.pivotGridControl.FieldAreaChanging += new DevExpress.XtraPivotGrid.PivotAreaChangingEventHandler(this.pivotGridControl_FieldAreaChanging);
            this.pivotGridControl.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridControl_CustomSummary);
            this.pivotGridControl.CustomFieldSort += new DevExpress.XtraPivotGrid.PivotGridCustomFieldSortEventHandler(this.pivotGridControl_CustomFieldSort);
            this.pivotGridControl.FieldAreaChanged += new DevExpress.XtraPivotGrid.PivotFieldEventHandler(this.pivotGridControl_FieldAreaChanged);
            // 
            // pivotGridFieldPremium
            // 
            this.pivotGridFieldPremium.AreaIndex = 27;
            this.pivotGridFieldPremium.Caption = "Премиальность ТТ";
            this.pivotGridFieldPremium.EmptyCellText = "0";
            this.pivotGridFieldPremium.FieldName = "fi5";
            this.pivotGridFieldPremium.Name = "pivotGridFieldPremium";
            // 
            // pivotGridFieldIdEdrpou
            // 
            this.pivotGridFieldIdEdrpou.AreaIndex = 26;
            this.pivotGridFieldIdEdrpou.Caption = "Код ЕДРПОУ";
            this.pivotGridFieldIdEdrpou.EmptyCellText = "0";
            this.pivotGridFieldIdEdrpou.FieldName = "ZKPO";
            this.pivotGridFieldIdEdrpou.Name = "pivotGridFieldIdEdrpou";
            // 
            // pivotGridFieldABI
            // 
            this.pivotGridFieldABI.AreaIndex = 26;
            this.pivotGridFieldABI.Caption = "ABI";
            this.pivotGridFieldABI.Name = "pivotGridFieldABI";
            this.pivotGridFieldABI.Visible = false;
            // 
            // pivotGridFieldBaltica
            // 
            this.pivotGridFieldBaltica.AreaIndex = 26;
            this.pivotGridFieldBaltica.Caption = "Балтика";
            this.pivotGridFieldBaltica.Name = "pivotGridFieldBaltica";
            this.pivotGridFieldBaltica.Visible = false;
            // 
            // pivotGridFieldEfes
            // 
            this.pivotGridFieldEfes.AreaIndex = 26;
            this.pivotGridFieldEfes.Caption = "Efes";
            this.pivotGridFieldEfes.Name = "pivotGridFieldEfes";
            this.pivotGridFieldEfes.Visible = false;
            // 
            // pivotGridFieldHeineken
            // 
            this.pivotGridFieldHeineken.AreaIndex = 26;
            this.pivotGridFieldHeineken.Caption = "Heineken";
            this.pivotGridFieldHeineken.Name = "pivotGridFieldHeineken";
            this.pivotGridFieldHeineken.Visible = false;
            // 
            // pivotGridFieldMPK
            // 
            this.pivotGridFieldMPK.AreaIndex = 26;
            this.pivotGridFieldMPK.Caption = "МПК";
            this.pivotGridFieldMPK.Name = "pivotGridFieldMPK";
            this.pivotGridFieldMPK.Visible = false;
            // 
            // pivotGridFieldOthers
            // 
            this.pivotGridFieldOthers.AreaIndex = 26;
            this.pivotGridFieldOthers.Caption = "Прочее";
            this.pivotGridFieldOthers.Name = "pivotGridFieldOthers";
            this.pivotGridFieldOthers.Visible = false;
            // 
            // pivotGridFieldConcurrents
            // 
            this.pivotGridFieldConcurrents.AreaIndex = 26;
            this.pivotGridFieldConcurrents.Caption = "Конкуренты";
            this.pivotGridFieldConcurrents.Name = "pivotGridFieldConcurrents";
            this.pivotGridFieldConcurrents.Visible = false;
            // 
            // pivotGridFieldContracts
            // 
            this.pivotGridFieldContracts.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldContracts.AreaIndex = 5;
            this.pivotGridFieldContracts.Caption = "Контракты";
            this.pivotGridFieldContracts.CellFormat.FormatString = "{0:N}";
            this.pivotGridFieldContracts.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldContracts.EmptyCellText = "0";
            this.pivotGridFieldContracts.FieldName = "Contract";
            this.pivotGridFieldContracts.Name = "pivotGridFieldContracts";
            this.pivotGridFieldContracts.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridFieldContracts.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldContracts.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldEquipmDoorsCount
            // 
            this.pivotGridFieldEquipmDoorsCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldEquipmDoorsCount.AreaIndex = 7;
            this.pivotGridFieldEquipmDoorsCount.Caption = "Оборудование, кол-во дверей";
            this.pivotGridFieldEquipmDoorsCount.CellFormat.FormatString = "{0:N}";
            this.pivotGridFieldEquipmDoorsCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldEquipmDoorsCount.EmptyCellText = "0";
            this.pivotGridFieldEquipmDoorsCount.FieldName = "countDoors";
            this.pivotGridFieldEquipmDoorsCount.Name = "pivotGridFieldEquipmDoorsCount";
            this.pivotGridFieldEquipmDoorsCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridFieldEquipmDoorsCount.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldEquipmDoorsCount.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldEquipmTapCount
            // 
            this.pivotGridFieldEquipmTapCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldEquipmTapCount.AreaIndex = 8;
            this.pivotGridFieldEquipmTapCount.Caption = "Оборудование, кол-во кранов";
            this.pivotGridFieldEquipmTapCount.CellFormat.FormatString = "{0:N}";
            this.pivotGridFieldEquipmTapCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldEquipmTapCount.EmptyCellText = "0";
            this.pivotGridFieldEquipmTapCount.FieldName = "CountTap";
            this.pivotGridFieldEquipmTapCount.Name = "pivotGridFieldEquipmTapCount";
            this.pivotGridFieldEquipmTapCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridFieldEquipmTapCount.TotalCellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldEquipmTapCount.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldCountPosCraftBeerInAssort
            // 
            this.pivotGridFieldCountPosCraftBeerInAssort.AreaIndex = 20;
            this.pivotGridFieldCountPosCraftBeerInAssort.Caption = "Кол-во позиций крафтового пива в ассортименте";
            this.pivotGridFieldCountPosCraftBeerInAssort.EmptyCellText = "0";
            this.pivotGridFieldCountPosCraftBeerInAssort.FieldName = "craftBeer";
            this.pivotGridFieldCountPosCraftBeerInAssort.Name = "pivotGridFieldCountPosCraftBeerInAssort";
            // 
            // pivotGridFieldCountPosWineInAssort
            // 
            this.pivotGridFieldCountPosWineInAssort.AreaIndex = 21;
            this.pivotGridFieldCountPosWineInAssort.Caption = "Кол-во позиций вина в ассортименте";
            this.pivotGridFieldCountPosWineInAssort.EmptyCellText = "0";
            this.pivotGridFieldCountPosWineInAssort.FieldName = "countVine";
            this.pivotGridFieldCountPosWineInAssort.Name = "pivotGridFieldCountPosWineInAssort";
            // 
            // pivotGridFieldCountPosCoctailInAssort
            // 
            this.pivotGridFieldCountPosCoctailInAssort.AreaIndex = 22;
            this.pivotGridFieldCountPosCoctailInAssort.Caption = "Кол-во позиций коктелей алкогольных в ассортименте";
            this.pivotGridFieldCountPosCoctailInAssort.EmptyCellText = "0";
            this.pivotGridFieldCountPosCoctailInAssort.FieldName = "countCoctail";
            this.pivotGridFieldCountPosCoctailInAssort.Name = "pivotGridFieldCountPosCoctailInAssort";
            // 
            // pivotGridFieldCountPosVodkaInAssort
            // 
            this.pivotGridFieldCountPosVodkaInAssort.AreaIndex = 23;
            this.pivotGridFieldCountPosVodkaInAssort.Caption = "Кол-во позиций водки в ассортименте";
            this.pivotGridFieldCountPosVodkaInAssort.EmptyCellText = "0";
            this.pivotGridFieldCountPosVodkaInAssort.FieldName = "countVodka";
            this.pivotGridFieldCountPosVodkaInAssort.Name = "pivotGridFieldCountPosVodkaInAssort";
            // 
            // pivotGridFieldOtherAlcololInAssort
            // 
            this.pivotGridFieldOtherAlcololInAssort.AreaIndex = 24;
            this.pivotGridFieldOtherAlcololInAssort.Caption = "Кол-во позиций другого  крепкого (Виски, Ром, Самбука, Абсент)  алкоголя в ассорт" +
    "именте";
            this.pivotGridFieldOtherAlcololInAssort.EmptyCellText = "0";
            this.pivotGridFieldOtherAlcololInAssort.FieldName = "otherStrong";
            this.pivotGridFieldOtherAlcololInAssort.Name = "pivotGridFieldOtherAlcololInAssort";
            // 
            // pivotGridFieldBoardGamesAvalibility
            // 
            this.pivotGridFieldBoardGamesAvalibility.AreaIndex = 25;
            this.pivotGridFieldBoardGamesAvalibility.Caption = "Наличие настольных игр";
            this.pivotGridFieldBoardGamesAvalibility.FieldName = "isExistGame";
            this.pivotGridFieldBoardGamesAvalibility.Name = "pivotGridFieldBoardGamesAvalibility";
            //
            //pivotGridFieldcountBud
            //
            this.pivotGridFieldcountBud.AreaIndex = 26;
            this.pivotGridFieldcountBud.Caption = "Продаётся в точке BUD";
            this.pivotGridFieldcountBud.EmptyCellText = "0";
            this.pivotGridFieldcountBud.FieldName = "countBud";
            this.pivotGridFieldcountBud.Name = "pivotGridFieldcountBud";
            // 
            // DataReportRU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pivotGridControl);
            this.Name = "DataReportRU";
            this.Size = new System.Drawing.Size(1415, 799);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldInbev;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldBrand;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMarketShareDraft;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMarketShareFasofki;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSalesDraft;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSalesFasofki;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRouteOwner;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSync;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistributor;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSubType;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldType;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStaffChannel;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPocChanel;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNetworkName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldUrbanRural;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldLocation;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStorageSize;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPhone;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDirector;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDeliveryAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTradingName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNameIsCloseTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPocId;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldABI;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldBaltica;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldEfes;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldHeineken;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMPK;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOthers;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldConcurrents;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldEquipmDoorsCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldEquipmTapCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCountPosCraftBeerInAssort;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCountPosWineInAssort;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCountPosCoctailInAssort;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCountPosVodkaInAssort;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOtherAlcololInAssort;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldBoardGamesAvalibility;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldcountBud;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldContracts;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPremium;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIdEdrpou;

    }
}
