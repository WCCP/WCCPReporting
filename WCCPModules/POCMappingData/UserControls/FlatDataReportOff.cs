﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using WccpReporting.POCMappingData.Core;

namespace WccpReporting.POCMappingData.UserControls {
    public partial class FlatDataReportOff : UserControl, IReport, IFlatReport {
        private DataTable _dataTable;

        public FlatDataReportOff() {
            InitializeComponent();
        }

        #region IReport Members

        public void ShowPrintPreview() {
            gridControl1.ShowPrintPreview();
        }

        public void ExportToXls(string fileName) {
            gridControl1.ExportToXls(fileName);
        }

        public void ExportToXlsx(string fileName)
        {
            //gridControl1.ExportToXlsx(fileName);
        }

        #endregion

        #region IFlatReport Members

        public void SetSource(DataTable source) {
            _dataTable = source;
        }

        public void ShowReport() {
            gridControl1.DataSource = _dataTable;

            for (int lI = 0; lI < bandedGridView1.Columns.Count; lI++) {
                GridColumn lColumn = bandedGridView1.Columns[lI];
                if (null == lColumn)
                    continue;
                lColumn.MinWidth = lColumn.Width;
                lColumn.MaxWidth = lColumn.Width;
            }
        }

        #endregion
    }
}