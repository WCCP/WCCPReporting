﻿namespace WccpReporting.POCMappingData.UserControls
{
    partial class FlatDataReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.vbandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.vbandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.vbandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.vbandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.vbandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.mbandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.mbandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.mbandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.mbandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.mbandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn46 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn47 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn48 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn50 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn52 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn53 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn54 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn55 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn56 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn58 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn60 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn59 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn49 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1093, 463);
            this.gridControl1.TabIndex = 27;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand9});
            this.bandedGridView1.ColumnPanelRowHeight = 60;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn14,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn35,
            this.bandedGridColumn18,
            this.bandedGridColumn19,
            this.bandedGridColumn20,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.vbandedGridColumn26,
            this.vbandedGridColumn27,
            this.vbandedGridColumn28,
            this.vbandedGridColumn29,
            this.vbandedGridColumn31,
            this.mbandedGridColumn32,
            this.mbandedGridColumn33,
            this.mbandedGridColumn34,
            this.mbandedGridColumn35,
            this.mbandedGridColumn37,
            this.bandedGridColumn38,
            this.bandedGridColumn39,
            this.bandedGridColumn40,
            this.bandedGridColumn41,
            this.bandedGridColumn44,
            this.bandedGridColumn45,
            this.bandedGridColumn46,
            this.bandedGridColumn47,
            this.bandedGridColumn48,
            this.bandedGridColumn50,
            this.bandedGridColumn52,
            this.bandedGridColumn53,
            this.bandedGridColumn54,
            this.bandedGridColumn55,
            this.bandedGridColumn56,
            this.bandedGridColumn58,
            this.bandedGridColumn59,
            this.bandedGridColumn60,
            this.bandedGridColumn23,
            this.bandedGridColumn26,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn31,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn34,
            this.bandedGridColumn24,
            this.bandedGridColumn27,
            this.bandedGridColumn36,
            this.bandedGridColumn37,
            this.bandedGridColumn42,
            this.bandedGridColumn43,
            this.bandedGridColumn49});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.Editable = false;
            this.bandedGridView1.OptionsFilter.MRUFilterListPopupCount = 70;
            this.bandedGridView1.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.bandedGridView1.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.bandedGridView1.OptionsSelection.MultiSelect = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.RowAutoHeight = true;
            this.bandedGridView1.OptionsView.ShowDetailButtons = false;
            this.bandedGridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.OptionsView.ShowIndicator = false;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn1.Caption = "Регион";
            this.bandedGridColumn1.FieldName = "Region_name";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.Visible = true;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn2.Caption = "M4";
            this.bandedGridColumn2.FieldName = "M4";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.Visible = true;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn3.Caption = "M3";
            this.bandedGridColumn3.FieldName = "M3";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.Visible = true;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn4.Caption = "M2";
            this.bandedGridColumn4.FieldName = "M2";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.Visible = true;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn5.Caption = "M1";
            this.bandedGridColumn5.FieldName = "M1";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.Visible = true;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn6.Caption = "Код ТТ";
            this.bandedGridColumn6.FieldName = "Ol_id";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.Visible = true;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn7.Caption = "Юр. имя";
            this.bandedGridColumn7.FieldName = "OLName";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn7.Visible = true;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn8.Caption = "Факт. имя";
            this.bandedGridColumn8.FieldName = "OLTradingName";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn8.Visible = true;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn9.Caption = "Юр. адрес";
            this.bandedGridColumn9.FieldName = "OLAddress";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn9.Visible = true;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn10.Caption = "Факт. адрес";
            this.bandedGridColumn10.FieldName = "OLDeliveryAddress";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn10.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn11.Caption = "Контактное лицо";
            this.bandedGridColumn11.FieldName = "OLDirector";
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn11.Visible = true;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn12.Caption = "Телефон";
            this.bandedGridColumn12.FieldName = "OLTelephone";
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn12.Visible = true;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn13.Caption = "Размер склада";
            this.bandedGridColumn13.FieldName = "OLWHSize";
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn13.Visible = true;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn14.Caption = "Населенный пункт";
            this.bandedGridColumn14.FieldName = "City_name";
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn14.Visible = true;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn15.Caption = "Urban/Rural";
            this.bandedGridColumn15.FieldName = "Settlement";
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn16.Caption = "Принадлежность к сети";
            this.bandedGridColumn16.FieldName = "OlNetwork";
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn17.Caption = "Канал ТТ";
            this.bandedGridColumn17.FieldName = "OlChannel";
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn35.Caption = "Канал персонала М3";
            this.bandedGridColumn35.FieldName = "Channel_Type";
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            this.bandedGridColumn35.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn35.Visible = true;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn18.Caption = "Тип ТТ";
            this.bandedGridColumn18.FieldName = "OLtype_name";
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn18.Visible = true;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn19.Caption = "Подтип ТТ";
            this.bandedGridColumn19.FieldName = "OLSubTypeName";
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn20.Caption = "Дистрибутор";
            this.bandedGridColumn20.FieldName = "DISTR_NAME";
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn20.Visible = true;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn21.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn21.Caption = "ТС";
            this.bandedGridColumn21.FieldName = "Cust_NAME";
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn21.Visible = true;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn22.Caption = "Маршрут/Владелец";
            this.bandedGridColumn22.FieldName = "OlOwner";
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn22.Visible = true;
            // 
            // vbandedGridColumn26
            // 
            this.vbandedGridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.vbandedGridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.vbandedGridColumn26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vbandedGridColumn26.Caption = "ИнБев";
            this.vbandedGridColumn26.DisplayFormat.FormatString = "{0:N2}";
            this.vbandedGridColumn26.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.vbandedGridColumn26.FieldName = "InBev Sales";
            this.vbandedGridColumn26.Name = "vbandedGridColumn26";
            this.vbandedGridColumn26.OptionsColumn.AllowEdit = false;
            this.vbandedGridColumn26.Visible = true;
            // 
            // vbandedGridColumn27
            // 
            this.vbandedGridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.vbandedGridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.vbandedGridColumn27.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vbandedGridColumn27.Caption = "Оболонь";
            this.vbandedGridColumn27.DisplayFormat.FormatString = "{0:N2}";
            this.vbandedGridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.vbandedGridColumn27.FieldName = "Obolon Sales";
            this.vbandedGridColumn27.Name = "vbandedGridColumn27";
            this.vbandedGridColumn27.OptionsColumn.AllowEdit = false;
            this.vbandedGridColumn27.Visible = true;
            // 
            // vbandedGridColumn28
            // 
            this.vbandedGridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.vbandedGridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.vbandedGridColumn28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vbandedGridColumn28.Caption = "Carlsberg (BBH)";
            this.vbandedGridColumn28.DisplayFormat.FormatString = "{0:N2}";
            this.vbandedGridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.vbandedGridColumn28.FieldName = "BBH Sales";
            this.vbandedGridColumn28.Name = "vbandedGridColumn28";
            this.vbandedGridColumn28.OptionsColumn.AllowEdit = false;
            this.vbandedGridColumn28.Visible = true;
            // 
            // vbandedGridColumn29
            // 
            this.vbandedGridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.vbandedGridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.vbandedGridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vbandedGridColumn29.Caption = "ППБ";
            this.vbandedGridColumn29.DisplayFormat.FormatString = "{0:N2}";
            this.vbandedGridColumn29.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.vbandedGridColumn29.FieldName = "PPB Sales";
            this.vbandedGridColumn29.Name = "vbandedGridColumn29";
            this.vbandedGridColumn29.OptionsColumn.AllowEdit = false;
            this.vbandedGridColumn29.Visible = true;
            // 
            // vbandedGridColumn31
            // 
            this.vbandedGridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.vbandedGridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.vbandedGridColumn31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vbandedGridColumn31.Caption = "Других производителей";
            this.vbandedGridColumn31.DisplayFormat.FormatString = "{0:N2}";
            this.vbandedGridColumn31.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.vbandedGridColumn31.FieldName = "Other Sales";
            this.vbandedGridColumn31.Name = "vbandedGridColumn31";
            this.vbandedGridColumn31.OptionsColumn.AllowEdit = false;
            this.vbandedGridColumn31.Visible = true;
            // 
            // mbandedGridColumn32
            // 
            this.mbandedGridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.mbandedGridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.mbandedGridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.mbandedGridColumn32.Caption = "ИнБев";
            this.mbandedGridColumn32.DisplayFormat.FormatString = "{0:N2} %";
            this.mbandedGridColumn32.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.mbandedGridColumn32.FieldName = "mbandedGridColumn32";
            this.mbandedGridColumn32.Name = "mbandedGridColumn32";
            this.mbandedGridColumn32.OptionsColumn.AllowEdit = false;
            this.mbandedGridColumn32.UnboundExpression = "Iif( ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales]) != 0,[I" +
    "nBev Sales] / ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales" +
    "]) * 100,0)";
            this.mbandedGridColumn32.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.mbandedGridColumn32.Visible = true;
            // 
            // mbandedGridColumn33
            // 
            this.mbandedGridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.mbandedGridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.mbandedGridColumn33.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.mbandedGridColumn33.Caption = "Оболонь";
            this.mbandedGridColumn33.DisplayFormat.FormatString = "{0:N2} %";
            this.mbandedGridColumn33.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.mbandedGridColumn33.FieldName = "mbandedGridColumn33";
            this.mbandedGridColumn33.Name = "mbandedGridColumn33";
            this.mbandedGridColumn33.OptionsColumn.AllowEdit = false;
            this.mbandedGridColumn33.UnboundExpression = "Iif( ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales]) != 0,[O" +
    "bolon Sales] / ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sale" +
    "s]) * 100,0)";
            this.mbandedGridColumn33.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.mbandedGridColumn33.Visible = true;
            // 
            // mbandedGridColumn34
            // 
            this.mbandedGridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.mbandedGridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.mbandedGridColumn34.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.mbandedGridColumn34.Caption = "Carlsberg (BBH)";
            this.mbandedGridColumn34.DisplayFormat.FormatString = "{0:N2} %";
            this.mbandedGridColumn34.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.mbandedGridColumn34.FieldName = "mbandedGridColumn34";
            this.mbandedGridColumn34.Name = "mbandedGridColumn34";
            this.mbandedGridColumn34.OptionsColumn.AllowEdit = false;
            this.mbandedGridColumn34.UnboundExpression = "Iif( ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales]) != 0,[B" +
    "BH Sales] / ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales])" +
    "*100,0)";
            this.mbandedGridColumn34.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.mbandedGridColumn34.Visible = true;
            // 
            // mbandedGridColumn35
            // 
            this.mbandedGridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.mbandedGridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.mbandedGridColumn35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.mbandedGridColumn35.Caption = "ППБ";
            this.mbandedGridColumn35.DisplayFormat.FormatString = "{0:N2} %";
            this.mbandedGridColumn35.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.mbandedGridColumn35.FieldName = "mbandedGridColumn35";
            this.mbandedGridColumn35.Name = "mbandedGridColumn35";
            this.mbandedGridColumn35.OptionsColumn.AllowEdit = false;
            this.mbandedGridColumn35.UnboundExpression = "Iif( ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales]) != 0,[P" +
    "PB Sales] / ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales])" +
    " * 100,0)";
            this.mbandedGridColumn35.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.mbandedGridColumn35.Visible = true;
            // 
            // mbandedGridColumn37
            // 
            this.mbandedGridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.mbandedGridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.mbandedGridColumn37.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.mbandedGridColumn37.Caption = "Других производителей";
            this.mbandedGridColumn37.DisplayFormat.FormatString = "{0:N2} %";
            this.mbandedGridColumn37.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.mbandedGridColumn37.FieldName = "mbandedGridColumn37";
            this.mbandedGridColumn37.Name = "mbandedGridColumn37";
            this.mbandedGridColumn37.OptionsColumn.AllowEdit = false;
            this.mbandedGridColumn37.UnboundExpression = "Iif( ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales]) != 0,[O" +
    "ther Sales] / ([InBev Sales]+[Obolon Sales]+[BBH Sales]+[PPB Sales]+[Other Sales" +
    "]) * 100,0)";
            this.mbandedGridColumn37.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.mbandedGridColumn37.Visible = true;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn38.Caption = "ИнБев";
            this.bandedGridColumn38.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn38.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn38.FieldName = "InBev Contract";
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn38.Visible = true;
            // 
            // bandedGridColumn39
            // 
            this.bandedGridColumn39.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn39.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn39.Caption = "Оболонь";
            this.bandedGridColumn39.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn39.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn39.FieldName = "Obolon Contract";
            this.bandedGridColumn39.Name = "bandedGridColumn39";
            this.bandedGridColumn39.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn39.Visible = true;
            // 
            // bandedGridColumn40
            // 
            this.bandedGridColumn40.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn40.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn40.Caption = "Carlsberg (BBH)";
            this.bandedGridColumn40.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn40.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn40.FieldName = "BBH Contract";
            this.bandedGridColumn40.Name = "bandedGridColumn40";
            this.bandedGridColumn40.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn40.Visible = true;
            // 
            // bandedGridColumn41
            // 
            this.bandedGridColumn41.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn41.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn41.Caption = "ППБ";
            this.bandedGridColumn41.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn41.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn41.FieldName = "PPB Contract";
            this.bandedGridColumn41.Name = "bandedGridColumn41";
            this.bandedGridColumn41.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn41.Visible = true;
            // 
            // bandedGridColumn44
            // 
            this.bandedGridColumn44.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn44.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn44.Caption = "Продажи бутылочного пива конкурентов";
            this.bandedGridColumn44.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn44.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn44.FieldName = "BotOther";
            this.bandedGridColumn44.Name = "bandedGridColumn44";
            this.bandedGridColumn44.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn44.Visible = true;
            // 
            // bandedGridColumn45
            // 
            this.bandedGridColumn45.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn45.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn45.Caption = "ИнБев";
            this.bandedGridColumn45.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn45.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn45.FieldName = "InBev Exclusivity";
            this.bandedGridColumn45.Name = "bandedGridColumn45";
            this.bandedGridColumn45.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn45.Visible = true;
            // 
            // bandedGridColumn46
            // 
            this.bandedGridColumn46.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn46.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn46.Caption = "Оболонь";
            this.bandedGridColumn46.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn46.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn46.FieldName = "Obolon Exclusivity";
            this.bandedGridColumn46.Name = "bandedGridColumn46";
            this.bandedGridColumn46.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn46.Visible = true;
            // 
            // bandedGridColumn47
            // 
            this.bandedGridColumn47.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn47.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn47.Caption = "Carlsberg (BBH)";
            this.bandedGridColumn47.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn47.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn47.FieldName = "BBH Exclusivity";
            this.bandedGridColumn47.Name = "bandedGridColumn47";
            this.bandedGridColumn47.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn47.Visible = true;
            // 
            // bandedGridColumn48
            // 
            this.bandedGridColumn48.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn48.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn48.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn48.Caption = "ППБ";
            this.bandedGridColumn48.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn48.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn48.FieldName = "PPB Exclusivity";
            this.bandedGridColumn48.Name = "bandedGridColumn48";
            this.bandedGridColumn48.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn48.Visible = true;
            // 
            // bandedGridColumn50
            // 
            this.bandedGridColumn50.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn50.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn50.Caption = "Других производителей";
            this.bandedGridColumn50.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn50.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn50.FieldName = "Other Exclusivity";
            this.bandedGridColumn50.Name = "bandedGridColumn50";
            this.bandedGridColumn50.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn50.Visible = true;
            // 
            // bandedGridColumn52
            // 
            this.bandedGridColumn52.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn52.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn52.Caption = "Продажи кваса конкурентов";
            this.bandedGridColumn52.DisplayFormat.FormatString = "{0:N2}";
            this.bandedGridColumn52.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn52.FieldName = "Other Kvass";
            this.bandedGridColumn52.Name = "bandedGridColumn52";
            this.bandedGridColumn52.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn52.Visible = true;
            // 
            // bandedGridColumn53
            // 
            this.bandedGridColumn53.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn53.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn53.Caption = "ИнБев";
            this.bandedGridColumn53.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn53.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn53.FieldName = "InBev Summer Platform";
            this.bandedGridColumn53.Name = "bandedGridColumn53";
            this.bandedGridColumn53.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn53.Visible = true;
            // 
            // bandedGridColumn54
            // 
            this.bandedGridColumn54.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn54.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn54.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn54.Caption = "Оболонь";
            this.bandedGridColumn54.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn54.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn54.FieldName = "Obolon Summer Platform";
            this.bandedGridColumn54.Name = "bandedGridColumn54";
            this.bandedGridColumn54.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn54.Visible = true;
            // 
            // bandedGridColumn55
            // 
            this.bandedGridColumn55.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn55.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn55.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn55.Caption = "Carlsberg (BBH)";
            this.bandedGridColumn55.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn55.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn55.FieldName = "BBH Summer Platform";
            this.bandedGridColumn55.Name = "bandedGridColumn55";
            this.bandedGridColumn55.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn55.Visible = true;
            // 
            // bandedGridColumn56
            // 
            this.bandedGridColumn56.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn56.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn56.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn56.Caption = "ППБ";
            this.bandedGridColumn56.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn56.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn56.FieldName = "PPB Summer Platform";
            this.bandedGridColumn56.Name = "bandedGridColumn56";
            this.bandedGridColumn56.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn56.Visible = true;
            // 
            // bandedGridColumn58
            // 
            this.bandedGridColumn58.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn58.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn58.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn58.Caption = "Других производителей";
            this.bandedGridColumn58.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn58.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn58.FieldName = "Other Summer Platform";
            this.bandedGridColumn58.Name = "bandedGridColumn58";
            this.bandedGridColumn58.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn58.Visible = true;
            // 
            // bandedGridColumn60
            // 
            this.bandedGridColumn60.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn60.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn60.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn60.Caption = "Охладитель InBev";
            this.bandedGridColumn60.FieldName = "POS";
            this.bandedGridColumn60.Name = "bandedGridColumn60";
            this.bandedGridColumn60.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn60.Visible = true;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn28.Caption = "ХШ ИнБев 1-дверный";
            this.bandedGridColumn28.FieldName = "POS Inbev Small";
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn28.Visible = true;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn29.Caption = "ХШ ИнБев 2-дверный";
            this.bandedGridColumn29.FieldName = "POS Inbev Medium";
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn29.Visible = true;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn30.Caption = "ХШ ИнБев 3-дверный";
            this.bandedGridColumn30.FieldName = "POS Inbev Large";
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn30.Visible = true;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn26.Caption = "Уличные ХШ ИнБев";
            this.bandedGridColumn26.DisplayFormat.FormatString = "{0:N0}";
            this.bandedGridColumn26.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn26.FieldName = "POS Inbev Outdoor";
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Visible = true;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn31.Caption = "Наличие лицензии на алкоголь";
            this.bandedGridColumn31.FieldName = "AlcoLicense";
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn31.Visible = true;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn32.Caption = "Наличие в ТТ крепких сортов алкоголя";
            this.bandedGridColumn32.FieldName = "AlcoStrong";
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn32.Visible = true;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn33.Caption = "Volume potential";
            this.bandedGridColumn33.FieldName = "VolumePotential";
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn33.Visible = true;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn34.Caption = "Influencer potential (On)";
            this.bandedGridColumn34.FieldName = "InfluencerPotential";
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn34.Visible = true;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn23.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn23.Caption = "Доля Премиум +";
            this.bandedGridColumn23.DisplayFormat.FormatString = "{0:N2} %";
            this.bandedGridColumn23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn23.FieldName = "bandedGridColumn23";
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.UnboundExpression = "Iif( [InBev Sales] != 0, [Sales Premium] / [InBev Sales] * 100,0)";
            this.bandedGridColumn23.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumn23.Visible = true;
            this.bandedGridColumn23.Width = 66;
            // 
            // bandedGridColumn59
            // 
            this.bandedGridColumn59.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn59.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn59.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn59.Caption = "Категория ТТ";
            this.bandedGridColumn59.FieldName = "Cathegorization";
            this.bandedGridColumn59.Name = "bandedGridColumn59";
            this.bandedGridColumn59.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn59.Visible = true;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn24.Caption = "Кол-во позиций крафтового пива";
            this.bandedGridColumn24.FieldName = "craftBeer";
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn24.Visible = true;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn27.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn27.Caption = "Продажи Сидра конкурентов (ДАЛ)";
            this.bandedGridColumn27.FieldName = "Sidr";
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn27.Visible = true;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn36.Caption = "Кол-во позиций вина";
            this.bandedGridColumn36.FieldName = "countVine";
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn36.Visible = true;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn37.Caption = "Кол-во позиций алкогольных коктейлей";
            this.bandedGridColumn37.FieldName = "countCoctail";
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn37.Visible = true;
            // 
            // bandedGridColumn42
            // 
            this.bandedGridColumn42.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn42.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn42.Caption = "Кол-во позиций водки";
            this.bandedGridColumn42.FieldName = "countVodka";
            this.bandedGridColumn42.Name = "bandedGridColumn42";
            this.bandedGridColumn42.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn42.Visible = true;
            // 
            // bandedGridColumn43
            // 
            this.bandedGridColumn43.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn43.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn43.Caption = "Кол-во позиций другого крепкого алкоголя (виски, коньяк)";
            this.bandedGridColumn43.FieldName = "otherStrong";
            this.bandedGridColumn43.Name = "bandedGridColumn43";
            this.bandedGridColumn43.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn43.Visible = true;
            // 
            // bandedGridColumn49
            // 
            this.bandedGridColumn49.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn49.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn49.Caption = "Наличие игр (бильярд, дартс, настольный футбол)";
            this.bandedGridColumn49.FieldName = "isExistGame";
            this.bandedGridColumn49.Name = "bandedGridColumn49";
            this.bandedGridColumn49.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn49.Visible = true;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.Columns.Add(this.bandedGridColumn3);
            this.gridBand1.Columns.Add(this.bandedGridColumn4);
            this.gridBand1.Columns.Add(this.bandedGridColumn5);
            this.gridBand1.Columns.Add(this.bandedGridColumn6);
            this.gridBand1.Columns.Add(this.bandedGridColumn7);
            this.gridBand1.Columns.Add(this.bandedGridColumn8);
            this.gridBand1.Columns.Add(this.bandedGridColumn9);
            this.gridBand1.Columns.Add(this.bandedGridColumn10);
            this.gridBand1.Columns.Add(this.bandedGridColumn11);
            this.gridBand1.Columns.Add(this.bandedGridColumn12);
            this.gridBand1.Columns.Add(this.bandedGridColumn13);
            this.gridBand1.Columns.Add(this.bandedGridColumn14);
            this.gridBand1.Columns.Add(this.bandedGridColumn15);
            this.gridBand1.Columns.Add(this.bandedGridColumn16);
            this.gridBand1.Columns.Add(this.bandedGridColumn17);
            this.gridBand1.Columns.Add(this.bandedGridColumn35);
            this.gridBand1.Columns.Add(this.bandedGridColumn18);
            this.gridBand1.Columns.Add(this.bandedGridColumn19);
            this.gridBand1.Columns.Add(this.bandedGridColumn20);
            this.gridBand1.Columns.Add(this.bandedGridColumn21);
            this.gridBand1.Columns.Add(this.bandedGridColumn22);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 1725;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Объем продажи пива всего, дал";
            this.gridBand2.Columns.Add(this.vbandedGridColumn26);
            this.gridBand2.Columns.Add(this.vbandedGridColumn27);
            this.gridBand2.Columns.Add(this.vbandedGridColumn28);
            this.gridBand2.Columns.Add(this.vbandedGridColumn29);
            this.gridBand2.Columns.Add(this.vbandedGridColumn31);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 375;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Market Share";
            this.gridBand3.Columns.Add(this.mbandedGridColumn32);
            this.gridBand3.Columns.Add(this.mbandedGridColumn33);
            this.gridBand3.Columns.Add(this.mbandedGridColumn34);
            this.gridBand3.Columns.Add(this.mbandedGridColumn35);
            this.gridBand3.Columns.Add(this.mbandedGridColumn37);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 375;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Наличие контракта";
            this.gridBand4.Columns.Add(this.bandedGridColumn38);
            this.gridBand4.Columns.Add(this.bandedGridColumn39);
            this.gridBand4.Columns.Add(this.bandedGridColumn40);
            this.gridBand4.Columns.Add(this.bandedGridColumn41);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 300;
            // 
            // gridBand5
            // 
            this.gridBand5.Columns.Add(this.bandedGridColumn44);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 75;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Эксклюзив";
            this.gridBand6.Columns.Add(this.bandedGridColumn45);
            this.gridBand6.Columns.Add(this.bandedGridColumn46);
            this.gridBand6.Columns.Add(this.bandedGridColumn47);
            this.gridBand6.Columns.Add(this.bandedGridColumn48);
            this.gridBand6.Columns.Add(this.bandedGridColumn50);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 375;
            // 
            // gridBand7
            // 
            this.gridBand7.Columns.Add(this.bandedGridColumn52);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 6;
            this.gridBand7.Width = 75;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Наличие раб. летней площадки";
            this.gridBand8.Columns.Add(this.bandedGridColumn53);
            this.gridBand8.Columns.Add(this.bandedGridColumn54);
            this.gridBand8.Columns.Add(this.bandedGridColumn55);
            this.gridBand8.Columns.Add(this.bandedGridColumn56);
            this.gridBand8.Columns.Add(this.bandedGridColumn58);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 7;
            this.gridBand8.Width = 375;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "ХО";
            this.gridBand10.Columns.Add(this.bandedGridColumn60);
            this.gridBand10.Columns.Add(this.bandedGridColumn28);
            this.gridBand10.Columns.Add(this.bandedGridColumn29);
            this.gridBand10.Columns.Add(this.bandedGridColumn30);
            this.gridBand10.Columns.Add(this.bandedGridColumn26);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 8;
            this.gridBand10.Width = 375;
            // 
            // gridBand11
            // 
            this.gridBand11.Columns.Add(this.bandedGridColumn31);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 9;
            this.gridBand11.Width = 75;
            // 
            // gridBand12
            // 
            this.gridBand12.Columns.Add(this.bandedGridColumn32);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 10;
            this.gridBand12.Width = 75;
            // 
            // gridBand13
            // 
            this.gridBand13.Columns.Add(this.bandedGridColumn24);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 11;
            this.gridBand13.Width = 75;
            // 
            // gridBand14
            // 
            this.gridBand14.Columns.Add(this.bandedGridColumn27);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 12;
            this.gridBand14.Width = 75;
            // 
            // gridBand15
            // 
            this.gridBand15.Columns.Add(this.bandedGridColumn36);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 13;
            this.gridBand15.Width = 75;
            // 
            // gridBand16
            // 
            this.gridBand16.Columns.Add(this.bandedGridColumn37);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 14;
            this.gridBand16.Width = 75;
            // 
            // gridBand17
            // 
            this.gridBand17.Columns.Add(this.bandedGridColumn42);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 15;
            this.gridBand17.Width = 75;
            // 
            // gridBand18
            // 
            this.gridBand18.Columns.Add(this.bandedGridColumn43);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 16;
            this.gridBand18.Width = 75;
            // 
            // gridBand19
            // 
            this.gridBand19.Columns.Add(this.bandedGridColumn49);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 17;
            this.gridBand19.Width = 75;
            // 
            // gridBand9
            // 
            this.gridBand9.Columns.Add(this.bandedGridColumn33);
            this.gridBand9.Columns.Add(this.bandedGridColumn34);
            this.gridBand9.Columns.Add(this.bandedGridColumn23);
            this.gridBand9.Columns.Add(this.bandedGridColumn59);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 18;
            this.gridBand9.Width = 291;
            // 
            // FlatDataReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "FlatDataReport";
            this.Size = new System.Drawing.Size(1093, 463);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn vbandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn vbandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn vbandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn vbandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn vbandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn mbandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn mbandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn mbandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn mbandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn mbandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn41;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn45;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn46;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn47;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn48;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn50;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn52;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn53;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn54;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn55;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn56;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn58;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn59;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn60;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn43;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn49;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
    }
}
