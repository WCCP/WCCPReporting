namespace WccpReporting.POCMappingData.UserControls
{
    partial class DataReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldM4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSales = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMarketShare = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAllPos = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPosSmall = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPosMedium = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPosLarge = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCompetitorsPosOther1Door = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCompetitorsPosOther2Door = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAlcoLicense = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAlcoStrong = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldVolumePotential = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldInfluencerPotential = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPOSInbevOutdoor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCooler = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldEksklyuzivCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTents = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldBaseNew = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSync = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPocId = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTradingName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDeliveryAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDirector = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPhone = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldStorageSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSubType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPocChanel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldStaffChannel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDistributor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTS = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldRouteOwner = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCategory = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldLocation = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldUrbanRural = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPremium = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldNetworkName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIdEdrpou = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSalesKvYarulo = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOffEksklyuzivBeer = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOff7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOn6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOn7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOn8 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOn9 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOn10 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldBrand = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTypeTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTradingNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridFieldM1
            // 
            this.pivotGridFieldM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM1.AreaIndex = 3;
            this.pivotGridFieldM1.Caption = "M1";
            this.pivotGridFieldM1.EmptyCellText = "0";
            this.pivotGridFieldM1.FieldName = "M1";
            this.pivotGridFieldM1.Name = "pivotGridFieldM1";
            // 
            // pivotGridFieldM2
            // 
            this.pivotGridFieldM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM2.AreaIndex = 2;
            this.pivotGridFieldM2.Caption = "M2";
            this.pivotGridFieldM2.EmptyCellText = "0";
            this.pivotGridFieldM2.FieldName = "M2";
            this.pivotGridFieldM2.Name = "pivotGridFieldM2";
            // 
            // pivotGridFieldM3
            // 
            this.pivotGridFieldM3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM3.AreaIndex = 1;
            this.pivotGridFieldM3.Caption = "M3";
            this.pivotGridFieldM3.EmptyCellText = "0";
            this.pivotGridFieldM3.FieldName = "M3";
            this.pivotGridFieldM3.Name = "pivotGridFieldM3";
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldM1,
            this.pivotGridFieldM2,
            this.pivotGridFieldM3,
            this.pivotGridFieldM4,
            this.pivotGridFieldCount,
            this.pivotGridFieldSales,
            this.pivotGridFieldMarketShare,
            this.pivotGridFieldAllPos,
            this.pivotGridFieldPosSmall,
            this.pivotGridFieldPosMedium,
            this.pivotGridFieldPosLarge,
            this.pivotGridFieldCompetitorsPosOther1Door,
            this.pivotGridFieldCompetitorsPosOther2Door,
            this.pivotGridFieldAlcoLicense,
            this.pivotGridFieldAlcoStrong,
            this.pivotGridFieldVolumePotential,
            this.pivotGridFieldInfluencerPotential,
            this.pivotGridFieldPOSInbevOutdoor,
            this.pivotGridFieldCooler,
            this.pivotGridFieldEksklyuzivCount,
            this.pivotGridFieldTents,
            this.pivotGridFieldBaseNew,
            this.pivotGridFieldRegion,
            this.pivotGridFieldSync,
            this.pivotGridFieldPocId,
            this.pivotGridFieldName,
            this.pivotGridFieldTradingName,
            this.pivotGridFieldDeliveryAddress,
            this.pivotGridFieldAddress,
            this.pivotGridFieldDirector,
            this.pivotGridFieldPhone,
            this.pivotGridFieldStorageSize,
            this.pivotGridFieldType,
            this.pivotGridFieldSubType,
            this.pivotGridFieldPocChanel,
            this.pivotGridFieldStaffChannel,
            this.pivotGridFieldDistributor,
            this.pivotGridFieldTS,
            this.pivotGridFieldRouteOwner,
            this.pivotGridFieldCategory,
            this.pivotGridFieldLocation,
            this.pivotGridFieldUrbanRural,
            this.pivotGridFieldPremium,
            this.pivotGridField4,
            this.pivotGridFieldNetworkName,
            this.pivotGridFieldIdEdrpou,
            this.pivotGridFieldSalesKvYarulo,
            this.pivotGridFieldOffEksklyuzivBeer,
            this.pivotGridFieldOff7,
            this.pivotGridFieldOn6,
            this.pivotGridFieldOn7,
            this.pivotGridFieldOn8,
            this.pivotGridFieldOn9,
            this.pivotGridFieldOn10,
            this.pivotGridFieldBrand,
            this.pivotGridField5,
            this.pivotGridField6,
            this.pivotGridField7});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.RowArea;
            this.pivotGridControl.OptionsDataField.AreaIndex = 4;
            this.pivotGridControl.OptionsDataField.RowHeaderWidth = 170;
            this.pivotGridControl.Size = new System.Drawing.Size(1436, 800);
            this.pivotGridControl.TabIndex = 0;
            this.pivotGridControl.FieldAreaChanging += new DevExpress.XtraPivotGrid.PivotAreaChangingEventHandler(this.pivotGridControl_FieldAreaChanging);
            this.pivotGridControl.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridControl_CustomSummary);
            this.pivotGridControl.CustomFieldSort += new DevExpress.XtraPivotGrid.PivotGridCustomFieldSortEventHandler(this.pivotGridControl_CustomFieldSort);
            this.pivotGridControl.FieldAreaChanged += new DevExpress.XtraPivotGrid.PivotFieldEventHandler(this.pivotGridControl_FieldAreaChanged);
            this.pivotGridControl.CustomCellDisplayText += new DevExpress.XtraPivotGrid.PivotCellDisplayTextEventHandler(this.pivotGridControl_CustomCellDisplayText);
            // 
            // pivotGridFieldM4
            // 
            this.pivotGridFieldM4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM4.AreaIndex = 0;
            this.pivotGridFieldM4.Caption = "M4";
            this.pivotGridFieldM4.EmptyCellText = "0";
            this.pivotGridFieldM4.FieldName = "M4";
            this.pivotGridFieldM4.Name = "pivotGridFieldM4";
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "���-�� ��";
            this.pivotGridFieldCount.EmptyCellText = "0";
            this.pivotGridFieldCount.FieldName = "M1";
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldCount.Width = 300;
            // 
            // pivotGridFieldSales
            // 
            this.pivotGridFieldSales.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSales.AreaIndex = 1;
            this.pivotGridFieldSales.Caption = "Sales Volume";
            this.pivotGridFieldSales.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldSales.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSales.EmptyCellText = "0";
            this.pivotGridFieldSales.FieldName = "Sales";
            this.pivotGridFieldSales.Name = "pivotGridFieldSales";
            // 
            // pivotGridFieldMarketShare
            // 
            this.pivotGridFieldMarketShare.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldMarketShare.AreaIndex = 2;
            this.pivotGridFieldMarketShare.Caption = "Market Share";
            this.pivotGridFieldMarketShare.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldMarketShare.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldMarketShare.EmptyCellText = "0";
            this.pivotGridFieldMarketShare.Name = "pivotGridFieldMarketShare";
            this.pivotGridFieldMarketShare.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldAllPos
            // 
            this.pivotGridFieldAllPos.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldAllPos.AreaIndex = 3;
            this.pivotGridFieldAllPos.Caption = "��� ������������";
            this.pivotGridFieldAllPos.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldAllPos.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldAllPos.EmptyCellText = "0";
            this.pivotGridFieldAllPos.Name = "pivotGridFieldAllPos";
            this.pivotGridFieldAllPos.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldAllPos.UnboundFieldName = "pivotGridFieldAllPos";
            this.pivotGridFieldAllPos.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // pivotGridFieldPosSmall
            // 
            this.pivotGridFieldPosSmall.AreaIndex = 0;
            this.pivotGridFieldPosSmall.Caption = "�� ����� 1-�������";
            this.pivotGridFieldPosSmall.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldPosSmall.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldPosSmall.EmptyCellText = "0";
            this.pivotGridFieldPosSmall.FieldName = "POS Inbev Small";
            this.pivotGridFieldPosSmall.Name = "pivotGridFieldPosSmall";
            // 
            // pivotGridFieldPosMedium
            // 
            this.pivotGridFieldPosMedium.AreaIndex = 1;
            this.pivotGridFieldPosMedium.Caption = "�� ����� 2-�������";
            this.pivotGridFieldPosMedium.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldPosMedium.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldPosMedium.EmptyCellText = "0";
            this.pivotGridFieldPosMedium.FieldName = "POS Inbev Medium";
            this.pivotGridFieldPosMedium.Name = "pivotGridFieldPosMedium";
            // 
            // pivotGridFieldPosLarge
            // 
            this.pivotGridFieldPosLarge.AreaIndex = 2;
            this.pivotGridFieldPosLarge.Caption = "�� ����� 3-�������";
            this.pivotGridFieldPosLarge.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldPosLarge.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldPosLarge.EmptyCellText = "0";
            this.pivotGridFieldPosLarge.FieldName = "POS Inbev Large";
            this.pivotGridFieldPosLarge.Name = "pivotGridFieldPosLarge";
            // 
            // pivotGridFieldCompetitorsPosOther1Door
            // 
            this.pivotGridFieldCompetitorsPosOther1Door.AreaIndex = 3;
            this.pivotGridFieldCompetitorsPosOther1Door.Caption = "������������ ����������� 1-�������";
            this.pivotGridFieldCompetitorsPosOther1Door.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldCompetitorsPosOther1Door.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCompetitorsPosOther1Door.EmptyCellText = "0";
            this.pivotGridFieldCompetitorsPosOther1Door.FieldName = "POS Other One Door";
            this.pivotGridFieldCompetitorsPosOther1Door.Name = "pivotGridFieldCompetitorsPosOther1Door";
            // 
            // pivotGridFieldCompetitorsPosOther2Door
            // 
            this.pivotGridFieldCompetitorsPosOther2Door.AreaIndex = 4;
            this.pivotGridFieldCompetitorsPosOther2Door.Caption = "������������ ����������� 2-�������";
            this.pivotGridFieldCompetitorsPosOther2Door.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldCompetitorsPosOther2Door.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCompetitorsPosOther2Door.EmptyCellText = "0";
            this.pivotGridFieldCompetitorsPosOther2Door.FieldName = "POS Other Two Door";
            this.pivotGridFieldCompetitorsPosOther2Door.Name = "pivotGridFieldCompetitorsPosOther2Door";
            // 
            // pivotGridFieldAlcoLicense
            // 
            this.pivotGridFieldAlcoLicense.AreaIndex = 35;
            this.pivotGridFieldAlcoLicense.Caption = "������� �������� �� ��������";
            this.pivotGridFieldAlcoLicense.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldAlcoLicense.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldAlcoLicense.EmptyCellText = "0";
            this.pivotGridFieldAlcoLicense.FieldName = "AlcoLicense";
            this.pivotGridFieldAlcoLicense.Name = "pivotGridFieldAlcoLicense";
            // 
            // pivotGridFieldAlcoStrong
            // 
            this.pivotGridFieldAlcoStrong.AreaIndex = 36;
            this.pivotGridFieldAlcoStrong.Caption = "������� � �� ������� ������ ��������";
            this.pivotGridFieldAlcoStrong.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldAlcoStrong.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldAlcoStrong.EmptyCellText = "0";
            this.pivotGridFieldAlcoStrong.FieldName = "AlcoStrong";
            this.pivotGridFieldAlcoStrong.Name = "pivotGridFieldAlcoStrong";
            // 
            // pivotGridFieldVolumePotential
            // 
            this.pivotGridFieldVolumePotential.AreaIndex = 37;
            this.pivotGridFieldVolumePotential.Caption = "Volume potential";
            this.pivotGridFieldVolumePotential.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldVolumePotential.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldVolumePotential.EmptyCellText = "0";
            this.pivotGridFieldVolumePotential.FieldName = "VolumePotential";
            this.pivotGridFieldVolumePotential.Name = "pivotGridFieldVolumePotential";
            // 
            // pivotGridFieldInfluencerPotential
            // 
            this.pivotGridFieldInfluencerPotential.AreaIndex = 38;
            this.pivotGridFieldInfluencerPotential.Caption = "Influencer potential (On)";
            this.pivotGridFieldInfluencerPotential.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldInfluencerPotential.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldInfluencerPotential.EmptyCellText = "0";
            this.pivotGridFieldInfluencerPotential.FieldName = "InfluencerPotential";
            this.pivotGridFieldInfluencerPotential.Name = "pivotGridFieldInfluencerPotential";
            // 
            // pivotGridFieldPOSInbevOutdoor
            // 
            this.pivotGridFieldPOSInbevOutdoor.AreaIndex = 34;
            this.pivotGridFieldPOSInbevOutdoor.Caption = "������� �� �����";
            this.pivotGridFieldPOSInbevOutdoor.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldPOSInbevOutdoor.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldPOSInbevOutdoor.EmptyCellText = "0";
            this.pivotGridFieldPOSInbevOutdoor.FieldName = "POS Inbev Outdoor";
            this.pivotGridFieldPOSInbevOutdoor.Name = "pivotGridFieldPOSInbevOutdoor";
            // 
            // pivotGridFieldCooler
            // 
            this.pivotGridFieldCooler.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCooler.AreaIndex = 4;
            this.pivotGridFieldCooler.Caption = "����������";
            this.pivotGridFieldCooler.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldCooler.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCooler.EmptyCellText = "0";
            this.pivotGridFieldCooler.FieldName = "POS";
            this.pivotGridFieldCooler.Name = "pivotGridFieldCooler";
            // 
            // pivotGridFieldEksklyuzivCount
            // 
            this.pivotGridFieldEksklyuzivCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldEksklyuzivCount.AreaIndex = 5;
            this.pivotGridFieldEksklyuzivCount.Caption = "���-�� ������������ ��";
            this.pivotGridFieldEksklyuzivCount.CellFormat.FormatString = "{0:N0}";
            this.pivotGridFieldEksklyuzivCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldEksklyuzivCount.EmptyCellText = "0";
            this.pivotGridFieldEksklyuzivCount.FieldName = "Exclusivity";
            this.pivotGridFieldEksklyuzivCount.Name = "pivotGridFieldEksklyuzivCount";
            this.pivotGridFieldEksklyuzivCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldEksklyuzivCount.UnboundFieldName = "pivotGridFieldEksklyuziv";
            this.pivotGridFieldEksklyuzivCount.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // pivotGridFieldTents
            // 
            this.pivotGridFieldTents.AreaIndex = 5;
            this.pivotGridFieldTents.Caption = "�����";
            this.pivotGridFieldTents.EmptyCellText = "0";
            this.pivotGridFieldTents.FieldName = "InBev Penthouse";
            this.pivotGridFieldTents.Name = "pivotGridFieldTents";
            // 
            // pivotGridFieldBaseNew
            // 
            this.pivotGridFieldBaseNew.AreaIndex = 6;
            this.pivotGridFieldBaseNew.Caption = "����/�����";
            this.pivotGridFieldBaseNew.EmptyCellText = "0";
            this.pivotGridFieldBaseNew.FieldName = "StatusName";
            this.pivotGridFieldBaseNew.Name = "pivotGridFieldBaseNew";
            this.pivotGridFieldBaseNew.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // pivotGridFieldRegion
            // 
            this.pivotGridFieldRegion.AreaIndex = 7;
            this.pivotGridFieldRegion.Caption = "������";
            this.pivotGridFieldRegion.EmptyCellText = "0";
            this.pivotGridFieldRegion.FieldName = "Region_name";
            this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
            // 
            // pivotGridFieldSync
            // 
            this.pivotGridFieldSync.AreaIndex = 8;
            this.pivotGridFieldSync.Caption = "����� �����.";
            this.pivotGridFieldSync.EmptyCellText = "0";
            this.pivotGridFieldSync.FieldName = "Cust_NAME";
            this.pivotGridFieldSync.Name = "pivotGridFieldSync";
            // 
            // pivotGridFieldPocId
            // 
            this.pivotGridFieldPocId.AreaIndex = 9;
            this.pivotGridFieldPocId.Caption = "��� ��";
            this.pivotGridFieldPocId.EmptyCellText = "0";
            this.pivotGridFieldPocId.FieldName = "Ol_id";
            this.pivotGridFieldPocId.Name = "pivotGridFieldPocId";
            // 
            // pivotGridFieldName
            // 
            this.pivotGridFieldName.AreaIndex = 10;
            this.pivotGridFieldName.Caption = "��. ��� ��";
            this.pivotGridFieldName.EmptyCellText = "0";
            this.pivotGridFieldName.FieldName = "OLName";
            this.pivotGridFieldName.Name = "pivotGridFieldName";
            // 
            // pivotGridFieldTradingName
            // 
            this.pivotGridFieldTradingName.AreaIndex = 11;
            this.pivotGridFieldTradingName.Caption = "����. ��� ��";
            this.pivotGridFieldTradingName.EmptyCellText = "0";
            this.pivotGridFieldTradingName.FieldName = "OLTradingName";
            this.pivotGridFieldTradingName.Name = "pivotGridFieldTradingName";
            // 
            // pivotGridFieldDeliveryAddress
            // 
            this.pivotGridFieldDeliveryAddress.AreaIndex = 12;
            this.pivotGridFieldDeliveryAddress.Caption = "����. ����� ��";
            this.pivotGridFieldDeliveryAddress.EmptyCellText = "0";
            this.pivotGridFieldDeliveryAddress.FieldName = "OLDeliveryAddress";
            this.pivotGridFieldDeliveryAddress.Name = "pivotGridFieldDeliveryAddress";
            // 
            // pivotGridFieldAddress
            // 
            this.pivotGridFieldAddress.AreaIndex = 13;
            this.pivotGridFieldAddress.Caption = "��. ����� ��";
            this.pivotGridFieldAddress.EmptyCellText = "0";
            this.pivotGridFieldAddress.FieldName = "OLAddress";
            this.pivotGridFieldAddress.Name = "pivotGridFieldAddress";
            // 
            // pivotGridFieldDirector
            // 
            this.pivotGridFieldDirector.AreaIndex = 14;
            this.pivotGridFieldDirector.Caption = "���������� ����";
            this.pivotGridFieldDirector.EmptyCellText = "0";
            this.pivotGridFieldDirector.FieldName = "OLDirector";
            this.pivotGridFieldDirector.Name = "pivotGridFieldDirector";
            // 
            // pivotGridFieldPhone
            // 
            this.pivotGridFieldPhone.AreaIndex = 15;
            this.pivotGridFieldPhone.Caption = "���. ��";
            this.pivotGridFieldPhone.EmptyCellText = "0";
            this.pivotGridFieldPhone.FieldName = "OLTelephone";
            this.pivotGridFieldPhone.Name = "pivotGridFieldPhone";
            // 
            // pivotGridFieldStorageSize
            // 
            this.pivotGridFieldStorageSize.AreaIndex = 39;
            this.pivotGridFieldStorageSize.Caption = "������ ������";
            this.pivotGridFieldStorageSize.EmptyCellText = "0";
            this.pivotGridFieldStorageSize.FieldName = "OLWHSize";
            this.pivotGridFieldStorageSize.Name = "pivotGridFieldStorageSize";
            // 
            // pivotGridFieldType
            // 
            this.pivotGridFieldType.AreaIndex = 16;
            this.pivotGridFieldType.Caption = "��� ��";
            this.pivotGridFieldType.EmptyCellText = "0";
            this.pivotGridFieldType.FieldName = "OLtype_name";
            this.pivotGridFieldType.Name = "pivotGridFieldType";
            // 
            // pivotGridFieldSubType
            // 
            this.pivotGridFieldSubType.AreaIndex = 17;
            this.pivotGridFieldSubType.Caption = "������ ��";
            this.pivotGridFieldSubType.FieldName = "OLSubTypeName";
            this.pivotGridFieldSubType.Name = "pivotGridFieldSubType";
            // 
            // pivotGridFieldPocChanel
            // 
            this.pivotGridFieldPocChanel.AreaIndex = 18;
            this.pivotGridFieldPocChanel.Caption = "����� ��";
            this.pivotGridFieldPocChanel.EmptyCellText = "0";
            this.pivotGridFieldPocChanel.FieldName = "OlChannel";
            this.pivotGridFieldPocChanel.Name = "pivotGridFieldPocChanel";
            // 
            // pivotGridFieldStaffChannel
            // 
            this.pivotGridFieldStaffChannel.AreaIndex = 19;
            this.pivotGridFieldStaffChannel.Caption = "����� ��������� �3";
            this.pivotGridFieldStaffChannel.EmptyCellText = "0";
            this.pivotGridFieldStaffChannel.FieldName = "Channel_Type";
            this.pivotGridFieldStaffChannel.Name = "pivotGridFieldStaffChannel";
            // 
            // pivotGridFieldDistributor
            // 
            this.pivotGridFieldDistributor.AreaIndex = 20;
            this.pivotGridFieldDistributor.Caption = "������������";
            this.pivotGridFieldDistributor.EmptyCellText = "0";
            this.pivotGridFieldDistributor.FieldName = "DISTR_NAME";
            this.pivotGridFieldDistributor.Name = "pivotGridFieldDistributor";
            // 
            // pivotGridFieldTS
            // 
            this.pivotGridFieldTS.AreaIndex = 42;
            this.pivotGridFieldTS.Caption = "��";
            this.pivotGridFieldTS.FieldName = "Cust_NAME";
            this.pivotGridFieldTS.Name = "pivotGridFieldTS";
            // 
            // pivotGridFieldRouteOwner
            // 
            this.pivotGridFieldRouteOwner.AreaIndex = 41;
            this.pivotGridFieldRouteOwner.Caption = "�������/��������";
            this.pivotGridFieldRouteOwner.FieldName = "OlOwner";
            this.pivotGridFieldRouteOwner.Name = "pivotGridFieldRouteOwner";
            // 
            // pivotGridFieldCategory
            // 
            this.pivotGridFieldCategory.AreaIndex = 21;
            this.pivotGridFieldCategory.Caption = "��������� ��";
            this.pivotGridFieldCategory.EmptyCellText = "0";
            this.pivotGridFieldCategory.FieldName = "Cathegorization";
            this.pivotGridFieldCategory.Name = "pivotGridFieldCategory";
            // 
            // pivotGridFieldLocation
            // 
            this.pivotGridFieldLocation.AreaIndex = 22;
            this.pivotGridFieldLocation.Caption = "���. ����� ��";
            this.pivotGridFieldLocation.EmptyCellText = "0";
            this.pivotGridFieldLocation.FieldName = "City_name";
            this.pivotGridFieldLocation.Name = "pivotGridFieldLocation";
            // 
            // pivotGridFieldUrbanRural
            // 
            this.pivotGridFieldUrbanRural.AreaIndex = 40;
            this.pivotGridFieldUrbanRural.Caption = "Urban/Rural";
            this.pivotGridFieldUrbanRural.FieldName = "Settlement";
            this.pivotGridFieldUrbanRural.Name = "pivotGridFieldUrbanRural";
            // 
            // pivotGridFieldPremium
            // 
            this.pivotGridFieldPremium.AreaIndex = 23;
            this.pivotGridFieldPremium.Caption = "������������� ��";
            this.pivotGridFieldPremium.EmptyCellText = "0";
            this.pivotGridFieldPremium.FieldName = "fi5";
            this.pivotGridFieldPremium.Name = "pivotGridFieldPremium";
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.AreaIndex = 24;
            this.pivotGridField4.Caption = "������� ����";
            this.pivotGridField4.FieldName = "isGathered";
            this.pivotGridField4.Name = "pivotGridField4";
            // 
            // pivotGridFieldNetworkName
            // 
            this.pivotGridFieldNetworkName.AreaIndex = 25;
            this.pivotGridFieldNetworkName.Caption = "�������������� � ����";
            this.pivotGridFieldNetworkName.EmptyCellText = "0";
            this.pivotGridFieldNetworkName.FieldName = "OlNetwork";
            this.pivotGridFieldNetworkName.Name = "pivotGridFieldNetworkName";
            // 
            // pivotGridFieldIdEdrpou
            // 
            this.pivotGridFieldIdEdrpou.AreaIndex = 26;
            this.pivotGridFieldIdEdrpou.Caption = "��� ������";
            this.pivotGridFieldIdEdrpou.EmptyCellText = "0";
            this.pivotGridFieldIdEdrpou.FieldName = "ZKPO";
            this.pivotGridFieldIdEdrpou.Name = "pivotGridFieldIdEdrpou";
            // 
            // pivotGridFieldSalesKvYarulo
            // 
            this.pivotGridFieldSalesKvYarulo.AreaIndex = 27;
            this.pivotGridFieldSalesKvYarulo.Caption = "������� ����� �����";
            this.pivotGridFieldSalesKvYarulo.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldSalesKvYarulo.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSalesKvYarulo.EmptyCellText = "0";
            this.pivotGridFieldSalesKvYarulo.FieldName = "InBev Kvass";
            this.pivotGridFieldSalesKvYarulo.Name = "pivotGridFieldSalesKvYarulo";
            // 
            // pivotGridFieldOffEksklyuzivBeer
            // 
            this.pivotGridFieldOffEksklyuzivBeer.AreaIndex = 28;
            this.pivotGridFieldOffEksklyuzivBeer.Caption = "������� ����. ����. ���� �����������";
            this.pivotGridFieldOffEksklyuzivBeer.EmptyCellText = "0";
            this.pivotGridFieldOffEksklyuzivBeer.FieldName = "ExclusivityBeer";
            this.pivotGridFieldOffEksklyuzivBeer.Name = "pivotGridFieldOffEksklyuzivBeer";
            // 
            // pivotGridFieldOff7
            // 
            this.pivotGridFieldOff7.AreaIndex = 29;
            this.pivotGridFieldOff7.Caption = "��������� �����������";
            this.pivotGridFieldOff7.EmptyCellText = "0";
            this.pivotGridFieldOff7.FieldName = "Contract";
            this.pivotGridFieldOff7.Name = "pivotGridFieldOff7";
            // 
            // pivotGridFieldOn6
            // 
            this.pivotGridFieldOn6.AreaIndex = 30;
            this.pivotGridFieldOn6.Caption = "������� ���. ���� �����������";
            this.pivotGridFieldOn6.EmptyCellText = "0";
            this.pivotGridFieldOn6.FieldName = "BotOther";
            this.pivotGridFieldOn6.Name = "pivotGridFieldOn6";
            this.pivotGridFieldOn6.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldOn7
            // 
            this.pivotGridFieldOn7.AreaIndex = 27;
            this.pivotGridFieldOn7.Caption = "���� �������+";
            this.pivotGridFieldOn7.EmptyCellText = "0";
            this.pivotGridFieldOn7.FieldName = "���� �������+";
            this.pivotGridFieldOn7.Name = "pivotGridFieldOn7";
            this.pivotGridFieldOn7.Visible = false;
            // 
            // pivotGridFieldOn8
            // 
            this.pivotGridFieldOn8.AreaIndex = 31;
            this.pivotGridFieldOn8.Caption = "������� ����� �����������";
            this.pivotGridFieldOn8.CellFormat.FormatString = "{0:N2}";
            this.pivotGridFieldOn8.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldOn8.EmptyCellText = "0";
            this.pivotGridFieldOn8.FieldName = "Other Kvass";
            this.pivotGridFieldOn8.Name = "pivotGridFieldOn8";
            // 
            // pivotGridFieldOn9
            // 
            this.pivotGridFieldOn9.AreaIndex = 32;
            this.pivotGridFieldOn9.Caption = "������� ��������� ";
            this.pivotGridFieldOn9.EmptyCellText = "0";
            this.pivotGridFieldOn9.FieldName = "Contract";
            this.pivotGridFieldOn9.Name = "pivotGridFieldOn9";
            // 
            // pivotGridFieldOn10
            // 
            this.pivotGridFieldOn10.AreaIndex = 33;
            this.pivotGridFieldOn10.Caption = "������� ���. ������ ��������";
            this.pivotGridFieldOn10.EmptyCellText = "0";
            this.pivotGridFieldOn10.FieldName = "Summer Platform";
            this.pivotGridFieldOn10.Name = "pivotGridFieldOn10";
            // 
            // pivotGridFieldBrand
            // 
            this.pivotGridFieldBrand.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldBrand.AreaIndex = 0;
            this.pivotGridFieldBrand.Caption = "Brand";
            this.pivotGridFieldBrand.EmptyCellText = "0";
            this.pivotGridFieldBrand.FieldName = "Brand";
            this.pivotGridFieldBrand.Name = "pivotGridFieldBrand";
            this.pivotGridFieldBrand.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField5.AreaIndex = 6;
            this.pivotGridField5.Caption = "���� �������+";
            this.pivotGridField5.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField5.Name = "pivotGridField5";
            this.pivotGridField5.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField6.AreaIndex = 7;
            this.pivotGridField6.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField6.FieldName = "Sales";
            this.pivotGridField6.Name = "pivotGridField6";
            this.pivotGridField6.Visible = false;
            // 
            // pivotGridField7
            // 
            this.pivotGridField7.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField7.AreaIndex = 7;
            this.pivotGridField7.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField7.FieldName = "Sales Premium";
            this.pivotGridField7.Name = "pivotGridField7";
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "��� ��";
            this.pivotGridField1.FieldName = "Ol_id";
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // pivotGridFieldTypeTT
            // 
            this.pivotGridFieldTypeTT.AreaIndex = 1;
            this.pivotGridFieldTypeTT.Caption = "��� ��";
            this.pivotGridFieldTypeTT.FieldName = "OLtype_name";
            this.pivotGridFieldTypeTT.Name = "pivotGridFieldTypeTT";
            // 
            // pivotGridFieldTradingNameTT
            // 
            this.pivotGridFieldTradingNameTT.AreaIndex = 2;
            this.pivotGridFieldTradingNameTT.Caption = "����. ��� ��";
            this.pivotGridFieldTradingNameTT.FieldName = "OLTradingName";
            this.pivotGridFieldTradingNameTT.Name = "pivotGridFieldTradingNameTT";
            // 
            // pivotGridFieldNameTT
            // 
            this.pivotGridFieldNameTT.AreaIndex = 6;
            this.pivotGridFieldNameTT.Caption = "��. ��� ��";
            this.pivotGridFieldNameTT.FieldName = "OLName";
            this.pivotGridFieldNameTT.Name = "pivotGridFieldNameTT";
            // 
            // pivotGridFieldAddressTT
            // 
            this.pivotGridFieldAddressTT.AreaIndex = 5;
            this.pivotGridFieldAddressTT.Caption = "��. ����� ��";
            this.pivotGridFieldAddressTT.FieldName = "OLAddress";
            this.pivotGridFieldAddressTT.Name = "pivotGridFieldAddressTT";
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.AreaIndex = 4;
            this.pivotGridField2.Caption = "����� ���������";
            this.pivotGridField2.FieldName = "Channel_Type";
            this.pivotGridField2.Name = "pivotGridField2";
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField3.Caption = "Brand";
            this.pivotGridField3.FieldName = "Brand";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // DataReport
            // 
            this.Controls.Add(this.pivotGridControl);
            this.Name = "DataReport";
            this.Size = new System.Drawing.Size(1436, 800);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM3;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldBaseNew;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSales;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMarketShare;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAllPos;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPosSmall;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPosMedium;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPosLarge;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCompetitorsPosOther1Door;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCompetitorsPosOther2Door;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCooler;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldEksklyuzivCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTents;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSync;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPocId;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTradingName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDeliveryAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTypeTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTradingNameTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNameTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAddressTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDirector;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPhone;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldType;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPocChanel;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistributor;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCategory;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldLocation;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNetworkName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIdEdrpou;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSalesKvYarulo;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOffEksklyuzivBeer;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOff7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOn6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOn7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOn8;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOn9;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOn10;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPremium;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldBrand;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPOSInbevOutdoor;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStaffChannel;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAlcoLicense;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAlcoStrong;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldVolumePotential;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldInfluencerPotential;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStorageSize;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldUrbanRural;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSubType;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTS;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRouteOwner;




    }
}
