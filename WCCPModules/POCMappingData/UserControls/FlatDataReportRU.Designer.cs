﻿namespace WccpReporting.POCMappingData.UserControls
{
    partial class FlatDataReportRU
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn111 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnIsCloseTT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn60 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn59 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn112 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn46 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn47 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn48 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn49 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn50 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn51 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn52 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn53 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn54 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn55 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn56 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn58 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn57 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1071, 544);
            this.gridControl1.TabIndex = 28;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand7,
            this.gridBand2});
            this.bandedGridView1.ColumnPanelRowHeight = 60;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn14,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn111,
            this.bandedGridColumn18,
            this.bandedGridColumn19,
            this.bandedGridColumn20,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.bandedGridColumnIsCloseTT,
            this.bandedGridColumn23,
            this.bandedGridColumn24,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn27,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn31,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn34,
            this.bandedGridColumn35,
            this.bandedGridColumn36,
            this.bandedGridColumn37,
            this.bandedGridColumn38,
            this.bandedGridColumn39,
            this.bandedGridColumn40,
            this.bandedGridColumn112,
            this.bandedGridColumn41,
            this.bandedGridColumn42,
            this.bandedGridColumn43,
            this.bandedGridColumn44,
            this.bandedGridColumn45,
            this.bandedGridColumn46,
            this.bandedGridColumn47,
            this.bandedGridColumn48,
            this.bandedGridColumn49,
            this.bandedGridColumn50,
            this.bandedGridColumn51,
            this.bandedGridColumn52,
            this.bandedGridColumn53,
            this.bandedGridColumn54,
            this.bandedGridColumn55,
            this.bandedGridColumn56,
            this.bandedGridColumn58,
            this.bandedGridColumn57,
            this.bandedGridColumn59,
            this.bandedGridColumn60});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.Editable = false;
            this.bandedGridView1.OptionsFilter.MRUFilterListPopupCount = 70;
            this.bandedGridView1.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.bandedGridView1.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.bandedGridView1.OptionsSelection.MultiSelect = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.RowAutoHeight = true;
            this.bandedGridView1.OptionsView.ShowDetailButtons = false;
            this.bandedGridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.OptionsView.ShowIndicator = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.Columns.Add(this.bandedGridColumn3);
            this.gridBand1.Columns.Add(this.bandedGridColumn4);
            this.gridBand1.Columns.Add(this.bandedGridColumn5);
            this.gridBand1.Columns.Add(this.bandedGridColumn6);
            this.gridBand1.Columns.Add(this.bandedGridColumn7);
            this.gridBand1.Columns.Add(this.bandedGridColumn8);
            this.gridBand1.Columns.Add(this.bandedGridColumn9);
            this.gridBand1.Columns.Add(this.bandedGridColumn10);
            this.gridBand1.Columns.Add(this.bandedGridColumn11);
            this.gridBand1.Columns.Add(this.bandedGridColumn12);
            this.gridBand1.Columns.Add(this.bandedGridColumn13);
            this.gridBand1.Columns.Add(this.bandedGridColumn14);
            this.gridBand1.Columns.Add(this.bandedGridColumn15);
            this.gridBand1.Columns.Add(this.bandedGridColumn16);
            this.gridBand1.Columns.Add(this.bandedGridColumn17);
            this.gridBand1.Columns.Add(this.bandedGridColumn111);
            this.gridBand1.Columns.Add(this.bandedGridColumn18);
            this.gridBand1.Columns.Add(this.bandedGridColumn19);
            this.gridBand1.Columns.Add(this.bandedGridColumn20);
            this.gridBand1.Columns.Add(this.bandedGridColumn21);
            this.gridBand1.Columns.Add(this.bandedGridColumn22);
            this.gridBand1.Columns.Add(this.bandedGridColumnIsCloseTT);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 1800;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn1.Caption = "Регион";
            this.bandedGridColumn1.FieldName = "Region_name";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.Visible = true;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn2.Caption = "M4";
            this.bandedGridColumn2.FieldName = "M4";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.Visible = true;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn3.Caption = "M3";
            this.bandedGridColumn3.FieldName = "M3";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.Visible = true;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn4.Caption = "M2";
            this.bandedGridColumn4.FieldName = "M2";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.Visible = true;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn5.Caption = "M1";
            this.bandedGridColumn5.FieldName = "M1";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.Visible = true;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn6.Caption = "Код ТТ";
            this.bandedGridColumn6.FieldName = "Ol_id";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.Visible = true;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn7.Caption = "Юр. имя";
            this.bandedGridColumn7.FieldName = "OLName";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn7.Visible = true;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn8.Caption = "Факт. имя";
            this.bandedGridColumn8.FieldName = "OLTradingName";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn8.Visible = true;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn9.Caption = "Юр. адрес";
            this.bandedGridColumn9.FieldName = "OLAddress";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn9.Visible = true;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn10.Caption = "Факт. адрес";
            this.bandedGridColumn10.FieldName = "OLDeliveryAddress";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn10.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn11.Caption = "Контактное лицо";
            this.bandedGridColumn11.FieldName = "OLDirector";
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn11.Visible = true;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn12.Caption = "Телефон";
            this.bandedGridColumn12.FieldName = "OLTelephone";
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn12.Visible = true;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn13.Caption = "Размер склада";
            this.bandedGridColumn13.FieldName = "OLWHSize";
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn13.Visible = true;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn14.Caption = "Населенный пункт";
            this.bandedGridColumn14.FieldName = "City_name";
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn14.Visible = true;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn15.Caption = "Urban/Rural";
            this.bandedGridColumn15.FieldName = "Settlement";
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn16.Caption = "Принадлежность к сети";
            this.bandedGridColumn16.FieldName = "OlNetwork";
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn17.Caption = "Канал ТТ";
            this.bandedGridColumn17.FieldName = "OlChannel";
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumn111
            // 
            this.bandedGridColumn111.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn111.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn111.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn111.Caption = "Канал персонала М3";
            this.bandedGridColumn111.FieldName = "Channel_Type";
            this.bandedGridColumn111.Name = "bandedGridColumn111";
            this.bandedGridColumn111.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn111.Visible = true;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn18.Caption = "Тип ТТ";
            this.bandedGridColumn18.FieldName = "OLtype_name";
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn18.Visible = true;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn19.Caption = "Подтип ТТ";
            this.bandedGridColumn19.FieldName = "OLSubTypeName";
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn20.Caption = "Дистрибутор";
            this.bandedGridColumn20.FieldName = "DISTR_NAME";
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn20.Visible = true;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn21.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn21.Caption = "ТС";
            this.bandedGridColumn21.FieldName = "Cust_NAME";
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn21.Visible = true;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn22.Caption = "Маршрут/Владелец";
            this.bandedGridColumn22.FieldName = "OlOwner";
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn22.Visible = true;
            // 
            // bandedGridColumnIsCloseTT
            // 
            this.bandedGridColumnIsCloseTT.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumnIsCloseTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumnIsCloseTT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumnIsCloseTT.Caption = "ТТ закрыта, не существует";
            this.bandedGridColumnIsCloseTT.FieldName = "isCloseTT";
            this.bandedGridColumnIsCloseTT.Name = "bandedGridColumnIsCloseTT";
            this.bandedGridColumnIsCloseTT.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnIsCloseTT.Visible = true;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Объем продажи драфта, дал";
            this.gridBand11.Columns.Add(this.bandedGridColumn23);
            this.gridBand11.Columns.Add(this.bandedGridColumn60);
            this.gridBand11.Columns.Add(this.bandedGridColumn24);
            this.gridBand11.Columns.Add(this.bandedGridColumn25);
            this.gridBand11.Columns.Add(this.bandedGridColumn26);
            this.gridBand11.Columns.Add(this.bandedGridColumn27);
            this.gridBand11.Columns.Add(this.bandedGridColumn28);
            this.gridBand11.Columns.Add(this.bandedGridColumn29);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 1;
            this.gridBand11.Width = 600;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn23.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn23.Caption = "ABI";
            this.bandedGridColumn23.FieldName = "ABI Vol Draft";
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn23.Visible = true;
            // 
            // bandedGridColumn60
            // 
            this.bandedGridColumn60.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn60.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn60.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn60.Caption = "ABI в системе";
            this.bandedGridColumn60.FieldName = "ABI SYS Vol Draft";
            this.bandedGridColumn60.Name = "bandedGridColumn60";
            this.bandedGridColumn60.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn60.Visible = true;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn24.Caption = "Балтика";
            this.bandedGridColumn24.FieldName = "Baltika Vol Draft";
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn24.Visible = true;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn25.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn25.Caption = "Efes";
            this.bandedGridColumn25.FieldName = "Efes Vol Draft";
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn25.Visible = true;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn26.Caption = "Heineken";
            this.bandedGridColumn26.FieldName = "Heineken Vol Draft";
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn26.Visible = true;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn27.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn27.Caption = "МПК";
            this.bandedGridColumn27.FieldName = "MPK Vol Draft";
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn27.Visible = true;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn28.Caption = "Прочее импорт";
            this.bandedGridColumn28.FieldName = "Other Vol Draft";
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn28.Visible = true;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn29.Caption = "Прочее  локальное";
            this.bandedGridColumn29.FieldName = "Other Local Vol Draft";
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn29.Visible = true;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Объем продажи фасовки, дал";
            this.gridBand12.Columns.Add(this.bandedGridColumn30);
            this.gridBand12.Columns.Add(this.bandedGridColumn59);
            this.gridBand12.Columns.Add(this.bandedGridColumn31);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 2;
            this.gridBand12.Width = 222;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn30.Caption = "ABI";
            this.bandedGridColumn30.FieldName = "ABI Vol Fasov";
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn30.Visible = true;
            this.bandedGridColumn30.Width = 73;
            // 
            // bandedGridColumn59
            // 
            this.bandedGridColumn59.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn59.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn59.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn59.Caption = "ABI в системе";
            this.bandedGridColumn59.FieldName = "ABI SYS Vol Fasov";
            this.bandedGridColumn59.Name = "bandedGridColumn59";
            this.bandedGridColumn59.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn59.Visible = true;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn31.Caption = "Конкуренты";
            this.bandedGridColumn31.FieldName = "Other Vol Fasov";
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn31.Visible = true;
            this.bandedGridColumn31.Width = 74;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Market Share, драфт";
            this.gridBand13.Columns.Add(this.bandedGridColumn32);
            this.gridBand13.Columns.Add(this.bandedGridColumn33);
            this.gridBand13.Columns.Add(this.bandedGridColumn34);
            this.gridBand13.Columns.Add(this.bandedGridColumn35);
            this.gridBand13.Columns.Add(this.bandedGridColumn36);
            this.gridBand13.Columns.Add(this.bandedGridColumn37);
            this.gridBand13.Columns.Add(this.bandedGridColumn38);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 3;
            this.gridBand13.Width = 525;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn32.Caption = "ABI";
            this.bandedGridColumn32.FieldName = "ABI MarketShare Draft";
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn32.Visible = true;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn33.Caption = "Балтика";
            this.bandedGridColumn33.FieldName = "Baltika MarketShare Draft";
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn33.Visible = true;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn34.Caption = "Efes";
            this.bandedGridColumn34.FieldName = "Efes MarketShare Draft";
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn34.Visible = true;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn35.Caption = "Heineken";
            this.bandedGridColumn35.FieldName = "Heineken MarketShare Draft";
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            this.bandedGridColumn35.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn35.Visible = true;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn36.Caption = "МПК";
            this.bandedGridColumn36.FieldName = "MPK MarketShare Draftt";
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn36.Visible = true;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn37.Caption = "Прочее импорт";
            this.bandedGridColumn37.FieldName = "Other MarketShare Draft";
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn37.Visible = true;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn38.Caption = "Прочее  локальное";
            this.bandedGridColumn38.FieldName = "Other Local MarketShare Draft";
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn38.Visible = true;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "Market Share, фасовка";
            this.gridBand14.Columns.Add(this.bandedGridColumn39);
            this.gridBand14.Columns.Add(this.bandedGridColumn40);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 4;
            this.gridBand14.Width = 150;
            // 
            // bandedGridColumn39
            // 
            this.bandedGridColumn39.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn39.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn39.Caption = "ABI";
            this.bandedGridColumn39.FieldName = "ABI MarketShare Fasov";
            this.bandedGridColumn39.Name = "bandedGridColumn39";
            this.bandedGridColumn39.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn39.Visible = true;
            // 
            // bandedGridColumn40
            // 
            this.bandedGridColumn40.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn40.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn40.Caption = "Конкуренты";
            this.bandedGridColumn40.FieldName = "Other MarketShare Fasov";
            this.bandedGridColumn40.Name = "bandedGridColumn40";
            this.bandedGridColumn40.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn40.Visible = true;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "Контракты";
            this.gridBand15.Columns.Add(this.bandedGridColumn112);
            this.gridBand15.Columns.Add(this.bandedGridColumn41);
            this.gridBand15.Columns.Add(this.bandedGridColumn42);
            this.gridBand15.Columns.Add(this.bandedGridColumn43);
            this.gridBand15.Columns.Add(this.bandedGridColumn44);
            this.gridBand15.Columns.Add(this.bandedGridColumn45);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 5;
            this.gridBand15.Width = 450;
            // 
            // bandedGridColumn112
            // 
            this.bandedGridColumn112.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn112.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn112.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn112.Caption = "ABI";
            this.bandedGridColumn112.FieldName = "ABI Contract";
            this.bandedGridColumn112.Name = "bandedGridColumn112";
            this.bandedGridColumn112.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn112.Visible = true;
            // 
            // bandedGridColumn41
            // 
            this.bandedGridColumn41.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn41.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn41.Caption = "Балтика";
            this.bandedGridColumn41.FieldName = "Baltika Contract";
            this.bandedGridColumn41.Name = "bandedGridColumn41";
            this.bandedGridColumn41.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn41.Visible = true;
            // 
            // bandedGridColumn42
            // 
            this.bandedGridColumn42.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn42.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn42.Caption = "Efes";
            this.bandedGridColumn42.FieldName = "Efes Contract";
            this.bandedGridColumn42.Name = "bandedGridColumn42";
            this.bandedGridColumn42.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn42.Visible = true;
            // 
            // bandedGridColumn43
            // 
            this.bandedGridColumn43.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn43.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn43.Caption = "Heineken";
            this.bandedGridColumn43.FieldName = "Heineken Contract";
            this.bandedGridColumn43.Name = "bandedGridColumn43";
            this.bandedGridColumn43.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn43.Visible = true;
            // 
            // bandedGridColumn44
            // 
            this.bandedGridColumn44.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn44.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn44.Caption = "МПК";
            this.bandedGridColumn44.FieldName = "MPK Contract";
            this.bandedGridColumn44.Name = "bandedGridColumn44";
            this.bandedGridColumn44.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn44.Visible = true;
            // 
            // bandedGridColumn45
            // 
            this.bandedGridColumn45.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn45.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn45.Caption = "Другие";
            this.bandedGridColumn45.FieldName = "Other Contract";
            this.bandedGridColumn45.Name = "bandedGridColumn45";
            this.bandedGridColumn45.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn45.Visible = true;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Оборудование, кол-во кранов";
            this.gridBand7.Columns.Add(this.bandedGridColumn46);
            this.gridBand7.Columns.Add(this.bandedGridColumn47);
            this.gridBand7.Columns.Add(this.bandedGridColumn48);
            this.gridBand7.Columns.Add(this.bandedGridColumn49);
            this.gridBand7.Columns.Add(this.bandedGridColumn50);
            this.gridBand7.Columns.Add(this.bandedGridColumn51);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 6;
            this.gridBand7.Width = 450;
            // 
            // bandedGridColumn46
            // 
            this.bandedGridColumn46.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn46.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn46.Caption = "ABI";
            this.bandedGridColumn46.FieldName = "ABI Count Tap";
            this.bandedGridColumn46.Name = "bandedGridColumn46";
            this.bandedGridColumn46.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn46.Visible = true;
            // 
            // bandedGridColumn47
            // 
            this.bandedGridColumn47.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn47.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn47.Caption = "Балтика";
            this.bandedGridColumn47.FieldName = "Baltika Count Tap";
            this.bandedGridColumn47.Name = "bandedGridColumn47";
            this.bandedGridColumn47.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn47.Visible = true;
            // 
            // bandedGridColumn48
            // 
            this.bandedGridColumn48.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn48.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn48.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn48.Caption = "Efes";
            this.bandedGridColumn48.FieldName = "Efes Count Tap";
            this.bandedGridColumn48.Name = "bandedGridColumn48";
            this.bandedGridColumn48.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn48.Visible = true;
            // 
            // bandedGridColumn49
            // 
            this.bandedGridColumn49.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn49.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn49.Caption = "Heineken";
            this.bandedGridColumn49.FieldName = "Heineken Count Tap";
            this.bandedGridColumn49.Name = "bandedGridColumn49";
            this.bandedGridColumn49.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn49.Visible = true;
            // 
            // bandedGridColumn50
            // 
            this.bandedGridColumn50.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn50.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn50.Caption = "МПК";
            this.bandedGridColumn50.FieldName = "MPK Count Tap";
            this.bandedGridColumn50.Name = "bandedGridColumn50";
            this.bandedGridColumn50.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn50.Visible = true;
            // 
            // bandedGridColumn51
            // 
            this.bandedGridColumn51.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn51.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn51.Caption = "Другие";
            this.bandedGridColumn51.FieldName = "Other Count Tap";
            this.bandedGridColumn51.Name = "bandedGridColumn51";
            this.bandedGridColumn51.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn51.Visible = true;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand2.Caption = "Дополнительная информация";
            this.gridBand2.Columns.Add(this.bandedGridColumn52);
            this.gridBand2.Columns.Add(this.bandedGridColumn53);
            this.gridBand2.Columns.Add(this.bandedGridColumn54);
            this.gridBand2.Columns.Add(this.bandedGridColumn55);
            this.gridBand2.Columns.Add(this.bandedGridColumn56);
            this.gridBand2.Columns.Add(this.bandedGridColumn58);
            this.gridBand2.Columns.Add(this.bandedGridColumn57);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 7;
            this.gridBand2.Width = 675;
            // 
            // bandedGridColumn52
            // 
            this.bandedGridColumn52.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn52.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn52.Caption = "Кол-во позиций крафтового пива в ассортименте";
            this.bandedGridColumn52.FieldName = "craftBeer";
            this.bandedGridColumn52.Name = "bandedGridColumn52";
            this.bandedGridColumn52.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn52.Visible = true;
            this.bandedGridColumn52.Width = 100;
            // 
            // bandedGridColumn53
            // 
            this.bandedGridColumn53.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn53.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn53.Caption = "Кол-во позиций вина в ассортименте";
            this.bandedGridColumn53.FieldName = "countVine";
            this.bandedGridColumn53.Name = "bandedGridColumn53";
            this.bandedGridColumn53.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn53.Visible = true;
            this.bandedGridColumn53.Width = 100;
            // 
            // bandedGridColumn54
            // 
            this.bandedGridColumn54.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn54.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn54.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn54.Caption = "Кол-во позиций коктелей алкогольных в ассортименте";
            this.bandedGridColumn54.FieldName = "countCoctail";
            this.bandedGridColumn54.Name = "bandedGridColumn54";
            this.bandedGridColumn54.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn54.Visible = true;
            this.bandedGridColumn54.Width = 100;
            // 
            // bandedGridColumn55
            // 
            this.bandedGridColumn55.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn55.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn55.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn55.Caption = "Кол-во позиций водки в ассортименте";
            this.bandedGridColumn55.FieldName = "countVodka";
            this.bandedGridColumn55.Name = "bandedGridColumn55";
            this.bandedGridColumn55.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn55.Visible = true;
            this.bandedGridColumn55.Width = 100;
            // 
            // bandedGridColumn56
            // 
            this.bandedGridColumn56.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn56.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn56.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn56.Caption = "Кол-во позиций другого  крепкого (Виски, Ром, Самбука, Абсент)  алкоголя в ассорт" +
    "именте";
            this.bandedGridColumn56.FieldName = "otherStrong";
            this.bandedGridColumn56.Name = "bandedGridColumn56";
            this.bandedGridColumn56.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn56.Visible = true;
            this.bandedGridColumn56.Width = 100;
            // 
            // bandedGridColumn58
            // 
            this.bandedGridColumn58.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn58.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn58.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn58.Caption = "Кол-во мест";
            this.bandedGridColumn58.FieldName = "countBud";
            this.bandedGridColumn58.Name = "bandedGridColumn58";
            this.bandedGridColumn58.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn58.Visible = true;
            this.bandedGridColumn58.Width = 100;
            // 
            // bandedGridColumn57
            // 
            this.bandedGridColumn57.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn57.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn57.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn57.Caption = "Стоимость кофе";
            this.bandedGridColumn57.FieldName = "Esspresso";
            this.bandedGridColumn57.Name = "bandedGridColumn57";
            this.bandedGridColumn57.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn57.Visible = true;
            // 
            // FlatDataReportRU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "FlatDataReportRU";
            this.Size = new System.Drawing.Size(1071, 544);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn41;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn43;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn111;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnIsCloseTT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn112;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn45;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn46;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn47;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn48;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn49;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn50;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn51;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn52;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn53;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn54;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn55;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn56;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn58;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn60;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn59;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn57;
    }
}
