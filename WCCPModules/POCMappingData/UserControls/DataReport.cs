using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using Logica.Reports.Common;
using WccpReporting.POCMappingData.Core;
using WccpReporting.POCMappingData.DataProvider;

namespace WccpReporting.POCMappingData.UserControls {
    public partial class DataReport : UserControl, IReport {
        #region Fields

        private DataTable _dataTable;

        private readonly IDictionary<string, int> _brandOrder = new Dictionary<string, int> {
            {SqlConstants.BrandInBev, 0},
            {SqlConstants.BrandObolon, 1},
            {SqlConstants.BrandBBH, 2},
            {SqlConstants.BrandPPB, 3},
            {SqlConstants.BrandSabmiller, 4},
            {SqlConstants.BrandOther, 5},
            {SqlConstants.PartOfPremium, 6}
        };

        private bool _customSummaryDisabled;
        private bool _summarySetted;

        #endregion

        public DataReport() {
            InitializeComponent();
        }

        public void ShowReport(SettingsForm setForm) {
            _customSummaryDisabled = true;
            try {
                pivotGridControl.DataSource = _dataTable;

                bool lIsOnChannel = setForm.SelectedData.SelectedChanel == CommonConstants.ChnlOnUAId;

                pivotGridFieldTents.Visible = lIsOnChannel;
                pivotGridFieldPremium.Visible = lIsOnChannel;
                pivotGridFieldCooler.Visible = lIsOnChannel;
                pivotGridFieldAllPos.Visible = !lIsOnChannel;

                pivotGridFieldOn6.Visible = lIsOnChannel;
                //pivotGridFieldOn7.Visible = on;
                pivotGridFieldOn8.Visible = lIsOnChannel;
                pivotGridFieldOn9.Visible = lIsOnChannel;
                pivotGridFieldOn10.Visible = lIsOnChannel;

                pivotGridFieldOff7.Visible = !lIsOnChannel;
                pivotGridFieldOffEksklyuzivBeer.Visible = !lIsOnChannel;

                pivotGridFieldPosSmall.Visible = lIsOnChannel;
                pivotGridFieldPosMedium.Visible = lIsOnChannel;
                pivotGridFieldPosLarge.Visible = lIsOnChannel;
            }
            catch (Exception lException) {
                ErrorManager.ShowErrorBox(lException.Message);
            }
            finally {
                _customSummaryDisabled = false;
            }
        }

        private void pivotGridControl_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e) {
            if (e.DataField == pivotGridFieldAllPos) {
                int lCount = 0;
                PivotDrillDownDataSource lDs = e.CreateDrillDownDataSource();

                foreach (PivotDrillDownDataRow lRow in lDs) {
                    lCount += (int) AsDouble(lRow[SqlConstants.FldPosSmall]);
                    lCount += (int) AsDouble(lRow[SqlConstants.FldPosMedium]);
                    lCount += (int) AsDouble(lRow[SqlConstants.FldPosLarge]);
                    lCount += (int) AsDouble(lRow[pivotGridFieldCompetitorsPosOther1Door.FieldName]);
                    lCount += (int) AsDouble(lRow[pivotGridFieldCompetitorsPosOther2Door.FieldName]);
                }

                e.CustomValue = lCount;
            }
            else if (e.DataField == pivotGridFieldEksklyuzivCount) {
                int lCount = 0;
                PivotDrillDownDataSource lDs = e.CreateDrillDownDataSource();

                foreach (PivotDrillDownDataRow lRow in lDs) {
                    int lValue;
                    if (lRow[e.DataField] != null && Int32.TryParse(lRow[e.DataField].ToString(), out lValue))
                        lCount += lValue;
                }
                e.CustomValue = lCount;
            }
            else if (e.DataField == pivotGridFieldCount) {
                IList<string> lTempList = new List<string>();

                foreach (PivotDrillDownDataRow lRow in e.CreateDrillDownDataSource()) {
                    if (lRow[pivotGridFieldPocId] != null && 
                        !string.IsNullOrEmpty(lRow[pivotGridFieldPocId].ToString()) &&
                        !lTempList.Contains(lRow[pivotGridFieldPocId].ToString())) {
                        lTempList.Add(lRow[pivotGridFieldPocId].ToString());
                    }
                }

                e.CustomValue = lTempList.Count;
            }
                        else if (e.DataField == pivotGridFieldOn6
                     || e.DataField == pivotGridFieldOn7
                     || e.DataField == pivotGridFieldOn8
                     || e.DataField == pivotGridFieldOffEksklyuzivBeer) 
            {
                IList<string> lTempList = new List<string>();
                double lSum = 0;

                foreach (PivotDrillDownDataRow lRow in e.CreateDrillDownDataSource()) {
                    if (lRow[pivotGridFieldPocId] != null &&
                        !string.IsNullOrEmpty(lRow[pivotGridFieldPocId].ToString()) &&
                        !lTempList.Contains(lRow[pivotGridFieldPocId].ToString()))
                    {
                        lTempList.Add(lRow[pivotGridFieldPocId].ToString());
                        lSum += AsDouble(lRow[e.DataField]);
                    }
                }

                e.CustomValue = lSum;
            }
        }

        private void pivotGridControl_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e) {
            const double epsilon = 0.00000001;
            if (e.DataField == pivotGridFieldMarketShare) {
                double lInBevSales = AsDouble(e.GetCellValue(pivotGridFieldSales));
                double lAllSales = AsDouble(e.GetColumnGrandTotal(pivotGridFieldSales));

                if (Math.Abs(lAllSales - 0.0) < epsilon)
                    e.DisplayText = "0 %";
                else
                    e.DisplayText = Math.Round(lInBevSales * 100 / lAllSales, 2) + " %";
            }

            if (e.DataField == pivotGridField5) {
                double lSalesPremium = AsDouble(e.GetCellValue(pivotGridField7));
                double lTotalSales = AsDouble(e.GetCellValue(pivotGridFieldSales));

                if (Math.Abs(lTotalSales - 0) < epsilon)
                    e.DisplayText = "0 %";
                else
                    e.DisplayText = Math.Round(lSalesPremium * 100 / lTotalSales, 2) + " %";
            }
        }

        private double AsDouble(object num) {
            try {
                if (num == null)
                    return 0;
                if (num is int || num is long || num is decimal || num is double)
                    return Convert.ToDouble(num);
                if (num.ToString() != string.Empty)
                    return double.Parse(num.ToString(), CultureInfo.InvariantCulture);
            }
            catch {
            }

            return 0;
        }

        private void pivotGridControl_FieldAreaChanging(object sender, PivotAreaChangingEventArgs e) {
            _summarySetted = false;
        }

        private void pivotGridControl_FieldAreaChanged(object sender, PivotFieldEventArgs e) {
            if (_customSummaryDisabled || _summarySetted || e.Field.Area != PivotArea.DataArea)
                return;
            CustomSummaryType lSummaryType = new CustomSummaryType();
            if (lSummaryType.ShowDialog() != DialogResult.OK)
                return;
            e.Field.SummaryType = lSummaryType.SelectedSummaryType;
            _summarySetted = true;
        }

        private void pivotGridControl_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e) {
            if (e.Field != pivotGridFieldBrand)
                return;
            e.Result = _brandOrder[(string) e.Value1].CompareTo(_brandOrder[(string) e.Value2]);
            e.Handled = true;
        }

        #region IReport Members

        public void ShowPrintPreview() {
            pivotGridControl.ShowPrintPreview();
        }

        public void ExportToXls(string fileName) {
            pivotGridControl.ExportToXls(fileName);
        }

        public void ExportToXlsx(string fileName)
        {
            //pivotGridControl.ExportToXlsx(fileName);
        }

        #endregion

        internal void SetSource(DataTable source) {
            _dataTable = source;
        }
    }
}