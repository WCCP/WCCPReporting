using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.Model;
using WccpReporting.DataProvider;

namespace WccpReporting
{
    public partial class SettingsForm
    {
        public bool FormDisabled { get; set; }
        private List<SheetParamCollection> listSheetParams = new List<SheetParamCollection>();
        private Report report;
        private string selectedRegion;
        private string selectedRegionName;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsForm"/> class.
        /// </summary>
        /// <param name="report">The report.</param>
        public SettingsForm(Report report)
        {
            this.report = report;

            InitializeComponent();
            BuildSettings();
        }

        /// <summary>
        /// Gets the list sheet params.
        /// </summary>
        /// <value>The list sheet params.</value>
        public List<SheetParamCollection> ListSheetParams
        {
            get { BuildSheetSettings(); return listSheetParams; }
        }

        /// <summary>
        /// Validates the data.
        /// </summary>
        /// <returns>True if data is valid otherwise false.</returns>
        public bool ValidateData()
        {
            selectedRegionName = string.Join(", ", chkListBoxRegion.CheckedItems.Cast<CheckedListBoxItem>().Where(item => item != chkListBoxRegion.Items[0]).Select(
                                                 item => item.Description).ToArray());
            selectedRegion = string.Join(",", chkListBoxRegion.CheckedItems.Cast<CheckedListBoxItem>().Where(item => item != chkListBoxRegion.Items[0]).Select(
                                             item => item.Value.ToString()).ToArray());

            return !string.IsNullOrEmpty(selectedRegion);
        }

        private void BuildSettings()
        {
            Text = Resources.ParamsFormCaption;
            labelRegion.Text = Resources.Region;
            btnYes.Text = Resources.Start;
            btnCancel.Text = Resources.Cancel;
            // Select data for Region
            chkListBoxRegion.Items.AddRange(GetRegionItems());
            SetBtnEnabling();
        }

        private CheckedListBoxItem[] GetRegionItems()
        {
            var items = new CheckedListBoxItemCollection();
            var allSelectItem = new CheckedListBoxItem { Value = -1, Description = "Select All", CheckState = CheckState.Checked };
            items.Add(allSelectItem);
            DataTable tableRegions = DataAccessProvider.GetRegions();
            foreach (DataRow row in tableRegions.Rows)
            {
                var item = new CheckedListBoxItem(row[SqlConstants.FldRegionId], row[SqlConstants.FldRegionName].ToString(), CheckState.Checked);
                items.Add(item);
            }
            if (items.Count == 1)
            {
                items[0].Enabled = false;
                FormDisabled = true;
            }
            var ret = new CheckedListBoxItem[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                ret[i] = items[i];
            }
            return ret;
        }

        /// <summary>
        /// Builds the sheet settings.
        /// </summary>
        /// <param name="report">The report.</param>
        private void BuildSheetSettings()
        {
            if (report != null)
            {
                listSheetParams.Clear();

                foreach (Tab tab in report.Tabs)
                {
                    listSheetParams.Add(GetParamCollection(tab.Id));
                }
            }
        }

        /// <summary>
        /// Gets the param collection.
        /// </summary>
        /// <param name="tabId">The tab id.</param>
        /// <returns></returns>
        private SheetParamCollection GetParamCollection(Guid tabId)
        {
            SheetParamCollection parameterCollection = new SheetParamCollection();

            parameterCollection.TabId = tabId;
            parameterCollection.TableDataType = TableType.Fact;

            parameterCollection.Add(new SheetParam
            {
                SqlParamName = SqlConstants.ParamRegions,
                DisplayParamName = Resources.Region,
                DisplayValue = selectedRegionName,
                Value = selectedRegion
            });
            return parameterCollection;
        }

        private void SetBtnEnabling()
        {
            btnYes.Enabled = chkListBoxRegion.Items[0].CheckState == CheckState.Checked || chkListBoxRegion.Items[0].CheckState == CheckState.Indeterminate;
        }

        private void btnYes_Click(Object sender, EventArgs e)
        {
            if (ValidateData())
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void chkListBoxRegion_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {
                if (e.Index == 0 && e.State != CheckState.Indeterminate)
                {
                    foreach (CheckedListBoxItem item in chkListBoxRegion.Items)
                    {
                        item.CheckState = e.State;
                    }
                    return;
                }
                if ((e.State == CheckState.Unchecked && chkListBoxRegion.Items[0].CheckState == CheckState.Checked)
                    || (e.State == CheckState.Checked && chkListBoxRegion.Items[0].CheckState == CheckState.Unchecked))
                {
                    chkListBoxRegion.Items[0].CheckState = CheckState.Indeterminate;
                    return;
                }
                if (chkListBoxRegion.Items.Cast<CheckedListBoxItem>().Where(item => item != chkListBoxRegion.Items[0]).Any(item => item.CheckState != e.State))
                {
                    return;
                }
                chkListBoxRegion.Items[0].CheckState = e.State;
            }
            finally 
            {
                SetBtnEnabling();
            }
        }
    }
}