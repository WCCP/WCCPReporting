﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraTab;
using Logica.Reports.DataAccess;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    //public partial class WccpUIControl : Form
    {
        private SettingsForm settingsForm;

        private List<string> hiddenColumns = new List<string> { "colFA_ID", "colFA_Description" };
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            settingsForm = new SettingsForm(Report);
            
            HideColumns();
            SettingsFormClick += WccpUIControl_SettingsFormClick;
        }

        public int ReportInit()
        {
            if ((settingsForm.FormDisabled && settingsForm.ValidateData()) || settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
                return 0;
            }

            return 1;
        }

        private void HideColumns()
        {
            try
            {
                if ((bool)DataAccessLayer.ExecuteScalarQuery("spDW_Capex_FA"))
                    return;
            }
            catch { }

            if(tabManager.TabPages.Count == 0)
                return;

            var tab = (BaseTab)tabManager.TabPages[0];
            var gridView = (GridView)tab.GetGrid(0).GridControl.DefaultView;
            if (gridView == null)
                return;
            foreach (GridColumn column in gridView.Columns)
            {
                if (hiddenColumns.Contains(column.Name))
                    column.Visible = false;
            }
        }

        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
            }
        }

        private void UpdateAllSheets()
        {
            UpdateSheets(settingsForm.ListSheetParams);
            RefreshControls();
        }

        void RefreshControls()
        {
            btnPrintAll.Visibility = BarItemVisibility.Never;
            btnExportAllTo.Visibility = BarItemVisibility.Never;
            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    (tab as BaseTab).AllowMultiSelect = true;
                    (tab as BaseTab).EnableAppearenceFocusedCell = false;
                }

                if (tab.Controls.Count == 2)
                {
                    Control grid = tab.Controls[0];
                    Control label = tab.Controls[1];

                    grid.Dock = DockStyle.Fill;
                    label.Dock = DockStyle.Fill;

                    tab.Controls.Remove(label);
                    tab.Controls.Remove(grid);

                    SplitContainer sc = new SplitContainer();
                    sc.Dock = DockStyle.Fill;
                    sc.Orientation = Orientation.Horizontal;
                    sc.Panel1MinSize = 19;
                    sc.SplitterDistance = 19;
                    sc.SizeChanged += (x, y) => { sc.SplitterDistance = 19; };
                    sc.IsSplitterFixed = true;
                    sc.SplitterWidth = 1;

                    sc.Panel1.Controls.Add(label);
                    sc.Panel2.Controls.Add(grid);

                    tab.Controls.Add(sc);
                }
            }
        }
    }
}
