namespace WccpExport.CommandLine {
    public interface ICommandLineParser {
        string[] Parse(string[] arguments);
        string GetDescription();
    }
}