using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace WccpExport.CommandLine {
    public class CommandLineParserContainer {
        private readonly List<ICommandLineParser> _parsers = new List<ICommandLineParser>();

        public void Parse(string[] arguments)
        {
            Debug.Assert(arguments != null);
            string[] lProcessedArgs = arguments;
            foreach (ICommandLineParser lParser in _parsers)
            {
                lProcessedArgs = lParser.Parse(lProcessedArgs);
            }
        }

        public void AddParser(ICommandLineParser parser) {
            Debug.Assert(parser != null);
            _parsers.Add(parser);
        }

        public int Count {
            get { return _parsers.Count; }
        }

        public ICommandLineParser this[int index] {
            get { return _parsers[index]; }
        }
    }
}