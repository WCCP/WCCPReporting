using System;
using System.Collections.Generic;

namespace WccpExport.CommandLine {
    public class ReportIdCmdLineParser : ICommandLineParser {
        private const string RPT_ID_PARAM = "-rpt_id";
        private string _rptIdStr;
        
        public ReportIdCmdLineParser() {
            // -1 means no report to run
            _rptIdStr = "-1";
        }

        public int GetReportId() {
            int lResult;
            try {
                lResult = Convert.ToInt32(_rptIdStr);
            }
            catch {
                lResult = -1;
            }

            return lResult;
        }

        public override string ToString() {
            int lId = GetReportId();
            return (lId == -1) ? "NULL" : lId.ToString();
        }

        public string[] Parse(string[] arguments) {
            string[] lResult = arguments;
            for (int lIndex = 0; lIndex < arguments.Length; lIndex++) {
                string lParam = arguments[lIndex];
                if (!RPT_ID_PARAM.Equals(lParam.ToLower()))
                    continue;
                if (arguments.Length <= lIndex)
                    continue;
                lIndex++;
                _rptIdStr = arguments[lIndex];
                List<string> lResultList = new List<string>(arguments);
                lResultList.Remove(RPT_ID_PARAM);
                lResultList.Remove(_rptIdStr);
                lResult = lResultList.ToArray();
            }
            return lResult;
        }

        public string GetDescription() {
            return RPT_ID_PARAM + " = " + ToString();
        }
    }
}