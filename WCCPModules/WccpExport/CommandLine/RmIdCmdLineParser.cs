﻿using System;
using System.Collections.Generic;

namespace WccpExport.CommandLine
{
    public class RmIdCmdLineParser : ICommandLineParser
    {
        private const string RM_ID_PARAM = "-rm_id";
        private string _rmIdStr;

        public RmIdCmdLineParser()
        {
            _rmIdStr = "-1";
        }

        public List<int> GetRmIds()
        {
            List<int> lResult;
            try
            {
                string[] ids = _rmIdStr.Split(',');
                lResult = new List<int>();
                foreach (var id in ids)
                {
                    lResult.Add(Int32.Parse(id));
                }
            }
            catch
            {
                lResult = new List<int>();
            }

            return lResult;
        }

        public string[] Parse(string[] arguments)
        {
            string[] lResult = arguments;
            for (int lIndex = 0; lIndex < arguments.Length; lIndex++)
            {
                string lParam = arguments[lIndex];
                if (!RM_ID_PARAM.Equals(lParam.ToLower()))
                    continue;
                if (arguments.Length <= lIndex)
                    continue;
                lIndex++;
                _rmIdStr = arguments[lIndex];
                List<string> lResultList = new List<string>(arguments);
                lResultList.Remove(RM_ID_PARAM);
                lResultList.Remove(_rmIdStr);
                lResult = lResultList.ToArray();
            }
            return lResult;
        }

        public string GetDescription()
        {
            return RM_ID_PARAM + " = " + _rmIdStr;
        }
    }
}