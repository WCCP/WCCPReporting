using System;
using System.Collections.Generic;

namespace WccpExport.CommandLine {
    public class DsmIdCmdLineParser : ICommandLineParser {
        private const string DSM_ID_PARAM = "-dsm_id";
        private string _dsmIdStr;
        
        public DsmIdCmdLineParser() {
            // -1 means all DSMs
            _dsmIdStr = "-1";
        }

        public int GetDsmId() {
            int lResult;
            try {
                lResult = Convert.ToInt32(_dsmIdStr);
            }
            catch {
                lResult = -1;
            }

            return lResult;
        }

        public override string ToString() {
            int lId = GetDsmId();
            return (lId == -1) ? "ALL" : lId.ToString();
        }

        public string[] Parse(string[] arguments) {
            string[] lResult = arguments;
            for (int lIndex = 0; lIndex < arguments.Length; lIndex++) {
                string lParam = arguments[lIndex];
                if (!DSM_ID_PARAM.Equals(lParam.ToLower()))
                    continue;
                if (arguments.Length <= lIndex)
                    continue;
                lIndex++;
                _dsmIdStr = arguments[lIndex];
                List<string> lResultList = new List<string>(arguments);
                lResultList.Remove(DSM_ID_PARAM);
                lResultList.Remove(_dsmIdStr);
                lResult = lResultList.ToArray();
            }
            return lResult;
        }

        public string GetDescription() {
            return DSM_ID_PARAM + " = " + ToString();
        }
    }
}