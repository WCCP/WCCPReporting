﻿using System.Collections.Generic;
using System.Globalization;

namespace WccpExport.CommandLine {
    public class RecalcCmdLineParser : ICommandLineParser {
        private const string RECALC_PARAM = "-recalc";
        private bool _recalc;

        public RecalcCmdLineParser() {
            _recalc = false;
        }

        public bool GetRecalc() {
            return _recalc;
        }

        public override string ToString() {
            return _recalc.ToString(CultureInfo.InvariantCulture);
        }

        public string[] Parse(string[] arguments) {
            string[] lResult = arguments;
            for (int lIndex = 0; lIndex < arguments.Length; lIndex++) {
                string lParam = arguments[lIndex];
                if (!RECALC_PARAM.Equals(lParam.ToLower()))
                    continue;
                _recalc = true;
                List<string> lResultList = new List<string>(arguments);
                lResultList.Remove(RECALC_PARAM);
                lResultList.Remove(lParam);
                lResult = lResultList.ToArray();
            }
            return lResult;
        }

        public string GetDescription() {
            return RECALC_PARAM + " = " + ToString();
        }
         
    }
}