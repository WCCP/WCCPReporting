﻿using System;
using System.Collections.Generic;

namespace WccpExport.CommandLine {
    public class SupervisorIdCmdLineParser : ICommandLineParser {
        private const string SV_ID_PARAM = "-sv_id";
        private string _supervisorIdStr;
        
        public SupervisorIdCmdLineParser() {
            // -1 means all supervisors
            _supervisorIdStr = "-1";
        }

        public int GetSupervisorId() {
            int lResult;
            try {
                lResult = Convert.ToInt32(_supervisorIdStr);
            }
            catch {
                lResult = -1;
            }

            return lResult;
        }

        public override string ToString() {
            int lId = GetSupervisorId();
            return (lId == -1) ? "ALL" : lId.ToString();
        }

        public string[] Parse(string[] arguments) {
            string[] lResult = arguments;
            for (int lIndex = 0; lIndex < arguments.Length; lIndex++) {
                string lParam = arguments[lIndex];
                if (!SV_ID_PARAM.Equals(lParam.ToLower()))
                    continue;
                if (arguments.Length <= lIndex)
                    continue;
                lIndex++;
                _supervisorIdStr = arguments[lIndex];
                List<string> lResultList = new List<string>(arguments);
                lResultList.Remove(SV_ID_PARAM);
                lResultList.Remove(_supervisorIdStr);
                lResult = lResultList.ToArray();
            }
            return lResult;
        }

        public string GetDescription() {
            return SV_ID_PARAM + " = " + ToString();
        }
    }
}