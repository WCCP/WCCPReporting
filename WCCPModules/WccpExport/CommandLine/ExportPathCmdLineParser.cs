using System;
using System.Collections.Generic;
using System.IO;

namespace WccpExport.CommandLine {
    public class ExportPathCmdLineParser : ICommandLineParser {
        private const string PATH_PARAM = "-path";
        private string _pathStr;
        
        public ExportPathCmdLineParser() {
            _pathStr = null;
        }

        public string GetExportPath() {
            try {
                if (string.IsNullOrEmpty(_pathStr))
                    return _pathStr;
                if (!Directory.Exists(_pathStr))
                    Directory.CreateDirectory(_pathStr);
            }
            catch {
                _pathStr = null;
            }

            return _pathStr;
        }

        public string[] Parse(string[] arguments) {
            string[] lResult = arguments;
            for (int lIndex = 0; lIndex < arguments.Length; lIndex++) {
                string lParam = arguments[lIndex];
                if (!PATH_PARAM.Equals(lParam.ToLower()))
                    continue;
                if (arguments.Length <= lIndex)
                    continue;
                lIndex++;
                _pathStr = arguments[lIndex];
                List<string> lResultList = new List<string>(arguments);
                lResultList.Remove(PATH_PARAM);
                lResultList.Remove(_pathStr);
                lResult = lResultList.ToArray();
            }
            return lResult;
        }

        public string GetDescription() {
            return PATH_PARAM + " = " + GetExportPath();
        }
    }
}