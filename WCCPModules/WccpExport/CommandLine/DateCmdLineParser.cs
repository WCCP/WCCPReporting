using System;
using System.Collections.Generic;
using System.Globalization;

namespace WccpExport.CommandLine {
    public class DateCmdLineParser : ICommandLineParser {
        private const string DATE_PARAM = "-date";
        private string _dateStr;
        
        public DateCmdLineParser() {
            _dateStr = "";
        }

        public DateTime GetDate() {
            DateTime lResult;
            if (!DateTime.TryParseExact(_dateStr, "yyyyMMdd", null, DateTimeStyles.None, out lResult))
                lResult = DateTime.Today;

            return lResult;
        }

        public override string ToString() {
            DateTime lDateTime = GetDate();
            return lDateTime.ToString("yyyy-MM-dd");
        }

        public string[] Parse(string[] arguments) {
            string[] lResult = arguments;
            for (int lIndex = 0; lIndex < arguments.Length; lIndex++) {
                string lParam = arguments[lIndex];
                if (!DATE_PARAM.Equals(lParam.ToLower()))
                    continue;
                if (arguments.Length <= lIndex)
                    continue;
                lIndex++;
                _dateStr = arguments[lIndex];
                List<string> lResultList = new List<string>(arguments);
                lResultList.Remove(DATE_PARAM);
                lResultList.Remove(_dateStr);
                lResult = lResultList.ToArray();
            }
            return lResult;
        }

        public string GetDescription() {
            return DATE_PARAM + " = " + ToString();
        }
    }
}