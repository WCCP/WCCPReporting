﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using WccpExport.CommandLine;
using System.Configuration;
using System.Runtime.InteropServices;
using DevExpress.Data.PLinq.Helpers;
using Logica.Reports.DataAccess;

namespace WccpExport {

    static class Program {
        private static CommandLineParserContainer _commandLineParser;
        private static ReportIdCmdLineParser _reportIdCmdLineParser = new ReportIdCmdLineParser();
        private static DateCmdLineParser _dateCmdLineParser = new DateCmdLineParser();
        private static RecalcCmdLineParser _recalcCmdLineParser = new RecalcCmdLineParser();
        private static RmIdCmdLineParser _rmIdCmdLineParser = new RmIdCmdLineParser();
        private static DsmIdCmdLineParser _dsmIdCmdLineParser = new DsmIdCmdLineParser();
        private static SupervisorIdCmdLineParser _supervisorIdCmdLineParser = new SupervisorIdCmdLineParser();
        private static ExportPathCmdLineParser _exportPathParser = new ExportPathCmdLineParser();
        private static string[] _runArguments;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            MainForm.LogMsgHeader(string.Format("Started WCCP Export version {0}.", Application.ProductVersion));
            MainForm.LogMsgHeader(string.Format(".NET Runtime version: {0}.", Environment.Version));

            _runArguments = args;
            ParseCommandLine();

            MainForm.LogMsgHeader("Run with parameters:");
            for (int lIndex = 0; lIndex < _commandLineParser.Count; lIndex++)
                MainForm.LogMessage("   " + _commandLineParser[lIndex].GetDescription());

            SetConnectionString();

//            Application.EnableVisualStyles();
//            Application.SetCompatibleTextRenderingDefault(false);
            MainForm lMainForm = new MainForm();
            //parameters
            lMainForm.ReportId = _reportIdCmdLineParser.GetReportId();
            lMainForm.ReportDate = _dateCmdLineParser.GetDate();
            lMainForm.Recalc = _recalcCmdLineParser.GetRecalc();

            try {
                if (lMainForm.ReportId != 178)
                {
                    lMainForm.ReportExporter.SetExportType(ExportToType.Pdf);
                    lMainForm.ReportExporter.SetExportMode(ExportModeEnum.M1);
                    lMainForm.ReportExporter.SetDsmId(_dsmIdCmdLineParser.GetDsmId());
                    lMainForm.ReportExporter.SetSupervisorId(_supervisorIdCmdLineParser.GetSupervisorId());
                }
                else
                {
                    ((IReportExporterExtFor178)lMainForm.ReportExporter).SetRMsIds(_rmIdCmdLineParser.GetRmIds());
                }
                lMainForm.ReportExporter.SetExportPath(_exportPathParser.GetExportPath());
//            MainForm.LogException("Used date: " + _dateCmdLineParser);
//            MainForm.LogException("Used Supervisor_ID: " + _supervisorIdCmdLineParser);

                

                //main run report and it export
                lMainForm.StartRun();
            }
            catch (Exception lException) {
                MainForm.LogException(lException);
            }
            finally {
                MainForm.LogMsgHeader("Finished");
                lMainForm.Close();
                lMainForm.Dispose();
            }
        }

        private static void SetConnectionString()
        {
            SqlConnectionStringBuilder lActiveConnectionSettings = new SqlConnectionStringBuilder();

            lActiveConnectionSettings.DataSource = ConfigurationManager.AppSettings["DataSource"];
            lActiveConnectionSettings.InitialCatalog = ConfigurationManager.AppSettings["InitialCatalog"];
            lActiveConnectionSettings.IntegratedSecurity = Boolean.Parse(ConfigurationManager.AppSettings["IntegratedSecurity"]);

            if (!lActiveConnectionSettings.IntegratedSecurity)
            {
                lActiveConnectionSettings.UserID = ConfigurationManager.AppSettings["UserId"];
                lActiveConnectionSettings.Password = ConfigurationManager.AppSettings["Password"];
            }

            DataAccessLayer.SetConnectionString(lActiveConnectionSettings.ConnectionString);
        }

        private static void ParseCommandLine() {
            _commandLineParser = new CommandLineParserContainer();
            _commandLineParser.AddParser(_exportPathParser);
            _commandLineParser.AddParser(_reportIdCmdLineParser);
            _commandLineParser.AddParser(_dateCmdLineParser);
            _commandLineParser.AddParser(_recalcCmdLineParser);
            _commandLineParser.AddParser(_dsmIdCmdLineParser);
            _commandLineParser.AddParser(_supervisorIdCmdLineParser);
            _commandLineParser.AddParser(_rmIdCmdLineParser);
            _commandLineParser.Parse(_runArguments);
        }
    }
}
