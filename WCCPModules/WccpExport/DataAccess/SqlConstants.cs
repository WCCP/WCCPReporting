namespace WccpExport.DataAccess {
    public class SqlConstants {
        public const string QueryLastSyncSuccessTime = @"select top 1 ParamValue from DW_Params where ParamName = 'Sync_LastSuccesTime'";
        public const string QueryLastUpdateData = @"select top 1 ParamValue from DW_Params where ParamName = 'Visits_With_Invoices_Update'";
        public const string SpDwOffTradeGetUpdateTime = "spDW_M_OffTrade_Get_UpdateTime";
        public const string SpRecalculateData = "dbo.spSW_ReCalculateData";
        
        //params
        public const string ParamDate = "@Date";
        public const string ParamRecalcDate = "RecalcDate";
        public const string ParamMonthCount = "monthCount";
    }
}