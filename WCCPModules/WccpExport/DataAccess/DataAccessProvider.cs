using System;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;

namespace WccpExport.DataAccess {
    public static class DataAccessProvider {
        public static DateTime GetLastSyncSuccessTime() {
            DateTime lDateTime;
            string lStrDateTime = string.Empty;
            object lRes = DataAccessLayer.ExecuteScalarQuery(SqlConstants.QueryLastSyncSuccessTime);

            if (lRes != null)
                lStrDateTime = (string) lRes;

            if (!DateTime.TryParse(lStrDateTime, out lDateTime))
                lDateTime = new DateTime(1900, 1, 1);
            return lDateTime;
        }

        public static DateTime GetLastUpdTime() {
            DateTime lDateTime;
            string lStrDateTime = (string) DataAccessLayer.ExecuteScalarQuery(SqlConstants.QueryLastUpdateData);
            if (!DateTime.TryParse(lStrDateTime, out lDateTime))
                lDateTime = new DateTime(1900, 1, 1);
            return lDateTime;
        }

        public static DateTime GetLastUpdateTime() {
            DateTime lDateTime = new DateTime(1900, 1, 1);
            object lRes = DataAccessLayer.ExecuteScalarStoredProcedure(SqlConstants.SpDwOffTradeGetUpdateTime,
                new[] {
                    new SqlParameter(SqlConstants.ParamDate, DateTime.Now)
                });
            
            if (lRes != null)
                lDateTime = (DateTime) lRes;

            return lDateTime;
        }

        public static void RecalculateData(DateTime date, int cmdTimeout = 3600, int monthCount = 1) {
            DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpRecalculateData, cmdTimeout,
                new[] {
                    new SqlParameter(SqlConstants.ParamRecalcDate, date),
                    new SqlParameter(SqlConstants.ParamMonthCount, monthCount)
                });
        }
    }
}