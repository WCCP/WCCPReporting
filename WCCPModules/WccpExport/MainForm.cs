﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.DataAccess;
using WccpExport.DataAccess;

namespace WccpExport {
    public partial class MainForm: Form {
        private IReportExporter _reportExporter;
        private int _reportId;
        private DateTime _reportDate;
        private int _timeOut = 30;
        private bool _recalc;
        private bool _isRptNotSelected = false;

        public MainForm() {
            InitializeComponent();
        }

        #region Properties

        public int ReportId {
            get { return _reportId; }
            set {
                if (value == -1)
                    _isRptNotSelected = true;

                if (_isRptNotSelected && DataAccessLayer.IsLDB) 
                {
                     _reportId = 129;//M1-M2 Off-trade
                    return;
                }

                _reportId = value;
            }
        }

        public DateTime ReportDate {
            get { return _reportDate; }
            set { _reportDate = value; }
        }

        public IReportExporter ReportExporter {
            get {
                if (_reportExporter == null)
                    CreateExporter();
                return _reportExporter;
            }
        }

        public bool Recalc {
            get { return _recalc; }
            set { _recalc = value; }
        }

        public int TimeOut {
            get { return _timeOut; }
            set { _timeOut = value; }
        }

        #endregion

        /// <summary>
        /// Starts processing of report
        /// </summary>
        public void StartRun() {
            try {
                if (null == _reportExporter) {
                    LogMsgHeader("Cannot run export with not recognized Report Id.");
                    return;
                }
                PrepareExportPath();

                if (DataAccessLayer.IsLDB && ReportId != 178)
                {
                    DateTime lLastSync = DataAccessProvider.GetLastSyncSuccessTime();
                    LogMsgHeader(string.Format("Last Sync time: {0}", lLastSync.ToString("yyyy-MM-dd HH:mm:ss")));

                    DateTime lLastRecalc = DataAccessProvider.GetLastUpdateTime();
                    LogMsgHeader(string.Format("Last Refresh data time (FSM & POCe): {0}",
                        lLastRecalc.ToString("yyyy-MM-dd HH:mm:ss")));

                    //if (_recalc || (DateTime.Now.Subtract(lLastRecalc) > TimeSpan.FromMinutes(_timeOut))) {
                    if (_recalc)
                    {
                        LogMsgHeader("Refreshing data (FSM & POCe)...");
                        DataAccessProvider.RecalculateData(ReportDate);
                        LogMsgHeader("Refreshing data completed.");
                    }
                }

                ReportExporter.SetReportDate(ReportDate);
                if (_recalc && _isRptNotSelected)
                    return;
                ReportExporter.ExportRun();
            }
            catch (Exception lException) {
                LogException(lException);
            }
        }

        public void CreateExporter() {
            if (_reportExporter != null)
                return;
            
            if (ReportId == 165)
            {
                _reportExporter = (IReportExporter)GetExporterInstance("wccp_m3KaTrade.dll");
            }
            else if (ReportId == 129)
            {
                _reportExporter = (IReportExporter)GetExporterInstance("wccpM1M2OffTrade.dll");
            }
            else if (ReportId == 178)
            {
                _reportExporter = (IReportExporter)GetExporterInstance("wccpEquipmentAccounting_new.dll");
            }
            else
                LogMsgHeader(string.Format("Report Id = {0} is not supported", _reportId));
        }

        private static object GetExporterInstance(string dllName) {
            Assembly lAssembly = Assembly.LoadFrom(Environment.CurrentDirectory + "\\" + dllName);
            FileVersionInfo lVersionInfo = FileVersionInfo.GetVersionInfo(lAssembly.Location);
            LogMsgHeader(string.Format("Module {0} of version {1} loaded.", dllName, lVersionInfo.FileVersion));

            Type[] lTypes = lAssembly.GetExportedTypes();
            Type lModuleType = null;
            for (int lIndex = 0; lIndex < lTypes.Length; lIndex++) {
                if (lTypes[lIndex].Name == "ReportExporter") {
                    lModuleType = lTypes[lIndex];
                    break;
                }
            }
            
            if (lModuleType == null) {
                LogMsgHeader(string.Format("Module {0} hasn't implemented ReportExporter!", dllName));
                return null;
            }

            return Activator.CreateInstance(lModuleType);
        }
        
        private void PrepareExportPath() {
            if (string.IsNullOrEmpty(ReportExporter.GetExportPath())) {
                string lExportPath = ConfigurationManager.AppSettings["ExportPath"];
                if (string.IsNullOrEmpty(lExportPath)) 
                    lExportPath = Environment.CurrentDirectory;
                if (!Directory.Exists(lExportPath))
                    Directory.CreateDirectory(lExportPath);
                if (!Directory.Exists(lExportPath))
                    lExportPath = Environment.CurrentDirectory;
                ReportExporter.SetExportPath(lExportPath);
            }
            LogMsgHeader("Used export path: " + ReportExporter.GetExportPath());
        }

        public static void LogMessage(string msg) {
            Console.WriteLine(msg);
        }

        public static void LogMsgHeader(string msg) {
            LogMessage(string.Format("{0} " + msg, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        }

        public static void LogException(Exception exception) {
            LogMsgHeader("\t" + exception.GetType().FullName);
            LogMessage("  Data: " + exception.Data);
            LogMessage("  Message: " + exception.Message);
            LogMessage("  Source: " + exception.Source);
            LogMessage("  StackTrace: " + exception.StackTrace);
            LogMessage("\n");
            
            if (exception.InnerException == null)
                return;
            LogMessage("Inner exception:");
            LogException(exception.InnerException);
        }

        // For testing
        private void RunReportBtn_Click(object sender, EventArgs e) {
            //TODO: add run report here
            string reportCaption = "M1-M2 Off Trade";
//            try {
//                wccpM1M2OffTradeSettingsForm setForm = new wccpM1M2OffTradeSettingsForm {ReportName = reportCaption};
//                if (setForm.ShowDialog() == DialogResult.OK) {
//                    WccpUIControl.AddForm(setForm);
//                    return WCCPAPI.CreateTabEx(reportCaption) > 0 ? 1 : 0;
//                    TabPage1.Text = reportCaption;
//                    TabPage1.PageVisible = false;
//                    WccpUIControl lRpt = new WccpUIControl();
//                    lRpt.Parent = TabPage1;
//                    lRpt.Dock = DockStyle.Fill;
//                    ExportToType lExportType = ExportToType.Pdf;
//                    string lFilePath = @"D:\Downloads\SS\ABInBev\ReportM1M2_off.pdf";
//                    lRpt.ExportReportToFile(lFilePath, lExportType);
//                }
//            }
//            catch (Exception lException) {
//                ErrorManager.ShowErrorBox(lException.Message);
//            }
        }
    }
}
