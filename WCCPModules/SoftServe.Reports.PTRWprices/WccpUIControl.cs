﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Core.Common.View;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.PTRWprices.Models;
using SoftServe.Reports.PTRWprices.Utils;
using DevExpress.XtraEditors.Controls;

namespace SoftServe.Reports.PTRWprices
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : SimpleControlView
    {
        #region Fields
        private SettingsForm _settingsForm;
        private Settings _currentSettings;
        private bool _isLoading;

        private const string COMPANY_DIRECTORY = "SoftServe";
        private const string PROGRAM_DIRECTORY = "WCCP Reporting";
        private const string LAYOUT_FILENAME = "PTRWprices.xml";
        #endregion

        #region Constructor

        public WccpUIControl()
        {
            InitializeComponent();
            _settingsForm = new SettingsForm();
            EnableEditButtons(false);
            Localizer.Active = new RussianEditorsLocalizer();
        }

        #endregion

        #region Methods
        public int InitReport()
        {
            WaitManager.StartWait();
            _settingsForm.LoadDataSources();
            WaitManager.StopWait();
            //RestoreLayout();
            return HandleSettingsForm() ? 0 : 1;
        }
        private void GenerateReport()
        {
            WaitManager.StartWait();
            string layoutPath = GetLayoutPath();
            //generate all selected reports

            foreach (XtraTabPage tab in tabControl.TabPages)
            {
                ITabReport report = null;
                IPriceType reportType = null;

                foreach (var control in tab.Controls)
                {
                    if (control is ITabReport && control is IPriceType)
                    {
                        report = control as ITabReport;
                        reportType = control as IPriceType;
                        break;
                    }
                }
                if (report != null && _currentSettings.PriceTypes.Contains(reportType.Type))
                {
                    report.LoadData(_currentSettings);
                    tab.PageVisible = true;
                    report.StartEdit += Report_StartEdit;
                    report.EndEdit += Report_EndEdit;
                }
                else
                    tab.PageVisible = false;

            }

            WaitManager.StopWait();
        }

        private void Report_EndEdit(object sender, EventArgs e)
        {
            EnableEditButtons(false);
            EnableMainButtons(true);
        }

        private void Report_StartEdit(object sender, EventArgs e)
        {
            EnableEditButtons(true);
            EnableMainButtons(false);
        }

        private bool HandleSettingsForm()
        {
            if (_settingsForm.HasError)
            {
                return false;
            }
            if (_settingsForm.ShowDialog(this) == DialogResult.OK)
            {
                _isLoading = true;
                _currentSettings = _settingsForm.Settings;
                btnEdit.Enabled = _currentSettings.UserLevel == AccessLevel.Admin;
                GenerateReport();
                _isLoading = !_isLoading;
                return true;
            }
            return false;
        }
        private string SelectFilePath(string reportCaption, ExportToType exportType)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = @"Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = reportCaption;
            sfd.Filter = String.Format("(*.{0})|*.{0}", exportType.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }

            return res;
        }
        private void ExportTab(ExportToType exportType)
        {
            string lPath = SelectFilePath(tabControl.SelectedTabPage.Text, exportType);

            if (!string.IsNullOrEmpty(lPath))
            {
                try
                {
                    //dataControl.Export(exportType, lPath);
                }
                catch (Exception)
                {
                    ErrorManager.ShowErrorBox("Ошибка при экспорте");
                }
            }
        }       

        #endregion      
       

        #region Save/restore layout

        private string GetLayoutPath()
        {
            string companyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), COMPANY_DIRECTORY);

            if (!Directory.Exists(companyDirPath))
            {
                Directory.CreateDirectory(companyDirPath);
            }

            string programDirPath = Path.Combine(companyDirPath, PROGRAM_DIRECTORY);

            if (!Directory.Exists(programDirPath))
            {
                Directory.CreateDirectory(programDirPath);
            }

            return Path.Combine(programDirPath, LAYOUT_FILENAME);
        }

        public void SaveLayout()
        {
            string layoutPath = GetLayoutPath();
            foreach (XtraTabPage tab in tabControl.TabPages)
            {
                foreach (var control in tab.Controls)
                {
                    try
                    {
                        var report = control as ITabReport;
                        if (report != null)
                        {
                            report.SaveLayout(layoutPath);
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }

            }
        }

        public void RestoreLayout()
        {
            string layoutPath = GetLayoutPath();
            foreach (XtraTabPage tab in tabControl.TabPages)
            {
                foreach (var control in tab.Controls)
                {
                    try
                    {
                        var report = control as ITabReport;
                        if (report != null)
                        {
                            report.RestoreLayout(layoutPath);
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
            }
        }

        #endregion

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            WaitManager.StartWait();            
            
                ITabReport report = null;
                IPriceType reportType = null;

                foreach (var control in tabControl.SelectedTabPage.Controls)
                {
                    if (control is ITabReport && control is IPriceType)
                    {
                        report = control as ITabReport;
                        reportType = control as IPriceType;
                        break;
                    }
                }
                if (report != null && _currentSettings.PriceTypes.Contains(reportType.Type))
                {
                    report.LoadData(_currentSettings);                   
                }            

            WaitManager.StopWait();
        }
        private void btnSetting_ItemClick(object sender, ItemClickEventArgs e)
        {
            HandleSettingsForm();
        }
        private void tabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
           
        }
        private void tabControl_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            ITabReport report = null;
            foreach (Control cnt in e.Page.Controls)
            {
                if (cnt is ITabReport)
                {
                    report = cnt as ITabReport;
                    break;
                }
            }
            if (report == null)
                return;
            EnableEditButtons(report.IsInEdit);
            EnableMainButtons(!report.IsInEdit);
        }
        private void btnEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            ITabReport report = null;
            foreach(Control cnt in tabControl.SelectedTabPage.Controls)
            {
                if (cnt is ITabReport)
                {
                    report = cnt as ITabReport;
                    break;
                }
            }
            if (report == null)
                return;

            report.GoEdit();
        }
        private void EnableEditButtons(bool enabled)
        {
            btnDeactiv.Enabled =
            btnActiv.Enabled = 
            btnOut.Enabled =
            btnSave.Enabled = enabled;
        }
        private void EnableMainButtons(bool enabled)
        {
            btnExportTo.Enabled =
            btnRefresh.Enabled =
            btnSetting.Enabled = enabled;
            btnEdit.Enabled = enabled && _currentSettings.UserLevel == AccessLevel.Admin;
        }

        private void btnDel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ITabReport report = null;
            foreach (Control cnt in tabControl.SelectedTabPage.Controls)
            {
                if (cnt is ITabReport)
                {
                    report = cnt as ITabReport;
                    break;
                }
            }
            if (report == null)
                return;

            report.DeactivateEdit();
        }

        private void btnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            ITabReport report = null;
            foreach (Control cnt in tabControl.SelectedTabPage.Controls)
            {
                if (cnt is ITabReport)
                {
                    report = cnt as ITabReport;
                    break;
                }
            }
            if (report == null)
                return;

            report.SaveEdit();
        }

        private void btnOut_ItemClick(object sender, ItemClickEventArgs e)
        {
            ITabReport report = null;
            foreach (Control cnt in tabControl.SelectedTabPage.Controls)
            {
                if (cnt is ITabReport)
                {
                    report = cnt as ITabReport;
                    break;
                }
            }
            if (report == null)
                return;

            report.StopEdit();
        }

        private void btnActiv_ItemClick(object sender, ItemClickEventArgs e)
        {
            ITabReport report = null;
            foreach (Control cnt in tabControl.SelectedTabPage.Controls)
            {
                if (cnt is ITabReport)
                {
                    report = cnt as ITabReport;
                    break;
                }
            }
            if (report == null)
                return;

            report.ActivateEdit();
        }

        private void btnExportToXlsx_ItemClick(object sender, ItemClickEventArgs e)
        {
            ITabReport report = null;
            foreach (Control cnt in tabControl.SelectedTabPage.Controls)
            {
                if (cnt is ITabReport)
                {
                    report = cnt as ITabReport;
                    break;
                }
            }
            if (report == null)
                return;

            string path = SelectFilePath(tabControl.SelectedTabPage.Text, ExportToType.Xlsx);

            if(!string.IsNullOrEmpty(path))
            report.Export(path);
        }
    }
}
