﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.Reports.PTRWprices
{
    class Program
    {
        public static void Main()
        {
            SettingsForm f = new SettingsForm();
            f.LoadDataSources();
            f.ShowDialog();
        }
    }
}
