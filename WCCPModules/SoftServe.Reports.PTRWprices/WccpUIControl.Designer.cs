﻿namespace SoftServe.Reports.PTRWprices
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.menuBar = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnSetting = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportToXlsx = new DevExpress.XtraBars.BarButtonItem();
            this.empty = new DevExpress.XtraBars.BarStaticItem();
            this.btnEdit = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnOut = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnActiv = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnDeactiv = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPTRPrices = new DevExpress.XtraTab.XtraTabPage();
            this.userControlPTR1 = new SoftServe.Reports.PTRWprices.Controls.UserControlPTR();
            this.tabPTWPrices = new DevExpress.XtraTab.XtraTabPage();
            this.userControlPTW1 = new SoftServe.Reports.PTRWprices.Controls.UserControlPTW();
            this.tabRegularDiscounts = new DevExpress.XtraTab.XtraTabPage();
            this.userControlRegularDiscounts1 = new SoftServe.Reports.PTRWprices.Controls.UserControlRegularDiscounts();
            this.tabRegularDiscountsNet = new DevExpress.XtraTab.XtraTabPage();
            this.userControlRegularDiscountsNet1 = new SoftServe.Reports.PTRWprices.Controls.UserControlRegularDiscountsNet();
            this.tabPromoDiscounts = new DevExpress.XtraTab.XtraTabPage();
            this.userControlPromoDiscounts1 = new SoftServe.Reports.PTRWprices.Controls.UserControlPromoDiscounts();
            this.tabPromoDiscountsNet = new DevExpress.XtraTab.XtraTabPage();
            this.userControlPromoDiscountsNet1 = new SoftServe.Reports.PTRWprices.Controls.UserControlPromoDiscountsNet();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPTRPrices.SuspendLayout();
            this.tabPTWPrices.SuspendLayout();
            this.tabRegularDiscounts.SuspendLayout();
            this.tabRegularDiscountsNet.SuspendLayout();
            this.tabPromoDiscounts.SuspendLayout();
            this.tabPromoDiscountsNet.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.menuBar,
            this.bar2,
            this.bar3});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnRefresh,
            this.btnSetting,
            this.btnEdit,
            this.btnSave,
            this.empty,
            this.btnDeactiv,
            this.btnActiv,
            this.btnOut,
            this.btnExportTo,
            this.btnExportToXlsx});
            this.barManager.LargeImages = this.imageCollection;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 22;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.barManager.StatusBar = this.bar3;
            // 
            // menuBar
            // 
            this.menuBar.BarName = "Tools";
            this.menuBar.DockCol = 0;
            this.menuBar.DockRow = 0;
            this.menuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.menuBar.FloatLocation = new System.Drawing.Point(730, 120);
            this.menuBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSetting),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTo, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.empty),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnOut),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnActiv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDeactiv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave)});
            this.menuBar.OptionsBar.AllowQuickCustomization = false;
            this.menuBar.OptionsBar.DisableClose = true;
            this.menuBar.OptionsBar.DisableCustomization = true;
            this.menuBar.OptionsBar.UseWholeRow = true;
            this.menuBar.Text = "Tools";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Обновить";
            this.btnRefresh.Id = 2;
            this.btnRefresh.LargeImageIndex = 0;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ShowCaptionOnBar = false;
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnSetting
            // 
            this.btnSetting.Caption = "Настройка параметров";
            this.btnSetting.Id = 3;
            this.btnSetting.LargeImageIndex = 2;
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.ShowCaptionOnBar = false;
            this.btnSetting.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSetting_ItemClick);
            // 
            // btnExportTo
            // 
            this.btnExportTo.Caption = "Экспортировать в ...";
            this.btnExportTo.Glyph = ((System.Drawing.Image)(resources.GetObject("btnExportTo.Glyph")));
            this.btnExportTo.Id = 18;
            this.btnExportTo.LargeImageIndex = 3;
            this.btnExportTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportToXlsx, DevExpress.XtraBars.BarItemPaintStyle.Standard)});
            this.btnExportTo.Name = "btnExportTo";
            // 
            // btnExportToXlsx
            // 
            this.btnExportToXlsx.Caption = "Xlsx";
            this.btnExportToXlsx.Glyph = ((System.Drawing.Image)(resources.GetObject("btnExportToXlsx.Glyph")));
            this.btnExportToXlsx.Id = 21;
            this.btnExportToXlsx.LargeImageIndex = 10;
            this.btnExportToXlsx.Name = "btnExportToXlsx";
            this.btnExportToXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportToXlsx_ItemClick);
            // 
            // empty
            // 
            this.empty.Id = 8;
            this.empty.Name = "empty";
            this.empty.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnEdit
            // 
            this.btnEdit.Caption = "Редактировать";
            this.btnEdit.Id = 6;
            this.btnEdit.LargeImageIndex = 7;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ShowCaptionOnBar = false;
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_ItemClick);
            // 
            // btnOut
            // 
            this.btnOut.Caption = "Выйти из режима редактирования";
            this.btnOut.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Left;
            this.btnOut.Id = 12;
            this.btnOut.LargeImageIndex = 8;
            this.btnOut.Name = "btnOut";
            this.btnOut.ShowCaptionOnBar = false;
            this.btnOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnOut_ItemClick);
            // 
            // btnActiv
            // 
            this.btnActiv.Caption = "Активировать";
            this.btnActiv.Id = 10;
            this.btnActiv.LargeImageIndex = 9;
            this.btnActiv.Name = "btnActiv";
            this.btnActiv.ShowCaptionOnBar = false;
            this.btnActiv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnActiv_ItemClick);
            // 
            // btnDeactiv
            // 
            this.btnDeactiv.Caption = "Деактивировать";
            this.btnDeactiv.Id = 9;
            this.btnDeactiv.LargeImageIndex = 10;
            this.btnDeactiv.Name = "btnDeactiv";
            this.btnDeactiv.ShowCaptionOnBar = false;
            this.btnDeactiv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDel_ItemClick);
            // 
            // btnSave
            // 
            this.btnSave.Caption = "Сохранить";
            this.btnSave.Id = 7;
            this.btnSave.LargeImageIndex = 5;
            this.btnSave.Name = "btnSave";
            this.btnSave.ShowCaptionOnBar = false;
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            this.bar2.Visible = false;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1405, 59);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 563);
            this.barDockControlBottom.Size = new System.Drawing.Size(1405, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 59);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 504);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1405, 59);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 504);
            // 
            // imageCollection
            // 
            this.imageCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "refresh_24.png");
            this.imageCollection.Images.SetKeyName(1, "Excel.png");
            this.imageCollection.Images.SetKeyName(2, "briefcase_ok.png");
            this.imageCollection.Images.SetKeyName(3, "image_refresh.png");
            this.imageCollection.Images.SetKeyName(4, "csv.png");
            this.imageCollection.Images.SetKeyName(5, "Save-icon (2).png");
            this.imageCollection.Images.SetKeyName(6, "xslx.png");
            this.imageCollection.Images.SetKeyName(7, "Text-Edit-icon.png");
            this.imageCollection.Images.SetKeyName(8, "Log-Out-icon.png");
            this.imageCollection.Images.SetKeyName(9, "Actions-arrow-right-icon.png");
            this.imageCollection.Images.SetKeyName(10, "Stop-32-icon (2).png");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tabControl.Location = new System.Drawing.Point(0, 59);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabPTRPrices;
            this.tabControl.Size = new System.Drawing.Size(1405, 504);
            this.tabControl.TabIndex = 4;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPTRPrices,
            this.tabPTWPrices,
            this.tabRegularDiscounts,
            this.tabRegularDiscountsNet,
            this.tabPromoDiscounts,
            this.tabPromoDiscountsNet});
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            this.tabControl.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.tabControl_SelectedPageChanging);
            // 
            // tabPTRPrices
            // 
            this.tabPTRPrices.Controls.Add(this.userControlPTR1);
            this.tabPTRPrices.Name = "tabPTRPrices";
            this.tabPTRPrices.Size = new System.Drawing.Size(1399, 476);
            this.tabPTRPrices.Text = "PTR цены";
            // 
            // userControlPTR1
            // 
            this.userControlPTR1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlPTR1.Location = new System.Drawing.Point(0, 0);
            this.userControlPTR1.Name = "userControlPTR1";
            this.userControlPTR1.Size = new System.Drawing.Size(1399, 476);
            this.userControlPTR1.TabIndex = 0;
            // 
            // tabPTWPrices
            // 
            this.tabPTWPrices.Controls.Add(this.userControlPTW1);
            this.tabPTWPrices.Name = "tabPTWPrices";
            this.tabPTWPrices.Size = new System.Drawing.Size(1399, 476);
            this.tabPTWPrices.Text = "PTW цены";
            // 
            // userControlPTW1
            // 
            this.userControlPTW1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlPTW1.Location = new System.Drawing.Point(0, 0);
            this.userControlPTW1.Name = "userControlPTW1";
            this.userControlPTW1.Size = new System.Drawing.Size(1399, 476);
            this.userControlPTW1.TabIndex = 0;
            // 
            // tabRegularDiscounts
            // 
            this.tabRegularDiscounts.Controls.Add(this.userControlRegularDiscounts1);
            this.tabRegularDiscounts.Name = "tabRegularDiscounts";
            this.tabRegularDiscounts.Size = new System.Drawing.Size(1399, 476);
            this.tabRegularDiscounts.Text = "Регулярные скидки по ТТ";
            // 
            // userControlRegularDiscounts1
            // 
            this.userControlRegularDiscounts1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlRegularDiscounts1.Location = new System.Drawing.Point(0, 0);
            this.userControlRegularDiscounts1.Name = "userControlRegularDiscounts1";
            this.userControlRegularDiscounts1.Size = new System.Drawing.Size(1399, 476);
            this.userControlRegularDiscounts1.TabIndex = 0;
            // 
            // tabRegularDiscountsNet
            // 
            this.tabRegularDiscountsNet.Controls.Add(this.userControlRegularDiscountsNet1);
            this.tabRegularDiscountsNet.Name = "tabRegularDiscountsNet";
            this.tabRegularDiscountsNet.Size = new System.Drawing.Size(1399, 476);
            this.tabRegularDiscountsNet.Text = "Регулярные скидки в сети";
            // 
            // userControlRegularDiscountsNet1
            // 
            this.userControlRegularDiscountsNet1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlRegularDiscountsNet1.Location = new System.Drawing.Point(0, 0);
            this.userControlRegularDiscountsNet1.Name = "userControlRegularDiscountsNet1";
            this.userControlRegularDiscountsNet1.Size = new System.Drawing.Size(1399, 476);
            this.userControlRegularDiscountsNet1.TabIndex = 0;
            // 
            // tabPromoDiscounts
            // 
            this.tabPromoDiscounts.Controls.Add(this.userControlPromoDiscounts1);
            this.tabPromoDiscounts.Name = "tabPromoDiscounts";
            this.tabPromoDiscounts.Size = new System.Drawing.Size(1399, 476);
            this.tabPromoDiscounts.Text = "Промо скидки по АП";
            // 
            // userControlPromoDiscounts1
            // 
            this.userControlPromoDiscounts1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlPromoDiscounts1.Location = new System.Drawing.Point(0, 0);
            this.userControlPromoDiscounts1.Name = "userControlPromoDiscounts1";
            this.userControlPromoDiscounts1.Size = new System.Drawing.Size(1399, 476);
            this.userControlPromoDiscounts1.TabIndex = 0;
            // 
            // tabPromoDiscountsNet
            // 
            this.tabPromoDiscountsNet.Controls.Add(this.userControlPromoDiscountsNet1);
            this.tabPromoDiscountsNet.Name = "tabPromoDiscountsNet";
            this.tabPromoDiscountsNet.Size = new System.Drawing.Size(1399, 476);
            this.tabPromoDiscountsNet.Text = "Промо скидки в сети";
            // 
            // userControlPromoDiscountsNet1
            // 
            this.userControlPromoDiscountsNet1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlPromoDiscountsNet1.Location = new System.Drawing.Point(0, 0);
            this.userControlPromoDiscountsNet1.Name = "userControlPromoDiscountsNet1";
            this.userControlPromoDiscountsNet1.Size = new System.Drawing.Size(1399, 476);
            this.userControlPromoDiscountsNet1.TabIndex = 0;
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(1405, 586);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPTRPrices.ResumeLayout(false);
            this.tabPTWPrices.ResumeLayout(false);
            this.tabRegularDiscounts.ResumeLayout(false);
            this.tabRegularDiscountsNet.ResumeLayout(false);
            this.tabPromoDiscounts.ResumeLayout(false);
            this.tabPromoDiscountsNet.ResumeLayout(false);
            this.ResumeLayout(false);

        }


        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar menuBar;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraBars.BarLargeButtonItem btnRefresh;
        private DevExpress.XtraBars.BarLargeButtonItem btnSetting;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabRegularDiscounts;
        private DevExpress.XtraTab.XtraTabPage tabPromoDiscounts;
        private DevExpress.XtraTab.XtraTabPage tabPTRPrices;
        private DevExpress.XtraTab.XtraTabPage tabPTWPrices;
        private DevExpress.XtraTab.XtraTabPage tabRegularDiscountsNet;
        private DevExpress.XtraTab.XtraTabPage tabPromoDiscountsNet;
        private Controls.UserControlPTR userControlPTR1;
        private Controls.UserControlPTW userControlPTW1;
        private Controls.UserControlRegularDiscounts userControlRegularDiscounts1;
        private Controls.UserControlRegularDiscountsNet userControlRegularDiscountsNet1;
        private Controls.UserControlPromoDiscounts userControlPromoDiscounts1;
        private Controls.UserControlPromoDiscountsNet userControlPromoDiscountsNet1;
        private DevExpress.XtraBars.BarStaticItem empty;
        private DevExpress.XtraBars.BarLargeButtonItem btnEdit;
        private DevExpress.XtraBars.BarLargeButtonItem btnSave;
        private DevExpress.XtraBars.BarLargeButtonItem btnDeactiv;
        private DevExpress.XtraBars.BarLargeButtonItem btnActiv;
        private DevExpress.XtraBars.BarLargeButtonItem btnOut;      
        private DevExpress.XtraBars.BarSubItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem btnExportToXlsx;
    }
}
