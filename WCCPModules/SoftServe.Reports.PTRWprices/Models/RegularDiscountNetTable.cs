﻿using System;
using System.Data;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices.Models
{
    class RegularDiscountNetTable : DataTable, ICloneErrorTable, IPriceType
    {
        DataColumn col1 = new DataColumn("Cust_Id", typeof(string));
        DataColumn col2 = new DataColumn("GrdCode", typeof(string));
        DataColumn col3 = new DataColumn("PercentD", typeof(string));
        DataColumn col4 = new DataColumn("StartDate", typeof(string));
        DataColumn col5 = new DataColumn("EndDate", typeof(string));
        public RegularDiscountNetTable()
        {
            this.Columns.AddRange(new DataColumn[]
           {
                 col1,
                 col2,
                 col3,
                 col4,
                 col5
           });
            Type = PtRwTypes.RegularDiscountsNet;
        }

        public PtRwTypes Type { get; set; }

        public DataTable CreaErrorableTable()
        {
            RegularDiscountNetTable t = new RegularDiscountNetTable();
            t.Columns.Add(new DataColumn("ErrorMessage", typeof(string)));
            return t;
        }
    }
}