﻿using System;
using System.Data;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices.Models
{
    class PTWPriceTable : DataTable, ICloneErrorTable, IPriceType
    {
        DataColumn col1 = new DataColumn("Cust_Id", typeof(string));
        DataColumn col2 = new DataColumn("HLCode", typeof(string));
        DataColumn col3 = new DataColumn("ProductCode", typeof(string));
        DataColumn col4 = new DataColumn("Price", typeof(string));
        DataColumn col5 = new DataColumn("StartDate", typeof(string));
        DataColumn col6 = new DataColumn("EndDate", typeof(string));
        
        public PTWPriceTable()
        {
            this.Columns.AddRange(new DataColumn[]
            {
                 col1,
                 col2,
                 col3,
                 col4,
                 col5,
                 col6
            });
            Type = PtRwTypes.PTWPrices;
        }

        public PtRwTypes Type { get; private set; }

        public DataTable CreaErrorableTable()
        {
            PTWPriceTable t = new PTWPriceTable();
            t.Columns.Add(new DataColumn("ErrorMessage", typeof(string)));
            return t;
        }
    }
}