﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.PTRWprices.Models
{
    //public abstract class BaseSourceModel : INotifyPropertyChanged
    //{
    //    public event PropertyChangedEventHandler PropertyChanged;
    //    public virtual long Id { get; set; }
    //    public virtual DateTime StartDate { get; set; }
    //    public virtual DateTime EndDate { get; set; }
    //    public abstract bool HasValuesChanged { get; }
    //    public abstract bool IsStatusModified { get; }
    //    public abstract void EnablePropetyNotify(bool enable);
    //    public abstract decimal GetValue();
    //    public abstract int GetStatus();
    //}
    public interface ISourceModel : INotifyPropertyChanged
    { 
         bool Check { get; set; }       
         long Id { get; set; }
         DateTime StartDate { get; set; }
         DateTime EndDate { get; set; }
         bool HasValuesChanged { get; }
         bool IsStatusModified { get; }
         void EnablePropetyNotify(bool enable);
         decimal GetValue();
         int GetStatus();
    }
}
