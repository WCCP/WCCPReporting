﻿using System;
using System.Data;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices.Models
{
    class PromoDiscountTable : DataTable, ICloneErrorTable, IPriceType
    {
        DataColumn col1 = new DataColumn("Cust_Id", typeof(string));
        DataColumn col2 = new DataColumn("Cust_NAME", typeof(string));
        DataColumn col3 = new DataColumn("ProductCode", typeof(string));
        DataColumn col4 = new DataColumn("ProductName", typeof(string));
        DataColumn col5 = new DataColumn("PercentD", typeof(string));
        DataColumn col6 = new DataColumn("StartDate", typeof(string));
        DataColumn col7 = new DataColumn("EndDate", typeof(string));
        DataColumn col8 = new DataColumn("NomerPS", typeof(string));
        DataColumn col9 = new DataColumn("NamePS", typeof(string));

        public PromoDiscountTable()
        {
            this.Columns.AddRange(new DataColumn[]
            {
                 col1,
                 col2,
                 col3,
                 col4,
                 col5,
                 col6,
                 col7,
                 col8,
                 col9
            });
            Type = PtRwTypes.PromoDiscounts;
        }

        public PtRwTypes Type{get; private set; }

        public DataTable CreaErrorableTable()
        {
            PromoDiscountTable t = new PromoDiscountTable();
            t.Columns.Add(new DataColumn("ErrorMessage", typeof(string)));
            return t;
        }
    }
}
