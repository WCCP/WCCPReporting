﻿using System;
using System.Data;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices.Models
{
    class RegularDiscountTable : DataTable, ICloneErrorTable, IPriceType
    {
        DataColumn col1 = new DataColumn("Cust_Id", typeof(string));
        DataColumn col2 = new DataColumn("Ol_ID", typeof(string));
        DataColumn col3 = new DataColumn("PercentD", typeof(string));
        DataColumn col4 = new DataColumn("StartDate", typeof(string));
        DataColumn col5 = new DataColumn("EndDate", typeof(string));
 

        public RegularDiscountTable()
        {
            this.Columns.AddRange(new DataColumn[]
            {
                 col1,
                 col2,
                 col3,
                 col4,
                 col5
            });
            Type = PtRwTypes.RegularDiscounts;
        }

        public PtRwTypes Type { get; private set; }

        public DataTable CreaErrorableTable()
        {
            RegularDiscountTable t = new RegularDiscountTable();
            t.Columns.Add(new DataColumn("ErrorMessage", typeof(string)));
            return t;
        }
    }
}