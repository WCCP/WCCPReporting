﻿using System;
using System.ComponentModel;
using BLToolkit.Mapping;

namespace SoftServe.Reports.PTRWprices.Models
{
    class PTW : ISourceModel
    {
        private bool _isNotifyOn = false;
        public event PropertyChangedEventHandler PropertyChanged;
        private decimal _price;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _status;

        // Original
        private decimal _priceOrig;
        private DateTime _startDateOrig;
        private DateTime _endDateOrig;
        private int _statusOrig;
        public bool Check { get; set; }

        [MapField("Id")]
        public long Id { get; set; }       

        [MapField("Cust_Id")]
        public /*int*/ string Cust_Id { get; set; }

        [MapField("Cust_NAME")]
        public string Cust_NAME { get; set; }
       
        [MapField("HLCode")]
        public string HLCode { get; set; }

        [MapField("ProductCnName")]
        public string ProductCnName { get; set;
        }
        [MapField("ProductCode")]
        public string ProductCode { get; set; }

        [MapField("ProductName")]
        public string ProductName { get; set; }

        [MapField("Price")]
        public decimal Price
        {
            get { return _price; }
            set
            {
                if (!_isNotifyOn)
                {
                    _price = value;
                }
                else
                {
                    if (_price != value)
                    {
                        _price = value;
                        OnNotify("Price");
                    }
                }
            }
        }

        [MapField("StartDate")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                if (!_isNotifyOn)
                {
                    _startDate = value;
                }
                else
                {
                    if (_startDate != value)
                    {
                        _startDate = value;
                        OnNotify("StartDate");
                    }
                }
            }
        }

        [MapField("EndDate")]
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                if (!_isNotifyOn)
                {
                    _endDate = value;
                }
                else
                {
                    if (_endDate != value)
                    {
                        _endDate = value;
                        OnNotify("EndDate");
                    }
                }
            }
        }

        [MapField("status")]
        public int status
        {
            get { return _status; }
            set
            {
                if (!_isNotifyOn)
                {
                    _status = value;
                }
                else
                {
                    if (_status != value)
                    {
                        _status = value;
                        OnNotify("Status");
                    }
                }
            }
        }

        [MapField("login")]
        public string login { get; set; }

        [MapField("dlm")]
        public string dlm { get; set; }

        [MapField("Country_id")]
        public string Country_id { get; set; }

        [MapField("Country_name")]
        public string Country_name { get; set; }

        [MapField("ErrorMessage")]
        public string ErrorMessage { get; set; }

        private void OnNotify(string propName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public bool IsStatusModified
        {
            get
            { return _statusOrig != _status; }
        }
        public bool HasValuesChanged
        {
            get
            {
                if ((_priceOrig == _price) && (_startDateOrig.Equals(_startDate)) && (_endDateOrig.Equals(_endDate)))
                {
                    return false;
                }
                return true;
            }
        }
        private void SaveOriginals()
        {
            _priceOrig = _price;
            _startDateOrig = _startDate;
            _endDateOrig = _endDate;
            _statusOrig = _status;
        }
        public void EnablePropetyNotify(bool enable)
        {
            SaveOriginals();
            _isNotifyOn = enable;
        }
        public decimal GetValue()
        {
            return Price;
        }
        public int GetStatus()
        {
            return status;
        }
    }
}