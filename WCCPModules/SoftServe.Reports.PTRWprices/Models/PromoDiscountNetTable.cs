﻿using System;
using System.Data;
using System.Drawing.Text;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices.Models
{
    class PromoDiscountNetTable : DataTable, ICloneErrorTable, IPriceType
    {
        DataColumn col1 = new DataColumn("DISTR_Id", typeof(string));
        DataColumn col2 = new DataColumn("Cust_Id", typeof(string));
        DataColumn col3 = new DataColumn("GrdCode", typeof(string));
        DataColumn col4 = new DataColumn("HLCode", typeof(string));
        DataColumn col5 = new DataColumn("PercentD", typeof(string));
        DataColumn col6 = new DataColumn("StartDate", typeof(string));
        DataColumn col7 = new DataColumn("EndDate", typeof(string));

        public PromoDiscountNetTable()
        {
            this.Columns.AddRange(new DataColumn[]
            {
                 col1,
                 col2,
                 col3,
                 col4,
                 col5,
                 col6,
                 col7
            });
            Type = PtRwTypes.PromoDiscountsNet;
        }
        public PtRwTypes Type { get; private set; }
        public DataTable CreaErrorableTable()
        {
            PromoDiscountNetTable t = new PromoDiscountNetTable();
            t.Columns.Add(new DataColumn("ErrorMessage", typeof(string)));
            return t;
        }
    }
}