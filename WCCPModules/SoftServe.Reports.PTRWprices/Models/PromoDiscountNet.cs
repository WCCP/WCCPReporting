﻿using System;
using System.ComponentModel;
using BLToolkit.Mapping;

namespace SoftServe.Reports.PTRWprices.Models
{
    class PromoDiscountNet : ISourceModel
    {
        private bool _isNotifyOn = false;
        public event PropertyChangedEventHandler PropertyChanged;
        private decimal _percent;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _status;

        // Original
        private decimal _percentOrig;
        private DateTime _startDateOrig;
        private DateTime _endDateOrig;
        private int _statusOrig;
        public bool Check { get; set; }

        [MapField("Id")]
        public long Id { get; set; }

        [MapField("DISTR_Id")]
        public /*int*/ string DISTR_Id { get; set; }

        [MapField("DISTR_NAME")]
        public string DISTR_NAME { get; set; }

        [MapField("Cust_Id")]
        public /*int*/ string Cust_Id { get; set; }

        [MapField("Cust_NAME")]
        public string Cust_NAME { get; set; }
        [MapField("GrdCode")]
        public string GrdCode { get; set; }

        [MapField("Network_Name")]
        public string Network_Name { get; set; }
        [MapField("HLCode")]
        public string HLCode { get; set; }

        [MapField("ProductCnName")]
        public string ProductCnName { get; set; }

        [MapField("PercentD")]
        public decimal PercentD
        {
            get { return _percent; }
            set
            {
                if (!_isNotifyOn)
                {
                    _percent = value;
                }
                else
                {
                    if (_percent != value)
                    {
                        _percent = value;
                        OnNotify("PercentD");
                    }
                }
            }
        }

        [MapField("StartDate")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                if (!_isNotifyOn)
                {
                    _startDate = value;
                }
                else
                {
                    if (_startDate != value)
                    {
                        _startDate = value;
                        OnNotify("StartDate");
                    }
                }
            }
        }

        [MapField("EndDate")]
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                if (!_isNotifyOn)
                {
                    _endDate = value;
                }
                else
                {
                    if (_endDate != value)
                    {
                        _endDate = value;
                        OnNotify("EndDate");
                    }
                }
            }
        }

        [MapField("status")]
        public int status
        {
            get { return _status; }
            set
            {
                if (!_isNotifyOn)
                {
                    _status = value;
                }
                else
                {
                    if (_status != value)
                    {
                        _status = value;
                        OnNotify("status");
                    }
                }
            }
        }

        [MapField("login")]
        public string login { get; set; }

        [MapField("dlm")]
        public string dlm { get; set; }

        [MapField("country_id")]
        public string country_id { get; set; }

        [MapField("Country_name")]
        public string Country_name { get; set; }

        [MapField("ErrorMessage")]
        public string ErrorMessage { get; set; }

        private void OnNotify(string propName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public bool IsStatusModified
        {
            get
            { return _statusOrig != _status; }
        }
        public bool HasValuesChanged
        {
            get
            {
                if ((_percentOrig == _percent) && (_startDateOrig.Equals(_startDate)) && (_endDateOrig.Equals(_endDate)))
                {
                    return false;
                }
                return true;
            }
        }
        private void SaveOriginals()
        {
            _percentOrig = _percent;
            _startDateOrig = _startDate;
            _endDateOrig = _endDate;
            _statusOrig = _status;
        }
        public void EnablePropetyNotify(bool enable)
        {
            SaveOriginals();
            _isNotifyOn = enable;
        }
        public decimal GetValue()
        {
            return PercentD;
        }
        public int GetStatus()
        {
            return status;
        }
    }
}
