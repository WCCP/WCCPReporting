﻿using System;
using System.Collections.Generic;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices.Models
{
   public class Settings
    {
        public AccessLevel UserLevel { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public List<string> DistributorsId { get; set; }
        public List<string> CustomersId { get; set; }
        public List<string> NetworksId { get; set; }
        public List<string> ProductsId { get; set; }
        public List<PtRwTypes> PriceTypes { get; set; }
        public int PriceStatus { get; set; }        
        public string CountryId { get; set; }
        public string Validate()
        {
            string msg = string.Empty;
            if (DateFrom == DateTime.MinValue || DateTo == DateTime.MinValue)
            {
                msg = string.Concat(msg, "\nДата не может быть пустой!");
            }
            if (DateFrom.Date > DateTo.Date)
                msg = string.Concat(msg, "\nДата начала периода просмотра не может быть больше даты окончания!");
            if (CountryId == string.Empty)
            {
                msg = string.Concat(msg, "\nНе выбрано ни одного региона!");
            }
            if (DistributorsId.Count == 0)
            {
                msg = string.Concat(msg, "\nНе выбрано ни одного дистрибутора!");
            }
            if (CustomersId.Count == 0)
            {
                msg = string.Concat(msg, "\nНе выбрано ни одной ТС!");
            }
            if (NetworksId.Count == 0)
            {
                msg = string.Concat(msg, "\nНе выбрано ни одной сети!");
            }
            if (PriceTypes.Count == 0)
            {
                msg = string.Concat(msg, "\nНе выбрано ни одного отчета!");
            }            
            if(!string.IsNullOrEmpty(msg))
                msg = msg.Remove(0, 1);

            return msg;
        }               
    }

    public enum AccessLevel
    {
        NoAccess,
        Reader,
        Admin
    }
}
