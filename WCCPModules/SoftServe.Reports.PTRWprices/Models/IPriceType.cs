﻿using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices.Models
{
    public interface IPriceType
    {
        PtRwTypes Type { get; }
    }
}
