﻿using System.Data;
namespace SoftServe.Reports.PTRWprices.Models
{
    public interface ICloneErrorTable
    {
        DataTable CreaErrorableTable();
    }
}