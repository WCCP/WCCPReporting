﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftServe.Reports.PTRWprices.Utils;
using SoftServe.Reports.PTRWprices.Models;

namespace SoftServe.Reports.PTRWprices.Utils
{
    public interface ITabReportView : ITabReport, IPriceType
    {
        GridView View { get; }
        void OnEditMOde();
        void OnCancelEditMode();
    }
}
