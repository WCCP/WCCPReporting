﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.PTRWprices.Utils
{
    public class TemplateUploader
    {
        public void UploadTemplateFile(PtRwTypes type, string path)        {         

            byte[] file;
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new BinaryReader(stream))
                {
                    file = reader.ReadBytes((int)stream.Length);
                }
            }
            DataAccess.DataProvider.UploadTemplateFile(file, type);
        }

        public void DownloadTemplateFile(PtRwTypes type)
        {

            byte[] file = DataAccess.DataProvider.DownloadTemplateFile(type);
            string lPath = string.Empty;

            string DIALOG_OPEN_FILE_FILTER = "Excel файл (*.xlsx)|*.xlsx";
            string DIALOG_OPEN_FILE = "Сохранить шаблон";
            string fileName = Enum.GetName(typeof(PtRwTypes), type);
            SaveFileDialog lFileDialog = new SaveFileDialog()
            {
                Title = DIALOG_OPEN_FILE,
                Filter = DIALOG_OPEN_FILE_FILTER,
                FileName = fileName
            };
            
            lFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            DialogResult result = lFileDialog.ShowDialog();
                       
            if (result == DialogResult.Cancel)
                return;
            lPath = lFileDialog.FileName;

            try
            {
                using (var fs = new FileStream(lPath, FileMode.Create, FileAccess.Write))
                    fs.Write(file, 0, file.Length);
                XtraMessageBox.Show("Шаблон успешно сохранен", "Сохранение");
            }
            catch (IOException e)
            {
                XtraMessageBox.Show(string.Format("Произошла ошибка: {0}", e.Message), "Сохранение");
            }
        }
    }
}
