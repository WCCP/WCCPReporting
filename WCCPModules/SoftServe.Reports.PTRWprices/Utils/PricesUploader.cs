﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.PTRWprices.DataAccess;
using SoftServe.Reports.PTRWprices.Models;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices
{
    internal class PricesUploader
    {
        private DataTable _data;
        private DataTable _invalidData;

        private Dictionary<int, string> _errorCodes;

        private List<string> _columnList;
        private bool HasIncorrectPlans
        {
            get { return _invalidData.Rows.Count > 0; }
        }
        public PricesUploader(PtRwTypes type)
        {
            switch (type)
            {
                case PtRwTypes.PTRPrices: _data = new PTRPriceTable(); break;
                case PtRwTypes.PTWPrices: _data = new PTWPriceTable(); break;
                case PtRwTypes.PromoDiscounts: _data = new PromoDiscountTable(); break;
                case PtRwTypes.PromoDiscountsNet: _data = new PromoDiscountNetTable(); break;
                case PtRwTypes.RegularDiscounts: _data = new RegularDiscountTable(); break;
                case PtRwTypes.RegularDiscountsNet: _data = new RegularDiscountNetTable(); break;
                default: _data = new DataTable(); break;
            }
        }
        public void UploadPlans(string path)
        {
            string lFilePath = path;

            if (lFilePath == string.Empty) // file wasn't chosen
                return;

            WaitManager.StartWait();

            if (!ReadData(lFilePath))
            {
                WaitManager.StopWait();
                return;
            }
            WaitManager.StopWait();

          if (_data.Rows.Count <= 1)
            {
                XtraMessageBox.Show("Корректных данных для загрузки не обнаружено", "Загрузка из EXCEL", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }

            try
            {
                _invalidData = DataProvider.SaveLoadedPlans(_data);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Загрузка из EXCEL", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }

            if (_invalidData.Rows.Count > 0)
            {
                FormImportErrors f = new FormImportErrors();
                f.SetDataSource(_invalidData);
                f.Show();
            }
            else
            XtraMessageBox.Show("Данные успешно загружены", "Загрузка из EXCEL", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }
        private bool ReadData(string filePath)
        {
            ExcelDataProvier excelProvider = new ExcelDataProvier(filePath, _data);

            bool wasReadingSuccsessful = true;

            try
            {
                excelProvider.TryToReadFile();
            }
            catch (IOException)
            {
                XtraMessageBox.Show("Файл уже открыт в другом приложении!", "Загрузка из EXCEL", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                wasReadingSuccsessful = false;

                return wasReadingSuccsessful;
            }
            catch (FormatException)
            {
                XtraMessageBox.Show("Некорректный формат файла! Нужно использовать xlsx, xls  форматы", "Загрузка данных",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                wasReadingSuccsessful = false;

                return wasReadingSuccsessful;
            }
            try
            {
                excelProvider.RaadFileData();
            }
            catch (Exception e)
            {
                XtraMessageBox.Show("Некорректный формат файла!", "Загрузка из EXCEL", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                wasReadingSuccsessful = false;
            }
           
            return wasReadingSuccsessful;
        }
        public static string ChooseFile()
        {
            string lPath = string.Empty;

            string DIALOG_OPEN_FILE_FILTER = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            string DIALOG_OPEN_FILE = "Загрузить файл";

            OpenFileDialog lFileDialog = new OpenFileDialog
            {
                Filter = DIALOG_OPEN_FILE_FILTER,
                Title = DIALOG_OPEN_FILE,
                CheckFileExists = true,
                RestoreDirectory = true
            };

            if (lFileDialog.ShowDialog() == DialogResult.OK)
            {
                lPath = lFileDialog.FileName;
            }

            return lPath;
        }
    }
}
