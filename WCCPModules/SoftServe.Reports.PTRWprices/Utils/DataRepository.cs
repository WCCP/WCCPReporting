﻿using System.Collections.Generic;
using System.Data;
using SoftServe.Reports.PTRWprices.DataAccess;
using SoftServe.Reports.PTRWprices.Models;
using System.Linq;

namespace SoftServe.Reports.PTRWprices.Utils
{
    public static class DataRepository
    {
        private static DataTable _pricesTypes = null;
        private static DataTable _regions = null;
        private static DataTable _distributors = null;
        private static DataTable _customers = null;
        private static DataTable _skuTree = null;
        private static DataTable distributorsDistinct = null;


        public static DataTable PricesTypes
        {
            get
            {
                if (_pricesTypes == null)
                {
                    _pricesTypes = DataProvider.GetPricesTypes();
                }
                return _pricesTypes;
            }
        }
        public static DataTable Regions
        {
            get
            {
                if (_regions == null)
                {
                    _regions = DataProvider.GetRegions();
                }                
                    return _regions;               
            }
        }
        public static DataTable Distributors
        {
            get
            {
                if (_distributors == null)
                {
                    _distributors = DataProvider.GetDistributors();
                }
               
                    return _distributors;               
            }
        }
        public static DataTable DistributorsDistinct
        {
            get
            {
                if (distributorsDistinct == null)
                {
                    DataTable lTable = Distributors.Copy();
                    lTable.Columns.Remove("region_id");
                    lTable.Columns.Remove("region_name");
                    distributorsDistinct = lTable.DefaultView.ToTable(true, "Distr_id", "DISTR_NAME");
                }
                return distributorsDistinct;
            }
        }
        public static DataTable Customers
        {
            get
            {
                if (_customers == null)
                {
                    _customers = DataProvider.GetCustomers();
                }                
                    return _customers;
            }
        }
        public static DataTable SKUTree
        {
            get
            {
                if (_skuTree == null)
                {
                    _skuTree = DataProvider.GetProductTree(null);
                }
                return _skuTree;
            }
            
        }       
    }
}