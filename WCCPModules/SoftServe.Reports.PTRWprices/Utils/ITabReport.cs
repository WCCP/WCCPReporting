﻿using System;
using DocumentFormat.OpenXml.Packaging;
using SoftServe.Reports.PTRWprices.Models;
using SoftServe.Reports.PTRWprices.Utils;

namespace SoftServe.Reports.PTRWprices
{
    public interface ITabReport
    {
        event EventHandler StartEdit;
        event EventHandler EndEdit;
        bool IsInEdit { get; }
        //string ReportText { get;  }

        void LoadData(Settings settings);

        void SaveLayout(string path);

        void RestoreLayout(string path);

        void Export(string path);

        void GoEdit();
        void StopEdit();
        void SaveEdit();
        void DeactivateEdit();
        void ActivateEdit();
    }
}
