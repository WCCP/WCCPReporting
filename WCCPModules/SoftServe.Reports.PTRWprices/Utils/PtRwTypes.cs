﻿namespace SoftServe.Reports.PTRWprices.Utils
{
    public enum PtRwTypes
    {
        PTRPrices = 1,
        PTWPrices,
        RegularDiscounts,
        RegularDiscountsNet,
        PromoDiscountsNet,
        PromoDiscounts
    }
}