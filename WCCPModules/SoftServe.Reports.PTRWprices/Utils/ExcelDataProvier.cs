﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Globalization;
using CellType = NPOI.SS.UserModel.CellType;

namespace SoftServe.Reports.PTRWprices.Utils
{
    class ExcelDataProvier
    {
        private string _filePath;
        private DataTable _table;
        private ISheet _sheet;

        private List<int> fieldDecimalIndx = new List<int>();
        private List<int> fieldDataIndx = new List<int>();
        private Dictionary<string, Type> listFieldsTypeToConvert = new Dictionary<string, Type>()
        {
            {"Price", typeof (decimal)},
            {"PercentD", typeof (decimal)},
            {"StartDate", typeof (DateTime)},
            {"EndDate", typeof (DateTime)}
        };
        public ExcelDataProvier(string filePath, DataTable table)
        {
            _filePath = filePath;
            _table = table;

            List<string> colFieldNames = table.Columns.Cast<DataColumn>().Select(col => col.ColumnName).ToList();
            foreach (var field in listFieldsTypeToConvert)
            {
                if (colFieldNames.Contains(field.Key))
                    switch (field.Value.Name)
                    {
                        case "Decimal":
                            fieldDecimalIndx.Add(table.Columns.IndexOf(table.Columns[field.Key]));
                            break;
                        case "DateTime":
                            fieldDataIndx.Add(table.Columns.IndexOf(table.Columns[field.Key]));
                            break;
                    }
            }
        }
        public void TryToReadFile()
        {
            _sheet = GetSheet(_filePath, 0);
        }
        public void RaadFileData()
        {
            ReadSheetDataIntoTable(_sheet, _table);
        }
        private ISheet GetSheet(string path, int sheetNum)
        {
            IWorkbook book = null;

            using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                if (path.Contains(@".xlsx"))
                    book = new XSSFWorkbook(file);
                else if (path.Contains(@".xls"))
                    book = new HSSFWorkbook(file);
                else throw new FormatException();
            }
            return book.GetSheetAt(sheetNum);
        }
        private void ReadSheetDataIntoTable(ISheet sheet, DataTable table)
        {
            for (int row = 0; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null) //null is when the row only contains empty cells 
                {
                    IRow r = sheet.GetRow(row);
                    DataRow dr = table.NewRow();

                    for (int i = 0; i < table.Columns.Count; i++)
                    {
                        ICell cell = r.GetCell(i, MissingCellPolicy.RETURN_NULL_AND_BLANK);

                        if (cell == null)
                            cell = r.CreateCell(i);

                        if (fieldDataIndx.Contains(i) && row > 0)
                            dr[i] = HandleDateCell(cell);
                        else if (fieldDecimalIndx.Contains(i) && row > 0)
                            dr[i] = HandleDecimalCell(cell);
                        else
                            dr[i] = cell.ToString();
                    }
                    var noEptycell = dr.ItemArray.FirstOrDefault(c => c.ToString() != string.Empty);
                    if(noEptycell == null)
                        continue;
                    table.Rows.Add(dr);
                }
            }
        }
        private string HandleDateCell(ICell cell)
        {
            string cellVal = string.Empty;
            try
            {
                cellVal = cell.CellType == CellType.Blank? cellVal: cell.DateCellValue.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            catch {}
            if (string.IsNullOrEmpty(cellVal))
            try
            {
                cellVal = DateTime.Parse(cell.StringCellValue).ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                cellVal = cell.ToString();
            }
            return cellVal;
        }
        private string HandleDecimalCell(ICell cell)
        {
            string cellVal = string.Empty;

            if (cell.CellStyle.GetDataFormatString().Contains('%') && cell.CellType == CellType.Numeric)
            {
                return Convert.ToDecimal(cell.NumericCellValue)
                    .ToString(cell.CellStyle.GetDataFormatString(), CultureInfo.InvariantCulture);
            }
            try
            {
                cellVal = cell.CellType == CellType.Blank? cellVal: Convert.ToDecimal(cell.NumericCellValue).ToString(CultureInfo.InvariantCulture);
            }
            catch{}
            if(string.IsNullOrEmpty(cellVal))
            try
            {
                cellVal = Convert.ToDecimal(cell.StringCellValue).ToString(CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                cellVal = cell.ToString();
            }
            return cellVal;
        }
    }
}
