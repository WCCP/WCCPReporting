﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SoftServe.Reports.PTRWprices.Models;

namespace SoftServe.Reports.PTRWprices
{
    internal class ExcelReader
    {
        private System.Globalization.CultureInfo _systemCulture;
        private SpreadsheetDocument _excelFile;
        private WorkbookPart _workbook;
        private List<int> fieldDecimalIndx = new List<int>();
        private List<int> fieldDataIndx = new List<int>();
        private Dictionary<string, Type> listFieldsTypeToConvert = new Dictionary<string, Type>()
        {
            {"Price", typeof (decimal)},
            {"PercentD", typeof (decimal)},
            {"StartDate", typeof (DateTime)},
            {"EndDate", typeof (DateTime)}
        };
        private string _fileName;

        public ExcelReader(string fileName, DataTable data)
        {
            List<string> colFieldNames = data.Columns.Cast<DataColumn>().Select(col => col.ColumnName).ToList();
            foreach (var field in listFieldsTypeToConvert)
            {
                if (colFieldNames.Contains(field.Key))
                    switch (field.Value.Name)
                    {
                        case "Decimal": fieldDecimalIndx.Add(data.Columns.IndexOf(data.Columns[field.Key])); break;
                        case "DateTime": fieldDataIndx.Add(data.Columns.IndexOf(data.Columns[field.Key])); break;
                    }
            }

            _fileName = fileName;
            _systemCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        }

        public void StartReading()
        {
            _excelFile = SpreadsheetDocument.Open(_fileName, false);
            _workbook = _excelFile.WorkbookPart;
        }

        public void FinishReading()
        {
            try
            {
                _excelFile.Close();
            }
            catch (Exception) { }
            finally
            {
                Thread.CurrentThread.CurrentCulture = _systemCulture;
            }
        }

        /// <summary>
        /// Gets the worksheet by number
        /// </summary>
        private SheetData GetWorkSheetData(int pageNumber, out string pageName)
        {
            _excelFile = SpreadsheetDocument.Open(_fileName, false);
            _workbook = _excelFile.WorkbookPart;

            Sheet lSheet = _workbook.Workbook.Descendants<Sheet>().ToArray()[pageNumber - 1];
            pageName = lSheet.Name;

            WorksheetPart lWorkSheet = (WorksheetPart)(_workbook.GetPartById(lSheet.Id));
            SheetData lSheetData = lWorkSheet.Worksheet.Elements<SheetData>().First();

            return lSheetData;
        }

        public void ReadFileWorksheet(DataTable dataFromFile, int worksheetNumber)
        {
            bool wasReadingSuccsessful = true;
            SheetData lSheetData = null;

            try
            {
                string lSheetName;
                lSheetData = GetWorkSheetData(worksheetNumber, out lSheetName);
                List<Row> rows = lSheetData.Elements<Row>().ToList();

                // Read the sheet data
                if (rows.Count > 1)// to flip over the header row :)
                {
                    for (var rowNum = 0; rowNum < rows.Count; rowNum++)
                    {
                        List<string> lCurrentRow = GetRowData(rows[rowNum]);

                        if (IsRowEmpty(lCurrentRow))
                            continue;

                        HandleRowLoading(rowNum + 1, lCurrentRow,   dataFromFile);
                    }
                }
                else
                {
                    wasReadingSuccsessful = false;
                }
            }
            catch (Exception)
            {
                wasReadingSuccsessful = false;
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            if (!wasReadingSuccsessful)
            {
                throw new Exception();
            }
        }

        private void HandleRowLoading(int rowNumber, List<string> currentRow,   DataTable dataFromFile)
        {
            ImportRow( currentRow, rowNumber, dataFromFile);
        }

        private bool ImportRow(List<string> row, int rowNumber, DataTable dataFromFile)
        {
            string fieldErrorMessage = "Некорректный формат данных";
            string rowErrorMessage = string.Empty;
            
            bool isCorrect = true;
          
            try
            {
                DataRow r = dataFromFile.NewRow();
                
                for (int i = 0; i < dataFromFile.Columns.Count; i++)
                {
                    string val = String.Empty;
                    try
                    {
                        val = row[i];
                    }
                    catch{}

                    if (fieldDataIndx.Contains(i))
                    {
                        r[i] = HandleDateTimeCellData(val);
                    }
                    else if (fieldDecimalIndx.Contains(i))
                    {
                        r[i] = HandleNumericCellData(val);
                    }
                    else
                    {
                        r[i] = val;
                    }
                }

                dataFromFile.Rows.Add(r);
            }
            catch (Exception)
            {
                rowErrorMessage += fieldErrorMessage;
                isCorrect = false;
            }

            return isCorrect;
        }

        private List<string> GetRowData(Row row)
        {
            var dataRow = new List<string>();

            var cellEnumerator = GetExcelCellEnumerator(row);
            while (cellEnumerator.MoveNext())
            {
                var cell = cellEnumerator.Current;
                var text = ReadExcelCell(cell, _workbook).Trim();

                dataRow.Add(text);
            }

            return dataRow;
        }

        private void FreeResourses()
        {
            if (_workbook != null)
            {
                ReleaseObject(_workbook);
            }

            _excelFile.Close();
            ReleaseObject(_excelFile);
        }

        /// <summary>
        /// Released resources
        /// </summary>
        /// <param name="obj">Object to be released</param>
        private void ReleaseObject(object obj)
        {
            if (obj != null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private bool IsRowEmpty(List<string> row)
        {
            bool isNotEmpty = row.Any(v => !string.IsNullOrEmpty(v));

            return !isNotEmpty;
        }

        private string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                // ASCII 'A' = 65
                int current = i == 0 ? letter - 65 : letter - 64;
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        /// <summary>
        /// Handles row reading cell by cell, cops with gaps in data
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)//empty cells
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }
        private string HandleNumericCellData(string val)
        {
                try
                {
                    return  Convert.ToDecimal(val).ToString(CultureInfo.InvariantCulture);
                   
                }
                catch (Exception)
                {
                return val;
                }
        }
        private string HandleDateTimeCellData(string val)
        {
            string value = null;
            try
            {
                value = DateTime.FromOADate(double.Parse(val))
                    .ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                value = null;
            }
            if (string.IsNullOrEmpty(val))
                try
                {
                    value = DateTime.Parse(val).ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                    value = null;
                }

            return  string.IsNullOrEmpty(value) ? val : value;
        }
    }
}
