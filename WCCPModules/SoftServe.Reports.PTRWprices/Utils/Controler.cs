﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.PLinq.Helpers;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SoftServe.Reports.PTRWprices.Utils;
using SoftServe.Reports.PTRWprices.Models;
using SoftServe.Reports.PTRWprices.DataAccess;
using DevExpress.XtraGrid.Columns;
using SoftServe.Reports.PTRWprices.Controls;

namespace SoftServe.Reports.PTRWprices.Utils
{
    public class Controler
    {
        GridView _view = null;
        ITabReportView _tabView = null;
        GridColumn _colCheck = null;
        GridColumn _colValue = null;
        GridColumn _colStartDate = null;
        GridColumn _colEndDate = null;
        GridColumn _colStatus = null;
        GridColumn _colErrorMessage = null;

        private List<long> _checkedIds;
        private Settings _settings;

        // Buffer for changed data
        private Dictionary<long, EditModel> _editedSource;
        // Main grid data source
        private List<ISourceModel> _source;
        // Grid source for edit mode
        private List<ISourceModel> _sourceToEdit;
        
        public Controler(ITabReportView tabView)
        {
            this._view = tabView.View;
            _tabView = tabView;
            _editedSource = new Dictionary<long, EditModel>();
            SetCustomColumnCaption();
            // Assign common aditional handlers
            AssignHandlers(_view);
        }

        public void SetEditableColumns(GridColumn check, GridColumn val, GridColumn start, GridColumn end, GridColumn status, GridColumn error)
        {
            _colCheck = check;
            _colValue = val;
            _colStartDate = start;
            _colEndDate = end;
            _colStatus = status;
            _colErrorMessage = error;
        }

        // On source changes tracking 
        private void OnSourceNotify(List<ISourceModel> source)
        {
            if (source == null)
                return;
            source.ForEach(promo => promo.PropertyChanged += PropertyChangedHandler);
            source.ForEach(promo => promo.EnablePropetyNotify(true));
        }

        // Adjusts grid view to proper mode (show\hide columns)
        private void SetGridEditMode(bool isEdit)
        {
            _colCheck.Visible = !isEdit;
            _colErrorMessage.Visible = isEdit;
            _colValue.OptionsColumn.AllowEdit = isEdit;
            _colStartDate.OptionsColumn.AllowEdit = isEdit;
            _colEndDate.OptionsColumn.AllowEdit = isEdit;
        }

        // Performs tracking source changes. Updates buffer.
        private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            ISourceModel row = sender as ISourceModel;
            EditModel edit = null;

            bool isPresent = _editedSource.TryGetValue(row.Id, out edit);

            if (isPresent && !row.HasValuesChanged && !row.IsStatusModified)
            {
                _editedSource.Remove(row.Id);
                return;
            }

            if (isPresent)
            {
                edit.Value = row.GetValue();
                edit.StartDate = row.StartDate;
                edit.EndDate = row.EndDate;
            }
            else
            {
                edit = new EditModel
                {
                    Id = row.Id,
                    Value = row.GetValue(),
                    StartDate = row.StartDate,
                    EndDate = row.EndDate
                };
                _editedSource.Add(row.Id, edit);
            }

            edit.Status = row.IsStatusModified ? row.GetStatus() : 0;
        }

        // Performs convertation and assign datasoure for grid.
        public void AssignDataSource(List<ISourceModel> source, bool isEditMode)
        {
            if (isEditMode)
            {
                OnSourceNotify(source);
            }
            else
            {
                _source = source;
            }
            switch (_tabView.Type)
            {
                case PtRwTypes.PTRPrices: _view.GridControl.DataSource = source.ConvertAll(row => (PTR)row); break;
                case PtRwTypes.PTWPrices: _view.GridControl.DataSource = source.ConvertAll(row => (PTW)row); break;
                case PtRwTypes.PromoDiscounts: _view.GridControl.DataSource = source.ConvertAll(row => (PromoDiscount)row); break;
                case PtRwTypes.PromoDiscountsNet: _view.GridControl.DataSource = source.ConvertAll(row => (PromoDiscountNet)row); break;
                case PtRwTypes.RegularDiscounts: _view.GridControl.DataSource = source.ConvertAll(row => (RegularDiscount)row); break;
                case PtRwTypes.RegularDiscountsNet: _view.GridControl.DataSource = source.ConvertAll(row => (RegularDiscountNet)row); break;
            }

            // Reset check column
            _source.ForEach(r => r.Check = false);
            UpdateCheckAllState(_view, null);
        }

        // Fetches data source from Db
        public void LoadData(Settings settings)
        {
            _settings = settings;

            switch (_tabView.Type)
            {
                case PtRwTypes.PTRPrices: _source = DataProvider.GetPTRView(_settings).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PTWPrices: _source = DataProvider.GetPTWView(_settings).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscounts: _source = DataProvider.GetPromoView(_settings).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscountsNet: _source = DataProvider.GetPromoNetView(_settings).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscounts: _source = DataProvider.GetRegularView(_settings).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscountsNet: _source = DataProvider.GetRegularNetView(_settings).ConvertAll(row => (ISourceModel)row); break;
            }
                        
            AssignDataSource(_source, false);
        }

        private List<ISourceModel> GetGridViewDataSource(GridView view, PtRwTypes type)
        {
            List<ISourceModel> lst = null;
            switch (type)
            {
                case PtRwTypes.PTRPrices: lst = ((List<PTR>)(view.GridControl.DataSource)).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PTWPrices: lst = ((List<PTW>)(view.GridControl.DataSource)).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscounts: lst = ((List<PromoDiscount>)(view.GridControl.DataSource)).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscountsNet: lst = ((List<PromoDiscountNet>)(view.GridControl.DataSource)).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscounts: lst = ((List<RegularDiscount>)(view.GridControl.DataSource)).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscountsNet: lst = ((List<RegularDiscountNet>)(view.GridControl.DataSource)).ConvertAll(row => (ISourceModel)row); break;
            }
            return lst;
        }

        public void Export(string path)
        {
            _view.GridControl.ExportToXlsx(path);
        }

        // Loads data source for editing
        private void LoadDataEditMode()
        {
            _checkedIds = new List<long>();
            _checkedIds = _source.Where(row => row.Check).Select(pr => pr.Id).Distinct().ToList();

            switch (_tabView.Type)
            {
                case PtRwTypes.PTRPrices: _sourceToEdit = DataProvider.GetEditPTRData(_checkedIds, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PTWPrices: _sourceToEdit = DataProvider.GetEditPTWData(_checkedIds, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscounts: _sourceToEdit = DataProvider.GetEditPromoData(_checkedIds, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscountsNet: _sourceToEdit = DataProvider.GetEditPromoNetData(_checkedIds, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscounts: _sourceToEdit = DataProvider.GetEditRegularData(_checkedIds, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscountsNet: _sourceToEdit = DataProvider.GetEditRegularNetData(_checkedIds, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
            }                       
            
            SetGridEditMode(true);
            AssignDataSource(_sourceToEdit, true);
        }

        public void RestoreLayout(string path)
        {

        }

        public void SaveLayout(string path)
        {

        }

        public void GoEdit()
        {
            if (!_source.Any(r => r.Check))
            {
                XtraMessageBox.Show(ReportConstants.MSG_NO_DATA_TO_EDIT, "Редактирование:");
                return;
            }
            LoadDataEditMode();
            _tabView.OnEditMOde();
        }

        public void StopEdit()
        {
            if (!_view.ValidateEditor())
            {
                return;
            }

            _view.CloseEditor();
            _view.UpdateCurrentRow();

            // if nothing changed
            if (_editedSource.Count == 0)
            {
                SetGridEditMode(false);
                AssignDataSource(_source, false);
                _tabView.OnCancelEditMode();
                return;
            }
            // if there are changes, show message and handle user decision
            else
            {
                DialogResult res = DialogResult.None;

                res = XtraMessageBox.Show(ReportConstants.MSG_UNSAVED_DATA_PRESENT, "Редактирование:",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
              
                if (res == DialogResult.No)
                {
                    _editedSource.Clear();
                    SetGridEditMode(false);
                    LoadData(_settings);
                    _tabView.OnCancelEditMode();
                    return; ;
                }
                else if (res == DialogResult.Yes)
                {
                    SaveEdit();
                }
            }
        }

        public void SaveEdit()
        {
            if (!_view.ValidateEditor())
            {
                return;
            }

            _view.CloseEditor();
            _view.UpdateCurrentRow();


            if (_editedSource.Count == 0)
            {
                SetGridEditMode(false);
                AssignDataSource(_source, false);
                _tabView.OnCancelEditMode();
                return;
            }

            List<ISourceModel> lst = new List<ISourceModel>();

            switch (_tabView.Type)
            {
                case PtRwTypes.PTRPrices: lst = DataProvider.SaveEditedPTRData(_editedSource, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PTWPrices: lst = DataProvider.SaveEditedPTWData(_editedSource, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscounts: lst = DataProvider.SaveEditedPromoData(_editedSource, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.PromoDiscountsNet: lst = DataProvider.SaveEditedPromoNetData(_editedSource, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscounts: lst = DataProvider.SaveEditedRegularData(_editedSource, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
                case PtRwTypes.RegularDiscountsNet: lst = DataProvider.SaveEditedRegularNetData(_editedSource, _tabView.Type).ConvertAll(row => (ISourceModel)row); break;
            }
            
            if (lst.Count == 0)
            {
                // Clear buffer
                _editedSource.Clear();
                SetGridEditMode(false);
                LoadData(_settings);
                _tabView.OnCancelEditMode();
                return;
            }
            else
            {
                Dictionary<long, EditModel> temp = new Dictionary<long, EditModel>();
                foreach (ISourceModel row in lst)
                {
                    if (_editedSource.ContainsKey(row.Id))
                        temp.Add(row.Id, _editedSource[row.Id]);
                }
                _editedSource = temp;
                AssignDataSource(lst, true);
            }
        }

        public void DeactivateEdit()
        {
            var rows = _view.GetSelectedRows();
            foreach (int i in rows)
            {
                _view.SetRowCellValue(i, _colStatus, 9);
            }
        }

        public void ActivateEdit()
        {
            var rows = _view.GetSelectedRows();
            foreach (int i in rows)
            {
                _view.SetRowCellValue(i, _colStatus, 2);
            }
        }


        #region Draw and handle custom checked column
        // Init Method
        private void SetCustomColumnCaption()
        {
            var itemIndex = _view.GridControl.RepositoryItems.Add(new RepositoryItemCheckEdit());
            _checkAllEdit = _view.GridControl.RepositoryItems[itemIndex] as RepositoryItemCheckEdit;
            _checkAllEdit.ValueChecked = 1;
            _checkAllEdit.ValueUnchecked = 0;
            _checkAllEditSatate = 0;
        }

        // Hides check box min auto filter row. Use it in CustomRowCellEdit
        private void HideAutofilterCheck(object sender, CustomRowCellEditEventArgs e)
        {
            if (_view.IsFilterRow(e.RowHandle) && !e.Column.OptionsFilter.AllowAutoFilter)
            {
                e.RepositoryItem = _emptyEdit;
            }
        }

        // Draws check box in column header.
        private void DrawCheckBox(Graphics g, Rectangle r, RepositoryItemCheckEdit checkEdit, int isCheck)
        {
            CheckEditViewInfo info = default(CheckEditViewInfo);
            CheckEditPainter painter = default(CheckEditPainter);
            ControlGraphicsInfoArgs args = default(ControlGraphicsInfoArgs);
            checkEdit.GlyphAlignment = HorzAlignment.Center;
            //checkEdit.Caption = @"Все";
            checkEdit.Caption = string.Empty;
            info = (CheckEditViewInfo)checkEdit.CreateViewInfo();
            painter = (CheckEditPainter)checkEdit.CreatePainter();

            info.EditValue = isCheck;
            _checkAllEditSatate = isCheck;
            info.Bounds = r;
            info.CalcViewInfo(g);
            args = new ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
            painter.Draw(args);
            args.Cache.Dispose();
        }

        // Draws column header. Use it in CustomDrawColumnHeader event handler
        private void DrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null || e.Column.Name != _colCheck.Name)
                return;

            Rectangle rect = e.Bounds;
            rect.Inflate(-1, -1);

            e.Info.InnerElements.Clear();
            e.Painter.DrawObject(e.Info);
            DrawCheckBox(e.Graphics, rect, _checkAllEdit, _checkAllEditSatate);
            e.Handled = true;
        }

        // Use it in gridView MouseDown event handler      
        private void MouseDownHeandler(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;

            GridHitInfo info = view.CalcHitInfo(e.Location);

            if (info.InColumnPanel && info.Column != null && info.Column.FieldName == _colCheck.FieldName)
            {
                int oldState = _checkAllEditSatate;

                List<ISourceModel> source = GetGridViewDataSource(view, _tabView.Type);

                int countSourceChecked = source.Where(r => r.Check).ToList().Count;

                if (countSourceChecked != view.DataRowCount)
                    source.ForEach(r => r.Check = false);

                for (int i = 0; i < view.DataRowCount; i++)
                {
                    ((ISourceModel)view.GetRow(i)).Check = oldState != 1;
                }
                UpdateCheckAllState((GridView)sender, null);
            }
        }

        // Updates header column check state. Use it ine CellValueChanged event handler.
        private void UpdateCheckAllState(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            _checkAllEdit.BeginUpdate();
            _checkAllEdit.CreateViewInfo().EditValue = _checkAllEditSatate = GetReportsChkBoxesState((GridView)sender);
            _checkAllEdit.EndUpdate();
        }

        // Gets check header state accordingly all rows
        private int GetReportsChkBoxesState(GridView view)
        {
            List<ISourceModel> rowsVisible = new List<ISourceModel>();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                rowsVisible.Add((ISourceModel)_view.GetRow(i));
            }

            int selectedCount = rowsVisible.Where(row => row.Check).ToList().Count;
            if (selectedCount == rowsVisible.Count && selectedCount != 0)
                return 1;
            else if (selectedCount < rowsVisible.Count && selectedCount > 0)
                return -1;
            else return 0;
        }

        // Sets check column value. Use it ine CellValueChanging event handler
        private void SetCheckColValue(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == _colCheck.FieldName)
            {
                e.Column.View.SetRowCellValue(e.RowHandle, e.Column.FieldName, e.Value);
            }
        }
        private void ColumnFilterChanged(object sender, EventArgs e)
        {
            UpdateCheckAllState((GridView)sender, null);
        }
        private void AssignHandlers(GridView view)
        {
            view.CustomRowCellEdit += HideAutofilterCheck;
            view.MouseDown += MouseDownHeandler;
            view.CustomDrawColumnHeader += DrawColumnHeader;
            view.CellValueChanged += UpdateCheckAllState;
            view.CellValueChanging += SetCheckColValue;
            view.ColumnFilterChanged += ColumnFilterChanged;
            view.ValidatingEditor += ValidatingEditor;
            view.RowStyle += gridView_RowStyle;
            view.ShownEditor += gridView_ShownEditor;
        }

        // fields
        private int _checkAllEditSatate;
        private RepositoryItemTextEdit _emptyEdit = new RepositoryItemTextEdit();
        private RepositoryItemCheckEdit _checkAllEdit;

        #endregion

        private void ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;

            if (view == null || view.FocusedColumn != _colValue)
                return;

            if (_tabView.Type == PtRwTypes.PTRPrices || _tabView.Type == PtRwTypes.PTWPrices)
            {
                try
                {
                    decimal val = Convert.ToDecimal(e.Value);
                    if (val < 0.01M)
                    {
                        e.Valid = false;
                        e.ErrorText = "Цена должна быть больше 0";
                    }
                }
                catch
                {
                    e.Valid = false;
                    e.ErrorText = "Не коррекное значение!";
                }
            }
            else
            {
                try
                {
                    decimal val = Convert.ToDecimal(e.Value);
                    if (val > 99.99M || val < 0.01M)
                    {
                        e.Valid = false;
                        e.ErrorText = "Скидка должна быть в пределах диапазона 0,01% - 99,99%";
                    }
                }
                catch
                {
                    e.Valid = false;
                    e.ErrorText = "Не коррекное значение!";
                }
            }
        }
        private void gridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.RowHandle < 0 || view == null)
                return;

            if (view.GetRowCellValue(e.RowHandle, _colStatus).ToString() == "9")
                e.Appearance.BackColor = ReportConstants.ColorDeactivated;
        }
        private void gridView_ShownEditor(object sender, EventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
                return;

            if (view.FocusedColumn.FieldName == "StartDate"|| view.FocusedColumn.FieldName == "EndDate")
            {
                ((DateEdit)view.ActiveEditor).Properties.MinValue =  DateTime.Now.AddDays(-90);
                ((DateEdit)view.ActiveEditor).Properties.MaxValue =  DateTime.Now.AddDays(90);
            }            
        }
    }
}
