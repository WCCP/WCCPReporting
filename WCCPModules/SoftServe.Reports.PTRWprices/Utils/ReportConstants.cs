﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SoftServe.Reports.PTRWprices.Utils
{
    public class ReportConstants
    {
        public const string PERIOD = "Период:";
        public const string REGION = "Регион:";
        public const string DISTRIBUTOR = "Дистрибутор:";
        public const string CUSTOMER = "ТС:";
        public const string ASSORTMENT = "Ассортимент:";
        public const string PRICES_TYPE = "Тип цен/скидок:";
        public const string PRICES_STATUS = "Статус цены:";

        public static readonly Color ColorDeactivated = Color.FromArgb(169, 169, 169);

        public const string MSG_UNSAVED_DATA_PRESENT = "Имеются не сохраненные изменения.\nЖелаете сохранить?.";
        public const string MSG_ACCESS_DENIED = "У вас нет прав для работы с отчетем \"PTR и PTW цены\".\nПожалуйста обратитесь к системному администратору.";
        public const string MSG_ACCESS_DENIED_CAPTION = "Нет доступа к отчету:";
        public const string MSG_NO_DATA_TO_EDIT = @"Выберите нужные записи для редактирования";              

    }
}
