﻿namespace SoftServe.Reports.PTRWprices.Controls
{
    partial class UserControlPTR
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlPTR = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPTRCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRRecordId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRDistrCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRDistrName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRCustCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRCustName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRNetworkCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRNetworkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRCombiProdCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRCombiProdName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRProdCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRProdName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColPTRStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTREndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRDLM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRError = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlPTR
            // 
            this.gridControlPTR.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlPTR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPTR.Location = new System.Drawing.Point(0, 0);
            this.gridControlPTR.MainView = this.gridView1;
            this.gridControlPTR.Name = "gridControlPTR";
            this.gridControlPTR.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControlPTR.Size = new System.Drawing.Size(1187, 662);
            this.gridControlPTR.TabIndex = 0;
            this.gridControlPTR.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPTRCheck,
            this.gridColPTRRecordId,
            this.gridColPTRDistrCode,
            this.gridColPTRDistrName,
            this.gridColPTRCustCode,
            this.gridColPTRCustName,
            this.gridColPTRNetworkCode,
            this.gridColPTRNetworkName,
            this.gridColPTRCombiProdCode,
            this.gridColPTRCombiProdName,
            this.gridColPTRProdCode,
            this.gridColPTRProdName,
            this.gridColPTRPrice,
            this.gridColPTRStartDate,
            this.gridColPTREndDate,
            this.gridColPTRStatus,
            this.gridColPTRLogin,
            this.gridColPTRDLM,
            this.gridColPTRError});
            this.gridView1.GridControl = this.gridControlPTR;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            // 
            // gridColPTRCheck
            // 
            this.gridColPTRCheck.Caption = "Все ";
            this.gridColPTRCheck.FieldName = "Check";
            this.gridColPTRCheck.MaxWidth = 60;
            this.gridColPTRCheck.MinWidth = 60;
            this.gridColPTRCheck.Name = "gridColPTRCheck";
            this.gridColPTRCheck.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPTRCheck.OptionsColumn.AllowMove = false;
            this.gridColPTRCheck.OptionsColumn.AllowShowHide = false;
            this.gridColPTRCheck.OptionsColumn.AllowSize = false;
            this.gridColPTRCheck.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPTRCheck.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPTRCheck.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPTRCheck.OptionsFilter.AllowAutoFilter = false;
            this.gridColPTRCheck.Visible = true;
            this.gridColPTRCheck.VisibleIndex = 0;
            this.gridColPTRCheck.Width = 60;
            // 
            // gridColPTRRecordId
            // 
            this.gridColPTRRecordId.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRRecordId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRRecordId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRRecordId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRRecordId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRRecordId.Caption = "ID записи";
            this.gridColPTRRecordId.FieldName = "Id";
            this.gridColPTRRecordId.Name = "gridColPTRRecordId";
            this.gridColPTRRecordId.OptionsColumn.AllowEdit = false;
            this.gridColPTRRecordId.Visible = true;
            this.gridColPTRRecordId.VisibleIndex = 1;
            this.gridColPTRRecordId.Width = 90;
            // 
            // gridColPTRDistrCode
            // 
            this.gridColPTRDistrCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRDistrCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRDistrCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRDistrCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRDistrCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRDistrCode.Caption = "Код дистрибутора";
            this.gridColPTRDistrCode.FieldName = "DISTR_Id";
            this.gridColPTRDistrCode.Name = "gridColPTRDistrCode";
            this.gridColPTRDistrCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRDistrCode.Visible = true;
            this.gridColPTRDistrCode.VisibleIndex = 2;
            this.gridColPTRDistrCode.Width = 90;
            // 
            // gridColPTRDistrName
            // 
            this.gridColPTRDistrName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRDistrName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRDistrName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRDistrName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRDistrName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRDistrName.Caption = "Название  дистрибутора";
            this.gridColPTRDistrName.FieldName = "DISTR_NAME";
            this.gridColPTRDistrName.Name = "gridColPTRDistrName";
            this.gridColPTRDistrName.OptionsColumn.AllowEdit = false;
            this.gridColPTRDistrName.Visible = true;
            this.gridColPTRDistrName.VisibleIndex = 3;
            this.gridColPTRDistrName.Width = 200;
            // 
            // gridColPTRCustCode
            // 
            this.gridColPTRCustCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRCustCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRCustCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRCustCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRCustCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRCustCode.Caption = "Код ТС";
            this.gridColPTRCustCode.FieldName = "Cust_Id";
            this.gridColPTRCustCode.Name = "gridColPTRCustCode";
            this.gridColPTRCustCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRCustCode.Visible = true;
            this.gridColPTRCustCode.VisibleIndex = 4;
            this.gridColPTRCustCode.Width = 90;
            // 
            // gridColPTRCustName
            // 
            this.gridColPTRCustName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRCustName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRCustName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRCustName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRCustName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRCustName.Caption = "Название ТС";
            this.gridColPTRCustName.FieldName = "Cust_NAME";
            this.gridColPTRCustName.Name = "gridColPTRCustName";
            this.gridColPTRCustName.OptionsColumn.AllowEdit = false;
            this.gridColPTRCustName.Visible = true;
            this.gridColPTRCustName.VisibleIndex = 5;
            this.gridColPTRCustName.Width = 200;
            // 
            // gridColPTRNetworkCode
            // 
            this.gridColPTRNetworkCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRNetworkCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRNetworkCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRNetworkCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRNetworkCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRNetworkCode.Caption = "Код сети";
            this.gridColPTRNetworkCode.FieldName = "GrdCode";
            this.gridColPTRNetworkCode.Name = "gridColPTRNetworkCode";
            this.gridColPTRNetworkCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRNetworkCode.Visible = true;
            this.gridColPTRNetworkCode.VisibleIndex = 6;
            this.gridColPTRNetworkCode.Width = 90;
            // 
            // gridColPTRNetworkName
            // 
            this.gridColPTRNetworkName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRNetworkName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRNetworkName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRNetworkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRNetworkName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRNetworkName.Caption = "Название сети";
            this.gridColPTRNetworkName.FieldName = "Network_Name";
            this.gridColPTRNetworkName.Name = "gridColPTRNetworkName";
            this.gridColPTRNetworkName.OptionsColumn.AllowEdit = false;
            this.gridColPTRNetworkName.Visible = true;
            this.gridColPTRNetworkName.VisibleIndex = 7;
            this.gridColPTRNetworkName.Width = 180;
            // 
            // gridColPTRCombiProdCode
            // 
            this.gridColPTRCombiProdCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRCombiProdCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRCombiProdCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRCombiProdCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRCombiProdCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRCombiProdCode.Caption = "Код комбипродукта";
            this.gridColPTRCombiProdCode.FieldName = "HLCode";
            this.gridColPTRCombiProdCode.Name = "gridColPTRCombiProdCode";
            this.gridColPTRCombiProdCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRCombiProdCode.Visible = true;
            this.gridColPTRCombiProdCode.VisibleIndex = 8;
            this.gridColPTRCombiProdCode.Width = 90;
            // 
            // gridColPTRCombiProdName
            // 
            this.gridColPTRCombiProdName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRCombiProdName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRCombiProdName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRCombiProdName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRCombiProdName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRCombiProdName.Caption = "Название комбипродукта";
            this.gridColPTRCombiProdName.FieldName = "ProductCnName";
            this.gridColPTRCombiProdName.Name = "gridColPTRCombiProdName";
            this.gridColPTRCombiProdName.OptionsColumn.AllowEdit = false;
            this.gridColPTRCombiProdName.Visible = true;
            this.gridColPTRCombiProdName.VisibleIndex = 9;
            this.gridColPTRCombiProdName.Width = 200;
            // 
            // gridColPTRProdCode
            // 
            this.gridColPTRProdCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRProdCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRProdCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRProdCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRProdCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRProdCode.Caption = "Код продукта";
            this.gridColPTRProdCode.FieldName = "ProductCode";
            this.gridColPTRProdCode.Name = "gridColPTRProdCode";
            this.gridColPTRProdCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRProdCode.Visible = true;
            this.gridColPTRProdCode.VisibleIndex = 10;
            this.gridColPTRProdCode.Width = 90;
            // 
            // gridColPTRProdName
            // 
            this.gridColPTRProdName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRProdName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRProdName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRProdName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRProdName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRProdName.Caption = "Название продукта";
            this.gridColPTRProdName.FieldName = "ProductName";
            this.gridColPTRProdName.Name = "gridColPTRProdName";
            this.gridColPTRProdName.OptionsColumn.AllowEdit = false;
            this.gridColPTRProdName.Visible = true;
            this.gridColPTRProdName.VisibleIndex = 11;
            this.gridColPTRProdName.Width = 200;
            // 
            // gridColPTRPrice
            // 
            this.gridColPTRPrice.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTRPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRPrice.Caption = "Цена";
            this.gridColPTRPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColPTRPrice.FieldName = "Price";
            this.gridColPTRPrice.Name = "gridColPTRPrice";
            this.gridColPTRPrice.OptionsColumn.AllowEdit = false;
            this.gridColPTRPrice.Visible = true;
            this.gridColPTRPrice.VisibleIndex = 12;
            this.gridColPTRPrice.Width = 100;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "######0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColPTRStartDate
            // 
            this.gridColPTRStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTRStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRStartDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRStartDate.Caption = "StartDate";
            this.gridColPTRStartDate.FieldName = "StartDate";
            this.gridColPTRStartDate.Name = "gridColPTRStartDate";
            this.gridColPTRStartDate.OptionsColumn.AllowEdit = false;
            this.gridColPTRStartDate.Visible = true;
            this.gridColPTRStartDate.VisibleIndex = 13;
            this.gridColPTRStartDate.Width = 80;
            // 
            // gridColPTREndDate
            // 
            this.gridColPTREndDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTREndDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTREndDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTREndDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTREndDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTREndDate.Caption = "EndDate";
            this.gridColPTREndDate.FieldName = "EndDate";
            this.gridColPTREndDate.Name = "gridColPTREndDate";
            this.gridColPTREndDate.OptionsColumn.AllowEdit = false;
            this.gridColPTREndDate.Visible = true;
            this.gridColPTREndDate.VisibleIndex = 14;
            this.gridColPTREndDate.Width = 80;
            // 
            // gridColPTRStatus
            // 
            this.gridColPTRStatus.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRStatus.Caption = "Статус";
            this.gridColPTRStatus.FieldName = "Status";
            this.gridColPTRStatus.Name = "gridColPTRStatus";
            this.gridColPTRStatus.OptionsColumn.AllowEdit = false;
            this.gridColPTRStatus.Visible = true;
            this.gridColPTRStatus.VisibleIndex = 15;
            this.gridColPTRStatus.Width = 60;
            // 
            // gridColPTRLogin
            // 
            this.gridColPTRLogin.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRLogin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRLogin.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRLogin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRLogin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRLogin.Caption = "Логин последнего редактирования";
            this.gridColPTRLogin.FieldName = "login";
            this.gridColPTRLogin.Name = "gridColPTRLogin";
            this.gridColPTRLogin.OptionsColumn.AllowEdit = false;
            this.gridColPTRLogin.Visible = true;
            this.gridColPTRLogin.VisibleIndex = 16;
            this.gridColPTRLogin.Width = 160;
            // 
            // gridColPTRDLM
            // 
            this.gridColPTRDLM.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRDLM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTRDLM.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRDLM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRDLM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRDLM.Caption = "Дата последнего редактирования";
            this.gridColPTRDLM.FieldName = "dlm";
            this.gridColPTRDLM.Name = "gridColPTRDLM";
            this.gridColPTRDLM.OptionsColumn.AllowEdit = false;
            this.gridColPTRDLM.Visible = true;
            this.gridColPTRDLM.VisibleIndex = 17;
            this.gridColPTRDLM.Width = 140;
            // 
            // gridColPTRError
            // 
            this.gridColPTRError.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRError.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRError.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRError.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRError.Caption = "Ошибка";
            this.gridColPTRError.FieldName = "ErrorMessage";
            this.gridColPTRError.Name = "gridColPTRError";
            this.gridColPTRError.OptionsColumn.AllowEdit = false;
            this.gridColPTRError.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPTRError.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPTRError.Width = 240;
            // 
            // UserControlPTR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlPTR);
            this.Name = "UserControlPTR";
            this.Size = new System.Drawing.Size(1187, 662);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlPTR;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRRecordId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRDistrCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRDistrName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRCustCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRCustName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRNetworkCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRNetworkName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRCombiProdCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRCombiProdName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRProdCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRProdName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTREndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRLogin;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRDLM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRCheck;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRError;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
