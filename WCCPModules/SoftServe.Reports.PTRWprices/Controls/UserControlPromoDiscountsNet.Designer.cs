﻿namespace SoftServe.Reports.PTRWprices.Controls
{
    partial class UserControlPromoDiscountsNet
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlPromoDiscountsNet = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPDRNCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNRecordId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNDistrId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNCustCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNCustName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNNetworkCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNNetworkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNHLCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNHLName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColPDNStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDNDLM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDRNError = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPromoDiscountsNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlPromoDiscountsNet
            // 
            this.gridControlPromoDiscountsNet.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlPromoDiscountsNet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPromoDiscountsNet.Location = new System.Drawing.Point(0, 0);
            this.gridControlPromoDiscountsNet.MainView = this.gridView2;
            this.gridControlPromoDiscountsNet.Name = "gridControlPromoDiscountsNet";
            this.gridControlPromoDiscountsNet.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControlPromoDiscountsNet.Size = new System.Drawing.Size(1282, 691);
            this.gridControlPromoDiscountsNet.TabIndex = 3;
            this.gridControlPromoDiscountsNet.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.ColumnPanelRowHeight = 40;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPDRNCheck,
            this.gridColPDNRecordId,
            this.gridColPDNDistrId,
            this.gridColPDNName,
            this.gridColPDNCustCode,
            this.gridColPDNCustName,
            this.gridColPDNNetworkCode,
            this.gridColPDNNetworkName,
            this.gridColPDNHLCode,
            this.gridColPDNHLName,
            this.gridColPDNPrice,
            this.gridColPDNStartDate,
            this.gridColPDNEndDate,
            this.gridColPDNStatus,
            this.gridColPDNLogin,
            this.gridColPDNDLM,
            this.gridColPDRNError});
            this.gridView2.GridControl = this.gridControlPromoDiscountsNet;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            // 
            // gridColPDRNCheck
            // 
            this.gridColPDRNCheck.Caption = "Все";
            this.gridColPDRNCheck.FieldName = "Check";
            this.gridColPDRNCheck.MaxWidth = 60;
            this.gridColPDRNCheck.MinWidth = 60;
            this.gridColPDRNCheck.Name = "gridColPDRNCheck";
            this.gridColPDRNCheck.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPDRNCheck.OptionsColumn.AllowMove = false;
            this.gridColPDRNCheck.OptionsColumn.AllowShowHide = false;
            this.gridColPDRNCheck.OptionsColumn.AllowSize = false;
            this.gridColPDRNCheck.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPDRNCheck.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPDRNCheck.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPDRNCheck.OptionsFilter.AllowAutoFilter = false;
            this.gridColPDRNCheck.Visible = true;
            this.gridColPDRNCheck.VisibleIndex = 0;
            this.gridColPDRNCheck.Width = 60;
            // 
            // gridColPDNRecordId
            // 
            this.gridColPDNRecordId.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNRecordId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNRecordId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNRecordId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNRecordId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNRecordId.Caption = "ID записи";
            this.gridColPDNRecordId.FieldName = "Id";
            this.gridColPDNRecordId.Name = "gridColPDNRecordId";
            this.gridColPDNRecordId.OptionsColumn.AllowEdit = false;
            this.gridColPDNRecordId.Visible = true;
            this.gridColPDNRecordId.VisibleIndex = 1;
            // 
            // gridColPDNDistrId
            // 
            this.gridColPDNDistrId.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNDistrId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNDistrId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNDistrId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNDistrId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNDistrId.Caption = "Код дистрибутора";
            this.gridColPDNDistrId.FieldName = "DISTR_Id";
            this.gridColPDNDistrId.Name = "gridColPDNDistrId";
            this.gridColPDNDistrId.OptionsColumn.AllowEdit = false;
            this.gridColPDNDistrId.Visible = true;
            this.gridColPDNDistrId.VisibleIndex = 2;
            this.gridColPDNDistrId.Width = 90;
            // 
            // gridColPDNName
            // 
            this.gridColPDNName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNName.Caption = "Название дистрибутора";
            this.gridColPDNName.FieldName = "DISTR_NAME";
            this.gridColPDNName.Name = "gridColPDNName";
            this.gridColPDNName.OptionsColumn.AllowEdit = false;
            this.gridColPDNName.Visible = true;
            this.gridColPDNName.VisibleIndex = 3;
            this.gridColPDNName.Width = 200;
            // 
            // gridColPDNCustCode
            // 
            this.gridColPDNCustCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNCustCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNCustCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNCustCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNCustCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNCustCode.Caption = "Код ТС";
            this.gridColPDNCustCode.FieldName = "Cust_Id";
            this.gridColPDNCustCode.Name = "gridColPDNCustCode";
            this.gridColPDNCustCode.OptionsColumn.AllowEdit = false;
            this.gridColPDNCustCode.Visible = true;
            this.gridColPDNCustCode.VisibleIndex = 4;
            this.gridColPDNCustCode.Width = 90;
            // 
            // gridColPDNCustName
            // 
            this.gridColPDNCustName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNCustName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNCustName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNCustName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNCustName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNCustName.Caption = "Название ТС";
            this.gridColPDNCustName.FieldName = "Cust_NAME";
            this.gridColPDNCustName.Name = "gridColPDNCustName";
            this.gridColPDNCustName.OptionsColumn.AllowEdit = false;
            this.gridColPDNCustName.Visible = true;
            this.gridColPDNCustName.VisibleIndex = 5;
            this.gridColPDNCustName.Width = 200;
            // 
            // gridColPDNNetworkCode
            // 
            this.gridColPDNNetworkCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNNetworkCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNNetworkCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNNetworkCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNNetworkCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNNetworkCode.Caption = "Код Сети";
            this.gridColPDNNetworkCode.FieldName = "GrdCode";
            this.gridColPDNNetworkCode.Name = "gridColPDNNetworkCode";
            this.gridColPDNNetworkCode.OptionsColumn.AllowEdit = false;
            this.gridColPDNNetworkCode.Visible = true;
            this.gridColPDNNetworkCode.VisibleIndex = 6;
            this.gridColPDNNetworkCode.Width = 90;
            // 
            // gridColPDNNetworkName
            // 
            this.gridColPDNNetworkName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNNetworkName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNNetworkName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNNetworkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNNetworkName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNNetworkName.Caption = "Название сети";
            this.gridColPDNNetworkName.FieldName = "Network_Name";
            this.gridColPDNNetworkName.Name = "gridColPDNNetworkName";
            this.gridColPDNNetworkName.OptionsColumn.AllowEdit = false;
            this.gridColPDNNetworkName.Visible = true;
            this.gridColPDNNetworkName.VisibleIndex = 7;
            this.gridColPDNNetworkName.Width = 200;
            // 
            // gridColPDNHLCode
            // 
            this.gridColPDNHLCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNHLCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNHLCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNHLCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNHLCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNHLCode.Caption = "Код комбипродукта";
            this.gridColPDNHLCode.FieldName = "HLCode";
            this.gridColPDNHLCode.Name = "gridColPDNHLCode";
            this.gridColPDNHLCode.OptionsColumn.AllowEdit = false;
            this.gridColPDNHLCode.Visible = true;
            this.gridColPDNHLCode.VisibleIndex = 8;
            this.gridColPDNHLCode.Width = 90;
            // 
            // gridColPDNHLName
            // 
            this.gridColPDNHLName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNHLName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNHLName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNHLName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNHLName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNHLName.Caption = "Название комбипродукта";
            this.gridColPDNHLName.FieldName = "ProductCnName";
            this.gridColPDNHLName.Name = "gridColPDNHLName";
            this.gridColPDNHLName.OptionsColumn.AllowEdit = false;
            this.gridColPDNHLName.Visible = true;
            this.gridColPDNHLName.VisibleIndex = 9;
            this.gridColPDNHLName.Width = 200;
            // 
            // gridColPDNPrice
            // 
            this.gridColPDNPrice.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDNPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNPrice.Caption = "% скидки";
            this.gridColPDNPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColPDNPrice.FieldName = "PercentD";
            this.gridColPDNPrice.Name = "gridColPDNPrice";
            this.gridColPDNPrice.OptionsColumn.AllowEdit = false;
            this.gridColPDNPrice.Visible = true;
            this.gridColPDNPrice.VisibleIndex = 10;
            this.gridColPDNPrice.Width = 60;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "##0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColPDNStartDate
            // 
            this.gridColPDNStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDNStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNStartDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNStartDate.Caption = "StartDate";
            this.gridColPDNStartDate.FieldName = "StartDate";
            this.gridColPDNStartDate.Name = "gridColPDNStartDate";
            this.gridColPDNStartDate.OptionsColumn.AllowEdit = false;
            this.gridColPDNStartDate.Visible = true;
            this.gridColPDNStartDate.VisibleIndex = 11;
            this.gridColPDNStartDate.Width = 80;
            // 
            // gridColPDNEndDate
            // 
            this.gridColPDNEndDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNEndDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDNEndDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNEndDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNEndDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNEndDate.Caption = "EndDate";
            this.gridColPDNEndDate.FieldName = "EndDate";
            this.gridColPDNEndDate.Name = "gridColPDNEndDate";
            this.gridColPDNEndDate.OptionsColumn.AllowEdit = false;
            this.gridColPDNEndDate.Visible = true;
            this.gridColPDNEndDate.VisibleIndex = 12;
            this.gridColPDNEndDate.Width = 80;
            // 
            // gridColPDNStatus
            // 
            this.gridColPDNStatus.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNStatus.Caption = "Статус";
            this.gridColPDNStatus.FieldName = "status";
            this.gridColPDNStatus.Name = "gridColPDNStatus";
            this.gridColPDNStatus.OptionsColumn.AllowEdit = false;
            this.gridColPDNStatus.Visible = true;
            this.gridColPDNStatus.VisibleIndex = 13;
            this.gridColPDNStatus.Width = 60;
            // 
            // gridColPDNLogin
            // 
            this.gridColPDNLogin.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNLogin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDNLogin.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNLogin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNLogin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNLogin.Caption = "Логин последнего редактирования";
            this.gridColPDNLogin.FieldName = "login";
            this.gridColPDNLogin.Name = "gridColPDNLogin";
            this.gridColPDNLogin.OptionsColumn.AllowEdit = false;
            this.gridColPDNLogin.Visible = true;
            this.gridColPDNLogin.VisibleIndex = 14;
            this.gridColPDNLogin.Width = 160;
            // 
            // gridColPDNDLM
            // 
            this.gridColPDNDLM.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDNDLM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDNDLM.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDNDLM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDNDLM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDNDLM.Caption = "Дата последнего редактирования";
            this.gridColPDNDLM.FieldName = "dlm";
            this.gridColPDNDLM.Name = "gridColPDNDLM";
            this.gridColPDNDLM.OptionsColumn.AllowEdit = false;
            this.gridColPDNDLM.Visible = true;
            this.gridColPDNDLM.VisibleIndex = 15;
            this.gridColPDNDLM.Width = 140;
            // 
            // gridColPDRNError
            // 
            this.gridColPDRNError.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDRNError.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDRNError.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDRNError.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDRNError.Caption = "Ошибка";
            this.gridColPDRNError.FieldName = "ErrorMessage";
            this.gridColPDRNError.Name = "gridColPDRNError";
            this.gridColPDRNError.OptionsColumn.AllowEdit = false;
            this.gridColPDRNError.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPDRNError.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPDRNError.Width = 240;
            // 
            // UserControlPromoDiscountsNet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlPromoDiscountsNet);
            this.Name = "UserControlPromoDiscountsNet";
            this.Size = new System.Drawing.Size(1282, 691);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPromoDiscountsNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlPromoDiscountsNet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNRecordId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNCustCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNCustName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNNetworkCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNNetworkName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNLogin;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNDLM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNDistrId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNHLCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDNHLName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDRNCheck;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDRNError;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
