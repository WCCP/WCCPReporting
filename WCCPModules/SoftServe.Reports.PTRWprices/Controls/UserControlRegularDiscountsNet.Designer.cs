﻿namespace SoftServe.Reports.PTRWprices.Controls
{
    partial class UserControlRegularDiscountsNet
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlRegularDiscountsNet = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColRDNCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNCustCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNCustName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNNetworkCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNNetworkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColRDNStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNDLM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDNError = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRegularDiscountsNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlRegularDiscountsNet
            // 
            this.gridControlRegularDiscountsNet.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlRegularDiscountsNet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRegularDiscountsNet.Location = new System.Drawing.Point(0, 0);
            this.gridControlRegularDiscountsNet.MainView = this.gridView1;
            this.gridControlRegularDiscountsNet.Name = "gridControlRegularDiscountsNet";
            this.gridControlRegularDiscountsNet.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControlRegularDiscountsNet.Size = new System.Drawing.Size(1336, 787);
            this.gridControlRegularDiscountsNet.TabIndex = 2;
            this.gridControlRegularDiscountsNet.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColRDNCheck,
            this.gridColRDN,
            this.gridColRDNCustCode,
            this.gridColRDNCustName,
            this.gridColRDNNetworkCode,
            this.gridColRDNNetworkName,
            this.gridColRDNPrice,
            this.gridColRDNStartDate,
            this.gridColRDNEndDate,
            this.gridColRDNStatus,
            this.gridColRDNLogin,
            this.gridColRDNDLM,
            this.gridColRDNError});
            this.gridView1.GridControl = this.gridControlRegularDiscountsNet;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            // 
            // gridColRDNCheck
            // 
            this.gridColRDNCheck.Caption = "Все";
            this.gridColRDNCheck.FieldName = "Check";
            this.gridColRDNCheck.MaxWidth = 60;
            this.gridColRDNCheck.MinWidth = 60;
            this.gridColRDNCheck.Name = "gridColRDNCheck";
            this.gridColRDNCheck.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColRDNCheck.OptionsColumn.AllowMove = false;
            this.gridColRDNCheck.OptionsColumn.AllowShowHide = false;
            this.gridColRDNCheck.OptionsColumn.AllowSize = false;
            this.gridColRDNCheck.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColRDNCheck.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColRDNCheck.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColRDNCheck.OptionsFilter.AllowAutoFilter = false;
            this.gridColRDNCheck.Visible = true;
            this.gridColRDNCheck.VisibleIndex = 0;
            this.gridColRDNCheck.Width = 60;
            // 
            // gridColRDN
            // 
            this.gridColRDN.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDN.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDN.Caption = "ID записи";
            this.gridColRDN.FieldName = "Id";
            this.gridColRDN.Name = "gridColRDN";
            this.gridColRDN.OptionsColumn.AllowEdit = false;
            this.gridColRDN.Visible = true;
            this.gridColRDN.VisibleIndex = 1;
            // 
            // gridColRDNCustCode
            // 
            this.gridColRDNCustCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNCustCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDNCustCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNCustCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNCustCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNCustCode.Caption = "Код ТС";
            this.gridColRDNCustCode.FieldName = "Cust_Id";
            this.gridColRDNCustCode.Name = "gridColRDNCustCode";
            this.gridColRDNCustCode.OptionsColumn.AllowEdit = false;
            this.gridColRDNCustCode.Visible = true;
            this.gridColRDNCustCode.VisibleIndex = 2;
            this.gridColRDNCustCode.Width = 90;
            // 
            // gridColRDNCustName
            // 
            this.gridColRDNCustName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNCustName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDNCustName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNCustName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNCustName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNCustName.Caption = "Название ТС";
            this.gridColRDNCustName.FieldName = "Cust_NAME";
            this.gridColRDNCustName.Name = "gridColRDNCustName";
            this.gridColRDNCustName.OptionsColumn.AllowEdit = false;
            this.gridColRDNCustName.Visible = true;
            this.gridColRDNCustName.VisibleIndex = 3;
            this.gridColRDNCustName.Width = 200;
            // 
            // gridColRDNNetworkCode
            // 
            this.gridColRDNNetworkCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNNetworkCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDNNetworkCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNNetworkCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNNetworkCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNNetworkCode.Caption = "Код Сети";
            this.gridColRDNNetworkCode.FieldName = "GrdCode";
            this.gridColRDNNetworkCode.Name = "gridColRDNNetworkCode";
            this.gridColRDNNetworkCode.OptionsColumn.AllowEdit = false;
            this.gridColRDNNetworkCode.Visible = true;
            this.gridColRDNNetworkCode.VisibleIndex = 4;
            this.gridColRDNNetworkCode.Width = 90;
            // 
            // gridColRDNNetworkName
            // 
            this.gridColRDNNetworkName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNNetworkName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDNNetworkName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNNetworkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNNetworkName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNNetworkName.Caption = "Название сети";
            this.gridColRDNNetworkName.FieldName = "Network_Name";
            this.gridColRDNNetworkName.Name = "gridColRDNNetworkName";
            this.gridColRDNNetworkName.OptionsColumn.AllowEdit = false;
            this.gridColRDNNetworkName.Visible = true;
            this.gridColRDNNetworkName.VisibleIndex = 5;
            this.gridColRDNNetworkName.Width = 200;
            // 
            // gridColRDNPrice
            // 
            this.gridColRDNPrice.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDNPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNPrice.Caption = "% скидки";
            this.gridColRDNPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColRDNPrice.FieldName = "PercentD";
            this.gridColRDNPrice.Name = "gridColRDNPrice";
            this.gridColRDNPrice.OptionsColumn.AllowEdit = false;
            this.gridColRDNPrice.Visible = true;
            this.gridColRDNPrice.VisibleIndex = 6;
            this.gridColRDNPrice.Width = 60;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "##0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColRDNStartDate
            // 
            this.gridColRDNStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDNStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNStartDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNStartDate.Caption = "StartDate";
            this.gridColRDNStartDate.FieldName = "StartDate";
            this.gridColRDNStartDate.Name = "gridColRDNStartDate";
            this.gridColRDNStartDate.OptionsColumn.AllowEdit = false;
            this.gridColRDNStartDate.Visible = true;
            this.gridColRDNStartDate.VisibleIndex = 7;
            this.gridColRDNStartDate.Width = 80;
            // 
            // gridColRDNEndDate
            // 
            this.gridColRDNEndDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNEndDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDNEndDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNEndDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNEndDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNEndDate.Caption = "EndDate";
            this.gridColRDNEndDate.FieldName = "EndDate";
            this.gridColRDNEndDate.Name = "gridColRDNEndDate";
            this.gridColRDNEndDate.OptionsColumn.AllowEdit = false;
            this.gridColRDNEndDate.Visible = true;
            this.gridColRDNEndDate.VisibleIndex = 8;
            this.gridColRDNEndDate.Width = 80;
            // 
            // gridColRDNStatus
            // 
            this.gridColRDNStatus.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNStatus.Caption = "Статус";
            this.gridColRDNStatus.FieldName = "status";
            this.gridColRDNStatus.Name = "gridColRDNStatus";
            this.gridColRDNStatus.OptionsColumn.AllowEdit = false;
            this.gridColRDNStatus.Visible = true;
            this.gridColRDNStatus.VisibleIndex = 9;
            this.gridColRDNStatus.Width = 60;
            // 
            // gridColRDNLogin
            // 
            this.gridColRDNLogin.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNLogin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDNLogin.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNLogin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNLogin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNLogin.Caption = "Логин последнего редактирования";
            this.gridColRDNLogin.FieldName = "login";
            this.gridColRDNLogin.Name = "gridColRDNLogin";
            this.gridColRDNLogin.OptionsColumn.AllowEdit = false;
            this.gridColRDNLogin.Visible = true;
            this.gridColRDNLogin.VisibleIndex = 10;
            this.gridColRDNLogin.Width = 160;
            // 
            // gridColRDNDLM
            // 
            this.gridColRDNDLM.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNDLM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDNDLM.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNDLM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNDLM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDNDLM.Caption = "Дата последнего редактирования";
            this.gridColRDNDLM.FieldName = "dlm";
            this.gridColRDNDLM.Name = "gridColRDNDLM";
            this.gridColRDNDLM.OptionsColumn.AllowEdit = false;
            this.gridColRDNDLM.Visible = true;
            this.gridColRDNDLM.VisibleIndex = 11;
            this.gridColRDNDLM.Width = 140;
            // 
            // gridColRDNError
            // 
            this.gridColRDNError.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDNError.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDNError.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDNError.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDNError.Caption = "Ошибка";
            this.gridColRDNError.FieldName = "ErrorMessage";
            this.gridColRDNError.Name = "gridColRDNError";
            this.gridColRDNError.OptionsColumn.AllowEdit = false;
            this.gridColRDNError.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColRDNError.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColRDNError.Width = 240;
            // 
            // UserControlRegularDiscountsNet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlRegularDiscountsNet);
            this.Name = "UserControlRegularDiscountsNet";
            this.Size = new System.Drawing.Size(1336, 787);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRegularDiscountsNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlRegularDiscountsNet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDN;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNCustCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNCustName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNNetworkCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNNetworkName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNLogin;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNDLM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNCheck;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDNError;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
