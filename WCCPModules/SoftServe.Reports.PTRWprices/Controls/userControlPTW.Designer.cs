﻿namespace SoftServe.Reports.PTRWprices.Controls
{
    partial class UserControlPTW
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlPTW = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPTRWCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWRecordId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWCustCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWCustName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWCombiProdCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWCombiProdName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWProdCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWProdName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColPTRWStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWDLM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPTRWError = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPTW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlPTW
            // 
            this.gridControlPTW.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlPTW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPTW.Location = new System.Drawing.Point(0, 0);
            this.gridControlPTW.MainView = this.gridView1;
            this.gridControlPTW.Name = "gridControlPTW";
            this.gridControlPTW.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControlPTW.Size = new System.Drawing.Size(1147, 622);
            this.gridControlPTW.TabIndex = 1;
            this.gridControlPTW.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPTRWCheck,
            this.gridColPTRWRecordId,
            this.gridColPTRWCustCode,
            this.gridColPTRWCustName,
            this.gridColPTRWCombiProdCode,
            this.gridColPTRWCombiProdName,
            this.gridColPTRWProdCode,
            this.gridColPTRWProdName,
            this.gridColPTRWPrice,
            this.gridColPTRWStartDate,
            this.gridColPTRWEndDate,
            this.gridColPTRWStatus,
            this.gridColPTRWLogin,
            this.gridColPTRWDLM,
            this.gridColPTRWError});
            this.gridView1.GridControl = this.gridControlPTW;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            // 
            // gridColPTRWCheck
            // 
            this.gridColPTRWCheck.Caption = "Все";
            this.gridColPTRWCheck.FieldName = "Check";
            this.gridColPTRWCheck.MaxWidth = 60;
            this.gridColPTRWCheck.MinWidth = 60;
            this.gridColPTRWCheck.Name = "gridColPTRWCheck";
            this.gridColPTRWCheck.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPTRWCheck.OptionsColumn.AllowMove = false;
            this.gridColPTRWCheck.OptionsColumn.AllowShowHide = false;
            this.gridColPTRWCheck.OptionsColumn.AllowSize = false;
            this.gridColPTRWCheck.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPTRWCheck.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPTRWCheck.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPTRWCheck.OptionsFilter.AllowAutoFilter = false;
            this.gridColPTRWCheck.Visible = true;
            this.gridColPTRWCheck.VisibleIndex = 0;
            this.gridColPTRWCheck.Width = 60;
            // 
            // gridColPTRWRecordId
            // 
            this.gridColPTRWRecordId.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWRecordId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWRecordId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWRecordId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWRecordId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWRecordId.Caption = "ID записи";
            this.gridColPTRWRecordId.FieldName = "Id";
            this.gridColPTRWRecordId.Name = "gridColPTRWRecordId";
            this.gridColPTRWRecordId.OptionsColumn.AllowEdit = false;
            this.gridColPTRWRecordId.Visible = true;
            this.gridColPTRWRecordId.VisibleIndex = 1;
            // 
            // gridColPTRWCustCode
            // 
            this.gridColPTRWCustCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWCustCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWCustCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWCustCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWCustCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWCustCode.Caption = "Код ТС";
            this.gridColPTRWCustCode.FieldName = "Cust_Id";
            this.gridColPTRWCustCode.Name = "gridColPTRWCustCode";
            this.gridColPTRWCustCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRWCustCode.Visible = true;
            this.gridColPTRWCustCode.VisibleIndex = 2;
            this.gridColPTRWCustCode.Width = 90;
            // 
            // gridColPTRWCustName
            // 
            this.gridColPTRWCustName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWCustName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWCustName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWCustName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWCustName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWCustName.Caption = "Название ТС";
            this.gridColPTRWCustName.FieldName = "Cust_NAME";
            this.gridColPTRWCustName.Name = "gridColPTRWCustName";
            this.gridColPTRWCustName.OptionsColumn.AllowEdit = false;
            this.gridColPTRWCustName.Visible = true;
            this.gridColPTRWCustName.VisibleIndex = 3;
            this.gridColPTRWCustName.Width = 200;
            // 
            // gridColPTRWCombiProdCode
            // 
            this.gridColPTRWCombiProdCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWCombiProdCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWCombiProdCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWCombiProdCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWCombiProdCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWCombiProdCode.Caption = "Код комбипродукта";
            this.gridColPTRWCombiProdCode.FieldName = "HLCode";
            this.gridColPTRWCombiProdCode.Name = "gridColPTRWCombiProdCode";
            this.gridColPTRWCombiProdCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRWCombiProdCode.Visible = true;
            this.gridColPTRWCombiProdCode.VisibleIndex = 4;
            this.gridColPTRWCombiProdCode.Width = 90;
            // 
            // gridColPTRWCombiProdName
            // 
            this.gridColPTRWCombiProdName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWCombiProdName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWCombiProdName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWCombiProdName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWCombiProdName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWCombiProdName.Caption = "Название комбипродукта";
            this.gridColPTRWCombiProdName.FieldName = "ProductCnName";
            this.gridColPTRWCombiProdName.Name = "gridColPTRWCombiProdName";
            this.gridColPTRWCombiProdName.OptionsColumn.AllowEdit = false;
            this.gridColPTRWCombiProdName.Visible = true;
            this.gridColPTRWCombiProdName.VisibleIndex = 5;
            this.gridColPTRWCombiProdName.Width = 200;
            // 
            // gridColPTRWProdCode
            // 
            this.gridColPTRWProdCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWProdCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWProdCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWProdCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWProdCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWProdCode.Caption = "Код продукта";
            this.gridColPTRWProdCode.FieldName = "ProductCode";
            this.gridColPTRWProdCode.Name = "gridColPTRWProdCode";
            this.gridColPTRWProdCode.OptionsColumn.AllowEdit = false;
            this.gridColPTRWProdCode.Visible = true;
            this.gridColPTRWProdCode.VisibleIndex = 6;
            this.gridColPTRWProdCode.Width = 90;
            // 
            // gridColPTRWProdName
            // 
            this.gridColPTRWProdName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWProdName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWProdName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWProdName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWProdName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWProdName.Caption = "Название продукта";
            this.gridColPTRWProdName.FieldName = "ProductName";
            this.gridColPTRWProdName.Name = "gridColPTRWProdName";
            this.gridColPTRWProdName.OptionsColumn.AllowEdit = false;
            this.gridColPTRWProdName.Visible = true;
            this.gridColPTRWProdName.VisibleIndex = 7;
            this.gridColPTRWProdName.Width = 200;
            // 
            // gridColPTRWPrice
            // 
            this.gridColPTRWPrice.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTRWPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWPrice.Caption = "Цена";
            this.gridColPTRWPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColPTRWPrice.FieldName = "Price";
            this.gridColPTRWPrice.Name = "gridColPTRWPrice";
            this.gridColPTRWPrice.OptionsColumn.AllowEdit = false;
            this.gridColPTRWPrice.Visible = true;
            this.gridColPTRWPrice.VisibleIndex = 8;
            this.gridColPTRWPrice.Width = 100;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "######0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColPTRWStartDate
            // 
            this.gridColPTRWStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTRWStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWStartDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWStartDate.Caption = "StartDate";
            this.gridColPTRWStartDate.FieldName = "StartDate";
            this.gridColPTRWStartDate.Name = "gridColPTRWStartDate";
            this.gridColPTRWStartDate.OptionsColumn.AllowEdit = false;
            this.gridColPTRWStartDate.Visible = true;
            this.gridColPTRWStartDate.VisibleIndex = 9;
            this.gridColPTRWStartDate.Width = 80;
            // 
            // gridColPTRWEndDate
            // 
            this.gridColPTRWEndDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWEndDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTRWEndDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWEndDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWEndDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWEndDate.Caption = "EndDate";
            this.gridColPTRWEndDate.FieldName = "EndDate";
            this.gridColPTRWEndDate.Name = "gridColPTRWEndDate";
            this.gridColPTRWEndDate.OptionsColumn.AllowEdit = false;
            this.gridColPTRWEndDate.Visible = true;
            this.gridColPTRWEndDate.VisibleIndex = 10;
            this.gridColPTRWEndDate.Width = 80;
            // 
            // gridColPTRWStatus
            // 
            this.gridColPTRWStatus.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWStatus.Caption = "Статус";
            this.gridColPTRWStatus.FieldName = "status";
            this.gridColPTRWStatus.Name = "gridColPTRWStatus";
            this.gridColPTRWStatus.OptionsColumn.AllowEdit = false;
            this.gridColPTRWStatus.Visible = true;
            this.gridColPTRWStatus.VisibleIndex = 11;
            this.gridColPTRWStatus.Width = 60;
            // 
            // gridColPTRWLogin
            // 
            this.gridColPTRWLogin.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWLogin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWLogin.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWLogin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWLogin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWLogin.Caption = "Логин последнего редактирования";
            this.gridColPTRWLogin.FieldName = "login";
            this.gridColPTRWLogin.Name = "gridColPTRWLogin";
            this.gridColPTRWLogin.OptionsColumn.AllowEdit = false;
            this.gridColPTRWLogin.Visible = true;
            this.gridColPTRWLogin.VisibleIndex = 12;
            this.gridColPTRWLogin.Width = 160;
            // 
            // gridColPTRWDLM
            // 
            this.gridColPTRWDLM.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWDLM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPTRWDLM.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWDLM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWDLM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPTRWDLM.Caption = "Дата последнего редактирования";
            this.gridColPTRWDLM.FieldName = "dlm";
            this.gridColPTRWDLM.Name = "gridColPTRWDLM";
            this.gridColPTRWDLM.OptionsColumn.AllowEdit = false;
            this.gridColPTRWDLM.Visible = true;
            this.gridColPTRWDLM.VisibleIndex = 13;
            this.gridColPTRWDLM.Width = 140;
            // 
            // gridColPTRWError
            // 
            this.gridColPTRWError.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPTRWError.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPTRWError.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPTRWError.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPTRWError.Caption = "Ошибка";
            this.gridColPTRWError.FieldName = "ErrorMessage";
            this.gridColPTRWError.Name = "gridColPTRWError";
            this.gridColPTRWError.OptionsColumn.AllowEdit = false;
            this.gridColPTRWError.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPTRWError.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPTRWError.Width = 240;
            // 
            // UserControlPTW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlPTW);
            this.Name = "UserControlPTW";
            this.Size = new System.Drawing.Size(1147, 622);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPTW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlPTW;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWRecordId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWCustCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWCustName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWCombiProdCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWCombiProdName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWProdCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWProdName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWLogin;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWDLM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWCheck;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPTRWError;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
