﻿namespace SoftServe.Reports.PTRWprices.Controls
{
    partial class UserControlPromoDiscounts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlPromoDiscounts = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPDRCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDRecordId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDPriorityNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDPriorityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDCustCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDCustName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDProductCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColPDStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColPDEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDDLM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPDRError = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPromoDiscounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlPromoDiscounts
            // 
            this.gridControlPromoDiscounts.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlPromoDiscounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPromoDiscounts.Location = new System.Drawing.Point(0, 0);
            this.gridControlPromoDiscounts.MainView = this.gridView2;
            this.gridControlPromoDiscounts.Name = "gridControlPromoDiscounts";
            this.gridControlPromoDiscounts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit,
            this.repositoryItemDateEdit,
            this.repositoryItemTextEdit1});
            this.gridControlPromoDiscounts.Size = new System.Drawing.Size(1236, 597);
            this.gridControlPromoDiscounts.TabIndex = 2;
            this.gridControlPromoDiscounts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.ColumnPanelRowHeight = 40;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPDRCheck,
            this.gridColPDRecordId,
            this.gridColPDPriorityNumber,
            this.gridColPDPriorityName,
            this.gridColPDCustCode,
            this.gridColPDCustName,
            this.gridColPDProductCode,
            this.gridColPDProductName,
            this.gridColPDPrice,
            this.gridColPDStatus,
            this.gridColPDLogin,
            this.gridColPDStartDate,
            this.gridColPDEndDate,
            this.gridColPDDLM,
            this.gridColPDRError});
            this.gridView2.GridControl = this.gridControlPromoDiscounts;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            // 
            // gridColPDRCheck
            // 
            this.gridColPDRCheck.Caption = "Все";
            this.gridColPDRCheck.FieldName = "Check";
            this.gridColPDRCheck.MaxWidth = 60;
            this.gridColPDRCheck.MinWidth = 60;
            this.gridColPDRCheck.Name = "gridColPDRCheck";
            this.gridColPDRCheck.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPDRCheck.OptionsColumn.AllowMove = false;
            this.gridColPDRCheck.OptionsColumn.AllowShowHide = false;
            this.gridColPDRCheck.OptionsColumn.AllowSize = false;
            this.gridColPDRCheck.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColPDRCheck.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPDRCheck.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPDRCheck.OptionsFilter.AllowAutoFilter = false;
            this.gridColPDRCheck.Visible = true;
            this.gridColPDRCheck.VisibleIndex = 0;
            this.gridColPDRCheck.Width = 60;
            // 
            // gridColPDRecordId
            // 
            this.gridColPDRecordId.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDRecordId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDRecordId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDRecordId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDRecordId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDRecordId.Caption = "ID записи";
            this.gridColPDRecordId.FieldName = "Id";
            this.gridColPDRecordId.Name = "gridColPDRecordId";
            this.gridColPDRecordId.OptionsColumn.AllowEdit = false;
            this.gridColPDRecordId.Visible = true;
            this.gridColPDRecordId.VisibleIndex = 1;
            this.gridColPDRecordId.Width = 90;
            // 
            // gridColPDPriorityNumber
            // 
            this.gridColPDPriorityNumber.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDPriorityNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDPriorityNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDPriorityNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDPriorityNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDPriorityNumber.Caption = "Номер приоритетного списка";
            this.gridColPDPriorityNumber.FieldName = "NomerPS";
            this.gridColPDPriorityNumber.Name = "gridColPDPriorityNumber";
            this.gridColPDPriorityNumber.OptionsColumn.AllowEdit = false;
            this.gridColPDPriorityNumber.Visible = true;
            this.gridColPDPriorityNumber.VisibleIndex = 2;
            this.gridColPDPriorityNumber.Width = 90;
            // 
            // gridColPDPriorityName
            // 
            this.gridColPDPriorityName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDPriorityName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDPriorityName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDPriorityName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDPriorityName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDPriorityName.Caption = "Название ПС";
            this.gridColPDPriorityName.FieldName = "NamePS";
            this.gridColPDPriorityName.Name = "gridColPDPriorityName";
            this.gridColPDPriorityName.OptionsColumn.AllowEdit = false;
            this.gridColPDPriorityName.Visible = true;
            this.gridColPDPriorityName.VisibleIndex = 3;
            this.gridColPDPriorityName.Width = 200;
            // 
            // gridColPDCustCode
            // 
            this.gridColPDCustCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDCustCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDCustCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDCustCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDCustCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDCustCode.Caption = "Код ТС";
            this.gridColPDCustCode.FieldName = "Cust_Id";
            this.gridColPDCustCode.Name = "gridColPDCustCode";
            this.gridColPDCustCode.OptionsColumn.AllowEdit = false;
            this.gridColPDCustCode.Visible = true;
            this.gridColPDCustCode.VisibleIndex = 4;
            this.gridColPDCustCode.Width = 90;
            // 
            // gridColPDCustName
            // 
            this.gridColPDCustName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDCustName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDCustName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDCustName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDCustName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDCustName.Caption = "Название ТС";
            this.gridColPDCustName.FieldName = "Cust_NAME";
            this.gridColPDCustName.Name = "gridColPDCustName";
            this.gridColPDCustName.OptionsColumn.AllowEdit = false;
            this.gridColPDCustName.Visible = true;
            this.gridColPDCustName.VisibleIndex = 5;
            this.gridColPDCustName.Width = 200;
            // 
            // gridColPDProductCode
            // 
            this.gridColPDProductCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDProductCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDProductCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDProductCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDProductCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDProductCode.Caption = "Код продукта";
            this.gridColPDProductCode.FieldName = "ProductCode";
            this.gridColPDProductCode.Name = "gridColPDProductCode";
            this.gridColPDProductCode.OptionsColumn.AllowEdit = false;
            this.gridColPDProductCode.Visible = true;
            this.gridColPDProductCode.VisibleIndex = 6;
            this.gridColPDProductCode.Width = 90;
            // 
            // gridColPDProductName
            // 
            this.gridColPDProductName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDProductName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDProductName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDProductName.Caption = "Название продукта";
            this.gridColPDProductName.FieldName = "ProductName";
            this.gridColPDProductName.Name = "gridColPDProductName";
            this.gridColPDProductName.OptionsColumn.AllowEdit = false;
            this.gridColPDProductName.Visible = true;
            this.gridColPDProductName.VisibleIndex = 7;
            this.gridColPDProductName.Width = 200;
            // 
            // gridColPDPrice
            // 
            this.gridColPDPrice.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDPrice.Caption = "% скидки";
            this.gridColPDPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColPDPrice.FieldName = "PercentD";
            this.gridColPDPrice.Name = "gridColPDPrice";
            this.gridColPDPrice.OptionsColumn.AllowEdit = false;
            this.gridColPDPrice.Visible = true;
            this.gridColPDPrice.VisibleIndex = 8;
            this.gridColPDPrice.Width = 60;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "##0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColPDStatus
            // 
            this.gridColPDStatus.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDStatus.Caption = "Статус";
            this.gridColPDStatus.FieldName = "status";
            this.gridColPDStatus.Name = "gridColPDStatus";
            this.gridColPDStatus.OptionsColumn.AllowEdit = false;
            this.gridColPDStatus.Visible = true;
            this.gridColPDStatus.VisibleIndex = 11;
            this.gridColPDStatus.Width = 60;
            // 
            // gridColPDLogin
            // 
            this.gridColPDLogin.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDLogin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDLogin.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDLogin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDLogin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDLogin.Caption = "Логин последнего редактирования";
            this.gridColPDLogin.FieldName = "login";
            this.gridColPDLogin.Name = "gridColPDLogin";
            this.gridColPDLogin.OptionsColumn.AllowEdit = false;
            this.gridColPDLogin.Visible = true;
            this.gridColPDLogin.VisibleIndex = 12;
            this.gridColPDLogin.Width = 160;
            // 
            // gridColPDStartDate
            // 
            this.gridColPDStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDStartDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDStartDate.Caption = "StartDate";
            this.gridColPDStartDate.ColumnEdit = this.repositoryItemDateEdit;
            this.gridColPDStartDate.FieldName = "StartDate";
            this.gridColPDStartDate.Name = "gridColPDStartDate";
            this.gridColPDStartDate.OptionsColumn.AllowEdit = false;
            this.gridColPDStartDate.Visible = true;
            this.gridColPDStartDate.VisibleIndex = 9;
            this.gridColPDStartDate.Width = 80;
            // 
            // repositoryItemDateEdit
            // 
            this.repositoryItemDateEdit.AutoHeight = false;
            this.repositoryItemDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit.Name = "repositoryItemDateEdit";
            // 
            // gridColPDEndDate
            // 
            this.gridColPDEndDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDEndDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDEndDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDEndDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDEndDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDEndDate.Caption = "EndDate";
            this.gridColPDEndDate.ColumnEdit = this.repositoryItemDateEdit;
            this.gridColPDEndDate.FieldName = "EndDate";
            this.gridColPDEndDate.Name = "gridColPDEndDate";
            this.gridColPDEndDate.OptionsColumn.AllowEdit = false;
            this.gridColPDEndDate.Visible = true;
            this.gridColPDEndDate.VisibleIndex = 10;
            this.gridColPDEndDate.Width = 80;
            // 
            // gridColPDDLM
            // 
            this.gridColPDDLM.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDDLM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColPDDLM.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDDLM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDDLM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColPDDLM.Caption = "Дата последнего редактирования";
            this.gridColPDDLM.FieldName = "dlm";
            this.gridColPDDLM.Name = "gridColPDDLM";
            this.gridColPDDLM.OptionsColumn.AllowEdit = false;
            this.gridColPDDLM.Visible = true;
            this.gridColPDDLM.VisibleIndex = 13;
            this.gridColPDDLM.Width = 140;
            // 
            // gridColPDRError
            // 
            this.gridColPDRError.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPDRError.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPDRError.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColPDRError.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColPDRError.Caption = "Ошибка";
            this.gridColPDRError.FieldName = "ErrorMessage";
            this.gridColPDRError.Name = "gridColPDRError";
            this.gridColPDRError.OptionsColumn.AllowEdit = false;
            this.gridColPDRError.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColPDRError.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColPDRError.Width = 240;
            // 
            // repositoryItemCheckEdit
            // 
            this.repositoryItemCheckEdit.AutoHeight = false;
            this.repositoryItemCheckEdit.Name = "repositoryItemCheckEdit";
            // 
            // UserControlPromoDiscounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlPromoDiscounts);
            this.Name = "UserControlPromoDiscounts";
            this.Size = new System.Drawing.Size(1236, 597);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPromoDiscounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.GridControl gridControlPromoDiscounts;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDRecordId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDCustCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDCustName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDProductCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDProductName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDLogin;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDDLM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDPriorityNumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDPriorityName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDRError;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPDRCheck;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
