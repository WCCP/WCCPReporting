﻿namespace SoftServe.Reports.PTRWprices.Controls
{
    partial class UserControlRegularDiscounts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlRegularDiscounts = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColRDCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDRecordId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDCustCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDCustName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDTTCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDTTName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColRDStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDDLM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColRDError = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRegularDiscounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlRegularDiscounts
            // 
            this.gridControlRegularDiscounts.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlRegularDiscounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRegularDiscounts.Location = new System.Drawing.Point(0, 0);
            this.gridControlRegularDiscounts.MainView = this.gridView1;
            this.gridControlRegularDiscounts.Name = "gridControlRegularDiscounts";
            this.gridControlRegularDiscounts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControlRegularDiscounts.Size = new System.Drawing.Size(1220, 746);
            this.gridControlRegularDiscounts.TabIndex = 1;
            this.gridControlRegularDiscounts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColRDCheck,
            this.gridColRDRecordId,
            this.gridColRDCustCode,
            this.gridColRDCustName,
            this.gridColRDTTCode,
            this.gridColRDTTName,
            this.gridColRDPrice,
            this.gridColRDStartDate,
            this.gridColRDEndDate,
            this.gridColRDStatus,
            this.gridColRDLogin,
            this.gridColRDDLM,
            this.gridColRDError});
            this.gridView1.GridControl = this.gridControlRegularDiscounts;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            // 
            // gridColRDCheck
            // 
            this.gridColRDCheck.Caption = "Все";
            this.gridColRDCheck.FieldName = "Check";
            this.gridColRDCheck.MaxWidth = 60;
            this.gridColRDCheck.MinWidth = 60;
            this.gridColRDCheck.Name = "gridColRDCheck";
            this.gridColRDCheck.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColRDCheck.OptionsColumn.AllowMove = false;
            this.gridColRDCheck.OptionsColumn.AllowShowHide = false;
            this.gridColRDCheck.OptionsColumn.AllowSize = false;
            this.gridColRDCheck.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColRDCheck.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColRDCheck.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColRDCheck.OptionsFilter.AllowAutoFilter = false;
            this.gridColRDCheck.Visible = true;
            this.gridColRDCheck.VisibleIndex = 0;
            this.gridColRDCheck.Width = 60;
            // 
            // gridColRDRecordId
            // 
            this.gridColRDRecordId.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDRecordId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDRecordId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDRecordId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDRecordId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDRecordId.Caption = "ID записи";
            this.gridColRDRecordId.FieldName = "Id";
            this.gridColRDRecordId.Name = "gridColRDRecordId";
            this.gridColRDRecordId.OptionsColumn.AllowEdit = false;
            this.gridColRDRecordId.Visible = true;
            this.gridColRDRecordId.VisibleIndex = 1;
            // 
            // gridColRDCustCode
            // 
            this.gridColRDCustCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDCustCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDCustCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDCustCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDCustCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDCustCode.Caption = "Код ТС";
            this.gridColRDCustCode.FieldName = "Cust_Id";
            this.gridColRDCustCode.Name = "gridColRDCustCode";
            this.gridColRDCustCode.OptionsColumn.AllowEdit = false;
            this.gridColRDCustCode.Visible = true;
            this.gridColRDCustCode.VisibleIndex = 2;
            this.gridColRDCustCode.Width = 90;
            // 
            // gridColRDCustName
            // 
            this.gridColRDCustName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDCustName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDCustName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDCustName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDCustName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDCustName.Caption = "Название ТС";
            this.gridColRDCustName.FieldName = "Cust_NAME";
            this.gridColRDCustName.Name = "gridColRDCustName";
            this.gridColRDCustName.OptionsColumn.AllowEdit = false;
            this.gridColRDCustName.Visible = true;
            this.gridColRDCustName.VisibleIndex = 3;
            this.gridColRDCustName.Width = 200;
            // 
            // gridColRDTTCode
            // 
            this.gridColRDTTCode.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDTTCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDTTCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDTTCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDTTCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDTTCode.Caption = "Код ТТ";
            this.gridColRDTTCode.FieldName = "Ol_ID";
            this.gridColRDTTCode.Name = "gridColRDTTCode";
            this.gridColRDTTCode.OptionsColumn.AllowEdit = false;
            this.gridColRDTTCode.Visible = true;
            this.gridColRDTTCode.VisibleIndex = 4;
            this.gridColRDTTCode.Width = 90;
            // 
            // gridColRDTTName
            // 
            this.gridColRDTTName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDTTName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDTTName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDTTName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDTTName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDTTName.Caption = "Название ТТ";
            this.gridColRDTTName.FieldName = "OLName";
            this.gridColRDTTName.Name = "gridColRDTTName";
            this.gridColRDTTName.OptionsColumn.AllowEdit = false;
            this.gridColRDTTName.Visible = true;
            this.gridColRDTTName.VisibleIndex = 5;
            this.gridColRDTTName.Width = 200;
            // 
            // gridColRDPrice
            // 
            this.gridColRDPrice.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDPrice.Caption = "% скидки";
            this.gridColRDPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColRDPrice.FieldName = "PercentD";
            this.gridColRDPrice.Name = "gridColRDPrice";
            this.gridColRDPrice.OptionsColumn.AllowEdit = false;
            this.gridColRDPrice.Visible = true;
            this.gridColRDPrice.VisibleIndex = 6;
            this.gridColRDPrice.Width = 60;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "##0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColRDStartDate
            // 
            this.gridColRDStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDStartDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDStartDate.Caption = "StartDate";
            this.gridColRDStartDate.FieldName = "StartDate";
            this.gridColRDStartDate.Name = "gridColRDStartDate";
            this.gridColRDStartDate.OptionsColumn.AllowEdit = false;
            this.gridColRDStartDate.Visible = true;
            this.gridColRDStartDate.VisibleIndex = 7;
            this.gridColRDStartDate.Width = 80;
            // 
            // gridColRDEndDate
            // 
            this.gridColRDEndDate.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDEndDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDEndDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDEndDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDEndDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDEndDate.Caption = "EndDate";
            this.gridColRDEndDate.FieldName = "EndDate";
            this.gridColRDEndDate.Name = "gridColRDEndDate";
            this.gridColRDEndDate.OptionsColumn.AllowEdit = false;
            this.gridColRDEndDate.Visible = true;
            this.gridColRDEndDate.VisibleIndex = 8;
            this.gridColRDEndDate.Width = 80;
            // 
            // gridColRDStatus
            // 
            this.gridColRDStatus.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDStatus.Caption = "Статус";
            this.gridColRDStatus.FieldName = "status";
            this.gridColRDStatus.Name = "gridColRDStatus";
            this.gridColRDStatus.OptionsColumn.AllowEdit = false;
            this.gridColRDStatus.Visible = true;
            this.gridColRDStatus.VisibleIndex = 9;
            this.gridColRDStatus.Width = 60;
            // 
            // gridColRDLogin
            // 
            this.gridColRDLogin.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDLogin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDLogin.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDLogin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDLogin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDLogin.Caption = "Логин последнего редактирования";
            this.gridColRDLogin.FieldName = "login";
            this.gridColRDLogin.Name = "gridColRDLogin";
            this.gridColRDLogin.OptionsColumn.AllowEdit = false;
            this.gridColRDLogin.Visible = true;
            this.gridColRDLogin.VisibleIndex = 10;
            this.gridColRDLogin.Width = 160;
            // 
            // gridColRDDLM
            // 
            this.gridColRDDLM.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDDLM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColRDDLM.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDDLM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDDLM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColRDDLM.Caption = "Дата последнего редактирования";
            this.gridColRDDLM.FieldName = "dlm";
            this.gridColRDDLM.Name = "gridColRDDLM";
            this.gridColRDDLM.OptionsColumn.AllowEdit = false;
            this.gridColRDDLM.Visible = true;
            this.gridColRDDLM.VisibleIndex = 11;
            this.gridColRDDLM.Width = 140;
            // 
            // gridColRDError
            // 
            this.gridColRDError.AppearanceCell.Options.UseTextOptions = true;
            this.gridColRDError.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColRDError.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColRDError.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColRDError.Caption = "Ошибка";
            this.gridColRDError.FieldName = "ErrorMessage";
            this.gridColRDError.Name = "gridColRDError";
            this.gridColRDError.OptionsColumn.AllowEdit = false;
            this.gridColRDError.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColRDError.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColRDError.Width = 240;
            // 
            // UserControlRegularDiscounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlRegularDiscounts);
            this.Name = "UserControlRegularDiscounts";
            this.Size = new System.Drawing.Size(1220, 746);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRegularDiscounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlRegularDiscounts;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDRecordId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDCustCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDCustName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDTTCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDTTName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDLogin;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDDLM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDCheck;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRDError;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
