﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.PLinq.Helpers;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SoftServe.Reports.PTRWprices.Utils;
using SoftServe.Reports.PTRWprices.Models;
using SoftServe.Reports.PTRWprices.DataAccess;

namespace SoftServe.Reports.PTRWprices.Controls
{
    public partial class UserControlPTR : UserControl, ITabReportView, IPriceType
    {
        public event EventHandler StartEdit;
        public event EventHandler EndEdit;        
        Controler _controler;

        public UserControlPTR()
        {
            InitializeComponent();            
            _controler = new Controler(this);
            _controler.SetEditableColumns(gridColPTRCheck, gridColPTRPrice, gridColPTRStartDate, gridColPTREndDate, gridColPTRStatus, gridColPTRError);
        }

        public PtRwTypes Type
        {
            get { return PtRwTypes.PTRPrices; }
        }
        public GridView View { get { return gridView1; } }
        public bool IsInEdit { get; private set; }

        public void LoadData(Settings settings)
        {
            _controler.LoadData(settings);
        }

        public void Export(string path)
        {
            gridControlPTR.ExportToXlsx(path);
        }

        public void RestoreLayout(string path)
        {

        }

        public void SaveLayout(string path)
        {

        }
        
        public void GoEdit()
        {
            _controler.GoEdit();
        }

        public void StopEdit()
        {
            _controler.StopEdit();
        }

        public void OnEditMOde()
        {
            StartEdit(this, new EventArgs());
            IsInEdit = true;
        }

        public void OnCancelEditMode()
        {
            EndEdit(this, new EventArgs());
            IsInEdit = false;
        }

        public void SaveEdit()
        {
            _controler.SaveEdit();
        }

        public void DeactivateEdit()
        {
            _controler.DeactivateEdit();
        }

        public void ActivateEdit()
        {
            _controler.ActivateEdit();
        }
    }
}