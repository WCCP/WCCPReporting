﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.Data.Linq;
using BLToolkit.Mapping;
using DevExpress.XtraRichEdit.API.Word;
using Logica.Reports.DataAccess;
using SoftServe.Reports.PTRWprices.Models;
using SoftServe.Reports.PTRWprices.Utils;
using System.Globalization;
using Settings = SoftServe.Reports.PTRWprices.Models.Settings;

namespace SoftServe.Reports.PTRWprices.DataAccess
{
    static class DataProvider
    {
        #region Fields
        private static DbManager _db;
        private static string _connectionString = String.Empty;
        #endregion

        #region Properties
        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManagerInit();
                }
                else if (CheckDbWasChanged())
                {
                    _db.Close();
                    DbManagerInit();
                }
                return _db;
            }
        }
        #endregion

        private static void DbManagerInit()
        {
            DbManager.DefaultConfiguration = ""; //to reset possible previous changes
            _connectionString = HostConfiguration.DBSqlConnection ??
                                @"Server = eegdadb18; Database = SalesWorks; User Id = dimasik; Password = 3111;";
            DbManager.AddConnectionString(_connectionString);
            _db = new DbManager();
            _db.Command.CommandTimeout = 15 * 60; // 30 minutes by default
        }
        private static bool CheckDbWasChanged()
        {
            return false;//return !HostConfiguration.DBSqlConnection.Contains(_db.Connection.ConnectionString);
        }

        #region SettingForm
        public static int GetUserAccesLevel()
        {
            int level = 0;
            try
            {
               level =  Db.SetSpCommand(Constants.SP_GET_ACCESS_LEVEL).ExecuteScalar<int>();
            }
            catch 
            {
                level = 0;
            }
            return level;
            }
        public static DataTable GetPricesTypes()
        {
            DataTable result =
                Db.SetCommand(Constants.SCR_GET_PRICES_TYPES).ExecuteDataTable();

            return result;
        }
        public static DataTable GetRegions()
        {
            DataTable source = Db.SetSpCommand(Constants.SP_GET_REGIONS).ExecuteDataTable();
            return source;
        }
        public static DataTable GetDistributors()
        {
            DataTable source = Db.SetSpCommand(Constants.SP_GET_DISTRIBUTORS).ExecuteDataTable();
            return source;
        }
        public static DataTable GetCustomers()
        {
            DataTable source = Db.SetSpCommand(Constants.SP_GET_CUSTOMERS).ExecuteDataTable();
            return source;
        }
        public static DataTable GetNetworks()
        {
            DataTable source = Db.SetSpCommand(Constants.SP_GET_NETWORKS).ExecuteDataTable();
            return source;
        }
        public static DataTable GetProductTree(string countryId)
        {
            DataTable dt = Db.SetSpCommand(Constants.SP_GET_TREE, Db.Parameter(Constants.PAR_COUNTRY, countryId)).ExecuteDataTable();
            return dt;

        }
        private static void GetTempTableName(string typeOfPrice, out string tableName)
        {
            tableName = string.Empty;
            Db.SetSpCommand(Constants.SP_GET_TEMP_TABLE,
                Db.Parameter(Constants.PAR_TYPE_FILE, typeOfPrice),
                Db.Parameter(ParameterDirection.Output, Constants.PAR_TABLE_NAME1, tableName)).ExecuteNonQuery();

            tableName = ((IDbDataParameter)Db.Command.Parameters[Constants.PAR_TABLE_NAME1]).Value.ToString();
        }
        internal static DataTable SaveLoadedPlans(DataTable data)
        {
            string tableName;
            IPriceType type = data as IPriceType;
            string typeName = type.Type.ToString();
            GetTempTableName(typeName, out tableName);


            InsertPricesIntoTmp(tableName, data, type);

            SavePricesToDB(tableName, type);
            return GetIncorrectPrices(tableName, data);

            //return data;
        }

        private static void InsertPricesIntoTmp(string tableName, DataTable data, IPriceType type)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                SqlBulkCopy bulk = new SqlBulkCopy(con);
                bulk.DestinationTableName = tableName;

                con.Open();
                bulk.BulkCopyTimeout = 900;
                bulk.WriteToServer(data);
            }
        }
        public static void InitSettingsTable()
        {
            Db.SetCommand(Constants.SCR_CREATE_TEMP_TABLES).ExecuteNonQuery();
        }
        public static void InitEditTable()
        {
            Db.SetCommand(Constants.SCR_CREATE_TEMP_TABLES_EDIT).ExecuteNonQuery();
        }

        public static List<PromoDiscount> GetPromoView(Settings sett)
        {
            List<PromoDiscount> source = Db.SetSpCommand(Constants.SP_GET_VIEW_PROMO_DISCOUNT,
                Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
                Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
                Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
                Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<PromoDiscount>();
            return source;
        }
        public static List<PromoDiscount> GetEditPromoData(List<long> ids, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWEdit(ids);

            List<PromoDiscount> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 2))
                    .ExecuteList<PromoDiscount>();
            return lst;
        }
        public static List<PromoDiscount> SaveEditedPromoData(Dictionary<long, EditModel> rows, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWSave(rows);

            List<PromoDiscount> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 1))
                    .ExecuteList<PromoDiscount>();
            return lst;
        }

        public static List<PromoDiscountNet> GetPromoNetView(Settings sett)
        {
            List<PromoDiscountNet> source = Db.SetSpCommand(Constants.SP_GET_VIEW_PROMO_DISCOUNT_NET,
                Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
                Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
                Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
                Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<PromoDiscountNet>();
            return source;
        }
        public static List<PromoDiscountNet> GetEditPromoNetData(List<long> ids, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWEdit(ids);

            List<PromoDiscountNet> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 2))
                    .ExecuteList<PromoDiscountNet>();
            return lst;
        }
        public static List<PromoDiscountNet> SaveEditedPromoNetData(Dictionary<long, EditModel> rows, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWSave(rows);

            List<PromoDiscountNet> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 1))
                    .ExecuteList<PromoDiscountNet>();
            return lst;
        }

        public static List<RegularDiscount> GetRegularView(Settings sett)
        {
            List<RegularDiscount> source = Db.SetSpCommand(Constants.SP_GET_VIEW_REGULAR_DISCOUNT,
                Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
                Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
                Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
                Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<RegularDiscount>();
            return source;
        }
        public static List<RegularDiscount> GetEditRegularData(List<long> ids, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWEdit(ids);

            List<RegularDiscount> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 2))
                    .ExecuteList<RegularDiscount>();
            return lst;
        }
        public static List<RegularDiscount> SaveEditedRegularData(Dictionary<long, EditModel> rows, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWSave(rows);

            List<RegularDiscount> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 1))
                    .ExecuteList<RegularDiscount>();
            return lst;
        }


        public static List<RegularDiscountNet> GetRegularNetView(Settings sett)
        {
            List<RegularDiscountNet> source = Db.SetSpCommand(Constants.SP_GET_VIEW_REGULAR_DISCOUNT_NET,
                Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
                Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
                Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
                Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<RegularDiscountNet>();
            return source;
        }
        public static List<RegularDiscountNet> GetEditRegularNetData(List<long> ids, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWEdit(ids);

            List<RegularDiscountNet> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 2))
                    .ExecuteList<RegularDiscountNet>();
            return lst;
        }
        public static List<RegularDiscountNet> SaveEditedRegularNetData(Dictionary<long, EditModel> rows, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWSave(rows);

            List<RegularDiscountNet> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 1))
                    .ExecuteList<RegularDiscountNet>();
            return lst;
        }

        public static List<PTR> GetPTRView(Settings sett)
        {
            List<PTR> source = Db.SetSpCommand(Constants.SP_GET_VIEW_PRICES_PTR,
                Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
                Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
                Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
                Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<PTR>();
            return source;
        }
        public static List<PTR> GetEditPTRData(List<long> ids, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWEdit(ids);

            List<PTR> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 2))
                    .ExecuteList<PTR>();
            return lst;
        }
        public static List<PTR> SaveEditedPTRData(Dictionary<long, EditModel> rows, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWSave(rows);

            List<PTR> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 1))
                    .ExecuteList<PTR>();
            return lst;
        }

        public static List<PTW> GetPTWView(Settings sett)
        {
            List<PTW> source = Db.SetSpCommand(Constants.SP_GET_VIEW_PRICES_PTW,
                Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
                Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
                Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
                Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<PTW>();
            return source;
        }
        public static List<PTW> GetEditPTWData(List<long> ids, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWEdit(ids);

            List<PTW> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 2))
                    .ExecuteList<PTW>();
            return lst;
        }
        public static List<PTW> SaveEditedPTWData(Dictionary<long, EditModel> rows, PtRwTypes type)
        {
            InitEditTable();
            PopulatePTRWSave(rows);

            List<PTW> lst =
                Db.SetSpCommand(Constants.SP_GET_EDIT_VIEW,
                Db.Parameter(Constants.PAR_PTRW_TYPE_ID, type),
                Db.Parameter(Constants.PAR_MODE, 1))
                    .ExecuteList<PTW>();
            return lst;
        }

        private static void PopulatePTRWSave(Dictionary<long, EditModel> rows)
        {
            StringBuilder sb = new StringBuilder(Constants.INSERT_INTO_PTRW_TABLE_SAVE);
            int counter = 0;
            foreach (EditModel row in rows.Values)
            {

                sb.Append(string.Format("({0}, {1}, '{2}', '{3}', {4}),",
                    row.Id,
                    row.Value.ToString(CultureInfo.InvariantCulture),
                    row.StartDate.ToString(CultureInfo.InvariantCulture),
                    row.EndDate.ToString(CultureInfo.InvariantCulture),
                    row.Status == 0 ? "NULL" : row.Status.ToString()));

                counter++;
                //id , value , startdate , enddate, status
                if (counter == 500)
                {
                    sb.Remove(sb.Length - 1, 1);
                    Db.SetCommand(sb.ToString()).ExecuteNonQuery();
                    sb.Clear();
                    sb.Append(Constants.INSERT_INTO_PTRW_TABLE_EDIT);
                    counter = 0;
                }
            }

            if (counter > 0)
            {
                sb.Remove(sb.Length - 1, 1);
                Db.SetCommand(sb.ToString()).ExecuteNonQuery();
            }
        }
        private static void PopulatePTRWEdit(List<long> ids)
        {
            StringBuilder sb = new StringBuilder(Constants.INSERT_INTO_PTRW_TABLE_EDIT);
            int counter = 0;
            foreach (long id in ids)
            {
                sb.Append(string.Format("({0}),", id));
                counter++;

                if (counter == 500)
                {
                    sb.Remove(sb.Length - 1, 1);
                    Db.SetCommand(sb.ToString()).ExecuteNonQuery();
                    sb.Clear();
                    sb.Append(Constants.INSERT_INTO_PTRW_TABLE_EDIT);
                    counter = 0;
                }
            }

            if (counter > 0)
            {
                sb.Remove(sb.Length - 1, 1);
                Db.SetCommand(sb.ToString()).ExecuteNonQuery();
            }
        }
        public static void PopulateSettingsTables(Settings settings)
        {
            Action<string, List<string>> write = (script, ids) =>
             {
                 string scriptTemplate = script;
                 int counter = 0;
                 foreach (string id in ids)
                 {
                     script += string.Format("({0}),", id);
                     counter++;

                     if (counter == 500)
                     {
                         script = script.Remove(script.Length - 1, 1);
                         Db.SetCommand(script).ExecuteNonQuery();
                         script = scriptTemplate;
                         counter = 0;
                     }
                 }

                 if (counter > 0)
                 {
                     script = script.Remove(script.Length - 1, 1);
                     Db.SetCommand(script).ExecuteNonQuery();
                 }
             };

            if (settings.DistributorsId.Count > 0)
            {
                write(Constants.INSERT_DISTRIBUTOR_TEMPLATE, settings.DistributorsId);
            }
            if (settings.CustomersId.Count > 0)
            {
                write(Constants.INSERT_CUSTOMER_TEMPLATE, settings.CustomersId);
            }
            if (settings.NetworksId.Count > 0)
            {
                write(Constants.INSERT_NETWORK_TEMPLATE, settings.NetworksId);
            }
            if (settings.ProductsId.Count > 0)
            {
                write(Constants.INSERT_PRODUCT_TEMPLATE, settings.ProductsId);
            }
        }
        private static void SavePricesToDB(string tableName, IPriceType type)
        {
            switch (type.Type)
            {
                case PtRwTypes.PTRPrices:
                    Db.SetSpCommand(Constants.SP_IMPORT_PTR_PRICES,
                        Db.Parameter(Constants.PAR_TABLE_NAME, tableName)).ExecuteNonQuery();
                    break;
                case PtRwTypes.PTWPrices:
                    Db.SetSpCommand(Constants.SP_IMPORT_PTW_PRICES,
                        Db.Parameter(Constants.PAR_TABLE_NAME, tableName)).ExecuteNonQuery();
                    break;
                case PtRwTypes.RegularDiscounts:
                    Db.SetSpCommand(Constants.SP_IMPORT_REGULAR_DISCOUNTS,
                        Db.Parameter(Constants.PAR_TABLE_NAME, tableName)).ExecuteNonQuery();
                    break;
                case PtRwTypes.RegularDiscountsNet:
                    Db.SetSpCommand(Constants.SP_IMPORT_REGULAR_DISCOUNTS_NET,
                        Db.Parameter(Constants.PAR_TABLE_NAME, tableName)).ExecuteNonQuery();
                    break;
                case PtRwTypes.PromoDiscounts:
                    Db.SetSpCommand(Constants.SP_IMPORT_PROMO_DISCOUNTS,
                        Db.Parameter(Constants.PAR_TABLE_NAME, tableName)).ExecuteNonQuery();
                    break;
                case PtRwTypes.PromoDiscountsNet:
                    Db.SetSpCommand(Constants.SP_IMPORT_PROMO_DISCOUNTS_NET,
                        Db.Parameter(Constants.PAR_TABLE_NAME, tableName)).ExecuteNonQuery();
                    break;
            }
        }
        private static DataTable GetIncorrectPrices(string tableName, DataTable data)
        {
            DataTable incorectData = null;

            if (data is ICloneErrorTable)
                incorectData = ((ICloneErrorTable)data).CreaErrorableTable();

            if (incorectData == null)
                return new DataTable();

            List<string> colNames =
             incorectData.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();
            string columns = String.Empty;
            foreach (var colN in colNames)
            {
                columns += string.Concat(colN, ", ");
            }
            columns = columns.Remove(columns.Length - 2);
            DataTable res =
                Db.SetSpCommand(Constants.SP_GET_INVALID_IMPORT_DATA, Db.Parameter(Constants.PAR_TABLE_NAME, tableName),
                    Db.Parameter(Constants.PAR_COLUMNS, columns)).ExecuteDataTable();
            return res;
        }
        public static void UploadTemplateFile(byte[] file, PtRwTypes type)
        {
            Db.SetCommand("UPDATE typeimportprices SET fileTemplate = (@file) WHERE id = (@type)",
                Db.Parameter("@file", file),
                Db.Parameter("@type", type)).ExecuteScalar();
        }
        public static byte[] DownloadTemplateFile(PtRwTypes type)
        {
            byte[] file =
            Db.SetCommand("SELECT fileTemplate FROM typeimportprices WHERE id = (@type)",
                Db.Parameter("@type", type)).ExecuteScalar<byte[]>();
            return file;
        }
        #endregion

        /* public static List<PromoDiscount> GetPromo(Settings sett)
       {
           List<PromoDiscount> lst = Db.SetSpCommand(Constants.SP_GET_VIEW_PROMO_DISCOUNT,
              Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
              Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
              Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
              Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<PromoDiscount>();

           List<PromoDiscount> lst2 = Db.SetSpCommand(Constants.SP_GET_VIEW_PROMO_DISCOUNT,
              Db.Parameter(Constants.PAR_COUNTRY, sett.CountryId),
              Db.Parameter(Constants.PAR_DATE_FROM, sett.DateFrom),
              Db.Parameter(Constants.PAR_DATE_TO, sett.DateTo),
              Db.Parameter(Constants.PAR_PRICE_STATUS, sett.PriceStatus)).ExecuteList<PromoDiscount>();

           while (lst.Count < 1000)
           {
               foreach (var promo in lst2)
               {
                   lst.Add(new PromoDiscount
                   {
                       Id = promo.Id,
                       Cust_Id = promo.Cust_Id,
                       Cust_NAME = promo.Cust_NAME,
                       ProductCode = promo.ProductCode,
                       ProductName = promo.ProductName,
                       PercentD = promo.PercentD,
                       StartDate = promo.StartDate,
                       EndDate = promo.EndDate,
                       NomerPS = promo.NomerPS,
                       NamePS = promo.NamePS,
                       status = promo.status,
                       login = promo.login,
                       dlm = promo.dlm,
                       Country_name = promo.Country_name
                   });
               }
           }
           return lst;
       }*/
    }
}

