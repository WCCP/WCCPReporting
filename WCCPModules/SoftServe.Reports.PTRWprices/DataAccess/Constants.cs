﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.Reports.PTRWprices.DataAccess
{
    public class Constants
    {
        //Procedures (import from excel)
        public const string SP_GET_SCRIPT = "dbo.spDW_ScriptGetByModule";
        public const string SP_GET_PRICES_TYPES = "dbo.spDW_ScriptGetByModule";
        public const string SP_GET_TEMP_TABLE = "dbo.spgetGlobalTable";
        public const string SP_IMPORT_PTR_PRICES = "dbo.spPTRPricesImport";
        public const string SP_IMPORT_PTW_PRICES = "dbo.spPTWPricesImport";
        public const string SP_IMPORT_REGULAR_DISCOUNTS = "dbo.spRegularDiscountsImport";
        public const string SP_IMPORT_REGULAR_DISCOUNTS_NET = "dbo.spRegularDiscountsNetImport";
        public const string SP_IMPORT_PROMO_DISCOUNTS = "dbo.spPromoDiscountsImport";
        public const string SP_IMPORT_PROMO_DISCOUNTS_NET = "dbo.spPromoDiscountsNetImport";
        public const string SP_GET_INVALID_IMPORT_DATA = "sp_GetPTRWInvalidImporData";


        //Settings form
        public const string SP_GET_ACCESS_LEVEL = "dbo.spPTRWGetAcces";
        public const string SP_GET_TREE = "spDW_PTRW_ProductsGetTree";
        public const string SP_GET_REGIONS = "spDW_PTRW_RegionsGetList";
        public const string SP_GET_DISTRIBUTORS = "spDW_PTRW_Distributors";
        public const string SP_GET_CUSTOMERS = "spDW_PTRW_Customers";
        public const string SP_GET_NETWORKS = "spDW_PTRW_Networks";

        public const string INSERT_CUSTOMER_TEMPLATE = @"INSERT INTO #customers ( cust_id ) VALUES";
        public const string INSERT_DISTRIBUTOR_TEMPLATE = @"INSERT INTO #distributors ( distr_id ) VALUES";
        public const string INSERT_NETWORK_TEMPLATE = @"INSERT INTO #networks ( network_id ) VALUES";
        public const string INSERT_PRODUCT_TEMPLATE = @"INSERT INTO #products ( product_id ) VALUES";
        public const string INSERT_INTO_PTRW_TABLE_EDIT = @"INSERT INTO #PTRWPrices ( id ) VALUES";
        public const string INSERT_INTO_PTRW_TABLE_SAVE = @"INSERT INTO #PTRWPrices ( id , value , startdate , enddate, status ) VALUES";

        //Reports
        public const string SP_GET_VIEW_PRICES_PTR = "spPTRPricesView";
        public const string SP_GET_VIEW_PRICES_PTW = "spPTWPricesView";
        public const string SP_GET_VIEW_REGULAR_DISCOUNT = "spRegularDiscountsView";
        public const string SP_GET_VIEW_REGULAR_DISCOUNT_NET = "spRegularDiscountsNetView";
        public const string SP_GET_VIEW_PROMO_DISCOUNT = "spPromoDiscountsView";
        public const string SP_GET_VIEW_PROMO_DISCOUNT_NET = "spPromoDiscountsNetView";

        // Edit Mode
        public const string SP_GET_EDIT_VIEW = "spPTRWPricesChange";

        //Parameters
        public const string PAR_MODULE_NAME = "@ModuleName";
        public const string PAR_TYPE_FILE = "@typeFile";
        public const string PAR_TABLE_NAME = "@tableName";
        public const string PAR_TABLE_NAME1 = "@tableNAme";
        public const string PAR_PRICE_TABLE = "@priceDataTable";
        public const string PAR_COLUMNS = "@columns";
        public const string PAR_COUNTRY  = "@country_id";
        public const string PAR_PRICE_STATUS = "@onlyActive";
        public const string PAR_DATE_FROM = "@startdate";
        public const string PAR_DATE_TO = "@enddate";
        public const string PAR_MODE = "@mode";
        public const string PAR_PTRW_TYPE_ID ="@id_file";
        //Const str
        public const string CONST_MODULE_NAME = "PTRW";

        //Scripts
        public const string SCR_GET_PRICES_TYPES = @"SELECT id, caption FROM dbo.typeimportprices;";
        public const string SCR_CREATE_TEMP_TABLES = @"IF OBJECT_ID('tempdb..#distributors') IS NOT NULL
                                                     DROP TABLE #distributors;
                                                     CREATE TABLE #distributors ( distr_id INT );
                                                     IF OBJECT_ID('tempdb..#customers') IS NOT NULL
                                                         DROP TABLE #customers;
                                                     CREATE TABLE #customers ( cust_id INT );
                                                     IF OBJECT_ID('tempdb..#networks') IS NOT NULL
                                                         DROP TABLE #networks;
                                                     CREATE TABLE #networks ( network_id INT );
                                                     IF OBJECT_ID('tempdb..#products') IS NOT NULL
                                                         DROP TABLE #products;
                                                     CREATE TABLE #products ( product_id INT );";

        public const string SCR_CREATE_TEMP_TABLES_EDIT = @"IF OBJECT_ID('tempdb..#PTRWPrices') IS NOT NULL
                                                     DROP TABLE #PTRWPrices; CREATE TABLE  #PTRWPrices (id bigint, 
                                                     [ErrorMessage] varchar (max) COLLATE DATABASE_DEFAULT ,
                                                     value numeric(9,2),startdate datetime ,enddate datetime,
                                                     country_id int, status int )";
        
    }    
}
