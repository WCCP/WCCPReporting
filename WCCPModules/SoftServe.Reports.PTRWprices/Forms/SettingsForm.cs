﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SoftServe.Reports.PTRWprices.DataAccess;
using SoftServe.Reports.PTRWprices.Utils;
using SoftServe.Reports.PTRWprices.Models;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.PTRWprices
{
    public partial class SettingsForm : XtraForm
    {
        private bool _isLoading = false;
        public Settings Settings = null;
        private AccessLevel level;
        public bool HasError
        {
            get
            {
                return (level != AccessLevel.Reader && level != AccessLevel.Admin);
            }
        }
        public SettingsForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Loads all Setting form sources
        /// </summary>
        public void LoadDataSources()
        {
            _isLoading = true;
            level = (AccessLevel)DataProvider.GetUserAccesLevel();
            if (HasError)
            {
                XtraMessageBox.Show(ReportConstants.MSG_ACCESS_DENIED, ReportConstants.MSG_ACCESS_DENIED_CAPTION,
                    MessageBoxButtons.OK, MessageBoxIcon.Error );
                return;
            }
            LoadModes();
            LoadPriceTypesImport();
            SetDatesDefaults();
            LoadRegions();
            LoadDistributors(null);
            LoadCustomers(null, null);
            LoadNetworks();
            LoadPriceTypes();
            LoadPriceStatuses();
            LoadProductTree(null);
            SetReportModeView();
            _isLoading = false;
        }

        /// <summary>
        /// Loads souce cbxMode source
        /// </summary>
        /// 
        private void LoadModes()
        {
            cbxMode.Properties.Items.AddRange(new[] { new SimpleItem { Id = 1, Name = "Просмотр цены" },
                new SimpleItem { Id = 2, Name = "Загрузка цен/ скидок" } });
            cbxMode.EditValue = cbxMode.Properties.Items[0]; // default value
        }
        /// <summary>
        /// Sets defaults
        /// </summary>
        private void SetDatesDefaults()
        {
            dateEditFrom.DateTime = DateTime.Now;
            dateEditTo.DateTime = DateTime.Now.AddDays(90);
            dateEditFrom.Properties.MinValue = DateTime.Now.AddDays(-90);
            dateEditFrom.Properties.MaxValue = DateTime.Now.AddDays(90);
            dateEditTo.Properties.MinValue = DateTime.Now.AddDays(-90);
            dateEditTo.Properties.MaxValue = DateTime.Now.AddDays(90);

            if (level == AccessLevel.Reader)
            {
                cbxMode.Enabled = false;
            }
        }

        /// <summary>
        /// Loads regions lookup source
        /// </summary>
        private void LoadRegions()
        {
            DataView view = DataRepository.Regions.DefaultView;
            view.Sort = "Country_Id, Region_name ASC";
            gridLookUpRegions.LoadDataSource(view.ToTable());
            gridLookUpRegions.SelectAll();
        }

        /// <summary>
        /// Loads Distributor lookup source
        /// </summary>
        /// <param name="regionFilter"> filter by region ID</param>
        private void LoadDistributors(string regionFilter)
        {
            if (_isLoading)
            {
                DataRepository.DistributorsDistinct.DefaultView.RowFilter = string.Empty;
                gridLookUpDistr.LoadDataSource(DataRepository.DistributorsDistinct);
            }
            else
            {
                string rFilt = string.IsNullOrEmpty(regionFilter) ? "-999" : regionFilter;
                gridLookUpDistr.ClearSelection();

                DataTable source = DataRepository.Distributors.Copy();
                DataView lDataView = source.DefaultView;
                lDataView.RowFilter = string.Format("region_id in ({0}) or region_id is null", rFilt);

                List<int> ids = lDataView.Cast<DataRowView>().Select(r => r.Row.Field<int>("Distr_id")).Distinct().ToList();
                string strIds = string.Empty;
                ids.ForEach(id => strIds+=string.Concat(id, ","));
                strIds = strIds.Remove(strIds.Length - 1, 1);

                DataView finalSoure = DataRepository.DistributorsDistinct.DefaultView;
                finalSoure.RowFilter = string.Format("Distr_id in ({0})", strIds);
                finalSoure.Sort = "Distr_id ASC";

                gridLookUpDistr.Properties.DataSource = finalSoure.ToTable();                
            }
            gridLookUpDistr.SelectAll();
        }

        /// <summary>
        /// Loads customers lookup source
        /// </summary>
        /// <param name="distrFilter"> filter by region ID</param>
        private void LoadCustomers(string distrFilter, string regionFilter)
        {
            if (_isLoading)
            {
                DataRepository.Customers.DefaultView.RowFilter = string.Empty;
                gridLookUpCust.LoadDataSource(DataRepository.Customers);
            }
            else
            {
                gridLookUpCust.LoadDataSource(DataRepository.Customers);
                string dFiltr = string.IsNullOrEmpty(distrFilter) ? "-999" : distrFilter;
                string rFiltr = string.IsNullOrEmpty(regionFilter) ? "-999" : regionFilter;
                gridLookUpCust.ClearSelection();
                DataTable source = DataRepository.Customers;
                DataView lDataView = source.DefaultView;
                if (dFiltr == "-1" && rFiltr != "-999")
                {
                    lDataView.RowFilter = string.Format("region_id in ({0}) or Distr_id is null",  rFiltr);
                }
                else
                {
                    lDataView.RowFilter = string.Format("(Distr_id in ({0}) and region_id in ({1})) or Distr_id is null", dFiltr, rFiltr);
                }
                lDataView.Sort = "cust_id ASC";
                gridLookUpCust.Properties.DataSource = lDataView.ToTable();

            }
            gridLookUpCust.SelectAll();
        }

        /// <summary>
        /// Loads networks lookup source
        /// </summary>        
        private void LoadNetworks()
        {
            gridLookUpNetwork.LoadDataSource(DataProvider.GetNetworks());
            gridLookUpNetwork.SelectAll();
        }

        /// <summary>
        /// Loads prices types lookup souce
        /// </summary>
        private void LoadPriceTypes()
        {
            gridLookUpPriceTypes.LoadDataSource(DataRepository.PricesTypes);
            gridLookUpPriceTypes.SelectAll();
        }

        /// <summary>
        /// Loads prices statuses lookup souce
        /// </summary>
        private void LoadPriceStatuses()
        {
            cbxPriceStatus.Properties.Items.AddRange(new[] { new SimpleItem { Id = 2, Name = "Активные цены/скидки" },
                new SimpleItem { Id = 0, Name = "Все цены" } });
            cbxPriceStatus.EditValue = cbxPriceStatus.Properties.Items[0]; // default value
        }
        
        /// <summary>
        /// Loads products treeList source  
        /// </summary>
        private void LoadProductTree(string regionFilter)
        {            
            treeProducts.BeginUpdate();
            var dataSource = DataRepository.SKUTree;
            string countryId = GetCountryByRegions(regionFilter);
            DataView view = dataSource.DefaultView;
            view.RowFilter = string.IsNullOrEmpty(countryId) ? "1=1" : string.Format("country_id in (0 ,{0})", countryId);
            treeProducts.DataSource = view.ToTable();
            treeProducts.ExpandAll();
            treeProducts.CheckAll();
            treeProducts.CollapseAll();
            treeProducts.EndUpdate();          
        }

        /// <summary>
        /// Loads prices types lookup (import data from Excel)souce
        /// </summary>
        private void LoadPriceTypesImport()
        {            
            lkpPricesTypes.Properties.DataSource = DataProvider.GetPricesTypes();
        }

        /// <summary>
        /// Sets form defaults for Excel data uploading mode view
        /// </summary>
        private void SetUploadingModeView()
        {
            // new size and controls visibility
            Size = new Size(348, 234);
            groupTimePeriod.Location = new Point(groupUploading.Location.X, groupUploading.Location.Y);
            groupUploading.Enabled = true;
            groupUploading.Visible = true;
            // hide 
            groupSKU.Visible = false;
            groupSKU.Enabled = false;
            groupTimePeriod.Visible = false;
            groupTimePeriod.Enabled = false;
            groupReporting.Visible = false;
            groupReporting.Enabled = false;

            this.CancelButton = this.btnCancelUpload;
            this.Refresh();
        }

        /// <summary>
        /// Sets form defaults for report mode view
        /// </summary>
        private void SetReportModeView()
        {
            // new size and controls visibility
            Size = new Size(734, 452);
            groupTimePeriod.Location = new Point(groupUploading.Location.X, groupUploading.Location.Y);
            groupReporting.Location = new Point(groupTimePeriod.Location.X, groupTimePeriod.Location.Y + 45);
            groupSKU.Visible = true;
            groupSKU.Enabled = true;
            groupTimePeriod.Visible = true;
            groupTimePeriod.Enabled = true;
            groupReporting.Visible = true;
            groupReporting.Enabled = true;
            // hide 
            groupUploading.Enabled = false;
            groupUploading.Visible = false;

            this.CancelButton = btnCancelReporting;
            this.Refresh();
        }
        private List<string> GetCheckedProducts()
        {
            var nodes = treeProducts.GetAllCheckedNodes().Where(n => !n.HasChildren).ToList();
            DataTable table = treeProducts.DataSource as DataTable;
            var source = nodes.Select(n => table.Rows[n.Id]).Where(row => row.Field<int>("Level") == 5); // Level = 5 -- product
            var ids = source.Cast<DataRow>().Select(r => r.Field<int>("DATA_ID").ToString()).Distinct().ToList();
            return ids;
        }
      
        /// <summary>
        /// Returns country ID. If there are RU and UA then return null.
        /// </summary>
        /// <param name="regionIds"> Filter by region Ids</param>
        /// <returns></returns>
        private  string GetCountryByRegions(string regionIds)
        {
            DataView view = DataRepository.Regions.DefaultView;
            view.RowFilter = string.IsNullOrEmpty(regionIds) ? "1 = 1" : string.Format("Region_id in ({0})", regionIds);
            List<string> countryIds = view.ToTable().AsEnumerable().Select(r => r.Field<string>("Country_Id")).Distinct().ToList();
            string countryId = countryIds.Count == 1 ? countryIds[0] : null;
            view.RowFilter = "1 = 1";
            return string.IsNullOrEmpty(regionIds)? string.Empty : countryId;
        }
        private void LoadSettings()
        {
            Settings = new Settings();
            Settings.CountryId = GetCountryByRegions(gridLookUpRegions.SelectedIdsString());
            Settings.DateFrom = dateEditFrom.DateTime;
            Settings.DateTo = dateEditTo.DateTime;
            Settings.DistributorsId = gridLookUpDistr.SelectedIds();
            Settings.CustomersId = gridLookUpCust.SelectedIds();
            Settings.NetworksId = gridLookUpNetwork.SelectedIds();
            Settings.ProductsId = GetCheckedProducts();
            Settings.PriceTypes = gridLookUpPriceTypes.SelectedIds().Select(id => (PtRwTypes)Convert.ToInt32(id)).ToList(); ;
            Settings.PriceStatus = ((SimpleItem)cbxPriceStatus.SelectedItem).Id;
            Settings.UserLevel = level;
        }
        private void btnUploadPlans_Click(object sender, EventArgs e)
        {
            PtRwTypes t;
            try
            {
                t = (PtRwTypes)lkpPricesTypes.EditValue;
            }
            catch (Exception)
            {
                MessageBox.Show("Выберите тип загрузочных данных!");
                return;
            }

            PricesUploader lUploader = new PricesUploader(t);
            lUploader.UploadPlans(txtPath.Text);
        }
        private void btnGetPath_Click(object sender, EventArgs e)
        {
            txtPath.Text = PricesUploader.ChooseFile();
        }
        private void btnUploadTemplate_Click(object sender, EventArgs e)
        {
            PtRwTypes t;
            try
            {
                t = (PtRwTypes)lkpPricesTypes.EditValue;
            }
            catch (Exception)
            {
                MessageBox.Show("Выберите тип шаблона данных!");
                return;
            }
            TemplateUploader uploader = new TemplateUploader();
            uploader.UploadTemplateFile(t, txtPath.Text);
        }
        private void btnGetTemplate_Click(object sender, EventArgs e)
        {
            PtRwTypes t;
            try
            {
                t = (PtRwTypes)lkpPricesTypes.EditValue;
            }
            catch (Exception)
            {
                MessageBox.Show("Выберите тип шаблона данных!");
                return;
            }
            TemplateUploader uploader = new TemplateUploader();
            uploader.DownloadTemplateFile(t);
        }
        private void cbxMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_isLoading)
                return;
            SimpleItem currentItem = (SimpleItem)cbxMode.SelectedItem;

            if (currentItem.Id == 1)
                SetReportModeView();
            else
                SetUploadingModeView();
        }
        private void gridLookUpRegions_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            string regionIds = gridLookUpRegions.SelectedIdsString();
            LoadDistributors(regionIds);
            string distrIds = gridLookUpDistr.SelectedIdsString();
            LoadCustomers(distrIds, regionIds);
            LoadProductTree(regionIds);
        }
        private void gridLookUpDistr_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            string distrIds = gridLookUpDistr.SelectedIdsString();
            string regionIds = gridLookUpRegions.SelectedIdsString();

            LoadCustomers(distrIds, regionIds);            
        }
        private void okButton_Click(object sender, EventArgs e)
        {
            
            LoadSettings();
            string msg = Settings.Validate();
            if (!string.IsNullOrEmpty(msg))
            {
                XtraMessageBox.Show(msg, "Некорректные параметры:", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DialogResult = DialogResult.OK;
            DataProvider.InitSettingsTable();
            DataProvider.PopulateSettingsTables(Settings);
        }             
    }
}
