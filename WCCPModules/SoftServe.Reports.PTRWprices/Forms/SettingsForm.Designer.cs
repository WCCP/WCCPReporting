﻿namespace SoftServe.Reports.PTRWprices
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.btnGetPath = new DevExpress.XtraEditors.SimpleButton();
            this.txtPath = new DevExpress.XtraEditors.TextEdit();
            this.lkpPricesTypes = new DevExpress.XtraEditors.LookUpEdit();
            this.cbxMode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnUploadFile = new DevExpress.XtraEditors.LabelControl();
            this.btnGetTemplate = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupMain = new DevExpress.XtraEditors.GroupControl();
            this.groupReporting = new DevExpress.XtraEditors.GroupControl();
            this.cbxPriceStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gridLookUpRegions = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancelReporting = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpPriceTypes = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPriceTypes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpDistr = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColDitrName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpNetwork = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColNetwork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpCust = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColCust = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupSKU = new DevExpress.XtraEditors.GroupControl();
            this.label1 = new System.Windows.Forms.Label();
            this.treeProducts = new DevExpress.XtraTreeList.TreeList();
            this.treeListColProduct = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.groupTimePeriod = new DevExpress.XtraEditors.GroupControl();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupUploading = new DevExpress.XtraEditors.GroupControl();
            this.btnCancelUpload = new DevExpress.XtraEditors.SimpleButton();
            this.btnUploadPlans = new DevExpress.XtraEditors.SimpleButton();
            this.btnUploadTemplate = new DevExpress.XtraEditors.SimpleButton();
            this.groupCommon = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.colWaveName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaveStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPricesTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupMain)).BeginInit();
            this.groupMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupReporting)).BeginInit();
            this.groupReporting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPriceStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpRegions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpPriceTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpDistr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpNetwork.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpCust.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupSKU)).BeginInit();
            this.groupSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTimePeriod)).BeginInit();
            this.groupTimePeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupUploading)).BeginInit();
            this.groupUploading.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupCommon)).BeginInit();
            this.groupCommon.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGetPath
            // 
            this.btnGetPath.Location = new System.Drawing.Point(280, 67);
            this.btnGetPath.Name = "btnGetPath";
            this.btnGetPath.Size = new System.Drawing.Size(46, 23);
            this.btnGetPath.TabIndex = 2;
            this.btnGetPath.Text = "Обзор";
            this.btnGetPath.Click += new System.EventHandler(this.btnGetPath_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(12, 70);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(260, 20);
            this.txtPath.TabIndex = 3;
            // 
            // lkpPricesTypes
            // 
            this.lkpPricesTypes.Location = new System.Drawing.Point(106, 15);
            this.lkpPricesTypes.Name = "lkpPricesTypes";
            this.lkpPricesTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpPricesTypes.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("caption", "Name")});
            this.lkpPricesTypes.Properties.DisplayMember = "caption";
            this.lkpPricesTypes.Properties.NullText = "";
            this.lkpPricesTypes.Properties.ShowFooter = false;
            this.lkpPricesTypes.Properties.ShowHeader = false;
            this.lkpPricesTypes.Properties.ValueMember = "id";
            this.lkpPricesTypes.Size = new System.Drawing.Size(220, 20);
            this.lkpPricesTypes.TabIndex = 4;
            // 
            // cbxMode
            // 
            this.cbxMode.EditValue = "";
            this.cbxMode.Location = new System.Drawing.Point(57, 13);
            this.cbxMode.Name = "cbxMode";
            this.cbxMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxMode.Size = new System.Drawing.Size(270, 20);
            this.cbxMode.TabIndex = 5;
            this.cbxMode.SelectedIndexChanged += new System.EventHandler(this.cbxMode_SelectedIndexChanged);
            // 
            // btnUploadFile
            // 
            this.btnUploadFile.Location = new System.Drawing.Point(12, 20);
            this.btnUploadFile.Name = "btnUploadFile";
            this.btnUploadFile.Size = new System.Drawing.Size(86, 13);
            this.btnUploadFile.TabIndex = 7;
            this.btnUploadFile.Text = "Тип цен/скидок: ";
            // 
            // btnGetTemplate
            // 
            this.btnGetTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnGetTemplate.Image")));
            this.btnGetTemplate.Location = new System.Drawing.Point(12, 110);
            this.btnGetTemplate.Name = "btnGetTemplate";
            this.btnGetTemplate.Size = new System.Drawing.Size(82, 23);
            this.btnGetTemplate.TabIndex = 9;
            this.btnGetTemplate.Text = "Шаблон";
            this.btnGetTemplate.Click += new System.EventHandler(this.btnGetTemplate_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 53);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(73, 13);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "Путь к файлу:";
            // 
            // groupMain
            // 
            this.groupMain.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.groupMain.AppearanceCaption.Options.UseFont = true;
            this.groupMain.AppearanceCaption.Options.UseTextOptions = true;
            this.groupMain.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupMain.Controls.Add(this.groupReporting);
            this.groupMain.Controls.Add(this.groupSKU);
            this.groupMain.Controls.Add(this.groupTimePeriod);
            this.groupMain.Controls.Add(this.groupUploading);
            this.groupMain.Controls.Add(this.groupCommon);
            this.groupMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupMain.Location = new System.Drawing.Point(0, 0);
            this.groupMain.Name = "groupMain";
            this.groupMain.Size = new System.Drawing.Size(732, 571);
            this.groupMain.TabIndex = 11;
            this.groupMain.Text = "PTR и PTW цены";
            // 
            // groupReporting
            // 
            this.groupReporting.Controls.Add(this.cbxPriceStatus);
            this.groupReporting.Controls.Add(this.gridLookUpRegions);
            this.groupReporting.Controls.Add(this.okButton);
            this.groupReporting.Controls.Add(this.labelControl6);
            this.groupReporting.Controls.Add(this.labelControl9);
            this.groupReporting.Controls.Add(this.btnCancelReporting);
            this.groupReporting.Controls.Add(this.labelControl10);
            this.groupReporting.Controls.Add(this.labelControl8);
            this.groupReporting.Controls.Add(this.labelControl11);
            this.groupReporting.Controls.Add(this.labelControl7);
            this.groupReporting.Controls.Add(this.gridLookUpPriceTypes);
            this.groupReporting.Controls.Add(this.gridLookUpDistr);
            this.groupReporting.Controls.Add(this.gridLookUpNetwork);
            this.groupReporting.Controls.Add(this.gridLookUpCust);
            this.groupReporting.Location = new System.Drawing.Point(2, 254);
            this.groupReporting.Name = "groupReporting";
            this.groupReporting.ShowCaption = false;
            this.groupReporting.Size = new System.Drawing.Size(338, 314);
            this.groupReporting.TabIndex = 17;
            // 
            // cbxPriceStatus
            // 
            this.cbxPriceStatus.EditValue = "";
            this.cbxPriceStatus.Location = new System.Drawing.Point(100, 242);
            this.cbxPriceStatus.Name = "cbxPriceStatus";
            this.cbxPriceStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxPriceStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxPriceStatus.Size = new System.Drawing.Size(225, 20);
            this.cbxPriceStatus.TabIndex = 7;
            // 
            // gridLookUpRegions
            // 
            this.gridLookUpRegions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpRegions.Delimiter = ", ";
            this.gridLookUpRegions.EditValue = "";
            this.gridLookUpRegions.Location = new System.Drawing.Point(100, 17);
            this.gridLookUpRegions.Name = "gridLookUpRegions";
            this.gridLookUpRegions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpRegions.Properties.DisplayMember = "Region_name";
            this.gridLookUpRegions.Properties.NullText = "";
            this.gridLookUpRegions.Properties.PopupFormSize = new System.Drawing.Size(255, 0);
            this.gridLookUpRegions.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.gridLookUpRegions.Properties.ValueMember = "Region_Id";
            this.gridLookUpRegions.Properties.View = this.gridView6;
            this.gridLookUpRegions.Size = new System.Drawing.Size(225, 20);
            this.gridLookUpRegions.TabIndex = 104;
            this.gridLookUpRegions.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.gridLookUpRegions_Closed);
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColRegionName});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // gridColRegionName
            // 
            this.gridColRegionName.Caption = "Название региона";
            this.gridColRegionName.CustomizationCaption = "Название региона";
            this.gridColRegionName.FieldName = "Region_name";
            this.gridColRegionName.Name = "gridColRegionName";
            this.gridColRegionName.OptionsColumn.AllowEdit = false;
            this.gridColRegionName.OptionsColumn.AllowShowHide = false;
            this.gridColRegionName.OptionsColumn.ReadOnly = true;
            this.gridColRegionName.Visible = true;
            this.gridColRegionName.VisibleIndex = 0;
            this.gridColRegionName.Width = 180;
            // 
            // okButton
            // 
            this.okButton.Image = ((System.Drawing.Image)(resources.GetObject("okButton.Image")));
            this.okButton.Location = new System.Drawing.Point(85, 282);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(146, 23);
            this.okButton.TabIndex = 96;
            this.okButton.Text = "Сгенерировать отчет";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 24);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 13);
            this.labelControl6.TabIndex = 90;
            this.labelControl6.Text = "Регион:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(12, 159);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(29, 13);
            this.labelControl9.TabIndex = 93;
            this.labelControl9.Text = "Сеть:";
            // 
            // btnCancelReporting
            // 
            this.btnCancelReporting.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelReporting.Image")));
            this.btnCancelReporting.Location = new System.Drawing.Point(245, 282);
            this.btnCancelReporting.Name = "btnCancelReporting";
            this.btnCancelReporting.Size = new System.Drawing.Size(80, 23);
            this.btnCancelReporting.TabIndex = 97;
            this.btnCancelReporting.Text = "Отмена";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(12, 249);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(69, 13);
            this.labelControl10.TabIndex = 94;
            this.labelControl10.Text = "Статус цены:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(12, 114);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(17, 13);
            this.labelControl8.TabIndex = 92;
            this.labelControl8.Text = "ТС:";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(12, 204);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(83, 13);
            this.labelControl11.TabIndex = 95;
            this.labelControl11.Text = "Тип цен/скидок:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 69);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(71, 13);
            this.labelControl7.TabIndex = 91;
            this.labelControl7.Text = "Дистрибутор:";
            // 
            // gridLookUpPriceTypes
            // 
            this.gridLookUpPriceTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpPriceTypes.Delimiter = ", ";
            this.gridLookUpPriceTypes.EditValue = "";
            this.gridLookUpPriceTypes.Location = new System.Drawing.Point(100, 197);
            this.gridLookUpPriceTypes.Name = "gridLookUpPriceTypes";
            this.gridLookUpPriceTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpPriceTypes.Properties.DisplayMember = "caption";
            this.gridLookUpPriceTypes.Properties.NullText = "";
            this.gridLookUpPriceTypes.Properties.PopupFormSize = new System.Drawing.Size(255, 0);
            this.gridLookUpPriceTypes.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.gridLookUpPriceTypes.Properties.ValueMember = "id";
            this.gridLookUpPriceTypes.Properties.View = this.gridView4;
            this.gridLookUpPriceTypes.Size = new System.Drawing.Size(225, 20);
            this.gridLookUpPriceTypes.TabIndex = 102;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPriceTypes});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.AllowFilterEditor = false;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColPriceTypes
            // 
            this.gridColPriceTypes.Caption = "Тип цен/скидок";
            this.gridColPriceTypes.CustomizationCaption = "Тип цен/скидок";
            this.gridColPriceTypes.FieldName = "caption";
            this.gridColPriceTypes.Name = "gridColPriceTypes";
            this.gridColPriceTypes.OptionsColumn.AllowEdit = false;
            this.gridColPriceTypes.OptionsColumn.AllowShowHide = false;
            this.gridColPriceTypes.OptionsColumn.ReadOnly = true;
            this.gridColPriceTypes.Visible = true;
            this.gridColPriceTypes.VisibleIndex = 0;
            this.gridColPriceTypes.Width = 180;
            // 
            // gridLookUpDistr
            // 
            this.gridLookUpDistr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpDistr.Delimiter = ", ";
            this.gridLookUpDistr.EditValue = "";
            this.gridLookUpDistr.Location = new System.Drawing.Point(100, 62);
            this.gridLookUpDistr.Name = "gridLookUpDistr";
            this.gridLookUpDistr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpDistr.Properties.DisplayMember = "DISTR_NAME";
            this.gridLookUpDistr.Properties.NullText = "";
            this.gridLookUpDistr.Properties.PopupFormSize = new System.Drawing.Size(255, 0);
            this.gridLookUpDistr.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.gridLookUpDistr.Properties.ValueMember = "Distr_id";
            this.gridLookUpDistr.Properties.View = this.gridView1;
            this.gridLookUpDistr.Size = new System.Drawing.Size(225, 20);
            this.gridLookUpDistr.TabIndex = 99;
            this.gridLookUpDistr.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.gridLookUpDistr_Closed);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColDitrName});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColDitrName
            // 
            this.gridColDitrName.Caption = "Название дистрибутора";
            this.gridColDitrName.CustomizationCaption = "Название дистрибутора";
            this.gridColDitrName.FieldName = "DISTR_NAME";
            this.gridColDitrName.Name = "gridColDitrName";
            this.gridColDitrName.OptionsColumn.AllowEdit = false;
            this.gridColDitrName.OptionsColumn.AllowShowHide = false;
            this.gridColDitrName.OptionsColumn.ReadOnly = true;
            this.gridColDitrName.Visible = true;
            this.gridColDitrName.VisibleIndex = 0;
            this.gridColDitrName.Width = 180;
            // 
            // gridLookUpNetwork
            // 
            this.gridLookUpNetwork.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpNetwork.Delimiter = ", ";
            this.gridLookUpNetwork.EditValue = "";
            this.gridLookUpNetwork.Location = new System.Drawing.Point(100, 152);
            this.gridLookUpNetwork.Name = "gridLookUpNetwork";
            this.gridLookUpNetwork.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpNetwork.Properties.DisplayMember = "Network_Name";
            this.gridLookUpNetwork.Properties.NullText = "";
            this.gridLookUpNetwork.Properties.PopupFormSize = new System.Drawing.Size(255, 0);
            this.gridLookUpNetwork.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.gridLookUpNetwork.Properties.ValueMember = "Network_id";
            this.gridLookUpNetwork.Properties.View = this.gridView3;
            this.gridLookUpNetwork.Size = new System.Drawing.Size(225, 20);
            this.gridLookUpNetwork.TabIndex = 101;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColNetwork});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColNetwork
            // 
            this.gridColNetwork.Caption = "Название сети";
            this.gridColNetwork.CustomizationCaption = "Название сети";
            this.gridColNetwork.FieldName = "Network_Name";
            this.gridColNetwork.Name = "gridColNetwork";
            this.gridColNetwork.OptionsColumn.AllowEdit = false;
            this.gridColNetwork.OptionsColumn.AllowShowHide = false;
            this.gridColNetwork.OptionsColumn.ReadOnly = true;
            this.gridColNetwork.Visible = true;
            this.gridColNetwork.VisibleIndex = 0;
            this.gridColNetwork.Width = 180;
            // 
            // gridLookUpCust
            // 
            this.gridLookUpCust.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpCust.Delimiter = ", ";
            this.gridLookUpCust.EditValue = "";
            this.gridLookUpCust.Location = new System.Drawing.Point(100, 107);
            this.gridLookUpCust.Name = "gridLookUpCust";
            this.gridLookUpCust.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpCust.Properties.DisplayMember = "Cust_NAME";
            this.gridLookUpCust.Properties.NullText = "";
            this.gridLookUpCust.Properties.PopupFormSize = new System.Drawing.Size(255, 0);
            this.gridLookUpCust.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.gridLookUpCust.Properties.ValueMember = "cust_id";
            this.gridLookUpCust.Properties.View = this.gridView2;
            this.gridLookUpCust.Size = new System.Drawing.Size(225, 20);
            this.gridLookUpCust.TabIndex = 100;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColCust});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColCust
            // 
            this.gridColCust.Caption = "Название ТС";
            this.gridColCust.CustomizationCaption = "Название ТС";
            this.gridColCust.FieldName = "Cust_NAME";
            this.gridColCust.Name = "gridColCust";
            this.gridColCust.OptionsColumn.AllowEdit = false;
            this.gridColCust.OptionsColumn.AllowShowHide = false;
            this.gridColCust.OptionsColumn.ReadOnly = true;
            this.gridColCust.Visible = true;
            this.gridColCust.VisibleIndex = 0;
            this.gridColCust.Width = 180;
            // 
            // groupSKU
            // 
            this.groupSKU.Controls.Add(this.label1);
            this.groupSKU.Controls.Add(this.treeProducts);
            this.groupSKU.Location = new System.Drawing.Point(342, 24);
            this.groupSKU.Name = "groupSKU";
            this.groupSKU.ShowCaption = false;
            this.groupSKU.Size = new System.Drawing.Size(384, 401);
            this.groupSKU.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Ассортимент:";
            // 
            // treeProducts
            // 
            this.treeProducts.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColProduct});
            this.treeProducts.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.treeProducts.Location = new System.Drawing.Point(2, 42);
            this.treeProducts.Name = "treeProducts";
            this.treeProducts.OptionsBehavior.AllowRecursiveNodeChecking = true;
            this.treeProducts.OptionsView.ShowCheckBoxes = true;
            this.treeProducts.OptionsView.ShowColumns = false;
            this.treeProducts.OptionsView.ShowIndicator = false;
            this.treeProducts.ParentFieldName = "PARENT_ID";
            this.treeProducts.Size = new System.Drawing.Size(380, 357);
            this.treeProducts.TabIndex = 15;
            // 
            // treeListColProduct
            // 
            this.treeListColProduct.FieldName = "DATA";
            this.treeListColProduct.MinWidth = 32;
            this.treeListColProduct.Name = "treeListColProduct";
            this.treeListColProduct.OptionsColumn.AllowEdit = false;
            this.treeListColProduct.OptionsColumn.AllowMove = false;
            this.treeListColProduct.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColProduct.OptionsColumn.ReadOnly = true;
            this.treeListColProduct.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColProduct.Visible = true;
            this.treeListColProduct.VisibleIndex = 0;
            // 
            // groupTimePeriod
            // 
            this.groupTimePeriod.Controls.Add(this.dateEditTo);
            this.groupTimePeriod.Controls.Add(this.dateEditFrom);
            this.groupTimePeriod.Controls.Add(this.labelControl5);
            this.groupTimePeriod.Controls.Add(this.labelControl4);
            this.groupTimePeriod.Location = new System.Drawing.Point(2, 209);
            this.groupTimePeriod.Name = "groupTimePeriod";
            this.groupTimePeriod.ShowCaption = false;
            this.groupTimePeriod.Size = new System.Drawing.Size(338, 43);
            this.groupTimePeriod.TabIndex = 14;
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Location = new System.Drawing.Point(216, 15);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Size = new System.Drawing.Size(110, 20);
            this.dateEditTo.TabIndex = 3;
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Location = new System.Drawing.Point(70, 15);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Size = new System.Drawing.Size(110, 20);
            this.dateEditFrom.TabIndex = 2;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(191, 20);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(16, 13);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "по:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 20);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(50, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Период с:";
            // 
            // groupUploading
            // 
            this.groupUploading.Controls.Add(this.btnCancelUpload);
            this.groupUploading.Controls.Add(this.btnUploadPlans);
            this.groupUploading.Controls.Add(this.btnUploadFile);
            this.groupUploading.Controls.Add(this.btnUploadTemplate);
            this.groupUploading.Controls.Add(this.lkpPricesTypes);
            this.groupUploading.Controls.Add(this.labelControl3);
            this.groupUploading.Controls.Add(this.txtPath);
            this.groupUploading.Controls.Add(this.btnGetPath);
            this.groupUploading.Controls.Add(this.btnGetTemplate);
            this.groupUploading.Location = new System.Drawing.Point(2, 66);
            this.groupUploading.Name = "groupUploading";
            this.groupUploading.ShowCaption = false;
            this.groupUploading.Size = new System.Drawing.Size(338, 142);
            this.groupUploading.TabIndex = 13;
            // 
            // btnCancelUpload
            // 
            this.btnCancelUpload.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelUpload.Image")));
            this.btnCancelUpload.Location = new System.Drawing.Point(247, 110);
            this.btnCancelUpload.Name = "btnCancelUpload";
            this.btnCancelUpload.Size = new System.Drawing.Size(80, 23);
            this.btnCancelUpload.TabIndex = 17;
            this.btnCancelUpload.Text = "Отмена";
            // 
            // btnUploadPlans
            // 
            this.btnUploadPlans.Image = ((System.Drawing.Image)(resources.GetObject("btnUploadPlans.Image")));
            this.btnUploadPlans.Location = new System.Drawing.Point(147, 110);
            this.btnUploadPlans.Name = "btnUploadPlans";
            this.btnUploadPlans.Size = new System.Drawing.Size(84, 23);
            this.btnUploadPlans.TabIndex = 16;
            this.btnUploadPlans.Text = "Загрузить";
            this.btnUploadPlans.Click += new System.EventHandler(this.btnUploadPlans_Click);
            // 
            // btnUploadTemplate
            // 
            this.btnUploadTemplate.Enabled = false;
            this.btnUploadTemplate.Location = new System.Drawing.Point(100, 111);
            this.btnUploadTemplate.Name = "btnUploadTemplate";
            this.btnUploadTemplate.Size = new System.Drawing.Size(24, 23);
            this.btnUploadTemplate.TabIndex = 11;
            this.btnUploadTemplate.Text = "upload file";
            this.btnUploadTemplate.Visible = false;
            this.btnUploadTemplate.Click += new System.EventHandler(this.btnUploadTemplate_Click);
            // 
            // groupCommon
            // 
            this.groupCommon.Controls.Add(this.labelControl1);
            this.groupCommon.Controls.Add(this.cbxMode);
            this.groupCommon.Location = new System.Drawing.Point(2, 24);
            this.groupCommon.Name = "groupCommon";
            this.groupCommon.ShowCaption = false;
            this.groupCommon.Size = new System.Drawing.Size(338, 40);
            this.groupCommon.TabIndex = 12;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Режим:";
            // 
            // colWaveName
            // 
            this.colWaveName.Caption = "Название Инвентаризации";
            this.colWaveName.CustomizationCaption = "Название Инвентаризации";
            this.colWaveName.FieldName = "FieldName";
            this.colWaveName.Name = "colWaveName";
            this.colWaveName.OptionsColumn.AllowEdit = false;
            this.colWaveName.OptionsColumn.AllowShowHide = false;
            this.colWaveName.OptionsColumn.ReadOnly = true;
            this.colWaveName.Width = 200;
            // 
            // colWaveStartDate
            // 
            this.colWaveStartDate.Caption = "Дата С";
            this.colWaveStartDate.CustomizationCaption = "Дата С";
            this.colWaveStartDate.FieldName = "StartDate";
            this.colWaveStartDate.Name = "colWaveStartDate";
            this.colWaveStartDate.OptionsColumn.AllowEdit = false;
            this.colWaveStartDate.OptionsColumn.AllowShowHide = false;
            this.colWaveStartDate.OptionsColumn.ReadOnly = true;
            this.colWaveStartDate.Width = 74;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 571);
            this.Controls.Add(this.groupMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры отчета";
            ((System.ComponentModel.ISupportInitialize)(this.txtPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPricesTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupMain)).EndInit();
            this.groupMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupReporting)).EndInit();
            this.groupReporting.ResumeLayout(false);
            this.groupReporting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPriceStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpRegions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpPriceTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpDistr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpNetwork.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpCust.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupSKU)).EndInit();
            this.groupSKU.ResumeLayout(false);
            this.groupSKU.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTimePeriod)).EndInit();
            this.groupTimePeriod.ResumeLayout(false);
            this.groupTimePeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupUploading)).EndInit();
            this.groupUploading.ResumeLayout(false);
            this.groupUploading.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupCommon)).EndInit();
            this.groupCommon.ResumeLayout(false);
            this.groupCommon.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnGetPath;
        private DevExpress.XtraEditors.TextEdit txtPath;
        private DevExpress.XtraEditors.LookUpEdit lkpPricesTypes;
        private DevExpress.XtraEditors.ComboBoxEdit cbxMode;
        private DevExpress.XtraEditors.LabelControl btnUploadFile;
        private DevExpress.XtraEditors.SimpleButton btnGetTemplate;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupMain;
        private DevExpress.XtraEditors.SimpleButton btnUploadTemplate;
        private DevExpress.XtraEditors.GroupControl groupUploading;
        private DevExpress.XtraEditors.GroupControl groupCommon;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupTimePeriod;
        private DevExpress.XtraEditors.DateEdit dateEditTo;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnUploadPlans;
        private DevExpress.XtraEditors.GroupControl groupSKU;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraTreeList.TreeList treeProducts;
        private DevExpress.XtraEditors.SimpleButton btnCancelUpload;
        private DevExpress.XtraEditors.GroupControl groupReporting;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SimpleButton btnCancelReporting;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking gridLookUpPriceTypes;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPriceTypes;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking gridLookUpDistr;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColDitrName;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking gridLookUpNetwork;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNetwork;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking gridLookUpCust;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCust;
        private DevExpress.XtraGrid.Columns.GridColumn colWaveName;
        private DevExpress.XtraGrid.Columns.GridColumn colWaveStartDate;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking gridLookUpRegions;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColRegionName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColProduct;
        private DevExpress.XtraEditors.ComboBoxEdit cbxPriceStatus;
    }
}