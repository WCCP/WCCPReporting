﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using System.Reflection;
using DevExpress.XtraPrinting;

namespace SoftServe.Reports.PTRWprices
{
    public partial class FormImportErrors : Form
    {
        public FormImportErrors()
        {
            InitializeComponent();
        }        

        public void SetDataSource(DataTable source)
        {
            AdjustGrid(source);
            gridControl1.DataSource = source;
        }

        private void AdjustGrid(DataTable source)
        {
            foreach (DataColumn col in source.Columns)
            {
                GridColumn column = gridView1.Columns.FirstOrDefault(vCol => vCol.FieldName == col.ColumnName);
                if(column==null)
                    throw new NullReferenceException(string.Format("Отсутствует столбец -- {0}",col.ColumnName));
                column.Visible = true;
                column.VisibleIndex = source.Columns.IndexOf(col);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            string path = SelectFilePath("Errors", Logica.Reports.BaseReportControl.ExportToType.Xlsx);
            if (!string.IsNullOrEmpty(path))
                gridControl1.ExportToXlsx(path);
        }

        private string SelectFilePath(string reportCaption, Logica.Reports.BaseReportControl.ExportToType exportType)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = @"Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = reportCaption;
            sfd.Filter = String.Format("(*.{0})|*.{0}", exportType.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }

            return res;
        }
    }
}
