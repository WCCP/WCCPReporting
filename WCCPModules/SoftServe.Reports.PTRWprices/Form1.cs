﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;

namespace SoftServe.Reports.PTRWprices
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public GridControl Grid {
            get { return gridControl1; }
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        public void SetDataSource(DataTable source)
        {
            AdjustGrid(source);
            gridControl1.DataSource = source;
        }

        private void AdjustGrid(DataTable source)
        {
            foreach (DataColumn col in source.Columns)
            {
                GridColumn column = gridView1.Columns.FirstOrDefault(vCol => vCol.FieldName == col.ColumnName);
                if(column==null)
                    throw new NullReferenceException(string.Format("Треба додати колонку -- {0}",col.ColumnName));
                column.Visible = true;
                column.VisibleIndex = source.Columns.IndexOf(col);
            }
        }

    }
}
