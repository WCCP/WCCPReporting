﻿using System;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using CreateConnection;
using System.Windows.Forms;

namespace Logica.Reports.wccpM1M2OnTrade.DataAccess {
    internal static class DataAccessProvider {
        private static string distributorName = "";

        public static DataTable AllM2 {
            get { return GetTable(SqlConstants.GET_ALL_M2, SqlConstants.GET_ALL_M2_PARAM_1, 100); }
        }

        public static DataTable AllM1 {
            get {
                DataTable lDataTable = null;

                DataAccessLayer.OpenConnection();
                try {
                    DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpGetMerch, new[] {
                            new SqlParameter(SqlConstants.ParamSupervisorId, DBNull.Value),
                            new SqlParameter(SqlConstants.ParamEntityType, DBNull.Value)
                        });

                    if (null != lDataSet && lDataSet.Tables.Count > 0)
                        lDataTable = lDataSet.Tables[0];
                }
                finally {
                    DataAccessLayer.CloseConnection();
                }

                return lDataTable;
            }
        }

        public static string DistributorName {
            get {
                if (string.IsNullOrEmpty(distributorName)) {
                    DataAccessLayer.OpenConnection();
                    DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GET_DISTRIBUTOR_NAME);
                    if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                        distributorName = (string) ds.Tables[0].Rows[0][SqlConstants.GET_DISTRIBUTOR_NAME_FLD_NAME];
                    }
                    DataAccessLayer.CloseConnection();
                }
                return distributorName;
            }
        }

        public static DataTable GetMerchesBySupervisor(object supervisorId) {
            DataTable res = null;

            DataAccessLayer.OpenConnection();

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpGetMerch,
                new[] {
                    new SqlParameter(SqlConstants.ParamSupervisorId, supervisorId),
                    new SqlParameter(SqlConstants.ParamEntityType, 2)
                });

            if (null != ds && ds.Tables.Count > 0) {
                res = ds.Tables[0];
            }

            DataAccessLayer.CloseConnection();

            return res;
        }

        public static DataTable GetWorkingDays(DateTime date) {
            return GetTable(SqlConstants.GET_WORKING_DAYS, SqlConstants.GET_WORKING_DAYS_PARAM_1, date);
        }

        public static DataTable GetUpdateDAta(DateTime date) {
            return GetTable(SqlConstants.SP_GET_UPDATE_TIME, SqlConstants.SP_GET_UPDATE_TIME_PARAM_1, date);
        }

        public static void RecalculateData(DateTime date, int monthCount = 1) {
            try {
                //DataAccessLayer.ExecuteStoredProcedure("sp_DW_Visits_With_Invoices_Update", new SqlParameter("@isFullUpdate", (byte) 1));

                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpRecalculateData, 3600,
                    new[] {
                        new SqlParameter(SqlConstants.SpRecalculateDataParam1, date),
                        new SqlParameter(SqlConstants.SpRecalculateDataParam2, monthCount)
                    });

            } catch (Exception lException) {
		        if (lException.InnerException != null && lException.InnerException is SqlException)
		        {
		            var lSqlException = (SqlException) lException.InnerException;
                    if (lSqlException.State == 100 && lSqlException.Class == 15)
                    {
                        throw new ApplicationException(lSqlException.Message, lSqlException);
                    }
		        }
                throw;
		    }
        }


        public static void SaveDailyPlan(DateTime date, int svId) {
            ConnectionToSWDB_V35 ConToSW_DDB = new ConnectionToSWDB_V35();
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(ConToSW_DDB.Connection.ConnectionString);            
            sb.InitialCatalog = "LDB";
            DataAccessLayer.SetConnectionString(sb.ConnectionString);            
            DataAccessLayer.OpenConnection();
            try
            {                
                 DataAccessLayer.ExecuteNonQueryStoredProcedure("spDW_M1_DailyPlanRecalc", new[] {
                    new SqlParameter("ReportDate", date),
                    new SqlParameter("svID", svId),
                    new SqlParameter("ReportID", 1)});
            }
            finally
            {
                DataAccessLayer.CloseConnection();
                DataAccessLayer.SetConnectionStringToDefault();
            }

        }
        
        private static DataTable GetTable(string procName, string sqlParam, object value) {
            DataTable dt;
            try {
                DataAccessLayer.OpenConnection();
                dt = DataAccessLayer.ExecuteStoredProcedure(procName, new[] {new SqlParameter(sqlParam, value)}).Tables[0];
            } finally {
                DataAccessLayer.CloseConnection();
            }

            return dt;
        }
    }
}