﻿namespace Logica.Reports.wccpM1M2OnTrade.DataAccess
{
    static class SqlConstants {
        public const string SpRecalculateData = "dbo.spSW_ReCalculateData";
        public const string SpRecalculateDataParam1 = "RecalcDate";
        public const string SpRecalculateDataParam2 = "monthCount";

        public const string GET_ALL_M1 = "select * from tblMerchandisers";

        public const string GET_ALL_M2 = "spDW_SupervisorsGetList";
        public const string GET_ALL_M2_PARAM_1 = "ReportId";
        public const string GET_ALL_M2_FLD_SUPERVISOR_ID = "Supervisor_id";
        public const string GET_ALL_M2_FLD_SUPERVISOR_NAME = "Supervisor_name";

        public const string SpGetMerch = "spDW_M1M2_getMerch";
        public const string ParamSupervisorId = "@Supervisor_id";
        public const string ParamEntityType = "@entityType";
        public const string GET_MERCH_FLD_MERCH_ID = "Merch_id";
        public const string GET_MERCH_FLD_MERCH_NAME = "MerchName";

        public const string GET_WORKING_DAYS = "spDW_OnWorkingDays";
        public const string GET_WORKING_DAYS_PARAM_1 = "Date";
        public const string GET_WORKING_DAYS_FLD_CURRENT_DATE = "CurrentDate";
        public const string GET_WORKING_DAYS_FLD_WORK_DAYS_IN_MONTH = "WorkDaysInMonth";
        public const string GET_WORKING_DAYS_FLD_WORK_DAY = "WorkDay";
        public const string WorkDayTextFieldName = "WorkDayText";


        public const string GET_DISTRIBUTOR_NAME = "spDW_M_GetDistributorName";
        public const string GET_DISTRIBUTOR_NAME_FLD_NAME = "DISTR_NAME";

        public const string SP_GET_UPDATE_TIME = "spDW_M_OffTrade_Get_UpdateTime";
        public const string SP_GET_UPDATE_TIME_PARAM_1 = "Date";
        public const string SP_GET_UPDATE_TIME_FLD_PREF_VALUE = "PrefValue";

        public const string SP_RECALCULATE_DATA = "sp_DW_Visits_With_Invoices_Update";
        public const string SP_RECALCULATE_DATA_PARAM_1 = "isFullUpdate";
    }
}
