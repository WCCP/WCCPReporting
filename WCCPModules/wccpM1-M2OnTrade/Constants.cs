﻿using System;
using System.Collections.Generic;

namespace Logica.Reports.wccpM1M2OnTrade
{
    static class Constants
    {
        internal static readonly Guid M1ATabId = new Guid("0fb4e4f6-d55e-4352-aa5c-5fdd54150488");
        internal static readonly Guid M1BTabId = new Guid("ce6cdca1-4c3f-4d9d-8d8d-6da71d7e9642");
        internal static readonly Guid M1CTabId = new Guid("92d9e060-6b7d-40d5-97eb-ffe9b7d95871");
        internal static Guid[] M1Tabs
        {
            get { return new[] {M1ATabId, M1BTabId, M1CTabId}; }
        }

        internal static Guid[] M1TabsPlan
        {
            get { return new[] { M1ATabId, M1BTabId}; }
        }

        internal const string M1_B = "M1_B";
    }
}
