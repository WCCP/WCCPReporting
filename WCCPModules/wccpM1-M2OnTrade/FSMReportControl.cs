﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.DataAccess;
using Logica.Reports.wccpM1M2OnTrade;
using Logica.Reports.wccpM1M2OnTrade.Properties;
using Logica.Reports.wccpM1M2OnTrade.DataAccess;

namespace WccpReporting
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : BaseReportUserControl
    {
        #region Readonly & Static Fields

        public static List<wccpM1M2OnTradeSettingsForm> Forms = null;
        public static WccpUIControl ReportControl = null;

        #endregion

        #region Fields

        private wccpM1M2OnTradeSettingsForm setForm = null;

        #endregion

        #region Constructors

        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            if (getForm() != null)
            {
                setForm = getForm();
                setForm.SetParent(this);
                off_reports = true;
                btnSaveForPDA.Visibility = BarItemVisibility.Always;
                UpdateSheets(setForm.SheetParamsList);
                tabManager.SelectedTabPageIndex = 0;
                foreach (XtraTabPage page in tabManager.TabPages)
                {
                    BaseTab tab = page as BaseTab;
                    if (null != tab)
                    {
                        tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
                    }
                }
                Attach_DetachShowingEditorEvent(true);
                AttachEventHandlers(null);
                CustomSettings();
            }
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            SaveClick += WccpUIControl_SaveClick;
            MenuCloseClick += WccpUIControl_CloseClick;
            ExportClick += WccpUIControl_ExportClick;
            RefreshClick += WccpUIControl_RefreshClick;
            SaveForPDAClick += WccpUIControl_SaveForPDAClick;
            ReportControl = this;
        }

        void WccpUIControl_SaveClick(object sender, XtraTabPage selectedPage)
        {
            Attach_DetachShowingEditorEvent(true);
        }

        #endregion

        #region Instance Methods

        private void M1C_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            int i;
            if (!int.TryParse(e.Column.FieldName, out i)) return;
            BandedGridView bandedGridView = (BandedGridView)sender;
            GridControl gridControl = bandedGridView.GridControl;

            DataRow row = ((DataTable)gridControl.DataSource).Rows[bandedGridView.GetDataSourceRowIndex(e.RowHandle)];
            if (!row.Table.Columns.Contains(e.Column.FieldName) || row.IsNull(e.Column.FieldName))
            {
                e.Appearance.BackColor = Color.Gainsboro;
                e.Appearance.BackColor2 = Color.Gainsboro;
            }
        }
        public bool HasUnsavedData()
        {
            foreach (XtraTabPage tab in this.tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsNeedFitTabToOnePage()
        {
            return true;
        }

        public bool SaveUnsavedData()
        {
            bool result = true;

            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        if (!page.SaveChanges())
                        {
                            result = false;
                        }
                    }
                }
            }

            return result;
        }

        protected override void OnControlRemoved(ControlEventArgs e)
        {
            Forms.Remove(setForm);
            base.OnControlRemoved(e);
        }

        private void Attach_DetachShowingEditorEvent(bool isAttaching)
        {
            foreach (XtraTabPage page in tabManager.TabPages)
            {
                Attach_DetachShowingEditorEvent(page, isAttaching);
            }
        }

        private void Attach_DetachShowingEditorEvent(XtraTabPage page, bool isAttaching)
        {
            BaseTab tab = page as BaseTab;
            if (tab == null
                || tab.SheetSettings.TableDataType != TableType.Plan
                || tab.Text.Length <= 4
                || !tab.Text.Substring(0, 4).Equals(Constants.M1_B)
                || tab.Length < 2)
                return;

            var grid = (BandedGridView)tab.GetGrid(1).GridControl.DefaultView;

            if (grid != null)
                if (isAttaching)
                {
                    grid.ShowingEditor += grid_ShowingEditor;
                }
                else
                {
                    grid.ShowingEditor -= grid_ShowingEditor;
                }
        }

        /// <summary>
        ///   Set custom properties for M1M2 report
        /// </summary>
        private void CustomSettings()
        {
            cmbTab.Visible = true;
            lblTab.Visible = true;
            btnPrintAll.ItemClick -= btnPrintAll_ItemClick;
            btnPrintAll.ItemClick += btnPrintAll_ItemClick_M1M2;
            cmbTab.SelectedIndexChanged += SelectedIndexChanged;

            FillComboBox();
        }

        private void Export(CompositeLink cl, string fName, ExportToType type, String header)
        {
            switch (type)
            {
                case ExportToType.Rtf:
                    cl.PrintingSystem.ExportToRtf(fName);
                    break;
                case ExportToType.Xls:
                    cl.PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = header, TextExportMode = TextExportMode.Text });
                    break;
                case ExportToType.Xlsx:
                    cl.PrintingSystem.ExportToXlsx(fName);
                    break;
            }
        }

        /// <summary>
        ///   Fill combobox with data for M1M2 report
        /// </summary>
        private void FillComboBox()
        {
            cmbTab.Properties.Items.Clear();
            List<XtraTabPage> tmpLst = new List<XtraTabPage>(tabManager.TabPages);
            foreach (XtraTabPage page in tmpLst)
            {
                cmbTab.Properties.Items.Add(new ComboBoxItem { Id = page, Text = page.Text });
            }
            //set first Tab name as combobox Text.
            try
            {
                cmbTab.Text = ((ComboBoxItem)cmbTab.Properties.Items[0]).Text;
            }
            catch (IndexOutOfRangeException)
            {
            }
        }

        private wccpM1M2OnTradeSettingsForm getForm()
        {
            if (Forms != null && Forms.Count > 0)
                return Forms[Forms.Count - 1];
            return null;
        }

        #endregion

        #region Event Handling

        /// <summary>
        ///   Occurs when index is changed
        /// </summary>
        /// <param name = "sender">cmbTab</param>
        /// <param name = "e">Event arguments</param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbTab.Text.Equals(Resources.All))
                tabManager.SelectedTabPage = ((XtraTabPage)((ComboBoxItem)cmbTab.SelectedItem).Id);
        }

        private bool WccpUIControl_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage is BaseTab)
            {
                BaseTab page = selectedPage as BaseTab;
                if (page.DataModified)
                {
                    DialogResult result = XtraMessageBox.Show(Resources.SaveData, Resources.DataNotSaved, MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.OK)
                    {
                        if (page.SaveChanges())
                        {
                            XtraMessageBox.Show(Resources.DataSaved);
                        }
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        ///   Occurs after export btn wass clicked.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "selectedPage">The selected page.</param>
        /// <param name = "type">The type.</param>
        /// <param name = "fName">Name of the f.</param>
        private void WccpUIControl_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
        {
            if (type == ExportToType.Rtf || type == ExportToType.Xls || type == ExportToType.Xlsx)
            {
                CompositeLink cl = new CompositeLink(new PrintingSystem());
                if (selectedPage is BaseTab)
                {
                    cl.Links.Add(((BaseTab)selectedPage).PrepareCompositeLink());
                    cl.BreakSpace = (int)(cl.PrintingSystem.PageSettings.UsablePageSizeInPixels.Height - 1.0);
                }

                cl.CreateDocument();
                Export(cl, fName, type, selectedPage.Text);
            }
        }

        private void WccpUIControl_RefreshClick(object sender, XtraTabPage selectedpage)
        {
            AttachEventHandlers(selectedpage);

            Attach_DetachShowingEditorEvent(selectedpage, true);
        }

        private void AttachEventHandlers(XtraTabPage tab)
        {
            var tabs = tab == null ? TabControl.TabPages.ToArray() : new[] { tab };
            foreach (var tabPage in tabs)
            {
                if (setForm.ActiveTableTypes == TableType.Fact)
                {
                    var bt = tabPage as BaseTab;
                    if (bt != null)
                    {
                        if (setForm.M1Tabs[Constants.M1CTabId].Contains(bt.SheetStructure.Id) && bt.Length == 2)
                        {
                            var grid1 = bt.GetGrid(0);
                            int grid1Height = ((DataTable)grid1.GridControl.DataSource).Rows.Count * 20 + 275;
                            grid1.Height = grid1Height;
                            grid1.MinimumSize = new Size(0, grid1Height);
                            grid1.MaximumSize = new Size(0, grid1Height);
                            ((BandedGridView)grid1.GridControl.DefaultView).RowCellStyle += M1C_RowCellStyle;

                            var grid2 = bt.GetGrid(1);
                            int grid2Height = ((DataTable)grid2.GridControl.DataSource).Rows.Count * 20 + 315;
                            grid2.Height = grid2Height;
                            grid2.MinimumSize = new Size(0, grid2Height);
                            grid2.MaximumSize = new Size(0, grid2Height);
                            ((BandedGridView)grid2.GridControl.DefaultView).RowCellStyle += M1C_RowCellStyle;


                            bt.Refresh();

                        }
                    }
                }
            }
        }

        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            bool isShowSettings = true;
            if (HasUnsavedData())
            {
                DialogResult dialogResult = XtraMessageBox.Show(Resources.SaveBeforeModifySettings, Resources.DataNotSaved, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes && ReportControl.SaveUnsavedData())
                {
                    XtraMessageBox.Show(Resources.DataSaved);
                }
                if (dialogResult == DialogResult.Cancel)
                {
                    isShowSettings = false;
                }
            }

            Attach_DetachShowingEditorEvent(false);
            if (isShowSettings && setForm.ShowDialog() == DialogResult.OK)
            {
                UpdateSheets(setForm.SheetParamsList);
                FillComboBox();
                AttachEventHandlers(null);
            }
            Attach_DetachShowingEditorEvent(true);
        }

        /// <summary>
        ///   Print button click, show print dialog box for M1M2 report
        /// </summary>
        /// <param name = "sender">Sender</param>
        /// <param name = "e">Event arguments</param>
        private void btnPrintAll_ItemClick_M1M2(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CompositeLink cl = new CompositeLink(new PrintingSystem());
            foreach (object tb in tabManager.TabPages)
            {
                if (tb is IPrint)
                {
                    cl.Links.Add(((IPrint)tb).PrepareCompositeLink());
                    cl.BreakSpace = (int)(cl.PrintingSystem.PageSettings.UsablePageSizeInPixels.Height - 1.0);
                }
            }
            cl.CreateDocument();
            cl.PrintingSystem.PreviewFormEx.Show();
        }

        private void grid_ShowingEditor(object sender, CancelEventArgs e)
        {
            var view = (BandedGridView)sender;
            if (view != null)
            {
                bool bsmEnabled;
                if (!bool.TryParse(view.GetRowCellValue(view.FocusedRowHandle, "POCeBSM").ToString(), out bsmEnabled))
                    return;
                if (view.FocusedColumn.FieldName == "POCeSD")
                {
                    if (!bsmEnabled)
                    {
                        e.Cancel = true;
                    }
                }
                if (view.FocusedColumn.FieldName == "POCeBSM")
                {
                    if (bsmEnabled)
                    {
                        view.SetRowCellValue(view.FocusedRowHandle, "POCeSD", false);
                    }
                }
            }
        }

        #endregion

        #region Class Methods

        public static void AddForm(wccpM1M2OnTradeSettingsForm form)
        {
            if (Forms == null) Forms = new List<wccpM1M2OnTradeSettingsForm>();
            Forms.Add(form);
        }

        #endregion

        private void WccpUIControl_SaveForPDAClick(object sender, XtraTabPage selectedPage)
        {
            try
            {
                foreach (var tab in tabManager.TabPages)
                {
                    if (tab is BaseTab)
                    {
                        var basetab = (BaseTab)tab;
                        basetab.SaveChanges();
                    }
                }
            }
            catch (Exception lException)
            {
                ErrorManager.ShowErrorBox("WccpUIControl_SaveForPDAClick: " + lException.Message);
            }

            DateTime lDate = setForm.ReportDate;
            int lSvId = setForm.SupervisorId;
            try
            {
                DataAccessProvider.SaveDailyPlan(lDate, lSvId);
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            DevExpress.XtraEditors.XtraMessageBox.Show("Данные успешно сохранены");

        }

    }
}