﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.wccpM1M2OnTrade.DataAccess;
using Logica.Reports.wccpM1M2OnTrade.Properties;

namespace Logica.Reports.wccpM1M2OnTrade
{
    public partial class wccpM1M2OnTradeSettingsForm : XtraForm
    {
        private DataTable dtSuper;
        private DataTable dataTableDate;
        private BaseReportUserControl bruc;
        private DateTime dtStart, dtEnd;
        private bool m2Selected;

        public Dictionary<Guid, List<Guid>> M1TabsPlan { get; set; }
        public Dictionary<Guid, List<Guid>> M1Tabs { get; set; }
        public TableType ActiveTableTypes { get; set; }

        public void SetParent(BaseReportUserControl parent)
        {
            bruc = parent;
        }

        public wccpM1M2OnTradeSettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            bruc = parent;
            dtSuper = DataAccessProvider.AllM2;
            FillM2();
            dateEdit.DateTime = DateTime.Now;
            rbType.SelectedIndex = 1;
            GetUpdData();
        }

        public wccpM1M2OnTradeSettingsForm()
        {
            InitializeComponent();

            dtSuper = DataAccessProvider.AllM2;
            FillM2();
            dateEdit.DateTime = DateTime.Now;
            rbType.SelectedIndex = 1;
            GetUpdData();
        }


        /// <summary>
        /// Fills Combobox with M2 names
        /// </summary>
        private void FillM2()
        {
            cbM2.Properties.Items.Clear();
            foreach (DataRow dr in dtSuper.Rows)
            {
                cbM2.Properties.Items.Add(new ComboBoxItem
                                              {
                                                  Id = dr[SqlConstants.GET_ALL_M2_FLD_SUPERVISOR_ID],
                                                  Text = dr[SqlConstants.GET_ALL_M2_FLD_SUPERVISOR_NAME].ToString()
                                              });
            }
            AddM1(DataAccessProvider.AllM1);
        }

        public DateTime ReportDate {
            get { return dateEdit.DateTime.Date; }
        }

        public int SupervisorId {
            get { return (int) ((ComboBoxItem) cbM2.SelectedItem).Id; }
        }

        /// <summary>
        /// Create list of SheetParams with all Tabs
        /// </summary>
        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                if (rbType.SelectedIndex == 0)
                {
                    dtStart = new DateTime(dateEdit.DateTime.Year, dateEdit.DateTime.Month, 1);
                    dtEnd = dtStart.AddMonths(1).AddMilliseconds(-1);
                }
                else
                {
                    dataTableDate = DataAccessProvider.GetWorkingDays(dateEdit.DateTime);
                }

                List<SheetParamCollection> list = new List<SheetParamCollection>();

                // remove m2 tabs if there are no m2 selected
                if (!m2Selected)
                {
                    bruc.Report.Tabs = bruc.Report.Tabs.Where(x => Constants.M1Tabs.Contains(x.Id)).ToArray();
                }
                M1Tabs = Constants.M1Tabs.ToDictionary(x=>x, x=>new List<Guid>());
                M1TabsPlan = Constants.M1TabsPlan.ToDictionary(x => x, x => new List<Guid>());

                //add M1_A,M1_B tabs
                if (rbType.SelectedIndex == 0)
                {
                    foreach (CheckedListBoxItem item in cmb1.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            Dictionary<Guid, Guid> clonedTabs = bruc.CloneTabs(item.Description);
                            foreach (var m1Tab in Constants.M1TabsPlan)
                            {
                                list.Add(FillParamCol(clonedTabs[m1Tab], item.Description, item.Value));
                                M1Tabs[m1Tab].Add(clonedTabs[m1Tab]);
                            }

                            if (clonedTabs.Count != bruc.Report.Tabs.Length)
                            // if new tabs was added, delete redundant M2 tabs
                            {
                                bruc.Report.Tabs = bruc.Report.Tabs.Where(x => !clonedTabs.ContainsValue(x.Id)
                                                                               || x.Id == clonedTabs[Constants.M1ATabId]
                                                                               || x.Id == clonedTabs[Constants.M1BTabId]
                                                                               || x.Id == clonedTabs[Constants.M1CTabId]
                                    ).ToArray();
                            }
                        }
                    }
                }

                //add M1_A,M1_B tabs
                if (rbType.SelectedIndex == 1)
                {
                    foreach (CheckedListBoxItem item in cmb1.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            Dictionary<Guid, Guid> clonedTabs = bruc.CloneTabs(item.Description);
                            foreach (var m1Tab in Constants.M1Tabs)
                            {
                                list.Add(FillParamCol(clonedTabs[m1Tab], item.Description, item.Value));
                                M1Tabs[m1Tab].Add(clonedTabs[m1Tab]);
                            }

                            if (clonedTabs.Count != bruc.Report.Tabs.Length)
                            // if new tabs was added, delete redundant M2 tabs
                            {
                                bruc.Report.Tabs = bruc.Report.Tabs.Where(x => !clonedTabs.ContainsValue(x.Id)
                                                                               || x.Id == clonedTabs[Constants.M1ATabId]
                                                                               || x.Id == clonedTabs[Constants.M1BTabId]
                                                                               || x.Id == clonedTabs[Constants.M1CTabId]
                                    ).ToArray();
                            }
                        }
                    }
                }

                dataTableDate = DataAccessProvider.GetWorkingDays(dateEdit.DateTime);

                foreach (Tab tab in bruc.Report.Tabs)
                {
                    if (!tab.Header.Substring(0, 3).Equals("M1_"))
                    {
                        foreach (Table tbl in tab.Tables)
                        {
                            if (rbType.SelectedIndex == 0 && tbl.Type == TableType.Plan)
                            {
                                tab.Header = tab.Header.Substring(0, tab.Header.Substring(0, 3).Equals("M2_") ? 4 : 8);
                                if (tab.Header.Substring(0, 4).Equals("M2_Д"))
                                {
                                    tab.Header = "М2_Дневные Планы";
                                    list.Add(FillParamCol(tab.Id, "", tab.Id));
                                }
                                    list.Add(FillParamCol(tab.Id, "", tab.Id));
                                break;
                            }
                            if (rbType.SelectedIndex == 1)
                            {
                                tab.Header = tab.Header.Substring(0, tab.Header.Substring(0, 3).Equals("M2_") ? 4 : 8);
                                if (!tab.Header.Substring(0, 4).Equals("M2_Д")) 
                                    list.Add(FillParamCol(tab.Id, "", tab.Id));
                                break;
                            }
                        }
                    }
                }

                foreach (Tab tab in bruc.Report.Tabs)
                {
                    if (tab.Header.Substring(0, 4).Equals("М2_Д") && rbType.SelectedIndex == 0) tab.Order = -1;
                    
                }

                return list;
            }
        }

        private SheetParamCollection FillParamCol(Guid id, string merchName, object merchId) //CheckedListBoxItem merch)
        {
            SheetParamCollection paramCollection = new SheetParamCollection
                                                       {
                                                           TabId = id,
                                                           TableDataType =
                                                               rbType.SelectedIndex == 0
                                                                   ? TableType.Plan
                                                                   : TableType.Fact
                                                       };
            if (!string.IsNullOrEmpty(merchName))
            {
                paramCollection.MainHeader = Resources.MainHeader;
                paramCollection.Add(new SheetParam
                                        {
                                            SqlParamName = Resources.M1Sql,
                                            Value = merchId,
                                            DisplayParamName = Resources.M1,
                                            DisplayValue = merchName
                                        });
                paramCollection.Add(new SheetParam {SqlParamName = Resources.SupervisorIdSql, Value = null});
            }
            else
            {
                paramCollection.MainHeader = Resources.MainHeader_Supervisor;
                paramCollection.Add(new SheetParam
                                        {
                                            DisplayParamName = Resources.M2,
                                            SqlParamName = Resources.SupervisorIdSql,
                                            Value = ((ComboBoxItem) cbM2.SelectedItem).Id,
                                            DisplayValue = ((ComboBoxItem) cbM2.SelectedItem).Text
                                        });
            }
            paramCollection.Add(new SheetParam
                                    {
                                        DisplayParamName = Resources.Distributor,
                                        DisplayValue = DataAccessProvider.DistributorName
                                    });
            paramCollection.Add(new SheetParam
                                    {
                                        SqlParamName = Resources.DateSql,
                                        Value = dateEdit.DateTime.Date
                                        /*, DisplayParamName = Resources.Date, DisplayValue = dateEdit.DateTime.ToShortDateString()*/
                                    });
            if (rbType.SelectedIndex == 1)
            {
                paramCollection.Add(new SheetParam
                                        {
                                            SqlParamName = Resources.PeriodSql,
                                            Value = rbFactType.SelectedIndex,
                                            DisplayParamName = Resources.Period,
                                            DisplayValue =
                                                rbFactType.SelectedIndex == 0 ? Resources.Morning : Resources.Evening
                                        });
                GetDateInfo(paramCollection);
            }
            else
            {
                paramCollection.Add(new SheetParam
                                        {
                                            SqlParamName = Resources.StartDateSql,
                                            Value = dtStart,
                                            DisplayParamName = Resources.ReportMounth,
                                            DisplayValue = dtStart.ToString("MMMM yyyy")
                                        });
                paramCollection.Add(new SheetParam {SqlParamName = Resources.EndDateSql, Value = dtEnd});
            }
            return paramCollection;
        }

        private void GetDateInfo(SheetParamCollection paramCollection)
        {
            paramCollection.Add(new SheetParam
                                    {
                                        DisplayParamName = Resources.ReportDay,
                                        DisplayValue =
                                            Convert.ToDateTime(
                                                dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_CURRENT_DATE]).
                                            ToShortDateString()
                                    });
            paramCollection.Add(new SheetParam
                                    {
                                        DisplayParamName = Resources.ReportMounth,
                                        DisplayValue =
                                            Convert.ToDateTime(
                                                dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_CURRENT_DATE]).
                                            ToString("MM yyyy")
                                    });
            decimal daysInMonth = ConvertEx.ToDecimal(dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_WORK_DAYS_IN_MONTH]);
            //decimal workDay = ConvertEx.ToDecimal(dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_WORK_DAY]);
            paramCollection.Add(new SheetParam
                                    {
                                        DisplayParamName = Resources.WorkDays,
                                        DisplayValue =
                                            string.Format(Math.Truncate(daysInMonth) != daysInMonth ? "{0:N1}" : "{0:N0}", daysInMonth)
                                    });
            paramCollection.Add(new SheetParam
                                    {
                                        DisplayParamName = Resources.WorkDay,
                                        //DisplayValue = string.Format(Math.Truncate(workDay) != workDay ? "{0:N1}" : "{0:N0}", workDay)
                                        DisplayValue = dataTableDate.Rows[0][SqlConstants.WorkDayTextFieldName].ToString()
                                    });
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            bool m1Selected =
                cmb1.Properties.Items.Cast<CheckedListBoxItem>().Any(item => item.CheckState == CheckState.Checked);

            m2Selected = cbM2.SelectedItem as ComboBoxItem != null;

            if (!m1Selected && !m2Selected)
            {
                XtraMessageBox.Show(Resources.ErrorSelectM2OrM1);
                return;
            }

            if (rbType.SelectedIndex == 0 && dateEdit.DateTime < new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
            {
                XtraMessageBox.Show(Resources.Error_Date);
                return;
            }
            
            ActiveTableTypes = rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact;
            DialogResult = DialogResult.OK;
        }

        private void rbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbFactType.Enabled = (rbType.SelectedIndex == 0 ? false : true);
            lbDate.Visible = dateEdit.Visible = true;
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void cbM2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbM2.SelectedItem != null && cbM2.SelectedItem is ComboBoxItem)
            {
                AddM1(DataAccessProvider.GetMerchesBySupervisor(((ComboBoxItem) cbM2.SelectedItem).Id));
                foreach (CheckedListBoxItem comboBoxItem in cmb1.Properties.Items)
                {
                    comboBoxItem.CheckState = CheckState.Checked;
                }
            }
        }

        private void AddM1(DataTable dtMerch)
        {
            cmb1.Properties.Items.Clear();
            foreach (DataRow dr in dtMerch.Rows)
            {
                cmb1.Properties.Items.Add(dr[SqlConstants.GET_MERCH_FLD_MERCH_ID],
                                          dr[SqlConstants.GET_MERCH_FLD_MERCH_NAME].ToString(), CheckState.Unchecked,
                                          true);
            }
        }

        private void GetUpdData()
        {
            lblUpd2.Text = "";

            dataTableDate = DataAccessProvider.GetUpdateDAta(DateTime.Now);
            foreach (DataRow dr in dataTableDate.Rows)
            {
                lblUpd2.Text = Convert.ToDateTime(dr[SqlConstants.SP_GET_UPDATE_TIME_FLD_PREF_VALUE]).ToString();
                break;
            }
            dataTableDate.Dispose();
        }

        private void btn_upd_Click(object sender, EventArgs e) {
            Enabled = false;
            WaitManager.StartWait();
            try {
                DataAccessProvider.RecalculateData(dateEdit.DateTime);
            }
            finally {
                Enabled = true;
                WaitManager.StopWait();
            }
            GetUpdData();
        }
    }

    internal class ComboBoxItem
    {
        public object Id { get; set; }

        public string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}