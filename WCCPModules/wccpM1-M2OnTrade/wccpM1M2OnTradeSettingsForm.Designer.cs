﻿namespace Logica.Reports.wccpM1M2OnTrade
{
    partial class wccpM1M2OnTradeSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(wccpM1M2OnTradeSettingsForm));
          this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
          this.groupData1 = new DevExpress.XtraEditors.GroupControl();
          this.dateEdit = new DevExpress.XtraEditors.DateEdit();
          this.lbDate = new DevExpress.XtraEditors.LabelControl();
          this.lblReport = new DevExpress.XtraEditors.LabelControl();
          this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
          this.cmb1 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
          this.lblM1 = new DevExpress.XtraEditors.LabelControl();
          this.cbM2 = new DevExpress.XtraEditors.ComboBoxEdit();
          this.lblM2 = new DevExpress.XtraEditors.LabelControl();
          this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
          this.rbType = new DevExpress.XtraEditors.RadioGroup();
          this.grTime = new DevExpress.XtraEditors.GroupControl();
          this.rbFactType = new DevExpress.XtraEditors.RadioGroup();
          this.btnYes = new DevExpress.XtraEditors.SimpleButton();
          this.btnNo = new DevExpress.XtraEditors.SimpleButton();
          this.grpUpd = new DevExpress.XtraEditors.GroupControl();
          this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
          this.lblUpd = new DevExpress.XtraEditors.LabelControl();
          this.btn_upd = new DevExpress.XtraEditors.SimpleButton();
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.groupData1)).BeginInit();
          this.groupData1.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
          this.groupControl2.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.cmb1.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
          this.groupControl3.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.grTime)).BeginInit();
          this.grTime.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.rbFactType.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.grpUpd)).BeginInit();
          this.grpUpd.SuspendLayout();
          this.SuspendLayout();
          // 
          // imCollection
          // 
          this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
          this.imCollection.Images.SetKeyName(0, "help_24.png");
          this.imCollection.Images.SetKeyName(1, "check_24.png");
          this.imCollection.Images.SetKeyName(2, "close_24.png");
          // 
          // groupData1
          // 
          this.groupData1.Controls.Add(this.dateEdit);
          this.groupData1.Controls.Add(this.lbDate);
          this.groupData1.Location = new System.Drawing.Point(6, 103);
          this.groupData1.Name = "groupData1";
          this.groupData1.Size = new System.Drawing.Size(339, 50);
          this.groupData1.TabIndex = 14;
          this.groupData1.Text = "Период";
          // 
          // dateEdit
          // 
          this.dateEdit.EditValue = null;
          this.dateEdit.Location = new System.Drawing.Point(47, 24);
          this.dateEdit.Name = "dateEdit";
          this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
          this.dateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
          this.dateEdit.Size = new System.Drawing.Size(100, 20);
          this.dateEdit.TabIndex = 5;
          // 
          // lbDate
          // 
          this.lbDate.Location = new System.Drawing.Point(12, 30);
          this.lbDate.Name = "lbDate";
          this.lbDate.Size = new System.Drawing.Size(26, 13);
          this.lbDate.TabIndex = 4;
          this.lbDate.Text = "Дата";
          // 
          // lblReport
          // 
          this.lblReport.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
          this.lblReport.Appearance.Options.UseFont = true;
          this.lblReport.Location = new System.Drawing.Point(105, 7);
          this.lblReport.Name = "lblReport";
          this.lblReport.Size = new System.Drawing.Size(121, 21);
          this.lblReport.TabIndex = 15;
          this.lblReport.Text = "M1-M2 On-trade";
          // 
          // groupControl2
          // 
          this.groupControl2.Appearance.BackColor = System.Drawing.Color.White;
          this.groupControl2.Appearance.BackColor2 = System.Drawing.Color.White;
          this.groupControl2.Appearance.Options.UseBackColor = true;
          this.groupControl2.Controls.Add(this.cmb1);
          this.groupControl2.Controls.Add(this.lblM1);
          this.groupControl2.Controls.Add(this.cbM2);
          this.groupControl2.Controls.Add(this.lblM2);
          this.groupControl2.Location = new System.Drawing.Point(6, 158);
          this.groupControl2.Name = "groupControl2";
          this.groupControl2.Size = new System.Drawing.Size(339, 82);
          this.groupControl2.TabIndex = 16;
          this.groupControl2.Text = "Пользователь:";
          // 
          // cmb1
          // 
          this.cmb1.Cursor = System.Windows.Forms.Cursors.IBeam;
          this.cmb1.Location = new System.Drawing.Point(37, 56);
          this.cmb1.Name = "cmb1";
          this.cmb1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
          this.cmb1.Size = new System.Drawing.Size(299, 20);
          this.cmb1.TabIndex = 16;
          // 
          // lblM1
          // 
          this.lblM1.Location = new System.Drawing.Point(10, 60);
          this.lblM1.Name = "lblM1";
          this.lblM1.Size = new System.Drawing.Size(18, 13);
          this.lblM1.TabIndex = 15;
          this.lblM1.Text = "M1:";
          // 
          // cbM2
          // 
          this.cbM2.Location = new System.Drawing.Point(37, 28);
          this.cbM2.Name = "cbM2";
          this.cbM2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
          this.cbM2.Size = new System.Drawing.Size(299, 20);
          this.cbM2.TabIndex = 14;
          this.cbM2.SelectedIndexChanged += new System.EventHandler(this.cbM2_SelectedIndexChanged);
          // 
          // lblM2
          // 
          this.lblM2.Location = new System.Drawing.Point(10, 30);
          this.lblM2.Name = "lblM2";
          this.lblM2.Size = new System.Drawing.Size(18, 13);
          this.lblM2.TabIndex = 9;
          this.lblM2.Text = "М2:";
          // 
          // groupControl3
          // 
          this.groupControl3.Controls.Add(this.rbType);
          this.groupControl3.Location = new System.Drawing.Point(6, 245);
          this.groupControl3.Name = "groupControl3";
          this.groupControl3.Size = new System.Drawing.Size(339, 59);
          this.groupControl3.TabIndex = 17;
          this.groupControl3.Text = "Тип отчета:";
          // 
          // rbType
          // 
          this.rbType.Dock = System.Windows.Forms.DockStyle.Fill;
          this.rbType.Location = new System.Drawing.Point(2, 22);
          this.rbType.Name = "rbType";
          this.rbType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Ввод планов"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Получение фактов")});
          this.rbType.Size = new System.Drawing.Size(335, 35);
          this.rbType.TabIndex = 10;
          this.rbType.SelectedIndexChanged += new System.EventHandler(this.rbType_SelectedIndexChanged);
          // 
          // grTime
          // 
          this.grTime.Appearance.BackColor = System.Drawing.Color.White;
          this.grTime.Appearance.BackColor2 = System.Drawing.Color.White;
          this.grTime.Appearance.Options.UseBackColor = true;
          this.grTime.Controls.Add(this.rbFactType);
          this.grTime.Location = new System.Drawing.Point(6, 309);
          this.grTime.Name = "grTime";
          this.grTime.Size = new System.Drawing.Size(339, 57);
          this.grTime.TabIndex = 18;
          this.grTime.Text = "Факты на:";
          // 
          // rbFactType
          // 
          this.rbFactType.Dock = System.Windows.Forms.DockStyle.Fill;
          this.rbFactType.Enabled = false;
          this.rbFactType.Location = new System.Drawing.Point(2, 22);
          this.rbFactType.Name = "rbFactType";
          this.rbFactType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Утро"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Вечер")});
          this.rbFactType.Size = new System.Drawing.Size(335, 33);
          this.rbFactType.TabIndex = 10;
          // 
          // btnYes
          // 
          this.btnYes.ImageIndex = 1;
          this.btnYes.ImageList = this.imCollection;
          this.btnYes.Location = new System.Drawing.Point(187, 373);
          this.btnYes.Name = "btnYes";
          this.btnYes.Size = new System.Drawing.Size(75, 25);
          this.btnYes.TabIndex = 19;
          this.btnYes.Text = "&Да";
          this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
          // 
          // btnNo
          // 
          this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btnNo.ImageIndex = 2;
          this.btnNo.ImageList = this.imCollection;
          this.btnNo.Location = new System.Drawing.Point(267, 373);
          this.btnNo.Name = "btnNo";
          this.btnNo.Size = new System.Drawing.Size(75, 25);
          this.btnNo.TabIndex = 20;
          this.btnNo.Text = "Н&ет";
          this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
          // 
          // grpUpd
          // 
          this.grpUpd.Controls.Add(this.lblUpd2);
          this.grpUpd.Controls.Add(this.lblUpd);
          this.grpUpd.Controls.Add(this.btn_upd);
          this.grpUpd.Location = new System.Drawing.Point(6, 34);
          this.grpUpd.Name = "grpUpd";
          this.grpUpd.Size = new System.Drawing.Size(339, 63);
          this.grpUpd.TabIndex = 22;
          this.grpUpd.Text = "Данные";
          // 
          // lblUpd2
          // 
          this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
          this.lblUpd2.Appearance.Options.UseFont = true;
          this.lblUpd2.Location = new System.Drawing.Point(77, 36);
          this.lblUpd2.Name = "lblUpd2";
          this.lblUpd2.Size = new System.Drawing.Size(0, 13);
          this.lblUpd2.TabIndex = 8;
          // 
          // lblUpd
          // 
          this.lblUpd.Location = new System.Drawing.Point(12, 36);
          this.lblUpd.Name = "lblUpd";
          this.lblUpd.Size = new System.Drawing.Size(59, 13);
          this.lblUpd.TabIndex = 7;
          this.lblUpd.Text = "Данные на:";
          // 
          // btn_upd
          // 
          this.btn_upd.ImageIndex = 1;
          this.btn_upd.ImageList = this.imCollection;
          this.btn_upd.Location = new System.Drawing.Point(233, 25);
          this.btn_upd.Name = "btn_upd";
          this.btn_upd.Size = new System.Drawing.Size(86, 33);
          this.btn_upd.TabIndex = 6;
          this.btn_upd.Text = " Обновить ";
          this.btn_upd.Click += new System.EventHandler(this.btn_upd_Click);
          // 
          // wccpM1M2OnTradeSettingsForm
          // 
          this.ClientSize = new System.Drawing.Size(352, 408);
          this.Controls.Add(this.grpUpd);
          this.Controls.Add(this.btnNo);
          this.Controls.Add(this.btnYes);
          this.Controls.Add(this.grTime);
          this.Controls.Add(this.groupControl3);
          this.Controls.Add(this.groupControl2);
          this.Controls.Add(this.lblReport);
          this.Controls.Add(this.groupData1);
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "wccpM1M2OnTradeSettingsForm";
          this.ShowIcon = false;
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Параметры отчёта";
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.groupData1)).EndInit();
          this.groupData1.ResumeLayout(false);
          this.groupData1.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
          this.groupControl2.ResumeLayout(false);
          this.groupControl2.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.cmb1.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
          this.groupControl3.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.grTime)).EndInit();
          this.grTime.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.rbFactType.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.grpUpd)).EndInit();
          this.grpUpd.ResumeLayout(false);
          this.grpUpd.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.GroupControl groupData1;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.LabelControl lbDate;
        private DevExpress.XtraEditors.LabelControl lblReport;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cbM2;
        private DevExpress.XtraEditors.LabelControl lblM2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.RadioGroup rbType;
        private DevExpress.XtraEditors.GroupControl grTime;
        private DevExpress.XtraEditors.RadioGroup rbFactType;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.LabelControl lblM1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmb1;
        private DevExpress.XtraEditors.GroupControl grpUpd;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraEditors.LabelControl lblUpd;
        private DevExpress.XtraEditors.SimpleButton btn_upd;
  
    }
}