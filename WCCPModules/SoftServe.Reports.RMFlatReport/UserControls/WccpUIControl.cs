﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.RMFlatReport;
using DevExpress.XtraTab;

namespace WccpReporting
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : BaseReportUserControl
    {

        private SettingsForm setForm = null;

        public static List<SettingsForm> Forms = null;
        public WccpUIControl(int reportId)
            : base(reportId)   
        {
            InitializeComponent();

            MenuButtonsRendering += WccpUIControl_MenuButtonsRendering;

            if (getForm() != null)
            {
                setForm = getForm();
                setForm.SetParent(this);
                off_reports = true;
               
             
                UpdateTabs();

                foreach (XtraTabPage lPage in tabManager.TabPages)
                {
                    BaseTab tab = lPage as BaseTab;
                    if (null != tab)
                        tab.IsNeedFitToPage = IsNeedFitTabToOnePage;                    
                }
                //CustomSettings();
            }
            // Events assigning
            //this.ExportClick += new ExportMenuClickHandler(WccpUIControl_ExportClick);
            //SettingsFormClick += WccpUIControl_SettingsFormClick;
            //MenuCloseClick += WccpUIControl_CloseClick;
            //SaveClick += WccpUIControl_SaveClick;
            //            PrepareExport += WccpUIControl_PrepareExport;
            //ReportControl = this;

            SettingsFormClick += WccpUIControl_SettingsFormClick;
   
        }

        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {             
                UpdateTabs();                
            }
        }

        private void WccpUIControl_MenuButtonsRendering(object sender, MenuButtonsRenderingEventArgs eventArgs)
        {
            
            eventArgs.ShowExportAllBtn = false;
        }

        private void UpdateTabs()
        {
            //            if (setForm.IsPlanInput)
            UpdateSheets(setForm.SheetParamsList);
            foreach (XtraTabPage page in tabManager.TabPages)
            {
                BaseTab tab = page as BaseTab;
                if (null != tab)
                    tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
            }
        }
        private SettingsForm getForm()
        {
            if (Forms != null && Forms.Count > 0)
                return Forms[Forms.Count - 1];
            return null;
        }
        public bool IsNeedFitTabToOnePage()
        {
            return true;
        }
        public static void AddForm(SettingsForm form)
        {
            if (Forms == null)
                Forms = new List<SettingsForm>();
            Forms.Add(form);
        }
    }
}
