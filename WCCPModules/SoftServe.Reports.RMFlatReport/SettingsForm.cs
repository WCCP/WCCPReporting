﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Softserve.Reports.RMFlatReport;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using SoftServe.Reports.RMFlatReport.Resources;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.RMFlatReport.DataProvider;
using Logica.Reports.ConfigXmlParser.Model;
using System.IO;
using System.Globalization;

namespace SoftServe.Reports.RMFlatReport
{
    public partial class SettingsForm : Form
    {
        private BaseReportUserControl _bruc;
        private DataTable personnel;
        private readonly Dictionary<Control, bool> validatedControls = new Dictionary<Control, bool>();
        private static readonly Color HIGLIGHT_COLOR;
        private DateTime _dtEnd;
        private DateTime _dtStart;

        public SettingsForm()
        {
            InitializeComponent();
            SetDefault();          
        }

       

        internal string PersonnelIdList
        {
            get
            {
                StringBuilder personnel = new StringBuilder();

                personnelTreeList.NodesIterator.DoOperation(node =>
                {
                    int personLevel = (int)node.GetValue("LEVEL"); // ВНИМАНИЕ: Регистр имен колонок должен полностью совпадать с тем, что возвращает хранимая процедура !!!
                    int personId = (int)node.GetValue("DATA_ID");
                    if (node.Checked && (personLevel == 1))
                    {
                        if (personnel.Length > 0)
                            personnel.Append(',');
                        personnel.Append(personId);
                    }
                });

                return personnel.ToString();
                //return "231,27,334";
            }
        }      

        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                List<SheetParamCollection> list = new List<SheetParamCollection>();
                SheetParamCollection paramCollection = new SheetParamCollection
                {
                    MainHeader = "Плоский отчёт",//Resource.MainHeader_M3,
                    TabId =  SqlConstants.FlatReport,// _bruc.Report.Tabs[0].Id
                    TableDataType = TableType.Fact

                     //param.TableDataType = rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact; //TableType.Fact;
                };
                //Tab lTab = _bruc.Report.Tabs.Where(x => x.Id == SqlConstants.FlatReport);
                _dtStart = ReportDateFrom.DateTime.Date;
                _dtEnd   = ReportDateTo.DateTime.Date;

                paramCollection.Add(new SheetParam { SqlParamName = "@DateFrom", Value = _dtStart, DisplayParamName = "Дата с:", DisplayValue = _dtStart.ToString("dd.MM.yyyy") });
                paramCollection.Add(new SheetParam { SqlParamName = "@DateTo", Value = _dtEnd, DisplayParamName = "Дата по:", DisplayValue = _dtEnd.ToString("dd.MM.yyyy") });
                paramCollection.Add(new SheetParam { SqlParamName = "@PersonnelIdList", Value = PersonnelIdList, DisplayParamName = "", DisplayValue = "" });

                list.Add(paramCollection);
                return list;
               
            }
        }
        private void SetPersonnel()
        {
            personnel = DataAccessProvider.GetPersonalTree(SqlConstants.LovestLevelInReport, null);
            personnelTreeList.DataSource = personnel;
            personnelTreeList.NodesIterator.DoOperation(node => node.Checked = false);            

            if (personnelTreeList.Nodes.Count > 0)
                personnelTreeList.Nodes[0].Expanded = true;            
        }

       
        public void SetParent(BaseReportUserControl parent)
        {
            _bruc = parent;           
        }
        private void Highlight(Control ctrl, Color clr, Graphics graphics = null)
        {
            if (graphics != null)
            {
                using (Pen p = new Pen(clr, 1))
                {
                    Rectangle r = ctrl.Bounds;
                    r.Inflate(1, 1);
                    graphics.DrawRectangle(p, r);
                }

            }
            else
            {
                using (Graphics g = CreateGraphics())
                {
                    using (Pen p = new Pen(clr, 1))
                    {
                        Rectangle r = ctrl.Bounds;
                        r.Inflate(1, 1);
                        g.DrawRectangle(p, r);
                    }
                }
            }
        }

        private void HighlightControl(Control ctrl)
        {
            validatedControls[ctrl] = true;
            Highlight(ctrl, HIGLIGHT_COLOR);
        }
        
        private void UnHighlightControl(Control ctrl)
        {
            validatedControls[ctrl] = false;
            Highlight(ctrl, BackColor);
        }

        private bool ValidateTreeList(TreeList tl)
        {
            bool allNodesUnchecked = true;

            tl.NodesIterator.DoOperation(node => { if (node.Checked) allNodesUnchecked = false; });

            if (allNodesUnchecked)
                HighlightControl(tl);
            else
                UnHighlightControl(tl);

            return !allNodesUnchecked;
        }

        private void Validate(object sender, EventArgs e)
        {
            bool isValid = true;

            // Проверить на допустимость дату начала отчетного периода и дату окончания.
            if (ReportDateFrom.DateTime > ReportDateTo.DateTime)
            {
                isValid = false;
                if (ReferenceEquals(sender, ReportDateFrom))
                {
                    ReportDateFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Error;
                    HighlightControl(ReportDateFrom);
                    ReportDateFrom.ToolTip = Resource1.DateDiapazon;
                }

                if (ReferenceEquals(sender, ReportDateTo))
                {
                    ReportDateTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Error;
                    HighlightControl(ReportDateTo);
                    ReportDateTo.ToolTip = Resource1.DateDiapazon; 
                }
            }
            else
            {
                UnHighlightControl(ReportDateFrom);
                UnHighlightControl(ReportDateTo);
                ReportDateFrom.ToolTip = ReportDateTo.ToolTip = null;
            }       

            // Проверить на допустимость выбор в дереве персонала.
            if (!ValidateTreeList(personnelTreeList))
                isValid = false;
                   

            btnYes.Enabled = isValid;
        }

        private void SetDefault()
        {
            DateTime lYesterday = DateTime.Now.AddDays(-1).Date;
            ReportDateTo.DateTime = lYesterday;
            ReportDateTo.EditValueChanged += Validate;
            ReportDateFrom.DateTime = lYesterday;
            ReportDateFrom.EditValueChanged += Validate;          

            SetPersonnel();          
        }

       
        private void PropagateCheckStateDown(object sender, NodeEventArgs e)
        {
            foreach (TreeListNode node in e.Node.Nodes)
            {
                node.Checked = e.Node.Checked;
                PropagateCheckStateDown(sender, new NodeEventArgs(node));
            }
        }

        private void PropagateCheckStateUp(TreeListNode node)
        {
            CheckState checkStateToPropagate = node.CheckState;

            if (node.ParentNode != null)
            {
                if (checkStateToPropagate == CheckState.Indeterminate)
                {
                    while (node.ParentNode != null)
                    {
                        node.ParentNode.CheckState = CheckState.Indeterminate;
                        node = node.ParentNode;
                    }
                }
                else
                {
                    foreach (TreeListNode tln in node.ParentNode.Nodes)
                    {
                        if (tln.CheckState != checkStateToPropagate)
                        {
                            checkStateToPropagate = CheckState.Indeterminate;
                            break;
                        }
                    }

                    node.ParentNode.CheckState = checkStateToPropagate;
                    PropagateCheckStateUp(node.ParentNode);
                }
            }
        }

        private void personnelTreeList_AfterCheckNode(object sender, NodeEventArgs e)
        {
            // После каждого клика мышкой состояние узла циклически меняется с 'Checked' на 'Unchecked' на 'Indeterminate', поэтому надо
            // принудительно менять состояние 'Indeterminate' на 'Checked', т.к. состояние 'Indeterminate' означает, что был клик на 
            // 'Unchecked' узле.
            if (e.Node.CheckState == CheckState.Indeterminate)
                e.Node.Checked = true;

            PropagateCheckStateDown(sender, e);
            PropagateCheckStateUp(e.Node);            
            Validate(sender, EventArgs.Empty);
        }

        private void personnelTreeList_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {

        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

       
        
    }
}





