﻿using SoftServe.Reports.RMFlatReport.Resources;
using System.Windows.Forms;

namespace SoftServe.Reports.RMFlatReport
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ReportDateTo = new DevExpress.XtraEditors.DateEdit();
            this.ReportDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.personnelTreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personnelTreeList)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ReportDateTo);
            this.groupBox1.Controls.Add(this.ReportDateFrom);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(335, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Период";
            // 
            // ReportDateTo
            // 
            this.ReportDateTo.EditValue = null;
            this.ReportDateTo.Location = new System.Drawing.Point(198, 24);
            this.ReportDateTo.Name = "ReportDateTo";
            this.ReportDateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ReportDateTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ReportDateTo.Size = new System.Drawing.Size(120, 20);
            this.ReportDateTo.TabIndex = 3;
            // 
            // ReportDateFrom
            // 
            this.ReportDateFrom.EditValue = null;
            this.ReportDateFrom.Location = new System.Drawing.Point(32, 24);
            this.ReportDateFrom.Name = "ReportDateFrom";
            this.ReportDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ReportDateFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ReportDateFrom.Size = new System.Drawing.Size(120, 20);
            this.ReportDateFrom.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(177, 26);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(12, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "по";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(19, 26);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(5, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "с";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(16, 81);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(48, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Персонал";
            // 
            // personnelTreeList
            // 
            this.personnelTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.personnelTreeList.Location = new System.Drawing.Point(16, 102);
            this.personnelTreeList.Name = "personnelTreeList";
            this.personnelTreeList.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.personnelTreeList.OptionsBehavior.Editable = false;
            this.personnelTreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.personnelTreeList.OptionsPrint.PrintPageHeader = false;
            this.personnelTreeList.OptionsSelection.MultiSelect = true;
            this.personnelTreeList.OptionsView.ShowCheckBoxes = true;
            this.personnelTreeList.OptionsView.ShowColumns = false;
            this.personnelTreeList.OptionsView.ShowIndicator = false;
            this.personnelTreeList.ParentFieldName = "PARENT_ID";
            this.personnelTreeList.Size = new System.Drawing.Size(330, 323);
            this.personnelTreeList.TabIndex = 3;
            this.personnelTreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.personnelTreeList_AfterCheckNode);
            this.personnelTreeList.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.personnelTreeList_FocusedNodeChanged);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "DATA";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 91;
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnYes.Enabled = false;
            this.btnYes.Image = ((System.Drawing.Image)(resources.GetObject("btnYes.Image")));
            this.btnYes.Location = new System.Drawing.Point(90, 439);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(152, 29);
            this.btnYes.TabIndex = 5;
            this.btnYes.Text = "&Сгенерировать отчет";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(249, 440);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(98, 28);
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "&Отменить";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(362, 485);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.personnelTreeList);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(368, 513);
            this.Name = "SettingsForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры отчёта";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personnelTreeList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraTreeList.TreeList personnelTreeList;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.DateEdit ReportDateTo;
        private DevExpress.XtraEditors.DateEdit ReportDateFrom;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;



    }
}

