﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;
//using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.RMFlatReport.DataProvider;


namespace Softserve.Reports.RMFlatReport
{
    public class DataAccessProvider
    {
        public static DataTable GetPersonalTree(int Level, string User)
        {
            DataTable AllPersonal = null;            
            DataAccessLayer.OpenConnection();                      

            DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GetPersonalTree);

            if (null != lDataSet && lDataSet.Tables.Count > 0)
                AllPersonal = lDataSet.Tables[0];

            DataAccessLayer.CloseConnection();

            return AllPersonal;
        }

        public static DataTable GetReportGrid(DateTime DateStart, DateTime DateEnd, string PersonalListId)
        {
            DataTable ReportGrid = null;
            DataAccessLayer.OpenConnection();

            DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SP_GET_RMT_Report, 
                                                                      new[] {new SqlParameter(SqlConstants.Param_DateFrom,      DateStart),
                                                                             new SqlParameter(SqlConstants.Param_DateTo,        DateEnd),
                                                                             new SqlParameter(SqlConstants.Param_Personal_List, PersonalListId)
                                                                      });

            if (null != lDataSet && lDataSet.Tables.Count > 0)
                ReportGrid = lDataSet.Tables[0];

            DataAccessLayer.CloseConnection();

            return ReportGrid;
        }

      

    }
}