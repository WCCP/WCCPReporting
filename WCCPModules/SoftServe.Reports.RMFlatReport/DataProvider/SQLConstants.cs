﻿using System;

namespace SoftServe.Reports.RMFlatReport.DataProvider
{
    public class SqlConstants
    { 
        //Stored Procedures
        public const string GetPersonalTree = "spDW_RMT_GET_Personal";        
        public const string SP_GET_RMT_Report = "spDW_RMT_Report";

        //SP Params
        public const string Param_DateFrom = "@DateFrom";
        public const string Param_DateTo = "@DateTo";
        public const string Param_Personal_List = "@PersonnelIdList";


        //Guid
        internal static Guid FlatReport = new Guid("8c112c83-ed23-45b2-b4db-5ebf2a02b828");

        //Lovest level in report
        public const int LovestLevelInReport = 1;
            
    }
}