﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.Common.Enums;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.ContractMgmt {
    /// <summary>
    /// 
    /// </summary>
    internal static class DataProvider {
        
        static DataTable _addressStatusTable;

        /// <summary>
        /// Gets the contract list.
        /// </summary>
        /// <value>The contract list.</value>
        internal static DataTable ContractList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_LIST); }
        }

        /// <summary>
        /// Gets the access level.
        /// </summary>
        /// <value>The access level.</value>
        internal static Collection<AccessLevelType> AccessLevel {
            get {
                Collection<AccessLevelType> lAccessLevels = new Collection<AccessLevelType>();
                DataTable lDataTable = ExecuteStoredProcedure(Constants.SP_GET_ACCESS_LEVEL);
                if (lDataTable != null && lDataTable.Rows.Count > 0)
                    foreach (DataRow lRow in lDataTable.Rows)
                        lAccessLevels.Add((AccessLevelType) ConvertEx.ToInt(lRow[0]));
                return lAccessLevels;
            }
        }


        /// <summary>
        /// Gets the user level. (M2, 3, 4...?)
        /// </summary>
        /// <value>The user level.</value>
        internal static UserLevelType UserLevel {
            get { return (UserLevelType) ConvertEx.ToInt(ExecuteScalarStoredProcedure(Constants.SP_GET_USER_LEVEL)); }
        }

        /// <summary>
        /// Gets the client list.
        /// </summary>
        /// <value>The client list.</value>
        internal static DataTable ClientList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_LIST); }
        }

        /// <summary>
        /// Gets the contract status list.
        /// </summary>
        /// <value>The contract status list.</value>
        internal static DataTable ContractStatusList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_STATUS_LIST); }
        }

        /// <summary>
        /// Gets the channel list.
        /// </summary>
        /// <value>The channel list.</value>
        internal static DataTable ChannelList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_CHANNEL_LIST); }
        }


        /// <summary>
        /// Gets the payment schema list.
        /// </summary>
        /// <value>The payment schema list.</value>
        internal static DataTable PaymentSchemaList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_EXPENSES_PAYMENT_SCHEMA_LIST); }
        }

        /// <summary>
        /// Gets the competitor list.
        /// </summary>
        /// <value>The competitor list.</value>
        internal static DataTable CompetitorList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_COMPETITORS_LIST); }
        }

        /// <summary>
        /// Gets the type of the payment.
        /// </summary>
        /// <value>The type of the payment.</value>
        internal static DataTable PaymentTypeList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_EXPENSES_PAYMENT_TYPE_LIST); }
        }

        internal static DataTable BudgetTypeList {
            get { return ExecuteStoredProcedure(Constants.SpGetBudgetTypeList); }
        }

        internal static DataTable BudgetOwnerList {
            get { return ExecuteStoredProcedure(Constants.SpGetBudgetOwnerList); }
        }

        /// <summary>
        /// Gets the address status list.
        /// </summary>
        /// <value>The address status list.</value>
        internal static DataTable AddressStatusTable {
            get {
                if (_addressStatusTable != null)
                    return _addressStatusTable;

                _addressStatusTable = new DataTable();
                _addressStatusTable.Columns.Add("ID", typeof (int));
                _addressStatusTable.Columns.Add("Name", typeof (string));
                _addressStatusTable.Rows.Add(new object[] {(int) AddressRowStatus.Common, "Нет изменений"});
                _addressStatusTable.Rows.Add(new object[] {(int) AddressRowStatus.New, "Добавлена"});
                _addressStatusTable.Rows.Add(new object[] {(int) AddressRowStatus.Removed, "Удалена"});
                _addressStatusTable.Rows.Add(new object[] {(int) AddressRowStatus.Edited, "Изменена"});
                return _addressStatusTable;
            }
        }

        /// <summary>
        /// Gets the advert event list.
        /// </summary>
        /// <value>The advert event list.</value>
        internal static DataTable AdvertEventList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_ADV_ALL_EVENTS); }
        }

        /// <summary>
        /// Gets the advert medium list.
        /// </summary>
        /// <value>The advert medium list.</value>
        internal static DataTable AdvertMediumList {
            get { return ExecuteStoredProcedure(Constants.SP_GET_ADV_ALL_MEDIUMS); }
        }

        internal static DataTable GetAddressesList(int contractId) {
            return GetAddressesList(contractId, false);
        }

        /// <summary>
        /// Gets the addresses list.
        /// </summary>
        /// <param name="contractId">Id of Contract</param>
        /// <param name="isControlGr">if true - get control group, else adreska</param>
        /// <returns></returns>
        internal static DataTable GetAddressesList(int contractId, bool isControlGr) {
            try {
                DataTable lDataTable = ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_ADDRESSES_LIST,
                    new SqlParameter[] {
                        new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                        new SqlParameter(Constants.SP_GET_CONTRACT_ADDRESSES_LIST_PARAM2, isControlGr)
                    });

                return lDataTable;
            }
            catch (Exception lException) {
                ErrorManager.ShowErrorBox(lException.Message);
            }

            return null;
        }

        public static DataTable GetClientOutlets(int clientId, object chanel, string promoOutlets, string controlOutlets) {
            try {
                DataTable lDataTable = ExecuteStoredProcedure(Constants.SP_GET_CLIENT_OUTLETS,
                    new SqlParameter[] {
                        new SqlParameter(Constants.SP_GET_CLIENT_OUTLETS_CLIENT, clientId),
                        new SqlParameter(Constants.SP_GET_CLIENT_OUTLETS_CHANEL, chanel),
                        new SqlParameter(Constants.SP_GET_CLIENT_OUTLETS_PROMO_GR, promoOutlets),
                        new SqlParameter(Constants.SP_GET_CLIENT_OUTLETS_CONTROL_GR, controlOutlets)
                    });
                return lDataTable;
            }
            catch (Exception lException) {
                ErrorManager.ShowErrorBox(lException.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the addresses list by OLID.
        /// </summary>
        /// <param name="strIdList">The STR id list.</param>
        /// <param name="dateSignPlanned">Planned date of contract sign</param>
        /// <param name="contractTerm">Term of contract in months</param>
        /// <param name="channel">Channel of contract</param>
        /// <returns></returns>
        internal static DataTable GetAddressesListByOLIDs(string strIdList, DateTime dateSignPlanned, int contractTerm, object channel) {
            try {
                DataTable lDataTable = ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID,
                    new SqlParameter[] {
                        new SqlParameter(Constants.SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM1, strIdList),
                        new SqlParameter(Constants.SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM2,
                            dateSignPlanned == DateTime.MinValue ? (object) DBNull.Value : dateSignPlanned),
                        new SqlParameter(Constants.SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM3, contractTerm),
                        new SqlParameter(Constants.SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM4, channel)
                    });
                return lDataTable;
            }
            catch (Exception lException) {
                ErrorManager.ShowErrorBox(lException.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the details breif.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetCommonDetails(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_BRIEF_DETAILS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_DETAILS_PARAM1, contractId)
                });
        }


        /// <summary>
        /// Sets the common details.
        /// </summary>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetCommonDetails(SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_COMMON_BRIEF_DETAILS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_SET_COMMON_BRIEF_DETAILS_PARAM1, sqlXml)
                });
        }

        /// <summary>
        /// Gets the details client breif.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        internal static DataTable GetDetailsClientBreif(int clientId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_BRIEF_CLIENT_DETAILS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_CLEINT_DETAILS_PARAM1, clientId)
                });
        }

        /// <summary>
        /// Gets the details sales avg.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="competitor">if set to <c>true</c> [competitor].</param>
        /// <returns></returns>
        internal static DataTable GetDetailsSalesAvg(int contractId, string strAddressList, bool competitor) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG_PARAM1, contractId),
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG_PARAM2, contractId > 0 ? string.Empty : strAddressList),
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG_PARAM3, competitor)
                });
        }

        /// <summary>
        /// Sets the details sales avg competitors.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetDetailsSalesAvgCompetitors(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_COMMON_BRIEF_CLIENT_SALES_AVG_COMPETITOR,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_COMMON_BRIEF_CLIENT_SALES_AVG_COMPETITOR_PARAM2, sqlXml)
                });
        }

        /// <summary>
        /// Gets the details distributor list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="channelId">The channel id.</param>
        /// <returns></returns>
        internal static DataTable GetDetailsDistributorList(int contractId, string strAddressList, int channelId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_BRIEF_DISTRIBUTORS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_DISTRIBUTORS_PARAM2, contractId > 0 ? string.Empty : strAddressList),
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_DISTRIBUTORS_PARAM3, channelId)
                });
        }

        /// <summary>
        /// Gets the details POC list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetDetailsPOCList(int contractId, string strAddressList) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_BRIEF_POC_LIST,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_GET_COMMON_BRIEF_POC_LIST_PARAM2, contractId > 0 ? string.Empty : strAddressList)
                });
        }

        /// <summary>
        /// Gets the photo list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetPhotoList(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_PHOTO_LIST,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                });
        }

        /// <summary>
        /// Gets the photo.
        /// </summary>
        /// <param name="photoId">The photo id.</param>
        /// <returns></returns>
        internal static byte[] GetPhoto(int photoId) {
            object lObjFile = ExecuteScalarStoredProcedure(Constants.SP_GET_COMMON_PHOTO_BY_ID,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_GET_COMMON_PHOTO_BY_ID_PARAM1, photoId)
                });

            return ConvertEx.ToBytesArray(lObjFile);
        }

        /// <summary>
        /// Creates the new contract.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="channelType">Type of the channel.</param>
        /// <param name="dateSignPlanned">The date sign planned.</param>
        /// <param name="contractTerm">The duration of contract in months</param>
        /// <returns></returns>
        internal static int CreateNewContract(int clientId, ChannelType channelType, DateTime dateSignPlanned, int contractTerm) {
            return ConvertEx.ToInt(ExecuteScalarStoredProcedure(Constants.SP_INSERT_NEW_CONTRACT,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_INSERT_NEW_CONTRACT_PARAM1, clientId),
                    new SqlParameter(Constants.SP_INSERT_NEW_CONTRACT_PARAM2, (int) channelType),
                    new SqlParameter(Constants.SP_INSERT_NEW_CONTRACT_PARAM3, dateSignPlanned),
                    new SqlParameter(Constants.SP_INSERT_NEW_CONTRACT_PARAM4, contractTerm)
                }));
        }

        /// <summary>
        /// Gets the has access to addresses.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static int GetHasAccessToAddresses(int contractId) {
            return ConvertEx.ToInt(ExecuteScalarStoredProcedure(Constants.SP_GET_HAS_ACCESS_TO_ADDRESSES,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                }));
        }

        /// <summary>
        /// Inserts the file.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileData">The file data.</param>
        /// <returns></returns>
        internal static int InsertFile(int contractId, string fileName, byte[] fileData) {
            object lResult = ExecuteScalarStoredProcedure(Constants.SP_INSERT_COMMON_PHOTO,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_INSERT_COMMON_PHOTO_PARAM2, fileName),
                    new SqlParameter(Constants.SP_INSERT_COMMON_PHOTO_PARAM3, fileData)
                });

            return ConvertEx.ToInt(lResult);
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="docId">The doc id.</param>
        /// <returns></returns>
        internal static int DeleteFile(int docId) {
            object lResult = ExecuteScalarStoredProcedure(Constants.SP_DELETE_COMMON_PHOTO,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_DELETE_COMMON_PHOTO_PARAM1, docId)
                });

            return ConvertEx.ToInt(lResult);
        }

        /// <summary>
        /// Gets the investment details.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetInvestmentDetails(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_EXPENSES_FIELDS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                });
        }

        /// <summary>
        /// Sets the investments fields.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetInvestmentsFields(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_EXPENSES_FIELDS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_EXPENSES_FIELDS_PARAM2, sqlXml)
                });
        }

        /// <summary>
        /// Sets the cost plan.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="xmlCostList">The XML cost list.</param>
        /// <returns></returns>
        internal static DataTable SetExpensesPlan(int contractId, SqlXml xmlCostList) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_EXPENSES_PLAN,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_EXPENSES_PLAN_PARAM2, xmlCostList)
                });
        }

        /// <summary>
        /// Gets the rates table.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetIndicatorsTable(int contractId) {
            return ExecuteStoredProcedure(Constants.SP_GET_EXPENSES_INDICATORS_TABLE,
                new SqlParameter[] {new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)}
                );
        }

        /// <summary>
        /// Sets the indicators table.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetIndicatorsTable(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_EXPENSES_INDICATORS_TABLE,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_EXPENSES_INDICATORS_TABLE_PARAM2, sqlXml == null ? (object) DBNull.Value : sqlXml),
                });
        }

        /// <summary>
        /// Sets the indicators sum table.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetIndicatorsSumTable(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_EXPENSES_INDICATORS_SUM,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_EXPENSES_INDICATORS_SUM_PARAM2, sqlXml == null ? (object) DBNull.Value : sqlXml),
                });
        }

        /// <summary>
        /// Gets the competitors.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetCompetitors(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_COMPETITORS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                });
        }

        /// <summary>
        /// Ges the previous contract.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GePreviousContract(int clientId, int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_PREV_CONTRACTS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_GET_COMMON_PREV_CONTRACTS_PARAM1, clientId),
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                });
        }

        /// <summary>
        /// Sets the competitors.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="xmlCompetitorList">The XML competitor list.</param>
        /// <returns></returns>
        internal static DataTable SetCompetitors(int contractId, SqlXml xmlCompetitorList) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_COMMON_COMPETITORS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_COMMON_COMPETITORS_PARAM2, xmlCompetitorList)
                });
        }

        /// <summary>
        /// Sets the budget plan.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="xml">The XML.</param>
        /// <returns></returns>
        internal static DataTable SetBudgetPlan(int contractId, SqlXml xml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_COMMON_BUDGET_PLAN,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_COMMON_BUDGET_PLAN_PARAM2, xml)
                });
        }

        /// <summary>
        /// Gets the budget plan.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetBudgetPlan(int contractId) {
            return ExecuteStoredProcedure(Constants.SP_GET_COMMON_BUDGET_PLAN, new SqlParameter[] {
                new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId == 0 ? (object) DBNull.Value : contractId)
            });
        }

        /// <summary>
        /// Gets the indicators.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        // TODO: Obsolete - delete
        internal static DataTable GetIndicators(int contractId, char decimalDelimiter, string strAddressList) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_COMMON_INDICATORS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_GET_COMMON_INDICATORS_PARAM2, decimalDelimiter),
                    new SqlParameter(Constants.SP_GET_COMMON_INDICATORS_PARAM3, strAddressList)
                });
        }

        /// <summary>
        /// Gets the advert fields.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetAdvertDetails(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_ADV_FIELDS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                });
        }

        /// <summary>
        /// Sets the advert details.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="strIsAdvertisement">The STR is advertisement.</param>
        /// <param name="strCompanyPartner">The STR company partner.</param>
        /// <returns></returns>
        internal static DataTable SetAdvertDetails(int contractId, object strIsAdvertisement, object strCompanyPartner) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_ADV_FIELDS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_ADV_FIELDS_PARAM2, strIsAdvertisement),
                    new SqlParameter(Constants.SP_SET_ADV_FIELDS_PARAM3, strCompanyPartner)
                });
        }

        /// <summary>
        /// Gets the advert events.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetAdvertEvents(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_ADV_EVENTS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                });
        }

        /// <summary>
        /// Sets the advert events.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetAdvertEvents(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_ADV_EVENTS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_ADV_EVENTS_PARAM2, sqlXml)
                });
        }

        /// <summary>
        /// Sets the conditions by SKU.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetConditionsBySKU(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_CONDITIONS_BY_SKU,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_CONDITIONS_BY_SKU_PARAM2, sqlXml)
                });
        }

        /// <summary>
        /// Gets the advert mediums.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetAdvertMediums(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_ADV_MEDIUMS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                });
        }

        /// <summary>
        /// Sets the advert medium.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetAdvertMedium(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_ADV_MEDIUMS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_ADV_MEDIUMS_PARAM2, sqlXml)
                });
        }

        /// <summary>
        /// Gets the advert planned changes.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetAdvertPlannedChanges(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_ADV_PlANNED_CHANGES,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                });
        }

        /// <summary>
        /// Sets the advert planned changes.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetAdvertPlannedChanges(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_ADV_PLANNED_CHANGES,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_ADV_PLANNED_CHANGES_PARAM2, sqlXml)
                });
        }

        /// <summary>
        /// Gets the advert planned advert.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetAdvertPlannedAdvert(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_ADV_PLANNED_ADV,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                });
        }

        /// <summary>
        /// Sets the advert planned advert.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable SetAdvertPlannedAdvert(int contractId, SqlXml sqlXml) {
            return ExecuteStoredProcedure(
                Constants.SP_SET_ADV_PLANNED_ADV,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_ADV_PLANNED_ADV_PARAM2, sqlXml)
                });
        }

        /// <summary>
        /// Gets the next contract statuses.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        internal static DataTable GetNextContractStatuses(int contractId, ContractStatus status) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_CONTRACT_NEXT_STATUSES,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_GET_CONTRACT_NEXT_STATUSES_PARAM2, (int) status)
                });
        }

        /// <summary>
        /// Deletes the contract.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static int DeleteContract(int contractId) {
            return ConvertEx.ToInt(ExecuteScalarStoredProcedure(
                Constants.SP_DELETE_CONTRACT,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                }));
        }

        /// <summary>
        /// Gets the contract status.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static ContractStatus GetContractStatus(int contractId) {
            return (ContractStatus) ConvertEx.ToInt(ExecuteScalarStoredProcedure(
                Constants.SP_GET_CONTRACT_STATUS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                }));
        }

        /// <summary>
        /// Sets the contract status.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        internal static int SetContractStatus(int contractId, ContractStatus status) {
            return ConvertEx.ToInt(ExecuteScalarStoredProcedure(
                Constants.SP_SET_CONTRACT_STATUS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_CONTRACT_STATUS_PARAM2, (int) status)
                }));
        }

        /// <summary>
        /// Sets the poc addresses list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="isControlGroup">if set to <c>true</c> [is control group].</param>
        /// <param name="pocListInsert">The poc list insert.</param>
        /// <param name="pocListDelete">The poc list delete.</param>
        /// <returns></returns>
        internal static int SetPocAddressesList(int contractId, bool isControlGroup, string pocListInsert, string pocListDelete) {
            return ConvertEx.ToInt(ExecuteScalarStoredProcedure(
                Constants.SP_SET_CONTRACT_ADDRESSES_LIST,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_CONTRACT_ADDRESSES_LIST_PARAM2, isControlGroup),
                    new SqlParameter(Constants.SP_SET_CONTRACT_ADDRESSES_LIST_PARAM3,
                        string.IsNullOrEmpty(pocListInsert) ? (object) DBNull.Value : pocListInsert),
                    new SqlParameter(Constants.SP_SET_CONTRACT_ADDRESSES_LIST_PARAM4,
                        string.IsNullOrEmpty(pocListDelete) ? (object) DBNull.Value : pocListDelete)
                }));
        }

        /// <summary>
        /// Gets the dynamic indicators by field.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="indicatorId">The indicator id.</param>
        /// <param name="periodColumnNum">Number of column for month</param>
        /// <param name="isPlanColumn">If true - Plan column else - Fact</param>
        /// <param name="xmlBaseIndicators">The XML representing basic indicators</param>
        /// <param name="xmlInvestmentSummary">The XML representing Investment tree</param>
        /// <param name="xmlIndicators">The XML representing Indicators grid</param>
        /// <returns></returns>
        internal static DataTable GetDynamicIndicatorsByField(int contractId, int indicatorId, int periodColumnNum, bool isPlanColumn,
                SqlXml xmlBaseIndicators, SqlXml xmlInvestmentSummary, SqlXml xmlIndicators) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM1, contractId),
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM2, indicatorId == 0 ? (object) DBNull.Value : indicatorId),
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM3, periodColumnNum == 0 ? (object) DBNull.Value : periodColumnNum)
                    ,
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM4, isPlanColumn),
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM5, xmlBaseIndicators),
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM6, xmlInvestmentSummary),
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM7, xmlIndicators == null ? (object) DBNull.Value : xmlIndicators)
                });
        }

        /// <summary>
        /// Gets the condition fields.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetConditionsFields(int contractId) {
            return ExecuteStoredProcedure(
                Constants.SP_GET_CONDITIONS_FIELDS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                });
        }

        /// <summary>
        /// Sets the condition fields.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="allowAddSku">if set to <c>true</c> [allow add sku].</param>
        /// <returns></returns>
        internal static int SetConditionsFields(int contractId, bool allowAddSku) {
            return ConvertEx.ToInt(ExecuteScalarStoredProcedure(
                Constants.SP_SET_CONDITIONS_FIELDS,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_SET_CONDITIONS_FIELDS_PARAM2, allowAddSku)
                }));
        }

        internal static void RecalcVPlan(int contractId) {
            DataAccessLayer.ExecuteNonQueryStoredProcedure(Constants.SP_RECALCULATE_VPLAN,
                new SqlParameter(Constants.SP_RECALCULATE_VPLAN_PARAM1, contractId));
        }

        /// <summary>
        /// Executes the scalar stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <returns></returns>
        private static object ExecuteScalarStoredProcedure(string spName) {
            return ExecuteScalarStoredProcedure(spName, null);
        }

        /// <summary>
        /// Executes the scalar stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <param name="paramList">The param list.</param>
        /// <returns></returns>
        private static object ExecuteScalarStoredProcedure(string spName, params SqlParameter[] paramList) {
            object lResult = null;

            WaitManager.StartWait();
            try {
                try {
                    lResult = null != paramList
                        ? DataAccessLayer.ExecuteScalarStoredProcedure(spName, paramList) : DataAccessLayer.ExecuteScalarStoredProcedure(spName);
                }
                catch (Exception lException) {
                    ExceptionHandler.HandleException(lException);
                }
            }
            finally {
                WaitManager.StopWait();                
            }
            return lResult;
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName) {
            return ExecuteStoredProcedure(spName, null);
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <param name="paramList">The param list.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName, params SqlParameter[] paramList) {
            DataTable lDataTable = null;
            WaitManager.StartWait();
            try {
                try {
                    DataSet lDataSet = paramList != null ? DataAccessLayer.ExecuteStoredProcedure(spName, paramList) : DataAccessLayer.ExecuteStoredProcedure(spName);
                    if (null != lDataSet && lDataSet.Tables.Count > 0)
                        lDataTable = lDataSet.Tables[0];
                }
                catch (Exception lException) {
                    ExceptionHandler.HandleException(lException);
                }
            }
            finally {
                WaitManager.StopWait();
            }
            return lDataTable;
        }

        public static decimal GetAverageMacoPerDal(int contractId) {
            object lResult = ExecuteScalarStoredProcedure(Constants.spDW_CM_GetAverageMacoPerDal,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId)
                });
            return ConvertEx.ToDecimal(lResult);
        }

        public static void SetOutletComment(int contractId, long olId, string commentText) {
            DataAccessLayer.ExecuteNonQueryStoredProcedure(
                Constants.SP_SET_OUTLET_COMMENT,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_ID, contractId),
                    new SqlParameter(Constants.SP_PARAM_OL_ID, olId),
                    new SqlParameter(Constants.SP_PARAM_COMMENT, commentText),
                });
        }

        public static bool CheckContractNo(int contractId, string contractNo) {
            object lResult = ExecuteScalarStoredProcedure(Constants.spDW_CM_CheckContractNo,
                new SqlParameter[] {
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_Id, contractId),
                    new SqlParameter(Constants.SP_PARAM_CONTRACT_NO, contractNo)
                });
            return 0 == ConvertEx.ToInt(lResult);
        }
    }
}