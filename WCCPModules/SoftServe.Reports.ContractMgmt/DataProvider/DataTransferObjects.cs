﻿using System;
using System.Data.SqlTypes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.Enums;

namespace SoftServe.Reports.ContractMgmt
{
    /// <summary>
    /// 
    /// </summary>
    public class CommonBaseExternalData
    {
        public event EventHandler ContractStatusChanging;
        public event EventHandler ContractStatusChanged;

        public class ContractStatusEventArgs : EventArgs
        {
            public ContractStatus PrevStatus { get; set; }
            public ContractStatus NewStatus { get; set; }

            public ContractStatusEventArgs(ContractStatus prevStatus, ContractStatus newStatus)
            {
                PrevStatus = prevStatus;
                NewStatus = newStatus;
            }
        }

        /// <summary>
        /// Fires the contract status changed.
        /// </summary>
        /// <param name="prevStatus">The prev status.</param>
        /// <param name="newStatus">The new status.</param>
        public void FireContractStatusChanged(ContractStatus prevStatus, ContractStatus newStatus)
        {
            if (ContractStatusChanged != null && prevStatus != newStatus)
            {
                ContractStatusChanged(this, new ContractStatusEventArgs(prevStatus, newStatus));
            }
        }

        /// <summary>
        /// Fires the contract status changing.
        /// </summary>
        /// <param name="prevStatus">The prev status.</param>
        /// <param name="newStatus">The new status.</param>
        public void FireContractStatusChanging(ContractStatus prevStatus, ContractStatus newStatus)
        {
            if (ContractStatusChanging != null && prevStatus != newStatus)
            {
                ContractStatusChanging(this, new ContractStatusEventArgs(prevStatus, newStatus));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseExternalData"/> class.
        /// </summary>
        public CommonBaseExternalData()
        {
            AddressesPocList = new List<string>();
            ControlGroupPocList = new List<string>();
        }

        public int ContractId { get; set; }
        public ContractStatus ContractStatus { get; set; } 
        public int ClientId { get; set; }
        public bool IsEditMode { get; set; }
        public bool HasAccessAddresses { get; set; }
        public string ClientName { get; set; }
        public ChannelType Channel { get; set; }
        public Collection<AccessLevelType> AccessLevel { get; set; }
        public UserLevelType UserLevel { get; set; }

        public DateTime DateSignPlanned { set; get; }
        public int ContractTerm { set; get; }
        public List<string> AddressesPocList { set; get; }
        public List<string> ControlGroupPocList { set; get; }

        public bool AllowEditContract 
        { 
            get
            {
                bool allow = CheckAccessLevel(AccessLevelType.AllowEdit) && UserLevel != UserLevelType.Unknown;
                if (allow)
                {
                    switch (ContractStatus)
                    {
                        case ContractStatus.New:
                            break;
                        case ContractStatus.ClientAgreementRequired:
                            allow = HasAccessAddresses;
                            break;
                        case ContractStatus.InitialApproveRequired:
                        case ContractStatus.FinalApproveRequired:
                            allow = UserLevel != UserLevelType.M2 && CheckAccessLevel(AccessLevelType.AllowApprove);
                            break;
                        case ContractStatus.SignRequired:
                            allow = HasAccessAddresses;
                            break;
                        case ContractStatus.InWork:
                            allow = false;
                            break;
                        case ContractStatus.Completed:
                        case ContractStatus.DeclinedInBev:
                        case ContractStatus.DeclinedClient:
                        case ContractStatus.TerminatedInBev:
                        case ContractStatus.TerminatedClient:
                            allow = false;
                            break;
                        default:
                            allow = false;
                            break;
                    }
                }

                return allow;
            } 
        }

        /// <summary>
        /// Gets a value indicating whether [allow address change].
        /// </summary>
        /// <value><c>true</c> if [allow address change]; otherwise, <c>false</c>.</value>
        public bool AllowAddressChange
        {
            get
            {
                bool allow = false;
                if (ContractStatus == ContractStatus.InWork)
                {
                    allow = CheckAccessLevel(AccessLevelType.AllowSignedContractAddressesEdit);
                }
                else
                {
                    allow = CheckAccessLevel(AccessLevelType.AllowEdit) && UserLevel != UserLevelType.Unknown;
                    if (allow)
                    {
                        if (IsContractSigned)
                        {
                            allow = CheckAccessLevel(AccessLevelType.AllowSignedContractAddressesEdit);
                        }
                    }
                }
                return allow;
            }
        }

        public bool IsContractSigned { get { return (int)ContractStatus > (int)ContractStatus.SignRequired; } }
        
        /// <summary>
        /// Checks the access level.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public bool CheckAccessLevel(AccessLevelType type) 
        {
            return AccessLevel != null ? AccessLevel.Contains(type) : false;
        }

        /////////////// Budget Label Update - begin ///////////////////////////////////
        public event EventHandler BudgetLabelUpdated;
        public void FireBudgetLabelUpdate()
        {
            if (BudgetLabelUpdated != null)
            {
                BudgetLabelUpdated(this, new EventArgs());
            }
        }

        private bool isBudgetNotApproved;
        public bool IsBudgetApproved { get { return isBudgetNotApproved; } set { isBudgetNotApproved = value; FireBudgetLabelUpdate(); } }

        private decimal totalExpensesInvestments;
        public decimal TotalExpensesInvestments { get { return totalExpensesInvestments; } set { totalExpensesInvestments = value; FireBudgetLabelUpdate(); } }

        private decimal totalBudgetInvestments;
        public decimal TotalBudgetInvestments { get { return totalBudgetInvestments; } set { totalBudgetInvestments = value; FireBudgetLabelUpdate(); } }

        public bool IsBudgetOverdraft { get { return TotalExpensesInvestments > TotalBudgetInvestments; } }
        /////////////// Budget Label Update - end ///////////////////////////////////
    }

    /// <summary>
    /// 
    /// </summary>
    internal class AddressDTO
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int OutletId { get; set; }
        public int RecordId { get; set; }
        public int ErrorCode { get; set; }
    }
}
