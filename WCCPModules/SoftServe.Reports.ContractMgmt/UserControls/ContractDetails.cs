﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.Enums;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ContractMgmt.Forms;
using WccpReporting;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// 
    /// </summary>
    public partial class ContractDetailsControl : CommonBaseControl {
        #region Fields

        private ContractDetailsCommon _controlCommon;
        private ContractDetailsAdvert _controlAdvert;
        private ContractDetailsInvestment _controlInvestments;
        private ContractDetailsConditions _controlConditions;
        private ContractDetailsAddresses _controlAddresses;
        private ContractDetailsControlGroup _controlControlGroup;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsControl"/> class.
        /// </summary>
        public ContractDetailsControl() {
            InitControls();
            InitializeComponent();
            MainData = new CommonBaseExternalData();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        /// <param name="contractId">The contract id.</param>
        /// <param name="clientId">The client id.</param>
        public ContractDetailsControl(CommonBaseTab parentTab, CommonBaseExternalData data) : base(parentTab) {
            InitControls();
            InitializeComponent();
            MainData = data;
            InitTabControl();
        }

        internal ContractDetailsTabType ChangedTab { get; set; }

        /// <summary>
        /// Gets the current tab.
        /// </summary>
        /// <value>The current tab.</value>
        private ContractDetailsTabType CurrentTab {
            get { return (ContractDetailsTabType) tabControl.SelectedTabPageIndex; }
        }

        /// <summary>
        /// Gets the current base tab.
        /// </summary>
        /// <value>The current base tab.</value>
        private CommonBaseTab CurrentBaseTab {
            get { return tabControl.SelectedTabPage as CommonBaseTab; }
        }

        /// <summary>
        /// Gets the current base control.
        /// </summary>
        /// <value>The current base control.</value>
        internal CommonBaseControl CurrentBaseControl {
            get { return CurrentBaseTab != null ? CurrentBaseTab.UserControl : null; }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData(bool reloadAllTabs) {
            LoadAccessLevel();
            LoadStatuses();

            LoadTabs(reloadAllTabs);
            UpdateBarButtons();
        }

        /// <summary>
        /// Loads the access level.
        /// </summary>
        private void LoadAccessLevel() {
            MainData.AccessLevel = DataProvider.AccessLevel;
            MainData.UserLevel = DataProvider.UserLevel;
            MainData.HasAccessAddresses = ConvertEx.ToBool(DataProvider.GetHasAccessToAddresses(MainData.ContractId));
        }

        /// <summary>
        /// Loads the statuses.
        /// </summary>
        private void LoadStatuses() {
            if (MainData.IsEditMode)
                MainData.ContractStatus = DataProvider.GetContractStatus(MainData.ContractId);

            barButtonContractStatus.Caption = ContractListControl.GetStatusName(MainData.ContractStatus);
            barButtonContractStatus.ItemLinks.Clear();

            DataTable lDataTable = DataProvider.GetNextContractStatuses(MainData.ContractId, MainData.ContractStatus);
            if (lDataTable != null)
                foreach (DataRow lRow in lDataTable.Rows) {
                    BarItem lBarItem = CreateBarItem((ContractStatus) ConvertEx.ToInt(lRow[Constants.FIELD_CONTRACT_STATE_ID]));
                    lBarItem.ImageIndex = barButtonContractStatus.ImageIndex;
                    barButtonContractStatus.AddItem(lBarItem);
                }

            UpdateReadOnlyMode();
        }

        /// <summary>
        /// Creates the bar item.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        private BarItem CreateBarItem(ContractStatus status) {
            string lCaption = ContractListControl.GetStatusName(status);
            BarButtonItem lBarItem = new BarButtonItem(barManager, lCaption);
            lBarItem.ItemClick += barButtonNewStatus_ItemClick;
            lBarItem.Tag = status;
            return lBarItem;
        }

        private void InitControls() {
            ChangedTab = ContractDetailsTabType.Unknown;
            _controlControlGroup = new ContractDetailsControlGroup {ContractDetailsControl = this};
            _controlAddresses = new ContractDetailsAddresses {ContractDetailsControl = this};
            _controlConditions = new ContractDetailsConditions {ContractDetailsControl = this};
            _controlInvestments = new ContractDetailsInvestment {ContractDetailsControl = this/*, ConditionsControl = _controlConditions*/};
            _controlAdvert = new ContractDetailsAdvert {ContractDetailsControl = this};
            _controlCommon = new ContractDetailsCommon {ContractDetailsControl = this};
        }

        /// <summary>
        /// Inits the tab control.
        /// </summary>
        private void InitTabControl() {
            AttachEvents(false);
            AddTab(_controlCommon, Resource.TabInfo);
            AddTab(_controlAdvert, Resource.TabAdvert);
            AddTab(_controlInvestments, Resource.TabInvestment);
            AddTab(_controlConditions, Resource.TabConditions);
            AddTab(_controlAddresses, Resource.TabAddresses);
            AddTab(_controlControlGroup, Resource.TabControlGroup);
            AttachEvents(true);
        }

        /// <summary>
        /// Loads the sub tabs.
        /// </summary>
        private void LoadTabs(bool reloadAllTabs) {
            if (MainData.IsEditMode) {
                if (reloadAllTabs) {
                    // initial load
                    if (_controlAddresses.IsDataLoaded)
                        _controlAddresses.LoadData();
                    if (_controlControlGroup.IsDataLoaded)
                        _controlControlGroup.LoadData();
                    _controlCommon.LoadData();
                    _controlAdvert.LoadData();
                    _controlInvestments.LoadData();
//                    if (_controlConditions.IsDataLoaded)
                    _controlConditions.LoadData();
                }
                else if (CurrentBaseControl != null)
                    CurrentBaseControl.LoadData();
            }
            else {
                // new contract created
                _controlAddresses.LoadData();
                _controlCommon.LoadData();
                _controlAdvert.LoadData();

                // simulate change so user cannot go to another tab untill save the current common tab
                // when a user will save the tab new contract will be created
                _controlCommon.OnChanged(true);
            }
        }

        /// <summary>
        /// Updates the bar menu.
        /// </summary>
        private void UpdateBarButtons() {
            barButtonSave.Enabled = MainData.AllowEditContract && CurrentBaseControl.IsChanged;

            // post processing - there is a couple of exceptions for state InWork...
            if (!barButtonSave.Enabled && CurrentBaseControl.IsChanged && MainData.ContractStatus == ContractStatus.InWork) {
                bool lPossiblyWeCanEditInvestments = (CurrentBaseControl == _controlInvestments && MainData.CheckAccessLevel(AccessLevelType.AllowEdit));
                bool lPossiblyWeCanEditAddresses = ((CurrentBaseControl == _controlAddresses || CurrentBaseControl == _controlControlGroup) &&
                    MainData.CheckAccessLevel(AccessLevelType.AllowSignedContractAddressesEdit));
                bool lPossiblyWeCanEditConditions = (CurrentBaseControl == _controlConditions && _controlConditions.IsAllowAddSku
                    && MainData.CheckAccessLevel(AccessLevelType.AllowSignedContractAddressesEdit));
                barButtonSave.Enabled = lPossiblyWeCanEditInvestments || lPossiblyWeCanEditAddresses || lPossiblyWeCanEditConditions;
            }

            barButtonRemove.Visibility = MainData.IsEditMode && MainData.ContractStatus == ContractStatus.New
                ? BarItemVisibility.Always : BarItemVisibility.Never;
            barButtonRemove.Enabled = MainData.AllowEditContract;

            bool lAllowChangeStatus = MainData.AllowEditContract;
            if (!lAllowChangeStatus && MainData.ContractStatus == ContractStatus.InWork)
                lAllowChangeStatus = true;
            barButtonContractStatus.Enabled = MainData.IsEditMode && barButtonContractStatus.ItemLinks.Count > 0 && lAllowChangeStatus;

            barButtonStorcheck.Visibility = BarItemVisibility.Never;
            //barButtonStorcheck.Visibility = (CurrentTab == ContractDetailsTabType.Conditions ? BarItemVisibility.Always : BarItemVisibility.Never);
            //barButtonStorcheck.Enabled = MainData.ContractStatus == ContractStatus.InWork && MainData.CheckAccessLevel(AccessLevelType.AllowStorcheck);

            barButtonAddCostItem.Visibility = (CurrentTab == ContractDetailsTabType.Investments ? BarItemVisibility.Always : BarItemVisibility.Never);
            barButtonAddCostItem.Enabled = MainData.AllowEditContract;

            barButtonPlanMACOCalc.Visibility = (CurrentTab == ContractDetailsTabType.Investments ? BarItemVisibility.Always : BarItemVisibility.Never);
            barButtonPlanMACOCalc.Enabled = MainData.AllowEditContract;
        }

        /// <summary>
        /// Handles the UpdateMainView event of the ContractDetailsControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUpdateMainView(object sender, EventArgs e) {
            UpdateBarButtons();
        }

        /// <summary>
        /// Selects the tab.
        /// </summary>
        /// <param name="tab">The tab.</param>
        private void SelectTab(ContractDetailsTabType tab) {
            tabControl.SelectedTabPageIndex = (int) tab;
        }

        /// <summary>
        /// Gets the sub tab.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private CommonBaseTab GetSubTab(ContractDetailsTabType type) {
            return tabControl.TabPages[(int) type] as CommonBaseTab;
        }

        /// <summary>
        /// Enables the sub tab.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private void EnableSubTab(ContractDetailsTabType type, bool enable) {
            AttachEvents(false);
            GetSubTab(type).PageEnabled = enable;
            AttachEvents(true);
        }

        /// <summary>
        /// Attaches the events.
        /// </summary>
        /// <param name="attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach) {
                tabControl.SelectedPageChanging += tabControl_SelectedPageChanging;
                tabControl.SelectedPageChanged += tabControl_SelectedPageChanged;
            }
            else {
                tabControl.SelectedPageChanged -= tabControl_SelectedPageChanged;
                tabControl.SelectedPageChanging -= tabControl_SelectedPageChanging;
            }
        }

        /// <summary>
        /// Adds the tab.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="tabName">Name of the tab.</param>
        private void AddTab(CommonBaseControl content, string tabName) {
            if (content == null || ParentTab == null)
                return;

            CommonBaseTab lNewCommonBaseTab = new CommonBaseTab();
            content.MainData = MainData;
            content.UpdateMainView += OnUpdateMainView;
            lNewCommonBaseTab.Init(content, tabName);
            tabControl.TabPages.Add(lNewCommonBaseTab);
            content.AutoScroll = true;
        }

        /// <summary>
        /// Deletes the contract.
        /// </summary>
        private void DeleteContract() {
            if (MainData.ContractId <= 0 || ParentTab == null)
                return;

            if (XtraMessageBox.Show(
                    string.Format(Resource.ConfirmContractDelete, ParentTab.Text),
                    Resource.ContractDeleting, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;

            if (DataProvider.DeleteContract(MainData.ContractId) <= 0)
                return;

            ParentTab.CloseTab();
            ParentTab.UpdateContractListTab();
        }

        /// <summary>
        /// Saves the contract.
        /// </summary>
        /// <param name="saveButtonPressed">if set to <c>true</c> [save button pressed].</param>
        /// <returns></returns>
        private bool SaveTab(bool saveButtonPressed) {
            bool lIsSaved = false;
            bool lIsValid = false;

            if (!MainData.IsEditMode && CurrentBaseControl == _controlCommon) {
                // Add mode
                if (_controlCommon.ValidateData().IsCorrect) {
                    MainData.ContractId = DataProvider.CreateNewContract(MainData.ClientId, MainData.Channel,
                        MainData.DateSignPlanned, MainData.ContractTerm);
                    MainData.IsEditMode = MainData.ContractId > 0;
                    lIsValid = _controlAddresses.SaveData() > 0;
                }
            }
            else if (CurrentBaseControl != null) {
                // Edit mode
                ValidateMetadata lValidateResult = CurrentBaseControl.ValidateData();
                lIsValid = lValidateResult.IsCorrect;
            }

            if (lIsValid) {
                // save current tab and reload related if required
                lIsSaved = SaveRelatedTabs(CurrentBaseTab, saveButtonPressed);
                if (lIsSaved)
                    CurrentBaseControl.ResetChangedFlag();
            }
            else
                XtraMessageBox.Show(Resource.SaveInvalidData,
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);

            return lIsSaved;
        }

        /// <summary>
        /// Saves the and update related tabs.
        /// </summary>
        /// <param name="baseTab">The base tab.</param>
        /// <param name="saveButtonPressed">true if Save button pressed</param>
        /// <returns></returns>
        private bool SaveRelatedTabs(CommonBaseTab baseTab, bool saveButtonPressed) {
            bool lIsSucceeded = false;
            if (baseTab == null || baseTab.UserControl == null)
                return false;

            if (!baseTab.UserControl.IsChanged)
                return false;

            bool lNeedContinue = true;
            string lMessage = string.Empty;
            Collection<CommonBaseControl> lRelatedControls = GetRelatedTabContext(baseTab.TabType);

            if (baseTab.UserControl.IsExternalRefreshRequired && lRelatedControls.Count > 0)
                lMessage = string.Format(Resource.TabSaveExternalChanges, GetTabsTextList(lRelatedControls));
            else if (!saveButtonPressed)
                lMessage = Resource.TabSaveChanges;

            if (!string.IsNullOrEmpty(lMessage))
                lNeedContinue = XtraMessageBox.Show(lMessage, Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) ==
                    DialogResult.OK;

            if (!lNeedContinue)
                return false;

            int lRetCode = baseTab.UserControl.SaveData();
            if (lRetCode > 0) {
                try {
                    bool lIsCalcVRequired = baseTab.UserControl.IsCalcVRequired;

                    if (lIsCalcVRequired) {
                        DataProvider.RecalcVPlan(MainData.ContractId);
                        if (baseTab.UserControl is ITwoStepSavingControl && ((ITwoStepSavingControl) baseTab.UserControl).IsSecondStepRequired)
                            ((ITwoStepSavingControl) baseTab.UserControl).SecondStageSave();
                    }

                    ChangedTab = baseTab.TabType;

                    if (baseTab.UserControl.IsExternalRefreshRequired) {
                        if (IsCalcVRequired)
                            baseTab.UserControl.LoadData();

                        foreach (CommonBaseControl lControl in lRelatedControls)
                            if (lControl != null)
                                lControl.LoadData();
                    }
                }
                finally {
                    ChangedTab = ContractDetailsTabType.Unknown;
                }
                UpdateTabName();
                lIsSucceeded = true;
            }
            else
                XtraMessageBox.Show(string.Format(Resource.SaveError, lRetCode),
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

            return lIsSucceeded;
        }

        /// <summary>
        /// It is the main save method
        /// </summary>
        /// <param name="prevStatus">The previous Status.</param>
        /// <param name="newStatus">The new status.</param>
        /// <param name="saveButtonPressed">If true Save button pressed.</param>
        private bool SaveTabWithState(ContractStatus prevStatus, ContractStatus newStatus, bool saveButtonPressed) {
            bool lStatusChanged = prevStatus != newStatus;

            // check we have correct base control object
            if (CurrentBaseControl == null)
                return false;

            MainData.FireContractStatusChanging(prevStatus, newStatus);

            ValidateMetadata lValidateResult = CurrentBaseControl.ValidateData();
            // validate current tab
            if (!lValidateResult.IsCorrect) {
                //Check whether validation is failed.
                //Display default message if validation result didn't return any message.
                XtraMessageBox.Show(lValidateResult.Message ?? Resource.SaveInvalidData,
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }

            //-------------------------------------------------------------------------------------------------------
            // Check workflow state and react on it - BEGIN - it will be decomposed to a separate method later
            //-------------------------------------------------------------------------------------------------------
            if (lStatusChanged
                && XtraMessageBox.Show(Resource.TryStatusChangeMessage, Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) !=
                    DialogResult.OK)
                return false;

            if (lStatusChanged && (newStatus == ContractStatus.DeclinedClient || newStatus == ContractStatus.DeclinedInBev ||
                newStatus == ContractStatus.TerminatedClient || newStatus == ContractStatus.TerminatedInBev)) {
                // add comment to declining or terminated contract 
                FormComment formComment = new FormComment();
                formComment.Text = Resource.DeclineReasonCaption;
                if (formComment.ShowDialog(this) == DialogResult.OK) {
                    _controlCommon.AppendComment(string.Format(Resource.DeclineReasonFormat, formComment.Comment));
                    saveButtonPressed = true;
                }
                else
                    return false;
            }
            else if (lStatusChanged && newStatus == ContractStatus.InitialApproveRequired) {
                // verify investments tab is valid
                if (!_controlInvestments.ValidateData().IsCorrect) {
                    if (CurrentBaseControl != _controlInvestments && !CurrentBaseControl.IsChanged)
                        SelectTab(ContractDetailsTabType.Investments);

                    XtraMessageBox.Show(Resource.InvalidInvestmentsTab, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            else if (lStatusChanged && prevStatus == ContractStatus.InitialApproveRequired && newStatus == ContractStatus.SignRequired) {
                // verify total investments are not greater then current budget
                if (_controlInvestments.PlanTotalInvestmentSum > _controlCommon.TotalBudgetInvestments) {
                    XtraMessageBox.Show(Resource.InvalidCommonTabBudgetPlan, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            else if (prevStatus == ContractStatus.SignRequired && newStatus == ContractStatus.InWork) {
                // here we have to validate common tab
                if (!_controlCommon.ValidateData().IsCorrect) {
                    if (CurrentBaseControl != _controlCommon && !CurrentBaseControl.IsChanged)
                        SelectTab(ContractDetailsTabType.Common);

                    XtraMessageBox.Show(Resource.InvalidCommonTabRequiredFields, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }

            if (GetNeedResetToClientAgrement(prevStatus, newStatus)) {
                // reset state to agreement required
                newStatus = ContractStatus.ClientAgreementRequired;
                lStatusChanged = prevStatus != newStatus;
                _controlCommon.ResetSignDate(); //Reseting sign date of the contract.
            }

            //-------------------------------------------------------------------------------------------------------
            // Check workflow state react on it - END
            //-------------------------------------------------------------------------------------------------------

            MainData.ContractStatus = newStatus; // set current new status

            // Save current tab
            bool lIsSaved;
            if (barButtonSave.Enabled && CurrentBaseControl.IsChanged)
                lIsSaved = SaveTab(saveButtonPressed);
            else
                lIsSaved = true;

            // Save new status (required to be done at the very end)
            if (lStatusChanged && lIsSaved) {
                lIsSaved = SaveContractStatus() > 0;
                if (lIsSaved) {
                    MainData.FireContractStatusChanged(prevStatus, newStatus);
                    LoadStatuses();
                }
            }

            LoadData(false); //Trigger LoadData method in order to refresh all data in contract details.

            if (prevStatus == ContractStatus.SignRequired && newStatus == ContractStatus.InWork && CurrentBaseControl != _controlCommon)
                _controlCommon.LoadData(); //Reload Common control to show sign date if it wasn't filled.

            if (lIsSaved) {
                if (ParentTab != null)
                    ParentTab.UpdateContractListTab();
                XtraMessageBox.Show(Resource.SavedSuccessfull, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MainData.ContractStatus = prevStatus; // rollback status to the previous one

            UpdateBarButtons();

            return lIsSaved;
        }

        /// <summary>
        /// Updates the read only mode.
        /// </summary>
        public override void UpdateReadOnlyMode() {
            if (MainData.AllowEditContract)
                return;

            SetReadOnlyModeRecursively(_controlCommon);
            SetReadOnlyModeRecursively(_controlAdvert);
            SetReadOnlyModeRecursively(_controlInvestments);
            SetReadOnlyModeRecursively(_controlConditions);
            SetReadOnlyModeRecursively(_controlAddresses);
            SetReadOnlyModeRecursively(_controlControlGroup);
        }

        /// <summary>
        /// Gets the need reset to client agrement.
        /// </summary>
        /// <param name="prevStatus">The prev status.</param>
        /// <param name="newStatus">The new status.</param>
        /// <returns></returns>
        private bool GetNeedResetToClientAgrement(ContractStatus prevStatus, ContractStatus newStatus) {
            if (newStatus == ContractStatus.DeclinedClient || newStatus == ContractStatus.DeclinedInBev)
                return false;
            if ((prevStatus != ContractStatus.InitialApproveRequired && prevStatus != ContractStatus.FinalApproveRequired)
                && prevStatus != ContractStatus.SignRequired)
                return false;
            if (MainData.ContractStatus == ContractStatus.SignRequired && CurrentBaseControl == _controlCommon && _controlCommon.IsChanged)
                return _controlCommon.IsAgreementStateChangeRequired;

            return CurrentBaseControl.IsChanged;
        }

        /// <summary>
        /// Saves the contract status.
        /// </summary>
        private int SaveContractStatus() {
            return DataProvider.SetContractStatus(MainData.ContractId, MainData.ContractStatus);
        }

        /// <summary>
        /// Updates the name of the tab.
        /// </summary>
        private void UpdateTabName() {
            if (ParentTab == null)
                return;

            string lCaptionEnding = !string.IsNullOrEmpty(_controlCommon.SelectedContractNumber)
                ? _controlCommon.SelectedContractNumber : MainData.ContractId.ToString();
            ParentTab.Text = string.Format(Resource.TabCaption, MainData.ClientName, lCaptionEnding);
        }

        #region Events

        /// <summary>
        /// Handles the ItemClick event of the barButtonSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonSave_ItemClick(object sender, ItemClickEventArgs e) {
            SaveTabWithState(MainData.ContractStatus, MainData.ContractStatus, true);
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonRemove control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonRemove_ItemClick(object sender, ItemClickEventArgs e) {
            DeleteContract();
        }

        /// <summary>
        /// Handles the SelectedPageChanged event of the tabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTab.TabPageChangedEventArgs"/> instance containing the event data.</param>
        private void tabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e) {
            UpdateBarButtons();
            if (WccpUIControl.Bruc == null)
                return;

            WccpUIControl lWccpControl = WccpUIControl.Bruc as WccpUIControl;
            if (lWccpControl == null)
                return;

            if (CurrentBaseControl != null && (CurrentBaseControl is ContractDetailsAddresses 
                || CurrentBaseControl is ContractDetailsControlGroup
                || CurrentBaseControl is ContractDetailsConditions))
                lWccpControl.SetExportButtonEnabled(true);
            else
                lWccpControl.SetExportButtonEnabled(false);
        }

        /// <summary>
        /// Handles the SelectedPageChanging event of the tabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTab.TabPageChangingEventArgs"/> instance containing the event data.</param>
        private void tabControl_SelectedPageChanging(object sender, TabPageChangingEventArgs e) {
            e.Cancel = false;
            CommonBaseTab lTab = e.Page as CommonBaseTab;
            if (null != lTab && null != lTab.UserControl) {
                if (lTab.UserControl is ContractDetailsAddresses && _controlAddresses != null && !_controlAddresses.IsDataLoaded) {
                    WaitManager.StartWait();
                    try {
                        _controlAddresses.LoadData();
                    }
                    finally {
                        WaitManager.StopWait();
                    }
                }
                else if (lTab.UserControl is ContractDetailsControlGroup && _controlControlGroup != null && !_controlControlGroup.IsDataLoaded) {
                    WaitManager.StartWait();
                    try {
                        _controlControlGroup.LoadData();
                    }
                    finally {
                        WaitManager.StopWait();
                    }
                }
                // commented because we need sync V in Conditions and Investment tabs
                //else if (lTab.UserControl is ContractDetailsConditions && _controlConditions != null && !_controlConditions.IsDataLoaded) {
                //    WaitManager.StartWait();
                //    try {
                //        _controlConditions.LoadData();
                //    }
                //    finally {
                //        WaitManager.StopWait();
                //    }
                //}
            }

            if (CurrentBaseControl != null && CurrentBaseControl.IsChanged)
                e.Cancel = !SaveTabWithState(MainData.ContractStatus, MainData.ContractStatus, false);
        }

        /// <summary>
        /// Gets the tabs text list.
        /// </summary>
        /// <param name="tabCollection">The tab collection.</param>
        /// <param name="tabControl">The tab control.</param>
        /// <returns></returns>
        private static string GetTabsTextList(Collection<CommonBaseControl> controls) {
            string lResult = string.Empty;
            foreach (CommonBaseControl lControl in controls)
                if (lControl != null && lControl.ParentTab != null)
                    lResult += string.Format("\t{0}\n", lControl.ParentTab.Text);
            
            return lResult.TrimEnd('\n');
        }

        /// <summary>
        /// Gets the context related tabs.
        /// </summary>
        /// <param name="tabType">Type of the tab.</param>
        /// <returns></returns>
        private Collection<CommonBaseControl> GetRelatedTabContext(ContractDetailsTabType tabType) {
            Collection<CommonBaseControl> lBaseControls = new Collection<CommonBaseControl>();
            switch (tabType) {
                case ContractDetailsTabType.Common:
                    lBaseControls.Add(_controlAddresses);
                    lBaseControls.Add(_controlInvestments);
                    lBaseControls.Add(_controlConditions);
                    lBaseControls.Add(_controlControlGroup);
                    break;
                case ContractDetailsTabType.Conditions:
//                    lBaseControls.Add(_controlCommon);
//                    lBaseControls.Add(_controlInvestments);
                    break;
                case ContractDetailsTabType.Addresses:
                    lBaseControls.Add(_controlCommon);
                    lBaseControls.Add(_controlInvestments);
                    lBaseControls.Add(_controlConditions);
                    break;
                case ContractDetailsTabType.ControlGroup:
                    lBaseControls.Add(_controlInvestments);
                    lBaseControls.Add(_controlConditions);
                    break;
                case ContractDetailsTabType.Investments:
                    lBaseControls.Add(_controlCommon);
                    lBaseControls.Add(_controlConditions);
                    break;
                default:
                    break;
            }
            return lBaseControls;
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonAddCostItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonAddCostItem_ItemClick(object sender, ItemClickEventArgs e) {
            _controlInvestments.AddExpense(MainData.ContractId);
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonNewStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonNewStatus_ItemClick(object sender, ItemClickEventArgs e) {
            SaveTabWithState(MainData.ContractStatus, (ContractStatus) e.Item.Tag, false);
        }

        private void barButtonPlanMACOCalc_ItemClick(object sender, ItemClickEventArgs e) {
            _controlInvestments.RecalculateMacoPlan();
        }

        #endregion

        public void UpdatePlanVolume(PlanVolumeChangedEventArgs args) {
            _controlConditions.UpdatePlanVolume(args);
        }
    }
}