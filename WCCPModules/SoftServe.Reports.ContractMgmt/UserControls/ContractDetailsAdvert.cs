﻿using System;
using System.Data;
using Logica.Reports.BaseReportControl.Utility;
using System.Data.SqlTypes;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using SoftServe.Reports.ContractMgmt.Utility;

namespace SoftServe.Reports.ContractMgmt.UserControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ContractDetailsAdvert : CommonBaseControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsAdvert"/> class.
        /// </summary>
        public ContractDetailsAdvert()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsAdvert"></see> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public ContractDetailsAdvert(CommonBaseTab parentTab)
            : base(parentTab)
        {
            InitializeComponent();   
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData() {
            AttachEvents(false);
            AllFieldsData = DataProvider.GetAdvertDetails(MainData.ContractId);
            LoadEvents();
            LoadMediums();
            LoadPlannedChanges();
            LoadPlannedAdvert();
            base.LoadData();
            AttachEvents(true);
        }

        /// <summary>
        /// Attaches the events.
        /// </summary>
        /// <param name="attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach)
        {
            if (attach)
            {
                memoIsAdvertising.EditValueChanged += new EventHandler(OnEditValueChanged);
                textCompanyPartner.EditValueChanged += new EventHandler(OnEditValueChanged);

                gridViewEvents.CellValueChanged += new CellValueChangedEventHandler(OnCellValueChanged);
                gridViewPlannedChanges.CellValueChanged += new CellValueChangedEventHandler(OnCellValueChanged);
                gridViewMediums.CellValueChanged += new CellValueChangedEventHandler(OnCellValueChanged);
                gridViewPlannedAdvert.CellValueChanged += new CellValueChangedEventHandler(OnCellValueChanged);
            }
            else
            {
                memoIsAdvertising.EditValueChanged -= new EventHandler(this.OnEditValueChanged);
                textCompanyPartner.EditValueChanged -= new EventHandler(this.OnEditValueChanged);

                gridViewEvents.CellValueChanged -= new CellValueChangedEventHandler(OnCellValueChanged);
                gridViewPlannedChanges.CellValueChanged -= new CellValueChangedEventHandler(OnCellValueChanged);
                gridViewMediums.CellValueChanged -= new CellValueChangedEventHandler(OnCellValueChanged);
                gridViewPlannedAdvert.CellValueChanged -= new CellValueChangedEventHandler(OnCellValueChanged);
            }
        }

        /// <summary>
        /// Loads the events.
        /// </summary>
        private void LoadEvents()
        {
            gridControlEvents.DataSource = DataProvider.GetAdvertEvents(MainData.ContractId);
            repositoryEventsLookUp.DataSource = DataProvider.AdvertEventList;
        }

        /// <summary>
        /// Loads the mediums.
        /// </summary>
        private void LoadMediums()
        {
            gridControlMediums.DataSource = DataProvider.GetAdvertMediums(MainData.ContractId);
            repositoryItemMediumsLookUp.DataSource = DataProvider.AdvertMediumList;
        }

        /// <summary>
        /// Loads the planned changes.
        /// </summary>
        private void LoadPlannedChanges()
        {
            gridControlPlannedChanges.DataSource = DataProvider.GetAdvertPlannedChanges(MainData.ContractId);
        }

        /// <summary>
        /// Loads the planned advert.
        /// </summary>
        private void LoadPlannedAdvert()
        {
            gridControlPlannedAdvert.DataSource = DataProvider.GetAdvertPlannedAdvert(MainData.ContractId);
        }

        /// <summary>
        /// Handles the Load event of the ContractDetailsAdvert control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ContractDetailsAdvert_Load(object sender, EventArgs e)
        {
            splitContainerMain.SplitterPosition = this.Width / 2;
            splitContainerLeft.SplitterPosition = splitContainerRight.SplitterPosition = this.Height / 2;
            DevExpress.XtraGrid.Localization.GridLocalizer.Active = new DefaultLocalizer();
        }

        /// <summary>
        /// Sets all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable AllFieldsData
        {
            set
            {
                if (value != null && value.Rows.Count == 1)
                {
                    DataRow row = value.Rows[0];

                    memoIsAdvertising.EditValue = ConvertEx.ToString(row[Constants.FIELD_ADV_IS_ADVERT]);
                    textCompanyPartner.EditValue = ConvertEx.ToString(row[Constants.FIELD_COMPANY_PARTNER]);
                }
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override int SaveData()
        {
            SaveFields();
            SaveEvents();
            SaveMediums();
            SavePlannedChanges();
            SavePlannedAdvert();
            return 1;
        }

        /// <summary>
        /// Saves the fields.
        /// </summary>
        private void SaveFields()
        {
            DataProvider.SetAdvertDetails(
                MainData.ContractId, 
                memoIsAdvertising.EditValue,
                textCompanyPartner.EditValue);
        }

        /// <summary>
        /// Saves the events.
        /// </summary>
        private void SaveEvents()
        {
            SqlXml sqlXml = ConvertEx.ToSqlXml(gridControlEvents.DataSource as DataTable,
                new string[] {
                    columnEventId.FieldName,
                    columnEventDescription.FieldName
                });

            DataProvider.SetAdvertEvents(MainData.ContractId, sqlXml);
        }

        /// <summary>
        /// Saves the mediums.
        /// </summary>
        private void SaveMediums()
        {
            SqlXml sqlXml = ConvertEx.ToSqlXml(gridControlMediums.DataSource as DataTable,
                new string[] {
                    columnMediumId.FieldName,
                    columnMediumDescription.FieldName
                });

            DataProvider.SetAdvertMedium(MainData.ContractId, sqlXml);
        }

        /// <summary>
        /// Saves the planned changes.
        /// </summary>
        private void SavePlannedChanges()
        {
            SqlXml sqlXml = ConvertEx.ToSqlXml(gridControlPlannedChanges.DataSource as DataTable,
                new string[] {
                    columnPlannedChangeName.FieldName,
                    columnPlannedChangeDate.FieldName
                });

            DataProvider.SetAdvertPlannedChanges(MainData.ContractId, sqlXml);
        }

        /// <summary>
        /// Saves the planned advert.
        /// </summary>
        private void SavePlannedAdvert()
        {
            SqlXml sqlXml = ConvertEx.ToSqlXml(gridControlPlannedAdvert.DataSource as DataTable,
                new string[] {
                    columnPlannedAdvName.FieldName,
                    columnPlannedAdvDescr.FieldName
                });

            DataProvider.SetAdvertPlannedAdvert(MainData.ContractId, sqlXml);
        }

        /// <summary>
        /// Validates the row cell is null.
        /// </summary>
        /// <param name="gridView">The grid view.</param>
        /// <param name="column">The column.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        private bool ValidateRowCellIsNull(GridView gridView, GridColumn column, string fieldName, ValidateRowEventArgs e)
        {
            if (gridView.GetRowCellValue(e.RowHandle, column) == DBNull.Value)
            {
                e.Valid = false;
                e.ErrorText = string.Format(Resource.TheFieldIsEmpty, fieldName);
            }
            return e.Valid;
        }

        /// <summary>
        /// Asks the delete action.
        /// </summary>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.NavigatorButtonClickEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        private void AskDeleteRecotdAction(NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Edit || e.Button.ButtonType == NavigatorButtonType.Remove || e.Button.ButtonType == NavigatorButtonType.Append)
            {
                OnChanged(false);
            }
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                if (XtraMessageBox.Show(Resource.AskRecordDelete, Application.ProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Handles the ButtonClick event of the gridControlEvents_EmbeddedNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.NavigatorButtonClickEventArgs"/> instance containing the event data.</param>
        private void gridControlEvents_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            AskDeleteRecotdAction(e);
        }

        /// <summary>
        /// Handles the ButtonClick event of the gridControlMediums_EmbeddedNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.NavigatorButtonClickEventArgs"/> instance containing the event data.</param>
        private void gridControlMediums_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            AskDeleteRecotdAction(e);
        }

        /// <summary>
        /// Handles the ButtonClick event of the gridControlPlannedChanges_EmbeddedNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.NavigatorButtonClickEventArgs"/> instance containing the event data.</param>
        private void gridControlPlannedChanges_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            AskDeleteRecotdAction(e);
        }

        /// <summary>
        /// Handles the ButtonClick event of the gridControlPlannedAdvert_EmbeddedNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.NavigatorButtonClickEventArgs"/> instance containing the event data.</param>
        private void gridControlPlannedAdvert_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            AskDeleteRecotdAction(e);
        }

        /// <summary>
        /// Handles the ValidateRow event of the gridViewEvents control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
        private void gridViewEvents_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            ValidateRowCellIsNull(gridViewEvents, columnEventId, repositoryEventsLookUp.Columns[0].Caption, e);
        }

        /// <summary>
        /// Handles the ValidateRow event of the gridViewMediums control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
        private void gridViewMediums_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            ValidateRowCellIsNull(gridViewMediums, columnMediumId, repositoryItemMediumsLookUp.Columns[0].Caption, e);
        }

        /// <summary>
        /// Handles the ValidateRow event of the gridViewPlannedChanges control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
        private void gridViewPlannedChanges_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            ValidateRowCellIsNull(gridViewPlannedChanges, columnPlannedChangeName, columnPlannedChangeName.Caption, e);
        }

        /// <summary>
        /// Handles the ValidateRow event of the gridViewPlannedAdvert control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
        private void gridViewPlannedAdvert_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            ValidateRowCellIsNull(gridViewPlannedAdvert, columnPlannedAdvName, columnPlannedAdvName.Caption, e);
        }

        /// <summary>
        /// Handles the InitNewRow event of the gridViewPlannedChanges control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs"/> instance containing the event data.</param>
        private void gridViewPlannedChanges_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridViewPlannedChanges.SetFocusedRowCellValue(columnPlannedChangeDate, DateTime.Today);
            gridViewPlannedChanges.SelectCell(e.RowHandle, columnPlannedChangeName);
        }

        /// <summary>
        /// Handles the InitNewRow event of the gridViewPlannedAdvert control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs"/> instance containing the event data.</param>
        private void gridViewPlannedAdvert_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridViewPlannedAdvert.SelectCell(e.RowHandle, columnPlannedAdvName);
        }

        /// <summary>
        /// Called when [edit value changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnEditValueChanged(object sender, EventArgs e)
        {
            OnChanged(false);
        }

        /// <summary>
        /// Called when [cell value changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            OnChanged(false);
        }
    }

}
