﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ContractDetailsConditions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scrollableArea = new DevExpress.XtraEditors.XtraScrollableControl();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.checkAllowAddSku = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.treeConditionsSku = new Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions.SKUTreeConditions();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.scrollableArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.checkAllowAddSku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // scrollableArea
            // 
            this.scrollableArea.Controls.Add(this.layoutControl);
            this.scrollableArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollableArea.Location = new System.Drawing.Point(0, 0);
            this.scrollableArea.Name = "scrollableArea";
            this.scrollableArea.Size = new System.Drawing.Size(862, 367);
            this.scrollableArea.TabIndex = 1;
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.checkAllowAddSku);
            this.layoutControl.Controls.Add(this.treeConditionsSku);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(862, 367);
            this.layoutControl.TabIndex = 5;
            this.layoutControl.Text = "layoutControl1";
            // 
            // checkAllowAddSku
            // 
            this.checkAllowAddSku.Location = new System.Drawing.Point(12, 12);
            this.checkAllowAddSku.Name = "checkAllowAddSku";
            this.checkAllowAddSku.Properties.Caption = "Разрешить добавлять новые СКЮ, если их группа включена в условия контракта";
            this.checkAllowAddSku.Size = new System.Drawing.Size(838, 19);
            this.checkAllowAddSku.StyleController = this.layoutControl;
            this.checkAllowAddSku.TabIndex = 4;
            this.checkAllowAddSku.EditValueChanged += new System.EventHandler(this.OnCheckEditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(862, 367);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // treeConditionsSku
            // 
            this.treeConditionsSku.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeConditionsSku.EditMode = Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions.DataAccess.SKUTreeConditionsEditMode.Common;
            this.treeConditionsSku.IsAllowAddSku = false;
            this.treeConditionsSku.Location = new System.Drawing.Point(12, 35);
            this.treeConditionsSku.Name = "treeConditionsSku";
            this.treeConditionsSku.Size = new System.Drawing.Size(838, 320);
            this.treeConditionsSku.TabIndex = 0;
            this.treeConditionsSku.EditValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.OnEditValueChanged);
            this.treeConditionsSku.SKUStructureChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.OnSKUStructureChanged);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkAllowAddSku;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(842, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.treeConditionsSku;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(842, 324);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // ContractDetailsConditions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.scrollableArea);
            this.Name = "ContractDetailsConditions";
            this.Size = new System.Drawing.Size(862, 367);
            this.scrollableArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.checkAllowAddSku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl scrollableArea;
        private Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions.SKUTreeConditions treeConditionsSku;
        private DevExpress.XtraEditors.CheckEdit checkAllowAddSku;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;









    }
}
