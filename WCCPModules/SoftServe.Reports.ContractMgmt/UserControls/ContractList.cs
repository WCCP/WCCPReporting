﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.Enums;
using SoftServe.Reports.ContractMgmt.Forms;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// Implements Contract list.
    /// </summary>
    public partial class ContractListControl : CommonBaseControl {
        /// <summary>
        /// Handles the Load event of the ClientListControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private static DataTable _contractStatusList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListControl"/> class.
        /// </summary>
        public ContractListControl() {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public ContractListControl(CommonBaseTab parentTab) : base(parentTab) {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the grid control.
        /// </summary>
        /// <value>The grid control.</value>
        public GridControl GridControl {
            get { return gridControl; }
        }

        /// <summary>
        /// Gets or sets the client list.
        /// </summary>
        /// <value>The client list.</value>
        public DataTable ContractList {
            set {
                gridControl.DataSource = value;
                LoadAccessLevel();
                UpdateButtonsState();
            }
            get {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable) gridControl.DataSource;
            }
        }

        private void ClientListControl_Load(object sender, EventArgs e) {
            if (!DesignMode)
                LoadData();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData() {
            LoadAccessLevel();

            LoadStatusList();
            LoadFilterDateFromTo();
            LoadRepositories();

            DateTime lDateFrom = barEditDateFrom.EditValue is DateTime ? (DateTime) barEditDateFrom.EditValue : new DateTime(1, 1, 1);
            DateTime lDateTo = barEditDateTo.EditValue is DateTime ? (DateTime) barEditDateTo.EditValue : new DateTime(9999, 12, 31);
            UpdateHighlightExpPeriod(lDateFrom, lDateTo);

            int lDays = ConvertEx.ToInt(barEditNotSignedDays.EditValue);
            UpdateHighlightNotSignedDays(lDays);

            UpdateButtonsState();

            CreateDefaultGridFilter();
        }

        /// <summary>
        /// Loads the filter date from to.
        /// </summary>
        private void LoadFilterDateFromTo() {
            DateTime lDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            barEditDateFrom.EditValue = lDateFrom;

            DateTime lDateTo = lDateFrom.AddDays(DateTime.DaysInMonth(lDateFrom.Year, lDateFrom.Month) - 1);
            barEditDateTo.EditValue = lDateTo;
        }

        /// <summary>
        /// Loads the status list.
        /// </summary>
        private void LoadStatusList() {
            _contractStatusList = DataProvider.ContractStatusList;
        }

        /// <summary>
        /// Loads the repositories.
        /// </summary>
        private void LoadRepositories() {
            repositoryItemStatusLookUp.DataSource = _contractStatusList;
            repositoryItemChannelLookUp.DataSource = DataProvider.ChannelList;
        }

        /// <summary>
        /// Loads the access level.
        /// </summary>
        private void LoadAccessLevel() {
            MainData.AccessLevel = DataProvider.AccessLevel;
            MainData.UserLevel = DataProvider.UserLevel;
        }

        /// <summary>
        /// Gets the name of the status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public static string GetStatusName(ContractStatus status) {
            if (_contractStatusList == null || _contractStatusList.Rows.Count <= 0)
                return string.Empty;

            DataView lDataView = new DataView(_contractStatusList);
            lDataView.Sort = Constants.FIELD_CONTRACT_STATE_ID;
            DataRowView[] lRows = lDataView.FindRows((int) status);
            if (lRows.Length == 1)
                return ConvertEx.ToString(lRows[0][1]);
            return string.Empty;
        }

        /// <summary>
        /// News this instance.
        /// </summary>
        private void New() {
            using (FormClientList lFormClientList = new FormClientList()) {
                if (lFormClientList.ShowDialog() == DialogResult.OK && lFormClientList.SelectedClientId > 0) {
                    LoadDetailsTab(false, lFormClientList.SelectedClientId,
                        lFormClientList.SelectedClientName, lFormClientList.SelectedChannelType);
                }
            }
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        private void Edit() {
            int lClientId = ConvertEx.ToInt(gridView.GetFocusedRowCellValue(columnClientId));
            string lClientName = ConvertEx.ToString(gridView.GetFocusedRowCellValue(columnName));
            ChannelType lChannelType = (ChannelType) ConvertEx.ToInt(gridView.GetFocusedRowCellValue(columnChannelId));
            LoadDetailsTab(true, lClientId, lClientName, lChannelType);
        }

        /// <summary>
        /// Loads the details tab.
        /// </summary>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        /// <param name="clientId">The client id.</param>
        private void LoadDetailsTab(bool isEditMode, int clientId, string clientName, ChannelType channelType) {
            if (ParentTab != null) {
                string lStrTabName = GetTabName(isEditMode, clientName);
                CommonBaseExternalData lMainData = GetMainData(isEditMode, clientId, clientName, channelType);
                ParentTab.LoadDetailsTab(isEditMode, lStrTabName, lMainData);
            }
        }

        /// <summary>
        /// Gets the name of the tab.
        /// </summary>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        /// <param name="clientName">Name of the client.</param>
        /// <returns></returns>
        private string GetTabName(bool isEditMode, string clientName) {
            string lStrContractNumber = string.Empty;
            string lStrContractId = Resource.NewDetailsTabName;

            if (isEditMode) {
                lStrContractNumber = ConvertEx.ToString(gridView.GetFocusedRowCellValue(columnNumber));
                lStrContractId = ConvertEx.ToString(gridView.GetFocusedRowCellValue(columnId));
            }

            return string.Format(Resource.TabCaption, clientName, !string.IsNullOrEmpty(lStrContractNumber) ? lStrContractNumber : lStrContractId);
        }

        /// <summary>
        /// Gets the main data.
        /// </summary>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        /// <param name="clientId">The client id.</param>
        /// <param name="clientName">Name of the client.</param>
        /// <returns></returns>
        private CommonBaseExternalData GetMainData(bool isEditMode, int clientId, string clientName, ChannelType channelType) {
            CommonBaseExternalData lMainData = new CommonBaseExternalData();
            lMainData.ClientId = clientId;
            lMainData.ClientName = clientName;
            lMainData.ContractStatus = ContractStatus.New;
            lMainData.ContractId = 0;
            lMainData.Channel = ChannelType.Unknown;
            lMainData.IsEditMode = isEditMode;
            lMainData.Channel = channelType;
            if (isEditMode) {
                lMainData.ContractStatus = (ContractStatus) ConvertEx.ToInt(gridView.GetFocusedRowCellValue(columnStatusId));
                lMainData.ContractId = ConvertEx.ToInt(gridView.GetFocusedRowCellValue(columnId));
                lMainData.HasAccessAddresses = ConvertEx.ToBool(DataProvider.GetHasAccessToAddresses(lMainData.ContractId));
            }
            return lMainData;
        }

        /// <summary>
        /// Updates the state of the buttons.
        /// </summary>
        private void UpdateButtonsState() {
            gridView.PostEditor();
            gridView.UpdateCurrentRow();

            barButtonNew.Enabled = MainData.CheckAccessLevel(AccessLevelType.AllowEdit) && !MainData.CheckAccessLevel(AccessLevelType.Unknown);

            barButtonEdit.Enabled = gridView.FocusedRowHandle >= 0;
            barButtonEdit.Caption = MainData.CheckAccessLevel(AccessLevelType.AllowEdit) ? Resource.ButtonEdit : Resource.ButtonView;
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) {
            UpdateButtonsState();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonNew control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            New();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            Edit();
        }

        /// <summary>
        /// Handles the DoubleClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DoubleClick(object sender, EventArgs e) {
            if (gridView.FocusedRowHandle >= 0) {
                Point pt = gridView.GridControl.PointToClient(Control.MousePosition);
                GridHitInfo info = gridView.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell) {
                    Edit();
                }
            }
        }

        /// <summary>
        /// Updates the highlight not signed days.
        /// </summary>
        /// <param name="days"> </param>
        private void UpdateHighlightNotSignedDays(int days) {
            if (gridView.FormatConditions.Count <= 1)
                return;
            
            gridView.BeginUpdate();
            try {
                gridView.FormatConditions[1].Expression = string.Empty;
                if (days > 0) {
                    gridView.FormatConditions[1].Expression = string.Format(
                        //old expression
                        //@"IsNullOrEmpty([{0}]) And (AddDays([{1}], {2}) <= Today()) And [{3}] < {4}",
                        //columnDateSigned.FieldName, columnDateCreation.FieldName, lDaysNotSigned, columnStatusId.FieldName, (int) ContractStatus.SignRequired);
                        @"(AddDays([{0}], {1}) <= Today()) And [{2}] < {3}",
                        columnDateCreation.FieldName, days, columnStatusId.FieldName, (int)ContractStatus.SignRequired);
                }
            }
            finally {
                gridView.EndUpdate();
            }
        }

        /// <summary>
        /// Updates the highlight exp period.
        /// </summary>
        private void UpdateHighlightExpPeriod(DateTime dateFrom, DateTime dateTo) {
            if (gridView.FormatConditions.Count <= 0)
                return;

            repositoryItemDateTo.MinValue = dateFrom;

            string lFieldName = columnDateEnd.FieldName;
            string lHighlightExpression = string.Format(@"[{0}] != ? And [{1}] = {2}", lFieldName, columnStatusId.FieldName, (int) ContractStatus.InWork);

            lHighlightExpression += string.Format(@" And [{0}] >= #{1}#",
                lFieldName, dateFrom.ToString(CultureInfo.CurrentUICulture));
            lHighlightExpression += string.Format(@" And [{0}] <= #{1}#",
                lFieldName, dateTo.ToString(CultureInfo.CurrentUICulture));

            gridView.FormatConditions[0].Expression = lHighlightExpression;
        }

        /// <summary>
        /// Creates default filter expression (available contracts) and sets it GridView.ActiveFilterString property.
        /// </summary>
        private void CreateDefaultGridFilter() {
            StringBuilder lExpressionBuilder = new StringBuilder(Constants.FIELD_CONTRACT_STATUS_ID);
            lExpressionBuilder.AppendFormat(" in ({0}, {1}, {2}, {3}, {4}, {5})",
                (int) ContractStatus.New, (int) ContractStatus.ClientAgreementRequired, (int) ContractStatus.InitialApproveRequired,
                (int) ContractStatus.FinalApproveRequired, (int) ContractStatus.SignRequired, (int) ContractStatus.InWork);
            String lFilterString = lExpressionBuilder.ToString();
            gridView.ActiveFilterString = lFilterString;
        }

        /// <summary>
        /// Handles Custom sort event for grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView_CustomColumnSort(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e) {
            //Check field name
            if (e.Column.FieldName == Constants.FIELD_CONTRACT_STATUS_ID) {
                if (ConvertEx.ToInt(e.Value1) == (int) ContractStatus.Completed) {
                    //If first compared value has completed status, place it to the end
                    e.Result = 1;
                    e.Handled = true;
                }
                else if (ConvertEx.ToInt(e.Value2) == (int) ContractStatus.Completed) {
                    //If second compared value has completed status, place it to the end
                    e.Result = -1;
                    e.Handled = true;
                }
            }
            else if (e.Column == columnDateCreation && e.Value1 is DateTime && e.Value2 is DateTime) {
                //#25264. УК: "Список контрактов": Некоретно сортується список контрактів по сумі контракту
                // Поскольку пользователю показывается только дата контракта, то сортировать мы должны по дате и без учета времени создания.
                DateTime lDate1 = ((DateTime) e.Value1).Date;
                DateTime lDate2 = ((DateTime) e.Value2).Date;
                e.Result = lDate1 == lDate2 ? 0 : lDate1 > lDate2 ? 1 : -1;
                e.Handled = true;
            }
        }

        private void repositoryItemDateFrom_DateTimeChanged(object sender, EventArgs e) {
            DateEdit lDateEdit = sender as DateEdit;
            if (null == lDateEdit)
                return;
            DateTime lDateFrom = lDateEdit.DateTime.Date;
            DateTime lDateTo = barEditDateTo.EditValue is DateTime ? (DateTime) barEditDateTo.EditValue : new DateTime(9999, 12, 31);
            UpdateHighlightExpPeriod(lDateFrom, lDateTo);
        }

        private void repositoryItemDateTo_DateTimeChanged(object sender, EventArgs e) {
            DateEdit lDateEdit = sender as DateEdit;
            if (null == lDateEdit)
                return;
            DateTime lDateTo = lDateEdit.DateTime.Date;
            DateTime lDateFrom = barEditDateFrom.EditValue is DateTime ? (DateTime) barEditDateFrom.EditValue : new DateTime(1, 1, 1);
            UpdateHighlightExpPeriod(lDateFrom, lDateTo);
        }

        private void repositoryItemNotSignedDays_EditValueChanged(object sender, EventArgs e) {
            SpinEdit lEdit = sender as SpinEdit;
            if (lEdit == null)
                return;
            if (lEdit.EditValue == null)
                return;
            int lDays = ConvertEx.ToInt(lEdit.EditValue);
            UpdateHighlightNotSignedDays(lDays);
        }
    }
}