﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Globalization;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Logica.Reports.BaseReportControl.Utility;
using System.ComponentModel;
using DevExpress.XtraGrid.Views.Base;
using Logica.Reports.Common.Enums;

namespace SoftServe.Reports.ContractMgmt.UserControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientListControl : CommonBaseControl
    {
        //
        // Summary:
        //     Occurs when a View is double-clicked.
        [Category("Action")]
        [Description("Occurs when a View is double-clicked.")]
         public event EventHandler RowDoubleClicked;

        //
        // Summary:
        //     Fires in response to changing row focus.
        [Category("Property Changed")]
        [Description("Fires in response to changing row focus.")]
        public event FocusedRowChangedEventHandler FocusedRowChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListControl"/> class.
        /// </summary>
        public ClientListControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads the channel combo.
        /// </summary>
        private void LoadChannelCombo()
        {
            DataTable dataTable = DataProvider.ChannelList;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = Constants.FIELD_CHANNEL_ID;
                dataView.RowFilter = string.Format("{0} IS NOT NULL", Constants.FIELD_CHANNEL_ID);
                DataTable filteredDataTable = dataView.ToTable();
                repositoryItemLookUpChannel.DataSource = filteredDataTable;
                if (filteredDataTable.Rows.Count > 0)
                {
                    barEditChannelCombo.EditValue = ConvertEx.ToInt(filteredDataTable.Rows[0][Constants.FIELD_CHANNEL_ID]);
                }
            }
        }

        /// <summary>
        /// Gets the selected client id.
        /// </summary>
        /// <value>The selected client id.</value>
        public int SelectedClientId
        {
            get
            {
                if (gridView.FocusedRowHandle >= 0)
                {
                    return ConvertEx.ToInt(gridView.GetFocusedRowCellValue(columnId));
                }
                return 0;
            }
        }

        /// <summary>
        /// Gets the type of the selected channel.
        /// </summary>
        /// <value>The type of the selected channel.</value>
        public ChannelType SelectedChannelType
        {
            get
            {
                return (ChannelType)ConvertEx.ToInt(barEditChannelCombo.EditValue);
            }
        }

        /// <summary>
        /// Gets the name of the selected client.
        /// </summary>
        /// <value>The name of the selected client.</value>
        public string SelectedClientName
        {
            get
            {
                if (gridView.FocusedRowHandle >= 0)
                {
                    return ConvertEx.ToString(gridView.GetFocusedRowCellValue(columnLegalName));
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>The data source.</value>
         public DataTable DataSource
        {
            set
            {
                gridControl.DataSource = value;
                UpdateClientsListFilter();
            }
            get
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable)gridControl.DataSource;
            }
        }

        /// <summary>
        /// Handles the Load event of the ClientListControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ClientListControl_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                DateTime dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                barEditDateFrom.EditValue = dateFrom;

                DateTime dateTo = dateFrom.AddDays(DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month) - 1);
                barEditDateTo.EditValue = dateTo;

                LoadChannelCombo();
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditDateFrom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditDateFrom_EditValueChanged(object sender, EventArgs e)
        {
            UpdateRowsHighlightState();
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditDateTo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditDateTo_EditValueChanged(object sender, EventArgs e)
        {
            UpdateRowsHighlightState();
        }

        /// <summary>
        /// Handles the DoubleClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DoubleClick(object sender, EventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0 && RowDoubleClicked != null)
            {
                Point pt = gridView.GridControl.PointToClient(Control.MousePosition);
                GridHitInfo info = gridView.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    RowDoubleClicked(sender, e);
                }
            }
        }

        /// <summary>
        /// Updates the state of the rows highlight.
        /// </summary>
        private void UpdateRowsHighlightState()
        {
            DateTime? dateFrom = barEditDateFrom.EditValue as DateTime?;
            DateTime? dateTo = barEditDateTo.EditValue as DateTime?;
            repositoryItemDateTo.MinValue = (DateTime)dateFrom;

            string highlightExpression = string.Empty;

            if (dateFrom != null || dateTo != null)
            {
                string strFieldName = columnExpirationDate.FieldName;
                highlightExpression = string.Format(@"[{0}] != ?", strFieldName);

                if (dateFrom != null)
                {
                    highlightExpression += string.Format(@" And [{0}] >= #{1}#", 
                        strFieldName, dateFrom.Value.ToString(CultureInfo.CurrentUICulture));
                }
                if (dateTo != null)
                {
                    highlightExpression += string.Format(@" And [{0}] <= #{1}#",
                        strFieldName, dateTo.Value.ToString(CultureInfo.CurrentUICulture));
                }
            }

            if(gridView.FormatConditions.Count > 0)
            {
                gridView.FormatConditions[0].Expression = highlightExpression;
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditChannelCombo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditChannelCombo_EditValueChanged(object sender, EventArgs e)
        {
            UpdateClientsListFilter();
        }

        /// <summary>
        /// Updates the clients list filter.
        /// </summary>
        private void UpdateClientsListFilter()
        {
            DataView dataView = gridView.DataSource as DataView;
            if (dataView != null)
            {
                int channelId = ConvertEx.ToInt(barEditChannelCombo.EditValue);
                if (channelId > 0)
                {
                    dataView.RowFilter = string.Format("{0} LIKE '*{1}*'", columnChannel.FieldName, channelId);
                }
                else
                {
                    dataView.RowFilter = string.Empty;
                }
            }
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (FocusedRowChanged != null)
            {
                FocusedRowChanged(sender, e);
            }
        }
    }
}
