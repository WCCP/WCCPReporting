﻿using System;
using System.Collections.Generic;
using System.Data;
using DevExpress.XtraTreeList;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// </summary>
    public partial class ContractDetailsConditions : CommonBaseControl, ITwoStepSavingControl {
        #region Fields

        private readonly List<string> _modifiedIds = new List<string>();
        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "ContractDetailsConditions" /> class.
        /// </summary>
        public ContractDetailsConditions() {
            IsSecondStepRequired = false;
            InitializeComponent();
            IsDataLoaded = false;
        }

        #endregion

        #region Instance Properties

        /// <summary>
        ///   Gets or sets a value indicating whether this instance is allow add sku.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is allow add sku; otherwise, <c>false</c>.
        /// </value>
        public bool IsAllowAddSku {
            get { return checkAllowAddSku.Checked; }
            set {
                treeConditionsSku.IsAllowAddSku = value;
                if (checkAllowAddSku.Checked == value)
                    return;

                checkAllowAddSku.Checked = value;
                OnChanged(false);
            }
        }

        /// <summary>
        /// Returns TreeList control from used in user defined class SKUTreeConditions
        /// </summary>
        public TreeList TreeList
        {
            get { return treeConditionsSku.TreeList; }
        }

        /// <summary>
        ///   Sets all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable AllFieldsData {
            set {
                DataTable lDataTableFields = value;
                if (lDataTableFields == null || lDataTableFields.Rows.Count != 1)
                    return;

                DataRow lRow = lDataTableFields.Rows[0];
                IsAllowAddSku = ConvertEx.ToBool(lRow[Constants.FIELD_CONDITION_ALLOW_ADD_SKU]);
            }
        }

        #endregion

        #region Instance Methods

        /// <summary>
        /// Customizes export
        /// </summary>
        public void PrepareToExport()
        {
            TreeList.OptionsPrint.AutoWidth = false;
            TreeList.OptionsPrint.PrintAllNodes = true;
            TreeList.OptionsPrint.PrintImages = false;
            TreeList.OptionsPrint.PrintTree = false;
            TreeList.OptionsPrint.PrintTreeButtons = false;
        }

        /// <summary>
        /// Sets all print options to default
        /// </summary>
        public void RestoreAfterExport()
        {
            TreeList.OptionsPrint.Reset();
        }

        /// <summary>
        ///   Loads the data.
        /// </summary>
        /// <param name = "contractId">The contract id.</param>
        public override void LoadData() {
            AttachEvents(false);
            AllFieldsData = DataProvider.GetConditionsFields(MainData.ContractId);
            LoadConditionsBySKU();
            base.LoadData();
            AttachEvents(true);
            if (treeConditionsSku.IsChanged)
                OnChanged(true);
        }

        /// <summary>
        ///   Saves this instance.
        /// </summary>
        public override int SaveData() {
            DataProvider.SetConditionsBySKU(MainData.ContractId, treeConditionsSku.SelectedConditionsXml);
            DataProvider.SetConditionsFields(MainData.ContractId, IsAllowAddSku);
            treeConditionsSku.IsChanged = false;
            return MainData.ContractId;
        }

        /// <summary>
        ///   Attaches the events.
        /// </summary>
        /// <param name = "attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach) {
                treeConditionsSku.EditValueChanged += OnEditValueChanged;
                treeConditionsSku.SKUStructureChanged += OnSKUStructureChanged;
                checkAllowAddSku.EditValueChanged += OnCheckEditValueChanged;
            }
            else {
                checkAllowAddSku.EditValueChanged -= OnCheckEditValueChanged;
                treeConditionsSku.EditValueChanged -= OnEditValueChanged;
                treeConditionsSku.SKUStructureChanged -= OnSKUStructureChanged;
            }
        }

        /// <summary>
        ///   Loads the conditions by SKU.
        /// </summary>
        private void LoadConditionsBySKU() {
            treeConditionsSku.Load(MainData.ContractId);
            if (treeConditionsSku.IsChanged) {
                DataProvider.SetConditionsBySKU(MainData.ContractId, treeConditionsSku.SelectedConditionsXml);
                treeConditionsSku.IsChanged = false;
            }
        }

        #endregion

        #region Event Handling

        private void OnCheckEditValueChanged(object sender, EventArgs e) {
            OnChanged(true);
        }

        /// <summary>
        ///   Handles the EditValueChanged event of the treeConditionsSku control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void OnEditValueChanged(object sender, CellValueChangedEventArgs e) {
            string lId = e.Node[treeConditionsSku.IdColumn].ToString();
            if (!_modifiedIds.Contains(lId)) {
                _modifiedIds.Add(lId);
                IsSecondStepRequired = _modifiedIds.Count > 0;
            }
            OnChanged(true);
        }

        private void OnSKUStructureChanged(object sender, CellValueChangedEventArgs e) {
            string lId = e.Node[treeConditionsSku.IdColumn].ToString();
            if (!e.Node.Checked && _modifiedIds.Contains(lId)) {
                _modifiedIds.Remove(lId);
                IsSecondStepRequired = _modifiedIds.Count > 0;
            }
            OnChanged(true);
            //IsCalcVRequired = true;
        }

        #endregion

        #region ITwoStepSavingControl Members

        public bool IsSecondStepRequired { get; set; }

        public void SecondStageSave() {
            SaveData();
        }

        #endregion

        public void UpdatePlanVolume(PlanVolumeChangedEventArgs args) {
            treeConditionsSku.UpdatePlanVolume(args);
        }
    }
}