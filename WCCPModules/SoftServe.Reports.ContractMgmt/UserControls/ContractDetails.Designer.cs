﻿
namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ContractDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractDetailsControl));
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barMain = new DevExpress.XtraBars.Bar();
            this.barButtonSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRemove = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonContractStatus = new DevExpress.XtraBars.BarSubItem();
            this.barButtonAddCostItem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonStorcheck = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonPlanMACOCalc = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRefreshText = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRefreshImg = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemComboChannel = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboChannel)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.AppearancePage.Header.Options.UseTextOptions = true;
            this.tabControl.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl.Location = new System.Drawing.Point(0, 34);
            this.tabControl.Name = "tabControl";
            this.tabControl.ShowHeaderFocus = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl.Size = new System.Drawing.Size(1056, 340);
            this.tabControl.TabIndex = 4;
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            this.tabControl.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.tabControl_SelectedPageChanging);
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMain});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonRemove,
            this.barButtonRefreshText,
            this.barButtonRefreshImg,
            this.barButtonSave,
            this.barButtonAddCostItem,
            this.barButtonContractStatus,
            this.barButtonStorcheck,
            this.barButtonPlanMACOCalc});
            this.barManager.MaxItemId = 31;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboChannel});
            // 
            // barMain
            // 
            this.barMain.BarName = "Main";
            this.barMain.DockCol = 0;
            this.barMain.DockRow = 0;
            this.barMain.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonRemove, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonContractStatus, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonAddCostItem, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonStorcheck, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonPlanMACOCalc, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonRefreshText),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonRefreshImg)});
            this.barMain.OptionsBar.AllowQuickCustomization = false;
            this.barMain.OptionsBar.DisableClose = true;
            this.barMain.OptionsBar.DisableCustomization = true;
            this.barMain.OptionsBar.DrawDragBorder = false;
            this.barMain.OptionsBar.UseWholeRow = true;
            this.barMain.Text = "Main";
            // 
            // barButtonSave
            // 
            this.barButtonSave.Caption = "Сохранить закладку";
            this.barButtonSave.Description = "Сохранить изменения";
            this.barButtonSave.Hint = "Пересчитать и сохранить все данные";
            this.barButtonSave.Id = 7;
            this.barButtonSave.ImageIndex = 4;
            this.barButtonSave.Name = "barButtonSave";
            this.barButtonSave.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonSave_ItemClick);
            // 
            // barButtonRemove
            // 
            this.barButtonRemove.Caption = "Удалить контракт";
            this.barButtonRemove.Hint = "Удалить контракт";
            this.barButtonRemove.Id = 4;
            this.barButtonRemove.ImageIndex = 2;
            this.barButtonRemove.Name = "barButtonRemove";
            this.barButtonRemove.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonRemove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonRemove_ItemClick);
            // 
            // barButtonContractStatus
            // 
            this.barButtonContractStatus.Caption = "Текущий статус";
            this.barButtonContractStatus.Hint = "Пересчитать, сохранить данные и установить статус контракта";
            this.barButtonContractStatus.Id = 28;
            this.barButtonContractStatus.ImageIndex = 5;
            this.barButtonContractStatus.Name = "barButtonContractStatus";
            // 
            // barButtonAddCostItem
            // 
            this.barButtonAddCostItem.Caption = "Добавить статью";
            this.barButtonAddCostItem.Hint = "Добавление статьи затрат в список статей затрат";
            this.barButtonAddCostItem.Id = 23;
            this.barButtonAddCostItem.ImageIndex = 1;
            this.barButtonAddCostItem.Name = "barButtonAddCostItem";
            this.barButtonAddCostItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonAddCostItem_ItemClick);
            // 
            // barButtonStorcheck
            // 
            this.barButtonStorcheck.Caption = "Сторчек";
            this.barButtonStorcheck.Hint = "Добавить сторчек";
            this.barButtonStorcheck.Id = 29;
            this.barButtonStorcheck.ImageIndex = 6;
            this.barButtonStorcheck.Name = "barButtonStorcheck";
            // 
            // barButtonPlanMACOCalc
            // 
            this.barButtonPlanMACOCalc.Caption = "Пересчитать МАСО";
            this.barButtonPlanMACOCalc.Hint = "Пересчитать бюджетное МАСО";
            this.barButtonPlanMACOCalc.Id = 30;
            this.barButtonPlanMACOCalc.Name = "barButtonPlanMACOCalc";
            this.barButtonPlanMACOCalc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonPlanMACOCalc_ItemClick);
            // 
            // barButtonRefreshText
            // 
            this.barButtonRefreshText.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonRefreshText.Caption = "Для отмены изменений нажать кнопку \"Обновить\"";
            this.barButtonRefreshText.Enabled = false;
            this.barButtonRefreshText.Id = 5;
            this.barButtonRefreshText.Name = "barButtonRefreshText";
            this.barButtonRefreshText.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonRefreshImg
            // 
            this.barButtonRefreshImg.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonRefreshImg.Enabled = false;
            this.barButtonRefreshImg.Id = 6;
            this.barButtonRefreshImg.ImageIndex = 3;
            this.barButtonRefreshImg.Name = "barButtonRefreshImg";
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "edit.png");
            this.imCollection.Images.SetKeyName(1, "add.png");
            this.imCollection.Images.SetKeyName(2, "delete.png");
            this.imCollection.Images.SetKeyName(3, "refresh_24.png");
            this.imCollection.Images.SetKeyName(4, "save.png");
            this.imCollection.Images.SetKeyName(5, "next_state.png");
            this.imCollection.Images.SetKeyName(6, "storcheck.png");
            // 
            // repositoryItemComboChannel
            // 
            this.repositoryItemComboChannel.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemComboChannel.AutoHeight = false;
            this.repositoryItemComboChannel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboChannel.Name = "repositoryItemComboChannel";
            this.repositoryItemComboChannel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // ContractDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ContractDetailsControl";
            this.Size = new System.Drawing.Size(1056, 374);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboChannel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar barMain;
        private DevExpress.XtraBars.BarButtonItem barButtonRemove;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarButtonItem barButtonRefreshText;
        private DevExpress.XtraBars.BarButtonItem barButtonRefreshImg;
        private DevExpress.XtraBars.BarButtonItem barButtonSave;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboChannel;
        private DevExpress.XtraBars.BarButtonItem barButtonAddCostItem;
        private DevExpress.XtraBars.BarSubItem barButtonContractStatus;
        private DevExpress.XtraBars.BarButtonItem barButtonStorcheck;
        private DevExpress.XtraBars.BarButtonItem barButtonPlanMACOCalc;


    }
}
