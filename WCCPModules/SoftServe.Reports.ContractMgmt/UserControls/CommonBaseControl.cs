﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl.CommonControls.ExpensesTree;
using Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid;
using Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.DataAccess;
using Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions;
using Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions.DataAccess;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.Enums;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// 
    /// </summary>
    public class CommonBaseControl : UserControl {

        #region Fields

        private readonly DXErrorProvider _errorProvider = new DXErrorProvider();
        private bool _isChanged;
        private bool _isCalcVRequired;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        public CommonBaseControl() : this(null) {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public CommonBaseControl(CommonBaseTab parentTab) {
            _isCalcVRequired = false;
            MainData = new CommonBaseExternalData();
            _errorProvider.ContainerControl = this;
            ParentTab = parentTab;
            IsDataLoaded = false;
        }

        #endregion

        #region Properties

        public event EventHandler UpdateMainView;

        public CommonBaseTab ParentTab { get; set; }
        internal CommonBaseExternalData MainData { get; set; }
        internal ContractDetailsControl ContractDetailsControl { get; set; }

        public bool IsDataLoaded { get; set; }

        public virtual bool IsCalcVRequired {
            get { return _isCalcVRequired; }
            set { _isCalcVRequired = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is external data refresh required.
        /// 	<c>true</c> if this instance is external data refresh required; otherwise, <c>false</c>.
        /// </summary>
        internal bool IsExternalRefreshRequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is changed.
        /// 	<c>true</c> if this instance is changed; otherwise, <c>false</c>.
        /// </summary>
        internal bool IsChanged {
            get { return _isChanged; }
            set {
                _isChanged = value;
                if (ParentTab == null)
                    return;

                if (_isChanged && !ParentTab.Text.EndsWith(Constants.TAB_POSTFIX_ASTERISK))
                    ParentTab.Text += Constants.TAB_POSTFIX_ASTERISK;
                else if (!_isChanged)
                    ParentTab.Text = ParentTab.Text.TrimEnd(Constants.TAB_POSTFIX_ASTERISK.ToCharArray());
                FireUpdateMainView();
            }
        }

        #endregion

        /// <summary>
        /// Loads this instance.
        /// </summary>
        public virtual void LoadData() {
            ResetChangedFlag();
            ClearErrors();
            UpdateReadOnlyMode();
            IsDataLoaded = true;
        }

        /// <summary>
        /// Validates the data.
        /// </summary>
        /// <returns></returns>
        public virtual ValidateMetadata ValidateData() {
            _errorProvider.ClearErrors();
            ValidateMetadata lResult = new ValidateMetadata();
            lResult.IsCorrect = ValidateChildren(ValidationConstraints.Selectable);
            return lResult;
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <returns></returns>
        public virtual int SaveData() {
            return 0;
        }

        /// <summary>
        /// Updates the read only mode.
        /// </summary>
        public virtual void UpdateReadOnlyMode() {
            if (!MainData.AllowEditContract)
                SetReadOnlyModeRecursively(this);
        }

        /// <summary>
        /// Sets the read only mode recursively.
        /// </summary>
        /// <param name="currentControl">The current control.</param>
        protected void SetReadOnlyModeRecursively(Control currentControl) {
            if (currentControl == null)
                return;

            foreach (Control lControl in currentControl.Controls) {
                if (lControl == null)
                    continue;

                if (lControl is ExpensesTree) {
                    ExpensesTree lTree = lControl as ExpensesTree;
                    lTree.ReadOnly = true;
                }
                else if (lControl is SKUTreeConditions) {
                    SKUTreeConditions lTree = lControl as SKUTreeConditions;
                    if (MainData.ContractStatus == ContractStatus.InWork && MainData.CheckAccessLevel(AccessLevelType.AllowEdit))
                        lTree.EditMode = SKUTreeConditionsEditMode.AllowCheckSkuOnly;
                    else
                        lTree.EditMode = SKUTreeConditionsEditMode.ReadOnly;
                }
                else if (lControl is IndicatorsGrid) {
                    IndicatorsGrid lGrid = lControl as IndicatorsGrid;
                    if (MainData.ContractStatus == ContractStatus.InWork && MainData.CheckAccessLevel(AccessLevelType.AllowEdit))
                        lGrid.EditMode = IndicatorEditMode.PlanReadOnly;
                    else
                        lGrid.EditMode = IndicatorEditMode.ReadOnly;
                }
                else if (lControl is ManageAddressesList) {
                    ManageAddressesList lAddressControl = lControl as ManageAddressesList;
                    if (MainData.ContractStatus == ContractStatus.InWork && MainData.CheckAccessLevel(AccessLevelType.AllowSignedContractAddressesEdit))
                        lAddressControl.EditMode = AddressEditMode.AllowAddOnly;
                    else
                        lAddressControl.EditMode = AddressEditMode.ReadOnly;
                }
                else if (lControl is IControlReadOnly)
                    (lControl as IControlReadOnly).ReadOnly = true;
                else if (lControl is DateEdit)
                    lControl.Enabled = false;
                else if (lControl is BaseEdit) {
                    (lControl as BaseEdit).Properties.AllowFocused = false;
                    (lControl as BaseEdit).Properties.ReadOnly = true;
                }
                else if (lControl is GridControl) {
                    GridControl lGridControl = lControl as GridControl;
                    foreach (GridView lView in lGridControl.Views)
                        lView.OptionsBehavior.Editable = false;
                }
                else if (lControl is TreeList) {
                    TreeList lTreeList = lControl as TreeList;
                    lTreeList.OptionsBehavior.Editable = false;
                }
                else if (lControl.HasChildren)
                    SetReadOnlyModeRecursively(lControl);
            }
        }

        /// <summary>
        /// Called when [changed].
        /// </summary>
        /// <param name="externalRefreshRequired">if set to <c>true</c> [external refresh required].</param>
        public void OnChanged(bool externalRefreshRequired) {
            IsChanged = true;
            IsExternalRefreshRequired = IsExternalRefreshRequired ? true : externalRefreshRequired;
        }

        /// <summary>
        /// Resets the change flag.
        /// </summary>
        internal void ResetChangedFlag() {
            IsChanged = false;
            IsExternalRefreshRequired = false;
            IsCalcVRequired = false;
            if (this is ITwoStepSavingControl)
                ((ITwoStepSavingControl) this).IsSecondStepRequired = false;
        }

        /// <summary>
        /// Sets the error.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="errorText">The error text.</param>
        internal void SetError(Control control, string errorText) {
            if (_errorProvider != null && control != null)
                _errorProvider.SetError(control, errorText);
        }

        /// <summary>
        /// Clears the errors.
        /// </summary>
        internal void ClearErrors() {
            if (_errorProvider != null)
                _errorProvider.ClearErrors();
        }

        /// <summary>
        /// Fires the update main view.
        /// </summary>
        protected void FireUpdateMainView() {
            if (UpdateMainView != null)
                UpdateMainView(this, new EventArgs());
        }
    }
}