﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ContractDetailsAddresses
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.columnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.manageAddressesList = new Logica.Reports.BaseReportControl.CommonControls.ManageAddressesList();
            this.gridControlAddresses = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnLegalPOCName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFactAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChannelId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOLId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSpave = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOLWHSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnVInBasePeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnAvgVal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOlChanel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPrem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInBevPart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractInBev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryActivity = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumnContractBBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractObolon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.scrollableArea = new DevExpress.XtraEditors.XtraScrollableControl();
            this.repositoryCommentEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryCommentTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemLookUpStatus)).BeginInit();
            this.manageAddressesList.WorkingArea.SuspendLayout();
            this.manageAddressesList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.gridControlAddresses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryActivity)).BeginInit();
            this.scrollableArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryCommentEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryCommentTextEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // columnStatus
            // 
            this.columnStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnStatus.Caption = "Статус ТТ в адреске";
            this.columnStatus.ColumnEdit = this.repositoryItemLookUpStatus;
            this.columnStatus.FieldName = "STATUS";
            this.columnStatus.Name = "columnStatus";
            this.columnStatus.OptionsColumn.AllowMove = false;
            this.columnStatus.OptionsColumn.ReadOnly = true;
            this.columnStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnStatus.Visible = true;
            this.columnStatus.VisibleIndex = 4;
            this.columnStatus.Width = 120;
            // 
            // repositoryItemLookUpStatus
            // 
            this.repositoryItemLookUpStatus.AutoHeight = false;
            this.repositoryItemLookUpStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpStatus.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Статус")});
            this.repositoryItemLookUpStatus.DisplayMember = "Name";
            this.repositoryItemLookUpStatus.Name = "repositoryItemLookUpStatus";
            this.repositoryItemLookUpStatus.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.repositoryItemLookUpStatus.ValueMember = "ID";
            // 
            // manageAddressesList
            // 
            this.manageAddressesList.AddPOCCommonOptionsVisible = false;
            this.manageAddressesList.ChanelIdFieldName = null;
            this.manageAddressesList.ChanelsDdlAllowsNull = false;
            this.manageAddressesList.ChanelsDdlVisible = false;
            this.manageAddressesList.ChannelId = null;
            this.manageAddressesList.CustomAddPOCOptionCaption = "Выбором";
            this.manageAddressesList.CustomAddPOCOptionVisible = true;
            this.manageAddressesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manageAddressesList.EditMode = Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess.AddressEditMode.AllowAll;
            this.manageAddressesList.FldNameStatus = "STATUS";
            this.manageAddressesList.FrozenColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.manageAddressesList.InBevVolumeFieldName = null;
            this.manageAddressesList.InsertButtonText = null;
            this.manageAddressesList.InvisibleColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.manageAddressesList.Location = new System.Drawing.Point(0, 0);
            this.manageAddressesList.Name = "manageAddressesList";
            this.manageAddressesList.Size = new System.Drawing.Size(1187, 566);
            this.manageAddressesList.StatisticsText = "Кол-во ТТ 123, Общая доля InBev 45%, Средний V на ТТ до активности 345 дал";
            this.manageAddressesList.SumColumnOlId = "OL_ID";
            this.manageAddressesList.TabIndex = 0;
            this.manageAddressesList.TargetGrid = this.gridControlAddresses;
            // 
            // manageAddressesList.WorkingArea
            // 
            this.manageAddressesList.WorkingArea.Controls.Add(this.gridControlAddresses);
            this.manageAddressesList.WorkingArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manageAddressesList.WorkingArea.Location = new System.Drawing.Point(0, 96);
            this.manageAddressesList.WorkingArea.Name = "WorkingArea";
            this.manageAddressesList.WorkingArea.Size = new System.Drawing.Size(1187, 470);
            this.manageAddressesList.WorkingArea.TabIndex = 5;
            this.manageAddressesList.OnFilterChanged += new Logica.Reports.BaseReportControl.CommonControls.ManageAddressesList.FilterChanged(this.manageAddressesList_OnFilterChanged);
            this.manageAddressesList.DeleteBtnClick += new System.EventHandler(this.manageAddressesList_DeleteBtnClick);
            this.manageAddressesList.CustomFilterShowing += new Logica.Reports.BaseReportControl.CommonControls.ManageAddressesList.CustomFilterShowingHandler(this.manageAddressesList_CustomFilterShowing);
            this.manageAddressesList.Validating += new System.ComponentModel.CancelEventHandler(this.manageAddressesList_Validating);
            // 
            // gridControlAddresses
            // 
            this.gridControlAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAddresses.Location = new System.Drawing.Point(2, 2);
            this.gridControlAddresses.MainView = this.gridView;
            this.gridControlAddresses.Name = "gridControlAddresses";
            this.gridControlAddresses.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryActivity,
            this.repositoryCommentEdit,
            this.repositoryCommentTextEdit});
            this.gridControlAddresses.Size = new System.Drawing.Size(1183, 466);
            this.gridControlAddresses.TabIndex = 5;
            this.gridControlAddresses.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            this.gridControlAddresses.DataSourceChanged += new System.EventHandler(this.gridView_DataSourceChanged);
            this.gridControlAddresses.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlAddresses_KeyDown);
            this.gridControlAddresses.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlAddresses_KeyDown);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnRegionName,
            this.columnM3,
            this.columnM2,
            this.columnM1,
            this.columnLegalPOCName,
            this.columnPhone,
            this.columnEmail,
            this.columnFactName,
            this.columnPOCCode,
            this.columnStatus,
            this.columnComment,
            this.columnCity,
            this.columnFactAddress,
            this.columnChannel,
            this.columnChannelId,
            this.columnOLId,
            this.gridColumnSpave,
            this.gridColumnOLWHSize,
            this.gridColumnVInBasePeriod,
            this.gridColumnAvgVal,
            this.gridColumnOlChanel,
            this.gridColumnPrem,
            this.gridColumnInBevPart,
            this.gridColumnContractInBev,
            this.gridColumnContractBBH,
            this.gridColumnContractObolon,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn12,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (255)))), ((int) (((byte) (192)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (255)))), ((int) (((byte) (192)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.columnStatus;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 1;
            styleFormatCondition2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Strikeout);
            styleFormatCondition2.Appearance.Options.UseFont = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.columnStatus;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 9;
            styleFormatCondition3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))), ((int) (((byte) (255)))), ((int) (((byte) (128)))));
            styleFormatCondition3.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int) (((byte) (255)))), ((int) (((byte) (255)))), ((int) (((byte) (128)))));
            styleFormatCondition3.Appearance.Options.UseBackColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.columnStatus;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = "-1";
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2,
            styleFormatCondition3});
            this.gridView.GridControl = this.gridControlAddresses;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.gridView.OptionsFilter.MaxCheckedListItemCount = 1000;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.ReadOnly = true;
            // 
            // columnRegionName
            // 
            this.columnRegionName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnRegionName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnRegionName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnRegionName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnRegionName.Caption = "Регион";
            this.columnRegionName.FieldName = "REGION_NAME";
            this.columnRegionName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnRegionName.Name = "columnRegionName";
            this.columnRegionName.OptionsColumn.AllowMove = false;
            this.columnRegionName.OptionsColumn.ReadOnly = true;
            this.columnRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnRegionName.Visible = true;
            this.columnRegionName.VisibleIndex = 0;
            this.columnRegionName.Width = 110;
            // 
            // columnM3
            // 
            this.columnM3.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM3.Caption = "М3";
            this.columnM3.FieldName = "M3_NAME";
            this.columnM3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnM3.Name = "columnM3";
            this.columnM3.OptionsColumn.AllowMove = false;
            this.columnM3.OptionsColumn.ReadOnly = true;
            this.columnM3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnM3.Visible = true;
            this.columnM3.VisibleIndex = 1;
            this.columnM3.Width = 110;
            // 
            // columnM2
            // 
            this.columnM2.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM2.Caption = "М2";
            this.columnM2.FieldName = "M2_NAME";
            this.columnM2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnM2.Name = "columnM2";
            this.columnM2.OptionsColumn.AllowMove = false;
            this.columnM2.OptionsColumn.ReadOnly = true;
            this.columnM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnM2.Visible = true;
            this.columnM2.VisibleIndex = 2;
            this.columnM2.Width = 110;
            // 
            // columnM1
            // 
            this.columnM1.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM1.Caption = "М1";
            this.columnM1.FieldName = "M1_NAME";
            this.columnM1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnM1.Name = "columnM1";
            this.columnM1.OptionsColumn.AllowMove = false;
            this.columnM1.OptionsColumn.ReadOnly = true;
            this.columnM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnM1.Visible = true;
            this.columnM1.VisibleIndex = 3;
            this.columnM1.Width = 110;
            // 
            // columnLegalPOCName
            // 
            this.columnLegalPOCName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnLegalPOCName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnLegalPOCName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnLegalPOCName.Caption = "Юр. имя ТТ";
            this.columnLegalPOCName.FieldName = "LEGAL_NAME";
            this.columnLegalPOCName.Name = "columnLegalPOCName";
            this.columnLegalPOCName.OptionsColumn.AllowMove = false;
            this.columnLegalPOCName.OptionsColumn.ReadOnly = true;
            this.columnLegalPOCName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnLegalPOCName.Visible = true;
            this.columnLegalPOCName.VisibleIndex = 9;
            this.columnLegalPOCName.Width = 110;
            // 
            // columnPhone
            // 
            this.columnPhone.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPhone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPhone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "OLTELEPHONE";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowMove = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 8;
            this.columnPhone.Width = 110;
            // 
            // columnEmail
            // 
            this.columnEmail.AppearanceHeader.Options.UseTextOptions = true;
            this.columnEmail.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnEmail.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnEmail.Caption = "Email";
            this.columnEmail.FieldName = "OLEMAIL";
            this.columnEmail.Name = "columnEmail";
            this.columnEmail.OptionsColumn.AllowMove = false;
            this.columnEmail.OptionsColumn.ReadOnly = true;
            this.columnEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnEmail.Visible = true;
            this.columnEmail.VisibleIndex = 7;
            this.columnEmail.Width = 110;
            // 
            // columnFactName
            // 
            this.columnFactName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFactName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFactName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFactName.Caption = "Факт. имя ТТ";
            this.columnFactName.FieldName = "TRADING_NAME";
            this.columnFactName.Name = "columnFactName";
            this.columnFactName.OptionsColumn.AllowMove = false;
            this.columnFactName.OptionsColumn.ReadOnly = true;
            this.columnFactName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnFactName.Visible = true;
            this.columnFactName.VisibleIndex = 6;
            this.columnFactName.Width = 110;
            // 
            // columnPOCCode
            // 
            this.columnPOCCode.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPOCCode.Caption = "Код ТТ";
            this.columnPOCCode.FieldName = "OUTLET_CODE";
            this.columnPOCCode.Name = "columnPOCCode";
            this.columnPOCCode.OptionsColumn.AllowMove = false;
            this.columnPOCCode.OptionsColumn.ReadOnly = true;
            this.columnPOCCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPOCCode.Visible = true;
            this.columnPOCCode.VisibleIndex = 5;
            this.columnPOCCode.Width = 110;
            // 
            // columnComment
            // 
            this.columnComment.AppearanceHeader.Options.UseTextOptions = true;
            this.columnComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnComment.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnComment.Caption = "Комментарии";
            this.columnComment.ColumnEdit = this.repositoryCommentEdit;
            this.columnComment.FieldName = "COMMENT";
            this.columnComment.Name = "columnComment";
            this.columnComment.OptionsColumn.AllowMove = false;
            this.columnComment.Visible = true;
            this.columnComment.VisibleIndex = 10;
            this.columnComment.Width = 100;
            // 
            // columnCity
            // 
            this.columnCity.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCity.Caption = "Нас. пункт";
            this.columnCity.FieldName = "CITY";
            this.columnCity.MinWidth = 100;
            this.columnCity.Name = "columnCity";
            this.columnCity.OptionsColumn.AllowMove = false;
            this.columnCity.OptionsColumn.ReadOnly = true;
            this.columnCity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCity.Width = 100;
            // 
            // columnFactAddress
            // 
            this.columnFactAddress.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFactAddress.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFactAddress.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFactAddress.Caption = "Факт. адрес";
            this.columnFactAddress.FieldName = "DELIVERY_ADDRESS";
            this.columnFactAddress.MinWidth = 100;
            this.columnFactAddress.Name = "columnFactAddress";
            this.columnFactAddress.OptionsColumn.AllowMove = false;
            this.columnFactAddress.OptionsColumn.ReadOnly = true;
            this.columnFactAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnFactAddress.Width = 100;
            // 
            // columnChannel
            // 
            this.columnChannel.AppearanceHeader.Options.UseTextOptions = true;
            this.columnChannel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnChannel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnChannel.Caption = "Канал персонала";
            this.columnChannel.FieldName = "TRADE_CHANNEL";
            this.columnChannel.MinWidth = 50;
            this.columnChannel.Name = "columnChannel";
            this.columnChannel.OptionsColumn.AllowMove = false;
            this.columnChannel.OptionsColumn.ReadOnly = true;
            this.columnChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // columnChannelId
            // 
            this.columnChannelId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnChannelId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnChannelId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnChannelId.Caption = "Канал";
            this.columnChannelId.FieldName = "TRADE_CHANNEL";
            this.columnChannelId.MinWidth = 50;
            this.columnChannelId.Name = "columnChannelId";
            this.columnChannelId.OptionsColumn.AllowMove = false;
            this.columnChannelId.OptionsColumn.ReadOnly = true;
            this.columnChannelId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // columnOLId
            // 
            this.columnOLId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnOLId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnOLId.Caption = "OL_ID";
            this.columnOLId.FieldName = "OL_ID";
            this.columnOLId.Name = "columnOLId";
            this.columnOLId.OptionsColumn.AllowMove = false;
            this.columnOLId.OptionsColumn.ReadOnly = true;
            this.columnOLId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnSpave
            // 
            this.gridColumnSpave.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnSpave.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnSpave.Caption = "Площадь ТТ";
            this.gridColumnSpave.FieldName = "OLSize";
            this.gridColumnSpave.Name = "gridColumnSpave";
            this.gridColumnSpave.OptionsColumn.AllowMove = false;
            this.gridColumnSpave.OptionsColumn.ReadOnly = true;
            this.gridColumnSpave.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnOLWHSize
            // 
            this.gridColumnOLWHSize.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOLWHSize.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOLWHSize.Caption = "Площадь склада";
            this.gridColumnOLWHSize.FieldName = "OLWHSize";
            this.gridColumnOLWHSize.Name = "gridColumnOLWHSize";
            this.gridColumnOLWHSize.OptionsColumn.AllowMove = false;
            this.gridColumnOLWHSize.OptionsColumn.ReadOnly = true;
            this.gridColumnOLWHSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnVInBasePeriod
            // 
            this.gridColumnVInBasePeriod.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnVInBasePeriod.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnVInBasePeriod.Caption = "V в базовом периоде дал";
            this.gridColumnVInBasePeriod.FieldName = "TOTAL_VOLUME";
            this.gridColumnVInBasePeriod.Name = "gridColumnVInBasePeriod";
            this.gridColumnVInBasePeriod.OptionsColumn.AllowMove = false;
            this.gridColumnVInBasePeriod.OptionsColumn.ReadOnly = true;
            this.gridColumnVInBasePeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnAvgVal
            // 
            this.gridColumnAvgVal.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnAvgVal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnAvgVal.Caption = "Средний V на ТТ дал до контракта";
            this.gridColumnAvgVal.FieldName = "AVERAGE_VOLUME";
            this.gridColumnAvgVal.Name = "gridColumnAvgVal";
            this.gridColumnAvgVal.OptionsColumn.AllowMove = false;
            this.gridColumnAvgVal.OptionsColumn.ReadOnly = true;
            this.gridColumnAvgVal.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnOlChanel
            // 
            this.gridColumnOlChanel.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOlChanel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOlChanel.Caption = "Канал ТТ";
            this.gridColumnOlChanel.FieldName = "OL_CHANNEL";
            this.gridColumnOlChanel.Name = "gridColumnOlChanel";
            this.gridColumnOlChanel.OptionsColumn.AllowMove = false;
            this.gridColumnOlChanel.OptionsColumn.ReadOnly = true;
            this.gridColumnOlChanel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnPrem
            // 
            this.gridColumnPrem.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnPrem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnPrem.Caption = "Премиальность ТТ";
            this.gridColumnPrem.FieldName = "PREMIALITY";
            this.gridColumnPrem.Name = "gridColumnPrem";
            this.gridColumnPrem.OptionsColumn.AllowMove = false;
            this.gridColumnPrem.OptionsColumn.ReadOnly = true;
            this.gridColumnPrem.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnInBevPart
            // 
            this.gridColumnInBevPart.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnInBevPart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnInBevPart.Caption = "Доля InBev %";
            this.gridColumnInBevPart.DisplayFormat.FormatString = "P";
            this.gridColumnInBevPart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnInBevPart.FieldName = "SHAREINBEV";
            this.gridColumnInBevPart.Name = "gridColumnInBevPart";
            this.gridColumnInBevPart.OptionsColumn.AllowMove = false;
            this.gridColumnInBevPart.OptionsColumn.ReadOnly = true;
            this.gridColumnInBevPart.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnContractInBev
            // 
            this.gridColumnContractInBev.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnContractInBev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnContractInBev.Caption = "Контракт InBev";
            this.gridColumnContractInBev.ColumnEdit = this.repositoryActivity;
            this.gridColumnContractInBev.FieldName = "CONTRACT_INBEV";
            this.gridColumnContractInBev.Name = "gridColumnContractInBev";
            this.gridColumnContractInBev.OptionsColumn.AllowMove = false;
            this.gridColumnContractInBev.OptionsColumn.ReadOnly = true;
            this.gridColumnContractInBev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // repositoryActivity
            // 
            this.repositoryActivity.AutoHeight = false;
            this.repositoryActivity.DisplayValueChecked = "1";
            this.repositoryActivity.DisplayValueUnchecked = "0";
            this.repositoryActivity.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryActivity.Name = "repositoryActivity";
            this.repositoryActivity.ValueGrayed = false;
            // 
            // gridColumnContractBBH
            // 
            this.gridColumnContractBBH.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnContractBBH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnContractBBH.Caption = "Контракт BBH";
            this.gridColumnContractBBH.ColumnEdit = this.repositoryActivity;
            this.gridColumnContractBBH.FieldName = "CONTRACT_BBH";
            this.gridColumnContractBBH.Name = "gridColumnContractBBH";
            this.gridColumnContractBBH.OptionsColumn.AllowMove = false;
            this.gridColumnContractBBH.OptionsColumn.ReadOnly = true;
            this.gridColumnContractBBH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumnContractObolon
            // 
            this.gridColumnContractObolon.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnContractObolon.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnContractObolon.Caption = "Контракт Оболонь";
            this.gridColumnContractObolon.ColumnEdit = this.repositoryActivity;
            this.gridColumnContractObolon.FieldName = "CONTRACT_OBOLON";
            this.gridColumnContractObolon.Name = "gridColumnContractObolon";
            this.gridColumnContractObolon.OptionsColumn.AllowMove = false;
            this.gridColumnContractObolon.OptionsColumn.ReadOnly = true;
            this.gridColumnContractObolon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "ХШ 1-дверный InBev";
            this.gridColumn1.FieldName = "ONEDOORFRIDGES_INBEV";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "ХШ 2-дверный InBev";
            this.gridColumn2.FieldName = "TWODOORFRIDGES_INBEV";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "ХШ 1-дверный Оболонь";
            this.gridColumn3.FieldName = "ONEDOORFRIDGES_OBOLON";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "ХШ 2-дверный Оболонь";
            this.gridColumn4.FieldName = "TWODOORFRIDGES_OBOLON";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "ХШ 1-дверный BBH";
            this.gridColumn5.FieldName = "ONEDOORFRIDGES_BBH";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "ХШ 2-дверный BBH";
            this.gridColumn6.FieldName = "TWODOORFRIDGES_BBH";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowMove = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "ХШ 1-дверный Всего";
            this.gridColumn13.FieldName = "ONEDOORFRIDGES";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowMove = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "ХШ 2-дверный Всего";
            this.gridColumn14.FieldName = "TWODOORFRIDGES";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowMove = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "ХШ не пивные";
            this.gridColumn12.FieldName = "NONBEERFRIDGES";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowMove = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "РУ InBev";
            this.gridColumn7.FieldName = "COOLERS_INBEV";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowMove = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "РУ всех конкурентов";
            this.gridColumn8.FieldName = "COOLERS_OTHER";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowMove = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "Статус ТТ в SW";
            this.gridColumn9.FieldName = "SW_STATUS";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowMove = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Промо ТТ в др. активности";
            this.gridColumn10.ColumnEdit = this.repositoryActivity;
            this.gridColumn10.FieldName = "IS_PROMO_MP";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowMove = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Контрольная ТТ в др. активности";
            this.gridColumn11.ColumnEdit = this.repositoryActivity;
            this.gridColumn11.FieldName = "IS_CONTROL_MP";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowMove = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // scrollableArea
            // 
            this.scrollableArea.AlwaysScrollActiveControlIntoView = false;
            this.scrollableArea.AutoScrollMinSize = new System.Drawing.Size(650, 500);
            this.scrollableArea.Controls.Add(this.manageAddressesList);
            this.scrollableArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollableArea.Location = new System.Drawing.Point(0, 0);
            this.scrollableArea.Name = "scrollableArea";
            this.scrollableArea.Size = new System.Drawing.Size(1187, 566);
            this.scrollableArea.TabIndex = 11;
            // 
            // repositoryCommentEdit
            // 
            this.repositoryCommentEdit.Name = "repositoryCommentEdit";
            // 
            // repositoryCommentTextEdit
            // 
            this.repositoryCommentTextEdit.AutoHeight = false;
            this.repositoryCommentTextEdit.Name = "repositoryCommentTextEdit";
            // 
            // ContractDetailsAddresses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.scrollableArea);
            this.Name = "ContractDetailsAddresses";
            this.Size = new System.Drawing.Size(1187, 566);
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemLookUpStatus)).EndInit();
            this.manageAddressesList.WorkingArea.ResumeLayout(false);
            this.manageAddressesList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.gridControlAddresses)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryActivity)).EndInit();
            this.scrollableArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.repositoryCommentEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryCommentTextEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Logica.Reports.BaseReportControl.CommonControls.ManageAddressesList manageAddressesList;
        private DevExpress.XtraGrid.GridControl gridControlAddresses;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn columnM3;
        private DevExpress.XtraGrid.Columns.GridColumn columnM2;
        private DevExpress.XtraGrid.Columns.GridColumn columnM1;
        private DevExpress.XtraGrid.Columns.GridColumn columnLegalPOCName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnEmail;
        private DevExpress.XtraGrid.Columns.GridColumn columnFactName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCCode;
        private DevExpress.XtraGrid.Columns.GridColumn columnCity;
        private DevExpress.XtraGrid.Columns.GridColumn columnFactAddress;
        private DevExpress.XtraGrid.Columns.GridColumn columnChannel;
        private DevExpress.XtraGrid.Columns.GridColumn columnStatus;
        private DevExpress.XtraGrid.Columns.GridColumn columnChannelId;
        private DevExpress.XtraGrid.Columns.GridColumn columnOLId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSpave;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOLWHSize;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnVInBasePeriod;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAvgVal;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOlChanel;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrem;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInBevPart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractInBev;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractBBH;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractObolon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpStatus;
        private Logica.Reports.BaseReportControl.CommonControls.SizeFixator sizeFixator1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryActivity;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.XtraScrollableControl scrollableArea;
        private DevExpress.XtraGrid.Columns.GridColumn columnComment;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryCommentEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentTextEdit;









    }
}
