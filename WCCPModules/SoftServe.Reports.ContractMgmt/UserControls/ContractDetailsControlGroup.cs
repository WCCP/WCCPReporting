﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl.CommonControls.AddressesFilters;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// 
    /// </summary>
    public partial class ContractDetailsControlGroup : CommonBaseControl {
        private const string ActivityColumnPrefix = "AFD_";

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsAddresses"/> class.
        /// </summary>
        public ContractDetailsControlGroup()
            : this(null) {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsAddresses"></see> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public ContractDetailsControlGroup(CommonBaseTab parentTab)
            : base(parentTab) {
            InitializeComponent();

            if (!DesignMode) {
                manageAddressesList.InvisibleColumns = new[] {columnId, columnChannelId, columnOLId};
                manageAddressesList.FrozenColumns = gridView.Columns.Cast<GridColumn>().Where(x => x.Fixed != FixedStyle.None).ToArray();
            }
            manageAddressesList.InsertButtonText = Resource.InsertSelectedTTIntoControlGroup;
            manageAddressesList.OnDisplayFilterChanged += manageAddressesList_OnDisplayFilterChanged;
            IsDataLoaded = false;
        }

        /// <summary>
        /// Gets the controls grid control
        /// </summary>
        public DevExpress.XtraGrid.GridControl GridControl
        {
            get
            {
                return gridControlAddresses;
            }
        }
        
        /// <summary>
        /// Disables custom formating
        /// </summary>
        public void PrepareToExport()
        {
            gridColumnInBevPart.DisplayFormat.FormatType = FormatType.None;
        }

        /// <summary>
        /// Restores custom formating
        /// </summary>
        public void RestoreAfterExport()
        {
            gridColumnInBevPart.DisplayFormat.FormatType = FormatType.Numeric;
            gridColumnInBevPart.DisplayFormat.FormatString = "P";
        }

        /// <summary>
        /// Gets or sets the chanel id.
        /// </summary>
        /// <value>The chanel id.</value>
        public object ChanelId {
            get { return manageAddressesList.ChannelId ?? (manageAddressesList.ChannelId = (int) MainData.Channel); }
            set { manageAddressesList.ChannelId = value; }
        }

        /// <summary>
        /// Gets or sets the addresses list.
        /// </summary>
        /// <value>The addresses list.</value>
        private DataTable AddressesList {
            set {
                if (null != value)
                    AddActivityColumnsToGrid(value);

                gridControlAddresses.DataSource = value;
                if (null != value)
                    AddressesList.PrimaryKey = new DataColumn[] {AddressesList.Columns[columnOLId.FieldName]};
            }
            get {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable) gridControlAddresses.DataSource;
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData() {
            repositoryItemLookUpStatus.DataSource = DataProvider.AddressStatusTable;
            DataTable lDataTable = DataProvider.GetAddressesList(MainData.ContractId, true);
            AddressesList = lDataTable;
            RefreshPOCList();
            MainData.HasAccessAddresses = ConvertEx.ToBool(DataProvider.GetHasAccessToAddresses(MainData.ContractId));
            ChanelId = (int) MainData.Channel;
            base.LoadData();
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <returns></returns>
        public override int SaveData() {
            int lRetCode = DataProvider.SetPocAddressesList(MainData.ContractId, true, GetAddressesList(AddressRowStatus.New),
                GetAddressesList(AddressRowStatus.Removed));
            if (lRetCode > 0)
                NormalizePocList();
            return lRetCode;
        }

        /// <summary>
        /// Normalizes the poc list.
        /// </summary>
        private void NormalizePocList() {
            DataTable lDataTable = gridControlAddresses.DataSource as DataTable;
            if (lDataTable == null || lDataTable.Rows.Count <= 0)
                return;
            foreach (DataRow row in lDataTable.Rows) {
                if (ConvertEx.ToInt(row[columnStatus.FieldName]) == (int) AddressRowStatus.Removed)
                    row.Delete();
                else
                    row[columnStatus.FieldName] = (int) AddressRowStatus.Common;
            }
            lDataTable.AcceptChanges();
        }

        /// <summary>
        /// Gets the addresses list.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public string GetAddressesList(AddressRowStatus status) {
            IList<string> lIdList = new List<string>();
            if (gridControlAddresses.DataSource == null)
                return string.Join(Constants.DELIMITER_ID_LIST, lIdList.ToArray());
            
            DataView lDataView = new DataView(gridControlAddresses.DataSource as DataTable);
            lDataView.RowFilter = string.Format("{0} = {1}", columnStatus.FieldName, (int) status);
            DataTable lDataTable = lDataView.ToTable();
            foreach (DataRow row in lDataTable.Rows)
                lIdList.Add(row[columnOLId.FieldName].ToString());

            return string.Join(Constants.DELIMITER_ID_LIST, lIdList.ToArray());
        }

        /// Gets the all addresses.
        /// </summary>
        /// <returns>List of all addresses</returns>
        public string GetAliveAddresses() {
            DataTable lTable = gridControlAddresses.DataSource as DataTable;
            if (lTable == null)
                return null;

            return string.Join(Constants.DELIMITER_ID_LIST, (from d in lTable.AsEnumerable()
                                                             let lStatus = (AddressRowStatus) d.Field<int>(columnStatus.FieldName)
                                                             where lStatus == AddressRowStatus.Common || lStatus == AddressRowStatus.New
                                                             select d.Field<String>(columnPOCCode.FieldName)).ToArray());
        }

        /// <summary>
        /// Manages the addresses list_ on filter changed.
        /// </summary>
        /// <param name="args">The args.</param>
        private void manageAddressesList_OnFilterChanged(FilterChangedArgs args) {
            AddSelectedPOC(args.PocIds);
            RefreshPOCList();
        }

        /// <summary>
        /// Handles the KeyDown event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
        private void gridView_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Delete)
                RemoveSelectedPOC();
        }

        private void manageAddressesList_DeleteBtnClick(object sender, EventArgs e) {
            RemoveSelectedPOC();
        }

        /// <summary>
        /// Handles the DataSourceChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DataSourceChanged(object sender, EventArgs e) {
            FireUpdateMainView();
        }

        private void manageAddressesList_FilterPreviewChanging(FilterPreviewChangingArgs args) {
            args.AddedOutlets = new XElement(Constants.XML_OUTLET_ROOT_NAME, MainData.AddressesPocList.Select(x =>
            {
                var e = new XElement(Constants.XML_OUTLET_ELEMENT_NAME);
                e.SetAttributeValue(Constants.XML_OUTLET_ATTRIBUTE_FIELD_ID, x);
                return e;
            })).ToString(SaveOptions.DisableFormatting);

            args.CtrlGrOutlets = new XElement(Constants.XML_OUTLET_ROOT_NAME, MainData.ControlGroupPocList.Select(x =>
            {
                var e = new XElement(Constants.XML_OUTLET_ELEMENT_NAME);
                e.SetAttributeValue(Constants.XML_OUTLET_ATTRIBUTE_FIELD_ID, x);
                return e;
            })).ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Removes the selected addresses.
        /// </summary>
        private void RemoveSelectedPOC() {
            bool lAllowRemove = manageAddressesList.EditMode == AddressEditMode.AllowAll || manageAddressesList.EditMode == AddressEditMode.AllowRemoveOnly;
            if (!MainData.AllowAddressChange || gridView.SelectedRowsCount <= 0 || !lAllowRemove)
                return;

            int[] lSelectedRows = gridView.GetSelectedRows();
            foreach (int lRowHandle in lSelectedRows) {
                DataRowView lRow = gridView.GetRow(lRowHandle) as DataRowView;
                AddressRowStatus lCurStatus = (AddressRowStatus) Convert.ToInt32(lRow[columnStatus.FieldName]);
                if (lCurStatus != AddressRowStatus.Removed) {
                    lRow[columnStatus.FieldName] = (int) AddressRowStatus.Removed;
                    gridView.UpdateCurrentRow();
                    OnChanged(true);
                    RefreshPOCList();
                }
            }
        }

        /// <summary>
        /// Adds the POC.
        /// </summary>
        /// <param name="strPOCIdList">The list of OL Ids separated by comma</param>
        private void AddSelectedPOC(string strPOCIdList) {
            if (!MainData.AllowAddressChange || string.IsNullOrEmpty(strPOCIdList))
                return;

            DataTable lDataTable = DataProvider.GetAddressesListByOLIDs(strPOCIdList, MainData.DateSignPlanned, MainData.ContractTerm, ChanelId);
            if (lDataTable == null || lDataTable.Rows.Count <= 0)
                return;

            lDataTable.PrimaryKey = new DataColumn[] {lDataTable.Columns[columnOLId.FieldName]};
            foreach (DataRow lRow in lDataTable.Rows)
                lRow[columnStatus.FieldName] = AddressRowStatus.New;

            if (AddressesList == null)
                AddressesList = lDataTable;
            else
                AddressesList.Merge(lDataTable, false, MissingSchemaAction.Ignore);

            FireUpdateMainView();
            OnChanged(true);
            RefreshPOCList();
        }

        /// <summary>
        /// Refreshes the POC list.
        /// </summary>
        private void RefreshPOCList() {
            if (MainData.ControlGroupPocList == null)
                return;

            MainData.ControlGroupPocList.Clear();
            string lStrAliveAddresses = GetAliveAddresses();
            if (!string.IsNullOrEmpty(lStrAliveAddresses))
                MainData.ControlGroupPocList = lStrAliveAddresses.Split(Constants.DELIMITER_ID_LIST.ToCharArray()).ToList();
        }

        /// <summary>
        /// Adds the activity columns to grid.
        /// </summary>
        /// <param name="table">The table.</param>
        private void AddActivityColumnsToGrid(DataTable table) {
            IEnumerable<GridColumn> lColumns = gridView.Columns.Cast<GridColumn>();

            foreach (DataColumn lColumn in table.Columns) {
                if (!lColumn.ColumnName.StartsWith(ActivityColumnPrefix) || lColumns.Where(x => x.FieldName == lColumn.ColumnName).Count() > 0)
                    continue;

                gridView.Columns.Add(NewGridColumn(lColumn.ColumnName, lColumn.ColumnName.Substring(ActivityColumnPrefix.Length)));
            }

            manageAddressesList.FrozenColumns = manageAddressesList.FrozenColumns;
        }

        /// <summary>
        /// News the grid column.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="displayName">The display name.</param>
        /// <returns></returns>
        private GridColumn NewGridColumn(String fieldName, String displayName) {
            GridColumn lColumn = new GridColumn();

            lColumn.AppearanceHeader.Options.UseTextOptions = true;
            lColumn.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            lColumn.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
            lColumn.Caption = displayName;
            lColumn.ColumnEdit = repositoryActivity;
            lColumn.FieldName = fieldName;
            lColumn.MinWidth = 50;
            lColumn.OptionsColumn.AllowEdit = false;
            lColumn.OptionsColumn.AllowMove = false;
            lColumn.OptionsColumn.ReadOnly = true;
            lColumn.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
            lColumn.Visible = false;
            lColumn.Width = 100;
            lColumn.ColumnEdit = repositoryActivity;

            return lColumn;
        }

        /// <summary>
        /// Addresseses the list CTRL_ on display filter changed.
        /// </summary>
        /// <param name="args">The args.</param>
        private void manageAddressesList_OnDisplayFilterChanged(FilterChangedArgs args) {
            string lFilterString = String.Format("[{0}] In ({1})", columnPOCCode.FieldName, args.PocIds);
            columnPOCCode.FilterInfo = new ColumnFilterInfo(ColumnFilterType.Custom, lFilterString);
        }

        private void gridControlAddresses_KeyDown(object sender, KeyEventArgs e) {
            if ((e.KeyCode == Keys.V) && e.Control)
                manageAddressesList.FilterDisplayedAddresses();
        }
    }
}