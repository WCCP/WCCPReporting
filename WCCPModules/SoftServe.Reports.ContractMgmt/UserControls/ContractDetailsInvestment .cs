﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.DataAccess;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.Enums;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// </summary>
    public partial class ContractDetailsInvestment : CommonBaseControl, ITwoStepSavingControl {
        #region Fields

        /// <summary>
        ///   Sets all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable _allFieldsTable;

        public CommonBaseControl ConditionsControl;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "ContractDetailsGoals" /> class.
        /// </summary>
        public ContractDetailsInvestment() {
            IsSecondStepRequired = false;
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        public override bool IsCalcVRequired {
            get {
                return LoadedPaymentSchema != PaymentSchema
                    || LoadedPaymentType != PaymentType
                        || LoadedMarketTrend != MarketTrend
                            || LoadedPlannedVolumeUplift != PlannedVolumeUplift;
                //|| LoadedPlannedAvrMacoPerDal != PlannedAvrMacoPerDal;
            }
            set { }
        }

        /// <summary>
        ///   Gets the plan total investment sum.
        /// </summary>
        /// <value>The plan total investment sum.</value>
        public decimal PlanTotalInvestmentSum {
            get { return controlIndicators.PlanTotalInvestmentSum; }
        }

        private DataTable AllFieldsData {
            set {
                _allFieldsTable = value;
                if (_allFieldsTable != null && _allFieldsTable.Rows.Count == 1) {
                    DataRow lRow = _allFieldsTable.Rows[0];

                    cbPaymentSchema.EditValue = ConvertEx.ToByte(lRow[Constants.FIELD_COST_PAYMENT_SCHEMA_ID]);
                    cbPaymentType.EditValue = ConvertEx.ToByte(lRow[Constants.FIELD_COST_PAYMENT_TYPE_ID]);
                    spinPrevAvrVolume.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_PREV_AVRVOLUME]);
                    spinMarketTrend.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_MARKET_TRAND]);
                    spinTrandedAvrVolume.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_TRANDED_AVRVOLUME]);
                    spinPlannedVolumeUplift.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_PLANNED_VOLUMEUPLIFT]);
                    spinUpliftedAvrVolume.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_UPLIFTED_AVRVOLUME]);
                    spinPlannedOutletsQty.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_PLANNED_OUTLETSQANTITY]);
                    spinExpensesPerOutlet.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_EXPENSES_PEROUTLET]);
                    spinPlannedAvrMacoPerDal.EditValue = ConvertEx.ToDecimal(lRow[Constants.FIELD_COST_PLANNED_AVRMACOPERDAL]);
                    //                    checkAdvertTax.Checked = ConvertEx.ToBool(row[Constants.FIELD_COST_HAS_ADVERT]);
                }
            }
            get {
                if (_allFieldsTable != null && _allFieldsTable.Rows.Count == 1) {
                    DataRow lRow = _allFieldsTable.Rows[0];

                    lRow[Constants.FIELD_COST_PAYMENT_SCHEMA_ID] = cbPaymentSchema.EditValue;
                    lRow[Constants.FIELD_COST_PAYMENT_TYPE_ID] = cbPaymentType.EditValue;
                    lRow[Constants.FIELD_COST_PREV_AVRVOLUME] = spinPrevAvrVolume.EditValue;
                    lRow[Constants.FIELD_COST_MARKET_TRAND] = spinMarketTrend.EditValue;
                    lRow[Constants.FIELD_COST_TRANDED_AVRVOLUME] = spinTrandedAvrVolume.EditValue;
                    lRow[Constants.FIELD_COST_PLANNED_VOLUMEUPLIFT] = spinPlannedVolumeUplift.EditValue;
                    lRow[Constants.FIELD_COST_UPLIFTED_AVRVOLUME] = spinUpliftedAvrVolume.EditValue;
                    lRow[Constants.FIELD_COST_PLANNED_OUTLETSQANTITY] = spinPlannedOutletsQty.EditValue;
                    lRow[Constants.FIELD_COST_EXPENSES_PEROUTLET] = spinExpensesPerOutlet.EditValue;
                    lRow[Constants.FIELD_COST_PLANNED_AVRMACOPERDAL] = spinPlannedAvrMacoPerDal.EditValue;
                    lRow[Constants.FIELD_COST_HAS_ADVERT] = false; //checkAdvertTax.Checked;
                }
                return _allFieldsTable;
            }
        }

        /// <summary>
        ///   Gets all fields XML.
        /// </summary>
        /// <value>All fields XML.</value>
        private SqlXml AllFieldsXml {
            get {
                return ConvertEx.ToSqlXml(AllFieldsData,
                    new string[] {
                        Constants.FIELD_COST_PAYMENT_SCHEMA_ID,
                        Constants.FIELD_COST_PAYMENT_TYPE_ID,
                        Constants.FIELD_COST_PREV_AVRVOLUME,
                        Constants.FIELD_COST_MARKET_TRAND,
                        Constants.FIELD_COST_TRANDED_AVRVOLUME,
                        Constants.FIELD_COST_PLANNED_VOLUMEUPLIFT,
                        Constants.FIELD_COST_UPLIFTED_AVRVOLUME,
                        Constants.FIELD_COST_PLANNED_OUTLETSQANTITY,
                        Constants.FIELD_COST_EXPENSES_PEROUTLET,
                        Constants.FIELD_COST_PLANNED_AVRMACOPERDAL,
                        Constants.FIELD_COST_HAS_ADVERT
                    });
            }
        }

        /// <summary>
        ///   Gets or sets value that indicates whether some of dependent field was changen on the UI.
        /// </summary>
        private bool DependentFieldChanged { get; set; }

        private bool IsProcessing { get; set; }

        private IndicatorEventArgs IndicatorInteractiveData { set; get; }
//        private decimal InitialTaxAdvert { set; get; }

        private decimal MarketTrend {
            get { return ConvertEx.ToDecimal(spinMarketTrend.EditValue); }
        }

        private decimal OldMarketTrend { get; set; }
        private decimal OldPlannedAvrMacoPerDal { get; set; }
        private decimal OldPlannedVolumeUplift { get; set; }

        private decimal LoadedMarketTrend { get; set; }
        private decimal LoadedPlannedAvrMacoPerDal { get; set; }
        private decimal LoadedPlannedVolumeUplift { get; set; }
        private byte LoadedPaymentSchema { get; set; }
        private byte LoadedPaymentType { get; set; }

        private byte PaymentSchema {
            get { return ConvertEx.ToByte(cbPaymentSchema.EditValue); }
        }

        private byte PaymentType {
            get { return ConvertEx.ToByte(cbPaymentType.EditValue); }
        }

        private decimal PlannedAvrMacoPerDal {
            get { return ConvertEx.ToDecimal(spinPlannedAvrMacoPerDal.EditValue); }
        }

        private decimal PlannedVolumeUplift {
            get { return ConvertEx.ToDecimal(spinPlannedVolumeUplift.EditValue); }
        }

        /// <summary>
        ///   Gets the selected expenses item tree XML.
        /// </summary>
        /// <value>The selected expenses item tree XML.</value>
        private SqlXml SelectedExpensesItemTreeXml {
            get { return expensesTree.ExpensesList; }
        }

        /// <summary>
        ///   Gets the selected expenses sum XML.
        /// </summary>
        /// <value>The selected expenses sum XML.</value>
        private SqlXml SelectedExpensesSumXml {
            get { return expensesTree.ExpensesSummaryXml; }
        }


        /// <summary>
        ///   Gets the selected indicators XML.
        /// </summary>
        /// <value>The selected indicators XML.</value>
        private SqlXml SelectedIndicatorsXml {
            get { return controlIndicators.SelectedIndicatorsXml; }
        }

        private decimal TaxAdvert { set; get; }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Loads the data.
        /// </summary>
        public override void LoadData() {
            AttachEvents(false);
            LoadDropDowns();
            AllFieldsData = DataProvider.GetInvestmentDetails(MainData.ContractId);
            LoadedPaymentSchema = PaymentSchema;
            LoadedPaymentType = PaymentType;
            OldMarketTrend = LoadedMarketTrend = MarketTrend;
            OldPlannedVolumeUplift = LoadedPlannedVolumeUplift = PlannedVolumeUplift;
            OldPlannedAvrMacoPerDal = LoadedPlannedAvrMacoPerDal = PlannedAvrMacoPerDal;
            LoadExpensesTree();
            LoadIndicators();
            AttachEvents(true);
            VerifyAllowEditingIndicators();
            base.LoadData();
        }

        /// <summary>
        ///   Saves this instance.
        /// </summary>
        public override int SaveData() {
            SaveFields();
            SaveExpensesPlan();
            SaveIndicatorsTable();
            int lResult = SaveExpensesSumPlan();
            return lResult;
        }

        /// <summary>
        ///   Adds the expenses.
        /// </summary>
        /// <param name = "contractId">The contract id.</param>
        public void AddExpense(int contractId) {
            expensesTree.AddExpenses(contractId);
        }

        /// <summary>
        ///   Loads presaved indicators
        /// </summary>
        public void LoadIndicators() {
            DataTable lDataTable = DataProvider.GetIndicatorsTable(MainData.ContractId);
            controlIndicators.LoadIndicators(lDataTable, MainData.DateSignPlanned);
        }

        /// <summary>
        ///   Recalculates the expenses.
        /// </summary>
        public void Recalculate(int indicatorId = 0) {
            AttachEvents(false);

            // Recaclulate TrendedAvrVolume
            RecalculateTrendedAvgVolume();

            // Recaclulate UpliftedAvrVolume
            RecalculateUpliftedAvgVolume();

            // Recalculate expenses tree
            RecalcExpensesTree();

            // Recalc indicators
            RecalcIndicatorsByField(indicatorId);

            // Recalculate ExpensesPerOutlet
            // should be after RecalcIndicatorsByField because depends on Total Investments
            RecalculateExpensesPerOutlet();

            ClearErrors();

            AttachEvents(true);

            OnChanged(true);
        }

        /// <summary>
        ///   Attaches the detach events.
        /// </summary>
        /// <param name = "attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach) {
                controlIndicators.EditValueChanged += OnEditValueChanged;
                //controlIndicators.PlanValueChanged += OnPlanVolumeChanged;
                expensesTree.EditValueChanged += OnEditValueChanged;
                cbPaymentType.EditValueChanged += OnEditValueChanged;
                cbPaymentSchema.EditValueChanged += OnEditValueChanged;
//                spinMarketTrend.EditValueChanged += OnEditValueChanged;
//                spinPlannedVolumeUplift.EditValueChanged += OnEditValueChanged;
            }
            else {
                controlIndicators.EditValueChanged -= OnEditValueChanged;
                //controlIndicators.PlanValueChanged -= OnPlanVolumeChanged;
                expensesTree.EditValueChanged -= OnEditValueChanged;
                cbPaymentType.EditValueChanged -= OnEditValueChanged;
                cbPaymentSchema.EditValueChanged -= OnEditValueChanged;
//                spinMarketTrend.EditValueChanged -= OnEditValueChanged;
//                spinPlannedVolumeUplift.EditValueChanged -= OnEditValueChanged;
            }
        }

        public void OnPlanVolumeChanged(object sender, EventArgs e) {
            PlanVolumeChangedEventArgs lArgs = e as PlanVolumeChangedEventArgs;
            if (lArgs == null || ContractDetailsControl == null)
                return;
            ContractDetailsControl.UpdatePlanVolume(lArgs);
        }

        /// <summary>
        ///   Returns currently selected payment schema in drow-down list.
        /// </summary>
        /// <returns></returns>
        private PaymentSchema GetSelectedPaymentSchema() {
            int lPaymentSchemaId = ConvertEx.ToInt(cbPaymentSchema.EditValue);
            return (PaymentSchema) lPaymentSchemaId;
        }

        /// <summary>
        ///   Returns currently selected payment type in drow-down list.
        /// </summary>
        /// <returns></returns>
        private PaymentType GetSelectedPaymentType() {
            int lPaymentTypeId = ConvertEx.ToInt(cbPaymentType.EditValue);
            return (PaymentType) lPaymentTypeId;
        }

        /// <summary>
        ///   Loads the drop downs.
        /// </summary>
        private void LoadDropDowns() {
            DataTable lPaymentSchemaTable = DataProvider.PaymentSchemaList;
            if (lPaymentSchemaTable != null && lPaymentSchemaTable.Rows.Count > 0) {
                cbPaymentSchema.Properties.DataSource = lPaymentSchemaTable;
                cbPaymentSchema.EditValue = lPaymentSchemaTable.Rows[0][cbPaymentSchema.Properties.ValueMember];
            }

            DataTable lPaymentTypeTable = DataProvider.PaymentTypeList;
            if (lPaymentTypeTable != null && lPaymentTypeTable.Rows.Count > 0) {
                cbPaymentType.Properties.DataSource = lPaymentTypeTable;
                cbPaymentType.EditValue = lPaymentTypeTable.Rows[0][cbPaymentType.Properties.ValueMember];
            }
        }

        /// <summary>
        ///   Loads the expenses tree.
        /// </summary>
        private void LoadExpensesTree() {
            expensesTree.LoadExpensesTree(MainData.ContractId);
            //TaxAdvert = InitialTaxAdvert = expensesTree.TaxAdvertValue;
            TaxAdvert = 0;
            RecalcExpensesTree();

            MainData.TotalExpensesInvestments = expensesTree.TotalInvestments;
        }

        /// <summary>
        ///   Recalcs the expenses tree.
        /// </summary>
        private void RecalcExpensesTree() {
            //TaxAdvert = checkAdvertTax.Checked ? InitialTaxAdvert : 0;
            TaxAdvert = 0;

            // Update expenses tree
            expensesTree.SetCalculationInput(
                ConvertEx.ToDecimal(spinPlannedAvrMacoPerDal.EditValue),
                ConvertEx.ToDecimal(spinUpliftedAvrVolume.EditValue),
                ConvertEx.ToInt(spinPlannedOutletsQty.EditValue),
                MainData.DateSignPlanned, MainData.ContractTerm, TaxAdvert);

            MainData.TotalExpensesInvestments = expensesTree.TotalInvestments;
        }

        /// <summary>
        ///   Recalculates the indicators.
        /// </summary>
        private void RecalcIndicatorsByField(int indicatorId) {
            if (MainData.IsEditMode) {
                DataTable lDataTable;

                if (IndicatorInteractiveData != null) {
                    // if changed Planned values for Investment Sum or Target V we need to reload grid Budget Plan
                    if ((IndicatorInteractiveData.IndicatorId == (int) IndicatorType.TargetVDal
                        || IndicatorInteractiveData.IndicatorId == (int) IndicatorType.InvestmentSumYtd)
                            && IndicatorInteractiveData.IsPlanColumn)
                        IsExternalRefreshRequired = true;

                    lDataTable = DataProvider.GetDynamicIndicatorsByField(MainData.ContractId, IndicatorInteractiveData.IndicatorId,
                        IndicatorInteractiveData.PeriodColumnNum, IndicatorInteractiveData.IsPlanColumn,
                        AllFieldsXml, SelectedExpensesSumXml, SelectedIndicatorsXml);
                }
                else
                    lDataTable = DataProvider.GetDynamicIndicatorsByField(MainData.ContractId, indicatorId, 0, true,
                        AllFieldsXml, SelectedExpensesSumXml, SelectedIndicatorsXml); // recalculate only based on baseIndicators

                controlIndicators.LoadIndicators(lDataTable, MainData.DateSignPlanned);
            }
            IndicatorInteractiveData = null;

            ValidateData();
        }

        /// <summary>
        ///   Method that recalulates expenses per outlet with formula
        ///   [План затрат на ТТ] = [Всего инвестиций, грн] / [Планируемое кол-во ТТ]"
        /// </summary>
        private void RecalculateExpensesPerOutlet() {
            spinExpensesPerOutlet.EditValue = 0;
            if (spinPlannedOutletsQty.Value > 0)
                spinExpensesPerOutlet.EditValue = Math.Round(PlanTotalInvestmentSum / spinPlannedOutletsQty.Value, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        ///   Recalculates tranded average volume.
        /// [Сред. V/TT с учетом рынка] = [Средний V/ТТ до контракта] * (1 + [Тренд рынка (рост/падение)])
        /// </summary>
        private void RecalculateTrendedAvgVolume() {
            decimal lPrevAvrVolume = ConvertEx.ToDecimal(spinPrevAvrVolume.EditValue);
            decimal lMarketTrend = ConvertEx.ToDecimal(spinMarketTrend.EditValue);
            spinTrandedAvrVolume.EditValue = Math.Round(lPrevAvrVolume * (1 + lMarketTrend), 3, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        ///   Recalculates uplifted average volume.
        /// [Сред. V/TT после старта] = [Средний V/ТТ с учетом рынка] * (1 + [Планируемый прирост])
        /// </summary>
        private void RecalculateUpliftedAvgVolume() {
            decimal lTrandedAvrVolume = ConvertEx.ToDecimal(spinTrandedAvrVolume.EditValue);
            decimal lPlannedVolumeUplift = ConvertEx.ToDecimal(spinPlannedVolumeUplift.EditValue);
            spinUpliftedAvrVolume.EditValue = Math.Round(lTrandedAvrVolume * (1 + lPlannedVolumeUplift), 3, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        ///   Saves the cost plan.
        /// </summary>
        private void SaveExpensesPlan() {
            DataProvider.SetExpensesPlan(MainData.ContractId, SelectedExpensesItemTreeXml);
        }

        /// <summary>
        ///   Saves the expenses sum plan.
        /// </summary>
        private int SaveExpensesSumPlan() {
            try {
                DataProvider.SetIndicatorsSumTable(MainData.ContractId, SelectedExpensesSumXml);
            }
            catch (Exception lEx) {
                if (lEx.InnerException != null && lEx.InnerException is SqlException) {
                    SqlException lSqlEx = (SqlException) lEx.InnerException;
                    if (lSqlEx.Class == 16 && lSqlEx.Number == 50000)
                        XtraMessageBox.Show(lSqlEx.Message, Resource.ErrorCaption, MessageBoxButtons.OK);
                    return 0;
                }
                throw;
            }
            return 1;
        }

        /// <summary>
        ///   Saves the fields.
        /// </summary>
        private void SaveFields() {
            DataProvider.SetInvestmentsFields(MainData.ContractId, AllFieldsXml);
        }

        /// <summary>
        ///   Saves the indicators table.
        /// </summary>
        private void SaveIndicatorsTable() {
            DataProvider.SetIndicatorsTable(MainData.ContractId, SelectedIndicatorsXml);
        }

        /// <summary>
        ///   Checks whether conditions are suitable for editing controlIndicators in current month and sets result to controlIndicators.AllowEditingForCurrentMonth property.
        /// </summary>
        private void VerifyAllowEditingIndicators() {
            if (MainData.ContractStatus == ContractStatus.DeclinedClient || MainData.ContractStatus == ContractStatus.DeclinedInBev
                || MainData.ContractStatus == ContractStatus.TerminatedClient || MainData.ContractStatus == ContractStatus.TerminatedInBev)
                controlIndicators.AllowEditingForCurrentMonth = false;
            else
                controlIndicators.AllowEditingForCurrentMonth = true;
        }

        #endregion

        #region Event Handling

        /// <summary>
        ///   Called when [edit value changed].
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void OnEditValueChanged(object sender, EventArgs e) {
            if (sender == controlIndicators) {
                IndicatorInteractiveData = e as IndicatorEventArgs; // memorize changed cell
                if (IndicatorInteractiveData != null) {
                    if (IndicatorInteractiveData.IndicatorId == (int) IndicatorType.TargetVDal)
                        IsSecondStepRequired = true;

                    // if changed Investment Sum or Target V we need to reload grid Budget Plan
                    if ((IndicatorInteractiveData.IndicatorId == (int) IndicatorType.TargetVDal
                        || IndicatorInteractiveData.IndicatorId == (int) IndicatorType.InvestmentSumYtd)
                            && IndicatorInteractiveData.IsPlanColumn)
                        IsExternalRefreshRequired = true;
                }
                Recalculate();
                IsChanged = true;
                DependentFieldChanged = true;
            }
            else if (sender == expensesTree) {
                IsExternalRefreshRequired = true;
                // Recalc indicators
                RecalcIndicatorsByField(0);
                RecalculateExpensesPerOutlet();
                IsChanged = true;
                DependentFieldChanged = true;
            }
            else if (sender == cbPaymentType) {
                DependentFieldChanged = true;
                Recalculate();
                IsCalcVRequired = true;
            }
            else if (sender == cbPaymentSchema) {
                DependentFieldChanged = true;
                Recalculate();
                IsCalcVRequired = true;
            }

            VerifyAllowEditingIndicators();

            DependentFieldChanged = true;
        }

        /// <summary>
        ///   Called when [validating].
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.ComponentModel.CancelEventArgs" /> instance containing the event data.</param>
        private void OnValidating(object sender, CancelEventArgs e) {
            if (sender == controlIndicators && controlIndicators.EditMode == IndicatorEditMode.Common) {
                controlIndicators.ClearErrorIndicators();

                // тимчасово відключено перевірку коректності даних в контролі
                /*
                bool zeroValues = controlIndicators.PlanTotalInvestmentSum == 0 && controlIndicators.PlanTotalContractSum == 0 && expensesTree.TotalWithoutTaxes == 0 &&
                                  expensesTree.TotalInvestments == 0;
                
                if (!zeroValues)
                {
                    bool isValid = Math.Round(controlIndicators.PlanTotalContractSum) <= Math.Round(controlIndicators.PlanTotalInvestmentSum);
                    if (!isValid)
                    {
                        controlIndicators.AddInvalidIndicators(IndicatorType.ContractSumYtd);
                        controlIndicators.AddInvalidIndicators(IndicatorType.InvestmentSumYtd);
                    }
                    isValid = controlIndicators.PlanTotalUpliftSum >= spinPlannedVolumeUplift.Value;
                    if (!isValid)
                    {
                        controlIndicators.AddInvalidIndicators(IndicatorType.Uplift);
                    }
                    isValid = Math.Round(controlIndicators.PlanTotalContractSum) <= Math.Round(expensesTree.TotalWithoutTaxes);
                    if (!isValid)
                    {
                        controlIndicators.AddInvalidIndicators(IndicatorType.ContractSumYtd);
                    }
                    isValid = Math.Round(controlIndicators.PlanTotalInvestmentSum) <= Math.Round(expensesTree.SumWithCapex);
                    if (!isValid)
                    {
                        controlIndicators.AddInvalidIndicators(IndicatorType.InvestmentSumYtd);
                    }
                    isValid = decimal.Round(controlIndicators.PlanTotalSeasonFactor, 2) <= 1.00M;
                    if (!isValid)
                    {
                        controlIndicators.AddInvalidIndicators(IndicatorType.SeasonFactor);
                    }

                    e.Cancel = controlIndicators.HasErrors;
                }
                 */
                return;
            }

            BaseEdit baseEdit = sender as BaseEdit;
            if (baseEdit != null) {
                bool isFieldEmpty = baseEdit.EditValue == null || string.IsNullOrEmpty(baseEdit.EditValue.ToString());
                if (isFieldEmpty) {
                    e.Cancel = true;
                    SetError(baseEdit, Resource.FieldEmptyError);
                }
                else if (baseEdit == spinPlannedVolumeUplift && ConvertEx.ToDecimal(spinPlannedVolumeUplift.EditValue) <= 0M) {
                    e.Cancel = true;
                    SetError(baseEdit, Resource.PositiveValueError);
                }
                else if (baseEdit == spinPlannedAvrMacoPerDal && ConvertEx.ToDecimal(spinPlannedAvrMacoPerDal.EditValue) <= 0M) {
                    e.Cancel = true;
                    SetError(baseEdit, Resource.PositiveValueError);
                }
            }
        }

        /// <summary>
        ///   Event handler that is used to correctly focus control after recalculation occured.
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        private void controlGetFocus(object sender, EventArgs e) {
            Control focusedControl = sender as Control;
            if (focusedControl != null)
                focusedControl.Focus();
        }

        /// <summary>
        ///   Event handler that is used to track whether value was modified and then start recalculation.
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        private void controlLostFocus(object sender, EventArgs e) {
            if (IsProcessing)
                return;
//            if (!DependentFieldChanged)
//                return;
            if (OldMarketTrend == MarketTrend && OldPlannedVolumeUplift == PlannedVolumeUplift)
                return;

            IsProcessing = true;
            ;
            try {
                Recalculate();
                IsCalcVRequired = true;
                OldMarketTrend = MarketTrend;
                OldPlannedVolumeUplift = PlannedVolumeUplift;
                DependentFieldChanged = false;
            }
            finally {
                IsProcessing = false;
            }
        }

        #endregion

        #region ITwoStepSavingControl Members

        public bool IsSecondStepRequired { get; set; }

        public void SecondStageSave() {
            SaveIndicatorsTable();
        }

        #endregion

        public void RecalculateMacoPlan() {
            //dbo.spDW_CM_GetAverageMacoPerDal
            decimal lAvgMacoPerDal = DataProvider.GetAverageMacoPerDal(MainData.ContractId);
            spinPlannedAvrMacoPerDal.EditValue = lAvgMacoPerDal;
            Recalculate(-1);
            OldPlannedAvrMacoPerDal = PlannedAvrMacoPerDal;
        }
    }
}