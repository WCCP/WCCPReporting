﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ContractDetailsAdvert
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scrollableArea = new DevExpress.XtraEditors.XtraScrollableControl();
            this.splitContainerMain = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerLeft = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupEvents = new DevExpress.XtraEditors.GroupControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.memoIsAdvertising = new DevExpress.XtraEditors.MemoEdit();
            this.textCompanyPartner = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlEvents = new DevExpress.XtraGrid.GridControl();
            this.gridViewEvents = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnEventId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryEventsLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columnEventDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupPlannedChanges = new DevExpress.XtraEditors.GroupControl();
            this.gridControlPlannedChanges = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlannedChanges = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnPlannedChangeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPlannedChangeTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnPlannedChangeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPlanChangeDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.splitContainerRight = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupAdvert = new DevExpress.XtraEditors.GroupControl();
            this.gridControlMediums = new DevExpress.XtraGrid.GridControl();
            this.gridViewMediums = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnMediumId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMediumsLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columnMediumDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupPlannedAdvert = new DevExpress.XtraEditors.GroupControl();
            this.gridControlPlannedAdvert = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlannedAdvert = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnPlannedAdvName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPlannedAdvTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnPlannedAdvDescr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.scrollableArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
            this.splitContainerMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).BeginInit();
            this.splitContainerLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupEvents)).BeginInit();
            this.groupEvents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoIsAdvertising.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCompanyPartner.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryEventsLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPlannedChanges)).BeginInit();
            this.groupPlannedChanges.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlannedChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlannedChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlannedChangeTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlanChangeDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlanChangeDateEdit.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerRight)).BeginInit();
            this.splitContainerRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupAdvert)).BeginInit();
            this.groupAdvert.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMediums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMediums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMediumsLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPlannedAdvert)).BeginInit();
            this.groupPlannedAdvert.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlannedAdvert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlannedAdvert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlannedAdvTextEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // scrollableArea
            // 
            this.scrollableArea.AutoScrollMinSize = new System.Drawing.Size(700, 500);
            this.scrollableArea.Controls.Add(this.splitContainerMain);
            this.scrollableArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollableArea.Location = new System.Drawing.Point(0, 0);
            this.scrollableArea.Name = "scrollableArea";
            this.scrollableArea.Size = new System.Drawing.Size(854, 667);
            this.scrollableArea.TabIndex = 0;
            // 
            // splitContainerMain
            // 
            this.splitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerMain.Location = new System.Drawing.Point(0, 0);
            this.splitContainerMain.Name = "splitContainerMain";
            this.splitContainerMain.Panel1.Controls.Add(this.splitContainerLeft);
            this.splitContainerMain.Panel1.Text = "Panel1";
            this.splitContainerMain.Panel2.Controls.Add(this.splitContainerRight);
            this.splitContainerMain.Panel2.Text = "Panel2";
            this.splitContainerMain.Size = new System.Drawing.Size(854, 667);
            this.splitContainerMain.SplitterPosition = 422;
            this.splitContainerMain.TabIndex = 2;
            this.splitContainerMain.Text = "splitContainerControl1";
            // 
            // splitContainerLeft
            // 
            this.splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerLeft.Horizontal = false;
            this.splitContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.splitContainerLeft.Name = "splitContainerLeft";
            this.splitContainerLeft.Panel1.Controls.Add(this.groupEvents);
            this.splitContainerLeft.Panel1.MinSize = 310;
            this.splitContainerLeft.Panel1.Text = "Panel1";
            this.splitContainerLeft.Panel2.Controls.Add(this.groupPlannedChanges);
            this.splitContainerLeft.Panel2.Text = "Panel2";
            this.splitContainerLeft.Size = new System.Drawing.Size(422, 667);
            this.splitContainerLeft.SplitterPosition = 320;
            this.splitContainerLeft.TabIndex = 2;
            this.splitContainerLeft.Text = "splitContainerControl2";
            // 
            // groupEvents
            // 
            this.groupEvents.Controls.Add(this.splitContainerControl1);
            this.groupEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupEvents.Location = new System.Drawing.Point(0, 0);
            this.groupEvents.Name = "groupEvents";
            this.groupEvents.Size = new System.Drawing.Size(422, 320);
            this.groupEvents.TabIndex = 0;
            this.groupEvents.Text = "Рекламная и промо активность на момент переговоров";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(2, 22);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.MinSize = 110;
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel2.MinSize = 170;
            this.splitContainerControl1.Panel2.Padding = new System.Windows.Forms.Padding(10);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(418, 296);
            this.splitContainerControl1.SplitterPosition = 130;
            this.splitContainerControl1.TabIndex = 11;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.memoIsAdvertising);
            this.layoutControl1.Controls.Add(this.textCompanyPartner);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(418, 120);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 71);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(97, 13);
            this.labelControl2.StyleController = this.layoutControl1;
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Компания-спонсор:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(153, 13);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "Рекламируется ли заведение:";
            // 
            // memoIsAdvertising
            // 
            this.memoIsAdvertising.Location = new System.Drawing.Point(12, 29);
            this.memoIsAdvertising.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.memoIsAdvertising.Name = "memoIsAdvertising";
            this.memoIsAdvertising.Size = new System.Drawing.Size(394, 38);
            this.memoIsAdvertising.StyleController = this.layoutControl1;
            this.memoIsAdvertising.TabIndex = 16;
            this.memoIsAdvertising.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            // 
            // textCompanyPartner
            // 
            this.textCompanyPartner.Location = new System.Drawing.Point(12, 88);
            this.textCompanyPartner.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.textCompanyPartner.Name = "textCompanyPartner";
            this.textCompanyPartner.Size = new System.Drawing.Size(394, 20);
            this.textCompanyPartner.StyleController = this.layoutControl1;
            this.textCompanyPartner.TabIndex = 17;
            this.textCompanyPartner.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(418, 120);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textCompanyPartner;
            this.layoutControlItem1.CustomizationFormText = "Компания-спонсор:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 76);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(398, 24);
            this.layoutControlItem1.Text = "Компания-спонсор:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.memoIsAdvertising;
            this.layoutControlItem2.CustomizationFormText = "Рекламируется ли заведение";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(398, 42);
            this.layoutControlItem2.Text = "Рекламируется ли заведение";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControl1;
            this.layoutControlItem3.CustomizationFormText = "Рекламируется ли заведение";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(398, 17);
            this.layoutControlItem3.Text = "Рекламируется ли заведение";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControl2;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(398, 17);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControlEvents);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(10, 10);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(398, 150);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Проводимые мероприятия";
            // 
            // gridControlEvents
            // 
            this.gridControlEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlEvents.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlEvents.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControlEvents.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControlEvents.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControlEvents.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControlEvents.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControlEvents.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControlEvents.EmbeddedNavigator.TextStringFormat = "{0} из {1}";
            this.gridControlEvents.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlEvents_EmbeddedNavigator_ButtonClick);
            this.gridControlEvents.Location = new System.Drawing.Point(2, 22);
            this.gridControlEvents.MainView = this.gridViewEvents;
            this.gridControlEvents.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.gridControlEvents.Name = "gridControlEvents";
            this.gridControlEvents.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryEventsLookUp});
            this.gridControlEvents.Size = new System.Drawing.Size(394, 126);
            this.gridControlEvents.TabIndex = 10;
            this.gridControlEvents.UseEmbeddedNavigator = true;
            this.gridControlEvents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEvents});
            // 
            // gridViewEvents
            // 
            this.gridViewEvents.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnEventId,
            this.columnEventDescription});
            this.gridViewEvents.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewEvents.GridControl = this.gridControlEvents;
            this.gridViewEvents.Name = "gridViewEvents";
            this.gridViewEvents.OptionsView.ShowColumnHeaders = false;
            this.gridViewEvents.OptionsView.ShowDetailButtons = false;
            this.gridViewEvents.OptionsView.ShowGroupPanel = false;
            this.gridViewEvents.OptionsView.ShowIndicator = false;
            this.gridViewEvents.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.OnCellValueChanged);
            this.gridViewEvents.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewEvents_ValidateRow);
            // 
            // columnEventId
            // 
            this.columnEventId.Caption = "Мероприятие";
            this.columnEventId.ColumnEdit = this.repositoryEventsLookUp;
            this.columnEventId.FieldName = "ID";
            this.columnEventId.Name = "columnEventId";
            this.columnEventId.Visible = true;
            this.columnEventId.VisibleIndex = 0;
            // 
            // repositoryEventsLookUp
            // 
            this.repositoryEventsLookUp.AutoHeight = false;
            this.repositoryEventsLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryEventsLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Мероприятие")});
            this.repositoryEventsLookUp.DisplayMember = "NAME";
            this.repositoryEventsLookUp.Name = "repositoryEventsLookUp";
            this.repositoryEventsLookUp.NullText = "";
            this.repositoryEventsLookUp.ValueMember = "ID";
            // 
            // columnEventDescription
            // 
            this.columnEventDescription.FieldName = "ADVDESCRIPTION";
            this.columnEventDescription.Name = "columnEventDescription";
            this.columnEventDescription.Visible = true;
            this.columnEventDescription.VisibleIndex = 1;
            // 
            // groupPlannedChanges
            // 
            this.groupPlannedChanges.Controls.Add(this.gridControlPlannedChanges);
            this.groupPlannedChanges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPlannedChanges.Location = new System.Drawing.Point(0, 0);
            this.groupPlannedChanges.Name = "groupPlannedChanges";
            this.groupPlannedChanges.Size = new System.Drawing.Size(422, 341);
            this.groupPlannedChanges.TabIndex = 1;
            this.groupPlannedChanges.Text = "Планируемые изменения у клиента";
            // 
            // gridControlPlannedChanges
            // 
            this.gridControlPlannedChanges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlannedChanges.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPlannedChanges.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControlPlannedChanges.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControlPlannedChanges.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControlPlannedChanges.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControlPlannedChanges.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControlPlannedChanges.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControlPlannedChanges.EmbeddedNavigator.TextStringFormat = "{0} из {1}";
            this.gridControlPlannedChanges.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPlannedChanges_EmbeddedNavigator_ButtonClick);
            this.gridControlPlannedChanges.Location = new System.Drawing.Point(2, 22);
            this.gridControlPlannedChanges.MainView = this.gridViewPlannedChanges;
            this.gridControlPlannedChanges.Name = "gridControlPlannedChanges";
            this.gridControlPlannedChanges.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPlanChangeDateEdit,
            this.repositoryItemPlannedChangeTextEdit});
            this.gridControlPlannedChanges.Size = new System.Drawing.Size(418, 317);
            this.gridControlPlannedChanges.TabIndex = 4;
            this.gridControlPlannedChanges.UseEmbeddedNavigator = true;
            this.gridControlPlannedChanges.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlannedChanges});
            // 
            // gridViewPlannedChanges
            // 
            this.gridViewPlannedChanges.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPlannedChangeName,
            this.columnPlannedChangeDate});
            this.gridViewPlannedChanges.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewPlannedChanges.GridControl = this.gridControlPlannedChanges;
            this.gridViewPlannedChanges.Name = "gridViewPlannedChanges";
            this.gridViewPlannedChanges.OptionsView.ShowDetailButtons = false;
            this.gridViewPlannedChanges.OptionsView.ShowGroupPanel = false;
            this.gridViewPlannedChanges.OptionsView.ShowIndicator = false;
            this.gridViewPlannedChanges.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewPlannedChanges_InitNewRow);
            this.gridViewPlannedChanges.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.OnCellValueChanged);
            this.gridViewPlannedChanges.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewPlannedChanges_ValidateRow);
            // 
            // columnPlannedChangeName
            // 
            this.columnPlannedChangeName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPlannedChangeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPlannedChangeName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPlannedChangeName.Caption = "Изменение";
            this.columnPlannedChangeName.ColumnEdit = this.repositoryItemPlannedChangeTextEdit;
            this.columnPlannedChangeName.FieldName = "CHANGE_DESCRIPTION";
            this.columnPlannedChangeName.Name = "columnPlannedChangeName";
            this.columnPlannedChangeName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPlannedChangeName.Visible = true;
            this.columnPlannedChangeName.VisibleIndex = 0;
            this.columnPlannedChangeName.Width = 200;
            // 
            // repositoryItemPlannedChangeTextEdit
            // 
            this.repositoryItemPlannedChangeTextEdit.AutoHeight = false;
            this.repositoryItemPlannedChangeTextEdit.Name = "repositoryItemPlannedChangeTextEdit";
            // 
            // columnPlannedChangeDate
            // 
            this.columnPlannedChangeDate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPlannedChangeDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPlannedChangeDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPlannedChangeDate.Caption = "Дата";
            this.columnPlannedChangeDate.ColumnEdit = this.repositoryItemPlanChangeDateEdit;
            this.columnPlannedChangeDate.FieldName = "CHANGE_DATE";
            this.columnPlannedChangeDate.Name = "columnPlannedChangeDate";
            this.columnPlannedChangeDate.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPlannedChangeDate.Visible = true;
            this.columnPlannedChangeDate.VisibleIndex = 1;
            this.columnPlannedChangeDate.Width = 50;
            // 
            // repositoryItemPlanChangeDateEdit
            // 
            this.repositoryItemPlanChangeDateEdit.AutoHeight = false;
            this.repositoryItemPlanChangeDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPlanChangeDateEdit.Name = "repositoryItemPlanChangeDateEdit";
            this.repositoryItemPlanChangeDateEdit.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // splitContainerRight
            // 
            this.splitContainerRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerRight.Horizontal = false;
            this.splitContainerRight.Location = new System.Drawing.Point(0, 0);
            this.splitContainerRight.Name = "splitContainerRight";
            this.splitContainerRight.Panel1.Controls.Add(this.groupAdvert);
            this.splitContainerRight.Panel1.Text = "Panel1";
            this.splitContainerRight.Panel2.Controls.Add(this.groupPlannedAdvert);
            this.splitContainerRight.Panel2.Text = "Panel2";
            this.splitContainerRight.Size = new System.Drawing.Size(426, 667);
            this.splitContainerRight.SplitterPosition = 319;
            this.splitContainerRight.TabIndex = 2;
            this.splitContainerRight.Text = "splitContainerControl2";
            // 
            // groupAdvert
            // 
            this.groupAdvert.Controls.Add(this.gridControlMediums);
            this.groupAdvert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupAdvert.Location = new System.Drawing.Point(0, 0);
            this.groupAdvert.Name = "groupAdvert";
            this.groupAdvert.Size = new System.Drawing.Size(426, 319);
            this.groupAdvert.TabIndex = 1;
            this.groupAdvert.Text = "Используемые рекламные носители:";
            // 
            // gridControlMediums
            // 
            this.gridControlMediums.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMediums.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlMediums.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControlMediums.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControlMediums.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControlMediums.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControlMediums.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControlMediums.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControlMediums.EmbeddedNavigator.TextStringFormat = "{0} из {1}";
            this.gridControlMediums.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlMediums_EmbeddedNavigator_ButtonClick);
            this.gridControlMediums.Location = new System.Drawing.Point(2, 22);
            this.gridControlMediums.MainView = this.gridViewMediums;
            this.gridControlMediums.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.gridControlMediums.Name = "gridControlMediums";
            this.gridControlMediums.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMediumsLookUp});
            this.gridControlMediums.Size = new System.Drawing.Size(422, 295);
            this.gridControlMediums.TabIndex = 5;
            this.gridControlMediums.UseEmbeddedNavigator = true;
            this.gridControlMediums.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMediums});
            // 
            // gridViewMediums
            // 
            this.gridViewMediums.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnMediumId,
            this.columnMediumDescription});
            this.gridViewMediums.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewMediums.GridControl = this.gridControlMediums;
            this.gridViewMediums.Name = "gridViewMediums";
            this.gridViewMediums.OptionsView.ShowColumnHeaders = false;
            this.gridViewMediums.OptionsView.ShowDetailButtons = false;
            this.gridViewMediums.OptionsView.ShowGroupPanel = false;
            this.gridViewMediums.OptionsView.ShowIndicator = false;
            this.gridViewMediums.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.OnCellValueChanged);
            this.gridViewMediums.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewMediums_ValidateRow);
            // 
            // columnMediumId
            // 
            this.columnMediumId.Caption = "Реклама";
            this.columnMediumId.ColumnEdit = this.repositoryItemMediumsLookUp;
            this.columnMediumId.FieldName = "ID";
            this.columnMediumId.Name = "columnMediumId";
            this.columnMediumId.Visible = true;
            this.columnMediumId.VisibleIndex = 0;
            // 
            // repositoryItemMediumsLookUp
            // 
            this.repositoryItemMediumsLookUp.AutoHeight = false;
            this.repositoryItemMediumsLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMediumsLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Рекламные носители")});
            this.repositoryItemMediumsLookUp.DisplayMember = "NAME";
            this.repositoryItemMediumsLookUp.Name = "repositoryItemMediumsLookUp";
            this.repositoryItemMediumsLookUp.NullText = "";
            this.repositoryItemMediumsLookUp.ValueMember = "ID";
            // 
            // columnMediumDescription
            // 
            this.columnMediumDescription.FieldName = "MEDIUM_DESCRIPTION";
            this.columnMediumDescription.Name = "columnMediumDescription";
            this.columnMediumDescription.Visible = true;
            this.columnMediumDescription.VisibleIndex = 1;
            // 
            // groupPlannedAdvert
            // 
            this.groupPlannedAdvert.Controls.Add(this.gridControlPlannedAdvert);
            this.groupPlannedAdvert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPlannedAdvert.Location = new System.Drawing.Point(0, 0);
            this.groupPlannedAdvert.Name = "groupPlannedAdvert";
            this.groupPlannedAdvert.Size = new System.Drawing.Size(426, 342);
            this.groupPlannedAdvert.TabIndex = 1;
            this.groupPlannedAdvert.Text = "Планируемая рекламная и промо активность";
            // 
            // gridControlPlannedAdvert
            // 
            this.gridControlPlannedAdvert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlannedAdvert.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPlannedAdvert.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControlPlannedAdvert.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControlPlannedAdvert.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControlPlannedAdvert.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControlPlannedAdvert.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControlPlannedAdvert.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControlPlannedAdvert.EmbeddedNavigator.TextStringFormat = "{0} из {1}";
            this.gridControlPlannedAdvert.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPlannedAdvert_EmbeddedNavigator_ButtonClick);
            this.gridControlPlannedAdvert.Location = new System.Drawing.Point(2, 22);
            this.gridControlPlannedAdvert.MainView = this.gridViewPlannedAdvert;
            this.gridControlPlannedAdvert.Name = "gridControlPlannedAdvert";
            this.gridControlPlannedAdvert.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPlannedAdvTextEdit});
            this.gridControlPlannedAdvert.Size = new System.Drawing.Size(422, 318);
            this.gridControlPlannedAdvert.TabIndex = 4;
            this.gridControlPlannedAdvert.UseEmbeddedNavigator = true;
            this.gridControlPlannedAdvert.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlannedAdvert});
            // 
            // gridViewPlannedAdvert
            // 
            this.gridViewPlannedAdvert.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPlannedAdvName,
            this.columnPlannedAdvDescr});
            this.gridViewPlannedAdvert.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewPlannedAdvert.GridControl = this.gridControlPlannedAdvert;
            this.gridViewPlannedAdvert.Name = "gridViewPlannedAdvert";
            this.gridViewPlannedAdvert.OptionsView.ShowDetailButtons = false;
            this.gridViewPlannedAdvert.OptionsView.ShowGroupPanel = false;
            this.gridViewPlannedAdvert.OptionsView.ShowIndicator = false;
            this.gridViewPlannedAdvert.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewPlannedAdvert_InitNewRow);
            this.gridViewPlannedAdvert.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.OnCellValueChanged);
            this.gridViewPlannedAdvert.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewPlannedAdvert_ValidateRow);
            // 
            // columnPlannedAdvName
            // 
            this.columnPlannedAdvName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPlannedAdvName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPlannedAdvName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPlannedAdvName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnPlannedAdvName.Caption = "Промо или рекламная активность брендирование заведений";
            this.columnPlannedAdvName.ColumnEdit = this.repositoryItemPlannedAdvTextEdit;
            this.columnPlannedAdvName.FieldName = "ACTIVITY_NAME";
            this.columnPlannedAdvName.Name = "columnPlannedAdvName";
            this.columnPlannedAdvName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPlannedAdvName.Visible = true;
            this.columnPlannedAdvName.VisibleIndex = 0;
            // 
            // repositoryItemPlannedAdvTextEdit
            // 
            this.repositoryItemPlannedAdvTextEdit.AutoHeight = false;
            this.repositoryItemPlannedAdvTextEdit.Name = "repositoryItemPlannedAdvTextEdit";
            // 
            // columnPlannedAdvDescr
            // 
            this.columnPlannedAdvDescr.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPlannedAdvDescr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPlannedAdvDescr.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPlannedAdvDescr.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnPlannedAdvDescr.Caption = "Комментарии";
            this.columnPlannedAdvDescr.FieldName = "ACTIVITY_DESCRIPTION";
            this.columnPlannedAdvDescr.Name = "columnPlannedAdvDescr";
            this.columnPlannedAdvDescr.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPlannedAdvDescr.Visible = true;
            this.columnPlannedAdvDescr.VisibleIndex = 1;
            // 
            // ContractDetailsAdvert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.scrollableArea);
            this.Name = "ContractDetailsAdvert";
            this.Size = new System.Drawing.Size(854, 667);
            this.Load += new System.EventHandler(this.ContractDetailsAdvert_Load);
            this.scrollableArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
            this.splitContainerMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).EndInit();
            this.splitContainerLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupEvents)).EndInit();
            this.groupEvents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoIsAdvertising.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCompanyPartner.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryEventsLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPlannedChanges)).EndInit();
            this.groupPlannedChanges.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlannedChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlannedChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlannedChangeTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlanChangeDateEdit.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlanChangeDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerRight)).EndInit();
            this.splitContainerRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupAdvert)).EndInit();
            this.groupAdvert.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMediums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMediums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMediumsLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPlannedAdvert)).EndInit();
            this.groupPlannedAdvert.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlannedAdvert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlannedAdvert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPlannedAdvTextEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl scrollableArea;
        private DevExpress.XtraEditors.GroupControl groupEvents;
        private DevExpress.XtraEditors.GroupControl groupPlannedAdvert;
        private DevExpress.XtraEditors.GroupControl groupPlannedChanges;
        private DevExpress.XtraEditors.GroupControl groupAdvert;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerMain;
        private DevExpress.XtraGrid.GridControl gridControlPlannedChanges;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPlannedChanges;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerRight;
        private DevExpress.XtraGrid.GridControl gridControlPlannedAdvert;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPlannedAdvert;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerLeft;
        private DevExpress.XtraGrid.GridControl gridControlMediums;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMediums;
        private DevExpress.XtraGrid.Columns.GridColumn columnMediumId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemMediumsLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columnMediumDescription;
        private DevExpress.XtraGrid.Columns.GridColumn columnPlannedChangeName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPlannedChangeDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemPlanChangeDateEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnPlannedAdvName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPlannedAdvDescr;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemPlannedChangeTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemPlannedAdvTextEdit;
        private DevExpress.XtraGrid.GridControl gridControlEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEvents;
        private DevExpress.XtraGrid.Columns.GridColumn columnEventId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryEventsLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columnEventDescription;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.TextEdit textCompanyPartner;
        private DevExpress.XtraEditors.MemoEdit memoIsAdvertising;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;









    }
}
