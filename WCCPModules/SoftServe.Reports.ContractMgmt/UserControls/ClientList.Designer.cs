﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ClientListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientListControl));
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnLegalName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPositionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMobilePhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTotalContracts = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnNewContractsCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpirationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.standaloneTool = new DevExpress.XtraBars.Bar();
            this.barEditDateFrom = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateFrom = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEditDateTo = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateTo = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEditChannelCombo = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemLookUpChannel = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 34);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(800, 313);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnLegalName,
            this.columnContactPerson,
            this.columnPositionName,
            this.columnPhone,
            this.columnMobilePhone,
            this.columnAddress,
            this.columnTotalContracts,
            this.columnNewContractsCount,
            this.columnExpirationDate,
            this.columnChannel});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(177)))), ((int)(((byte)(100)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(177)))), ((int)(((byte)(100)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            this.gridView.DoubleClick += new System.EventHandler(this.gridView_DoubleClick);
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowShowHide = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // columnLegalName
            // 
            this.columnLegalName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnLegalName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnLegalName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnLegalName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnLegalName.Caption = "Юридическое имя";
            this.columnLegalName.FieldName = "LEGAL_NAME";
            this.columnLegalName.MinWidth = 100;
            this.columnLegalName.Name = "columnLegalName";
            this.columnLegalName.OptionsColumn.AllowEdit = false;
            this.columnLegalName.OptionsColumn.AllowShowHide = false;
            this.columnLegalName.OptionsColumn.ReadOnly = true;
            this.columnLegalName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnLegalName.Visible = true;
            this.columnLegalName.VisibleIndex = 0;
            this.columnLegalName.Width = 100;
            // 
            // columnContactPerson
            // 
            this.columnContactPerson.AppearanceHeader.Options.UseTextOptions = true;
            this.columnContactPerson.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnContactPerson.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnContactPerson.Caption = "Контактное лицо";
            this.columnContactPerson.FieldName = "ASSIGNEE";
            this.columnContactPerson.Name = "columnContactPerson";
            this.columnContactPerson.OptionsColumn.AllowEdit = false;
            this.columnContactPerson.OptionsColumn.AllowShowHide = false;
            this.columnContactPerson.OptionsColumn.ReadOnly = true;
            this.columnContactPerson.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnContactPerson.Visible = true;
            this.columnContactPerson.VisibleIndex = 1;
            // 
            // columnPositionName
            // 
            this.columnPositionName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPositionName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPositionName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPositionName.Caption = "Должность";
            this.columnPositionName.FieldName = "ASSIGNEE_APPOINTMENT";
            this.columnPositionName.Name = "columnPositionName";
            this.columnPositionName.OptionsColumn.AllowEdit = false;
            this.columnPositionName.OptionsColumn.AllowShowHide = false;
            this.columnPositionName.OptionsColumn.ReadOnly = true;
            this.columnPositionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPositionName.Visible = true;
            this.columnPositionName.VisibleIndex = 2;
            // 
            // columnPhone
            // 
            this.columnPhone.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPhone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPhone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "ASSIGNEE_PHONE";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowEdit = false;
            this.columnPhone.OptionsColumn.AllowShowHide = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 3;
            // 
            // columnMobilePhone
            // 
            this.columnMobilePhone.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMobilePhone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMobilePhone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMobilePhone.Caption = "Мобильный";
            this.columnMobilePhone.FieldName = "ASSIGNEE_MOBILE";
            this.columnMobilePhone.Name = "columnMobilePhone";
            this.columnMobilePhone.OptionsColumn.AllowEdit = false;
            this.columnMobilePhone.OptionsColumn.AllowShowHide = false;
            this.columnMobilePhone.OptionsColumn.ReadOnly = true;
            this.columnMobilePhone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnMobilePhone.Visible = true;
            this.columnMobilePhone.VisibleIndex = 4;
            // 
            // columnAddress
            // 
            this.columnAddress.AppearanceHeader.Options.UseTextOptions = true;
            this.columnAddress.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnAddress.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnAddress.Caption = "Юр. адрес";
            this.columnAddress.FieldName = "LEGAL_ADDRESS";
            this.columnAddress.Name = "columnAddress";
            this.columnAddress.OptionsColumn.AllowEdit = false;
            this.columnAddress.OptionsColumn.AllowShowHide = false;
            this.columnAddress.OptionsColumn.ReadOnly = true;
            this.columnAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnAddress.Visible = true;
            this.columnAddress.VisibleIndex = 5;
            // 
            // columnTotalContracts
            // 
            this.columnTotalContracts.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTotalContracts.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTotalContracts.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTotalContracts.Caption = "Активных конт-ов";
            this.columnTotalContracts.FieldName = "TOTAL_CONTRACTS";
            this.columnTotalContracts.Name = "columnTotalContracts";
            this.columnTotalContracts.OptionsColumn.AllowEdit = false;
            this.columnTotalContracts.OptionsColumn.AllowShowHide = false;
            this.columnTotalContracts.OptionsColumn.ReadOnly = true;
            this.columnTotalContracts.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnTotalContracts.Visible = true;
            this.columnTotalContracts.VisibleIndex = 6;
            // 
            // columnNewContractsCount
            // 
            this.columnNewContractsCount.AppearanceHeader.Options.UseTextOptions = true;
            this.columnNewContractsCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnNewContractsCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnNewContractsCount.Caption = "Новых конт-ов";
            this.columnNewContractsCount.FieldName = "NEW_CONTRACTS";
            this.columnNewContractsCount.Name = "columnNewContractsCount";
            this.columnNewContractsCount.OptionsColumn.AllowEdit = false;
            this.columnNewContractsCount.OptionsColumn.AllowShowHide = false;
            this.columnNewContractsCount.OptionsColumn.ReadOnly = true;
            this.columnNewContractsCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnNewContractsCount.Visible = true;
            this.columnNewContractsCount.VisibleIndex = 7;
            // 
            // columnExpirationDate
            // 
            this.columnExpirationDate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpirationDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpirationDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpirationDate.Caption = "Дата окончания";
            this.columnExpirationDate.FieldName = "EXPIRE_DATE";
            this.columnExpirationDate.Name = "columnExpirationDate";
            this.columnExpirationDate.OptionsColumn.AllowEdit = false;
            this.columnExpirationDate.OptionsColumn.AllowShowHide = false;
            this.columnExpirationDate.OptionsColumn.ReadOnly = true;
            this.columnExpirationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpirationDate.Visible = true;
            this.columnExpirationDate.VisibleIndex = 8;
            // 
            // columnChannel
            // 
            this.columnChannel.Caption = "On";
            this.columnChannel.FieldName = "CHANNEL";
            this.columnChannel.Name = "columnChannel";
            this.columnChannel.OptionsColumn.AllowEdit = false;
            this.columnChannel.OptionsColumn.ReadOnly = true;
            this.columnChannel.OptionsColumn.ShowInCustomizationForm = false;
            this.columnChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.standaloneTool});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditDateFrom,
            this.barEditDateTo,
            this.barEditChannelCombo});
            this.barManager.MaxItemId = 20;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateFrom,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemDateTo,
            this.repositoryItemColorEdit1,
            this.repositoryItemLookUpChannel});
            // 
            // standaloneTool
            // 
            this.standaloneTool.BarName = "Tools";
            this.standaloneTool.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.standaloneTool.DockCol = 0;
            this.standaloneTool.DockRow = 0;
            this.standaloneTool.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.standaloneTool.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditDateFrom),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditDateTo),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barEditChannelCombo, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.standaloneTool.OptionsBar.AllowQuickCustomization = false;
            this.standaloneTool.OptionsBar.DisableCustomization = true;
            this.standaloneTool.OptionsBar.DrawDragBorder = false;
            this.standaloneTool.OptionsBar.UseWholeRow = true;
            this.standaloneTool.Text = "Tools";
            // 
            // barEditDateFrom
            // 
            this.barEditDateFrom.Caption = "Клиент с истекающим контраком в период с:";
            this.barEditDateFrom.Edit = this.repositoryItemDateFrom;
            this.barEditDateFrom.Id = 11;
            this.barEditDateFrom.ImageIndex = 0;
            this.barEditDateFrom.Name = "barEditDateFrom";
            this.barEditDateFrom.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditDateFrom.Width = 100;
            this.barEditDateFrom.EditValueChanged += new System.EventHandler(this.barEditDateFrom_EditValueChanged);
            // 
            // repositoryItemDateFrom
            // 
            this.repositoryItemDateFrom.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateFrom.AutoHeight = false;
            this.repositoryItemDateFrom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateFrom.Name = "repositoryItemDateFrom";
            this.repositoryItemDateFrom.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // barEditDateTo
            // 
            this.barEditDateTo.Caption = "по:";
            this.barEditDateTo.Edit = this.repositoryItemDateTo;
            this.barEditDateTo.Id = 14;
            this.barEditDateTo.Name = "barEditDateTo";
            this.barEditDateTo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditDateTo.Width = 100;
            this.barEditDateTo.EditValueChanged += new System.EventHandler(this.barEditDateTo_EditValueChanged);
            // 
            // repositoryItemDateTo
            // 
            this.repositoryItemDateTo.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateTo.AutoHeight = false;
            this.repositoryItemDateTo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateTo.Name = "repositoryItemDateTo";
            this.repositoryItemDateTo.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // barEditChannelCombo
            // 
            this.barEditChannelCombo.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditChannelCombo.Caption = "Канал персонала:";
            this.barEditChannelCombo.Edit = this.repositoryItemLookUpChannel;
            this.barEditChannelCombo.Id = 19;
            this.barEditChannelCombo.Name = "barEditChannelCombo";
            this.barEditChannelCombo.Width = 100;
            this.barEditChannelCombo.EditValueChanged += new System.EventHandler(this.barEditChannelCombo_EditValueChanged);
            // 
            // repositoryItemLookUpChannel
            // 
            this.repositoryItemLookUpChannel.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemLookUpChannel.AutoHeight = false;
            this.repositoryItemLookUpChannel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpChannel.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType_id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "Канал")});
            this.repositoryItemLookUpChannel.DisplayMember = "ChanelType";
            this.repositoryItemLookUpChannel.Name = "repositoryItemLookUpChannel";
            this.repositoryItemLookUpChannel.NullText = "";
            this.repositoryItemLookUpChannel.ValueMember = "ChanelType_id";
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "highlight_expired.PNG");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            // 
            // ClientListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ClientListControl";
            this.Size = new System.Drawing.Size(800, 347);
            this.Load += new System.EventHandler(this.ClientListControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnLegalName;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar standaloneTool;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraGrid.Columns.GridColumn columnContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn columnPositionName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnMobilePhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnAddress;
        private DevExpress.XtraGrid.Columns.GridColumn columnTotalContracts;
        private DevExpress.XtraGrid.Columns.GridColumn columnNewContractsCount;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpirationDate;
        private DevExpress.XtraBars.BarEditItem barEditDateFrom;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateFrom;
        private DevExpress.XtraBars.BarEditItem barEditDateTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraBars.BarEditItem barEditChannelCombo;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpChannel;
        private DevExpress.XtraGrid.Columns.GridColumn columnChannel;


    }
}
