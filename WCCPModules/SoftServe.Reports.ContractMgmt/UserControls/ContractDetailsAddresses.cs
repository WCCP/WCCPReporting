﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.ContractMgmt.Forms;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// 
    /// </summary>
    public partial class ContractDetailsAddresses : CommonBaseControl {
        private const String ActivityColumnPrefix = "AFD_";
        private const string FieldOldStatus = "OldStatusColunm";
        private const string FieldInternalStatusMask = "InternalStatusMask";

        private readonly IList<string> _deletedIds = new List<string>();
        private readonly IList<string> _insertedIds = new List<string>();
        private readonly IList<string> _changedIds = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsAddresses"/> class.
        /// </summary>
        public ContractDetailsAddresses()
            : this(null) {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsAddresses"></see> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public ContractDetailsAddresses(CommonBaseTab parentTab) : base(parentTab) {
            InitializeComponent();

            if (!DesignMode) {
                manageAddressesList.InvisibleColumns = new[] {columnId, columnChannelId, columnOLId};
                manageAddressesList.FrozenColumns = gridView.Columns.Cast<GridColumn>().Where(x => x.Fixed != FixedStyle.None).ToArray();
            }

            manageAddressesList.OnDisplayFilterChanged += manageAddressesList_OnDisplayFilterChanged;
            IsDataLoaded = false;
        }

        /// <summary>
        /// Gets the controls grid control
        /// </summary>
        public DevExpress.XtraGrid.GridControl GridControl
        {
            get
            {
                return gridControlAddresses;
            }
        }

        /// <summary>
        /// Disables custom formating
        /// </summary>
        public void PrepareToExport()
        {
            gridColumnInBevPart.DisplayFormat.FormatType = FormatType.None;
        }

        /// <summary>
        /// Restores custom formating
        /// </summary>
        public void RestoreAfterExport()
        {
            gridColumnInBevPart.DisplayFormat.FormatType = FormatType.Numeric;
            gridColumnInBevPart.DisplayFormat.FormatString = "P";
        }

        /// <summary>
        /// Gets or sets the addresses list.
        /// </summary>
        /// <value>The addresses list.</value>
        private DataTable AddressesList {
            set {
                if (value != null)
                    AddActivityColumnsToGrid(value);

                gridControlAddresses.DataSource = value;
                if (value != null)
                    AddressesList.PrimaryKey = new DataColumn[] {AddressesList.Columns[columnOLId.FieldName]};
            }
            get {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable) gridControlAddresses.DataSource;
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData() {
            manageAddressesList.ChannelId = (int) MainData.Channel;
            repositoryItemLookUpStatus.DataSource = DataProvider.AddressStatusTable;

            if (MainData.IsEditMode) {
                DataTable lTable = DataProvider.GetAddressesList(MainData.ContractId);
                lTable.Columns.Add(FieldOldStatus);
                lTable.Columns.Add(FieldInternalStatusMask);
                foreach (DataRow lRow in lTable.Rows)
                    lRow[FieldInternalStatusMask] = RowStatusHelper.Unchanged;
                AddressesList = lTable;
            }
            else {
                FilterFromClientForm lFilter = new FilterFromClientForm(MainData.ClientId, (int) MainData.Channel,
                    String.Join(Constants.DELIMITER_ID_LIST, MainData.AddressesPocList.ToArray()),
                    String.Join(Constants.DELIMITER_ID_LIST, MainData.ControlGroupPocList.ToArray()));

                AddSelectedPOC(lFilter.POCList);
            }

            RefreshPOCList();
            MainData.HasAccessAddresses = ConvertEx.ToBool(DataProvider.GetHasAccessToAddresses(MainData.ContractId));
            base.LoadData();
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override int SaveData() {
            int lRetCode = 1;
            if(AddressesList!=null)
                AddressesList.AcceptChanges();
            PrepareAddressesList();
            string lDeletedIds = string.Join(Constants.DELIMITER_ID_LIST, _deletedIds.ToArray());
            string lInsertedIds = string.Join(Constants.DELIMITER_ID_LIST, _insertedIds.ToArray());
            string lChangedIds = string.Join(Constants.DELIMITER_ID_LIST, _changedIds.ToArray());

            if (string.IsNullOrEmpty(lDeletedIds) && string.IsNullOrEmpty(lInsertedIds) && string.IsNullOrEmpty(lChangedIds))
                return 1;

            if (!string.IsNullOrEmpty(lDeletedIds) || !string.IsNullOrEmpty(lInsertedIds))
                lRetCode = DataProvider.SetPocAddressesList(MainData.ContractId, false, lInsertedIds, lDeletedIds);

            if (lRetCode > 0 && !string.IsNullOrEmpty(lChangedIds)) {
                DataView lDataView = new DataView(gridControlAddresses.DataSource as DataTable);
                lDataView.RowFilter = string.Format("{0} IN ({1})", columnOLId.FieldName, lChangedIds);
                DataTable lDataTable = lDataView.ToTable();
                foreach (DataRow lRow in lDataTable.Rows)
                    //spDW_CM_SetOutletComment
                    DataProvider.SetOutletComment(MainData.ContractId, ConvertEx.ToLongInt(lRow[columnOLId.FieldName]), lRow[columnComment.FieldName].ToString());
            }
            if (lRetCode > 0)
                NormalizePocList();
            return lRetCode;
        }

        /// <summary>
        /// Normalizes the poc list.
        /// </summary>
        private void NormalizePocList() {
            DataTable lDataTable = gridControlAddresses.DataSource as DataTable;
            if (lDataTable == null || lDataTable.Rows.Count <= 0)
                return;

            foreach (DataRow lRow in lDataTable.Rows) {
                lRow[FieldInternalStatusMask] = RowStatusHelper.Unchanged;
                if (ConvertEx.ToInt(lRow[columnStatus.FieldName]) == (int) AddressRowStatus.Removed)
                    lRow.Delete();
                else
                    lRow[columnStatus.FieldName] = (int) AddressRowStatus.Common;
            }

            lDataTable.AcceptChanges();
        }

        /// <summary>
        /// Gets the addresses list.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public void PrepareAddressesList() {
            _deletedIds.Clear();
            _insertedIds.Clear();
            _changedIds.Clear();
            DataTable lDataTable = gridControlAddresses.DataSource as DataTable;
            if (lDataTable == null)
                return;
            
            foreach (DataRow lRow in lDataTable.Rows) {
                int lStatusMask = ConvertEx.ToInt(lRow[FieldInternalStatusMask]);
                bool lIsDeleted = (lStatusMask & RowStatusHelper.Deleted) == RowStatusHelper.Deleted;
                if (lIsDeleted)
                    _deletedIds.Add(lRow[columnOLId.FieldName].ToString());
                if (!lIsDeleted && (lStatusMask & RowStatusHelper.Inserted) == RowStatusHelper.Inserted)
                    _insertedIds.Add(lRow[columnOLId.FieldName].ToString());
                if (!lIsDeleted && (lStatusMask & RowStatusHelper.Changed) == RowStatusHelper.Changed)
                    _changedIds.Add(lRow[columnOLId.FieldName].ToString());
            }
        }

        /// <summary>
        /// Gets the all addresses.
        /// </summary>
        /// <returns>List of all addresses</returns>
        public string GetAliveAddresses() {
            DataTable lTable = gridControlAddresses.DataSource as DataTable;
            if (lTable == null)
                return null;

            return string.Join(Constants.DELIMITER_ID_LIST, (from d in lTable.AsEnumerable()
                                                             let status = (AddressRowStatus) d.Field<int>(columnStatus.FieldName)
                                                             where status != AddressRowStatus.Removed
                                                             select d.Field<String>(columnPOCCode.FieldName)).ToArray());
        }

        /// <summary>
        /// Manages the addresses list_ on filter changed.
        /// </summary>
        /// <param name="args">The args.</param>
        private void manageAddressesList_OnFilterChanged(FilterChangedArgs args) {
            AddSelectedPOC(args.PocIds);
        }

        /// <summary>
        /// Manages the addresses list_ custom filter showing.
        /// </summary>
        /// <param name="args">The args.</param>
        private void manageAddressesList_CustomFilterShowing(CustomFilterShowingArgs args) {
            args.FilterForm = new FilterFromClientForm(MainData.ClientId, (int) MainData.Channel,
                String.Join(Constants.DELIMITER_ID_LIST, MainData.AddressesPocList.ToArray()),
                String.Join(Constants.DELIMITER_ID_LIST, MainData.ControlGroupPocList.ToArray()));
        }

        /// <summary>
        /// Handles the KeyDown event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
        private void gridView_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Delete)
                RemoveSelectedPOC();
        }

        private void manageAddressesList_DeleteBtnClick(object sender, EventArgs e) {
            RemoveSelectedPOC();
        }

        /// <summary>
        /// Handles the DataSourceChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DataSourceChanged(object sender, EventArgs e) {
            FireUpdateMainView();
        }

        /// <summary>
        /// Handles the Validating event of the manageAddressesList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void manageAddressesList_Validating(object sender, CancelEventArgs e) {
            e.Cancel = MainData.AddressesPocList.Count == 0;
        }

        /// <summary>
        /// Removes the selected addresses.
        /// </summary>
        private void RemoveSelectedPOC() {
            bool lAllowRemove = manageAddressesList.EditMode == AddressEditMode.AllowAll || manageAddressesList.EditMode == AddressEditMode.AllowRemoveOnly;
            if (!MainData.AllowAddressChange || gridView.SelectedRowsCount <= 0 || !lAllowRemove)
                return;
            
            gridView.BeginUpdate();
            try {
                int[] lSelectedRows = gridView.GetSelectedRows();
                foreach (int lRowHandle in lSelectedRows) {
                    DataRowView lRow = gridView.GetRow(lRowHandle) as DataRowView;
                    AddressRowStatus lCurStatus = (AddressRowStatus) Convert.ToInt32(lRow[columnStatus.FieldName]);
                    lRow[FieldInternalStatusMask] = ConvertEx.ToInt(lRow[FieldInternalStatusMask]) | RowStatusHelper.Deleted;
                    if (lCurStatus == AddressRowStatus.Removed)
                        lRow[columnStatus.FieldName] = lRow[FieldOldStatus];
                    else {
                        lRow[FieldOldStatus] = (int) lCurStatus;
                        lRow[columnStatus.FieldName] = (int) AddressRowStatus.Removed;
                    }

                    //gridView.UpdateCurrentRow();
                }
                OnChanged(true);
                RefreshPOCList();
                IsCalcVRequired = true;
            }
            finally {
                gridView.EndUpdate();
            }
        }

        /// <summary>
        /// Adds the POC list to Adreska.
        /// </summary>
        /// <param name="strPOCIdList">list of POC Ids delimited by comma</param>
        private void AddSelectedPOC(string strPOCIdList) {
            if (!MainData.AllowAddressChange || string.IsNullOrEmpty(strPOCIdList))
                return;

            DataTable lDataTable = DataProvider.GetAddressesListByOLIDs(strPOCIdList, MainData.DateSignPlanned, MainData.ContractTerm, (int) MainData.Channel);
            if (lDataTable == null || lDataTable.Rows.Count <= 0)
                return;

            lDataTable.PrimaryKey = new DataColumn[] {lDataTable.Columns[columnOLId.FieldName]};
            //TODO: extract method AddInternalStatusMask(lDataTable);
            lDataTable.Columns.Add(FieldInternalStatusMask);
            foreach (DataRow lRow in lDataTable.Rows)
                lRow[FieldInternalStatusMask] = RowStatusHelper.Unchanged;

            foreach (DataRow lRow in lDataTable.Rows) {
                lRow[columnStatus.FieldName] = AddressRowStatus.New;
                lRow[FieldInternalStatusMask] = ConvertEx.ToInt(lRow[FieldInternalStatusMask]) | RowStatusHelper.Inserted;
            }

            if (AddressesList == null)
                AddressesList = lDataTable;
            else
                AddressesList.Merge(lDataTable, false, MissingSchemaAction.Ignore);

            RefreshPOCList();
            OnChanged(true);
            IsCalcVRequired = true;
            FireUpdateMainView();
        }

        /// <summary>
        /// Refreshes the external data.
        /// </summary>
        private void RefreshPOCList() {
            if (MainData.AddressesPocList == null)
                return;

            MainData.AddressesPocList.Clear();
            string lStrAliveAddresses = GetAliveAddresses();
            if (!string.IsNullOrEmpty(lStrAliveAddresses))
                MainData.AddressesPocList = lStrAliveAddresses.Split(Constants.DELIMITER_ID_LIST.ToCharArray()).ToList();
        }

        /// <summary>
        /// Adds the activity columns to grid.
        /// </summary>
        /// <param name="table">The table.</param>
        private void AddActivityColumnsToGrid(DataTable table) {
            IEnumerable<GridColumn> lColumns = gridView.Columns.Cast<GridColumn>();

            foreach (DataColumn lColumn in table.Columns) {
                if (!lColumn.ColumnName.StartsWith(ActivityColumnPrefix) || lColumns.Where(x => x.FieldName == lColumn.ColumnName).Count() > 0)
                    continue;

                gridView.Columns.Add(NewGridColumn(
                    lColumn.ColumnName,
                    lColumn.ColumnName.Substring(ActivityColumnPrefix.Length)
                    ));
            }

            manageAddressesList.FrozenColumns = manageAddressesList.FrozenColumns;
        }

        /// <summary>
        /// News the grid column.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="displayName">The display name.</param>
        /// <returns></returns>
        private GridColumn NewGridColumn(String fieldName, String displayName) {
            GridColumn lColumn = new GridColumn();

            lColumn.AppearanceHeader.Options.UseTextOptions = true;
            lColumn.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            lColumn.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
            lColumn.Caption = displayName;
            lColumn.ColumnEdit = repositoryActivity;
            lColumn.FieldName = fieldName;
            lColumn.MinWidth = 50;
            lColumn.OptionsColumn.AllowEdit = false;
            lColumn.OptionsColumn.AllowMove = false;
            lColumn.OptionsColumn.ReadOnly = true;
            lColumn.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
            lColumn.Visible = false;
            lColumn.Width = 100;
            lColumn.ColumnEdit = repositoryActivity;

            return lColumn;
        }

        /// <summary>
        /// Addresseses the list CTRL_ on display filter changed.
        /// </summary>
        /// <param name="args">The args.</param>
        private void manageAddressesList_OnDisplayFilterChanged(FilterChangedArgs args) {
            string lFilter = String.Format("[{0}] In ({1})", columnPOCCode.FieldName, args.PocIds);
            columnPOCCode.FilterInfo = new ColumnFilterInfo(ColumnFilterType.Custom, lFilter);
        }

        private void gridControlAddresses_KeyDown(object sender, KeyEventArgs e) {
            if ((e.KeyCode == Keys.V) && e.Control)
                manageAddressesList.FilterDisplayedAddresses();
        }

        /// <summary>
        /// Validates data in controls and returns appropriate validation result.
        /// </summary>
        /// <returns></returns>
        public override ValidateMetadata ValidateData() {
            ValidateMetadata lResult = base.ValidateData();
            bool lIsGridValid = ValidateAddressesGrid();
            if (!lIsGridValid) {
                lResult.IsCorrect = false;
                lResult.Message = Resource.CantSaveContractWithoutAddresses;
            }
            return lResult;
        }

        /// <summary>
        /// Validates data in AddressesGrid and returns value that indicates whether data is correct.
        /// </summary>
        /// <returns></returns>
        private bool ValidateAddressesGrid() {
            return MainData.AddressesPocList.Count > 0;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e) {
            if (e.Column.FieldName == "COMMENT") {
                OnChanged(true);
                if (ConvertEx.ToInt(AddressesList.Rows[e.RowHandle][columnStatus.FieldName]) != (int) AddressRowStatus.Removed)
                    AddressesList.Rows[e.RowHandle][columnStatus.FieldName] = AddressRowStatus.Edited;
                else
                    AddressesList.Rows[e.RowHandle][FieldOldStatus] = AddressRowStatus.Edited;
                AddressesList.Rows[e.RowHandle][FieldInternalStatusMask] = ConvertEx.ToInt(AddressesList.Rows[e.RowHandle][FieldInternalStatusMask]) | RowStatusHelper.Changed;
            }
        }
    }
}