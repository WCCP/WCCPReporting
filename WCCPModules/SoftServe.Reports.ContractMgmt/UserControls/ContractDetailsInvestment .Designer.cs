﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ContractDetailsInvestment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbPaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPaymentSchema = new DevExpress.XtraEditors.LookUpEdit();
            this.spinTrandedAvrVolume = new DevExpress.XtraEditors.SpinEdit();
            this.spinMarketTrend = new DevExpress.XtraEditors.SpinEdit();
            this.spinPrevAvrVolume = new DevExpress.XtraEditors.SpinEdit();
            this.spinExpensesPerOutlet = new DevExpress.XtraEditors.SpinEdit();
            this.spinPlannedOutletsQty = new DevExpress.XtraEditors.SpinEdit();
            this.spinUpliftedAvrVolume = new DevExpress.XtraEditors.SpinEdit();
            this.spinPlannedVolumeUplift = new DevExpress.XtraEditors.SpinEdit();
            this.spinPlannedAvrMacoPerDal = new DevExpress.XtraEditors.SpinEdit();
            this.expensesTree = new Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.ExpensesTree();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.scrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.controlIndicators = new Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.IndicatorsGrid();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.cbPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.cbPaymentSchema.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinTrandedAvrVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinMarketTrend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPrevAvrVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinExpensesPerOutlet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPlannedOutletsQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinUpliftedAvrVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPlannedVolumeUplift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPlannedAvrMacoPerDal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem2)).BeginInit();
            this.scrollableControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.AutoScroll = false;
            this.layoutControl.Controls.Add(this.groupControl1);
            this.layoutControl.Controls.Add(this.expensesTree);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(904, 340);
            this.layoutControl.TabIndex = 10;
            this.layoutControl.Text = "layoutControl1";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl16);
            this.groupControl1.Controls.Add(this.labelControl17);
            this.groupControl1.Controls.Add(this.labelControl15);
            this.groupControl1.Controls.Add(this.labelControl14);
            this.groupControl1.Controls.Add(this.labelControl12);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl18);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.cbPaymentType);
            this.groupControl1.Controls.Add(this.cbPaymentSchema);
            this.groupControl1.Controls.Add(this.spinTrandedAvrVolume);
            this.groupControl1.Controls.Add(this.spinMarketTrend);
            this.groupControl1.Controls.Add(this.spinPrevAvrVolume);
            this.groupControl1.Controls.Add(this.spinExpensesPerOutlet);
            this.groupControl1.Controls.Add(this.spinPlannedOutletsQty);
            this.groupControl1.Controls.Add(this.spinUpliftedAvrVolume);
            this.groupControl1.Controls.Add(this.spinPlannedVolumeUplift);
            this.groupControl1.Controls.Add(this.spinPlannedAvrMacoPerDal);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(288, 316);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Общая информация";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 120);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(151, 13);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "Тренд рынка (рост/падение):";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 176);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(117, 13);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Планируемый прирост:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(12, 289);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(130, 13);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "План среднее МАСО/дал:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(12, 261);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(97, 13);
            this.labelControl9.TabIndex = 13;
            this.labelControl9.Text = "План затрат на TT:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 232);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(123, 13);
            this.labelControl7.TabIndex = 13;
            this.labelControl7.Text = "Планируемое кол-во TT:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 204);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(129, 13);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "Сред. V/TT после старта:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 148);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(141, 13);
            this.labelControl4.TabIndex = 19;
            this.labelControl4.Text = "Сред. V/TT с учетом рынка:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 90);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(132, 13);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Сред. V/TT до контракта:";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(239, 289);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(17, 13);
            this.labelControl16.TabIndex = 17;
            this.labelControl16.Text = "грн";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(239, 261);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(17, 13);
            this.labelControl17.TabIndex = 17;
            this.labelControl17.Text = "грн";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(239, 232);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(14, 13);
            this.labelControl15.TabIndex = 17;
            this.labelControl15.Text = "шт";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(239, 204);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(19, 13);
            this.labelControl14.TabIndex = 17;
            this.labelControl14.Text = "дал";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(239, 148);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(19, 13);
            this.labelControl12.TabIndex = 17;
            this.labelControl12.Text = "дал";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(239, 90);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(19, 13);
            this.labelControl10.TabIndex = 17;
            this.labelControl10.Text = "дал";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(12, 60);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(63, 13);
            this.labelControl18.TabIndex = 17;
            this.labelControl18.Text = "Тип оплаты:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 13);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Схема оплаты:";
            // 
            // cbPaymentType
            // 
            this.cbPaymentType.Location = new System.Drawing.Point(94, 56);
            this.cbPaymentType.Name = "cbPaymentType";
            this.cbPaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPaymentType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYMENT_NAME", "Тип оплаты")});
            this.cbPaymentType.Properties.DisplayMember = "PAYMENT_NAME";
            this.cbPaymentType.Properties.NullText = "Не выбрано";
            this.cbPaymentType.Properties.ValueMember = "PAYMENTKIND_ID";
            this.cbPaymentType.Size = new System.Drawing.Size(139, 20);
            this.cbPaymentType.TabIndex = 11;
            this.cbPaymentType.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.cbPaymentType.Enter += new System.EventHandler(this.controlGetFocus);
            this.cbPaymentType.Leave += new System.EventHandler(this.controlLostFocus);
            this.cbPaymentType.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // cbPaymentSchema
            // 
            this.cbPaymentSchema.Location = new System.Drawing.Point(94, 28);
            this.cbPaymentSchema.Name = "cbPaymentSchema";
            this.cbPaymentSchema.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPaymentSchema.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SCHEMA_NAME", "Схема оплаты")});
            this.cbPaymentSchema.Properties.DisplayMember = "SCHEMA_NAME";
            this.cbPaymentSchema.Properties.NullText = "Не выбрано";
            this.cbPaymentSchema.Properties.ValueMember = "ID";
            this.cbPaymentSchema.Size = new System.Drawing.Size(139, 20);
            this.cbPaymentSchema.TabIndex = 11;
            this.cbPaymentSchema.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.cbPaymentSchema.Enter += new System.EventHandler(this.controlGetFocus);
            this.cbPaymentSchema.Leave += new System.EventHandler(this.controlLostFocus);
            this.cbPaymentSchema.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // spinTrandedAvrVolume
            // 
            this.spinTrandedAvrVolume.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinTrandedAvrVolume.Location = new System.Drawing.Point(169, 144);
            this.spinTrandedAvrVolume.Name = "spinTrandedAvrVolume";
            this.spinTrandedAvrVolume.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, null, null, true)});
            this.spinTrandedAvrVolume.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinTrandedAvrVolume.Properties.DisplayFormat.FormatString = "{0:N3}";
            this.spinTrandedAvrVolume.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinTrandedAvrVolume.Properties.Mask.EditMask = "n3";
            this.spinTrandedAvrVolume.Properties.MaxLength = 10000000;
            this.spinTrandedAvrVolume.Properties.ReadOnly = true;
            this.spinTrandedAvrVolume.Size = new System.Drawing.Size(64, 20);
            this.spinTrandedAvrVolume.TabIndex = 6;
            // 
            // spinMarketTrend
            // 
            this.spinMarketTrend.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinMarketTrend.Location = new System.Drawing.Point(169, 116);
            this.spinMarketTrend.Name = "spinMarketTrend";
            this.spinMarketTrend.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinMarketTrend.Properties.DisplayFormat.FormatString = "p1";
            this.spinMarketTrend.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinMarketTrend.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.spinMarketTrend.Properties.Mask.EditMask = "p1";
            this.spinMarketTrend.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinMarketTrend.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.spinMarketTrend.Size = new System.Drawing.Size(64, 20);
            this.spinMarketTrend.TabIndex = 7;
            this.spinMarketTrend.Enter += new System.EventHandler(this.controlGetFocus);
            this.spinMarketTrend.Leave += new System.EventHandler(this.controlLostFocus);
            // 
            // spinPrevAvrVolume
            // 
            this.spinPrevAvrVolume.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPrevAvrVolume.Location = new System.Drawing.Point(169, 86);
            this.spinPrevAvrVolume.Name = "spinPrevAvrVolume";
            this.spinPrevAvrVolume.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, null, null, true)});
            this.spinPrevAvrVolume.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinPrevAvrVolume.Properties.DisplayFormat.FormatString = "{0:N3}";
            this.spinPrevAvrVolume.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPrevAvrVolume.Properties.Mask.EditMask = "n3";
            this.spinPrevAvrVolume.Properties.MaxLength = 10000000;
            this.spinPrevAvrVolume.Properties.ReadOnly = true;
            this.spinPrevAvrVolume.Size = new System.Drawing.Size(64, 20);
            this.spinPrevAvrVolume.TabIndex = 4;
            // 
            // spinExpensesPerOutlet
            // 
            this.spinExpensesPerOutlet.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinExpensesPerOutlet.Location = new System.Drawing.Point(169, 257);
            this.spinExpensesPerOutlet.Name = "spinExpensesPerOutlet";
            this.spinExpensesPerOutlet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, null, null, true)});
            this.spinExpensesPerOutlet.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinExpensesPerOutlet.Properties.DisplayFormat.FormatString = "N2";
            this.spinExpensesPerOutlet.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinExpensesPerOutlet.Properties.Mask.EditMask = "n2";
            this.spinExpensesPerOutlet.Properties.MaxLength = 10000000;
            this.spinExpensesPerOutlet.Properties.ReadOnly = true;
            this.spinExpensesPerOutlet.Size = new System.Drawing.Size(64, 20);
            this.spinExpensesPerOutlet.TabIndex = 5;
            // 
            // spinPlannedOutletsQty
            // 
            this.spinPlannedOutletsQty.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPlannedOutletsQty.Location = new System.Drawing.Point(169, 228);
            this.spinPlannedOutletsQty.Name = "spinPlannedOutletsQty";
            this.spinPlannedOutletsQty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, null, null, true)});
            this.spinPlannedOutletsQty.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinPlannedOutletsQty.Properties.DisplayFormat.FormatString = "N0";
            this.spinPlannedOutletsQty.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPlannedOutletsQty.Properties.IsFloatValue = false;
            this.spinPlannedOutletsQty.Properties.Mask.EditMask = "n0";
            this.spinPlannedOutletsQty.Properties.MaxLength = 10000000;
            this.spinPlannedOutletsQty.Properties.ReadOnly = true;
            this.spinPlannedOutletsQty.Size = new System.Drawing.Size(64, 20);
            this.spinPlannedOutletsQty.TabIndex = 5;
            // 
            // spinUpliftedAvrVolume
            // 
            this.spinUpliftedAvrVolume.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinUpliftedAvrVolume.Location = new System.Drawing.Point(169, 200);
            this.spinUpliftedAvrVolume.Name = "spinUpliftedAvrVolume";
            this.spinUpliftedAvrVolume.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, global::SoftServe.Reports.ContractMgmt.Resource.TabSaveExternalChanges, null, null, true)});
            this.spinUpliftedAvrVolume.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinUpliftedAvrVolume.Properties.DisplayFormat.FormatString = "{0:N3}";
            this.spinUpliftedAvrVolume.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinUpliftedAvrVolume.Properties.Mask.EditMask = "n3";
            this.spinUpliftedAvrVolume.Properties.MaxLength = 10000000;
            this.spinUpliftedAvrVolume.Properties.ReadOnly = true;
            this.spinUpliftedAvrVolume.Size = new System.Drawing.Size(64, 20);
            this.spinUpliftedAvrVolume.TabIndex = 10;
            // 
            // spinPlannedVolumeUplift
            // 
            this.spinPlannedVolumeUplift.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPlannedVolumeUplift.Location = new System.Drawing.Point(169, 172);
            this.spinPlannedVolumeUplift.Name = "spinPlannedVolumeUplift";
            this.spinPlannedVolumeUplift.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinPlannedVolumeUplift.Properties.DisplayFormat.FormatString = "p1";
            this.spinPlannedVolumeUplift.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPlannedVolumeUplift.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.spinPlannedVolumeUplift.Properties.Mask.EditMask = "p1";
            this.spinPlannedVolumeUplift.Properties.MaxLength = 100;
            this.spinPlannedVolumeUplift.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinPlannedVolumeUplift.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.spinPlannedVolumeUplift.Size = new System.Drawing.Size(64, 20);
            this.spinPlannedVolumeUplift.TabIndex = 9;
            this.spinPlannedVolumeUplift.Enter += new System.EventHandler(this.controlGetFocus);
            this.spinPlannedVolumeUplift.Leave += new System.EventHandler(this.controlLostFocus);
            // 
            // spinPlannedAvrMacoPerDal
            // 
            this.spinPlannedAvrMacoPerDal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPlannedAvrMacoPerDal.Location = new System.Drawing.Point(169, 285);
            this.spinPlannedAvrMacoPerDal.Name = "spinPlannedAvrMacoPerDal";
            this.spinPlannedAvrMacoPerDal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.spinPlannedAvrMacoPerDal.Properties.DisplayFormat.FormatString = "{0:N2}";
            this.spinPlannedAvrMacoPerDal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPlannedAvrMacoPerDal.Properties.Mask.EditMask = "n2";
            this.spinPlannedAvrMacoPerDal.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spinPlannedAvrMacoPerDal.Properties.ReadOnly = true;
            this.spinPlannedAvrMacoPerDal.Size = new System.Drawing.Size(64, 20);
            this.spinPlannedAvrMacoPerDal.TabIndex = 8;
            // 
            // expensesTree
            // 
            this.expensesTree.Location = new System.Drawing.Point(304, 12);
            this.expensesTree.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.expensesTree.Name = "expensesTree";
            this.expensesTree.ReadOnly = false;
            this.expensesTree.Size = new System.Drawing.Size(588, 316);
            this.expensesTree.TabIndex = 8;
            this.expensesTree.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(904, 340);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(292, 320);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.expensesTree;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(292, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(592, 320);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // scrollableControl
            // 
            this.scrollableControl.AlwaysScrollActiveControlIntoView = false;
            this.scrollableControl.AutoScrollMinSize = new System.Drawing.Size(650, 668);
            this.scrollableControl.Controls.Add(this.controlIndicators);
            this.scrollableControl.Controls.Add(this.layoutControl);
            this.scrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollableControl.Location = new System.Drawing.Point(0, 0);
            this.scrollableControl.Name = "scrollableControl";
            this.scrollableControl.Size = new System.Drawing.Size(904, 668);
            this.scrollableControl.TabIndex = 11;
            // 
            // controlIndicators
            // 
            this.controlIndicators.AllowEditingForCurrentMonth = false;
            this.controlIndicators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlIndicators.EditMode = Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.DataAccess.IndicatorEditMode.Common;
            this.controlIndicators.Location = new System.Drawing.Point(0, 340);
            this.controlIndicators.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.controlIndicators.MinimumSize = new System.Drawing.Size(0, 310);
            this.controlIndicators.Name = "controlIndicators";
            this.controlIndicators.Size = new System.Drawing.Size(904, 328);
            this.controlIndicators.TabIndex = 9;
            this.controlIndicators.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.controlIndicators.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // ContractDetailsInvestment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.scrollableControl);
            this.Name = "ContractDetailsInvestment";
            this.Size = new System.Drawing.Size(904, 668);
            ((System.ComponentModel.ISupportInitialize) (this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.cbPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.cbPaymentSchema.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinTrandedAvrVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinMarketTrend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPrevAvrVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinExpensesPerOutlet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPlannedOutletsQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinUpliftedAvrVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPlannedVolumeUplift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.spinPlannedAvrMacoPerDal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.layoutControlItem2)).EndInit();
            this.scrollableControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit cbPaymentSchema;
        private DevExpress.XtraEditors.SpinEdit spinTrandedAvrVolume;
        private DevExpress.XtraEditors.SpinEdit spinMarketTrend;
        private DevExpress.XtraEditors.SpinEdit spinPrevAvrVolume;
        private DevExpress.XtraEditors.SpinEdit spinExpensesPerOutlet;
        private DevExpress.XtraEditors.SpinEdit spinPlannedOutletsQty;
        private DevExpress.XtraEditors.SpinEdit spinUpliftedAvrVolume;
        private DevExpress.XtraEditors.SpinEdit spinPlannedVolumeUplift;
        private DevExpress.XtraEditors.SpinEdit spinPlannedAvrMacoPerDal;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LookUpEdit cbPaymentType;
        private Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.ExpensesTree expensesTree;
        private Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.IndicatorsGrid controlIndicators;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.XtraScrollableControl scrollableControl;











    }
}
