﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ContractDetailsCommon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scrollableArea = new DevExpress.XtraEditors.XtraScrollableControl();
            this.mainSplitterControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.contractCreatorPanelCtrl = new DevExpress.XtraEditors.PanelControl();
            this.lblDCRName = new DevExpress.XtraEditors.LabelControl();
            this.lblIsBudget = new DevExpress.XtraEditors.LabelControl();
            this.marketPlacesDistrPanel = new DevExpress.XtraEditors.PanelControl();
            this.distributorslayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.groupDistributors1 = new DevExpress.XtraEditors.GroupControl();
            this.listDistributorsRest = new DevExpress.XtraEditors.ListBoxControl();
            this.groupDistributors2 = new DevExpress.XtraEditors.GroupControl();
            this.listDistributorsKeg = new DevExpress.XtraEditors.ListBoxControl();
            this.distributorsLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.groupDistributors1LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupDistributors2LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlPOCList = new DevExpress.XtraGrid.GridControl();
            this.gridViewPOCList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnPOCPremiality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.monthConcurentAvgSalesLayoutCtrl = new DevExpress.XtraLayout.LayoutControl();
            this.groupAvgSales = new DevExpress.XtraEditors.GroupControl();
            this.monthlySalesInBev = new Logica.Reports.BaseReportControl.CommonControls.Editors.MonthlySales();
            this.gropuSalesAvgCompetitors = new DevExpress.XtraEditors.GroupControl();
            this.monthlySalesCompetitors = new Logica.Reports.BaseReportControl.CommonControls.Editors.MonthlySales();
            this.monthSalesLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.monthAvgInBevSalesLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.monthAvgBerrSalesLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupClientInfo = new DevExpress.XtraEditors.GroupControl();
            this.gridControlClientInfo = new DevExpress.XtraGrid.GridControl();
            this.gridViewClientInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnClientId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnClientItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnClientItemValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupContractInfo = new DevExpress.XtraEditors.GroupControl();
            this.ContractInfoLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.chDontUseBaskets = new DevExpress.XtraEditors.CheckEdit();
            this.cbBudgetOwner = new DevExpress.XtraEditors.LookUpEdit();
            this.lblSignDate = new DevExpress.XtraEditors.LabelControl();
            this.lblContractNum = new DevExpress.XtraEditors.LabelControl();
            this.lblDateSignPlanStart = new DevExpress.XtraEditors.LabelControl();
            this.cbBudgetType = new DevExpress.XtraEditors.LookUpEdit();
            this.lblContractTerm = new DevExpress.XtraEditors.LabelControl();
            this.editContractNum = new DevExpress.XtraEditors.TextEdit();
            this.dateSignDate = new DevExpress.XtraEditors.DateEdit();
            this.lblDatePlanSign = new DevExpress.XtraEditors.LabelControl();
            this.spinContractTerm = new DevExpress.XtraEditors.SpinEdit();
            this.dateSignPlanned = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.memoContractCommentAfter = new DevExpress.XtraEditors.MemoEdit();
            this.lblContractChannel = new DevExpress.XtraEditors.LabelControl();
            this.memoContractComment = new DevExpress.XtraEditors.MemoEdit();
            this.contractTermStart = new DevExpress.XtraEditors.LabelControl();
            this.photoUploader = new Logica.Reports.BaseReportControl.CommonControls.FileUploader();
            this.contractInfoLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.contractNumLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ContractTermLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DatePlanSignLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ContractChannelLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.singDatelayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.photoUploaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.contractTermStartLayoutCtrlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblContractTermLayoutCtrlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblDatePlanSignLayoutCtrlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblDateSignPlanStartLayoutCtrlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.l = new DevExpress.XtraLayout.LayoutControlItem();
            this.dateSignLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.CommentsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBudgetType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBudgetOwner = new DevExpress.XtraLayout.LayoutControlItem();
            this.CommentAfterLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.panelGrids = new System.Windows.Forms.Panel();
            this.gridControlBudgetPlan = new DevExpress.XtraGrid.GridControl();
            this.gridViewBudgetPlan = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnBudgetPlanId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnBudgetPlanStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnBudgetPlanName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnBudgetPlanTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControlCompetitors = new DevExpress.XtraEditors.GroupControl();
            this.gridControlCompetitors = new DevExpress.XtraGrid.GridControl();
            this.gridViewCompetitors = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnCompetitorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryCompetitorId = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columnCompetitorContractSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryContractSumEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.columnCompetitorRetrobonus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryContractRetrobonusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.columnCopetitorStitchedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryContractStitchedOnEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.groupPrevContract = new DevExpress.XtraEditors.GroupControl();
            this.gridControlPrevContract = new DevExpress.XtraGrid.GridControl();
            this.gridViewPrevContract = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnPrevContractColName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrevContractColValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrevContractColName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrevContractColValue2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryBudgetItemSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.scrollableArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitterControl)).BeginInit();
            this.mainSplitterControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contractCreatorPanelCtrl)).BeginInit();
            this.contractCreatorPanelCtrl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marketPlacesDistrPanel)).BeginInit();
            this.marketPlacesDistrPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.distributorslayoutControl)).BeginInit();
            this.distributorslayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors1)).BeginInit();
            this.groupDistributors1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsRest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors2)).BeginInit();
            this.groupDistributors2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsKeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.distributorsLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors1LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors2LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPOCList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPOCList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthConcurentAvgSalesLayoutCtrl)).BeginInit();
            this.monthConcurentAvgSalesLayoutCtrl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupAvgSales)).BeginInit();
            this.groupAvgSales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gropuSalesAvgCompetitors)).BeginInit();
            this.gropuSalesAvgCompetitors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.monthSalesLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthAvgInBevSalesLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthAvgBerrSalesLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupClientInfo)).BeginInit();
            this.groupClientInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlClientInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupContractInfo)).BeginInit();
            this.groupContractInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContractInfoLayoutControl)).BeginInit();
            this.ContractInfoLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chDontUseBaskets.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBudgetOwner.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBudgetType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editContractNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinContractTerm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignPlanned.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignPlanned.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoContractCommentAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoContractComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractInfoLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractNumLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTermLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatePlanSignLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractChannelLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.singDatelayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.photoUploaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractTermStartLayoutCtrlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContractTermLayoutCtrlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDatePlanSignLayoutCtrlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateSignPlanStartLayoutCtrlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.l)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBudgetType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBudgetOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentAfterLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.panelGrids.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBudgetPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBudgetPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlCompetitors)).BeginInit();
            this.groupControlCompetitors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCompetitors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompetitors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCompetitorId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryContractSumEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryContractRetrobonusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryContractStitchedOnEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPrevContract)).BeginInit();
            this.groupPrevContract.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPrevContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPrevContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBudgetItemSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // scrollableArea
            // 
            this.scrollableArea.AlwaysScrollActiveControlIntoView = false;
            this.scrollableArea.AutoScrollMargin = new System.Drawing.Size(300, 200);
            this.scrollableArea.AutoScrollMinSize = new System.Drawing.Size(600, 460);
            this.scrollableArea.Controls.Add(this.mainSplitterControl);
            this.scrollableArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollableArea.Location = new System.Drawing.Point(0, 0);
            this.scrollableArea.Margin = new System.Windows.Forms.Padding(0);
            this.scrollableArea.Name = "scrollableArea";
            this.scrollableArea.Size = new System.Drawing.Size(1135, 724);
            this.scrollableArea.TabIndex = 0;
            // 
            // mainSplitterControl
            // 
            this.mainSplitterControl.AlwaysScrollActiveControlIntoView = false;
            this.mainSplitterControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitterControl.Location = new System.Drawing.Point(0, 0);
            this.mainSplitterControl.Margin = new System.Windows.Forms.Padding(0);
            this.mainSplitterControl.Name = "mainSplitterControl";
            this.mainSplitterControl.Panel1.Controls.Add(this.contractCreatorPanelCtrl);
            this.mainSplitterControl.Panel1.Controls.Add(this.marketPlacesDistrPanel);
            this.mainSplitterControl.Panel1.Controls.Add(this.monthConcurentAvgSalesLayoutCtrl);
            this.mainSplitterControl.Panel1.Controls.Add(this.groupClientInfo);
            this.mainSplitterControl.Panel1.Text = "LeftPanel";
            this.mainSplitterControl.Panel2.Controls.Add(this.groupContractInfo);
            this.mainSplitterControl.Panel2.Controls.Add(this.panelGrids);
            this.mainSplitterControl.Panel2.Text = "RightPanel";
            this.mainSplitterControl.Size = new System.Drawing.Size(1135, 724);
            this.mainSplitterControl.SplitterPosition = 521;
            this.mainSplitterControl.TabIndex = 47;
            this.mainSplitterControl.Text = "MainSplitter";
            // 
            // contractCreatorPanelCtrl
            // 
            this.contractCreatorPanelCtrl.Controls.Add(this.lblDCRName);
            this.contractCreatorPanelCtrl.Controls.Add(this.lblIsBudget);
            this.contractCreatorPanelCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.contractCreatorPanelCtrl.Location = new System.Drawing.Point(0, 0);
            this.contractCreatorPanelCtrl.Name = "contractCreatorPanelCtrl";
            this.contractCreatorPanelCtrl.Size = new System.Drawing.Size(521, 32);
            this.contractCreatorPanelCtrl.TabIndex = 48;
            // 
            // lblDCRName
            // 
            this.lblDCRName.Location = new System.Drawing.Point(2, 9);
            this.lblDCRName.Name = "lblDCRName";
            this.lblDCRName.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblDCRName.Size = new System.Drawing.Size(46, 13);
            this.lblDCRName.TabIndex = 45;
            this.lblDCRName.Text = "Создал:";
            // 
            // lblIsBudget
            // 
            this.lblIsBudget.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblIsBudget.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblIsBudget.Appearance.Options.UseForeColor = true;
            this.lblIsBudget.Appearance.Options.UseTextOptions = true;
            this.lblIsBudget.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblIsBudget.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblIsBudget.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblIsBudget.Location = new System.Drawing.Point(268, 2);
            this.lblIsBudget.Name = "lblIsBudget";
            this.lblIsBudget.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.lblIsBudget.Size = new System.Drawing.Size(251, 28);
            this.lblIsBudget.TabIndex = 46;
            this.lblIsBudget.Text = "Контракт не бюджетный, превышает бюджет";
            // 
            // marketPlacesDistrPanel
            // 
            this.marketPlacesDistrPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.marketPlacesDistrPanel.Controls.Add(this.distributorslayoutControl);
            this.marketPlacesDistrPanel.Controls.Add(this.groupControl1);
            this.marketPlacesDistrPanel.Location = new System.Drawing.Point(0, 182);
            this.marketPlacesDistrPanel.Margin = new System.Windows.Forms.Padding(0);
            this.marketPlacesDistrPanel.Name = "marketPlacesDistrPanel";
            this.marketPlacesDistrPanel.Size = new System.Drawing.Size(521, 252);
            this.marketPlacesDistrPanel.TabIndex = 47;
            // 
            // distributorslayoutControl
            // 
            this.distributorslayoutControl.Controls.Add(this.groupDistributors1);
            this.distributorslayoutControl.Controls.Add(this.groupDistributors2);
            this.distributorslayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.distributorslayoutControl.Location = new System.Drawing.Point(213, 2);
            this.distributorslayoutControl.Name = "distributorslayoutControl";
            this.distributorslayoutControl.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.distributorslayoutControl.Root = this.distributorsLayoutControlGroup;
            this.distributorslayoutControl.Size = new System.Drawing.Size(306, 248);
            this.distributorslayoutControl.TabIndex = 39;
            this.distributorslayoutControl.Text = "layoutControl1";
            // 
            // groupDistributors1
            // 
            this.groupDistributors1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupDistributors1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupDistributors1.AppearanceCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.groupDistributors1.Controls.Add(this.listDistributorsRest);
            this.groupDistributors1.Location = new System.Drawing.Point(10, 10);
            this.groupDistributors1.Margin = new System.Windows.Forms.Padding(0);
            this.groupDistributors1.Name = "groupDistributors1";
            this.groupDistributors1.Padding = new System.Windows.Forms.Padding(2);
            this.groupDistributors1.Size = new System.Drawing.Size(142, 228);
            this.groupDistributors1.TabIndex = 38;
            this.groupDistributors1.Text = "Дистрибьюторы бут, ПЕТ, банка";
            // 
            // listDistributorsRest
            // 
            this.listDistributorsRest.DisplayMember = "DISTR_NAME";
            this.listDistributorsRest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listDistributorsRest.Location = new System.Drawing.Point(4, 24);
            this.listDistributorsRest.Margin = new System.Windows.Forms.Padding(0);
            this.listDistributorsRest.Name = "listDistributorsRest";
            this.listDistributorsRest.Padding = new System.Windows.Forms.Padding(2);
            this.listDistributorsRest.Size = new System.Drawing.Size(134, 200);
            this.listDistributorsRest.TabIndex = 0;
            this.listDistributorsRest.ValueMember = "DISTR_ID";
            // 
            // groupDistributors2
            // 
            this.groupDistributors2.AppearanceCaption.Options.UseTextOptions = true;
            this.groupDistributors2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupDistributors2.AppearanceCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.groupDistributors2.Controls.Add(this.listDistributorsKeg);
            this.groupDistributors2.Location = new System.Drawing.Point(156, 10);
            this.groupDistributors2.Margin = new System.Windows.Forms.Padding(0);
            this.groupDistributors2.Name = "groupDistributors2";
            this.groupDistributors2.Padding = new System.Windows.Forms.Padding(2);
            this.groupDistributors2.Size = new System.Drawing.Size(140, 228);
            this.groupDistributors2.TabIndex = 37;
            this.groupDistributors2.Text = "Дистрибьюторы кега";
            // 
            // listDistributorsKeg
            // 
            this.listDistributorsKeg.DisplayMember = "DISTR_NAME";
            this.listDistributorsKeg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listDistributorsKeg.Location = new System.Drawing.Point(4, 24);
            this.listDistributorsKeg.Margin = new System.Windows.Forms.Padding(0);
            this.listDistributorsKeg.Name = "listDistributorsKeg";
            this.listDistributorsKeg.Padding = new System.Windows.Forms.Padding(2);
            this.listDistributorsKeg.Size = new System.Drawing.Size(132, 200);
            this.listDistributorsKeg.TabIndex = 0;
            this.listDistributorsKeg.ValueMember = "DISTR_ID";
            // 
            // distributorsLayoutControlGroup
            // 
            this.distributorsLayoutControlGroup.CustomizationFormText = "layoutControlGroup4";
            this.distributorsLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.distributorsLayoutControlGroup.GroupBordersVisible = false;
            this.distributorsLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.groupDistributors1LayoutControlItem,
            this.groupDistributors2LayoutControlItem});
            this.distributorsLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.distributorsLayoutControlGroup.Name = "distributorsLayoutControlGroup";
            this.distributorsLayoutControlGroup.OptionsItemText.TextToControlDistance = 5;
            this.distributorsLayoutControlGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 8, 8, 8);
            this.distributorsLayoutControlGroup.Size = new System.Drawing.Size(306, 248);
            this.distributorsLayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.distributorsLayoutControlGroup.Text = "distributorsLayoutControlGroup";
            this.distributorsLayoutControlGroup.TextVisible = false;
            // 
            // groupDistributors1LayoutControlItem
            // 
            this.groupDistributors1LayoutControlItem.Control = this.groupDistributors1;
            this.groupDistributors1LayoutControlItem.CustomizationFormText = "groupDistributors1LayoutControlItem";
            this.groupDistributors1LayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.groupDistributors1LayoutControlItem.Name = "groupDistributors1LayoutControlItem";
            this.groupDistributors1LayoutControlItem.Size = new System.Drawing.Size(146, 232);
            this.groupDistributors1LayoutControlItem.Text = "groupDistributors1LayoutControlItem";
            this.groupDistributors1LayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.groupDistributors1LayoutControlItem.TextToControlDistance = 0;
            this.groupDistributors1LayoutControlItem.TextVisible = false;
            // 
            // groupDistributors2LayoutControlItem
            // 
            this.groupDistributors2LayoutControlItem.Control = this.groupDistributors2;
            this.groupDistributors2LayoutControlItem.CustomizationFormText = "groupDistributors2LayoutControlItem";
            this.groupDistributors2LayoutControlItem.Location = new System.Drawing.Point(146, 0);
            this.groupDistributors2LayoutControlItem.Name = "groupDistributors2LayoutControlItem";
            this.groupDistributors2LayoutControlItem.Size = new System.Drawing.Size(144, 232);
            this.groupDistributors2LayoutControlItem.Text = "groupDistributors2LayoutControlItem";
            this.groupDistributors2LayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.groupDistributors2LayoutControlItem.TextToControlDistance = 0;
            this.groupDistributors2LayoutControlItem.TextVisible = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControlPOCList);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(211, 248);
            this.groupControl1.TabIndex = 38;
            this.groupControl1.Text = "Торговые точки";
            // 
            // gridControlPOCList
            // 
            this.gridControlPOCList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPOCList.Location = new System.Drawing.Point(2, 22);
            this.gridControlPOCList.MainView = this.gridViewPOCList;
            this.gridControlPOCList.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlPOCList.Name = "gridControlPOCList";
            this.gridControlPOCList.Size = new System.Drawing.Size(207, 224);
            this.gridControlPOCList.TabIndex = 0;
            this.gridControlPOCList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPOCList});
            // 
            // gridViewPOCList
            // 
            this.gridViewPOCList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPOCPremiality,
            this.columnPOCType,
            this.columnPOCCount});
            this.gridViewPOCList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewPOCList.GridControl = this.gridControlPOCList;
            this.gridViewPOCList.Name = "gridViewPOCList";
            this.gridViewPOCList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPOCList.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewPOCList.OptionsView.ShowDetailButtons = false;
            this.gridViewPOCList.OptionsView.ShowFooter = true;
            this.gridViewPOCList.OptionsView.ShowGroupPanel = false;
            this.gridViewPOCList.OptionsView.ShowIndicator = false;
            // 
            // columnPOCPremiality
            // 
            this.columnPOCPremiality.Caption = "Премиальность";
            this.columnPOCPremiality.FieldName = "PREMIALITY";
            this.columnPOCPremiality.Name = "columnPOCPremiality";
            this.columnPOCPremiality.OptionsColumn.AllowEdit = false;
            this.columnPOCPremiality.OptionsColumn.ReadOnly = true;
            this.columnPOCPremiality.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // columnPOCType
            // 
            this.columnPOCType.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPOCType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.columnPOCType.Caption = "Тип ТТ";
            this.columnPOCType.FieldName = "OLTYPE_NAME";
            this.columnPOCType.MinWidth = 10;
            this.columnPOCType.Name = "columnPOCType";
            this.columnPOCType.OptionsColumn.AllowEdit = false;
            this.columnPOCType.OptionsColumn.AllowMove = false;
            this.columnPOCType.OptionsColumn.ReadOnly = true;
            this.columnPOCType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPOCType.SummaryItem.FieldName = "PREMIALITY";
            this.columnPOCType.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Min;
            this.columnPOCType.Visible = true;
            this.columnPOCType.VisibleIndex = 0;
            this.columnPOCType.Width = 148;
            // 
            // columnPOCCount
            // 
            this.columnPOCCount.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPOCCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.columnPOCCount.Caption = "Кол-во";
            this.columnPOCCount.FieldName = "OLTYPE_QUANTITY";
            this.columnPOCCount.MinWidth = 10;
            this.columnPOCCount.Name = "columnPOCCount";
            this.columnPOCCount.OptionsColumn.AllowEdit = false;
            this.columnPOCCount.OptionsColumn.AllowMove = false;
            this.columnPOCCount.OptionsColumn.ReadOnly = true;
            this.columnPOCCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPOCCount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.columnPOCCount.Visible = true;
            this.columnPOCCount.VisibleIndex = 1;
            this.columnPOCCount.Width = 60;
            // 
            // monthConcurentAvgSalesLayoutCtrl
            // 
            this.monthConcurentAvgSalesLayoutCtrl.Controls.Add(this.groupAvgSales);
            this.monthConcurentAvgSalesLayoutCtrl.Controls.Add(this.gropuSalesAvgCompetitors);
            this.monthConcurentAvgSalesLayoutCtrl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.monthConcurentAvgSalesLayoutCtrl.Location = new System.Drawing.Point(0, 433);
            this.monthConcurentAvgSalesLayoutCtrl.Name = "monthConcurentAvgSalesLayoutCtrl";
            this.monthConcurentAvgSalesLayoutCtrl.Root = this.monthSalesLayoutControlGroup;
            this.monthConcurentAvgSalesLayoutCtrl.Size = new System.Drawing.Size(521, 291);
            this.monthConcurentAvgSalesLayoutCtrl.TabIndex = 46;
            this.monthConcurentAvgSalesLayoutCtrl.Text = "layoutControl1";
            // 
            // groupAvgSales
            // 
            this.groupAvgSales.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupAvgSales.AppearanceCaption.Options.UseTextOptions = true;
            this.groupAvgSales.AppearanceCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.groupAvgSales.Controls.Add(this.monthlySalesInBev);
            this.groupAvgSales.Location = new System.Drawing.Point(12, 154);
            this.groupAvgSales.Name = "groupAvgSales";
            this.groupAvgSales.Padding = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.groupAvgSales.Size = new System.Drawing.Size(497, 125);
            this.groupAvgSales.TabIndex = 35;
            this.groupAvgSales.Text = "Месячные средние продажи пива InBev, дал (12 мес до подписания контракта) ";
            // 
            // monthlySalesInBev
            // 
            this.monthlySalesInBev.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.monthlySalesInBev.DataSource = null;
            this.monthlySalesInBev.Location = new System.Drawing.Point(2, 23);
            this.monthlySalesInBev.Name = "monthlySalesInBev";
            this.monthlySalesInBev.ReadOnly = true;
            this.monthlySalesInBev.Size = new System.Drawing.Size(493, 101);
            this.monthlySalesInBev.TabIndex = 0;
            // 
            // gropuSalesAvgCompetitors
            // 
            this.gropuSalesAvgCompetitors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gropuSalesAvgCompetitors.AppearanceCaption.Options.UseTextOptions = true;
            this.gropuSalesAvgCompetitors.AppearanceCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gropuSalesAvgCompetitors.Controls.Add(this.monthlySalesCompetitors);
            this.gropuSalesAvgCompetitors.Location = new System.Drawing.Point(12, 12);
            this.gropuSalesAvgCompetitors.Name = "gropuSalesAvgCompetitors";
            this.gropuSalesAvgCompetitors.Size = new System.Drawing.Size(497, 138);
            this.gropuSalesAvgCompetitors.TabIndex = 36;
            this.gropuSalesAvgCompetitors.Text = "Месячные средние продажи пива конкурентов";
            // 
            // monthlySalesCompetitors
            // 
            this.monthlySalesCompetitors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.monthlySalesCompetitors.DataSource = null;
            this.monthlySalesCompetitors.Location = new System.Drawing.Point(2, 20);
            this.monthlySalesCompetitors.Name = "monthlySalesCompetitors";
            this.monthlySalesCompetitors.Size = new System.Drawing.Size(495, 116);
            this.monthlySalesCompetitors.TabIndex = 0;
            this.monthlySalesCompetitors.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            // 
            // monthSalesLayoutControlGroup
            // 
            this.monthSalesLayoutControlGroup.CustomizationFormText = "layoutControlGroup1";
            this.monthSalesLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.monthSalesLayoutControlGroup.GroupBordersVisible = false;
            this.monthSalesLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.monthAvgInBevSalesLayoutControlItem,
            this.monthAvgBerrSalesLayoutControlItem});
            this.monthSalesLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.monthSalesLayoutControlGroup.Name = "monthSalesLayoutControlGroup";
            this.monthSalesLayoutControlGroup.OptionsItemText.TextToControlDistance = 5;
            this.monthSalesLayoutControlGroup.Size = new System.Drawing.Size(521, 291);
            this.monthSalesLayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.monthSalesLayoutControlGroup.Text = "monthSalesLayoutControlGroup";
            this.monthSalesLayoutControlGroup.TextVisible = false;
            // 
            // monthAvgInBevSalesLayoutControlItem
            // 
            this.monthAvgInBevSalesLayoutControlItem.Control = this.groupAvgSales;
            this.monthAvgInBevSalesLayoutControlItem.CustomizationFormText = "layoutControlItem3";
            this.monthAvgInBevSalesLayoutControlItem.Location = new System.Drawing.Point(0, 142);
            this.monthAvgInBevSalesLayoutControlItem.Name = "monthAvgInBevSalesLayoutControlItem";
            this.monthAvgInBevSalesLayoutControlItem.Size = new System.Drawing.Size(501, 129);
            this.monthAvgInBevSalesLayoutControlItem.Text = "monthAvgInBevSalesLayoutControlItem";
            this.monthAvgInBevSalesLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.monthAvgInBevSalesLayoutControlItem.TextToControlDistance = 0;
            this.monthAvgInBevSalesLayoutControlItem.TextVisible = false;
            // 
            // monthAvgBerrSalesLayoutControlItem
            // 
            this.monthAvgBerrSalesLayoutControlItem.Control = this.gropuSalesAvgCompetitors;
            this.monthAvgBerrSalesLayoutControlItem.CustomizationFormText = "layoutControlItem4";
            this.monthAvgBerrSalesLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.monthAvgBerrSalesLayoutControlItem.Name = "monthAvgBerrSalesLayoutControlItem";
            this.monthAvgBerrSalesLayoutControlItem.Size = new System.Drawing.Size(501, 142);
            this.monthAvgBerrSalesLayoutControlItem.Text = "monthAvgBerrSalesLayoutControlItem";
            this.monthAvgBerrSalesLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.monthAvgBerrSalesLayoutControlItem.TextToControlDistance = 0;
            this.monthAvgBerrSalesLayoutControlItem.TextVisible = false;
            // 
            // groupClientInfo
            // 
            this.groupClientInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupClientInfo.Controls.Add(this.gridControlClientInfo);
            this.groupClientInfo.Location = new System.Drawing.Point(0, 30);
            this.groupClientInfo.Name = "groupClientInfo";
            this.groupClientInfo.Size = new System.Drawing.Size(521, 154);
            this.groupClientInfo.TabIndex = 34;
            this.groupClientInfo.Text = "Информация о клиенте";
            // 
            // gridControlClientInfo
            // 
            this.gridControlClientInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlClientInfo.Location = new System.Drawing.Point(2, 22);
            this.gridControlClientInfo.MainView = this.gridViewClientInfo;
            this.gridControlClientInfo.Margin = new System.Windows.Forms.Padding(0);
            this.gridControlClientInfo.Name = "gridControlClientInfo";
            this.gridControlClientInfo.Size = new System.Drawing.Size(517, 130);
            this.gridControlClientInfo.TabIndex = 3;
            this.gridControlClientInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewClientInfo,
            this.gridView1});
            // 
            // gridViewClientInfo
            // 
            this.gridViewClientInfo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnClientId,
            this.columnClientItem,
            this.columnClientItemValue});
            this.gridViewClientInfo.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewClientInfo.GridControl = this.gridControlClientInfo;
            this.gridViewClientInfo.Name = "gridViewClientInfo";
            this.gridViewClientInfo.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewClientInfo.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewClientInfo.OptionsView.ShowDetailButtons = false;
            this.gridViewClientInfo.OptionsView.ShowGroupPanel = false;
            this.gridViewClientInfo.OptionsView.ShowIndicator = false;
            // 
            // columnClientId
            // 
            this.columnClientId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnClientId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnClientId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnClientId.Caption = "ID";
            this.columnClientId.Name = "columnClientId";
            this.columnClientId.OptionsColumn.AllowEdit = false;
            this.columnClientId.OptionsColumn.AllowMove = false;
            this.columnClientId.OptionsColumn.ReadOnly = true;
            this.columnClientId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // columnClientItem
            // 
            this.columnClientItem.AppearanceHeader.Options.UseTextOptions = true;
            this.columnClientItem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnClientItem.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnClientItem.Caption = "Поле карточки клиента";
            this.columnClientItem.FieldName = "Item";
            this.columnClientItem.Name = "columnClientItem";
            this.columnClientItem.OptionsColumn.AllowEdit = false;
            this.columnClientItem.OptionsColumn.AllowMove = false;
            this.columnClientItem.OptionsColumn.ReadOnly = true;
            this.columnClientItem.OptionsColumn.ShowInCustomizationForm = false;
            this.columnClientItem.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnClientItem.Visible = true;
            this.columnClientItem.VisibleIndex = 0;
            // 
            // columnClientItemValue
            // 
            this.columnClientItemValue.AppearanceHeader.Options.UseTextOptions = true;
            this.columnClientItemValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnClientItemValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnClientItemValue.Caption = "Значение";
            this.columnClientItemValue.FieldName = "ItemValue";
            this.columnClientItemValue.Name = "columnClientItemValue";
            this.columnClientItemValue.OptionsColumn.AllowEdit = false;
            this.columnClientItemValue.OptionsColumn.AllowMove = false;
            this.columnClientItemValue.OptionsColumn.ReadOnly = true;
            this.columnClientItemValue.OptionsColumn.ShowInCustomizationForm = false;
            this.columnClientItemValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnClientItemValue.Visible = true;
            this.columnClientItemValue.VisibleIndex = 1;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControlClientInfo;
            this.gridView1.Name = "gridView1";
            // 
            // groupContractInfo
            // 
            this.groupContractInfo.Controls.Add(this.ContractInfoLayoutControl);
            this.groupContractInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupContractInfo.Location = new System.Drawing.Point(0, 0);
            this.groupContractInfo.Name = "groupContractInfo";
            this.groupContractInfo.Size = new System.Drawing.Size(608, 353);
            this.groupContractInfo.TabIndex = 39;
            this.groupContractInfo.Text = "Контракт";
            // 
            // ContractInfoLayoutControl
            // 
            this.ContractInfoLayoutControl.Controls.Add(this.chDontUseBaskets);
            this.ContractInfoLayoutControl.Controls.Add(this.cbBudgetOwner);
            this.ContractInfoLayoutControl.Controls.Add(this.lblSignDate);
            this.ContractInfoLayoutControl.Controls.Add(this.lblContractNum);
            this.ContractInfoLayoutControl.Controls.Add(this.lblDateSignPlanStart);
            this.ContractInfoLayoutControl.Controls.Add(this.cbBudgetType);
            this.ContractInfoLayoutControl.Controls.Add(this.lblContractTerm);
            this.ContractInfoLayoutControl.Controls.Add(this.editContractNum);
            this.ContractInfoLayoutControl.Controls.Add(this.dateSignDate);
            this.ContractInfoLayoutControl.Controls.Add(this.lblDatePlanSign);
            this.ContractInfoLayoutControl.Controls.Add(this.spinContractTerm);
            this.ContractInfoLayoutControl.Controls.Add(this.dateSignPlanned);
            this.ContractInfoLayoutControl.Controls.Add(this.labelControl1);
            this.ContractInfoLayoutControl.Controls.Add(this.memoContractCommentAfter);
            this.ContractInfoLayoutControl.Controls.Add(this.lblContractChannel);
            this.ContractInfoLayoutControl.Controls.Add(this.memoContractComment);
            this.ContractInfoLayoutControl.Controls.Add(this.contractTermStart);
            this.ContractInfoLayoutControl.Controls.Add(this.photoUploader);
            this.ContractInfoLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContractInfoLayoutControl.Location = new System.Drawing.Point(2, 22);
            this.ContractInfoLayoutControl.Margin = new System.Windows.Forms.Padding(0);
            this.ContractInfoLayoutControl.Name = "ContractInfoLayoutControl";
            this.ContractInfoLayoutControl.Padding = new System.Windows.Forms.Padding(2);
            this.ContractInfoLayoutControl.Root = this.contractInfoLayoutControlGroup;
            this.ContractInfoLayoutControl.Size = new System.Drawing.Size(604, 329);
            this.ContractInfoLayoutControl.TabIndex = 12;
            this.ContractInfoLayoutControl.Text = "ContractInfoLayoutControl";
            // 
            // chDontUseBaskets
            // 
            this.chDontUseBaskets.Location = new System.Drawing.Point(2, 141);
            this.chDontUseBaskets.Name = "chDontUseBaskets";
            this.chDontUseBaskets.Properties.Caption = "Не использовать разбивку по корзинам при расчете Volume Uplift";
            this.chDontUseBaskets.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chDontUseBaskets.Size = new System.Drawing.Size(358, 19);
            this.chDontUseBaskets.StyleController = this.ContractInfoLayoutControl;
            this.chDontUseBaskets.TabIndex = 51;
            this.chDontUseBaskets.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            // 
            // cbBudgetOwner
            // 
            this.cbBudgetOwner.Location = new System.Drawing.Point(220, 77);
            this.cbBudgetOwner.Name = "cbBudgetOwner";
            this.cbBudgetOwner.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbBudgetOwner.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BudgetOwner_Name", "Владелец бюджета")});
            this.cbBudgetOwner.Properties.DisplayMember = "BudgetOwner_Name";
            this.cbBudgetOwner.Properties.NullText = "Не выбрано";
            this.cbBudgetOwner.Properties.ValueMember = "BudgetOwner_ID";
            this.cbBudgetOwner.Size = new System.Drawing.Size(140, 20);
            this.cbBudgetOwner.StyleController = this.ContractInfoLayoutControl;
            this.cbBudgetOwner.TabIndex = 12;
            // 
            // lblSignDate
            // 
            this.lblSignDate.Location = new System.Drawing.Point(2, 2);
            this.lblSignDate.Name = "lblSignDate";
            this.lblSignDate.Size = new System.Drawing.Size(123, 23);
            this.lblSignDate.StyleController = this.ContractInfoLayoutControl;
            this.lblSignDate.TabIndex = 50;
            this.lblSignDate.Text = "Действует с:";
            // 
            // lblContractNum
            // 
            this.lblContractNum.Location = new System.Drawing.Point(2, 29);
            this.lblContractNum.Name = "lblContractNum";
            this.lblContractNum.Size = new System.Drawing.Size(92, 24);
            this.lblContractNum.StyleController = this.ContractInfoLayoutControl;
            this.lblContractNum.TabIndex = 49;
            this.lblContractNum.Text = "Номер контракта:";
            // 
            // lblDateSignPlanStart
            // 
            this.lblDateSignPlanStart.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblDateSignPlanStart.Appearance.Options.UseForeColor = true;
            this.lblDateSignPlanStart.Location = new System.Drawing.Point(108, 83);
            this.lblDateSignPlanStart.Name = "lblDateSignPlanStart";
            this.lblDateSignPlanStart.Size = new System.Drawing.Size(17, 22);
            this.lblDateSignPlanStart.StyleController = this.ContractInfoLayoutControl;
            this.lblDateSignPlanStart.TabIndex = 48;
            this.lblDateSignPlanStart.Text = "*";
            // 
            // cbBudgetType
            // 
            this.cbBudgetType.Location = new System.Drawing.Point(220, 22);
            this.cbBudgetType.Name = "cbBudgetType";
            this.cbBudgetType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbBudgetType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BudgetType_Name", "Тип бюджета")});
            this.cbBudgetType.Properties.DisplayMember = "BudgetType_Name";
            this.cbBudgetType.Properties.NullText = "Не выбрано";
            this.cbBudgetType.Properties.ValueMember = "BudgetType_ID";
            this.cbBudgetType.Size = new System.Drawing.Size(140, 20);
            this.cbBudgetType.StyleController = this.ContractInfoLayoutControl;
            this.cbBudgetType.TabIndex = 12;
            // 
            // lblContractTerm
            // 
            this.lblContractTerm.Location = new System.Drawing.Point(2, 57);
            this.lblContractTerm.Name = "lblContractTerm";
            this.lblContractTerm.Size = new System.Drawing.Size(89, 22);
            this.lblContractTerm.StyleController = this.ContractInfoLayoutControl;
            this.lblContractTerm.TabIndex = 46;
            this.lblContractTerm.Text = "Срок контракта:";
            // 
            // editContractNum
            // 
            this.editContractNum.Location = new System.Drawing.Point(129, 29);
            this.editContractNum.Name = "editContractNum";
            this.editContractNum.Padding = new System.Windows.Forms.Padding(5);
            this.editContractNum.Size = new System.Drawing.Size(85, 20);
            this.editContractNum.StyleController = this.ContractInfoLayoutControl;
            this.editContractNum.TabIndex = 8;
            this.editContractNum.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.editContractNum.Enter += new System.EventHandler(this.controlGetFocus);
            this.editContractNum.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // dateSignDate
            // 
            this.dateSignDate.EditValue = null;
            this.dateSignDate.Enabled = false;
            this.dateSignDate.Location = new System.Drawing.Point(129, 2);
            this.dateSignDate.Name = "dateSignDate";
            this.dateSignDate.Padding = new System.Windows.Forms.Padding(5);
            this.dateSignDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateSignDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateSignDate.Size = new System.Drawing.Size(85, 20);
            this.dateSignDate.StyleController = this.ContractInfoLayoutControl;
            this.dateSignDate.TabIndex = 44;
            this.dateSignDate.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.dateSignDate.Enter += new System.EventHandler(this.controlGetFocus);
            this.dateSignDate.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // lblDatePlanSign
            // 
            this.lblDatePlanSign.Location = new System.Drawing.Point(2, 83);
            this.lblDatePlanSign.Name = "lblDatePlanSign";
            this.lblDatePlanSign.Size = new System.Drawing.Size(102, 22);
            this.lblDatePlanSign.StyleController = this.ContractInfoLayoutControl;
            this.lblDatePlanSign.TabIndex = 47;
            this.lblDatePlanSign.Text = "Действует с (план):";
            // 
            // spinContractTerm
            // 
            this.spinContractTerm.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinContractTerm.Location = new System.Drawing.Point(129, 57);
            this.spinContractTerm.Name = "spinContractTerm";
            this.spinContractTerm.Padding = new System.Windows.Forms.Padding(0, 5, 5, 0);
            this.spinContractTerm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinContractTerm.Properties.Mask.EditMask = "##";
            this.spinContractTerm.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinContractTerm.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinContractTerm.Size = new System.Drawing.Size(85, 20);
            this.spinContractTerm.StyleController = this.ContractInfoLayoutControl;
            this.spinContractTerm.TabIndex = 7;
            this.spinContractTerm.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.spinContractTerm.Enter += new System.EventHandler(this.controlGetFocus);
            this.spinContractTerm.Leave += new System.EventHandler(this.spinContractTerm_Leave);
            // 
            // dateSignPlanned
            // 
            this.dateSignPlanned.EditValue = null;
            this.dateSignPlanned.Location = new System.Drawing.Point(129, 83);
            this.dateSignPlanned.Name = "dateSignPlanned";
            this.dateSignPlanned.Padding = new System.Windows.Forms.Padding(0, 5, 5, 0);
            this.dateSignPlanned.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateSignPlanned.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateSignPlanned.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateSignPlanned.Size = new System.Drawing.Size(85, 20);
            this.dateSignPlanned.StyleController = this.ContractInfoLayoutControl;
            this.dateSignPlanned.TabIndex = 11;
            this.dateSignPlanned.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.dateSignPlanned.Enter += new System.EventHandler(this.controlGetFocus);
            this.dateSignPlanned.Leave += new System.EventHandler(this.dateSignPlanned_Leave);
            this.dateSignPlanned.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(98, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 24);
            this.labelControl1.StyleController = this.ContractInfoLayoutControl;
            this.labelControl1.TabIndex = 48;
            this.labelControl1.Text = "*";
            // 
            // memoContractCommentAfter
            // 
            this.memoContractCommentAfter.Location = new System.Drawing.Point(301, 189);
            this.memoContractCommentAfter.Name = "memoContractCommentAfter";
            this.memoContractCommentAfter.Size = new System.Drawing.Size(299, 136);
            this.memoContractCommentAfter.StyleController = this.ContractInfoLayoutControl;
            this.memoContractCommentAfter.TabIndex = 10;
            this.memoContractCommentAfter.ToolTip = "Какие положительные стороны подписания контракта, в чем ТТ выиграют, соглашаясь п" +
    "одписать контракт.";
            // 
            // lblContractChannel
            // 
            this.lblContractChannel.Location = new System.Drawing.Point(2, 107);
            this.lblContractChannel.Margin = new System.Windows.Forms.Padding(0);
            this.lblContractChannel.Name = "lblContractChannel";
            this.lblContractChannel.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.lblContractChannel.Size = new System.Drawing.Size(212, 21);
            this.lblContractChannel.StyleController = this.ContractInfoLayoutControl;
            this.lblContractChannel.TabIndex = 10;
            this.lblContractChannel.Text = "Канал перcонала:";
            // 
            // memoContractComment
            // 
            this.memoContractComment.Location = new System.Drawing.Point(4, 189);
            this.memoContractComment.Name = "memoContractComment";
            this.memoContractComment.Size = new System.Drawing.Size(289, 136);
            this.memoContractComment.StyleController = this.ContractInfoLayoutControl;
            this.memoContractComment.TabIndex = 9;
            this.memoContractComment.ToolTip = "Какой была ситуация в точках до подписания контракта? Почему мы хотим подписать к" +
    "онтрактное соглашение?";
            this.memoContractComment.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            this.memoContractComment.Enter += new System.EventHandler(this.controlGetFocus);
            // 
            // contractTermStart
            // 
            this.contractTermStart.Appearance.ForeColor = System.Drawing.Color.Red;
            this.contractTermStart.Appearance.Options.UseForeColor = true;
            this.contractTermStart.Location = new System.Drawing.Point(95, 57);
            this.contractTermStart.Name = "contractTermStart";
            this.contractTermStart.Size = new System.Drawing.Size(30, 22);
            this.contractTermStart.StyleController = this.ContractInfoLayoutControl;
            this.contractTermStart.TabIndex = 45;
            this.contractTermStart.Text = "*";
            // 
            // photoUploader
            // 
            this.photoUploader.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.photoUploader.ButtonLoadText = "Загрузить файл";
            this.photoUploader.FieldNameFileData = "BINARY_DATA";
            this.photoUploader.FieldNameFileName = "BINARYDATA_NAME";
            this.photoUploader.FieldNameID = "ID";
            this.photoUploader.Location = new System.Drawing.Point(368, 22);
            this.photoUploader.Margin = new System.Windows.Forms.Padding(0);
            this.photoUploader.MaxItemsCount = 4;
            this.photoUploader.MinimumSize = new System.Drawing.Size(120, 0);
            this.photoUploader.Name = "photoUploader";
            this.photoUploader.ReadOnly = false;
            this.photoUploader.Size = new System.Drawing.Size(232, 107);
            this.photoUploader.TabIndex = 6;
            this.photoUploader.GetFileData += new Logica.Reports.BaseReportControl.CommonControls.FileUploader.FileDataEventHandler(this.photoUploader_GetFileData);
            this.photoUploader.EditValueChanged += new System.EventHandler(this.OnEditValueChanged);
            // 
            // contractInfoLayoutControlGroup
            // 
            this.contractInfoLayoutControlGroup.CustomizationFormText = "layoutControlGroup2";
            this.contractInfoLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.contractInfoLayoutControlGroup.GroupBordersVisible = false;
            this.contractInfoLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.contractNumLayoutItem,
            this.ContractTermLayoutControlItem,
            this.DatePlanSignLayoutControlItem,
            this.ContractChannelLayoutControlItem,
            this.singDatelayoutControlItem,
            this.photoUploaderLayoutControlItem,
            this.contractTermStartLayoutCtrlItem,
            this.lblContractTermLayoutCtrlItem,
            this.lblDatePlanSignLayoutCtrlItem,
            this.lblDateSignPlanStartLayoutCtrlItem,
            this.l,
            this.dateSignLayoutControlItem,
            this.CommentsLayoutControlItem,
            this.layoutControlItemBudgetType,
            this.layoutControlItemBudgetOwner,
            this.CommentAfterLayoutControlItem,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.emptySpaceItem1});
            this.contractInfoLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.contractInfoLayoutControlGroup.Name = "Root";
            this.contractInfoLayoutControlGroup.OptionsItemText.TextToControlDistance = 5;
            this.contractInfoLayoutControlGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.contractInfoLayoutControlGroup.Size = new System.Drawing.Size(604, 329);
            this.contractInfoLayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.contractInfoLayoutControlGroup.Text = "Root";
            this.contractInfoLayoutControlGroup.TextVisible = false;
            // 
            // contractNumLayoutItem
            // 
            this.contractNumLayoutItem.Control = this.editContractNum;
            this.contractNumLayoutItem.CustomizationFormText = "contractNumLayoutItem";
            this.contractNumLayoutItem.Location = new System.Drawing.Point(127, 27);
            this.contractNumLayoutItem.MaxSize = new System.Drawing.Size(89, 28);
            this.contractNumLayoutItem.MinSize = new System.Drawing.Size(89, 28);
            this.contractNumLayoutItem.Name = "contractNumLayoutItem";
            this.contractNumLayoutItem.Size = new System.Drawing.Size(89, 28);
            this.contractNumLayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.contractNumLayoutItem.Text = "Номер контракта:";
            this.contractNumLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.contractNumLayoutItem.TextToControlDistance = 0;
            this.contractNumLayoutItem.TextVisible = false;
            // 
            // ContractTermLayoutControlItem
            // 
            this.ContractTermLayoutControlItem.Control = this.spinContractTerm;
            this.ContractTermLayoutControlItem.CustomizationFormText = "Срок контракта";
            this.ContractTermLayoutControlItem.Location = new System.Drawing.Point(127, 55);
            this.ContractTermLayoutControlItem.MaxSize = new System.Drawing.Size(89, 26);
            this.ContractTermLayoutControlItem.MinSize = new System.Drawing.Size(89, 26);
            this.ContractTermLayoutControlItem.Name = "ContractTermLayoutControlItem";
            this.ContractTermLayoutControlItem.Size = new System.Drawing.Size(89, 26);
            this.ContractTermLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ContractTermLayoutControlItem.Text = "Срок контракта:";
            this.ContractTermLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.ContractTermLayoutControlItem.TextToControlDistance = 0;
            this.ContractTermLayoutControlItem.TextVisible = false;
            // 
            // DatePlanSignLayoutControlItem
            // 
            this.DatePlanSignLayoutControlItem.Control = this.dateSignPlanned;
            this.DatePlanSignLayoutControlItem.CustomizationFormText = "Действует с (план):";
            this.DatePlanSignLayoutControlItem.Location = new System.Drawing.Point(127, 81);
            this.DatePlanSignLayoutControlItem.MaxSize = new System.Drawing.Size(89, 26);
            this.DatePlanSignLayoutControlItem.MinSize = new System.Drawing.Size(89, 26);
            this.DatePlanSignLayoutControlItem.Name = "DatePlanSignLayoutControlItem";
            this.DatePlanSignLayoutControlItem.Size = new System.Drawing.Size(89, 26);
            this.DatePlanSignLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DatePlanSignLayoutControlItem.Text = "Действует с (план):";
            this.DatePlanSignLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.DatePlanSignLayoutControlItem.TextToControlDistance = 0;
            this.DatePlanSignLayoutControlItem.TextVisible = false;
            // 
            // ContractChannelLayoutControlItem
            // 
            this.ContractChannelLayoutControlItem.Control = this.lblContractChannel;
            this.ContractChannelLayoutControlItem.CustomizationFormText = "ContractChannelLayoutControlItem";
            this.ContractChannelLayoutControlItem.Location = new System.Drawing.Point(0, 107);
            this.ContractChannelLayoutControlItem.MaxSize = new System.Drawing.Size(216, 26);
            this.ContractChannelLayoutControlItem.MinSize = new System.Drawing.Size(216, 26);
            this.ContractChannelLayoutControlItem.Name = "ContractChannelLayoutControlItem";
            this.ContractChannelLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 5);
            this.ContractChannelLayoutControlItem.Size = new System.Drawing.Size(216, 26);
            this.ContractChannelLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ContractChannelLayoutControlItem.Text = "ContractChannelLayoutControlItem";
            this.ContractChannelLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.ContractChannelLayoutControlItem.TextToControlDistance = 0;
            this.ContractChannelLayoutControlItem.TextVisible = false;
            // 
            // singDatelayoutControlItem
            // 
            this.singDatelayoutControlItem.Control = this.dateSignDate;
            this.singDatelayoutControlItem.CustomizationFormText = "Действует с:";
            this.singDatelayoutControlItem.Location = new System.Drawing.Point(127, 0);
            this.singDatelayoutControlItem.MaxSize = new System.Drawing.Size(89, 27);
            this.singDatelayoutControlItem.MinSize = new System.Drawing.Size(89, 27);
            this.singDatelayoutControlItem.Name = "singDatelayoutControlItem";
            this.singDatelayoutControlItem.Size = new System.Drawing.Size(89, 27);
            this.singDatelayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.singDatelayoutControlItem.Text = "Действует с:";
            this.singDatelayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.singDatelayoutControlItem.TextToControlDistance = 0;
            this.singDatelayoutControlItem.TextVisible = false;
            // 
            // photoUploaderLayoutControlItem
            // 
            this.photoUploaderLayoutControlItem.Control = this.photoUploader;
            this.photoUploaderLayoutControlItem.CustomizationFormText = "photoUploaderLayoutControlItem";
            this.photoUploaderLayoutControlItem.Location = new System.Drawing.Point(364, 0);
            this.photoUploaderLayoutControlItem.MinSize = new System.Drawing.Size(120, 36);
            this.photoUploaderLayoutControlItem.Name = "photoUploaderLayoutControlItem";
            this.photoUploaderLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.photoUploaderLayoutControlItem.Size = new System.Drawing.Size(240, 133);
            this.photoUploaderLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.photoUploaderLayoutControlItem.Text = "Прикрепленные файлы:";
            this.photoUploaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.photoUploaderLayoutControlItem.TextSize = new System.Drawing.Size(298, 13);
            this.photoUploaderLayoutControlItem.TextToControlDistance = 5;
            // 
            // contractTermStartLayoutCtrlItem
            // 
            this.contractTermStartLayoutCtrlItem.Control = this.contractTermStart;
            this.contractTermStartLayoutCtrlItem.CustomizationFormText = "contractTermStartLayoutCtrlItem";
            this.contractTermStartLayoutCtrlItem.Location = new System.Drawing.Point(93, 55);
            this.contractTermStartLayoutCtrlItem.MaxSize = new System.Drawing.Size(34, 26);
            this.contractTermStartLayoutCtrlItem.MinSize = new System.Drawing.Size(34, 26);
            this.contractTermStartLayoutCtrlItem.Name = "contractTermStartLayoutCtrlItem";
            this.contractTermStartLayoutCtrlItem.Size = new System.Drawing.Size(34, 26);
            this.contractTermStartLayoutCtrlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.contractTermStartLayoutCtrlItem.Text = "contractTermStartLayoutCtrlItem";
            this.contractTermStartLayoutCtrlItem.TextSize = new System.Drawing.Size(0, 0);
            this.contractTermStartLayoutCtrlItem.TextToControlDistance = 0;
            this.contractTermStartLayoutCtrlItem.TextVisible = false;
            // 
            // lblContractTermLayoutCtrlItem
            // 
            this.lblContractTermLayoutCtrlItem.Control = this.lblContractTerm;
            this.lblContractTermLayoutCtrlItem.CustomizationFormText = "lblContractTermLayoutCtrlItem";
            this.lblContractTermLayoutCtrlItem.Location = new System.Drawing.Point(0, 55);
            this.lblContractTermLayoutCtrlItem.MaxSize = new System.Drawing.Size(93, 26);
            this.lblContractTermLayoutCtrlItem.MinSize = new System.Drawing.Size(93, 26);
            this.lblContractTermLayoutCtrlItem.Name = "lblContractTermLayoutCtrlItem";
            this.lblContractTermLayoutCtrlItem.Size = new System.Drawing.Size(93, 26);
            this.lblContractTermLayoutCtrlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblContractTermLayoutCtrlItem.Text = "lblContractTermLayoutCtrlItem";
            this.lblContractTermLayoutCtrlItem.TextSize = new System.Drawing.Size(0, 0);
            this.lblContractTermLayoutCtrlItem.TextToControlDistance = 0;
            this.lblContractTermLayoutCtrlItem.TextVisible = false;
            // 
            // lblDatePlanSignLayoutCtrlItem
            // 
            this.lblDatePlanSignLayoutCtrlItem.Control = this.lblDatePlanSign;
            this.lblDatePlanSignLayoutCtrlItem.CustomizationFormText = "lblDatePlanSignLayoutCtrlItem";
            this.lblDatePlanSignLayoutCtrlItem.Location = new System.Drawing.Point(0, 81);
            this.lblDatePlanSignLayoutCtrlItem.MaxSize = new System.Drawing.Size(106, 26);
            this.lblDatePlanSignLayoutCtrlItem.MinSize = new System.Drawing.Size(106, 26);
            this.lblDatePlanSignLayoutCtrlItem.Name = "lblDatePlanSignLayoutCtrlItem";
            this.lblDatePlanSignLayoutCtrlItem.Size = new System.Drawing.Size(106, 26);
            this.lblDatePlanSignLayoutCtrlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblDatePlanSignLayoutCtrlItem.Text = "lblDatePlanSignLayoutCtrlItem";
            this.lblDatePlanSignLayoutCtrlItem.TextSize = new System.Drawing.Size(0, 0);
            this.lblDatePlanSignLayoutCtrlItem.TextToControlDistance = 0;
            this.lblDatePlanSignLayoutCtrlItem.TextVisible = false;
            // 
            // lblDateSignPlanStartLayoutCtrlItem
            // 
            this.lblDateSignPlanStartLayoutCtrlItem.Control = this.lblDateSignPlanStart;
            this.lblDateSignPlanStartLayoutCtrlItem.CustomizationFormText = "lblDateSignPlanStartLayoutCtrlItem";
            this.lblDateSignPlanStartLayoutCtrlItem.Location = new System.Drawing.Point(106, 81);
            this.lblDateSignPlanStartLayoutCtrlItem.MaxSize = new System.Drawing.Size(21, 26);
            this.lblDateSignPlanStartLayoutCtrlItem.MinSize = new System.Drawing.Size(21, 26);
            this.lblDateSignPlanStartLayoutCtrlItem.Name = "lblDateSignPlanStartLayoutCtrlItem";
            this.lblDateSignPlanStartLayoutCtrlItem.Size = new System.Drawing.Size(21, 26);
            this.lblDateSignPlanStartLayoutCtrlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblDateSignPlanStartLayoutCtrlItem.Text = "lblDateSignPlanStartLayoutCtrlItem";
            this.lblDateSignPlanStartLayoutCtrlItem.TextSize = new System.Drawing.Size(0, 0);
            this.lblDateSignPlanStartLayoutCtrlItem.TextToControlDistance = 0;
            this.lblDateSignPlanStartLayoutCtrlItem.TextVisible = false;
            // 
            // l
            // 
            this.l.Control = this.lblContractNum;
            this.l.CustomizationFormText = "Номер контракта:";
            this.l.Location = new System.Drawing.Point(0, 27);
            this.l.MinSize = new System.Drawing.Size(96, 17);
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(96, 28);
            this.l.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.l.Text = "Номер контракта:";
            this.l.TextSize = new System.Drawing.Size(0, 0);
            this.l.TextToControlDistance = 0;
            this.l.TextVisible = false;
            this.l.TrimClientAreaToControl = false;
            // 
            // dateSignLayoutControlItem
            // 
            this.dateSignLayoutControlItem.Control = this.lblSignDate;
            this.dateSignLayoutControlItem.CustomizationFormText = "dateSignLayoutControlItem";
            this.dateSignLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.dateSignLayoutControlItem.MaxSize = new System.Drawing.Size(127, 27);
            this.dateSignLayoutControlItem.MinSize = new System.Drawing.Size(127, 27);
            this.dateSignLayoutControlItem.Name = "dateSignLayoutControlItem";
            this.dateSignLayoutControlItem.Size = new System.Drawing.Size(127, 27);
            this.dateSignLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.dateSignLayoutControlItem.Text = "dateSignLayoutControlItem";
            this.dateSignLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.dateSignLayoutControlItem.TextToControlDistance = 0;
            this.dateSignLayoutControlItem.TextVisible = false;
            // 
            // CommentsLayoutControlItem
            // 
            this.CommentsLayoutControlItem.Control = this.memoContractComment;
            this.CommentsLayoutControlItem.CustomizationFormText = "Комментарии (ситуация до подписания):";
            this.CommentsLayoutControlItem.Location = new System.Drawing.Point(0, 167);
            this.CommentsLayoutControlItem.MinSize = new System.Drawing.Size(280, 42);
            this.CommentsLayoutControlItem.Name = "CommentsLayoutControlItem";
            this.CommentsLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.CommentsLayoutControlItem.Size = new System.Drawing.Size(297, 162);
            this.CommentsLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.CommentsLayoutControlItem.Text = "Ситуация в точках клиента до подписания контракта:";
            this.CommentsLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.CommentsLayoutControlItem.TextSize = new System.Drawing.Size(298, 13);
            this.CommentsLayoutControlItem.TextToControlDistance = 5;
            // 
            // layoutControlItemBudgetType
            // 
            this.layoutControlItemBudgetType.Control = this.cbBudgetType;
            this.layoutControlItemBudgetType.CustomizationFormText = "Тип бюджета:";
            this.layoutControlItemBudgetType.Location = new System.Drawing.Point(216, 0);
            this.layoutControlItemBudgetType.MinSize = new System.Drawing.Size(120, 54);
            this.layoutControlItemBudgetType.Name = "layoutControlItemBudgetType";
            this.layoutControlItemBudgetType.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlItemBudgetType.Size = new System.Drawing.Size(148, 55);
            this.layoutControlItemBudgetType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemBudgetType.Text = "Тип бюджета:";
            this.layoutControlItemBudgetType.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemBudgetType.TextSize = new System.Drawing.Size(298, 13);
            this.layoutControlItemBudgetType.TextToControlDistance = 5;
            // 
            // layoutControlItemBudgetOwner
            // 
            this.layoutControlItemBudgetOwner.Control = this.cbBudgetOwner;
            this.layoutControlItemBudgetOwner.CustomizationFormText = "Владелец бюджета:";
            this.layoutControlItemBudgetOwner.Location = new System.Drawing.Point(216, 55);
            this.layoutControlItemBudgetOwner.MinSize = new System.Drawing.Size(100, 54);
            this.layoutControlItemBudgetOwner.Name = "layoutControlItemBudgetOwner";
            this.layoutControlItemBudgetOwner.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlItemBudgetOwner.Size = new System.Drawing.Size(148, 78);
            this.layoutControlItemBudgetOwner.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemBudgetOwner.Text = "Владелец бюджета:";
            this.layoutControlItemBudgetOwner.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemBudgetOwner.TextSize = new System.Drawing.Size(298, 13);
            this.layoutControlItemBudgetOwner.TextToControlDistance = 5;
            // 
            // CommentAfterLayoutControlItem
            // 
            this.CommentAfterLayoutControlItem.Control = this.memoContractCommentAfter;
            this.CommentAfterLayoutControlItem.CustomizationFormText = "Комментарии (ситуация после подписания):";
            this.CommentAfterLayoutControlItem.Location = new System.Drawing.Point(297, 167);
            this.CommentAfterLayoutControlItem.MinSize = new System.Drawing.Size(290, 42);
            this.CommentAfterLayoutControlItem.Name = "CommentAfterLayoutControlItem";
            this.CommentAfterLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.CommentAfterLayoutControlItem.Size = new System.Drawing.Size(307, 162);
            this.CommentAfterLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.CommentAfterLayoutControlItem.Text = "Ситуация в точках клиента после подписания контракта:";
            this.CommentAfterLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.CommentAfterLayoutControlItem.TextSize = new System.Drawing.Size(298, 13);
            this.CommentAfterLayoutControlItem.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(96, 27);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(10, 17);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(31, 28);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            this.layoutControlItem1.TrimClientAreaToControl = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.chDontUseBaskets;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 133);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(362, 34);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(362, 34);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 8, 2);
            this.layoutControlItem3.Size = new System.Drawing.Size(362, 34);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(362, 133);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(242, 34);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // panelGrids
            // 
            this.panelGrids.Controls.Add(this.gridControlBudgetPlan);
            this.panelGrids.Controls.Add(this.groupControlCompetitors);
            this.panelGrids.Controls.Add(this.groupPrevContract);
            this.panelGrids.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGrids.Location = new System.Drawing.Point(0, 353);
            this.panelGrids.Name = "panelGrids";
            this.panelGrids.Size = new System.Drawing.Size(608, 371);
            this.panelGrids.TabIndex = 42;
            // 
            // gridControlBudgetPlan
            // 
            this.gridControlBudgetPlan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gridControlBudgetPlan.Location = new System.Drawing.Point(0, 1);
            this.gridControlBudgetPlan.MainView = this.gridViewBudgetPlan;
            this.gridControlBudgetPlan.Margin = new System.Windows.Forms.Padding(0);
            this.gridControlBudgetPlan.Name = "gridControlBudgetPlan";
            this.gridControlBudgetPlan.Size = new System.Drawing.Size(608, 91);
            this.gridControlBudgetPlan.TabIndex = 44;
            this.gridControlBudgetPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBudgetPlan});
            // 
            // gridViewBudgetPlan
            // 
            this.gridViewBudgetPlan.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnBudgetPlanId,
            this.columnBudgetPlanStartDate,
            this.columnBudgetPlanName,
            this.columnBudgetPlanTotal});
            this.gridViewBudgetPlan.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewBudgetPlan.GridControl = this.gridControlBudgetPlan;
            this.gridViewBudgetPlan.Name = "gridViewBudgetPlan";
            this.gridViewBudgetPlan.OptionsBehavior.Editable = false;
            this.gridViewBudgetPlan.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewBudgetPlan.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewBudgetPlan.OptionsView.ColumnAutoWidth = false;
            this.gridViewBudgetPlan.OptionsView.ShowDetailButtons = false;
            this.gridViewBudgetPlan.OptionsView.ShowGroupPanel = false;
            this.gridViewBudgetPlan.OptionsView.ShowIndicator = false;
            this.gridViewBudgetPlan.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewBudgetPlan_CustomDrawCell);
            this.gridViewBudgetPlan.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewBudgetPlan_CustomColumnDisplayText);
            // 
            // columnBudgetPlanId
            // 
            this.columnBudgetPlanId.Caption = "ID";
            this.columnBudgetPlanId.FieldName = "ID";
            this.columnBudgetPlanId.Name = "columnBudgetPlanId";
            this.columnBudgetPlanId.OptionsColumn.AllowEdit = false;
            this.columnBudgetPlanId.OptionsColumn.AllowFocus = false;
            this.columnBudgetPlanId.OptionsColumn.ReadOnly = true;
            this.columnBudgetPlanId.OptionsColumn.ShowCaption = false;
            // 
            // columnBudgetPlanStartDate
            // 
            this.columnBudgetPlanStartDate.Caption = "START_DATE";
            this.columnBudgetPlanStartDate.FieldName = "START_DATE";
            this.columnBudgetPlanStartDate.Name = "columnBudgetPlanStartDate";
            this.columnBudgetPlanStartDate.OptionsColumn.AllowEdit = false;
            this.columnBudgetPlanStartDate.OptionsColumn.AllowFocus = false;
            this.columnBudgetPlanStartDate.OptionsColumn.ReadOnly = true;
            this.columnBudgetPlanStartDate.OptionsColumn.ShowCaption = false;
            // 
            // columnBudgetPlanName
            // 
            this.columnBudgetPlanName.Caption = "План для бюджета";
            this.columnBudgetPlanName.FieldName = "INDICATOR_NAME";
            this.columnBudgetPlanName.Name = "columnBudgetPlanName";
            this.columnBudgetPlanName.OptionsColumn.AllowFocus = false;
            this.columnBudgetPlanName.Visible = true;
            this.columnBudgetPlanName.VisibleIndex = 0;
            this.columnBudgetPlanName.Width = 130;
            // 
            // columnBudgetPlanTotal
            // 
            this.columnBudgetPlanTotal.Caption = "Всего";
            this.columnBudgetPlanTotal.FieldName = "TOTAL";
            this.columnBudgetPlanTotal.Name = "columnBudgetPlanTotal";
            this.columnBudgetPlanTotal.OptionsColumn.AllowFocus = false;
            this.columnBudgetPlanTotal.Visible = true;
            this.columnBudgetPlanTotal.VisibleIndex = 1;
            // 
            // groupControlCompetitors
            // 
            this.groupControlCompetitors.Controls.Add(this.gridControlCompetitors);
            this.groupControlCompetitors.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControlCompetitors.Location = new System.Drawing.Point(0, 92);
            this.groupControlCompetitors.Name = "groupControlCompetitors";
            this.groupControlCompetitors.Size = new System.Drawing.Size(608, 153);
            this.groupControlCompetitors.TabIndex = 43;
            this.groupControlCompetitors.Text = "Список конкурентов";
            // 
            // gridControlCompetitors
            // 
            this.gridControlCompetitors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.First.Enabled = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.Last.Enabled = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.Next.Enabled = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.NextPage.Enabled = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.Prev.Enabled = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.gridControlCompetitors.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControlCompetitors.EmbeddedNavigator.TextStringFormat = "{0} из {1}";
            this.gridControlCompetitors.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlCompetitors_EmbeddedNavigator_ButtonClick);
            this.gridControlCompetitors.Location = new System.Drawing.Point(2, 22);
            this.gridControlCompetitors.MainView = this.gridViewCompetitors;
            this.gridControlCompetitors.Name = "gridControlCompetitors";
            this.gridControlCompetitors.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryContractSumEdit,
            this.repositoryContractRetrobonusEdit,
            this.repositoryContractStitchedOnEdit,
            this.repositoryCompetitorId});
            this.gridControlCompetitors.Size = new System.Drawing.Size(604, 129);
            this.gridControlCompetitors.TabIndex = 3;
            this.gridControlCompetitors.UseEmbeddedNavigator = true;
            this.gridControlCompetitors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCompetitors});
            // 
            // gridViewCompetitors
            // 
            this.gridViewCompetitors.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnCompetitorId,
            this.columnCompetitorContractSum,
            this.columnCompetitorRetrobonus,
            this.columnCopetitorStitchedOn});
            this.gridViewCompetitors.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewCompetitors.GridControl = this.gridControlCompetitors;
            this.gridViewCompetitors.Name = "gridViewCompetitors";
            this.gridViewCompetitors.NewItemRowText = "Введите нового конкурнета здесь";
            this.gridViewCompetitors.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewCompetitors.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewCompetitors.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewCompetitors.OptionsView.ShowDetailButtons = false;
            this.gridViewCompetitors.OptionsView.ShowGroupPanel = false;
            this.gridViewCompetitors.OptionsView.ShowIndicator = false;
            // 
            // columnCompetitorId
            // 
            this.columnCompetitorId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCompetitorId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCompetitorId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCompetitorId.Caption = "Конкурент";
            this.columnCompetitorId.ColumnEdit = this.repositoryCompetitorId;
            this.columnCompetitorId.FieldName = "CONCURRENT_ID";
            this.columnCompetitorId.Name = "columnCompetitorId";
            this.columnCompetitorId.OptionsColumn.ShowInCustomizationForm = false;
            this.columnCompetitorId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCompetitorId.Visible = true;
            this.columnCompetitorId.VisibleIndex = 0;
            // 
            // repositoryCompetitorId
            // 
            this.repositoryCompetitorId.AutoHeight = false;
            this.repositoryCompetitorId.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryCompetitorId.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ConcurrentBrandName", "Конкурент")});
            this.repositoryCompetitorId.DisplayMember = "ConcurrentBrandName";
            this.repositoryCompetitorId.Name = "repositoryCompetitorId";
            this.repositoryCompetitorId.NullText = "";
            this.repositoryCompetitorId.ValueMember = "ID";
            // 
            // columnCompetitorContractSum
            // 
            this.columnCompetitorContractSum.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCompetitorContractSum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCompetitorContractSum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCompetitorContractSum.Caption = "Сумма контракта, грн";
            this.columnCompetitorContractSum.ColumnEdit = this.repositoryContractSumEdit;
            this.columnCompetitorContractSum.DisplayFormat.FormatString = "{0:N2}";
            this.columnCompetitorContractSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnCompetitorContractSum.FieldName = "CONTRACT_COST";
            this.columnCompetitorContractSum.Name = "columnCompetitorContractSum";
            this.columnCompetitorContractSum.OptionsColumn.ShowInCustomizationForm = false;
            this.columnCompetitorContractSum.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCompetitorContractSum.Visible = true;
            this.columnCompetitorContractSum.VisibleIndex = 1;
            // 
            // repositoryContractSumEdit
            // 
            this.repositoryContractSumEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryContractSumEdit.AutoHeight = false;
            this.repositoryContractSumEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryContractSumEdit.Mask.EditMask = "n2";
            this.repositoryContractSumEdit.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositoryContractSumEdit.Name = "repositoryContractSumEdit";
            // 
            // columnCompetitorRetrobonus
            // 
            this.columnCompetitorRetrobonus.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCompetitorRetrobonus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCompetitorRetrobonus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCompetitorRetrobonus.Caption = "Ретробонус, %";
            this.columnCompetitorRetrobonus.ColumnEdit = this.repositoryContractRetrobonusEdit;
            this.columnCompetitorRetrobonus.DisplayFormat.FormatString = "{0:N2}";
            this.columnCompetitorRetrobonus.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnCompetitorRetrobonus.FieldName = "CONTRACT_RETROBONUSE";
            this.columnCompetitorRetrobonus.Name = "columnCompetitorRetrobonus";
            this.columnCompetitorRetrobonus.OptionsColumn.ShowInCustomizationForm = false;
            this.columnCompetitorRetrobonus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCompetitorRetrobonus.Visible = true;
            this.columnCompetitorRetrobonus.VisibleIndex = 2;
            // 
            // repositoryContractRetrobonusEdit
            // 
            this.repositoryContractRetrobonusEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryContractRetrobonusEdit.AutoHeight = false;
            this.repositoryContractRetrobonusEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryContractRetrobonusEdit.Mask.EditMask = "n2";
            this.repositoryContractRetrobonusEdit.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryContractRetrobonusEdit.Name = "repositoryContractRetrobonusEdit";
            // 
            // columnCopetitorStitchedOn
            // 
            this.columnCopetitorStitchedOn.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCopetitorStitchedOn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCopetitorStitchedOn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCopetitorStitchedOn.Caption = "Отсрочка, дни";
            this.columnCopetitorStitchedOn.ColumnEdit = this.repositoryContractStitchedOnEdit;
            this.columnCopetitorStitchedOn.DisplayFormat.FormatString = "{0:N0}";
            this.columnCopetitorStitchedOn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnCopetitorStitchedOn.FieldName = "DAYS_DELAY";
            this.columnCopetitorStitchedOn.Name = "columnCopetitorStitchedOn";
            this.columnCopetitorStitchedOn.OptionsColumn.ShowInCustomizationForm = false;
            this.columnCopetitorStitchedOn.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCopetitorStitchedOn.Visible = true;
            this.columnCopetitorStitchedOn.VisibleIndex = 3;
            // 
            // repositoryContractStitchedOnEdit
            // 
            this.repositoryContractStitchedOnEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryContractStitchedOnEdit.AutoHeight = false;
            this.repositoryContractStitchedOnEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryContractStitchedOnEdit.IsFloatValue = false;
            this.repositoryContractStitchedOnEdit.Mask.EditMask = "n";
            this.repositoryContractStitchedOnEdit.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositoryContractStitchedOnEdit.Name = "repositoryContractStitchedOnEdit";
            // 
            // groupPrevContract
            // 
            this.groupPrevContract.Controls.Add(this.gridControlPrevContract);
            this.groupPrevContract.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupPrevContract.Location = new System.Drawing.Point(0, 245);
            this.groupPrevContract.Name = "groupPrevContract";
            this.groupPrevContract.Size = new System.Drawing.Size(608, 126);
            this.groupPrevContract.TabIndex = 42;
            this.groupPrevContract.Text = "Предыдущий контракт InBev";
            // 
            // gridControlPrevContract
            // 
            this.gridControlPrevContract.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPrevContract.Location = new System.Drawing.Point(2, 22);
            this.gridControlPrevContract.MainView = this.gridViewPrevContract;
            this.gridControlPrevContract.Margin = new System.Windows.Forms.Padding(0);
            this.gridControlPrevContract.Name = "gridControlPrevContract";
            this.gridControlPrevContract.Size = new System.Drawing.Size(604, 102);
            this.gridControlPrevContract.TabIndex = 3;
            this.gridControlPrevContract.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPrevContract});
            // 
            // gridViewPrevContract
            // 
            this.gridViewPrevContract.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPrevContractColName1,
            this.columnPrevContractColValue1,
            this.columnPrevContractColName2,
            this.columnPrevContractColValue2});
            this.gridViewPrevContract.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewPrevContract.GridControl = this.gridControlPrevContract;
            this.gridViewPrevContract.Name = "gridViewPrevContract";
            this.gridViewPrevContract.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPrevContract.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewPrevContract.OptionsView.ShowColumnHeaders = false;
            this.gridViewPrevContract.OptionsView.ShowDetailButtons = false;
            this.gridViewPrevContract.OptionsView.ShowGroupPanel = false;
            this.gridViewPrevContract.OptionsView.ShowIndicator = false;
            // 
            // columnPrevContractColName1
            // 
            this.columnPrevContractColName1.AppearanceCell.BackColor = System.Drawing.SystemColors.Control;
            this.columnPrevContractColName1.AppearanceCell.BackColor2 = System.Drawing.SystemColors.Control;
            this.columnPrevContractColName1.AppearanceCell.Options.UseBackColor = true;
            this.columnPrevContractColName1.FieldName = "DESCR1";
            this.columnPrevContractColName1.Name = "columnPrevContractColName1";
            this.columnPrevContractColName1.OptionsColumn.AllowEdit = false;
            this.columnPrevContractColName1.OptionsColumn.ReadOnly = true;
            this.columnPrevContractColName1.OptionsColumn.ShowCaption = false;
            this.columnPrevContractColName1.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPrevContractColName1.Visible = true;
            this.columnPrevContractColName1.VisibleIndex = 0;
            // 
            // columnPrevContractColValue1
            // 
            this.columnPrevContractColValue1.FieldName = "VAL1";
            this.columnPrevContractColValue1.Name = "columnPrevContractColValue1";
            this.columnPrevContractColValue1.OptionsColumn.AllowEdit = false;
            this.columnPrevContractColValue1.OptionsColumn.ReadOnly = true;
            this.columnPrevContractColValue1.OptionsColumn.ShowCaption = false;
            this.columnPrevContractColValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPrevContractColValue1.Visible = true;
            this.columnPrevContractColValue1.VisibleIndex = 1;
            // 
            // columnPrevContractColName2
            // 
            this.columnPrevContractColName2.AppearanceCell.BackColor = System.Drawing.SystemColors.Control;
            this.columnPrevContractColName2.AppearanceCell.BackColor2 = System.Drawing.SystemColors.Control;
            this.columnPrevContractColName2.AppearanceCell.Options.UseBackColor = true;
            this.columnPrevContractColName2.FieldName = "DESCR2";
            this.columnPrevContractColName2.Name = "columnPrevContractColName2";
            this.columnPrevContractColName2.OptionsColumn.AllowEdit = false;
            this.columnPrevContractColName2.OptionsColumn.ReadOnly = true;
            this.columnPrevContractColName2.OptionsColumn.ShowCaption = false;
            this.columnPrevContractColName2.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPrevContractColName2.Visible = true;
            this.columnPrevContractColName2.VisibleIndex = 2;
            // 
            // columnPrevContractColValue2
            // 
            this.columnPrevContractColValue2.FieldName = "VAL2";
            this.columnPrevContractColValue2.Name = "columnPrevContractColValue2";
            this.columnPrevContractColValue2.OptionsColumn.AllowEdit = false;
            this.columnPrevContractColValue2.OptionsColumn.ReadOnly = true;
            this.columnPrevContractColValue2.OptionsColumn.ShowCaption = false;
            this.columnPrevContractColValue2.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPrevContractColValue2.Visible = true;
            this.columnPrevContractColValue2.VisibleIndex = 3;
            // 
            // repositoryBudgetItemSpinEdit
            // 
            this.repositoryBudgetItemSpinEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryBudgetItemSpinEdit.AutoHeight = false;
            this.repositoryBudgetItemSpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryBudgetItemSpinEdit.Mask.EditMask = "n2";
            this.repositoryBudgetItemSpinEdit.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositoryBudgetItemSpinEdit.Name = "repositoryBudgetItemSpinEdit";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(120, 61);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.memoContractComment;
            this.layoutControlItem2.CustomizationFormText = "Комментарии:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(22, 37);
            this.layoutControlItem2.Name = "CommentsLayoutControlItem";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 5, 16);
            this.layoutControlItem2.Size = new System.Drawing.Size(647, 160);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Комментарии:";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.CustomizationFormText = "LabelsimpleLabelItem1";
            this.simpleLabelItem1.Location = new System.Drawing.Point(80, 27);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(302, 28);
            this.simpleLabelItem1.Text = "LabelsimpleLabelItem1";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(298, 13);
            // 
            // ContractDetailsCommon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.scrollableArea);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ContractDetailsCommon";
            this.Size = new System.Drawing.Size(1135, 724);
            this.Load += new System.EventHandler(this.ContractDetailsCommon_Load);
            this.scrollableArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitterControl)).EndInit();
            this.mainSplitterControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.contractCreatorPanelCtrl)).EndInit();
            this.contractCreatorPanelCtrl.ResumeLayout(false);
            this.contractCreatorPanelCtrl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marketPlacesDistrPanel)).EndInit();
            this.marketPlacesDistrPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.distributorslayoutControl)).EndInit();
            this.distributorslayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors1)).EndInit();
            this.groupDistributors1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsRest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors2)).EndInit();
            this.groupDistributors2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsKeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.distributorsLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors1LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors2LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPOCList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPOCList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthConcurentAvgSalesLayoutCtrl)).EndInit();
            this.monthConcurentAvgSalesLayoutCtrl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupAvgSales)).EndInit();
            this.groupAvgSales.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gropuSalesAvgCompetitors)).EndInit();
            this.gropuSalesAvgCompetitors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.monthSalesLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthAvgInBevSalesLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthAvgBerrSalesLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupClientInfo)).EndInit();
            this.groupClientInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlClientInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupContractInfo)).EndInit();
            this.groupContractInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ContractInfoLayoutControl)).EndInit();
            this.ContractInfoLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chDontUseBaskets.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBudgetOwner.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBudgetType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editContractNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinContractTerm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignPlanned.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignPlanned.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoContractCommentAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoContractComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractInfoLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractNumLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTermLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatePlanSignLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractChannelLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.singDatelayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.photoUploaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractTermStartLayoutCtrlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContractTermLayoutCtrlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDatePlanSignLayoutCtrlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateSignPlanStartLayoutCtrlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.l)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSignLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBudgetType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBudgetOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentAfterLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.panelGrids.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBudgetPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBudgetPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlCompetitors)).EndInit();
            this.groupControlCompetitors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCompetitors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompetitors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCompetitorId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryContractSumEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryContractRetrobonusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryContractStitchedOnEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPrevContract)).EndInit();
            this.groupPrevContract.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPrevContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPrevContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBudgetItemSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl scrollableArea;
        private DevExpress.XtraEditors.GroupControl groupDistributors1;
        private DevExpress.XtraEditors.ListBoxControl listDistributorsRest;
        private DevExpress.XtraEditors.GroupControl groupDistributors2;
        private DevExpress.XtraEditors.ListBoxControl listDistributorsKeg;
        private DevExpress.XtraEditors.GroupControl groupContractInfo;
        private DevExpress.XtraEditors.LabelControl lblContractChannel;
        private DevExpress.XtraEditors.MemoEdit memoContractComment;
        private DevExpress.XtraEditors.TextEdit editContractNum;
        private DevExpress.XtraEditors.SpinEdit spinContractTerm;
        private DevExpress.XtraEditors.GroupControl groupAvgSales;
        private DevExpress.XtraGrid.GridControl gridControlPOCList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPOCList;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCPremiality;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCType;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCCount;
        private DevExpress.XtraEditors.DateEdit dateSignPlanned;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsUpdatable;
        private Logica.Reports.BaseReportControl.CommonControls.Editors.MonthlySales monthlySalesInBev;
        private DevExpress.XtraEditors.GroupControl gropuSalesAvgCompetitors;
        private Logica.Reports.BaseReportControl.CommonControls.Editors.MonthlySales monthlySalesCompetitors;
        private DevExpress.XtraEditors.LabelControl lblIsBudget;
        private DevExpress.XtraEditors.DateEdit dateSignDate;
        private DevExpress.XtraEditors.SplitContainerControl mainSplitterControl;
        private Logica.Reports.BaseReportControl.CommonControls.FileUploader photoUploader;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryBudgetItemSpinEdit;
        private DevExpress.XtraLayout.LayoutControl ContractInfoLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup contractInfoLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem contractNumLayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem ContractTermLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem DatePlanSignLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ContractChannelLayoutControlItem;
        private DevExpress.XtraEditors.GroupControl groupClientInfo;
        private DevExpress.XtraEditors.LabelControl lblDCRName;
        private DevExpress.XtraGrid.GridControl gridControlClientInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewClientInfo;
        private DevExpress.XtraGrid.Columns.GridColumn columnClientId;
        private DevExpress.XtraGrid.Columns.GridColumn columnClientItem;
        private DevExpress.XtraGrid.Columns.GridColumn columnClientItemValue;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl monthConcurentAvgSalesLayoutCtrl;
        private DevExpress.XtraLayout.LayoutControlGroup monthSalesLayoutControlGroup;
        private DevExpress.XtraEditors.PanelControl marketPlacesDistrPanel;
        private DevExpress.XtraLayout.LayoutControl distributorslayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup distributorsLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem groupDistributors1LayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem groupDistributors2LayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem monthAvgInBevSalesLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem monthAvgBerrSalesLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem CommentsLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem singDatelayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem photoUploaderLayoutControlItem;
        private DevExpress.XtraEditors.PanelControl contractCreatorPanelCtrl;
        private DevExpress.XtraEditors.LabelControl contractTermStart;
        private DevExpress.XtraEditors.LabelControl lblContractTerm;
        private DevExpress.XtraLayout.LayoutControlItem contractTermStartLayoutCtrlItem;
        private DevExpress.XtraLayout.LayoutControlItem lblContractTermLayoutCtrlItem;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl lblDateSignPlanStart;
        private DevExpress.XtraEditors.LabelControl lblDatePlanSign;
        private DevExpress.XtraLayout.LayoutControlItem lblDatePlanSignLayoutCtrlItem;
        private DevExpress.XtraLayout.LayoutControlItem lblDateSignPlanStartLayoutCtrlItem;
        private DevExpress.XtraEditors.LabelControl lblContractNum;
        private DevExpress.XtraLayout.LayoutControlItem l;
        private DevExpress.XtraEditors.LabelControl lblSignDate;
        private DevExpress.XtraLayout.LayoutControlItem dateSignLayoutControlItem;
        private DevExpress.XtraEditors.MemoEdit memoContractCommentAfter;
        private DevExpress.XtraLayout.LayoutControlItem CommentAfterLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LookUpEdit cbBudgetOwner;
        private DevExpress.XtraEditors.LookUpEdit cbBudgetType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBudgetType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBudgetOwner;
        private System.Windows.Forms.Panel panelGrids;
        private DevExpress.XtraGrid.GridControl gridControlBudgetPlan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBudgetPlan;
        private DevExpress.XtraGrid.Columns.GridColumn columnBudgetPlanId;
        private DevExpress.XtraGrid.Columns.GridColumn columnBudgetPlanStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnBudgetPlanName;
        private DevExpress.XtraGrid.Columns.GridColumn columnBudgetPlanTotal;
        private DevExpress.XtraEditors.GroupControl groupControlCompetitors;
        private DevExpress.XtraGrid.GridControl gridControlCompetitors;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCompetitors;
        private DevExpress.XtraGrid.Columns.GridColumn columnCompetitorId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryCompetitorId;
        private DevExpress.XtraGrid.Columns.GridColumn columnCompetitorContractSum;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryContractSumEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnCompetitorRetrobonus;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryContractRetrobonusEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnCopetitorStitchedOn;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryContractStitchedOnEdit;
        private DevExpress.XtraEditors.GroupControl groupPrevContract;
        private DevExpress.XtraGrid.GridControl gridControlPrevContract;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPrevContract;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrevContractColName1;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrevContractColValue1;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrevContractColName2;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrevContractColValue2;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit chDontUseBaskets;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;




    }
}
