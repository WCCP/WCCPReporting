﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.Enums;
using SoftServe.Reports.ContractMgmt.Utility;

namespace SoftServe.Reports.ContractMgmt.UserControls {
    /// <summary>
    /// </summary>
    public partial class ContractDetailsCommon : CommonBaseControl, ITwoStepSavingControl {
        #region Fields

        private readonly Collection<Control> _changedFieldsCollection = new Collection<Control>();
        private DataTable _dataTableFields;

        /// <summary>
        ///   Private field that is used for updating information that depends on another dynamic value (text box or date editor).
        /// </summary>
        private bool _dependedFieldChanged = false;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "ContractDetailsCommon" /> class.
        /// </summary>
        public ContractDetailsCommon() {
            InitializeComponent();
            IsSecondStepRequired = false;
        }

        #endregion

        #region Instance Properties

        public override bool IsCalcVRequired {
            get { return LoadedContractTerm == -1 || LoadedContractTerm != SelectedContractTerm || LoadedSignPlanned != SelectedDateSignPlanned; }
            set { }
        }

        /// <summary>
        ///   Gets a value indicating whether this instance is agreement state required.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is agreement state required; otherwise, <c>false</c>.
        /// </value>
        public bool IsAgreementStateChangeRequired {
            get {
                if (IsChanged) {
                    if (_changedFieldsCollection.Count == 1)
                        return !(_changedFieldsCollection.Contains(dateSignDate) || _changedFieldsCollection.Contains(editContractNum));
                    if (_changedFieldsCollection.Count == 2)
                        return !(_changedFieldsCollection.Contains(dateSignDate) && _changedFieldsCollection.Contains(editContractNum));
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        ///   Gets the contract number.
        /// </summary>
        /// <value>The contract number.</value>
        public string SelectedContractNumber {
            get { return ConvertEx.ToString(editContractNum.EditValue); }
        }

        /// <summary>
        ///   Gets the selected contract term.
        /// </summary>
        /// <value>The selected contract term.</value>
        public int SelectedContractTerm {
            get { return ConvertEx.ToInt(spinContractTerm.EditValue); }
        }

        /// <summary>
        ///   Gets the selected date sign planned.
        /// </summary>
        /// <value>The selected date sign planned.</value>
        public DateTime SelectedDateSignPlanned {
            get { return dateSignPlanned.DateTime; }
        }

        /// <summary>
        ///   Gets the total budget investments.
        /// </summary>
        /// <value>The total budget investments.</value>
        public decimal TotalBudgetInvestments {
            get {
                for (int lRowHandle = 0; lRowHandle < gridViewBudgetPlan.RowCount; lRowHandle++) {
                    int lId = ConvertEx.ToInt(gridViewBudgetPlan.GetRowCellValue(lRowHandle, columnBudgetPlanId));
                    if (lId == (int) BudgetIndicator.TotalInvestments)
                        return ConvertEx.ToDecimal(gridViewBudgetPlan.GetRowCellValue(lRowHandle, columnBudgetPlanTotal));
                }
                return 0M;
            }
        }

        /// <summary>
        ///   Gets the address list.
        /// </summary>
        /// <value>The address list.</value>
        private string AddressList {
            get { return string.Join(Constants.DELIMITER_ID_LIST, MainData.AddressesPocList.ToArray()); }
        }

        /// <summary>
        ///   Sets all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable AllFieldsData {
            set {
                _dataTableFields = value;

                if (_dataTableFields != null && _dataTableFields.Rows.Count == 1) {
                    DataRow lRow = _dataTableFields.Rows[0];

                    lblDCRName.Text = string.Empty;
                    if (MainData.IsEditMode)
                        lblDCRName.Text = string.Format(Resource.LabelContractCreator,
                            lRow[Constants.FIELD_COMMON_CREATOR_NAME],
                            ConvertEx.ToDateTime(lRow[Constants.FIELD_COMMON_CREATION_DATE]).Date.ToString("d"));

                    dateSignDate.DateTime = ConvertEx.ToDateTime(lRow[Constants.FIELD_COMMON_CONTRACT_DATE_SIGNED]);
                    if (dateSignDate.DateTime == DateTime.MinValue)
                        dateSignDate.EditValue = null;

                    dateSignDate.Properties.MaxValue = DateTime.Today.AddYears(1);
                    MainData.IsBudgetApproved = ConvertEx.ToBool(lRow[Constants.FIELD_COMMON_CONTRACT_IS_BUDGET]); // it sets budget label as well
                    editContractNum.EditValue = ConvertEx.ToString(lRow[Constants.FIELD_COMMON_CONTRACT_NUM]);
                    spinContractTerm.EditValue = MainData.ContractTerm = OldContractTerm = ConvertEx.ToInt(lRow[Constants.FIELD_COMMON_CONTRACT_LENGTH]);
                    dateSignPlanned.DateTime =
                        MainData.DateSignPlanned = OldSignPlanned = ConvertEx.ToDateTime(lRow[Constants.FIELD_COMMON_CONTRACT_PLAN_SIGNDATE]);
                    LoadedContractTerm = MainData.IsEditMode ? OldContractTerm : -1;
                    LoadedSignPlanned = OldSignPlanned;

                    string lChannel = ConvertEx.ToString(lRow[Constants.FIELD_COMMON_CONTRACT_CHANNEL]);
                    lChannel = string.IsNullOrEmpty(lChannel) ? ChannelTypeHelper.GetChannelName(MainData.Channel) : lChannel;
                    lblContractChannel.Text = string.Format(Resource.LabelContractChannel, lChannel);
                    cbBudgetType.EditValue = ConvertEx.ToInt(lRow[Constants.FIELD_COMMON_BUDGETTYPE_ID]);
                    cbBudgetOwner.EditValue = ConvertEx.ToInt(lRow[Constants.FIELD_COMMON_BUDGETOWNER_ID]);
                    memoContractComment.Text = ConvertEx.ToString(lRow[Constants.FIELD_COMMON_CONTRACT_COMMENT]);
                    memoContractCommentAfter.Text = ConvertEx.ToString(lRow[Constants.FIELD_COMMON_CONTRACT_COMMENT2]);
                    chDontUseBaskets.Checked = lRow.IsNull(Constants.FIELD_COMMON_CalcUpliftWOBaskets) ? false : ConvertEx.ToBool(lRow[Constants.FIELD_COMMON_CalcUpliftWOBaskets]);
                }
                else if (!MainData.IsEditMode) {
                    spinContractTerm.EditValue = MainData.ContractTerm = OldContractTerm = 12;
                    dateSignPlanned.DateTime = MainData.DateSignPlanned = OldSignPlanned = new DateTime(DateTime.Today.AddYears(1).Year, 1, 1);
                    LoadedContractTerm = -1;
                }
            }
            get {
                if (_dataTableFields != null && _dataTableFields.Rows.Count == 1) {
                    DataRow lRow = _dataTableFields.Rows[0];
                    lRow[Constants.FIELD_COMMON_CONTRACT_ID] = MainData.ContractId;
                    lRow[Constants.FIELD_COMMON_CONTRACT_DATE_SIGNED] = dateSignDate.DateTime == DateTime.MinValue
                        ? DBNull.Value : (object) dateSignDate.DateTime;
                    lRow[Constants.FIELD_COMMON_CONTRACT_NUM] = editContractNum.EditValue;
                    lRow[Constants.FIELD_COMMON_CONTRACT_LENGTH] = spinContractTerm.EditValue;
                    lRow[Constants.FIELD_COMMON_CONTRACT_PLAN_SIGNDATE] = dateSignPlanned.DateTime;
                    lRow[Constants.FIELD_COMMON_CONTRACT_COMMENT] = memoContractComment.Text;
                    lRow[Constants.FIELD_COMMON_CONTRACT_COMMENT2] = memoContractCommentAfter.Text;
                    lRow[Constants.FIELD_COMMON_BUDGETTYPE_ID] = cbBudgetType.EditValue;
                    lRow[Constants.FIELD_COMMON_BUDGETOWNER_ID] = cbBudgetOwner.EditValue;
                    lRow[Constants.FIELD_COMMON_CalcUpliftWOBaskets] = chDontUseBaskets.Checked;
                    _dataTableFields.AcceptChanges();
                    return _dataTableFields;
                }
                return null;
            }
        }

        /// <summary>
        ///   Gets the competitors list.
        /// </summary>
        /// <value>The competitors list.</value>
        private SqlXml CompetitorsList {
            get {
                return ConvertEx.ToSqlXml(gridControlCompetitors.DataSource as DataTable,
                    new[] {
                        columnCompetitorId.FieldName,
                        columnCompetitorId.FieldName,
                        columnCompetitorContractSum.FieldName,
                        columnCompetitorRetrobonus.FieldName,
                        columnCopetitorStitchedOn.FieldName
                    });
            }
        }

        /// <summary>
        ///   Gets the monthly sales competitors list.
        /// </summary>
        /// <value>The monthly sales competitors list.</value>
        private SqlXml MonthlySalesCompetitorsList {
            get { return monthlySalesCompetitors.DataXml; }
        }

        private int OldContractTerm { get; set; }
        private DateTime OldSignPlanned { get; set; }

        private int LoadedContractTerm { get; set; }
        private DateTime LoadedSignPlanned { get; set; }

        private bool RequireSignDateContractNum { get; set; }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Loads the data.
        /// </summary>
        public override void LoadData() {
            _changedFieldsCollection.Clear();
            MainData.ContractStatusChanged += MainData_ContractStatusChanged;
            MainData.ContractStatusChanging += MainData_ContractStatusChanging;
            MainData.BudgetLabelUpdated += MainData_BudgetLabelUpdated;

            AttachEvents(false);

            LoadDropDowns();
            AllFieldsData = DataProvider.GetCommonDetails(MainData.ContractId);
            LoadClientInfo();
            LoadSalesAverageInBev();
            LoadSalesAverageCompetitors();
            LoadDistributors();
            LoadPOCList();
            LoadCompetitors();
            LoadPreviousContract();
            LoadBudgetPlan();
            LoadFiles();
            UpdateFieldsStatus(ContractStatus.Unknown, MainData.ContractStatus);
            UpdateReadOnlyControls();
            base.LoadData();

            AttachEvents(true);
        }

        /// <summary>
        ///   Saves this instance.
        /// </summary>
        public override int SaveData() {
            if (IsChanged)
                UpdateMainData();

            SaveFields();
            SaveDocuments();
            SaveMonthlySalesCompetitors();
            SaveCompetitors();
            //SaveIndicators();

            _changedFieldsCollection.Clear();

            return MainData.ContractId;
        }

        /// <summary>
        ///   Validates data in ContractDetailsCommon control
        /// </summary>
        /// <returns></returns>
        public override ValidateMetadata ValidateData() {
            ValidateMetadata lResult = new ValidateMetadata();
            lResult.IsCorrect = Validate();
            if (lResult.IsCorrect) {
                CancelEventArgs lAgrs = new CancelEventArgs(false);
                OnValidating(editContractNum, lAgrs);
                lResult.IsCorrect = !lAgrs.Cancel;
            }
            return lResult;
        }

        /// <summary>
        ///   Gets or sets the current comment.
        /// </summary>
        /// <value>The current comment.</value>
        public void AppendComment(string comment) {
            StringCollection lLines = new StringCollection();
            lLines.AddRange(memoContractComment.Lines);
            lLines.Add(comment);
            memoContractComment.Lines = lLines.Cast<string>().ToList().ToArray();
            SaveFields();
        }

        /// <summary>
        ///   Resets sign date of the contract and saves this information into database.
        /// </summary>
        public void ResetSignDate() {
            dateSignDate.EditValue = null;
            dateSignDate.Properties.NullDate = null;
            _dataTableFields.Rows[0][Constants.FIELD_COMMON_CONTRACT_DATE_SIGNED] = DateTime.MinValue;
            SaveFields();
        }

        /// <summary>
        ///   Updates the budget label.
        /// </summary>
        public void UpdateBudgetLabel() {
            if (MainData.IsBudgetOverdraft && !MainData.IsBudgetApproved)
                lblIsBudget.Text = Resource.LabelIsNotBudgetAndOverhead;
            else if (MainData.IsBudgetOverdraft)
                lblIsBudget.Text = Resource.LabelIsBudgetOverhead;
            else if (!MainData.IsBudgetApproved)
                lblIsBudget.Text = Resource.LabelIsNotBudget;
            else
                lblIsBudget.Text = string.Empty;
        }

        /// <summary>
        ///   Attaches the events.
        /// </summary>
        /// <param name = "attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach) {
                spinContractTerm.Leave += spinContractTerm_Leave;
                dateSignPlanned.Leave += dateSignPlanned_Leave;
                dateSignDate.EditValueChanged += OnEditValueChanged;
                editContractNum.EditValueChanged += OnEditValueChanged;
                dateSignPlanned.EditValueChanged += OnEditValueChanged;
                memoContractComment.EditValueChanged += OnEditValueChanged;
                memoContractCommentAfter.EditValueChanged += OnEditValueChanged;
                spinContractTerm.EditValueChanged += OnEditValueChanged;
                cbBudgetType.EditValueChanged += OnEditValueChanged;
                cbBudgetOwner.EditValueChanged += OnEditValueChanged;
            }
            else {
                spinContractTerm.Leave -= spinContractTerm_Leave;
                dateSignPlanned.Leave -= dateSignPlanned_Leave;
                dateSignDate.EditValueChanged -= OnEditValueChanged;
                editContractNum.EditValueChanged -= OnEditValueChanged;
                dateSignPlanned.EditValueChanged -= OnEditValueChanged;
                memoContractComment.EditValueChanged -= OnEditValueChanged;
                memoContractCommentAfter.EditValueChanged -= OnEditValueChanged;
                spinContractTerm.EditValueChanged -= OnEditValueChanged;
                cbBudgetType.EditValueChanged -= OnEditValueChanged;
                cbBudgetOwner.EditValueChanged -= OnEditValueChanged;
            }
        }

/*
        /// <summary>
        ///   Gets the budget max period value.
        /// </summary>
        /// <param name = "rowHandle">The row handle.</param>
        /// <param name = "numColumnEnd">The num column end.</param>
        /// <returns></returns>
        private decimal GetBudgetMaxPeriodValue(int rowHandle, int numColumnEnd) {
            decimal lMaxValue = 0;
            gridViewBudgetPlan.UpdateCurrentRow();
            foreach (GridColumn lColumn in gridViewBudgetPlan.Columns) {
                if (IsPeriodColumn(lColumn)) {
                    if (numColumnEnd > 0 && GetBudgetPeriodColumnNumber(lColumn) >= numColumnEnd)
                        break;

                    decimal lCurrentValue = ConvertEx.ToDecimal(gridViewBudgetPlan.GetRowCellValue(rowHandle, lColumn));
                    if (lCurrentValue > lMaxValue)
                        lMaxValue = lCurrentValue;
                }
            }
            return lMaxValue;
        }
*/

/*
        private decimal GetBudgetSumPeriodValue(int rowHandle, int numColumnEnd) {
            decimal lSumValue = 0;
            gridViewBudgetPlan.UpdateCurrentRow();
            foreach (GridColumn lColumn in gridViewBudgetPlan.Columns) {
                if (IsPeriodColumn(lColumn)) {
                    if (numColumnEnd > 0 && GetBudgetPeriodColumnNumber(lColumn) >= numColumnEnd)
                        break;

                    lSumValue += ConvertEx.ToDecimal(gridViewBudgetPlan.GetRowCellValue(rowHandle, lColumn));
                }
            }
            return lSumValue;
        }
*/

        /// <summary>
        ///   Gets the budget period column number.
        /// </summary>
        /// <param name = "column">The column.</param>
        /// <returns></returns>
        private static int GetBudgetPeriodColumnNumber(GridColumn column) {
            return GetPeriodColumnNumber(column, new[] {Constants.DYNAMIC_COLUMN_PREFIX_DB});
        }

        /// <summary>
        ///   Gets the document changes.
        /// </summary>
        /// <param name = "rowStates">The row states.</param>
        /// <returns></returns>
        private Collection<FileUploader.FileDataInfo> GetPhotosChanges(DataRowState rowStates) {
            return photoUploader.GetFiles(rowStates);
        }


        /// <summary>
        ///   Loads the budget plan.
        /// </summary>
        private void LoadBudgetPlan() {
            int lContractId = MainData.ContractId;

            LoadBudgetPlanDynamically(lContractId, SelectedDateSignPlanned);
        }

        /// <summary>
        ///   Loads the budget plan.
        /// </summary>
        private void LoadBudgetPlanDynamically(int contratId, DateTime newBeginDate) {
            gridControlBudgetPlan.BeginUpdate();
            int lScrollX = gridViewBudgetPlan.LeftCoord;
            try {
                DataTable lDataTable = DataProvider.GetBudgetPlan(contratId);
                gridControlBudgetPlan.DataSource = lDataTable;

                // rename all columns adequatelly
                if (lDataTable == null || lDataTable.Rows.Count <= 0)
                    return;

                gridViewBudgetPlan.PopulateColumns();

                foreach (GridColumn lColumn in gridViewBudgetPlan.Columns) {
                    // dev express adds col prefix when populate column by its own                    
                    if (IsPeriodColumn(lColumn)) {
                        int lPeriodNumber = GetBudgetPeriodColumnNumber(lColumn);
                        int lMonth = newBeginDate.Month + lPeriodNumber - 1;
                        int lYear = (int) (newBeginDate.Year + Math.Ceiling(lMonth / 12.0)) - 1;
                        lColumn.Caption = string.Format("{0} {1}", ConvertEx.ToMonthName(lMonth), lYear);
                        lColumn.ColumnEdit = repositoryBudgetItemSpinEdit;
                    }

                    if (MainData.IsBudgetApproved) {
                        lColumn.OptionsColumn.ReadOnly = true;
                        lColumn.OptionsColumn.AllowEdit = false;
                    }

                    lColumn.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    lColumn.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
                    lColumn.DisplayFormat.FormatType = FormatType.Numeric;
                    lColumn.DisplayFormat.FormatString = "{0:N3}";
                }

                // restrore columns settings
                gridViewBudgetPlan.Columns[columnBudgetPlanId.FieldName].Visible = false;
                gridViewBudgetPlan.Columns[columnBudgetPlanStartDate.FieldName].Visible = false;
                gridViewBudgetPlan.Columns[columnBudgetPlanName.FieldName].OptionsColumn.ReadOnly = true;
                gridViewBudgetPlan.Columns[columnBudgetPlanTotal.FieldName].OptionsColumn.ReadOnly = true;
                gridViewBudgetPlan.Columns[columnBudgetPlanName.FieldName].OptionsColumn.AllowEdit = false;
                gridViewBudgetPlan.Columns[columnBudgetPlanTotal.FieldName].OptionsColumn.AllowEdit = false;
                gridViewBudgetPlan.Columns[columnBudgetPlanName.FieldName].Caption = columnBudgetPlanName.Caption;
                gridViewBudgetPlan.Columns[columnBudgetPlanTotal.FieldName].Caption = columnBudgetPlanTotal.Caption;
                gridViewBudgetPlan.BestFitColumns();

//                NormalizeBudgetTable();
//                UpdateBudgetTotals();
            }
            finally {
                gridViewBudgetPlan.LeftCoord = lScrollX;
                gridControlBudgetPlan.EndUpdate();
            }
        }

        /// <summary>
        ///   Loads the client info.
        /// </summary>
        private void LoadClientInfo() {
            gridControlClientInfo.DataSource = DataProvider.GetDetailsClientBreif(MainData.ClientId);
        }

        /// <summary>
        ///   Loads the competitors.
        /// </summary>
        private void LoadCompetitors() {
            gridControlCompetitors.DataSource = DataProvider.GetCompetitors(MainData.ContractId);
        }

        /// <summary>
        ///   Loads the competitors drop down.
        /// </summary>
        private void LoadCompetitorsDropDown() {
            repositoryCompetitorId.DataSource = DataProvider.CompetitorList;
        }

        /// <summary>
        ///   Loads the distributors.
        /// </summary>
        private void LoadDistributors() {
            listDistributorsKeg.DataSource = DataProvider.GetDetailsDistributorList(MainData.ContractId, AddressList, 1);
            listDistributorsRest.DataSource = DataProvider.GetDetailsDistributorList(MainData.ContractId, AddressList, 2);
        }

        private void LoadBudgetTypes() {
            DataTable lDtBudgetTypeList = DataProvider.BudgetTypeList;
            if (lDtBudgetTypeList == null || lDtBudgetTypeList.Rows.Count <= 0)
                return;

            cbBudgetType.Properties.DataSource = lDtBudgetTypeList;
            //cbBudgetType.EditValue = lDtBudgetTypeList.Rows[0][cbBudgetType.Properties.ValueMember];
        }

        private void LoadBudgetOwners() {
            DataTable lDtBudgetOwnerList = DataProvider.BudgetOwnerList;
            if (lDtBudgetOwnerList == null || lDtBudgetOwnerList.Rows.Count <= 0)
                return;

            cbBudgetOwner.Properties.DataSource = lDtBudgetOwnerList;
            //cbBudgetOwner.EditValue = lDtBudgetOwnerList.Rows[0][cbBudgetOwner.Properties.ValueMember];
        }

        /// <summary>
        ///   Loads the drop downs.
        /// </summary>
        private void LoadDropDowns() {
            LoadCompetitorsDropDown();
            LoadBudgetTypes();
            LoadBudgetOwners();
        }

        /// <summary>
        ///   Loads the POC list.
        /// </summary>
        private void LoadPOCList() {
            gridControlPOCList.DataSource = DataProvider.GetDetailsPOCList(MainData.ContractId, AddressList);
        }

        /// <summary>
        ///   Loads the files.
        /// </summary>
        private void LoadFiles() {
            photoUploader.Data = DataProvider.GetPhotoList(MainData.ContractId);
        }

        /// <summary>
        ///   Loads the previous contract.
        /// </summary>
        private void LoadPreviousContract() {
            gridControlPrevContract.DataSource = DataProvider.GePreviousContract(MainData.ClientId, MainData.ContractId);
        }

        /// <summary>
        ///   Loads the sales average competitors.
        /// </summary>
        private void LoadSalesAverageCompetitors() {
            monthlySalesCompetitors.DataSource = DataProvider.GetDetailsSalesAvg(MainData.ContractId, AddressList, true);
        }

        /// <summary>
        ///   Loads the sales average.
        /// </summary>
        private void LoadSalesAverageInBev() {
            monthlySalesInBev.DataSource = DataProvider.GetDetailsSalesAvg(MainData.ContractId, AddressList, false);
        }

//        /// <summary>
//        ///   Normalizes the budget row.
//        /// </summary>
//        /// <param name = "rowHandle">The row handle.</param>
//        /// <param name = "periodNumber"></param>
//        /// <param name = "newValue"></param>
//        /// <returns></returns>
//        private void NormalizeBudgetRow(int rowHandle, int periodNumber, decimal newValue) {
//            foreach (GridColumn lColumn in gridViewBudgetPlan.Columns) {
//                if (IsPeriodColumn(lColumn) && GetBudgetPeriodColumnNumber(lColumn) > periodNumber) {
//                    decimal lCurrentValue = ConvertEx.ToDecimal(gridViewBudgetPlan.GetRowCellValue(rowHandle, lColumn));
//                    if (lCurrentValue > newValue)
//                        newValue = lCurrentValue;
//
//                    if (lCurrentValue < newValue)
//                        gridViewBudgetPlan.SetRowCellValue(rowHandle, lColumn, newValue);
//                }
//            }
//            UpdateBudgetTotals();
//        }

//        /// <summary>
//        ///   Normalizes the budget table.
//        /// </summary>
//        private void NormalizeBudgetTable() {
//            GridColumn lFirstPeriodColumn = null;
//            foreach (GridColumn lColumn in gridViewBudgetPlan.Columns) {
//                if (IsPeriodColumn(lColumn)) {
//                    lFirstPeriodColumn = lColumn;
//                    break;
//                }
//            }
//
//            if (lFirstPeriodColumn != null) {
//                for (int rowHandle = 0; rowHandle < gridViewBudgetPlan.RowCount; rowHandle++) {
//                    decimal firstValue = ConvertEx.ToDecimal(gridViewBudgetPlan.GetRowCellValue(rowHandle, lFirstPeriodColumn));
//                    NormalizeBudgetRow(rowHandle, GetBudgetPeriodColumnNumber(lFirstPeriodColumn), firstValue);
//                }
//            }
//        }

        /// <summary>
        ///   Saves the competitors.
        /// </summary>
        private void SaveCompetitors() {
            DataProvider.SetCompetitors(MainData.ContractId, CompetitorsList);
        }

        /// <summary>
        ///   Saves the documents.
        /// </summary>
        private void SaveDocuments() {
            // Save newly added documents
            Collection<FileUploader.FileDataInfo> lDocumentsAdded = GetPhotosChanges(DataRowState.Added);
            foreach (FileUploader.FileDataInfo lDoc in lDocumentsAdded) {
                if (!string.IsNullOrEmpty(lDoc.FilePath) && lDoc.FileData != null && lDoc.FileData.Length > 0)
                    DataProvider.InsertFile(MainData.ContractId, lDoc.FilePath, lDoc.FileData);
            }

            // Deleted documents
            Collection<FileUploader.FileDataInfo> lDocumentsDeleted = GetPhotosChanges(DataRowState.Deleted);
            foreach (FileUploader.FileDataInfo lDoc in lDocumentsDeleted) {
                if (lDoc.Id > 0)
                    DataProvider.DeleteFile(lDoc.Id);
            }
        }

        /// <summary>
        ///   Saves the fields of contract details.
        /// </summary>
        private void SaveFields() {
            SqlXml lSQLXml = ConvertEx.ToSqlXml(AllFieldsData,
                new[] {
                    Constants.FIELD_COMMON_CONTRACT_ID,
                    Constants.FIELD_COMMON_CONTRACT_DATE_SIGNED,
                    Constants.FIELD_COMMON_CONTRACT_NUM,
                    Constants.FIELD_COMMON_CONTRACT_LENGTH,
                    Constants.FIELD_COMMON_CONTRACT_PLAN_SIGNDATE,
                    Constants.FIELD_COMMON_CONTRACT_COMMENT,
                    Constants.FIELD_COMMON_CONTRACT_COMMENT2,
                    Constants.FIELD_COMMON_BUDGETTYPE_ID,
                    Constants.FIELD_COMMON_BUDGETOWNER_ID,
                    Constants.FIELD_COMMON_CalcUpliftWOBaskets
                });
            DataProvider.SetCommonDetails(lSQLXml);
            //Trigger saving addresses with null parameters in order to recalculate data in DB related to this contract.
            DataProvider.SetPocAddressesList(MainData.ContractId, false, null, null);
        }

        /// <summary>
        ///   Saves the monthly sales competitors.
        /// </summary>
        private void SaveMonthlySalesCompetitors() {
            DataProvider.SetDetailsSalesAvgCompetitors(MainData.ContractId, MonthlySalesCompetitorsList);
        }

//        /// <summary>
//        ///   Updates the budget totals.
//        /// </summary>
//        private void UpdateBudgetTotals() {
//            // calculate total column
//            for (int lRowHandle = 0; lRowHandle < gridViewBudgetPlan.RowCount; lRowHandle++) {
//                decimal lTotalSum;
//                if (ConvertEx.ToInt(gridViewBudgetPlan.GetRowCellValue(lRowHandle, columnBudgetPlanId.FieldName)) == 1) {
//                    lTotalSum = GetBudgetSumPeriodValue(lRowHandle, 0);
//                    gridViewBudgetPlan.SetRowCellValue(lRowHandle, columnBudgetPlanTotal, lTotalSum);
//                }
//                else {
//                    lTotalSum = GetBudgetMaxPeriodValue(lRowHandle, 0);
//                    gridViewBudgetPlan.SetRowCellValue(lRowHandle, columnBudgetPlanTotal, lTotalSum);
//                }
//            }
//            MainData.TotalBudgetInvestments = TotalBudgetInvestments;
//        }

        /// <summary>
        ///   Updates the fields status.
        /// </summary>
        private void UpdateFieldsStatus(ContractStatus prevStatus, ContractStatus newStatus) {
            dateSignDate.Enabled = newStatus == ContractStatus.SignRequired;
            dateSignPlanned.Enabled = !dateSignDate.Enabled;
            if (dateSignPlanned.Enabled)
                dateSignPlanned.Enabled = !MainData.IsContractSigned;

            dateSignDate.Properties.MinValue = dateSignDate.Enabled ? DateTime.Today : DateTime.MinValue;
        }

        /// <summary>
        ///   Synchronizes MainData property with inner appropriate controls.
        /// </summary>
        private void UpdateMainData() {
            MainData.DateSignPlanned = ConvertEx.ToDateTime(SelectedDateSignPlanned);
            OldSignPlanned = SelectedDateSignPlanned;
        }

        private void UpdateReadOnlyControls() {
            spinContractTerm.Enabled = !MainData.IsBudgetApproved;
            dateSignPlanned.Enabled = !MainData.IsBudgetApproved;
        }

        #endregion

        #region Event Handling

        private void ContractDetailsCommon_Load(object sender, EventArgs e) {
            GridLocalizer.Active = new DefaultLocalizer();
        }

        /// <summary>
        ///   Handles the BudgetLabelUpdated event of the MainData control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void MainData_BudgetLabelUpdated(object sender, EventArgs e) {
            UpdateBudgetLabel();
        }

        /// <summary>
        ///   Handles the ContractStatusChanged event of the MainData control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void MainData_ContractStatusChanged(object sender, EventArgs e) {
            CommonBaseExternalData.ContractStatusEventArgs lArgs = e as CommonBaseExternalData.ContractStatusEventArgs;
            if (lArgs != null)
                UpdateFieldsStatus(lArgs.PrevStatus, lArgs.NewStatus);
        }

        /// <summary>
        ///   Handles the ContractStatusChanging event of the MainData control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void MainData_ContractStatusChanging(object sender, EventArgs e) {
            CommonBaseExternalData.ContractStatusEventArgs lArgs = e as CommonBaseExternalData.ContractStatusEventArgs;
            if (lArgs != null)
                RequireSignDateContractNum = lArgs.PrevStatus == ContractStatus.SignRequired && lArgs.NewStatus == ContractStatus.InWork;
        }

        /// <summary>
        ///   Called when [edit value changed].
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void OnEditValueChanged(object sender, EventArgs e) {
            IsChanged = true;
            _dependedFieldChanged = true;
            if (!IsExternalRefreshRequired)
                IsExternalRefreshRequired = (sender == spinContractTerm || sender == dateSignPlanned);

            // memorize changed field
            Control lControl = sender as Control;
            if (lControl != null && !_changedFieldsCollection.Contains(lControl))
                _changedFieldsCollection.Add(lControl);
        }

        /// <summary>
        ///   Called when [validating].
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.ComponentModel.CancelEventArgs" /> instance containing the event data.</param>
        private void OnValidating(object sender, CancelEventArgs e) {
            BaseEdit lBaseEdit = sender as BaseEdit;
            if (lBaseEdit == null || !lBaseEdit.Enabled)
                return;

            bool lIsFieldEmpty = lBaseEdit.EditValue == null || string.IsNullOrEmpty(lBaseEdit.EditValue.ToString());
            if (lIsFieldEmpty) {
                if (lBaseEdit == editContractNum)
                    e.Cancel = true;
                else if (lBaseEdit == dateSignDate)
                    e.Cancel = RequireSignDateContractNum;

                if (e.Cancel)
                    SetError(lBaseEdit, Resource.FieldEmptyError);
            }
            else if (lBaseEdit == editContractNum) {
                if (ContractListTab.ContractList != null) {
                    SetError(editContractNum, null);
                    e.Cancel = DataProvider.CheckContractNo(MainData.ContractId, editContractNum.Text);
                    if (e.Cancel)
                        SetError(editContractNum, Resource.ContractUniqueError);
                }
            }
        }

        /// <summary>
        ///   Event handler that is used to correctly focus control after recalculation occured.
        ///   You have to attach this method to all keyboard-editable controls (Enter event).
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        private void controlGetFocus(object sender, EventArgs e) {
            Control lFocusedControl = sender as Control;
            if (lFocusedControl != null)
                lFocusedControl.Focus();
        }

        /// <summary>
        ///   Handles the Leave event of the dateSignPlanned control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void dateSignPlanned_Leave(object sender, EventArgs e) {
            if (OldSignPlanned != SelectedDateSignPlanned) {
                //TODO: Needs to be reviewed
                //LoadBudgetPlan();
                UpdateMainData();
            }
        }

        /// <summary>
        ///   Handles the ButtonClick event of the gridControlCompetitors_EmbeddedNavigator control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "DevExpress.XtraEditors.NavigatorButtonClickEventArgs" /> instance containing the event data.</param>
        private void gridControlCompetitors_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e) {
            if (e.Button.ButtonType == NavigatorButtonType.Remove) {
                if (XtraMessageBox.Show(Resource.AskRecordDelete, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    e.Handled = true;
            }
        }


        private void gridViewBudgetPlan_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e) {
            GridView gv = (GridView) sender;
            if (e.Value.GetType() == typeof (decimal) &&
                ConvertEx.ToInt(gv.GetListSourceRowCellValue(e.ListSourceRowIndex, columnBudgetPlanId.FieldName)) == (int) BudgetIndicator.TotalInvestments)
                e.DisplayText = ConvertEx.ToDecimal(e.Value).ToString("N2");
        }

        /// <summary>
        ///   Event handler, that is used to draw border around selected cell of GridView.
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        private void gridViewBudgetPlan_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e) {
            GridView lView = sender as GridView;
            if (lView != null &&
                e.Column == lView.FocusedColumn &&
                    e.RowHandle == lView.FocusedRowHandle &&
                        IsPeriodColumn(e.Column)) {
                const int penWidth = 2;
                Rectangle lRect = e.Bounds;
                lRect.Inflate(penWidth, penWidth);
                e.Graphics.DrawRectangle(new Pen(Color.RoyalBlue, penWidth), e.Bounds);
            }
        }

        /// <summary>
        ///   Photoes the uploader_ get file data.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The e.</param>
        private void photoUploader_GetFileData(object sender, FileUploader.FileDataInfo e) {
            e.FileData = DataProvider.GetPhoto(e.Id);
        }

        /// <summary>
        ///   Handles the leave event of the spinContractTerm control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void spinContractTerm_Leave(object sender, EventArgs e) {
            //Check whether depended field was changed
            if (OldContractTerm != SelectedContractTerm) {
                //TODO: Needs to be reviewed
                //LoadBudgetPlan();
                OldContractTerm = MainData.ContractTerm = SelectedContractTerm;
            }
        }

        #endregion

        #region ITwoStepSavingControl Members

        public bool IsSecondStepRequired { get; set; }

        public void SecondStageSave() {
            //SaveBudgetPlan();
        }

        #endregion

        #region Class Methods

        /// <summary>
        ///   Gets the period column number.
        /// </summary>
        /// <param name = "column">The column.</param>
        /// <param name = "possiblePrefixes"></param>
        /// <returns></returns>
        public static int GetPeriodColumnNumber(GridColumn column, string[] possiblePrefixes) {
            int lNum = 0;
            if (IsPeriodColumn(column) && possiblePrefixes != null && possiblePrefixes.Length > 0) {
                try {
                    foreach (string lPrefix in possiblePrefixes) {
                        if (column.FieldName.Contains(lPrefix)) {
                            lNum = ConvertEx.ToInt(column.FieldName.TrimStart(lPrefix.ToCharArray()));
                            if (lNum > 0)
                                return lNum;
                        }
                    }
                }
                catch (InvalidCastException) {
                    lNum = 0;
                }
            }
            return lNum;
        }

        /// <summary>
        ///   Determines whether [is period column] [the specified column].
        /// </summary>
        /// <param name = "column">The column.</param>
        /// <returns>
        ///   <c>true</c> if [is period column] [the specified column]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPeriodColumn(GridColumn column) {
            return column != null && column.FieldName.StartsWith(Constants.DYNAMIC_COLUMN_PREFIX_DB);
        }

        #endregion
    }
}