﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    partial class ContractListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractListControl));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.columnStatusId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.standaloneTool = new DevExpress.XtraBars.Bar();
            this.barButtonNew = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonEdit = new DevExpress.XtraBars.BarButtonItem();
            this.barEditNotSignedDays = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemNotSignedDays = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.barTextDays = new DevExpress.XtraBars.BarStaticItem();
            this.barEditDateFrom = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateFrom = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEditDateTo = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateTo = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnClientId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnResponsible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTerm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMoneySum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDateSigned = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDateEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDateCreation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChannelId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemChannelLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemNotSignedDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemChannelLookUp)).BeginInit();
            this.SuspendLayout();
            // 
            // columnStatusId
            // 
            this.columnStatusId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStatusId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStatusId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnStatusId.Caption = "Статус";
            this.columnStatusId.ColumnEdit = this.repositoryItemStatusLookUp;
            this.columnStatusId.FieldName = "CONTRACT_STATUS_ID";
            this.columnStatusId.Name = "columnStatusId";
            this.columnStatusId.OptionsColumn.AllowEdit = false;
            this.columnStatusId.OptionsColumn.AllowShowHide = false;
            this.columnStatusId.OptionsColumn.ReadOnly = true;
            this.columnStatusId.OptionsColumn.ShowInCustomizationForm = false;
            this.columnStatusId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnStatusId.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.columnStatusId.Visible = true;
            this.columnStatusId.VisibleIndex = 3;
            this.columnStatusId.Width = 82;
            // 
            // repositoryItemStatusLookUp
            // 
            this.repositoryItemStatusLookUp.AutoHeight = false;
            this.repositoryItemStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Статус")});
            this.repositoryItemStatusLookUp.DisplayMember = "NAME";
            this.repositoryItemStatusLookUp.Name = "repositoryItemStatusLookUp";
            this.repositoryItemStatusLookUp.ReadOnly = true;
            this.repositoryItemStatusLookUp.ValueMember = "ID";
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.standaloneTool});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonNew,
            this.barButtonEdit,
            this.barEditDateFrom,
            this.barEditDateTo,
            this.barEditNotSignedDays,
            this.barTextDays});
            this.barManager.LargeImages = this.imCollection;
            this.barManager.MaxItemId = 22;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateFrom,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemDateTo,
            this.repositoryItemColorEdit1,
            this.repositoryItemNotSignedDays,
            this.repositoryItemTextEdit3});
            // 
            // standaloneTool
            // 
            this.standaloneTool.BarName = "Tools";
            this.standaloneTool.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.standaloneTool.DockCol = 0;
            this.standaloneTool.DockRow = 0;
            this.standaloneTool.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.standaloneTool.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonEdit, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barEditNotSignedDays, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTextDays),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditDateFrom),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditDateTo)});
            this.standaloneTool.OptionsBar.AllowQuickCustomization = false;
            this.standaloneTool.OptionsBar.DisableCustomization = true;
            this.standaloneTool.OptionsBar.DrawDragBorder = false;
            this.standaloneTool.OptionsBar.UseWholeRow = true;
            this.standaloneTool.Text = "Tools";
            // 
            // barButtonNew
            // 
            this.barButtonNew.Caption = "Новый";
            this.barButtonNew.Id = 5;
            this.barButtonNew.ImageIndex = 0;
            this.barButtonNew.Name = "barButtonNew";
            this.barButtonNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonNew_ItemClick);
            // 
            // barButtonEdit
            // 
            this.barButtonEdit.Caption = "Редактировать";
            this.barButtonEdit.Id = 10;
            this.barButtonEdit.ImageIndex = 1;
            this.barButtonEdit.Name = "barButtonEdit";
            this.barButtonEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonEdit_ItemClick);
            // 
            // barEditNotSignedDays
            // 
            this.barEditNotSignedDays.Caption = "Не подписанный контракт за:";
            this.barEditNotSignedDays.Edit = this.repositoryItemNotSignedDays;
            this.barEditNotSignedDays.EditValue = "21";
            this.barEditNotSignedDays.Id = 19;
            this.barEditNotSignedDays.ImageIndex = 4;
            this.barEditNotSignedDays.Name = "barEditNotSignedDays";
            // 
            // repositoryItemNotSignedDays
            // 
            this.repositoryItemNotSignedDays.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemNotSignedDays.AutoHeight = false;
            this.repositoryItemNotSignedDays.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemNotSignedDays.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemNotSignedDays.IsFloatValue = false;
            this.repositoryItemNotSignedDays.Mask.EditMask = "###";
            this.repositoryItemNotSignedDays.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.repositoryItemNotSignedDays.Name = "repositoryItemNotSignedDays";
            this.repositoryItemNotSignedDays.EditValueChanged += new System.EventHandler(this.repositoryItemNotSignedDays_EditValueChanged);
            // 
            // barTextDays
            // 
            this.barTextDays.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barTextDays.Caption = "дней";
            this.barTextDays.Id = 21;
            this.barTextDays.Name = "barTextDays";
            this.barTextDays.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barEditDateFrom
            // 
            this.barEditDateFrom.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditDateFrom.Caption = "Истекающий контракт с:";
            this.barEditDateFrom.Edit = this.repositoryItemDateFrom;
            this.barEditDateFrom.EditValue = new System.DateTime(2010, 12, 1, 11, 8, 48, 765);
            this.barEditDateFrom.Id = 11;
            this.barEditDateFrom.ImageIndex = 3;
            this.barEditDateFrom.Name = "barEditDateFrom";
            this.barEditDateFrom.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditDateFrom.Width = 100;
            // 
            // repositoryItemDateFrom
            // 
            this.repositoryItemDateFrom.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateFrom.AutoHeight = false;
            this.repositoryItemDateFrom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateFrom.Name = "repositoryItemDateFrom";
            this.repositoryItemDateFrom.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateFrom.DateTimeChanged += new System.EventHandler(this.repositoryItemDateFrom_DateTimeChanged);
            // 
            // barEditDateTo
            // 
            this.barEditDateTo.Caption = "по:";
            this.barEditDateTo.Edit = this.repositoryItemDateTo;
            this.barEditDateTo.EditValue = new System.DateTime(2010, 12, 1, 11, 8, 56, 421);
            this.barEditDateTo.Id = 14;
            this.barEditDateTo.Name = "barEditDateTo";
            this.barEditDateTo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditDateTo.Width = 100;
            // 
            // repositoryItemDateTo
            // 
            this.repositoryItemDateTo.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateTo.AutoHeight = false;
            this.repositoryItemDateTo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateTo.Name = "repositoryItemDateTo";
            this.repositoryItemDateTo.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateTo.DateTimeChanged += new System.EventHandler(this.repositoryItemDateTo_DateTimeChanged);
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "add.png");
            this.imCollection.Images.SetKeyName(1, "edit.png");
            this.imCollection.Images.SetKeyName(2, "delete.png");
            this.imCollection.Images.SetKeyName(3, "highlight_expired.PNG");
            this.imCollection.Images.SetKeyName(4, "highlight_not_signed.PNG");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 34);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemStatusLookUp,
            this.repositoryItemChannelLookUp});
            this.gridControl.Size = new System.Drawing.Size(956, 352);
            this.gridControl.TabIndex = 4;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnClientId,
            this.columnName,
            this.columnNumber,
            this.columnResponsible,
            this.columnStatusId,
            this.columnTerm,
            this.columnMoneySum,
            this.columnBudget,
            this.columnDateSigned,
            this.columnDateEnd,
            this.columnDateCreation,
            this.columnChannelId});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(177)))), ((int)(((byte)(100)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(177)))), ((int)(((byte)(100)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(117)))), ((int)(((byte)(117)))));
            styleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(117)))), ((int)(((byte)(117)))));
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition3.Expression = "Today()  > [DATE_VALID_TILL]";
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.columnStatusId;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition4.Expression = "[CONTRACT_STATUS_ID] == 7 Or  [CONTRACT_STATUS_ID] == 8  Or  [CONTRACT_STATUS_ID]" +
    " == 9 Or  [CONTRACT_STATUS_ID] == 10  Or  [CONTRACT_STATUS_ID] == 11";
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2,
            styleFormatCondition3,
            styleFormatCondition4});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.columnResponsible, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.columnStatusId, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.columnDateCreation, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.columnMoneySum, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            this.gridView.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.gridView_CustomColumnSort);
            this.gridView.DoubleClick += new System.EventHandler(this.gridView_DoubleClick);
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnId.Caption = "Id";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowShowHide = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // columnClientId
            // 
            this.columnClientId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnClientId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnClientId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnClientId.Caption = "CLIENT_ID";
            this.columnClientId.FieldName = "CLIENT_ID";
            this.columnClientId.Name = "columnClientId";
            // 
            // columnName
            // 
            this.columnName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnName.Caption = global::SoftServe.Reports.ContractMgmt.Resource.LegalClientName;
            this.columnName.FieldName = "LEGAL_NAME";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowShowHide = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            this.columnName.Width = 89;
            // 
            // columnNumber
            // 
            this.columnNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.columnNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnNumber.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractNum;
            this.columnNumber.FieldName = "CONTRACT_NUM";
            this.columnNumber.Name = "columnNumber";
            this.columnNumber.OptionsColumn.AllowEdit = false;
            this.columnNumber.OptionsColumn.AllowShowHide = false;
            this.columnNumber.OptionsColumn.ReadOnly = true;
            this.columnNumber.OptionsColumn.ShowInCustomizationForm = false;
            this.columnNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnNumber.Visible = true;
            this.columnNumber.VisibleIndex = 1;
            this.columnNumber.Width = 89;
            // 
            // columnResponsible
            // 
            this.columnResponsible.AppearanceHeader.Options.UseTextOptions = true;
            this.columnResponsible.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnResponsible.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnResponsible.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractCurrentResponsible;
            this.columnResponsible.FieldName = "CURRENT_RESPONSIBLE";
            this.columnResponsible.Name = "columnResponsible";
            this.columnResponsible.OptionsColumn.AllowEdit = false;
            this.columnResponsible.OptionsColumn.AllowShowHide = false;
            this.columnResponsible.OptionsColumn.ReadOnly = true;
            this.columnResponsible.OptionsColumn.ShowInCustomizationForm = false;
            this.columnResponsible.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnResponsible.Visible = true;
            this.columnResponsible.VisibleIndex = 2;
            this.columnResponsible.Width = 130;
            // 
            // columnTerm
            // 
            this.columnTerm.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTerm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTerm.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTerm.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractTerm;
            this.columnTerm.FieldName = "TERM";
            this.columnTerm.Name = "columnTerm";
            this.columnTerm.OptionsColumn.AllowEdit = false;
            this.columnTerm.OptionsColumn.AllowShowHide = false;
            this.columnTerm.OptionsColumn.ReadOnly = true;
            this.columnTerm.OptionsColumn.ShowInCustomizationForm = false;
            this.columnTerm.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnTerm.Visible = true;
            this.columnTerm.VisibleIndex = 4;
            this.columnTerm.Width = 82;
            // 
            // columnMoneySum
            // 
            this.columnMoneySum.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMoneySum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMoneySum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMoneySum.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractSum;
            this.columnMoneySum.FieldName = "COST";
            this.columnMoneySum.Name = "columnMoneySum";
            this.columnMoneySum.OptionsColumn.AllowEdit = false;
            this.columnMoneySum.OptionsColumn.AllowShowHide = false;
            this.columnMoneySum.OptionsColumn.ReadOnly = true;
            this.columnMoneySum.OptionsColumn.ShowInCustomizationForm = false;
            this.columnMoneySum.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnMoneySum.Visible = true;
            this.columnMoneySum.VisibleIndex = 5;
            this.columnMoneySum.Width = 128;
            // 
            // columnBudget
            // 
            this.columnBudget.AppearanceHeader.Options.UseTextOptions = true;
            this.columnBudget.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnBudget.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnBudget.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractBudget;
            this.columnBudget.FieldName = "IS_BUDGET";
            this.columnBudget.Name = "columnBudget";
            this.columnBudget.OptionsColumn.AllowEdit = false;
            this.columnBudget.OptionsColumn.AllowShowHide = false;
            this.columnBudget.OptionsColumn.ReadOnly = true;
            this.columnBudget.OptionsColumn.ShowInCustomizationForm = false;
            this.columnBudget.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnBudget.Visible = true;
            this.columnBudget.VisibleIndex = 6;
            this.columnBudget.Width = 51;
            // 
            // columnDateSigned
            // 
            this.columnDateSigned.AppearanceHeader.Options.UseTextOptions = true;
            this.columnDateSigned.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnDateSigned.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnDateSigned.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractDateSigned;
            this.columnDateSigned.FieldName = "DATE_SIGNED";
            this.columnDateSigned.Name = "columnDateSigned";
            this.columnDateSigned.OptionsColumn.AllowEdit = false;
            this.columnDateSigned.OptionsColumn.AllowShowHide = false;
            this.columnDateSigned.OptionsColumn.ReadOnly = true;
            this.columnDateSigned.OptionsColumn.ShowInCustomizationForm = false;
            this.columnDateSigned.Visible = true;
            this.columnDateSigned.VisibleIndex = 7;
            this.columnDateSigned.Width = 76;
            // 
            // columnDateEnd
            // 
            this.columnDateEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.columnDateEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnDateEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnDateEnd.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractDateFinish;
            this.columnDateEnd.FieldName = "DATE_VALID_TILL";
            this.columnDateEnd.Name = "columnDateEnd";
            this.columnDateEnd.OptionsColumn.AllowEdit = false;
            this.columnDateEnd.OptionsColumn.AllowShowHide = false;
            this.columnDateEnd.OptionsColumn.ReadOnly = true;
            this.columnDateEnd.OptionsColumn.ShowInCustomizationForm = false;
            this.columnDateEnd.Visible = true;
            this.columnDateEnd.VisibleIndex = 8;
            this.columnDateEnd.Width = 76;
            // 
            // columnDateCreation
            // 
            this.columnDateCreation.AppearanceHeader.Options.UseTextOptions = true;
            this.columnDateCreation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnDateCreation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnDateCreation.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractDateCreated;
            this.columnDateCreation.FieldName = "DATE_CREATED";
            this.columnDateCreation.Name = "columnDateCreation";
            this.columnDateCreation.OptionsColumn.AllowEdit = false;
            this.columnDateCreation.OptionsColumn.AllowShowHide = false;
            this.columnDateCreation.OptionsColumn.ReadOnly = true;
            this.columnDateCreation.OptionsColumn.ShowInCustomizationForm = false;
            this.columnDateCreation.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.columnDateCreation.Visible = true;
            this.columnDateCreation.VisibleIndex = 9;
            this.columnDateCreation.Width = 76;
            // 
            // columnChannelId
            // 
            this.columnChannelId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnChannelId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnChannelId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnChannelId.Caption = global::SoftServe.Reports.ContractMgmt.Resource.ContractChannel;
            this.columnChannelId.ColumnEdit = this.repositoryItemChannelLookUp;
            this.columnChannelId.FieldName = "CHANNEL_ID";
            this.columnChannelId.Name = "columnChannelId";
            this.columnChannelId.OptionsColumn.AllowEdit = false;
            this.columnChannelId.OptionsColumn.ReadOnly = true;
            this.columnChannelId.OptionsColumn.ShowInCustomizationForm = false;
            this.columnChannelId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnChannelId.Visible = true;
            this.columnChannelId.VisibleIndex = 10;
            this.columnChannelId.Width = 50;
            // 
            // repositoryItemChannelLookUp
            // 
            this.repositoryItemChannelLookUp.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemChannelLookUp.AutoHeight = false;
            this.repositoryItemChannelLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemChannelLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "Канал")});
            this.repositoryItemChannelLookUp.DisplayMember = "ChanelType";
            this.repositoryItemChannelLookUp.Name = "repositoryItemChannelLookUp";
            this.repositoryItemChannelLookUp.ReadOnly = true;
            this.repositoryItemChannelLookUp.ValueMember = "ChanelType_id";
            // 
            // ContractListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ContractListControl";
            this.Size = new System.Drawing.Size(956, 386);
            this.Load += new System.EventHandler(this.ClientListControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemNotSignedDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemChannelLookUp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar standaloneTool;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonNew;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarButtonItem barButtonEdit;
        private DevExpress.XtraBars.BarEditItem barEditDateFrom;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateFrom;
        private DevExpress.XtraBars.BarEditItem barEditDateTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnNumber;
        private DevExpress.XtraGrid.Columns.GridColumn columnResponsible;
        private DevExpress.XtraGrid.Columns.GridColumn columnStatusId;
        private DevExpress.XtraGrid.Columns.GridColumn columnTerm;
        private DevExpress.XtraGrid.Columns.GridColumn columnMoneySum;
        private DevExpress.XtraGrid.Columns.GridColumn columnBudget;
        private DevExpress.XtraGrid.Columns.GridColumn columnDateSigned;
        private DevExpress.XtraGrid.Columns.GridColumn columnDateEnd;
        private DevExpress.XtraBars.BarEditItem barEditNotSignedDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemNotSignedDays;
        private DevExpress.XtraBars.BarStaticItem barTextDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn columnDateCreation;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnClientId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemStatusLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columnChannelId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemChannelLookUp;


    }
}
