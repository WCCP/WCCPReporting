﻿namespace SoftServe.Reports.ContractMgmt.UserControls
{
    interface ITwoStepSavingControl
    {
        bool IsSecondStepRequired { get; set; }
        void SecondStageSave();
    }
}
