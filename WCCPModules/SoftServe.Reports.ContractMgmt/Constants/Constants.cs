﻿
namespace SoftServe.Reports.ContractMgmt
{
    /// <summary>
    /// 
    /// </summary>
    public static class Constants
    {
        public const int TAB_MAX_COUNT = 10;
        public const string DELIMITER_ID_LIST = ",";
        public const string TAB_POSTFIX_ASTERISK = " *";
        public const string DYNAMIC_COLUMN_PREFIX_DB = "PERIOD_";

        public const string SP_GET_COMPETITORS_LIST = "spDW_GetConcurrentBrands";

        public const string XML_OUTLET_ROOT_NAME = "root";
        public const string XML_OUTLET_ELEMENT_NAME = "row";
        public const string XML_OUTLET_ATTRIBUTE_FIELD_ID = "field_id";

        //*****************************************************************************************************************
        // Stored Procedures
        //*****************************************************************************************************************
        // Common
        public const string SP_GET_CONTRACT_LIST = "spDW_CM_ContractsList";
        public const string SP_GET_CLIENT_LIST = "spDW_CM_ContractClientsList";
        public const string SP_GET_CONTRACT_STATUS_LIST = "spDW_CM_GetContractStatusesList";
        public const string SP_GET_CLIENT_CHANNEL_LIST = "spDW_ChanelTypeList";
        public const string SpGetBudgetTypeList = "spDW_CM_BudgetTypes";
        public const string SpGetBudgetOwnerList = "spDW_CM_BudgetOwners";
        public const string SP_GET_COMMON_BRIEF_DETAILS = "spDW_CM_ContractBriefDetails";
        public const string SP_SET_COMMON_BRIEF_DETAILS = "spDW_CM_SetContractBriefDetails";
        public const string SP_GET_COMMON_BRIEF_CLIENT_DETAILS = "spDW_CM_ContractClientBriefDetails";
        public const string SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG = "spDW_CM_PastAverageMonthlyBeerSales";
        public const string SP_SET_COMMON_BRIEF_CLIENT_SALES_AVG_COMPETITOR = "spDW_CM_SetPastAverageMonthlyBeerSalesForConcurrents";
        public const string SP_GET_COMMON_BRIEF_DISTRIBUTORS = "spDW_CM_PastDistributors";
        public const string SP_GET_COMMON_BRIEF_POC_LIST = "spDW_CM_PastOutletsDepository";
        public const string SP_GET_COMMON_PHOTO_LIST = "spDW_CM_GetPhotoList";
        public const string SP_GET_COMMON_PHOTO_BY_ID = "spDW_CM_GetPhotoByID";
        public const string SP_INSERT_COMMON_PHOTO = "spDW_CM_InsertPhoto";
        public const string SP_DELETE_COMMON_PHOTO = "spDW_CM_DeletePhoto";
        public const string SP_GET_COMMON_COMPETITORS = "spDW_CM_ConcurentContractsList";
        public const string SP_SET_COMMON_COMPETITORS = "spDW_CM_SetConcurentContracts";
        public const string SP_GET_COMMON_PREV_CONTRACTS = "spDW_CM_PreviouseInBevContract";
        public const string SP_GET_COMMON_BUDGET_PLAN = "spDW_CM_GetContractBudgetPlan";
        public const string SP_SET_COMMON_BUDGET_PLAN = "spDW_CM_SetContractBudgetPlan";
        public const string SP_GET_COMMON_INDICATORS = "spDW_CM_GetOutletsStateIndicators";
        public const string SP_SET_COMMON_INDICATORS = "spDW_CM_SetOutletsStateIndicators";
        public const string SP_GET_CONTRACT_NEXT_STATUSES = "spDW_CM_GetPossibleContractStatuses";
        public const string SP_GET_ACCESS_LEVEL = "spDW_CM_GetAccessLevel";
        public const string SP_GET_USER_LEVEL = "spDW_CM_GetUserLevel";
        public const string SP_GET_CONTRACT_STATUS = "spDW_CM_GetContractStatus";
        public const string SP_SET_CONTRACT_STATUS = "spDW_CM_SetContractStatus";
        public const string SP_DELETE_CONTRACT = "spDW_CM_DeleteContract";
        public const string SP_INSERT_NEW_CONTRACT = "spDW_CM_InsertNewContract";
        public const string SP_GET_HAS_ACCESS_TO_ADDRESSES = "spDW_CM_UserHasAccessToAllContractPromoOutlets";
        public const string spDW_CM_CheckContractNo = "spDW_CM_CheckContractNo";
        // Conditions
        public const string SP_GET_CONDITIONS_FIELDS = "spDW_CM_CT_GetCommonConditions";
        public const string SP_SET_CONDITIONS_FIELDS = "spDW_CM_CT_SetCommonConditions";
        public const string SP_SET_CONDITIONS_BY_SKU = "spDW_CM_CT_SetContractVolumeTerms";
        // Investments
        public const string SP_SET_EXPENSES_FIELDS = "spDW_CM_SetContractInvestmentsBaseIndicators";       
        public const string SP_SET_EXPENSES_PLAN = "spDW_CM_SetExpensesPlan";
        public const string SP_GET_EXPENSES_FIELDS = "spDW_CM_GetContractBaseInvestmentIndicators";
        public const string SP_GET_EXPENSES_PAYMENT_SCHEMA_LIST = "spDW_CM_PaymentSchema";
        public const string SP_GET_EXPENSES_PAYMENT_TYPE_LIST = "spDW_CM_PaymentKind";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD = "spDW_CM_GetDynamicIndicators";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE = "spDW_CM_GetContractInvestments";
        public const string SP_SET_EXPENSES_INDICATORS_TABLE = "spDW_CM_SetContractInvestments";
        public const string SP_SET_EXPENSES_INDICATORS_SUM = "spDW_CM_SetContractBaseInvestmentIndicatorsSummary";
        public const string spDW_CM_GetAverageMacoPerDal = "spDW_CM_GetAverageMacoPerDal";
        // Avertisment
        public const string SP_GET_ADV_FIELDS = "spDW_CM_ADVERTISMENT_GetContractBaseInfo";
        public const string SP_SET_ADV_FIELDS = "spDW_CM_ADVERTISMENT_SetContractBaseInfo";
        public const string SP_GET_EVENTS = "spDW_CM_ADVERTISMENT_GetContractAdvertismentActivities";
        public const string SP_GET_ALL_EVENTS = "spDW_CM_ADVERTISMENT_GetAvailableAdvertismentActivities";
        public const string SP_SET_EVENTS = "spDW_CM_ADVERTISMENT_SetContractAdvertismentActivities";
        public const string SP_GET_ADV_EVENTS = "spDW_CM_ADVERTISMENT_GetContractAdvertismentActivities";
        public const string SP_GET_ADV_ALL_EVENTS = "spDW_CM_ADVERTISMENT_GetAvailableAdvertismentActivities";
        public const string SP_SET_ADV_EVENTS = "spDW_CM_ADVERTISMENT_SetContractAdvertismentActivities";
        public const string SP_GET_ADV_MEDIUMS = "spDW_CM_ADVERTISMENT_GetContractAdvertismentMediums";
        public const string SP_GET_ADV_ALL_MEDIUMS = "spDW_CM_ADVERTISMENT_GetAvailableAdvertismentMediums";
        public const string SP_SET_ADV_MEDIUMS = "spDW_CM_ADVERTISMENT_SetContractAdvertismentMediums";
        public const string SP_GET_ADV_PlANNED_CHANGES = "spDW_CM_ADVERTISMENT_GetContractAdvertismentPlannedChanges";
        public const string SP_SET_ADV_PLANNED_CHANGES = "spDW_CM_ADVERTISMENT_SetContractAdvertismentPlannedChanges";
        public const string SP_GET_ADV_PLANNED_ADV = "spDW_CM_ADVERTISMENT_GetContractPlannedPromotionalActivity";
        public const string SP_SET_ADV_PLANNED_ADV = "spDW_CM_ADVERTISMENT_SetContractPlannedPromotionalActivity";
        // Addresses
        public const string SP_GET_CONTRACT_ADDRESSES_LIST = "spDW_CM_GetContractOutletList";
        public const string SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID = "spDW_CM_GetOutletData";
        public const string SP_GET_CLIENT_OUTLETS = "spDW_CM_ADDR_GetContractClientAccesibleOutlets";
        public const string SP_SET_CONTRACT_ADDRESSES_LIST = "spDW_CM_ADDR_SetContractOutletsList";
        public const string SP_SET_OUTLET_COMMENT = "spDW_CM_SetOutletComment";

        //*****************************************************************************************************************
        // Stored Procedures params
        //*****************************************************************************************************************
        // Common
        public const string SP_PARAM_CONTRACT_ID = "CONTRACT_ID";
        public const string SP_PARAM_OL_ID = "OL_ID";
        public const string SP_PARAM_COMMENT = "Comment";
        public const string SP_PARAM_CONTRACT_Id = "contractId";
        public const string SP_PARAM_CONTRACT_NO = "contractNo";

        public const string SP_GET_COMMON_BRIEF_DETAILS_PARAM1 = "ID";
        public const string SP_SET_COMMON_BRIEF_DETAILS_PARAM1 = "BRIEF_DETAILS";
        public const string SP_GET_COMMON_BRIEF_CLEINT_DETAILS_PARAM1 = "CLIENT_ID";
        public const string SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG_PARAM1 = "ID";
        public const string SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG_PARAM2 = "ClientOutletsList";
        public const string SP_GET_COMMON_BRIEF_CLIENT_SALES_AVG_PARAM3 = "CONCURRENT_SALES";
        public const string SP_SET_COMMON_BRIEF_CLIENT_SALES_AVG_COMPETITOR_PARAM2 = "CONCURRENTSALES_LIST";
        public const string SP_GET_COMMON_BRIEF_DISTRIBUTORS_PARAM2 = "ClientOutletsList";
        public const string SP_GET_COMMON_BRIEF_DISTRIBUTORS_PARAM3 = "CHANNEL_ID";
        public const string SP_GET_COMMON_BRIEF_POC_LIST_PARAM2 = "ClientOutletsList";
        public const string SP_GET_COMMON_PHOTO_BY_ID_PARAM1 = "BINARYDATA_ID";        
        public const string SP_INSERT_COMMON_PHOTO_PARAM2 = "BINARYDATA_NAME";
        public const string SP_INSERT_COMMON_PHOTO_PARAM3 = "BINARY_DATA";
        public const string SP_DELETE_COMMON_PHOTO_PARAM1 = "BINARYDATA_ID";
        public const string SP_GET_COMMON_PREV_CONTRACTS_PARAM1 = "@CLIENT_ID";
        public const string SP_SET_COMMON_BUDGET_PLAN_PARAM2 = "XML_DATA";
        public const string SP_GET_COMMON_INDICATORS_PARAM2 = "PREFFERED_DECIMAL_SEPARATOR";
        public const string SP_GET_COMMON_INDICATORS_PARAM3 = "OUTLETS_LIST";
        public const string SP_SET_COMMON_INDICATORS_PARAM2 = "XML_DATA";
        public const string SP_SET_COMMON_COMPETITORS_PARAM2 = "XML_DATA";
        public const string SP_GET_CONTRACT_NEXT_STATUSES_PARAM2 = "CURRENT_STATE";
        public const string SP_SET_CONTRACT_STATUS_PARAM2 = "CONTRACT_STATUS_ID";
        public const string SP_INSERT_NEW_CONTRACT_PARAM1 = "CLIENT_ID";
        public const string SP_INSERT_NEW_CONTRACT_PARAM2 = "CHANNEL_ID";
        public const string SP_INSERT_NEW_CONTRACT_PARAM3 = "PLANNED_SIGN_DATE";
        public const string SP_INSERT_NEW_CONTRACT_PARAM4 = "CONTRACT_LENGTH";
        // Conditions
        public const string SP_SET_CONDITIONS_FIELDS_PARAM2 = "ALLOW_ADD_SKU";
        public const string SP_SET_CONDITIONS_BY_SKU_PARAM2 = "XML_DATA";   
        // Investments
        public const string SP_GET_EXPENSES_INDICATORS_TABLE_PARAM2 = "CONTRACT_BEGIN_DATE";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE_PARAM3 = "CONTRACT_TERM_LENGTH";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE_PARAM4 = "XML_DATA";
        public const string SP_SET_EXPENSES_INDICATORS_TABLE_PARAM2 = "XML_DATA";
        public const string SP_SET_EXPENSES_PLAN_PARAM2 = "EXPENSES_LIST_XML";
        public const string SP_SET_EXPENSES_FIELDS_PARAM2 = "XML_DATA";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM1 = "ContractId";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM2 = "IndicatorId";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM3 = "PeriodId";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM4 = "IsPlan";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM5 = "BaseIndicators";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM6 = "InvestmentSummary";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM7 = "Indicators";
        public const string SP_SET_EXPENSES_INDICATORS_SUM_PARAM2= "XML_DATA";
        // Advertisment
        public const string SP_SET_ADV_FIELDS_PARAM2 = "ADVERTISMENT_DESCRIPTION";
        public const string SP_SET_ADV_FIELDS_PARAM3 = "SPONSOR_COMPANY";
        public const string SP_SET_ADV_EVENTS_PARAM2 = "ADVERTISMENT_ACTIVITIES_LIST";
        public const string SP_SET_ADV_MEDIUMS_PARAM2 = "ADVERTISMENT_MEDIUMS_LIST";
        public const string SP_SET_ADV_PLANNED_CHANGES_PARAM2 = "ADVERTISMENT_CHANGES_LIST";
        public const string SP_SET_ADV_PLANNED_ADV_PARAM2 = "ADVERTISMENT_PLANNEDACTIVITY";
        // Addresses
        public const string SP_GET_CONTRACT_ADDRESSES_LIST_PARAM2 = "ShowControlOutlets";
        public const string SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM1 = "OUTLET_LIST";
        public const string SP_DELETE_ADDRESS_PARAM1 = "ID";
        public const string SP_GET_CLIENT_OUTLETS_CLIENT = "CLIENT_ID";
        public const string SP_GET_CLIENT_OUTLETS_CHANEL = "TRADE_CHANNEL";
        public const string SP_GET_CLIENT_OUTLETS_PROMO_GR = "PROMOTIONAL_OUTLETS";
        public const string SP_GET_CLIENT_OUTLETS_CONTROL_GR = "CONTROL_OUTLETS";
        public const string SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM2 = "CONTRACT_START";
        public const string SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM3 = "CONTRACT_TERM_LENGTH";
        public const string SP_GET_CONTRACT_ADDRESSES_LIST_BY_OLID_PARAM4 = "CONTRACT_CHANNEL";
        public const string SP_SET_CONTRACT_ADDRESSES_LIST_PARAM2 = "IS_CONTROL";
        public const string SP_SET_CONTRACT_ADDRESSES_LIST_PARAM3 = "OUTLETS_TO_INSERT";
        public const string SP_SET_CONTRACT_ADDRESSES_LIST_PARAM4 = "OUTLETS_TO_DELETE";
        
        //*****************************************************************************************************************
        // Fields
        //*****************************************************************************************************************
        public const string FIELD_CONTRACT_STATUS_ID = "CONTRACT_STATUS_ID";
        public const string FIELD_CHANNEL_ID = "ChanelType_id";
        public const string FIELD_CHANNEL_NAME = "ChanelType";
        // Common
        public const string FIELD_COMMON_CONTRACT_ID = "ID";
        public const string FIELD_COMMON_CLIENT_ID = "CLIENT_ID";
        public const string FIELD_COMMON_CREATOR_NAME = "CONTRACT_CREATOR";
        public const string FIELD_COMMON_CREATION_DATE = "CONTRACT_DATECREATED";
        public const string FIELD_COMMON_CONTRACT_NUM = "CONTRACT_NUM";
        public const string FIELD_COMMON_CONTRACT_LENGTH = "CONTRACT_LENGTH";
        public const string FIELD_COMMON_CONTRACT_CHANNEL = "CONTRACT_STAFFCHANNEL";
        public const string FIELD_COMMON_CONTRACT_COMMENT = "CONTRACT_COMMENT";
        public const string FIELD_COMMON_CONTRACT_COMMENT2 = "CONTRACT_COMMENT2";
        public const string FIELD_COMMON_BUDGETTYPE_ID = "BUDGET_TYPE_ID";
        public const string FIELD_COMMON_BUDGETOWNER_ID = "BUDGET_OWNER_ID";
        public const string FIELD_COMMON_CalcUpliftWOBaskets = "CalcUpliftWOBaskets";
        public const string FIELD_COMMON_CONTRACT_IS_BUDGET = "CONTRACT_ISBUDGET";
        public const string FIELD_COMMON_CONTRACT_PLAN_SIGNDATE = "SUSPECTED_SIGNDATE";
        public const string FIELD_COMMON_CONTRACT_DATE_SIGNED = "CONTRACT_DATESIGNED";
        public const string FIELD_CONTRACT_STATE_ID = "ID";
        public const string FIELD_CONTRACT_STATE_NAME = "NAME";
        // Investments
        public const string FIELD_COST_PAYMENT_SCHEMA_ID = "PAYMENT_SCHEMA_ID";
        public const string FIELD_COST_PAYMENT_TYPE_ID = "PAYMENT_KIND";
        public const string FIELD_COST_PREV_AVRVOLUME = "PREV_AVRVOLUME";
        public const string FIELD_COST_MARKET_TRAND = "MARKET_TRAND";
        public const string FIELD_COST_TRANDED_AVRVOLUME = "TRANDED_AVRVOLUME";
        public const string FIELD_COST_PLANNED_VOLUMEUPLIFT = "PLANNED_VOLUMEUPLIFT";
        public const string FIELD_COST_UPLIFTED_AVRVOLUME = "UPLIFTED_AVRVOLUME";
        public const string FIELD_COST_PLANNED_OUTLETSQANTITY = "PLANNED_OUTLETSQUANTITY";
        public const string FIELD_COST_EXPENSES_PEROUTLET = "EXPENSES_PEROUTLET";
        public const string FIELD_COST_PLANNED_AVRMACOPERDAL = "PLANNED_AVRMACOPERDAL";
        public const string FIELD_COST_HAS_ADVERT = "HAS_ADVERTISMENT";
        // Advertisment
        public const string FIELD_ADV_IS_ADVERT = "ADV_ADVERTISMENT_DESCR";
        public const string FIELD_COMPANY_PARTNER = "ADV_SPONSORCOMPANY";
        // Conditions
        public const string FIELD_CONDITION_ALLOW_ADD_SKU = "ALLOW_ADD_SKU";
        //RecalculateVPlan
        public const string SP_RECALCULATE_VPLAN = "spDW_CM_Recalculate_VPlan";
        public const string SP_RECALCULATE_VPLAN_PARAM1 = "Contract_ID";
    }


    /// <summary>
    /// 
    /// </summary>
    public enum ContractDetailsTabType
    {
        Unknown = -1,
        /// <summary>
        /// 
        /// </summary>
        Common = 0,
        /// <summary>
        /// 
        /// </summary>
        Advert = 1,
        /// 
        /// </summary>
        Investments = 2,
        /// <summary>
        /// 
        /// </summary>
        Conditions = 3,
        /// <summary>
        /// 
        /// </summary>
        Addresses = 4,
        /// <summary>
        /// 
        /// </summary>
        ControlGroup = 5
    }


    /// <summary>
    /// 
    /// </summary>
    public enum BudgetIndicator
    {
        Unknown = 0,
        TotalInvestments = 1,
        TargetV = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ColumnDynamicType
    {
        /// <summary>
        /// 
        /// </summary>
        ReadOnly = 0,
        /// <summary>
        /// 
        /// </summary>
        Bit = 1,
        /// <summary>
        /// 
        /// </summary>
        Int = 2,
        /// <summary>
        /// 
        /// </summary>
        Decimal = 3,
        /// <summary>
        /// 
        /// </summary>
        String = 4
    }

    /// <summary>
    /// 
    /// </summary>
    public enum UserLevelType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        M2 = 2,
        /// <summary>
        /// 
        /// </summary>
        M3 = 3,
        /// <summary>
        /// 
        /// </summary>
        M4 = 4,
        /// <summary>
        /// 
        /// </summary>
        M5 = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public enum AccessLevelType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        ReadOnly = 1,
        /// <summary>
        /// 
        /// </summary>
        AllowEdit = 2,
        /// <summary>
        /// 
        /// </summary>
        AllowApprove = 3,
        /// <summary>
        /// 
        /// </summary>
        AllowApproveNotBudgetOnly = 4,
        /// <summary>
        /// 
        /// </summary>
        AllowSign = 5,
        /// <summary>
        /// 
        /// </summary>
        AllowDecline = 6,
        /// <summary>
        /// 
        /// </summary>
        AllowTerminate = 7,
        /// <summary>
        /// 
        /// </summary>
        AllowStorcheck = 8,
        /// <summary>
        /// 
        /// </summary>
        AllowSignedContractAddressesEdit = 9,   // only for signed contracts
    }


    /// <summary>
    /// Defines payment type.
    /// </summary>
    public enum PaymentType
    {
        Prepayment = 2, //Предоплата
        Postpayment = 3, //Постоплата
        Mix = 4 //Микс

    }

    /// <summary>
    /// Defines payment schema.
    /// </summary>
    public enum PaymentSchema
    {
        Monthly = 0, //Помесячно
        Quarterly = 1 //Поквартально
    }

}

