﻿using System;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace SoftServe.Reports.ContractMgmt
{
    /// <summary>
    /// 
    /// </summary>
    public static class ExceptionHandler
    {
        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void HandleException(Exception ex)
        {
            string message = string.Empty;
            if (ex != null)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    message = ex.Message;
                }
                else if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    message = ex.InnerException.Message;
                }
            }
            if (!string.IsNullOrEmpty(message))
            {
                XtraMessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
