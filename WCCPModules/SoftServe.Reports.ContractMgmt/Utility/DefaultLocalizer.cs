﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.ContractMgmt.Utility
{
    /// <summary>
    /// Class that defines default localizer for grid and that is used in order to customize grid displaying messages.
    /// </summary>
    public class DefaultLocalizer : DevExpress.XtraGrid.Localization.GridLocalizer
    {
        public override string GetLocalizedString(DevExpress.XtraGrid.Localization.GridStringId id)
        {
            if (id == DevExpress.XtraGrid.Localization.GridStringId.ColumnViewExceptionMessage)
                return String.Empty;
            DevExpress.XtraGrid.Localization.GridStringId current = id;
            return base.GetLocalizedString(id);
        }
    }
}
