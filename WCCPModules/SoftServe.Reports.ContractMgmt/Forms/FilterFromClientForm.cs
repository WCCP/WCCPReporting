﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraLayout.Utils;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl.CommonControls.AddressesFilters;

namespace SoftServe.Reports.ContractMgmt.Forms
{
  /// <summary>
  /// 
  /// </summary>
  public partial class FilterFromClientForm : FilterBaseForm
  {
    private int clientId;
    private object chanel;
    private string promoOutlets;
    private string controlOutlets;

    /// <summary>
    /// Initializes a new instance of the <see cref="FilterRegionForm"/> class.
    /// </summary>
    public FilterFromClientForm(int clientId, object chanel, string promoOutlets, string controlOutlets)
    {
      InitializeComponent();

      this.clientId = clientId;
      this.chanel = chanel;
      this.promoOutlets = promoOutlets;
      this.controlOutlets = controlOutlets;

      LoadData();
    }

    #region Properties
    /// <summary>
    /// Gets the poc id list.
    /// </summary>
    /// <value>The poc id list.</value>
    public override string POCList
    {
      get
      {
        return olList.GetSelectedOls();
      }
    }

    /// <summary>
    /// Gets the selected items with additional information.
    /// </summary>
    /// <value>
    /// AddressesInformation object which represents selected items with additional information.
    /// </value>
    public override AddressesInformation SelectedItems
    {
      get
      {
        return new AddressesInformation()
        {
          AddressesList = new List<string>(POCList.Split(new char[] { ',' }))
        };
      }
    }
    #endregion

    /// <summary>
    /// Loads the data.
    /// </summary>
    private void LoadData()
    {
        olList.SetData(DataProvider.GetClientOutlets(clientId, chanel, promoOutlets, controlOutlets));
    }

    /// <summary>
    /// Updates the buttons.
    /// </summary>
    private void UpdateButtons()
    {
        btnOk.Enabled = POCList != null && POCList.Length > 0;
    }

    private void olList_POCListChanged(object sender, EventArgs e)
    {
        UpdateButtons();
    }
  }
}
