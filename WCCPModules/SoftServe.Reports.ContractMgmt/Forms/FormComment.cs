using System;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.ContractMgmt.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FormComment : XtraForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormComment"/> class.
        /// </summary>
        public FormComment()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the comment.
        /// </summary>
        /// <value>The comment.</value>
        public string Comment
        {
            get
            {
                return memoComment.Text;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnOK control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (ValidateChildren())
            {
                dxErrorProvider.ClearErrors();
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
        }

        /// <summary>
        /// Handles the Load event of the FormComment control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FormComment_Load(object sender, EventArgs e)
        {
            memoComment.Focus();
        }

        /// <summary>
        /// Handles the Click event of the memoComment control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void memoComment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(memoComment.Text))
            {
                e.Cancel = true;
                dxErrorProvider.SetError(memoComment, Resource.FieldEmptyError);
            }
        }
    }
}