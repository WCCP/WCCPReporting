using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.Enums;

namespace SoftServe.Reports.ContractMgmt.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FormClientList : XtraForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormClientList"/> class.
        /// </summary>
        public FormClientList()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the selected client id.
        /// </summary>
        /// <value>The selected client id.</value>
        public int SelectedClientId
        {
            get
            {
                return controlClientList.SelectedClientId;
            }
        }

        /// <summary>
        /// Gets the name of the selected client.
        /// </summary>
        /// <value>The name of the selected client.</value>
        public string SelectedClientName
        {
            get
            {
                return controlClientList.SelectedClientName;
            }
        }

        /// <summary>
        /// Gets the name of the selected client.
        /// </summary>
        /// <value>The name of the selected client.</value>
        public ChannelType SelectedChannelType
        {
            get
            {
                return controlClientList.SelectedChannelType;
            }
        }


        /// <summary>
        /// Handles the RowDoubleClicked event of the controlClientList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void controlClientList_RowDoubleClicked(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the controlClientList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void controlClientList_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            btnOk.Enabled = SelectedClientId > 0;
        }

        /// <summary>
        /// Method that handles shown event of the form, and loads appropriate data to the datasource. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormClientList_Shown(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                controlClientList.DataSource = DataProvider.ClientList;
            }
        }
    }
}