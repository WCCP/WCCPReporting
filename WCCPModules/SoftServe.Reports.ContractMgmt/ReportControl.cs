﻿using System;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraTab;
using SoftServe.Reports.ContractMgmt;
using System.Windows.Forms;
using SoftServe.Reports.ContractMgmt.Utility;
using Logica.Reports.BaseReportControl.Helpers;
using SoftServe.Reports.ContractMgmt.UserControls;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        public static BaseReportUserControl Bruc { get; set; }
        private ContractListTab tabContractsList;

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            Bruc = this;

            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.DomainUnload += new EventHandler(CurrentDomain_DomainUnload);
            Localizer.Active = new ControlsLocalizer();

            SetExportType(ExportToType.Rtf, false);
            SetExportButtonEnabled(false);

            InitializeComponent();

            tabContractsList = new ContractListTab();

            TabControl.ClosePageButtonShowMode = ClosePageButtonShowMode.InAllTabPageHeaders;
            TabControl.SelectedPageChanged += new TabPageChangedEventHandler(TabControl_SelectedPageChanged);

            UpdateAllSheets();
        }
        
        /// <summary>
        /// Sets export button enabled/disabled
        /// </summary>
        /// <param name="isEnabled">determines whether export button is to be made enabled</param>
        public void SetExportButtonEnabled(bool isEnabled)
        {
            btnExportTo.Enabled = isEnabled;
        }

        /// <summary>
        /// Handles the DomainUnload event of the CurrentDomain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void CurrentDomain_DomainUnload(object sender, EventArgs e)
        {
            //MessageBox.Show("You are about to close the report.");
        }

        /// <summary>
        /// Handles the ThreadException event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Threading.ThreadExceptionEventArgs"/> instance containing the event data.</param>
        private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            ExceptionHandler.HandleException(e.Exception);
        }

        /// <summary>
        /// Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            SheetParamCollection parameterCollection = new SheetParamCollection { TabId = tabContractsList.TabId, TableDataType = TableType.Fact };
            tabContractsList.UpdateSheet(parameterCollection);
        }

        /// <summary>
        /// Handles TabControl SelectedPageChanged event: enebles/disables export button depending on the page selected
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event args</param>
        private void TabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            if (e.Page != null && e.Page is ContractListTab)// contracts list
            {
                SetExportButtonEnabled(false);
            }
            else
                if (e.Page != null && e.Page is ContractDetailsTab) // contract details
                {
                    ContractDetailsTab tab = e.Page as ContractDetailsTab;

                    if (tab != null)
                    {
                        ContractDetailsControl cont = tab.UserControl as ContractDetailsControl;

                        if (cont != null && cont.CurrentBaseControl != null && (cont.CurrentBaseControl is ContractDetailsAddresses 
                            || cont.CurrentBaseControl is ContractDetailsControlGroup
                            || cont.CurrentBaseControl is ContractDetailsConditions))
                        {
                            SetExportButtonEnabled(true);
                        }
                        else
                        {
                            SetExportButtonEnabled(false);
                        }
                    }
                }
        }
    }
}
