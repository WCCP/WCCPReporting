﻿using System;
using System.Collections.Generic;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using SoftServe.Reports.ContractMgmt;
using System.Data;
using SoftServe.Reports.ContractMgmt.UserControls;
using WccpReporting;

namespace SoftServe.Reports.ContractMgmt
{
    /// <summary>
    /// 
    /// </summary>
    public class ContractListTab : CommonBaseTab, IPrint
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContractListTab"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public ContractListTab()
        {
            if (!this.DesignMode)
            {
                Init(new ContractListControl(this), Resource.ContractList);

                ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;

                WccpUIControl.Bruc.ExportClick += new BaseReportUserControl.ExportMenuClickHandler(parent_ExportClick);
            }
        }

        /// <summary>
        /// Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public ContractListControl Content
        {
            get
            {
                return base.UserControl as ContractListControl;
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData(bool reloadAllTabs)
        {
            Content.ContractList = DataProvider.ContractList;
        }

        /// <summary>
        /// Gets the contract list tab.
        /// </summary>
        /// <value>The contract list tab.</value>
        public static ContractListTab ContractList
        {
            get
            {
                foreach (CommonBaseTab tab in WccpUIControl.Bruc.TabControl.TabPages)
                {
                    if (tab is ContractListTab)
                    {
                        return tab as ContractListTab;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Checks the contract by client exists.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="contract">The contract.</param>
        /// <returns></returns>
        public bool CheckContractByClientExists(int clientId, string contract, int currentContractId)
        {
            DataTable dataTable = Content.ContractList;
            if (!string.IsNullOrEmpty(contract) && dataTable != null && dataTable.Rows.Count > 0)
            {
                DataView dataView = new DataView(dataTable);
                dataView.RowFilter = string.Format("{0} = {1} AND {2} = '{3}' AND {4} <> {5}", 
                    Constants.FIELD_COMMON_CLIENT_ID, clientId, 
                    Constants.FIELD_COMMON_CONTRACT_NUM, contract,
                    Constants.FIELD_COMMON_CONTRACT_ID, currentContractId);
                return dataView.Count > 0;
            }
            return false;
        }

        /// <summary>
        /// Prepare container of elements for exporting
        /// </summary>
        /// <returns></returns>
        public CompositeLink PrepareCompositeLink()
        {
            CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
            compositeLink.Links.Add(new PrintableComponentLink() { Component = Content.GridControl });
            compositeLink.CreateDocument();
            return compositeLink;
        }

        /// <summary>
        /// Parent_s the export click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        /// <param name="type">The type.</param>
        /// <param name="fName">Name of the f.</param>
        private void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
        {
            if (this == selectedPage)
            {
                switch (type)
                {
                    case ExportToType.Html:
                        PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
                        break;
                    case ExportToType.Mht:
                        PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
                        break;
                    case ExportToType.Pdf:
                        PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions() { Compressed = true });
                        break;
                    case ExportToType.Rtf:
                        PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
                        break;
                    case ExportToType.Txt:
                        PrepareCompositeLink().PrintingSystem.ExportToText(fName);
                        break;
                    case ExportToType.Xls:
                        PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = Text });
                        break;
                    case ExportToType.Xlsx:
                        PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
                        break;
                    case ExportToType.Bmp:
                        PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
                        break;
                    case ExportToType.Csv:
                        PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
                        break;
                }
            }
        }
    }
}
