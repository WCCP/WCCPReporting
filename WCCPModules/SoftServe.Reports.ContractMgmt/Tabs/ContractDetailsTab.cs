﻿using System;
using System.Collections.Generic;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using SoftServe.Reports.ContractMgmt;
using System.Data;
using DevExpress.XtraTab.ViewInfo;
using SoftServe.Reports.ContractMgmt.UserControls;
using WccpReporting;

namespace SoftServe.Reports.ContractMgmt
{
    /// <summary>
    /// 
    /// </summary>
    public class ContractDetailsTab : CommonBaseTab, IPrint
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsTab"/> class.
        /// </summary>
        /// <param name="bruc">The bruc.</param>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        /// <param name="tabName">Name of the tab.</param>
        public ContractDetailsTab(string tabName, CommonBaseExternalData data)
        {
            if (!this.DesignMode)
            {
                ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;

                Init(new ContractDetailsControl(this, data), tabName);

                WccpUIControl.Bruc.ExportClick += new BaseReportUserControl.ExportMenuClickHandler(parent_ExportClick);
                WccpUIControl.Bruc.TabControl.CloseButtonClick += new EventHandler(TabControl_CloseButtonClick);
            }
        }

        /// <summary>
        /// Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public ContractDetailsControl Content
        {
            get
            {
                return base.UserControl as ContractDetailsControl;
            }
        }

        /// <summary>
        /// Parent_s the tool button visibility check.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void parent_ToolButtonVisibilityCheck(object sender, XtraTabPage selectedPage)
        {
            if (this == selectedPage)
            {
                DevExpress.XtraBars.BarItem barItem = sender as DevExpress.XtraBars.BarItem;
                if (barItem != null)
                {
                    barItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                }
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData(bool reloadAllTabs)
        {
            if (Content != null)
                Content.LoadData(reloadAllTabs);
        }

        /// <summary>
        /// Prepare container of elements for exporting
        /// </summary>
        /// <returns></returns>
        public CompositeLink PrepareCompositeLink()
        {
            CompositeLink compositeLink = new CompositeLink(new PrintingSystem());

            if (Content.CurrentBaseControl is ContractDetailsAddresses)
            {
                ContractDetailsAddresses addresses = Content.CurrentBaseControl as ContractDetailsAddresses;

                addresses.PrepareToExport();
                compositeLink.Links.Add(new PrintableComponentLink() { Component = addresses.GridControl });
                compositeLink.CreateDocument();
                addresses.RestoreAfterExport();
            }
            else
                if (Content.CurrentBaseControl is ContractDetailsControlGroup)
                {
                    ContractDetailsControlGroup control = Content.CurrentBaseControl as ContractDetailsControlGroup;

                    control.PrepareToExport();
                    compositeLink.Links.Add(new PrintableComponentLink() { Component = control.GridControl });
                    compositeLink.CreateDocument();
                    control.RestoreAfterExport();
                }
                else if (Content.CurrentBaseControl is ContractDetailsConditions)
                {
                    ContractDetailsConditions control = Content.CurrentBaseControl as ContractDetailsConditions;

                    control.PrepareToExport();
                    compositeLink.Links.Add(new PrintableComponentLink() { Component = control.TreeList });
                    compositeLink.CreateDocument();
                    control.RestoreAfterExport();
                }

            return compositeLink;
        }

        /// <summary>
        /// Parent_s the export click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        /// <param name="type">The type.</param>
        /// <param name="fName">Name of the f.</param>
        private void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
        {
            if (this == selectedPage)
            {
                switch (type)
                {
                    case ExportToType.Html:
                        PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
                        break;
                    case ExportToType.Mht:
                        PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
                        break;
                    case ExportToType.Pdf:
                        PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions() { Compressed = true });
                        break;
                    case ExportToType.Rtf:
                        PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
                        break;
                    case ExportToType.Txt:
                        PrepareCompositeLink().PrintingSystem.ExportToText(fName);
                        break;
                    case ExportToType.Xls:
                        PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = Text });
                        break;
                    case ExportToType.Xlsx:
                        PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
                        break;
                    case ExportToType.Bmp:
                        PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
                        break;
                    case ExportToType.Csv:
                        PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
                        break;
                }
            }
        }

        /// <summary>
        /// Handles the CloseButtonClick event of the TabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabControl_CloseButtonClick(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            if (arg != null && arg.Page == this)
            {
                WccpUIControl.Bruc.TabControl.TabPages.Remove(this);
            }
        }
    }
}
