﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.Model;
using SoftServe.Reports.ContractMgmt.UserControls;
using WccpReporting;

namespace SoftServe.Reports.ContractMgmt {
    public class CommonBaseTab : XtraTabPage {
        private readonly Guid _tabId = Guid.NewGuid();

        #region Properties

        /// <summary>
        /// Gets the type of the tab.
        /// </summary>
        /// <value>The type of the tab.</value>
        public ContractDetailsTabType TabType {
            get {
                if (TabControl.TabPages.Contains(this))
                    return (ContractDetailsTabType) TabControl.TabPages.IndexOf(this);
                return ContractDetailsTabType.Unknown;
            }
        }

        /// <summary>
        /// Gets or sets the sheet settings.
        /// </summary>
        /// <value>The sheet settings.</value>
        protected SheetParamCollection SheetSettings { get; set; }

        /// <summary>
        /// Gets or sets the user control.
        /// </summary>
        /// <value>The user control.</value>
        public CommonBaseControl UserControl { get; set; }

        /// <summary>
        /// Gets the tab id.
        /// </summary>
        /// <value>The tab id.</value>
        public Guid TabId {
            get { return _tabId; }
        }

        #endregion

        /// <summary>
        /// Inits the specified user control.
        /// </summary>
        /// <param name="userControl">The user control.</param>
        /// <param name="tabName">Name of the tab.</param>
        public void Init(CommonBaseControl userControl, string tabName) {
            if (DesignMode)
                return;
            Text = tabName;
            LoadControl(userControl);

            if (WccpUIControl.Bruc == null)
                return;
            WccpUIControl.Bruc.RefreshClick += new BaseReportUserControl.MenuClickHandler(parent_RefreshClick);
            WccpUIControl.Bruc.MenuButtonsRendering += (x, args) => {
                if (args.SelectedPage != this)
                    return;
                args.ShowParametersBtn = false;
                args.ShowExportAllBtn = false;
                args.ShowExportBtn = true;
                args.ShowPrintAllBtn = false;
                args.ShowPrintBtn = false;
            };
        }

        /// <summary>
        /// Loads the control.
        /// </summary>
        /// <param name="userControl">The user control.</param>
        private void LoadControl(CommonBaseControl userControl) {
            if (userControl == null)
                return;
            UserControl = userControl;
            userControl.Dock = DockStyle.Fill;
            userControl.ParentTab = this;
            Controls.Add(userControl);
        }

        /// <summary>
        /// Activates the tab.
        /// </summary>
        public void ActivateTab() {
            TabControl.SelectedTabPage = this;
        }

        /// <summary>
        /// Updates the sheet.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public void UpdateSheet(SheetParamCollection settings) {
            if (settings != null) {
                if (!WccpUIControl.Bruc.TabControl.TabPages.Contains(this)) {
                    WccpUIControl.Bruc.TabControl.TabPages.Add(this);
                }
                UpdateData(settings, false, true);
                WccpUIControl.Bruc.TabControl.SelectedTabPage = this;
            }
            else if (WccpUIControl.Bruc.TabControl.TabPages.Contains(this)) {
                WccpUIControl.Bruc.TabControl.TabPages.Remove(this);
            }
        }

        /// <summary>
        /// Updates the data.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="isForce">if set to <c>true</c> [is force].</param>
        /// <param name="reloadAllTabs">If true reloads data for all tabs</param>
        public void UpdateData(SheetParamCollection parameters, bool isForce, bool reloadAllTabs) {
            if (parameters == null || (!isForce && CompareSettings(parameters)))
                return;

            SheetSettings = parameters;
            WccpUIControl.Bruc.ConstructTemporaryData(parameters);
            LoadData(reloadAllTabs);
            WccpUIControl.Bruc.DestructTemporaryData();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public virtual void LoadData(bool reloadAllTabs) {
        }

        /// <summary>
        /// Loads the details tab.
        /// </summary>
        public void LoadDetailsTab(bool checkUniqueName, string tabName, CommonBaseExternalData data) {
            CommonBaseTab lExistingTab = null;
            if (checkUniqueName) {
                lExistingTab = GetTabByName(tabName);
            }

            if (lExistingTab == null) {
                if (WccpUIControl.Bruc.TabControl.TabPages.Count > Constants.TAB_MAX_COUNT) {
                    XtraMessageBox.Show(Resource.TabMaxCount, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                ContractDetailsTab lTabContractList = new ContractDetailsTab(tabName, data);
                SheetParamCollection lParamCollection = new SheetParamCollection {TabId = lTabContractList.TabId, TableDataType = TableType.Fact};
                lTabContractList.UpdateSheet(lParamCollection);
            }
            else
                WccpUIControl.Bruc.TabControl.SelectedTabPage = lExistingTab;
        }

        /// <summary>
        /// Closes the tab.
        /// </summary>
        public void CloseTab() {
            WccpUIControl.Bruc.TabControl.TabPages.Remove(this);
        }

        /// <summary>
        /// Updates the contract list tab.
        /// </summary>
        public void UpdateContractListTab() {
            if (ContractListTab.ContractList != null)
                ContractListTab.ContractList.UpdateData(SheetSettings, true, true);
        }

        /// <summary>
        /// Gets the name of the tab by.
        /// </summary>
        /// <param name="tabName">Name of the tab.</param>
        /// <returns></returns>
        private CommonBaseTab GetTabByName(string tabName) {
            foreach (CommonBaseTab lTab in WccpUIControl.Bruc.TabControl.TabPages)
                if (string.Compare(lTab.Text, tabName, false) == 0)
                    return lTab;

            return null;
        }

        /// <summary>
        /// Compares the settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        private bool CompareSettings(SheetParamCollection settings) {
            if (null == SheetSettings || null == settings || SheetSettings.Count != settings.Count)
                return false;

            foreach (SheetParam lParam in settings) {
                SheetParam lPar = lParam;
                if (!SheetSettings.Exists(p => p.SqlParamName.Equals(lPar.SqlParamName) && p.Value.Equals(lPar.Value)))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Parent_s the refresh click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void parent_RefreshClick(object sender, XtraTabPage selectedPage) {
            if (this == selectedPage)
                UpdateData(SheetSettings, true, false);
        }
    }
}