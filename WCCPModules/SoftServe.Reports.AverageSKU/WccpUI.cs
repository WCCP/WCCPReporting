﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using ModularWinApp.Core.Interfaces;
using SoftServe.Reports.AverageSKU;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpAverageSKU.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                _reportControl = new WccpUIControl();
                _reportCaption = reportCaption;

                DataAccessLayer.OpenConnection();
                DataAccessProvider.ReportCaption = reportCaption;

	            SettingsForm form = new SettingsForm();

                if (form.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.SettingsForm = form;
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }


        public void CloseUI()
        {
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}
