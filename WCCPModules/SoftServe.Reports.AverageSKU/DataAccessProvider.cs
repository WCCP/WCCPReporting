﻿using System;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.AverageSKU
{
    internal static partial class DataAccessProvider
    {
        // Кэшированные данные по всем имеющимся упаковкам. Персонал и продукты не кэшируются!
        private static DataTable packages; // Кэшированные данные по упаковкам.
        private static UserLevel userLevel = UserLevel.Unknown; // Уровень текущего пользователя (M5, M4, M3, M2) кэшируется.


        #region Settings Form
        internal static UserLevel GetUserLevel()
        {
            if (userLevel == UserLevel.Unknown)
            {
                object result = DataAccessLayer.ExecuteScalarStoredProcedure(SP_GET_USER_LEVEL);
                userLevel = (result == null) ? UserLevel.Unknown : (UserLevel)result;
            }

            return userLevel;
        }

        internal static DataTable GetPersonnel(int lowLevel, int channel, string userLogin)
        {
            var personnel = DataAccessLayer.ExecuteStoredProcedure(SP_GET_PERSONNEL,
                                                                   new[]
                                                                       {
                                                                           new SqlParameter(SP_GET_PERSONNEL_PARAM1, lowLevel),
                                                                           new SqlParameter(SP_GET_PERSONNEL_PARAM2, channel),
                                                                           new SqlParameter(SP_GET_PERSONNEL_PARAM3, (!String.IsNullOrEmpty(userLogin))
                                                                                                                     ? (object) userLogin
                                                                                                                     : DBNull.Value)
                                                                       });
            if (personnel != null && personnel.Tables.Count > 0)
                return personnel.Tables[0];
            return null;
        }


        internal static DataTable GetPackages()
        {
            if (packages == null)
            {
                var dataSet = DataAccessLayer.ExecuteStoredProcedure(SP_GET_PACKAGES);
                if (dataSet != null && dataSet.Tables.Count > 0)
                    packages = dataSet.Tables[0];
            }

            return packages;
        }


        internal static DataTable GetProducts(string productIds)
        {
            if (String.IsNullOrEmpty(productIds))
                throw new ArgumentException("String of concatenated productIds is required.'");

            var products = DataAccessLayer.ExecuteStoredProcedure(SP_GET_PRODUCTS,
                                                                  new[]
                                                                      {
                                                                          new SqlParameter(SP_GET_PRODUCTS_PARAM1, productIds)
                                                                      });
            if (products != null && products.Tables.Count > 0)
                return products.Tables[0];
            return null;
        }
        #endregion


        #region Main Report
        internal static DataTable GetReport(DateTime periodStart, DateTime periodEnd, UserLevel lowestUserLevel, string personnelIdList,
                                            bool useMinimalShipment, string productIdList)
        {
            var reportData = DataAccessLayer.ExecuteStoredProcedure(SP_GET_REPORT_DATA,
                                                                    new[]
                                                                        {
                                                                            new SqlParameter(SP_GET_REPORT_DATA_PARAM1, periodStart),
                                                                            new SqlParameter(SP_GET_REPORT_DATA_PARAM2, periodEnd),
                                                                            new SqlParameter(SP_GET_REPORT_DATA_PARAM3, lowestUserLevel),
                                                                            new SqlParameter(SP_GET_REPORT_DATA_PARAM4, personnelIdList),
                                                                            new SqlParameter(SP_GET_REPORT_DATA_PARAM5, useMinimalShipment),
                                                                            new SqlParameter(SP_GET_REPORT_DATA_PARAM6, productIdList)
                                                                        });
            if (reportData != null && reportData.Tables.Count > 0)
                return reportData.Tables[0];

            return null;
        }

        /*
        // Тестовые данные для отчета берутся из файла "C:\temp\data.csv".
        
        private static DataTable testData;
        internal static DataTable GetReport()
        {
            if (testData == null)
            {
                testData = CreateTestTable();
                string[] lines = File.ReadAllLines(@"C:\temp\data.csv");
                foreach (string line in lines)
                {
                    string[] v = line.Split(';');
                    testData.Rows.Add(v[0], v[1], v[2], v[3], v[4], v[5], v[6], (v[7] == String.Empty) ? null : v[7], v[8],
                                      Double.Parse(v[9]), String.Empty, "ФИО менеджера М1", 1);
                }

            }

            return testData;
        }


        private static DataTable CreateTestTable()
        {
            DataTable result = new DataTable();

            result.Columns.Add("CodeTT", typeof(string));
            result.Columns.Add("M4", typeof(string));
            result.Columns.Add("M3", typeof(string));
            result.Columns.Add("M2", typeof(string));
            result.Columns.Add("TypeTT", typeof(string));
            result.Columns.Add("Province", typeof(string));
            result.Columns.Add("Region", typeof(string));
            result.Columns.Add("Network", typeof(string));
            result.Columns.Add("TerritoryType", typeof(string));
            result.Columns.Add("AverageSKU", typeof(double));
            result.Columns.Add("ChannelM3", typeof(string)); // Currently is not provided in test data !!!!!
            result.Columns.Add("M1", typeof(string)); // Currently is not provided in test data !!!!!
            result.Columns.Add("NumberTT", typeof(int)); // Currently is not provided in test data !!!!!

            return result;
        }
        */
        #endregion

        public static DataTable GetUpdateData(DateTime now) {
            if (!DataAccessLayer.IsLDB)
                return null;
            DataSet lResult = DataAccessLayer.ExecuteStoredProcedure(SP_GET_UPDATE_TIME,
                new[] {
                    new SqlParameter(SP_GET_UPDATE_TIME_PRM_1, now)
                });
            return lResult.Tables.Count > 0 ? lResult.Tables[0] : null;
        }

        public static void UpdateData() {
            if (!DataAccessLayer.IsLDB)
                return;
            DataAccessLayer.ExecuteStoredProcedure(SP_RECALCULATE_DATA, new SqlParameter(SP_RECALCULATE_DATA_PRM_1, (byte)1));
        }
    }


    internal enum UserLevel
    {
        Unknown = 0,
        M1 = 1,
        M2 = 2,
        M3 = 3,
        M4 = 4,
        M5 = 5
    }


    [Flags]
    internal enum UserChannel
    {
        None = 0,
        OnTrade = 1,
        OffTrade = 2,
        KaTrade = 4,
    }

}