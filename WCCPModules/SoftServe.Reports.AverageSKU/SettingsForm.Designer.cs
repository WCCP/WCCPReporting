﻿namespace SoftServe.Reports.AverageSKU
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.generateReportButton = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.reportPeriodFrom = new DevExpress.XtraEditors.DateEdit();
            this.reportPeriodTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.userLevelRadioGroup = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.offTradeChannel = new DevExpress.XtraEditors.CheckEdit();
            this.onTradeChannel = new DevExpress.XtraEditors.CheckEdit();
            this.kaTradeChannel = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.personnelTreeList = new DevExpress.XtraTreeList.TreeList();
            this.personnelDataColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.productsTreeList = new DevExpress.XtraTreeList.TreeList();
            this.productDataColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.packagesTreeList = new DevExpress.XtraTreeList.TreeList();
            this.packageDataColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.useMinShipment = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.lblUpd = new DevExpress.XtraEditors.LabelControl();
            this.btn_upd = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadioGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offTradeChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onTradeChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaTradeChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personnelTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packagesTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.useMinShipment.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // generateReportButton
            // 
            this.generateReportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.generateReportButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.generateReportButton.Image = global::SoftServe.Reports.AverageSKU.Properties.Resources.Check_16;
            this.generateReportButton.Location = new System.Drawing.Point(540, 417);
            this.generateReportButton.Name = "generateReportButton";
            this.generateReportButton.Size = new System.Drawing.Size(146, 23);
            this.generateReportButton.TabIndex = 0;
            this.generateReportButton.Text = "&Сгенерировать отчет";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Image = global::SoftServe.Reports.AverageSKU.Properties.Resources.Close_16;
            this.simpleButton2.Location = new System.Drawing.Point(692, 417);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(86, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "&Отменить";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(156, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Период времени для выборки:";
            // 
            // reportPeriodFrom
            // 
            this.reportPeriodFrom.EditValue = null;
            this.reportPeriodFrom.Location = new System.Drawing.Point(23, 31);
            this.reportPeriodFrom.Name = "reportPeriodFrom";
            this.reportPeriodFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.reportPeriodFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reportPeriodFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.reportPeriodFrom.Size = new System.Drawing.Size(100, 20);
            this.reportPeriodFrom.TabIndex = 3;
            // 
            // reportPeriodTo
            // 
            this.reportPeriodTo.EditValue = null;
            this.reportPeriodTo.Location = new System.Drawing.Point(162, 31);
            this.reportPeriodTo.Name = "reportPeriodTo";
            this.reportPeriodTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.reportPeriodTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reportPeriodTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.reportPeriodTo.Size = new System.Drawing.Size(100, 20);
            this.reportPeriodTo.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 71);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(192, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "Нижний уровень персонала в отчете:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 34);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(5, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "с";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(144, 34);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(12, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "по";
            // 
            // userLevelRadioGroup
            // 
            this.userLevelRadioGroup.EditValue = 1D;
            this.userLevelRadioGroup.Location = new System.Drawing.Point(13, 91);
            this.userLevelRadioGroup.Name = "userLevelRadioGroup";
            this.userLevelRadioGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.userLevelRadioGroup.Properties.Appearance.Options.UseBackColor = true;
            this.userLevelRadioGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.userLevelRadioGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(4D, "M4"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3D, "M3"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2D, "M2"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1D, "M1")});
            this.userLevelRadioGroup.Size = new System.Drawing.Size(191, 23);
            this.userLevelRadioGroup.TabIndex = 8;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 132);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(91, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Канал персонала:";
            // 
            // offTradeChannel
            // 
            this.offTradeChannel.Location = new System.Drawing.Point(12, 152);
            this.offTradeChannel.Name = "offTradeChannel";
            this.offTradeChannel.Properties.Caption = "off trade";
            this.offTradeChannel.Size = new System.Drawing.Size(68, 19);
            this.offTradeChannel.TabIndex = 10;
            // 
            // onTradeChannel
            // 
            this.onTradeChannel.Location = new System.Drawing.Point(100, 152);
            this.onTradeChannel.Name = "onTradeChannel";
            this.onTradeChannel.Properties.Caption = "on trade";
            this.onTradeChannel.Size = new System.Drawing.Size(68, 19);
            this.onTradeChannel.TabIndex = 11;
            // 
            // kaTradeChannel
            // 
            this.kaTradeChannel.Location = new System.Drawing.Point(194, 152);
            this.kaTradeChannel.Name = "kaTradeChannel";
            this.kaTradeChannel.Properties.Caption = "ka trade";
            this.kaTradeChannel.Size = new System.Drawing.Size(68, 19);
            this.kaTradeChannel.TabIndex = 12;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 187);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(52, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Персонал:";
            // 
            // personnelTreeList
            // 
            this.personnelTreeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.personnelTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.personnelDataColumn});
            this.personnelTreeList.Location = new System.Drawing.Point(12, 207);
            this.personnelTreeList.Name = "personnelTreeList";
            this.personnelTreeList.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.personnelTreeList.OptionsView.ShowCheckBoxes = true;
            this.personnelTreeList.OptionsView.ShowColumns = false;
            this.personnelTreeList.OptionsView.ShowIndicator = false;
            this.personnelTreeList.ParentFieldName = "PARENT_ID";
            this.personnelTreeList.RootValue = null;
            this.personnelTreeList.Size = new System.Drawing.Size(250, 200);
            this.personnelTreeList.TabIndex = 14;
            // 
            // personnelDataColumn
            // 
            this.personnelDataColumn.Caption = "treeListColumn1";
            this.personnelDataColumn.FieldName = "DATA";
            this.personnelDataColumn.Name = "personnelDataColumn";
            this.personnelDataColumn.OptionsColumn.AllowEdit = false;
            this.personnelDataColumn.OptionsColumn.AllowMove = false;
            this.personnelDataColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.personnelDataColumn.OptionsColumn.ReadOnly = true;
            this.personnelDataColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.personnelDataColumn.Visible = true;
            this.personnelDataColumn.VisibleIndex = 0;
            this.personnelDataColumn.Width = 91;
            // 
            // productsTreeList
            // 
            this.productsTreeList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.productsTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.productDataColumn});
            this.productsTreeList.Location = new System.Drawing.Point(490, 71);
            this.productsTreeList.Name = "productsTreeList";
            this.productsTreeList.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.productsTreeList.OptionsView.ShowCheckBoxes = true;
            this.productsTreeList.OptionsView.ShowColumns = false;
            this.productsTreeList.OptionsView.ShowIndicator = false;
            this.productsTreeList.ParentFieldName = "PARENT_ID";
            this.productsTreeList.Size = new System.Drawing.Size(288, 336);
            this.productsTreeList.TabIndex = 15;
            // 
            // productDataColumn
            // 
            this.productDataColumn.Caption = "treeListColumn1";
            this.productDataColumn.FieldName = "DATA";
            this.productDataColumn.Name = "productDataColumn";
            this.productDataColumn.OptionsColumn.AllowEdit = false;
            this.productDataColumn.OptionsColumn.AllowMove = false;
            this.productDataColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.productDataColumn.OptionsColumn.ReadOnly = true;
            this.productDataColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.productDataColumn.Visible = true;
            this.productDataColumn.VisibleIndex = 0;
            this.productDataColumn.Width = 91;
            // 
            // packagesTreeList
            // 
            this.packagesTreeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.packagesTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.packageDataColumn});
            this.packagesTreeList.Location = new System.Drawing.Point(283, 71);
            this.packagesTreeList.Name = "packagesTreeList";
            this.packagesTreeList.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.packagesTreeList.OptionsView.ShowCheckBoxes = true;
            this.packagesTreeList.OptionsView.ShowColumns = false;
            this.packagesTreeList.OptionsView.ShowIndicator = false;
            this.packagesTreeList.ParentFieldName = "PARENT_ID";
            this.packagesTreeList.RootValue = null;
            this.packagesTreeList.Size = new System.Drawing.Size(186, 336);
            this.packagesTreeList.TabIndex = 16;
            // 
            // packageDataColumn
            // 
            this.packageDataColumn.Caption = "treeListColumn1";
            this.packageDataColumn.FieldName = "DATA";
            this.packageDataColumn.Name = "packageDataColumn";
            this.packageDataColumn.OptionsColumn.AllowEdit = false;
            this.packageDataColumn.OptionsColumn.AllowMove = false;
            this.packageDataColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.packageDataColumn.OptionsColumn.ReadOnly = true;
            this.packageDataColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.packageDataColumn.Visible = true;
            this.packageDataColumn.VisibleIndex = 0;
            this.packageDataColumn.Width = 91;
            // 
            // useMinShipment
            // 
            this.useMinShipment.Location = new System.Drawing.Point(281, 12);
            this.useMinShipment.Name = "useMinShipment";
            this.useMinShipment.Properties.Caption = "Использовать настроенные в БД минимальные отгрузки в 1 ТТ по комби продукту";
            this.useMinShipment.Size = new System.Drawing.Size(497, 19);
            this.useMinShipment.TabIndex = 17;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(283, 52);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(53, 13);
            this.labelControl7.TabIndex = 18;
            this.labelControl7.Text = "Упаковка:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(490, 52);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 13);
            this.labelControl8.TabIndex = 19;
            this.labelControl8.Text = "Продукция:";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Appearance.Options.UseFont = true;
            this.lblUpd2.Location = new System.Drawing.Point(77, 422);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(0, 13);
            this.lblUpd2.TabIndex = 22;
            // 
            // lblUpd
            // 
            this.lblUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUpd.Location = new System.Drawing.Point(12, 422);
            this.lblUpd.Name = "lblUpd";
            this.lblUpd.Size = new System.Drawing.Size(59, 13);
            this.lblUpd.TabIndex = 21;
            this.lblUpd.Text = "Данные на:";
            // 
            // btn_upd
            // 
            this.btn_upd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_upd.ImageIndex = 1;
            this.btn_upd.Location = new System.Drawing.Point(229, 417);
            this.btn_upd.Name = "btn_upd";
            this.btn_upd.Size = new System.Drawing.Size(86, 23);
            this.btn_upd.TabIndex = 20;
            this.btn_upd.Text = " Обновить ";
            this.btn_upd.Click += new System.EventHandler(this.btn_upd_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 452);
            this.Controls.Add(this.lblUpd2);
            this.Controls.Add(this.lblUpd);
            this.Controls.Add(this.btn_upd);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.useMinShipment);
            this.Controls.Add(this.packagesTreeList);
            this.Controls.Add(this.productsTreeList);
            this.Controls.Add(this.personnelTreeList);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.kaTradeChannel);
            this.Controls.Add(this.onTradeChannel);
            this.Controls.Add(this.offTradeChannel);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.userLevelRadioGroup);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.reportPeriodTo);
            this.Controls.Add(this.reportPeriodFrom);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.generateReportButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 480);
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры к отчету \"Среднее СКЮ\"";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SettingsForm_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportPeriodTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadioGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offTradeChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onTradeChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaTradeChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personnelTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packagesTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.useMinShipment.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.SimpleButton generateReportButton;
		private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit reportPeriodFrom;
        private DevExpress.XtraEditors.DateEdit reportPeriodTo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.RadioGroup userLevelRadioGroup;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit offTradeChannel;
        private DevExpress.XtraEditors.CheckEdit onTradeChannel;
        private DevExpress.XtraEditors.CheckEdit kaTradeChannel;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraTreeList.TreeList personnelTreeList;
        private DevExpress.XtraTreeList.TreeList productsTreeList;
        private DevExpress.XtraTreeList.TreeList packagesTreeList;
        private DevExpress.XtraEditors.CheckEdit useMinShipment;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraTreeList.Columns.TreeListColumn personnelDataColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn packageDataColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn productDataColumn;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraEditors.LabelControl lblUpd;
        private DevExpress.XtraEditors.SimpleButton btn_upd;
	}
}