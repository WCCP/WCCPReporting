﻿using System;

namespace SoftServe.Reports.AverageSKU
{
    internal static partial class DataAccessProvider
    {

        public static string ReportCaption { get; set; }

        // Имя хранимой процедуры для определения уровня текущего пользователя(M5, M4, M3, M2, M1). Процедура не имеет параметров.
        private static readonly string SP_GET_USER_LEVEL = "spDW_GetUserLevel";

        // Имя хранимой процедуры для выборки персонала, а также имена ее параметров.
        private static readonly string SP_GET_PERSONNEL = "spDW_AvrSKU_GET_Personal";
        private static readonly string SP_GET_PERSONNEL_PARAM1 = "LowLevel";
        private static readonly string SP_GET_PERSONNEL_PARAM2 = "Channel";
        private static readonly string SP_GET_PERSONNEL_PARAM3 = "UserLogin";

        // Имя хранимой процедуры для выборки имеющихся упаковок. Процедура не имеет параметров.
        private static readonly string SP_GET_PACKAGES = "spDW_AvrSKU_GET_Packages";

        // Имя хранимой процедуры для выборки персонала, а также имена ее параметров.
        private static readonly string SP_GET_PRODUCTS = "spDW_AvrSKU_GET_Products";
        private static readonly string SP_GET_PRODUCTS_PARAM1 = "ProductIdList";

        // Имя хранимой процедуры для получения данных основного отчета, а также имена ее параметров.
        private static readonly string SP_GET_REPORT_DATA = "spDW_AverageSKU";
        private static readonly string SP_GET_REPORT_DATA_PARAM1 = "DateFrom";
        private static readonly string SP_GET_REPORT_DATA_PARAM2 = "DateTo";
        private static readonly string SP_GET_REPORT_DATA_PARAM3 = "LowestLevel";
        private static readonly string SP_GET_REPORT_DATA_PARAM4 = "PersonnelIdList";
        private static readonly string SP_GET_REPORT_DATA_PARAM5 = "UseMinShipment";
        private static readonly string SP_GET_REPORT_DATA_PARAM6 = "ProductIdList";


        public const string SP_GET_UPDATE_TIME = "spDW_M_OffTrade_Get_UpdateTime";
        public const string SP_GET_UPDATE_TIME_PRM_1 = "Date";
        public const string SP_GET_UPDATE_TIME_FLD_PREF_VALUE = "PrefValue";

        public const string SP_RECALCULATE_DATA = "sp_DW_Visits_With_Invoices_Update";
        public const string SP_RECALCULATE_DATA_PRM_1 = "isFullUpdate";
    }
}
