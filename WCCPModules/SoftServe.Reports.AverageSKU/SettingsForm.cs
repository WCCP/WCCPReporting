﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.AverageSKU
{
	public partial class SettingsForm : XtraForm
	{
        private DataTable personnel;
        private DataTable packages;
        private DataTable products;
	    private readonly Dictionary<Control, bool> validatedControls = new Dictionary<Control, bool>();

	    private bool isInitializedToDefaults = false;
	    private static readonly Color HIGLIGHT_COLOR;

	    public SettingsForm()
		{
			InitializeComponent();
		}

	    static SettingsForm()
	    {
	        HIGLIGHT_COLOR = Color.Red;
	    }

	    #region ControlsEventHandlers
        private void SettingsForm_Load(object sender, EventArgs e)
        {
            if (!isInitializedToDefaults)
            {
                SetDefaults();
                isInitializedToDefaults = true;
            }
            else
            {
                Validate(this, EventArgs.Empty);
            }
            GetUpdData();
        }


        private void LevelOrChannelChanged(object sender, EventArgs e)
        {
            SetPersonnel((int)(double)userLevelRadioGroup.EditValue, Channel);
            Validate(this, EventArgs.Empty);
        }


        private void packagesTreeList_AfterCheckNode(object sender, NodeEventArgs e)
        {
            // После каждого клика мышкой состояние узла циклически меняется с 'Checked' на 'Unchecked' на 'Indeterminate', поэтому надо
            // принудительно менять состояние 'Indeterminate' на 'Checked', т.к. состояние 'Indeterminate' означает, что был клик на 
            // 'Unchecked' узле.
            if (e.Node.CheckState == CheckState.Indeterminate)
                e.Node.Checked = true;

            PropagateCheckStateDown(sender, e);
            PropagateCheckStateUp(e.Node);
            SetProducts(ProductInPackageIdList);
            Validate(sender, EventArgs.Empty);
        }


        private void personnelTreeList_productsTreeList_AfterCheckNode(object sender, NodeEventArgs e)
        {
            if (e.Node.CheckState == CheckState.Indeterminate)
                e.Node.Checked = true;

            PropagateCheckStateDown(sender, e);
            PropagateCheckStateUp(e.Node);
            Validate(sender, EventArgs.Empty);
        }


        private void PropagateCheckStateDown(object sender, NodeEventArgs e)
        {
            foreach (TreeListNode node in e.Node.Nodes)
            {
                node.Checked = e.Node.Checked;
                PropagateCheckStateDown(sender, new NodeEventArgs(node));
            }
        }


        private void PropagateCheckStateUp(TreeListNode node)
        {
            CheckState checkStateToPropagate = node.CheckState;

            if (node.ParentNode != null)
            {
                if (checkStateToPropagate == CheckState.Indeterminate)
                {
                    while (node.ParentNode != null)
                    {
                        node.ParentNode.CheckState = CheckState.Indeterminate;
                        node = node.ParentNode;
                    }
                }
                else
                {
                    foreach (TreeListNode tln in node.ParentNode.Nodes)
                    {
                        if (tln.CheckState != checkStateToPropagate)
                        {
                            checkStateToPropagate = CheckState.Indeterminate;
                            break;
                        }
                    }

                    node.ParentNode.CheckState = checkStateToPropagate;
                    PropagateCheckStateUp(node.ParentNode);
                }
            }
        }
        #endregion


        #region Validation Logic
        private void Validate(object sender, EventArgs e)
        {
            bool isValid = true;

            // Проверить на допустимость дату начала отчетного периода и дату окончания.
            if (reportPeriodFrom.DateTime > reportPeriodTo.DateTime)
            {
                isValid = false;
                if (ReferenceEquals(sender, reportPeriodFrom))
                {
                    reportPeriodFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Error;
                    HighlightControl(reportPeriodFrom);
                    reportPeriodFrom.ToolTip = "Дата начала отчетного периода не может превышать дату окончания!";
                }

                if (ReferenceEquals(sender, reportPeriodTo))
                {
                    reportPeriodTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Error;
                    HighlightControl(reportPeriodTo);
                    reportPeriodTo.ToolTip = "Дата начала отчетного периода не может превышать дату окончания!";
                }
            }
            else
            {
                UnHighlightControl(reportPeriodFrom);
                UnHighlightControl(reportPeriodTo);
                reportPeriodFrom.ToolTip = reportPeriodTo.ToolTip = null;
            }

            // Проверить условие, что выбран хотя бы 1 канал.
            if (!offTradeChannel.Checked && !onTradeChannel.Checked && !kaTradeChannel.Checked)
                isValid = false;

            // Проверить на допустимость выбор в дереве персонала.
            if (!ValidateTreeList(personnelTreeList))
                isValid = false;

            // Проверить на допустимость выбор в дереве упаковок.
            if (!ValidateTreeList(packagesTreeList))
                isValid = false;

            // Проверить на допустимость выбор в дереве продуктов.
            if (!ValidateTreeList(productsTreeList))
                isValid = false;

            generateReportButton.Enabled = isValid;
        }


        private bool ValidateTreeList(TreeList tl)
        {
            bool allNodesUnchecked = true;

            tl.NodesIterator.DoOperation(node => { if (node.Checked) allNodesUnchecked = false; });

            if (allNodesUnchecked)
                HighlightControl(tl);
            else
                UnHighlightControl(tl);

            return !allNodesUnchecked;
        }


        private void Highlight(Control ctrl, Color clr, Graphics graphics = null)
        {
            if (graphics != null)
            {
                using (Pen p = new Pen(clr, 1))
                {
                    Rectangle r = ctrl.Bounds;
                    r.Inflate(1, 1);
                    graphics.DrawRectangle(p, r);
                }

            }
            else
            {
                using (Graphics g = CreateGraphics())
                {
                    using (Pen p = new Pen(clr, 1))
                    {
                        Rectangle r = ctrl.Bounds;
                        r.Inflate(1, 1);
                        g.DrawRectangle(p, r);
                    }
                }
            }
        }


        private void HighlightControl(Control ctrl)
        {
            validatedControls[ctrl] = true;
            Highlight(ctrl, HIGLIGHT_COLOR);
        }


        private void UnHighlightControl(Control ctrl)
        {
            validatedControls[ctrl] = false;
            Highlight(ctrl, BackColor);
        }
        #endregion


        private void SetDefaults()
        {
            // Период времени для выборки: с 1-го числа текущего месяца по текущее число.
            reportPeriodTo.DateTime = DateTime.Now;
            reportPeriodTo.EditValueChanged += Validate;
            reportPeriodFrom.DateTime = new DateTime(reportPeriodTo.DateTime.Year, reportPeriodTo.DateTime.Month, 1);
            reportPeriodFrom.EditValueChanged += Validate;

            // Нижний уровень персонала: для LDB - М1, для SalesWorks - M3. С LDB работает пользователь уровня М2, а выше М2 с SalesWorks.
            // Нижний уровень не может быть выбран выше текущего уровня пользователя.
            UserLevel userLevel = DataAccessProvider.GetUserLevel();
            userLevelRadioGroup.EditValue = (double)((userLevel <= UserLevel.M2) ? 1 : (userLevel == UserLevel.M3) ? 2 : 3);

            switch (userLevel)
            {
                case UserLevel.M2:
                    userLevelRadioGroup.Properties.Items[0].Enabled = userLevelRadioGroup.Properties.Items[1].Enabled = false;
                    break;
                case UserLevel.M3:
                    userLevelRadioGroup.Properties.Items[0].Enabled = false;
                    break;
            }

            // Обновление данных недоступно на SalesWorks. Оно должно быть доступно только на площадках
            //if (userLevel != UserLevel.M2)
            if (!DataAccessLayer.IsLDB)
                lblUpd.Visible = lblUpd2.Visible = btn_upd.Visible = false;

            userLevelRadioGroup.SelectedIndexChanged += LevelOrChannelChanged;

            // Канал персонала: выбраны все. Флаг минимальных отгрузок также установлен.
            offTradeChannel.Checked = onTradeChannel.Checked = kaTradeChannel.Checked = useMinShipment.Checked = true;
            offTradeChannel.CheckedChanged += LevelOrChannelChanged;
            onTradeChannel.CheckedChanged += LevelOrChannelChanged;
            kaTradeChannel.CheckedChanged += LevelOrChannelChanged;

            // Персонал.
            SetPersonnel((int)(double)userLevelRadioGroup.EditValue, Channel);

            // Выбрать все имеющиеся типы упаковок.
            packages = DataAccessProvider.GetPackages();
            packagesTreeList.DataSource = packages;
            packagesTreeList.NodesIterator.DoOperation(node => node.Checked = true);

            // Выбрать все имеющиеся продукты.
            SetProducts(ProductInPackageIdList);

            // Для все контролов TreeList установка/снятие состояния 'Checked' должно отражаться на дочерних узлах. Кроме того, для упаковок
            // должно перестраиваться дерево продуктов.
            packagesTreeList.AfterCheckNode += packagesTreeList_AfterCheckNode;
            personnelTreeList.AfterCheckNode += personnelTreeList_productsTreeList_AfterCheckNode;
            productsTreeList.AfterCheckNode += personnelTreeList_productsTreeList_AfterCheckNode;
        }


        #region Internal Properties
        internal DateTime PeriodStart                      // ВНИМАНИЕ: Важно вернуть именно дату и отбросить время, т.к. в противном случае
        { get { return reportPeriodFrom.DateTime.Date; } } // возвращается текущее время, что влияет на результат запроса к хранимой процедуре!!!


        internal DateTime PeriodEnd                        // ВНИМАНИЕ: Важно вернуть именно дату и отбросить время!!! Хранимая процедура
        { get { return reportPeriodTo.DateTime.Date; } }   // ожидает дату с обнуленным временем!!!


        internal UserLevel LowestUserLevel
        { get { return (UserLevel)Enum.Parse(typeof(UserLevel), ((int)(double)userLevelRadioGroup.EditValue).ToString()); } }


        internal string PersonnelIdList
        {
            get
            {
                StringBuilder personnel = new StringBuilder();

                personnelTreeList.NodesIterator.DoOperation(node =>
                    {
                        int personLevel = (int)node.GetValue("LEVEL"); // ВНИМАНИЕ: Регистр имен колонок должен полностью совпадать с тем, что возвращает хранимая процедура !!!
                        int personId = (int)node.GetValue("DATA_ID");
                        if (node.Checked && (personLevel == (int)LowestUserLevel))
                        {
                            if (personnel.Length > 0)
                                personnel.Append(',');
                            personnel.Append(personId);
                        }
                    });

                return personnel.ToString();
                //return "231,27,334";
            }
        }


        internal int Channel
        {
            get
            {
                UserChannel channel = UserChannel.None;
                if (offTradeChannel.Checked)
                    channel = channel | UserChannel.OffTrade;
                if (onTradeChannel.Checked)
                    channel = channel | UserChannel.OnTrade;
                if (kaTradeChannel.Checked)
                    channel = channel | UserChannel.KaTrade;

                return (int)channel;
            }
        }


        internal bool UseMinimalShipment
        { get { return useMinShipment.Checked; } }


        internal string ProductInPackageIdList
        {
            get
            {
                StringBuilder products = new StringBuilder();

                packagesTreeList.NodesIterator.DoOperation(node =>
                    {
                        object val = node.GetValue("Products"); // ВНИМАНИЕ: Регистр имен колонок должен полностью совпадать с тем, что возвращает хранимая процедура !!!
                        if (node.Checked && !(val is DBNull) && !String.IsNullOrEmpty((string)val))
                        {
                            if (products.Length > 0)
                                products.Append(',');
                            products.Append(val);
                        }
                    });

                return products.ToString();
                //return "1,111,4,7";
            }
        }


        internal string ProductIdList
        {
            get
            {
                StringBuilder products = new StringBuilder();

                productsTreeList.NodesIterator.DoOperation(node =>
                    {
                        int productId = (int)node.GetValue("DATA_ID"); // ВНИМАНИЕ: Регистр имен колонок должен полностью совпадать с тем, что возвращает хранимая процедура !!!
                        if (node.Checked && !node.HasChildren)
                        {
                            if (products.Length > 0)
                                products.Append(',');
                            products.Append(productId);
                        }
                    });

                return products.ToString();
                //return "1,111,4,7";
            }
        }
        #endregion


        private void SetPersonnel(int lowLevel, int channel)
        {
            personnel = DataAccessProvider.GetPersonnel(lowLevel, channel, null);
            personnelTreeList.DataSource = personnel;
            personnelTreeList.NodesIterator.DoOperation(node => node.Checked = true);

            if (personnelTreeList.Nodes.Count > 0)
                personnelTreeList.Nodes[0].Expanded = true;
        }


        private void SetProducts(string productIds)
        {
            if (String.IsNullOrEmpty(productIds))
            {
                productsTreeList.ClearNodes();
                return;
            }

            products = DataAccessProvider.GetProducts(productIds);
            productsTreeList.DataSource = products;
            productsTreeList.NodesIterator.DoOperation(node => node.Checked = true);
            if (productsTreeList.Nodes.Count > 0)
                productsTreeList.Nodes[0].Expanded = true;
        }

        private void SettingsForm_Paint(object sender, PaintEventArgs e)
        {
            foreach (var validatedControl in validatedControls)
            {
                if (validatedControl.Value)
                {
                    Highlight(validatedControl.Key, HIGLIGHT_COLOR, e.Graphics);
                }
            }
        }

        private void btn_upd_Click(object sender, EventArgs e)
        {
            Enabled = false;
            WaitManager.StartWait();
            try
            {
                DataAccessProvider.UpdateData();
            }
            catch (Exception) { }

            GetUpdData();
            Enabled = true;
            WaitManager.StopWait();
        }
        
        private void GetUpdData()
        {
            lblUpd2.Text = "";

            var dataTableDate = DataAccessProvider.GetUpdateData(DateTime.Now);
            if (dataTableDate == null)
                return;

            foreach (DataRow dr in dataTableDate.Rows)
            {
                lblUpd2.Text = Convert.ToDateTime(dr[DataAccessProvider.SP_GET_UPDATE_TIME_FLD_PREF_VALUE]).ToString();
                break;
            }
        }

	}
}
