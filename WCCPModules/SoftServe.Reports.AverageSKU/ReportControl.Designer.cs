﻿namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.barMenuManager = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnSettings = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportToXlsx = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.pivotGridFieldsCheckedComboBoxEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.imCollection = new DevExpress.Utils.ImageCollection();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pgfNetwork = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfRegion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfProvince = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfTerritoryType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfTypeTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfCodeTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM3Channel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfAverageSKU = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfCountTT = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridFieldsCheckedComboBoxEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // barMenuManager
            // 
            this.barMenuManager.AllowCustomization = false;
            this.barMenuManager.AllowMoveBarOnToolbar = false;
            this.barMenuManager.AllowQuickCustomization = false;
            this.barMenuManager.AllowShowToolbarsPopup = false;
            this.barMenuManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barMenuManager.Controller = this.barAndDockingController1;
            this.barMenuManager.DockControls.Add(this.barDockControlTop);
            this.barMenuManager.DockControls.Add(this.barDockControlBottom);
            this.barMenuManager.DockControls.Add(this.barDockControlLeft);
            this.barMenuManager.DockControls.Add(this.barDockControlRight);
            this.barMenuManager.DockControls.Add(this.standaloneBarDockControl1);
            this.barMenuManager.Form = this;
            this.barMenuManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSettings,
            this.btnPrint,
            this.btnRefresh,
            this.btnExportTo,
            this.btnExportToXlsx,
            this.barButtonItem1,
            this.barStaticItem1,
            this.barEditItem1});
            this.barMenuManager.LargeImages = this.imCollection;
            this.barMenuManager.MaxItemId = 47;
            this.barMenuManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.pivotGridFieldsCheckedComboBoxEdit});
            // 
            // bar1
            // 
            this.bar1.BarName = "menuBar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(74, 165);
            this.bar1.FloatSize = new System.Drawing.Size(48, 32);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnRefresh, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSettings),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTo, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.barEditItem1, "", false, true, true, 469)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "menuBar";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Обновить";
            this.btnRefresh.Id = 6;
            this.btnRefresh.LargeImageIndex = 0;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnSettings
            // 
            this.btnSettings.Caption = "Параметры отчета";
            this.btnSettings.Id = 3;
            this.btnSettings.LargeImageIndex = 6;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSettings_ItemClick);
            // 
            // btnExportTo
            // 
            this.btnExportTo.Caption = "Экспортировать в ...";
            this.btnExportTo.Id = 10;
            this.btnExportTo.LargeImageIndex = 10;
            this.btnExportTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToXlsx)});
            this.btnExportTo.Name = "btnExportTo";
            // 
            // btnExportToXlsx
            // 
            this.btnExportToXlsx.Caption = "Xlsx";
            this.btnExportToXlsx.Id = 16;
            this.btnExportToXlsx.LargeImageIndex = 15;
            this.btnExportToXlsx.Name = "btnExportToXlsx";
            this.btnExportToXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "Печать";
            this.btnPrint.Id = 5;
            this.btnPrint.LargeImageIndex = 19;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem1.Caption = "Показывать итоги по:";
            this.barStaticItem1.Id = 42;
            this.barStaticItem1.LeftIndent = 75;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.pivotGridFieldsCheckedComboBoxEdit;
            this.barEditItem1.Id = 46;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.Width = 200;
            this.barEditItem1.EditValueChanged += new System.EventHandler(this.barEditItem1_EditValueChanged);
            // 
            // pivotGridFieldsCheckedComboBoxEdit
            // 
            this.pivotGridFieldsCheckedComboBoxEdit.AutoHeight = false;
            this.pivotGridFieldsCheckedComboBoxEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pivotGridFieldsCheckedComboBoxEdit.DropDownRows = 14;
            this.pivotGridFieldsCheckedComboBoxEdit.Name = "pivotGridFieldsCheckedComboBoxEdit";
            this.pivotGridFieldsCheckedComboBoxEdit.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.pivotGridFieldsCheckedComboBoxEdit_Closed);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(956, 32);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "d";
            this.barButtonItem1.Id = 41;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "refresh_24.png");
            this.imCollection.Images.SetKeyName(1, "briefcase_prev_24.png");
            this.imCollection.Images.SetKeyName(2, "del_24.png");
            this.imCollection.Images.SetKeyName(3, "Excel_42.bmp");
            this.imCollection.Images.SetKeyName(4, "ExcelAll_42.bmp");
            this.imCollection.Images.SetKeyName(5, "print_24.png");
            this.imCollection.Images.SetKeyName(6, "briefcase_ok_24.png");
            this.imCollection.Images.SetKeyName(7, "briefcase_save_24.png");
            this.imCollection.Images.SetKeyName(15, "xlsx.png");
            this.imCollection.Images.SetKeyName(16, "bmp.PNG");
            this.imCollection.Images.SetKeyName(17, "csv.PNG");
            this.imCollection.Images.SetKeyName(18, "copy_prev_24.png");
            this.imCollection.Images.SetKeyName(19, "doc_prev_24.png");
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.AppearancePrint.Cell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pivotGridControl.AppearancePrint.Cell.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.CustomTotalCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.CustomTotalCell.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.FieldHeader.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.FieldHeader.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.FieldValue.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.FieldValue.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.FieldValueGrandTotal.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.FieldValueGrandTotal.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.FieldValueTotal.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.FieldValueTotal.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.FilterSeparator.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.FilterSeparator.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.GrandTotalCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.GrandTotalCell.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.HeaderGroupLine.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.HeaderGroupLine.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.Lines.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.Lines.Options.UseFont = true;
            this.pivotGridControl.AppearancePrint.TotalCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.pivotGridControl.AppearancePrint.TotalCell.Options.UseFont = true;
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pgfNetwork,
            this.pgfRegion,
            this.pgfProvince,
            this.pgfTerritoryType,
            this.pgfM4,
            this.pgfM3,
            this.pgfM2,
            this.pgfM1,
            this.pgfTypeTT,
            this.pgfCodeTT,
            this.pgfNameTT,
            this.pgfAddressTT,
            this.pgfM3Channel,
            this.pgfAverageSKU,
            this.pgfCountTT});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 32);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsBehavior.CopyToClipboardWithFieldValues = true;
            this.pivotGridControl.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pivotGridControl.OptionsChartDataSource.SelectionOnly = false;
            //this.pivotGridControl.OptionsChartDataSource.ShowRowTotals = false;
            this.pivotGridControl.OptionsLayout.Columns.AddNewColumns = false;
            this.pivotGridControl.OptionsLayout.Columns.RemoveOldColumns = false;
            this.pivotGridControl.OptionsLayout.StoreDataSettings = false;
            this.pivotGridControl.OptionsLayout.StoreVisualOptions = false;
            this.pivotGridControl.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridControl.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridControl.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridControl.OptionsPrint.UsePrintAppearance = true;
            this.pivotGridControl.OptionsView.ShowDataHeaders = false;
            this.pivotGridControl.Size = new System.Drawing.Size(956, 444);
            this.pivotGridControl.TabIndex = 13;
            this.pivotGridControl.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridControl_CustomSummary);
            // 
            // pgfNetwork
            // 
            this.pgfNetwork.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfNetwork.AreaIndex = 0;
            this.pgfNetwork.Caption = "Сеть";
            this.pgfNetwork.EmptyValueText = "(не задана)";
            this.pgfNetwork.FieldName = "Network";
            this.pgfNetwork.Name = "pgfNetwork";
            this.pgfNetwork.Width = 150;
            // 
            // pgfRegion
            // 
            this.pgfRegion.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfRegion.AreaIndex = 1;
            this.pgfRegion.Caption = "Регион";
            this.pgfRegion.FieldName = "Region";
            this.pgfRegion.Name = "pgfRegion";
            this.pgfRegion.Width = 170;
            // 
            // pgfProvince
            // 
            this.pgfProvince.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfProvince.AreaIndex = 2;
            this.pgfProvince.Caption = "Область";
            this.pgfProvince.FieldName = "Province";
            this.pgfProvince.Name = "pgfProvince";
            this.pgfProvince.Width = 220;
            // 
            // pgfTerritoryType
            // 
            this.pgfTerritoryType.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfTerritoryType.AreaIndex = 3;
            this.pgfTerritoryType.Caption = "Тип территории";
            this.pgfTerritoryType.FieldName = "TerritoryType";
            this.pgfTerritoryType.Name = "pgfTerritoryType";
            this.pgfTerritoryType.Width = 150;
            // 
            // pgfM4
            // 
            this.pgfM4.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfM4.AreaIndex = 4;
            this.pgfM4.Caption = "M4";
            this.pgfM4.FieldName = "M4";
            this.pgfM4.Name = "pgfM4";
            this.pgfM4.Width = 150;
            // 
            // pgfM3
            // 
            this.pgfM3.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfM3.AreaIndex = 5;
            this.pgfM3.Caption = "M3";
            this.pgfM3.FieldName = "M3";
            this.pgfM3.Name = "pgfM3";
            this.pgfM3.Width = 150;
            // 
            // pgfM2
            // 
            this.pgfM2.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfM2.AreaIndex = 6;
            this.pgfM2.Caption = "M2";
            this.pgfM2.FieldName = "M2";
            this.pgfM2.Name = "pgfM2";
            this.pgfM2.Width = 150;
            // 
            // pgfM1
            // 
            this.pgfM1.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfM1.AreaIndex = 7;
            this.pgfM1.Caption = "M1";
            this.pgfM1.FieldName = "M1";
            this.pgfM1.Name = "pgfM1";
            this.pgfM1.Width = 150;
            // 
            // pgfTypeTT
            // 
            this.pgfTypeTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfTypeTT.AreaIndex = 8;
            this.pgfTypeTT.Caption = "Тип ТТ";
            this.pgfTypeTT.FieldName = "TypeTT";
            this.pgfTypeTT.Name = "pgfTypeTT";
            // 
            // pgfCodeTT
            // 
            this.pgfCodeTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfCodeTT.AreaIndex = 9;
            this.pgfCodeTT.Caption = "Код ТТ";
            this.pgfCodeTT.FieldName = "CodeTT";
            this.pgfCodeTT.Name = "pgfCodeTT";
            // 
            // pgfNameTT
            // 
            this.pgfNameTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfNameTT.AreaIndex = 10;
            this.pgfNameTT.Caption = "Название ТТ";
            this.pgfNameTT.FieldName = "NameTT";
            this.pgfNameTT.Name = "pgfNameTT";
            // 
            // pgfAddressTT
            // 
            this.pgfAddressTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfAddressTT.AreaIndex = 11;
            this.pgfAddressTT.Caption = "Адрес ТТ";
            this.pgfAddressTT.FieldName = "AddressTT";
            this.pgfAddressTT.Name = "pgfAddressTT";
            // 
            // pgfM3Channel
            // 
            this.pgfM3Channel.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfM3Channel.AreaIndex = 12;
            this.pgfM3Channel.Caption = "Канал M1";
            this.pgfM3Channel.FieldName = "ChannelM3";
            this.pgfM3Channel.Name = "pgfM3Channel";
            this.pgfM3Channel.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            this.pgfM3Channel.Width = 160;
            // 
            // pgfAverageSKU
            // 
            this.pgfAverageSKU.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
            this.pgfAverageSKU.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfAverageSKU.AreaIndex = 0;
            this.pgfAverageSKU.Caption = "Среднее по полю количество СКЮ";
            this.pgfAverageSKU.CellFormat.FormatString = "N2";
            this.pgfAverageSKU.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfAverageSKU.FieldName = "AverageSKU";
            this.pgfAverageSKU.Name = "pgfAverageSKU";
            this.pgfAverageSKU.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pgfAverageSKU.Width = 241;
            // 
            // pgfCountTT
            // 
            this.pgfCountTT.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pgfCountTT.FieldName = "CountTT";
            this.pgfCountTT.Name = "pgfCountTT";
            this.pgfCountTT.Options.ShowInCustomizationForm = false;
            this.pgfCountTT.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            this.pgfCountTT.Visible = false;
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pivotGridControl);
            this.Controls.Add(this.standaloneBarDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(956, 476);
            this.Load += new System.EventHandler(this.WccpUIControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridFieldsCheckedComboBoxEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barMenuManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem btnSettings;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarSubItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem btnExportToXlsx;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        public DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit pivotGridFieldsCheckedComboBoxEdit;
        internal DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pgfNetwork;
        private DevExpress.XtraPivotGrid.PivotGridField pgfRegion;
        private DevExpress.XtraPivotGrid.PivotGridField pgfProvince;
        private DevExpress.XtraPivotGrid.PivotGridField pgfTerritoryType;
        private DevExpress.XtraPivotGrid.PivotGridField pgfM4;
        private DevExpress.XtraPivotGrid.PivotGridField pgfM3;
        private DevExpress.XtraPivotGrid.PivotGridField pgfM2;
        private DevExpress.XtraPivotGrid.PivotGridField pgfM1;
        private DevExpress.XtraPivotGrid.PivotGridField pgfTypeTT;
        private DevExpress.XtraPivotGrid.PivotGridField pgfCodeTT;
        private DevExpress.XtraPivotGrid.PivotGridField pgfM3Channel;
        private DevExpress.XtraPivotGrid.PivotGridField pgfAverageSKU;
        private DevExpress.XtraPivotGrid.PivotGridField pgfCountTT;
        private DevExpress.XtraPivotGrid.PivotGridField pgfNameTT;
        private DevExpress.XtraPivotGrid.PivotGridField pgfAddressTT;

    }
}
