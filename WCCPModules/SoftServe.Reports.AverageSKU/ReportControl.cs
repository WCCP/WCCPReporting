﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.AverageSKU;

namespace WccpReporting {
    public partial class WccpUIControl : XtraUserControl {
        #region Fields

        private byte[] pivotLayout;

        #endregion

        #region Constructors

        public WccpUIControl() {
            InitializeComponent();
            using (var ms = new MemoryStream()) {
                pivotGridControl.SaveLayoutToStream(ms);
                pivotLayout = ms.ToArray();
            }
        }

        #endregion

        #region Instance Indexers

        private PivotGridField this[string caption] {
            get {
                foreach (PivotGridField lField in pivotGridControl.Fields)
                    if (lField.Caption == caption)
                        return lField;

                return null;
            }
        }

        #endregion

        #region Instance Properties

        private List<string> FieldsAvailableForTotals {
            get {
                List<string> lFields = new List<string>(14);
                foreach (PivotGridField lField in pivotGridControl.Fields)
                    if (lField.Visible && !ReferenceEquals(lField, pgfAverageSKU))
                        lFields.Add(lField.Caption);
                lFields.Sort();

                return lFields;
            }
        }

        private IEnumerable<PrintableComponentLink> PrintableComponents {
            get { return new[] {new PrintableComponentLink {Component = pivotGridControl}}; }
        }

        #endregion

        #region Instance Methods

        private void CallSettingsForm() {
            if (SettingsForm.ShowDialog() == DialogResult.OK)
                GenerateReport();
        }

        private void ChangeTotalsCalculation(string[] fieldsUsedForTotals) {
            pivotGridControl.BeginUpdate();
            try {
                foreach (string lFieldName in FieldsAvailableForTotals) {
                    PivotGridField lField = this[lFieldName];
                    if (fieldsUsedForTotals.Contains(lFieldName))
                        lField.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
                    else
                        lField.TotalsVisibility = PivotTotalsVisibility.None;
                }
            }
            finally {
                pivotGridControl.EndUpdate();
            }
        }

        private void GenerateReport() {
            if (DesignMode)
                return;

            WaitManager.StartWait();

            try {
                pivotGridControl.DataSource = DataAccessProvider.GetReport(SettingsForm.PeriodStart, SettingsForm.PeriodEnd,
                    SettingsForm.LowestUserLevel,
                    SettingsForm.PersonnelIdList,
                    SettingsForm.UseMinimalShipment,
                    SettingsForm.ProductIdList);
                ////pivotGridControl.DataSource = DataAccessProvider.GetReport(); // Это вызов тестового метода!
                SetPivotGridFields();
            }
            finally {
                WaitManager.StopWait();
            }
        }

        private CompositeLink PrepareCompositeLink() {
            CompositeLink lCompositeLink = new CompositeLink(new PrintingSystem());
            foreach (PrintableComponentLink lLink in PrintableComponents)
                lCompositeLink.Links.Add(lLink);

            lCompositeLink.PaperKind = PaperKind.A4Rotated;
            PageHeaderFooter lHeaderFooter = lCompositeLink.PageHeaderFooter as PageHeaderFooter;
            lHeaderFooter.Header.Content.Clear();
            lHeaderFooter.Header.Content.AddRange(new[] {String.Empty, (string) barEditItem1.EditValue, String.Empty});
            lHeaderFooter.Header.Font = new Font(lHeaderFooter.Header.Font.FontFamily, lHeaderFooter.Header.Font.Size + 2, FontStyle.Bold);
            lHeaderFooter.Header.LineAlignment = BrickAlignment.Center;

            lCompositeLink.CreateDocument();
            return lCompositeLink;
        }

        private string SelectFilePath(ExportToType type) {
            string lFileName = string.Empty;
            SaveFileDialog lSaveDialog = new SaveFileDialog();
            lSaveDialog.Title = "Укажите имя файла"; //// Resources.SpecifyFileName;
            lSaveDialog.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            lSaveDialog.FileName = "AverageSKU report"; //// Resources.DefaultFileName;
            lSaveDialog.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            lSaveDialog.FilterIndex = 1;
            lSaveDialog.OverwritePrompt = true;
            lSaveDialog.RestoreDirectory = true;

            if (lSaveDialog.ShowDialog() != DialogResult.Cancel)
                lFileName = lSaveDialog.FileName;
            return lFileName;
        }

        private void SetPivotGridFields() {
            UserLevel lUserLevel = DataAccessProvider.GetUserLevel();
            UserLevel lLowestUserLevel = SettingsForm.LowestUserLevel;
            if (lUserLevel < lLowestUserLevel)
                throw new ArgumentException("Current user level is less than the lowest level specified in the 'Settings' form.");

            //Restore initial control layout
            using (MemoryStream lMs = new MemoryStream(pivotLayout, false))
                pivotGridControl.RestoreLayoutFromStream(lMs);

            string[] lNames = Enum.GetNames(typeof (UserLevel));
            Array lValues = Enum.GetValues(typeof (UserLevel));

            pivotGridControl.BeginUpdate();
            try {
                // Управление видимостью и областью размещения полей М4, М3, М2, М1.
                for (int lI = lValues.Length - 1; lI > 0; lI--) {
                    PivotGridField lField = pivotGridControl.Fields[lNames[lI]];
                    if (lField == null)
                        continue;

                    int lValue = (int) lValues.GetValue(lI);
                    lField.Visible = ((int) lUserLevel >= lValue && lValue >= (int) lLowestUserLevel);

                    if (lField.Visible)
                        lField.Area = (lUserLevel > lLowestUserLevel && (int) lUserLevel == lValue)
                                         ? PivotArea.FilterArea
                                         : PivotArea.RowArea;
                }

                // Управление видимостью полей "Код TT" и "Тип ТТ".
                pivotGridControl.Fields["CodeTT"].Visible = (lLowestUserLevel == UserLevel.M1);
                pivotGridControl.Fields["TypeTT"].Visible = (lLowestUserLevel == UserLevel.M1);
                pivotGridControl.Fields["NameTT"].Visible = (lLowestUserLevel == UserLevel.M1);
                pivotGridControl.Fields["AddressTT"].Visible = (lLowestUserLevel == UserLevel.M1);
            }
            finally {
                pivotGridControl.EndUpdate();
            }

            // Установить поля, по которым будут подводиться итоги.
            pivotGridFieldsCheckedComboBoxEdit.Items.Clear();
            foreach (string lFieldName in FieldsAvailableForTotals)
                pivotGridFieldsCheckedComboBoxEdit.Items.Add(lFieldName, CheckState.Checked, true);

            barEditItem1.EditValue = pivotGridFieldsCheckedComboBoxEdit.GetCheckedItems();
        }

        #endregion

        #region Event Handling

        private void WccpUIControl_Load(object sender, EventArgs e) {
            GenerateReport();
        }

        private void barEditItem1_EditValueChanged(object sender, EventArgs e) {
            string lFieldsUsedForTotals = (string) barEditItem1.EditValue;
            string[] lFields = lFieldsUsedForTotals.Split(pivotGridFieldsCheckedComboBoxEdit.SeparatorChar);
            for (int i = 0; i < lFields.Length; i++)
                lFields[i] = lFields[i].Trim();

            ChangeTotalsCalculation(lFields);
        }

        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e) {
            string lFName = SelectFilePath(ExportToType.Xlsx);
            if (string.IsNullOrEmpty(lFName))
                return;

            XlsxExportOptions lXlsxOptions = new XlsxExportOptions();
            lXlsxOptions.ExportHyperlinks = false;
            lXlsxOptions.SheetName = DataAccessProvider.ReportCaption;
            pivotGridControl.ExportToXlsx(lFName, lXlsxOptions);
        }

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e) {
            PrepareCompositeLink().PrintingSystem.PreviewFormEx.Show();
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e) {
            GenerateReport();
        }

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e) {
            CallSettingsForm();
        }

        private void pivotGridControl_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e) {
            PivotDrillDownDataSource lDataSource = e.CreateDrillDownDataSource();
            int lSumSku = lDataSource.Cast<PivotDrillDownDataRow>().Sum(r => ConvertEx.ToInt(r[pgfAverageSKU]));
            int lCountTT = lDataSource.Cast<PivotDrillDownDataRow>().Sum(r => ConvertEx.ToInt(r[pgfCountTT]));
            e.CustomValue = (lCountTT == 0) ? 0 : 1.0*lSumSku/lCountTT;
        }

        private void pivotGridFieldsCheckedComboBoxEdit_Closed(object sender, ClosedEventArgs e) {
            barEditItem1.EditValue = pivotGridFieldsCheckedComboBoxEdit.GetCheckedItems();
        }

        #endregion

        #region Class Properties

        public static SettingsForm SettingsForm { get; set; }

        #endregion
    }
}