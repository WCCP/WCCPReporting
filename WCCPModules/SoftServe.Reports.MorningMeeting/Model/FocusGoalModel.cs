﻿using System;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class FocusGoalModel : EditableObject<FocusGoalModel>
    {
        [MapField("FocusTarget_ID")]
        public abstract int TargetId { get; set; }

        [MapField("M3_ID")]
        public abstract int M3Id { get; set; }

        [MapField("FocusTargetName")]
        public abstract string TargetName { get; set; }

        [MapField("IsManualTarget")]
        public abstract bool IsManual { get; set; }

        [MapField("DateCreated")]
        public abstract DateTime CreationDate { get; set; }

        [MapField("StartDate")]
        public abstract DateTime StartDate { get; set; }

        [MapField("EndDate")]
        public abstract DateTime EndDate { get; set; }

        [MapField("ChannelMask")]
        public abstract int ChannelMask { get; set; }
        
        public override int GetHashCode()
        {
            return TargetId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is FocusGoalModel)
                return TargetId.Equals((obj as FocusGoalModel).TargetId);
            return ReferenceEquals(obj, this);
        }
    }
}
