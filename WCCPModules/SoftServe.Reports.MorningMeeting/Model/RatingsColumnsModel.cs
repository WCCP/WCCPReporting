﻿using System;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class RatingsColumnsModel : EditableObject<RatingsColumnsModel>
    {
        [MapField("M3_ID")]
        public abstract int M3Id { get; set; }

        [MapField("ID")]
        public abstract int ColumnId { get; set; }

        public abstract string ColumnIdDescription { get; set; }

        [MapField("Name")]
        public abstract string Name { get; set; }

        [MapField("ColumnType_ID")]
        public abstract int KpiId { get; set; }

        [MapField("ValueType")]
        public abstract RatingsSetType ValueType { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }
    }
}
