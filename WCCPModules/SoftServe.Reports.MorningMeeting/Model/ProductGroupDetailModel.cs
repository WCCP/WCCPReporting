﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model {
    public class ProductGroupDetailModel : EditableObject<ProductGroupDetailModel> {
        public int SKUGroupId { get; set; }
        
        [MapField("Item_ID")]
        public int ItemId { get; set; }
        public int Level { get; set; }
    }
}