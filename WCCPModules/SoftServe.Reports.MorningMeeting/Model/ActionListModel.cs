﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class ActionListModel : EditableObject <ActionListModel>
    {
        [MapField("Country_ID")]
        public abstract int CountryId { get; set; }

        [MapField("M3_ID")]
        public abstract int M3Id { get; set; }

        [MapField("Item_ID")]
        public abstract int ItemId { get; set; }
        
        public override int GetHashCode()
        {
            return (CountryId + M3Id).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ActionListModel)
            {
                return CountryId.Equals((obj as ActionListModel).CountryId) && M3Id.Equals((obj as ActionListModel).M3Id) && ItemId.Equals((obj as ActionListModel).ItemId);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
