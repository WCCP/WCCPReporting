﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public class ProductListItemModel
    {
        [MapField("ID")]
        public int Id { get; set; }

        public string Name { get; set; }

        [MapField("M3_ID")]
        public int M3Id { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ProductListItemModel)
            {
                return Id.Equals((obj as ProductListItemModel).Id);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
