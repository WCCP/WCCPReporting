﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public class KpiModel
    {
        [PrimaryKey, NonUpdatable]
        [MapField("ID")]
        public int Id { get; set; }

        public string Name { get; set; }

        [MapField("KpiType_ID")]
        public int KpiTypeId { get; set; }

        [MapField("ValueType")]
        public RatingsSetType ValueType { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is KpiModel)
            {
                return Id.Equals((obj as KpiModel).Id);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
