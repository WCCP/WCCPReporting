﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public class InvalidPlanModel
    {
        [MapField("ID")]
        public int Id { get; set; }

        [MapField("StaffID")]
        public int StaffId { get; set; }

        [MapField("ErrorCode")]
        public int Status { get; set; }
    }
}
