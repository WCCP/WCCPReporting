﻿using System;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class RatingsModel : EditableObject<RatingsModel>
    {
        [MapField("RKPI_ID")]
        public abstract int Id { get; set; }

        [MapField("M3_ID")]
        public abstract int M3Id { get; set; }

        [MapField("ChannelType_ID")]
        public abstract int ChannelId { get; set; }
        
        [MapField("KpiType_ID")]
        public abstract int KpiTypeId { get; set; }

        [MapField("KPI_ID")]
        public abstract int KpiId { get; set; }

        [MapField("Value")]
        public abstract string Values { get; set; }

        [MapField("RKPI_Weight")]
        public abstract int Weight { get; set; }

        [MapField("Column_ID")]
        public abstract int ColumnId { get; set; }

        [MapField("ColumnName")]
        public abstract string ColumnName { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is RatingsModel)
                return Id.Equals((obj as RatingsModel).Id);
            return ReferenceEquals(obj, this);
        }
    }
}
