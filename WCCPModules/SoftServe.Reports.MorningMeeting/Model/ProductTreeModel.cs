﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model {
    public class ProductTreeModel {
        [MapField("ID"), PrimaryKey, NonUpdatable]
        public int Id { get; set; }

        [MapField("ParentID")]
        public int? ParentId { get; set; }

        public string Name { get; set; }
        public int Level { get; set; }
        
        [MapField("Item_Id")]
        public int? ItemId { get; set; }

        [MapIgnore]
        [DefaultValue(true)]
        public bool Editable { get; set; }

        public override string ToString() {
            return Name ?? string.Empty;
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj) {
            if (obj is ProductTreeModel)
                return Id.Equals((obj as ProductTreeModel).Id);
            return ReferenceEquals(obj, this);
        }
         
    }
}