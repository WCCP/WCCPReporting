﻿using System;
using System.Collections.Generic;
using System.Globalization;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;
using SoftServe.Reports.MorningMeeting.DataAccess;

namespace SoftServe.Reports.MorningMeeting.Model {
    public abstract class SKUListItemModel : EditableObject<SKUListItemModel> {
        // List of ID. Id is the PK in SKU Tree (Id).
        private string _selectedIdList = string.Empty;
        private bool _isIdListModified;

        #region Properties

        public abstract int ListType { get; set; }
        
        [MapField("Country_ID")]
        public abstract int CountryId { get; set; }
        
        [MapField("M3_ID")]
        public abstract int M3Id { get; set; }
        
        public abstract string GroupName { get; set; }
        public abstract int SetId { get; set; }
        public abstract int SKUGroupId { get; set; }
        public abstract string SKUGroupName { get; set; }
        public abstract double SortOrder { get; set; }
        public abstract int ChannelMask { get; set; }
        [MapField("InPackage")]
        public abstract bool IsInPackage { get; set; }

        [MapField("ProductCn_ID")]
        public abstract int ProductCnId { get; set; }

        public List<int> IdList {
            get {
                string[] lStrIdArray = _selectedIdList.Split(SqlConstants.DelimiterIdList.ToCharArray());
                List<int> lIdList = new List<int>(lStrIdArray.Length);
                if (_selectedIdList != "")
                    for (int lIndex = 0; lIndex < lStrIdArray.Length; lIndex++) {
                        int lId = Convert.ToInt32(lStrIdArray[lIndex]);
                        if (!lIdList.Contains(lId))
                            lIdList.Add(lId);
                    }
                lIdList.Sort();

                return lIdList; 
            }
        }

        //[MapField("NonUpdatable")]
        public string SelectedIdList {
            get { return GetSelectedIdList(); }
            set { SetSelectedIdList(value); }
        }

        public bool IsIdListModified { get { return _isIdListModified; } }

        #endregion

        public string GetSelectedIdList() {
            return _selectedIdList;
        }

        public void SetSelectedIdList(string strIdList) {
            if (_selectedIdList.Equals(strIdList))
                return;
            _selectedIdList = strIdList;
            _isIdListModified = true;
        }

        public void AddIdToList(int id) {
            _selectedIdList += string.IsNullOrEmpty(_selectedIdList)
                                   ? id.ToString(CultureInfo.InvariantCulture)
                                   : "," + id.ToString(CultureInfo.InvariantCulture);
        }

        public void ClearIdList() {
            _selectedIdList = string.Empty;
        }

        public override void AcceptChanges() {
            base.AcceptChanges();
            _isIdListModified = false;
        }

        public override void RejectChanges() {
            base.RejectChanges();
            _isIdListModified = false;
        }

        public override bool IsDirty {
            get {
                return base.IsDirty || _isIdListModified;
            }
        }
         
    }
}