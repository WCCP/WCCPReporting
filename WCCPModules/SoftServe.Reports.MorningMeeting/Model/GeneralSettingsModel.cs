﻿using System;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class GeneralSettingsModel : EditableObject<GeneralSettingsModel>
    {
        [MapField("Country_ID")]
        public abstract int CountryId { get; set; }

        [MapField("M3_ID")]
        public abstract int M3Id { get; set; }

        public abstract int AmountOfTheWorstOLs { get; set; }

        public abstract int AmountOfFocusOLs { get; set; }

        public abstract DateTime TimeOfFirstVisit { get; set; }

        public abstract DateTime TimeOfLastVisit { get; set; }

        public abstract decimal InBevBeerVolumeLimit { get; set; }

        public abstract int AmountOfDistrPushSKU { get; set; }

        public abstract int AmountOfFocusSKU { get; set; }

        public abstract int AmountOfMP { get; set; }
        
        public abstract int AmountOfFocusTargets { get; set; }

        [MapField("DSMFocusText")]
        public abstract string FocusByDSM { get; set; }

        public override int GetHashCode()
        {
            return (CountryId + M3Id).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is GeneralSettingsModel)
                return CountryId.Equals((obj as GeneralSettingsModel).CountryId) && M3Id.Equals((obj as GeneralSettingsModel).M3Id);
            return ReferenceEquals(obj, this);
        }
    }
}
