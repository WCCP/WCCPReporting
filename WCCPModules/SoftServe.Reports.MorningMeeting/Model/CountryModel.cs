﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model {
    public class CountryModel {
        [MapField("Country_id"), PrimaryKey, NonUpdatable]
        public int Id { get; set; }

        [MapField("Country_Name")]
        public string Name { get; set; }

        public override string ToString() {
            return Name ?? string.Empty;
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj) {
            if (obj is CountryModel)
                return Id.Equals((obj as CountryModel).Id);
            return ReferenceEquals(obj, this);
        }
    }
}