﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model {
    public abstract class BaseSKUGroupModel : EditableObject {
        public abstract byte ListType { get; set; }
        
        [MapField("Country_ID")]
        public abstract int CountryId { get; set; }
        
        [MapField("M3_ID")]
        public abstract int M3Id { get; set; }
        
        public abstract string GroupName { get; set; }
        public abstract int SetId { get; set; }
        public abstract int SKUGroupId { get; set; }
        public abstract string SKUGroupName { get; set; }
        public abstract double SortOrder { get; set; }
        public abstract int ChannelMask { get; set; }
    }
}