﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class AutoGoalDetailsModel : EditableObject<AutoGoalDetailsModel>
    {
        [MapField("FocusTarget_ID")]
        public abstract int TargetId { get; set; }

        [MapField("TargetKPI_ID")]
        public abstract int TargetTypeId { get; set; }

        [MapField("ProductCn_ID")]
        public abstract string Products { get; set; }
        
        public override int GetHashCode()
        {
            return TargetId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is AutoGoalDetailsModel)
            {
                return TargetId.Equals((obj as AutoGoalDetailsModel).TargetId);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
