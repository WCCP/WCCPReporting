﻿using System;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class BindedColumnModel : EditableObject<BindedColumnModel>
    {
        [MapField("Column_ID")]
        public abstract int Id { get; set; }

        [MapField("ColumnName")]
        public abstract string Name { get; set; }

        public abstract string BandName { get; set; }

        [MapField("CapsBinding")]
        public abstract int CapsBinding { get; set; }

        [MapField("CapsSubtarget_ID")]
        public abstract long? CapsSubtargetId { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is BindedColumnModel)
                return Id.Equals((obj as BindedColumnModel).Id);
            return ReferenceEquals(obj, this);
        }
    }
}
