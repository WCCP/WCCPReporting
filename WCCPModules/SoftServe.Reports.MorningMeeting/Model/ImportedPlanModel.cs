﻿using System;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public class ImportedPlanModel
    {
        public int EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public string KpiName { get; set; }

        public DateTime Date { get; set; }

        public decimal Plan { get; set; }

        public string PageName { get; set; }

        public long RowNumber { get; set; }

        public bool Status { get; set; }

        public string StatusDescription { get; set; }
    }
}
