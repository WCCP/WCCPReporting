﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model {
    public class StaffTreeModel {
        [MapField("StaffID"), PrimaryKey, NonUpdatable]
        public int Id { get; set; }

        [MapField("ParentID")]
        public int? ParentId { get; set; }

        public string Name { get; set; }
        public int Level { get; set; }
        
        [MapField("Item_ID")]
        public int? ItemId { get; set; }

        public override string ToString() {
            return Name ?? string.Empty;
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj) {
            if (obj is StaffTreeModel)
                return Id.Equals((obj as StaffTreeModel).Id);
            return ReferenceEquals(obj, this);
        }
         
    }
}