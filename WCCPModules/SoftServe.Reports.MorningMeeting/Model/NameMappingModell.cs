﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public class NameMappingModel
    {
        public string NameUI { get; set; }

        public string NameDB { get; set; }
    }
}
