﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class CapsSubtargetModel : EditableObject<CapsSubtargetModel>
    {
        [MapField("ID")]
        public abstract long Id { get; set; }

        [MapField("Name")]
        public abstract string Name { get; set; }

        [MapField("IsAlreadyBinded")]
        public abstract bool IsAlreadyBinded { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is CapsSubtargetModel)
            {
                return Id.Equals((obj as CapsSubtargetModel).Id);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
