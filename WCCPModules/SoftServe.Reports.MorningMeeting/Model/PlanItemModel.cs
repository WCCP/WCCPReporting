﻿namespace SoftServe.Reports.MorningMeeting.Model
{
    public class PlanItemModel
    {
        public int EmployeeId { get; set; }

        public int ColumnId { get; set; }

        public decimal? Value { get; set; }
    }
}
