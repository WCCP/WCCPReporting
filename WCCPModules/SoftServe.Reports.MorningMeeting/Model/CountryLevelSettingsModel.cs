﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class CountryLevelSettingsModel : EditableObject<CountryLevelSettingsModel>
    {
        [MapField("SaleGracePeriod")]
        public abstract int SalesGracePeriod { get; set; }

        [MapField("PackageDistributionPeriod")]
        public abstract int PackageDistributionPeriod { get; set; }

        [MapField("ACB_Method_ID")]
        public abstract int AkbCalcMethodId { get; set; }

        [MapField("SR_Method_ID")]
        public abstract int SrCalcMethodId { get; set; }
    }
}
