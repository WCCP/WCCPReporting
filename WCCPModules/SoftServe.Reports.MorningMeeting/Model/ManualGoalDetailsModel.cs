﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public abstract class ManualGoalDetailsModel : EditableObject<ManualGoalDetailsModel>
    {
        [MapField("FocusTarget_ID")]
        public abstract int TargetId { get; set; }

        [MapField("TaskText")]
        public abstract string Description { get; set; }
        
        public override int GetHashCode()
        {
            return TargetId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ManualGoalDetailsModel)
            {
                return TargetId.Equals((obj as ManualGoalDetailsModel).TargetId);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
