﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Model
{
    public class ListItemModel
    {
        [MapField("ID")]
        public int Id { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ListItemModel)
            {
                return Id.Equals((obj as ListItemModel).Id) && Name.Equals((obj as ListItemModel).Name);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
