﻿namespace SoftServe.Reports.MorningMeeting.Model {
    public class RoleModel {
        public string RoleName { get; set; }
        public string Access { get; set; }
    }
}