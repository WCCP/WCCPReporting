﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTreeList.Nodes;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View
{
    [ToolboxItem(true)]
    public partial class FocusGoalsView : SimpleControlView, IFocusGoalsView
    {
        private IFocusGoalsController _focusGoalsController;

        private Dictionary<GridColumn, int> _colMasks = new Dictionary<GridColumn, int>();
        private int _countryLevel = -1;

        public FocusGoalsView()
        {
            InitializeComponent();

            rDateEdit.NullDate = DateTime.MinValue;
            rDateEdit.NullText = String.Empty;
        }

        private bool IsCountryLevelRow
        {
            get
            {
                int lRowHandle = gridViewGoals.FocusedRowHandle;

                if (gridViewGoals.IsGroupRow(lRowHandle))
                {
                    lRowHandle = gridViewGoals.GetChildRowHandle(lRowHandle, 0);
                }

                return Convert.ToInt32(gridViewGoals.GetRowCellValue(lRowHandle, colM3)) == _countryLevel;
            }
        }

        private int GetTargetId(int rowHandle)
        {
            return Convert.ToInt32(gridViewGoals.GetRowCellValue(rowHandle, colTargetId));
        }

        private bool GetIsManual(int rowHandle)
        {
            return Convert.ToBoolean(gridViewGoals.GetRowCellValue(rowHandle, colIsManual));
        }

        #region Implementation of IView

        public override IViewController GetController()
        {
            return _focusGoalsController;
        }

        public override void SetController(IViewController viewController)
        {
            _focusGoalsController = (IFocusGoalsController)viewController;
            base.SetController(viewController);
        }

        public override object GetDockObject(ISimpleControlView controlView)
        {
            return panelFilter;
        }

        #endregion

        #region Implementation of IFocusGoalsView

        public void AssignProductTree(List<ProductTreeModel> list)
        {
            treeSKU.ClearNodes();
            treeSKU.DataSource = list;

            treeSKU.ExpandAll();
            treeSKU.CollapseAll();
        }

        public void AssignGoalTypes(List<ListItemModel> types)
        {
            cGoalType.Properties.Items.Clear();

            cGoalType.Properties.Items.AddRange(types);

            if (cGoalType.Properties.Items.Count > 0)
            {
                cGoalType.EditValue = cGoalType.Properties.Items[0];
            }
        }

        public void AssignAutoGoalTypes(List<ListItemModel> types)
        {
            cGoal.Properties.Items.Clear();

            cGoal.Properties.Items.AddRange(types);

            if (cGoal.Properties.Items.Count > 0)
            {
                cGoal.EditValue = cGoal.Properties.Items[0];
            }
        }

        public void AssignGoals(List<FocusGoalModel> goals)
        {
            gridViewGoals.FocusedRowChanged -= gridViewGoals_FocusedRowChanged;

            gridControlGoals.DataSource = goals;

            gridViewGoals.FocusedRowChanged += gridViewGoals_FocusedRowChanged;
        }

        public void AssignChannels(List<ChannelModel> channels)
        {
            // channel have already been added
            if (_colMasks.Count > 0)
            {
                return;
            }

            gridViewGoals.BeginUpdate();
            for (int i = 0; i < channels.Count; i++)
            {
                string lName = "colChannel" + channels[i].Name;
                GridColumn col = AddChannelColumn(lName, channels[i].Name);

                gridViewGoals.Columns.Add(col);
                _colMasks.Add(col, channels[i].Mask);
            }
            gridViewGoals.EndUpdate();

            gridViewGoals.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] { colStartDate, colEndDate });
        }

        public void DisplayManualGoalDetails(ManualGoalDetailsModel details)
        {
            if (details != null)
            {
                tDescription.Text = details.Description;
            }
            else
            {
                tDescription.Text = string.Empty;
            }
        }

        public void GetManualGoalDetails(ManualGoalDetailsModel details)
        {
            if (tDescription.Text != details.Description)
            {
                details.Description = tDescription.Text;
                _focusGoalsController.HasUnsavedChanges = true;
            }
        }

        //TODO: replace selection, create new methods
        public void DisplayAutoGoalDetails(AutoGoalDetailsModel details)
        {
            // load goal type
            if (details == null || details.TargetTypeId == 0)
            {
                cGoal.SelectedIndex = 0;
            }
            else
            {
                cGoal.SelectedIndex = details.TargetTypeId - 1;
            }

            // load product tree details
            List<int> list = new List<int>();

            if (!string.IsNullOrEmpty(details.Products))
            {
                list = details.Products.Split(',').Select(int.Parse).ToList();
            }

            foreach (TreeListNode node in treeSKU.Nodes)
            {
                CheckSelectedSKU(node, list);
            }

            cSelectAll.CheckStateChanged -= cSelectAll_CheckedChanged;
            cSelectAll.CheckState = DefineEditState();
            cSelectAll.CheckStateChanged += cSelectAll_CheckedChanged;

            treeSKU.CollapseAll();
        }

        //TODO: replace selection, create new methods
        public void GatAutoGoalDetails(AutoGoalDetailsModel details)
        {
            // get goal type
            if (cGoal.SelectedItem != null)
            {
                int lGoalType = (cGoal.SelectedItem as ListItemModel).Id;

                if (lGoalType != details.TargetTypeId)
                {
                    details.TargetTypeId = lGoalType;
                    _focusGoalsController.HasUnsavedChanges = true;
                }
            }

            // get goal products list
            List<int> list = new List<int>();

            foreach (TreeListNode node in treeSKU.Nodes)
            {
                GetSelectedSKU(node, list, false);
            }

            string lProducts = string.Join(",", (from a in list select a.ToString()).ToArray());

            if (details.Products != lProducts)
            {
                details.Products = lProducts;
            }
        }

        public void OnM3FilterChange(int newM3)
        {
            if (gridViewGoals.IsValidRowHandle(gridViewGoals.FocusedRowHandle) && gridViewGoals.FocusedRowHandle >= 0)
            {
                _focusGoalsController.UpdateGoal(GetTargetId(gridViewGoals.FocusedRowHandle), GetIsManual(gridViewGoals.FocusedRowHandle));
            }

            gridViewGoals.ActiveFilterString = string.Format("M3Id = -1 || M3Id = {0}", newM3);

            if (gridViewGoals.IsValidRowHandle(gridViewGoals.FocusedRowHandle) && gridViewGoals.FocusedRowHandle >= 0)
            {
                if (GetIsManual(gridViewGoals.FocusedRowHandle))
                {
                    ShowManualGooal();
                }
                else
                {
                    ShowAutoGooal();
                }
            }
            else
            {
                HideGooals();
            }

            bAdd.Enabled = GetAddButtonAvailability();
            bDelete.Enabled = GetDeleteButtonAvailability();
        }

        public void SaveActions()
        {
            gridViewGoals.PostEditor();

            if (IsValid)
            {
                if (gridViewGoals.FocusedRowHandle >= 0)
                {
                    _focusGoalsController.UpdateGoal(GetTargetId(gridViewGoals.FocusedRowHandle),
                                                     GetIsManual(gridViewGoals.FocusedRowHandle));
                }

                _focusGoalsController.SaveData();
                ShowMessChangesWereSaved();
            }
        }

        public void RefreshActions()
        {
            gridViewGoals.PostEditor();

            if (gridViewGoals.FocusedRowHandle >= 0)
            {
                _focusGoalsController.UpdateGoal(GetTargetId(gridViewGoals.FocusedRowHandle),
                                                 GetIsManual(gridViewGoals.FocusedRowHandle));
            }


            if (_focusGoalsController.HasUnsavedChanges)
            {
                if (SaveChangesAndProceed())
                {
                    return;
                }
            }

            _focusGoalsController.LoadData(true);
            HideGooals();
        }

        public virtual bool LeaveTabActions()
        {
            gridViewGoals.PostEditor();

            if (gridViewGoals.FocusedRowHandle >= 0)
            {
                _focusGoalsController.UpdateGoal(GetTargetId(gridViewGoals.FocusedRowHandle),
                                                 GetIsManual(gridViewGoals.FocusedRowHandle));
            }

            bool lAllowLeavePage = true;
            bool isValid = true;

            if (_focusGoalsController.HasUnsavedChanges)
            {
                DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                   MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                switch (res)
                {
                    case DialogResult.Yes:
                        {
                            isValid = IsValid;

                            if (isValid)
                            {
                                _focusGoalsController.SaveData();
                                ShowMessChangesWereSaved();
                            }
                        }
                        break;
                    case DialogResult.No:
                        _focusGoalsController.LoadData(true);
                        HideGooals();
                        break;
                    case DialogResult.Cancel:
                        lAllowLeavePage = false;
                        break;
                }
            }

            return lAllowLeavePage && isValid;
        }

        public bool IsValid
        {
            get { return HandleRowValidation(gridViewGoals.FocusedRowHandle); }
        }

        public bool SaveChangesAndProceed()
        {
            bool isValid = true;
            DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                   MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                isValid = TrySave();
            }

            return res == DialogResult.Cancel || !isValid;
        }

        public void PostEditor()
        {
            gridViewGoals.PostEditor();
        }

        public void OnResize()
        {
            splitContainer.SplitterPosition += 2;
            splitContainer.SplitterPosition -= 2;
        }

        #endregion

        #region Event Handling

        private void bAdd_Click(object sender, EventArgs e)
        {
            if (!HandleRowValidation(gridViewGoals.FocusedRowHandle))
            {
                return;
            }

            gridViewGoals.BeforeLeaveRow -= gridViewGoals_BeforeLeaveRow;

            if (((ListItemModel)cGoalType.SelectedItem).Id == (int)TargetTypes.Manual)
            {
                _focusGoalsController.AddNewManualGoal(_focusGoalsController.CurrentM3.Id);
                ShowManualGooal();
            }
            else
            {
                _focusGoalsController.AddNewAutoGoal(_focusGoalsController.CurrentM3.Id);
                ShowAutoGooal();
            }

            gridViewGoals.RefreshData();
            gridViewGoals.MoveLastVisible();

            gridViewGoals.BeforeLeaveRow += gridViewGoals_BeforeLeaveRow;
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            if (gridViewGoals.FocusedRowHandle < 0)
            {
                return;
            }

            DialogResult lDialogResult = XtraMessageBox.Show("Вы действительно хотите удалить цель?", "Фокусные Цели",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (lDialogResult == DialogResult.No)
                return;

            int lGoalId = GetTargetId(gridViewGoals.FocusedRowHandle);

            gridViewGoals.BeforeLeaveRow -= gridViewGoals_BeforeLeaveRow;

            _focusGoalsController.DeleteGoal(lGoalId);

            gridViewGoals.RefreshData();
            gridViewGoals.MoveLastVisible();

            gridViewGoals.BeforeLeaveRow += gridViewGoals_BeforeLeaveRow;
        }

        private void gridViewGoals_CustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;

            if (info == null)
            {
                return;
            }

            if (info.Column == colM3)
            {
                int lRowHandle = gridViewGoals.GetChildRowHandle(e.RowHandle, 0);
                object lM3Value = gridViewGoals.GetRowCellValue(lRowHandle, info.Column);

                if (Convert.ToInt32(lM3Value) == _countryLevel)
                {
                    info.GroupText = "По стране:";
                }
                else
                {
                    info.GroupText = _focusGoalsController.CurrentM3.Name + ":";

                }
            }
        }

        private void gridViewGoals_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;

            object lM3Value = gridViewGoals.GetRowCellValue(e.RowHandle, colM3);
            bool isCountryRow = Convert.ToInt32(lM3Value) == _countryLevel;

            bool canEditCurrentRow = _focusGoalsController.IsCountryLevel && isCountryRow && RoleManager.IsInRole(MMRoleEnum.FocusGoalM5) ||
                       !_focusGoalsController.IsCountryLevel && !isCountryRow && RoleManager.IsInRole(MMRoleEnum.FocusGoalM3);

            if (!canEditCurrentRow)
            {
                e.Appearance.BackColor = Color.LightGray;
                e.Appearance.BackColor2 = Color.LightGray;

                return;
            }

            GridViewInfo vi = gridViewGoals.GetViewInfo() as GridViewInfo;

            if (e.RowHandle % 2 == 0)
            {
                e.Appearance.Assign(vi.PaintAppearance.OddRow);
            }
        }

        private void gridViewGoals_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = !CanEditCurrentRow;
        }

        private void gridViewGoals_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            int h = gridViewGoals.GetRowHandle(e.ListSourceRowIndex);
            if (e.Column == colFocus)
            {
                e.Value = gridViewGoals.GetRowHandle(e.ListSourceRowIndex) + 1;
                //e.Value = e.RowHandle + 1;
            }

            if (_colMasks.ContainsKey(e.Column))
            {
                int lGeneralMask = (e.Row as FocusGoalModel).ChannelMask;
                int lColumnMask = Convert.ToInt32(_colMasks[e.Column]);

                if (e.IsGetData)
                {
                    e.Value = (lGeneralMask & lColumnMask) > 0;
                }
                else
                {
                    int lCurrValue = Convert.ToBoolean(e.Value) ? 1 : 0;
                    e.Value = lGeneralMask & (~lColumnMask) | (lColumnMask * lCurrValue);
                    gridViewGoals.SetFocusedRowCellValue(colChannelMask, e.Value);
                }
            }
        }

        private void gridViewGoals_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            // set delete button access
            if (e.FocusedRowHandle < 0)
            {
                bDelete.Enabled = false;
                HideGooals();
            }
            else
            {
                bDelete.Enabled = GetDeleteButtonAvailability();
            }

            // download the previous goal
            if (e.PrevFocusedRowHandle >= 0 && gridViewGoals.IsValidRowHandle(e.PrevFocusedRowHandle) && (e.FocusedRowHandle >= 0))
            {
                _focusGoalsController.UpdateGoal(GetTargetId(e.PrevFocusedRowHandle), GetIsManual(e.PrevFocusedRowHandle));
            }

            // upload the current goal
            if (e.FocusedRowHandle >= 0 && gridViewGoals.IsValidRowHandle(e.FocusedRowHandle))
            {
                _focusGoalsController.LoadGoal(GetTargetId(e.FocusedRowHandle), GetIsManual(e.FocusedRowHandle));

                if (GetIsManual(e.FocusedRowHandle))
                {
                    ShowManualGooal();
                }
                else
                {
                    ShowAutoGooal();
                }
            }
        }

        private void gridViewGoals_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            //Suppress displaying the error message box
            e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void gridViewGoals_BeforeLeaveRow(object sender, RowAllowEventArgs e)
        {
            if (e.RowHandle < 0 || !gridViewGoals.IsValidRowHandle(e.RowHandle))
            {
                return;
            }

            e.Allow = HandleRowValidation(e.RowHandle);
        }

        private bool HandleRowValidation(int rowHandle)
        {
            if (rowHandle < 0)
            {
                return true;
            }

            bool lAllow;

            int lGeneralMask = Convert.ToInt32(gridViewGoals.GetRowCellValue(rowHandle, colChannelMask));
            lAllow = lGeneralMask > 0;

            if (!lAllow)
            {
                XtraMessageBox.Show("Пожалуйста выберите канал цели");
                return false;
            }

            lAllow = CheckGoalDetails(rowHandle);

            if (!lAllow)
            {
                XtraMessageBox.Show(GetIsManual(rowHandle) ? "Пожалуйста заполните описание цели" : "Пожалуйста выберите продукцию для цели");
            }

            return lAllow;
        }

        private void treeSKU_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            _focusGoalsController.HasUnsavedChanges = true;

            if (e.Node.HasChildren)
            {
                treeSKU.BeginUpdate();
                CheckChildren(e.Node, e.Node.CheckState);
                treeSKU.EndUpdate();
            }

            if (e.Node.ParentNode != null)
            {
                CheckParents(e.Node);
            }

            cSelectAll.CheckStateChanged -= cSelectAll_CheckedChanged;
            cSelectAll.CheckState = DefineEditState();
            cSelectAll.CheckStateChanged += cSelectAll_CheckedChanged;
        }

        private void treeSKU_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            if (e.PrevState == CheckState.Unchecked)
            {
                e.State = CheckState.Checked;
            }

            if (e.PrevState == CheckState.Checked)
            {
                e.State = CheckState.Unchecked;
            }
        }

        private void cSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            _focusGoalsController.HasUnsavedChanges = true;

            if (cSelectAll.CheckState == CheckState.Indeterminate)
            {
                cSelectAll.CheckStateChanged -= cSelectAll_CheckedChanged;
                cSelectAll.CheckState = CheckState.Unchecked;
                cSelectAll.CheckStateChanged += cSelectAll_CheckedChanged;
            }

            foreach (TreeListNode node in treeSKU.Nodes)
            {
                CheckChildren(node, cSelectAll.CheckState);
            }
        }

        private void gridViewGoals_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (gridViewGoals.FocusedColumn == colTargetName)
            {
                string lName = Convert.ToString(e.Value);

                if (string.IsNullOrEmpty(lName))
                {
                    e.Valid = false;
                    e.ErrorText = "Название не может быть пустым";
                }
            }
        }

        private void gridViewGoals_ShownEditor(object sender, EventArgs e)
        {
            if (gridViewGoals.FocusedColumn == colStartDate)
            {
                DateEdit editor = (DateEdit)gridViewGoals.ActiveEditor;

                editor.Properties.MaxValue = Convert.ToDateTime(gridViewGoals.GetFocusedRowCellValue(colEndDate)).Date;
            }

            if (gridViewGoals.FocusedColumn == colEndDate)
            {
                DateEdit editor = (DateEdit)gridViewGoals.ActiveEditor;

                editor.Properties.MinValue = Convert.ToDateTime(gridViewGoals.GetFocusedRowCellValue(colStartDate)).Date;
            }
        }

        #endregion

        #region Tree Helpers

        private void GetSelectedSKU(TreeListNode node, List<int> checkedNodes, bool selectAllChildren)
        {
            if (node.Level == 2 && (selectAllChildren || node.Checked))
            {
                checkedNodes.Add((int)node[colItemId.FieldName]);
            }

            foreach (TreeListNode child in node.Nodes)
            {
                GetSelectedSKU(child, checkedNodes, selectAllChildren || node.Checked);
            }
        }

        private void CheckSelectedSKU(TreeListNode node, List<int> checkedNodes)
        {
            if (checkedNodes == null)
                return;

            if (node.Level == 2 && checkedNodes.Contains((int)node[colItemId.FieldName]))
            {
                node.Checked = true;
            }
            else
            {
                node.Checked = false;
            }

            if (node.ParentNode != null)
            {
                CheckParents(node);
            }

            foreach (TreeListNode child in node.Nodes)
            {
                CheckSelectedSKU(child, checkedNodes);
            }
        }

        private void CheckChildren(TreeListNode node, CheckState state)
        {
            node.CheckState = state;

            foreach (TreeListNode child in node.Nodes)
            {
                CheckChildren(child, state);
            }
        }

        private void CheckParents(TreeListNode node)
        {
            TreeListNode lParentNode = node.ParentNode;

            if (lParentNode == null)
                return;

            if (lParentNode.CheckState == node.CheckState)
                return;

            lParentNode.CheckState = DefineNodeState(lParentNode);

            CheckParents(lParentNode);
        }

        private CheckState DefineNodeState(TreeListNode node)
        {
            CheckState lCheckState = CheckState.Indeterminate;

            int checkedCount = 0;
            int uncheckedCount = 0;
            int visibleCount = 0;

            foreach (TreeListNode child in node.Nodes)
            {
                if (child.Visible)
                {
                    visibleCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Checked)
                {
                    checkedCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Unchecked)
                {
                    uncheckedCount++;
                }
            }

            if (checkedCount == 0 && uncheckedCount == visibleCount)
            {
                lCheckState = CheckState.Unchecked;
            }
            else
            {
                if (uncheckedCount == 0 && checkedCount == visibleCount)
                {
                    lCheckState = CheckState.Checked;
                }
            }

            return lCheckState;

        }

        private CheckState DefineEditState()
        {
            CheckState lCheckState = CheckState.Indeterminate;

            int checkedCount = 0;
            int uncheckedCount = 0;
            int visibleCount = 0;

            foreach (TreeListNode child in treeSKU.Nodes)
            {
                if (child.Visible)
                {
                    visibleCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Checked)
                {
                    checkedCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Unchecked)
                {
                    uncheckedCount++;
                }
            }

            if (checkedCount == 0 && uncheckedCount == visibleCount)
            {
                lCheckState = CheckState.Unchecked;
            }
            else
            {
                if (uncheckedCount == 0 && checkedCount == visibleCount)
                {
                    lCheckState = CheckState.Checked;
                }
            }

            return lCheckState;
        }

        #endregion

        private GridColumn AddChannelColumn(string name, string caption)
        {
            GridColumn col = new GridColumn();

            col.UnboundType = UnboundColumnType.Boolean;
            col.Name = name;
            col.FieldName = name;
            col.Visible = true;
            col.Width = 85;
            col.MinWidth = 85;
            col.MaxWidth = 85;
            col.OptionsColumn.AllowEdit = true;
            col.Caption = caption;
            col.ColumnEdit = rCheckEdit;
            col.OptionsFilter.AllowFilter = false;

            return col;
        }

        private void HideGooals()
        {
            panelAutoGoal.Visible = false;
            panelManualGoal.Visible = false;
        }

        private void ShowManualGooal()
        {
            panelManualGoal.Enabled = CanEditCurrentRow;

            panelAutoGoal.Visible = false;

            panelManualGoal.Dock = DockStyle.Fill;
            panelManualGoal.Visible = true;
        }

        private void ShowAutoGooal()
        {
            panelAutoGoal.Enabled = CanEditCurrentRow;

            panelManualGoal.Visible = false;

            panelAutoGoal.Dock = DockStyle.Fill;
            panelAutoGoal.Visible = true;
        }

        private bool CheckGoalDetails(int rowHandle)
        {
            if (rowHandle < 0)
            {
                return true;
            }

            bool lAllow = true;

            if (GetIsManual(rowHandle))
            {
                lAllow = !string.IsNullOrEmpty(tDescription.Text);
            }
            else
            {
                lAllow = cSelectAll.CheckState != CheckState.Unchecked && cGoal.EditValue != null;
            }

            return lAllow;
        }

        public bool TrySave()
        {
            gridViewGoals.PostEditor();

            bool isValid = IsValid;

            if (isValid)
            {
                if (gridViewGoals.FocusedRowHandle >= 0)
                {
                    _focusGoalsController.UpdateGoal(GetTargetId(gridViewGoals.FocusedRowHandle), GetIsManual(gridViewGoals.FocusedRowHandle));
                }

                _focusGoalsController.SaveData();
                ShowMessChangesWereSaved();
            }

            return isValid;
        }

        private void gridViewGoals_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            _focusGoalsController.HasUnsavedChanges = true;
        }

        private bool GetAddButtonAvailability()
        {
            return HasEditPermissionsOnCurM3;
        }

        private bool GetDeleteButtonAvailability()
        {
            bool isButtonAccessibleByPermissions = CanEditCurrentRow;
            bool isGridViewNonEmpty = gridViewGoals.RowCount > 0;

            return isButtonAccessibleByPermissions && isGridViewNonEmpty;
        }

        private bool HasEditPermissionsOnCurM3
        {
            get
            {
                return _focusGoalsController.IsCountryLevel && RoleManager.IsInRole(MMRoleEnum.FocusGoalM5) ||
                       !_focusGoalsController.IsCountryLevel && RoleManager.IsInRole(MMRoleEnum.FocusGoalM3);
            }
        }

        private bool CanEditCurrentRow
        {
            get
            {
                return _focusGoalsController.IsCountryLevel && IsCountryLevelRow && RoleManager.IsInRole(MMRoleEnum.FocusGoalM5) ||
                       !_focusGoalsController.IsCountryLevel && !IsCountryLevelRow && RoleManager.IsInRole(MMRoleEnum.FocusGoalM3);
            }
        }

        private void ShowMessChangesWereSaved()
        {
            XtraMessageBox.Show("Данные успешно сохранены!", "Фокусные цели", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
