using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View {
    public interface IFilterView : ISimpleControlView {
        bool ShowListType { get; set; }
        bool ShowCheckM1 { get; set; }
        bool ShowPlanDate { get; set; }
        bool ShowDatePeriod { get; set; }

        ListTypeEnum ListType { get; set; }

        void SetCheckShowM1Value(bool showM1);
        
        event ListItemChangeEventHandler SKUListTypeItemChanged;
        event ListItemChangingEventHandler SKUListTypeItemChanging;
        event DateChangeEventHandler DateChanged;
        event DateChangingEventHandler DateChanging;
        event ListItemChangeEventHandler CountryChanged;
        event ListItemChangingEventHandler CountryChanging;
        event ListItemChangeEventHandler M3Changed;
        event ListItemChangingEventHandler M3Changing;
        event IntervalChangeEventHandler IntervalChanged;
        event IntervalChangingEventHandler IntervalChanging;
        event ShowM1ChangeEventHandler ShowM1Changed;
        
        void OnSKUListTypeItemChanged(object sender, ListItemChangeEventArgs args);
        void OnSKUListTypeItemChanging(object sender, ListItemChangingEventArgs args);
        void OnDateChanged(object sender, DateChangeEventArgs args);
        void OnDateChanging(object sender, DateChangingEventArgs args);
        void OnIntervalChanged(object sender, IntervalChangeEventArgs args);
        void OnIntervalChanging(object sender, IntervalChangingEventArgs args);
        void OnCountryChanged(object sender, ListItemChangeEventArgs args);
        void OnCountryChanging(object sender, ListItemChangingEventArgs args);
        void OnM3Changed(object sender, ListItemChangeEventArgs args);
        void OnM3Changing(object sender, ListItemChangingEventArgs args);
        void OnShowM1Changed(object sender, ShowM1ChangeEventArgs args);
    }
}