﻿using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View
{
    [ToolboxItem(true)]
    public partial class NetworksPlanView : BasePlanView, INetworksPlanView
    {
        private INetworksPlanController _networksPlanController;

        private GridBand bandMonthPlan;
        private GridBand bandMix;
        private GridBand bandAvgSku;
        private GridBand bandPoceBase;
        /*private GridBand bandPoceAdvanced;
        private GridBand bandPoceLeader;*/    // ---- Bushido_Gap_B2 101 2015.09.08

        public NetworksPlanView()
        {
            InitializeComponent();
        }

        #region Implementation of IView

        public override IViewController GetController()
        {
            return _networksPlanController;
        }

        public override void SetController(IViewController viewController)
        {
            _networksPlanController = (INetworksPlanController)viewController;
            base.SetController(viewController);
        }

        #endregion

        public override void ConstructView(List<BindedColumnModel> columns)
        {
            ResetView();
            base.ConstructView(columns);

            foreach (var col in _columns)
            {
                GridColumn addedColumn = AddColumn(col.Name + col.Id.ToString(), col.Name, col.Id.ToString(), 120, col.BandName);
                addedColumn.ColumnEdit = rTextVolume;

                if (col.BandName.Contains("СКЮ"))
                {
                    addedColumn.ColumnEdit = rTextTwoDigits;
                }

                if (col.BandName.Contains("%"))
                {
                    addedColumn.ColumnEdit = rTextPercents;
                }
            }
        }

        protected override bool CanBeBindedToSkip(GridColumn column)
        {
            // for networks tab manual binding should be disabled
            return false;
        }

        protected override bool IsColumnBindedToSkip(GridColumn column)
        {
            // we treat all plan volume columns as autobinded and forbid binding for other bands
            BandedGridColumn lBandedGridColumn = column as BandedGridColumn;

            if (lBandedGridColumn != null && lBandedGridColumn.OwnerBand == bandMonthPlan)
            {
                return true;
            }

            return false;
        }

        protected virtual bool IsAutoBindedToSkip(GridColumn column)
        {
            // we treat all plan volume columns as autobinded and forbid binding for other bands
            return IsColumnBindedToSkip(column);
        }

        private void InitializeComponent()
        {
            this.bandMonthPlan = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandMix = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandAvgSku = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandPoceBase = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            /*this.bandPoceAdvanced = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandPoceLeader = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();*/    // ---- Bushido_Gap_B2 101 2015.09.08

            this.gridViewPlans.BeginUpdate();
            this.gridViewPlans.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandMonthPlan,
            this.bandMix,
            this.bandAvgSku,
            this.bandPoceBase,
            /*this.bandPoceAdvanced,
            this.bandPoceLeader*/});  // ---- Bushido_Gap_B2 101 2015.09.08
            this.gridViewPlans.ColumnPanelRowHeight = 35;
            // 
            // bandMonthPlan
            // 
            this.bandMonthPlan.Caption = "План на месяц, дал";
            this.bandMonthPlan.Name = "bandMonthPlan";
            this.bandMonthPlan.Width = 150;
            // 
            // bandMix
            // 
            this.bandMix.Caption = "Микс План на месяц, %";
            this.bandMix.Name = "bandMix";
            this.bandMix.Width = 150;
            // 
            // bandAvgSku
            // 
            this.bandAvgSku.Caption = "Ср. СКЮ План на месяц";
            this.bandAvgSku.Name = "bandAvgSku";
            this.bandAvgSku.Width = 150;
            // 
            // bandPoceBase
            // 
            this.bandPoceBase.Caption = "POCE"/* Базовые стандарты, План на месяц, %"*/;         // ---- Bushido_Gap_B2 101 2015.09.08
            this.bandPoceBase.Name = "bandPoceBase";
            this.bandPoceBase.Width = 150;
            //// 
            //// bandPoceAdvanced
            //// 
            //this.bandPoceAdvanced.Caption = "POCE Продвинутые стандарты, План на месяц, %";   // ---- Bushido_Gap_B2 101 2015.09.08
            //this.bandPoceAdvanced.Name = "bandPoceAdvanced";
            //this.bandPoceAdvanced.Width = 150;
            //// 
            //// bandPoceLeader
            //// 
            //this.bandPoceLeader.Caption = "POCE Лидерские стандарты, План на месяц, %";      // ---- Bushido_Gap_B2 101 2015.09.08
            //this.bandPoceLeader.Name = "bandPoceLeader";
            //this.bandPoceLeader.Width = 150; 

            this.gridViewPlans.EndUpdate();
        }
    }
}
