﻿using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraTreeList.Columns;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.UserControl;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View {
    public class SKUListView : BaseSKUConfigView, ISKUListView {
        private ISKUListController _skuListController;
        private readonly GridColumn _colSkuName;
        private readonly TreeListColumn _columnProductName;
        private readonly GridColumn _colIsInPackage;

        public SKUListView() {
            SkuTree.CheckNodes = CheckNodesEnum.LeafOnly;
            if (null == _colSkuName)
                _colSkuName = SkuGridView.Columns.ColumnByFieldName("SKUGroupName");
            //_colSkuName.ColumnEdit.ReadOnly = true;
            if (null == _columnProductName)
                _columnProductName = SkuTree.Columns.ColumnByFieldName("Name");

            //add column "Входит в пакетку"
            SkuGridView.BeginUpdate();
            try
            {
                string lFieldName = "colIsInPackage";
                _colIsInPackage = SkuGridView.Columns.AddField(lFieldName);
                _colIsInPackage.FieldName = "IsInPackage";
                _colIsInPackage.Visible = true;
                _colIsInPackage.Width = 85;
                _colIsInPackage.MinWidth = 85;
                _colIsInPackage.MaxWidth = 85;
                _colIsInPackage.OptionsColumn.AllowEdit = true;
                _colIsInPackage.Caption = "Входит в пакетку";
                _colIsInPackage.ColumnEdit = riCheckEdit;
                _colIsInPackage.OptionsFilter.AllowFilter = false;
                _colIsInPackage.AppearanceHeader.TextOptions.WordWrap = WordWrap.Wrap;
                _colIsInPackage.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            }
            finally
            {
                SkuGridView.EndUpdate();
            }

            SkuGridView.ColumnPanelRowHeight = 35;
        }

        #region Implementation of IView

        public override IViewController GetController() {
            return _skuListController;
        }

        public override void SetController(IViewController viewController) {
            _skuListController = (ISKUListController) viewController;
            base.SetController(viewController);
        }

        public override void RefreshData() {
            SkuGridControl.DataSource = _skuListController.SKUListItems;
            SkuTree.DataSource = _skuListController.Products;
            SkuTree.ExpandAll();
            SkuTree.CollapseAll();
            base.RefreshData();
            if (CheckedNodes.Count > 0)
                PrevCheckedNode = CheckedNodes[0];
        }

        public override bool RefreshAction() {
            if (base.RefreshAction())
                _skuListController.LoadData(true);

            return true;
        }

        public override bool LeaveTabActions() {
            SkuGridView.PostEditor();

            bool lAllowLeavePage = true;
            bool isValid = true;

            if (_baseSKUConfigController.HasUnsavedChanges) {
                DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                switch (res) {
                    case DialogResult.Yes:
                        isValid = SaveData();
                        break;
                    case DialogResult.No:
                        _skuListController.LoadData(true);
                        break;
                    case DialogResult.Cancel:
                        lAllowLeavePage = false;
                        break;
                }
            }

            return lAllowLeavePage && isValid;
        }

        #endregion

        public override void OnListTypeChanged(int newId)
        {
            base.OnListTypeChanged(newId);
            _colIsInPackage.Visible = newId == 1;//DistrPush (from DataProvider)
        }

        public override void SkuTreeAfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e) {
            base.SkuTreeAfterCheckNode(sender, e);

            SkuTree.BeginUpdate();
            try {
                if (PrevCheckedNode != null) {
                    PrevCheckedNode.Checked = false;
                    TreeUtils.UpdateParentsCheckState(PrevCheckedNode);
                    if (CheckedNodes.Contains(PrevCheckedNode))
                        CheckedNodes.Remove(PrevCheckedNode);
                }
                PrevCheckedNode = null;
                if (e.Node.CheckState == CheckState.Checked) {
                    SkuGridView.SetFocusedRowCellValue(_colSkuName, e.Node.GetDisplayText(_columnProductName));
                    PrevCheckedNode = e.Node;
                }
            }
            finally {
                SkuTree.EndUpdate();
            }
        }

        public override bool CanModifyCountryLevel {
            get {
                return _skuListController.IsFilterCountryLevel &&
                       ((RoleManager.IsInRole(MMRoleEnum.DistrPushM5) && _skuListController.FilterController.CurrentListTypeItem.Id == 1) ||
                        (RoleManager.IsInRole(MMRoleEnum.FocusSkuM5) && _skuListController.FilterController.CurrentListTypeItem.Id == 2));
            }
        }

        public override bool CanModifyM3Level {
            get {
                return !_skuListController.IsFilterCountryLevel &&
                       ((RoleManager.IsInRole(MMRoleEnum.DistrPushM3) && _skuListController.FilterController.CurrentListTypeItem.Id == 1) ||
                        (RoleManager.IsInRole(MMRoleEnum.FocusSkuM3) && _skuListController.FilterController.CurrentListTypeItem.Id == 2));
            }
        }
    }
}