﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.Forms;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.UserControl;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View {
    [ToolboxItem(false)]
    public partial class BaseSKUConfigView : SimpleControlView, IBaseSKUConfigView {
        protected IBaseSKUConfigController _baseSKUConfigController;
        private readonly Dictionary<GridColumn, int> _colMasks = new Dictionary<GridColumn, int>();
        private bool _internalFocusedRowChanging;
        private int _rowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle;
        private bool _isSkuTreeModified;
        private readonly List<TreeListNode> _checkedNodes = new List<TreeListNode>();
        private TreeListNode _prevCheckedNode;

        public BaseSKUConfigView() {
            InitializeComponent();
            _colMasks = CreateHelper.CreateChannelColumns(gridViewSKU);
            foreach (KeyValuePair<GridColumn, int> lColMask in _colMasks)
                lColMask.Key.ColumnEdit = riCheckEdit;
            AttachSkuTreeEvents(true);
        }

        private static string GetFilterString(int listTypeId, int m3Id) {
            string lFilterStr = "ListType = {0} and ";
            if (m3Id == -1)
                lFilterStr += "M3Id = {1}";
            else
                lFilterStr += "(M3Id = -1 or M3Id = {1})";
            return string.Format(lFilterStr, listTypeId, m3Id);
        }

        #region Implementation of IBaseSKUConfigView

        public bool IsValid {
            get { return ValidateItem(gridViewSKU.FocusedRowHandle); }
        }

        public void OnDateChanged(DateTime newDate) {
            _baseSKUConfigController.LoadData(true);
            AllowEdit();
        }

        public void OnCountryChanged(int newId) {
            _baseSKUConfigController.LoadData(true);
            AllowEdit();
        }

        public void OnM3Changed(int newId) {
            int lListTypeId = _baseSKUConfigController.FilterController.CurrentListTypeItem.Id;
            gridViewSKU.ActiveFilterString = GetFilterString(lListTypeId, newId);
            AllowEdit();
        }

        public void OnDateChanging(DateChangingEventArgs args) {
            args.IsCanceled = !IsValid;
            if (args.IsCanceled)
                return;
            args.IsCanceled = !SaveChangesWithConfirm();
        }

        public void OnCountryChanging(ListItemChangingEventArgs args) {
            args.IsCanceled = !IsValid;
            if (args.IsCanceled)
                return;
            args.IsCanceled = !SaveChangesWithConfirm();
        }

        public virtual void OnListTypeChanged(int newId) {
            int lM3Id = _baseSKUConfigController.FilterController.CurrentM3.Id;
            gridViewSKU.ActiveFilterString = GetFilterString(newId, lM3Id);
            AllowEdit();
        }

        public virtual void SaveAction() {
            SaveData();
        }

        public virtual bool RefreshAction() {
            return SaveChangesWithConfirm();
        }

        public virtual bool LeaveTabActions() {
            return true;
        }

        public void PostEditor()
        {
            gridViewSKU.PostEditor();
        }

        public void OnResize()
        {
            splitContainer.SplitterPosition += 2;
            splitContainer.SplitterPosition -= 2;
        }

        #endregion

        public GridControl SkuGridControl {
            get { return gridControlSKU; }
        }

        public GridView SkuGridView {
            get { return gridViewSKU; }
        }

        public CheckedTree SkuTree {
            get { return treeSKU; }
        }

        public List<TreeListNode> CheckedNodes {
            get { return _checkedNodes; }
        }

        public TreeListNode PrevCheckedNode {
            get { return _prevCheckedNode; }
            set { _prevCheckedNode = value; }
        }

        #region Implementation of IView

        public override IViewController GetController() {
            return _baseSKUConfigController;
        }

        public override void SetController(IViewController viewController) {
            _baseSKUConfigController = (IBaseSKUConfigController) viewController;
        }

        public override object GetDockObject(ISimpleControlView controlView) {
            return panelFilter;
        }

        public override void RefreshData() {
            _isSkuTreeModified = false;
            int lListTypeId = _baseSKUConfigController.FilterController.CurrentListTypeItem.Id;
            int lM3Id = _baseSKUConfigController.FilterController.CurrentM3.Id;
            gridViewSKU.ActiveFilterString = GetFilterString(lListTypeId, lM3Id);
            AllowEdit();
        }

        #endregion

        private void AllowEdit()
        {
            bool lHasSets = gridViewSKU.RowCount > 0;
            bool lEditEnabledCurrRow = IsRowCountryLevel && CanModifyCountryLevel || !IsRowCountryLevel && CanModifyM3Level;
            bool lEnableItemAction = _baseSKUConfigController.IsCurrentSkuGroup();
            bool lUserCanEditCurrM3 = _baseSKUConfigController.IsFilterCountryLevel && CanModifyCountryLevel
                || !_baseSKUConfigController.IsFilterCountryLevel && CanModifyM3Level;

            treeSKU.OptionsBehavior.Editable = lEnableItemAction && lEditEnabledCurrRow;

            btnAdd.Enabled = lUserCanEditCurrM3;
            btnDelete.Enabled = lEnableItemAction && lEditEnabledCurrRow;
            btnUp.Enabled = lEnableItemAction && lEditEnabledCurrRow && lHasSets;
            btnDown.Enabled = lEnableItemAction && lEditEnabledCurrRow && lHasSets;
            btnCopy.Enabled = lUserCanEditCurrM3 && lHasSets;
        }

        #region Grid events

        private void gridViewSKU_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
           if (_colMasks.ContainsKey(e.Column))
            {
                int lGeneralMask = e.Row is SKUListItemModel ? (e.Row as SKUListItemModel).ChannelMask : (e.Row as ProductGroupModel).ChannelMask;
                int lColumnMask = Convert.ToInt32(_colMasks[e.Column]);

                if (e.IsGetData)
                    e.Value = (lGeneralMask & lColumnMask) > 0;
                else
                {
                    int lCurrValue = Convert.ToBoolean(e.Value) ? 1 : 0;
                    e.Value = lGeneralMask & (~lColumnMask) | (lColumnMask * lCurrValue);
                    gridViewSKU.SetFocusedRowCellValue(colChannelMask, e.Value);
                }
            }
        }

        private void gridViewSKU_CustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e) {
            GridGroupRowInfo lInfo = e.Info as GridGroupRowInfo;
            if (lInfo == null)
                return;

            if (lInfo.Column == colM3Id) {
                int lRowHandle = gridViewSKU.GetChildRowHandle(e.RowHandle, 0);
                int lM3Id = Convert.ToInt32(gridViewSKU.GetRowCellValue(lRowHandle, lInfo.Column));
                if (lM3Id == -1)
                    lInfo.GroupText = "По стране:";
                else
                    lInfo.GroupText = _baseSKUConfigController.FilterController.CurrentM3.Name + ":";
            }
        }

        private void gridViewSKU_RowStyle(object sender, RowStyleEventArgs e) {
            if (e.RowHandle < 0)
                return;
            object lM3Value = gridViewSKU.GetRowCellValue(e.RowHandle, colM3Id);
            if (Convert.ToInt32(lM3Value) == -1) {
                if (!CanModifyCountryLevel) {
                    e.Appearance.BackColor = Color.Silver;
                    e.Appearance.BackColor2 = Color.LightGray;
                    return;
                }
            }
            else if (!CanModifyM3Level) {
                e.Appearance.BackColor = Color.Silver;
                e.Appearance.BackColor2 = Color.LightGray;
                return;
            }

            GridViewInfo lGridViewInfo = gridViewSKU.GetViewInfo() as GridViewInfo;
            if (null == lGridViewInfo)
                return;
            if (e.RowHandle % 2 == 0)
                e.Appearance.Assign(lGridViewInfo.PaintAppearance.OddRow);
            else
                e.Appearance.Assign(lGridViewInfo.PaintAppearance.EvenRow);
        }

        private void gridViewSKU_ShowingEditor(object sender, CancelEventArgs e) {
            if (SkuTree.CheckNodes == CheckNodesEnum.LeafOnly && (gridViewSKU.FocusedColumn == colSKUGroupName)) {
                e.Cancel = true;
                return;
            }
            e.Cancel = IsRowCountryLevel && !CanModifyCountryLevel;
            if (e.Cancel)
                return;
            e.Cancel = !IsRowCountryLevel && !CanModifyM3Level;
        }

        private void gridViewSKU_BeforeLeaveRow(object sender, RowAllowEventArgs e) {
            if (e.RowHandle < 0 || gridViewSKU.IsGroupRow(e.RowHandle))
                return;
            _baseSKUConfigController.CurrentIndex = gridViewSKU.GetDataSourceRowIndex(e.RowHandle);
            e.Allow = ValidateItem(e.RowHandle);
        }

        private bool ValidateItem(int rowHandle) {
            gridViewSKU.PostEditor();
            gridViewSKU.UpdateCurrentRow();
            if (rowHandle < 0 || !gridViewSKU.IsValidRowHandle(rowHandle))
                return true;
            if (!_baseSKUConfigController.IsCurrentSkuGroup())
                return true;
            bool lResult = ValidateName(rowHandle);
            if (lResult)
                lResult = ValidateChannel(rowHandle);
            if (lResult)
                lResult = ValidateSkuTree();
            return lResult;
        }

        private bool ValidateName(int rowHandle) {
            string lSkuGroupName = Convert.ToString(gridViewSKU.GetRowCellValue(rowHandle, colSKUGroupName));
            if (string.IsNullOrEmpty(lSkuGroupName)) {
                XtraMessageBox.Show("Название не может быть пустым!", "Настройка Продуктов",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private bool ValidateChannel(int rowHandle) {
            if (_colMasks.Count == 0)
                return true;
            int lChannelMask = Convert.ToInt32(gridViewSKU.GetRowCellValue(rowHandle, colChannelMask));
            if (lChannelMask == 0) {
                XtraMessageBox.Show("Выберите хотя бы один канал!", "Настройка Продуктов",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private bool ValidateSkuTree() {
            if (!_baseSKUConfigController.IsCurrentSkuGroup())
                return true;
            if (_isSkuTreeModified) {
                string lStrIds = GetCheckedIds();
                if (lStrIds == "") {
                    XtraMessageBox.Show("Выберите хотя бы один продукт в дереве.", "Настройка Продуктов",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                _baseSKUConfigController.SetSelectedIdList(lStrIds);
            }
            return true;
        }

        private void gridViewSKU_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) {
            if (_baseSKUConfigController.IsAddingMode)
                return;
            if (_internalFocusedRowChanging)
                return;

            //handle modified row
            _baseSKUConfigController.CurrentIndex = gridViewSKU.IsGroupRow(e.PrevFocusedRowHandle)
                                                        ? -1
                                                        : gridViewSKU.GetDataSourceRowIndex(e.PrevFocusedRowHandle);
            if (!ValidateItem(e.PrevFocusedRowHandle))
                return;
            _isSkuTreeModified = false;

            //handle change position
            _baseSKUConfigController.CurrentIndex = gridViewSKU.IsGroupRow(e.FocusedRowHandle)
                                                        ? -1
                                                        : gridViewSKU.GetDataSourceRowIndex(e.FocusedRowHandle);
            AllowEdit();
            RefreshSkuTreeState();
        }

        private void timer_Tick(object sender, EventArgs e) {
            _internalFocusedRowChanging = true;
            try {
                timer.Stop();
                gridViewSKU.FocusedRowHandle = _rowHandle;
                _rowHandle = GridControl.InvalidRowHandle;
            }
            finally {
                _internalFocusedRowChanging = false;
            }
        }

        private void gridViewSKU_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e) {
            _baseSKUConfigController.HasUnsavedChanges = true;
        }

        #endregion

        #region SKU Tree events and methods

        public virtual void SkuTreeBeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e) {
            e.CanCheck = treeSKU.OptionsBehavior.Editable;
            if (e.PrevState == CheckState.Unchecked)
                e.State = CheckState.Checked;
            if (e.PrevState == CheckState.Checked)
                e.State = CheckState.Unchecked;
        }

        public virtual void SkuTreeAfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e) {
            _isSkuTreeModified = true;
            _baseSKUConfigController.HasUnsavedChanges = true;

            treeSKU.BeginUpdate();
            try {
                FillCheckedNodes(e.Node);
                TreeUtils.UpdateParentsCheckState(e.Node);
            }
            finally {
                treeSKU.EndUpdate();
            }
        }

        private void FillCheckedNodes(TreeListNode node) {
            if (node.Checked) {
                if (!CheckedNodes.Contains(node))
                    CheckedNodes.Add(node);
            }
            else if (CheckedNodes.Contains(node))
                CheckedNodes.Remove(node);
        }

        public void AttachSkuTreeEvents(bool attach) {
            if (attach) {
                treeSKU.AfterCheckNode += SkuTreeAfterCheckNode;
                treeSKU.BeforeCheckNode += SkuTreeBeforeCheckNode;
            }
            else {
                treeSKU.BeforeCheckNode -= SkuTreeBeforeCheckNode;
                treeSKU.AfterCheckNode -= SkuTreeAfterCheckNode;
            }
        }

        private string GetCheckedIds() {
            treeSKU.PostEditor();
            TreeListColumn lColumnId = treeSKU.Columns.ColumnByFieldName("Id");
            GetFinalCheckedNodesOperation lNodesOperation = new GetFinalCheckedNodesOperation(lColumnId,
                SkuTree.CheckNodes == CheckNodesEnum.LeafOnly);
            treeSKU.NodesIterator.DoOperation(lNodesOperation);
            if (lNodesOperation.Count > 0)
                return lNodesOperation.SelectedIdList;
            return string.Empty;
        }

        private void RefreshSkuTreeState() {
            treeSKU.BeginUpdate();
            try {
                TreeListColumn lColumnId = treeSKU.Columns.ColumnByFieldName("Id");
                TreeListColumn lColumnEditable = treeSKU.Columns.ColumnByFieldName("Editable");
                List<int> lIdList;
                if (_baseSKUConfigController.IsCurrentSkuGroup())
                    lIdList = _baseSKUConfigController.GetCurrentIdList();
                else
                    lIdList = new List<int>();
                UpdateNodesCheckStateOperation lOp = new UpdateNodesCheckStateOperation(lColumnId, lIdList);
                treeSKU.NodesIterator.DoOperation(lOp);
                _checkedNodes.Clear();
                _checkedNodes.AddRange(lOp.CheckedNodes);
                PrevCheckedNode = CheckedNodes.Count > 0 ? CheckedNodes[0] : null;
                foreach (TreeListNode lNode in _checkedNodes) {
                    if (lNode.ParentNode != null)
                        TreeUtils.UpdateParentsCheckState(lNode);
                    if (lNode.HasChildren && SkuTree.CheckNodes == CheckNodesEnum.Independent)
                        TreeUtils.OnNodeCheckRecursive(lNode, lNode.Checked, lColumnEditable);
                }
                treeSKU.ExpandAll();
                treeSKU.CollapseAll();
            }
            finally {
                treeSKU.EndUpdate();
            }
            treeSKU.NodesIterator.DoOperation(new ExpandCheckedNodesOperation());
        }

        #endregion

        #region Button handles

        private void btnAdd_Click(object sender, EventArgs e) {
            _baseSKUConfigController.IsAddingMode = true;
            try {
                // handle SKU Tree changes
                _baseSKUConfigController.CurrentIndex = gridViewSKU.GetDataSourceRowIndex(gridViewSKU.FocusedRowHandle);
                if (!ValidateItem(gridViewSKU.FocusedRowHandle))
                    return;
                // add new SKU
                _isSkuTreeModified = true;
                _baseSKUConfigController.AddSkuGroup();
                gridViewSKU.RefreshData();
                gridViewSKU.MoveLastVisible();
                _baseSKUConfigController.CurrentIndex = gridViewSKU.GetDataSourceRowIndex(gridViewSKU.FocusedRowHandle);
                _baseSKUConfigController.HasUnsavedChanges = true;
                AllowEdit();
                RefreshSkuTreeState();
            }
            finally {
                _baseSKUConfigController.IsAddingMode = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            DialogResult lDialogResult = XtraMessageBox.Show("Вы действительно хотите удалить цель?", "Настройка Продуктов",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (lDialogResult == DialogResult.No)
                return;
            _baseSKUConfigController.CurrentIndex = gridViewSKU.GetDataSourceRowIndex(gridViewSKU.FocusedRowHandle);
            int lErrorCode = _baseSKUConfigController.DeleteSkuGroup();
            if (lErrorCode != 0)
                return;
            int lPrevRowHandle = gridViewSKU.FocusedRowHandle;
            gridViewSKU.DeleteRow(gridViewSKU.FocusedRowHandle);
            if (gridViewSKU.FocusedRowHandle == _rowHandle) {
                RefreshSkuTreeState();
            }
            if (lPrevRowHandle == gridViewSKU.FocusedRowHandle)
                gridViewSKU_FocusedRowChanged(gridViewSKU, new FocusedRowChangedEventArgs(_rowHandle, gridViewSKU.FocusedRowHandle));

            _baseSKUConfigController.HasUnsavedChanges = true;
        }

        private void btnDown_Click(object sender, EventArgs e) {
            if (!ValidateItem(gridViewSKU.FocusedRowHandle))
                return;

            _baseSKUConfigController.CurrentIndex = gridViewSKU.IsGroupRow(gridViewSKU.FocusedRowHandle)
                                                        ? -1
                                                        : gridViewSKU.GetDataSourceRowIndex(gridViewSKU.FocusedRowHandle);
            if (_baseSKUConfigController.CurrentIndex < 0)
                return;

            // if last position in group do nothing
            int lNextRowHandle =
                gridViewSKU.GetVisibleRowHandle(gridViewSKU.GetNextVisibleRow(gridViewSKU.GetVisibleIndex(gridViewSKU.FocusedRowHandle)));
            if (lNextRowHandle == gridViewSKU.FocusedRowHandle || gridViewSKU.IsGroupRow(lNextRowHandle)
                || !gridViewSKU.IsValidRowHandle(lNextRowHandle))
                return;

            int lNextIndex = gridViewSKU.GetDataSourceRowIndex(lNextRowHandle);
            _baseSKUConfigController.SwapItems(_baseSKUConfigController.CurrentIndex, lNextIndex);

            gridViewSKU.BeforeLeaveRow -= gridViewSKU_BeforeLeaveRow;
            gridViewSKU.FocusedRowChanged -= gridViewSKU_FocusedRowChanged;

            gridViewSKU.FocusedRowHandle = gridViewSKU.GetRowHandle(lNextIndex);

            gridViewSKU.BeforeLeaveRow += gridViewSKU_BeforeLeaveRow;
            gridViewSKU.FocusedRowChanged += gridViewSKU_FocusedRowChanged;

            _baseSKUConfigController.HasUnsavedChanges = true;
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (!ValidateItem(gridViewSKU.FocusedRowHandle))
                return;
            
            _baseSKUConfigController.CurrentIndex = gridViewSKU.IsGroupRow(gridViewSKU.FocusedRowHandle)
                                                        ? -1
                                                        : gridViewSKU.GetDataSourceRowIndex(gridViewSKU.FocusedRowHandle);
            if (_baseSKUConfigController.CurrentIndex < 0)
                return;

            // if first position do nothing
            int lRowHandle =
                gridViewSKU.GetVisibleRowHandle(gridViewSKU.GetPrevVisibleRow(gridViewSKU.GetVisibleIndex(gridViewSKU.FocusedRowHandle)));
            if (lRowHandle == gridViewSKU.FocusedRowHandle || gridViewSKU.IsGroupRow(lRowHandle)
                || !gridViewSKU.IsValidRowHandle(lRowHandle))
                return;
            
            int lPrevIndex = gridViewSKU.GetDataSourceRowIndex(lRowHandle);
            
            _baseSKUConfigController.SwapItems(lPrevIndex, _baseSKUConfigController.CurrentIndex, true);

            gridViewSKU.BeforeLeaveRow -= gridViewSKU_BeforeLeaveRow;
            gridViewSKU.FocusedRowChanged -= gridViewSKU_FocusedRowChanged;

            gridViewSKU.FocusedRowHandle = gridViewSKU.GetRowHandle(lPrevIndex);

            gridViewSKU.BeforeLeaveRow += gridViewSKU_BeforeLeaveRow;
            gridViewSKU.FocusedRowChanged += gridViewSKU_FocusedRowChanged;

            _baseSKUConfigController.HasUnsavedChanges = true;
        }

        private void btnCopy_Click(object sender, EventArgs e) {
            gridViewSKU.PostEditor();
            gridViewSKU.UpdateCurrentRow();
            if (_baseSKUConfigController.HasUnsavedChanges)
            {
                XtraMessageBox.Show("Не все измения сохранены. Перед копированием нужно либо сохранить изменения, либо отменить", "Настройка продуктов",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            CopySettingsToForm lCopyForm = new CopySettingsToForm();
            lCopyForm.StaffDataSource = DataRepository.GetStaffTreeList(_baseSKUConfigController.FilterController.CurrentCountry.Id,
                                                                        _baseSKUConfigController.IsFilterCountryLevel);
            lCopyForm.DateFrom = new DateTime(_baseSKUConfigController.FilterController.Date.Year, _baseSKUConfigController.FilterController.Date.Month, 1);
            lCopyForm.StaffId = _baseSKUConfigController.FilterController.CurrentM3.Id;
            lCopyForm.LoadDefaultSettings();

            DialogResult lDialogResult = lCopyForm.ShowDialog();
            if (lDialogResult == DialogResult.Cancel)
                return;

            _baseSKUConfigController.CopySku(lCopyForm.DateTo, lCopyForm.SelectedIds);

            //show recent changes
            if (lCopyForm.DateFrom == lCopyForm.DateTo)
            {
                _baseSKUConfigController.LoadData(true);
            }
       }

        #endregion

        public bool IsRowCountryLevel {
            get {
                int lRowHandle = gridViewSKU.FocusedRowHandle;
                if (gridViewSKU.IsGroupRow(lRowHandle))
                    lRowHandle = gridViewSKU.GetChildRowHandle(lRowHandle, 0);

                return Convert.ToInt32(gridViewSKU.GetRowCellValue(lRowHandle, colM3Id)) == -1;
            }
        }

        public virtual bool CanModifyCountryLevel {
            get { return true; }
        }

        public virtual bool CanModifyM3Level {
            get { return true; }
        }

        public virtual void EnableButtons(bool enable) {
            btnAdd.Enabled = enable;
            btnDelete.Enabled = enable;
            btnUp.Enabled = enable;
            btnDown.Enabled = enable;
            btnCopy.Enabled = enable;
        }

        /// <summary>
        /// Saves data to DB with confirmation dialog if data was changed.
        /// </summary>
        /// <returns><c>true</c> if data saved successfully or the user doesn't want to store the data. Otherwise returns <c>false</c>.</returns>
        public bool SaveChangesWithConfirm() {
            gridViewSKU.PostEditor();
            gridViewSKU.UpdateCurrentRow();
            if (!_baseSKUConfigController.HasUnsavedChanges)
                return true;
            DialogResult lRes = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Настройка продуктов",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (lRes == DialogResult.Cancel)
                return false;
            if (lRes == DialogResult.No)
                return true;
            bool lIsValid = true;
            if (lRes == DialogResult.Yes)
                lIsValid = SaveData();

            return lIsValid;
        }

        public bool SaveData() {
            gridViewSKU.PostEditor();
            gridViewSKU.UpdateCurrentRow();
            if (!ValidateItem(gridViewSKU.FocusedRowHandle))
                return false;

            _baseSKUConfigController.SaveData();
            _isSkuTreeModified = false;
            _baseSKUConfigController.HasUnsavedChanges = false;
            XtraMessageBox.Show("Данные успешно сохранены!", "Настройка продуктов", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return true;
        }
    }
}