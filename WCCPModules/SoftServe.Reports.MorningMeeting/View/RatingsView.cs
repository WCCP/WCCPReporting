﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils.Win;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Popup;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTreeList.Nodes;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.UserControl;
using SoftServe.Reports.MorningMeeting.Utils;
using FilterControl = SoftServe.Reports.MorningMeeting.UserControl.FilterControl;

namespace SoftServe.Reports.MorningMeeting.View
{
    [ToolboxItem(true)]
    public partial class RatingsView : SimpleControlView, IRatingsView
    {
        private const int Max_Rating_Count = 4;

        private PopupContainerControl popupControl = new PopupContainerControl();
        private Label lSkipChannel = new Label();
        private ComboBoxEdit cSkipChannel = new ComboBoxEdit();
        private Label lProcuctSet = new Label();
        private ComboBoxEdit cProcuctSet = new ComboBoxEdit();


        private IRatingsController _ratingsController;
        private List<KpiModel> _kpis;
        private List<ListItemModel> _skipChannels;
        private List<ProductListItemModel> _productSet;
        private List<RatingsColumnsModel> _ratingsColumns;
        private List<ChannelModel> _channels;

        public RatingsView()
        {
            InitializeComponent();


            lSkipChannel.Text = "СКИП канал:";
            lSkipChannel.Location = new Point(5, 0);
            lSkipChannel.Size = new Size(190, 15);

            cSkipChannel.Location = new Point(5, 15);
            cSkipChannel.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cSkipChannel.Properties.DropDownRows = 10;
            cSkipChannel.Size = new Size(190, 20);

            lProcuctSet.Text = "Продуктовый набор:";
            lProcuctSet.Location = new Point(5, 50);
            lProcuctSet.Size = new Size(190, 15);

            cProcuctSet.Location = new Point(5, 65);
            cProcuctSet.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            cProcuctSet.Size = new Size(190, 20);

            popupControl.Controls.Add(lSkipChannel);
            popupControl.Controls.Add(cSkipChannel);
            popupControl.Controls.Add(lProcuctSet);
            popupControl.Controls.Add(cProcuctSet);

            rPopupContainerEdit.PopupControl = popupControl;
        }

        #region Implementation of IView

        public override IViewController GetController()
        {
            return _ratingsController;
        }

        public override void SetController(IViewController viewController)
        {
            _ratingsController = (IRatingsController)viewController;
        }

        public override object GetDockObject(ISimpleControlView controlView)
        {
            return panelFilter;
        }

        #endregion

        #region Implementation of IRatingsView

        public void AssignKpiTypes(List<ListItemModel> types)
        {
            rKpiTypes.DataSource = types;
        }

        public void AssignKPIs(List<KpiModel> types)
        {
            _kpis = types;
            rKpis.DataSource = types;
        }

        public void AssignProductSets(List<ProductListItemModel> types)
        {
            _productSet = types;

            rProductSets.DataSource = GetColumnsByValueType(RatingsSetType.ProductSet);
            cProcuctSet.Properties.Items.AddRange(GetProductGroups());
        }

        public void AssignAverageSkuList()
        {
            rAvrSkuList.DataSource = GetColumnsByValueType(RatingsSetType.AvlSkuSet);
        }

        public void AssignNetworks()
        {
            rNetworks.DataSource = GetNetworksColumns();
        }

        public void AssignSkipChannels(List<ListItemModel> types)
        {
            _skipChannels = types;
            cSkipChannel.Properties.Items.Clear();
            cSkipChannel.Properties.Items.AddRange(types);
        }

        public void AssignRatings(List<RatingsModel> ratings)
        {
            gridViewRatings.FocusedRowChanged -= gridViewGoals_FocusedRowChanged;
            gridViewRatings.BeginDataUpdate();
            gridControlRatings.DataSource = ratings;
            gridViewRatings.EndDataUpdate();
            gridViewRatings.FocusedRowChanged += gridViewGoals_FocusedRowChanged;

            EnableAddButton();
        }

        public void AssignChannels(List<ChannelModel> channels)
        {
            _channels = channels;

            cChannel.Properties.Items.Clear();
            cChannel.Properties.Items.AddRange(channels);
            cChannel.EditValue = cChannel.Properties.Items[0];
        }

        public void AssignColumnsList(List<RatingsColumnsModel> ratingsColumnsList)
        {
            _ratingsColumns = ratingsColumnsList;
        }

        public void SaveActions()
        {
            gridViewRatings.PostEditor();
            gridViewRatings.UpdateGroupSummary();

            if (IsValid && CheckRatingsWeightSum())
            {
                _ratingsController.SaveData();
                ShowMessChangesWereSaved();
            }
        }

        public void RefreshActions()
        {
            gridViewRatings.PostEditor();

            if (_ratingsController.HasUnsavedChanges)
            {
                if (SaveChangesAndProceed())
                {
                    return;
                }
            }

            _ratingsController.LoadData(true);
        }

        public virtual bool LeaveTabActions()
        {
            gridViewRatings.PostEditor();
            gridViewRatings.UpdateGroupSummary();

            bool lAllowLeavePage = true;
            bool isValid = true;

            if (_ratingsController.HasUnsavedChanges)
            {
                DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                   MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                switch (res)
                {
                    case DialogResult.Yes:
                        {
                            isValid = IsValid && CheckRatingsWeightSum();

                            if (isValid)
                            {
                                _ratingsController.SaveData();
                                ShowMessChangesWereSaved();
                            }
                        }
                        break;
                    case DialogResult.No:
                        _ratingsController.LoadData(true);
                        break;
                    case DialogResult.Cancel:
                        lAllowLeavePage = false;
                        break;
                }
            }

            return lAllowLeavePage && isValid;
        }

        public bool IsValid
        {
            get { return CheckRow(gridViewRatings.FocusedRowHandle); }
        }

        public bool SaveChangesAndProceed()
        {
            gridViewRatings.PostEditor();
            gridViewRatings.UpdateGroupSummary();

            bool isValid = true;
            DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                   MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                isValid = IsValid && CheckRatingsWeightSum();

                if (isValid)
                {
                    _ratingsController.SaveData();
                    ShowMessChangesWereSaved();
                }
            }

            return res == DialogResult.Cancel || !isValid;
        }

        public void PostEditor()
        {
            gridViewRatings.PostEditor();
        }

        public void ShowMessage(string message, string caption)
        {
            XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        #endregion

        #region Event Handling

        private void bAdd_Click(object sender, EventArgs e)
        {
            if (!CheckRow(gridViewRatings.FocusedRowHandle))
            {
                return;
            }

            int lChannelId = (cChannel.EditValue as ChannelModel).Id;

            _ratingsController.AddRating(_ratingsController.CurrentM3.Id, lChannelId);

            gridViewRatings.RefreshData();

            gridViewRatings.FocusedRowHandle = gridViewRatings.GetRowHandle((gridControlRatings.DataSource as List<RatingsModel>).Count - 1);

            EnableAddButton();
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            if (gridViewRatings.FocusedRowHandle < 0)
            {
                return;
            }

            if (XtraMessageBox.Show("Вы действительно хотите удалить KPI?", "Удалить KPI", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int lRatingId = (int)(gridViewRatings.GetFocusedRowCellValue(colId));

                _ratingsController.DeleteRating(lRatingId);

                gridViewRatings.RefreshData();

                EnableAddButton();
            }
        }

        private void gridViewGoals_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;

            if (info == null)
            {
                return;
            }

            if (info.Column == colChannelId)
            {
                int lChannelId = GetGroupRowChannel(e.RowHandle);
                string lChannelName = _channels.First(c => c.Id == lChannelId).Name;

                info.GroupText = lChannelName + ":";
            }
        }

        private void gridViewGoals_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            bDelete.Enabled = GetDeleteAvailability() && e.FocusedRowHandle >= 0;
        }

        private void gridViewGoals_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            //Suppress displaying the error message box
            e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void cChannel_EditValueChanged(object sender, EventArgs e)
        {
            EnableAddButton();
        }

        private void gridViewRatings_CustomDrawRowFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            if (e.Info.Column == colWeight)
            {
                if (Convert.ToInt32(e.Info.Value) != 100)
                {
                    Brush brush = e.Cache.GetGradientBrush(e.Bounds, Color.Salmon, Color.SeaShell,
                                                           LinearGradientMode.Vertical);
                    Rectangle r = e.Bounds;
                    //Create a raised effect for a cell
                    ControlPaint.DrawBorder(e.Graphics, r, Color.Salmon, ButtonBorderStyle.Solid);
                    //Fill the inner region of the cell
                    r.Inflate(-1, -1);
                    e.Graphics.FillRectangle(brush, r);
                    //Draw a summary value
                    r.Inflate(-2, 0);
                    e.Appearance.DrawString(e.Cache, e.Info.DisplayText, r);
                    //Prevent default drawing of the cell
                    e.Handled = true;
                }
            }

            if (e.Info.Column == colKpiTypeId)
            {
                e.Appearance.DrawString(e.Cache, "Общий вес:", e.Bounds);
                e.Handled = true;
            }
        }

        private void gridViewRatings_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (gridViewRatings.FocusedColumn == colWeight)
            {
                string lWeight = Convert.ToString(e.Value);

                if (string.IsNullOrEmpty(lWeight))
                {
                    e.Valid = false;
                    e.ErrorText = "Пожалуйста введите вес";
                }
            }

            if (gridViewRatings.FocusedColumn == colValues && (gridViewRatings.ActiveEditor is PopupContainerEdit))
            {
                string[] lValues = Convert.ToString(e.Value).Split(',');

                if (string.IsNullOrEmpty(lValues[0]) || string.IsNullOrEmpty(lValues[1]))
                {
                    e.Valid = false;
                    e.ErrorText = "Пожалуйста выберите канал и набор";
                }
            }
        }

        private void gridViewRatings_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (!CanEditCurrentM3)
            {
                e.Cancel = true;
                return;
            }

            if (gridViewRatings.FocusedColumn == colValues)
            {
                object lKpi = gridViewRatings.GetFocusedRowCellValue(colKpiId);

                if (lKpi == null)
                {
                    e.Cancel = true;
                    return;
                }

                int lKpiId = Convert.ToInt32(lKpi);

                KpiModel lKpiModel = _kpis.FirstOrDefault(k => k.Id == lKpiId);

                if (lKpiModel == null)
                {
                    e.Cancel = true;
                    return;
                }

                if (lKpiModel.ValueType == RatingsSetType.Empty)
                {
                    e.Cancel = true;
                }
            }
        }

        private void gridViewRatings_ShownEditor(object sender, EventArgs e)
        {
            if (gridViewRatings.FocusedColumn == colKpiId)
            {
                LookUpEdit editor = (LookUpEdit)gridViewRatings.ActiveEditor;

                editor.Properties.DataSource = FormKpiList(Convert.ToInt32(gridViewRatings.GetFocusedRowCellValue(colKpiTypeId)));

                string lCurStatusInt = Convert.ToString(gridViewRatings.GetFocusedRowCellValue(colValues));

                editor.EditValue = lCurStatusInt;
            }

            if (gridViewRatings.FocusedColumn == colValues && (gridViewRatings.ActiveEditor is LookUpEdit))
            {
                LookUpEdit editor = (LookUpEdit)gridViewRatings.ActiveEditor;

                object lKpi = gridViewRatings.GetFocusedRowCellValue(colKpiId);

                if (lKpi == null)
                {
                    return;
                }

                int lKpiId = Convert.ToInt32(lKpi);

                KpiModel lKpiModel = _kpis.FirstOrDefault(k => k.Id == lKpiId);

                if (lKpiModel == null)
                {
                    return;
                }

                RatingsSetType lSetType = lKpiModel.ValueType;


                switch (lSetType)
                {
                    case RatingsSetType.ProductSet:
                        editor.Properties.DataSource = GetColumnsByValueType(RatingsSetType.ProductSet, lKpiId);
                        break;
                    case RatingsSetType.AvlSkuSet:
                        editor.Properties.DataSource = GetColumnsByValueType(RatingsSetType.AvlSkuSet, lKpiId);
                        break;
                    case RatingsSetType.SkipChannelAndProductSet:
                        break;
                    case RatingsSetType.Network:
                        editor.Properties.DataSource = GetNetworksColumnsByValueType(lKpiId);
                        break;
                    case RatingsSetType.Empty:
                        break;
                }

                string lCurStatusInt = Convert.ToString(gridViewRatings.GetFocusedRowCellValue(colValues));
                editor.EditValue = lCurStatusInt;
            }
        }

        private void gridViewRatings_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            _ratingsController.HasUnsavedChanges = true;
            _ratingsController.HasUnsavedChangesForCurrentM3 = true;

            if (e.Column == colKpiTypeId)
            {
                gridViewRatings.SetRowCellValue(e.RowHandle, colKpiId, -17); // or any values
                gridViewRatings.SetRowCellValue(e.RowHandle, colValues, null);

                cSkipChannel.EditValue = null;
                cProcuctSet.EditValue = null;
            }

            if (e.Column == colKpiId)
            {
                object lKpi = gridViewRatings.GetRowCellValue(e.RowHandle, colKpiId);

                if (lKpi == null)
                {
                    return;
                }

                int lKpiId = Convert.ToInt32(lKpi);
                KpiModel lKpiModel = _kpis.FirstOrDefault(k => k.Id == lKpiId);

                RatingsColumnsModel lColumn = GetColumnsByValueType(lKpiId);

                if (lKpiModel != null && lKpiModel.ValueType == RatingsSetType.Empty && lColumn != null)
                {
                    int lColumnId = lColumn.ColumnId;

                    RatingsModel rating = gridViewRatings.GetFocusedRow() as RatingsModel;
                    rating.ColumnId = lColumnId;
                }
            }

            if (e.Column == colValues)
            {
                object lKpi = gridViewRatings.GetRowCellValue(e.RowHandle, colKpiId);

                if (lKpi == null)
                {
                    return;
                }

                int lKpiId = Convert.ToInt32(lKpi);
                KpiModel lKpiModel = _kpis.FirstOrDefault(k => k.Id == lKpiId);

                if (lKpiModel != null && lKpiModel.ValueType != RatingsSetType.Empty && lKpiModel.ValueType != RatingsSetType.SkipChannelAndProductSet)
                {
                    RatingsModel rating = gridViewRatings.GetFocusedRow() as RatingsModel;

                    if (!string.IsNullOrEmpty(rating.Values))
                    {
                        rating.ColumnId = int.Parse(rating.Values);
                    }
                }
            }
        }

        private void gridViewRatings_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column != colValues)
            {
                return;
            }

            object lKpi = gridViewRatings.GetRowCellValue(e.RowHandle, colKpiId);

            if (lKpi == null)
            {
                return;
            }

            int lKpiId = Convert.ToInt32(lKpi);

            KpiModel lKpiModel = _kpis.FirstOrDefault(k => k.Id == lKpiId);

            if (lKpiModel == null)
            {
                return;
            }

            RatingsSetType lSetType = lKpiModel.ValueType;


            switch (lSetType)
            {
                case RatingsSetType.ProductSet:
                    e.RepositoryItem = rProductSets;
                    break;
                case RatingsSetType.AvlSkuSet:
                    e.RepositoryItem = rAvrSkuList;
                    break;
                case RatingsSetType.SkipChannelAndProductSet:
                    e.RepositoryItem = rPopupContainerEdit;
                    break;
                case RatingsSetType.Network:
                    e.RepositoryItem = rNetworks;
                    break;

                case RatingsSetType.Empty:
                    e.RepositoryItem = null;
                    break;
            }
        }

        private void rPopupContainerEdit_QueryDisplayText(object sender, QueryDisplayTextEventArgs e)
        {
            string lEditValue = Convert.ToString(e.EditValue);

            if (lEditValue != string.Empty)
            {
                string[] lValues = lEditValue.Split(',');

                ListItemModel lSkipChannel = !string.IsNullOrEmpty(lValues[0])
                                                 ? _skipChannels.FirstOrDefault(c => c.Id == int.Parse(lValues[0]))
                                                 : null;
                string lSkipChannelName = lSkipChannel != null ? lSkipChannel.Name : "";


                ProductListItemModel lProductSet = !string.IsNullOrEmpty(lValues[1])
                                                       ? _productSet.FirstOrDefault(c => c.Id == int.Parse(lValues[1]))
                                                       : null;
                string lProductSetName = lProductSet != null ? lProductSet.Name : "";

                e.DisplayText = lSkipChannelName + "; " + lProductSetName;
            }
            else
            {
                e.DisplayText = "";
            }
        }

        private void rPopupContainerEdit_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            ListItemModel lCurChannel = cSkipChannel.EditValue as ListItemModel;
            ProductListItemModel lCurSet = cProcuctSet.EditValue as ProductListItemModel;

            e.Value = (lCurChannel != null ? lCurChannel.Id.ToString() : "") + "," + (lCurSet != null ? lCurSet.Id.ToString() : "");
        }

        private void gridViewRatings_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
        {
            if (gridViewRatings.FocusedRowHandle < 0 || !gridViewRatings.IsValidRowHandle(gridViewRatings.FocusedRowHandle))
                return;

            e.Allow = CheckRow(gridViewRatings.FocusedRowHandle);
        }

        #endregion

        public void OnM3FilterChange(int newM3)
        {
            gridViewRatings.PostEditor();

            gridViewRatings.ActiveFilterString = string.Format("M3Id = {0}", newM3);
            EnableAddButton();
            bDelete.Enabled = GetDeleteAvailability();

            cProcuctSet.Properties.Items.Clear();
            cProcuctSet.Properties.Items.AddRange(GetProductGroups());
        }

        private bool CheckRatingsWeightSum()
        {
            bool result = true;

            int lGroupRowHandle = -1;

            while (gridViewRatings.IsValidRowHandle(lGroupRowHandle))
            {
                int lGroupWeight = Convert.ToInt32(gridViewRatings.GetRowSummaryItem(lGroupRowHandle, colWeight).Value);
                result &= lGroupWeight <= 100;

                lGroupRowHandle--;
            }

            if (!result)
            {
                XtraMessageBox.Show("Для некоторых каналов общий вес превышает 100%", "Рейтинги", MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
            }

            return result;
        }

        private void EnableAddButton()
        {
            if (!CanEditCurrentM3)
            {
                bAdd.Enabled = false;
                return;
            }

            int groupRowHandle = 0;
            int lCurChannel = GetCurrentChannelId();

            while (gridViewRatings.IsValidRowHandle(--groupRowHandle))
            {
                if (GetGroupRowChannel(groupRowHandle) == lCurChannel)
                // get children count for curr channel
                {
                    int lVisibleCount = 0;
                    int lChildRow = gridViewRatings.GetChildRowHandle(groupRowHandle, 0);

                    while (gridViewRatings.GetParentRowHandle(lChildRow) == groupRowHandle)
                    {
                        lVisibleCount++;
                        lChildRow = gridViewRatings.GetNextVisibleRow(lChildRow);
                    }

                    bAdd.Enabled = GetAddButtonAvailability() && lVisibleCount < Max_Rating_Count;

                    return;
                }
                // get children count for curr channel
            }

            bAdd.Enabled = GetAddButtonAvailability();
        }

        private int GetCurrentChannelId()
        {
            int channelId = -1;
            ChannelModel channel = cChannel.EditValue as ChannelModel;

            if (channel != null)
            {
                channelId = channel.Id;
            }

            return channelId;
        }

        private int GetGroupRowChannel(int groupRowHandle)
        {
            int lRowHandle = gridViewRatings.GetChildRowHandle(groupRowHandle, 0);
            object lChannelValue = gridViewRatings.GetRowCellValue(lRowHandle, colChannelId);

            if (lChannelValue == null)
            {
                return -1;
            }

            return Convert.ToInt32(lChannelValue);
        }

        private List<KpiModel> FormKpiList(int kpiType)
        {
            return _kpis.Where(k => k.KpiTypeId == kpiType).Select(p => p).ToList();
        }

        private bool CheckRow(int rowHandle)
        {
            if (!gridViewRatings.IsValidRowHandle(gridViewRatings.FocusedRowHandle) || gridViewRatings.FocusedRowHandle < 0)
            {
                return true;
            }

            bool isCorrect = true;

            string lKpiTypeName = gridViewRatings.GetRowCellDisplayText(rowHandle, colKpiTypeId);

            if (string.IsNullOrEmpty(lKpiTypeName))
            {
                isCorrect = false;
                XtraMessageBox.Show("Пожалуйста выберите тип KPI");
            }
            else
            {
                string lKpiName = gridViewRatings.GetRowCellDisplayText(rowHandle, colKpiId);

                if (string.IsNullOrEmpty(lKpiName))
                {
                    isCorrect = false;
                    XtraMessageBox.Show("Пожалуйста выберите KPI");
                }
                else
                {
                    int lKpiId = Convert.ToInt32(gridViewRatings.GetRowCellValue(rowHandle, colKpiId));

                    KpiModel lKpiModel = _kpis.First(k => k.Id == lKpiId);

                    if (lKpiModel.ValueType != RatingsSetType.Empty &&
                        string.IsNullOrEmpty(gridViewRatings.GetRowCellDisplayText(rowHandle, colValues)))
                    {
                        isCorrect = false;
                        XtraMessageBox.Show("Пожалуйста выберите показатель");
                    }
                }
            }

            return isCorrect;
        }

        private List<ProductListItemModel> GetProductGroups()
        {
            return _productSet.Where(s => s.M3Id == _ratingsController.CurrentM3.Id || s.M3Id == -1).ToList();
        }

        private List<RatingsColumnsModel> GetColumnsByValueType(RatingsSetType type)
        {
            return _ratingsColumns.Where(s => (s.M3Id == _ratingsController.CurrentM3.Id || s.M3Id == -1) && s.ValueType == type).ToList();
        }

        private List<RatingsColumnsModel> GetNetworksColumns()
        {
            return _ratingsColumns.Where(s => s.M3Id == _ratingsController.CurrentM3.Id && s.ValueType == RatingsSetType.Network).ToList();
        }

        private List<RatingsColumnsModel> GetColumnsByValueType(RatingsSetType type, int kpiId)
        {
            List<RatingsColumnsModel> l = _ratingsColumns.Where(s => (s.M3Id == _ratingsController.CurrentM3.Id || s.M3Id == -1) && s.ValueType == type && s.KpiId == kpiId).ToList();
            return l;
        }

        private List<RatingsColumnsModel> GetNetworksColumnsByValueType(int kpiId)
        {
            List<RatingsColumnsModel> l = _ratingsColumns.Where(s => s.M3Id == _ratingsController.CurrentM3.Id && s.ValueType == RatingsSetType.Network && s.KpiId == kpiId).ToList();
            return l;
        }

        private RatingsColumnsModel GetColumnsByValueType(int kpiId)
        {
            List<RatingsColumnsModel> l = _ratingsColumns.Where(s => (s.M3Id == _ratingsController.CurrentM3.Id || s.M3Id == -1) && s.KpiId == kpiId).ToList();
            return l.Count > 0 ? l[0] : null;
        }

        private void gridControlRatings_Validating(object sender, CancelEventArgs e)
        {
            if (bDelete.ContainsFocus && bDelete.Enabled)
            {
                gridViewRatings.ClearColumnErrors();
                e.Cancel = false;
            }
        }

        private void gridViewRatings_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (!CanEditCurrentM3)
                {
                    e.Appearance.BackColor = Color.LightGray;
                    e.Appearance.BackColor2 = Color.LightGray;
                }
            }
        }

        private bool GetAddButtonAvailability()
        {
            return CanEditCurrentM3;
        }

        private bool GetDeleteAvailability()
        {
            bool isButtonAccessibleByPermissions = CanEditCurrentM3;
            bool isGridViewNonEmpty = gridViewRatings.RowCount > 0;

            return isButtonAccessibleByPermissions && isGridViewNonEmpty;
        }

        private bool CanEditCurrentM3
        {
            get { return _ratingsController.IsCountryLevel && RoleManager.IsInRole(MMRoleEnum.KpiRatingM5) || !_ratingsController.IsCountryLevel && RoleManager.IsInRole(MMRoleEnum.KpiRatingM3); }
        }

        private void ShowMessChangesWereSaved()
        {
            XtraMessageBox.Show("Данные успешно сохранены!", "Рейтинги", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
