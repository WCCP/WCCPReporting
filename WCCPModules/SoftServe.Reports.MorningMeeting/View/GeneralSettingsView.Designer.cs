﻿namespace SoftServe.Reports.MorningMeeting.View
{
    partial class GeneralSettingsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelFilter = new DevExpress.XtraEditors.PanelControl();
            this.panelControls = new DevExpress.XtraEditors.PanelControl();
            this.cMethodSrCalc = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cMethodAkbCalc = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lMethodSrCalc = new DevExpress.XtraEditors.LabelControl();
            this.lMethodAkbCalc = new DevExpress.XtraEditors.LabelControl();
            this.tPackageDistributionPeriod = new DevExpress.XtraEditors.TextEdit();
            this.lPackageDistributionPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lFocusByDsm = new DevExpress.XtraEditors.LabelControl();
            this.mFocusByDsm = new DevExpress.XtraEditors.MemoEdit();
            this.tSaleGracePeriod = new DevExpress.XtraEditors.TextEdit();
            this.lSaleGracePeriod = new DevExpress.XtraEditors.LabelControl();
            this.cMPList = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.tAmountOfMP = new DevExpress.XtraEditors.TextEdit();
            this.lAmountOfMP = new DevExpress.XtraEditors.LabelControl();
            this.lMPList = new DevExpress.XtraEditors.LabelControl();
            this.tAmountOfDistPushSKU = new DevExpress.XtraEditors.TextEdit();
            this.tInBevBeerVolumeLimit = new DevExpress.XtraEditors.TextEdit();
            this.tAmountOfFocusSKU = new DevExpress.XtraEditors.TextEdit();
            this.tAmountOfFocusTargets = new DevExpress.XtraEditors.TextEdit();
            this.lInBevBeerVolumeLimit = new DevExpress.XtraEditors.LabelControl();
            this.lAmountOfDistPushSKU = new DevExpress.XtraEditors.LabelControl();
            this.lAmountOfFocusSKU = new DevExpress.XtraEditors.LabelControl();
            this.lAmountOfFocusTargets = new DevExpress.XtraEditors.LabelControl();
            this.tTimeOfFirstVisit = new DevExpress.XtraEditors.TimeEdit();
            this.tTimeOfLastVisit = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lTimeOfLastVisit = new DevExpress.XtraEditors.LabelControl();
            this.lTimeOfFirstVisit = new DevExpress.XtraEditors.LabelControl();
            this.tAmountOfFocusOLs = new DevExpress.XtraEditors.TextEdit();
            this.lAmountOfFocusOLs = new DevExpress.XtraEditors.LabelControl();
            this.tAmountOfTheWorstOLs = new DevExpress.XtraEditors.TextEdit();
            this.lAmountOfTheWorstOLs = new DevExpress.XtraEditors.LabelControl();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControls)).BeginInit();
            this.panelControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMethodSrCalc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMethodAkbCalc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPackageDistributionPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFocusByDsm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSaleGracePeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMPList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfMP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfDistPushSKU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tInBevBeerVolumeLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfFocusSKU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfFocusTargets.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTimeOfFirstVisit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTimeOfLastVisit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfFocusOLs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfTheWorstOLs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFilter
            // 
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFilter.Location = new System.Drawing.Point(0, 0);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(1096, 64);
            this.panelFilter.TabIndex = 1;
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.cMethodSrCalc);
            this.panelControls.Controls.Add(this.cMethodAkbCalc);
            this.panelControls.Controls.Add(this.lMethodSrCalc);
            this.panelControls.Controls.Add(this.lMethodAkbCalc);
            this.panelControls.Controls.Add(this.tPackageDistributionPeriod);
            this.panelControls.Controls.Add(this.lPackageDistributionPeriod);
            this.panelControls.Controls.Add(this.lFocusByDsm);
            this.panelControls.Controls.Add(this.mFocusByDsm);
            this.panelControls.Controls.Add(this.tSaleGracePeriod);
            this.panelControls.Controls.Add(this.lSaleGracePeriod);
            this.panelControls.Controls.Add(this.cMPList);
            this.panelControls.Controls.Add(this.tAmountOfMP);
            this.panelControls.Controls.Add(this.lAmountOfMP);
            this.panelControls.Controls.Add(this.lMPList);
            this.panelControls.Controls.Add(this.tAmountOfDistPushSKU);
            this.panelControls.Controls.Add(this.tInBevBeerVolumeLimit);
            this.panelControls.Controls.Add(this.tAmountOfFocusSKU);
            this.panelControls.Controls.Add(this.tAmountOfFocusTargets);
            this.panelControls.Controls.Add(this.lInBevBeerVolumeLimit);
            this.panelControls.Controls.Add(this.lAmountOfDistPushSKU);
            this.panelControls.Controls.Add(this.lAmountOfFocusSKU);
            this.panelControls.Controls.Add(this.lAmountOfFocusTargets);
            this.panelControls.Controls.Add(this.tTimeOfFirstVisit);
            this.panelControls.Controls.Add(this.tTimeOfLastVisit);
            this.panelControls.Controls.Add(this.labelControl14);
            this.panelControls.Controls.Add(this.lTimeOfLastVisit);
            this.panelControls.Controls.Add(this.lTimeOfFirstVisit);
            this.panelControls.Controls.Add(this.tAmountOfFocusOLs);
            this.panelControls.Controls.Add(this.lAmountOfFocusOLs);
            this.panelControls.Controls.Add(this.tAmountOfTheWorstOLs);
            this.panelControls.Controls.Add(this.lAmountOfTheWorstOLs);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControls.Location = new System.Drawing.Point(0, 64);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(1096, 360);
            this.panelControls.TabIndex = 2;
            // 
            // cMethodSrCalc
            // 
            this.cMethodSrCalc.Location = new System.Drawing.Point(731, 337);
            this.cMethodSrCalc.Name = "cMethodSrCalc";
            this.cMethodSrCalc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cMethodSrCalc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cMethodSrCalc.Size = new System.Drawing.Size(264, 20);
            this.cMethodSrCalc.TabIndex = 79;
            this.cMethodSrCalc.EditValueChanged += new System.EventHandler(this.combobox_EditValueChanged);
            this.cMethodSrCalc.Validating += new System.ComponentModel.CancelEventHandler(this.combobox_Validating);
            // 
            // cMethodAkbCalc
            // 
            this.cMethodAkbCalc.Location = new System.Drawing.Point(731, 316);
            this.cMethodAkbCalc.Name = "cMethodAkbCalc";
            this.cMethodAkbCalc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cMethodAkbCalc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cMethodAkbCalc.Size = new System.Drawing.Size(264, 20);
            this.cMethodAkbCalc.TabIndex = 78;
            this.cMethodAkbCalc.EditValueChanged += new System.EventHandler(this.combobox_EditValueChanged);
            this.cMethodAkbCalc.Validating += new System.ComponentModel.CancelEventHandler(this.combobox_Validating);
            // 
            // lMethodSrCalc
            // 
            this.lMethodSrCalc.Location = new System.Drawing.Point(608, 340);
            this.lMethodSrCalc.Name = "lMethodSrCalc";
            this.lMethodSrCalc.Size = new System.Drawing.Size(97, 13);
            this.lMethodSrCalc.TabIndex = 77;
            this.lMethodSrCalc.Text = "Метод расчета SR:";
            // 
            // lMethodAkbCalc
            // 
            this.lMethodAkbCalc.Location = new System.Drawing.Point(601, 319);
            this.lMethodAkbCalc.Name = "lMethodAkbCalc";
            this.lMethodAkbCalc.Size = new System.Drawing.Size(104, 13);
            this.lMethodAkbCalc.TabIndex = 76;
            this.lMethodAkbCalc.Text = "Метод расчета АКБ:";
            // 
            // tPackageDistributionPeriod
            // 
            this.tPackageDistributionPeriod.Location = new System.Drawing.Point(731, 162);
            this.tPackageDistributionPeriod.Name = "tPackageDistributionPeriod";
            this.tPackageDistributionPeriod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tPackageDistributionPeriod.Properties.Mask.EditMask = "\\d{0,9}";
            this.tPackageDistributionPeriod.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tPackageDistributionPeriod.Size = new System.Drawing.Size(264, 20);
            this.tPackageDistributionPeriod.TabIndex = 75;
            this.tPackageDistributionPeriod.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            this.tPackageDistributionPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.tPackageDistributionPeriod_Validating);
            // 
            // lPackageDistributionPeriod
            // 
            this.lPackageDistributionPeriod.Location = new System.Drawing.Point(501, 165);
            this.lPackageDistributionPeriod.Name = "lPackageDistributionPeriod";
            this.lPackageDistributionPeriod.Size = new System.Drawing.Size(204, 13);
            this.lPackageDistributionPeriod.TabIndex = 74;
            this.lPackageDistributionPeriod.Text = "Период учета пакетной дистрибьюции:";
            // 
            // lFocusByDsm
            // 
            this.lFocusByDsm.Location = new System.Drawing.Point(623, 215);
            this.lFocusByDsm.Name = "lFocusByDsm";
            this.lFocusByDsm.Size = new System.Drawing.Size(82, 13);
            this.lFocusByDsm.TabIndex = 73;
            this.lFocusByDsm.Text = "Фокус от ДСМа:";
            // 
            // mFocusByDsm
            // 
            this.mFocusByDsm.Location = new System.Drawing.Point(731, 212);
            this.mFocusByDsm.Name = "mFocusByDsm";
            this.mFocusByDsm.Size = new System.Drawing.Size(264, 98);
            this.mFocusByDsm.TabIndex = 72;
            this.mFocusByDsm.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            // 
            // tSaleGracePeriod
            // 
            this.tSaleGracePeriod.Location = new System.Drawing.Point(731, 136);
            this.tSaleGracePeriod.Name = "tSaleGracePeriod";
            this.tSaleGracePeriod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tSaleGracePeriod.Properties.Mask.EditMask = "\\d{0,9}";
            this.tSaleGracePeriod.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tSaleGracePeriod.Size = new System.Drawing.Size(264, 20);
            this.tSaleGracePeriod.TabIndex = 71;
            this.tSaleGracePeriod.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            this.tSaleGracePeriod.Validating += new System.ComponentModel.CancelEventHandler(this.tSaleGracePeriod_Validating);
            // 
            // lSaleGracePeriod
            // 
            this.lSaleGracePeriod.Location = new System.Drawing.Point(529, 139);
            this.lSaleGracePeriod.Name = "lSaleGracePeriod";
            this.lSaleGracePeriod.Size = new System.Drawing.Size(176, 13);
            this.lSaleGracePeriod.TabIndex = 68;
            this.lSaleGracePeriod.Text = "Период учета заявок как продаж:";
            // 
            // cMPList
            // 
            this.cMPList.EditValue = "";
            this.cMPList.Location = new System.Drawing.Point(731, 64);
            this.cMPList.Name = "cMPList";
            this.cMPList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cMPList.Properties.PopupFormSize = new System.Drawing.Size(350, 300);
            this.cMPList.Properties.SelectAllItemVisible = false;
            this.cMPList.Properties.ShowButtons = false;
            this.cMPList.Properties.ShowPopupCloseButton = false;
            this.cMPList.Size = new System.Drawing.Size(264, 20);
            this.cMPList.TabIndex = 67;
            this.cMPList.Popup += new System.EventHandler(this.checkedComboBox_Popup);
            this.cMPList.CloseUp += new DevExpress.XtraEditors.Controls.CloseUpEventHandler(this.checkedComboBox_CloseUp);
            this.cMPList.Validating += new System.ComponentModel.CancelEventHandler(this.cMPList_Validating);
            // 
            // tAmountOfMP
            // 
            this.tAmountOfMP.Location = new System.Drawing.Point(731, 38);
            this.tAmountOfMP.Name = "tAmountOfMP";
            this.tAmountOfMP.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tAmountOfMP.Properties.Mask.EditMask = "\\d{0,9}";
            this.tAmountOfMP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tAmountOfMP.Size = new System.Drawing.Size(264, 20);
            this.tAmountOfMP.TabIndex = 66;
            this.tAmountOfMP.EditValueChanged += new System.EventHandler(this.text_EditValueChanged);
            this.tAmountOfMP.Leave += new System.EventHandler(this.tAmountOfMP_Leave);
            this.tAmountOfMP.Validating += new System.ComponentModel.CancelEventHandler(this.text_Validating);
            // 
            // lAmountOfMP
            // 
            this.lAmountOfMP.Location = new System.Drawing.Point(549, 41);
            this.lAmountOfMP.Name = "lAmountOfMP";
            this.lAmountOfMP.Size = new System.Drawing.Size(156, 13);
            this.lAmountOfMP.TabIndex = 65;
            this.lAmountOfMP.Text = "Количество Маркет Программ:";
            // 
            // lMPList
            // 
            this.lMPList.Location = new System.Drawing.Point(573, 67);
            this.lMPList.Name = "lMPList";
            this.lMPList.Size = new System.Drawing.Size(132, 13);
            this.lMPList.TabIndex = 64;
            this.lMPList.Text = "Список Маркет Программ:";
            // 
            // tAmountOfDistPushSKU
            // 
            this.tAmountOfDistPushSKU.Location = new System.Drawing.Point(382, 238);
            this.tAmountOfDistPushSKU.Name = "tAmountOfDistPushSKU";
            this.tAmountOfDistPushSKU.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tAmountOfDistPushSKU.Properties.Mask.EditMask = "\\d{0,9}";
            this.tAmountOfDistPushSKU.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tAmountOfDistPushSKU.Size = new System.Drawing.Size(101, 20);
            this.tAmountOfDistPushSKU.TabIndex = 63;
            this.tAmountOfDistPushSKU.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            this.tAmountOfDistPushSKU.Validating += new System.ComponentModel.CancelEventHandler(this.text_Validating);
            // 
            // tInBevBeerVolumeLimit
            // 
            this.tInBevBeerVolumeLimit.Location = new System.Drawing.Point(382, 212);
            this.tInBevBeerVolumeLimit.Name = "tInBevBeerVolumeLimit";
            this.tInBevBeerVolumeLimit.Properties.Mask.EditMask = "\\d{0,7}\\,\\d{0,3}";
            this.tInBevBeerVolumeLimit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tInBevBeerVolumeLimit.Size = new System.Drawing.Size(101, 20);
            this.tInBevBeerVolumeLimit.TabIndex = 62;
            this.tInBevBeerVolumeLimit.EditValueChanged += new System.EventHandler(this.text_EditValueChanged);
            this.tInBevBeerVolumeLimit.Validating += new System.ComponentModel.CancelEventHandler(this.text_Validating);
            // 
            // tAmountOfFocusSKU
            // 
            this.tAmountOfFocusSKU.Location = new System.Drawing.Point(382, 264);
            this.tAmountOfFocusSKU.Name = "tAmountOfFocusSKU";
            this.tAmountOfFocusSKU.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tAmountOfFocusSKU.Properties.Mask.EditMask = "\\d{0,9}";
            this.tAmountOfFocusSKU.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tAmountOfFocusSKU.Size = new System.Drawing.Size(101, 20);
            this.tAmountOfFocusSKU.TabIndex = 61;
            this.tAmountOfFocusSKU.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            this.tAmountOfFocusSKU.Validating += new System.ComponentModel.CancelEventHandler(this.text_Validating);
            // 
            // tAmountOfFocusTargets
            // 
            this.tAmountOfFocusTargets.Location = new System.Drawing.Point(382, 290);
            this.tAmountOfFocusTargets.Name = "tAmountOfFocusTargets";
            this.tAmountOfFocusTargets.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tAmountOfFocusTargets.Properties.Mask.EditMask = "\\d{0,9}";
            this.tAmountOfFocusTargets.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tAmountOfFocusTargets.Size = new System.Drawing.Size(101, 20);
            this.tAmountOfFocusTargets.TabIndex = 60;
            this.tAmountOfFocusTargets.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            this.tAmountOfFocusTargets.Validating += new System.ComponentModel.CancelEventHandler(this.text_Validating);
            // 
            // lInBevBeerVolumeLimit
            // 
            this.lInBevBeerVolumeLimit.Location = new System.Drawing.Point(129, 215);
            this.lInBevBeerVolumeLimit.Name = "lInBevBeerVolumeLimit";
            this.lInBevBeerVolumeLimit.Size = new System.Drawing.Size(228, 13);
            this.lInBevBeerVolumeLimit.TabIndex = 59;
            this.lInBevBeerVolumeLimit.Text = "Порог по обьему по всем пивным СКЮ InBev:";
            // 
            // lAmountOfDistPushSKU
            // 
            this.lAmountOfDistPushSKU.Location = new System.Drawing.Point(193, 241);
            this.lAmountOfDistPushSKU.Name = "lAmountOfDistPushSKU";
            this.lAmountOfDistPushSKU.Size = new System.Drawing.Size(164, 13);
            this.lAmountOfDistPushSKU.TabIndex = 58;
            this.lAmountOfDistPushSKU.Text = "Количество СКЮ для Distr Push:";
            // 
            // lAmountOfFocusSKU
            // 
            this.lAmountOfFocusSKU.Location = new System.Drawing.Point(212, 267);
            this.lAmountOfFocusSKU.Name = "lAmountOfFocusSKU";
            this.lAmountOfFocusSKU.Size = new System.Drawing.Size(145, 13);
            this.lAmountOfFocusSKU.TabIndex = 57;
            this.lAmountOfFocusSKU.Text = "Количество Фокусных СКЮ:";
            // 
            // lAmountOfFocusTargets
            // 
            this.lAmountOfFocusTargets.Location = new System.Drawing.Point(206, 294);
            this.lAmountOfFocusTargets.Name = "lAmountOfFocusTargets";
            this.lAmountOfFocusTargets.Size = new System.Drawing.Size(151, 13);
            this.lAmountOfFocusTargets.TabIndex = 56;
            this.lAmountOfFocusTargets.Text = "Количество Фокусных целей:";
            // 
            // tTimeOfFirstVisit
            // 
            this.tTimeOfFirstVisit.EditValue = new System.DateTime(2014, 3, 31, 0, 0, 0, 0);
            this.tTimeOfFirstVisit.Location = new System.Drawing.Point(382, 136);
            this.tTimeOfFirstVisit.Name = "tTimeOfFirstVisit";
            this.tTimeOfFirstVisit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tTimeOfFirstVisit.Properties.Mask.EditMask = "HH:mm";
            this.tTimeOfFirstVisit.Size = new System.Drawing.Size(101, 20);
            this.tTimeOfFirstVisit.TabIndex = 55;
            this.tTimeOfFirstVisit.EditValueChanged += new System.EventHandler(this.tTime_EditValueChanged);
            // 
            // tTimeOfLastVisit
            // 
            this.tTimeOfLastVisit.EditValue = new System.DateTime(2014, 3, 31, 0, 0, 0, 0);
            this.tTimeOfLastVisit.Location = new System.Drawing.Point(382, 162);
            this.tTimeOfLastVisit.Name = "tTimeOfLastVisit";
            this.tTimeOfLastVisit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tTimeOfLastVisit.Properties.Mask.EditMask = "HH:mm";
            this.tTimeOfLastVisit.Size = new System.Drawing.Size(101, 20);
            this.tTimeOfLastVisit.TabIndex = 54;
            this.tTimeOfLastVisit.EditValueChanged += new System.EventHandler(this.tTime_EditValueChanged);
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(111, 114);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(246, 13);
            this.labelControl14.TabIndex = 53;
            this.labelControl14.Text = "Временные рамки для расчета Времени в полях:";
            // 
            // lTimeOfLastVisit
            // 
            this.lTimeOfLastVisit.Location = new System.Drawing.Point(88, 165);
            this.lTimeOfLastVisit.Name = "lTimeOfLastVisit";
            this.lTimeOfLastVisit.Size = new System.Drawing.Size(269, 13);
            this.lTimeOfLastVisit.TabIndex = 52;
            this.lTimeOfLastVisit.Text = "Время окончания последнего визита, учитывать до ";
            // 
            // lTimeOfFirstVisit
            // 
            this.lTimeOfFirstVisit.Location = new System.Drawing.Point(108, 139);
            this.lTimeOfFirstVisit.Name = "lTimeOfFirstVisit";
            this.lTimeOfFirstVisit.Size = new System.Drawing.Size(249, 13);
            this.lTimeOfFirstVisit.TabIndex = 51;
            this.lTimeOfFirstVisit.Text = "Время начала первого визита, учитывать после ";
            // 
            // tAmountOfFocusOLs
            // 
            this.tAmountOfFocusOLs.Location = new System.Drawing.Point(382, 68);
            this.tAmountOfFocusOLs.Name = "tAmountOfFocusOLs";
            this.tAmountOfFocusOLs.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tAmountOfFocusOLs.Properties.Mask.EditMask = "\\d{0,9}";
            this.tAmountOfFocusOLs.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tAmountOfFocusOLs.Size = new System.Drawing.Size(101, 20);
            this.tAmountOfFocusOLs.TabIndex = 48;
            this.tAmountOfFocusOLs.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            this.tAmountOfFocusOLs.Validating += new System.ComponentModel.CancelEventHandler(this.text_Validating);
            // 
            // lAmountOfFocusOLs
            // 
            this.lAmountOfFocusOLs.Location = new System.Drawing.Point(50, 71);
            this.lAmountOfFocusOLs.Name = "lAmountOfFocusOLs";
            this.lAmountOfFocusOLs.Size = new System.Drawing.Size(307, 13);
            this.lAmountOfFocusOLs.TabIndex = 47;
            this.lAmountOfFocusOLs.Text = "Количество Фокусных ТТ по эффективности оборудования:";
            // 
            // tAmountOfTheWorstOLs
            // 
            this.tAmountOfTheWorstOLs.Location = new System.Drawing.Point(382, 42);
            this.tAmountOfTheWorstOLs.Name = "tAmountOfTheWorstOLs";
            this.tAmountOfTheWorstOLs.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tAmountOfTheWorstOLs.Properties.Mask.EditMask = "\\d{0,9}";
            this.tAmountOfTheWorstOLs.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.tAmountOfTheWorstOLs.Size = new System.Drawing.Size(101, 20);
            this.tAmountOfTheWorstOLs.TabIndex = 40;
            this.tAmountOfTheWorstOLs.EditValueChanged += new System.EventHandler(this.textGeneral_EditValueChanged);
            this.tAmountOfTheWorstOLs.Validating += new System.ComponentModel.CancelEventHandler(this.text_Validating);
            // 
            // lAmountOfTheWorstOLs
            // 
            this.lAmountOfTheWorstOLs.Location = new System.Drawing.Point(236, 45);
            this.lAmountOfTheWorstOLs.Name = "lAmountOfTheWorstOLs";
            this.lAmountOfTheWorstOLs.Size = new System.Drawing.Size(121, 13);
            this.lAmountOfTheWorstOLs.TabIndex = 39;
            this.lAmountOfTheWorstOLs.Text = "Количество Худших ТТ:";
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // GeneralSettingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.panelFilter);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "GeneralSettingsView";
            this.Size = new System.Drawing.Size(1096, 424);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControls)).EndInit();
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMethodSrCalc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMethodAkbCalc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPackageDistributionPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFocusByDsm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSaleGracePeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMPList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfMP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfDistPushSKU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tInBevBeerVolumeLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfFocusSKU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfFocusTargets.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTimeOfFirstVisit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTimeOfLastVisit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfFocusOLs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAmountOfTheWorstOLs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelFilter;
        private DevExpress.XtraEditors.PanelControl panelControls;
        private DevExpress.XtraEditors.TextEdit tAmountOfTheWorstOLs;
        private DevExpress.XtraEditors.LabelControl lAmountOfTheWorstOLs;
        private DevExpress.XtraEditors.TextEdit tAmountOfFocusOLs;
        private DevExpress.XtraEditors.LabelControl lAmountOfFocusOLs;
        private DevExpress.XtraEditors.TimeEdit tTimeOfFirstVisit;
        private DevExpress.XtraEditors.TimeEdit tTimeOfLastVisit;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl lTimeOfLastVisit;
        private DevExpress.XtraEditors.LabelControl lTimeOfFirstVisit;
        private DevExpress.XtraEditors.TextEdit tAmountOfDistPushSKU;
        private DevExpress.XtraEditors.TextEdit tInBevBeerVolumeLimit;
        private DevExpress.XtraEditors.TextEdit tAmountOfFocusSKU;
        private DevExpress.XtraEditors.TextEdit tAmountOfFocusTargets;
        private DevExpress.XtraEditors.LabelControl lInBevBeerVolumeLimit;
        private DevExpress.XtraEditors.LabelControl lAmountOfDistPushSKU;
        private DevExpress.XtraEditors.LabelControl lAmountOfFocusSKU;
        private DevExpress.XtraEditors.LabelControl lAmountOfFocusTargets;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cMPList;
        private DevExpress.XtraEditors.TextEdit tAmountOfMP;
        private DevExpress.XtraEditors.LabelControl lAmountOfMP;
        private DevExpress.XtraEditors.LabelControl lMPList;
        private DevExpress.XtraEditors.TextEdit tSaleGracePeriod;
        private DevExpress.XtraEditors.LabelControl lSaleGracePeriod;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private DevExpress.XtraEditors.LabelControl lFocusByDsm;
        private DevExpress.XtraEditors.MemoEdit mFocusByDsm;
        private DevExpress.XtraEditors.TextEdit tPackageDistributionPeriod;
        private DevExpress.XtraEditors.LabelControl lPackageDistributionPeriod;
        private DevExpress.XtraEditors.ComboBoxEdit cMethodSrCalc;
        private DevExpress.XtraEditors.ComboBoxEdit cMethodAkbCalc;
        private DevExpress.XtraEditors.LabelControl lMethodSrCalc;
        private DevExpress.XtraEditors.LabelControl lMethodAkbCalc;
    }
}
