﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View
{
    [ToolboxItem(true)]
    public partial class GeneralPlanView : BasePlanView, IGeneralPlanView
    {
        private IGeneralPlanController _generalPlanController;

        private string _timeInFieldColName;
        private readonly string TimeInFieldColName = "TimeInFields";

        private GridBand bandPlans;
        
        public GeneralPlanView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.bandPlans = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            
            this.gridViewPlans.BeginUpdate();
            this.gridViewPlans.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandPlans});
            gridViewPlans.OptionsView.ShowBands = false;
            // 
            // bandPlans
            // 
            this.bandPlans.Caption = "bandPlans";
            this.bandPlans.Name = "bandPlans";
            this.bandPlans.Width = 150;
            
            this.gridViewPlans.EndUpdate();
        }

        #region Implementation of IView

        public override IViewController GetController()
        {
            return _generalPlanController;
        }

        public override void SetController(IViewController viewController)
        {
            _generalPlanController = (IGeneralPlanController)viewController;
            base.SetController(viewController);
        }

        #endregion

        public override void ConstructView(List<BindedColumnModel> columns)
        {
            ResetView();
            base.ConstructView(columns);

            foreach (var col in _columns)
            {
                BandedGridColumn addedColumn = AddColumn(col.Name + col.Id.ToString(), col.Name, col.Id.ToString(), 120, col.BandName);

                if (col.Name.Contains("Время"))
                {
                    addedColumn.ColumnEdit = rTextTime;
                    _timeInFieldColName = addedColumn.FieldName;
                    addedColumn.FieldName = TimeInFieldColName;
                }
                else
                {
                    addedColumn.ColumnEdit = rTextVolume;

                    if (col.Name.Contains("АТТ") || col.Name.Contains("ТТ"))
                    {
                        addedColumn.ColumnEdit = rTextInt;
                    }

                    if (col.Name.Contains("Среднее количество визитов в день") || col.Name.Contains("СКЮ") || col.Name.Contains("РОСЕ") || col.Name.Contains("Оборачиваемость"))
                    {
                        addedColumn.ColumnEdit = rTextTwoDigits;
                    }

                    if (col.Name.Contains("%"))
                    {
                        addedColumn.ColumnEdit = rTextPercents;
                    }
                }
            }
        }

        public override void AssignDataSource(DataTable plans)
        {
            plans.Columns.Add(TimeInFieldColName, typeof(TimeEx));

            if (!string.IsNullOrEmpty(_timeInFieldColName))
            {
                foreach (DataRow row in plans.Rows)
                {
                    if(row[_timeInFieldColName] != null && row[_timeInFieldColName] != DBNull.Value)
                    {
                        int lTimeInFields = Convert.ToInt32(row[_timeInFieldColName]);
                        TimeEx lTimeEx = new TimeEx(lTimeInFields);
                        
                        row[TimeInFieldColName] = lTimeEx;
                    }
                }
            }

            base.AssignDataSource(plans);
        }

        protected override PlanItemModel GetCurrentEditedPlan(string fieldName, object value)
        {
            if (fieldName == TimeInFieldColName)
            {
                int lColumnId = int.Parse(_timeInFieldColName);
                int lEmployeeId = Convert.ToInt32(gridViewPlans.GetFocusedRowCellValue(_colEmployeeId));

                decimal? lValue = value == DBNull.Value ? default(decimal?) : ((TimeEx)value).Value;
                
                return new PlanItemModel { ColumnId = lColumnId, EmployeeId = lEmployeeId, Value = lValue };
            }
            else
            {
                return base.GetCurrentEditedPlan(fieldName, value);
            }
        }
    }
}
