﻿using System.Collections.Generic;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.View
{
    interface IRatingsView : ISimpleControlView
    {
        void AssignChannels(List<ChannelModel> channels);
        void AssignProductSets(List<ProductListItemModel> types);
        void AssignAverageSkuList();
        void AssignNetworks();
        void AssignSkipChannels(List<ListItemModel> types);

        void AssignKpiTypes(List<ListItemModel> types);
        void AssignKPIs(List<KpiModel> types);
        void AssignColumnsList(List<RatingsColumnsModel> ratingsColumnsList);
        void AssignRatings(List<RatingsModel> ratings);

        void ShowMessage(string message, string caption);

        void OnM3FilterChange(int newM3);
        bool IsValid { get; }
        void PostEditor();

        void SaveActions();
        void RefreshActions();
        bool LeaveTabActions();
        bool SaveChangesAndProceed();
    }
}
