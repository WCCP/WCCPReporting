﻿using System;
using DevExpress.XtraEditors.Controls;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.UserControl;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View {
    public class FilterView : SimpleControlView, IFilterView {
        #region Fields

        private FilterControl _filterControl;
        private IFilterController _filterController;
        private ListTypeEnum _listType;

        #endregion

        #region Events

        public event ListItemChangeEventHandler SKUListTypeItemChanged;
        public event ListItemChangingEventHandler SKUListTypeItemChanging;
        public event DateChangeEventHandler DateChanged;
        public event DateChangingEventHandler DateChanging;
        public event ListItemChangeEventHandler CountryChanged;
        public event ListItemChangingEventHandler CountryChanging;
        public event ListItemChangeEventHandler M3Changed;
        public event ListItemChangingEventHandler M3Changing;
        public event ShowM1ChangeEventHandler ShowM1Changed;
        public event IntervalChangeEventHandler IntervalChanged;
        public event IntervalChangingEventHandler IntervalChanging;

        #endregion

        public FilterView() {
            InitializeComponent();

            AssignEvents(true);
        }

        public override void RefreshData() {
            _listType = ListTypeEnum.ProductSet;
            _filterControl.SKUListType.Properties.Items.Clear();
            _filterControl.SKUListType.Properties.Items.AddRange(_filterController.ListTypeItems);
            _filterControl.SKUListType.EditValue = _filterController.CurrentListTypeItem;
            _filterControl.PlanDateEdit.EditValue = _filterController.Date;
            _filterControl.StartDateEdit.EditValue = DateTime.Now;
            _filterControl.EndDateEdit.EditValue = DateTime.Now;
            _filterControl.CountryComboBox.Properties.Items.Clear();
            _filterControl.CountryComboBox.Properties.Items.AddRange(_filterController.CountryList);
            _filterControl.CountryComboBox.EditValue = _filterController.CurrentCountry;
            _filterControl.StaffTree.DataSource = _filterController.StaffTreeList;
            _filterControl.StaffM3.EditValue = _filterController.CurrentM3;
        }

        public override void SetController(Core.Common.Controller.IViewController viewController) {
            _filterController = (IFilterController) viewController;
        }

        private void AssignEvents(bool assign) {
            if (assign) {
                _filterControl.SKUListType.EditValueChanged += SKUListTypeValueChanged;
                _filterControl.SKUListType.EditValueChanging += SKUListTypeValueChanging;
                _filterControl.PlanDateEdit.EditValueChanged += DateValueChanged;
                _filterControl.PlanDateEdit.EditValueChanging += DateValueChanging;
                _filterControl.CountryComboBox.EditValueChanged += CountryValueChanged;
                _filterControl.CountryComboBox.EditValueChanging += CountryValueChanging;
                _filterControl.StaffM3.EditValueChanged += M3ValueChanged;
                _filterControl.StaffM3.EditValueChanging += M3ValueChanging;
                _filterControl.ShowM1CheckEdit.CheckedChanged += ShowM1CheckedChanged;
                _filterControl.StartDateEdit.EditValueChanged += IntervalValueChanged;
                _filterControl.EndDateEdit.EditValueChanged += IntervalValueChanged;
                _filterControl.StartDateEdit.EditValueChanging += IntervalValueChanging;
                _filterControl.EndDateEdit.EditValueChanging += IntervalValueChanging;
            }
            else {
                _filterControl.SKUListType.EditValueChanged -= SKUListTypeValueChanged;
                _filterControl.SKUListType.EditValueChanging -= SKUListTypeValueChanging;
                _filterControl.PlanDateEdit.EditValueChanged -= DateValueChanged;
                _filterControl.PlanDateEdit.EditValueChanging -= DateValueChanging;
                _filterControl.CountryComboBox.EditValueChanged -= CountryValueChanged;
                _filterControl.CountryComboBox.EditValueChanging -= CountryValueChanging;
                _filterControl.StaffM3.EditValueChanged -= M3ValueChanged;
                _filterControl.StaffM3.EditValueChanging -= M3ValueChanging;
                _filterControl.ShowM1CheckEdit.CheckedChanged -= ShowM1CheckedChanged;
                _filterControl.StartDateEdit.EditValueChanged -= IntervalValueChanged;
                _filterControl.EndDateEdit.EditValueChanged -= IntervalValueChanged;
                _filterControl.StartDateEdit.EditValueChanging -= IntervalValueChanging;
                _filterControl.EndDateEdit.EditValueChanging -= IntervalValueChanging;
            }
        }

        public ListTypeEnum ListType {
            get { return _listType; }
            set {
                ListTypeEnum lOldListType = _listType;
                _listType = value;
                _filterController.ChangeListType(_listType);
                if (lOldListType == _listType)
                    return;
                _filterControl.LabelListType.Text = _listType == ListTypeEnum.ProductSet ? "Набор продуктов:" : "Список СКЮ:";
                _filterControl.SKUListType.Properties.Items.Clear();
                _filterControl.SKUListType.Properties.Items.AddRange(_filterController.ListTypeItems);
                _filterControl.SKUListType.EditValue = _filterController.CurrentListTypeItem;
            }
        }

        private void SKUListTypeValueChanging(object sender, ChangingEventArgs e) {
            ListItemChangingEventArgs args = new ListItemChangingEventArgs(e.OldValue == null ? -2 : ((ListItemModel) e.OldValue).Id,
                e.NewValue == null ? -2 : ((ListItemModel) e.NewValue).Id);

            OnSKUListTypeItemChanging(this, args);
            e.Cancel = args.IsCanceled;
        }

        private void SKUListTypeValueChanged(object sender, EventArgs e) {
            _filterController.CurrentListTypeItem = (ListItemModel) _filterControl.SKUListType.EditValue;
        }

        private void DateValueChanged(object sender, EventArgs e) {
            _filterController.Date = _filterControl.PlanDateEdit.DateTime.Date;
        }

        private void DateValueChanging(object sender, ChangingEventArgs e) {
            DateChangingEventArgs args = new DateChangingEventArgs(e.OldValue == null ? DateTime.Now : ((DateTime) e.OldValue),
                e.NewValue == null ? DateTime.Now : ((DateTime) e.NewValue));

            OnDateChanging(this, args);
            e.Cancel = args.IsCanceled;
        }

        private void CountryValueChanging(object sender, ChangingEventArgs e) {
            ListItemChangingEventArgs args = new ListItemChangingEventArgs(e.OldValue == null ? -2 : ((CountryModel) e.OldValue).Id,
                e.NewValue == null ? -2 : ((CountryModel) e.NewValue).Id);

            OnCountryChanging(this, args);
            e.Cancel = args.IsCanceled;
        }

        private void CountryValueChanged(object sender, EventArgs e) {
            _filterController.CurrentCountry = (CountryModel) _filterControl.CountryComboBox.EditValue;
        }

        private void M3ValueChanging(object sender, ChangingEventArgs e) {
            ListItemChangingEventArgs args = new ListItemChangingEventArgs(e.OldValue == null ? -2 : ((StaffTreeModel) e.OldValue).Id,
                e.NewValue == null ? -2 : ((StaffTreeModel) e.NewValue).Id);

            OnM3Changing(this, args);
            e.Cancel = args.IsCanceled;
        }

        private void M3ValueChanged(object sender, EventArgs e) {
            _filterController.CurrentM3 = (StaffTreeModel) _filterControl.StaffM3.EditValue;
        }

        private void ShowM1CheckedChanged(object sender, EventArgs e) {
            _filterController.ShowM1 = _filterControl.ShowM1CheckEdit.Checked;
        }

        public void SetCheckShowM1Value(bool showM1)
        {
            _filterControl.ShowM1CheckEdit.Checked = showM1;
        }

        private void IntervalValueChanged(object sender, EventArgs e) {
            if (sender.Equals(_filterControl.StartDateEdit)) {
                _filterControl.EndDateEdit.Properties.MinValue = !string.IsNullOrEmpty(_filterControl.StartDateEdit.Text)
                                                                     ? _filterControl.StartDateEdit.DateTime
                                                                     : DateTime.MinValue;
            }

            if (sender.Equals(_filterControl.EndDateEdit)) {
                _filterControl.StartDateEdit.Properties.MaxValue = !string.IsNullOrEmpty(_filterControl.EndDateEdit.Text)
                                                                       ? _filterControl.EndDateEdit.DateTime
                                                                       : DateTime.MaxValue;
            }

            _filterController.StartDate = _filterControl.StartDateEdit.DateTime.Date;
            _filterController.EndDate = _filterControl.EndDateEdit.DateTime.Date;
        }

        private void IntervalValueChanging(object sender, ChangingEventArgs e) {
            IntervalChangingEventArgs args = new IntervalChangingEventArgs(e.OldValue == null ? DateTime.Now : ((DateTime) e.OldValue),
                e.NewValue == null ? DateTime.Now : ((DateTime) e.NewValue));

            OnIntervalChanging(this, args);
            e.Cancel = args.IsCanceled;
        }

        #region Implementation of IFilterView

        public bool ShowListType {
            get { return _filterControl.ShowListType; }
            set {
                if (_filterControl.ShowListType == value)
                    return;
                _filterControl.ShowListType = value;
            }
        }

        public bool ShowCheckM1 {
            get { return _filterControl.ShowDisplayM1; }
            set {
                if (_filterControl.ShowDisplayM1 == value)
                    return;
                _filterControl.ShowDisplayM1 = value;
            }
        }

        public bool ShowPlanDate {
            get { return _filterControl.ShowPlanDate; }
            set {
                if (_filterControl.ShowPlanDate == value)
                    return;
                _filterControl.ShowPlanDate = value;
            }
        }

        public bool ShowDatePeriod {
            get { return _filterControl.ShowDateInterval; }
            set {
                if (_filterControl.ShowDateInterval == value)
                    return;
                _filterControl.ShowDateInterval = value;
            }
        }

        public void OnSKUListTypeItemChanged(object sender, ListItemChangeEventArgs args) {
            ListItemChangeEventHandler lHandler = SKUListTypeItemChanged;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnSKUListTypeItemChanging(object sender, ListItemChangingEventArgs args) {
            ListItemChangingEventHandler lHandler = SKUListTypeItemChanging;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnDateChanged(object sender, DateChangeEventArgs args) {
            DateChangeEventHandler lHandler = DateChanged;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnDateChanging(object sender, DateChangingEventArgs args) {
            DateChangingEventHandler lHandler = DateChanging;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnCountryChanged(object sender, ListItemChangeEventArgs args) {
            ListItemChangeEventHandler lHandler = CountryChanged;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnCountryChanging(object sender, ListItemChangingEventArgs args) {
            ListItemChangingEventHandler lHandler = CountryChanging;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnM3Changed(object sender, ListItemChangeEventArgs args) {
            ListItemChangeEventHandler lHandler = M3Changed;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnM3Changing(object sender, ListItemChangingEventArgs args) {
            ListItemChangingEventHandler lHandler = M3Changing;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnShowM1Changed(object sender, ShowM1ChangeEventArgs args) {
            ShowM1ChangeEventHandler lHandler = ShowM1Changed;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnIntervalChanged(object sender, IntervalChangeEventArgs args) {
            IntervalChangeEventHandler lHandler = IntervalChanged;
            if (lHandler != null)
                lHandler(this, args);
        }

        public void OnIntervalChanging(object sender, IntervalChangingEventArgs args) {
            IntervalChangingEventHandler lHandler = IntervalChanging;
            if (lHandler != null)
                lHandler(this, args);
        }

        #endregion

        private void InitializeComponent() {
            this._filterControl = new SoftServe.Reports.MorningMeeting.UserControl.FilterControl();
            ((System.ComponentModel.ISupportInitialize) (this._filterControl)).BeginInit();
            this.SuspendLayout();
            // 
            // _filterControl
            // 
            this._filterControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._filterControl.Location = new System.Drawing.Point(0, 0);
            this._filterControl.Name = "_filterControl";
            this._filterControl.ShowListType = false;
            this._filterControl.Size = new System.Drawing.Size(640, 64);
            this._filterControl.TabIndex = 0;
            // 
            // FilterView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this._filterControl);
            this.Name = "FilterView";
            this.Size = new System.Drawing.Size(640, 64);
            ((System.ComponentModel.ISupportInitialize) (this._filterControl)).EndInit();
            this.ResumeLayout(false);
        }
    }
}