﻿using System.Collections.Generic;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.View
{
    interface IFocusGoalsView : ISimpleControlView
    {
        void AssignChannels(List<ChannelModel> channels);
        void AssignProductTree(List<ProductTreeModel> list);

        void AssignGoals(List<FocusGoalModel> goals);

        void DisplayManualGoalDetails(ManualGoalDetailsModel details);
        void GetManualGoalDetails(ManualGoalDetailsModel details);

        void DisplayAutoGoalDetails(AutoGoalDetailsModel details);
        void GatAutoGoalDetails(AutoGoalDetailsModel details);

        void AssignGoalTypes(List<ListItemModel> types);
        void AssignAutoGoalTypes(List<ListItemModel> types);

        void OnM3FilterChange(int newM3);

        void SaveActions();
        void RefreshActions();
        bool LeaveTabActions();
        bool SaveChangesAndProceed();

        void PostEditor();
        bool IsValid { get; }

        void OnResize();
    }
}
