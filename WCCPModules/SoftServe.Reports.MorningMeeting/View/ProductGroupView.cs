using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Columns;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.UserControl;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View {
    internal class ProductGroupView : BaseSKUConfigView, IProductGroupView {
        private IProductGroupController _productViewController;

        public ProductGroupView() {
            SkuTree.CheckNodes = CheckNodesEnum.Independent;
        }

        #region Implementation of IView

        public override IViewController GetController() {
            return _productViewController;
        }

        public override void SetController(IViewController viewController) {
            _productViewController = (IProductGroupController) viewController;
            base.SetController(viewController);
        }

        public override void RefreshData() {
            SkuGridControl.DataSource = _productViewController.ProductGroups;
            SkuTree.DataSource = _productViewController.Products;
            SkuTree.ExpandAll();
            SkuTree.CollapseAll();
            base.RefreshData();
        }

        public override bool RefreshAction() {
            if (base.RefreshAction())
                _productViewController.LoadData(true);

            return true;
        }

        public override bool LeaveTabActions() {
            SkuGridView.PostEditor();

            bool lAllowLeavePage = true;
            bool isValid = true;

            if (_baseSKUConfigController.HasUnsavedChanges) {
                DialogResult res = XtraMessageBox.Show("�� ��� ������� ���������. ���������?", "�� ��� ������� ���������",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                switch (res) {
                    case DialogResult.Yes:
                        isValid = SaveData();
                        break;
                    case DialogResult.No:
                        _productViewController.LoadData(true);
                        break;
                    case DialogResult.Cancel:
                        lAllowLeavePage = false;
                        break;
                }
            }

            return lAllowLeavePage && isValid;
        }

        #endregion

        #region SKU Tree events and methods

        public override void SkuTreeAfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e) {
            base.SkuTreeAfterCheckNode(sender, e);

            SkuTree.BeginUpdate();
            AttachSkuTreeEvents(false);
            try {
                TreeListColumn lColEditable = SkuTree.Columns.ColumnByFieldName("Editable");
                TreeUtils.OnNodeCheckRecursive(e.Node, e.Node.Checked, lColEditable);
            }
            finally {
                AttachSkuTreeEvents(true);
                SkuTree.EndUpdate();
            }
        }

        #endregion

        public override bool CanModifyCountryLevel {
            get { return _productViewController.IsFilterCountryLevel && RoleManager.IsInRole(MMRoleEnum.SkuM5); }
        }

        public override bool CanModifyM3Level {
            get { return !_productViewController.IsFilterCountryLevel && RoleManager.IsInRole(MMRoleEnum.SkuM3); }
        }
    }
}