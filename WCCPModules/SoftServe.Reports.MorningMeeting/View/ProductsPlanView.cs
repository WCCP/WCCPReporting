﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.UserControl;

namespace SoftServe.Reports.MorningMeeting.View
{
    [ToolboxItem(true)]
    public partial class ProductsPlanView : BasePlanView, IProductsPlanView
    {
        private IProductsPlanController _productsPlanController;

        private GridBand bandVolume;
        private GridBand bandVolumePieces;
        private GridBand bandATT;
        private GridBand bandCoverage;
        private GridBand bandCoveragePieces;
        private GridBand bandTotal;
        private GridBand bandAvgSku;
        private GridBand bandDistrPush;

        public ProductsPlanView()
        {
            InitializeComponent();
        }

        #region Implementation of IView

        public override IViewController GetController()
        {
            return _productsPlanController;
        }

        public override void SetController(IViewController viewController)
        {
            _productsPlanController = (IProductsPlanController)viewController;
            base.SetController(viewController);
        }

        #endregion

        public override void ConstructView(List<BindedColumnModel> columns)
        {
            ResetView();
            base.ConstructView(columns);

            foreach (var col in _columns)
            {
                GridColumn addedColumn = AddColumn(string.Format("col{0}", col.Id), col.Name, col.Id.ToString(), 120, col.BandName);

                addedColumn.ColumnEdit = rTextVolume;

                if (col.BandName.Contains("АТТ") || col.Name.Contains("АТТ") || col.BandName.Contains(", штук") || col.Name.Contains(", штук"))
                {
                    addedColumn.ColumnEdit = rTextInt;
                }

                if (col.BandName.Contains("СКЮ") || col.Name.Contains("СКЮ") || col.Name.Contains("Микс"))
                {
                    addedColumn.ColumnEdit = rTextTwoDigits;
                }

                if (col.BandName.Contains(" штук, %"))
                {
                    addedColumn.ColumnEdit = rTextPercentOneDigit;
                }
                else
                    if (col.BandName.Contains("%") || col.Name.Contains("План по покрытию Total"))
                    {
                        addedColumn.ColumnEdit = rTextPercents;
                    }
            }
        }

        private void InitializeComponent()
        {
            this.bandVolume = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandVolumePieces = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandATT = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandCoverage = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandCoveragePieces = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandTotal = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandAvgSku = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandDistrPush = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();

            this.gridViewPlans.BeginUpdate();
            this.gridViewPlans.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandVolume,
            this.bandVolumePieces,
            this.bandATT,
            this.bandCoverage,
            this.bandCoveragePieces,
            this.bandTotal,
            this.bandAvgSku,
            this.bandDistrPush});
            this.gridViewPlans.ColumnPanelRowHeight = 35;
            // 
            // bandVolume
            // 
            this.bandVolume.Caption = "Объемная цель";
            this.bandVolume.Name = "bandVolume";
            this.bandVolume.Width = 150;
            // 
            // bandVolumePieces
            // 
            this.bandVolumePieces.Caption = "Объемная цель, штук";
            this.bandVolumePieces.Name = "bandVolumePieces";
            this.bandVolumePieces.Width = 150;
            // 
            // bandATT
            // 
            this.bandATT.Caption = "Цели по АТТ";
            this.bandATT.Name = "bandATT";
            this.bandATT.Width = 150;
            // 
            // bandCoverage
            // 
            this.bandCoverage.Caption = "План по покрытию, %";
            this.bandCoverage.Name = "bandCoverage";
            this.bandCoverage.Width = 150;
            // 
            // bandCoveragePieces
            // 
            this.bandCoveragePieces.Caption = "План по покрытию штук, %";
            this.bandCoveragePieces.Name = "bandCoveragePieces";
            this.bandCoveragePieces.Width = 150;
            // 
            // bandTotal
            // 
            this.bandTotal.Caption = "Total";
            this.bandTotal.Name = "bandTotal";
            this.bandTotal.Width = 150;
            // 
            // bandAvgSku
            // 
            this.bandAvgSku.Caption = "Среднее СКЮ";
            this.bandAvgSku.Name = "bandAvgSku";
            this.bandAvgSku.Width = 150;
            // 
            // bandDistrPush
            // 
            this.bandDistrPush.Caption = "Дистр пуш";
            this.bandDistrPush.Name = "bandDistrPush";
            this.bandDistrPush.Width = 150;

            this.gridViewPlans.EndUpdate();
        }
    }
}
