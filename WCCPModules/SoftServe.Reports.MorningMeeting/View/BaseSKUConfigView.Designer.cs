﻿namespace SoftServe.Reports.MorningMeeting.View {
    partial class BaseSKUConfigView {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.panelFilter = new DevExpress.XtraEditors.PanelControl();
            this.splitContainer = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlSKU = new DevExpress.XtraGrid.GridControl();
            this.gridViewSKU = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colListType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountryId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSetId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSKUGroupId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSKUGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChannelMask = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductCnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.pnlButtons = new DevExpress.XtraEditors.PanelControl();
            this.btnCopy = new DevExpress.XtraEditors.SimpleButton();
            this.btnUp = new DevExpress.XtraEditors.SimpleButton();
            this.btnDown = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.treeSKU = new SoftServe.Reports.MorningMeeting.UserControl.CheckedTree();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).BeginInit();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeSKU)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFilter
            // 
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFilter.Location = new System.Drawing.Point(0, 0);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(704, 64);
            this.panelFilter.TabIndex = 0;
            // 
            // splitContainer
            // 
            this.splitContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 64);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Panel1.Controls.Add(this.gridControlSKU);
            this.splitContainer.Panel1.Controls.Add(this.pnlButtons);
            this.splitContainer.Panel1.Text = "Panel1";
            this.splitContainer.Panel2.Controls.Add(this.treeSKU);
            this.splitContainer.Panel2.Text = "Panel2";
            this.splitContainer.Size = new System.Drawing.Size(704, 336);
            this.splitContainer.SplitterPosition = 510;
            this.splitContainer.TabIndex = 1;
            this.splitContainer.Text = "splitContainerControl1";
            // 
            // gridControlSKU
            // 
            this.gridControlSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSKU.Location = new System.Drawing.Point(0, 49);
            this.gridControlSKU.MainView = this.gridViewSKU;
            this.gridControlSKU.Name = "gridControlSKU";
            this.gridControlSKU.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riCheckEdit});
            this.gridControlSKU.Size = new System.Drawing.Size(510, 287);
            this.gridControlSKU.TabIndex = 1;
            this.gridControlSKU.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSKU});
            // 
            // gridViewSKU
            // 
            this.gridViewSKU.Appearance.EvenRow.BackColor = System.Drawing.Color.Lavender;
            this.gridViewSKU.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AliceBlue;
            this.gridViewSKU.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewSKU.Appearance.OddRow.BackColor = System.Drawing.Color.Wheat;
            this.gridViewSKU.Appearance.OddRow.BackColor2 = System.Drawing.Color.OldLace;
            this.gridViewSKU.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewSKU.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colListType,
            this.colCountryId,
            this.colM3Id,
            this.colSetId,
            this.colSKUGroupId,
            this.colSKUGroupName,
            this.colSortOrder,
            this.colChannelMask,
            this.colProductCnId});
            this.gridViewSKU.GridControl = this.gridControlSKU;
            this.gridViewSKU.GroupCount = 1;
            this.gridViewSKU.GroupFormat = "{1} {2}";
            this.gridViewSKU.Name = "gridViewSKU";
            this.gridViewSKU.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewSKU.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewSKU.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewSKU.OptionsCustomization.AllowFilter = false;
            this.gridViewSKU.OptionsCustomization.AllowGroup = false;
            this.gridViewSKU.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewSKU.OptionsCustomization.AllowSort = false;
            this.gridViewSKU.OptionsDetail.AllowZoomDetail = false;
            this.gridViewSKU.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewSKU.OptionsDetail.ShowDetailTabs = false;
            this.gridViewSKU.OptionsDetail.SmartDetailExpand = false;
            this.gridViewSKU.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridViewSKU.OptionsFilter.AllowFilterEditor = false;
            this.gridViewSKU.OptionsFilter.AllowMRUFilterList = false;
            this.gridViewSKU.OptionsFilter.ColumnFilterPopupRowCount = 100;
            this.gridViewSKU.OptionsFilter.ColumnFilterPopupMaxRecordsCount = 200;
            this.gridViewSKU.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gridViewSKU.OptionsMenu.EnableColumnMenu = false;
            this.gridViewSKU.OptionsMenu.EnableFooterMenu = false;
            this.gridViewSKU.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewSKU.OptionsView.ShowDetailButtons = false;
            this.gridViewSKU.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSKU.OptionsView.ShowGroupPanel = false;
            this.gridViewSKU.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colM3Id, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewSKU.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewSKU_CustomDrawGroupRow);
            this.gridViewSKU.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewSKU_RowStyle);
            this.gridViewSKU.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewSKU_ShowingEditor);
            this.gridViewSKU.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewSKU_FocusedRowChanged);
            this.gridViewSKU.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewSKU_CellValueChanged);
            this.gridViewSKU.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridViewSKU_BeforeLeaveRow);
            this.gridViewSKU.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewSKU_CustomUnboundColumnData);
            // 
            // colListType
            // 
            this.colListType.Caption = "List Type";
            this.colListType.FieldName = "ListType";
            this.colListType.Name = "colListType";
            // 
            // colCountryId
            // 
            this.colCountryId.Caption = "Country ID";
            this.colCountryId.FieldName = "CountryId";
            this.colCountryId.Name = "colCountryId";
            // 
            // colM3Id
            // 
            this.colM3Id.Caption = "M3 ID";
            this.colM3Id.FieldName = "M3Id";
            this.colM3Id.Name = "colM3Id";
            // 
            // colSetId
            // 
            this.colSetId.Caption = "Set Id";
            this.colSetId.FieldName = "SetId";
            this.colSetId.Name = "colSetId";
            // 
            // colSKUGroupId
            // 
            this.colSKUGroupId.Caption = "SKU Group Id";
            this.colSKUGroupId.FieldName = "SKUGroupId";
            this.colSKUGroupId.Name = "colSKUGroupId";
            // 
            // colSKUGroupName
            // 
            this.colSKUGroupName.Caption = "Цель";
            this.colSKUGroupName.FieldName = "SKUGroupName";
            this.colSKUGroupName.Name = "colSKUGroupName";
            this.colSKUGroupName.Visible = true;
            this.colSKUGroupName.VisibleIndex = 0;
            this.colSKUGroupName.Width = 100;
            // 
            // colSortOrder
            // 
            this.colSortOrder.Caption = "Sort Order";
            this.colSortOrder.FieldName = "SortOrder";
            this.colSortOrder.Name = "colSortOrder";
            // 
            // colChannelMask
            // 
            this.colChannelMask.Caption = "Channel Mask";
            this.colChannelMask.FieldName = "ChannelMask";
            this.colChannelMask.Name = "colChannelMask";
            // 
            // colProductCnId
            // 
            this.colProductCnId.Caption = "ProductCn ID";
            this.colProductCnId.FieldName = "ProductCn_ID";
            this.colProductCnId.Name = "colProductCnId";
            // 
            // riCheckEdit
            // 
            this.riCheckEdit.AutoHeight = false;
            this.riCheckEdit.Name = "riCheckEdit";
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnCopy);
            this.pnlButtons.Controls.Add(this.btnUp);
            this.pnlButtons.Controls.Add(this.btnDown);
            this.pnlButtons.Controls.Add(this.btnDelete);
            this.pnlButtons.Controls.Add(this.btnAdd);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(510, 49);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnCopy
            // 
            this.btnCopy.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.copy;
            this.btnCopy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCopy.Location = new System.Drawing.Point(136, 8);
            this.btnCopy.Margin = new System.Windows.Forms.Padding(2);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(30, 32);
            this.btnCopy.TabIndex = 40;
            this.btnCopy.ToolTip = "Копировать";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnUp
            // 
            this.btnUp.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.arrow_up_blue;
            this.btnUp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnUp.Location = new System.Drawing.Point(104, 8);
            this.btnUp.Margin = new System.Windows.Forms.Padding(2);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(30, 32);
            this.btnUp.TabIndex = 39;
            this.btnUp.ToolTip = "Переместить вверх";
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.arrow_down_blue;
            this.btnDown.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDown.Location = new System.Drawing.Point(72, 8);
            this.btnDown.Margin = new System.Windows.Forms.Padding(2);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(30, 32);
            this.btnDown.TabIndex = 38;
            this.btnDown.ToolTip = "Переместить вниз";
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.delete;
            this.btnDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDelete.Location = new System.Drawing.Point(40, 8);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(30, 32);
            this.btnDelete.TabIndex = 33;
            this.btnDelete.ToolTip = "Удалить";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.add;
            this.btnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(8, 8);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(30, 32);
            this.btnAdd.TabIndex = 31;
            this.btnAdd.ToolTip = "Добавить";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // treeSKU
            // 
            this.treeSKU.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.treeSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeSKU.KeyFieldName = "Id";
            this.treeSKU.Location = new System.Drawing.Point(0, 0);
            this.treeSKU.Name = "treeSKU";
            this.treeSKU.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.treeSKU.OptionsBehavior.Editable = false;
            this.treeSKU.OptionsView.ShowCheckBoxes = true;
            this.treeSKU.OptionsView.ShowColumns = false;
            this.treeSKU.OptionsView.ShowHorzLines = false;
            this.treeSKU.OptionsView.ShowIndicator = false;
            this.treeSKU.OptionsView.ShowVertLines = false;
            this.treeSKU.ParentFieldName = "ParentId";
            this.treeSKU.RootValue = null;
            this.treeSKU.Size = new System.Drawing.Size(188, 336);
            this.treeSKU.TabIndex = 0;
            // 
            // timer
            // 
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // BaseSKUConfigView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panelFilter);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "BaseSKUConfigView";
            this.Size = new System.Drawing.Size(704, 400);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).EndInit();
            this.pnlButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeSKU)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelFilter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainer;
        private DevExpress.XtraEditors.PanelControl pnlButtons;
        private DevExpress.XtraGrid.GridControl gridControlSKU;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSKU;
        private UserControl.CheckedTree treeSKU;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnDown;
        private DevExpress.XtraEditors.SimpleButton btnUp;
        private DevExpress.XtraEditors.SimpleButton btnCopy;
        private DevExpress.XtraGrid.Columns.GridColumn colListType;
        private DevExpress.XtraGrid.Columns.GridColumn colCountryId;
        private DevExpress.XtraGrid.Columns.GridColumn colM3Id;
        private DevExpress.XtraGrid.Columns.GridColumn colSetId;
        private DevExpress.XtraGrid.Columns.GridColumn colSKUGroupId;
        private DevExpress.XtraGrid.Columns.GridColumn colSKUGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colSortOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colChannelMask;
        private DevExpress.XtraGrid.Columns.GridColumn colProductCnId;
        protected DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit riCheckEdit;
        private System.Windows.Forms.Timer timer;
    }
}
