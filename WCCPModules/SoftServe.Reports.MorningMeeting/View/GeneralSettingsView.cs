﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils.Win;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Popup;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View
{
    [ToolboxItem(true)]
    public partial class GeneralSettingsView : SimpleControlView, IGeneralSettingsView
    {
        private IGeneralSettingsController _generalSettingsController;

        private bool _showErrors = false;
        private bool _isLoading = false;

        public GeneralSettingsView()
        {
            InitializeComponent();
        }

        #region Implementation of IView

        public override IViewController GetController()
        {
            return _generalSettingsController;
        }

        public override void SetController(IViewController viewController)
        {
            _generalSettingsController = (IGeneralSettingsController)viewController;
        }

        public override object GetDockObject(ISimpleControlView controlView)
        {
            return panelFilter;
        }

        #endregion

        #region Implementation of IGeneralSettingsView

        public void AssignSettings(GeneralSettingsModel settings)
        {
            _isLoading = true;

            if (settings == null || settings.AmountOfTheWorstOLs == -1)
            {
                tAmountOfTheWorstOLs.Text = string.Empty;
                tAmountOfFocusOLs.Text = string.Empty;
                tTimeOfFirstVisit.Time = DateTime.Now.Date;
                tTimeOfLastVisit.Time = DateTime.Now.Date;
                tInBevBeerVolumeLimit.Text = string.Empty;
                tAmountOfDistPushSKU.Text = string.Empty;
                tAmountOfFocusSKU.Text = string.Empty;
                tAmountOfFocusTargets.Text = string.Empty;

                tAmountOfMP.Text = string.Empty;
                mFocusByDsm.Text = string.Empty;
            }
            else
            {
                tAmountOfTheWorstOLs.Text = settings.AmountOfTheWorstOLs.ToString();
                tAmountOfFocusOLs.Text = settings.AmountOfFocusOLs.ToString();
                tTimeOfFirstVisit.Time = settings.TimeOfFirstVisit;
                tTimeOfLastVisit.Time = settings.TimeOfLastVisit;
                tInBevBeerVolumeLimit.Text = settings.InBevBeerVolumeLimit.ToString();
                tAmountOfDistPushSKU.Text = settings.AmountOfDistrPushSKU.ToString();
                tAmountOfFocusSKU.Text = settings.AmountOfFocusSKU.ToString();
                tAmountOfFocusTargets.Text = settings.AmountOfFocusTargets.ToString();

                tAmountOfMP.Text = settings.AmountOfMP.ToString();
                mFocusByDsm.Text = settings.FocusByDSM;
            }

            lFocusByDsm.Visible = !_generalSettingsController.IsCountryLevel;
            mFocusByDsm.Visible = !_generalSettingsController.IsCountryLevel;

            _isLoading = false;
        }

        public void GetSettings(GeneralSettingsModel settings)
        {
            if (!string.IsNullOrEmpty(tAmountOfTheWorstOLs.Text))
            {
                settings.AmountOfTheWorstOLs = int.Parse(tAmountOfTheWorstOLs.Text);
            }

            if (!string.IsNullOrEmpty(tAmountOfFocusOLs.Text))
            {
                settings.AmountOfFocusOLs = int.Parse(tAmountOfFocusOLs.Text);
            }

            settings.TimeOfFirstVisit = tTimeOfFirstVisit.Time;
            settings.TimeOfLastVisit = tTimeOfLastVisit.Time;

            if (!string.IsNullOrEmpty(tInBevBeerVolumeLimit.Text))
            {
                settings.InBevBeerVolumeLimit = decimal.Parse(tInBevBeerVolumeLimit.Text);
            }

            if (!string.IsNullOrEmpty(tAmountOfDistPushSKU.Text))
            {
                settings.AmountOfDistrPushSKU = int.Parse(tAmountOfDistPushSKU.Text);
            }

            if (!string.IsNullOrEmpty(tAmountOfFocusSKU.Text))
            {
                settings.AmountOfFocusSKU = int.Parse(tAmountOfFocusSKU.Text);
            }

            if (!string.IsNullOrEmpty(tAmountOfFocusTargets.Text))
            {
                settings.AmountOfFocusTargets = int.Parse(tAmountOfFocusTargets.Text);
            }

            if (!string.IsNullOrEmpty(tAmountOfMP.Text))
            {
                settings.AmountOfMP = int.Parse(tAmountOfMP.Text);
            }

            settings.FocusByDSM = mFocusByDsm.Text;
        }

        public void GetCountrySettings(CountryLevelSettingsModel settings)
        {
            settings.SalesGracePeriod = string.IsNullOrEmpty(tSaleGracePeriod.Text) ? 0 : int.Parse(tSaleGracePeriod.Text);
            settings.PackageDistributionPeriod = string.IsNullOrEmpty(tPackageDistributionPeriod.Text) ? 0 : int.Parse(tPackageDistributionPeriod.Text);
            settings.AkbCalcMethodId = string.IsNullOrEmpty(cMethodAkbCalc.Text) ? 0 : (cMethodAkbCalc.SelectedItem as ListItemModel).Id;
            settings.SrCalcMethodId = string.IsNullOrEmpty(cMethodSrCalc.Text) ? 0 : (cMethodSrCalc.SelectedItem as ListItemModel).Id;
        }

        public void AssignCountrySettings(CountryLevelSettingsModel settings)
        {
            _isLoading = true;

            if (settings != null)
            {
                tSaleGracePeriod.Text = settings.SalesGracePeriod.ToString();
                tPackageDistributionPeriod.Text = settings.PackageDistributionPeriod.ToString();
                cMethodAkbCalc.SelectedIndex = GetItemIndex(cMethodAkbCalc, settings.AkbCalcMethodId);
                cMethodSrCalc.SelectedIndex = GetItemIndex(cMethodSrCalc, settings.SrCalcMethodId);
            }
            else
            {
                tSaleGracePeriod.Text = string.Empty;
                tPackageDistributionPeriod.Text = string.Empty;
                cMethodAkbCalc.SelectedIndex = -1;
                cMethodSrCalc.SelectedIndex = -1;
            }

            lSaleGracePeriod.Visible = RoleManager.IsInRole(MMRoleEnum.GeneralM5) && _generalSettingsController.IsCountryLevel;
            tSaleGracePeriod.Visible = RoleManager.IsInRole(MMRoleEnum.GeneralM5) && _generalSettingsController.IsCountryLevel;
            lPackageDistributionPeriod.Visible = RoleManager.IsInRole(MMRoleEnum.GeneralM5) && _generalSettingsController.IsCountryLevel;
            tPackageDistributionPeriod.Visible = RoleManager.IsInRole(MMRoleEnum.GeneralM5) && _generalSettingsController.IsCountryLevel;

            AlignCalculationMethodsDetails();

            _isLoading = false;
        }

        public void GetSelectedMP(List<ActionListModel> programs, int countryId, int m3Id)
        {
            programs.Clear();

            foreach (var item in GetSelectedItems(cMPList))
            {
                ActionListModel action = (ActionListModel.CreateInstance());
                action.CountryId = countryId;
                action.M3Id = m3Id;
                action.ItemId = item;

                programs.Add(action);
            }
        }

        public void AssignSelectedMP(List<ActionListModel> programs)
        {
            _isLoading = true;

            if (programs != null)
            {
                SelectItems(cMPList, programs.Select(p => p.ItemId).ToList());
            }

            _isLoading = false;
        }

        public void LoadMPs(List<ListItemModel> programs)
        {
            LoadItems(cMPList, programs);
        }

        public void LoadAkbCalcMethods(List<ListItemModel> methods)
        {
            cMethodAkbCalc.Properties.Items.Clear();
            cMethodAkbCalc.Properties.Items.AddRange(methods);
        }

        public void LoadSrCalcMethods(List<ListItemModel> methods)
        {
            cMethodSrCalc.Properties.Items.Clear();
            cMethodSrCalc.Properties.Items.AddRange(methods);
        }

        public bool ValidateRecord()
        {
            _showErrors = true;
            ValidateChildren(ValidationConstraints.Visible);

            bool lIsValid = true;

            foreach (Control control in panelControls.Controls)
            {
                lIsValid = errorProvider.GetError(control).Length == 0;

                if (!lIsValid)
                {
                    break;
                }
            }

            if (lIsValid)
            {
                _showErrors = false;
            }

            return lIsValid;
        }

        public void HideValidation()
        {
            foreach (Control control in panelControls.Controls)
            {
                errorProvider.SetError(control, "");
            }
        }

        public void SaveActions()
        {
            if (ValidateRecord())
            {
                _generalSettingsController.UpdateCurrentSettingSet();
                _generalSettingsController.SaveData();
                ShowMessChangesWereSaved();
            }
        }

        public void RefreshActions()
        {
            if (_generalSettingsController.HasUnsavedChanges)
            {
                if (SaveChangesAndProceed())
                {
                    return;
                }
            }

            _showErrors = false;
            ValidateChildren(ValidationConstraints.Visible);

            _generalSettingsController.LoadData(true);
        }

        public virtual bool LeaveTabActions()
        {
            bool lAllowLeavePage = true;
            bool isValid = true;

            if (_generalSettingsController.HasUnsavedChanges)
            {
                DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                       MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                switch (res)
                {
                    case DialogResult.Yes:
                        {
                            isValid = ValidateRecord();

                            if (isValid)
                            {
                                _generalSettingsController.UpdateCurrentSettingSet();
                                _generalSettingsController.SaveData();
                                ShowMessChangesWereSaved();
                            }
                        }
                        break;
                    case DialogResult.No:
                        _generalSettingsController.LoadData(true);
                        break;
                    case DialogResult.Cancel:
                        lAllowLeavePage = false;
                        break;
                }
            }

            return lAllowLeavePage && isValid;
        }

        public void SetAccessibility(bool canEdit)
        {
            foreach (Control control in panelControls.Controls)
            {
                if (control is TextEdit)
                {
                    (control as TextEdit).Enabled = canEdit;
                }
            }

            cMethodAkbCalc.Enabled = cMethodSrCalc.Enabled = RoleManager.IsInRole(MMRoleEnum.GeneralM5) && _generalSettingsController.IsCountryLevel;
        }

        public bool SaveChangesAndProceed()
        {
            bool isValid = true;
            DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                   MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                isValid = ValidateRecord();

                if (isValid)
                {
                    _generalSettingsController.UpdateCurrentSettingSet();
                    _generalSettingsController.SaveData();
                    ShowMessChangesWereSaved();
                }
            }

            return res == DialogResult.Cancel || !isValid;
        }

        #endregion

        #region Event Handling

        private void tAmountOfMP_Leave(object sender, EventArgs e)
        {
            if (GetCount(tAmountOfMP) < GetSelectedItemsCount(cMPList))
            {
                DeselectAll(cMPList);
            }
        }

        private void text_Validating(object sender, CancelEventArgs e)
        {
            TextEdit edit = sender as TextEdit;

            if (_showErrors && string.IsNullOrEmpty(edit.Text))
            {
                errorProvider.SetError(edit, "Введите значение");
            }
            else
            {
                errorProvider.SetError(edit, "");
            }
        }

        private void cMPList_Validating(object sender, CancelEventArgs e)
        {
            CheckedComboBoxEdit edit = sender as CheckedComboBoxEdit;

            if (_showErrors && GetSelectedItemsCount(edit) < GetCount(tAmountOfMP))
            {
                errorProvider.SetError(edit, "Недостаточно МП");
            }
            else
            {
                errorProvider.SetError(edit, "");
            }
        }

        private void text_EditValueChanged(object sender, EventArgs e)
        {
            if (!_isLoading)
            {
                _generalSettingsController.HasUnsavedChanges = true;
                _generalSettingsController.HasUnsavedChangesForCurrentM3 = true;
            }

            ValidateChildren();
        }

        private void checkedComboBox_Popup(object sender, EventArgs e)
        {
            PopupContainerForm lForm = (sender as IPopupControl).PopupWindow as PopupContainerForm;
            CheckedListBoxControl lListBox = lForm.ActiveControl as CheckedListBoxControl;

            if (lListBox != null)
            {
                if ((sender as CheckedComboBoxEdit) == cMPList)
                {
                    lListBox.ItemChecking += OnItemChecking;
                }
            }
        }

        void OnItemChecking(object sender, ItemCheckingEventArgs e)
        {
            if (!_isLoading)
            {
                _generalSettingsController.HasUnsavedChanges = true;
                _generalSettingsController.HasUnsavedChangesForCurrentM3 = true;
            }

            CheckedListBoxControl lEdit = sender as CheckedListBoxControl;

            e.Cancel = GetSelectedItemsCount(lEdit, e.NewValue) > GetCount(tAmountOfMP);
        }

        private void checkedComboBox_CloseUp(object sender, CloseUpEventArgs e)
        {
            PopupContainerForm f = (sender as IPopupControl).PopupWindow as PopupContainerForm;
            CheckedListBoxControl listBox = f.ActiveControl as CheckedListBoxControl;

            if (listBox != null)
            {
                if ((sender as CheckedComboBoxEdit) == cMPList)
                {
                    listBox.ItemChecking -= OnItemChecking;
                }
            }
        }

        private void tTime_EditValueChanged(object sender, EventArgs e)
        {
            if (!_isLoading)
            {
                _generalSettingsController.HasUnsavedChanges = true;
                _generalSettingsController.HasUnsavedChangesForCurrentM3 = true;
            }

            TimeEdit edit = sender as TimeEdit;

            if (edit == tTimeOfFirstVisit)
            {
                if (tTimeOfFirstVisit.Time > tTimeOfLastVisit.Time)
                {
                    tTimeOfLastVisit.Time = tTimeOfFirstVisit.Time;
                }
            }

            if (edit == tTimeOfLastVisit)
            {
                if (tTimeOfFirstVisit.Time > tTimeOfLastVisit.Time)
                {
                    tTimeOfFirstVisit.Time = tTimeOfLastVisit.Time;
                }
            }
        }

        private void textGeneral_EditValueChanged(object sender, EventArgs e)
        {
            if (!_isLoading)
            {
                _generalSettingsController.HasUnsavedChanges = true;
                _generalSettingsController.HasUnsavedChangesForCurrentM3 = true;
            }
        }

        private void combobox_EditValueChanged(object sender, EventArgs e)
        {
            if (!_isLoading)
            {
                _generalSettingsController.HasUnsavedChanges = true;
                _generalSettingsController.HasUnsavedChangesForCurrentM3 = true;
            }

            ValidateChildren();
        }

        private void combobox_Validating(object sender, CancelEventArgs e)
        {
            ComboBoxEdit edit = sender as ComboBoxEdit;

            if (!RoleManager.IsInRole(MMRoleEnum.GeneralM5) || !_generalSettingsController.IsCountryLevel)
            {
                errorProvider.SetError(edit, "");
                return;
            }

            if (RoleManager.IsInRole(MMRoleEnum.GeneralM5) && _showErrors && string.IsNullOrEmpty(edit.Text))
            {
                errorProvider.SetError(edit, "Выберите значение");
            }
            else
            {
                errorProvider.SetError(edit, "");
            }
        }

        private void tSaleGracePeriod_Validating(object sender, CancelEventArgs e)
        {
            TextEdit edit = sender as TextEdit;

            if (!RoleManager.IsInRole(MMRoleEnum.GeneralM5))
            {
                errorProvider.SetError(edit, "");
                return;
            }

            if (_showErrors && string.IsNullOrEmpty(edit.Text))
            {
                errorProvider.SetError(edit, "Введите значение");
            }
            else
            {
                errorProvider.SetError(edit, "");
            }
        }

        private void tPackageDistributionPeriod_Validating(object sender, CancelEventArgs e)
        {
            TextEdit edit = sender as TextEdit;

            if (!RoleManager.IsInRole(MMRoleEnum.GeneralM5))
            {
                errorProvider.SetError(edit, "");
                return;
            }

            if (_showErrors && !string.IsNullOrEmpty(edit.Text))
            {
                if (int.Parse(edit.Text) > 128 || int.Parse(edit.Text) < 1)
                {
                    errorProvider.SetError(edit, "Может принимать значение от 1 до 128");
                }
                else
                {
                    errorProvider.SetError(edit, "");
                }

                return;
            }

            if (_showErrors && string.IsNullOrEmpty(edit.Text))
            {
                errorProvider.SetError(edit, "Введите значение");
            }
            else
            {
                errorProvider.SetError(edit, "");
            }
        }


        #endregion

        #region List & String Helpers

        private int GetSelectedItemsCount(CheckedListBoxControl edit, CheckState newValue)
        {
            int selectedItemsCount =
                (from CheckedListBoxItem item in edit.Items
                 where item.CheckState == CheckState.Checked
                 select item.Value.ToString()).Count();

            selectedItemsCount = selectedItemsCount + (newValue == CheckState.Checked ? 1 : -1);

            return selectedItemsCount;
        }

        private int GetSelectedItemsCount(CheckedComboBoxEdit edit)
        {
            int selectedItemsCount =
                (from CheckedListBoxItem item in edit.Properties.Items
                 where item.CheckState == CheckState.Checked
                 select item.Value.ToString()).Count();

            return selectedItemsCount;
        }

        private int GetCount(TextEdit edit)
        {
            return (edit.Text == string.Empty ? 0 : int.Parse(edit.Text));
        }

        private void SelectItems(CheckedComboBoxEdit edit, List<int> selectedItems)
        {
            edit.Properties.Items.BeginUpdate();

            for (int i = 0; i < edit.Properties.Items.Count; i++)
            {
                edit.Properties.Items[i].CheckState = selectedItems.Contains((int)edit.Properties.Items[i].Value) ? CheckState.Checked : CheckState.Unchecked;
            }

            edit.Properties.Items.EndUpdate();
        }

        private void LoadItems(CheckedComboBoxEdit edit, List<ListItemModel> items)
        {
            edit.Properties.Items.Clear();

            foreach (ListItemModel item in items)
            {
                edit.Properties.Items.Add(item.Id, item.Name);
            }
        }

        private List<int> GetSelectedItems(CheckedComboBoxEdit edit)
        {
            List<int> lSelectedItems = (from CheckedListBoxItem item in edit.Properties.Items where item.CheckState == CheckState.Checked select (int)item.Value).ToList();

            return lSelectedItems;
        }

        private void DeselectAll(CheckedComboBoxEdit edit)
        {
            for (int i = 0; i < edit.Properties.Items.Count; i++)
            {
                edit.Properties.Items[i].CheckState = CheckState.Unchecked;
            }
        }

        private int GetItemIndex(ComboBoxEdit comboEdit, int id)
        {
            int index = -1;

            for (int i = 0; i < comboEdit.Properties.Items.Count; i++)
            {
                if ((comboEdit.Properties.Items[i] as ListItemModel).Id == id)
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        #endregion

        private void ShowMessChangesWereSaved()
        {
            XtraMessageBox.Show("Данные успешно сохранены!", "Общие настройки", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Align sAKB and SR calc info for M3 and Country level in different positions
        /// </summary>
        private void AlignCalculationMethodsDetails()
        {
            if (_generalSettingsController.IsCountryLevel)
            {
                lMethodAkbCalc.Location = new Point(lMethodAkbCalc.Location.X, lInBevBeerVolumeLimit.Location.Y);
                cMethodAkbCalc.Location = new Point(cMethodAkbCalc.Location.X, tInBevBeerVolumeLimit.Location.Y);

                lMethodSrCalc.Location = new Point(lMethodSrCalc.Location.X, lAmountOfDistPushSKU.Location.Y);
                cMethodSrCalc.Location = new Point(cMethodSrCalc.Location.X, tAmountOfDistPushSKU.Location.Y);
            }
            else
            {
                lMethodAkbCalc.Location = new Point(lMethodAkbCalc.Location.X, lTimeOfFirstVisit.Location.Y);
                cMethodAkbCalc.Location = new Point(cMethodAkbCalc.Location.X, tTimeOfFirstVisit.Location.Y);

                lMethodSrCalc.Location = new Point(lMethodSrCalc.Location.X, lTimeOfLastVisit.Location.Y);
                cMethodSrCalc.Location = new Point(cMethodSrCalc.Location.X, tTimeOfLastVisit.Location.Y);
            }
        }

    }
}
