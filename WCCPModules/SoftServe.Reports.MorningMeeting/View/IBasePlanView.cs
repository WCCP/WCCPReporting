﻿using System.Collections.Generic;
using System.Data;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.View {
    public interface IBasePlanView : ISimpleControlView 
    {
        void ShowM1(bool showM1);
        void ConstructView(List<BindedColumnModel> columns);
        void AssignDataSource(DataTable plans);
        void SaveActions();
        void RefreshActions();
        bool LeaveTabActions();
        bool CanBindCapsSubtargets { get; }
        void HandleRefreshCapsBinding();
        bool SaveChangesAndProceed();
        void ImportPlansFromExcelFile();
        void PostEditor();
        void ClearFilter();
    }
}