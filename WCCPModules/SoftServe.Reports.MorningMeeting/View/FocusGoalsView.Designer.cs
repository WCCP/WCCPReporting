﻿namespace SoftServe.Reports.MorningMeeting.View
{
    partial class FocusGoalsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFilter = new DevExpress.XtraEditors.PanelControl();
            this.splitContainer = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlGoals = new DevExpress.XtraGrid.GridControl();
            this.gridViewGoals = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFocus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsManual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChannelMask = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.pnlButtons = new DevExpress.XtraEditors.PanelControl();
            this.bDelete = new DevExpress.XtraEditors.SimpleButton();
            this.bAdd = new DevExpress.XtraEditors.SimpleButton();
            this.lGoalType = new DevExpress.XtraEditors.LabelControl();
            this.cGoalType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelManualGoal = new DevExpress.XtraEditors.PanelControl();
            this.tDescription = new DevExpress.XtraEditors.MemoEdit();
            this.lDescription = new DevExpress.XtraEditors.LabelControl();
            this.panelAutoGoal = new DevExpress.XtraEditors.PanelControl();
            this.cSelectAll = new DevExpress.XtraEditors.CheckEdit();
            this.cGoal = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lTreeSKU = new DevExpress.XtraEditors.LabelControl();
            this.lAutoGoalType = new DevExpress.XtraEditors.LabelControl();
            this.treeSKU = new SoftServe.Reports.MorningMeeting.UserControl.CheckedTree();
            this.colItemId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGoals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rDateEdit.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).BeginInit();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cGoalType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelManualGoal)).BeginInit();
            this.panelManualGoal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelAutoGoal)).BeginInit();
            this.panelAutoGoal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGoal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeSKU)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFilter
            // 
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFilter.Location = new System.Drawing.Point(0, 0);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(1170, 64);
            this.panelFilter.TabIndex = 1;
            // 
            // splitContainer
            // 
            this.splitContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 64);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Panel1.Controls.Add(this.gridControlGoals);
            this.splitContainer.Panel1.Controls.Add(this.pnlButtons);
            this.splitContainer.Panel1.MinSize = 350;
            this.splitContainer.Panel1.Text = "Panel1";
            this.splitContainer.Panel2.Controls.Add(this.panelManualGoal);
            this.splitContainer.Panel2.Controls.Add(this.panelAutoGoal);
            this.splitContainer.Panel2.MinSize = 190;
            this.splitContainer.Panel2.Text = "Panel2";
            this.splitContainer.Size = new System.Drawing.Size(1170, 402);
            this.splitContainer.SplitterPosition = 740;
            this.splitContainer.TabIndex = 2;
            this.splitContainer.Text = "splitContainerControl1";
            // 
            // gridControlGoals
            // 
            this.gridControlGoals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlGoals.Location = new System.Drawing.Point(0, 52);
            this.gridControlGoals.MainView = this.gridViewGoals;
            this.gridControlGoals.Name = "gridControlGoals";
            this.gridControlGoals.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rTextEdit,
            this.rDateEdit,
            this.rCheckEdit});
            this.gridControlGoals.Size = new System.Drawing.Size(740, 350);
            this.gridControlGoals.TabIndex = 1;
            this.gridControlGoals.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewGoals});
            // 
            // gridViewGoals
            // 
            this.gridViewGoals.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridViewGoals.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFocus,
            this.colTargetName,
            this.colIsManual,
            //this.colStartDate,
            //this.colEndDate,
            this.colTargetId,
            this.colCreationDate,
            this.colM3,
            this.colChannelMask});
            this.gridViewGoals.GridControl = this.gridControlGoals;
            this.gridViewGoals.GroupCount = 1;
            this.gridViewGoals.GroupFormat = "{1} {2}";
            this.gridViewGoals.Name = "gridViewGoals";
            this.gridViewGoals.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewGoals.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewGoals.OptionsCustomization.AllowGroup = false;
            this.gridViewGoals.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewGoals.OptionsCustomization.AllowSort = false;
            this.gridViewGoals.OptionsFilter.AllowFilterEditor = false;
            this.gridViewGoals.OptionsFilter.AllowMRUFilterList = false;
            this.gridViewGoals.OptionsFilter.ColumnFilterPopupRowCount = 100;
            this.gridViewGoals.OptionsFilter.ColumnFilterPopupMaxRecordsCount = 200;
            this.gridViewGoals.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gridViewGoals.OptionsMenu.EnableColumnMenu = false;
            this.gridViewGoals.OptionsMenu.EnableFooterMenu = false;
            this.gridViewGoals.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewGoals.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewGoals.OptionsView.ShowGroupPanel = false;
            this.gridViewGoals.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colM3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewGoals.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewGoals_CustomDrawGroupRow);
            this.gridViewGoals.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewGoals_RowStyle);
            this.gridViewGoals.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewGoals_ShowingEditor);
            this.gridViewGoals.ShownEditor += new System.EventHandler(this.gridViewGoals_ShownEditor);
            this.gridViewGoals.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewGoals_FocusedRowChanged);
            this.gridViewGoals.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewGoals_CellValueChanged);
            this.gridViewGoals.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewGoals_InvalidRowException);
            this.gridViewGoals.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridViewGoals_BeforeLeaveRow);
            this.gridViewGoals.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewGoals_CustomUnboundColumnData);
            this.gridViewGoals.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewGoals_ValidatingEditor);
            // 
            // colFocus
            // 
            this.colFocus.Caption = "Фокус";
            this.colFocus.FieldName = "Focus";
            this.colFocus.MaxWidth = 60;
            this.colFocus.MinWidth = 60;
            this.colFocus.Name = "colFocus";
            this.colFocus.OptionsColumn.AllowEdit = false;
            this.colFocus.OptionsColumn.AllowSize = false;
            this.colFocus.OptionsColumn.FixedWidth = true;
            this.colFocus.OptionsFilter.AllowFilter = false;
            this.colFocus.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.colFocus.Visible = true;
            this.colFocus.VisibleIndex = 0;
            this.colFocus.Width = 79;
            // 
            // colTargetName
            // 
            this.colTargetName.Caption = "Название цели";
            this.colTargetName.ColumnEdit = this.rTextEdit;
            this.colTargetName.FieldName = "TargetName";
            this.colTargetName.Name = "colTargetName";
            this.colTargetName.OptionsFilter.AllowFilter = false;
            this.colTargetName.Visible = true;
            this.colTargetName.VisibleIndex = 1;
            this.colTargetName.Width = 188;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // colIsManual
            // 
            this.colIsManual.Caption = "Ручная";
            this.colIsManual.FieldName = "IsManual";
            this.colIsManual.MaxWidth = 60;
            this.colIsManual.MinWidth = 60;
            this.colIsManual.Name = "colIsManual";
            this.colIsManual.OptionsColumn.AllowEdit = false;
            this.colIsManual.OptionsColumn.AllowSize = false;
            this.colIsManual.OptionsColumn.FixedWidth = true;
            this.colIsManual.OptionsFilter.AllowFilter = false;
            this.colIsManual.Visible = true;
            this.colIsManual.VisibleIndex = 2;
            this.colIsManual.Width = 60;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Дата С";
            this.colStartDate.ColumnEdit = this.rDateEdit;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.MaxWidth = 80;
            this.colStartDate.MinWidth = 80;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsFilter.AllowFilter = false;
            this.colStartDate.Visible = true;
            this.colStartDate.Width = 80;
            // 
            // rDateEdit
            // 
            this.rDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.rDateEdit.AutoHeight = false;
            this.rDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rDateEdit.Name = "rDateEdit";
            this.rDateEdit.NullText = " ";
            this.rDateEdit.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "Дата По";
            this.colEndDate.ColumnEdit = this.rDateEdit;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.MaxWidth = 80;
            this.colEndDate.MinWidth = 80;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowSize = false;
            this.colEndDate.OptionsColumn.FixedWidth = true;
            this.colEndDate.OptionsFilter.AllowFilter = false;
            this.colEndDate.Visible = true;
            this.colEndDate.Width = 80;
            // 
            // colTargetId
            // 
            this.colTargetId.Caption = "TargetId";
            this.colTargetId.FieldName = "TargetId";
            this.colTargetId.Name = "colTargetId";
            this.colTargetId.OptionsFilter.AllowFilter = false;
            // 
            // colCreationDate
            // 
            this.colCreationDate.Caption = "CreationDate";
            this.colCreationDate.FieldName = "CreationDate";
            this.colCreationDate.Name = "colCreationDate";
            this.colCreationDate.OptionsFilter.AllowFilter = false;
            // 
            // colM3
            // 
            this.colM3.Caption = "M3";
            this.colM3.FieldName = "M3Id";
            this.colM3.GroupFormat.FormatString = "еееее{0}";
            this.colM3.Name = "colM3";
            this.colM3.OptionsFilter.AllowFilter = false;
            // 
            // colChannelMask
            // 
            this.colChannelMask.Caption = "ChannelMask";
            this.colChannelMask.FieldName = "ChannelMask";
            this.colChannelMask.Name = "colChannelMask";
            this.colChannelMask.OptionsFilter.AllowFilter = false;
            // 
            // rCheckEdit
            // 
            this.rCheckEdit.AutoHeight = false;
            this.rCheckEdit.Name = "rCheckEdit";
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.bDelete);
            this.pnlButtons.Controls.Add(this.bAdd);
            this.pnlButtons.Controls.Add(this.lGoalType);
            this.pnlButtons.Controls.Add(this.cGoalType);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(740, 52);
            this.pnlButtons.TabIndex = 0;
            // 
            // bDelete
            // 
            this.bDelete.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.delete;
            this.bDelete.Location = new System.Drawing.Point(40, 7);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(32, 32);
            this.bDelete.TabIndex = 7;
            this.bDelete.Text = "simpleButton2";
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bAdd
            // 
            this.bAdd.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.add;
            this.bAdd.Location = new System.Drawing.Point(5, 7);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(32, 32);
            this.bAdd.TabIndex = 6;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // lGoalType
            // 
            this.lGoalType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lGoalType.Location = new System.Drawing.Point(575, 5);
            this.lGoalType.Name = "lGoalType";
            this.lGoalType.Size = new System.Drawing.Size(49, 13);
            this.lGoalType.TabIndex = 5;
            this.lGoalType.Text = "Тип цели:";
            // 
            // cGoalType
            // 
            this.cGoalType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cGoalType.Location = new System.Drawing.Point(572, 20);
            this.cGoalType.Name = "cGoalType";
            this.cGoalType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cGoalType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cGoalType.Size = new System.Drawing.Size(163, 20);
            this.cGoalType.TabIndex = 4;
            // 
            // panelManualGoal
            // 
            this.panelManualGoal.Controls.Add(this.tDescription);
            this.panelManualGoal.Controls.Add(this.lDescription);
            this.panelManualGoal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelManualGoal.Location = new System.Drawing.Point(0, 256);
            this.panelManualGoal.Name = "panelManualGoal";
            this.panelManualGoal.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.panelManualGoal.Size = new System.Drawing.Size(424, 146);
            this.panelManualGoal.TabIndex = 2;
            this.panelManualGoal.Visible = false;
            // 
            // tDescription
            // 
            this.tDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tDescription.Location = new System.Drawing.Point(2, 32);
            this.tDescription.Name = "tDescription";
            this.tDescription.Size = new System.Drawing.Size(420, 112);
            this.tDescription.TabIndex = 1;
            // 
            // lDescription
            // 
            this.lDescription.Location = new System.Drawing.Point(5, 13);
            this.lDescription.Name = "lDescription";
            this.lDescription.Size = new System.Drawing.Size(53, 13);
            this.lDescription.TabIndex = 0;
            this.lDescription.Text = "Описание:";
            // 
            // panelAutoGoal
            // 
            this.panelAutoGoal.Controls.Add(this.cSelectAll);
            this.panelAutoGoal.Controls.Add(this.cGoal);
            this.panelAutoGoal.Controls.Add(this.lTreeSKU);
            this.panelAutoGoal.Controls.Add(this.lAutoGoalType);
            this.panelAutoGoal.Controls.Add(this.treeSKU);
            this.panelAutoGoal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAutoGoal.Location = new System.Drawing.Point(0, 0);
            this.panelAutoGoal.Name = "panelAutoGoal";
            this.panelAutoGoal.Padding = new System.Windows.Forms.Padding(0, 60, 0, 0);
            this.panelAutoGoal.Size = new System.Drawing.Size(424, 184);
            this.panelAutoGoal.TabIndex = 1;
            this.panelAutoGoal.Visible = false;
            // 
            // cSelectAll
            // 
            this.cSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cSelectAll.Location = new System.Drawing.Point(376, 43);
            this.cSelectAll.Name = "cSelectAll";
            this.cSelectAll.Properties.AllowGrayed = true;
            this.cSelectAll.Properties.Caption = "Все";
            this.cSelectAll.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cSelectAll.Size = new System.Drawing.Size(48, 19);
            this.cSelectAll.TabIndex = 2;
            this.cSelectAll.CheckStateChanged += new System.EventHandler(this.cSelectAll_CheckedChanged);
            // 
            // cGoal
            // 
            this.cGoal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cGoal.Location = new System.Drawing.Point(2, 20);
            this.cGoal.MaximumSize = new System.Drawing.Size(500, 0);
            this.cGoal.MinimumSize = new System.Drawing.Size(186, 0);
            this.cGoal.Name = "cGoal";
            this.cGoal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cGoal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cGoal.Size = new System.Drawing.Size(376, 20);
            this.cGoal.TabIndex = 3;
            // 
            // lTreeSKU
            // 
            this.lTreeSKU.Location = new System.Drawing.Point(3, 46);
            this.lTreeSKU.Name = "lTreeSKU";
            this.lTreeSKU.Size = new System.Drawing.Size(100, 13);
            this.lTreeSKU.TabIndex = 1;
            this.lTreeSKU.Text = "Дерево продукции:";
            // 
            // lAutoGoalType
            // 
            this.lAutoGoalType.Location = new System.Drawing.Point(3, 4);
            this.lAutoGoalType.Name = "lAutoGoalType";
            this.lAutoGoalType.Size = new System.Drawing.Size(30, 13);
            this.lAutoGoalType.TabIndex = 2;
            this.lAutoGoalType.Text = "Цель:";
            // 
            // treeSKU
            // 
            this.treeSKU.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.treeSKU.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colItemId});
            this.treeSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeSKU.KeyFieldName = "Id";
            this.treeSKU.Location = new System.Drawing.Point(2, 62);
            this.treeSKU.Name = "treeSKU";
            this.treeSKU.OptionsBehavior.AllowIndeterminateCheckState = true;
            this.treeSKU.OptionsBehavior.Editable = false;
            this.treeSKU.OptionsView.ShowCheckBoxes = true;
            this.treeSKU.OptionsView.ShowColumns = false;
            this.treeSKU.OptionsView.ShowHorzLines = false;
            this.treeSKU.OptionsView.ShowIndicator = false;
            this.treeSKU.OptionsView.ShowVertLines = false;
            this.treeSKU.ParentFieldName = "ParentId";
            this.treeSKU.RootValue = null;
            this.treeSKU.Size = new System.Drawing.Size(420, 120);
            this.treeSKU.TabIndex = 0;
            this.treeSKU.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeSKU_BeforeCheckNode);
            this.treeSKU.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeSKU_AfterCheckNode);
            // 
            // colItemId
            // 
            this.colItemId.Caption = "ItemId";
            this.colItemId.FieldName = "ItemId";
            this.colItemId.Name = "colItemId";
            this.colItemId.OptionsColumn.AllowEdit = false;
            this.colItemId.OptionsColumn.AllowFocus = false;
            this.colItemId.OptionsColumn.AllowMove = false;
            this.colItemId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colItemId.OptionsColumn.AllowSize = false;
            this.colItemId.OptionsColumn.AllowSort = false;
            this.colItemId.OptionsColumn.ReadOnly = true;
            this.colItemId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // FocusGoalsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panelFilter);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "FocusGoalsView";
            this.Size = new System.Drawing.Size(1170, 466);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGoals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rDateEdit.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).EndInit();
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cGoalType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelManualGoal)).EndInit();
            this.panelManualGoal.ResumeLayout(false);
            this.panelManualGoal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelAutoGoal)).EndInit();
            this.panelAutoGoal.ResumeLayout(false);
            this.panelAutoGoal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGoal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeSKU)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelFilter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainer;
        private DevExpress.XtraGrid.GridControl gridControlGoals;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewGoals;
        private DevExpress.XtraEditors.PanelControl pnlButtons;
        private UserControl.CheckedTree treeSKU;
        private DevExpress.XtraEditors.PanelControl panelManualGoal;
        private DevExpress.XtraEditors.LabelControl lDescription;
        private DevExpress.XtraEditors.PanelControl panelAutoGoal;
        private DevExpress.XtraEditors.LabelControl lTreeSKU;
        private DevExpress.XtraEditors.LabelControl lAutoGoalType;
        private DevExpress.XtraEditors.ComboBoxEdit cGoal;
        private DevExpress.XtraEditors.MemoEdit tDescription;
        private DevExpress.XtraEditors.LabelControl lGoalType;
        private DevExpress.XtraEditors.ComboBoxEdit cGoalType;
        private DevExpress.XtraEditors.SimpleButton bDelete;
        private DevExpress.XtraEditors.SimpleButton bAdd;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetId;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetName;
        private DevExpress.XtraGrid.Columns.GridColumn colIsManual;
        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colChannelMask;
        private DevExpress.XtraGrid.Columns.GridColumn colFocus;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rCheckEdit;
        private DevExpress.XtraEditors.CheckEdit cSelectAll;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colItemId;

    }
}
