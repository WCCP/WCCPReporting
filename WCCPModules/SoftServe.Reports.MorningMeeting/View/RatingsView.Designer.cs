﻿namespace SoftServe.Reports.MorningMeeting.View
{
    partial class RatingsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFilter = new DevExpress.XtraEditors.PanelControl();
            this.gridControlRatings = new DevExpress.XtraGrid.GridControl();
            this.gridViewRatings = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKpiTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rKpiTypes = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colKpiId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rKpis = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colValues = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colChannelId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rPopupContainerEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.rValues = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rProductSets = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rAvrSkuList = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rNetworks = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pnlButtons = new DevExpress.XtraEditors.PanelControl();
            this.bDelete = new DevExpress.XtraEditors.SimpleButton();
            this.bAdd = new DevExpress.XtraEditors.SimpleButton();
            this.lChannel = new DevExpress.XtraEditors.LabelControl();
            this.cChannel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.colProductName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRatings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRatings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rKpiTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rKpis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rPopupContainerEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rProductSets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAvrSkuList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rNetworks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).BeginInit();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cChannel.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFilter
            // 
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFilter.Location = new System.Drawing.Point(0, 0);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(808, 64);
            this.panelFilter.TabIndex = 1;
            // 
            // gridControlRatings
            // 
            this.gridControlRatings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRatings.Location = new System.Drawing.Point(0, 116);
            this.gridControlRatings.MainView = this.gridViewRatings;
            this.gridControlRatings.Name = "gridControlRatings";
            this.gridControlRatings.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rTextEdit,
            this.rPopupContainerEdit,
            this.rValues,
            this.rKpiTypes,
            this.rKpis,
            this.rProductSets,
            this.rAvrSkuList,
            this.rNetworks});
            this.gridControlRatings.Size = new System.Drawing.Size(808, 284);
            this.gridControlRatings.TabIndex = 1;
            this.gridControlRatings.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRatings});
            this.gridControlRatings.Validating += new System.ComponentModel.CancelEventHandler(this.gridControlRatings_Validating);
            // 
            // gridViewRatings
            // 
            this.gridViewRatings.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridViewRatings.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colM3,
            this.colKpiTypeId,
            this.colKpiId,
            this.colValues,
            this.colWeight,
            this.colChannelId});
            this.gridViewRatings.GridControl = this.gridControlRatings;
            this.gridViewRatings.GroupCount = 1;
            this.gridViewRatings.GroupFormat = "{1} {2}";
            this.gridViewRatings.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Weight", this.colWeight, "{0}%"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "KpiTypeId", this.colKpiTypeId, "")});
            this.gridViewRatings.Name = "gridViewRatings";
            this.gridViewRatings.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewRatings.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewRatings.OptionsCustomization.AllowGroup = false;
            this.gridViewRatings.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewRatings.OptionsCustomization.AllowSort = false;
            this.gridViewRatings.OptionsFilter.AllowFilterEditor = false;
            this.gridViewRatings.OptionsFilter.AllowMRUFilterList = false;
            this.gridViewRatings.OptionsFilter.ColumnFilterPopupRowCount = 100;
            this.gridViewRatings.OptionsFilter.ColumnFilterPopupMaxRecordsCount = 200;
            this.gridViewRatings.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gridViewRatings.OptionsMenu.EnableColumnMenu = false;
            this.gridViewRatings.OptionsMenu.EnableFooterMenu = false;
            this.gridViewRatings.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewRatings.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewRatings.OptionsView.ShowGroupPanel = false;
            this.gridViewRatings.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colChannelId, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewRatings.CustomDrawRowFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gridViewRatings_CustomDrawRowFooterCell);
            this.gridViewRatings.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewGoals_CustomDrawGroupRow);
            this.gridViewRatings.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewRatings_RowStyle);
            this.gridViewRatings.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewRatings_CustomRowCellEdit);
            this.gridViewRatings.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewRatings_ShowingEditor);
            this.gridViewRatings.ShownEditor += new System.EventHandler(this.gridViewRatings_ShownEditor);
            this.gridViewRatings.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewGoals_FocusedRowChanged);
            this.gridViewRatings.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewRatings_CellValueChanged);
            this.gridViewRatings.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewGoals_InvalidRowException);
            this.gridViewRatings.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridViewRatings_BeforeLeaveRow);
            this.gridViewRatings.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewRatings_ValidatingEditor);
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colM3
            // 
            this.colM3.Caption = "M3";
            this.colM3.FieldName = "M3Id";
            this.colM3.Name = "colM3";
            this.colM3.OptionsFilter.AllowFilter = false;
            // 
            // colKpiTypeId
            // 
            this.colKpiTypeId.Caption = "Тип KPI";
            this.colKpiTypeId.ColumnEdit = this.rKpiTypes;
            this.colKpiTypeId.FieldName = "KpiTypeId";
            this.colKpiTypeId.Name = "colKpiTypeId";
            this.colKpiTypeId.OptionsFilter.AllowFilter = false;
            this.colKpiTypeId.Visible = true;
            this.colKpiTypeId.VisibleIndex = 0;
            // 
            // rKpiTypes
            // 
            this.rKpiTypes.AutoHeight = false;
            this.rKpiTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rKpiTypes.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.rKpiTypes.DisplayMember = "Name";
            this.rKpiTypes.DropDownRows = 9;
            this.rKpiTypes.Name = "rKpiTypes";
            this.rKpiTypes.NullText = "";
            this.rKpiTypes.ShowFooter = false;
            this.rKpiTypes.ShowHeader = false;
            this.rKpiTypes.ShowLines = false;
            this.rKpiTypes.ValueMember = "Id";
            // 
            // colKpiId
            // 
            this.colKpiId.Caption = "KPI";
            this.colKpiId.ColumnEdit = this.rKpis;
            this.colKpiId.FieldName = "KpiId";
            this.colKpiId.Name = "colKpiId";
            this.colKpiId.OptionsFilter.AllowFilter = false;
            this.colKpiId.Visible = true;
            this.colKpiId.VisibleIndex = 1;
            // 
            // rKpis
            // 
            this.rKpis.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.rKpis.AutoHeight = false;
            this.rKpis.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rKpis.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 150, "Name")});
            this.rKpis.DisplayMember = "Name";
            this.rKpis.Name = "rKpis";
            this.rKpis.NullText = "";
            this.rKpis.ShowFooter = false;
            this.rKpis.ShowHeader = false;
            this.rKpis.ShowLines = false;
            this.rKpis.ValueMember = "Id";
            // 
            // colValues
            // 
            this.colValues.Caption = "Показатель";
            this.colValues.FieldName = "Values";
            this.colValues.Name = "colValues";
            this.colValues.OptionsFilter.AllowFilter = false;
            this.colValues.Visible = true;
            this.colValues.VisibleIndex = 2;
            // 
            // colWeight
            // 
            this.colWeight.Caption = "Вес";
            this.colWeight.ColumnEdit = this.rTextEdit;
            this.colWeight.FieldName = "Weight";
            this.colWeight.Name = "colWeight";
            this.colWeight.OptionsFilter.AllowFilter = false;
            this.colWeight.Visible = true;
            this.colWeight.VisibleIndex = 3;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.DisplayFormat.FormatString = "{0}%";
            this.rTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rTextEdit.Mask.EditMask = "\\d{0,3}";
            this.rTextEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // colChannelId
            // 
            this.colChannelId.Caption = "Канал";
            this.colChannelId.FieldName = "ChannelId";
            this.colChannelId.Name = "colChannelId";
            this.colChannelId.OptionsFilter.AllowFilter = false;
            this.colChannelId.Visible = true;
            this.colChannelId.VisibleIndex = 0;
            // 
            // rPopupContainerEdit
            // 
            this.rPopupContainerEdit.AutoHeight = false;
            this.rPopupContainerEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rPopupContainerEdit.CloseOnLostFocus = false;
            this.rPopupContainerEdit.CloseOnOuterMouseClick = false;
            this.rPopupContainerEdit.Name = "rPopupContainerEdit";
            this.rPopupContainerEdit.PopupSizeable = false;
            this.rPopupContainerEdit.ShowPopupCloseButton = false;
            this.rPopupContainerEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.rPopupContainerEdit_QueryResultValue);
            this.rPopupContainerEdit.QueryDisplayText += new DevExpress.XtraEditors.Controls.QueryDisplayTextEventHandler(this.rPopupContainerEdit_QueryDisplayText);
            // 
            // rValues
            // 
            this.rValues.AutoHeight = false;
            this.rValues.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rValues.DisplayMember = "Name";
            this.rValues.Name = "rValues";
            this.rValues.NullText = "";
            this.rValues.ValueMember = "Id";
            // 
            // rProductSets
            // 
            this.rProductSets.AutoHeight = false;
            this.rProductSets.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rProductSets.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.rProductSets.DisplayMember = "Name";
            this.rProductSets.Name = "rProductSets";
            this.rProductSets.NullText = "";
            this.rProductSets.ShowFooter = false;
            this.rProductSets.ShowHeader = false;
            this.rProductSets.ShowLines = false;
            this.rProductSets.ThrowExceptionOnInvalidLookUpEditValueType = true;
            this.rProductSets.ValueMember = "ColumnIdDescription";
            // 
            // rAvrSkuList
            // 
            this.rAvrSkuList.AutoHeight = false;
            this.rAvrSkuList.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rAvrSkuList.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.rAvrSkuList.DisplayMember = "Name";
            this.rAvrSkuList.Name = "rAvrSkuList";
            this.rAvrSkuList.NullText = "";
            this.rAvrSkuList.ShowFooter = false;
            this.rAvrSkuList.ShowHeader = false;
            this.rAvrSkuList.ShowLines = false;
            this.rAvrSkuList.ValueMember = "ColumnIdDescription";
            // 
            // rNetworks
            // 
            this.rNetworks.AutoHeight = false;
            this.rNetworks.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rNetworks.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.rNetworks.DisplayMember = "Name";
            this.rNetworks.Name = "rNetworks";
            this.rNetworks.NullText = "";
            this.rNetworks.ShowFooter = false;
            this.rNetworks.ShowHeader = false;
            this.rNetworks.ShowLines = false;
            this.rNetworks.ThrowExceptionOnInvalidLookUpEditValueType = true;
            this.rNetworks.ValueMember = "ColumnIdDescription";
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.bDelete);
            this.pnlButtons.Controls.Add(this.bAdd);
            this.pnlButtons.Controls.Add(this.lChannel);
            this.pnlButtons.Controls.Add(this.cChannel);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButtons.Location = new System.Drawing.Point(0, 64);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(808, 52);
            this.pnlButtons.TabIndex = 0;
            // 
            // bDelete
            // 
            this.bDelete.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.delete;
            this.bDelete.Location = new System.Drawing.Point(40, 7);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(32, 32);
            this.bDelete.TabIndex = 7;
            this.bDelete.Text = "simpleButton2";
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bAdd
            // 
            this.bAdd.Image = global::SoftServe.Reports.MorningMeeting.Properties.Resources.add;
            this.bAdd.Location = new System.Drawing.Point(5, 7);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(32, 32);
            this.bAdd.TabIndex = 6;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // lChannel
            // 
            this.lChannel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lChannel.Location = new System.Drawing.Point(643, 5);
            this.lChannel.Name = "lChannel";
            this.lChannel.Size = new System.Drawing.Size(35, 13);
            this.lChannel.TabIndex = 5;
            this.lChannel.Text = "Канал:";
            // 
            // cChannel
            // 
            this.cChannel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cChannel.Location = new System.Drawing.Point(640, 20);
            this.cChannel.Name = "cChannel";
            this.cChannel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cChannel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cChannel.Size = new System.Drawing.Size(163, 20);
            this.cChannel.TabIndex = 4;
            this.cChannel.EditValueChanged += new System.EventHandler(this.cChannel_EditValueChanged);
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Name";
            this.colProductName.FieldName = "Name";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.AllowMove = false;
            this.colProductName.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colProductName.OptionsColumn.AllowSize = false;
            this.colProductName.OptionsColumn.AllowSort = false;
            this.colProductName.OptionsColumn.FixedWidth = true;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.OptionsColumn.ShowInCustomizationForm = false;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 0;
            this.colProductName.Width = 91;
            // 
            // RatingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlRatings);
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.panelFilter);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "RatingsView";
            this.Size = new System.Drawing.Size(808, 400);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRatings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRatings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rKpiTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rKpis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rPopupContainerEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rProductSets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAvrSkuList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rNetworks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).EndInit();
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cChannel.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelFilter;
        private DevExpress.XtraGrid.GridControl gridControlRatings;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRatings;
        private DevExpress.XtraEditors.PanelControl pnlButtons;
        private DevExpress.XtraEditors.LabelControl lChannel;
        private DevExpress.XtraEditors.ComboBoxEdit cChannel;
        private DevExpress.XtraEditors.SimpleButton bDelete;
        private DevExpress.XtraEditors.SimpleButton bAdd;
        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextEdit;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colChannelId;
        private DevExpress.XtraGrid.Columns.GridColumn colKpiTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colKpiId;
        private DevExpress.XtraGrid.Columns.GridColumn colValues;
        private DevExpress.XtraGrid.Columns.GridColumn colWeight;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit rPopupContainerEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rValues;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rKpiTypes;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rKpis;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rProductSets;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rAvrSkuList;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rNetworks;

    }
}
