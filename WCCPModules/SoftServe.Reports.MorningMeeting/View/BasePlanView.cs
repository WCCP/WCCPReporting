﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View
{
    [ToolboxItem(true)]
    public class BasePlanView : SimpleControlView, IBasePlanView
    {
        private IBasePlanController _basePlanController;

        #region Design Fields

        private PanelControl panelFilter;

        public GridControl gridControlPlans;

        protected DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextPercents;
        protected DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextVolume;
        protected DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextInt;
        protected DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextTime;
        protected DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextTwoDigits;
        protected DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextPercentOneDigit;

        private ImageCollection _buttonImages;
        private IContainer components;
        private ToolTipController _toolTipController;

        #endregion
        public BandedGridView gridViewPlans;
        protected BandedGridColumn _colEmployee;
        protected BandedGridColumn _colLevel;
        protected BandedGridColumn _colEmployeeId;
        private GridBand bandGeneralInfo;

        private bool _displayM1;
        private string FILTER_Hide_M1 = "[EmployeeLevel] <> 'M1'";
        private bool _isM1CheckBeingChanged = false;
        private bool _isFilterBeingCleared = false;

        protected List<BindedColumnModel> _columns;

        protected bool IsM2 { get; set; }

        public BasePlanView()
        {
            InitializeComponent();
            _displayM1 = false;
        }

        #region Implementation of IView

        public override IViewController GetController()
        {
            return _basePlanController;
        }

        public override void SetController(IViewController viewController)
        {
            _basePlanController = (IBasePlanController)viewController;
        }

        public override object GetDockObject(ISimpleControlView controlView)
        {
            return panelFilter;
        }

        #endregion

        #region Implementation of IBasePlan

        public void ShowM1(bool showM1)
        {
            if (_isM1CheckBeingChanged)
                return;

            _displayM1 = showM1;

            if ((gridControlPlans.DataSource as DataTable) != null && (gridControlPlans.DataSource as DataTable).Columns.Contains("EmployeeLevel"))
            {
                if (showM1)
                {
                    gridViewPlans.ActiveFilter.Remove(_colLevel);
                }
                else
                {
                    gridViewPlans.ActiveFilter.Add(_colLevel, new ColumnFilterInfo(FILTER_Hide_M1, ""));
                }
            }
        }

        public virtual void AssignDataSource(DataTable plans)
        {
            if (plans != null && plans.Columns.Contains("EmployeeLevel"))
            {
                plans.DefaultView.Sort = "[EmployeeLevel] DESC, [EmployeeName] ASC";
            }

            gridControlPlans.DataSource = plans;

            bool lContainsM3 = false;

            if (plans != null && plans.Columns.Contains(_colLevel.FieldName))
            {
                foreach (DataRow row in plans.Rows)
                {
                    if (row[_colLevel.FieldName] != DBNull.Value && Convert.ToString(row[_colLevel.FieldName]) == "M3")
                    {
                        lContainsM3 = true;
                        break;
                    }
                }
            }

            IsM2 = !lContainsM3;
        }

        public virtual void ConstructView(List<BindedColumnModel> columns)
        {
            _columns = columns;
        }

        public virtual void SaveActions()
        {
            gridViewPlans.PostEditor();
            _basePlanController.SaveData();

            ShowMessChangesWereSaved();
        }

        public virtual void RefreshActions()
        {
            gridViewPlans.PostEditor();

            if (_basePlanController.HasUnsavedChanges)
            {
                if (SaveChangesAndProceed())
                {
                    return;
                }
            }

            _basePlanController.LoadData(true);
        }

        public virtual bool LeaveTabActions()
        {
            gridViewPlans.PostEditor();

            bool lAllowLeavePage = true;

            if (_basePlanController.HasUnsavedChanges)
            {
                DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                   MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                switch (res)
                {
                    case DialogResult.Yes: _basePlanController.SaveData();
                        ShowMessChangesWereSaved();
                        break;
                    case DialogResult.No: _basePlanController.ClearUpdatedValues();
                        _basePlanController.LoadData(true);
                        break;
                    case DialogResult.Cancel:
                        lAllowLeavePage = false;
                        break;
                }
            }

            return lAllowLeavePage;
        }

        public void PostEditor()
        {
            gridViewPlans.PostEditor();
        }

        protected virtual PlanItemModel GetCurrentEditedPlan(string fieldName, object value)
        {
            int lColumnId = int.Parse(fieldName);
            int lEmployeeId = Convert.ToInt32(gridViewPlans.GetFocusedRowCellValue(_colEmployeeId));
            decimal? lValue = value == DBNull.Value ? default(decimal?) : Convert.ToDecimal(value);

            return new PlanItemModel { ColumnId = lColumnId, EmployeeId = lEmployeeId, Value = lValue };
        }

        protected virtual PlanItemModel GetCapsPlan(string fieldName, int employeeId, object value)
        {
            int lColumnId = int.Parse(fieldName);
            int lEmployeeId = employeeId;
            decimal? lValue = value == DBNull.Value ? default(decimal?) : Convert.ToDecimal(value);

            return new PlanItemModel { ColumnId = lColumnId, EmployeeId = lEmployeeId, Value = lValue };
        }

        public bool CanBindCapsSubtargets
        {
            get
            {
                return !IsM2;
            }
        }

        public void HandleRefreshCapsBinding()
        {
            if (XtraMessageBox.Show("Цели будут автоматически обновлены, ранее введенные цели могут быть автоматически заменены целями СКиП",
                "Обновить цели из СКиП", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                _basePlanController.ForceAutoMapping();
                _basePlanController.LoadData(true);
            }
        }

        public bool SaveChangesAndProceed()
        {
            DialogResult res = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                                                   MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                _basePlanController.SaveData();
                ShowMessChangesWereSaved();
            }

            if (res == DialogResult.No)
            {
                _basePlanController.ClearUpdatedValues();
            }

            return res == DialogResult.Cancel;
        }

        public void ImportPlansFromExcelFile()
        {
            PlansUploader lUploader = new PlansUploader();

            lUploader.UploadPlans();
        }

        public void ClearFilter()
        {
            _isFilterBeingCleared = true;
            gridViewPlans.ActiveFilter.Clear();

            if (!_displayM1)
            {
                try
                {
                    gridViewPlans.ActiveFilter.Add(_colLevel, new ColumnFilterInfo("[EmployeeLevel] != 'M1'", ""));
                }
                catch (Exception)
                { }
            }
            _isFilterBeingCleared = false;
        }

        #endregion

        #region Properties

        protected bool IsM2Row
        {
            get
            {
                bool isM2Row = gridViewPlans.GetFocusedRowCellDisplayText(_colLevel) == "M2";

                return isM2Row && IsM2;
            }
        }

        protected virtual bool IsColumnBindedToSkip(GridColumn column)
        {
            return IsColumnBindedToSkip(column.FieldName);
        }

        private bool IsColumnBindedToSkip(string fieldName)
        {
            BindedColumnModel record = GetCorrespondingCol(fieldName);
            bool result = record != null && record.CapsBinding != (int)BindingToSkip.Unbinded;

            return result;
        }

        protected virtual bool CanBeBindedToSkip(GridColumn column)
        {
            BindedColumnModel record = GetCorrespondingCol(column);
            bool result = record != null && record.CapsBinding != (int)BindingToSkip.AutoBinded;

            return result;
        }

        private bool CanBeUnBindedToSkip(GridColumn column)
        {
            BindedColumnModel record = GetCorrespondingCol(column);
            bool result = record != null && record.CapsBinding == (int)BindingToSkip.ManuallyBinded;

            return result;
        }

        protected virtual bool IsAutoBindedToSkip(GridColumn column)
        {
            BindedColumnModel record = GetCorrespondingCol(column);
            bool result = record != null && record.CapsBinding == (int)BindingToSkip.AutoBinded;

            return result;
        }

        private BindedColumnModel GetCorrespondingCol(GridColumn col)
        {
            return GetCorrespondingCol(col.FieldName);
        }

        private BindedColumnModel GetCorrespondingCol(string fieldName)
        {
            if (_columns != null)
            {
                return _columns.FirstOrDefault(col => (col.Id.ToString()) == fieldName);
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region View Update

        protected void ResetView()
        {
            gridControlPlans.DataSource = null;

            gridViewPlans.BeginUpdate();

            while (gridViewPlans.Columns.Count > 3)
            {
                gridViewPlans.Columns.RemoveAt(3);
            }

            gridViewPlans.EndUpdate();
        }

        protected BandedGridColumn AddColumn(string name, string caption, string fieldName, int width, string bandCaption)
        {
            GridBand lBand = GetColumnsBand(bandCaption);

            BandedGridColumn col = gridViewPlans.Columns.AddField(name);

            col.FieldName = fieldName;
            col.Visible = true;
            col.Width = width;
            col.MinWidth = 40;
            col.Caption = caption;
            col.CustomizationCaption = string.Format("{0} {1}", bandCaption, caption);
            col.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
            col.VisibleIndex = gridViewPlans.VisibleColumns.Count;

            if (lBand != null)
            {
                lBand.Columns.Add(col);
            }

            SetColumnEditOptions(col);

            return col;
        }

        protected GridBand GetColumnsBand(string bandCaption)
        {
            GridBand lBand = null;

            foreach (GridBand band in gridViewPlans.Bands)
            {
                if (band.Caption.Trim() == bandCaption.Trim())
                {
                    lBand = band;
                }
            }

            return lBand;
        }

        private void SetColumnEditOptions(GridColumn col)
        {
            col.AppearanceCell.BackColor = Color.LightGray;

            bool realOnly = IsColumnBindedToSkip(col);

            col.AppearanceCell.Options.UseBackColor = realOnly;
            col.OptionsColumn.ReadOnly = realOnly;
            col.OptionsColumn.AllowEdit = !realOnly;
        }

        private void gridViewPlans_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (IsM2Row)
            {
                e.Cancel = true;
            }
        }

        #endregion

        #region Binding to Caps Subtargets

        private void DoClickHeaderButton(GridColumn col)
        {
            gridViewPlans.BeginSelection();
            gridViewPlans.ClearSelection();
            gridViewPlans.SelectCells(0, col, gridViewPlans.DataRowCount - 1, col);
            gridViewPlans.EndSelection();

            if (CanBeUnBindedToSkip(col))
            {
                HandleUnbinding(col);
            }
            else
            {
                HandleBinding(col);
            }

            SetColumnEditOptions(col);
        }

        private void HandleBinding(GridColumn col)
        {
            gridViewPlans.PostEditor();

            if (_basePlanController.HasUnsavedChanges)
            {
                if (SaveChangesAndProceed())
                {
                    return;
                }
            }

            if (_basePlanController.BindingForm.StartDialog() == DialogResult.OK)
            {
                BindGoal(col, _basePlanController.BindingForm.BindedTargetId.Value);
            }

            _basePlanController.LoadData(true);
        }

        private void BindGoal(GridColumn col, long subtargetId)
        {
            BindedColumnModel bindedColumn = GetCorrespondingCol(col);

            if (bindedColumn != null)
            {
                bindedColumn.CapsSubtargetId = subtargetId;
                bindedColumn.CapsBinding = (int)BindingToSkip.ManuallyBinded;

                DataProvider.BindColumnToSubtargets(bindedColumn.Id, subtargetId);
            }
        }

        private void HandleUnbinding(GridColumn col)
        {
            if (XtraMessageBox.Show("Отменить привязку к цели СКиП?", "Привязка к целям из СКиП", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.OK)
            {
                gridViewPlans.PostEditor();

                DataTable plans = gridControlPlans.DataSource as DataTable;

                foreach (DataRow row in plans.Rows)
                {
                    _basePlanController.AddCapsPlanValue(GetCapsPlan(col.FieldName, Convert.ToInt32(row[_colEmployeeId.FieldName]), row[col.FieldName]));
                }

                _basePlanController.SaveCapsPlans();
                UnbindGoal(col);
            }
        }

        private void UnbindGoal(GridColumn col)
        {
            BindedColumnModel bindedColumn = GetCorrespondingCol(col);

            if (bindedColumn != null)
            {
                _basePlanController.BindingForm.ClearBinding(bindedColumn.CapsSubtargetId.Value);

                DataProvider.UnbindColumnToSubtargets(bindedColumn.Id, bindedColumn.CapsSubtargetId.Value);

                bindedColumn.CapsSubtargetId = null;
                bindedColumn.CapsBinding = (int)BindingToSkip.Unbinded;
            }
        }

        #endregion

        private void gridViewPlans_ShowCustomizationForm(object sender, EventArgs e)
        {
            gridViewPlans.CustomizationForm.TopMost = true;
        }

        private void gridViewPlans_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            _basePlanController.AddUpdatedValue(GetCurrentEditedPlan(e.Column.FieldName, e.Value));
        }

        private void rText_ParseEditValue(object sender, ConvertEditValueEventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(e.Value)))
            {
                e.Value = null;
            }
        }

        private void rTextTime_ParseEditValue(object sender, ConvertEditValueEventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(e.Value)))
            {
                e.Value = null;
            }
            else
            {
                e.Value = new TimeEx(Convert.ToString(e.Value));
            }
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BasePlanView));
            this.panelFilter = new DevExpress.XtraEditors.PanelControl();
            this.gridControlPlans = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlans = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.bandGeneralInfo = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._colEmployee = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._colLevel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._colEmployeeId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rTextPercents = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rTextVolume = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rTextInt = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rTextTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rTextTwoDigits = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rTextPercentOneDigit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this._buttonImages = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextPercents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextTwoDigits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextPercentOneDigit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonImages)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFilter
            // 
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFilter.Location = new System.Drawing.Point(0, 0);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(773, 64);
            this.panelFilter.TabIndex = 0;
            // 
            // gridControlPlans
            // 
            this.gridControlPlans.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlPlans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlans.Location = new System.Drawing.Point(0, 64);
            this.gridControlPlans.MainView = this.gridViewPlans;
            this.gridControlPlans.Name = "gridControlPlans";
            this.gridControlPlans.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rTextPercents,
            this.rTextVolume,
            this.rTextInt,
            this.rTextTime,
            this.rTextTwoDigits,
            this.rTextPercentOneDigit});
            this.gridControlPlans.Size = new System.Drawing.Size(773, 379);
            this.gridControlPlans.TabIndex = 1;
            this.gridControlPlans.ToolTipController = this._toolTipController;
            this.gridControlPlans.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlans});
            // 
            // gridViewPlans
            // 
            this.gridViewPlans.Appearance.BandPanel.Options.UseTextOptions = true;
            this.gridViewPlans.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewPlans.Appearance.BandPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewPlans.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewPlans.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewPlans.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewPlans.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewPlans.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewPlans.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandGeneralInfo});
            this.gridViewPlans.ColumnPanelRowHeight = 50;
            this.gridViewPlans.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this._colEmployee,
            this._colLevel,
            this._colEmployeeId});
            this.gridViewPlans.GridControl = this.gridControlPlans;
            this.gridViewPlans.Name = "gridViewPlans";
            this.gridViewPlans.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewPlans.OptionsCustomization.AllowGroup = false;
            this.gridViewPlans.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridViewPlans.OptionsFilter.AllowFilterEditor = false;
            this.gridViewPlans.OptionsFilter.ColumnFilterPopupRowCount = 100;
            this.gridViewPlans.OptionsMenu.EnableFooterMenu = false;
            this.gridViewPlans.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewPlans.OptionsSelection.MultiSelect = true;
            this.gridViewPlans.OptionsView.ColumnAutoWidth = false;
            this.gridViewPlans.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPlans.OptionsView.ShowGroupPanel = false;
            this.gridViewPlans.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.gridViewPlans_CustomDrawColumnHeader);
            this.gridViewPlans.ShowCustomizationForm += new System.EventHandler(this.gridViewPlans_ShowCustomizationForm);
            this.gridViewPlans.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewPlans_ShowingEditor);
            this.gridViewPlans.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewPlans_CellValueChanged);
            this.gridViewPlans.ColumnFilterChanged += new System.EventHandler(this.gridViewPlans_ColumnFilterChanged);
            this.gridViewPlans.ShowFilterPopupCheckedListBox += new DevExpress.XtraGrid.Views.Grid.FilterPopupCheckedListBoxEventHandler(this.gridViewPlans_ShowFilterPopupCheckedListBox);
            this.gridViewPlans.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewPlans_MouseDown);
            this.gridViewPlans.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewPlans_MouseUp);
            this.gridViewPlans.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridViewPlans_MouseMove);
            this.gridViewPlans.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewPlans_ValidatingEditor);
            // 
            // bandGeneralInfo
            // 
            this.bandGeneralInfo.Columns.Add(this._colEmployee);
            this.bandGeneralInfo.Columns.Add(this._colLevel);
            this.bandGeneralInfo.Columns.Add(this._colEmployeeId);
            this.bandGeneralInfo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.bandGeneralInfo.Name = "bandGeneralInfo";
            this.bandGeneralInfo.VisibleIndex = 0;
            this.bandGeneralInfo.Width = 300;
            // 
            // _colEmployee
            // 
            this._colEmployee.Caption = "Сотрудник";
            this._colEmployee.FieldName = "EmployeeName";
            this._colEmployee.MinWidth = 100;
            this._colEmployee.Name = "_colEmployee";
            this._colEmployee.OptionsColumn.AllowEdit = false;
            this._colEmployee.OptionsColumn.ReadOnly = true;
            this._colEmployee.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this._colEmployee.Visible = true;
            this._colEmployee.Width = 120;
            // 
            // _colLevel
            // 
            this._colLevel.Caption = "Уровень";
            this._colLevel.FieldName = "EmployeeLevel";
            this._colLevel.MinWidth = 80;
            this._colLevel.Name = "_colLevel";
            this._colLevel.OptionsColumn.AllowEdit = false;
            this._colLevel.OptionsColumn.ReadOnly = true;
            this._colLevel.OptionsFilter.AllowAutoFilter = false;
            this._colLevel.OptionsFilter.AllowFilter = false;
            this._colLevel.Visible = true;
            this._colLevel.Width = 80;
            // 
            // _colEmployeeId
            // 
            this._colEmployeeId.Caption = "ID сотрудника";
            this._colEmployeeId.FieldName = "EmployeeID";
            this._colEmployeeId.MinWidth = 100;
            this._colEmployeeId.Name = "_colEmployeeId";
            this._colEmployeeId.OptionsColumn.AllowEdit = false;
            this._colEmployeeId.OptionsColumn.ReadOnly = true;
            this._colEmployeeId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this._colEmployeeId.Visible = true;
            this._colEmployeeId.Width = 100;
            // 
            // rTextPercents
            // 
            this.rTextPercents.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rTextPercents.AutoHeight = false;
            this.rTextPercents.DisplayFormat.FormatString = "n2";
            this.rTextPercents.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rTextPercents.Mask.EditMask = "\\d{0,3}\\,\\d{0,2}";
            this.rTextPercents.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.rTextPercents.Name = "rTextPercents";
            this.rTextPercents.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.rText_ParseEditValue);
            // 
            // rTextVolume
            // 
            this.rTextVolume.AutoHeight = false;
            this.rTextVolume.DisplayFormat.FormatString = "n3";
            this.rTextVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rTextVolume.Mask.EditMask = "\\d{0,12}\\,\\d{0,3}";
            this.rTextVolume.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.rTextVolume.Name = "rTextVolume";
            this.rTextVolume.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.rText_ParseEditValue);
            // 
            // rTextInt
            // 
            this.rTextInt.AutoHeight = false;
            this.rTextInt.DisplayFormat.FormatString = "n0";
            this.rTextInt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rTextInt.Mask.EditMask = "###########0";
            this.rTextInt.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.rTextInt.Name = "rTextInt";
            this.rTextInt.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.rText_ParseEditValue);
            // 
            // rTextTime
            // 
            this.rTextTime.AutoHeight = false;
            this.rTextTime.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rTextTime.Mask.EditMask = "\\d{0,3}:\\d{0,2}";
            this.rTextTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.rTextTime.Mask.PlaceHolder = '0';
            this.rTextTime.Mask.ShowPlaceHolders = false;
            this.rTextTime.Name = "rTextTime";
            this.rTextTime.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.rTextTime_ParseEditValue);
            // 
            // rTextTwoDigits
            // 
            this.rTextTwoDigits.AutoHeight = false;
            this.rTextTwoDigits.DisplayFormat.FormatString = "n2";
            this.rTextTwoDigits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rTextTwoDigits.Mask.EditMask = "\\d{0,12}\\,\\d{0,2}";
            this.rTextTwoDigits.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.rTextTwoDigits.Name = "rTextTwoDigits";
            this.rTextTwoDigits.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.rText_ParseEditValue);
            // 
            // rTextOneDigit
            // 
            this.rTextPercentOneDigit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rTextPercentOneDigit.AutoHeight = false;
            this.rTextPercentOneDigit.DisplayFormat.FormatString = "n1";
            this.rTextPercentOneDigit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.rTextPercentOneDigit.Mask.EditMask = "\\d{0,3}\\,\\d{0,1}";
            this.rTextPercentOneDigit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.rTextPercentOneDigit.Name = "rTextPercentOneDigit";
            this.rTextPercentOneDigit.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.rText_ParseEditValue);
            // 
            // _toolTipController
            // 
            this._toolTipController.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this._toolTipController_GetActiveObjectInfo);
            // 
            // _buttonImages
            // 
            this._buttonImages.ImageSize = new System.Drawing.Size(24, 24);
            this._buttonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_buttonImages.ImageStream")));
            this._buttonImages.Images.SetKeyName(0, "bind.png");
            this._buttonImages.Images.SetKeyName(1, "unbind.png");
            // 
            // BasePlanView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlPlans);
            this.Controls.Add(this.panelFilter);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "BasePlanView";
            this.Size = new System.Drawing.Size(773, 443);
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextPercents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextTwoDigits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextPercentOneDigit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonImages)).EndInit();
            this.ResumeLayout(false);

        }

        private void ShowMessChangesWereSaved()
        {
            XtraMessageBox.Show("Данные успешно сохранены!", "Планы", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #region Header buttons handling

        private EditorButton button;
        protected EditorButton Button
        {
            get
            {
                if (button == null)
                {
                    button = new EditorButton(ButtonPredefines.Ellipsis);
                }
                return button;
            }
        }

        private bool isPressed = false;

        private void DrawButton(GraphicsCache cache, Rectangle bounds, ActiveLookAndFeelStyle lookAndFeel, AppearanceObject appearance, ObjectState state, string Caption, GridColumn col)
        {
            EditorButtonObjectInfoArgs args = new EditorButtonObjectInfoArgs(cache, Button, appearance);
            args.Bounds = bounds;
            args.State = state;

            args.Button.Kind = ButtonPredefines.Glyph;

            if (col != null)
            {
                if (CanBeUnBindedToSkip(col))
                {
                    args.Button.Image = _buttonImages.Images[1];//unbind.png
                }
                else
                {
                    args.Button.Image = _buttonImages.Images[0];//bind.png
                }
            }

            EditorButtonPainter painter = EditorButtonHelper.GetPainter(BorderStyles.Default, gridControlPlans.LookAndFeel.ActiveLookAndFeel);
            painter.DrawObject(args);
        }

        private Rectangle GetButtonBoundsByHeaderBounds(Rectangle headerBounds)
        {
            Rectangle buttonBounds = headerBounds;
            buttonBounds.Inflate(-2, -2);
            buttonBounds.Width = 30;
            buttonBounds.Height = 30;
            return buttonBounds;
        }

        private void gridViewPlans_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (IsM2)
            {
                return;
            }

            if (e.Column == null)
            {
                return;
            }


            if (!CanBeBindedToSkip(e.Column))
            {
                return;
            }

            Rectangle buttonBounds = GetButtonBoundsByHeaderBounds(e.Bounds);
            Rectangle captionRect = e.Info.CaptionRect;
            Rectangle prevCaptionRect = captionRect;
            captionRect.Offset(4 + buttonBounds.Width, 0);
            captionRect.Width = captionRect.Width - (4 + buttonBounds.Width);
            e.Info.CaptionRect = captionRect;

            e.Painter.DrawObject(e.Info);

            e.Info.CaptionRect = prevCaptionRect;
            ObjectState state = (e.Column == FocusedColumn && isPressed) ? ObjectState.Pressed : e.Info.State;

            DrawButton(e.Cache, buttonBounds, gridControlPlans.LookAndFeel.ActiveLookAndFeel.ActiveStyle, e.Appearance, state, "...", e.Column);

            e.Handled = true;
        }

        private Rectangle GetColumnHeaderBounds(GridColumn col)
        {
            GridViewInfo vi = gridViewPlans.GetViewInfo() as GridViewInfo;
            return vi.ColumnsInfo[col].Bounds;
        }

        private bool IsMouseOverHeaderButton(GridColumn col)
        {
            Point pt = gridControlPlans.PointToClient(MousePosition);
            GridHitInfo hi = gridViewPlans.CalcHitInfo(pt);
            if (hi.InColumnPanel && hi.Column == col)
            {
                Rectangle r = GetColumnHeaderBounds(hi.Column);
                r = GetButtonBoundsByHeaderBounds(r);
                if (r.Contains(pt))
                    return true;
            }
            return false;
        }

        private GridColumn FocusedColumn
        {
            get
            {
                Point pt = gridControlPlans.PointToClient(MousePosition);
                GridHitInfo hi = gridViewPlans.CalcHitInfo(pt);

                return hi.Column;
            }
        }

        private void gridViewPlans_MouseDown(object sender, MouseEventArgs e)
        {
            if (FocusedColumn == null)
                return;

            if (e.Button != MouseButtons.Left)
                return;

            if (IsMouseOverHeaderButton(FocusedColumn) && CanBeBindedToSkip(FocusedColumn))
            {
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                isPressed = true;
                gridViewPlans.InvalidateColumnHeader(FocusedColumn);
                DoClickHeaderButton(FocusedColumn);
            }
        }

        private void gridViewPlans_MouseUp(object sender, MouseEventArgs e)
        {
            if (isPressed)
            {
                isPressed = false;
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                gridViewPlans.InvalidateColumnHeader(FocusedColumn);
            }
        }

        private void gridViewPlans_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPressed && !IsMouseOverHeaderButton(FocusedColumn))
            {
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                isPressed = false;
                gridViewPlans.InvalidateColumnHeader(FocusedColumn);
            }
        }

        private void _toolTipController_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (IsM2)
            {
                return;
            }

            GridHitInfo hitInfo = gridViewPlans.CalcHitInfo(e.ControlMousePosition);
            if (hitInfo.HitTest != GridHitTest.Column)
            {
                return;
            }

            if (!CanBeBindedToSkip(hitInfo.Column))
            {
                return;
            }

            if (IsMouseOverHeaderButton(hitInfo.Column))
            {
                e.Info = new ToolTipControlInfo(hitInfo.Column, CanBeUnBindedToSkip(hitInfo.Column) ? "Отменить привязку к цели СКиП" : "Выбрать цель из СКИПа");
            }
        }

        #endregion

        private void gridViewPlans_ShowFilterPopupCheckedListBox(object sender, FilterPopupCheckedListBoxEventArgs e)
        {
            if (_displayM1)
                return;

            if (e.Column == _colEmployee)
            {
                for (int i = e.CheckedComboBox.Items.Count - 1; i >= 0; i--)
                {
                    CheckedListBoxItem item = e.CheckedComboBox.Items[i];
                    string itemValue = (string)(item.Value as FilterItem).Value;

                    if (!IsValuePresentInView(e.Column, itemValue))
                    {
                        e.CheckedComboBox.Items.RemoveAt(i);
                    }
                }

                return;
            }

            for (int i = e.CheckedComboBox.Items.Count - 1; i >= 0; i--)
            {
                CheckedListBoxItem item = e.CheckedComboBox.Items[i];
                object itemValue = (item.Value as FilterItem).Value;

                if (itemValue != DBNull.Value && itemValue != null)
                {
                    if (!IsValuePresentInView(e.Column, Convert.ToDecimal(itemValue)))
                    {
                        e.CheckedComboBox.Items.RemoveAt(i);
                    }
                }
            }
        }

        private bool IsValuePresentInView(GridColumn col, string value)
        {
            bool isPresent = false;

            foreach (DataRow row in (gridControlPlans.DataSource as DataTable).Rows)
            {
                string textToCompare = row[_colEmployee.FieldName] == DBNull.Value || row[_colEmployee.FieldName] == null ? string.Empty : Convert.ToString(row[_colEmployee.FieldName]);
                string levelToCompare = Convert.ToString(row[_colLevel.FieldName]);

                if (textToCompare == value && levelToCompare != "M1")
                {
                    isPresent = true;
                    break;
                }
            }

            return isPresent;
        }

        private bool IsValuePresentInView(GridColumn col, decimal value)
        {
            bool isPresent = false;

            foreach (DataRow row in (gridControlPlans.DataSource as DataTable).Rows)
            {
                object valueToCompare = row[col.FieldName];
                if (valueToCompare == DBNull.Value || valueToCompare == null)
                {
                    continue;
                }

                string levelToCompare = Convert.ToString(row[_colLevel.FieldName]);

                if (Convert.ToDecimal(valueToCompare) == value && levelToCompare != "M1")
                {
                    isPresent = true;
                    break;
                }
            }

            return isPresent;
        }

        private void gridViewPlans_ColumnFilterChanged(object sender, EventArgs e)
        {
            if (_isFilterBeingCleared)
                return;

            if (gridViewPlans.ActiveFilterString == "")// on filter turning off
            {
                _displayM1 = true;
                _basePlanController.SetShowM1Value(true);
            }

            if (gridViewPlans.ActiveFilterString == FILTER_Hide_M1)// don't show default filter
            {
                gridViewPlans.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
                gridViewPlans.Invalidate();
            }
            else// synchronize check and filter
            {
                if (gridViewPlans.ActiveFilterEnabled)
                {
                    _basePlanController.SetShowM1Value(_displayM1);
                }
                else
                {
                    _isM1CheckBeingChanged = true;
                    _basePlanController.SetShowM1Value(true);
                    _isM1CheckBeingChanged = false;
                }

                gridViewPlans.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Default;
            }
        }

        private void gridViewPlans_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (gridViewPlans.FocusedColumn.ColumnEdit == rTextTime)
            {
                if(e.Value == null)
                    return;

                //Get the currently edited value 
                TimeEx lTimeEx = (TimeEx)e.Value;

                //Specify validation criteria 
                if (lTimeEx.Minutes >= 60)
                {
                    e.Valid = false;
                    e.ErrorText = "Некоррекный формат времени";
                }
            }
        }

    }
}
