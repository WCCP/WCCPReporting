﻿using System;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.View {
    public interface IBaseSKUConfigView : ISimpleControlView {
        bool IsValid { get; }

        void SaveAction();
        bool RefreshAction();
        bool LeaveTabActions();
        void PostEditor();

        void OnListTypeChanged(int newId);
        void OnDateChanged(DateTime newDate);
        void OnCountryChanged(int newId);
        void OnM3Changed(int newId);
        void OnDateChanging(DateChangingEventArgs args);
        void OnCountryChanging(ListItemChangingEventArgs args);

        void OnResize();
    }
}