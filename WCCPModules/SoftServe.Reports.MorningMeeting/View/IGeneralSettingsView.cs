﻿using System.Collections.Generic;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.View
{
    interface IGeneralSettingsView : ISimpleControlView
    {
        void SetAccessibility(bool canEdit);

        void LoadMPs(List<ListItemModel> programs);

        void LoadAkbCalcMethods(List<ListItemModel> methods);
        void LoadSrCalcMethods(List<ListItemModel> methods);
        
        void AssignSettings(GeneralSettingsModel settings);
        void GetSettings(GeneralSettingsModel settings);

        void GetCountrySettings(CountryLevelSettingsModel settings);
        void AssignCountrySettings(CountryLevelSettingsModel settings);

        void GetSelectedMP(List<ActionListModel> programs, int countryId, int m3Id);
        void AssignSelectedMP(List<ActionListModel> programs);

        bool ValidateRecord();
        void HideValidation(); 
        
        void SaveActions();
        void RefreshActions();
        bool LeaveTabActions();
        bool SaveChangesAndProceed();
    }
}
