﻿namespace SoftServe.Reports.MorningMeeting.DataAccess
{
    public static class SqlConstants
    {
        public static string DelimiterIdList = ",";
        public static string ItemIdFieldName = "Item_ID";
        public static string LevelFieldName = "Level";

        #region SP Params

        public const string ParamDate = "@Date";
        public const string ParamCountryId = "@Country_ID";

        public const string ParamM3Id = "@M3_ID";
        public const string ParamListType = "@ListType";
        public const string ParamSetId = "@setId";
        public const string ParamSKUGroupId = "@SKUGroupId";
        public const string ParamSKUGroupName = "@SKUGroupName";
        public const string ParamSortOrder = "@SortOrder";
        public const string ParamChannelMask = "@channelMask";
        public const string ParamData = "@Data";
        public const string ParamProductCnId = "@productCnId";
        public const string ParamIsInPackage = "@InPackage";

        public const string ParamForceDelete = "@force_delete";
        public const string ParamErrorCode = "@ERROR_CODE";

        //public const string Param = "";

        #endregion


        #region Filter & Helpers

        public const string SP_GET_COUNTRY_LIST = "mm.spDW_GetCountryList";
        public const string SP_GET_STAFF_TREE = "mm.spDW_GetStaffTreeByCountry";
        public const string SP_GET_CHANNEL_LIST = "MM.spDW_GetChannelList";
        public const string SpDwUrmGetUserRoles = "dbo.spDW_URM_GetUserRoles";

        #endregion

        #region Product Set & SKU List

        public const string SpGetProductGroups = "MM.spDW_GetProductGroups";
        public const string SpGetProductGroupDetail = "MM.spDW_GetProductGroupDetails";
        public const string SpSetProductGroup = "MM.spDW_SetProductGroup";
        public const string SpSetProductGroupDetails = "MM.spDW_SetProductGroupDetails";
        public const string SpDeleteProductGroup = "MM.spDW_DeleteProductGroup";
        public const string SpGetSkuListItems = "MM.spDW_GetSKUListItems";
        public const string SpSetSkuListItem = "MM.spDW_SetSKUListItem";
        public const string SpDeleteSkuListItem = "MM.spDW_DeleteSKUListItem";
        public const string SpGetProductGroupsDoublicates = "MM.spDW_GetProductGroupsDoublicates";
        
        #endregion

        #region Settings Tab

        public const string spGetSettingsSet = "MM.spDW_GetCommonSettings";
        public const string spGetSettingsSet_Date = "Date";
        public const string spGetSettingsSet_CountryId = "Country_ID";

        public const string spSetSettingsSet = "MM.spDW_SetCommonSettings";
        public const string spSetSettingsSet_Date = "Date";
        public const string spSetSettingsSet_CountryId = "Country_ID";
        public const string spSetSettingsSet_M3Id = "M3_ID";
        public const string spSetSettingsSet_AmountOfTheWorstOLs = "AmountOfTheWorstOLs";
        public const string spSetSettingsSet_AmountOfFocusOLs = "AmountOfFocusOLs";
        public const string spSetSettingsSet_TimeOfFirstVisit = "TimeOfFirstVisit";
        public const string spSetSettingsSet_TimeOfLastVisit = "TimeOfLastVisit";
        public const string spSetSettingsSet_InBevBeerVolumeLimit = "InBevBeerVolumeLimit";
        public const string spSetSettingsSet_AmountOfDistPushSKU = "AmountOfDistrPushSKU";
        public const string spSetSettingsSet_AmountOfFocusSKU = "AmountOfFocusSKU";
        public const string spSetSettingsSet_AmountOfMP = "AmountOfMP";
        public const string spSetSettingsSet_AmountOfContracts = "AmountOfContracts";
        public const string spSetSettingsSet_AmountOfFocusTargets = "AmountOfFocusTargets";
        public const string spSetSettingsSet_FocusByDSM = "DSMFocusText";

        public const string spGetCountrySettings = "MM.spDW_GetCountryCommonSettings";
        public const string spGetCountrySettings_CountryId = "Country_Id";

        public const string spSetCountrySettings = "MM.spDW_SetCountryCommonSettings";
        public const string spSetCountrySettings_CountryId = "Country_Id";
        public const string spSetCountrySettings_SaleGracePeriod = "SaleGracePeriod";
        public const string spSetCountrySettings_PackageDistributionPeriod = "PackageDistributionPeriod";
        public const string spSetCountrySettings_ACB_Method_ID = "ACB_Method_ID";
        public const string spSetCountrySettings_SR_Method_ID = "SR_Method_ID";

        public const string spGetMarketProgramsList = "MM.spDW_GetActionList";
        public const string spGetMarketProgramsList_Date = "Date";
        public const string spGetMarketProgramsList_CountryId = "Country_ID";
        
        public const string spGetSelectedMarketProgramsList = "MM.spDW_GetSettingsMP";
        public const string spGetSelectedMarketProgramsList_Date = "Date";
        public const string spGetSelectedMarketProgramsList_CountryId = "Country_ID";
        
        public const string spSetSelectedMarketProgramsList = "MM.spDW_SetSettingsMP";
        public const string spSetSelectedMarketProgramsList_Date = "Date";
        public const string spSetSelectedMarketProgramsList_CountryId = "Country_ID";
        public const string spSetSelectedMarketProgramsList_M3Id = "M3_ID";
        public const string spSetSelectedMarketProgramsList_MPId = "Items";

        #endregion Settings Tab

        #region FocusGoals

        public const string spGetGoalTypesList = "MM.spDW_GetFocusTargetTypes";

        public const string spGetAutoGoalsTypesList = "MM.spDW_GetListOfFocusTargetKPI";

        public const string spGetGoalsList = "MM.spDW_GetListOfFocusTargets";
        public const string spGetGoalsList_DateFrom = "DateFrom";
        public const string spGetGoalsList_DateTo = "DateTo";
        public const string spGetGoalsList_CountryId = "Country_ID";

        public const string spGetManualDetailsList = "MM.spDW_GetListOfFocusTargetType_Manual";
        public const string spGetManualDetailsList_DateFrom = "DateFrom";
        public const string spGetManualDetailsList_DateTo = "DateTo";
        public const string spGetManualDetailsList_CountryId = "Country_ID";

        public const string spGetAutoDetailsList = "MM.spDW_GetListOfFocusTargetType_Auto";
        public const string spGetAutoDetailsList_DateFrom = "DateFrom";
        public const string spGetAutoDetailsList_DateTo = "DateTo";
        public const string spGetAutoDetailsList_CountryId = "Country_ID";

        public const string spGetProductTree = "MM.spDW_GetProductTree";
        public const string spGetProductTree_ShowLastLevel = "ShowLastLevel";

        public const string spDeleteGoal = "MM.spDW_FocusTarget_Delete";
        public const string spDeleteGoal_FocusTarget_ID = "FocusTarget_ID";

        public const string spSetFocusGoal = "MM.spDW_FocusTarget_Edit";
        public const string spSetFocusGoal_FocusTarget_ID = "FocusTarget_ID";
        public const string spSetFocusGoal_CountryID = "CountryID";
        public const string spSetFocusGoal_M3_ID = "M3_ID";
        public const string spSetFocusGoal_FocusTargetName = "FocusTargetName";
        public const string spSetFocusGoal_IsManualTarget = "IsManualTarget";
        public const string spSetFocusGoal_DateCreated = "DateCreated";
        public const string spSetFocusGoal_StartDate = "StartDate";
        public const string spSetFocusGoal_EndDate = "EndDate";
        public const string spSetFocusGoal_ChannelMask = "ChannelMask";

        public const string spSetManualDetails = "MM.spDW_FocusTargetManual_Edit";
        public const string spSetManualDetails_FocusTarget_ID = "FocusTarget_ID";
        public const string spSetManualDetails_TaskText = "TaskText";

        public const string spSetAutoDetails = "MM.spDW_FocusTargetAuto_Edit";
        public const string spSetAutoDetails_FocusTarget_ID = "FocusTarget_ID";
        public const string spSetAutoDetails_TargetKPI_ID = "TargetKPI_ID";
        public const string spSetAutoDetails_ProductCn_ID = "ProductCn_ID";

        #endregion

        #region Ratings

        public const string spGetProductGroupList = "MM.spDW_GetListOfProductSets";
        public const string spGetProductGroupList_CountryId = "Country_ID";
        public const string spGetProductGroupList_Date = "Date";

        public const string spGetSkipChannelList = "spDW_CAPS_ListOfChannelTypes";

        public const string spKpiTypesList = "MM.spDW_GetListOfKPITypes";

        public const string spKpiList = "MM.spDW_GetListOfKPIs";

        public const string spDeleteRating = "MM.spDW_DeleteRatingKPI";
        public const string spDeleteRating_KPI_ID = "RKPI_ID";

        public const string spGetRatingsList = "MM.spDW_GetListOfRatings";
        public const string spGetRatingsList_CountryId = "Country_ID";
        public const string spGetRatingsList_Date = "Date";

        public const string spGetRatingsColumnsList = "MM.spDW_GetListOfKPIColumns";
        public const string spGetRatingsColumnsList_CountryId = "Country_ID";
        public const string spGetRatingsColumnsList_Date = "Date";

        public const string spSaveRatingCaps = "MM.spDW_EditRatingKPIToCapsChannels";
        public const string spSaveRatingCaps_RKPI_ID = "RKPI_ID";
        public const string spSaveRatingCaps_Date = "Date";
        public const string spSaveRatingCaps_CountryId = "Country_ID";
        public const string spSaveRatingCaps_M3_ID = "M3_ID";
        public const string spSaveRatingCaps_Channel_ID = "Channel_ID";
        public const string spSaveRatingCaps_Weight = "Weight";
        public const string spSaveRatingCaps_CapsChannel_ID = "CapsChannel_ID";
        public const string spSaveRatingCaps_ProductGroup_ID = "ProductGroup_ID";

        public const string spSaveRatingColumn = "MM.spDW_EditRatingKPI";
        public const string spSaveRatingColumn_RKPI_ID = "RKPI_ID";
        public const string spSaveRatingColumn_Date = "Date";
        public const string spSaveRatingColumn_CountryId = "Country_ID";
        public const string spSaveRatingColumn_M3_ID = "M3_ID";
        public const string spSaveRatingColumn_Channel_ID = "Channel_ID";
        public const string spSaveRatingColumn_Weight = "Weight";
        public const string spSaveRatingColumn_Column_ID = "Column_ID";

        #endregion

        #region Networks Plans

        public const string spGetNetworkColumnList = "MM.spDW_GetNetworksPlanMeta";
        public const string spGetNetworkColumnList_CountryId = "CountryID";
        public const string spGetNetworkColumnList_M3_ID = "M3_ID";
        public const string spGetNetworkColumnList_Date = "Date";

        public const string spGetNetworkPlans = "MM.spDW_GetNetworksPlanData";
        public const string spGetNetworkPlans_CountryId = "CountryID";
        public const string spGetNetworkPlans_M3_ID = "M3_ID";
        public const string spGetNetworkPlans_Date = "Date";

        #endregion

        #region Products Plans

        public const string spGetProductColumnList = "MM.spDW_GetProductsPlanMeta";
        public const string spGetProductColumnList_CountryId = "CountryID";
        public const string spGetProductColumnList_M3_ID = "M3_ID";
        public const string spGetProductColumnList_Date = "Date";

        public const string spGetProductPlans = "MM.spDW_GetProductsPlanData";
        public const string spGetProductPlans_CountryId = "CountryID";
        public const string spGetProductPlans_M3_ID = "M3_ID";
        public const string spGetProductPlans_Date = "Date";

        #endregion

        #region General Plans

        public const string spGetGeneraColumnList = "MM.spDW_GetSimplePlanMeta";
        public const string spGetGeneraColumnList_CountryId = "CountryID";
        public const string spGetGeneraColumnList_M3_ID = "M3_ID";
        public const string spGetGeneraColumnList_Date = "Date";

        public const string spGetGeneralPlans = "MM.spDW_GetSimplePlanData";
        public const string spGetGeneralPlans_CountryId = "CountryID";
        public const string spGetGeneralPlans_M3_ID = "M3_ID";
        public const string spGetGeneralPlans_Date = "Date";

        #endregion

        #region Plans Editing

        public const string spEditPlan = "MM.spDW_EditMonthlyPlan";
        public const string spEditPlan_StaffID = "StaffID";
        public const string spEditPlan_Column_ID = "Column_ID";
        public const string spEditPlan_Value = "Value";

        #endregion

        #region Binding to Caps

        public const string spGetCapsSubtargets = "MM.spDW_GetListOfCapsSubtargets";
        public const string spGetCapsSubtargets_Country_ID = "Country_ID";
        public const string spGetCapsSubtargets_Date = "Date";

        public const string spAutoMapCapsSubtargets = "MM.spDW_CapsSubtargetsAutoMapping";
        public const string spAutoMapCapsSubtargets_Country_ID = "Country_ID";
        public const string spAutoMapCapsSubtargets_Date = "date";
        public const string spAutoMapCapsSubtargets_M3_ID = "M3_ID";

        public const string spBindColumnToSubtargets = "MM.spDW_AddCapsSubtargetToColumn";
        public const string spBindColumnToSubtargets_Column_ID = "Column_ID";
        public const string spBindColumnToSubtargets_Subtarget_id = "Subtarget_id";

        public const string spUnbindColumnToSubtargets = "MM.spDW_RemoveCapsSubtargetToColumn";
        public const string spUnbindColumnToSubtargets_Column_ID = "Column_ID";
        public const string spUnbindColumnToSubtargets_Subtarget_id = "Subtarget_id";

        #endregion

        #region Import From Excel File
        public const string spGetInvalidPlans = "MM.spDW_CheckImportFile";

        public const string spSavePlans = "MM.spDW_UpdateMonthlyPlanFromImportFile";
        #endregion
    }
}