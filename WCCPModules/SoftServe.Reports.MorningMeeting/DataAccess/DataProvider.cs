﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using BLToolkit.Data;
using Logica.Reports.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.DataAccess
{
    public static class DataProvider
    {
        private static DbManager _db;

        private static readonly string INSERT_PLAN_TEMPLATE = @"INSERT INTO #DW_MonthlyPlanLoadTable 
                                               (ID, StaffID, StaffName, Column_Name, Column_Date, Column_PlanValue) 
                                                VALUES";

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        #endregion

        #region Common helpers

        public static List<ChannelModel> GetChannelList()
        {
            return Db.SetSpCommand(SqlConstants.SP_GET_CHANNEL_LIST).ExecuteList<ChannelModel>();
        }

        public static List<RoleModel> GetUserRoles()
        {
            return Db.SetCommand(SqlConstants.SpDwUrmGetUserRoles).ExecuteList<RoleModel>();
        }

        public static List<ProductTreeModel> GetProductList(int countryId, bool showLastLevel)
        {
            return Db.SetSpCommand(SqlConstants.spGetProductTree,
                Db.Parameter(SqlConstants.ParamCountryId, countryId),
                Db.Parameter(SqlConstants.spGetProductTree_ShowLastLevel, showLastLevel)
                ).ExecuteList<ProductTreeModel>();
        }

        #endregion

        #region FilterControl

        public static List<ListItemModel> GetListTypes(ListTypeEnum listType)
        {
            List<ListItemModel> lList = new List<ListItemModel>(2);
            ListItemModel lItem;

            switch (listType)
            {
                case ListTypeEnum.ProductSet:
                    {
                        lItem = new ListItemModel { Id = 1, Name = "Продукция" };
                        lList.Add(lItem);
                        lItem = new ListItemModel { Id = 2, Name = "Среднее СКЮ" };
                        lList.Add(lItem);
                        lItem = new ListItemModel { Id = 3, Name = "В Штуках" };// ---- Bushido_Gap_B2 101 2015.10.12
                        lList.Add(lItem);
                    }
                    break;
                case ListTypeEnum.SKUList:
                    {
                        lItem = new ListItemModel { Id = 1, Name = "DistrPush" };
                        lList.Add(lItem);
                        lItem = new ListItemModel { Id = 2, Name = "Фокусные СКЮ" };
                        lList.Add(lItem);
                    }
                    break;
            }

            return lList;
        }

        public static List<CountryModel> GetCountryList()
        {
            return Db.SetSpCommand(SqlConstants.SP_GET_COUNTRY_LIST).ExecuteList<CountryModel>();
        }

        public static List<StaffTreeModel> GetStaffTreeList(int countryId)
        {
            return Db.SetSpCommand(SqlConstants.SP_GET_STAFF_TREE,
                Db.Parameter(SqlConstants.ParamCountryId, countryId)).ExecuteList<StaffTreeModel>();
        }

        #endregion

        #region Product Groups

        public static List<ProductGroupModel> GetProductGroups(DateTime date, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.SpGetProductGroups,
                Db.Parameter(SqlConstants.ParamDate, date),
                Db.Parameter(SqlConstants.ParamCountryId, countryId)
                ).ExecuteList<ProductGroupModel>();
        }

        public static List<ProductGroupDetailModel> GetProductGroupsDetail(DateTime date, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.SpGetProductGroupDetail,
                Db.Parameter(SqlConstants.ParamDate, date),
                Db.Parameter(SqlConstants.ParamCountryId, countryId)
                ).ExecuteList<ProductGroupDetailModel>();
        }

        public static int SetProductGroup(DateTime date, ProductGroupModel productGroup, out bool wasChangedBySbdElse)
        {
            wasChangedBySbdElse = false;

            IDbDataParameter lSKUGroupIdParam = Db.Parameter(ParameterDirection.InputOutput, SqlConstants.ParamSKUGroupId,
                productGroup.SKUGroupId);
            IDbDataParameter lSetIdParam = Db.Parameter(ParameterDirection.InputOutput, SqlConstants.ParamSetId, productGroup.SetId);
            IDbDataParameter lGroupSortOrderParam = Db.Parameter(ParameterDirection.InputOutput, SqlConstants.ParamSortOrder, productGroup.SortOrder);

            int lResult = 0;
            try
            {
                Db.SetSpCommand(SqlConstants.SpSetProductGroup,
                    lSKUGroupIdParam,
                    Db.Parameter(SqlConstants.ParamDate, date),
                    Db.Parameter(SqlConstants.ParamListType, productGroup.ListType),
                    Db.Parameter(SqlConstants.ParamCountryId, productGroup.CountryId),
                    Db.Parameter(SqlConstants.ParamM3Id, productGroup.M3Id),
                    lSetIdParam,
                    Db.Parameter(SqlConstants.ParamSKUGroupName, productGroup.SKUGroupName),
                    lGroupSortOrderParam,
                    Db.Parameter(SqlConstants.ParamChannelMask, productGroup.ChannelMask)
                    ).ExecuteNonQuery();

                productGroup.SKUGroupId = (int)Db.Parameter(SqlConstants.ParamSKUGroupId).Value;
                productGroup.SetId = (int)Db.Parameter(SqlConstants.ParamSetId).Value;

                if (productGroup.SortOrder != (double)Db.Parameter(SqlConstants.ParamSortOrder).Value)
                {
                    wasChangedBySbdElse = true;
                    productGroup.SortOrder = (double)Db.Parameter(SqlConstants.ParamSortOrder).Value;
                }
            }
            catch (Exception)
            {
                lResult = -1;
            }
            return lResult;
        }

        public static int SetProductGroupDetails(int skuGroupId, SqlXml xmlData)
        {
            int lResult = 0;
            try
            {
                Db.SetSpCommand(SqlConstants.SpSetProductGroupDetails,
                    Db.Parameter(SqlConstants.ParamSKUGroupId, skuGroupId),
                    Db.Parameter(SqlConstants.ParamData, xmlData)
                    ).ExecuteNonQuery();
            }
            catch
            {
                lResult = -1;
            }
            return lResult;
        }

        public static int DeleteProductGroup(int skuGroupId, bool force = false)
        {
            int lErrorCode = 0;
            Db.SetSpCommand(SqlConstants.SpDeleteProductGroup,
                Db.Parameter(SqlConstants.ParamSKUGroupId, skuGroupId),
                Db.Parameter(SqlConstants.ParamForceDelete, force),
                Db.Parameter(ParameterDirection.InputOutput, SqlConstants.ParamErrorCode, lErrorCode)
                ).ExecuteNonQuery();
            lErrorCode = Convert.ToInt32(Db.Parameter(SqlConstants.ParamErrorCode).Value);
            return lErrorCode;
        }

        public static List<string> GetProductGroupsDoubles(DateTime date, int countryId, int m3Id, int productGroupId, string productGroupName)
        {
            DataTable table = Db.SetSpCommand(SqlConstants.SpGetProductGroupsDoublicates,
                                       Db.Parameter(SqlConstants.ParamDate, date),
                                       Db.Parameter(SqlConstants.ParamCountryId, countryId),
                                       Db.Parameter(SqlConstants.ParamM3Id, m3Id),
                                       Db.Parameter(SqlConstants.ParamSKUGroupId, productGroupId),
                                       Db.Parameter(SqlConstants.ParamSKUGroupName, productGroupName)
                    ).ExecuteDataTable();

            return (from DataRow row in table.Rows select row.Field<string>(0)).ToList();
        }

        #endregion

        #region SKU List

        public static List<SKUListItemModel> GetSkuListItems(DateTime date, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.SpGetSkuListItems,
                Db.Parameter(SqlConstants.ParamDate, date),
                Db.Parameter(SqlConstants.ParamCountryId, countryId)
                ).ExecuteList<SKUListItemModel>();
        }

        public static int SetSkuListItem(DateTime date, SKUListItemModel sku)
        {
            IDbDataParameter lSKUGroupIdParam = Db.Parameter(ParameterDirection.InputOutput, SqlConstants.ParamSKUGroupId, sku.SKUGroupId);
            IDbDataParameter lSetIdParam = Db.Parameter(ParameterDirection.InputOutput, SqlConstants.ParamSetId, sku.SetId);

            int lResult = 0;
            try
            {
                Db.SetSpCommand(SqlConstants.SpSetSkuListItem,
                    lSKUGroupIdParam,
                    Db.Parameter(SqlConstants.ParamDate, date),
                    Db.Parameter(SqlConstants.ParamListType, sku.ListType),
                    Db.Parameter(SqlConstants.ParamCountryId, sku.CountryId),
                    Db.Parameter(SqlConstants.ParamM3Id, sku.M3Id),
                    lSetIdParam,
                    Db.Parameter(SqlConstants.ParamSortOrder, sku.SortOrder),
                    Db.Parameter(SqlConstants.ParamChannelMask, sku.ChannelMask),
                    Db.Parameter(SqlConstants.ParamProductCnId, sku.ProductCnId),
                    Db.Parameter(SqlConstants.ParamIsInPackage, sku.IsInPackage)
                    ).ExecuteNonQuery();

                sku.SKUGroupId = (int)Db.Parameter(SqlConstants.ParamSKUGroupId).Value;
                sku.SetId = (int)Db.Parameter(SqlConstants.ParamSetId).Value;
            }
            catch
            {
                lResult = -1;
            }
            return lResult;
        }

        public static int DeleteSkuListItem(int skuGroupId, bool force = false)
        {
            int lErrorCode = 0;
            Db.SetSpCommand(SqlConstants.SpDeleteSkuListItem,
                Db.Parameter(SqlConstants.ParamSKUGroupId, skuGroupId),
                Db.Parameter(SqlConstants.ParamForceDelete, force),
                Db.Parameter(ParameterDirection.InputOutput, SqlConstants.ParamErrorCode, lErrorCode)
                ).ExecuteNonQuery();
            lErrorCode = Convert.ToInt32(Db.Parameter(SqlConstants.ParamErrorCode).Value);
            return lErrorCode;
        }

        #endregion

        #region Settings Tab Procedures

        public static List<GeneralSettingsModel> GetSettingsSet(DateTime date, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.spGetSettingsSet,
                Db.Parameter(SqlConstants.spGetSettingsSet_Date, date),
                Db.Parameter(SqlConstants.spGetSettingsSet_CountryId, countryId)
                ).ExecuteList<GeneralSettingsModel>();
        }

        public static void SetSettingsSet(DateTime date, GeneralSettingsModel setting)
        {
            object lTimeOfFirstVisit = DBNull.Value;
            if (setting.TimeOfFirstVisit != DateTime.MinValue)
            {
                lTimeOfFirstVisit = setting.TimeOfFirstVisit;
            }

            object lTimeOfLasttVisit = DBNull.Value;
            if (setting.TimeOfLastVisit != DateTime.MinValue)
            {
                lTimeOfLasttVisit = setting.TimeOfLastVisit;
            }

            try
            {
                Db.SetSpCommand(SqlConstants.spSetSettingsSet,
                    Db.Parameter(SqlConstants.spSetSettingsSet_Date, date),
                    Db.Parameter(SqlConstants.spSetSettingsSet_CountryId, setting.CountryId),
                    Db.Parameter(SqlConstants.spSetSettingsSet_M3Id, setting.M3Id),
                    Db.Parameter(SqlConstants.spSetSettingsSet_AmountOfTheWorstOLs, setting.AmountOfTheWorstOLs),
                    Db.Parameter(SqlConstants.spSetSettingsSet_AmountOfFocusOLs, setting.AmountOfFocusOLs),
                    Db.Parameter(SqlConstants.spSetSettingsSet_TimeOfFirstVisit, lTimeOfFirstVisit),
                    Db.Parameter(SqlConstants.spSetSettingsSet_TimeOfLastVisit, lTimeOfLasttVisit),
                    Db.Parameter(SqlConstants.spSetSettingsSet_InBevBeerVolumeLimit, setting.InBevBeerVolumeLimit),
                    Db.Parameter(SqlConstants.spSetSettingsSet_AmountOfDistPushSKU, setting.AmountOfDistrPushSKU),
                    Db.Parameter(SqlConstants.spSetSettingsSet_AmountOfFocusSKU, setting.AmountOfFocusSKU),
                    Db.Parameter(SqlConstants.spSetSettingsSet_AmountOfMP, setting.AmountOfMP),
                    Db.Parameter(SqlConstants.spSetSettingsSet_AmountOfContracts, DBNull.Value),
                    Db.Parameter(SqlConstants.spSetSettingsSet_AmountOfFocusTargets, setting.AmountOfFocusTargets),
                    Db.Parameter(SqlConstants.spSetSettingsSet_FocusByDSM, setting.FocusByDSM)
                    ).ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static CountryLevelSettingsModel GetCountrySettings(int countryId)
        {
            return
                Db.SetSpCommand(SqlConstants.spGetCountrySettings, Db.Parameter(SqlConstants.spGetCountrySettings_CountryId, countryId)).
                    ExecuteObject<CountryLevelSettingsModel>();
        }

        public static void SetCountrySettings(int countryId, CountryLevelSettingsModel settings)
        {
            Db.SetSpCommand(SqlConstants.spSetCountrySettings,
                Db.Parameter(SqlConstants.spSetCountrySettings_CountryId, countryId),
                Db.Parameter(SqlConstants.spSetCountrySettings_SaleGracePeriod, settings.SalesGracePeriod),
                Db.Parameter(SqlConstants.spSetCountrySettings_PackageDistributionPeriod, settings.PackageDistributionPeriod),
                Db.Parameter(SqlConstants.spSetCountrySettings_ACB_Method_ID, settings.AkbCalcMethodId),
                Db.Parameter(SqlConstants.spSetCountrySettings_SR_Method_ID, settings.SrCalcMethodId)
                ).ExecuteNonQuery();
        }

        public static List<ListItemModel> GetMarketProgramsList(DateTime date, int countryId)
        {
            //return new List<ListItemModel>();
            return Db.SetSpCommand(SqlConstants.spGetMarketProgramsList,
                Db.Parameter(SqlConstants.spGetMarketProgramsList_Date, date),
                Db.Parameter(SqlConstants.spGetMarketProgramsList_CountryId, countryId)
                ).ExecuteList<ListItemModel>();
        }

        public static List<ListItemModel> GetAkbCalcMethods()
        {
            string selectAkbList = "SELECT ACB_Method_ID AS ID, ACB_Method_Name AS Name FROM MM.DW_ACB_Methods";

            return Db.SetCommand(selectAkbList).ExecuteList<ListItemModel>();
        }

        public static List<ListItemModel> GetSrCalcMethods()
        {
            string selectSrList = "SELECT SR_Method_ID AS ID, SR_Method_Name AS Name FROM MM.DW_SR_Methods";

            return Db.SetCommand(selectSrList).ExecuteList<ListItemModel>();
        }

        public static List<ActionListModel> GetSelectedMarketProgramsList(DateTime date, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.spGetSelectedMarketProgramsList,
                Db.Parameter(SqlConstants.spGetSelectedMarketProgramsList_Date, date),
                Db.Parameter(SqlConstants.spGetSelectedMarketProgramsList_CountryId, countryId)
                ).ExecuteList<ActionListModel>();
        }

        public static void SetSelectedMarketProgramsList(DateTime date, int countryId, int M3Id, string marketPrograms)
        {
            Db.SetSpCommand(SqlConstants.spSetSelectedMarketProgramsList,
                Db.Parameter(SqlConstants.spSetSelectedMarketProgramsList_Date, date),
                Db.Parameter(SqlConstants.spSetSelectedMarketProgramsList_CountryId, countryId),
                Db.Parameter(SqlConstants.spSetSelectedMarketProgramsList_M3Id, M3Id),
                Db.Parameter(SqlConstants.spSetSelectedMarketProgramsList_MPId, marketPrograms)).ExecuteNonQuery();
        }

        #endregion

        #region FocusGoals Procedures

        public static List<ListItemModel> GetGoalTypesList()
        {
            return Db.SetSpCommand(SqlConstants.spGetGoalTypesList).ExecuteList<ListItemModel>();
        }

        public static List<ListItemModel> GetAutoGoalsTypesList()
        {
            return Db.SetSpCommand(SqlConstants.spGetAutoGoalsTypesList).ExecuteList<ListItemModel>();
        }

        public static List<FocusGoalModel> GetGoalsList(DateTime dateFrom, DateTime dateTo, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.spGetGoalsList,
                Db.Parameter(SqlConstants.spGetGoalsList_DateFrom, dateFrom.Date),
                Db.Parameter(SqlConstants.spGetGoalsList_DateTo, dateTo.Date),
                Db.Parameter(SqlConstants.spGetGoalsList_CountryId, countryId)
                ).ExecuteList<FocusGoalModel>();
        }

        public static List<ManualGoalDetailsModel> GetManualDetailsList(DateTime dateFrom, DateTime dateTo, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.spGetManualDetailsList,
                Db.Parameter(SqlConstants.spGetManualDetailsList_DateFrom, dateFrom),
                Db.Parameter(SqlConstants.spGetManualDetailsList_DateTo, dateTo),
                Db.Parameter(SqlConstants.spGetManualDetailsList_CountryId, countryId)
                ).ExecuteList<ManualGoalDetailsModel>();
        }

        public static List<AutoGoalDetailsModel> GetAutoDetailsList(DateTime dateFrom, DateTime dateTo, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.spGetAutoDetailsList,
                Db.Parameter(SqlConstants.spGetAutoDetailsList_DateFrom, dateFrom),
                Db.Parameter(SqlConstants.spGetAutoDetailsList_DateTo, dateTo),
                Db.Parameter(SqlConstants.spGetAutoDetailsList_CountryId, countryId)
                ).ExecuteList<AutoGoalDetailsModel>();
        }

        public static int UpdateGoal(FocusGoalModel goal, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.spSetFocusGoal,
                Db.Parameter(SqlConstants.spSetFocusGoal_FocusTarget_ID, goal.TargetId),
                Db.Parameter(SqlConstants.spSetFocusGoal_CountryID, countryId),
                Db.Parameter(SqlConstants.spSetFocusGoal_M3_ID, goal.M3Id),
                Db.Parameter(SqlConstants.spSetFocusGoal_FocusTargetName, goal.TargetName),
                Db.Parameter(SqlConstants.spSetFocusGoal_IsManualTarget, goal.IsManual),
                Db.Parameter(SqlConstants.spSetFocusGoal_DateCreated, goal.CreationDate),
                Db.Parameter(SqlConstants.spSetFocusGoal_StartDate, goal.StartDate.Date),
                Db.Parameter(SqlConstants.spSetFocusGoal_EndDate, goal.EndDate.Date),
                Db.Parameter(SqlConstants.spSetFocusGoal_ChannelMask, goal.ChannelMask)
                ).ExecuteScalar<int>();
        }

        public static void UpdateManualDetails(ManualGoalDetailsModel details)
        {
            Db.SetSpCommand(SqlConstants.spSetManualDetails,
                Db.Parameter(SqlConstants.spSetManualDetails_FocusTarget_ID, details.TargetId),
                Db.Parameter(SqlConstants.spSetManualDetails_TaskText, details.Description)
                ).ExecuteNonQuery();
        }

        public static void UpdateAutoDetails(AutoGoalDetailsModel details)
        {
            Db.SetSpCommand(SqlConstants.spSetAutoDetails,
                Db.Parameter(SqlConstants.spSetAutoDetails_FocusTarget_ID, details.TargetId),
                Db.Parameter(SqlConstants.spSetAutoDetails_TargetKPI_ID, details.TargetTypeId),
                Db.Parameter(SqlConstants.spSetAutoDetails_ProductCn_ID, details.Products)
                ).ExecuteNonQuery();
        }

        public static void DeleteGoal(int targetId)
        {
            Db.SetSpCommand(SqlConstants.spDeleteGoal,
                Db.Parameter(SqlConstants.spDeleteGoal_FocusTarget_ID, targetId)
                ).ExecuteNonQuery();
        }

        #endregion

        #region Ratings Procedures

        public static List<RatingsModel> GetRatingsList(DateTime date, int countryId)
        {
            List<RatingsModel> ratingsList = Db.SetSpCommand(SqlConstants.spGetRatingsList,
                Db.Parameter(SqlConstants.spGetRatingsList_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetRatingsList_Date, date)
                ).ExecuteList<RatingsModel>();

            return ratingsList;
        }

        public static List<RatingsColumnsModel> GetRatingsColumnsList(DateTime date, int countryId)
        {
            List<RatingsColumnsModel> ratingsList = Db.SetSpCommand(SqlConstants.spGetRatingsColumnsList,
                Db.Parameter(SqlConstants.spGetRatingsList_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetRatingsList_Date, date)
                ).ExecuteList<RatingsColumnsModel>();

            // DevEx: for  LookUpEdit repository item: ValueMember property should point to a data column,
            //whose data type is exactly the same as the data type of the grid column's underlying field
            // - e.g. string
            foreach (RatingsColumnsModel col in ratingsList)
            {
                col.ColumnIdDescription = col.ColumnId.ToString();
            }

            return ratingsList;
        }

        public static List<ListItemModel> GetKpiTypesList()
        {
            List<ListItemModel> l = Db.SetSpCommand(SqlConstants.spKpiTypesList).ExecuteList<ListItemModel>();

            return l;
        }

        public static List<KpiModel> GetKpiList()
        {
            List<KpiModel> l = Db.SetSpCommand(SqlConstants.spKpiList).ExecuteList<KpiModel>();

            return l;
        }

        public static List<ProductListItemModel> GetProductSetList(DateTime date, int countryId)
        {
            return Db.SetSpCommand(SqlConstants.spGetProductGroupList,
                Db.Parameter(SqlConstants.spGetProductGroupList_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetProductGroupList_Date, date)).ExecuteList<ProductListItemModel>();
        }

        public static List<ListItemModel> GetSkipChannelList()
        {
            return Db.SetSpCommand(SqlConstants.spGetSkipChannelList).ExecuteList<ListItemModel>();
        }

        public static int UpdateRating(int coutryId, DateTime date, RatingsModel rating)
        {
            int ratingId = -1;

            if (rating.KpiTypeId == (int)RatingsSetType.SkipChannelAndProductSet)
            {
                int lCapsChannelId = int.Parse(rating.Values.Split(',')[0]);
                int lProductGroupId = int.Parse(rating.Values.Split(',')[1]);

                ratingId = Db.SetSpCommand(SqlConstants.spSaveRatingCaps,
                    Db.Parameter(SqlConstants.spSaveRatingCaps_RKPI_ID, rating.Id),
                    Db.Parameter(SqlConstants.spSaveRatingCaps_Date, date),
                    Db.Parameter(SqlConstants.spSaveRatingCaps_CountryId, coutryId),
                    Db.Parameter(SqlConstants.spSaveRatingCaps_M3_ID, rating.M3Id),
                    Db.Parameter(SqlConstants.spSaveRatingCaps_Channel_ID, rating.ChannelId),
                    Db.Parameter(SqlConstants.spSaveRatingCaps_Weight, rating.Weight),
                    Db.Parameter(SqlConstants.spSaveRatingCaps_CapsChannel_ID, lCapsChannelId),
                    Db.Parameter(SqlConstants.spSaveRatingCaps_ProductGroup_ID, lProductGroupId)
                    ).ExecuteScalar<int>();
            }
            else
            {
                ratingId = Db.SetSpCommand(SqlConstants.spSaveRatingColumn,
                    Db.Parameter(SqlConstants.spSaveRatingColumn_RKPI_ID, rating.Id),
                    Db.Parameter(SqlConstants.spSaveRatingColumn_Date, date),
                    Db.Parameter(SqlConstants.spSaveRatingColumn_CountryId, coutryId),
                    Db.Parameter(SqlConstants.spSaveRatingColumn_M3_ID, rating.M3Id),
                    Db.Parameter(SqlConstants.spSaveRatingColumn_Channel_ID, rating.ChannelId),
                    Db.Parameter(SqlConstants.spSaveRatingColumn_Weight, rating.Weight),
                    Db.Parameter(SqlConstants.spSaveRatingColumn_Column_ID, rating.ColumnId)
                    ).ExecuteScalar<int>();
            }

            return ratingId;
        }

        public static void DeleteRating(int ratingId)
        {
            Db.SetSpCommand(SqlConstants.spDeleteRating,
                Db.Parameter(SqlConstants.spDeleteRating_KPI_ID, ratingId)
                ).ExecuteNonQuery();
        }

        #endregion

        #region General Plans Procedures

        public static List<BindedColumnModel> GetGeneralColumnList(DateTime date, int countryId, int m3Id)
        {
            List<BindedColumnModel> generalColumns = Db.SetSpCommand(SqlConstants.spGetGeneraColumnList,
                Db.Parameter(SqlConstants.spGetGeneraColumnList_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetGeneraColumnList_M3_ID, m3Id),
                Db.Parameter(SqlConstants.spGetGeneraColumnList_Date, date)
                ).ExecuteList<BindedColumnModel>();

            foreach (var column in generalColumns)
            {
                //all captions of UI columns are equal with caption from the database. No need for mapping 

                /* foreach (NameMappingModel mapping in DataRepository.GeneralColumns)
                 {
                     column.Name = column.Name.Replace(mapping.NameDB, mapping.NameUI);
                 }*/

                column.BandName = "bandPlans";
            }

            return generalColumns;
        }

        public static DataTable GetGeneralPlans(DateTime date, int countryId, int m3Id)
        {
            //return new DataTable();

            DataTable dt = Db.SetSpCommand(SqlConstants.spGetGeneralPlans,
                Db.Parameter(SqlConstants.spGetGeneralPlans_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetGeneralPlans_M3_ID, m3Id),
                Db.Parameter(SqlConstants.spGetGeneralPlans_Date, date)
                ).ExecuteDataTable();

            return dt;
        }

        #endregion

        #region Networks Plans Procedures

        public static List<BindedColumnModel> GetNetworkColumnList(DateTime date, int countryId, int m3Id)
        {
            List<BindedColumnModel> networksColumns = Db.SetSpCommand(SqlConstants.spGetNetworkColumnList,
                Db.Parameter(SqlConstants.spGetNetworkColumnList_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetNetworkColumnList_M3_ID, m3Id),
                Db.Parameter(SqlConstants.spGetNetworkColumnList_Date, date)
                ).ExecuteList<BindedColumnModel>();

            foreach (var column in networksColumns)
            {
                //all captions of UI columns are equal with caption from the database. No need for mapping 

                // 
                /*foreach (NameMappingModel mapping in DataRepository.NetworkColumns)
                {
                    column.Name = column.Name.Replace(mapping.NameDB, mapping.NameUI);
                }*/

                column.BandName = column.Name.Substring(0, column.Name.IndexOf('-') - 1);

                column.Name = column.Name.Contains("-")
                                  ? column.Name.Substring(column.Name.IndexOf('-') + 2)
                                  : column.Name;
            }

            return networksColumns;
        }

        public static DataTable GetNetworksPlans(DateTime date, int countryId, int m3Id)
        {
            DataTable dt = Db.SetSpCommand(SqlConstants.spGetNetworkPlans,
                Db.Parameter(SqlConstants.spGetNetworkPlans_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetNetworkPlans_M3_ID, m3Id),
                Db.Parameter(SqlConstants.spGetNetworkPlans_Date, date)
                ).ExecuteDataTable();

            return dt;
        }

        #endregion

        #region Products Plans Procedures

        public static List<BindedColumnModel> GetProductsColumnList(DateTime date, int countryId, int m3Id)
        {
            List<BindedColumnModel> productColumns = Db.SetSpCommand(SqlConstants.spGetProductColumnList,
                Db.Parameter(SqlConstants.spGetProductColumnList_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetProductColumnList_M3_ID, m3Id),
                Db.Parameter(SqlConstants.spGetProductColumnList_Date, date)
                ).ExecuteList<BindedColumnModel>();

            foreach (var column in productColumns)
            {
                // holder for column name
                string dbColName = column.Name;

                foreach (NameMappingModel mapping in DataRepository.ProductColumns)
                {
                    // here it's some kind of special column renaming. 
                    column.Name = column.Name.Replace(mapping.NameDB, mapping.NameUI); 
                }
                // to determine appropriate column band 
                column.BandName = column.Name.Contains("-")
                                      ? column.Name.Substring(0, column.Name.IndexOf('-') - 1)
                                      : (column.Name.Contains("Total") ? "Total" : "Дистр пуш");

                //the block below is commented because is unnecessary
                /*column.Name = column.Name.Contains("-")
                                  ? column.Name.Substring(column.Name.IndexOf('-') + 2) // 
                                  : column.Name;
                                  */
                // return original column name, that was fetched from the database
                column.Name = dbColName; 
            }

            return productColumns;
        }

        public static DataTable GetProductPlans(DateTime date, int countryId, int m3Id)
        {
            DataTable dt = Db.SetSpCommand(SqlConstants.spGetProductPlans,
                Db.Parameter(SqlConstants.spGetProductPlans_CountryId, countryId),
                Db.Parameter(SqlConstants.spGetProductPlans_M3_ID, m3Id),
                Db.Parameter(SqlConstants.spGetProductPlans_Date, date)
                ).ExecuteDataTable();

            return dt;
        }

        #endregion

        #region Plans Editing

        public static void UpdatePlan(PlanItemModel item)
        {
            Db.SetSpCommand(SqlConstants.spEditPlan,
                Db.Parameter(SqlConstants.spEditPlan_StaffID, item.EmployeeId),
                Db.Parameter(SqlConstants.spEditPlan_Column_ID, item.ColumnId),
                Db.Parameter(SqlConstants.spEditPlan_Value, item.Value)
                ).ExecuteNonQuery();
        }

        #endregion

        #region Binding to Caps Subtargets

        public static List<CapsSubtargetModel> GetCapsSubtargets(int countryId, DateTime date)
        {
            List<CapsSubtargetModel> subTargetsList = Db.SetSpCommand(SqlConstants.spGetCapsSubtargets,
                Db.Parameter(SqlConstants.spGetCapsSubtargets_Country_ID, countryId),
                Db.Parameter(SqlConstants.spGetCapsSubtargets_Date, date)
                ).ExecuteList<CapsSubtargetModel>();

            return subTargetsList;
        }

        public static void ForceSubtargetsAutoMapping(int countryId, int m3Id, DateTime date)
        {
            Db.SetSpCommand(SqlConstants.spAutoMapCapsSubtargets,
                Db.Parameter(SqlConstants.spAutoMapCapsSubtargets_Country_ID, countryId),
                Db.Parameter(SqlConstants.spAutoMapCapsSubtargets_M3_ID, m3Id),
                Db.Parameter(SqlConstants.spAutoMapCapsSubtargets_Date, date)
                ).ExecuteNonQuery();
        }

        public static void BindColumnToSubtargets(int columnId, long subtargetId)
        {
            Db.SetSpCommand(SqlConstants.spBindColumnToSubtargets,
                Db.Parameter(SqlConstants.spBindColumnToSubtargets_Column_ID, columnId),
                Db.Parameter(SqlConstants.spBindColumnToSubtargets_Subtarget_id, subtargetId)
                ).ExecuteNonQuery();
        }

        public static void UnbindColumnToSubtargets(int columnId, long subtargetId)
        {
            Db.SetSpCommand(SqlConstants.spUnbindColumnToSubtargets,
                Db.Parameter(SqlConstants.spUnbindColumnToSubtargets_Column_ID, columnId),
                Db.Parameter(SqlConstants.spUnbindColumnToSubtargets_Subtarget_id, subtargetId)
                ).ExecuteNonQuery();
        }

        #endregion

        #region Import from Excel

        public static void SendProductPlansToDB(Dictionary<int, ImportedPlanModel> _validPlans)
        {
            string lCurrentKpiName;
            string script = INSERT_PLAN_TEMPLATE;
            int i = 0;

            foreach (int planId in _validPlans.Keys)
            {
                lCurrentKpiName = _validPlans[planId].KpiName.Trim();

                //all captions of UI columns are equal with caption from the database. No need for mapping 

                /*if (lCurrentKpiName.StartsWith("Total"))
                {
                    foreach (NameMappingModel mapping in DataRepository.ProductTotalColumns)
                    {
                        lCurrentKpiName = lCurrentKpiName.Replace(mapping.NameUI, mapping.NameDB);
                    }
                }
                else
                {
                    foreach (NameMappingModel mapping in DataRepository.ProductColumns)
                    {
                        lCurrentKpiName = lCurrentKpiName.Replace(mapping.NameUI, mapping.NameDB + " -");
                    }
                }*/ 

                ImportedPlanModel plan = _validPlans[planId];

                string dateStr = GetDateDescr(plan.Date);

                script += string.Format(
                        @"({0}, {1}, '{2}', '{3}', '{4}', {5}),",
                        planId, plan.EmployeeId, plan.EmployeeName, lCurrentKpiName, dateStr,
                        plan.Plan.ToString(CultureInfo.InvariantCulture));
                i++;

                if (i == 500)
                {
                    script = RemoveLastComma(script);
                    Db.SetCommand(CommandType.Text, script).ExecuteNonQuery();
                    
                    i = 0;
                    script = INSERT_PLAN_TEMPLATE;
                }
            }

            if (i != 0)
            {
                script = RemoveLastComma(script);
                Db.SetCommand(CommandType.Text, script).ExecuteNonQuery();
            }
        }

        public static void SendGeneralPlansToDB(Dictionary<int, ImportedPlanModel> _validPlans)
        {
            string lCurrentKpiName;
            string script = INSERT_PLAN_TEMPLATE;
            int i = 0;

            foreach (int planId in _validPlans.Keys)
            {
                lCurrentKpiName = _validPlans[planId].KpiName.Trim();

                //all captions of UI columns are equal with caption from the database. No need for mapping 

                /*foreach (NameMappingModel mapping in DataRepository.GeneralColumns)
                {
                    lCurrentKpiName = lCurrentKpiName.Replace(mapping.NameUI, mapping.NameDB);
                }
                */
                ImportedPlanModel plan = _validPlans[planId];

                string dateStr = GetDateDescr(plan.Date);

                script += string.Format(
                        @"({0}, {1}, '{2}', '{3}', '{4}', {5}),",
                        planId, plan.EmployeeId, plan.EmployeeName, lCurrentKpiName, dateStr,
                        plan.Plan.ToString(CultureInfo.InvariantCulture));
                i++;

                if (i == 500)
                {
                    script = RemoveLastComma(script);
                    Db.SetCommand(CommandType.Text, script).ExecuteNonQuery();
                    i = 0;
                    script = INSERT_PLAN_TEMPLATE;
                }
            }

            if (i != 0)
            {
                script = RemoveLastComma(script);
                Db.SetCommand(CommandType.Text, script).ExecuteNonQuery();
            }
        }

        public static void SendNetworksPlansToDB(Dictionary<int, ImportedPlanModel> _validPlans)
        {
            string lCurrentKpiName;
            string script = INSERT_PLAN_TEMPLATE;
            int i = 0;

            foreach (int planId in _validPlans.Keys)
            {
                lCurrentKpiName = _validPlans[planId].KpiName.Trim();

                //all captions of UI columns are equal with caption from the database. No need for mapping 
                /*
                foreach (NameMappingModel mapping in DataRepository.NetworkColumns)
                {
                    lCurrentKpiName = lCurrentKpiName.Replace(mapping.NameUI, mapping.NameDB + " -");
                }
                */

                ImportedPlanModel plan = _validPlans[planId];

                string dateStr = GetDateDescr(plan.Date);

                script += string.Format(
                        @"({0}, {1}, '{2}', '{3}', '{4}', {5}),",
                        planId, plan.EmployeeId, plan.EmployeeName, lCurrentKpiName, dateStr,
                        plan.Plan.ToString(CultureInfo.InvariantCulture));
                i++;

                if (i == 500)
                {
                    script = RemoveLastComma(script);
                    Db.SetCommand(CommandType.Text, script).ExecuteNonQuery();
                    i = 0;
                    script = INSERT_PLAN_TEMPLATE;
                }
            }

            if (i != 0)
            {
                script = RemoveLastComma(script);
                Db.SetCommand(CommandType.Text, script).ExecuteNonQuery();
            }
        }

        public static List<InvalidPlanModel> GetIncorrectPlans()
        {
            List<InvalidPlanModel> plans = Db.SetSpCommand(SqlConstants.spGetInvalidPlans).ExecuteList<InvalidPlanModel>();

            return plans;
        }

        public static void SaveLoadedPlans()
        {
            Db.SetSpCommand(SqlConstants.spSavePlans).ExecuteNonQuery();
        }

        private static string RemoveLastComma(string script)
        {
            return script.Remove(script.Length - 1, 1);
        }

        private static string GetDateDescr(DateTime date)
        {
            return date.Year.ToString("D4") + date.Month.ToString("D2") + "01";
        }

        public static void InitTempTables()
        {
            string tableForValidation = @"
                                          if object_id('tempdb..#DW_MonthlyPlanLoadTable') is not null
                                          DROP TABLE #DW_MonthlyPlanLoadTable
    	
                                          CREATE TABLE #DW_MonthlyPlanLoadTable(
                                            ID int null,
                                            StaffID int null,     
                                            StaffName varchar(600) null,
                                            Column_Name varchar(255) null,
                                            Column_Date date null,
                                            Column_PlanValue decimal(15,3) null,    
                                            ErrorCode tinyint null,
                                            ColumnID int null 
                                          )";
   
            Db.SetCommand(tableForValidation).ExecuteNonQuery();
        }

        #endregion
    }
}