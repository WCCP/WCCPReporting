﻿using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    public class ProductsPlanController : BasePlanController, IProductsPlanController
    {
        #region Fields
        
        private IProductsPlanView _productsPlanView;

        #endregion

        public ProductsPlanController(IProductsPlanView view)
            : base(view)
        {
            _productsPlanView = (IProductsPlanView)GetView();
        }

        protected override IView CreateView()
        {
            return _productsPlanView ?? new ProductsPlanView();
        }

        public override void LoadData(bool reload = false)
        {
            _productsPlanView.ConstructView(DataProvider.GetProductsColumnList(_filterController.Date, _filterController.CurrentCountry.Id, _filterController.CurrentM3.Id));
            _productsPlanView.AssignDataSource(DataProvider.GetProductPlans(_filterController.Date, _filterController.CurrentCountry.Id, _filterController.CurrentM3.Id));
            base.LoadData(reload);
        }

    }
}