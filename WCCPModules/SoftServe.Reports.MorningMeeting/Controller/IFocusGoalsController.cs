﻿using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    interface IFocusGoalsController : IViewController
    {
        void AddNewManualGoal(int m3);
        void AddNewAutoGoal(int m3);

        void UpdateGoal(int goalId, bool isManual);
        void LoadGoal(int goalId, bool isManual);
        void DeleteGoal(int goalId);

        bool IsCountryLevel { get; }
        bool HasUnsavedChanges { get; set; }

        StaffTreeModel CurrentM3 { get; }
    }
}
