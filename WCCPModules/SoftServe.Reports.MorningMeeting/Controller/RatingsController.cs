﻿using System.Collections.Generic;
using System.Linq;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    internal class RatingsController : BaseViewController, IRatingsController
    {
        #region Fields

        private readonly IFilterController _filterController;
        private readonly IFilterView _filterView;

        private IRatingsView _ratingsView;

        private List<DeletedRatingModel> _deletedRatings = new List<DeletedRatingModel>();
        private List<RatingsModel> _ratings;

        private List<RatingsColumnsModel> _ratingsColumns = new List<RatingsColumnsModel>(); 

        private int _countryLevel = -1;

        private int _index = -1;

        private const int MaxRatingQuantity = 4;

        private bool _m3HasRatingsCopiedFromCountryOnly;// ratings are inherited from country, but not saved directly

        #endregion

        public StaffTreeModel CurrentM3
        {
            get { return _filterController.CurrentM3; }
        }

        public RatingsController(IRatingsView view)
            : base(view)
        {
            _ratingsView = view;

            _ratingsView = (IRatingsView)GetView();
            _filterController = new FilterController(_filterView);
            _filterController.Start();
            _filterView = (IFilterView)_filterController.GetView();
            _filterView.ShowListType = false;
            _filterView.StartTo(_ratingsView);

            _filterView.DateChanging += OnDateChanging;
            _filterView.DateChanged += OnDateChanged;
            _filterView.CountryChanging += OnCountryChanging;
            _filterView.CountryChanged += OnCountryChanged;
            _filterView.M3Changing += OnM3Changing;
            _filterView.M3Changed += OnM3Changed;

            HasUnsavedChanges = false;
            HasUnsavedChangesForCurrentM3 = false;

            _m3HasRatingsCopiedFromCountryOnly = false;
        }

        #region IRatingsController members

        public bool IsCountryLevel
        {
            get { return _filterController.CurrentM3.Id == _countryLevel; }
        }

        public bool HasUnsavedChanges { get; set; }

        public bool HasUnsavedChangesForCurrentM3 { get; set; }

        public int AddRating(int m3, int channenId)
        {
            RatingsModel item = RatingsModel.CreateInstance();
            item.Id = _index;
            item.M3Id = m3;
            item.ChannelId = channenId;

            _ratings.Add(item);

            HasUnsavedChanges = true;
            HasUnsavedChangesForCurrentM3 = true;
            _index--;

            return item.Id;
        }

        public void DeleteRating(int id)
        {
            if (_ratings != null)
            {
                RatingsModel lRating = _ratings.FirstOrDefault(r => r.Id == id);

                if (lRating != null)
                {
                    _ratings.Remove(lRating);

                    if (lRating.Id >= 0)
                    {
                        _deletedRatings.Add(new DeletedRatingModel { Id = lRating.Id, M3Id = lRating.M3Id, CnannelId = lRating.ChannelId });
                    }
                }

                HasUnsavedChanges = true;
                HasUnsavedChangesForCurrentM3 = true;
            }
        }

        #endregion

        protected override IView CreateView()
        {
            return _ratingsView ?? new RatingsView();
        }

        public override void LoadData(bool reload = false)
        {
            InitDataSources();

            _ratingsView.OnM3FilterChange(_filterController.CurrentM3.Id);

            HasUnsavedChanges = false;
            HasUnsavedChangesForCurrentM3 = false;
        }

        public override void SaveData()
        {
            List<int> cantBeSaved;
            string message;
            bool canChangesBeSaved = CheckWhetherCurrentChangesCanBeSaved(out cantBeSaved, out message);

            if (_ratings != null)
            {
                foreach (RatingsModel rating in _ratings)
                {
                    if (!cantBeSaved.Contains(rating.M3Id) && rating.IsDirty)
                    {
                        int lUpdatedRatingId = DataProvider.UpdateRating(_filterController.CurrentCountry.Id, _filterController.Date, rating);
                        rating.Id = lUpdatedRatingId;
                    }
                }
            }

            if (_deletedRatings != null)
            {
                foreach (DeletedRatingModel rating in _deletedRatings)
                {
                    if (!cantBeSaved.Contains(rating.M3Id))
                    {
                        DataProvider.DeleteRating(rating.Id);
                    }
                }

                _deletedRatings = new List<DeletedRatingModel>();
            }

            HasUnsavedChanges = false;
            HasUnsavedChangesForCurrentM3 = false;
            _m3HasRatingsCopiedFromCountryOnly = false;

            if (!canChangesBeSaved)
            {
                _ratingsView.ShowMessage(message, "Сохранение рейтингов");
                LoadData();
            }
        }


        private void OnM3Changed(object sender, ListItemChangeEventArgs listItemChangeEventArgs)
        {
            // old m3 hasn't changes in comparison to country level 
            if (listItemChangeEventArgs.OldId != _countryLevel && !HasUnsavedChangesForCurrentM3 && _m3HasRatingsCopiedFromCountryOnly)
            {
                DeleteTemporaryRatingsFromCountry(listItemChangeEventArgs.OldId);
            }

            if (_ratings.FirstOrDefault(r => r.M3Id == listItemChangeEventArgs.NewId) == null)
            {
                _m3HasRatingsCopiedFromCountryOnly = true;
                CopyRatingsFromCountry(listItemChangeEventArgs.NewId);
            }
            else
            {
                _m3HasRatingsCopiedFromCountryOnly = false;
            }

            _ratingsView.OnM3FilterChange(listItemChangeEventArgs.NewId);

            HasUnsavedChangesForCurrentM3 = false;
            _ratingsView.AssignNetworks();
        }

        private void OnM3Changing(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            listItemChangeEventArgs.IsCanceled = !_ratingsView.IsValid;
        }
        
        private void OnCountryChanged(object sender, ListItemChangeEventArgs listItemChangeEventArgs)
        {
            LoadData();
        }

        private void OnCountryChanging(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _ratingsView.SaveChangesAndProceed();
            }
        }

        private void OnDateChanged(object sender, DateChangeEventArgs dateChangeEventArgs)
        {
            LoadData();
        }

        private void OnDateChanging(object sender, DateChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _ratingsView.SaveChangesAndProceed();
            }
        }


        private void CopyRatingsFromCountry(int m3Id)
        {
            List<RatingsModel> lCountryLevelRatings = null;

            if (_ratings != null)
            {
                lCountryLevelRatings = _ratings.Where(s => s.M3Id == _countryLevel).Select(s => s).ToList();

                foreach (RatingsModel rating in lCountryLevelRatings)// for networks we have additional conditions
                {
                    RatingsColumnsModel lCurColumn = _ratingsColumns.FirstOrDefault(s => s.ColumnId == rating.ColumnId);

                    if (lCurColumn != null && lCurColumn.ValueType == RatingsSetType.Network)
                    {
                        //we have columnnId of the column for country level
                        //lets get the appropriate column for cur M3:
                        RatingsColumnsModel lColumn =
                            _ratingsColumns.FirstOrDefault(
                                s =>
                                s.M3Id == CurrentM3.Id && s.ValueType == RatingsSetType.Network &&
                                s.KpiId == rating.KpiId && s.Name == lCurColumn.Name);

                        if (lColumn != null)
                        {
                            AddRating(m3Id, rating.ChannelId, rating.KpiTypeId, rating.KpiId, lColumn.ColumnId.ToString(), rating.Weight,
                                  lColumn.ColumnId);
                        }
                    }
                    else
                    {
                        AddRating(m3Id, rating.ChannelId, rating.KpiTypeId, rating.KpiId, rating.Values, rating.Weight,
                                  rating.ColumnId);
                    }
                }
            }
        }

        private void DeleteTemporaryRatingsFromCountry(int m3Id)
        {
            List<RatingsModel> lm3Ratings = null;

            if (_ratings != null)
            {
                lm3Ratings = _ratings.Where(s => s.M3Id == m3Id).Select(s => s).ToList();

                foreach (RatingsModel rating in lm3Ratings)
                {
                    DeleteRating(rating.Id);
                }
            }
        }

        private bool CheckWhetherCurrentChangesCanBeSaved(out List<int> cantBeSaved, out string message)
        {
            cantBeSaved = new List<int>();

            bool canSave = true;
            message = string.Empty;

            List<RatingsModel> lRatingsFromDB = DataProvider.GetRatingsList(_filterController.Date, _filterController.CurrentCountry.Id);

            foreach (int m3Id in lRatingsFromDB.Select(r => r.M3Id).Distinct())
            {
                if (!CheckWhetherRatingsQuantityIsOk(m3Id, lRatingsFromDB))
                {
                    message += string.Format("Нельзя сохранить изменения для {0}: количество рейтингов изменено другим пользователем; \n", GetM3Name(m3Id));
                    cantBeSaved.Add(m3Id);

                    canSave = false;
                }
            }

            return canSave;
        }

        private bool CheckWhetherRatingsQuantityIsOk(int m3Id, List<RatingsModel> lRatingsFromDB)
        {
            bool canEdit = true;

            int quantityAdded = 0, quantityDB = 0, quantityDeleted = 0;

            foreach (ChannelModel channel in DataProvider.GetChannelList())
            {
                quantityAdded = _ratings.Count(r => r.M3Id == m3Id && r.Id < 0 && r.ChannelId == channel.Id);
                quantityDeleted = _deletedRatings.Count(r => r.M3Id == m3Id && r.CnannelId == channel.Id);
                quantityDB = lRatingsFromDB.Count(r => r.M3Id == m3Id && r.ChannelId == channel.Id);

                canEdit = canEdit && (quantityAdded + quantityDB - quantityDeleted <= MaxRatingQuantity);
            }

            return canEdit;
        }

        private void InitDataSources()
        {
            _ratings = DataProvider.GetRatingsList(_filterController.Date, _filterController.CurrentCountry.Id);

            _ratingsView.AssignChannels(DataProvider.GetChannelList());

            _ratingsColumns = DataProvider.GetRatingsColumnsList(_filterController.Date, _filterController.CurrentCountry.Id);
            _ratingsView.AssignColumnsList(_ratingsColumns);

            _ratingsView.AssignAverageSkuList();
            _ratingsView.AssignProductSets(DataProvider.GetProductSetList(_filterController.Date, _filterController.CurrentCountry.Id));
            _ratingsView.AssignSkipChannels(DataProvider.GetSkipChannelList());
            _ratingsView.AssignNetworks();

            _ratingsView.AssignKpiTypes(DataProvider.GetKpiTypesList());

            List<KpiModel> lKpis = DataProvider.GetKpiList();
            _ratingsView.AssignKPIs(lKpis);

            foreach (RatingsModel rating in _ratings)// get ratings value from column dependent kpis
            {
                KpiModel lKpi = lKpis.FirstOrDefault(r => r.Id == rating.KpiId);

                if (lKpi != null && lKpi.ValueType != RatingsSetType.Empty && lKpi.ValueType != RatingsSetType.SkipChannelAndProductSet)
                {
                    rating.Values = rating.ColumnId.ToString();
                }
            }

            _ratingsView.AssignRatings(_ratings);
        }

        public int AddRating(int m3, int channenId, int kpiTypeId, int kpiId, string values, int weight, int columnId)
        {
            RatingsModel item = RatingsModel.CreateInstance();
            item.Id = _index;
            item.M3Id = m3;
            item.ChannelId = channenId;
            item.KpiTypeId = kpiTypeId;
            item.KpiId = kpiId;
            item.Values = string.Copy(values ?? string.Empty);
            item.ColumnId = columnId;
            item.Weight = weight;

            _ratings.Add(item);

            _index--;

            return item.Id;
        }

        private string GetM3Name(int m3Id)
        {
            List<StaffTreeModel> staffing = DataRepository.GetStaffTreeList(_filterController.CurrentCountry.Id, true);

            return staffing.First(s => s.Id == m3Id).Name;
        }
    }
}
