﻿using System;
using System.Collections.Generic;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public interface IBaseSKUConfigController : IViewController {
        IFilterController FilterController { get; }
        IFilterView FilterView { get; }

        int CurrentIndex { get; set; }
        List<ProductTreeModel> Products { get; set; }

        bool IsAddingMode { get; set; }
        int SetIdCounter { get; set; }
        int SKUGroupIdCounter { get; set; }
        bool IsFilterCountryLevel { get; }

        List<int> GetCurrentIdList();
        string GetSelectedIdList();
        void SetSelectedIdList(string strIdList);
        bool IsCurrentSkuGroup();
        bool HasUnsavedChanges { get; set; }
        
        void AddSkuGroup();
        int DeleteSkuGroup();

        void SwapItems(int index1, int index2, bool isUp = false);

        void CopySku(DateTime dateTo, List<string> staffIds);
    }
}