﻿using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    public class GeneralPlanController : BasePlanController, IGeneralPlanController
    {
        #region Fields

        private IGeneralPlanView _generalPlanView;

        #endregion

        public GeneralPlanController(IGeneralPlanView view)
            : base(view)
        {
            _generalPlanView = (IGeneralPlanView)GetView();
        }

        protected override IView CreateView()
        {
            return _generalPlanView ?? new GeneralPlanView();
        }

        public override void LoadData(bool reload = false)
        {
            _generalPlanView.ConstructView(DataProvider.GetGeneralColumnList(_filterController.Date, _filterController.CurrentCountry.Id, _filterController.CurrentM3.Id));
            _generalPlanView.AssignDataSource(DataProvider.GetGeneralPlans(_filterController.Date, _filterController.CurrentCountry.Id, _filterController.CurrentM3.Id));
            base.LoadData(reload);
        }
    }
}