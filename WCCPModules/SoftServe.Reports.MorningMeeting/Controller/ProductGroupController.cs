﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.View;
using System.Text.RegularExpressions;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public class ProductGroupController : BaseSKUConfigController, IProductGroupController {
        #region Fields

        private IProductGroupView _productGroupView;
        private ProductGroupModel _currentProductGroup;
        private List<ProductGroupModel> _productGroups;
        private List<ProductGroupDetailModel> _productGroupDetails;

        #endregion

        public ProductGroupController(IProductGroupView view) : base(view) {
            _productGroupView = (IProductGroupView) GetView();
        }

        #region Implementation of IProductGroupController

        public ProductGroupModel CurrentProductGroup {
            get { return _currentProductGroup; }
            set { _currentProductGroup = value; }
        }

        public List<ProductGroupModel> ProductGroups {
            get { return _productGroups; }
        }

        #endregion

        #region Implementation of IViewController

        protected override IView CreateView() {
            return _productGroupView ?? new ProductGroupView();
        }

        public override void LoadData(bool reload = false) {
            Products = DataProvider.GetProductList(FilterController.CurrentCountry.Id, true);
            _productGroups = DataProvider.GetProductGroups(FilterController.Date, FilterController.CurrentCountry.Id);
            _productGroupDetails = DataProvider.GetProductGroupsDetail(FilterController.Date, FilterController.CurrentCountry.Id);
            for (int lI = 0; lI < _productGroups.Count; lI++) {
                ProductGroupModel lProductGroup = _productGroups[lI];
                List<ProductGroupDetailModel> lDetails =
                    _productGroupDetails.Where(item => item.SKUGroupId == lProductGroup.SKUGroupId).ToList();
                foreach (ProductGroupDetailModel lGroupDetail in lDetails) {
                    List<ProductTreeModel> lProducts =
                        Products.Where(item => item.ItemId == lGroupDetail.ItemId && item.Level == lGroupDetail.Level).ToList();
                    foreach (ProductTreeModel lProduct in lProducts) {
                        lProductGroup.AddIdToList(lProduct.Id);
                    }
                }
            }
            if (reload)
                _productGroupView.RefreshData();

            base.LoadData(reload);
        }

        public override void SaveData() {
            bool lListWasUpdatedBySmd = false;
            foreach (ProductGroupModel lProductGroup in _productGroups) {
                if (!lProductGroup.IsDirty)
                    continue;

                bool lCurItemWasUpdatedBySmd;
                int lResult = DataProvider.SetProductGroup(FilterController.Date, lProductGroup, out lCurItemWasUpdatedBySmd);
                //save detail
                if (lResult == 0 && lProductGroup.IsIdListModified) {
                    SqlXml lXmlData = GetSelectedSkuXml(lProductGroup.IdList);
                    lResult = DataProvider.SetProductGroupDetails(lProductGroup.SKUGroupId, lXmlData);
                }

                if (lResult == 0)
                    lProductGroup.AcceptChanges();

                lListWasUpdatedBySmd = lListWasUpdatedBySmd || lCurItemWasUpdatedBySmd;
            }
            
            if (lListWasUpdatedBySmd)
            {
                XtraMessageBox.Show("Порядок следования наборов был изменен другими пользователями", "Настройка продуктов",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadData(true);
            }

            base.SaveData();
        }

        #endregion

        public override List<int> GetCurrentIdList() {
            if (CurrentIndex < 0 || CurrentIndex >= _productGroups.Count)
                return new List<int>();
            return _productGroups[CurrentIndex].IdList;
        }

        protected override void SetCurrentIndex(int value) {
            base.SetCurrentIndex(value);
            if (IsCurrentSkuGroup())
                _currentProductGroup = _productGroups[CurrentIndex];
            else
                _currentProductGroup = null;
        }

        public override void AddSkuGroup() {
            base.AddSkuGroup();
            ProductGroupModel lProductGroup = ProductGroupModel.CreateInstance();
            lProductGroup.ListType = FilterController.CurrentListTypeItem.Id;
            lProductGroup.CountryId = FilterController.CurrentCountry.Id;
            lProductGroup.M3Id = FilterController.CurrentM3.Id;
            lProductGroup.SKUGroupId = SKUGroupIdCounter--;
            lProductGroup.SKUGroupName = "Новая цель";
            lProductGroup.SetId = SetIdCounter--;
            lProductGroup.ChannelMask = 0;
            lProductGroup.SortOrder = (_productGroups.Count == 0)
                                          ? 1.0
                                          : _productGroups.Max(g => g.SortOrder) + 1.0;
            _productGroups.Add(lProductGroup);
            CurrentIndex = _productGroups.Count - 1;
        }

        public override int DeleteSkuGroup() {
            if (!IsCurrentSkuGroup())
                return -100;
            ProductGroupModel lProductGroup = _productGroups[CurrentIndex];
            //TODO: security - check permissions
//            if () {
//                XtraMessageBox.Show("Вам запрещено удалять неактивные СКЮ.", "Предупреждение", MessageBoxButtons.OK,
//                    MessageBoxIcon.Warning);
//                return -100;
//            }
            int lErrorCode = 0;
            int lSKUGroupId = lProductGroup.SKUGroupId;
            if (lSKUGroupId > 0)
                lErrorCode = DataProvider.DeleteProductGroup(lProductGroup.SKUGroupId);

            if (lErrorCode == -1 || lErrorCode == -11 || lErrorCode == -12)
            {
                string lDescription = "подсчитанные факты или введены планы";
                if(lErrorCode == -11)
                    lDescription = "Рейтинги";
                if (lErrorCode == -12)
                    lDescription = "подсчитанные факты или введены планы и заполнены Рейтинги";

                DialogResult lDialogResult = XtraMessageBox.Show(
                    string.Format("Существуют {0} за текущий месяц по набору.\n Вы действительно хотите удалить СКЮ и его показатели в текущем месяце?", lDescription),
                    "Предупреждение", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation);

                if (lDialogResult == DialogResult.Yes)
                    lErrorCode = DataProvider.DeleteProductGroup(lProductGroup.SKUGroupId, true);
                else
                    return lErrorCode;
            }

            if (lErrorCode == -2 || lErrorCode == -22 || lErrorCode == -24)
            {
                XtraMessageBox.Show("Невозможно удалить СКЮ. Существуют подсчитанные факты или введены планы или заполнены Рейтинги за предыдущие месяцы.", "Предупреждение",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return lErrorCode;
            }
            return lErrorCode;
        }

        public override void SwapItems(int index1, int index2, bool isUp = false) {
            ProductGroupModel lProductGroup1 = _productGroups[index1];
            ProductGroupModel lProductGroup2 = _productGroups[index2];
            _productGroups[index2] = lProductGroup1;
            _productGroups[index1] = lProductGroup2;
            //move down
            if (!isUp) {
                if (index2 + 1 > _productGroups.Count - 1 || lProductGroup2.SortOrder > _productGroups[index2 + 1].SortOrder)
                    lProductGroup1.SortOrder = lProductGroup2.SortOrder + 1.0;
                else
                    lProductGroup1.SortOrder = (lProductGroup2.SortOrder + _productGroups[index2 + 1].SortOrder) / 2.0;
            }
            //move up
            if (isUp) {
                if (index1 - 1 < 0 || lProductGroup1.SortOrder < _productGroups[index1 - 1].SortOrder)// move to the first position OR on the vedge between country and M3 settings (if country SortOrder is greater)  
                    lProductGroup2.SortOrder = lProductGroup1.SortOrder - 1.0;
                else
                    lProductGroup2.SortOrder = (lProductGroup1.SortOrder + _productGroups[index1 - 1].SortOrder) / 2.0;
            }
        }

        public override string GetSelectedIdList() {
            if (CurrentIndex < 0 || CurrentIndex >= _productGroups.Count)
                return string.Empty;
            return _productGroups[CurrentIndex].SelectedIdList;
        }

        public override void SetSelectedIdList(string strIdList) {
            if (CurrentIndex < 0 || CurrentIndex >= _productGroups.Count)
                return;
            _currentProductGroup = _productGroups[CurrentIndex];
            _currentProductGroup.SelectedIdList = strIdList;
        }

        public override bool IsCurrentSkuGroup() {
            if (CurrentIndex < 0 || CurrentIndex >= _productGroups.Count)
                return false;
            return true;
        }

        private ProductGroupModel CopySkuGroup(ProductGroupModel productGroup, int m3Id)
        {
            ProductGroupModel lProductGroup = ProductGroupModel.CreateInstance();
            lProductGroup.ListType = FilterController.CurrentListTypeItem.Id;
            lProductGroup.CountryId = FilterController.CurrentCountry.Id;
            lProductGroup.M3Id = m3Id;
            lProductGroup.SKUGroupId = -1;
            lProductGroup.SKUGroupName = productGroup.SKUGroupName;
            lProductGroup.SetId = -2;
            lProductGroup.ChannelMask = productGroup.ChannelMask;
            lProductGroup.SortOrder = productGroup.SortOrder;
            lProductGroup.SelectedIdList = productGroup.SelectedIdList;

            return lProductGroup;
        }

        public override void CopySku(DateTime dateTo, List<string> staffIds)
        {
            List<ProductGroupModel> lProductGroupsForM3 =
                _productGroups.Where(g => g.M3Id == FilterController.CurrentM3.Id && g.ListType == FilterController.CurrentListTypeItem.Id).ToList();

            List<ProductGroupModel> lProductGroupsForSave = new List<ProductGroupModel>();

            foreach (string m3 in staffIds)
            {
                int m3Id = int.Parse(m3);

                foreach (ProductGroupModel productGroup in lProductGroupsForM3)
                {
                    lProductGroupsForSave.Add(CopySkuGroup(productGroup, m3Id));
                }
            }
            
            string lMessage = CheckConflictsBeforeCopy(dateTo, staffIds, lProductGroupsForSave);
            if (lMessage != string.Empty)
            {
                DialogResult lRes = XtraMessageBox.Show(lMessage, "Копировать наборы", MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Exclamation);

                if(lRes == DialogResult.No)
                    return;
            }

            bool lListWasUpdatedBySmd = false;
            foreach (ProductGroupModel productGroup in lProductGroupsForSave)
            {
                //save group
                int lResult = DataProvider.SetProductGroup(dateTo, productGroup, out lListWasUpdatedBySmd);
                //save details
                if (lResult == 0)
                {
                    SqlXml lXmlData = GetSelectedSkuXml(productGroup.IdList);
                    DataProvider.SetProductGroupDetails(productGroup.SKUGroupId, lXmlData);
                }
            }
        }

        private string CheckConflictsBeforeCopy(DateTime dateTo, List<string> staffIds, List<ProductGroupModel> productGroups)
        {
            string message = "Для некоторых М3 наборы с таким названием уже существуют: \r\n";
            int lIssuesCount = 0;

            foreach (string staffId in staffIds)
            {
                int m3Id = int.Parse(staffId);
                string messageForM3 = "\t- для " +
                                      FilterController.StaffTreeList.First(s => s.Id == m3Id).Name +
                                      ": \r\n";

                List<ProductGroupModel> lProductGroupsForM3 = productGroups.Where(g => g.M3Id == m3Id).ToList();

                int lM3IssuesCount = 0;

                foreach (ProductGroupModel productGroup in lProductGroupsForM3)
                {
                    string name = GetSupposedName(DataProvider.GetProductGroupsDoubles(dateTo, FilterController.CurrentCountry.Id,
                                                                             m3Id, productGroup.SKUGroupId,
                                                                             productGroup.SKUGroupName), productGroup.SKUGroupName);

                    if (productGroup.SKUGroupName != name)
                    {
                        messageForM3 += "\t\t" + productGroup.SKUGroupName + "\r\n";
                        productGroup.SKUGroupName = name;

                        lM3IssuesCount++;
                    }
                }
               
                if (lM3IssuesCount > 0)
                {
                    message += messageForM3;
                    lIssuesCount++;
                }
            }

            message += "\r\n Копировать все равно?";

            return lIssuesCount > 0 ? message : string.Empty;
        }

        private string GetSupposedName(List<string> list, string curName)
        {
            string result = curName;
            string DublicationTemplate = " - копия ({0})";

            int lIndex = 0;

            if (list.Contains(result))
            {
                lIndex = 1;
            }
            else
            {
                return result;
            }
           
            foreach (string name in list)
            {
                if (Regex.IsMatch(name, string.Format(DublicationTemplate, "\\([0-9]+\\)")))
                {
                    Match lMatch = Regex.Match(name, string.Format(DublicationTemplate, "\\([0-9]+\\)"), RegexOptions.RightToLeft);

                    if(!name.EndsWith(lMatch.Value))
                        continue;

                    int lNameIndex = int.Parse(Regex.Match(lMatch.Value, "[0-9]+").Value);

                    if (lNameIndex >= lIndex)
                    {
                        lIndex = lNameIndex + 1;
                    }
                }
            }

            if (lIndex > 0)
            {
                result += string.Format(DublicationTemplate, lIndex);
            }

            return result;
        }

        private SqlXml GetSelectedSkuXml(List<int> selectedIdList) {
            List<ProductTreeModel> lSelectedProducts = new List<ProductTreeModel>();
            if (selectedIdList.Count > 0)
                lSelectedProducts.AddRange(Products.Where(product => selectedIdList.Contains(product.Id)));

            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings lSettings = new XmlWriterSettings();
            lSettings.OmitXmlDeclaration = true;

            XmlWriter lWriter = XmlWriter.Create(lStream, lSettings);
            lWriter.WriteStartElement("root");
            foreach (ProductTreeModel lProduct in lSelectedProducts) {
                lWriter.WriteStartElement("row");
                AddFullEndXmlElement(lWriter, SqlConstants.ItemIdFieldName.ToLower(), lProduct.ItemId);
                AddFullEndXmlElement(lWriter, SqlConstants.LevelFieldName.ToLower(), lProduct.Level);
                lWriter.WriteEndElement();
            }
            lWriter.WriteFullEndElement();
            lWriter.Flush();
            lStream.Position = 0;

            return new SqlXml(lStream);
        }

        private static void AddFullEndXmlElement(XmlWriter writer, string nodeName, object nodeValue) {
            writer.WriteStartElement(nodeName);
            if (nodeValue != null && nodeValue != DBNull.Value) {
                int lValue = Convert.ToInt32(nodeValue);
                writer.WriteString(lValue.ToString(CultureInfo.InvariantCulture));
            }
            writer.WriteEndElement();
        }
    }
}