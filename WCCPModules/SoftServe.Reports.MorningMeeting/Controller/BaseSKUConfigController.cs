﻿using System;
using System.Collections.Generic;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public class BaseSKUConfigController : BaseViewController, IBaseSKUConfigController {
        #region Fields

        private IFilterController _filterController;
        private IFilterView _filterView;
        private IBaseSKUConfigView _baseSKUConfigView;

        private List<ProductTreeModel> _products;
        private int _currentIndex = -1;

        #endregion

        public BaseSKUConfigController(IBaseSKUConfigView view) : base(view) {
            SetIdCounter = -1;
            SKUGroupIdCounter = -1;
            _filterController = new FilterController(_filterView);
            _filterController.Start();
            _filterView = (IFilterView) _filterController.GetView();
            _filterView.ShowListType = true;
            _baseSKUConfigView = (IBaseSKUConfigView) GetView();
            _filterView.StartTo(_baseSKUConfigView);

            _currentIndex = -1;
            IsAddingMode = false;
            HasUnsavedChanges = false;

            //FilterView events
            FilterView.DateChanged += OnDateChanged;
            FilterView.DateChanging += OnDateChanging;
            FilterView.SKUListTypeItemChanged += OnListTypeChanged;
            FilterView.SKUListTypeItemChanging += OnListTypeChanging;
            FilterView.CountryChanged += OnCountryChanged;
            FilterView.CountryChanging += OnCountryChanging;
            FilterView.M3Changed += OnM3Changed;
            FilterView.M3Changing += OnM3Changing;
        }

        private void OnCountryChanged(object sender, ListItemChangeEventArgs e) {
            _baseSKUConfigView.OnCountryChanged(e.NewId);
        }

        private void OnCountryChanging(object sender, ListItemChangingEventArgs e) {
            _baseSKUConfigView.OnCountryChanging(e);
        }

        private void OnDateChanged(object sender, DateChangeEventArgs e) {
            _baseSKUConfigView.OnDateChanged(e.NewDate);
        }

        private void OnDateChanging(object sender, DateChangingEventArgs e) {
            _baseSKUConfigView.OnDateChanging(e);
        }

        private void OnM3Changed(object sender, ListItemChangeEventArgs e) {
            _baseSKUConfigView.OnM3Changed(e.NewId);
        }

        private void OnM3Changing(object sender, ListItemChangingEventArgs e) {
            e.IsCanceled = !_baseSKUConfigView.IsValid;
        }

        private void OnListTypeChanged(object sender, ListItemChangeEventArgs e) {
            _baseSKUConfigView.OnListTypeChanged(e.NewId);
        }

        private void OnListTypeChanging(object sender, ListItemChangingEventArgs e) {
            e.IsCanceled = !_baseSKUConfigView.IsValid;
        }

        #region Properties

        public IFilterController FilterController {
            get { return _filterController; }
        }

        public IFilterView FilterView {
            get { return _filterView; }
        }

        public int CurrentIndex {
            get { return _currentIndex; }
            set { SetCurrentIndex(value); }
        }

        protected virtual void SetCurrentIndex(int value) {
            _currentIndex = value;
        }

        public List<ProductTreeModel> Products {
            get { return _products; }
            set { _products = value; }
        }

        public virtual List<int> GetCurrentIdList() {
            return new List<int>();
        }

        public bool IsAddingMode { get; set; }

        public int SetIdCounter { get; set; }

        public int SKUGroupIdCounter { get; set; }

        public virtual bool IsCurrentSkuGroup() {
            return false;
        }

        public bool IsFilterCountryLevel {
            get { return _filterController.CurrentM3.Id == -1; }
        }

        public virtual void AddSkuGroup() {
        }

        public virtual int DeleteSkuGroup() {
            return 0;
        }

        public virtual void SwapItems(int index1, int index2, bool isUp = false) {
        }

        public virtual string GetSelectedIdList() {
            return string.Empty;
        }

        public virtual void SetSelectedIdList(string strIdList) {
        }

        public bool HasUnsavedChanges { get; set; }

        public override void LoadData(bool reload = false) {
            HasUnsavedChanges = false;

            base.LoadData(reload);
        }

        public override void SaveData() {
            HasUnsavedChanges = false;

            base.SaveData();
        }

        public virtual void CopySku(DateTime dateTo, List<string> staffIds){
        }

        #endregion

        protected override IView CreateView() {
            return _baseSKUConfigView ?? new BaseSKUConfigView();
        }
    }
}