﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public class SKUListController : BaseSKUConfigController, ISKUListController {
        #region Fields

        private ISKUListView _skuListView;
        private SKUListItemModel _currentSKUListItem;
        private List<SKUListItemModel> _skuListItems;

        #endregion

        public SKUListController(ISKUListView view) : base(view) {
            _skuListView = (ISKUListView) GetView();
            FilterView.ListType = ListTypeEnum.SKUList;
        }

        #region Implementation of ISKUListController

        public SKUListItemModel CurrentSKUListItem {
            get { return _currentSKUListItem; }
        }

        public List<SKUListItemModel> SKUListItems {
            get { return _skuListItems; }
        }

        #endregion

        #region Implementation of IViewController

        protected override IView CreateView() {
            return _skuListView ?? new SKUListView();
        }

        public override void LoadData(bool reload = false) {
            Products = DataProvider.GetProductList(FilterController.CurrentCountry.Id, false);
            _skuListItems = DataProvider.GetSkuListItems(FilterController.Date, FilterController.CurrentCountry.Id);
            for (int lI = 0; lI < _skuListItems.Count; lI++) {
                SKUListItemModel lSKUItem = _skuListItems[lI];
                List<ProductTreeModel> lProducts = Products.Where(item => item.ItemId == lSKUItem.ProductCnId && item.Level == 2).ToList();
                foreach (ProductTreeModel lProduct in lProducts) {
                    lSKUItem.AddIdToList(lProduct.Id);
                }
            }

            if (reload)
                _skuListView.RefreshData();

            base.LoadData(reload);
        }

        public override void SaveData() {
            foreach (SKUListItemModel lSKUItem in _skuListItems) {
                if (!lSKUItem.IsDirty)
                    continue;
                List<int> lIdList = lSKUItem.IdList;
                if (lIdList.Count > 0) {
                    List<ProductTreeModel> lSelectedProducts = new List<ProductTreeModel>();
                    lSelectedProducts.AddRange(Products.Where(product => lIdList.Contains(product.Id)));
                    if (lSelectedProducts.Count > 0)
                        lSKUItem.ProductCnId = lSelectedProducts[0].ItemId ?? -1;
                }
                int lResult = DataProvider.SetSkuListItem(FilterController.Date, lSKUItem);

                if (lResult == 0)
                    lSKUItem.AcceptChanges();
            }

            base.SaveData();
        }

        #endregion

        #region Implementation of IBaseSKUConfigController

        protected override void SetCurrentIndex(int value) {
            base.SetCurrentIndex(value);
            if (IsCurrentSkuGroup())
                _currentSKUListItem = _skuListItems[CurrentIndex];
            else
                _currentSKUListItem = null;
        }

        public override bool IsCurrentSkuGroup() {
            if (CurrentIndex < 0 || CurrentIndex >= _skuListItems.Count)
                return false;
            return true;
        }

        private SKUListItemModel CopySkuGroup(SKUListItemModel skuItem, int m3Id)
        {
            SKUListItemModel lSKUItem = SKUListItemModel.CreateInstance();
            lSKUItem.ListType = FilterController.CurrentListTypeItem.Id;
            lSKUItem.CountryId = FilterController.CurrentCountry.Id;
            lSKUItem.M3Id = m3Id;
            lSKUItem.SKUGroupId = -1;
            lSKUItem.SKUGroupName = skuItem.SKUGroupName;
            lSKUItem.SetId = -2;
            lSKUItem.ChannelMask = skuItem.ChannelMask;
            lSKUItem.SortOrder = skuItem.SortOrder;
            lSKUItem.SelectedIdList = skuItem.SelectedIdList;
            lSKUItem.ProductCnId = skuItem.ProductCnId;
            lSKUItem.IsInPackage = skuItem.IsInPackage;

            return lSKUItem;
        }

        public override void CopySku(DateTime dateTo, List<string> staffIds)
        {
            List<SKUListItemModel> lSkuItemsForM3 =
                _skuListItems.Where(g => g.M3Id == FilterController.CurrentM3.Id && g.ListType == FilterController.CurrentListTypeItem.Id).ToList();

            foreach (string m3 in staffIds)
            {
                int m3Id = int.Parse(m3);

                foreach (SKUListItemModel skuItem in lSkuItemsForM3)
                {
                    SKUListItemModel lCopy = CopySkuGroup(skuItem, m3Id);
                    //save group
                    List<int> lIdList = lCopy.IdList;
                    if (lIdList.Count > 0)
                    {
                        List<ProductTreeModel> lSelectedProducts = new List<ProductTreeModel>();
                        lSelectedProducts.AddRange(Products.Where(product => lIdList.Contains(product.Id)));
                        if (lSelectedProducts.Count > 0)
                            lCopy.ProductCnId = lSelectedProducts[0].ItemId ?? -1;
                    }
                    int lResult = DataProvider.SetSkuListItem(dateTo, lCopy);
                }
            }
        }

        public override List<int> GetCurrentIdList() {
            if (CurrentIndex < 0 || CurrentIndex >= _skuListItems.Count) {
                return new List<int>();
            }
            return _skuListItems[CurrentIndex].IdList;
        }

        public override string GetSelectedIdList() {
            return _currentSKUListItem.SelectedIdList;
        }

        public override void SetSelectedIdList(string strIdList) {
            _currentSKUListItem.SelectedIdList = strIdList;
        }

        public override void AddSkuGroup() {
            base.AddSkuGroup();
            SKUListItemModel lSKUItem = SKUListItemModel.CreateInstance();
            lSKUItem.ListType = FilterController.CurrentListTypeItem.Id;
            lSKUItem.CountryId = FilterController.CurrentCountry.Id;
            lSKUItem.M3Id = FilterController.CurrentM3.Id;
            lSKUItem.SKUGroupId = SKUGroupIdCounter--;
            lSKUItem.SKUGroupName = "Новая цель";
            lSKUItem.SetId = SetIdCounter--;
            lSKUItem.ChannelMask = 0;
            lSKUItem.ProductCnId = -1;
            lSKUItem.SortOrder = (_skuListItems.Count == 0)
                                     ? 1.0
                                     : _skuListItems.Max(g => g.SortOrder) + 1.0;
            _skuListItems.Add(lSKUItem);
            CurrentIndex = _skuListItems.Count - 1;
        }

        public override int DeleteSkuGroup() {
            if (!IsCurrentSkuGroup())
                return -100;
            SKUListItemModel lSKUItem = _skuListItems[CurrentIndex];
            //TODO: security - check permissions
//            if () {
//                XtraMessageBox.Show("Вам запрещено удалять неактивные СКЮ.", "Предупреждение", MessageBoxButtons.OK,
//                    MessageBoxIcon.Warning);
//                return -100;
//            }
            int lErrorCode = 0;
            int lSKUGroupId = lSKUItem.SKUGroupId;
            if (lSKUGroupId > 0)
                lErrorCode = DataProvider.DeleteSkuListItem(lSKUItem.SKUGroupId);

            if (lErrorCode < 0) {
                DialogResult lDialogResult = XtraMessageBox.Show(
                    "Существуют подсчитанные факты или введены планы на месяц.\n Вы действительно хотите удалить СКЮ и его факты в текущем месяце?",
                    "Предупреждение", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation);

                if (lDialogResult == DialogResult.Yes)
                    lErrorCode = DataProvider.DeleteSkuListItem(lSKUItem.SKUGroupId, true);
                else
                    return lErrorCode;
            }

            if (lErrorCode != 0) {
                XtraMessageBox.Show("Невозможно удалить СКЮ. Существуют подсчитанные факты или введены планы на месяц.", "Предупреждение",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return lErrorCode;
            }
            return lErrorCode;
        }

        public override void SwapItems(int index1, int index2, bool isUp = false) {
            SKUListItemModel lSKUItem1 = _skuListItems[index1];
            SKUListItemModel lSKUItem2 = _skuListItems[index2];
            _skuListItems[index2] = lSKUItem1;
            _skuListItems[index1] = lSKUItem2;
            //move up
            if (isUp) {
                if (index1 - 1 < 0 || lSKUItem1.SortOrder < _skuListItems[index1 - 1].SortOrder)// move to the first position OR on the vedge between country and M3 settings (if country SortOrder is greater)  
                    lSKUItem2.SortOrder = lSKUItem1.SortOrder - 1.0;
                else
                    lSKUItem2.SortOrder = (lSKUItem1.SortOrder + _skuListItems[index1 - 1].SortOrder) / 2.0;
            }
            else {
                //move down
                if (index2 + 1 > _skuListItems.Count - 1 || lSKUItem2.SortOrder > _skuListItems[index2 + 1].SortOrder)
                    lSKUItem1.SortOrder = lSKUItem2.SortOrder + 1.0;
                else
                    lSKUItem1.SortOrder = (lSKUItem2.SortOrder + _skuListItems[index2 + 1].SortOrder) / 2.0;
            }
        }

        #endregion
    }
}