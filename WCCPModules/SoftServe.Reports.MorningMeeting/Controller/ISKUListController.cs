﻿using System.Collections.Generic;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public interface ISKUListController : IBaseSKUConfigController {
        #region Properties

        SKUListItemModel CurrentSKUListItem { get; }
        List<SKUListItemModel> SKUListItems { get; }

        #endregion
         
    }
}