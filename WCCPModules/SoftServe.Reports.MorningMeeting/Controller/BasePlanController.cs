﻿using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Forms;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    public class BasePlanController : BaseViewController, IBasePlanController
    {
        #region Fields

        protected readonly IFilterController _filterController;
        protected readonly IFilterView _filterView;

        protected IBasePlanView _basePlanView;
        protected BindingToCapsForm _bindingToCapsForm;

        protected EditedPlansList _editedPlansList;
        protected EditedPlansList _capsPlansList;
        #endregion

        public BasePlanController(IBasePlanView view)
            : base(view)
        {
            _basePlanView = (IBasePlanView)GetView();
            _filterController = new FilterController(_filterView);
            _filterController.ShowM3StaffItem = false;
            _filterController.Start();
            _filterView = (IFilterView)_filterController.GetView();
            _filterView.ShowListType = false;
            _filterView.ShowCheckM1 = true;
            _filterView.StartTo(_basePlanView);

            _filterView.DateChanged += OnDateChanged;
            _filterView.DateChanging += OnDateChanging;
            _filterView.CountryChanging += OnCountryChanging;
            _filterView.M3Changed += OnM3Changed;
            _filterView.M3Changing += OnM3Changing;
            _filterView.ShowM1Changed += OnShowM1Changed;


            _bindingToCapsForm = new BindingToCapsForm();
            _editedPlansList = new EditedPlansList();
            _capsPlansList = new EditedPlansList();
        }

        private void OnShowM1Changed(object sender, ShowM1ChangeEventArgs showM1ChangeEventArgs)
        {
            _basePlanView.ShowM1(_filterController.ShowM1);
        }

        private void OnM3Changed(object sender, ListItemChangeEventArgs listItemChangeEventArgs)//handles both M3 and coutry changes
        {
            _basePlanView.ClearFilter();
            LoadData();
        }

        private void OnM3Changing(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _basePlanView.SaveChangesAndProceed();
            }
        }
        
        private void OnCountryChanging(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _basePlanView.SaveChangesAndProceed();
            }
        }

        private void OnDateChanged(object sender, DateChangeEventArgs dateChangeEventArgs)
        {
            LoadData();
        }

        private void OnDateChanging(object sender, DateChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _basePlanView.SaveChangesAndProceed();
            }
        }

        protected override IView CreateView()
        {
            return _basePlanView ?? new BasePlanView();
        }

        public BindingToCapsForm BindingForm
        {
            get { return _bindingToCapsForm; }
        }

        public override void LoadData(bool reload = false)
        {
            _bindingToCapsForm.LoadData(_filterController.CurrentCountry.Id, _filterController.CurrentM3.Id, _filterController.Date);
            _basePlanView.ShowM1(_filterController.ShowM1);
            _editedPlansList.Clear();
            _capsPlansList.Clear();
        }

        public override void SaveData()
        {
            _editedPlansList.SaveChangedToDb();
            ClearUpdatedValues();
        }

        public virtual void AddUpdatedValue(PlanItemModel item)
        {
            _editedPlansList.Add(item);
        }

        public void AddCapsPlanValue(PlanItemModel item)
        {
            _capsPlansList.Add(item);
        }

        public void SaveCapsPlans()
        {
            _capsPlansList.SaveChangedToDb();
            _capsPlansList.Clear();
        }

        public void ClearUpdatedValues()
        {
            _editedPlansList.Clear();
        }

        public void ForceAutoMapping()
        {
            DataProvider.ForceSubtargetsAutoMapping(_filterController.CurrentCountry.Id, _filterController.CurrentM3.Id, _filterController.Date);
        }

        public bool HasUnsavedChanges
        {
            get
            {
                _basePlanView.PostEditor();
                return !_editedPlansList.IsEmpty;
            }
        }

        public void SetShowM1Value(bool val)
        {
            _filterView.SetCheckShowM1Value(val);
        }
    }
}