﻿using System;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Forms;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    public interface IBasePlanController : IViewController
    {
        void AddUpdatedValue(PlanItemModel item);
        void AddCapsPlanValue(PlanItemModel item);
        void ClearUpdatedValues();
        void ForceAutoMapping();
        BindingToCapsForm BindingForm { get; }
        bool HasUnsavedChanges { get; }
        void SaveCapsPlans();
        void SetShowM1Value(bool val);
    }
}