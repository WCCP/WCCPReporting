﻿using System.Collections.Generic;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public interface IProductGroupController : IBaseSKUConfigController {
        #region Properties

        ProductGroupModel CurrentProductGroup { get; }
        List<ProductGroupModel> ProductGroups { get; }

        #endregion

    }
}