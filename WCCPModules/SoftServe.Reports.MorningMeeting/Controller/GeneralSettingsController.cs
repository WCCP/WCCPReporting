﻿using System.Collections.Generic;
using System.Linq;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    class GeneralSettingsController : BaseViewController, IGeneralSettingsController
    {
        #region Fields

        private readonly IFilterController _filterController;
        private readonly IFilterView _filterView;

        private IGeneralSettingsView _generalSettingsView;

        private List<GeneralSettingsModel> _settings;
        private CountryLevelSettingsModel _countryLevelSettings;
        private List<ActionListModel> _selectedMP = new List<ActionListModel>();
        private List<int> _updatedMPs = new List<int>();

        private const int _countryLevel = -1;
        private const string Delimeter = ",";

        private bool _m3HasSettingsCopiedFromCountryOnly;// settings are inherited from country, but not saved directly
        #endregion
        
        public GeneralSettingsController(IGeneralSettingsView view)
            : base(view)
        {
            _generalSettingsView = (IGeneralSettingsView)GetView();
            _filterController = new FilterController(_filterView);
            _filterController.Start();
            _filterView = (IFilterView)_filterController.GetView();
            _filterView.ShowListType = false;
            _filterView.StartTo(_generalSettingsView);

            (_filterView as FilterView).DateChanging += OnDateChanging;
            (_filterView as FilterView).DateChanged += OnDateChanged;
            (_filterView as FilterView).CountryChanging += OnCountryChanging;
            (_filterView as FilterView).CountryChanged += OnCountryChanged;
            (_filterView as FilterView).M3Changing += OnM3Changing;
            (_filterView as FilterView).M3Changed += OnM3Changed;

            SetAccessibility();
            HasUnsavedChanges = false;
        }

        private void OnM3Changed(object sender, ListItemChangeEventArgs listItemChangeEventArgs)
        {
            // old m3 hasn't changes in comparison to country level 
            if (listItemChangeEventArgs.OldId != _countryLevel && !HasUnsavedChangesForCurrentM3 && _m3HasSettingsCopiedFromCountryOnly)
            {
                DeleteSettings(listItemChangeEventArgs.OldId);
            }
            else
            {
                UpdateCurrentSettingSet(listItemChangeEventArgs.OldId);
            }

            if (_settings.FirstOrDefault(r => r.M3Id == listItemChangeEventArgs.NewId) == null)
            {
                _m3HasSettingsCopiedFromCountryOnly = true;
            }
            else
            {
                _m3HasSettingsCopiedFromCountryOnly = false;
            }

            LoadCurrentSettingSet(listItemChangeEventArgs.NewId);
            SetAccessibility();
            _generalSettingsView.HideValidation();
        }

        private void OnM3Changing(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChangesForCurrentM3)
            {
                listItemChangeEventArgs.IsCanceled = !_generalSettingsView.ValidateRecord();
            }
        }

        private void OnCountryChanged(object sender, ListItemChangeEventArgs listItemChangeEventArgs)
        {
            LoadData(true);
            SetAccessibility();

            _generalSettingsView.HideValidation();
        }

        private void OnCountryChanging(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _generalSettingsView.SaveChangesAndProceed();
            }
        }

        private void OnDateChanged(object sender, DateChangeEventArgs dateChangeEventArgs)
        {
            LoadData(true);
            SetAccessibility();
            _generalSettingsView.HideValidation();
        }

        private void OnDateChanging(object sender, DateChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _generalSettingsView.SaveChangesAndProceed();
            }
        }

        protected override IView CreateView()
        {
            return _generalSettingsView ?? new GeneralSettingsView();
        }

        public override void LoadData(bool reload = false)
        {
            if (reload)
            {
                InitDataSources();
            }

            if (_filterController.CurrentCountry != null)
                LoadCurrentSettingSet(_filterController.CurrentM3.Id);

            HasUnsavedChanges = false;
        }

        public override void SaveData()
        {
            // save settings
            if (_settings != null)
            {
                foreach (GeneralSettingsModel setting in _settings)
                {
                    if (setting.IsDirty && setting.AmountOfFocusOLs != -1)
                    {
                        DataProvider.SetSettingsSet(_filterController.Date, setting);
                    }
                }
            }

            // save country level settings
            if (_countryLevelSettings != null)
            {
                if (RoleManager.IsInRole(MMRoleEnum.GeneralM5))
                {
                    DataProvider.SetCountrySettings(_filterController.CurrentCountry.Id, _countryLevelSettings);
                }
            }

            // save updated MPs
            if (_selectedMP != null)
            {
                foreach (int m3 in _updatedMPs)
                {
                    List<ActionListModel> lActions = GetMarketProgs(m3, false);
                    DataProvider.SetSelectedMarketProgramsList(_filterController.Date, _filterController.CurrentCountry.Id, _filterController.CurrentM3.Id, GetListDescription(lActions));
                }

                _updatedMPs.Clear();
            }

            _m3HasSettingsCopiedFromCountryOnly = false;
            HasUnsavedChanges = false;
            HasUnsavedChangesForCurrentM3 = false;
        }

        #region IGeneralSettingController

        public void LoadCurrentSettingSet(int m3)
        {
            _generalSettingsView.AssignSettings(GetCurSettings(m3, true));
            _generalSettingsView.AssignCountrySettings(_countryLevelSettings);

            _generalSettingsView.AssignSelectedMP(GetMarketProgs(m3, true));

            HasUnsavedChangesForCurrentM3 = false;
        }

        public void UpdateCurrentSettingSet()
        {
            UpdateCurrentSettingSet(_filterController.CurrentM3.Id);
        }

        public bool IsCountryLevel
        {
            get { return _filterController.CurrentM3.Id == -1; }
        }

        public bool HasUnsavedChanges { get; set; }

        public bool HasUnsavedChangesForCurrentM3 { get; set; }

        #endregion

        private void InitDataSources()
        {
            if (_filterController.CurrentCountry != null)
            {
                _settings = DataProvider.GetSettingsSet(_filterController.Date, _filterController.CurrentCountry.Id);
                _countryLevelSettings = DataProvider.GetCountrySettings(_filterController.CurrentCountry.Id);

                _selectedMP = DataProvider.GetSelectedMarketProgramsList(_filterController.Date, _filterController.CurrentCountry.Id);

                _generalSettingsView.LoadMPs(DataProvider.GetMarketProgramsList(_filterController.Date, _filterController.CurrentCountry.Id));
                _generalSettingsView.LoadAkbCalcMethods(DataRepository.AkbCalcMethodsList);
                _generalSettingsView.LoadSrCalcMethods(DataRepository.SrCalcMethodsList);
            }
        }

        public void UpdateCurrentSettingSet(int m3)
        {
            UpdateSettingsList(m3);
            UpdateCountrySettings();

            UpdateMPList(m3);
        }

        public GeneralSettingsModel GetCurSettings(int m3, bool showCountryIfEmpty)
        {
            GeneralSettingsModel lSett = null;

            if (_settings != null)
            {
                lSett = _settings.FirstOrDefault(s => s.M3Id == m3);

                if (lSett == null && showCountryIfEmpty)
                {
                    lSett = _settings.FirstOrDefault(s => s.M3Id == _countryLevel);

                    lSett = lSett ?? AddSetting(m3);
                }
            }

            return lSett;
        }

        public List<ActionListModel> GetMarketProgs(int m3, bool showCountryIfEmpty)
        {
            List<ActionListModel> lPrograms = null;

            if (_selectedMP != null)
            {
                lPrograms = _selectedMP.Where(s => s.M3Id == m3).Select(s => s).ToList();

                if (lPrograms.Count == 0 && showCountryIfEmpty)
                {
                    lPrograms = _selectedMP.Where(s => s.M3Id == _countryLevel).Select(s => s).ToList();
                }
            }

            return lPrograms;
        }

        private void UpdateSettingsList(int m3)
        {
            GeneralSettingsModel sett = GetCurSettings(m3, false) ?? AddSetting(m3);

            _generalSettingsView.GetSettings(sett);
        }

        private void UpdateCountrySettings()
        {
            if (_countryLevelSettings == null)
            {
                _countryLevelSettings = CountryLevelSettingsModel.CreateInstance();
            }

             _generalSettingsView.GetCountrySettings(_countryLevelSettings);
        }

        private void UpdateMPList(int m3)
        {
            List<ActionListModel> lPrograms = GetMarketProgs(m3, false);

            //remove old MPs
            foreach (var actionListModel in lPrograms)
            {
                _selectedMP.Remove(actionListModel);
            }

            //add new
            List<ActionListModel> lProgramsNew = new List<ActionListModel>();

            _generalSettingsView.GetSelectedMP(lProgramsNew, _filterController.CurrentCountry.Id, m3);
            _selectedMP.AddRange(lProgramsNew);

            //check as updated
            if (!AreListsEqual(lPrograms, lProgramsNew))
            {
                _updatedMPs.Add(m3);
            }
        }

        private GeneralSettingsModel AddSetting(int m3)
        {
            GeneralSettingsModel sett = GeneralSettingsModel.CreateInstance();
            sett.CountryId = _filterController.CurrentCountry.Id;
            sett.M3Id = m3;

            sett.AmountOfTheWorstOLs = -1;
            sett.AmountOfFocusOLs = -1;
            sett.InBevBeerVolumeLimit = -1;
            sett.AmountOfDistrPushSKU = -1;
            sett.AmountOfFocusSKU = -1;
            sett.AmountOfMP = -1;
            sett.AmountOfFocusTargets = -1;

            _settings.Add(sett);

            return sett;
        }

        private void SetAccessibility()
        {
            _generalSettingsView.SetAccessibility(CanEditCurrentM3);
        }

        private void DeleteSettings(int m3Id)
        {
            if (_settings != null)
            {
                GeneralSettingsModel settings = _settings.FirstOrDefault(s => s.M3Id == m3Id);

                _settings.Remove(settings);
            }

            if (_selectedMP != null)
            {
                ActionListModel program = _selectedMP.FirstOrDefault(s => s.M3Id == m3Id);
                _selectedMP.Remove(program);

                if (_updatedMPs.Contains(m3Id))
                {
                    _updatedMPs.Remove(m3Id);
                }
            }
        }

        private bool CanEditCurrentM3
        {
            get { return IsCountryLevel && RoleManager.IsInRole(MMRoleEnum.GeneralM5) || !IsCountryLevel && RoleManager.IsInRole(MMRoleEnum.GeneralM3); }
        }

        #region List Helpers

        private bool AreListsEqual(List<ActionListModel> currentList, List<ActionListModel> newList)
        {
            if (currentList.Count != newList.Count)
            {
                return false;
            }

            bool lAreEqual = newList.All(currentList.Contains) && currentList.All(newList.Contains);

            return lAreEqual;
        }

        private string GetListDescription(List<ActionListModel> list)
        {
            return string.Join(Delimeter, list.Select(a => a.ItemId.ToString()).ToArray());
        }

        #endregion
    }
}
