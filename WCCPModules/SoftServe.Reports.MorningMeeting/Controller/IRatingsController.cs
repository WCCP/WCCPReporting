﻿using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    interface IRatingsController : IViewController
    {
        StaffTreeModel CurrentM3 { get; }

        int AddRating(int m3, int channenId);
        void DeleteRating(int id);

        bool IsCountryLevel { get; }
        bool HasUnsavedChanges { get; set; }
        bool HasUnsavedChangesForCurrentM3 { get; set; }
    }
}
