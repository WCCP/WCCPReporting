﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    internal class FocusGoalsController : BaseViewController, IFocusGoalsController
    {
        #region Fields

        private readonly IFilterController _filterController;
        private readonly IFilterView _filterView;

        private IFocusGoalsView _focusGoalsview;

        private List<int> _deletedGoalsId = new List<int>();

        private List<FocusGoalModel> _goals;
        private List<ManualGoalDetailsModel> _manualDetails;
        private List<AutoGoalDetailsModel> _autoDetails;

        private int _countryLevel = -1;

        private int _index = -1;

        #endregion

        public StaffTreeModel CurrentM3
        {
            get { return _filterController.CurrentM3; }
        }

        public FocusGoalsController(IFocusGoalsView view)
            : base(view)
        {
            _focusGoalsview = (IFocusGoalsView)GetView();
            _filterController = new FilterController(_filterView);
            _filterController.Start();
            _filterView = (IFilterView)_filterController.GetView();
            _filterView.ShowListType = false;
            _filterView.ShowPlanDate = false;
            _filterView.ShowDatePeriod = true;
            _filterView.StartTo(_focusGoalsview);

            _filterView.CountryChanging += OnCountryChanging;
            _filterView.CountryChanged += OnCountryChanged;
            _filterView.M3Changing += OnM3Changing;
            _filterView.M3Changed += OnM3Changed;
            _filterView.IntervalChanged += OnIntervalChanged;
            _filterView.IntervalChanging += OnIntervalChanging;

            HasUnsavedChanges = false;
        }

        private void OnIntervalChanged(object sender, IntervalChangeEventArgs intervalChangeEventArgs)
        {
            LoadData();
        }

        private void OnIntervalChanging(object sender, IntervalChangingEventArgs intervalChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                intervalChangeEventArgs.IsCanceled = _focusGoalsview.SaveChangesAndProceed();
            }
        }

        private void OnM3Changed(object sender, ListItemChangeEventArgs listItemChangeEventArgs)
        {
            _focusGoalsview.OnM3FilterChange(listItemChangeEventArgs.NewId);
        }

        private void OnM3Changing(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            listItemChangeEventArgs.IsCanceled = !_focusGoalsview.IsValid;
        }

        private void OnCountryChanged(object sender, ListItemChangeEventArgs listItemChangeEventArgs)
        {
            LoadData();
        }

        private void OnCountryChanging(object sender, ListItemChangingEventArgs listItemChangeEventArgs)
        {
            if (HasUnsavedChanges)
            {
                listItemChangeEventArgs.IsCanceled = _focusGoalsview.SaveChangesAndProceed();
            }
        }


        protected override IView CreateView()
        {
            return _focusGoalsview ?? new FocusGoalsView();
        }

        public override void LoadData(bool reload = false)
        {
            InitDataSources();

            _focusGoalsview.AssignGoalTypes(DataProvider.GetGoalTypesList());
            _focusGoalsview.AssignAutoGoalTypes(DataProvider.GetAutoGoalsTypesList());

            _focusGoalsview.AssignChannels(DataProvider.GetChannelList());
            _focusGoalsview.AssignProductTree(DataProvider.GetProductList(_filterController.CurrentCountry.Id, false));

            _focusGoalsview.AssignGoals(_goals);
            _focusGoalsview.OnM3FilterChange(_filterController.CurrentM3.Id);

            HasUnsavedChanges = false;
        }

        #region IFocusGoalsController

        public void AddNewManualGoal(int m3)
        {
            int lGoalId = AddGoal(m3, true);

            ManualGoalDetailsModel item = ManualGoalDetailsModel.CreateInstance();
            item.TargetId = lGoalId;
            item.Description = string.Empty;

            _manualDetails.Add(item);
        }

        public void AddNewAutoGoal(int m3)
        {
            int lGoalId = AddGoal(m3, false);

            AutoGoalDetailsModel item = AutoGoalDetailsModel.CreateInstance();
            item.TargetId = lGoalId;

            _autoDetails.Add(item);
        }

        public void LoadGoal(int goalId, bool isManual)
        {
            if (isManual)
            {
                ManualGoalDetailsModel item = GetManualDetails(goalId);
                _focusGoalsview.DisplayManualGoalDetails(item);
            }
            else
            {
                AutoGoalDetailsModel item = GetAutoDetails(goalId);
                _focusGoalsview.DisplayAutoGoalDetails(item);
            }
        }

        public void UpdateGoal(int goalId, bool isManual)
        {
            if (isManual)
            {
                UpdateManualGoal(goalId);
            }
            else
            {
                UpdateAutoGoal(goalId);
            }
        }

        public void DeleteGoal(int lGoalId)
        {
            if (_goals != null)
            {
                FocusGoalModel lGoal = _goals.FirstOrDefault(g => g.TargetId == lGoalId);

                if (lGoal != null)
                {
                    if (lGoal.IsManual)
                    {
                        DeleteManualDetail(lGoalId);
                    }
                    else
                    {
                        DeleteAutoDetail(lGoalId);
                    }

                    _goals.Remove(lGoal);

                    _deletedGoalsId.Add(lGoal.TargetId);
                }
            }

            HasUnsavedChanges = true;
        }

        public override void SaveData()
        {
            if (HasUnsavedChanges)
            {
                SaveUpdatedGoalsToDb();
                SaveDeletedGoalsToDb();
            }

            HasUnsavedChanges = false;
        }

        public bool IsCountryLevel
        {
            get { return _filterController.CurrentM3.Id == _countryLevel; }
        }

        public bool HasUnsavedChanges { get; set; }

        #endregion

        private void InitDataSources()
        {
            if (_filterController.CurrentCountry != null)
            {
                _goals = DataProvider.GetGoalsList(_filterController.StartDate, _filterController.EndDate,
                                                   _filterController.CurrentCountry.Id);
                _manualDetails = DataProvider.GetManualDetailsList(_filterController.StartDate,
                                                                   _filterController.EndDate,
                                                                   _filterController.CurrentCountry.Id);
                _autoDetails = DataProvider.GetAutoDetailsList(_filterController.StartDate, _filterController.EndDate,
                                                               _filterController.CurrentCountry.Id);
            }
        }

        public AutoGoalDetailsModel GetAutoDetails(int goalId)
        {
            AutoGoalDetailsModel autoDetails = null;

            if (_autoDetails != null)
            {
                autoDetails = _autoDetails.FirstOrDefault(d => d.TargetId == goalId);
            }

            return autoDetails;
        }

        public ManualGoalDetailsModel GetManualDetails(int goalId)
        {
            ManualGoalDetailsModel manualDetails = null;

            if (_manualDetails != null)
            {
                manualDetails = _manualDetails.FirstOrDefault(d => d.TargetId == goalId);
            }

            return manualDetails;
        }

        private int AddGoal(int m3, bool isManual)
        {
            FocusGoalModel item = FocusGoalModel.CreateInstance();
            item.TargetId = _index;
            item.TargetName = "Новая цель" + (-_index).ToString();
            item.M3Id = m3;
            item.IsManual = isManual;
            item.CreationDate = DateTime.Now;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            item.ChannelMask = 0;
            _goals.Add(item);

            _index--;

            HasUnsavedChanges = true;

            return item.TargetId;
        }

        private void UpdateManualGoal(int goalId)
        {
            ManualGoalDetailsModel item = GetManualDetails(goalId);
            _focusGoalsview.GetManualGoalDetails(item);
        }

        private void UpdateAutoGoal(int goalId)
        {
            AutoGoalDetailsModel item = GetAutoDetails(goalId);
            _focusGoalsview.GatAutoGoalDetails(item);
        }

        private void DeleteManualDetail(int lGoalId)
        {
            ManualGoalDetailsModel lManualDetails = GetManualDetails(lGoalId);

            if (lManualDetails != null)
            {
                _manualDetails.Remove(lManualDetails);
            }
        }

        private void DeleteAutoDetail(int lGoalId)
        {
            AutoGoalDetailsModel lAUtoDetails = GetAutoDetails(lGoalId);

            if (lAUtoDetails != null)
            {
                _autoDetails.Remove(lAUtoDetails);
            }
        }

        private void SaveDeletedGoalsToDb()
        {
            if (_deletedGoalsId != null)
            {
                foreach (int goal in _deletedGoalsId)
                {
                    DataProvider.DeleteGoal(goal);
                }

                _deletedGoalsId.Clear();
            }
        }

        private void SaveUpdatedGoalsToDb()
        {
            if (_goals != null)
            {
                foreach (FocusGoalModel goal in _goals)
                {
                    int lGoalId = goal.TargetId;

                    if (goal.IsDirty)
                    {
                        lGoalId = DataProvider.UpdateGoal(goal, _filterController.CurrentCountry.Id);
                    }

                    if (goal.IsManual)//save manual details
                    {
                        ManualGoalDetailsModel manualDetails = GetManualDetails(goal.TargetId);

                        if (manualDetails != null && manualDetails.IsDirty)
                        {
                            manualDetails.TargetId = lGoalId;
                            DataProvider.UpdateManualDetails(manualDetails);
                        }
                    }
                    else//save auto details
                    {
                        AutoGoalDetailsModel autoDetails = GetAutoDetails(goal.TargetId);

                        if (autoDetails != null && autoDetails.IsDirty)
                        {
                            autoDetails.TargetId = lGoalId;
                            DataProvider.UpdateAutoDetails(autoDetails);
                        }
                    }

                    goal.TargetId = lGoalId;
                }
            }
        }
    }
}
