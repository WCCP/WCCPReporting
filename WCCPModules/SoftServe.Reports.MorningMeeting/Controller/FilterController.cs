﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public class FilterController : BaseViewController, IFilterController {
        #region Fields

        private IFilterView _view;
        private ListTypeEnum _listType;
        private ListItemModel _currentListTypeItem;
        private ListItemModel _oldListTypeItem;
        private DateTime _date;
        private DateTime _oldDate;
        private DateTime _startDate;
        private DateTime _endDate;
        private CountryModel _currentCountry;
        private CountryModel _oldCountry;
        private StaffTreeModel _currentM3;
        private StaffTreeModel _oldM3;
        private bool _oldShowM1Value = false;
        private bool _showM1Value = false;

        private bool _showListItemAllM3 = true;

        private List<ListItemModel> _listTypes;
        private List<CountryModel> _countryList;
        private List<StaffTreeModel> _staffTreeList;

        #endregion

        public FilterController(IFilterView view)
            : base(view) {
            _view = (IFilterView) GetView();
        }

        #region Implementation of IViewController

        protected override IView CreateView() {
            return _view ?? new FilterView();
        }

        public override void LoadData(bool reload = false) {
            _date = DateTime.Today.Date;
            _oldDate = _date;

            _startDate = DateTime.Today.Date;
            _endDate = DateTime.Today.Date;

            _listTypes = DataProvider.GetListTypes(ListTypeEnum.ProductSet);
            _currentListTypeItem = _listTypes[0];
            _oldListTypeItem = _currentListTypeItem;

            _countryList = DataRepository.Countries;
            if (_countryList.Count <= 0)
                return;
            _currentCountry = _countryList[0];
            _oldCountry = _currentCountry;

            LoadStaffTree();
        }

        private void LoadStaffTree() {
            _currentM3 = null;
            _staffTreeList = DataRepository.GetStaffTreeList(_currentCountry.Id, _showListItemAllM3);
            if (_staffTreeList.Count <= 0)
                return;

            if (_showListItemAllM3)
                _currentM3 = _staffTreeList.FirstOrDefault(s => s.Id == -1); // Все М3
            else {
                StaffTreeModel lFirstM4 = _staffTreeList.FirstOrDefault(s => s.ItemId != -1 && s.ParentId == null);
                StaffTreeModel lFirstM3 = lFirstM4 == null ? null : _staffTreeList.FirstOrDefault(s => s.ParentId == lFirstM4.Id);
                CurrentM3 = lFirstM3;
            }
            _oldM3 = _currentM3;
        }

        public bool ShowM3StaffItem {
            get { return _showListItemAllM3; }
            set { _showListItemAllM3 = value; }
        }

        #endregion

        #region Implementation of IFilterController

        public ListTypeEnum ListType {
            get { return _listType; }
            set {
                if (_listType == value)
                    return;
                ChangeListType(value);
            }
        }

        public void ChangeListType(ListTypeEnum listType) {
            _listType = listType;
            _listTypes = DataProvider.GetListTypes(listType);
            _currentListTypeItem = _listTypes[0];
        }

        public ListItemModel CurrentListTypeItem {
            get { return _currentListTypeItem; }
            set {
                if (_currentListTypeItem.Equals(value))
                    return;
                _oldListTypeItem = _currentListTypeItem;
                _currentListTypeItem = value;
                ChangeListTypeItem();
            }
        }

        private void ChangeListTypeItem() {
            if (_oldListTypeItem.Equals(_currentListTypeItem))
                return;
            _view.OnSKUListTypeItemChanged(this, new ListItemChangeEventArgs(_oldListTypeItem.Id, _currentListTypeItem.Id));
        }

        public DateTime Date {
            get { return _date; }
            set {
                if (_date == value)
                    return;
                _oldDate = _date;
                _date = value;
                ChangeDate();
            }
        }

        private void ChangeDate() {
            if (_oldDate.Year == _date.Year && _oldDate.Month == _date.Month)
                return;
            _view.OnDateChanged(this, new DateChangeEventArgs(_oldDate, _date));
        }

        public DateTime StartDate {
            get { return _startDate; }
            set {
                if (_startDate == value)
                    return;
                _startDate = value;
                ChangeInterval();
            }
        }

        public DateTime EndDate {
            get { return _endDate; }
            set {
                if (_endDate == value)
                    return;
                _endDate = value;
                ChangeInterval();
            }
        }

        private void ChangeInterval() {
            if (_startDate > _endDate)
                return;
            _view.OnIntervalChanged(this, new IntervalChangeEventArgs(_startDate, _endDate));
        }

        public CountryModel CurrentCountry {
            get { return _currentCountry; }
            set {
                if (_currentCountry.Equals(value))
                    return;
                _oldCountry = _currentCountry;
                _currentCountry = value;
                ChangeCountry();
            }
        }

        private void ChangeCountry() {
            if (_oldCountry.Equals(_currentCountry))
                return;
            
            LoadStaffTree();
            
            _view.OnCountryChanged(this, new ListItemChangeEventArgs(_oldCountry.Id, _currentCountry.Id));
            _view.RefreshData();
        }

        public StaffTreeModel CurrentM3 {
            get { return _currentM3; }
            set {
                if (_currentM3 != null && _currentM3.Equals(value))
                    return;

                _oldM3 = _currentM3;
                _currentM3 = value;
                ChangeM3();
            }
        }

        private void ChangeM3() {
            if(_currentM3 == null)
                return;

            _view.OnM3Changed(this, new ListItemChangeEventArgs(_oldM3 == null ? -2 : _oldM3.Id, _currentM3.Id));
        }

        public bool ShowM1 {
            get { return _showM1Value; }
            set {
                if (_showM1Value.Equals(value))
                    return;
                _oldShowM1Value = _showM1Value;
                _showM1Value = value;
                ChangeShowM1Value();
            }
        }

        private void ChangeShowM1Value() {
            if (_oldShowM1Value.Equals(_showM1Value))
                return;
            _view.OnShowM1Changed(this, new ShowM1ChangeEventArgs(_oldShowM1Value, _showM1Value));
        }

        public List<ListItemModel> ListTypeItems {
            get { return _listTypes; }
        }

        public List<CountryModel> CountryList {
            get { return _countryList; }
        }

        public List<StaffTreeModel> StaffTreeList {
            get { return _staffTreeList; }
        }

        #endregion
    }
}