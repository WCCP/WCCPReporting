﻿using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.View;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    public class NetworksPlanController : BasePlanController, INetworksPlanController
    {
        #region Fields
        
        private INetworksPlanView _networksPlanView;

        #endregion

        public NetworksPlanController(INetworksPlanView view)
            : base(view)
        {
            _networksPlanView = (INetworksPlanView)GetView();
        }

        protected override IView CreateView()
        {
            return _networksPlanView ?? new NetworksPlanView();
        }

        public override void LoadData(bool reload = false)
        {
            _networksPlanView.ConstructView(DataProvider.GetNetworkColumnList(_filterController.Date, _filterController.CurrentCountry.Id, _filterController.CurrentM3.Id));
            _networksPlanView.AssignDataSource(DataProvider.GetNetworksPlans(_filterController.Date, _filterController.CurrentCountry.Id, _filterController.CurrentM3.Id));
            base.LoadData(reload);
        }

    }
}