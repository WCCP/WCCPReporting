﻿using SoftServe.Core.Common.Controller;

namespace SoftServe.Reports.MorningMeeting.Controller
{
    internal interface IGeneralSettingsController : IViewController
    {
        void LoadCurrentSettingSet(int m3);
        void UpdateCurrentSettingSet();

        bool IsCountryLevel { get; }
        bool HasUnsavedChanges { get; set; }
        bool HasUnsavedChangesForCurrentM3 { get; set; }
    }
}