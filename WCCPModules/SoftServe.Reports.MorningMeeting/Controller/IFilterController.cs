﻿using System;
using System.Collections.Generic;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.Controller {
    public interface IFilterController : IViewController {
        DateTime Date { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        ListItemModel CurrentListTypeItem { get; set; }
        CountryModel CurrentCountry { get; set; }
        StaffTreeModel CurrentM3 { get; set; }
        bool ShowM1 { get; set; }
        bool ShowM3StaffItem { get; set; }

        List<ListItemModel> ListTypeItems { get; }
        List<CountryModel> CountryList { get; }
        List<StaffTreeModel> StaffTreeList { get; }
        
        void ChangeListType(ListTypeEnum listType);
    }
}