﻿namespace SoftServe.Reports.MorningMeeting.Utils {
    public enum ListTypeEnum {
        ProductSet = 1,
        SKUList = 2
    }
}