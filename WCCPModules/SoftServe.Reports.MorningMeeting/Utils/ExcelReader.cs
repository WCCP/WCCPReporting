﻿using DevExpress.XtraEditors;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SoftServe.Reports.MorningMeeting.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace SoftServe.Reports.MorningMeeting.Utils
{
    /// <summary>
    /// Describes the Excel reading methods
    /// </summary>
    internal class ExcelReader
    {
        private System.Globalization.CultureInfo _systemCulture;
        private SpreadsheetDocument _excelFile;
        private WorkbookPart _workbook;

        private string _fileName;

        public ExcelReader(string fileName)
        {
            _fileName = fileName;
            _systemCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        }

        public void StartReading()
        {
            _excelFile = SpreadsheetDocument.Open(_fileName, false);
            _workbook = _excelFile.WorkbookPart;
        }

        public void FinishReading()
        {
            try
            {
                _excelFile.Close();
            }
            catch (Exception) { }
            finally
            {
                Thread.CurrentThread.CurrentCulture = _systemCulture;
            }
        }

        /// <summary>
        /// Gets the worksheet by number
        /// </summary>
        private SheetData GetWorkSheetData(int pageNumber, out string pageName)
        {
            _excelFile = SpreadsheetDocument.Open(_fileName, false);
            _workbook = _excelFile.WorkbookPart;

            Sheet lSheet = _workbook.Workbook.Descendants<Sheet>().ToArray()[pageNumber - 1];
            pageName = lSheet.Name;

            WorksheetPart lWorkSheet = (WorksheetPart)(_workbook.GetPartById(lSheet.Id));
            SheetData lSheetData = lWorkSheet.Worksheet.Elements<SheetData>().First();

            return lSheetData;
        }

        public void ReadFileWorksheet(Dictionary<int, ImportedPlanModel> validPlansFromFile, DataTable invalidPlansFromFile,
                                      int columnsFromFileCount, int worksheetNumber)
        {
            bool wasReadingSuccsessful = true;
            SheetData lSheetData = null;

            try
            {
                string lSheetName;
                lSheetData = GetWorkSheetData(worksheetNumber, out lSheetName);
                List<Row> rows = lSheetData.Elements<Row>().ToList();

                // Read the sheet data
                if (rows.Count > 1)// to flip over the header row :)
                {
                    for (var rowNum = 1; rowNum < rows.Count; rowNum++)
                    {
                        List<string> lCurrentRow = GetRowData(rows[rowNum]);

                        if (IsRowEmpty(lCurrentRow))
                            continue;

                        HandleRowLoading(rowNum + 1, lCurrentRow, worksheetNumber, lSheetName, columnsFromFileCount, validPlansFromFile,
                            invalidPlansFromFile);
                    }
                }
            }
            catch (Exception)
            {
                wasReadingSuccsessful = false;
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            if (!wasReadingSuccsessful)
            {
                throw new Exception();
            }
        }

        private void HandleRowLoading(int rowNumber, List<string> currentRow, int worksheetNumber, string worksheetName,
                                      int columnsFromFileCount, Dictionary<int, ImportedPlanModel> validPlansFromFile,
                                      DataTable invalidPlansFromFile)
        {
            ImportedPlanModel lPlan;
            bool isCorrect = ImportRow(worksheetName, currentRow, rowNumber, out lPlan);

            if (isCorrect)
            {
                int id = 10 * rowNumber + worksheetNumber;
                validPlansFromFile.Add(id, lPlan);
            }
            else
            {
                invalidPlansFromFile.Rows.Add(GetTableRow(currentRow, columnsFromFileCount, worksheetName, rowNumber,
                    lPlan.StatusDescription));
            }
        }

        private static object[] GetTableRow(List<string> rowFromFile, int columnsFromFileCount, string tabName, int rowNumber,
                                            string rowErrorMessage)
        {
            object[] row = new object[columnsFromFileCount + 3];

            for (int j = 0; j < columnsFromFileCount; j++)
            {
                row[j] = j < rowFromFile.Count ? rowFromFile[j] : null;
            }

            row[columnsFromFileCount] = tabName;
            row[columnsFromFileCount + 1] = rowNumber;
            row[columnsFromFileCount + 2] = rowErrorMessage;

            return row;
        }

        private bool ImportRow(string tabName, List<string> row, int rowNumber, out ImportedPlanModel plan)
        {
            string fieldErrorMessage = "Некорректный формат данных";
            string rowErrorMessage = string.Empty;
            plan = new ImportedPlanModel();
            bool isCorrect = true;

            try
            {
                plan.EmployeeId = Convert.ToInt32(row[0], CultureInfo.InvariantCulture);//staff id
                plan.EmployeeName = row[1];// staff name
                plan.KpiName = row[2];// kpi name

                try
                {
                    plan.Date = DateTime.FromOADate(double.Parse(row[3]));//date
                }
                catch (Exception)
                {
                    plan.Date = DateTime.Parse(Convert.ToString(row[3]), new CultureInfo("ru-Ru"));
                }

                if (plan.KpiName.Contains("Время"))//plan value
                {
                    string[] lTimeTokens = row[4].Split(':');
                    int lHours = int.Parse(lTimeTokens[0]);
                    int lMinutes = int.Parse(lTimeTokens[1]);

                    plan.Plan = lHours * 60 + lMinutes;
                }
                else
                {
                    plan.Plan = decimal.Parse(row[4], NumberStyles.Any, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception)
            {
                rowErrorMessage += fieldErrorMessage;
                isCorrect = false;
            }

            plan.PageName = tabName;
            plan.RowNumber = rowNumber;
            plan.StatusDescription = !string.IsNullOrEmpty(rowErrorMessage) ? "Некорректный формат данных" : string.Empty;

            return isCorrect;
        }

        private List<string> GetRowData(Row row)
        {
            var dataRow = new List<string>();

            var cellEnumerator = GetExcelCellEnumerator(row);
            while (cellEnumerator.MoveNext())
            {
                var cell = cellEnumerator.Current;
                var text = ReadExcelCell(cell, _workbook).Trim();

                dataRow.Add(text);
            }

            return dataRow;
        }

        private void FreeResourses()
        {
            if (_workbook != null)
            {
                ReleaseObject(_workbook);
            }

            _excelFile.Close();
            ReleaseObject(_excelFile);
        }

        /// <summary>
        /// Released resources
        /// </summary>
        /// <param name="obj">Object to be released</param>
        private void ReleaseObject(object obj)
        {
            if (obj != null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private bool IsRowEmpty(List<string> row)
        {
            bool isNotEmpty = row.Any(v => !string.IsNullOrEmpty(v));

            return !isNotEmpty;
        }

        private string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                // ASCII 'A' = 65
                int current = i == 0 ? letter - 65 : letter - 64;
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        /// <summary>
        /// Handles row reading cell by cell, cops with gaps in data
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)//empty cells
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }

    }
}