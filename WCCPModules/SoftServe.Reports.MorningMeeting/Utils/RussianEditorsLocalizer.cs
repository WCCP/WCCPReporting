﻿using DevExpress.XtraEditors.Controls;

namespace SoftServe.Reports.MorningMeeting.Utils
{
    public class RussianEditorsLocalizer : Localizer
    {
        public override string Language
        {
            get { return "Russian"; }
        }

        public override string GetLocalizedString(StringId id)
        {
            string ret;

            switch (id)
            {
                case StringId.XtraMessageBoxYesButtonText:
                    ret = "Да";
                    break;
                case StringId.XtraMessageBoxNoButtonText:
                    ret = "Нет";
                    break;
                case StringId.XtraMessageBoxCancelButtonText:
                    ret = "Отмена";
                    break;
                default: ret = base.GetLocalizedString(id);
                    break;
            }

            return ret;
        }
    }
}
