﻿using System.Collections.Generic;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Utils {
    /// <summary>
    /// Coordinates roles
    /// </summary>
    public static class RoleManager {
        #region Fields

        private const string RoleSKU_M5 = "URM_DP_SKU_RW_M5";
        private const string RoleSKU_M3 = "URM_DP_SKU_RW_M3";
        private const string RoleDistrPush_M5 = "URM_DP_DistrPush_RW_M5";
        private const string RoleDistrPush_M3 = "URM_DP_DistrPush_RW_M3";
        private const string RoleFocusSKU_M5 = "URM_DP_FocusSKU_RW_M5";
        private const string RoleFocusSKU_M3 = "URM_DP_FocusSKU_RW_M3";
        private const string RoleFocusGoal_M5 = "URM_DP_Focus_Goal_RW_M5";
        private const string RoleFocusGoal_M3 = "URM_DP_Focus_Goal_RW_M3";
        private const string RoleKPIRating_M5 = "URM_DP_KPI_Rating_RW_M5";
        private const string RoleKPIRating_M3 = "URM_DP_KPI_Rating_RW_M3";
        private const string RoleGeneral_M5 = "URM_DP_General_RW_M5";
        private const string RoleGeneral_M3 = "URM_DP_General_RW_M3";

        private static Dictionary<MMRoleEnum, string> _mmRoles = null;
        private static List<string> _userRoles = null;
        private static Dictionary<MMRoleEnum, string> _accessibleRoles = null;

        #endregion

        #region Properties

        public static List<string> UserRoles {
            get {
                if (_userRoles == null) {
                    _userRoles = new List<string>();
                    List<RoleModel> lRoles = DataProvider.GetUserRoles();
                    foreach (RoleModel lRole in lRoles) {
                        _userRoles.Add(lRole.RoleName);
                    }
                }
                return _userRoles;
            }
        }

        public static Dictionary<MMRoleEnum, string> MMRoles {
            get {
                if (_mmRoles == null)
                    FillMMRolesDictionary();
                return _mmRoles;
            }
        }

        #endregion

        private static void FillMMRolesDictionary() {
            _mmRoles = new Dictionary<MMRoleEnum, string>();

            _mmRoles.Add(MMRoleEnum.SkuM5, RoleSKU_M5);
            _mmRoles.Add(MMRoleEnum.SkuM3, RoleSKU_M3);
            _mmRoles.Add(MMRoleEnum.DistrPushM5, RoleDistrPush_M5);
            _mmRoles.Add(MMRoleEnum.DistrPushM3, RoleDistrPush_M3);
            _mmRoles.Add(MMRoleEnum.FocusSkuM5, RoleFocusSKU_M5);
            _mmRoles.Add(MMRoleEnum.FocusSkuM3, RoleFocusSKU_M3);
            _mmRoles.Add(MMRoleEnum.FocusGoalM5, RoleFocusGoal_M5);
            _mmRoles.Add(MMRoleEnum.FocusGoalM3, RoleFocusGoal_M3);
            _mmRoles.Add(MMRoleEnum.KpiRatingM5, RoleKPIRating_M5);
            _mmRoles.Add(MMRoleEnum.KpiRatingM3, RoleKPIRating_M3);
            _mmRoles.Add(MMRoleEnum.GeneralM5, RoleGeneral_M5);
            _mmRoles.Add(MMRoleEnum.GeneralM3, RoleGeneral_M3);
        }

        /// <summary>
        /// Checkes whether the user has the roleEnum in question
        /// </summary>
        /// <param name="roleEnum">roleEnum to be checked</param>
        /// <returns><c>true</c> if the user has the roleEnum</returns>
        public static bool IsInRole(MMRoleEnum roleEnum) {
            if (_accessibleRoles == null) {
                _accessibleRoles = new Dictionary<MMRoleEnum, string>();
                foreach (KeyValuePair<MMRoleEnum, string> lMMRole in MMRoles) {
                    if (UserRoles.Contains(lMMRole.Value))
                        _accessibleRoles.Add(lMMRole.Key, lMMRole.Value);
                }
            }
            return _accessibleRoles.ContainsKey(roleEnum);
        }
    }

    /// <summary>
    /// Describes MM roleEnum set
    /// </summary>
    public enum MMRoleEnum {
        SkuM5,
        SkuM3,
        DistrPushM5,
        DistrPushM3,
        FocusSkuM5,
        FocusSkuM3,
        FocusGoalM5,
        FocusGoalM3,
        KpiRatingM5,
        KpiRatingM3,
        GeneralM5,
        GeneralM3
    }
}
