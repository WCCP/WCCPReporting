﻿namespace SoftServe.Reports.MorningMeeting.Utils
{
    public enum AutoTargetsKpis
    {
        Volume = 1,
        Percentage = 2
    }
}
