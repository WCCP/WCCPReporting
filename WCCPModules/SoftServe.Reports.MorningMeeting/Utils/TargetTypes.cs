﻿namespace SoftServe.Reports.MorningMeeting.Utils
{
    public enum TargetTypes
    {
        Manual = 1,
        Automatic = 2
    }
}
