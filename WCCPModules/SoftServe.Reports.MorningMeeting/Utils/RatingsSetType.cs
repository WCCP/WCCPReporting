﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MorningMeeting.Utils
{
    public enum RatingsSetType
    {
        [MapValue("1")] 
        ProductSet = 1,
        [MapValue("2")] 
        SkipChannelAndProductSet,
        [MapValue("3")] 
        AvlSkuSet,
        [MapValue("4")] 
        Network,
        [MapValue("5")] 
        Empty
    }
}
