﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Forms;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Utils
{
    /// <summary>
    /// Performs file uploading 
    /// </summary>
    internal class PlansUploader
    {
        public const int COLUMNS__FROM_FILE_COUNT = 5;

        #region Colunm Names

        private string colEmployeeId = "EmployeeId";
        private string colEmployeeName = "EmployeeName";
        private string colKpiName = "KpiName";
        private string colDate = "Date";
        private string colPlan = "Plan";

        private string colPageName = "PageName";
        private string colRowNumber = "RowNumber";
        private string colStatusDescr = "StatusDescription";

        #endregion

        private Dictionary<int, ImportedPlanModel> _validPlansProducts;
        private Dictionary<int, ImportedPlanModel> _validPlansGeneral;
        private Dictionary<int, ImportedPlanModel> _validPlansNetworks;

        private DataTable _invalidPlansTable;

        private Dictionary<int, string> _errorCodes;

        private List<string> _columnList;

        private bool HasIncorrectPlans
        {
            get { return _invalidPlansTable.Rows.Count > 0; }
        }

        public PlansUploader()
        {
            InitColumnList();
            InitInvalidPlanTable();
            InitErrorCodesList();

            _validPlansProducts = new Dictionary<int, ImportedPlanModel>();
            _validPlansGeneral = new Dictionary<int, ImportedPlanModel>();
            _validPlansNetworks = new Dictionary<int, ImportedPlanModel>();
        }

        public void UploadPlans()
        {
            string lFilePath = GetFileName();

            if (lFilePath == string.Empty) // file wasn't chosen
                return;

            WaitManager.StartWait();

            if (!ReadData(lFilePath))
            {
                WaitManager.StopWait();
                return;
            }

            ValidateData();

            WaitManager.StopWait();

            if (HasIncorrectPlans && !CheckForLoadingContinuation())
            {
                return;
            }

            if (_validPlansProducts.Count == 0 && _validPlansGeneral.Count == 0 && _validPlansNetworks.Count == 0)
            {
                XtraMessageBox.Show("Корректных планов не обнаружено", "Загрузка планов", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DataProvider.SaveLoadedPlans();

            XtraMessageBox.Show("Планы успешно загружены", "Загрузка планов", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void InitColumnList()
        {
            _columnList = new List<string>();
            _columnList.Add(colEmployeeId);
            _columnList.Add(colEmployeeName);
            _columnList.Add(colKpiName);
            _columnList.Add(colDate);
            _columnList.Add(colPlan);
            _columnList.Add(colPageName);
            _columnList.Add(colRowNumber);
            _columnList.Add(colStatusDescr);
        }

        private void InitErrorCodesList()
        {
            _errorCodes = new Dictionary<int, string>();

            _errorCodes.Add(1, "Не найден сотрудник");
            _errorCodes.Add(2, "Не совпадает название сотрудника");
            _errorCodes.Add(3, "Не найден KPI");
            _errorCodes.Add(4, "Нет прав для редактирования планов");
            _errorCodes.Add(5, "Некорректный формат данных");
            _errorCodes.Add(6, "Задвоение данных");
        }

        private bool ReadData(string filePath)
        {
            ExcelReader fileReader = new ExcelReader(filePath);
            bool wasReadingSuccsessful = true;

            try
            {
                fileReader.StartReading();
            }
            catch (IOException)
            {
                XtraMessageBox.Show("Файл уже открыт в другом приложении!", "Загрузка планов", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                wasReadingSuccsessful = false;

                return wasReadingSuccsessful;
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Некорректный формат файла! Нужно использовать xlsx формат", "Загрузка планов", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                wasReadingSuccsessful = false;

                return wasReadingSuccsessful;
            }
            finally
            {
                fileReader.FinishReading();
            }
            
            try
            {
                fileReader.ReadFileWorksheet(_validPlansProducts, _invalidPlansTable, COLUMNS__FROM_FILE_COUNT, 1);
                fileReader.ReadFileWorksheet(_validPlansGeneral, _invalidPlansTable, COLUMNS__FROM_FILE_COUNT, 2);
                fileReader.ReadFileWorksheet(_validPlansNetworks, _invalidPlansTable, COLUMNS__FROM_FILE_COUNT, 3);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Некорректный формат файла!", "Загрузка планов", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                wasReadingSuccsessful = false;
            }
            finally
            {
                fileReader.FinishReading();
            }

            return wasReadingSuccsessful;
        }

        private void ValidateData()
        {
            DataProvider.InitTempTables();

            DataProvider.SendProductPlansToDB(_validPlansProducts);
            DataProvider.SendGeneralPlansToDB(_validPlansGeneral);
            DataProvider.SendNetworksPlansToDB(_validPlansNetworks);

            List<InvalidPlanModel> incorrectPlans = DataProvider.GetIncorrectPlans();

            MergePlans(incorrectPlans);
        }

        private void MergePlans(List<InvalidPlanModel> incorrectPlans)
        {
            foreach (InvalidPlanModel incorrectPlan in incorrectPlans)
            {
                ImportedPlanModel plan = null;

                if (_validPlansProducts.ContainsKey(incorrectPlan.Id))
                {
                    plan = _validPlansProducts[incorrectPlan.Id];
                    _validPlansProducts.Remove(incorrectPlan.Id);
                }

                if (_validPlansGeneral.ContainsKey(incorrectPlan.Id))
                {
                    plan = _validPlansGeneral[incorrectPlan.Id];
                    _validPlansGeneral.Remove(incorrectPlan.Id);
                }

                if (_validPlansNetworks.ContainsKey(incorrectPlan.Id))
                {
                    plan = _validPlansNetworks[incorrectPlan.Id];
                    _validPlansNetworks.Remove(incorrectPlan.Id);
                }

                if (plan == null)
                    return;

                _invalidPlansTable.Rows.Add(plan.EmployeeId, plan.EmployeeName, plan.KpiName, plan.Date, plan.Plan,
                    plan.PageName, plan.RowNumber, _errorCodes[incorrectPlan.Status]);
            }
        }

        private string GetFileName()
        {
            string lFilePath = string.Empty;

            try
            {
                lFilePath = ChooseFile();
            }
            catch (IOException)
            {
                XtraMessageBox.Show("Файл уже открыт в другом приложении", "Предупреждение", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            return lFilePath;
        }

        private bool CheckForLoadingContinuation()
        {
            PlansWithErrorsDisplayForm plansWithErrorsForm = new PlansWithErrorsDisplayForm();
            plansWithErrorsForm.LoadData(_invalidPlansTable);

            DialogResult result = plansWithErrorsForm.ShowDialog();

            return result == DialogResult.Yes;
        }

        private string ChooseFile()
        {
            string lPath = string.Empty;

            string DIALOG_OPEN_FILE_FILTER = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            string DIALOG_OPEN_FILE = "Загрузить файл";

            OpenFileDialog lFileDialog = new OpenFileDialog
            {
                Filter = DIALOG_OPEN_FILE_FILTER,
                Title = DIALOG_OPEN_FILE,
                CheckFileExists = true,
                RestoreDirectory = true
            };

            if (lFileDialog.ShowDialog() == DialogResult.OK)
            {
                lPath = lFileDialog.FileName;
            }

            return lPath;
        }

        private void InitInvalidPlanTable()
        {
            _invalidPlansTable = new DataTable();

            foreach (string columnName in _columnList)
            {
                _invalidPlansTable.Columns.Add(columnName);
            }
        }
    }
}
