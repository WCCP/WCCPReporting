﻿using System;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace SoftServe.Reports.MorningMeeting.Utils {
    public static class LoggerHelper {
        private static Logger _logger = null;
        private const string FILE_PATTERN = "WCCPreporting{0}.log";
        
        public static Logger Logger {
            get { return GetLogger(); }
        }

        private static Logger GetLogger() {
            if (null != _logger)
                return _logger;
            // Step 1. Create configuration object 
            LoggingConfiguration lLogConfig = new LoggingConfiguration();

            // Step 2. Create targets and add them to the configuration 
            FileTarget lFileTarget = new FileTarget();
            lLogConfig.AddTarget("file", lFileTarget);

            // Step 3. Set target properties 
            lFileTarget.Layout = @"${date:format=yyyy\-MM\-dd\ HH\:mm\:ss} ${level}: ${message}";
            DateTime lDateTime = DateTime.Today;
            string lPath = AppDomain.CurrentDomain.BaseDirectory;
            string lFileName = string.Format(FILE_PATTERN, "");
            string lFullFileName = Path.Combine(lPath, lFileName);
            int lCounter = 1;
            while (File.Exists(lFullFileName)) {
                lFileName = string.Format(FILE_PATTERN, lCounter);
                lFullFileName = Path.Combine(lPath, lFileName);
                lCounter++;
            }
            lFileTarget.FileName = @"${basedir}/" + lFileName;

            // Step 4. Define rules
            LoggingRule lRule = new LoggingRule("*", lFileTarget);
            lRule.EnableLoggingForLevel(LogLevel.Info);
            lRule.EnableLoggingForLevel(LogLevel.Warn);
            lRule.EnableLoggingForLevel(LogLevel.Error);
            lRule.EnableLoggingForLevel(LogLevel.Debug);
            lLogConfig.LoggingRules.Add(lRule);

            // Step 5. Activate the configuration
            LogManager.Configuration = lLogConfig;

            // Example usage
            _logger = LogManager.GetLogger("LDBManager");
            return _logger;
        }

    }
}