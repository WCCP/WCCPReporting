﻿using System;

namespace SoftServe.Reports.MorningMeeting.Utils {
    public class ListItemChangeEventArgs : EventArgs {
        private readonly int _oldId;
        private readonly int _newId;

        public ListItemChangeEventArgs(int oldId, int newId) {
            _oldId = oldId;
            _newId = newId;
        }

        public int OldId { get { return _oldId; } }
        public int NewId { get { return _newId; } }
    }

    public delegate void ListItemChangeEventHandler(object sender, ListItemChangeEventArgs e);

    public class ListItemChangingEventArgs : EventArgs
    {
        private readonly int _oldId;
        private readonly int _newId;

        public ListItemChangingEventArgs(int oldId, int newId)
        {
            _oldId = oldId;
            _newId = newId;
            IsCanceled = false;
        }

        public int OldId { get { return _oldId; } }
        public int NewId { get { return _newId; } }
        public bool IsCanceled { get; set; }
    }

    public delegate void ListItemChangingEventHandler(object sender, ListItemChangingEventArgs e);

    public class DateChangeEventArgs : EventArgs {
        private readonly DateTime _oldDate;
        private readonly DateTime _newDate;

        public DateChangeEventArgs(DateTime oldDate, DateTime newDate) {
            _oldDate = oldDate;
            _newDate = newDate;
        }

        public DateTime OldDate { get { return _oldDate; } }
        public DateTime NewDate { get { return _newDate; } }
    }

    public delegate void DateChangeEventHandler(object sender, DateChangeEventArgs e);

    public class DateChangingEventArgs : EventArgs
    {
        private readonly DateTime _oldDate;
        private readonly DateTime _newDate;

        public DateChangingEventArgs(DateTime oldDate, DateTime newDate)
        {
            _oldDate = oldDate;
            _newDate = newDate;
            IsCanceled = false;
        }

        public DateTime OldDate { get { return _oldDate; } }
        public DateTime NewDate { get { return _newDate; } }
        public bool IsCanceled { get; set; }
    }

    public delegate void DateChangingEventHandler(object sender, DateChangingEventArgs e);

    public class ShowM1ChangeEventArgs : EventArgs
    {
        private readonly bool _oldDate;
        private readonly bool _newDate;

        public ShowM1ChangeEventArgs(bool oldChecked, bool newChecked)
        {
            _oldDate = oldChecked;
            _newDate = newChecked;
        }

        public bool OldValue { get { return _oldDate; } }
        public bool NewValue { get { return _newDate; } }
    }

    public delegate void ShowM1ChangeEventHandler(object sender, ShowM1ChangeEventArgs e);

    public class IntervalChangeEventArgs : EventArgs
    {
        private readonly DateTime _startDate;
        private readonly DateTime _endDate;

        public IntervalChangeEventArgs(DateTime startDate, DateTime endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
        }

        public DateTime StartDate { get { return _startDate; } }
        public DateTime EndDate { get { return _endDate; } }
    }

    public delegate void IntervalChangeEventHandler(object sender, IntervalChangeEventArgs e);

    public class IntervalChangingEventArgs : EventArgs
    {
        private readonly DateTime _startDate;
        private readonly DateTime _endDate;

        public IntervalChangingEventArgs(DateTime startDate, DateTime endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
            IsCanceled = false;
        }

        public DateTime StartDate { get { return _startDate; } }
        public DateTime EndDate { get { return _endDate; } }
        public bool IsCanceled { get; set; }
    }

    public delegate void IntervalChangingEventHandler(object sender, IntervalChangingEventArgs e);
}