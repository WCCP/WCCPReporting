﻿using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace SoftServe.Reports.MorningMeeting.Utils {
    public static class TreeUtils {
        public static void UpdateParentsCheckState(TreeListNode node) {
            TreeListNode lParentNode = node.ParentNode;
            if (lParentNode == null)
                return;
            //if (lParentNode.CheckState == CheckState.Checked)
            //    return;
            //if (lParentNode.CheckState == node.CheckState)
            //    return;

            lParentNode.CheckState = GetNodeCheckState(lParentNode);
            UpdateParentsCheckState(lParentNode);
        }

        public static CheckState GetNodeCheckState(TreeListNode node) {
            int lCheckedCount = 0;
            int lUncheckedCount = 0;

            foreach (TreeListNode lChildNode in node.Nodes) {
                if (lChildNode.CheckState == CheckState.Checked)
                    lCheckedCount++;
                if (lChildNode.CheckState == CheckState.Unchecked)
                    lUncheckedCount++;
            }

            CheckState lCheckState = CheckState.Indeterminate;
            if (lUncheckedCount == node.Nodes.Count)
                lCheckState = CheckState.Unchecked;
            else if (lCheckedCount == node.Nodes.Count)
                lCheckState = CheckState.Checked;

            return lCheckState;
        }

        public static void OnNodeCheckRecursive(TreeListNode node, bool isChecked, TreeListColumn colEditable) {
            if (node == null)
                return;
            //TODO:
            //if (Convert.ToBoolean(node.GetValue(colEditable)))
            node.Checked = isChecked;
            foreach (TreeListNode lChildNode in node.Nodes)
                OnNodeCheckRecursive(lChildNode, isChecked, colEditable);
        }
    }

    /// <summary>
    /// Expands checked nodes.
    /// </summary>
    public class ExpandCheckedNodesOperation : TreeListOperation {
        public override void Execute(TreeListNode node) {
            if (!node.Checked)
                return;
            TreeListNode lParentNode = node.ParentNode;
            while (lParentNode != null) {
                if (!lParentNode.Checked)
                    lParentNode.Expanded = true;
                lParentNode = lParentNode.ParentNode;
            }
        }
    }

    /// <summary>
    /// Set Checked to ids from list.
    /// </summary>
    public class UpdateNodesCheckStateOperation : TreeListOperation {
        private readonly TreeListColumn _columnId;
        private List<int> _idList;
        private List<TreeListNode> _checkedNodes;

        public UpdateNodesCheckStateOperation(TreeListColumn columnId, List<int> idList) {
            _columnId = columnId;
            _idList = idList;
            _checkedNodes = new List<TreeListNode>(_idList.Count);
        }

        public List<TreeListNode> CheckedNodes {
            get { return _checkedNodes; }
        }

        public override void Execute(TreeListNode node) {
            node.Checked = false;
            int lIdValue = (int) node.GetValue(_columnId);
            if (!_idList.Contains(lIdValue))
                return;
            node.Checked = true;
            _checkedNodes.Add(node);
        }
    }

    /// <summary>
    /// Gets comma separated list of Id for checked nodes.
    /// </summary>
    public class GetFinalCheckedNodesOperation : TreeListOperation {
        private const string DELIMITER_ID_LIST = ",";
        private string _selectedIdList = string.Empty;
        private readonly TreeListColumn _columnId;
        private int _count;
        private readonly bool _leafOnly;

        public GetFinalCheckedNodesOperation(TreeListColumn columnId, bool leafOnly) {
            _columnId = columnId;
            _leafOnly = leafOnly;
        }

        public int Count {
            get { return _count; }
        }

        public string SelectedIdList {
            get { return _selectedIdList.TrimEnd(DELIMITER_ID_LIST.ToCharArray()); }
        }

        public override void Execute(TreeListNode node) {
            if (node.CheckState != CheckState.Checked)
                return;
            if (_leafOnly) {
                if (node.HasChildren)
                    return;
            }
            else if (node.ParentNode != null)
                if (node.ParentNode.Checked)
                    return;
            _selectedIdList = string.Concat(_selectedIdList, node.GetValue(_columnId), DELIMITER_ID_LIST);
            _count++;
        }
    }
}