﻿namespace SoftServe.Reports.MorningMeeting.Utils
{
    /// <summary>
    /// Describes MM role set
    /// </summary>
    enum BindingToSkip
    {
        Unbinded = 0,
        AutoBinded = 1,
        ManuallyBinded = 2
     }
}
