﻿using System;

namespace SoftServe.Reports.MorningMeeting.Utils
{
    class TimeEx
    {
        public int Hours { get; set; }

        public int Minutes { get; set; }
        
        public TimeEx()
        {
            Hours = Minutes = 0;
        }

        public TimeEx(int value)
        {
            Hours = value / 60;
            Minutes = value % 60;
        }

        public TimeEx(string value)
            : this()
        {
            if (string.IsNullOrEmpty(value))
                return;

            string[] lTimeTokens = value.Split(new []{":"}, StringSplitOptions.RemoveEmptyEntries );

            if(lTimeTokens.Length == 0)
                return;

            if (lTimeTokens.Length == 1)
            {
                if (value.Trim().StartsWith(":"))// minutes only
                {
                    Hours = 0;
                    Minutes = int.Parse(lTimeTokens[0]);
                }
                else// hours only
                {
                    Hours = int.Parse(lTimeTokens[0]);
                    Minutes = 0;
                }

                return;
            }

            // hours, minutes, ...
            Hours = int.Parse(lTimeTokens[0]);
            Minutes = int.Parse(lTimeTokens[1]);
        }

        public int Value
        {
            get { return 60 * Hours + Minutes; }
        }

        public override string ToString()
        {
            return string.Format("{0}:{1:D2}", Hours, Minutes);
        }

        //public static implicit operator TimeEx(string value)
        //{
        //    return new TimeEx(value);
        //}
    }
}
