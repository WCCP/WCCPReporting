﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Utils
{
    public static class DataRepository
    {
        private static Dictionary<string, List<CapsSubtargetModel>> _capsSubtargetsByDate = new Dictionary<string, List<CapsSubtargetModel>>();

        private static List<CountryModel> _countryList;

        private static Dictionary<int, List<StaffTreeModel>> _staffByCountries = new Dictionary<int, List<StaffTreeModel>>();

        public static List<CapsSubtargetModel> GetCapsSubtargets(int countryId, DateTime date)
        {
            string key = GetCapsKey(countryId, date);

            if (!_capsSubtargetsByDate.ContainsKey(key))
            {
                _capsSubtargetsByDate.Add(key, DataProvider.GetCapsSubtargets(countryId, date));
            }

            return _capsSubtargetsByDate[key];
        }

        public static void SetCapsSubtargets(int countryId, DateTime date, List<CapsSubtargetModel> subtargets)
        {
            string key = GetCapsKey(countryId, date);

            if (!_capsSubtargetsByDate.ContainsKey(key))
            {
                return;
            }

            _capsSubtargetsByDate[key] = subtargets;
        }


        public static List<CountryModel> Countries
        {
            get
            {
                if (_countryList == null)
                {
                    _countryList = DataProvider.GetCountryList();
                }

                return _countryList;
            }
        }

        public static List<StaffTreeModel> GetStaffTreeList(int countryId, bool showAllM3)
        {
            if (!_staffByCountries.ContainsKey(countryId))
            {
                List<StaffTreeModel> unsortedStaff = DataProvider.GetStaffTreeList(countryId);
                unsortedStaff.Sort(CompareStaff);

                _staffByCountries.Add(countryId, unsortedStaff);
            }

            List<StaffTreeModel> staffList = new List<StaffTreeModel>(_staffByCountries[countryId]);

            if (!showAllM3)
            {
                StaffTreeModel allM3Item = staffList.FirstOrDefault(s => s.Id == -1);// Все М3
                staffList.Remove(allM3Item);
            }

            return staffList;
        }

        private static int CompareStaff(StaffTreeModel s1, StaffTreeModel s2)
        {
            if (s1.Id == -1 && s2.Id == -1)//  Все М3 should be the first
                return 0;

            if (s1.Id == -1)//  Все М3 should be the first
                return -1;

            if (s2.Id == -1)//  Все М3 should be the first
                return 1;

            return string.Compare(s1.Name, s2.Name);
        }

        private static string GetCapsKey(int countryId, DateTime date)
        {
            return countryId.ToString() + date.Year.ToString() + date.Month.ToString();
        }

        public static List<NameMappingModel> ProductColumns
        {
            get
            {
                List<NameMappingModel> list = new List<NameMappingModel>();
                list.Add(new NameMappingModel { NameDB = "Объем по продуктам, штук", NameUI = "Объемная цель, штук" });
                list.Add(new NameMappingModel { NameDB = "Объем по продуктам", NameUI = "Объемная цель" });
                list.Add(new NameMappingModel { NameDB = "Объем общий, штук", NameUI = "Объемная цель, штук Total" });
                list.Add(new NameMappingModel { NameDB = "Объем общий", NameUI = "Объемная цель Total" });
                list.Add(new NameMappingModel { NameDB = "АТТ по продуктам", NameUI = "Цели по АТТ" });
                list.Add(new NameMappingModel { NameDB = "АТТ общее", NameUI = "Цели по АТТ Total" });
                list.Add(new NameMappingModel { NameDB = "Покрытие по продуктам, штук", NameUI = "План по покрытию штук, %" });
                list.Add(new NameMappingModel { NameDB = "Покрытие по продуктам", NameUI = "План по покрытию, %" });
                list.Add(new NameMappingModel { NameDB = "Покрытие общее", NameUI = "План по покрытию Total" });
                list.Add(new NameMappingModel { NameDB = "Ср. СКЮ по продуктам", NameUI = "Среднее СКЮ" });
                list.Add(new NameMappingModel { NameDB = "Ср. СКЮ ДИСТРПУШ", NameUI = "Среднее СКЮ из Дистр пуша" });
                list.Add(new NameMappingModel { NameDB = "Сумма СКЮ ДИСТРПУШ", NameUI = "Сумма СКЮ Дистр пуш" });
                list.Add(new NameMappingModel { NameDB = "Ср. СКЮ общее", NameUI = "Среднее СКЮ Total" });
                list.Add(new NameMappingModel { NameDB = "Микс общий", NameUI = "Микс Total" });
                

                return list;
            }
        }

        public static List<NameMappingModel> ProductTotalColumns
        {
            get
            {
                List<NameMappingModel> list = new List<NameMappingModel>();
                list.Add(new NameMappingModel { NameDB = "Total Объем по продуктам - Total", NameUI = "Total Объемная цель Total" });
                list.Add(new NameMappingModel { NameDB = "Total АТТ по продуктам - Total", NameUI = "Total Цели по АТТ Total" });
                list.Add(new NameMappingModel { NameDB = "Total План по покрытию - Total", NameUI = "Total План по покрытию Total" });
                list.Add(new NameMappingModel { NameDB = "Total Среднее СКЮ - Total", NameUI = "Total Среднее СКЮ Total" });
                list.Add(new NameMappingModel { NameDB = "Total Микс - Total", NameUI = "Total Микс Total" });

                return list;
            }
        }

        public static List<NameMappingModel> NetworkColumns
        {
            get
            {
                List<NameMappingModel> list = new List<NameMappingModel>();
                list.Add(new NameMappingModel { NameDB = "Обьем по сетям", NameUI = "План на месяц, дал" });
                list.Add(new NameMappingModel { NameDB = "Среднее СКЮ", NameUI = "Ср. СКЮ План на месяц" });
                list.Add(new NameMappingModel { NameDB = "Микс", NameUI = "Микс План на месяц, %" });
                list.Add(new NameMappingModel { NameDB = "POCE, Базовые стандарты", NameUI = "POCE Базовые стандарты, План на месяц, %" });
                list.Add(new NameMappingModel { NameDB = "POCE, Продвинутые стандарты", NameUI = "POCE Продвинутые стандарты, План на месяц, %" });
                list.Add(new NameMappingModel { NameDB = "POCE, Лидерские стандарты", NameUI = "POCE Лидерские стандарты, План на месяц, %" });

                return list;
            }
        }

        public static List<NameMappingModel> GeneralColumns
        {
            get
            {
                List<NameMappingModel> list = new List<NameMappingModel>();
                /*list.Add(new NameMappingModel { NameDB = "ХО % эфф. оборудования", NameUI = "ХО: % эфф. оборудования" });
                list.Add(new NameMappingModel { NameDB = "ХО % ТТ с эфф. оборудованием", NameUI = "ХО: % ТТ с эфф. оборудованием" });
                list.Add(new NameMappingModel { NameDB = "ХО ТТ с нулевыми продажами", NameUI = "ХО: ТТ с нулевыми продажами" });
                list.Add(new NameMappingModel { NameDB = "ДО % эфф. оборудования", NameUI = "ДО: % эфф. оборудования" });
                list.Add(new NameMappingModel { NameDB = "ДО % ТТ с эфф. оборудованием", NameUI = "ДО: % ТТ с эфф. оборудованием" });
                list.Add(new NameMappingModel { NameDB = "ДО ТТ с нулевыми продажами", NameUI = "ДО: ТТ с нулевыми продажами" });*/

                list.Add(new NameMappingModel { NameDB = "РОСЕ"/*"Базовые стандарты"*/, NameUI = "РОСЕ"/* Базовые стандарты,  План на месяц, % "*/ });  // ---- Bushido_Gap_B2 101 2015.09.08
                list.Add(new NameMappingModel { NameDB = "Продвинутые стандарты", NameUI = "РОСЕ Продвинутые стандарты,  План на месяц, % " });
                list.Add(new NameMappingModel { NameDB = "Лидерские стандарты", NameUI = "РОСЕ Лидерские стандарты,  План на месяц, % " });

                return list;
            }
        }

        private static List<ListItemModel> _akbCalcMethodsList;
        private static List<ListItemModel> _srCalcMethodsList;

        public static List<ListItemModel> AkbCalcMethodsList
        {
            get
            {
                if (_akbCalcMethodsList == null)
                {
                    _akbCalcMethodsList = DataProvider.GetAkbCalcMethods();
                }

                return _akbCalcMethodsList;
            }
        }

        public static List<ListItemModel> SrCalcMethodsList
        {
            get
            {
                if (_srCalcMethodsList == null)
                {
                    _srCalcMethodsList = DataProvider.GetSrCalcMethods();
                }

                return _srCalcMethodsList;
            }
        }
    }
}