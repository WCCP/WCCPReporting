﻿using System.Collections.Generic;
using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Utils {
    public static class CreateHelper {
        private static List<ChannelModel> _channels;

        public static List<ChannelModel> GetChannels(bool reload = false) {
            if (reload || null == _channels) {
                _channels = DataProvider.GetChannelList();
                return _channels;
            }
            return _channels;
        }

        public static Dictionary<GridColumn, int> CreateChannelColumns(GridView gridView) {
            Dictionary<GridColumn, int> lColMasks = new Dictionary<GridColumn, int>();
            List<ChannelModel> lChannels = GetChannels();
             gridView.BeginUpdate();
             try {
                 for (int lI = 0; lI < lChannels.Count; lI++) {
                     string lFieldName = "colChannel" + lChannels[lI].Id;
                     GridColumn lColumn = CreateChannelColumn(gridView, lFieldName, lChannels[lI].Name);
                     lColumn.VisibleIndex = gridView.VisibleColumns.Count + lI;
                     lColMasks.Add(lColumn, lChannels[lI].Mask);
                 }
             }
             finally {
                 gridView.EndUpdate();
             }
             return lColMasks;
         }

        private static GridColumn CreateChannelColumn(GridView gridView, string fieldName, string caption)
        {
            GridColumn lColumn = gridView.Columns.AddField(fieldName);
            lColumn.UnboundType = UnboundColumnType.Boolean;
            lColumn.FieldName = fieldName;
            lColumn.Visible = true;
            lColumn.Width = 85;
            lColumn.MinWidth = 85;
            lColumn.MaxWidth = 85;
            lColumn.OptionsColumn.AllowEdit = true;
            lColumn.Caption = caption;
            //col.ColumnEdit = rCheckEdit;
            lColumn.OptionsFilter.AllowFilter = false;

            return lColumn;
        }

    }
}