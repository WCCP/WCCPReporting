﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftServe.Reports.MorningMeeting.DataAccess;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.Utils
{
    public class EditedPlansList
    {
        private List<PlanItemModel> _records { get; set; }

        public EditedPlansList()
        {
            _records = new List<PlanItemModel>();
        }

        public bool IsEmpty
        {
            get { return _records.Count == 0; }
        }

        public void Add(PlanItemModel record)
        {
            PlanItemModel item = _records.FirstOrDefault(r => r.ColumnId == record.ColumnId && r.EmployeeId == record.EmployeeId);

            if (item != null)
            {
                item.Value = record.Value;
            }
            else
            {
                _records.Add(record);
            }
        }

        public void Clear()
        {
            _records.Clear();
        }

        public void SaveChangedToDb()
        {
            foreach (PlanItemModel item in _records)
            {
                DataProvider.UpdatePlan(item);
            }
        }
    }
}
