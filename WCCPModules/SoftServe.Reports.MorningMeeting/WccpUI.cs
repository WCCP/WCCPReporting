﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;

namespace WccpReporting
{
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpMorningMeeting.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Shows the UI.
        /// </summary>
        /// <param name="isSkined">The is skined.</param>
        /// <param name="reportCaption">The report caption.</param>
        /// <returns></returns>
        public int ShowUI(int isSkined, string reportCaption)
        {
            try
            {

                _reportControl = new WccpUIControl();
                _reportCaption = reportCaption;

                return 0;
            }
            catch (Exception lException)
            {
                ErrorManager.ShowErrorBox(lException.Message);
            }

            return 0;
        }

        /// <summary>
        /// Closes the UI.
        /// </summary>
        public void CloseUI()
        { }

        public bool AllowClose()
        {
            //return false;
            if (ReportControl == null)
                return true;

            if (!_reportControl.HasUnsavedChanges())
                return true;

            DialogResult lDialogResult = XtraMessageBox.Show("Не все измения сохранены. Сохранить?", "Не все измения сохранены",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (lDialogResult == DialogResult.Yes)
            {
                _reportControl.OnCloseActions();
            }

            return true;
        }
    }
}