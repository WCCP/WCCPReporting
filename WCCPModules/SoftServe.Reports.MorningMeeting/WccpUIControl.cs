﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using NLog;
using SoftServe.Core.Common.View;
using SoftServe.Reports.MorningMeeting.Controller;
using SoftServe.Reports.MorningMeeting.Utils;
using SoftServe.Reports.MorningMeeting.View;

namespace WccpReporting {
    [ToolboxItem(false)]
    public partial class WccpUIControl : SimpleControlView {
        #region Fields

        private Logger _logger = LoggerHelper.Logger;
        private IProductGroupController _productGroupController;
        private ISKUListController _skuListController;
        private IGeneralSettingsController _generalSettingsController;
        private IFocusGoalsController _focusGoalsController;
        private IRatingsController _ratingsController;
        private IGeneralPlanController _generalPlanController;
        private INetworksPlanController _networksPlanController;
        private IProductsPlanController _productsPlanController;

        private IProductGroupView _productGroupView;
        private ISKUListView _skuListView;

        private Control _currentDockObject;

        #endregion

        public WccpUIControl() {
            if (!AreUserSettingsCorrect())
            return;

            InitializeComponent();
            Init();
            Localizer.Active = new RussianEditorsLocalizer();
        }

        private void Init() {
            //Need this because WccpUIControl instanciated from Delphi ManagedVCL library
            
            // Product Set
            try {
                if (_productGroupController == null) {
                    _productGroupController = new ProductGroupController(null);
                }
                _productGroupController.Start();
                _productGroupView = (IProductGroupView) _productGroupController.GetView();
                _productGroupView.StartTo(this);

                // SKU List
                if (_skuListController == null) {
                    _skuListController = new SKUListController(null);
                }
                _skuListController.Start();
                _skuListView = (ISKUListView) _skuListController.GetView();
                _skuListView.StartTo(this);

                // General Settings
                if (_generalSettingsController == null) {
                    _generalSettingsController = new GeneralSettingsController(_generalSettingsView);
                }
                _generalSettingsController.LoadData(true);

                // Focus Goals
                if (_focusGoalsController == null) {
                    _focusGoalsController = new FocusGoalsController(_focusGoalsView);
                }
                _focusGoalsController.LoadData();

                // Ratings
                if (_ratingsController == null) {
                    _ratingsController = new RatingsController(_ratingsView);
                }
                _ratingsController.LoadData();

                // General Plan
                if (_generalPlanController == null) {
                    _generalPlanController = new GeneralPlanController(_generalPlanView);
                }
                _generalPlanController.LoadData();

                // Networks Plan
                if (_networksPlanController == null) {
                    _networksPlanController = new NetworksPlanController(_networksPlanView);
                }
                _networksPlanController.LoadData();

                // Products Plan
                if (_productsPlanController == null) {
                    _productsPlanController = new ProductsPlanController(_productsPlanView);
                }
                _productsPlanController.LoadData();
            }
            catch (Exception lEx) {
                _logger.LogException(LogLevel.Error, " - ", lEx);
                MessageBox.Show(lEx.Message);
            }

            barBtnRefreshCapsBinding.Visibility = _productsPlanView.CanBindCapsSubtargets
                                                      ? BarItemVisibility.Always
                                                      : BarItemVisibility.Never;
        }

        private bool AreUserSettingsCorrect()
        {
            if (DataRepository.Countries.Count == 0)
            {
                XtraMessageBox.Show("Ошибка в настройках пользователя: нет привязки к стране", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (DataRepository.GetStaffTreeList(DataRepository.Countries[0].Id, false).Count == 0)
            {
                XtraMessageBox.Show(string.Format("Ошибка в настройках пользователя: нет привязки персонала по {0}", DataRepository.Countries[0].Name), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        public static void LogException(Exception exception) {
            LogMsgHeader("\t" + exception.GetType().FullName);
            LogMessage("  Data: " + exception.Data);
            LogMessage("  Message: " + exception.Message);
            LogMessage("  Source: " + exception.Source);
            LogMessage("  StackTrace: " + exception.StackTrace);
            LogMessage("\n");

            if (exception.InnerException == null)
                return;
            LogMessage("Inner exception:");
            LogException(exception.InnerException);
        }

        public static void LogMessage(string msg) {
            Console.WriteLine(msg);
        }

        public static void LogMsgHeader(string msg) {
            LogMessage(string.Format("{0} " + msg, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        }

        public override object GetDockObject(ISimpleControlView controlView) {
            //if (null == controlView)
            //TODO: raise exception
            GetNextDockControl();
            return _currentDockObject;
        }

        private void GetNextDockControl() {
            if (_currentDockObject == null) {
                _currentDockObject = TabPageProductSet;
                return;
            }
            if (_currentDockObject == TabPageProductSet) {
                _currentDockObject = TabPageSKUList;
                return;
            }
        }

        private void barBtnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            if (TabControl.SelectedTabPage == TabPageConfig){
                if (TabControlConfig.SelectedTabPage == TabPageProductSet)
                    _productGroupView.SaveAction();
                if (TabControlConfig.SelectedTabPage == TabPageSKUList)
                    _skuListView.SaveAction();
                if (TabControlConfig.SelectedTabPage == TabPageCommon)
                    _generalSettingsView.SaveActions();
                if (TabControlConfig.SelectedTabPage == TabPageFocusTarget)
                    _focusGoalsView.SaveActions();
                if (TabControlConfig.SelectedTabPage == TabPageRatings)
                    _ratingsView.SaveActions();
            }

            if (TabControl.SelectedTabPage == TabPagePlanProduct)
                _productsPlanView.SaveActions();
            if (TabControl.SelectedTabPage == TabPagePlanGeneral)
                _generalPlanView.SaveActions();
            if (TabControl.SelectedTabPage == TabPagePlanNetwork)
                _networksPlanView.SaveActions();
        }

        private void TabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e) {
            if (TabControl.SelectedTabPage == TabPageConfig) {
                barBtnRefreshCapsBinding.Visibility = BarItemVisibility.Never;
                barBtnImportFromXls.Visibility = BarItemVisibility.Never;
                return;
            }

            bool canBindCapsSubtargets = true;
            if (TabControl.SelectedTabPage == TabPagePlanProduct)
                canBindCapsSubtargets = _productsPlanView.CanBindCapsSubtargets;
            if (TabControl.SelectedTabPage == TabPagePlanGeneral)
                canBindCapsSubtargets = _generalPlanView.CanBindCapsSubtargets;
            if (TabControl.SelectedTabPage == TabPagePlanNetwork)
                canBindCapsSubtargets = _networksPlanView.CanBindCapsSubtargets;

            barBtnImportFromXls.Visibility = BarItemVisibility.Always;
            barBtnRefreshCapsBinding.Visibility = canBindCapsSubtargets ? BarItemVisibility.Always : BarItemVisibility.Never;
        }

        private void barBtnRefreshCapsBinding_ItemClick(object sender, ItemClickEventArgs e) {
            if (TabControl.SelectedTabPage == TabPageConfig)
                return;

            if (TabControl.SelectedTabPage == TabPagePlanProduct)
                _productsPlanView.HandleRefreshCapsBinding();
            if (TabControl.SelectedTabPage == TabPagePlanGeneral)
                _generalPlanView.HandleRefreshCapsBinding();
            if (TabControl.SelectedTabPage == TabPagePlanNetwork)
                _networksPlanView.HandleRefreshCapsBinding();
        }

        private void barBtnRefresh_ItemClick(object sender, ItemClickEventArgs e) {
            if (TabControl.SelectedTabPage == TabPageConfig){
                if (TabControlConfig.SelectedTabPage == TabPageProductSet)
                    _productGroupView.RefreshAction();
                if (TabControlConfig.SelectedTabPage == TabPageSKUList)
                    _skuListView.RefreshAction();
                if (TabControlConfig.SelectedTabPage == TabPageCommon)
                    _generalSettingsView.RefreshActions();
                if (TabControlConfig.SelectedTabPage == TabPageFocusTarget)
                    _focusGoalsView.RefreshActions();
                if (TabControlConfig.SelectedTabPage == TabPageRatings)
                    _ratingsView.RefreshActions();
            }

            if (TabControl.SelectedTabPage == TabPagePlanProduct)
                _productsPlanView.RefreshActions();
            if (TabControl.SelectedTabPage == TabPagePlanGeneral)
                _generalPlanView.RefreshActions();
            if (TabControl.SelectedTabPage == TabPagePlanNetwork)
                _networksPlanView.RefreshActions();
        }

        private void TabControl_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e) {
            if (e.PrevPage == TabPageConfig)
                e.Cancel = !HandleTabControlConfigLeave();
            if (e.PrevPage == TabPagePlanProduct)
                e.Cancel = !_productsPlanView.LeaveTabActions();
            if (e.PrevPage == TabPagePlanGeneral)
                e.Cancel = !_generalPlanView.LeaveTabActions();
            if (e.PrevPage == TabPagePlanNetwork)
                e.Cancel = !_networksPlanView.LeaveTabActions();
        }

        private void TabControlConfig_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e) {
            if (e.PrevPage == TabPageProductSet)
                e.Cancel = !_productGroupView.LeaveTabActions();
            if (e.PrevPage == TabPageSKUList)
                e.Cancel = !_skuListView.LeaveTabActions();
            if (e.PrevPage == TabPageCommon)
                e.Cancel = !_generalSettingsView.LeaveTabActions();
            if (e.PrevPage == TabPageFocusTarget)
                e.Cancel = !_focusGoalsView.LeaveTabActions();
            if (e.PrevPage == TabPageRatings)
                e.Cancel = !_ratingsView.LeaveTabActions();
        }

        private bool HandleTabControlConfigLeave() {
            bool lAllowLeaveTab = true;

            if (TabControlConfig.SelectedTabPage == TabPageProductSet)
                lAllowLeaveTab = _productGroupView.LeaveTabActions();
            if (TabControlConfig.SelectedTabPage == TabPageSKUList)
                lAllowLeaveTab = _skuListView.LeaveTabActions();
            if (TabControlConfig.SelectedTabPage == TabPageCommon)
                lAllowLeaveTab = _generalSettingsView.LeaveTabActions();
            if (TabControlConfig.SelectedTabPage == TabPageFocusTarget)
                lAllowLeaveTab = _focusGoalsView.LeaveTabActions();
            if (TabControlConfig.SelectedTabPage == TabPageRatings)
                lAllowLeaveTab = _ratingsView.LeaveTabActions();

            return lAllowLeaveTab;
        }

        public bool HasUnsavedChanges() {
            bool lResult = true;

            if (TabControlConfig.SelectedTabPage == TabPageProductSet) {
                _productGroupView.PostEditor();
                lResult = _productGroupController.HasUnsavedChanges;
            }
            if (TabControlConfig.SelectedTabPage == TabPageSKUList) {
                _skuListView.PostEditor();
                lResult = _skuListController.HasUnsavedChanges;
            }
            if (TabControlConfig.SelectedTabPage == TabPageCommon)
                lResult = _generalSettingsController.HasUnsavedChanges;
            if (TabControlConfig.SelectedTabPage == TabPageFocusTarget) {
                _focusGoalsView.PostEditor();
                lResult = _focusGoalsController.HasUnsavedChanges;
            }
            if (TabControlConfig.SelectedTabPage == TabPageRatings) {
                _ratingsView.PostEditor();
                lResult = _ratingsController.HasUnsavedChanges;
            }
            if (TabControl.SelectedTabPage == TabPagePlanProduct)
                lResult = _productsPlanController.HasUnsavedChanges;
            if (TabControl.SelectedTabPage == TabPagePlanGeneral)
                lResult = _generalPlanController.HasUnsavedChanges;
            if (TabControl.SelectedTabPage == TabPagePlanNetwork)
                lResult = _networksPlanController.HasUnsavedChanges;

            return lResult;
        }

        public void OnCloseActions() {
            if (TabControlConfig.SelectedTabPage == TabPageProductSet)
                _productGroupView.SaveAction();
            if (TabControlConfig.SelectedTabPage == TabPageSKUList)
                _skuListView.SaveAction();
            if (TabControlConfig.SelectedTabPage == TabPageCommon)
                _generalSettingsView.SaveActions();
            if (TabControlConfig.SelectedTabPage == TabPageFocusTarget)
                _focusGoalsView.SaveActions();
            if (TabControlConfig.SelectedTabPage == TabPageRatings)
                _ratingsView.SaveActions();
            if (TabControl.SelectedTabPage == TabPagePlanProduct)
                _productsPlanView.SaveActions();
            if (TabControl.SelectedTabPage == TabPagePlanGeneral)
                _generalPlanView.SaveActions();
            if (TabControl.SelectedTabPage == TabPagePlanNetwork)
                _networksPlanView.SaveActions();
        }

        private void barBtnImportFromXls_ItemClick(object sender, ItemClickEventArgs e) {
            if (TabControl.SelectedTabPage == TabPageConfig) {
                return;
            }

            if (TabControl.SelectedTabPage == TabPagePlanProduct)
                _productsPlanView.ImportPlansFromExcelFile();
            if (TabControl.SelectedTabPage == TabPagePlanGeneral)
                _generalPlanView.ImportPlansFromExcelFile();
            if (TabControl.SelectedTabPage == TabPagePlanNetwork)
                _networksPlanView.ImportPlansFromExcelFile();

            _productsPlanController.LoadData();
            _generalPlanController.LoadData();
            _networksPlanController.LoadData();
        }

        private void WccpUIControl_Resize(object sender, EventArgs e)
        {
            _focusGoalsView.OnResize();
            _skuListView.OnResize();
            _productGroupView.OnResize();
        }
    }
}