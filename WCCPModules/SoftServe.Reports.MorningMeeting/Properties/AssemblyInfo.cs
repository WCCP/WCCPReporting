﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SoftServe.Reports.MorningMeeting")]
[assembly: AssemblyDescription("Morning Meeting Plan")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SoftServe Inc.")]
[assembly: AssemblyProduct("WCCP Reporting")]
[assembly: AssemblyCopyright("Copyright (c) 2014 SoftServe Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6d318a1a-3f2e-4c94-97c1-1b4f2fc079d3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.64999")]
[assembly: AssemblyFileVersion("1.0.0.64999")]
