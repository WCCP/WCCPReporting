﻿using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;

namespace SoftServe.Reports.MorningMeeting.UserControl {
    public class StaffTree : TreeList {
        private const string ID_FIELD_NAME = "Id";
        private const string PARENT_ID_FIELD_NAME = "ParentId";
        private const string STAFF_NANE_FIELD_NAME = "Name";
        
        public StaffTree() {
            BorderStyle = BorderStyles.Simple;

            OptionsBehavior.Editable = false;
            RootValue = null;

            OptionsView.ShowCheckBoxes = false;
            OptionsView.ShowColumns = false;
            OptionsView.ShowIndicator = false;
            OptionsView.ShowHorzLines = false;
            OptionsView.ShowVertLines = false;

            //Data related
            KeyFieldName = ID_FIELD_NAME;
            ParentFieldName = PARENT_ID_FIELD_NAME;

            TreeListColumn colStaff = new TreeListColumn();
            colStaff.FieldName = STAFF_NANE_FIELD_NAME;
            colStaff.OptionsColumn.AllowEdit = false;
            colStaff.OptionsColumn.AllowMoveToCustomizationForm = false;
            colStaff.OptionsColumn.ReadOnly = true;
            colStaff.Visible = true;
            colStaff.VisibleIndex = 0;

            Columns.AddRange(new[] { colStaff });
        }
         
    }
}