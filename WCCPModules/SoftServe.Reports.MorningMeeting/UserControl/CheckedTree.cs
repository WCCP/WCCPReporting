﻿using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;

namespace SoftServe.Reports.MorningMeeting.UserControl {
    public class CheckedTree : TreeList {
        private const string ID_FIELD_NAME = "Id";
        private const string PARENT_ID_FIELD_NAME = "ParentId";
        private const string NAME_FIELD_NAME = "Name";

        private CheckNodesEnum _checkNodes = CheckNodesEnum.Independent;

        public CheckedTree() {
            BorderStyle = BorderStyles.Simple;

            OptionsBehavior.Editable = false;

            OptionsView.ShowCheckBoxes = true;
            OptionsView.ShowColumns = false;
            OptionsView.ShowIndicator = false;
            OptionsView.ShowHorzLines = false;
            OptionsView.ShowVertLines = false;

            //Data related
            KeyFieldName = ID_FIELD_NAME;
            ParentFieldName = PARENT_ID_FIELD_NAME;

            TreeListColumn lColumn = new TreeListColumn();
            lColumn.Name = "columnId";
            lColumn.FieldName = ID_FIELD_NAME;
            lColumn.OptionsColumn.AllowEdit = false;
            lColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            lColumn.OptionsColumn.ReadOnly = true;
            lColumn.Visible = false;
            Columns.AddRange(new[] { lColumn });

            lColumn = new TreeListColumn();
            lColumn.Name = "columnParentId";
            lColumn.FieldName = PARENT_ID_FIELD_NAME;
            lColumn.OptionsColumn.AllowEdit = false;
            lColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            lColumn.OptionsColumn.ReadOnly = true;
            lColumn.Visible = false;
            Columns.AddRange(new[] { lColumn });

            lColumn = new TreeListColumn();
            lColumn.Name = "columnName";
            lColumn.FieldName = NAME_FIELD_NAME;
            lColumn.OptionsColumn.AllowEdit = false;
            lColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            lColumn.OptionsColumn.ReadOnly = true;
            lColumn.Visible = true;
            lColumn.VisibleIndex = 0;

            Columns.AddRange(new[] { lColumn });
        }

        [DefaultValue(CheckNodesEnum.Independent)]
        public CheckNodesEnum CheckNodes {
            get { return _checkNodes; }
            set {
                if (_checkNodes == value)
                    return;
                ChangeCheckBehaviour(value);
            }
        }

        private void ChangeCheckBehaviour(CheckNodesEnum value) {
            _checkNodes = value;
        }

        protected override CheckNodeEventArgs RaiseBeforeCheckNode(TreeListNode node, CheckState prevState, CheckState state) {
            CheckNodeEventArgs lArgs = base.RaiseBeforeCheckNode(node, prevState, state);
            if (_checkNodes == CheckNodesEnum.LeafOnly)
                if (node.HasChildren)
                    lArgs.CanCheck = false;
            return lArgs;
        }
    }

    public enum CheckNodesEnum {
        Independent = 0,
        LeafOnly = 1
    }

}