﻿using System;
using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList;
using SoftServe.Reports.MorningMeeting.Model;

namespace SoftServe.Reports.MorningMeeting.UserControl {
    [ToolboxItem(true)]
    public class FilterControl : PanelControl {
        private LabelControl _labelListType;
        private LabelControl _labelDate;
        private LabelControl _labelCountry;
        private LabelControl _labelM3;
        private LabelControl _labelDateTo;
        private LabelControl _labelDateFrom;
        private LabelControl _labelInterval;

        private ComboBoxEdit _cbSKUListType;
        private MonthYearEdit _planDateEditEdit;
        private ComboBoxEdit _cbCountry;
        private PopupContainerEdit _pceStaffM3;
        private CheckEdit _chDisplayM1;

        private DateEdit _dateTo;
        private DateEdit _dateFrom;

        private PopupContainerControl _popupControl;
        private StaffTree _staffTree;
        private bool _showListType;
        private bool _showDisplayM1;
        private bool _showPlanDate;
        private bool _showDateInterval;
        private int _offset = 124;
        private int _intervalOffset = 256;

        public FilterControl() {
            InitControls();
            _showListType = true;
            _showDisplayM1 = false;
            _showPlanDate = true;
            _showDateInterval = false;
        }

        #region Properties

        public LabelControl LabelListType {
            get { return _labelListType; }
        }

        public ComboBoxEdit SKUListType {
            get { return _cbSKUListType; }
        }

        public DateEdit PlanDateEdit {
            get { return _planDateEditEdit; }
        }

        public DateEdit StartDateEdit {
            get { return _dateFrom; }
        }

        public DateEdit EndDateEdit {
            get { return _dateTo; }
        }

        public ComboBoxEdit CountryComboBox {
            get { return _cbCountry; }
        }

        public PopupContainerEdit StaffM3 {
            get { return _pceStaffM3; }
        }

        public StaffTree StaffTree {
            get { return _staffTree; }
        }

        public CheckEdit ShowM1CheckEdit {
            get { return _chDisplayM1; }
        }

        [DefaultValue(true)]
        public bool ShowListType {
            get { return _showListType; }
            set {
                if (_showListType == value)
                    return;
                _showListType = value;
                ChangeShowListType(value);
            }
        }

        [DefaultValue(false)]
        public bool ShowDisplayM1 {
            get { return _showDisplayM1; }
            set {
                if (_showDisplayM1 == value)
                    return;
                _showDisplayM1 = value;
                ChangeShowDisplayM3(value);
            }
        }

        [DefaultValue(true)]
        public bool ShowPlanDate {
            get { return _showPlanDate; }
            set {
                if (_showPlanDate == value)
                    return;
                _showPlanDate = value;
                ChangeShowPlanDate(value);
            }
        }

        [DefaultValue(false)]
        public bool ShowDateInterval {
            get { return _showDateInterval; }
            set {
                if (_showDateInterval == value)
                    return;
                _showDateInterval = value;
                ChangeShowDateInterval(value);
            }
        }

        #endregion

        private void ChangeShowListType(bool listType) {
            _labelListType.Enabled = listType;
            _cbSKUListType.Enabled = listType;
            _labelListType.Visible = listType;
            _cbSKUListType.Visible = listType;

            int lOffset = (listType) ? 0 : _offset;
            if (listType) {
                _labelListType.Location = new System.Drawing.Point(12, 12);
                _cbSKUListType.Location = new System.Drawing.Point(12, 28);
            }
            _labelDate.Location = new System.Drawing.Point(136 - lOffset, 12);
            _planDateEditEdit.Location = new System.Drawing.Point(136 - lOffset, 28);
            _labelCountry.Location = new System.Drawing.Point(268 - lOffset, 12);
            _cbCountry.Location = new System.Drawing.Point(268 - lOffset, 28);
            _labelM3.Location = new System.Drawing.Point(392 - lOffset, 12);
            _pceStaffM3.Location = new System.Drawing.Point(392 - lOffset, 28);
        }

        private void ChangeShowDisplayM3(bool showM3) {
            _chDisplayM1.Enabled = showM3;
            _chDisplayM1.Visible = showM3;
        }

        private void ChangeShowPlanDate(bool showPlanDate) {
            _labelDate.Visible = showPlanDate;

            _planDateEditEdit.Enabled = showPlanDate;
            _planDateEditEdit.Visible = showPlanDate;

            int lOffset = showPlanDate ? 0 : _intervalOffset;

            _labelCountry.Location = new System.Drawing.Point(268 - lOffset, 12);
            _labelM3.Location = new System.Drawing.Point(392 - lOffset, 12);

            _cbCountry.Location = new System.Drawing.Point(268 - lOffset, 28);
            _pceStaffM3.Location = new System.Drawing.Point(392 - lOffset, 28);
        }

        private void ChangeShowDateInterval(bool showDateInterval) {
            _labelInterval.Visible = showDateInterval;
            _labelDateFrom.Visible = showDateInterval;
            _labelDateTo.Visible = showDateInterval;

            _dateFrom.Enabled = showDateInterval;
            _dateFrom.Visible = showDateInterval;

            _dateTo.Enabled = showDateInterval;
            _dateTo.Visible = showDateInterval;
        }

        private void InitControls() {
            _labelListType = new LabelControl();
            _labelDate = new LabelControl();
            _labelCountry = new LabelControl();
            _labelM3 = new LabelControl();
            _cbSKUListType = new ComboBoxEdit();
            _planDateEditEdit = new MonthYearEdit();
            _cbCountry = new ComboBoxEdit();
            _pceStaffM3 = new PopupContainerEdit();
            _popupControl = new PopupContainerControl();
            _staffTree = new StaffTree();
            _chDisplayM1 = new CheckEdit();
            _labelDateTo = new LabelControl();
            _dateFrom = new DateEdit();
            _labelDateFrom = new LabelControl();
            _labelInterval = new LabelControl();
            _dateTo = new DateEdit();
            // begin init
            BeginInit();
            SuspendLayout();
            _cbSKUListType.Properties.BeginInit();
            _planDateEditEdit.Properties.BeginInit();
            _cbCountry.Properties.BeginInit();
            _pceStaffM3.Properties.BeginInit();
            _chDisplayM1.Properties.BeginInit();
            _dateFrom.Properties.VistaTimeProperties.BeginInit();
            _dateFrom.Properties.BeginInit();
            _dateTo.Properties.VistaTimeProperties.BeginInit();
            _dateTo.Properties.BeginInit();
            // add controls
            Controls.Add(_labelListType);
            Controls.Add(_labelDate);
            Controls.Add(_labelCountry);
            Controls.Add(_labelM3);
            Controls.Add(_cbSKUListType);
            Controls.Add(_planDateEditEdit);
            Controls.Add(_cbCountry);
            Controls.Add(_pceStaffM3);
            Controls.Add(_chDisplayM1);
            Controls.Add(_labelDateFrom);
            Controls.Add(_labelDateTo);
            Controls.Add(_labelInterval);
            Controls.Add(_dateFrom);
            Controls.Add(_dateTo);

            Dock = System.Windows.Forms.DockStyle.None;
            Size = new System.Drawing.Size(640, 64);
            // 
            // labelListType
            // 
            _labelListType.Location = new System.Drawing.Point(12, 12);
            _labelListType.Name = "labelListType";
            _labelListType.Size = new System.Drawing.Size(93, 13);
            _labelListType.TabIndex = 4;
            _labelListType.Text = "Набор продуктов:";
            // 
            // labelDate
            // 
            _labelDate.Location = new System.Drawing.Point(136, 12);
            _labelDate.Name = "labelDate";
            _labelDate.Size = new System.Drawing.Size(105, 13);
            _labelDate.TabIndex = 5;
            _labelDate.Text = "Дата планирования:";
            // 
            // labelCountry
            // 
            _labelCountry.Location = new System.Drawing.Point(268, 12);
            _labelCountry.Name = "labelCountry";
            _labelCountry.Size = new System.Drawing.Size(41, 13);
            _labelCountry.TabIndex = 6;
            _labelCountry.Text = "Страна:";
            // 
            // labelM3
            // 
            _labelM3.Location = new System.Drawing.Point(392, 12);
            _labelM3.Name = "labelM3";
            _labelM3.Size = new System.Drawing.Size(18, 13);
            _labelM3.TabIndex = 7;
            _labelM3.Text = "М3:";
            // 
            // cbSKUListType
            // 
            _cbSKUListType.Location = new System.Drawing.Point(12, 28);
            _cbSKUListType.Name = "cbSKUListType";
            _cbSKUListType.Properties.Buttons.AddRange(new[] {
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
            });
            _cbSKUListType.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            _cbSKUListType.Size = new System.Drawing.Size(104, 20);
            _cbSKUListType.TabIndex = 3;
            // 
            // pceStaffM3
            // 
            _pceStaffM3.Location = new System.Drawing.Point(392, 28);
            _pceStaffM3.Name = "pceStaffM3";
            _pceStaffM3.Properties.Buttons.AddRange(new[] {
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
            });
            _pceStaffM3.Size = new System.Drawing.Size(152, 20);
            _pceStaffM3.Properties.ShowPopupCloseButton = false;
            _pceStaffM3.TabIndex = 2;
            _popupControl.Controls.Add(_staffTree);
            _popupControl.Size = new System.Drawing.Size(152, 200);
            _staffTree.Dock = System.Windows.Forms.DockStyle.Fill;
            _pceStaffM3.Properties.PopupControl = _popupControl;
            _pceStaffM3.QueryDisplayText += pceStaffM3_QueryDisplayText;
            _pceStaffM3.QueryResultValue += _pceStaffM3_QueryResultValue;
            _staffTree.Click += StaffTreeOnClick;
            _staffTree.NodesReloaded += _staffTree_NodesReloaded;
            // 
            // cbCountry
            // 
            _cbCountry.Location = new System.Drawing.Point(268, 28);
            _cbCountry.Name = "cbCountry";
            _cbCountry.Properties.Buttons.AddRange(new[] {
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
            });
            _cbCountry.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            _cbCountry.Size = new System.Drawing.Size(108, 20);
            _cbCountry.TabIndex = 1;
            // 
            // dateEdit
            // 
            _planDateEditEdit.EditValue = new System.DateTime(2014, 3, 1, 0, 0, 0, 0);
            _planDateEditEdit.Location = new System.Drawing.Point(136, 28);
            _planDateEditEdit.Name = "dateEdit";
            _planDateEditEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            _planDateEditEdit.Properties.Buttons.AddRange(new[] {
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
            });
            _planDateEditEdit.Properties.VistaTimeProperties.Buttons.AddRange(new[] {
                new DevExpress.XtraEditors.Controls.EditorButton()
            });
            _planDateEditEdit.Size = new System.Drawing.Size(116, 20);
            _planDateEditEdit.TabIndex = 0;
            // 
            // chDisplayM3
            // 
            _chDisplayM1.Location = new System.Drawing.Point(500, 28);
            _chDisplayM1.Name = "_chDisplayM1";
            _chDisplayM1.Properties.Caption = "Показать М1";
            _chDisplayM1.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            _chDisplayM1.Size = new System.Drawing.Size(100, 20);
            _chDisplayM1.Visible = false;
            _chDisplayM1.TabIndex = 24;
            // 
            // lDateTo
            // 
            _labelDateTo.Location = new System.Drawing.Point(471, 30);
            _labelDateTo.Name = "lDateTo";
            _labelDateTo.Size = new System.Drawing.Size(12, 13);
            _labelDateTo.TabIndex = 10;
            _labelDateTo.Text = "по";
            _labelDateTo.Visible = false;
            // 
            // dFrom
            // 
            _dateFrom.EditValue = null;
            _dateFrom.Location = new System.Drawing.Point(364, 28);
            _dateFrom.Name = "dFrom";
            _dateFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            _dateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
            });
            _dateFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
                new DevExpress.XtraEditors.Controls.EditorButton()
            });
            _dateFrom.Size = new System.Drawing.Size(89, 20);
            _dateFrom.TabIndex = 6;
            _dateFrom.Visible = false;
            //this.dFrom.EditValueChanged += new System.EventHandler(this.date_EditValueChanged);
            // 
            // lDateFrom
            // 
            _labelDateFrom.Location = new System.Drawing.Point(353, 30);
            _labelDateFrom.Name = "lDateFrom";
            _labelDateFrom.Size = new System.Drawing.Size(5, 13);
            _labelDateFrom.TabIndex = 9;
            _labelDateFrom.Text = "c";
            _labelDateFrom.Visible = false;
            // 
            // lInterval
            // 
            _labelInterval.Location = new System.Drawing.Point(344, 12);
            _labelInterval.Name = "lInterval";
            _labelInterval.Size = new System.Drawing.Size(53, 13);
            _labelInterval.TabIndex = 8;
            _labelInterval.Text = "Интервал:";
            _labelInterval.Visible = false;
            // 
            // dTo
            // 
            _dateTo.EditValue = null;
            _dateTo.Location = new System.Drawing.Point(489, 28);
            _dateTo.Name = "dTo";
            _dateTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            _dateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
            });
            _dateTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
                new DevExpress.XtraEditors.Controls.EditorButton()
            });
            _dateTo.Size = new System.Drawing.Size(86, 20);
            _dateTo.TabIndex = 7;
            _dateTo.Visible = false;
            //this.dTo.EditValueChanged += new System.EventHandler(this.date_EditValueChanged);

            // end init
            _cbSKUListType.Properties.EndInit();
            _planDateEditEdit.Properties.EndInit();
            _cbCountry.Properties.EndInit();
            _pceStaffM3.Properties.EndInit();
            _chDisplayM1.Properties.EndInit();
            _dateFrom.Properties.VistaTimeProperties.EndInit();
            _dateFrom.Properties.EndInit();
            _dateTo.Properties.VistaTimeProperties.EndInit();
            _dateTo.Properties.EndInit();

            EndInit();
            ResumeLayout(false);
            PerformLayout();
        }
        
        private void StaffTreeOnClick(object sender, EventArgs eventArgs) {
            if (!IsM3Selected) {
                return;
            }

            TreeList lStaffTree = sender as TreeList;
            TreeListHitInfo info = lStaffTree.CalcHitInfo(lStaffTree.PointToClient(MousePosition));

            if (info.HitInfoType == HitInfoType.Cell) {
                _pceStaffM3.ClosePopup();
            }
        }

        private void pceStaffM3_QueryDisplayText(object sender, QueryDisplayTextEventArgs e) {
            StaffTreeModel lEditValue = e.EditValue as StaffTreeModel;

            if (lEditValue != null) {
                e.DisplayText = lEditValue.Name;
                }
            else {
                e.DisplayText = string.Empty;
            }
        }

        private void _pceStaffM3_QueryResultValue(object sender, QueryResultValueEventArgs e) {
            if (!IsM3Selected) {
                return;
            }

            e.Value = _staffTree.GetDataRecordByNode(_staffTree.FocusedNode);
        }

        private bool IsM3Selected {
            get {
                return _staffTree.FocusedNode != null &&
                       (_staffTree.FocusedNode.Level != 0 || Convert.ToInt32(_staffTree.FocusedNode[_staffTree.KeyFieldName]) == -1);
            }
        }

        void _staffTree_NodesReloaded(object sender, EventArgs e)
        {
            _staffTree.CollapseAll();
            if (_staffTree.Nodes.Count > 0 && _staffTree.Nodes[0] != null && _staffTree.Nodes[0].HasChildren)
            {
                _staffTree.Nodes[0].ExpandAll();
                _staffTree.SetFocusedNode(_staffTree.Nodes[0].FirstNode);
            }
        }
    }
}