﻿namespace SoftServe.Reports.MorningMeeting.Forms
{
    partial class BindingToCapsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bOK = new DevExpress.XtraEditors.SimpleButton();
            this.bCancel = new DevExpress.XtraEditors.SimpleButton();
            this.cCapsGoal = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lCapsGoal = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.cCapsGoal.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(115, 87);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(75, 23);
            this.bOK.TabIndex = 0;
            this.bOK.Text = "Привязать";
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(196, 87);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "Отмена";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // cCapsGoal
            // 
            this.cCapsGoal.Location = new System.Drawing.Point(11, 36);
            this.cCapsGoal.Name = "cCapsGoal";
            this.cCapsGoal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cCapsGoal.Properties.DropDownRows = 25;
            this.cCapsGoal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cCapsGoal.Size = new System.Drawing.Size(260, 20);
            this.cCapsGoal.TabIndex = 3;
            // 
            // lCapsGoal
            // 
            this.lCapsGoal.Location = new System.Drawing.Point(14, 20);
            this.lCapsGoal.Name = "lCapsGoal";
            this.lCapsGoal.Size = new System.Drawing.Size(75, 13);
            this.lCapsGoal.TabIndex = 4;
            this.lCapsGoal.Text = "Цель из СКИП:";
            // 
            // BindingToCapsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(284, 122);
            this.Controls.Add(this.lCapsGoal);
            this.Controls.Add(this.cCapsGoal);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BindingToCapsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Привязка к целям из СКиП";
            ((System.ComponentModel.ISupportInitialize)(this.cCapsGoal.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton bOK;
        private DevExpress.XtraEditors.SimpleButton bCancel;
        private DevExpress.XtraEditors.ComboBoxEdit cCapsGoal;
        private DevExpress.XtraEditors.LabelControl lCapsGoal;
    }
}