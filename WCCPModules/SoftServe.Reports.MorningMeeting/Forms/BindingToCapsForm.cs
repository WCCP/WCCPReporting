﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SoftServe.Reports.MorningMeeting.Model;
using SoftServe.Reports.MorningMeeting.Utils;

namespace SoftServe.Reports.MorningMeeting.Forms
{
    public partial class BindingToCapsForm : XtraForm
    {
        protected List<CapsSubtargetModel> _capsSubtargets;

        private long? _lastBindedTargetId;
        private int _currentCountry;
        private DateTime _currentDate;

        public long? BindedTargetId
        {
            get { return _lastBindedTargetId; }
        }

        public BindingToCapsForm()
        {
            InitializeComponent();
        }

        public void LoadData(int countryId, int m3Id, DateTime date)
        {
            _currentCountry = countryId;
            _currentDate = date;

            RefreshSubtargetList();
        }

        public void ClearBinding(long subtargetId)
        {
            CapsSubtargetModel lSubtarget = GetSubtarget(subtargetId);

            if(lSubtarget != null)
            {
                lSubtarget.IsAlreadyBinded = false;
            }
        }

        public DialogResult StartDialog()
        {
            RefreshSubtargetList();
            DialogResult lResult = ShowDialog();
            
            return lResult;
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            _lastBindedTargetId = null;

            Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cCapsGoal.Text))
            {
                XtraMessageBox.Show("Нужно выбрать цель", "Выбор цели", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
            if (CurrentSubtarget != null)
            {
                CurrentSubtarget.IsAlreadyBinded = true;
                _lastBindedTargetId = CurrentSubtarget.Id;

                DataRepository.SetCapsSubtargets(_currentCountry, _currentDate, _capsSubtargets);
            }

            DialogResult = DialogResult.OK;
        }

        private void RefreshSubtargetList()
        {
            _capsSubtargets = DataRepository.GetCapsSubtargets(_currentCountry, _currentDate);

            cCapsGoal.EditValue = null;

            cCapsGoal.Properties.Items.Clear();
            cCapsGoal.Properties.Items.AddRange(_capsSubtargets.Where(t => !t.IsAlreadyBinded).ToList());

        }

        private CapsSubtargetModel CurrentSubtarget
        {
            get { return _capsSubtargets.FirstOrDefault(t => t == (cCapsGoal.EditValue as CapsSubtargetModel)); }
        }

        private CapsSubtargetModel GetSubtarget(long subtargetId)
        {
            return _capsSubtargets.FirstOrDefault(t => t.Id == subtargetId);
        }
    }
}
