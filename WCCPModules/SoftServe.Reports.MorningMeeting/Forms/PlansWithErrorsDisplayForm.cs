using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.MorningMeeting.Forms
{
    public partial class PlansWithErrorsDisplayForm : XtraForm
    {

        /// <summary>
        /// Create the new instance of PlansWithErrorsDisplayForm object
        /// </summary>
        public PlansWithErrorsDisplayForm()
        {
            InitializeComponent();
        }

        public void LoadData(DataTable table)
        {
            ChangeDateFormat(table);

            gridControl.DataSource = table;

            gcStatus.Text = string.Format("������: ���������� ��������� ��������� ������ ({0})", table.Rows.Count);
        }

        private void ChangeDateFormat(DataTable table)
        {
            foreach (DataRow row in table.Rows)
            {
                string lDate = string.Empty;

                try
                {
                    DateTime lRowDate = Convert.ToDateTime(row[colDate.FieldName]);
                    lDate = lRowDate.Date.ToString("dd.MM.yy");
                }
                catch (Exception)
                {
                    lDate = row[colDate.FieldName] == DBNull.Value || row[colDate.FieldName] == null ? string.Empty : row[colDate.FieldName].ToString();
                }

                row[colDate.FieldName] = lDate;
            }
        }

        /// <summary>
        /// Finishes the dialog with OK result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }

        /// <summary>
        /// Finishes the dialog with Cancel result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}