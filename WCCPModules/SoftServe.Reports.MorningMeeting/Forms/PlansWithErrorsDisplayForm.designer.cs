namespace SoftServe.Reports.MorningMeeting.Forms
{
    partial class PlansWithErrorsDisplayForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.gcStatus = new DevExpress.XtraEditors.GroupControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.plansGridView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.bandFileParams = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTabName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRowNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDecription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandData = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colEmployeeId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colEmployeeName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colKpiName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.simpleOK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).BeginInit();
            this.gcStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plansGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // gcStatus
            // 
            this.gcStatus.Controls.Add(this.gridControl);
            this.gcStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcStatus.Location = new System.Drawing.Point(10, 10);
            this.gcStatus.Name = "gcStatus";
            this.gcStatus.Size = new System.Drawing.Size(1061, 354);
            this.gcStatus.TabIndex = 13;
            this.gcStatus.Text = "������: ";
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(2, 22);
            this.gridControl.MainView = this.plansGridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(1057, 330);
            this.gridControl.TabIndex = 3;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.plansGridView});
            // 
            // plansGridView
            // 
            this.plansGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.plansGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.plansGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.plansGridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandFileParams,
            this.bandData});
            this.plansGridView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colKpiName,
            this.colDate,
            this.colPlan,
            this.colTabName,
            this.colRowNumber,
            this.colDecription,
            this.colEmployeeId,
            this.colEmployeeName});
            this.plansGridView.CustomizationFormBounds = new System.Drawing.Rectangle(1688, 742, 223, 215);
            this.plansGridView.GridControl = this.gridControl;
            this.plansGridView.Name = "plansGridView";
            this.plansGridView.OptionsBehavior.Editable = false;
            this.plansGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.plansGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.plansGridView.OptionsSelection.MultiSelect = true;
            this.plansGridView.OptionsView.ColumnAutoWidth = false;
            this.plansGridView.OptionsView.ShowAutoFilterRow = true;
            this.plansGridView.OptionsView.ShowGroupPanel = false;
            this.plansGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTabName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRowNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // bandFileParams
            // 
            this.bandFileParams.AppearanceHeader.Options.UseTextOptions = true;
            this.bandFileParams.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandFileParams.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandFileParams.Caption = "��������������";
            this.bandFileParams.Columns.Add(this.colTabName);
            this.bandFileParams.Columns.Add(this.colRowNumber);
            this.bandFileParams.Columns.Add(this.colDecription);
            this.bandFileParams.MinWidth = 20;
            this.bandFileParams.Name = "bandFileParams";
            this.bandFileParams.Width = 385;
            // 
            // colTabName
            // 
            this.colTabName.Caption = "���������";
            this.colTabName.FieldName = "PageName";
            this.colTabName.Name = "colTabName";
            this.colTabName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTabName.Visible = true;
            this.colTabName.Width = 91;
            // 
            // colRowNumber
            // 
            this.colRowNumber.Caption = "������";
            this.colRowNumber.FieldName = "RowNumber";
            this.colRowNumber.Name = "colRowNumber";
            this.colRowNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRowNumber.Visible = true;
            this.colRowNumber.Width = 69;
            // 
            // colDecription
            // 
            this.colDecription.Caption = "�������";
            this.colDecription.FieldName = "StatusDescription";
            this.colDecription.Name = "colDecription";
            this.colDecription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDecription.Visible = true;
            this.colDecription.Width = 225;
            // 
            // bandData
            // 
            this.bandData.AppearanceHeader.Options.UseTextOptions = true;
            this.bandData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandData.Caption = "������ �� �����";
            this.bandData.Columns.Add(this.colEmployeeId);
            this.bandData.Columns.Add(this.colEmployeeName);
            this.bandData.Columns.Add(this.colKpiName);
            this.bandData.Columns.Add(this.colDate);
            this.bandData.Columns.Add(this.colPlan);
            this.bandData.MinWidth = 20;
            this.bandData.Name = "bandData";
            this.bandData.Width = 651;
            // 
            // colEmployeeId
            // 
            this.colEmployeeId.Caption = "ID ����������";
            this.colEmployeeId.FieldName = "EmployeeId";
            this.colEmployeeId.Name = "colEmployeeId";
            this.colEmployeeId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEmployeeId.Visible = true;
            this.colEmployeeId.Width = 101;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Caption = "���������";
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEmployeeName.Visible = true;
            this.colEmployeeName.Width = 164;
            // 
            // colKpiName
            // 
            this.colKpiName.Caption = "�������� KPI";
            this.colKpiName.FieldName = "KpiName";
            this.colKpiName.Name = "colKpiName";
            this.colKpiName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colKpiName.Visible = true;
            this.colKpiName.Width = 226;
            // 
            // colDate
            // 
            this.colDate.Caption = "����";
            this.colDate.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.colDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colDate.FieldName = "Date";
            this.colDate.Name = "colDate";
            this.colDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDate.Visible = true;
            this.colDate.Width = 80;
            // 
            // colPlan
            // 
            this.colPlan.Caption = "����";
            this.colPlan.FieldName = "Plan";
            this.colPlan.Name = "colPlan";
            this.colPlan.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPlan.Visible = true;
            this.colPlan.Width = 80;
            // 
            // simpleOK
            // 
            this.simpleOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleOK.Location = new System.Drawing.Point(852, 373);
            this.simpleOK.Name = "simpleOK";
            this.simpleOK.Size = new System.Drawing.Size(136, 23);
            this.simpleOK.TabIndex = 14;
            this.simpleOK.Text = "��� ����� ���������";
            this.simpleOK.Click += new System.EventHandler(this.simpleOK_Click);
            // 
            // simpleCancel
            // 
            this.simpleCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleCancel.Location = new System.Drawing.Point(994, 373);
            this.simpleCancel.Name = "simpleCancel";
            this.simpleCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleCancel.TabIndex = 15;
            this.simpleCancel.Text = "������";
            this.simpleCancel.Click += new System.EventHandler(this.simpleCancel_Click);
            // 
            // PlansWithErrorsDisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 404);
            this.Controls.Add(this.simpleCancel);
            this.Controls.Add(this.simpleOK);
            this.Controls.Add(this.gcStatus);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(644, 370);
            this.Name = "PlansWithErrorsDisplayForm";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 40);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������� ������";
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).EndInit();
            this.gcStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plansGridView)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl gcStatus;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraEditors.SimpleButton simpleOK;
    private DevExpress.XtraEditors.SimpleButton simpleCancel;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView plansGridView;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandFileParams;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTabName;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRowNumber;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDecription;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandData;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEmployeeId;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEmployeeName;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colKpiName;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDate;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlan;



  }
}