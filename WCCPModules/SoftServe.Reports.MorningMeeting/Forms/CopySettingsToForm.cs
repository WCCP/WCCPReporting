﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;

namespace SoftServe.Reports.MorningMeeting.Forms
{
    public partial class CopySettingsToForm : Form
    {
        public CopySettingsToForm()
        {
            InitializeComponent();
        }

        public bool ListType { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo
        {
            get { return dateTo.DateTime; }
        }

        public int StaffId { get; set; }

        public object StaffDataSource
        {
            get { return staffTree.DataSource; }
            set
            {
                staffTree.ClearNodes();
                staffTree.DataSource = value;
                staffTree.ExpandAll();
                staffTree.CollapseAll();
            }
        }

        public List<string> SelectedIds
        {
            get { return GetCheckedIds(); }
        }

        public void LoadDefaultSettings()
        {
            if (StaffId == -1)
                ShowFormForCountryLevel();
            else
                ShowFormForM3();
        }

        private void ShowFormForM3()
        {
            dateTo.Properties.MinValue = DateFrom;
            dateTo.DateTime = dateTo.Properties.MinValue;
        }

        private void ShowFormForCountryLevel()
        {
            DateTime lNextMonth = DateFrom.AddMonths(1);
            dateTo.Properties.MinValue = lNextMonth;
            dateTo.DateTime = dateTo.Properties.MinValue;

            CheckAllM3();
            staffTree.Enabled = false;
        }

        private List<string> GetCheckedIds()
        {
            List<string> res = new List<string>();

            if (StaffId == -1)
            {
                res.Add("-1");
                return res;
            }

            foreach (TreeListNode m4 in staffTree.Nodes)
            {
                foreach (TreeListNode m3 in m4.Nodes)
                {
                    if (!IsThisM3Disabled(m3) && m3.Checked)
                    {
                        res.Add(m3["Id"].ToString());
                    }
                }
            }

            return res;
        }

        private void CheckAllM3()
        {
            foreach (TreeListNode m4 in staffTree.Nodes)
            {
                if (Convert.ToInt32(m4["Id"]) == -1)
                {
                    m4.Checked = true;
                }
            }
        }

        private void UnCheckedCurM3()
        {
            foreach (TreeListNode m4 in staffTree.Nodes)
            {
                foreach (TreeListNode m3 in m4.Nodes)
                {
                    if (IsThisM3Disabled(m3))
                    {
                        m3.Checked = false;
                    }
                }
            }
        }

        private void staffTree_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            if (IsThisM3Disabled(e.Node))
            {
                e.CanCheck = false;
                return;
            }

            if (e.PrevState == CheckState.Unchecked)
            {
                e.State = CheckState.Checked;
            }

            if (e.PrevState == CheckState.Checked)
            {
                e.State = CheckState.Unchecked;
            }
        }

        private void staffTree_CustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e)
        {
            if (IsThisM3Disabled(e.Node))
            {
                e.Appearance.ForeColor = Color.Gray;
            }
        }

        private void staffTree_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            if (e.Node.HasChildren)
            {
                staffTree.BeginUpdate();
                CheckChildren(e.Node, e.Node.CheckState);
                staffTree.EndUpdate();
            }

            if (e.Node.ParentNode != null)
            {
                CheckParents(e.Node);
            }
        }

        private void CheckChildren(TreeListNode node, CheckState state)
        {
            node.CheckState = state;

            foreach (TreeListNode child in node.Nodes)
            {
                if (!IsThisM3Disabled(child))
                    CheckChildren(child, state);
            }
        }

        private void CheckParents(TreeListNode node)
        {
            TreeListNode lParentNode = node.ParentNode;

            if (lParentNode == null)
                return;

            if (lParentNode.CheckState == node.CheckState)
                return;

            lParentNode.CheckState = DefineNodeState(lParentNode);

            CheckParents(lParentNode);
        }

        private CheckState DefineNodeState(TreeListNode node)
        {
            CheckState lCheckState = CheckState.Indeterminate;

            int checkedCount = 0;
            int uncheckedCount = 0;

            foreach (TreeListNode child in node.Nodes)
            {
                if (child.CheckState == CheckState.Checked || IsThisM3Disabled(child))// to take cur m3 into account
                {
                    checkedCount++;
                    continue;
                }

                if (child.CheckState == CheckState.Unchecked)
                {
                    uncheckedCount++;
                }
            }

            if (checkedCount == 0 && uncheckedCount == node.Nodes.Count)
            {
                lCheckState = CheckState.Unchecked;
            }
            else
            {
                if (uncheckedCount == 0 && checkedCount == node.Nodes.Count)
                {
                    lCheckState = CheckState.Checked;
                }
            }

            return lCheckState;

        }

        private bool IsThisM3Disabled(TreeListNode node)
        {
            return Convert.ToInt32(node["Id"]) == StaffId && dateTo.DateTime == DateFrom;
        }

        private void dateTo_EditValueChanged(object sender, EventArgs e)
        {
            //undcheck current M3 in staffTree for curr month 
            if (DateTo == DateFrom)
            {
                UnCheckedCurM3();
            }

            staffTree.Refresh();
        }
    }
}
