﻿using SoftServe.Reports.MorningMeeting.View;

namespace WccpReporting {
    partial class WccpUIControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.TabControl = new DevExpress.XtraTab.XtraTabControl();
            this.TabPagePlanProduct = new DevExpress.XtraTab.XtraTabPage();
            this._productsPlanView = new SoftServe.Reports.MorningMeeting.View.ProductsPlanView();
            this.TabPagePlanGeneral = new DevExpress.XtraTab.XtraTabPage();
            this._generalPlanView = new SoftServe.Reports.MorningMeeting.View.GeneralPlanView();
            this.TabPagePlanNetwork = new DevExpress.XtraTab.XtraTabPage();
            this._networksPlanView = new SoftServe.Reports.MorningMeeting.View.NetworksPlanView();
            this.TabPageConfig = new DevExpress.XtraTab.XtraTabPage();
            this.TabControlConfig = new DevExpress.XtraTab.XtraTabControl();
            this.TabPageCommon = new DevExpress.XtraTab.XtraTabPage();
            this._generalSettingsView = new SoftServe.Reports.MorningMeeting.View.GeneralSettingsView();
            this.TabPageProductSet = new DevExpress.XtraTab.XtraTabPage();
            this.TabPageSKUList = new DevExpress.XtraTab.XtraTabPage();
            this.TabPageFocusTarget = new DevExpress.XtraTab.XtraTabPage();
            this._focusGoalsView = new SoftServe.Reports.MorningMeeting.View.FocusGoalsView();
            this.TabPageRatings = new DevExpress.XtraTab.XtraTabPage();
            this._ratingsView = new SoftServe.Reports.MorningMeeting.View.RatingsView();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barMain = new DevExpress.XtraBars.Bar();
            this.barBtnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barBtnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barBtnImportFromXls = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barBtnRefreshCapsBinding = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barMainMenu = new DevExpress.XtraBars.Bar();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imgCollection = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.TabPagePlanProduct.SuspendLayout();
            this.TabPagePlanGeneral.SuspendLayout();
            this.TabPagePlanNetwork.SuspendLayout();
            this.TabPageConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlConfig)).BeginInit();
            this.TabControlConfig.SuspendLayout();
            this.TabPageCommon.SuspendLayout();
            this.TabPageFocusTarget.SuspendLayout();
            this.TabPageRatings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.TabControl.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.TabControl.Location = new System.Drawing.Point(0, 59);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedTabPage = this.TabPagePlanProduct;
            this.TabControl.Size = new System.Drawing.Size(1100, 519);
            this.TabControl.TabIndex = 0;
            this.TabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TabPagePlanProduct,
            this.TabPagePlanGeneral,
            this.TabPagePlanNetwork,
            this.TabPageConfig});
            this.TabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.TabControl_SelectedPageChanged);
            this.TabControl.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.TabControl_SelectedPageChanging);
            // 
            // TabPagePlanProduct
            // 
            this.TabPagePlanProduct.Controls.Add(this._productsPlanView);
            this.TabPagePlanProduct.Name = "TabPagePlanProduct";
            this.TabPagePlanProduct.Size = new System.Drawing.Size(1093, 490);
            this.TabPagePlanProduct.Text = "Планы на месяц Продукты";
            // 
            // _productsPlanView
            // 
            this._productsPlanView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productsPlanView.Location = new System.Drawing.Point(0, 0);
            this._productsPlanView.Name = "_productsPlanView";
            this._productsPlanView.Size = new System.Drawing.Size(1093, 490);
            this._productsPlanView.TabIndex = 0;
            // 
            // TabPagePlanGeneral
            // 
            this.TabPagePlanGeneral.Controls.Add(this._generalPlanView);
            this.TabPagePlanGeneral.Name = "TabPagePlanGeneral";
            this.TabPagePlanGeneral.Size = new System.Drawing.Size(1093, 490);
            this.TabPagePlanGeneral.Text = "Планы на месяц Общие";
            // 
            // _generalPlanView
            // 
            this._generalPlanView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._generalPlanView.Location = new System.Drawing.Point(0, 0);
            this._generalPlanView.Name = "_generalPlanView";
            this._generalPlanView.Size = new System.Drawing.Size(1093, 490);
            this._generalPlanView.TabIndex = 0;
            // 
            // TabPagePlanNetwork
            // 
            this.TabPagePlanNetwork.Controls.Add(this._networksPlanView);
            this.TabPagePlanNetwork.Name = "TabPagePlanNetwork";
            this.TabPagePlanNetwork.Size = new System.Drawing.Size(1093, 490);
            this.TabPagePlanNetwork.Text = "Планы на месяц Сети";
            // 
            // _networksPlanView
            // 
            this._networksPlanView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._networksPlanView.Location = new System.Drawing.Point(0, 0);
            this._networksPlanView.Name = "_networksPlanView";
            this._networksPlanView.Size = new System.Drawing.Size(1093, 490);
            this._networksPlanView.TabIndex = 0;
            // 
            // TabPageConfig
            // 
            this.TabPageConfig.Controls.Add(this.TabControlConfig);
            this.TabPageConfig.Name = "TabPageConfig";
            this.TabPageConfig.Size = new System.Drawing.Size(1093, 490);
            this.TabPageConfig.Text = "Настройка Показателей";
            // 
            // TabControlConfig
            // 
            this.TabControlConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlConfig.Location = new System.Drawing.Point(0, 0);
            this.TabControlConfig.Name = "TabControlConfig";
            this.TabControlConfig.SelectedTabPage = this.TabPageCommon;
            this.TabControlConfig.Size = new System.Drawing.Size(1093, 490);
            this.TabControlConfig.TabIndex = 0;
            this.TabControlConfig.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TabPageCommon,
            this.TabPageProductSet,
            this.TabPageSKUList,
            this.TabPageFocusTarget,
            this.TabPageRatings});
            this.TabControlConfig.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.TabControlConfig_SelectedPageChanging);
            // 
            // TabPageCommon
            // 
            this.TabPageCommon.Controls.Add(this._generalSettingsView);
            this.TabPageCommon.Name = "TabPageCommon";
            this.TabPageCommon.Size = new System.Drawing.Size(1086, 461);
            this.TabPageCommon.Text = "Общие";
            // 
            // _generalSettingsView
            // 
            this._generalSettingsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._generalSettingsView.Location = new System.Drawing.Point(0, 0);
            this._generalSettingsView.Name = "_generalSettingsView";
            this._generalSettingsView.Size = new System.Drawing.Size(1086, 461);
            this._generalSettingsView.TabIndex = 0;
            // 
            // TabPageProductSet
            // 
            this.TabPageProductSet.Name = "TabPageProductSet";
            this.TabPageProductSet.Size = new System.Drawing.Size(1086, 461);
            this.TabPageProductSet.Text = "Продуктовые наборы";
            // 
            // TabPageSKUList
            // 
            this.TabPageSKUList.Name = "TabPageSKUList";
            this.TabPageSKUList.Size = new System.Drawing.Size(1086, 461);
            this.TabPageSKUList.Text = "Списки СКЮ";
            // 
            // TabPageFocusTarget
            // 
            this.TabPageFocusTarget.Controls.Add(this._focusGoalsView);
            this.TabPageFocusTarget.Name = "TabPageFocusTarget";
            this.TabPageFocusTarget.Size = new System.Drawing.Size(1086, 461);
            this.TabPageFocusTarget.Text = "Фокусные цели";
            // 
            // _focusGoalsView
            // 
            this._focusGoalsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._focusGoalsView.Location = new System.Drawing.Point(0, 0);
            this._focusGoalsView.Name = "_focusGoalsView";
            this._focusGoalsView.Size = new System.Drawing.Size(1086, 461);
            this._focusGoalsView.TabIndex = 0;
            // 
            // TabPageRatings
            // 
            this.TabPageRatings.Controls.Add(this._ratingsView);
            this.TabPageRatings.Name = "TabPageRatings";
            this.TabPageRatings.Size = new System.Drawing.Size(1086, 461);
            this.TabPageRatings.Text = "Рейтинги";
            // 
            // _ratingsView
            // 
            this._ratingsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ratingsView.Location = new System.Drawing.Point(0, 0);
            this._ratingsView.Name = "_ratingsView";
            this._ratingsView.Size = new System.Drawing.Size(1086, 461);
            this._ratingsView.TabIndex = 0;
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowItemAnimatedHighlighting = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMain,
            this.barMainMenu,
            this.barStatus});
            this.barManager.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Main", new System.Guid("f053d282-150b-414d-9a37-d452564e9cc0"))});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barBtnSave,
            this.barBtnRefreshCapsBinding,
            this.barBtnRefresh,
            this.barBtnImportFromXls});
            this.barManager.LargeImages = this.imgCollection;
            this.barManager.MainMenu = this.barMainMenu;
            this.barManager.MaxItemId = 8;
            this.barManager.StatusBar = this.barStatus;
            // 
            // barMain
            // 
            this.barMain.BarName = "Main";
            this.barMain.CanDockStyle = ((DevExpress.XtraBars.BarCanDockStyle)((DevExpress.XtraBars.BarCanDockStyle.Top | DevExpress.XtraBars.BarCanDockStyle.Standalone)));
            this.barMain.DockCol = 0;
            this.barMain.DockRow = 0;
            this.barMain.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, this.barBtnSave, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "CTRL+S", ""),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnImportFromXls),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnRefreshCapsBinding)});
            this.barMain.OptionsBar.AllowQuickCustomization = false;
            this.barMain.OptionsBar.DisableClose = true;
            this.barMain.OptionsBar.DisableCustomization = true;
            this.barMain.Text = "Tools";
            // 
            // barBtnSave
            // 
            this.barBtnSave.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Top;
            this.barBtnSave.Hint = "Сохранить";
            this.barBtnSave.Id = 0;
            this.barBtnSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barBtnSave.LargeImageIndex = 0;
            this.barBtnSave.Name = "barBtnSave";
            this.barBtnSave.ShowCaptionOnBar = false;
            this.barBtnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnSave_ItemClick);
            // 
            // barBtnRefresh
            // 
            this.barBtnRefresh.Hint = "Обновить данные";
            this.barBtnRefresh.Id = 6;
            this.barBtnRefresh.LargeImageIndex = 2;
            this.barBtnRefresh.Name = "barBtnRefresh";
            this.barBtnRefresh.ShowCaptionOnBar = false;
            this.barBtnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnRefresh_ItemClick);
            // 
            // barBtnImportFromXls
            // 
            this.barBtnImportFromXls.Hint = "Загрузить планы";
            this.barBtnImportFromXls.Id = 7;
            this.barBtnImportFromXls.LargeImageIndex = 3;
            this.barBtnImportFromXls.Name = "barBtnImportFromXls";
            this.barBtnImportFromXls.ShowCaptionOnBar = false;
            this.barBtnImportFromXls.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnImportFromXls_ItemClick);
            // 
            // barBtnRefreshCapsBinding
            // 
            this.barBtnRefreshCapsBinding.Hint = "Обновить цели из СКиП";
            this.barBtnRefreshCapsBinding.Id = 5;
            this.barBtnRefreshCapsBinding.LargeImageIndex = 1;
            this.barBtnRefreshCapsBinding.Name = "barBtnRefreshCapsBinding";
            this.barBtnRefreshCapsBinding.ShowCaptionOnBar = false;
            this.barBtnRefreshCapsBinding.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnRefreshCapsBinding_ItemClick);
            // 
            // barMainMenu
            // 
            this.barMainMenu.BarName = "Main menu";
            this.barMainMenu.DockCol = 0;
            this.barMainMenu.DockRow = 1;
            this.barMainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMainMenu.FloatLocation = new System.Drawing.Point(103, 123);
            this.barMainMenu.OptionsBar.MultiLine = true;
            this.barMainMenu.OptionsBar.UseWholeRow = true;
            this.barMainMenu.Text = "Main menu";
            this.barMainMenu.Visible = false;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.RotateWhenVertical = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status";
            this.barStatus.Visible = false;
            // 
            // imgCollection
            // 
            this.imgCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imgCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgCollection.ImageStream")));
            this.imgCollection.Images.SetKeyName(0, "save24x24.png");
            this.imgCollection.Images.SetKeyName(1, "refresh 24x24.png");
            this.imgCollection.Images.SetKeyName(2, "refresh_24.png");
            this.imgCollection.Images.SetKeyName(3, "Excel_42.bmp");
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(1100, 600);
            this.Resize += new System.EventHandler(this.WccpUIControl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.TabPagePlanProduct.ResumeLayout(false);
            this.TabPagePlanGeneral.ResumeLayout(false);
            this.TabPagePlanNetwork.ResumeLayout(false);
            this.TabPageConfig.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControlConfig)).EndInit();
            this.TabControlConfig.ResumeLayout(false);
            this.TabPageCommon.ResumeLayout(false);
            this.TabPageFocusTarget.ResumeLayout(false);
            this.TabPageRatings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl TabControl;
        private DevExpress.XtraTab.XtraTabPage TabPagePlanProduct;
        private DevExpress.XtraTab.XtraTabPage TabPagePlanGeneral;
        private DevExpress.XtraTab.XtraTabPage TabPagePlanNetwork;
        private DevExpress.XtraTab.XtraTabPage TabPageConfig;
        private DevExpress.XtraTab.XtraTabControl TabControlConfig;
        private DevExpress.XtraTab.XtraTabPage TabPageCommon;
        private DevExpress.XtraTab.XtraTabPage TabPageProductSet;
        private DevExpress.XtraTab.XtraTabPage TabPageSKUList;
        private DevExpress.XtraTab.XtraTabPage TabPageFocusTarget;
        private DevExpress.XtraTab.XtraTabPage TabPageRatings;
        private GeneralSettingsView _generalSettingsView;
        private FocusGoalsView _focusGoalsView;
        private RatingsView _ratingsView;
        private GeneralPlanView _generalPlanView;
        private NetworksPlanView _networksPlanView;
        private ProductsPlanView _productsPlanView;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar barMain;
        private DevExpress.XtraBars.Bar barMainMenu;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem barBtnSave;
        private DevExpress.Utils.ImageCollection imgCollection;
        private DevExpress.XtraBars.BarLargeButtonItem barBtnRefreshCapsBinding;
        private DevExpress.XtraBars.BarLargeButtonItem barBtnRefresh;
        private DevExpress.XtraBars.BarLargeButtonItem barBtnImportFromXls;
    }
}
