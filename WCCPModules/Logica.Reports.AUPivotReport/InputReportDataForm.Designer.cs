namespace WccpReporting
{
    partial class InputReportDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputReportDataForm));
            this.lbWave = new System.Windows.Forms.Label();
            this.reportDataSet = new WccpReporting.ReportDataSet();
            this.bsAction = new System.Windows.Forms.BindingSource(this.components);
            this.chboxIsCheckCompliance = new DevExpress.XtraEditors.CheckEdit();
            this.ppcedAction = new DevExpress.XtraEditors.PopupContainerEdit();
            this.action_ppContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this.tvActions = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colActionLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colAction_Start = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colAction_End = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colUCR = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).BeginInit();
            this.PageControlOptions.SuspendLayout();
            this.OptionsTabSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).BeginInit();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).BeginInit();
            this.grpOptionsReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).BeginInit();
            this.PanelClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chboxIsCheckCompliance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppcedAction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.action_ppContainerControl)).BeginInit();
            this.action_ppContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // PageControlOptions
            // 
            // 
            // OptionsTabSheet
            // 
            this.OptionsTabSheet.ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
            // 
            // OptionsPanel
            // 
            // 
            // grpOptionsReport
            // 
            this.grpOptionsReport.Controls.Add(this.action_ppContainerControl);
            this.grpOptionsReport.Controls.Add(this.ppcedAction);
            this.grpOptionsReport.Controls.Add(this.chboxIsCheckCompliance);
            this.grpOptionsReport.Controls.Add(this.lbWave);
            this.grpOptionsReport.TabStop = true;
            // 
            // edReportName
            // 
            this.edReportName.EditValue = "";
            this.edReportName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.edReportName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edReportName.Properties.Appearance.Options.UseBackColor = true;
            this.edReportName.Properties.Appearance.Options.UseFont = true;
            // 
            // btnHelp
            // 
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnYes
            // 
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // lbWave
            // 
            this.lbWave.AutoSize = true;
            this.lbWave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbWave.Location = new System.Drawing.Point(8, 24);
            this.lbWave.Name = "lbWave";
            this.lbWave.Size = new System.Drawing.Size(42, 13);
            this.lbWave.TabIndex = 0;
            this.lbWave.Text = "&�����:";
            // 
            // reportDataSet
            // 
            this.reportDataSet.DataSetName = "ReportDataSet";
            this.reportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bsAction
            // 
            this.bsAction.DataMember = "tblTreeAction";
            this.bsAction.DataSource = this.reportDataSet;
            // 
            // chboxIsCheckCompliance
            // 
            this.chboxIsCheckCompliance.Location = new System.Drawing.Point(6, 68);
            this.chboxIsCheckCompliance.Name = "chboxIsCheckCompliance";
            this.chboxIsCheckCompliance.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.chboxIsCheckCompliance.Properties.Caption = "&������ �� ����������� ������� �����";
             this.chboxIsCheckCompliance.Size = new System.Drawing.Size(332, 18);
            this.chboxIsCheckCompliance.TabIndex = 2;
            // 
            // ppcedAction
            // 
            this.ppcedAction.Location = new System.Drawing.Point(8, 41);
            this.ppcedAction.Name = "ppcedAction";
            this.ppcedAction.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ppcedAction.Properties.NullText = "�� �������";
            this.ppcedAction.Properties.PopupControl = this.action_ppContainerControl;
            this.ppcedAction.Properties.ShowPopupCloseButton = false;
            this.ppcedAction.Properties.ShowPopupShadow = false;
            this.ppcedAction.Size = new System.Drawing.Size(330, 22);
            this.ppcedAction.TabIndex = 3;
            // 
            // action_ppContainerControl
            // 
            this.action_ppContainerControl.Controls.Add(this.tvActions);
            this.action_ppContainerControl.Location = new System.Drawing.Point(8, 86);
            this.action_ppContainerControl.Name = "action_ppContainerControl";
            this.action_ppContainerControl.Size = new System.Drawing.Size(330, 165);
            this.action_ppContainerControl.TabIndex = 4;
            // 
            // tvActions
            // 
            this.tvActions.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tvActions.Appearance.HeaderPanel.Options.UseFont = true;
            this.tvActions.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colId,
            this.colActionLevel,
            this.colAction_Start,
            this.colAction_End,
            this.colUCR});
            this.tvActions.DataSource = this.bsAction;
            this.tvActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvActions.ImageIndexFieldName = "isActive";
            this.tvActions.KeyFieldName = "Item_Id";
            this.tvActions.Location = new System.Drawing.Point(0, 0);
            this.tvActions.Name = "tvActions";
            this.tvActions.OptionsBehavior.AutoSelectAllInEditor = false;
            this.tvActions.OptionsBehavior.Editable = false;
            this.tvActions.OptionsView.AutoWidth = false;
            this.tvActions.OptionsView.ShowIndicator = false;
            this.tvActions.ParentFieldName = "Parent_Id";
            this.tvActions.SelectImageList = this.imageCollection1;
            this.tvActions.ShowButtonMode = DevExpress.XtraTreeList.ShowButtonModeEnum.ShowOnlyInEditor;
            this.tvActions.Size = new System.Drawing.Size(330, 165);
            this.tvActions.TabIndex = 0;
            this.tvActions.DoubleClick += new System.EventHandler(this.tvActions_DoubleClick);
            // 
            // colName
            // 
            this.colName.Caption = "������ �����";
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 35;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowSort = false;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 300;
            // 
            // colId
            // 
            this.colId.Caption = "���.";
            this.colId.FieldName = "keyID";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowSort = false;
            this.colId.Visible = true;
            this.colId.VisibleIndex = 1;
            this.colId.Width = 35;
            // 
            // colActionLevel
            // 
            this.colActionLevel.Caption = "�������";
            this.colActionLevel.FieldName = "ActionLevel";
            this.colActionLevel.Name = "colActionLevel";
            this.colActionLevel.OptionsColumn.AllowSort = false;
            this.colActionLevel.Visible = true;
            this.colActionLevel.VisibleIndex = 2;
            this.colActionLevel.Width = 59;
            // 
            // colAction_Start
            // 
            this.colAction_Start.Caption = "���� ������";
            this.colAction_Start.FieldName = "Action_Start";
            this.colAction_Start.Format.FormatString = "d";
            this.colAction_Start.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colAction_Start.Name = "colAction_Start";
            this.colAction_Start.OptionsColumn.AllowSort = false;
            this.colAction_Start.Visible = true;
            this.colAction_Start.VisibleIndex = 3;
            this.colAction_Start.Width = 85;
            // 
            // colAction_End
            // 
            this.colAction_End.Caption = "���� ���������";
            this.colAction_End.FieldName = "Action_End";
            this.colAction_End.Format.FormatString = "d";
            this.colAction_End.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colAction_End.Name = "colAction_End";
            this.colAction_End.OptionsColumn.AllowSort = false;
            this.colAction_End.Visible = true;
            this.colAction_End.VisibleIndex = 4;
            this.colAction_End.Width = 105;
            // 
            // colUCR
            // 
            this.colUCR.Caption = "���������";
            this.colUCR.FieldName = "UCR";
            this.colUCR.Name = "colUCR";
            this.colUCR.OptionsColumn.AllowSort = false;
            this.colUCR.Visible = true;
            this.colUCR.VisibleIndex = 5;
            this.colUCR.Width = 200;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "activity_16_dis.png");
            this.imageCollection1.Images.SetKeyName(1, "activity_16.png");
            // 
            // InputReportDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(369, 450);
            this.Name = "InputReportDataForm";
            this.Load += new System.EventHandler(this.InputReportDataForm_Load);
            this.Shown += new System.EventHandler(this.InputProgressReportDataForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).EndInit();
            this.PageControlOptions.ResumeLayout(false);
            this.OptionsTabSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).EndInit();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).EndInit();
            this.grpOptionsReport.ResumeLayout(false);
            this.grpOptionsReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).EndInit();
            this.PanelClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chboxIsCheckCompliance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppcedAction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.action_ppContainerControl)).EndInit();
            this.action_ppContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbWave;
        private ReportDataSet reportDataSet;
        private DevExpress.XtraEditors.CheckEdit chboxIsCheckCompliance;
        private System.Windows.Forms.BindingSource bsAction;
        private DevExpress.XtraEditors.PopupContainerControl action_ppContainerControl;
        private DevExpress.XtraTreeList.TreeList tvActions;
        private DevExpress.XtraEditors.PopupContainerEdit ppcedAction;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colActionLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colAction_Start;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colAction_End;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colUCR;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}
