namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.pnReportSheet = new DevExpress.XtraEditors.PanelControl();
            this.tcReports = new DevExpress.XtraTab.XtraTabControl();
            this.tpTP = new DevExpress.XtraTab.XtraTabPage();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.pgReport = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pnReportHeader = new DevExpress.XtraEditors.PanelControl();
            this.lbActionName = new DevExpress.XtraEditors.LabelControl();
            this.edActionText = new DevExpress.XtraEditors.TextEdit();
            this.barManagerReport = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.tbtnReportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.tbtnReportRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imgNormal = new DevExpress.Utils.ImageCollection(this.components);
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.imgLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.lbReportTitle = new System.Windows.Forms.Label();
            this.pnReportToolBar = new DevExpress.XtraEditors.PanelControl();
            this.reportDataSet = new WccpReporting.ReportDataSet();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).BeginInit();
            this.pnReportSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcReports)).BeginInit();
            this.tcReports.SuspendLayout();
            this.tpTP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).BeginInit();
            this.pnReportHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edActionText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).BeginInit();
            this.pnReportToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // pnReportSheet
            // 
            this.pnReportSheet.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnReportSheet.Controls.Add(this.tcReports);
            this.pnReportSheet.Controls.Add(this.pnReportHeader);
            this.pnReportSheet.Controls.Add(this.pnReportToolBar);
            this.pnReportSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnReportSheet.Location = new System.Drawing.Point(0, 0);
            this.pnReportSheet.Name = "pnReportSheet";
            this.pnReportSheet.Size = new System.Drawing.Size(640, 480);
            this.pnReportSheet.TabIndex = 0;
            // 
            // tcReports
            // 
            this.tcReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReports.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tcReports.Location = new System.Drawing.Point(0, 102);
            this.tcReports.Name = "tcReports";
            this.tcReports.SelectedTabPage = this.tpTP;
            this.tcReports.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcReports.Size = new System.Drawing.Size(640, 378);
            this.tcReports.TabIndex = 2;
            this.tcReports.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpTP});
            // 
            // tpTP
            // 
            this.tpTP.Controls.Add(this.dataLayoutControl1);
            this.tpTP.Name = "tpTP";
            this.tpTP.Size = new System.Drawing.Size(636, 374);
            this.tpTP.Tag = "1";
            this.tpTP.Text = "AUPivotReport";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.pgReport);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(636, 374);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // pgReport
            // 
            this.pgReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.pgReport.Location = new System.Drawing.Point(0, 0);
            this.pgReport.Name = "pgReport";
            this.pgReport.OptionsCustomization.AllowEdit = false;
            this.pgReport.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pgReport.OptionsLayout.Columns.AddNewColumns = false;
            this.pgReport.OptionsLayout.Columns.RemoveOldColumns = false;
            this.pgReport.OptionsLayout.Columns.StoreAllOptions = true;
            this.pgReport.OptionsLayout.Columns.StoreAppearance = true;
            this.pgReport.OptionsLayout.StoreAllOptions = true;
            this.pgReport.OptionsLayout.StoreAppearance = true;
            this.pgReport.Size = new System.Drawing.Size(636, 374);
            this.pgReport.TabIndex = 4;
            this.pgReport.CustomUnboundFieldData += new DevExpress.XtraPivotGrid.CustomFieldDataEventHandler(this.pgReport_CustomUnboundFieldData);
            this.pgReport.ShowingCustomizationForm += new DevExpress.XtraPivotGrid.CustomizationFormShowingEventHandler(this.pgReport_ShowingCustomizationForm);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(636, 374);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pgReport;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(636, 374);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // pnReportHeader
            // 
            this.pnReportHeader.Controls.Add(this.lbActionName);
            this.pnReportHeader.Controls.Add(this.edActionText);
            this.pnReportHeader.Controls.Add(this.lbReportTitle);
            this.pnReportHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportHeader.Location = new System.Drawing.Point(0, 40);
            this.pnReportHeader.Name = "pnReportHeader";
            this.pnReportHeader.Size = new System.Drawing.Size(640, 62);
            this.pnReportHeader.TabIndex = 1;
            // 
            // lbActionName
            // 
            this.lbActionName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbActionName.Appearance.Options.UseFont = true;
            this.lbActionName.Location = new System.Drawing.Point(7, 39);
            this.lbActionName.Name = "lbActionName";
            this.lbActionName.Size = new System.Drawing.Size(39, 13);
            this.lbActionName.TabIndex = 1;
            this.lbActionName.Text = "�����:";
            // 
            // edActionText
            // 
            this.edActionText.Location = new System.Drawing.Point(52, 36);
            this.edActionText.MenuManager = this.barManagerReport;
            this.edActionText.Name = "edActionText";
            this.edActionText.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edActionText.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edActionText.Properties.Appearance.Options.UseBackColor = true;
            this.edActionText.Properties.Appearance.Options.UseFont = true;
            this.edActionText.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edActionText.Properties.ReadOnly = true;
            this.edActionText.Size = new System.Drawing.Size(582, 20);
            this.edActionText.TabIndex = 2;
            this.edActionText.TabStop = false;
            // 
            // barManagerReport
            // 
            this.barManagerReport.AllowCustomization = false;
            this.barManagerReport.AllowMoveBarOnToolbar = false;
            this.barManagerReport.AllowQuickCustomization = false;
            this.barManagerReport.AllowShowToolbarsPopup = false;
            this.barManagerReport.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManagerReport.Controller = this.barAndDockingController1;
            this.barManagerReport.DockControls.Add(this.barDockControlTop);
            this.barManagerReport.DockControls.Add(this.barDockControlBottom);
            this.barManagerReport.DockControls.Add(this.barDockControlLeft);
            this.barManagerReport.DockControls.Add(this.barDockControlRight);
            this.barManagerReport.DockControls.Add(this.standaloneBarDockControl1);
            this.barManagerReport.Form = this;
            this.barManagerReport.Images = this.imgNormal;
            this.barManagerReport.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.tbtnReportToExcel,
            this.tbtnReportRefresh,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManagerReport.LargeImages = this.imgLarge;
            this.barManagerReport.MainMenu = this.bar2;
            this.barManagerReport.MaxItemId = 6;
            this.barManagerReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            // 
            // bar2
            // 
            this.bar2.BarName = "menuReport";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(217, 125);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this.tbtnReportToExcel, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this.tbtnReportRefresh, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "menuReport";
            // 
            // tbtnReportToExcel
            // 
            this.tbtnReportToExcel.Caption = "����� � Excel";
            this.tbtnReportToExcel.Hint = "����� � Excel";
            this.tbtnReportToExcel.Id = 0;
            this.tbtnReportToExcel.LargeImageIndex = 1;
            this.tbtnReportToExcel.Name = "tbtnReportToExcel";
            this.tbtnReportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportToExcel_ItemClick);
            // 
            // tbtnReportRefresh
            // 
            this.tbtnReportRefresh.Caption = "�������� �����";
            this.tbtnReportRefresh.Hint = "�������� �����";
            this.tbtnReportRefresh.Id = 2;
            this.tbtnReportRefresh.LargeImageIndex = 0;
            this.tbtnReportRefresh.Name = "tbtnReportRefresh";
            this.tbtnReportRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportRefresh_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(3, 3);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(634, 34);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
           this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // imgNormal
            // 
            this.imgNormal.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgNormal.ImageStream")));
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 4;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 5;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // imgLarge
            // 
            this.imgLarge.ImageSize = new System.Drawing.Size(24, 24);
            this.imgLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgLarge.ImageStream")));
            this.imgLarge.Images.SetKeyName(0, "briefcase_ok_24.png");
            this.imgLarge.Images.SetKeyName(1, "Excel_32.bmp");
            this.imgLarge.Images.SetKeyName(2, "briefcase_prev_24.png");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // lbReportTitle
            // 
            this.lbReportTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbReportTitle.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbReportTitle.Location = new System.Drawing.Point(3, 3);
            this.lbReportTitle.Name = "lbReportTitle";
            this.lbReportTitle.Size = new System.Drawing.Size(634, 18);
            this.lbReportTitle.TabIndex = 0;
            this.lbReportTitle.Text = "Title";
            this.lbReportTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnReportToolBar
            // 
            this.pnReportToolBar.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pnReportToolBar.Controls.Add(this.standaloneBarDockControl1);
            this.pnReportToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportToolBar.Location = new System.Drawing.Point(0, 0);
            this.pnReportToolBar.Name = "pnReportToolBar";
            this.pnReportToolBar.Size = new System.Drawing.Size(640, 40);
            this.pnReportToolBar.TabIndex = 0;
            // 
            // reportDataSet
            // 
            this.reportDataSet.DataSetName = "ReportDataSet";
            this.reportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // WccpUIControl
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnReportSheet);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(640, 480);
            this.Load += new System.EventHandler(this.ReportControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).EndInit();
            this.pnReportSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcReports)).EndInit();
            this.tcReports.ResumeLayout(false);
            this.tpTP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).EndInit();
            this.pnReportHeader.ResumeLayout(false);
            this.pnReportHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edActionText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).EndInit();
            this.pnReportToolBar.ResumeLayout(false);
            this.pnReportToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnReportSheet;
        private DevExpress.XtraEditors.PanelControl pnReportToolBar;
        private DevExpress.XtraBars.BarManager barManagerReport;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem tbtnReportToExcel;
        private DevExpress.Utils.ImageCollection imgLarge;
        private DevExpress.XtraEditors.PanelControl pnReportHeader;
        private System.Windows.Forms.Label lbReportTitle;
        private DevExpress.XtraEditors.TextEdit edActionText;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem tbtnReportRefresh;
        private ReportDataSet reportDataSet;
        private DevExpress.XtraTab.XtraTabControl tcReports;
        private DevExpress.XtraTab.XtraTabPage tpTP;
        private DevExpress.Utils.ImageCollection imgNormal;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraEditors.LabelControl lbActionName;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        public DevExpress.XtraPivotGrid.PivotGridControl pgReport;
    }
}
