using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.OleDb;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.AUPivotReport.Properties;

namespace WccpReporting
{
    public partial class InputReportDataForm : WccpReporting.CustomInputReportDataForm
    {
        private string _reportName;
        public string ReportName
        {
            get { return _reportName; }
            set { _reportName = value;
                  edReportName.Text = value;
                }
        }

        private int _ActionId;
        public int ActionId
        {
            get { return _ActionId; }
            set { _ActionId = value; }
        }

        private string _ActionName;
        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }

        private bool _IsCheckCompliance;
        public bool IsCheckCompliance
        {
            get { return _IsCheckCompliance; }
            set { _IsCheckCompliance = value; }
        }

        public InputReportDataForm()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                if (ppcedAction.EditValue == null)
                {
                  throw new Exception(WCCPConst.msgErrorActionIsNull);
                };

                ActionName = ppcedAction.Text;
                IsCheckCompliance = chboxIsCheckCompliance.Checked;

                Settings.Default.ActionId = ActionId;
                Settings.Default.ActionName = ActionName;
                Settings.Default.IsCheckCompliance = IsCheckCompliance;
                Settings.Default.Save();
                //
                ReportOptions.ActionId = ActionId;
                ReportOptions.ActionName = ActionName;
                ReportOptions.IsCheckCompliance = IsCheckCompliance;
                //
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                ppcedAction.Focus();
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(WCCPConst.msgNoHelp, "����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void InputReportDataForm_Load(object sender, EventArgs e)
        {
            using (OleDbCommand cmdspDW_AU_TreeAction = new OleDbCommand("spDW_AU_TreeAction", DM.Connection))
            {
                cmdspDW_AU_TreeAction.CommandTimeout = WCCPConst.CommandTimeout;
                cmdspDW_AU_TreeAction.CommandType = CommandType.StoredProcedure;
                cmdspDW_AU_TreeAction.Parameters.AddWithValue("@Actions_only", true);
                try
                {
                    using (OleDbDataAdapter daAction = new OleDbDataAdapter(cmdspDW_AU_TreeAction))
                    {
                        daAction.Fill(reportDataSet.tblTreeAction);
                        foreach (DataRow row in reportDataSet.tblTreeAction.Rows)
                        {
                            if (row["Parent_Id"] != DBNull.Value){

                                string key = Convert.ToString(row["Parent_Id"]).Substring(0, 1);

                                if (key == "3")
                                {
                                    row["keyID"] = row["Id"];
                                };
                            };
                        };
                    };
                }
                catch(Exception ex)
                {
                    MessageBox.Show(WCCPConst.msgErrorCreateActionList + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                };
            };
        }

        private void InputProgressReportDataForm_Shown(object sender, EventArgs e)
        {
            if (Settings.Default.ActionId > 0)
            {
                ActionId = Settings.Default.ActionId;
                ppcedAction.Text = Settings.Default.ActionName;
            };
            chboxIsCheckCompliance.Checked = Settings.Default.IsCheckCompliance;
            tvActions.Nodes.FirstNode.ExpandAll();
            ppcedAction.Focus();
        }

        private void tvActions_DoubleClick(object sender, EventArgs e)
        {
            if ((reportDataSet.tblTreeAction.Rows.Count > 0) && (reportDataSet.tblTreeAction[bsAction.Position]["keyID"] != DBNull.Value) && (Convert.ToInt32(reportDataSet.tblTreeAction[bsAction.Position]["isActive"]) == 1))
            {
                ppcedAction.Text = Convert.ToString(reportDataSet.tblTreeAction[bsAction.Position]["Name"]);
                ActionId = Convert.ToInt32(reportDataSet.tblTreeAction[bsAction.Position]["Id"]);
                ActionName = Convert.ToString(reportDataSet.tblTreeAction[bsAction.Position]["Name"]);
                ppcedAction.ClosePopup();
            }
            else
              return;
        }

    }
}

