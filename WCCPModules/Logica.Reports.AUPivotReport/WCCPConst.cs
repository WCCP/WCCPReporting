using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using System.IO;

namespace WccpReporting
{
    static class ReportOptions
    {        
        static public string ReportTitle;
        static public int    ActionId;
        static public string ActionName;
        static public bool   IsCheckCompliance;
        static public PivotGridControl reportControl;

        static public void SaveOptions()
        {
            try
            {
                if (reportControl != null)
                {
                    reportControl.SaveLayoutToXml(WCCPConst.root + WCCPConst.frptoptReport);
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorSaveLayoutReport + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static public void LoadOptions()
        {
            try
            {
                if (reportControl != null && File.Exists(WCCPConst.root + WCCPConst.frptoptReport))
                {
                    reportControl.RestoreLayoutFromXml(WCCPConst.root + WCCPConst.frptoptReport);
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorLoadLayoutReport + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    };

    static class WCCPConst
    {
        static public string root {
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }
        static public bool   IsSkin = false;
        static public int    CommandTimeout = 900;
        static public string frptexlReport = "TRAUReport.xls";
        static public string frptoptReport = "TRAUReport.xml";

        static public string msgNoHelp                = "� ������ ������ ��������� ������� �����������";
        static public string msgErrorCreateActionList = "�� ���� ������� ������ �����";
        static public string msgErrorCreateReport     = "�� ���� ������� �����";
        static public string msgErrorSaveOptions      = "�� ���� ��������� �����";
        static public string msgErrorSaveLayoutReport = "�� ���� ��������� ������� ��� ������";
        static public string msgErrorLoadLayoutReport = "�� ���� ��������� ������� ��� ������";
        static public string msgErrorActionIsNull     = "����� ������ ���� ������";
        static public string msgErrorPrepare          = "�� ���� ������������ ��������������� ����� ������";
        static public string msgErrorConnect          = "�� ���� ������������ � ���� ������";
        static public string msgErrorOpenConnection   = "�� ���� ������� ����������";
        static public string msgErrorCloseConnection  = "�� ���� ������� ����������";
    }

}
