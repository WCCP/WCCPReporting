using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.OleDb;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using System.Collections;
using DevExpress.XtraPrinting;
using DevExpress.XtraPivotGrid;
using System.Diagnostics;
using Logica.Reports.Common.WaitWindow;


namespace WccpReporting
{
    public partial class WccpUIControl : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable dataDataView = new DataTable();

        private string _ReportTitle;
        public string ReportTitle
        {
            get { return _ReportTitle; }
            set { _ReportTitle = value; lbReportTitle.Text = value; }
        }

        private string _ActionName;
        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; edActionText.Text = value; }
        }

        private int _ActionId;
        public int ActionId
        {
            get { return _ActionId; }
            set { _ActionId = value; }
        }

        private bool _IsCheckCompliance;
        public bool IsCheckCompliance
        {
            get { return _IsCheckCompliance; }
            set { _IsCheckCompliance = value; }
        }

        public WccpUIControl()
        {
            InitializeComponent();
            Disposed += new EventHandler(ReportControl_Disposed);
        }

        void ReportControl_Disposed(object sender, EventArgs e)
        {
            try
            {
                DM.Connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCloseConnection + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private bool CleanDirForReportFiles(string exlFile)
        {
            try
            {
                if (File.Exists(exlFile))
                {
                    File.Delete(exlFile);
                    return true;
                }
                else
                    return true;
            }
            catch
            {
                MessageBox.Show("�������� ���� " + exlFile + " � ��������� �������", "����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            };
        }

        //������������ ��������������� ����� ������
        static public bool Prepare()
        {
            using (OleDbCommand cmdPrepare = new OleDbCommand("spDW_ScriptGetByModule", DM.Connection))
            {
                cmdPrepare.CommandTimeout = WCCPConst.CommandTimeout;
                cmdPrepare.CommandType = CommandType.StoredProcedure;
                cmdPrepare.Parameters.AddWithValue("@ModuleName", "AU_ReportPivot");
                cmdPrepare.Parameters.AddWithValue("@isDesktop", DBNull.Value);
                try
                {
                    DataTable dtSql = new DataTable();
                    try
                    {
                        dtSql.Load(cmdPrepare.ExecuteReader());

                        if (dtSql.Rows.Count > 0 && Convert.ToString(dtSql.Rows[0][0]) != String.Empty)
                        {
                            using (OleDbCommand cmdScript = new OleDbCommand(Convert.ToString(dtSql.Rows[0][0]), DM.Connection))
                            {
                                cmdScript.ExecuteNonQuery();
                            };
                        };
                        return true;
                    }
                    finally
                    {
                        dtSql.Dispose();
                    };
                }
                catch (Exception ex)
                {
                    MessageBox.Show("������: " + WCCPConst.msgErrorPrepare + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                };
            };
        }

        //���������� �����
        private void ShowReport()
        {
            try
            {
                using (OleDbCommand cmdspDW_AU_ReportPivot = new OleDbCommand("spDW_AU_ReportPivot", DM.Connection))
                {
                    cmdspDW_AU_ReportPivot.CommandTimeout = WCCPConst.CommandTimeout;
                    cmdspDW_AU_ReportPivot.CommandType = CommandType.StoredProcedure;
                    cmdspDW_AU_ReportPivot.Parameters.AddWithValue("@Action_ID", ActionId);
                    cmdspDW_AU_ReportPivot.Parameters.AddWithValue("@isCheckCompliance", IsCheckCompliance);

                    dataDataView.Load(cmdspDW_AU_ReportPivot.ExecuteReader());

                    pgReport.BeginUpdate();
                    pgReport.Fields.Clear();

                    foreach (DataColumn col in dataDataView.Columns)
                    {
                        PivotGridField newField = new PivotGridField(col.Caption, PivotArea.FilterArea);
                        newField.Caption = col.Caption;
                        newField.Appearance.Header.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        if (col.DataType != typeof(string))
                        {
                           newField.CellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                           newField.CellFormat.FormatString = "0.00";
                           newField.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                           newField.TotalCellFormat.FormatString = "0.00";
                           newField.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                           newField.GrandTotalCellFormat.FormatString = "0.00";
                           //newField.ValueFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                           //newField.ValueFormat.FormatString = "0.00";
                           newField.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                           newField.TotalValueFormat.FormatString = "0.00";
                        };
                        newField.Visible = false;
                        pgReport.Fields.Add(newField);
                    };

                    PivotGridField unboundFieldUplift_Volume_Per = pgReport.Fields.Add("Uplift_Volume,% 2", PivotArea.RowArea);
                    unboundFieldUplift_Volume_Per.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                    unboundFieldUplift_Volume_Per.CellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldUplift_Volume_Per.CellFormat.FormatString = "0.00";
                    unboundFieldUplift_Volume_Per.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldUplift_Volume_Per.TotalCellFormat.FormatString = "0.00";
                    unboundFieldUplift_Volume_Per.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldUplift_Volume_Per.GrandTotalCellFormat.FormatString = "0.00";
                    unboundFieldUplift_Volume_Per.ValueFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldUplift_Volume_Per.ValueFormat.FormatString = "0.00";
                    unboundFieldUplift_Volume_Per.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldUplift_Volume_Per.TotalValueFormat.FormatString = "0.00";
                    unboundFieldUplift_Volume_Per.Visible = false;

                    PivotGridField unboundFieldField_Variance_Volume_Per = pgReport.Fields.Add("Variance_Volume,% 2", PivotArea.RowArea);
                    unboundFieldField_Variance_Volume_Per.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                    unboundFieldField_Variance_Volume_Per.CellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldField_Variance_Volume_Per.CellFormat.FormatString = "0.00";
                    unboundFieldField_Variance_Volume_Per.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldField_Variance_Volume_Per.TotalCellFormat.FormatString = "0.00";
                    unboundFieldField_Variance_Volume_Per.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldField_Variance_Volume_Per.GrandTotalCellFormat.FormatString = "0.00";
                    unboundFieldField_Variance_Volume_Per.ValueFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldField_Variance_Volume_Per.ValueFormat.FormatString = "0.00";
                    unboundFieldField_Variance_Volume_Per.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    unboundFieldField_Variance_Volume_Per.TotalValueFormat.FormatString = "0.00";
                    unboundFieldField_Variance_Volume_Per.Visible = false;

                    pgReport.EndUpdate();
                    ReportOptions.reportControl = pgReport;
                    ReportOptions.LoadOptions();

                    pgReport.DataSource = dataDataView.DefaultView;

                };
            }
            catch (Exception ex)
            {
                pgReport.EndUpdate();
                MessageBox.Show(WCCPConst.msgErrorCreateReport + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                
            };
        }

        private void ReportControl_Load(object sender, EventArgs e)
        {
            ReportTitle = ReportOptions.ReportTitle;
            ActionId = ReportOptions.ActionId;
            ActionName = ReportOptions.ActionName;
            IsCheckCompliance = ReportOptions.IsCheckCompliance;

            WaitManager.StartWait();
            try
            {
                ShowReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        //����� � Excel
        private void tbtnReportToExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (CleanDirForReportFiles(WCCPConst.root + WCCPConst.frptexlReport))
                {
                    pgReport.ExportToXls(WCCPConst.root + WCCPConst.frptexlReport);
                    Process proc = new Process();
                    proc.EnableRaisingEvents = false;
                    proc.StartInfo.FileName = "excel";
                    proc.StartInfo.Arguments = WCCPConst.root + WCCPConst.frptexlReport;
                    proc.Start();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("�� ���� ������� " + WCCPConst.root + WCCPConst.frptexlReport + "\n���������� ������� ���� ���� ����� Excel" + "\n(" + ex.Message + ")", "��������������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            };
        }

        //�������� �����
        private void tbtnReportRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (InputReportDataForm fmInputReportDataForm = new InputReportDataForm())
            {
                fmInputReportDataForm.ReportName = ReportTitle;
                if (fmInputReportDataForm.ShowDialog() == DialogResult.OK)
                {
                    System.Windows.Forms.Application.DoEvents();

                    ActionId          = ReportOptions.ActionId;
                    ActionName        = ReportOptions.ActionName;
                    IsCheckCompliance = ReportOptions.IsCheckCompliance;

                    WaitManager.StartWait();
                    try
                    {
                        DM.Connection.Close();
                        DM.Connection.Open();
                        Prepare();
                        ShowReport();
                    }
                    catch (Exception ex)
                    {
                        DM.Connection.Close();
                        MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        WaitManager.StopWait();
                    }

                }
            }
        }

        //���������� ������� ����������� ����� ��������� �����
        private void pgReport_ShowingCustomizationForm(object sender, CustomizationFormShowingEventArgs e)
        {
            e.CustomizationForm.TopMost = true;
        }

        private void pgReport_CustomUnboundFieldData(object sender, CustomFieldDataEventArgs e)
        {
            if (e.Field.FieldName == "Uplift_Volume,% 2")
            {
                decimal Actual_Volume_dl = Convert.ToDecimal(0);
                decimal Forecast_Volume_dl = Convert.ToDecimal(0);

                if (e.GetListSourceColumnValue(e.ListSourceRowIndex, "Actual_Volume,dl") != DBNull.Value && e.GetListSourceColumnValue("Actual_Volume,dl") != null)
                {
                    Actual_Volume_dl = Convert.ToDecimal(e.GetListSourceColumnValue("Actual_Volume,dl"));
                }
                else
                {
                    Actual_Volume_dl = Convert.ToDecimal(0);
                };

                if (e.GetListSourceColumnValue(e.ListSourceRowIndex, "Forecast_Volume,dl") != DBNull.Value && e.GetListSourceColumnValue("Forecast_Volume,dl") != null)
                {
                    Forecast_Volume_dl = Convert.ToDecimal(e.GetListSourceColumnValue("Forecast_Volume,dl"));
                }
                else
                {
                    Forecast_Volume_dl = Convert.ToDecimal(0);
                };

                if (Forecast_Volume_dl > 0)
                {
                  e.Value = Convert.ToDecimal(((Actual_Volume_dl/Forecast_Volume_dl)-1));
                }
                else
                {
                  e.Value = Convert.ToDecimal(0);
                };
            };

            if (e.Field.FieldName == "Variance_Volume,% 2")
            {
                decimal Actual_Volume_dl = Convert.ToDecimal(0);
                decimal BL_Volume_dl = Convert.ToDecimal(0);

                if (e.GetListSourceColumnValue(e.ListSourceRowIndex, "Actual_Volume,dl") != DBNull.Value && e.GetListSourceColumnValue("Actual_Volume,dl") != null)
                {
                    Actual_Volume_dl = Convert.ToDecimal(e.GetListSourceColumnValue("Actual_Volume,dl"));
                }
                else
                {
                    Actual_Volume_dl = Convert.ToDecimal(0);
                };

                if (e.GetListSourceColumnValue(e.ListSourceRowIndex, "BL_Volume,dl") != DBNull.Value && e.GetListSourceColumnValue("BL_Volume,dl") != null)
                {
                    BL_Volume_dl = Convert.ToDecimal(e.GetListSourceColumnValue("BL_Volume,dl"));
                }
                else
                {
                    BL_Volume_dl = Convert.ToDecimal(0);
                };

                if (BL_Volume_dl > 0)
                {
                  e.Value = Convert.ToDecimal(((Actual_Volume_dl/BL_Volume_dl)-1));
                }
                else
                {
                  e.Value = Convert.ToDecimal(0);
                };
            };
            
        }

    }  
    
}
