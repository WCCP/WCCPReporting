using System;
using System.Windows.Forms;
using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpAUPivotReport.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        //���������� ������ ������
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        //����� ����� ������
        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                ReportOptions.ReportTitle = reportCaption;

                string winAuth = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog={0};Data Source={1};Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;Tag with column collation when possible=False;";
                string serverAuth = "Provider=SQLOLEDB.1;Password={0};Persist Security Info=True;User ID={1};Initial Catalog={2};Data Source={3};Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;Tag with column collation when possible=False";
                
                SqlConnectionStringBuilder con = new SqlConnectionStringBuilder(HostConfiguration.DBSqlConnection);

                string lConnectionString;

                if (con.IntegratedSecurity)
                {
                    lConnectionString = string.Format(winAuth, con.InitialCatalog, con.DataSource);
                }
                else
                {
                    lConnectionString = string.Format(serverAuth, con.Password, con.UserID, con.InitialCatalog,
                                                      con.DataSource);
                }

                DM.Connect(lConnectionString);
                DM.Connection.Open();
                WccpUIControl.Prepare();

                using (InputReportDataForm fmInputReportDataForm = new InputReportDataForm())
                {
                    fmInputReportDataForm.ReportName = reportCaption;

                    if (fmInputReportDataForm.ShowDialog() == DialogResult.OK)
                    {
                        _reportCaption = reportCaption + " " + fmInputReportDataForm.ActionName;
                        _reportControl = new WccpUIControl();

                        fmInputReportDataForm.ReportName = _reportCaption;

                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DM.Connection.Close();
            }

            return 1;
        }

        //����� ������ ������
        public void CloseUI()
        {
            try
            {
                ReportOptions.SaveOptions();
            }
            catch (Exception ex)
            {
                MessageBox.Show("������: " + ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        public bool AllowClose()
        {
            return true;
        }
    }
}
