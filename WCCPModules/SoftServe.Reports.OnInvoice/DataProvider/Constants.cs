﻿namespace SoftServe.Reports.OnInvoice.DataProviders
{
    /// <summary>
    /// Stored procedures and their params names 
    /// </summary>
    public static class Constants
    {
        #region Settings Form

        public const string spTerritoryTree = "spDW_GetTerritoryTree";
        public const string spTerritoryTree_LevelFrom = "LevelFrom";
        public const string spTerritoryTree_LevelTo = "LevelTo";

        public const string spStaffTree = "spDW_MP_GetHistoryStaff";
        public const string spStaffTree_Date = "Date";
        
        public const string spDistributorsTree = "spDW_GetDistributorsTree";
        public const string spDistributorsTree_LevelFrom = "LevelFrom";
        public const string spDistributorsTree_LevelTo = "LevelTo";

        public const string spActionList = "spDW_MP_ActionListInPeriod";
        public const string spActionList_DateFrom = "StardDate";
        public const string spActionList_DateTo = "EndDate";

        public const string spLimitPeriodList = "spDW_MP_ActionLimitPeriod";
        public const string spLimitPeriodList_DateFrom = "StardDate";
        public const string spLimitPeriodList_DateTo = "EndDate";

        #endregion Settings Window

        #region Main report tab

        public const string spGetReportData = "spDW_MP_InvoiceNoReport";
        public const string spGetReportData_StartDate = "DateStartReport";
        public const string spGetReportData_EndDate = "DateEndReport";
        public const string spGetReportData_StaffIds = "StaffID";
        public const string spGetReportData_ActionIds = "Action_ID";
        public const string spGetReportData_PopIds = "FieldCode";
        public const string spGetReportData_Details_Orders = "InvoiceNoShow";
        public const string spGetReportData_Details_SKU = "SKU_Detail";
        public const string spGetReportData_Details_Action_SKU_ = "SKU_Discount";
        
        #endregion Main report tab
    }
}
