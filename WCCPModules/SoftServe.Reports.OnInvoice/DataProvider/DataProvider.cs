﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BLToolkit.Data;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.OnInvoice.DataProviders
{
    public static class DataProvider
    {
        private static DbManager _db;

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60;  // 15 minutes by default
                }
                return _db;
            }
        }

        #endregion

        /// <summary>
        /// Gets the territory tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of territory division</param>
        /// <param name="levelTo">The lowest level of territory division</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetTerritoryTree(int levelFrom, int levelTo)
        {
            DataTable lDataTable = Db.SetSpCommand(Constants.spTerritoryTree,
                                           Db.Parameter(Constants.spTerritoryTree_LevelFrom, levelFrom),
                                           Db.Parameter(Constants.spTerritoryTree_LevelTo, levelTo)
                ).ExecuteDataTable();
            
            return lDataTable;
        }

        /// <summary>
        /// Gets the distributors tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of distributors hierarchy</param>
        /// <param name="levelTo">The lowest level of distributors hierarchy</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetDistributorsTree(out int distrLevel)
        {
            DataSet dataSet = Db.SetSpCommand(Constants.spDistributorsTree).ExecuteDataSet();
            DataTable lDataTable = null;

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                distrLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                distrLevel = 4;
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the staffing tree actual for the date chosen
        /// </summary>
        /// <param name="date">The date of cheching</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetStaffingTree(DateTime date)
        {
            DataTable lDataTable = Db.SetSpCommand(Constants.spStaffTree,
                Db.Parameter(Constants.spStaffTree_Date, date)).ExecuteDataTable();

            DataView lStaffingView = lDataTable.DefaultView;
            lStaffingView.RowFilter = "Level > 0";
            lDataTable = lStaffingView.ToTable();
            
            return lDataTable;
        }

        public static DataTable GetActions(DateTime dateFrom, DateTime dateTo)
        {
            DataTable lDataTable = Db.SetSpCommand(Constants.spActionList,
                Db.Parameter(Constants.spActionList_DateFrom, dateFrom),
                Db.Parameter(Constants.spActionList_DateTo, dateTo)).ExecuteDataTable();

            return lDataTable;
        }

        public static DataTable GetLimitPeriods(DateTime dateFrom, DateTime dateTo)
        {
            DataTable lDataTable = Db.SetSpCommand(Constants.spLimitPeriodList,
            Db.Parameter(Constants.spLimitPeriodList_DateFrom, dateFrom),
            Db.Parameter(Constants.spLimitPeriodList_DateTo, dateTo)).ExecuteDataTable();
            
            return lDataTable;
        }
        
        /// <summary>
        /// Gets the main report data.
        /// </summary>
        /// <returns>The main report data.</returns>
        public static DataTable GetMainReportData(ReportInfo settings)
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.spGetReportData, new[] { 
                new SqlParameter(Constants.spGetReportData_StartDate, settings.DateFrom),
                new SqlParameter(Constants.spGetReportData_EndDate, settings.DateTo),
                new SqlParameter(Constants.spGetReportData_StaffIds, XmlHelper.GetXmlDescription(settings.StaffingsIds)),
                new SqlParameter(Constants.spGetReportData_ActionIds, XmlHelper.GetXmlDescription(settings.ActionIds)),
                new SqlParameter(Constants.spGetReportData_PopIds, XmlHelper.GetXmlDescription(settings.PosIds)),
                new SqlParameter(Constants.spGetReportData_Details_Orders, settings.DetalisationByOrders),
                new SqlParameter(Constants.spGetReportData_Details_SKU, settings.DetalisationBySKUs),
                new SqlParameter(Constants.spGetReportData_Details_Action_SKU_, settings.DetalisationByActionSKUs)
                });

            if (dataSet != null)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }
    }
}
