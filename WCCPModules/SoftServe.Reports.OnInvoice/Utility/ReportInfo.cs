﻿using System;
using System.Collections.Generic;

namespace SoftServe.Reports.OnInvoice
{
    /// <summary>
    /// Describes the report settings
    /// </summary>
    public class ReportInfo
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        public List<string> ActionIds { get; set; }

        public List<string> RegionIds { get; set; }
        public List<string> StaffingsIds { get; set; }

        public List<string> PosIds { get; set; }
        public List<string> DistrIds { get; set; }
        
        public bool DetalisationByOrders { get; set; }
        public bool DetalisationBySKUs { get; set; }
        public bool DetalisationByActionSKUs { get; set; }
    }
}
