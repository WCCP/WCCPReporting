﻿namespace SoftServe.Reports.OnInvoice.UserControls
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colM5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOLCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOlJurName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOlFactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOlJurAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOlFactAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLimit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLimitEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLimitStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountPercForPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSkuCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSumWithOutDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSumWithDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountPer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLimitLeft = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLimitLeftDistr = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rTextEdit});
            this.gridControl.Size = new System.Drawing.Size(900, 500);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 45;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colM5,
            this.colM4,
            this.colM3,
            this.colM2,
            this.colM1,
            this.colINN,
            this.colOLCode,
            this.colOlJurName,
            this.colOlFactName,
            this.colOlJurAdress,
            this.colOlFactAdress,
            this.colTerritory,
            this.colRegion,
            this.colDistr,
            this.colPOS,
            this.colActionName,
            this.colDateFrom,
            this.colDateTo,
            this.colLimit,
            this.colLimitEndDate,
            this.colLimitStartDate,
            this.colDiscountPercForPeriod,
            this.colOrderNumber,
            this.colOrderDate,
            this.colSkuCode,
            this.colSku,
            this.colVolume,
            this.colSumWithOutDiscount,
            this.colSumWithDiscount,
            this.colDiscount,
            this.colDiscountPer,
            this.colLimitLeft,
            this.colLimitLeftDistr});
            this.gridView.GridControl = this.gridControl;
            this.gridView.GroupFormat = "{0}: {1}";
            this.gridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "UpliftPct", null, "{0:N2}", "2"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftDal", null, "{0:N3}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftMACO", null, "{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "AvgMACO", null, "{0:N3}", "7"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SalesDal_Promo", null, "{0:N3}")});
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsFilter.ColumnFilterPopupRowCount = 30;
            this.gridView.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Дата с  _  - , Дата по  _ ";
            this.gridView.ShowCustomizationForm += new System.EventHandler(this.gridView_ShowCustomizationForm);
            // 
            // colM5
            // 
            this.colM5.Caption = "М5";
            this.colM5.FieldName = "M5";
            this.colM5.Name = "colM5";
            this.colM5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM5.Visible = true;
            this.colM5.VisibleIndex = 0;
            this.colM5.Width = 120;
            // 
            // colM4
            // 
            this.colM4.Caption = "М4";
            this.colM4.FieldName = "M4";
            this.colM4.Name = "colM4";
            this.colM4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM4.Visible = true;
            this.colM4.VisibleIndex = 1;
            this.colM4.Width = 120;
            // 
            // colM3
            // 
            this.colM3.Caption = "М3";
            this.colM3.FieldName = "M3";
            this.colM3.Name = "colM3";
            this.colM3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3.Visible = true;
            this.colM3.VisibleIndex = 2;
            this.colM3.Width = 120;
            // 
            // colM2
            // 
            this.colM2.Caption = "М2";
            this.colM2.FieldName = "M2";
            this.colM2.Name = "colM2";
            this.colM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2.Visible = true;
            this.colM2.VisibleIndex = 3;
            this.colM2.Width = 120;
            // 
            // colM1
            // 
            this.colM1.Caption = "М1";
            this.colM1.FieldName = "M1";
            this.colM1.Name = "colM1";
            this.colM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1.Visible = true;
            this.colM1.VisibleIndex = 4;
            this.colM1.Width = 120;
            // 
            // colINN
            // 
            this.colINN.Caption = "ИНН";
            this.colINN.ColumnEdit = this.rTextEdit;
            this.colINN.FieldName = "unn";
            this.colINN.Name = "colINN";
            this.colINN.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colINN.Visible = true;
            this.colINN.VisibleIndex = 5;
            this.colINN.Width = 120;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // colOLCode
            // 
            this.colOLCode.Caption = "Код ТТ";
            this.colOLCode.ColumnEdit = this.rTextEdit;
            this.colOLCode.FieldName = "ol_id";
            this.colOLCode.Name = "colOLCode";
            this.colOLCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLCode.Visible = true;
            this.colOLCode.VisibleIndex = 6;
            this.colOLCode.Width = 120;
            // 
            // colOlJurName
            // 
            this.colOlJurName.Caption = "Юридическое название ТТ";
            this.colOlJurName.FieldName = "OLTradingName";
            this.colOlJurName.Name = "colOlJurName";
            this.colOlJurName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOlJurName.Visible = true;
            this.colOlJurName.VisibleIndex = 7;
            this.colOlJurName.Width = 150;
            // 
            // colOlFactName
            // 
            this.colOlFactName.Caption = "Фактическое название ТТ";
            this.colOlFactName.FieldName = "OLName";
            this.colOlFactName.Name = "colOlFactName";
            this.colOlFactName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOlFactName.Visible = true;
            this.colOlFactName.VisibleIndex = 8;
            this.colOlFactName.Width = 150;
            // 
            // colOlJurAdress
            // 
            this.colOlJurAdress.Caption = "Юридический адрес ТТ";
            this.colOlJurAdress.FieldName = "OLDeliveryAddress";
            this.colOlJurAdress.Name = "colOlJurAdress";
            this.colOlJurAdress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOlJurAdress.Visible = true;
            this.colOlJurAdress.VisibleIndex = 9;
            this.colOlJurAdress.Width = 150;
            // 
            // colOlFactAdress
            // 
            this.colOlFactAdress.Caption = "Фактический адрес ТТ";
            this.colOlFactAdress.FieldName = "OLAddress";
            this.colOlFactAdress.Name = "colOlFactAdress";
            this.colOlFactAdress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOlFactAdress.Visible = true;
            this.colOlFactAdress.VisibleIndex = 10;
            this.colOlFactAdress.Width = 150;
            // 
            // colTerritory
            // 
            this.colTerritory.Caption = "Территория";
            this.colTerritory.FieldName = "Teritoty";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTerritory.Visible = true;
            this.colTerritory.VisibleIndex = 11;
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "Region";
            this.colRegion.Name = "colRegion";
            this.colRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 12;
            // 
            // colDistr
            // 
            this.colDistr.Caption = "Дистрибьютор";
            this.colDistr.FieldName = "Distr_name";
            this.colDistr.Name = "colDistr";
            this.colDistr.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistr.Visible = true;
            this.colDistr.VisibleIndex = 13;
            this.colDistr.Width = 100;
            // 
            // colPOS
            // 
            this.colPOS.Caption = "ТС";
            this.colPOS.FieldName = "cust_name";
            this.colPOS.Name = "colPOS";
            this.colPOS.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOS.Visible = true;
            this.colPOS.VisibleIndex = 14;
            this.colPOS.Width = 212;
            // 
            // colActionName
            // 
            this.colActionName.Caption = "Название акции";
            this.colActionName.FieldName = "Aim_name";
            this.colActionName.Name = "colActionName";
            this.colActionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActionName.Visible = true;
            this.colActionName.VisibleIndex = 15;
            this.colActionName.Width = 185;
            // 
            // colDateFrom
            // 
            this.colDateFrom.Caption = "Период с";
            this.colDateFrom.FieldName = "Action_Start";
            this.colDateFrom.Name = "colDateFrom";
            this.colDateFrom.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDateFrom.Visible = true;
            this.colDateFrom.VisibleIndex = 16;
            this.colDateFrom.Width = 80;
            // 
            // colDateTo
            // 
            this.colDateTo.Caption = "Период по";
            this.colDateTo.FieldName = "Action_End";
            this.colDateTo.Name = "colDateTo";
            this.colDateTo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDateTo.Visible = true;
            this.colDateTo.VisibleIndex = 17;
            this.colDateTo.Width = 80;
            // 
            // colLimit
            // 
            this.colLimit.Caption = "Лимит";
            this.colLimit.DisplayFormat.FormatString = "n2";
            this.colLimit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLimit.FieldName = "Limit";
            this.colLimit.Name = "colLimit";
            this.colLimit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLimit.Visible = true;
            this.colLimit.VisibleIndex = 18;
            this.colLimit.Width = 90;
            // 
            // colLimitEndDate
            // 
            this.colLimitEndDate.Caption = "Дата начала действия лимита и % скидки";
            this.colLimitEndDate.FieldName = "DateFrom";
            this.colLimitEndDate.Name = "colLimitEndDate";
            this.colLimitEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLimitEndDate.Visible = true;
            this.colLimitEndDate.VisibleIndex = 19;
            this.colLimitEndDate.Width = 80;
            // 
            // colLimitStartDate
            // 
            this.colLimitStartDate.Caption = "Дата окончания действия лимита и % скидки";
            this.colLimitStartDate.FieldName = "DateTo";
            this.colLimitStartDate.Name = "colLimitStartDate";
            this.colLimitStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLimitStartDate.Visible = true;
            this.colLimitStartDate.VisibleIndex = 20;
            this.colLimitStartDate.Width = 80;
            // 
            // colDiscountPercForPeriod
            // 
            this.colDiscountPercForPeriod.Caption = "Процент скидки (%)";
            this.colDiscountPercForPeriod.DisplayFormat.FormatString = "n2";
            this.colDiscountPercForPeriod.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscountPercForPeriod.FieldName = "LimitPercent";
            this.colDiscountPercForPeriod.Name = "colDiscountPercForPeriod";
            this.colDiscountPercForPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDiscountPercForPeriod.Visible = true;
            this.colDiscountPercForPeriod.VisibleIndex = 21;
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.Caption = "№ накладной";
            this.colOrderNumber.FieldName = "OrderNo";
            this.colOrderNumber.Name = "colOrderNumber";
            this.colOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOrderNumber.Visible = true;
            this.colOrderNumber.VisibleIndex = 22;
            this.colOrderNumber.Width = 135;
            // 
            // colOrderDate
            // 
            this.colOrderDate.Caption = "Дата накладной";
            this.colOrderDate.FieldName = "date";
            this.colOrderDate.Name = "colOrderDate";
            this.colOrderDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOrderDate.Visible = true;
            this.colOrderDate.VisibleIndex = 23;
            this.colOrderDate.Width = 80;
            // 
            // colSkuCode
            // 
            this.colSkuCode.Caption = "Код СКЮ";
            this.colSkuCode.FieldName = "ProductCN_ID";
            this.colSkuCode.Name = "colSkuCode";
            this.colSkuCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSkuCode.Visible = true;
            this.colSkuCode.VisibleIndex = 24;
            // 
            // colSku
            // 
            this.colSku.Caption = "СКЮ";
            this.colSku.FieldName = "ProductCnName";
            this.colSku.Name = "colSku";
            this.colSku.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSku.Visible = true;
            this.colSku.VisibleIndex = 25;
            // 
            // colVolume
            // 
            this.colVolume.Caption = "Обьем, дал";
            this.colVolume.DisplayFormat.FormatString = "n3";
            this.colVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colVolume.FieldName = "SaleDal";
            this.colVolume.Name = "colVolume";
            this.colVolume.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colVolume.Visible = true;
            this.colVolume.VisibleIndex = 26;
            this.colVolume.Width = 109;
            // 
            // colSumWithOutDiscount
            // 
            this.colSumWithOutDiscount.Caption = "Сумма без скидки";
            this.colSumWithOutDiscount.DisplayFormat.FormatString = "n2";
            this.colSumWithOutDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumWithOutDiscount.FieldName = "SumWithOutDiscount";
            this.colSumWithOutDiscount.Name = "colSumWithOutDiscount";
            this.colSumWithOutDiscount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSumWithOutDiscount.Visible = true;
            this.colSumWithOutDiscount.VisibleIndex = 27;
            this.colSumWithOutDiscount.Width = 103;
            // 
            // colSumWithDiscount
            // 
            this.colSumWithDiscount.Caption = "Сумма со скидкой";
            this.colSumWithDiscount.DisplayFormat.FormatString = "n2";
            this.colSumWithDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumWithDiscount.FieldName = "SumWithDiscount";
            this.colSumWithDiscount.Name = "colSumWithDiscount";
            this.colSumWithDiscount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSumWithDiscount.Visible = true;
            this.colSumWithDiscount.VisibleIndex = 28;
            this.colSumWithDiscount.Width = 102;
            // 
            // colDiscount
            // 
            this.colDiscount.Caption = "Скидка";
            this.colDiscount.DisplayFormat.FormatString = "n2";
            this.colDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 29;
            // 
            // colDiscountPer
            // 
            this.colDiscountPer.Caption = "% Скидки";
            this.colDiscountPer.DisplayFormat.FormatString = "n2";
            this.colDiscountPer.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscountPer.FieldName = "PercentDiscount";
            this.colDiscountPer.Name = "colDiscountPer";
            this.colDiscountPer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDiscountPer.Visible = true;
            this.colDiscountPer.VisibleIndex = 30;
            // 
            // colLimitLeft
            // 
            this.colLimitLeft.Caption = "Остаток лимита";
            this.colLimitLeft.DisplayFormat.FormatString = "n2";
            this.colLimitLeft.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLimitLeft.FieldName = "LimitRest";
            this.colLimitLeft.Name = "colLimitLeft";
            this.colLimitLeft.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLimitLeft.Visible = true;
            this.colLimitLeft.VisibleIndex = 31;
            this.colLimitLeft.Width = 96;
            // 
            // colLimitLeftDistr
            // 
            this.colLimitLeftDistr.Caption = "Остаток лимита Дистрибьютор";
            this.colLimitLeftDistr.DisplayFormat.FormatString = "n2";
            this.colLimitLeftDistr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLimitLeftDistr.FieldName = "LimitDistr";
            this.colLimitLeftDistr.Name = "colLimitLeftDistr";
            this.colLimitLeftDistr.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLimitLeftDistr.Visible = true;
            this.colLimitLeftDistr.VisibleIndex = 32;
            this.colLimitLeftDistr.Width = 145;
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(900, 500);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gridControl;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colM5;
        private DevExpress.XtraGrid.Columns.GridColumn colM4;
        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraGrid.Columns.GridColumn colM2;
        private DevExpress.XtraGrid.Columns.GridColumn colM1;
        private DevExpress.XtraGrid.Columns.GridColumn colINN;
        private DevExpress.XtraGrid.Columns.GridColumn colOLCode;
        private DevExpress.XtraGrid.Columns.GridColumn colOlJurName;
        private DevExpress.XtraGrid.Columns.GridColumn colOlFactName;
        private DevExpress.XtraGrid.Columns.GridColumn colOlJurAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colOlFactAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colDistr;
        private DevExpress.XtraGrid.Columns.GridColumn colPOS;
        private DevExpress.XtraGrid.Columns.GridColumn colActionName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTo;
        private DevExpress.XtraGrid.Columns.GridColumn colLimit;
        private DevExpress.XtraGrid.Columns.GridColumn colLimitEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLimitStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSkuCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSku;
        private DevExpress.XtraGrid.Columns.GridColumn colVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colSumWithOutDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colSumWithDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountPer;
        private DevExpress.XtraGrid.Columns.GridColumn colLimitLeft;
        private DevExpress.XtraGrid.Columns.GridColumn colLimitLeftDistr;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountPercForPeriod;

    }
}
