﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using SoftServe.Reports.OnInvoice.Tabs;


namespace SoftServe.Reports.OnInvoice.UserControls
{
    [ToolboxItem(false)]
    public partial class MainControl : UserControl
    {
        /// <summary>
        /// Is used for storing column width before view modification before export
        /// </summary>
        private List<int> _currentColumnsConf = new List<int>();

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl(ReportInfo settings)
        {
            InitializeComponent();

            gridView.ViewCaption = string.Format("Дата    с   {0}      по   {1}",
                                                     settings.DateFrom.Date.ToString("dd.MM.yyyy"),
                                                     settings.DateTo.Date.ToString("dd.MM.yyyy"));

            colOrderDate.Visible = settings.DetalisationByOrders;
            colOrderDate.OptionsColumn.ShowInCustomizationForm = settings.DetalisationByOrders;

            colOrderNumber.Visible = settings.DetalisationByOrders;
            colOrderNumber.OptionsColumn.ShowInCustomizationForm = settings.DetalisationByOrders;


            colSku.Visible = settings.DetalisationBySKUs;
            colSku.OptionsColumn.ShowInCustomizationForm = settings.DetalisationBySKUs;

            colSkuCode.Visible = settings.DetalisationBySKUs;
            colSkuCode.OptionsColumn.ShowInCustomizationForm = settings.DetalisationBySKUs;
        }

        /// <summary>
        /// Gets or sets the grid datasource
        /// </summary>
        public DataTable DataSource
        {
            get
            {
                return gridControl.DataSource as DataTable;
            }

            set
            {
                gridControl.BeginUpdate();
                gridControl.DataSource = value;
                gridControl.EndUpdate();
            }
        }

        /// <summary>
        /// Changes view in order to export it properly 
        /// </summary>
        internal void PrepareViewToExport()
        {
            //turn off view caption
            gridView.OptionsView.ShowViewCaption = false;

            //turn off formatting
            foreach (GridColumn col in gridView.Columns)
            {
                col.DisplayFormat.FormatString = string.Empty;
            }

            //save column conf
            _currentColumnsConf.Clear();

            foreach (GridColumn column in gridView.Columns)
            {
                _currentColumnsConf.Add(column.Width);
            }

            //tune-up columns width
            gridView.BestFitColumns();

            //for custom header display (in xls file)
            if (gridView.VisibleColumns.Count >= 2)
            {
                gridView.VisibleColumns[0].Width = Math.Max(MainTab.HEADER_WIDTH, gridView.VisibleColumns[0].Width);
                gridView.VisibleColumns[1].Width = Math.Max(MainTab.HEADER_WIDTH, gridView.VisibleColumns[1].Width);
            }
        }

        /// <summary>
        /// Restores view state after export 
        /// </summary>
        internal void RestoreViewAfterExport()
        {
            //turn on view caption
            gridView.OptionsView.ShowViewCaption = true;

            // turn on formatting
            foreach (GridColumn col in gridView.Columns)
            {
                if (col == colVolume)
                {
                    col.DisplayFormat.FormatString = "N3";
                }
                else
                {
                    if (col.DisplayFormat.FormatType == FormatType.Numeric)
                    {
                        col.DisplayFormat.FormatString = "N2";
                    }
                }
            }

            //restore columns width
            for (int i = 0; i < _currentColumnsConf.Count; i++)
            {
                gridView.Columns[i].Width = _currentColumnsConf[i];
            }
        }

        /// <summary>
        /// Handles ShowCustomizationForm event - is needed to show form over Report, not beneath
        /// </summary>
        /// <param name="sender">gridView</param>
        /// <param name="e">EventArgs</param>
        private void gridView_ShowCustomizationForm(object sender, EventArgs e)
        {
            gridView.CustomizationForm.TopMost = true;
        }
    }
}
