﻿using Logica.Reports.BaseReportControl.CommonControls;

namespace SoftServe.Reports.OnInvoice.Forms
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bGenerateReport = new DevExpress.XtraEditors.SimpleButton();
            this.bCancel = new DevExpress.XtraEditors.SimpleButton();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.lDistr = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.tTerritory = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colTerritory = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cluDistr = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrFieldParentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbPos = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.component1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gcByTerritory = new DevExpress.XtraEditors.GroupControl();
            this.gcTerritory = new DevExpress.XtraEditors.GroupControl();
            this.gcStaff = new DevExpress.XtraEditors.GroupControl();
            this.tStaffing = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colPersName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPersParendId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPersStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPersStaffIdReal = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cStaffing = new DevExpress.XtraEditors.CheckEdit();
            this.gcAction = new DevExpress.XtraEditors.GroupControl();
            this.mLimitPeriods = new DevExpress.XtraEditors.MemoEdit();
            this.lLimitPeriods = new DevExpress.XtraEditors.LabelControl();
            this.lAction = new DevExpress.XtraEditors.LabelControl();
            this.cbActions = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.cDetalisationByActionSkus = new DevExpress.XtraEditors.CheckEdit();
            this.cDetalisationByOrders = new DevExpress.XtraEditors.CheckEdit();
            this.cDetalisationBySkus = new DevExpress.XtraEditors.CheckEdit();
            this.dFrom = new DevExpress.XtraEditors.DateEdit();
            this.dTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluDistr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcByTerritory)).BeginInit();
            this.gcByTerritory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerritory)).BeginInit();
            this.gcTerritory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcStaff)).BeginInit();
            this.gcStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAction)).BeginInit();
            this.gcAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mLimitPeriods.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalisationByActionSkus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalisationByOrders.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalisationBySkus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // bGenerateReport
            // 
            this.bGenerateReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bGenerateReport.Image = ((System.Drawing.Image)(resources.GetObject("bGenerateReport.Image")));
            this.bGenerateReport.Location = new System.Drawing.Point(595, 388);
            this.bGenerateReport.Name = "bGenerateReport";
            this.bGenerateReport.Size = new System.Drawing.Size(146, 23);
            this.bGenerateReport.TabIndex = 0;
            this.bGenerateReport.Text = "Сгенерировать отчет";
            this.bGenerateReport.Click += new System.EventHandler(this.generateReportButton_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Image = ((System.Drawing.Image)(resources.GetObject("bCancel.Image")));
            this.bCancel.Location = new System.Drawing.Point(746, 388);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(86, 23);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "Отмена";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Appearance.Options.UseFont = true;
            this.lblUpd2.Location = new System.Drawing.Point(77, 391);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(0, 13);
            this.lblUpd2.TabIndex = 22;
            // 
            // lDistr
            // 
            this.lDistr.Location = new System.Drawing.Point(7, 26);
            this.lDistr.Name = "lDistr";
            this.lDistr.Size = new System.Drawing.Size(80, 13);
            this.lDistr.TabIndex = 30;
            this.lDistr.Text = "Дистрибьютор:";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(7, 68);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(17, 13);
            this.labelControl15.TabIndex = 34;
            this.labelControl15.Text = "ТС:";
            // 
            // tTerritory
            // 
            this.tTerritory.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colTerritory});
            this.tTerritory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tTerritory.KeyFieldName = "GeographyID";
            this.tTerritory.Location = new System.Drawing.Point(2, 22);
            this.tTerritory.Name = "tTerritory";
            this.tTerritory.Size = new System.Drawing.Size(274, 197);
            this.tTerritory.TabIndex = 77;
            this.tTerritory.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tTerritory_AfterNodeChecked);
            // 
            // colTerritory
            // 
            this.colTerritory.Caption = "Территория";
            this.colTerritory.CustomizationCaption = "Территория";
            this.colTerritory.FieldName = "GeographyName";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsColumn.AllowEdit = false;
            this.colTerritory.OptionsColumn.ReadOnly = true;
            this.colTerritory.Visible = true;
            this.colTerritory.VisibleIndex = 0;
            // 
            // cluDistr
            // 
            this.cluDistr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cluDistr.Delimiter = ", ";
            this.cluDistr.EditValue = "";
            this.cluDistr.Location = new System.Drawing.Point(4, 41);
            this.cluDistr.Name = "cluDistr";
            this.cluDistr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cluDistr.Properties.DisplayMember = "FieldName";
            this.cluDistr.Properties.NullText = "";
            this.cluDistr.Properties.ValueMember = "FIeldCode";
            this.cluDistr.Properties.View = this.gridView1;
            this.cluDistr.Size = new System.Drawing.Size(268, 20);
            this.cluDistr.TabIndex = 100;
            this.cluDistr.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbDistr_Closed);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.colDistrFieldParentID});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Код";
            this.gridColumn4.CustomizationCaption = "Код";
            this.gridColumn4.FieldName = "FIeldCode";
            this.gridColumn4.MaxWidth = 100;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowShowHide = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Название дистрибьютора";
            this.gridColumn5.CustomizationCaption = "Название дистрибьютора";
            this.gridColumn5.FieldName = "FieldName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowShowHide = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Id";
            this.gridColumn6.FieldName = "FieldID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowShowHide = false;
            this.gridColumn6.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Level";
            this.gridColumn7.FieldName = "Level";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowShowHide = false;
            this.gridColumn7.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDistrFieldParentID
            // 
            this.colDistrFieldParentID.Caption = "ParentId";
            this.colDistrFieldParentID.FieldName = "FieldParentID";
            this.colDistrFieldParentID.Name = "colDistrFieldParentID";
            this.colDistrFieldParentID.OptionsColumn.AllowShowHide = false;
            this.colDistrFieldParentID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cbPos
            // 
            this.cbPos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPos.Delimiter = ",";
            this.cbPos.Location = new System.Drawing.Point(4, 84);
            this.cbPos.Name = "cbPos";
            this.cbPos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPos.Properties.DisplayMember = "FieldName";
            this.cbPos.Properties.DropDownRows = 15;
            this.cbPos.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbPos.Properties.ShowButtons = false;
            this.cbPos.Properties.ValueMember = "FieldCode";
            this.cbPos.Size = new System.Drawing.Size(268, 20);
            this.cbPos.TabIndex = 99;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // component1View
            // 
            this.component1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.component1View.Name = "component1View";
            this.component1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.component1View.OptionsView.ShowGroupPanel = false;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Дислокация";
            this.treeListColumn2.CustomizationCaption = "Дислокация";
            this.treeListColumn2.FieldName = "GeographyName";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // gcByTerritory
            // 
            this.gcByTerritory.Controls.Add(this.cbPos);
            this.gcByTerritory.Controls.Add(this.labelControl15);
            this.gcByTerritory.Controls.Add(this.cluDistr);
            this.gcByTerritory.Controls.Add(this.lDistr);
            this.gcByTerritory.Location = new System.Drawing.Point(288, 264);
            this.gcByTerritory.Margin = new System.Windows.Forms.Padding(0);
            this.gcByTerritory.Name = "gcByTerritory";
            this.gcByTerritory.Size = new System.Drawing.Size(276, 110);
            this.gcByTerritory.TabIndex = 80;
            this.gcByTerritory.Text = "По территории";
            this.gcByTerritory.CustomDrawCaption += new DevExpress.XtraEditors.GroupCaptionCustomDrawEventHandler(this.groupControl_CustomDrawCaption);
            // 
            // gcTerritory
            // 
            this.gcTerritory.Controls.Add(this.tTerritory);
            this.gcTerritory.Location = new System.Drawing.Point(287, 41);
            this.gcTerritory.Margin = new System.Windows.Forms.Padding(0);
            this.gcTerritory.Name = "gcTerritory";
            this.gcTerritory.Size = new System.Drawing.Size(278, 221);
            this.gcTerritory.TabIndex = 0;
            this.gcTerritory.Text = "Территория";
            this.gcTerritory.CustomDrawCaption += new DevExpress.XtraEditors.GroupCaptionCustomDrawEventHandler(this.groupControl_CustomDrawCaption);
            // 
            // gcStaff
            // 
            this.gcStaff.Controls.Add(this.tStaffing);
            this.gcStaff.Controls.Add(this.cStaffing);
            this.gcStaff.Location = new System.Drawing.Point(568, 41);
            this.gcStaff.Margin = new System.Windows.Forms.Padding(0);
            this.gcStaff.Name = "gcStaff";
            this.gcStaff.Size = new System.Drawing.Size(265, 332);
            this.gcStaff.TabIndex = 1;
            this.gcStaff.Text = "Персонал";
            this.gcStaff.CustomDrawCaption += new DevExpress.XtraEditors.GroupCaptionCustomDrawEventHandler(this.groupControl_CustomDrawCaption);
            // 
            // tStaffing
            // 
            this.tStaffing.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colPersName,
            this.colPersParendId,
            this.colPersStaffId,
            this.colPersStaffIdReal});
            this.tStaffing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tStaffing.KeyFieldName = "wStaffID";
            this.tStaffing.Location = new System.Drawing.Point(2, 22);
            this.tStaffing.Name = "tStaffing";
            this.tStaffing.ParentFieldName = "wParentID";
            this.tStaffing.Size = new System.Drawing.Size(261, 308);
            this.tStaffing.TabIndex = 82;
            // 
            // colPersName
            // 
            this.colPersName.Caption = "Персонал";
            this.colPersName.CustomizationCaption = "Территория";
            this.colPersName.FieldName = "Name";
            this.colPersName.Name = "colPersName";
            this.colPersName.OptionsColumn.AllowEdit = false;
            this.colPersName.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersName.OptionsColumn.ReadOnly = true;
            this.colPersName.Visible = true;
            this.colPersName.VisibleIndex = 0;
            // 
            // colPersParendId
            // 
            this.colPersParendId.Caption = "wParentID";
            this.colPersParendId.FieldName = "wParentID";
            this.colPersParendId.Name = "colPersParendId";
            this.colPersParendId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersParendId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colPersStaffId
            // 
            this.colPersStaffId.Caption = "wStaffID";
            this.colPersStaffId.FieldName = "wStaffID";
            this.colPersStaffId.Name = "colPersStaffId";
            this.colPersStaffId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersStaffId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colPersStaffIdReal
            // 
            this.colPersStaffIdReal.Caption = "StaffID";
            this.colPersStaffIdReal.FieldName = "StaffID";
            this.colPersStaffIdReal.Name = "colPersStaffIdReal";
            this.colPersStaffIdReal.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPersStaffIdReal.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cStaffing
            // 
            this.cStaffing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cStaffing.EditValue = true;
            this.cStaffing.Location = new System.Drawing.Point(217, 3);
            this.cStaffing.Name = "cStaffing";
            this.cStaffing.Properties.AllowGrayed = true;
            this.cStaffing.Properties.Caption = "Все";
            this.cStaffing.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cStaffing.Size = new System.Drawing.Size(43, 19);
            this.cStaffing.TabIndex = 84;
            this.cStaffing.TabStop = false;
            // 
            // gcAction
            // 
            this.gcAction.Controls.Add(this.mLimitPeriods);
            this.gcAction.Controls.Add(this.lLimitPeriods);
            this.gcAction.Controls.Add(this.lAction);
            this.gcAction.Controls.Add(this.cbActions);
            this.gcAction.Location = new System.Drawing.Point(10, 41);
            this.gcAction.Name = "gcAction";
            this.gcAction.Size = new System.Drawing.Size(275, 266);
            this.gcAction.TabIndex = 81;
            this.gcAction.Text = "Акция";
            this.gcAction.CustomDrawCaption += new DevExpress.XtraEditors.GroupCaptionCustomDrawEventHandler(this.groupControl_CustomDrawCaption);
            // 
            // mLimitPeriods
            // 
            this.mLimitPeriods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mLimitPeriods.EditValue = "";
            this.mLimitPeriods.Location = new System.Drawing.Point(5, 80);
            this.mLimitPeriods.Name = "mLimitPeriods";
            this.mLimitPeriods.Properties.ReadOnly = true;
            this.mLimitPeriods.Size = new System.Drawing.Size(265, 181);
            this.mLimitPeriods.TabIndex = 103;
            // 
            // lLimitPeriods
            // 
            this.lLimitPeriods.Location = new System.Drawing.Point(8, 65);
            this.lLimitPeriods.Name = "lLimitPeriods";
            this.lLimitPeriods.Size = new System.Drawing.Size(169, 13);
            this.lLimitPeriods.TabIndex = 102;
            this.lLimitPeriods.Text = "Периоды Лимитов суммы скидки:";
            // 
            // lAction
            // 
            this.lAction.Location = new System.Drawing.Point(8, 23);
            this.lAction.Name = "lAction";
            this.lAction.Size = new System.Drawing.Size(35, 13);
            this.lAction.TabIndex = 101;
            this.lAction.Text = "Акция:";
            // 
            // cbActions
            // 
            this.cbActions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbActions.Delimiter = ",";
            this.cbActions.Location = new System.Drawing.Point(5, 39);
            this.cbActions.Name = "cbActions";
            this.cbActions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbActions.Properties.DisplayMember = "Action_Name";
            this.cbActions.Properties.DropDownRows = 15;
            this.cbActions.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbActions.Properties.ShowButtons = false;
            this.cbActions.Properties.ValueMember = "Action_ID";
            this.cbActions.Size = new System.Drawing.Size(265, 20);
            this.cbActions.TabIndex = 100;
            this.cbActions.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbActions_Closed);
            // 
            // cDetalisationByActionSkus
            // 
            this.cDetalisationByActionSkus.Enabled = false;
            this.cDetalisationByActionSkus.Location = new System.Drawing.Point(10, 351);
            this.cDetalisationByActionSkus.Name = "cDetalisationByActionSkus";
            this.cDetalisationByActionSkus.Properties.Caption = "Детализация информации по акционным СКЮ ";
            this.cDetalisationByActionSkus.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDetalisationByActionSkus.Size = new System.Drawing.Size(270, 19);
            this.cDetalisationByActionSkus.TabIndex = 82;
            // 
            // cDetalisationByOrders
            // 
            this.cDetalisationByOrders.Location = new System.Drawing.Point(10, 313);
            this.cDetalisationByOrders.Name = "cDetalisationByOrders";
            this.cDetalisationByOrders.Properties.Caption = "Детализация информации по накладным ";
            this.cDetalisationByOrders.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDetalisationByOrders.Size = new System.Drawing.Size(270, 19);
            this.cDetalisationByOrders.TabIndex = 83;
            // 
            // cDetalisationBySkus
            // 
            this.cDetalisationBySkus.Location = new System.Drawing.Point(10, 333);
            this.cDetalisationBySkus.Name = "cDetalisationBySkus";
            this.cDetalisationBySkus.Properties.Caption = "Детализация информации по СКЮ ";
            this.cDetalisationBySkus.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDetalisationBySkus.Size = new System.Drawing.Size(270, 19);
            this.cDetalisationBySkus.TabIndex = 84;
            this.cDetalisationBySkus.CheckedChanged += new System.EventHandler(this.cDetalisationBySkus_CheckedChanged);
            // 
            // dFrom
            // 
            this.dFrom.EditValue = null;
            this.dFrom.Location = new System.Drawing.Point(152, 9);
            this.dFrom.Name = "dFrom";
            this.dFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dFrom.Size = new System.Drawing.Size(100, 20);
            this.dFrom.TabIndex = 85;
            // 
            // dTo
            // 
            this.dTo.EditValue = null;
            this.dTo.Location = new System.Drawing.Point(296, 9);
            this.dTo.Name = "dTo";
            this.dTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dTo.Size = new System.Drawing.Size(100, 20);
            this.dTo.TabIndex = 86;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(278, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(12, 13);
            this.labelControl1.TabIndex = 101;
            this.labelControl1.Text = "по";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(39, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(107, 13);
            this.labelControl3.TabIndex = 103;
            this.labelControl3.Text = "Период времени:     с";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(843, 421);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.dTo);
            this.Controls.Add(this.dFrom);
            this.Controls.Add(this.cDetalisationByActionSkus);
            this.Controls.Add(this.cDetalisationByOrders);
            this.Controls.Add(this.cDetalisationBySkus);
            this.Controls.Add(this.gcAction);
            this.Controls.Add(this.gcByTerritory);
            this.Controls.Add(this.gcTerritory);
            this.Controls.Add(this.gcStaff);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bGenerateReport);
            this.Controls.Add(this.lblUpd2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры к отчету \"On-Invoice\"";
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluDistr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcByTerritory)).EndInit();
            this.gcByTerritory.ResumeLayout(false);
            this.gcByTerritory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerritory)).EndInit();
            this.gcTerritory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcStaff)).EndInit();
            this.gcStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAction)).EndInit();
            this.gcAction.ResumeLayout(false);
            this.gcAction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mLimitPeriods.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalisationByActionSkus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalisationByOrders.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalisationBySkus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.SimpleButton bGenerateReport;
        private DevExpress.XtraEditors.SimpleButton bCancel;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraEditors.LabelControl lDistr;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.Grid.GridView component1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tTerritory;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTerritory;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private ComboboxExtended cbPos;
        private GridLookUpWithMultipleChecking cluDistr;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrFieldParentID;
        private DevExpress.XtraEditors.GroupControl gcByTerritory;
        private DevExpress.XtraEditors.GroupControl gcTerritory;
        private DevExpress.XtraEditors.GroupControl gcStaff;
        private DevExpress.XtraEditors.CheckEdit cStaffing;
        private TreeListIncremental tStaffing;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersParendId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPersStaffIdReal;
        private DevExpress.XtraEditors.GroupControl gcAction;
        private DevExpress.XtraEditors.LabelControl lLimitPeriods;
        private DevExpress.XtraEditors.LabelControl lAction;
        private ComboboxExtended cbActions;
        private DevExpress.XtraEditors.MemoEdit mLimitPeriods;
        private DevExpress.XtraEditors.CheckEdit cDetalisationByActionSkus;
        private DevExpress.XtraEditors.CheckEdit cDetalisationByOrders;
        private DevExpress.XtraEditors.CheckEdit cDetalisationBySkus;
        private DevExpress.XtraEditors.DateEdit dFrom;
        private DevExpress.XtraEditors.DateEdit dTo;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
	}
}