﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.OnInvoice.DataProviders;

namespace SoftServe.Reports.OnInvoice.Forms
{
    public partial class SettingsForm : XtraForm
    {
        #region Constants

        private const char STRING_DELIMITER = ',';

        private const string DumbFilter = "-17";

        private const string FilterByLevel = "Level = {0}";
        private const string FilterByLevelLess = "Level < {0}";
        private const string AndFilterByColumn = " and {0} in ({1})";
        private const string FilterByColumn = "{0} in ({1})";

        private const string Level = "Level";

        private const string PosParentIdField = "FieldParentID";
        private const string DistrWorkSchema = "SchemaOfWork";

        #endregion

        #region Private Fields

        private int DistributorsLevel;
        private int PosLevel;

        private DataTable distributors;
        private DataTable limits;
        private DataTable staff;

        #endregion

        /// <summary>
        /// Creates the new instance od <c>SettingsForm</c>/>
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();

            SetDateIntervals();

            InitDataSources();
            ReloadStaffData();
            ReloadActionsData();

            SetInitialSettings();

            dFrom.DateTimeChanged += date_EditValueChanged;
            dTo.DateTimeChanged += date_EditValueChanged;
        }

        /// <summary>
        /// Performs gathering of settings info
        /// </summary>
        /// <returns>Current setting info</returns>
        internal ReportInfo GetSettingsData()
        {
            ReportInfo settings = new ReportInfo();

            settings.DateFrom = dFrom.DateTime;
            settings.DateTo = dTo.DateTime;

            settings.RegionIds = tTerritory.GetCheckedNodesIds();
            settings.StaffingsIds = tStaffing.GetCheckedColumnValues(colPersStaffIdReal.FieldName, Level, 6);

            settings.PosIds = cbPos.GetComboBoxCheckedValues();
            settings.DistrIds = cluDistr.AllValues("FieldCode");

            settings.ActionIds = cbActions.GetComboBoxCheckedValues();
            settings.DetalisationByOrders = cDetalisationByOrders.Checked;
            settings.DetalisationBySKUs = cDetalisationBySkus.Checked;
            settings.DetalisationByActionSKUs = cDetalisationByActionSkus.Checked;

            return settings;
        }

        /// <summary>
        /// Assigns datasources to contols
        /// </summary>
        private void InitDataSources()
        {
            distributors = DataProvider.GetDistributorsTree(out DistributorsLevel);
            PosLevel = DistributorsLevel + 1;

            tTerritory.LoadDataSource(DataProvider.GetTerritoryTree(2, 3));

            cluDistr.LoadDataSource(distributors.Copy());
            cbPos.LoadDataSource(distributors.Copy());

        }

        /// <summary>
        /// Sets initial settings
        /// </summary>
        private void SetInitialSettings()
        {
            tTerritory.CheckNodes(CheckState.Checked);
            FilterStaffingTree();

            tStaffing.CheckNodes(CheckState.Checked);
            tStaffing.AssignCheckEdit(cStaffing);

            cbActions.SelectAll();
            FilterLimitsByActions();

            FilterDistrGeneral();
            cluDistr.SelectAll();

            FilterPosListGeneral();
            cbPos.SelectAll();

        }

        /// <summary>
        /// Sets default date intervals
        /// </summary>
        private void SetDateIntervals()
        {
            DateTime lCurrentMonthBeginning = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            dFrom.DateTime = lCurrentMonthBeginning;
            dTo.DateTime = DateTime.Now;

            dFrom.Properties.MaxValue = dTo.DateTime;
            dTo.Properties.MinValue = dFrom.DateTime;
        }

        /// <summary>
        /// Handles report end date changes
        /// </summary>
        private void ReloadStaffData()
        {
            DateTime lDate = dTo.DateTime > DateTime.Now ? DateTime.Now : dTo.DateTime;
            staff = DataProvider.GetStaffingTree(lDate);
            tStaffing.LoadDataSource(staff.Copy());
            FilterStaffingTree();
        }

        private void ReloadActionsData()
        {
            List<string> values = cbActions.GetComboBoxCheckedValues();

            cbActions.Properties.Items.Clear();
            cbActions.LoadDataSource(DataProvider.GetActions(dFrom.DateTime, dTo.DateTime));

            cbActions.SetComboBoxCheckedValues(values);

            limits = DataProvider.GetLimitPeriods(dFrom.DateTime, dTo.DateTime);

            FilterLimitsByActions();
        }

        /// <summary>
        /// Validates selected data
        /// </summary>
        /// <param name="reportInfo">Settings to be checked</param>
        /// <param name="message">Warning message</param>
        /// <returns><c>true</c> if settings are valid, <c>false</c> otherwise</returns>
        private bool AreSettingsValid(ReportInfo reportInfo, out string message)
        {
            message = string.Empty;

            message += reportInfo.RegionIds.Count == 0 ? "\t- не выбрано территорию;\r\n" : string.Empty;

            message += reportInfo.StaffingsIds.Count == 0 ? "\t- не выбрано ни одного М1;\r\n" : string.Empty;

            message += reportInfo.DistrIds.Count > 0
                          ? string.Empty
                          : "\t- не выбрано ни одного дистрибьютора;\r\n";

            message += reportInfo.PosIds.Count > 0
                           ? string.Empty
                           : "\t- не выбрано ни одной TC;\r\n";

            message += reportInfo.ActionIds.Count > 0
                           ? string.Empty
                           : "\t- не выбрано ни одной акции;\r\n";

            return message.Length == 0;
        }

        #region Filtering

        /// <summary>
        /// Performs stuffing filtering - by regions
        /// </summary>
        private void FilterStaffingTree()
        {
            //filter M4 by regions
            List<string> lTerritoryFilter = tTerritory.GetCheckedColumnValues("GeographyOldId", Level, 3);

            string lFilter = string.Format(FilterByLevel, 2) + " or ";

            if (lTerritoryFilter.Count > 0)
            {
                lFilter += string.Format(FilterByColumn, "RegionID", string.Join(",", lTerritoryFilter.ToArray()));
            }
            else// no territory is selected
            {
                lFilter = string.Format(FilterByColumn, "RegionID", DumbFilter);
            }

            DataView lStaffView = staff.DefaultView;
            lStaffView.RowFilter = lFilter;

            DataTable lFilteredStaffTable = FilterOutEmptyM5(lStaffView);
            
            // save checked values on filtering
            List<string> values = tStaffing.GetCheckedColumnValuesAll(tStaffing.KeyFieldName);

            tStaffing.ClearNodes();
            tStaffing.LoadDataSource(lFilteredStaffTable);

            foreach (TreeListNode node in tStaffing.Nodes)
            {
                node.Expanded = true;
            }

            switch (cStaffing.CheckState)
            {
                case CheckState.Checked:
                    tStaffing.CheckNodes(CheckState.Checked);
                    break;
                case CheckState.Unchecked:
                    tStaffing.CheckNodes(CheckState.Unchecked);
                    break;
                case CheckState.Indeterminate:
                    if (lTerritoryFilter.Count == 0)
                    {
                        cStaffing.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        tStaffing.CheckNodes(values, CheckState.Checked);
                    }
                    break;
            }
        }

        /// <summary>
        /// Performs filtering of distributors by territory
        /// </summary>
        private void FilterDistrGeneral()
        {
            string lFilter = string.Format(FilterByLevel, DistributorsLevel);

            //by region
            string regionFilter = tTerritory.GetCheckedIdsValues();

            if (!string.IsNullOrEmpty(regionFilter)) //there are selected regions
            {
                lFilter += string.Format(AndFilterByColumn, colDistrFieldParentID.FieldName, regionFilter);
            }
            else
            {
                lFilter += string.Format(AndFilterByColumn, colDistrFieldParentID.FieldName, DumbFilter);
            }

            List<string> values = cluDistr.SelectedIds();

            //apply filter
            cluDistr.ClearSelection();

            DataView lDataView = distributors.DefaultView;
            lDataView.RowFilter = lFilter;
            lDataView.Sort = cluDistr.Properties.DisplayMember;
            cluDistr.Properties.DataSource = lDataView.ToTable(true, cluDistr.Properties.ValueMember,
                                                                  cluDistr.Properties.DisplayMember, DistrWorkSchema);

            cluDistr.SetComboBoxCheckedValues(values);
        }

        /// <summary>
        /// Performs filtering of Pos by distributors (so as by territory)
        /// </summary>
        private void FilterPosListGeneral()
        {
            string lFilter = string.Format(FilterByLevel, PosLevel);

            //by distr and territory
            string lDistrCheckedFilter = cluDistr.SelectedIdsString();
            if (!string.IsNullOrEmpty(lDistrCheckedFilter)) //there are distr selected
            {
                lFilter += string.Format(AndFilterByColumn, PosParentIdField, GetDistrStringDescription(lDistrCheckedFilter));
            }
            else //there are no distr selected == by territory only
            {
                lDistrCheckedFilter = cluDistr.AllIdsString();
                if (!string.IsNullOrEmpty(lDistrCheckedFilter))
                {
                    lFilter += string.Format(AndFilterByColumn, PosParentIdField, GetDistrStringDescription(lDistrCheckedFilter));
                }
                else // for параноїків
                {
                    lFilter += string.Format(AndFilterByColumn, PosParentIdField, DumbFilter);
                }
            }

            List<string> values = cbPos.GetComboBoxCheckedValues();
            //apply filter
            cbPos.FilterList(lFilter);

            cbPos.SetComboBoxCheckedValues(values);
        }

        private void FilterLimitsByActions()
        {
            string lFilter = string.Empty;
            string lActionFilter = cbActions.GetComboBoxCheckedValuesList();

            if (!string.IsNullOrEmpty(lActionFilter))
            {
                lFilter = string.Format(FilterByColumn, "ActionID", lActionFilter);
            }
            else
            {
                lFilter = string.Format(FilterByColumn, "ActionID", DumbFilter);
            }

            mLimitPeriods.Text = GetLimitPeriodsStringDescription(lFilter);
        }

        private string GetDistrStringDescription(string filterCode)
        {
            DataView lDataView = distributors.DefaultView;
            lDataView.RowFilter = string.Format(FilterByLevel, DistributorsLevel) + string.Format(AndFilterByColumn, cluDistr.Properties.ValueMember, filterCode);// "FIeldCode in (" + filterCode + ") and Level = 4";

            List<string> lFilterIds = new List<string>();

            foreach (DataRow row in lDataView.ToTable().Rows)
            {
                lFilterIds.Add(row["FieldID"].ToString());
            }

            return string.Join(",", lFilterIds.ToArray());
        }

        private string GetLimitPeriodsStringDescription(string filter)
        {
            DataView lDataView = limits.DefaultView;
            lDataView.RowFilter = filter;

            List<string> lFilterIds = new List<string>();

            foreach (DataRow row in lDataView.ToTable().Rows)
            {
                lFilterIds.Add(row["Name"].ToString());
            }

            return string.Join(Environment.NewLine, lFilterIds.ToArray());
        }

        /// <summary>
        /// Removes M5s that has no children
        /// </summary>
        private DataTable FilterOutEmptyM5(DataView staffing)
        {
            DataTable lFilteredStaffTable = staffing.ToTable();

            for(int i = lFilteredStaffTable.Rows.Count-1; i >= 0; i--)
            {
                DataRow row = lFilteredStaffTable.Rows[i];
                if(Convert.ToInt64(row["Level"]) == 2)//M5
                {
                    //check whether there are children for M5
                    long lM5Id = Convert.ToInt64(row["wStaffID"]);
                    bool lHasChildren = false;
                    
                    foreach (DataRow staff in lFilteredStaffTable.Rows)
                    {
                        if (!staff.IsNull("wParentID") && Convert.ToInt64(staff["wParentID"]) == lM5Id)
                        {
                            lHasChildren = true;
                            break;
                        }
                    }

                    if (!lHasChildren)
                    {
                        lFilteredStaffTable.Rows.RemoveAt(i);
                    }
                }
            }
            
            return lFilteredStaffTable;
        }

        #endregion

        #region Event Handling

        /// <summary>
        /// Handles AfterCheckNode event of tTerritory: performs territory dependent filtering
        /// </summary>
        /// <param name="sender">tTerritory</param>
        /// <param name="e">NodeEventArgs</param>
        private void tTerritory_AfterNodeChecked(object sender, EventArgs args)
        {
            FilterStaffingTree();

            FilterDistrGeneral();
            FilterPosListGeneral();
        }

        /// <summary>
        /// Handles Closed event of cbDistr: performs dist dependent filtering
        /// </summary>
        /// <param name="sender">cbDistr</param>
        /// <param name="e">ClosedEventArgs</param>
        private void cbDistr_Closed(object sender, ClosedEventArgs e)
        {
            lDistr.Focus();
            FilterPosListGeneral();
        }

        private void cbActions_Closed(object sender, ClosedEventArgs e)
        {
            lAction.Focus();
            FilterLimitsByActions();
        }

        /// <summary>
        /// Handles Click event of generateReportButton
        /// </summary>
        /// <param name="sender">generateReportButton</param>
        /// <param name="e">EventArgs</param>
        private void generateReportButton_Click(object sender, EventArgs e)
        {
            ReportInfo settings = GetSettingsData();
            string message;

            if (AreSettingsValid(settings, out message))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Не все параметры введены:\n\r\n" + message, "Предупреждение", MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
            }
        }

        private void date_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(dFrom))
            {
                dTo.Properties.MinValue = !string.IsNullOrEmpty(dFrom.Text) ? dFrom.DateTime : DateTime.MinValue;
            }

            if (sender.Equals(dTo))
            {
                dFrom.Properties.MaxValue = !string.IsNullOrEmpty(dTo.Text) ? dTo.DateTime : DateTime.Now;

                WaitManager.StartWait();
                ReloadStaffData();
                WaitManager.StopWait();
            }

            WaitManager.StartWait();
            ReloadActionsData();
            WaitManager.StopWait();
        }

        private void cDetalisationBySkus_CheckedChanged(object sender, EventArgs e)
        {
            if (cDetalisationBySkus.Checked)
            {
                cDetalisationByActionSkus.Enabled = true;
            }
            else
            {
                cDetalisationByActionSkus.Checked = false;
                cDetalisationByActionSkus.Enabled = false;
            }
        }
        
        /// <summary>
        /// Changes caption padding ;)
        /// </summary>
        private void groupControl_CustomDrawCaption(object sender, GroupCaptionCustomDrawEventArgs e)
        {
            //paint standart background
            e.Graphics.FillRectangle(e.Info.AppearanceCaption.GetBorderBrush(e.Cache), e.CaptionBounds);
            e.Graphics.FillRectangle(e.Info.AppearanceCaption.GetBackBrush(e.Cache), e.CaptionBounds);

            //get offset
            int delta = 6;
            if (sender == gcByTerritory)
                delta = 4;

            if (sender == gcStaff)
                delta = 5;

            //draw caption
            Rectangle innerRect = Rectangle.Inflate(e.CaptionBounds, -delta, -3);
            StringFormat outStrFormat = new StringFormat();
            outStrFormat.Alignment = StringAlignment.Near;
            outStrFormat.LineAlignment = StringAlignment.Near;
            e.Graphics.DrawString(e.Info.Caption, e.Info.AppearanceCaption.Font,
              e.Info.AppearanceCaption.GetForeBrush(e.Cache), innerRect, outStrFormat);

            e.Handled = true;
        }

        #endregion

    }
}
