﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.InconsistencyReport;
using SoftServe.Reports.ThR.InconsistencyReport.DataAccess;
using SoftServe.Reports.ThR.InconsistencyReport.Properties;
using ModularWinApp;

namespace WccpReporting {
    [ToolboxItem(false)]
    public partial class WccpUIControl : XtraUserControl {
        #region Fields
        private string _userLogin;
        private string _folderForExport = "ExportedReports";
        private string _userExportRoot;
        private InconsistencyParams _params;
        private DataTable _inconsistencyData;
        private readonly List<string> _modifiedColumns = new List<string>(2);
        private readonly List<string> _hiddenColumns = new List<string>(3) {
            DataProvider.IncIdFieldName, DataProvider.WaveIdFieldName, DataProvider.SeverityFieldName
        };
        private readonly List<string> _editedColumns = new List<string>(2) {DataProvider.ValueFieldName, DataProvider.ApproveFieldName};
        private readonly List<string> _invalidWidthColumns = new List<string>(4) {DataProvider.TTFieldName, DataProvider.UrNameTTFieldName,
            DataProvider.AddressTTFieldName, DataProvider.UrAddressTTFieldName};
        private readonly bool _isAllowedEdit;

        #endregion

        public WccpUIControl() {
            InitializeComponent();
            Params = SettingsForm.Params;
            _isAllowedEdit = DataProvider.IsAllowedEdit(Params);
            //TODO: !!! comment next line for release. For debug purposes only !!!
            //_isAllowedEdit = true;
            barBtnSave.Enabled = _isAllowedEdit;
            ReportControl = this;
            ShowReport();
        }

        public static WccpUIControl ReportControl;
        public static SettingsForm SettingsForm { get; set; }

        private InconsistencyParams Params {
            get { return _params; }
            set {
                _params = value;
                labelReportName.Text = "";
                labelWaveName.Text = _params.WaveName;
                if (SettingsForm != null)
                    labelReportName.Text = SettingsForm.ReportName;
            }
        }

        public string UserLogin
        {
            get { return _userLogin; }
            set
            {
                string temp = string.Empty;
                _userLogin = string.Empty;

                if (value.Contains("\\"))
                {
                    temp = value.Remove(0, value.LastIndexOf('\\') + 1);
                }
                else
                {
                    temp = value;
                }
                _userLogin += temp.Replace('.', '_');
            }
        }

        private void ShowReport() {
            WaitManager.StartWait();
            try {
                _inconsistencyData = DataProvider.GetInconsistencyData(Params);
                if (null == _inconsistencyData)
                    return;

                _inconsistencyData.RowChanged += IncRowChanged;
                gridControl.DataSource = _inconsistencyData;
                SetupColumns();
            }
            finally {
                WaitManager.StopWait();
            }
        }

        public void SaveData() {
            if (barBtnSave.Enabled)
                barBtnSave_ItemClick(this, null);
        }

        private void IncRowChanged(object sender, DataRowChangeEventArgs e) {
            if (e.Row.RowState != DataRowState.Modified)
                return;
            if (Convert.ToBoolean(rgEditMode.EditValue)) {
                byte lSeverity = 0;
                int lIncId = -1;
                double lValue = ConvertEx.ToDouble(e.Row[DataProvider.ValueFieldName]);
                bool lIsApproved = ConvertEx.ToBool(e.Row[DataProvider.ApproveFieldName]);
                byte lFlag = 0;
                e.Row.RejectChanges();
                _inconsistencyData.RowChanged -= IncRowChanged;
                GridColumn lColSeverity = gridView.Columns.ColumnByFieldName(DataProvider.SeverityFieldName);
                GridColumn lColIncId = gridView.Columns.ColumnByFieldName(DataProvider.IncIdFieldName);
                GridColumn lColValue = gridView.Columns.ColumnByFieldName(DataProvider.ValueFieldName);
                GridColumn lColApproved = gridView.Columns.ColumnByFieldName(DataProvider.ApproveFieldName);
                if (_modifiedColumns.Contains(DataProvider.ValueFieldName) &&
                    _modifiedColumns.Contains(DataProvider.ApproveFieldName))
                    lFlag = 0;
                else if (_modifiedColumns.Contains(DataProvider.ValueFieldName))
                    lFlag = 1;
                else if (_modifiedColumns.Contains(DataProvider.ApproveFieldName))
                    lFlag = 2;
                    
                gridView.BeginDataUpdate();
                Cursor.Current = Cursors.WaitCursor;
                try {
                    int lDataRowCount = gridView.DataRowCount;
                    for (int lI = 0; lI < lDataRowCount; lI++) {
                        lSeverity = Convert.ToByte(gridView.GetRowCellValue(lI, lColSeverity));
                        if (lSeverity == 1 && lIsApproved)
                            continue;

                        lIncId = Convert.ToInt32(gridView.GetRowCellValue(lI, lColIncId));
                        if (lFlag == 0 || lFlag == 1)
                            gridView.SetRowCellValue(lI, lColValue, lValue);
                        if (lFlag == 0 || lFlag == 2)
                            gridView.SetRowCellValue(lI, lColApproved, lIsApproved);
                        gridView.PostEditor();
                        DataProvider.SaveChanges(lIncId, lValue, lIsApproved, lFlag);
                        DataRow lRow = gridView.GetDataRow(lI);
                        lRow.AcceptChanges();
                    }
                }
                finally {
                    gridView.EndDataUpdate();
                    Cursor.Current = Cursors.Default;
                    _inconsistencyData.RowChanged += IncRowChanged;
                }
            }
            else {
                int lIncId = Convert.ToInt32(e.Row[DataProvider.IncIdFieldName]);
                double lValue = ConvertEx.ToDouble(e.Row[DataProvider.ValueFieldName]);
                bool lIsApproved = ConvertEx.ToBool(e.Row[DataProvider.ApproveFieldName]);
                DataProvider.SaveChanges(lIncId, lValue, lIsApproved);
                e.Row.AcceptChanges();
            }
            _modifiedColumns.Clear();
            barBtnSave.Enabled = false;
        }

        private void SetupColumns()
        {
            bool isNeedOfColumn = false;
            int index = 0;
            foreach (GridColumn lColumn in gridView.Columns)
            {
                lColumn.OptionsColumn.AllowEdit = false;
                lColumn.OptionsColumn.ReadOnly = true;
                lColumn.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
                lColumn.BestFit();
                if (_hiddenColumns.Contains(lColumn.FieldName))
                    lColumn.Visible = false;
                if (_isAllowedEdit)
                    if (_editedColumns.Contains(lColumn.FieldName))
                    {
                        lColumn.OptionsColumn.AllowEdit = true;
                        lColumn.OptionsColumn.ReadOnly = false;
                    }
                if (_invalidWidthColumns.Contains(lColumn.FieldName))
                    lColumn.Width = 300;
                if (lColumn.FieldName == DataProvider.ApproveFieldName)
                {
                    lColumn.ColumnEdit = riCheckEdit;
                    lColumn.ColumnEdit.KeyUp += gridControl_KeyUp;
                }
                if (lColumn.FieldName == DataProvider.ValueFieldName)
                {
                    lColumn.ColumnEdit = riTextEdit;
                    lColumn.ColumnEdit.KeyUp += gridControl_KeyUp;
                }
                if (lColumn.FieldName == DataProvider.InRoute)
                {
                    isNeedOfColumn = true;
                    index = lColumn.VisibleIndex;
                    lColumn.Visible = false;
                }
            }
            if (isNeedOfColumn)
            {
                GridColumn unbCol = new GridColumn();
                unbCol.FieldName = "InRouteUnb";
                gridView.Columns.Insert(gridView.Columns.Count, unbCol);
                unbCol.Visible = true;
                unbCol.Caption = DataProvider.InRoute;
                unbCol.UnboundType = UnboundColumnType.String;
                unbCol.VisibleIndex = index;
                unbCol.OptionsColumn.ReadOnly = true;
                unbCol.OptionsColumn.AllowEdit = false;
            }
        }

        private void barBtnExcelExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            if (!CleanDirForReportFiles())
                return;

            try {
                gridControl.ExportToXls(_userExportRoot);
                
                Process proc = new Process();
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = Path.GetFileName(_userExportRoot);
                psi.WorkingDirectory = Path.GetDirectoryName(_userExportRoot);
                proc.EnableRaisingEvents = false;
                proc.StartInfo = psi;
                proc.Start();
            }
            catch (Exception lException) {
                MessageBox.Show("Не могу открыть " + _userExportRoot + "\nПопробуйте открыть этот файл через Excel" + "\n("
                                + lException.Message + ")", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool CleanDirForReportFiles() {
            try {
                UserLogin = HostConnectionSettings.ActiveConnectionSettings.UserName;
                _userExportRoot = Path.Combine(DataProvider.RootDir, _folderForExport, UserLogin + "_" + DataProvider.InconsistencyReportFile);
                if (!Directory.Exists(Path.Combine(DataProvider.RootDir, _folderForExport)))
                {
                    Directory.CreateDirectory(Path.Combine(DataProvider.RootDir, _folderForExport));
                }

                if (File.Exists(_userExportRoot))
                    File.Delete(_userExportRoot);
                return true;
            }
            catch (Exception lException) {
                MessageBox.Show("Закройте файл " + _userExportRoot
                                + "/n и повторите попытку.\n" + lException.Message,
                    "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        private void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e) {
            barBtnSave.Enabled = true;
            _modifiedColumns.Add(e.Column.FieldName);
        }

        private void barBtnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            gridView.PostEditor();
            gridView.UpdateCurrentRow();
        }

        private void gridControl_KeyUp(object sender, KeyEventArgs e) {
            if ((e.KeyCode == Keys.S) && e.Control && barBtnSave.Enabled) {
                barBtnSave_ItemClick(this, null);
                e.Handled = true;
            }
        }

        private void gridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e) {
            e.Handled = false;
            if (e.RowHandle < 0)
                return;
            bool lIsApproved = ConvertEx.ToBool(gridView.GetRowCellValue(e.RowHandle, DataProvider.ApproveFieldName));
            if (lIsApproved)
                return;
            e.Appearance.BackColor = Color.DarkSalmon;
        }

        private void gridControl_KeyDown(object sender, KeyEventArgs e) {
            if ((e.KeyCode == Keys.V) && e.Control)
                FilterDisplayedTT();
        }

        private void FilterDisplayedTT() {
            string lTTList = String.Join(",",
                ParseClipboard.GetCodes().Select(it => String.Format("'{0}'", it)).ToArray());

            if (String.IsNullOrEmpty(lTTList)) {
                MessageBox.Show(Resources.ClipboardEmptyError, Resources.Information, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
        
            GridColumn lColTT = gridView.Columns.ColumnByFieldName(DataProvider.CodeTTFieldName);
            string lFilter = String.Format("[{0}] In ({1})", lColTT.FieldName, lTTList);
            lColTT.FilterInfo = new ColumnFilterInfo(ColumnFilterType.Custom, lFilter);
        }

        private void gridView_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column == null)
            {
                return;
            }
            if (e.Column.FieldName == "InRouteUnb")
            {
                e.Value = (bool) gridView.GetListSourceRowCellValue(e.ListSourceRowIndex,
                    gridView.Columns[DataProvider.InRoute])
                    ? "Да"
                    : "Нет";
            }
        }

        private void gridView_ShowFilterPopupListBox(object sender, FilterPopupListBoxEventArgs e)
        {
            if (e.Column == null)
            {
                return;
            }
            if (e.Column == gridView.Columns["InRouteUnb"])
                Text = e.ComboBox.LinkCount.ToString();
            int index = 0;
            string custom = DevExpress.XtraGrid.Localization.GridLocalizer.Active.GetLocalizedString(DevExpress.XtraGrid.Localization.GridStringId.PopupFilterCustom);
            for (int i = 0; i < e.ComboBox.Items.Count; i++)
                if ((e.ComboBox.Items[i] as string) == custom)
                {
                    index = i;
                    break;
                }
            e.ComboBox.Items.RemoveAt(index);
        }
    }
}