﻿using System;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.ThR.InconsistencyReport.DataAccess {
    public static class DataProvider {
        #region Fields

        private static string _rootDir;

        private const string SQLGetWaves =    @"SELECT Wave_ID, WaveName, isActive FROM dbo.DW_TomasResearch_Wave w INNER JOIN dbo.fn_CountryByUser(system_user) f ON w.Country_id = f.CountryID WHERE w.isHidden = 0 ORDER BY WaveName";
        private const string SpDwThRInconsistencyReport = "dbo.DW_TomasResearch_InconsistenceReport";
        private const string SpDwThRIncUpdate = "DW_TomasResearch_IncUpdate";
        private const string SpDwThRIncEditAllowed = "DW_TomasResearch_IncEditAllowed";

        private const string ParamWaveId = "@wave_id";
        private const string ParamIsAllowed = "@isAllowed";
        private const string ParamFValue = "@FValue";
        private const string ParamIsApproved = "@isApproved";
        private const string ParamIncId = "@Inc_ID";
        private const string ParamColumnsFlag = "@columnsFlag";

        #endregion

        public static string InconsistencyReportFile = "InconsistencyReport.xls";
        public static string IncIdFieldName = "Inc_ID";
        public static string WaveIdFieldName = "Wave_Id";
        public static string SeverityFieldName = "Severity";
        public static string ValueFieldName = "Значение";
        public static string ApproveFieldName = "Подтверждено";
        public static string CodeTTFieldName = "КодTT";
        public static string TTFieldName = "ТТ";
        public static string UrNameTTFieldName = "ЮрИмяТТ";
        public static string AddressTTFieldName = "АдресТТ";
        public static string UrAddressTTFieldName = "ЮрАдресТТ";
        public static string InRoute = "ТТ на маршруте";
        

        public static string RootDir {
            get {
                if (string.IsNullOrEmpty(_rootDir))
                    _rootDir = AppDomain.CurrentDomain.BaseDirectory;
                return _rootDir;
            }
        }

        public static DataTable GetWavesList() {
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteQuery(SQLGetWaves);

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            
            return lDataTable;
        }

        public static DataTable GetInconsistencyData(InconsistencyParams pars) {
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SpDwThRInconsistencyReport,
                    new SqlParameter(ParamWaveId, pars.WaveId));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            
            return lDataTable;
        }

        public static bool IsAllowedEdit(InconsistencyParams pars) {
            bool lResult = false;
            DataAccessLayer.OpenConnection();
            try {
                SqlParameter lParamIsAllowed = new SqlParameter(ParamIsAllowed, lResult);
                lParamIsAllowed.Direction = ParameterDirection.Output;
                SqlParameterCollection lOutput = DataAccessLayer.ExecuteNonQueryStoredProcedureWithOutput(SpDwThRIncEditAllowed,
                    new SqlParameter(ParamWaveId, pars.WaveId),
                    lParamIsAllowed);
                
                if (null != lOutput && lOutput.Contains(ParamIsAllowed))
                    lResult = Convert.ToBoolean(lOutput[ParamIsAllowed].Value);
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            
            return lResult;
        }

        public static void SaveChanges(int incId, double value, bool approved, byte columnsFlag = 0) {
            DataAccessLayer.OpenConnection();
            try {
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SpDwThRIncUpdate,
                    new SqlParameter(ParamIncId, incId),
                    new SqlParameter(ParamFValue, value),
                    new SqlParameter(ParamIsApproved, approved),
                    new SqlParameter(ParamColumnsFlag, columnsFlag));
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
        }

    }
}