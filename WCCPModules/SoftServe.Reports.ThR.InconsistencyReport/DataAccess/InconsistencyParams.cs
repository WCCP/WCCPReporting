﻿namespace SoftServe.Reports.ThR.InconsistencyReport.DataAccess {
    public class InconsistencyParams {
        public int WaveId;
        public string WaveName;
    }
}