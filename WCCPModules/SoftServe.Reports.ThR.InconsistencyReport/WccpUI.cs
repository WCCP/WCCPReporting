﻿using System;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using SoftServe.Reports.ThR.InconsistencyReport;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpThRInconsistencyReport.dll")]
    public class WccpUI : IStartupClass
    {
        public static string ReportName { get; private set; }

        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                ReportName = reportCaption;

                DataAccessLayer.OpenConnection();

                SettingsForm setForm = new SettingsForm();
                setForm.ReportName = ReportName;

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.SettingsForm = setForm;

                    _reportControl = new WccpUIControl();
                    _reportCaption = reportCaption;

                    return 0;
                }
            }
            catch (Exception lException)
            {
                ErrorManager.ShowErrorBox(lException.Message);
            }

            return 1;
        }

        public void CloseUI()
        {
            WccpUIControl.ReportControl.SaveData();
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}