using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.InconsistencyReport.DataAccess;
using WccpReporting;

namespace SoftServe.Reports.ThR.InconsistencyReport {
    public partial class SettingsForm : XtraForm {
        #region Fields

        private bool isDataLoaded;
        private DataTable _wavesTable;
        private DataTable _activeWaveTable;

        #endregion

        #region Constructors

        public SettingsForm() {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        public InconsistencyParams Params { get; set; }
        public string ReportName { get; set; }

        #endregion

        #region Instance Methods

        private void LoadData() {
            Enabled = false;
            WaitManager.StartWait();
            try {
                Text = WccpUI.ReportName;

                _wavesTable = DataProvider.GetWavesList();
                DataView lDataView = new DataView(_wavesTable);
                lDataView.RowFilter = "isActive = 1";
                _activeWaveTable = lDataView.ToTable();

                SetDataSource(chkActiveWave.Checked ? _activeWaveTable : _wavesTable);
            }
            finally {
                Enabled = true;
                WaitManager.StopWait();
            }
        }

        private void SetDataSource(DataTable dataTable) {
            lkpWave.Properties.DataSource = dataTable;
            if (dataTable.Rows.Count > 0)
                lkpWave.EditValue = dataTable.Rows[dataTable.Rows.Count - 1][lkpWave.Properties.ValueMember];
        }

        private bool ValidateData() {
            if (string.IsNullOrEmpty(lkpWave.Text))
            {
                XtraMessageBox.Show("����� ������� �����", "��������������", MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        #endregion

        #region Event Handling

        private void SettingsForm_Load(object sender, EventArgs e) {
            if (isDataLoaded)
                return;

            LoadData();
            isDataLoaded = true;
        }

        private void btnYes_Click(object sender, EventArgs e) {
            if (!ValidateData()) // put show msg here
                return;

            Params = new InconsistencyParams() {
                WaveId = Convert.ToInt32(lkpWave.EditValue),
                WaveName = lkpWave.Properties.GetDisplayText(lkpWave.EditValue)
            };
            DialogResult = DialogResult.OK;
        }

        #endregion

        private void chkActiveWave_CheckedChanged(object sender, EventArgs e) {
            SetDataSource(chkActiveWave.Checked ? _activeWaveTable : _wavesTable);
        }
    }
}