﻿namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.panelAll = new DevExpress.XtraEditors.PanelControl();
            this.panelGrid = new DevExpress.XtraEditors.PanelControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.riCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.riTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panelCaption = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.rgEditMode = new DevExpress.XtraEditors.RadioGroup();
            this.barManager = new DevExpress.XtraBars.BarManager();
            this.barMain = new DevExpress.XtraBars.Bar();
            this.barBtnExcelExport = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barBtnSave = new DevExpress.XtraBars.BarLargeButtonItem();
            this.stdBarDockControl = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imgCollection = new DevExpress.Utils.ImageCollection();
            this.labelWaveName = new DevExpress.XtraEditors.LabelControl();
            this.labelReportName = new DevExpress.XtraEditors.LabelControl();
            this.labelWave = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelAll)).BeginInit();
            this.panelAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).BeginInit();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCaption)).BeginInit();
            this.panelCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgEditMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // panelAll
            // 
            this.panelAll.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.panelAll.Controls.Add(this.panelGrid);
            this.panelAll.Controls.Add(this.panelCaption);
            this.panelAll.Controls.Add(this.stdBarDockControl);
            this.panelAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAll.Location = new System.Drawing.Point(0, 0);
            this.panelAll.Name = "panelAll";
            this.panelAll.Size = new System.Drawing.Size(650, 435);
            this.panelAll.TabIndex = 0;
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gridControl);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(2, 83);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(646, 350);
            this.panelGrid.TabIndex = 2;
            // 
            // gridControl
            // 
            this.gridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(2, 2);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riCheckEdit,
            this.riTextEdit});
            this.gridControl.Size = new System.Drawing.Size(642, 346);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            this.gridControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControl_KeyDown);
            this.gridControl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridControl_KeyUp);
            // 
            // gridView
            // 
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsFilter.ColumnFilterPopupMaxRecordsCount = 10000;
            this.gridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowFooter = true;
            this.gridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView_CustomDrawCell);
            this.gridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanging);
            this.gridView.ShowFilterPopupListBox += new DevExpress.XtraGrid.Views.Grid.FilterPopupListBoxEventHandler(this.gridView_ShowFilterPopupListBox);
            this.gridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView_CustomUnboundColumnData);
            // 
            // riCheckEdit
            // 
            this.riCheckEdit.AutoHeight = false;
            this.riCheckEdit.Name = "riCheckEdit";
            // 
            // riTextEdit
            // 
            this.riTextEdit.AutoHeight = false;
            this.riTextEdit.Name = "riTextEdit";
            // 
            // panelCaption
            // 
            this.panelCaption.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.panelCaption.Controls.Add(this.labelControl1);
            this.panelCaption.Controls.Add(this.rgEditMode);
            this.panelCaption.Controls.Add(this.labelWaveName);
            this.panelCaption.Controls.Add(this.labelReportName);
            this.panelCaption.Controls.Add(this.labelWave);
            this.panelCaption.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCaption.Location = new System.Drawing.Point(2, 37);
            this.panelCaption.Name = "panelCaption";
            this.panelCaption.Size = new System.Drawing.Size(646, 46);
            this.panelCaption.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Location = new System.Drawing.Point(312, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(140, 14);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Режим редактирования:";
            // 
            // rgEditMode
            // 
            this.rgEditMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgEditMode.EditValue = false;
            this.rgEditMode.Location = new System.Drawing.Point(460, 20);
            this.rgEditMode.MenuManager = this.barManager;
            this.rgEditMode.Name = "rgEditMode";
            this.rgEditMode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgEditMode.Properties.Appearance.Options.UseBackColor = true;
            this.rgEditMode.Properties.EnableFocusRect = true;
            this.rgEditMode.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Одиночный"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Групповой")});
            this.rgEditMode.Size = new System.Drawing.Size(184, 24);
            this.rgEditMode.TabIndex = 5;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMain});
            this.barManager.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Main", new System.Guid("72495bd1-cbf3-4b9f-aed0-39d083144e77"))});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.DockControls.Add(this.stdBarDockControl);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barBtnExcelExport,
            this.barBtnSave});
            this.barManager.LargeImages = this.imgCollection;
            this.barManager.MaxItemId = 2;
            // 
            // barMain
            // 
            this.barMain.BarName = "Main";
            this.barMain.DockCol = 0;
            this.barMain.DockRow = 0;
            this.barMain.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barMain.FloatLocation = new System.Drawing.Point(86, 181);
            this.barMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnExcelExport),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, this.barBtnSave, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "CTRL+S", "")});
            this.barMain.OptionsBar.AllowQuickCustomization = false;
            this.barMain.OptionsBar.DisableClose = true;
            this.barMain.OptionsBar.DisableCustomization = true;
            this.barMain.StandaloneBarDockControl = this.stdBarDockControl;
            this.barMain.Text = "Main Bar";
            // 
            // barBtnExcelExport
            // 
            this.barBtnExcelExport.Hint = "Экспорт в Excel";
            this.barBtnExcelExport.Id = 0;
            this.barBtnExcelExport.LargeImageIndex = 0;
            this.barBtnExcelExport.Name = "barBtnExcelExport";
            this.barBtnExcelExport.ShowCaptionOnBar = false;
            this.barBtnExcelExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnExcelExport_ItemClick);
            // 
            // barBtnSave
            // 
            this.barBtnSave.Hint = "Сохранить";
            this.barBtnSave.Id = 1;
            this.barBtnSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barBtnSave.LargeImageIndex = 1;
            this.barBtnSave.Name = "barBtnSave";
            this.barBtnSave.ShowCaptionOnBar = false;
            this.barBtnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnSave_ItemClick);
            // 
            // stdBarDockControl
            // 
            this.stdBarDockControl.CausesValidation = false;
            this.stdBarDockControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.stdBarDockControl.Location = new System.Drawing.Point(2, 2);
            this.stdBarDockControl.Name = "stdBarDockControl";
            this.stdBarDockControl.Size = new System.Drawing.Size(646, 35);
            this.stdBarDockControl.Text = "standaloneBarDockControl1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(650, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 435);
            this.barDockControlBottom.Size = new System.Drawing.Size(650, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 435);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(650, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 435);
            // 
            // imgCollection
            // 
            this.imgCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imgCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgCollection.ImageStream")));
            this.imgCollection.Images.SetKeyName(0, "Excel_24.png");
            this.imgCollection.Images.SetKeyName(1, "save24x24.png");
            // 
            // labelWaveName
            // 
            this.labelWaveName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelWaveName.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWaveName.Appearance.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelWaveName.Location = new System.Drawing.Point(56, 24);
            this.labelWaveName.Name = "labelWaveName";
            this.labelWaveName.Size = new System.Drawing.Size(35, 16);
            this.labelWaveName.TabIndex = 4;
            this.labelWaveName.Text = "Name";
            // 
            // labelReportName
            // 
            this.labelReportName.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelReportName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelReportName.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelReportName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelReportName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelReportName.Location = new System.Drawing.Point(2, 2);
            this.labelReportName.Name = "labelReportName";
            this.labelReportName.Size = new System.Drawing.Size(642, 16);
            this.labelReportName.TabIndex = 3;
            this.labelReportName.Text = "labelReportName";
            // 
            // labelWave
            // 
            this.labelWave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelWave.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWave.Location = new System.Drawing.Point(8, 24);
            this.labelWave.Name = "labelWave";
            this.labelWave.Size = new System.Drawing.Size(43, 14);
            this.labelWave.TabIndex = 2;
            this.labelWave.Text = "Волна:";
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelAll);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(650, 435);
            ((System.ComponentModel.ISupportInitialize)(this.panelAll)).EndInit();
            this.panelAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).EndInit();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCaption)).EndInit();
            this.panelCaption.ResumeLayout(false);
            this.panelCaption.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgEditMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelAll;
        private DevExpress.XtraEditors.PanelControl panelGrid;
        private DevExpress.XtraEditors.PanelControl panelCaption;
        private DevExpress.XtraEditors.LabelControl labelWaveName;
        private DevExpress.XtraEditors.LabelControl labelReportName;
        private DevExpress.XtraEditors.LabelControl labelWave;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraBars.StandaloneBarDockControl stdBarDockControl;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar barMain;
        private DevExpress.XtraBars.BarLargeButtonItem barBtnExcelExport;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imgCollection;
        private DevExpress.XtraBars.BarLargeButtonItem barBtnSave;
        private DevExpress.XtraEditors.RadioGroup rgEditMode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit riCheckEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riTextEdit;
    }
}
