﻿namespace SoftServe.Reports.ThR.InconsistencyReport
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkActiveWave = new DevExpress.XtraEditors.CheckEdit();
            this.lkpWave = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveWave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpWave.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnYes.Image = global::SoftServe.Reports.ThR.InconsistencyReport.Properties.Resources.Check_16;
            this.btnYes.ImageIndex = 0;
            this.btnYes.Location = new System.Drawing.Point(140, 133);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(72, 23);
            this.btnYes.TabIndex = 1;
            this.btnYes.Text = "Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = global::SoftServe.Reports.ThR.InconsistencyReport.Properties.Resources.Close_16;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.Location = new System.Drawing.Point(225, 133);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Нет";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.chkActiveWave);
            this.groupControl1.Controls.Add(this.lkpWave);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(285, 108);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Опции отчета:";
            // 
            // chkActiveWave
            // 
            this.chkActiveWave.EditValue = true;
            this.chkActiveWave.Location = new System.Drawing.Point(4, 72);
            this.chkActiveWave.Name = "chkActiveWave";
            this.chkActiveWave.Properties.Caption = "Активная";
            this.chkActiveWave.Size = new System.Drawing.Size(108, 19);
            this.chkActiveWave.TabIndex = 8;
            this.chkActiveWave.CheckedChanged += new System.EventHandler(this.chkActiveWave_CheckedChanged);
            // 
            // lkpWave
            // 
            this.lkpWave.Location = new System.Drawing.Point(5, 44);
            this.lkpWave.Name = "lkpWave";
            this.lkpWave.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lkpWave.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpWave.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WaveName", "WaveName")});
            this.lkpWave.Properties.DisplayMember = "WaveName";
            this.lkpWave.Properties.NullText = "";
            this.lkpWave.Properties.ShowFooter = false;
            this.lkpWave.Properties.ShowHeader = false;
            this.lkpWave.Properties.ValueMember = "Wave_ID";
            this.lkpWave.Size = new System.Drawing.Size(275, 20);
            this.lkpWave.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Волна:";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 168);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Summary report";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveWave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpWave.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit chkActiveWave;
        private DevExpress.XtraEditors.LookUpEdit lkpWave;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}