﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Logica.Reports {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Logica.Reports.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Тип канала:.
        /// </summary>
        internal static string ChannelType {
            get {
                return ResourceManager.GetString("ChannelType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата начала:.
        /// </summary>
        internal static string DateFrom {
            get {
                return ResourceManager.GetString("DateFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата окончания:.
        /// </summary>
        internal static string DateTo {
            get {
                return ResourceManager.GetString("DateTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка.
        /// </summary>
        internal static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите тип канала.
        /// </summary>
        internal static string IncorrectChannel {
            get {
                return ResourceManager.GetString("IncorrectChannel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите уровень персонала.
        /// </summary>
        internal static string IncorrectStaffLevel {
            get {
                return ResourceManager.GetString("IncorrectStaffLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Некоректная дата.
        /// </summary>
        internal static string InvalidDate {
            get {
                return ResourceManager.GetString("InvalidDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Параметры.
        /// </summary>
        internal static string Params {
            get {
                return ResourceManager.GetString("Params", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Подключено ТТ.
        /// </summary>
        internal static string ReportAdherence {
            get {
                return ResourceManager.GetString("ReportAdherence", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Адресная программа.
        /// </summary>
        internal static string ReportOutlets {
            get {
                return ResourceManager.GetString("ReportOutlets", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Отчеты:.
        /// </summary>
        internal static string Reports {
            get {
                return ResourceManager.GetString("Reports", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Уровень персонала:.
        /// </summary>
        internal static string StaffType {
            get {
                return ResourceManager.GetString("StaffType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to USMReport.
        /// </summary>
        internal static string USMReport {
            get {
                return ResourceManager.GetString("USMReport", resourceCulture);
            }
        }
    }
}
