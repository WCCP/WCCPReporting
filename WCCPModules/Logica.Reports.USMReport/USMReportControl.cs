﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        ReportOutlets customTab1;
        ReportAdherence customTab2;
        USMSettingsForm setForm;
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            customTab1 = new ReportOutlets(this);
            customTab2 = new ReportAdherence(this);
            
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
        }

        public int ReportInit()
        {
            setForm = new USMSettingsForm(this);

            if (setForm.ShowDialog() == DialogResult.OK)
            {
                UpdateSheets(setForm.SheetParamsList);
                customTab1.UpdateSheet(setForm.SheetParamsList);
                customTab2.UpdateSheet(setForm.SheetParamsList);

                return 0;
            }

            return 1;
        }

        void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                UpdateSheets(setForm.SheetParamsList);
                customTab1.UpdateSheet(setForm.SheetParamsList);
                customTab2.UpdateSheet(setForm.SheetParamsList);
            }
        }
    }
}
