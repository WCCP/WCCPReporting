﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPivotGrid;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using System.Data;
using DevExpress.XtraGrid.Views.Grid;

namespace Logica.Reports
{
    public class ReportOutlets : XtraTabPage, IPrint
    {
        static Guid tabId = Guid.NewGuid();
        public static Guid TabId
        {
            get { return tabId; }
        }
        public static string TabName
        {
            get { return Resource.ReportOutlets; }
        }

        BaseReportUserControl parent;
        PivotGridControl pivotGrid = new PivotGridControl();
        PanelControl headerPanel = new PanelControl();
        LabelControl lbDateFromHeader = new LabelControl();
        LabelControl lbDateFrom = new LabelControl();
        LabelControl lbDateToHeader = new LabelControl();
        LabelControl lbDateTo = new LabelControl();
        LabelControl lbChannelHeader = new LabelControl();
        LabelControl lbChannel = new LabelControl();
        LabelControl lbStaffLevelHeader = new LabelControl();
        LabelControl lbStaffLevel = new LabelControl();
        public ReportOutlets(BaseReportUserControl parent)
        {
            this.parent = parent;
            InitializeComponent();
            parent.RefreshClick += new BaseReportUserControl.MenuClickHandler(parent_RefreshClick);
            parent.ExportClick += new BaseReportUserControl.ExportMenuClickHandler(parent_ExportClick);
            parent.CloseClick += new BaseReportUserControl.MenuClickHandler(parent_CloseClick);
            pivotGrid.FieldAreaChanging += new PivotAreaChangingEventHandler(pivotGrid_FieldAreaChanging);
            pivotGrid.FieldAreaChanged += new PivotFieldEventHandler(pivotGrid_FieldAreaChanged);
            pivotGrid.CellDoubleClick += new PivotCellEventHandler(pivotGrid_CellDoubleClick);
            SetupColumns();
        }
        void pivotGrid_CellDoubleClick(object sender, PivotCellEventArgs e)
        {
            if (null != e.DataField)
            {
                CustomSummaryType cst = new CustomSummaryType(e.DataField.SummaryType);
                if (cst.ShowDialog() == DialogResult.OK)
                    e.DataField.SummaryType = cst.SelectedSummaryType;
            }
        }
        bool isSet = false;
        void pivotGrid_FieldAreaChanged(object sender, PivotFieldEventArgs e)
        {
            if (!isSet && e.Field.Area == PivotArea.DataArea)
            {
                CustomSummaryType cst = new CustomSummaryType();
                if (cst.ShowDialog() == DialogResult.OK)
                {
                    e.Field.SummaryType = cst.SelectedSummaryType;
                    isSet = true;
                }
            }
        }

        void pivotGrid_FieldAreaChanging(object sender, PivotAreaChangingEventArgs e)
        {
            isSet = false;
        }

        private void InitializeComponent()
        {
            Text = TabName;
            Tag = 0;
            AutoScroll = true;
            //Create main grid
            pivotGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            pivotGrid.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintHeadersOnEveryPage = true;
            Controls.Add(pivotGrid);
            //Create header
            int shift = 75;
            headerPanel.Dock = DockStyle.Top;
            headerPanel.Height = 20;
            headerPanel.Controls.AddRange(new Control[] { 
                lbDateFromHeader, lbDateFrom, lbDateToHeader, lbDateTo, lbChannelHeader, lbChannel, lbStaffLevelHeader, lbStaffLevel });
            lbDateFromHeader.Font = new System.Drawing.Font(lbDateFromHeader.Font, System.Drawing.FontStyle.Bold);
            lbDateFromHeader.Width = 100;
            lbDateFromHeader.Location = new System.Drawing.Point(shift, 2);
            lbDateFrom.Width = 100;
            lbDateFrom.Location = new System.Drawing.Point(100 + shift, 2);

            lbDateToHeader.Font = new System.Drawing.Font(lbDateToHeader.Font, System.Drawing.FontStyle.Bold);
            lbDateToHeader.Width = 120;
            lbDateToHeader.Location = new System.Drawing.Point(200 + shift, 2);
            lbDateTo.Width = 100;
            lbDateTo.Location = new System.Drawing.Point(320 + shift, 2);

            lbChannelHeader.Font = new System.Drawing.Font(lbChannelHeader.Font, System.Drawing.FontStyle.Bold);
            lbChannelHeader.Width = 100;
            lbChannelHeader.Location = new System.Drawing.Point(420 + shift, 2);
            lbChannel.Width = 100;
            lbChannel.Location = new System.Drawing.Point(520 + shift, 2);

            lbStaffLevelHeader.Font = new System.Drawing.Font(lbStaffLevelHeader.Font, System.Drawing.FontStyle.Bold);
            lbStaffLevelHeader.Width = 150;
            lbStaffLevelHeader.Location = new System.Drawing.Point(620 + shift, 2);
            lbStaffLevel.Width = 100;
            lbStaffLevel.Location = new System.Drawing.Point(770 + shift, 2);
            Controls.Add(headerPanel);
        }
        private void SetupColumns()
        {

            PivotGridField rmName = new PivotGridField("RM_Name", PivotArea.RowArea) { Caption = "M4" };
            PivotGridField dsmName = new PivotGridField("DSM_Name", PivotArea.RowArea) { Caption = "M3" };
            PivotGridField supervisorName = new PivotGridField("Supervisor_Name", PivotArea.RowArea) { Caption = "M2" };
            PivotGridField merchName = new PivotGridField("MerchName", PivotArea.RowArea) { Caption = "M1" }; 
            PivotGridField actionTypeName = new PivotGridField("ActionType_Name", PivotArea.FilterArea) { Caption = "Тип активности" };
            PivotGridField template = new PivotGridField("Template", PivotArea.FilterArea) { Caption = "Шаблон активности" }; ;
            PivotGridField templateID = new PivotGridField("Template_ID", PivotArea.FilterArea) { Caption = "ID шаблона активности" }; ;
            PivotGridField actionName = new PivotGridField("Action_Name", PivotArea.FilterArea) { Caption = "Активность" }; ;
            PivotGridField actionID = new PivotGridField("Action_ID", PivotArea.FilterArea) { Caption = "ID акции" };//"Кол. акций"
            PivotGridField olID = new PivotGridField("OL_ID", PivotArea.FilterArea) { Caption="ТТ" };
            PivotGridField olName = new PivotGridField("OLName", PivotArea.FilterArea) { Caption = "Факт имя" };
            PivotGridField olTradingName = new PivotGridField("OLTradingName", PivotArea.FilterArea) { Caption = "Юр. имя" };
            PivotGridField olAddress = new PivotGridField("OLAddress", PivotArea.FilterArea) { Caption = "Юр. адрес" };
            PivotGridField olDeliveryAddress = new PivotGridField("OLDeliveryAddress", PivotArea.FilterArea) { Caption = "Факт. адрес" };
            PivotGridField yMonth = new PivotGridField("yMonth", PivotArea.FilterArea) { Caption = "Период" };
            PivotGridField isAdhered = new PivotGridField("isAdhered", PivotArea.FilterArea) { Caption = "Признак подключения ТТ" };
            PivotGridField regionName = new PivotGridField("Region_name", PivotArea.RowArea) { Caption = "Регион" };
            PivotGridField tcName = new PivotGridField("Cust_NAME", PivotArea.RowArea) { Caption = "TC" };
            PivotGridField districtName = new PivotGridField("District_name", PivotArea.RowArea) { Caption = "Область" };

            //actionID.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count; 
            pivotGrid.Fields.AddRange(new PivotGridField[]{
                rmName,dsmName, supervisorName, merchName, actionTypeName, template, templateID,actionName,actionID,
            olID, olName, olTradingName, olAddress, olDeliveryAddress, yMonth, isAdhered, regionName, tcName, districtName
            });
        }

        #region Events
        void parent_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage == this)
            {
                if (parent.TabControl.TabPages.Contains(this))
                    parent.TabControl.TabPages.Remove(this);
            }
        }

        public void UpdateSheet(List<SheetParamCollection> listSettings)
        {
            SheetParamCollection settings = listSettings.Find(p => p.TabId == ReportOutlets.TabId);
            if (null != settings)
            {
                if (!parent.TabControl.TabPages.Contains(this))
                    parent.TabControl.TabPages.Add(this);
                UpdateData(settings);
            }
            else if (parent.TabControl.TabPages.Contains(this))
                parent.TabControl.TabPages.Remove(this);
        }
        
        void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
        {
            if (this != selectedPage)
                return;
            switch (type)
            {
                case ExportToType.Html:
                    PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
                    break;
                case ExportToType.Mht:
                    PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
                    break;
                case ExportToType.Pdf:
                    PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions() { Compressed = true });
                    break;
                case ExportToType.Rtf:
                    PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
                    break;
                case ExportToType.Txt:
                    PrepareCompositeLink().PrintingSystem.ExportToText(fName);
                    break;
                case ExportToType.Xls:
                    PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = TabName });
                    break;
                case ExportToType.Xlsx:
                    PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
                    break;
                case ExportToType.Bmp:
                    PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
                    break;
                case ExportToType.Csv:
                    PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
                    break;
            }
        }
        public CompositeLink PrepareCompositeLink()
        {
            CompositeLink pl = new CompositeLink(new PrintingSystem());
            GridControl tmpGrid = new GridControl();
            GridView tmprGridView = new GridView();
            tmprGridView.OptionsPrint.PrintHeader = false;
            tmpGrid.MainView = tmprGridView;

            DataTable source = new DataTable();
            List<object> lst = new List<object>();
            while (source.Columns.Count < 8)
            {
                source.Columns.Add();
                lst.Add("");
            }
            source.Rows.Add(lst.ToArray());
            source.Rows[0][0] = lbDateFromHeader.Text;
            source.Rows[0][1] = lbDateFrom.Text;
            source.Rows[0][2] = lbDateToHeader.Text;
            source.Rows[0][3] = lbDateTo.Text;
            source.Rows[0][4] = lbChannelHeader.Text;
            source.Rows[0][5] = lbChannel.Text;
            source.Rows[0][6] = lbStaffLevelHeader.Text;
            source.Rows[0][7] = lbStaffLevel.Text;
            tmpGrid.DataSource = source;
            this.Controls.Add(tmpGrid);
            pl.Links.Add(new PrintableComponentLink() { Component = tmpGrid });
            pl.Links.Add(new PrintableComponentLink() { Component = pivotGrid });
            pl.CreateDocument();
            this.Controls.Remove(tmpGrid);
            return pl;
        }
        void parent_RefreshClick(object sender, XtraTabPage selectedPage)
        {
            if (this != selectedPage)
                return;
            UpdateData(sheetSettings, true);
        }
        #endregion

        #region Update
        private SheetParamCollection sheetSettings;
        private void UpdateData(SheetParamCollection settings)
        {
            UpdateData(settings, false);
        }
        public void UpdateData(SheetParamCollection parameters, bool isForce)
        {
            if (!isForce && CompareSettings(parameters))
                return;

            WaitManager.StartWait();
            List<SqlParameter> list = new List<SqlParameter>();
            foreach (SheetParam par in parameters)
            {
                SqlParameter sPar = new SqlParameter(par.SqlParamName, par.Value);
                list.Add(sPar);
            }
            pivotGrid.DataSource = DataAccessLayer.ExecuteStoredProcedure("spDW_AU_ReportOutlets_New", list.ToArray());
            pivotGrid.DataMember = "Table";
            pivotGrid.BestFit();
            sheetSettings = parameters;
            CreateSettingSection(parameters);
            WaitManager.StopWait();
        }
        private bool CompareSettings(SheetParamCollection settings)
        {
            if (null == sheetSettings || sheetSettings.Count != settings.Count)
                return false;
            foreach (SheetParam param in settings)
                if (!sheetSettings.Exists(p => p.SqlParamName.Equals(param.SqlParamName) && p.Value.Equals(param.Value)))
                    return false;
            return true;
        }
        #endregion

        #region Settings section
        private void CreateSettingSection(SheetParamCollection dispPar)
        {
            lbDateFromHeader.Text = dispPar[0].DisplayParamName;
            lbDateFrom.Text = dispPar[0].DisplayValue.ToString();
            lbDateToHeader.Text = dispPar[1].DisplayParamName;
            lbDateTo.Text = dispPar[1].DisplayValue.ToString();
            lbChannelHeader.Text = dispPar[2].DisplayParamName;
            lbChannel.Text = dispPar[2].DisplayValue.ToString();
            lbStaffLevelHeader.Text = dispPar[3].DisplayParamName;
            lbStaffLevel.Text = dispPar[3].DisplayValue.ToString();
        }
        #endregion
    }
}
