﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.DataAccess;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using Logica.Reports.Common;
using DevExpress.XtraEditors;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports
{
    public partial class USMSettingsForm : XtraForm
    {
        BaseReportUserControl parent;
        Dictionary<object, object> channelTypeCollection = new Dictionary<object, object>();
        public DateTime DtStartDate
        {
            get { return new DateTime((cbYear1.SelectedItem as ComboBoxItem).Id, (cbMounth1.SelectedItem as ComboBoxItem).Id, 1); }
        }
        public DateTime DtEndDate
        {
            get { return new DateTime((cbYear2.SelectedItem as ComboBoxItem).Id, (cbMounth2.SelectedItem as ComboBoxItem).Id, 1).AddMonths(1).AddMilliseconds(-1); }
        }
        
        public USMSettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            lbReportsHeader.Text = Resource.Reports;
            BuildDates();
            BuildTree(null);
            BuildChanelType();
            BuildReportList(parent.Report);
            this.parent = parent;
            parent.CloseClick += new BaseReportUserControl.MenuClickHandler(parent_CloseClick);
        }
        
        void parent_CloseClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            Guid guid = Guid.Empty;
            
            if(selectedPage is BaseTab)
                guid = (selectedPage as BaseTab).SheetSettings.TabId;
            else if (selectedPage is ReportOutlets)
                guid = ReportOutlets.TabId;
            else if (selectedPage is ReportAdherence)
                guid = ReportAdherence.TabId;
            else
                throw new ArgumentNullException("Undefined selectedPage type");
            foreach(CheckedListBoxItem item in clbReports.Items)
            {
                if (item.Value.ToString() == guid.ToString())
                {
                    item.CheckState = CheckState.Unchecked;
                    break;
                }
            }
        }

        #region Build Controls
        private void BuildDates()
        {
            lbDateFrom.Text = Resource.DateFrom;
            lbDateTo.Text = Resource.DateTo;
            cbMounth1.Properties.Items.Clear();
            cbMounth2.Properties.Items.Clear();
            cbYear1.Properties.Items.Clear();
            cbYear2.Properties.Items.Clear();
            string[] mns = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.MonthNames;
            for (int i = 0; i < mns.Length - 1; i++)
            {
                cbMounth1.Properties.Items.Add(new ComboBoxItem() { Id = i + 1, Text = mns[i] });
                cbMounth2.Properties.Items.Add(new ComboBoxItem() { Id = i + 1, Text = mns[i] });
            }
            cbMounth1.SelectedIndex = DateTime.Now.Month - 1;
            cbMounth2.SelectedIndex = DateTime.Now.Month - 1;

            for (int i = DateTime.Now.Year - 5; i < 2025; i++)
            {
                cbYear1.Properties.Items.Add(new ComboBoxItem() { Id = i, Text = i.ToString() });
                cbYear2.Properties.Items.Add(new ComboBoxItem() { Id = i, Text = i.ToString() });
            }
            cbYear1.SelectedIndex = 5;
            cbYear2.SelectedIndex = 5;
        }
        private void BuildReportList(Report report)
        {
            clbReports.Items.Clear();
            clbReports.Items.Add(ReportOutlets.TabId, ReportOutlets.TabName, CheckState.Checked, true);
            clbReports.Items.Add(ReportAdherence.TabId, ReportAdherence.TabName, CheckState.Checked, true);
            
        }
        private void BuildChanelType()
        {
            lbChannelType.Text = Resource.ChannelType;
            DataSet ds2 = DataAccessLayer.ExecuteStoredProcedure("spDW_ChanelTypeList", new SqlParameter[] { });
            foreach (DataRow dr in ds2.Tables[0].Rows)
            {
                if (null == dr["ChanelType_id"] || String.IsNullOrEmpty(dr["ChanelType_id"].ToString()))
                    continue;
                channelTypeCollection.Add(dr["ChanelType"], dr["ChanelType_id"]);
                cbChannelType.Properties.Items.Add(dr["ChanelType"]);
            }
            cbChannelType.SelectedIndex = 0;
        }

        #region BuildTree
        private void BuildTree(object id)
        {
            staffLevelTree.ClearNodes();
            DataSet ds1;
            DataAccessLayer.OpenConnection();
            if(null != id)
                ds1 = DataAccessLayer.ExecuteStoredProcedure("spDW_StaffTree", new SqlParameter[] { new SqlParameter("@ChanelType_ID", id) });
            else
                ds1 = DataAccessLayer.ExecuteStoredProcedure("spDW_StaffTree", new SqlParameter[] { });
            DataAccessLayer.CloseConnection();
            AddChildNodes(null, ds1.Tables[0]);
        }
        private void AddChildNodes(TreeListNode parent,DataTable table)
        {
            foreach(DataRow dr in table.Rows)
            {
                if ((null == parent && dr["Parent_ID"].Equals(0)) || (null != parent && dr["Parent_ID"].Equals(((IList<object>)parent.Tag)[0])))
                {
                    TreeListNode node = staffLevelTree.AppendNode(new object[] { dr["Name"] }, parent);
                    node.Tag = new List<object>() { dr["Item_ID"], dr["StaffLevel_ID"], dr["ID"] };
                    AddChildNodes(node, table);
                }
            }
        }
        private void cbChannelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(cbChannelType.SelectedText))
                BuildTree(channelTypeCollection[cbChannelType.SelectedText]);
        }
        #endregion
        #endregion

        #region Base property
        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                List<SheetParamCollection> lst = new List<SheetParamCollection>();
                foreach (CheckedListBoxItem item in clbReports.CheckedItems)
                {
                    SheetParamCollection pars = new SheetParamCollection();
                    pars.MainHeader = Resource.Params;
                    pars.TabId = (Guid)item.Value;
                    pars.TableDataType = TableType.Fact;
                    pars.Add(new SheetParam() { DisplayParamName = Resource.DateFrom, SqlParamName = "@yMonth1", Value = DtStartDate.ToString("yyyyMM"), DisplayValue = DtStartDate.ToString("yyyy MMM") });
                    pars.Add(new SheetParam() { DisplayParamName = Resource.DateTo, SqlParamName = "@yMonth2", Value = DtEndDate.ToString("yyyyMM"), DisplayValue = DtEndDate.ToString("yyyy MMM") });
                    pars.Add(new SheetParam() { DisplayParamName = Resource.ChannelType, SqlParamName = "@ParChannel", Value = channelTypeCollection[cbChannelType.SelectedItem.ToString()], DisplayValue = cbChannelType.SelectedItem.ToString() });
                    pars.Add(new SheetParam() { DisplayParamName = Resource.StaffType, SqlParamName = "@StaffLevel_ID", Value = ((IList<object>)staffLevelTree.Selection[0].Tag)[1], DisplayValue = staffLevelTree.Selection[0].GetDisplayText(0) });
                    pars.Add(new SheetParam() { SqlParamName = "@ID", Value = ((IList<object>)staffLevelTree.Selection[0].Tag)[2] });
                    pars.Add(new SheetParam() { SqlParamName = "@debug", Value = 0 });
                    lst.Add(pars);
                }
                return lst;
            }
        }
        
        #endregion

        public bool ValidateData()
        {
            bool isValid = false;
            if(DtStartDate > DtEndDate)
                MessageBox.Show(Resource.InvalidDate, Resource.Error,MessageBoxButtons.OK,MessageBoxIcon.Warning);
            else if(null == cbChannelType.SelectedItem || String.IsNullOrEmpty(cbChannelType.SelectedItem.ToString()))
                MessageBox.Show(Resource.IncorrectChannel, Resource.Error,MessageBoxButtons.OK,MessageBoxIcon.Warning);
            else if (null == staffLevelTree.Selection || 0 == staffLevelTree.Selection.Count || String.IsNullOrEmpty(staffLevelTree.Selection[0].ToString()))
                MessageBox.Show(Resource.IncorrectStaffLevel, Resource.Error,MessageBoxButtons.OK,MessageBoxIcon.Warning);
            else
                isValid = true;
            return isValid;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (ValidateData())
                DialogResult = DialogResult.OK;
        }
    }
    internal class ComboBoxItem
    {
        int id;
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        public override string ToString()
        {
            return Text;
        }
    }
}
