﻿namespace Logica.Reports
{
    partial class USMSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(USMSettingsForm));
            this.panel1 = new DevExpress.XtraEditors.PanelControl();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainer1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.staffLevelTree = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.lbDateTo = new DevExpress.XtraEditors.LabelControl();
            this.lbReportsHeader = new DevExpress.XtraEditors.LabelControl();
            this.cbChannelType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbChannelType = new DevExpress.XtraEditors.LabelControl();
            this.lbDateFrom = new DevExpress.XtraEditors.LabelControl();
            this.clbReports = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.cbMounth1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbYear1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbYear2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbMounth2 = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.staffLevelTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMounth1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbYear1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbYear2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMounth2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Appearance.Options.UseBackColor = true;
            this.panel1.Controls.Add(this.btnHelp);
            this.panel1.Controls.Add(this.btnNo);
            this.panel1.Controls.Add(this.btnYes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 256);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(505, 36);
            this.panel1.TabIndex = 0;
            // 
            // btnHelp
            // 
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(424, 6);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 2;
            this.btnHelp.Text = "&Справка";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(343, 6);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 1;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(262, 6);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 0;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Panel1.Controls.Add(this.staffLevelTree);
            this.splitContainer1.Panel2.Controls.Add(this.cbYear2);
            this.splitContainer1.Panel2.Controls.Add(this.cbMounth2);
            this.splitContainer1.Panel2.Controls.Add(this.cbYear1);
            this.splitContainer1.Panel2.Controls.Add(this.cbMounth1);
            this.splitContainer1.Panel2.Controls.Add(this.lbDateTo);
            this.splitContainer1.Panel2.Controls.Add(this.lbReportsHeader);
            this.splitContainer1.Panel2.Controls.Add(this.cbChannelType);
            this.splitContainer1.Panel2.Controls.Add(this.lbChannelType);
            this.splitContainer1.Panel2.Controls.Add(this.lbDateFrom);
            this.splitContainer1.Panel2.Controls.Add(this.clbReports);
            this.splitContainer1.Size = new System.Drawing.Size(505, 256);
            this.splitContainer1.SplitterPosition = 210;
            this.splitContainer1.TabIndex = 1;
            // 
            // staffLevelTree
            // 
            this.staffLevelTree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.staffLevelTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.staffLevelTree.Location = new System.Drawing.Point(0, 0);
            this.staffLevelTree.Name = "staffLevelTree";
            this.staffLevelTree.OptionsSelection.InvertSelection = true;
            this.staffLevelTree.OptionsSelection.MultiSelect = true;
            this.staffLevelTree.OptionsSelection.UseIndicatorForSelection = true;
            this.staffLevelTree.OptionsView.ShowColumns = false;
            this.staffLevelTree.OptionsView.ShowIndicator = false;
            this.staffLevelTree.Size = new System.Drawing.Size(210, 256);
            this.staffLevelTree.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // lbDateTo
            // 
            this.lbDateTo.Location = new System.Drawing.Point(13, 50);
            this.lbDateTo.Name = "lbDateTo";
            this.lbDateTo.Size = new System.Drawing.Size(87, 13);
            this.lbDateTo.TabIndex = 11;
            this.lbDateTo.Text = "Дата окончания:";
            // 
            // lbReportsHeader
            // 
            this.lbReportsHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbReportsHeader.Appearance.Options.UseFont = true;
            this.lbReportsHeader.Location = new System.Drawing.Point(13, 115);
            this.lbReportsHeader.Name = "lbReportsHeader";
            this.lbReportsHeader.Size = new System.Drawing.Size(49, 13);
            this.lbReportsHeader.TabIndex = 9;
            this.lbReportsHeader.Text = "Отчеты:";
            // 
            // cbChannelType
            // 
            this.cbChannelType.Location = new System.Drawing.Point(125, 78);
            this.cbChannelType.Name = "cbChannelType";
            this.cbChannelType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbChannelType.Size = new System.Drawing.Size(152, 20);
            this.cbChannelType.TabIndex = 4;
            this.cbChannelType.SelectedIndexChanged += new System.EventHandler(this.cbChannelType_SelectedIndexChanged);
            // 
            // lbChannelType
            // 
            this.lbChannelType.Location = new System.Drawing.Point(13, 81);
            this.lbChannelType.Name = "lbChannelType";
            this.lbChannelType.Size = new System.Drawing.Size(61, 13);
            this.lbChannelType.TabIndex = 3;
            this.lbChannelType.Text = "Тип канала:";
            // 
            // lbDateFrom
            // 
            this.lbDateFrom.Location = new System.Drawing.Point(13, 16);
            this.lbDateFrom.Name = "lbDateFrom";
            this.lbDateFrom.Size = new System.Drawing.Size(69, 13);
            this.lbDateFrom.TabIndex = 2;
            this.lbDateFrom.Text = "Дата начала:";
            // 
            // clbReports
            // 
            this.clbReports.CheckOnClick = true;
            this.clbReports.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clbReports.Location = new System.Drawing.Point(0, 142);
            this.clbReports.Name = "clbReports";
            this.clbReports.Size = new System.Drawing.Size(289, 114);
            this.clbReports.TabIndex = 0;
            // 
            // cbMounth1
            // 
            this.cbMounth1.Location = new System.Drawing.Point(125, 13);
            this.cbMounth1.Name = "cbMounth1";
            this.cbMounth1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbMounth1.Size = new System.Drawing.Size(73, 20);
            this.cbMounth1.TabIndex = 12;
            // 
            // cbYear1
            // 
            this.cbYear1.Location = new System.Drawing.Point(204, 13);
            this.cbYear1.Name = "cbYear1";
            this.cbYear1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbYear1.Size = new System.Drawing.Size(73, 20);
            this.cbYear1.TabIndex = 13;
            // 
            // cbYear2
            // 
            this.cbYear2.Location = new System.Drawing.Point(204, 47);
            this.cbYear2.Name = "cbYear2";
            this.cbYear2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbYear2.Size = new System.Drawing.Size(73, 20);
            this.cbYear2.TabIndex = 15;
            // 
            // cbMounth2
            // 
            this.cbMounth2.Location = new System.Drawing.Point(125, 47);
            this.cbMounth2.Name = "cbMounth2";
            this.cbMounth2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbMounth2.Size = new System.Drawing.Size(73, 20);
            this.cbMounth2.TabIndex = 14;
            // 
            // USMSettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(505, 292);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "USMSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры";
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.staffLevelTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMounth1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbYear1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbYear2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMounth2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        
        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainer1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.LabelControl lbChannelType;
        private DevExpress.XtraEditors.ComboBoxEdit cbChannelType;
        private DevExpress.XtraEditors.LabelControl lbReportsHeader;
        private DevExpress.XtraTreeList.TreeList staffLevelTree;
        private DevExpress.XtraEditors.CheckedListBoxControl clbReports;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.PanelControl panel1;
        private DevExpress.XtraEditors.LabelControl lbDateTo;
        private DevExpress.XtraEditors.LabelControl lbDateFrom;
        private DevExpress.XtraEditors.ComboBoxEdit cbYear2;
        private DevExpress.XtraEditors.ComboBoxEdit cbMounth2;
        private DevExpress.XtraEditors.ComboBoxEdit cbYear1;
        private DevExpress.XtraEditors.ComboBoxEdit cbMounth1;
    }
}