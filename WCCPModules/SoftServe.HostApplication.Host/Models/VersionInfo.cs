﻿namespace SoftServe.HostApplication.Host.Models
{
    public class VersionInfo
    {
        private readonly string _fileVersion;

        public int MajorPart { get; private set; }
        public int MinorPart { get; private set; }
        public int BuildPart { get; private set; }
        public int PrivatePart { get; private set; }

        public VersionInfo(string fileVersion)
        {
            _fileVersion = fileVersion;

            string[] lVersionDescr = fileVersion.Split('.');

            MajorPart = int.Parse(lVersionDescr[0]);
            MinorPart = int.Parse(lVersionDescr[1]);
            BuildPart = int.Parse(lVersionDescr[2]);
            PrivatePart = int.Parse(lVersionDescr[3]);
        }

        public override string ToString()
        {
            return _fileVersion;
        }

        public override bool Equals(object obj)
        {
            return obj.ToString() == ToString();
        }

        public override int GetHashCode()
        {
            return _fileVersion.GetHashCode();
        }

        public static string DefaultVersion
        {
            get { return "0.0.0.0"; }
        }

        public static bool operator <(VersionInfo info1, VersionInfo info2)
        {
            if (info1.Equals(info2))
                return false;

            if (info1.MajorPart != info2.MajorPart)
                return info1.MajorPart < info2.MajorPart;

            if (info1.MinorPart != info2.MinorPart)
                return info1.MinorPart < info2.MinorPart;

            if (info1.BuildPart != info2.BuildPart)
                return info1.BuildPart < info2.BuildPart;

            if (info1.PrivatePart != info2.PrivatePart)
                return info1.PrivatePart < info2.PrivatePart;

            return false;
        }

        public static bool operator >(VersionInfo info1, VersionInfo info2)
        {
            if (info1.Equals(info2))
                return false;

            if (info1.MajorPart != info2.MajorPart)
                return info1.MajorPart > info2.MajorPart;

            if (info1.MinorPart != info2.MinorPart)
                return info1.MinorPart > info2.MinorPart;

            if (info1.BuildPart != info2.BuildPart)
                return info1.BuildPart > info2.BuildPart;

            if (info1.PrivatePart != info2.PrivatePart)
                return info1.PrivatePart > info2.PrivatePart;

            return false;
        }
    }
}
