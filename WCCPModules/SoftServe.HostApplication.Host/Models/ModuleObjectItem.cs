﻿using BLToolkit.Mapping;

namespace ModularWinApp.Models
{
    public class ModuleObjectItem
    {
        [MapField("Object_ID")]
        public int ReportId { get; set; }

        [MapField("ObjectShortName")]
        public string Name { get; set; }
        
        [MapField("ObjectLibrary")]
        public string ObjectLibrary { get; set; }
        [MapField("ObjectTypeLibrary")]
        public string ObjectTypeLibrary { get; set; }

        public string ObjectVersion { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return ReportId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ModuleTreeItem)
            {
                return ReportId.Equals((obj as ModuleTreeItem).ReportId);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
