﻿using BLToolkit.Mapping;

namespace ModularWinApp.Models
{
    public class ModuleTreeItem
    {
        [MapField("Parent_ID")]
        public int ParentId { get; set; }

        [MapField("Item_ID")]
        public int ItemId { get; set; }

        [MapField("ID")]
        public int ReportId { get; set; }

        public string Name { get; set; }
        public string SortOrder { get; set; }

        [MapField("ObjectLibrary")]
        public string ObjectLibrary { get; set; }
        [MapField("ObjectTypeLibrary")]
        public string ObjectTypeLibrary { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return ReportId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ModuleTreeItem)
            {
                return ReportId.Equals((obj as ModuleTreeItem).ReportId);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
