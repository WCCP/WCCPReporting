﻿using SoftServe.HostApplication.Host.Utils;

namespace SoftServe.HostApplication.Host.Models
{
    public class ModuleVersionInfo
    {
        public int NeedsUpdate
        {
            get
            {
                if (CurrVersion > ServerVersion)
                    return (int)VersionStatus.ServerVersionIsLess;

                if (CurrVersion < ServerVersion)
                    return (int)VersionStatus.CurrentVersionIsLess;

                return (int)VersionStatus.VersionsAreEqual;
            }
        }

        public bool WillBeUpdated { get; set; }

        public int ReportId { get; set; }

        public string Name { get; set; }

        public string ObjectLibrary { get; set; }

        public VersionInfo CurrVersion { get; set; }

        public VersionInfo ServerVersion { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return ReportId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ModuleVersionInfo)
            {
                return ReportId.Equals((obj as ModuleVersionInfo).ReportId);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
