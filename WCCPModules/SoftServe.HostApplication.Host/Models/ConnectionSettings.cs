﻿using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Text;

namespace SoftServe.HostApplication.Host.Models
{
    [Serializable]
    public class ConnectionSettings
    {
        SqlConnectionStringBuilder _connectionStringBuilder;

        public ConnectionSettings()
        {
            _connectionStringBuilder = new SqlConnectionStringBuilder();
        }

        public string UserName
        {
            get
            {
                return UseWidowsAuth ? string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName) : _connectionStringBuilder.UserID;
            }

            set { _connectionStringBuilder.UserID = value; }
        }

        public bool UseWidowsAuth
        {
            get
            {
                return _connectionStringBuilder.IntegratedSecurity;
            }

            set { _connectionStringBuilder.IntegratedSecurity = value; }
        }

        public string ServerName
        {
            get
            {
                return _connectionStringBuilder.DataSource;
            }

            set { _connectionStringBuilder.DataSource = value; }
        }
        public string DatabaseName
        {
            get
            {
                return _connectionStringBuilder.InitialCatalog;
            }

            set { _connectionStringBuilder.InitialCatalog = value; }
        }

        public string Password
        {
            get
            {
                return _connectionStringBuilder.Password;
            }

            set { _connectionStringBuilder.Password = value; }
        }

        public bool IsLDB
        {
            get { return !string.IsNullOrEmpty(DatabaseName) && DatabaseName.ToUpper().Contains("LDB"); }
        }

        public bool IsEmpty
        {
            get { return _connectionStringBuilder == null || string.IsNullOrEmpty(_connectionStringBuilder.ConnectionString); }
        }

        public string ConnectionString
        {
            get { return _connectionStringBuilder != null ? _connectionStringBuilder.ConnectionString : string.Empty; }
        }

        public ConnectionSettings CreateCopy()
        {
            ConnectionSettings copy = new ConnectionSettings();

            copy.ServerName = string.Copy(ServerName);
            copy.DatabaseName = string.Copy(DatabaseName);
            copy.UseWidowsAuth = UseWidowsAuth;
            copy.UserName = string.Copy(UserName);
            copy.Password = string.Copy(Password);

            return copy;
        }
        
    }
}
