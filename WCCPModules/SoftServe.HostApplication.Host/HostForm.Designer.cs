﻿namespace ModularWinApp
{
    partial class HostForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, null, true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HostForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barModuleTree = new ModularWinApp.VisualComponents.WccpModuleTree(this.components);
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.barAndDockingController = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.dockPanelModuleTree = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelInnerModuleTree = new DevExpress.XtraEditors.PanelControl();
            this.searchControlModules = new DevExpress.XtraEditors.SearchControl();
            this.documentManager = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.barAndDockingControllerRibbon = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.tabbedView = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.rStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.tServer = new DevExpress.XtraBars.BarStaticItem();
            this.tServerValue = new DevExpress.XtraBars.BarStaticItem();
            this.tDataBase = new DevExpress.XtraBars.BarStaticItem();
            this.tDataBaseValue = new DevExpress.XtraBars.BarStaticItem();
            this.tUserName = new DevExpress.XtraBars.BarStaticItem();
            this.tUserNameValue = new DevExpress.XtraBars.BarStaticItem();
            this.tDisconnected = new DevExpress.XtraBars.BarStaticItem();
            this.tMoluleName = new DevExpress.XtraBars.BarStaticItem();
            this.tModuleVersion = new DevExpress.XtraBars.BarStaticItem();
            this.bVersionImage = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollectionVersion = new DevExpress.Utils.ImageCollection(this.components);
            this.rMainMenu = new ModularWinApp.VisualComponents.WccpRibbonControl();
            this.bDbSettings = new DevExpress.XtraBars.BarButtonItem();
            this.bModuleSettings = new DevExpress.XtraBars.BarButtonItem();
            this.bAbout = new DevExpress.XtraBars.BarButtonItem();
            this.rpMenu = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgDbSettings = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgModuleSettings = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgAbout = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barModuleTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).BeginInit();
            this.dockPanelModuleTree.SuspendLayout();
            this.dockPanel_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelInnerModuleTree)).BeginInit();
            this.panelInnerModuleTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControlModules.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingControllerRibbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rMainMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // barModuleTree
            // 
            this.barModuleTree.ActiveGroup = null;
            this.barModuleTree.Appearance.ButtonPressed.BackColor = System.Drawing.Color.White;
            this.barModuleTree.Appearance.ButtonPressed.Options.UseBackColor = true;
            this.barModuleTree.Appearance.GroupHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barModuleTree.Appearance.GroupHeader.Options.UseFont = true;
            this.barModuleTree.Appearance.GroupHeaderActive.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barModuleTree.Appearance.GroupHeaderActive.Options.UseFont = true;
            this.barModuleTree.Appearance.Item.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barModuleTree.Appearance.Item.Options.UseFont = true;
            this.barModuleTree.Appearance.ItemActive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barModuleTree.Appearance.ItemActive.Options.UseFont = true;
            this.barModuleTree.Appearance.ItemHotTracked.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barModuleTree.Appearance.ItemHotTracked.Options.UseFont = true;
            this.barModuleTree.Appearance.ItemPressed.BackColor = System.Drawing.Color.White;
            this.barModuleTree.Appearance.ItemPressed.Options.UseBackColor = true;
            this.barModuleTree.BackColor = System.Drawing.Color.White;
            this.barModuleTree.ContentButtonHint = null;
            this.barModuleTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.barModuleTree.ExplorerBarGroupInterval = 0;
            this.barModuleTree.ExplorerBarGroupOuterIndent = 0;
            this.barModuleTree.HotTrackedItemCursor = System.Windows.Forms.Cursors.Default;
            this.barModuleTree.LinkInterval = 0;
            this.barModuleTree.LoadedModuleMark = null;
            this.barModuleTree.Location = new System.Drawing.Point(2, 32);
            this.barModuleTree.Name = "barModuleTree";
            this.barModuleTree.OptionsNavPane.ExpandedWidth = 283;
            this.barModuleTree.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.barModuleTree.Size = new System.Drawing.Size(283, 607);
            this.barModuleTree.SmallImages = this.imageCollection;
            this.barModuleTree.TabIndex = 2;
            this.barModuleTree.Text = "barModuleTree";
            this.barModuleTree.View = new DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("DevExpress Dark Style");
            this.barModuleTree.EnterClick += new System.EventHandler(this.barModuleTree_EnterClick);
            this.barModuleTree.DoubleClick += new System.EventHandler(this.barModuleTree_DoubleClick);
            // 
            // imageCollection
            // 
            this.imageCollection.ImageSize = new System.Drawing.Size(20, 20);
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "filler.png");
            this.imageCollection.Images.SetKeyName(1, "check.png");
            this.imageCollection.Images.SetKeyName(2, "cross2.png");
            // 
            // dockManager
            // 
            this.dockManager.Controller = this.barAndDockingController;
            this.dockManager.DockingOptions.HideImmediatelyOnAutoHide = true;
            this.dockManager.Form = this;
            this.dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelModuleTree});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "ModularWinApp.VisualComponents.WccpRibbonControl"});
            // 
            // barAndDockingController
            // 
            this.barAndDockingController.AppearancesDocking.HideContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barAndDockingController.AppearancesDocking.PanelCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barAndDockingController.AppearancesDocking.PanelCaption.Options.UseFont = true;
            this.barAndDockingController.AppearancesDocking.PanelCaptionActive.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barAndDockingController.AppearancesDocking.PanelCaptionActive.Options.UseFont = true;
            this.barAndDockingController.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.barAndDockingController.LookAndFeel.UseDefaultLookAndFeel = false;
            this.barAndDockingController.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // dockPanelModuleTree
            // 
            this.dockPanelModuleTree.Appearance.BackColor = System.Drawing.Color.DimGray;
            this.dockPanelModuleTree.Appearance.BackColor2 = System.Drawing.Color.DimGray;
            this.dockPanelModuleTree.Appearance.Options.UseBackColor = true;
            this.dockPanelModuleTree.Controls.Add(this.dockPanel_Container);
            this.dockPanelModuleTree.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelModuleTree.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dockPanelModuleTree.ID = new System.Guid("46214533-951d-423f-9537-209331bb6b45");
            this.dockPanelModuleTree.Location = new System.Drawing.Point(0, 61);
            this.dockPanelModuleTree.Name = "dockPanelModuleTree";
            this.dockPanelModuleTree.Options.ShowCloseButton = false;
            this.dockPanelModuleTree.OriginalSize = new System.Drawing.Size(295, 200);
            this.dockPanelModuleTree.Size = new System.Drawing.Size(295, 669);
            this.dockPanelModuleTree.Text = "БИЗНЕС МОДУЛИ";
            // 
            // dockPanel_Container
            // 
            this.dockPanel_Container.Controls.Add(this.panelInnerModuleTree);
            this.dockPanel_Container.Location = new System.Drawing.Point(4, 24);
            this.dockPanel_Container.Name = "dockPanel_Container";
            this.dockPanel_Container.Size = new System.Drawing.Size(287, 641);
            this.dockPanel_Container.TabIndex = 0;
            // 
            // panelInnerModuleTree
            // 
            this.panelInnerModuleTree.Appearance.BackColor = System.Drawing.Color.White;
            this.panelInnerModuleTree.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.panelInnerModuleTree.Appearance.Options.UseBackColor = true;
            this.panelInnerModuleTree.Controls.Add(this.searchControlModules);
            this.panelInnerModuleTree.Controls.Add(this.barModuleTree);
            this.panelInnerModuleTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInnerModuleTree.Location = new System.Drawing.Point(0, 0);
            this.panelInnerModuleTree.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.panelInnerModuleTree.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelInnerModuleTree.Name = "panelInnerModuleTree";
            this.panelInnerModuleTree.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.panelInnerModuleTree.Size = new System.Drawing.Size(287, 641);
            this.panelInnerModuleTree.TabIndex = 4;
            // 
            // searchControlModules
            // 
            this.searchControlModules.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchControlModules.Client = this.barModuleTree;
            this.searchControlModules.Location = new System.Drawing.Point(4, 6);
            this.searchControlModules.Name = "searchControlModules";
            this.searchControlModules.Properties.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.searchControlModules.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.searchControlModules.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.searchControlModules.Properties.Appearance.Options.UseBackColor = true;
            this.searchControlModules.Properties.Appearance.Options.UseForeColor = true;
            this.searchControlModules.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControlModules.Properties.Client = this.barModuleTree;
            this.searchControlModules.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.searchControlModules.Properties.NullValuePrompt = "Поиск...";
            this.searchControlModules.Properties.UseCtrlScroll = true;
            this.searchControlModules.Size = new System.Drawing.Size(279, 20);
            this.searchControlModules.TabIndex = 3;
            this.searchControlModules.TextChanged += new System.EventHandler(this.searchControlModules_TextChanged);
            this.searchControlModules.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchControlModules_KeyDown);
            // 
            // documentManager
            // 
            this.documentManager.BarAndDockingController = this.barAndDockingControllerRibbon;
            this.documentManager.MdiParent = this;
            this.documentManager.View = this.tabbedView;
            this.documentManager.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView});
            // 
            // barAndDockingControllerRibbon
            // 
            this.barAndDockingControllerRibbon.AppearancesRibbon.Item.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.Item.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.barAndDockingControllerRibbon.AppearancesRibbon.Item.Options.UseFont = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.Item.Options.UseForeColor = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescription.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescription.Options.UseFont = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescriptionDisabled.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescriptionDisabled.Options.UseFont = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescriptionHovered.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescriptionHovered.Options.UseFont = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescriptionPressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDescriptionPressed.Options.UseFont = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDisabled.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemDisabled.Options.UseFont = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemHovered.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemHovered.Options.UseFont = true;
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemPressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barAndDockingControllerRibbon.AppearancesRibbon.ItemPressed.Options.UseFont = true;
            this.barAndDockingControllerRibbon.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingControllerRibbon.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // tabbedView
            // 
            this.tabbedView.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabbedView.AppearancePage.Header.Options.UseFont = true;
            this.tabbedView.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabbedView.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DarkOrange;
            this.tabbedView.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tabbedView.AppearancePage.HeaderActive.Options.UseForeColor = true;
            this.tabbedView.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabbedView.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.tabbedView.AppearancePage.HeaderSelected.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabbedView.AppearancePage.HeaderSelected.Options.UseFont = true;
            this.tabbedView.DocumentGroupProperties.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            serializableAppearanceObject1.Options.UseFont = true;
            this.tabbedView.DocumentGroupProperties.CustomHeaderButtons.AddRange(new DevExpress.XtraTab.Buttons.CustomHeaderButton[] {
            new DevExpress.XtraTab.Buttons.CustomHeaderButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "ЗАКРЫТЬ ВСЕ", -1, true, true, DevExpress.XtraEditors.ImageLocation.MiddleLeft, ((System.Drawing.Image)(resources.GetObject("tabbedView.DocumentGroupProperties.CustomHeaderButtons"))), serializableAppearanceObject1, "", null, null, true)});
            this.tabbedView.CustomHeaderButtonClick += new DevExpress.XtraBars.Docking2010.Views.CustomHeaderButtonEventHandler(this.tabbedView_CustomHeaderButtonClick);
            // 
            // rStatusBar
            // 
            this.rStatusBar.ItemLinks.Add(this.tServer, true);
            this.rStatusBar.ItemLinks.Add(this.tServerValue);
            this.rStatusBar.ItemLinks.Add(this.tDataBase, true);
            this.rStatusBar.ItemLinks.Add(this.tDataBaseValue);
            this.rStatusBar.ItemLinks.Add(this.tUserName, true);
            this.rStatusBar.ItemLinks.Add(this.tUserNameValue);
            this.rStatusBar.ItemLinks.Add(this.tDisconnected, true);
            this.rStatusBar.ItemLinks.Add(this.tMoluleName, true);
            this.rStatusBar.ItemLinks.Add(this.tModuleVersion);
            this.rStatusBar.ItemLinks.Add(this.bVersionImage);
            this.rStatusBar.Location = new System.Drawing.Point(0, 730);
            this.rStatusBar.Name = "rStatusBar";
            this.rStatusBar.Ribbon = this.rMainMenu;
            this.rStatusBar.Size = new System.Drawing.Size(1256, 31);
            this.rStatusBar.ToolTipController = this.toolTipController;
            // 
            // tServer
            // 
            this.tServer.Caption = "Сервер:";
            this.tServer.Id = 5;
            this.tServer.Name = "tServer";
            this.tServer.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // tServerValue
            // 
            this.tServerValue.Caption = "Cherry";
            this.tServerValue.Id = 11;
            this.tServerValue.Name = "tServerValue";
            this.tServerValue.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // tDataBase
            // 
            this.tDataBase.Caption = "База:";
            this.tDataBase.Id = 6;
            this.tDataBase.Name = "tDataBase";
            this.tDataBase.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // tDataBaseValue
            // 
            this.tDataBaseValue.Caption = "SalesWorks";
            this.tDataBaseValue.Id = 12;
            this.tDataBaseValue.Name = "tDataBaseValue";
            this.tDataBaseValue.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // tUserName
            // 
            this.tUserName.Caption = "Пользователь: ";
            this.tUserName.Id = 7;
            this.tUserName.Name = "tUserName";
            this.tUserName.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // tUserNameValue
            // 
            this.tUserNameValue.Caption = "Alisa Selezneva";
            this.tUserNameValue.Id = 13;
            this.tUserNameValue.Name = "tUserNameValue";
            this.tUserNameValue.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // tDisconnected
            // 
            this.tDisconnected.Caption = "Не удалось подключиться к серверу";
            this.tDisconnected.Id = 10;
            this.tDisconnected.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tDisconnected.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Red;
            this.tDisconnected.ItemAppearance.Normal.Options.UseFont = true;
            this.tDisconnected.ItemAppearance.Normal.Options.UseForeColor = true;
            this.tDisconnected.Name = "tDisconnected";
            this.tDisconnected.TextAlignment = System.Drawing.StringAlignment.Near;
            this.tDisconnected.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // tMoluleName
            // 
            this.tMoluleName.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.tMoluleName.Caption = " wccpMorningMeeting.dll";
            this.tMoluleName.Id = 14;
            this.tMoluleName.Name = "tMoluleName";
            this.tMoluleName.TextAlignment = System.Drawing.StringAlignment.Near;
            this.tMoluleName.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // tModuleVersion
            // 
            this.tModuleVersion.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.tModuleVersion.Caption = "2.0.0.9999";
            this.tModuleVersion.Id = 8;
            this.tModuleVersion.Name = "tModuleVersion";
            this.tModuleVersion.TextAlignment = System.Drawing.StringAlignment.Near;
            this.tModuleVersion.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bVersionImage
            // 
            this.bVersionImage.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bVersionImage.CanOpenEdit = false;
            this.bVersionImage.Caption = "Статус";
            this.bVersionImage.CaptionAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bVersionImage.Edit = this.repositoryItemImageComboBox1;
            this.bVersionImage.Id = 16;
            this.bVersionImage.Name = "bVersionImage";
            this.bVersionImage.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            this.bVersionImage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bVersionImage.Width = 30;
            this.bVersionImage.EditValueChanged += new System.EventHandler(this.bVersionImage_EditValueChanged);
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemImageComboBox1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemImageComboBox1.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemImageComboBox1.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemImageComboBox1.Appearance.Options.UseBackColor = true;
            this.repositoryItemImageComboBox1.Appearance.Options.UseBorderColor = true;
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemImageComboBox1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Версии совпадают", 0, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Рабочая версия ниже", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Серверная версия ниже", 1, 2)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.ReadOnly = true;
            this.repositoryItemImageComboBox1.SmallImages = this.imageCollectionVersion;
            // 
            // imageCollectionVersion
            // 
            this.imageCollectionVersion.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionVersion.ImageStream")));
            this.imageCollectionVersion.Images.SetKeyName(0, "bad.png");
            this.imageCollectionVersion.Images.SetKeyName(1, "good.png");
            this.imageCollectionVersion.Images.SetKeyName(2, "warning.png");
            // 
            // rMainMenu
            // 
            this.rMainMenu.AllowKeyTips = false;
            this.rMainMenu.Controller = this.barAndDockingControllerRibbon;
            this.rMainMenu.ExpandCollapseItem.Id = 0;
            this.rMainMenu.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.rMainMenu.ExpandCollapseItem,
            this.bDbSettings,
            this.bModuleSettings,
            this.bAbout,
            this.tServer,
            this.tDataBase,
            this.tUserName,
            this.tModuleVersion,
            this.tDisconnected,
            this.tServerValue,
            this.tDataBaseValue,
            this.tUserNameValue,
            this.tMoluleName,
            this.bVersionImage});
            this.rMainMenu.Location = new System.Drawing.Point(0, 0);
            this.rMainMenu.MaxItemId = 17;
            this.rMainMenu.Name = "rMainMenu";
            this.rMainMenu.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMenu});
            this.rMainMenu.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1});
            this.rMainMenu.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.TabletOfficeEx;
            this.rMainMenu.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.rMainMenu.ShowGroupCaption = false;
            this.rMainMenu.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.rMainMenu.Size = new System.Drawing.Size(1256, 61);
            this.rMainMenu.StatusBar = this.rStatusBar;
            this.rMainMenu.ToolTipController = this.toolTipController;
            // 
            // bDbSettings
            // 
            this.bDbSettings.Caption = "НАСТРОЙКИ БД";
            this.bDbSettings.Id = 1;
            this.bDbSettings.Name = "bDbSettings";
            this.bDbSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barDbConfig_ItemClick);
            // 
            // bModuleSettings
            // 
            this.bModuleSettings.Caption = "НАСТРОЙКИ";
            this.bModuleSettings.Id = 2;
            this.bModuleSettings.Name = "bModuleSettings";
            this.bModuleSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSettings_ItemClick);
            // 
            // bAbout
            // 
            this.bAbout.Caption = "О ПРОГРАММЕ";
            this.bAbout.Glyph = ((System.Drawing.Image)(resources.GetObject("bAbout.Glyph")));
            this.bAbout.Id = 3;
            this.bAbout.Name = "bAbout";
            this.bAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAbout_ItemClick);
            // 
            // rpMenu
            // 
            this.rpMenu.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgDbSettings,
            this.rpgModuleSettings,
            this.rpgAbout});
            this.rpMenu.Name = "rpMenu";
            this.rpMenu.Text = "ГЛАВНОЕ МЕНЮ";
            // 
            // rpgDbSettings
            // 
            this.rpgDbSettings.ItemLinks.Add(this.bDbSettings);
            this.rpgDbSettings.Name = "rpgDbSettings";
            this.rpgDbSettings.Text = "НАСТРОЙКИ БД";
            // 
            // rpgModuleSettings
            // 
            this.rpgModuleSettings.ItemLinks.Add(this.bModuleSettings);
            this.rpgModuleSettings.Name = "rpgModuleSettings";
            this.rpgModuleSettings.Text = "НАСТРОЙКИ";
            // 
            // rpgAbout
            // 
            this.rpgAbout.ItemLinks.Add(this.bAbout);
            this.rpgAbout.Name = "rpgAbout";
            this.rpgAbout.Tag = "AlignRight";
            this.rpgAbout.Text = "О ПРОГАММЕ";
            // 
            // toolTipController
            // 
            this.toolTipController.BeforeShow += new DevExpress.Utils.ToolTipControllerBeforeShowEventHandler(this.toolTipController_BeforeShow);
            // 
            // HostForm
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.True;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1256, 761);
            this.Controls.Add(this.dockPanelModuleTree);
            this.Controls.Add(this.rStatusBar);
            this.Controls.Add(this.rMainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "HostForm";
            this.Opacity = 0D;
            this.Ribbon = this.rMainMenu;
            this.StatusBar = this.rStatusBar;
            this.Text = "WCCP reporting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHost_FormClosing);
            this.Load += new System.EventHandler(this.frmHost_Load);
            this.Shown += new System.EventHandler(this.frmHost_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barModuleTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).EndInit();
            this.dockPanelModuleTree.ResumeLayout(false);
            this.dockPanel_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelInnerModuleTree)).EndInit();
            this.panelInnerModuleTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControlModules.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingControllerRibbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rMainMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ModularWinApp.VisualComponents.WccpModuleTree barModuleTree;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelModuleTree;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel_Container;
        private DevExpress.XtraEditors.SearchControl searchControlModules;
        private DevExpress.XtraEditors.PanelControl panelInnerModuleTree;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar rStatusBar;
        private ModularWinApp.VisualComponents.WccpRibbonControl rMainMenu;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpMenu;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgDbSettings;
        private DevExpress.XtraBars.BarButtonItem bDbSettings;
        private DevExpress.XtraBars.BarButtonItem bModuleSettings;
        private DevExpress.XtraBars.BarButtonItem bAbout;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgModuleSettings;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAbout;
        private DevExpress.XtraBars.BarStaticItem tServer;
        private DevExpress.XtraBars.BarStaticItem tDataBase;
        private DevExpress.XtraBars.BarStaticItem tUserName;
        private DevExpress.XtraBars.BarStaticItem tModuleVersion;
        private DevExpress.XtraBars.BarStaticItem tDisconnected;
        private DevExpress.XtraBars.BarStaticItem tServerValue;
        private DevExpress.XtraBars.BarStaticItem tDataBaseValue;
        private DevExpress.XtraBars.BarStaticItem tUserNameValue;
        private DevExpress.XtraBars.BarStaticItem tMoluleName;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingControllerRibbon;
        private DevExpress.XtraBars.BarEditItem bVersionImage;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.Utils.ImageCollection imageCollectionVersion;
        private DevExpress.Utils.ToolTipController toolTipController;
    }
}