﻿using System;
using System.Windows.Forms;
using SoftServe.HostApplication.Host.Utils;

namespace ModularWinApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            HostUpdateLauncher launcher = new HostUpdateLauncher();
            launcher.ExecuteLauncher();

            Application.Run(new HostForm());
        }
    }
}
