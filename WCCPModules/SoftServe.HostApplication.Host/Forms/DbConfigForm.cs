﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using System;
using SoftServe.HostApplication.Host.Models;
using SoftServe.HostApplication.Host.Properties;

namespace ModularWinApp.Forms
{
    partial class DbConfigForm : XtraForm
    {
        public DbConfigForm()
        {
            InitializeComponent();
        }
                
        private void rgAuthentificationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool enableSqlServerAuthorization = rgAuthentificationType.SelectedIndex == 1;

            lUserName.Enabled = enableSqlServerAuthorization;
            lPassWord.Enabled = enableSqlServerAuthorization;
            tUserName.Enabled = enableSqlServerAuthorization;
            tPassWord.Enabled = enableSqlServerAuthorization;
        }

        private void DbConfigForm_Load(object sender, EventArgs e)
        {
            if (HostConnectionSettings.Servers.Count > 0)
            {
                cServer.Properties.Items.AddRange(HostConnectionSettings.Servers);
                cServer.SelectedIndex = 0;
            }

            if (HostConnectionSettings.DataBases.Count > 0)
            {
                cDataBase.Properties.Items.AddRange(HostConnectionSettings.DataBases);
                cDataBase.SelectedIndex = 0;
            }

            ConnectionSettings lSettings = HostConnectionSettings.ActiveConnectionSettings ?? HostConnectionSettings.TestConnectionSettings;
            cServer.Text = lSettings.ServerName;
            cDataBase.Text = lSettings.DatabaseName;

            rgAuthentificationType.SelectedIndex = lSettings.UseWidowsAuth ? 0 : 1;

            if (!lSettings.UseWidowsAuth)
            {
                tUserName.Text = lSettings.UserName;
                tPassWord.Text = lSettings.Password;
            }
        }

        private void bYes_Click(object sender, EventArgs e)
        {
            ConnectionSettings lSettings = new ConnectionSettings();

            lSettings.ServerName = cServer.Text.Trim();
            lSettings.DatabaseName = cDataBase.Text.Trim();

            bool lUseWinAuth = rgAuthentificationType.SelectedIndex == 0;

            lSettings.UseWidowsAuth = lUseWinAuth;

            if (!lUseWinAuth)
            {
                lSettings.UserName = tUserName.Text.Trim();
                lSettings.Password = tPassWord.Text;
            }

            HostConnectionSettings.TestConnectionSettings = lSettings;

            if (HostConnectionSettings.TestConnection())
            {
                DialogResult = DialogResult.Yes;
                Close();
            }
            else
            {
                XtraMessageBox.Show(Resources.ErrorConnectionToServer, Resources.ErrorConnectionToServer, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
