﻿namespace ModularWinApp.Forms
{
    partial class DbConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcConnection = new DevExpress.XtraEditors.GroupControl();
            this.lPassWord = new DevExpress.XtraEditors.LabelControl();
            this.lUserName = new DevExpress.XtraEditors.LabelControl();
            this.tPassWord = new DevExpress.XtraEditors.TextEdit();
            this.tUserName = new DevExpress.XtraEditors.TextEdit();
            this.rgAuthentificationType = new DevExpress.XtraEditors.RadioGroup();
            this.lDataBase = new DevExpress.XtraEditors.LabelControl();
            this.lServer = new DevExpress.XtraEditors.LabelControl();
            this.cServer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cDataBase = new DevExpress.XtraEditors.ComboBoxEdit();
            this.bYes = new DevExpress.XtraEditors.SimpleButton();
            this.bNo = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcConnection)).BeginInit();
            this.gcConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tPassWord.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgAuthentificationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataBase.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcConnection
            // 
            this.gcConnection.Controls.Add(this.lPassWord);
            this.gcConnection.Controls.Add(this.lUserName);
            this.gcConnection.Controls.Add(this.tPassWord);
            this.gcConnection.Controls.Add(this.tUserName);
            this.gcConnection.Controls.Add(this.rgAuthentificationType);
            this.gcConnection.Location = new System.Drawing.Point(8, 42);
            this.gcConnection.Name = "gcConnection";
            this.gcConnection.Size = new System.Drawing.Size(317, 137);
            this.gcConnection.TabIndex = 1;
            this.gcConnection.Text = "Соединение:";
            // 
            // lPassWord
            // 
            this.lPassWord.Enabled = false;
            this.lPassWord.Location = new System.Drawing.Point(60, 107);
            this.lPassWord.Name = "lPassWord";
            this.lPassWord.Size = new System.Drawing.Size(41, 13);
            this.lPassWord.TabIndex = 4;
            this.lPassWord.Text = "Пароль:";
            // 
            // lUserName
            // 
            this.lUserName.Enabled = false;
            this.lUserName.Location = new System.Drawing.Point(25, 83);
            this.lUserName.Name = "lUserName";
            this.lUserName.Size = new System.Drawing.Size(76, 13);
            this.lUserName.TabIndex = 3;
            this.lUserName.Text = "Пользователь:";
            // 
            // tPassWord
            // 
            this.tPassWord.Enabled = false;
            this.tPassWord.Location = new System.Drawing.Point(107, 104);
            this.tPassWord.Name = "tPassWord";
            this.tPassWord.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.tPassWord.Properties.PasswordChar = '*';
            this.tPassWord.Size = new System.Drawing.Size(196, 20);
            this.tPassWord.TabIndex = 2;
            // 
            // tUserName
            // 
            this.tUserName.Enabled = false;
            this.tUserName.Location = new System.Drawing.Point(107, 80);
            this.tUserName.Name = "tUserName";
            this.tUserName.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.tUserName.Size = new System.Drawing.Size(196, 20);
            this.tUserName.TabIndex = 1;
            // 
            // rgAuthentificationType
            // 
            this.rgAuthentificationType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rgAuthentificationType.EditValue = 0;
            this.rgAuthentificationType.Location = new System.Drawing.Point(12, 22);
            this.rgAuthentificationType.Name = "rgAuthentificationType";
            this.rgAuthentificationType.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgAuthentificationType.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.rgAuthentificationType.Properties.Appearance.Options.UseBackColor = true;
            this.rgAuthentificationType.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rgAuthentificationType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Использовать Windows авторизацию"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Использовать SQL Server авторизацию")});
            this.rgAuthentificationType.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.rgAuthentificationType.Size = new System.Drawing.Size(303, 53);
            this.rgAuthentificationType.TabIndex = 0;
            this.rgAuthentificationType.SelectedIndexChanged += new System.EventHandler(this.rgAuthentificationType_SelectedIndexChanged);
            // 
            // lDataBase
            // 
            this.lDataBase.Location = new System.Drawing.Point(20, 197);
            this.lDataBase.Name = "lDataBase";
            this.lDataBase.Size = new System.Drawing.Size(69, 13);
            this.lDataBase.TabIndex = 1;
            this.lDataBase.Text = "База данных:";
            // 
            // lServer
            // 
            this.lServer.Location = new System.Drawing.Point(20, 15);
            this.lServer.Name = "lServer";
            this.lServer.Size = new System.Drawing.Size(63, 13);
            this.lServer.TabIndex = 2;
            this.lServer.Text = "SQL Сервер:";
            // 
            // cServer
            // 
            this.cServer.Location = new System.Drawing.Point(115, 12);
            this.cServer.Name = "cServer";
            this.cServer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cServer.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.cServer.Properties.Sorted = true;
            this.cServer.Size = new System.Drawing.Size(208, 20);
            this.cServer.TabIndex = 0;
            // 
            // cDataBase
            // 
            this.cDataBase.Location = new System.Drawing.Point(115, 194);
            this.cDataBase.Name = "cDataBase";
            this.cDataBase.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cDataBase.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.cDataBase.Size = new System.Drawing.Size(208, 20);
            this.cDataBase.TabIndex = 2;
            // 
            // bYes
            // 
            this.bYes.Location = new System.Drawing.Point(169, 230);
            this.bYes.Name = "bYes";
            this.bYes.Size = new System.Drawing.Size(75, 23);
            this.bYes.TabIndex = 5;
            this.bYes.Text = "Да";
            this.bYes.Click += new System.EventHandler(this.bYes_Click);
            // 
            // bNo
            // 
            this.bNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bNo.Location = new System.Drawing.Point(250, 230);
            this.bNo.Name = "bNo";
            this.bNo.Size = new System.Drawing.Size(75, 23);
            this.bNo.TabIndex = 6;
            this.bNo.Text = "Нет";
            // 
            // DbConfigForm
            // 
            this.AcceptButton = this.bYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bNo;
            this.ClientSize = new System.Drawing.Size(333, 260);
            this.Controls.Add(this.bNo);
            this.Controls.Add(this.bYes);
            this.Controls.Add(this.cDataBase);
            this.Controls.Add(this.cServer);
            this.Controls.Add(this.lServer);
            this.Controls.Add(this.lDataBase);
            this.Controls.Add(this.gcConnection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DbConfigForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки БД";
            this.Load += new System.EventHandler(this.DbConfigForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcConnection)).EndInit();
            this.gcConnection.ResumeLayout(false);
            this.gcConnection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tPassWord.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgAuthentificationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDataBase.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl gcConnection;
        private DevExpress.XtraEditors.LabelControl lPassWord;
        private DevExpress.XtraEditors.LabelControl lUserName;
        private DevExpress.XtraEditors.TextEdit tPassWord;
        private DevExpress.XtraEditors.TextEdit tUserName;
        private DevExpress.XtraEditors.RadioGroup rgAuthentificationType;
        private DevExpress.XtraEditors.LabelControl lDataBase;
        private DevExpress.XtraEditors.LabelControl lServer;
        private DevExpress.XtraEditors.ComboBoxEdit cServer;
        private DevExpress.XtraEditors.ComboBoxEdit cDataBase;
        private DevExpress.XtraEditors.SimpleButton bYes;
        private DevExpress.XtraEditors.SimpleButton bNo;
    }
}