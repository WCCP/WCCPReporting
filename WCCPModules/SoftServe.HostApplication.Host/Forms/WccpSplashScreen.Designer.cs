﻿namespace SoftServe.HostApplication.Host
{
    partial class WccpSplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lStatus = new DevExpress.XtraEditors.LabelControl();
            this.SuspendLayout();
            // 
            // lStatus
            // 
            this.lStatus.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lStatus.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lStatus.Location = new System.Drawing.Point(56, 112);
            this.lStatus.Name = "lStatus";
            this.lStatus.Size = new System.Drawing.Size(57, 16);
            this.lStatus.TabIndex = 7;
            this.lStatus.Text = "Starting...";
            // 
            // WccpSplashScreen
            // 
            this.AllowControlsInImageMode = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 282);
            this.Controls.Add(this.lStatus);
            this.Name = "WccpSplashScreen";
            this.ShowMode = DevExpress.XtraSplashScreen.ShowMode.Image;
            this.SplashImage = global::SoftServe.HostApplication.Host.Properties.Resources.Splash_Screen17;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lStatus;
    }
}
