﻿namespace ModularWinApp.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.gModules = new DevExpress.XtraGrid.GridControl();
            this.vModules = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rImageComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.colMolulName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServerVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gModules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vModules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rImageComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gModules
            // 
            this.gModules.Cursor = System.Windows.Forms.Cursors.Default;
            this.gModules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gModules.Location = new System.Drawing.Point(0, 0);
            this.gModules.MainView = this.vModules;
            this.gModules.Name = "gModules";
            this.gModules.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rImageComboBox});
            this.gModules.Size = new System.Drawing.Size(751, 338);
            this.gModules.TabIndex = 0;
            this.gModules.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.vModules});
            // 
            // vModules
            // 
            this.vModules.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStatus,
            this.colMolulName,
            this.colFileName,
            this.colClientVersion,
            this.colServerVersion});
            this.vModules.GridControl = this.gModules;
            this.vModules.Name = "vModules";
            this.vModules.OptionsBehavior.ReadOnly = true;
            this.vModules.OptionsView.ColumnAutoWidth = false;
            this.vModules.OptionsView.ShowAutoFilterRow = true;
            this.vModules.OptionsView.ShowGroupPanel = false;
            // 
            // colStatus
            // 
            this.colStatus.Caption = " ";
            this.colStatus.ColumnEdit = this.rImageComboBox;
            this.colStatus.FieldName = "NeedsUpdate";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 0;
            this.colStatus.Width = 40;
            // 
            // rImageComboBox
            // 
            this.rImageComboBox.AutoHeight = false;
            this.rImageComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rImageComboBox.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rImageComboBox.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Версии совпадают", 0, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Рабочая версия ниже", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Версия на сервере ниже", 1, 2)});
            this.rImageComboBox.Name = "rImageComboBox";
            this.rImageComboBox.SmallImages = this.imageCollection;
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "bad.png");
            this.imageCollection.Images.SetKeyName(1, "good.png");
            this.imageCollection.Images.SetKeyName(2, "warning.png");
            // 
            // colMolulName
            // 
            this.colMolulName.Caption = "Модуль";
            this.colMolulName.FieldName = "Name";
            this.colMolulName.Name = "colMolulName";
            this.colMolulName.OptionsColumn.ReadOnly = true;
            this.colMolulName.Visible = true;
            this.colMolulName.VisibleIndex = 1;
            this.colMolulName.Width = 210;
            // 
            // colFileName
            // 
            this.colFileName.Caption = "Файл";
            this.colFileName.FieldName = "ObjectLibrary";
            this.colFileName.Name = "colFileName";
            this.colFileName.OptionsColumn.ReadOnly = true;
            this.colFileName.Visible = true;
            this.colFileName.VisibleIndex = 2;
            this.colFileName.Width = 201;
            // 
            // colClientVersion
            // 
            this.colClientVersion.Caption = "Текущая Версия";
            this.colClientVersion.FieldName = "CurrVersion";
            this.colClientVersion.Name = "colClientVersion";
            this.colClientVersion.OptionsColumn.ReadOnly = true;
            this.colClientVersion.Visible = true;
            this.colClientVersion.VisibleIndex = 3;
            this.colClientVersion.Width = 125;
            // 
            // colServerVersion
            // 
            this.colServerVersion.Caption = "Версия на Сервере";
            this.colServerVersion.FieldName = "ServerVersion";
            this.colServerVersion.Name = "colServerVersion";
            this.colServerVersion.OptionsColumn.ReadOnly = true;
            this.colServerVersion.Visible = true;
            this.colServerVersion.VisibleIndex = 4;
            this.colServerVersion.Width = 125;
            // 
            // cOk
            // 
            this.cOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cOk.Location = new System.Drawing.Point(668, 347);
            this.cOk.Name = "cOk";
            this.cOk.Size = new System.Drawing.Size(75, 23);
            this.cOk.TabIndex = 2;
            this.cOk.Text = "OK";
            this.cOk.Click += new System.EventHandler(this.cOk_Click);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.cOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cOk;
            this.ClientSize = new System.Drawing.Size(751, 378);
            this.Controls.Add(this.cOk);
            this.Controls.Add(this.gModules);
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "SettingsForm";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 0, 40);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gModules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vModules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rImageComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gModules;
        private DevExpress.XtraGrid.Views.Grid.GridView vModules;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colMolulName;
        private DevExpress.XtraGrid.Columns.GridColumn colFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colServerVersion;
        private DevExpress.XtraEditors.SimpleButton cOk;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rImageComboBox;
        private DevExpress.Utils.ImageCollection imageCollection;
    }
}