﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace SoftServe.HostApplication.Host
{
    public partial class WccpSplashScreen : SplashScreen
    {
        public WccpSplashScreen()
        {
            InitializeComponent();
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);

            SplashScreenCommand command = (SplashScreenCommand)cmd;
            if (command == SplashScreenCommand.SetProgress)
            {
                lStatus.Text = ((string)arg).ToString();
            }
        }

        #endregion

        public enum SplashScreenCommand
        {
            SetProgress
        }
    }
}