﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SoftServe.HostApplication.Host.Models;

namespace ModularWinApp.Forms
{
    public partial class SettingsForm : XtraForm
    {
        private List<ModuleVersionInfo> _versionInfos = new List<ModuleVersionInfo>();

        public SettingsForm(List<ModuleVersionInfo> versionInfos)
        {
            InitializeComponent();

            _versionInfos = versionInfos;
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            gModules.DataSource = _versionInfos;
        }

        private void cOk_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
