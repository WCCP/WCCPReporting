﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraSplashScreen;
using Logica.Reports.Common;
using ModularWinApp.Core;
using ModularWinApp.Core.Interfaces;
using ModularWinApp.Forms;
using ModularWinApp.Models;
using SoftServe.HostApplication.Host;
using SoftServe.HostApplication.Host.Models;
using SoftServe.HostApplication.Host.Properties;
using SoftServe.HostApplication.Host.Utils;
using SoftServe.ModularWinApp.DataAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using SoftServe.Core.Common.Logging;

namespace ModularWinApp
{
    public partial class HostForm : RibbonForm
    {
        private int _tabCounter = 1;
        private List<ModuleObjectItem> _dbModulesInfo;
        private List<ModuleVersionInfo> _versionInfos;
        private bool _isFirstConnection = true;

        public static ReportParameter _rpHostForm = new ReportParameter()
        {
            Id = 0,
            LogId = Guid.Empty,
            Name = "WccpReporting"
        };

        private ModuleHandler _modHandler = new ModuleHandler();

        public HostForm()
        {
            Visible = false;
            UseAdvancedDisplayRectangle = true;
            InitializeComponent();
        }

        #region IHost

        public void CreateReportTab(string tabCaption, IStartupClass reportModule, string libraryName, Control control)
        {
            ReportTabForm f = new ReportTabForm();
            f.Module = reportModule;
            f.LibraryName = libraryName;
            control.Dock = DockStyle.Fill;
            f.Controls.Add(control);
            f.Text = tabCaption.ToUpper() + " " + _tabCounter;
            f.MdiParent = this;
            f.Activated += OnTabActivated;
            f.FormClosed += OnTabClosed;
            f.Show();

            _tabCounter++;
        }

        private void OnTabActivated(object sender, EventArgs eventArgs)
        {
            ReportTabForm lActiveTab = sender as ReportTabForm;
            if (lActiveTab == null)
                return;

            tMoluleName.Caption = lActiveTab.LibraryName;
            tModuleVersion.Caption = lActiveTab.Module.GetVersion();
            bVersionImage.EditValue = _versionInfos.First(m => m.ObjectLibrary == lActiveTab.LibraryName).NeedsUpdate;

            tModuleVersion.Visibility = tMoluleName.Visibility = bVersionImage.Visibility = BarItemVisibility.Always;
        }

        #endregion

        #region Host Form Events

        private void frmHost_Load(object sender, EventArgs e)
        {
            SuspendLayout();
            WindowState = FormWindowState.Maximized;
            ResumeLayout();

            string lWarning = string.Empty;

            // Open a Splash Screen
            SplashScreenManager.ShowForm(this, typeof(WccpSplashScreen), true, true, false);

            SplashScreenManager.Default.SendCommand(WccpSplashScreen.SplashScreenCommand.SetProgress, "Подключение к удаленному серверу...");
            Thread.Sleep(100);

            if (!HostConnectionSettings.TestConnection())
            {
                SplashScreenManager.CloseForm(false);

                ErrorManager.ShowErrorBox(Resources.ErrorConnectionToServer + Environment.NewLine + Environment.NewLine + Resources.CheckConnection);
                SetStatusBarItemsVisibility(false);

                DbConfigForm form = new DbConfigForm();

                if (form.ShowDialog(this) == DialogResult.Yes)
                {
                    lWarning = HandleConnectionChange();
                }
            }
            else
            {
                WccpLogger.WccpOpentLog(_rpHostForm);
                
                SplashScreenManager.Default.SendCommand(WccpSplashScreen.SplashScreenCommand.SetProgress, "Обновление модулей...");
                Thread.Sleep(100);

                SetStatusBarItemsVisibility(true);
                lWarning = HandleConnectionChange();
            }

            if (SplashScreenManager.Default != null)
            {
                SplashScreenManager.Default.SendCommand(WccpSplashScreen.SplashScreenCommand.SetProgress, "Загрузка модулей...");
                Thread.Sleep(100);
            }

            _modHandler.InitializeModules();

            SplashScreenManager.CloseForm(false);

            if (lWarning != string.Empty)
            {
                ErrorManager.ShowWarningBox(lWarning);
                ShowSettingsForm();
            }
        }

        private void frmHost_Shown(object sender, EventArgs e)
        {
            Opacity = 1;
            Visible = true;
        }

        private void frmHost_FormClosing(object sender, FormClosingEventArgs e)
        {
            WccpLogger.WccpExitLog(_rpHostForm);
            Exit();
        }

        private void Exit()
        {
            //if (Program._modHandler != null)
            //{
            //    //call the ModuleHandler Dispose method
            //    Program._modHandler.Dispose();
            //    Program._modHandler = null;
            //}

            if (_modHandler != null)
            {
                //call the ModuleHandler Dispose method
                _modHandler.Dispose();
                _modHandler = null;
            }

            Application.Exit();
        }

        #endregion

        #region Menu Events

        private void barDbConfig_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DbConfigForm form = new DbConfigForm();

            if (form.ShowDialog(this) == DialogResult.Yes)
            {
                string lWarning = HandleConnectionChange();

                if (lWarning != string.Empty)
                {
                    ErrorManager.ShowWarningBox(lWarning);
                    ShowSettingsForm();
                }
            }
        }

        private void SetStatusBarItemsVisibility(bool isConnected)
        {
            tServer.Visibility =
                        tServerValue.Visibility =
                            tDataBase.Visibility =
                                tDataBaseValue.Visibility =
                                tUserName.Visibility = tUserNameValue.Visibility
                                = bVersionImage.Visibility = isConnected ? BarItemVisibility.Always : BarItemVisibility.Never;

            tDisconnected.Visibility = isConnected ? BarItemVisibility.Never : BarItemVisibility.Always;
        }

        private void barSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ShowSettingsForm();
        }

        private void ShowSettingsForm()
        {
            SettingsForm form = new SettingsForm(_versionInfos);
            form.ShowDialog(this);
        }

        private void barAbout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AboutForm form = new AboutForm();
            form.ShowDialog(this);
        }

        private string HandleConnectionChange()
        {
            string lWarning = string.Empty;

            SetStatusBarItemsVisibility(true);

            tServerValue.Caption = HostConnectionSettings.ActiveConnectionSettings.ServerName;
            tDataBaseValue.Caption = HostConnectionSettings.ActiveConnectionSettings.DatabaseName;
            tUserNameValue.Caption = HostConnectionSettings.ActiveConnectionSettings.UserName;

            barModuleTree.LoadedModuleMark = imageCollection.Images[1];
            barModuleTree.AssignDataSource(DataProvider.GetModuleTreeList(HostConnectionSettings.ActiveConnectionSettings.IsLDB));

            _dbModulesInfo = DataProvider.GetModuleList();
            GetModulesVersionInfo();

            if (_isFirstConnection)
            {
                _isFirstConnection = false;

                if (!UpdateModuleVersions())
                {
                    ErrorManager.ShowWarningBox("Не удалось обновить версии модулей");
                    ShowSettingsForm();
                }
                else
                {
                    lWarning = AreModuleVersionsEqual();
                }
            }
            else//not _isFirstConnection
            {
                lWarning = AreModuleVersionsEqual();
            }

            return lWarning;
        }

        private string AreModuleVersionsEqual()
        {
            string lWarning = string.Empty;
            int lDifferentVersionCount = _versionInfos.Count(m => m.NeedsUpdate != (int)VersionStatus.VersionsAreEqual);

            if (lDifferentVersionCount > 0)
            {
                lWarning = "Не все версии модулей совпадают с серверными";
            }

            return lWarning;
        }

        private void bVersionImage_EditValueChanged(object sender, EventArgs e)
        {
            int lValue = Convert.ToInt32(bVersionImage.EditValue);

            switch ((VersionStatus)lValue)
            {
                case VersionStatus.VersionsAreEqual:
                    bVersionImage.Hint = "Версии совпадают";
                    return;
                case VersionStatus.ServerVersionIsLess:
                    bVersionImage.Hint = "Серверная версия ниже";
                    return;
                case VersionStatus.CurrentVersionIsLess:
                    bVersionImage.Hint = "Рабочая версия ниже";
                    return;
            }
        }

        //displays tooltip only for Status when it's visible
        private void toolTipController_BeforeShow(object sender, DevExpress.Utils.ToolTipControllerShowEventArgs e)
        {
            if (bVersionImage.Visibility == BarItemVisibility.Always && bVersionImage.EditValue != null && (e.SelectedObject is BarItemLink) && (e.SelectedObject as BarItemLink).ItemId == bVersionImage.Id)
                return;

            e.Show = false;
        }

        #endregion

        #region Document Manager Events

        private void tabbedView_CustomHeaderButtonClick(object sender, DevExpress.XtraBars.Docking2010.Views.CustomHeaderButtonEventArgs e)
        {
            tabbedView.Controller.CloseAllButThis(e.Document);
            tabbedView.Controller.Close(e.Document);
        }

        #endregion

        #region Modules Loading

        private void barModuleTree_DoubleClick(object sender, EventArgs e)
        {
            if (barModuleTree.ActiveLink != null)
            {
                HandleLinkClick(barModuleTree.ActiveLink.Caption);
            }
        }

        private void barModuleTree_EnterClick(object sender, EventArgs e)
        {
            if (barModuleTree.ActiveLink != null)
            {
                HandleLinkClick(barModuleTree.ActiveLink.Caption);
            }
        }

        private void HandleLinkClick(string caption)
        {
            string libraryName = barModuleTree.ModuleTree.GetLibraryNameByTreeItemCaption(caption);

            try
            {
                LoadLibrary(libraryName, caption);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowExtendedMessage(ex, string.Format(Resources.ErrorCantCreateReport, caption));
            }
        }

        private void LoadLibrary(string libraryName, string caption)
        {
            IStartupClass reportModule = GetLoadingModule(libraryName);

            if (reportModule == null)
                return;

            PerformTabCreation(caption, libraryName, reportModule);
        }

        private IStartupClass GetLoadingModule(string libraryName)
        {
            IStartupClass reportModule;

            try
            {
                reportModule = _modHandler.GetWccpModuleInstance(libraryName);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowExtendedMessage(ex, string.Format(Resources.ErrorOnModuleLoad, libraryName));

                return null;
            }

            if (reportModule == null)
            {
                ErrorManager.ShowErrorBox(string.Format(Resources.ErrorCantFindModule, libraryName));
            }

            return reportModule;
        }

        private void PerformTabCreation(string caption, string libraryName, IStartupClass reportModule)
        {
            if (barModuleTree.Cursor == Cursors.WaitCursor)
            {
                return;
            }
            barModuleTree.Cursor = Cursors.WaitCursor;
            int tempTabCounter = _tabCounter;
            int reportId = barModuleTree.ModuleTree.GetLibraryIdByTreeItemCaption(libraryName);

            try
            {
                TimeParameter tp = WccpLogger.InitTimeParameter();

                int result = reportModule.ShowUI(reportId, caption);

                WccpLogger.DoReportTraceLog(_rpHostForm, "ShowUI()", "Host Form", "UI", tp);

                if (result == 0)
                {
                    CreateReportTab(reportModule.ReportCaption, reportModule, libraryName, reportModule.ReportControl);
                }

                if (tempTabCounter != _tabCounter)
                {
                    barModuleTree.ActiveLink = null;
                    barModuleTree.CheckModule(libraryName);
                    barModuleTree.Refresh();
                }

                barModuleTree.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                barModuleTree.Cursor = Cursors.Default;

                ErrorManager.ShowExtendedMessage(ex, string.Format(Resources.ErrorOnReportLoad, caption));
            }
        }

        private void OnTabClosed(object sender, FormClosedEventArgs formClosedEventArgs)
        {
            ReportTabForm f = sender as ReportTabForm;
            barModuleTree.UncheckModule(f.LibraryName);
            barModuleTree.Refresh();

            if (MdiChildren.Count() == 1)
            {
                tModuleVersion.Visibility = tMoluleName.Visibility = bVersionImage.Visibility = BarItemVisibility.Never;
            }
        }

        #endregion

        #region Module Update

        private void GetModulesVersionInfo()
        {
            _versionInfos = new List<ModuleVersionInfo>();
            List<ModuleTreeItem> lReportList = barModuleTree.ModuleTree.GetReportModules();

            foreach (ModuleTreeItem accessibleModule in lReportList)
            {
                ModuleObjectItem moduleFromSer = _dbModulesInfo.FirstOrDefault(m => m.ReportId == accessibleModule.ReportId);
                string moduleFromDirVer = GetCurrentModuleVersion(accessibleModule.ObjectLibrary);

                ModuleVersionInfo verInfo = new ModuleVersionInfo
                    {
                        ReportId = accessibleModule.ReportId,
                        Name = accessibleModule.Name,
                        ObjectLibrary = accessibleModule.ObjectLibrary,
                        CurrVersion = new VersionInfo(moduleFromDirVer),
                        ServerVersion = new VersionInfo(moduleFromSer != null ? moduleFromSer.ObjectVersion : VersionInfo.DefaultVersion)
                    };

                _versionInfos.Add(verInfo);
            }
        }

        private bool UpdateModuleVersions()
        {
            int lFailedCount = 0;

            try
            {
                foreach (ModuleVersionInfo module in _versionInfos)
                {
                    if (module.CurrVersion < module.ServerVersion)
                    {
                        if (UpdateModule(module.ReportId, module.ObjectLibrary))
                        {
                            module.CurrVersion = new VersionInfo(module.ServerVersion.ToString());
                        }
                        else
                        {
                            lFailedCount++;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return lFailedCount == 0;
        }

        private bool UpdateModule(int reportId, string objectLibrary)
        {
            MemoryStream lModuleContent = null;
            FileStream lDestinationFile = null;

            bool doesSucceed = true;

            try
            {
                lModuleContent = new MemoryStream(DataProvider.GetModuleDll(reportId));
                lDestinationFile = new FileStream(objectLibrary, FileMode.Create, FileAccess.Write);

                lModuleContent.WriteTo(lDestinationFile);
            }
            catch (Exception)
            {
                doesSucceed = false;
            }
            finally
            {
                if (lModuleContent != null)
                    lModuleContent.Close();

                if (lDestinationFile != null)
                    lDestinationFile.Close();
            }

            return doesSucceed;
        }

        private string GetCurrentModuleVersion(string libraryName)
        {
            string lFileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), libraryName);

            if (!File.Exists(lFileName))
                return VersionInfo.DefaultVersion;

            FileVersionInfo lDirectoryFileVersion = FileVersionInfo.GetVersionInfo(lFileName);

            return lDirectoryFileVersion.FileVersion;
        }

        #endregion

        private void searchControlModules_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 40 && !e.Control)
            {
                barModuleTree.Focus();
                barModuleTree.Activate();
            }
        }

        private void searchControlModules_TextChanged(object sender, EventArgs e)
        {
            barModuleTree.Deactivate();
        }
    }
}
