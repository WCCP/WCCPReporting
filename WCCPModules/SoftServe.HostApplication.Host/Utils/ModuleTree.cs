﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ModularWinApp.Models;
using SoftServe.HostApplication.Host.Models;

namespace ModularWinApp.Utils
{
    /// <summary>
    /// Incapsulates module tree mapping functionality
    /// </summary>
    public class ModuleTree : List<ModuleTreeItem>
    {
        public string GetLibraryNameByTreeItemCaption(string caption)
        {
            return this.First(m => string.Equals(m.Name, caption, StringComparison.InvariantCultureIgnoreCase) && m.ParentId != 0).ObjectLibrary;
        }

        public int GetLibraryIdByTreeItemCaption(string caption)
        {
            return this.First(m => string.Equals(m.ObjectLibrary, caption, StringComparison.InvariantCultureIgnoreCase) && m.ParentId != 0).ReportId;
        }

        public List<ModuleTreeItem> GetReportModules()
        {
            return this.Where(m => m.ParentId != 0).ToList();
        }
    }
}
