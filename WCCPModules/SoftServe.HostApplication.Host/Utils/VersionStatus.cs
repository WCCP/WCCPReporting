﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.HostApplication.Host.Utils
{
    public enum VersionStatus: int
    {
        VersionsAreEqual = 0,
        ServerVersionIsLess = 1,
        CurrentVersionIsLess = 2
    }
}
