﻿using System.Collections.Generic;

namespace ModularWinApp.Utils
{
    /// <summary>
    /// Describes loaded modules along with loaded instances count
    /// </summary>
    class LoadedModules
    {
        private Dictionary<string, int> _loadedModules;

        public LoadedModules()
        {
            _loadedModules = new Dictionary<string, int>();
        }

        public void AddModule(string libraryName)
        {
            if (_loadedModules.ContainsKey(libraryName))
            {
                _loadedModules[libraryName]++;
            }
            else
            {
                _loadedModules.Add(libraryName, 1);
            }
        }

        public void DeleteModule(string libraryName)
        {
            if (ContainsModule(libraryName))
            {
                _loadedModules[libraryName]--;
            }
        }

        public bool ContainsModule(string libraryName)
        {
            return _loadedModules.ContainsKey(libraryName) && _loadedModules[libraryName] > 0;
        }

        public int Count(string libraryName)
        {
            return ContainsModule(libraryName) ? _loadedModules[libraryName] : -1;
        }
    }
}
