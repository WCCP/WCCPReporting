﻿using System;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using SoftServe.HostApplication.Host.UpdateService;
using SoftServe.HostApplication.Host.Models;
using System.Configuration;
using Logica.Reports.Common;

namespace SoftServe.HostApplication.Host.Utils
{
    class HostUpdateLauncher
    {
        private string _tempDirPath;
        private string _currentDirPath;
        private UpdateServiceSoapClient _updateService;

        private List<string> _modulesToUpdate;

        private List<UpdateFileInfo> _baseModules;

        public HostUpdateLauncher()
        {
            _currentDirPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _tempDirPath = Path.Combine(_currentDirPath, "temp");

            _modulesToUpdate = new List<string>();
            _baseModules = new List<UpdateFileInfo>();
        }

        public bool ExecuteLauncher()
        {
            try
            {
                _updateService = new UpdateServiceSoapClient();
            }
            catch (Exception e)
            {
                ErrorManager.ShowExtendedMessage(e, "Ошибка при подключении к сервису обновлений");
                return false;
            }

            ClearTempFiles();

            if (CheckForNewUpdates())
            {
                if (XtraMessageBox.Show("Обнаружена новая версия WCCP reporting. Установить?", "WCCP reporting update", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        DownloadUpdate();

                        if (ReplaceRunningApplication())
                        {
                            Application.Restart();
                            Environment.Exit(0);
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorManager.ShowExtendedMessage(e, "Ошибка при обновлении WCCP reporting");
                    }
                }
            }

            return true;
        }

        public bool CheckForNewUpdates()
        {
            try
            {
                _baseModules.AddRange(_updateService.GetHostBaseFileList().FileList);

                foreach (UpdateFileInfo module in _baseModules)
                {
                    var updateVersion = new VersionInfo(module.FileVersion ?? VersionInfo.DefaultVersion);
                    VersionInfo curFileVersion;
                    if (module.FileName.EndsWith("exe.config"))
                    {
                        curFileVersion = new VersionInfo(GetAppConfigVersion(module.FileName));
                    }
                    else
                    {
                        curFileVersion = new VersionInfo(GetCurrentModuleVersion(module.FileName));
                    }

                    if (updateVersion > curFileVersion)
                    {
                        _modulesToUpdate.Add(module.FileName);
                    }
                }

                return _modulesToUpdate.Count > 0;
            }
            catch (Exception e)
            {
                //XtraMessageBox.Show("Проблема в работе сервера обновлений", "WCCP reporting update", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public void DownloadUpdate()
        {
            if (!Directory.Exists(_tempDirPath))
            {
                Directory.CreateDirectory(_tempDirPath);
            }

            foreach (string module in _modulesToUpdate)
            {
                byte[] lByteModuleContent = _updateService.GetModuleWithPath(new GetModuleRequest { ModuleName = module }, ConfigurationManager.AppSettings["UpdatePath"]).ModuleContent;

                MemoryStream lModuleContent = new MemoryStream(lByteModuleContent);
                FileStream lDestinationFile = new FileStream(Path.Combine(_tempDirPath, module), FileMode.Create, FileAccess.Write);

                lModuleContent.WriteTo(lDestinationFile);
            }
        }

        private void ClearTempFiles()
        {
            if (Directory.Exists(_tempDirPath))
            {
                foreach (string file in Directory.GetFiles(_tempDirPath))
                {
                    DeleteTempFile(file);
                }
            }

            foreach (string file in Directory.GetFiles(_currentDirPath))
            {
                if (file.Contains(".bak"))
                {
                    DeleteTempFile(file);
                }
            }
        }

        private void DeleteTempFile(string filePath)
        {
            try
            {
                File.Delete(filePath);
            }
            catch (Exception e) { }
        }

        private bool ReplaceRunningApplication()
        {
            try
            {
                foreach (string fileName in _modulesToUpdate)
                {
                    UpdateFile(fileName);
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show("Не удалось установить обновление", "WCCP reporting update", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void UpdateFile(string fileName)
        {
            string lFileToBeReplaced = Path.Combine(_currentDirPath, fileName);
            string lFileTemp = GenerateTempFileName(fileName);
            string lFileSource = Path.Combine(_tempDirPath, fileName);

            if (File.Exists(lFileToBeReplaced))
            {
                File.Move(lFileToBeReplaced, lFileTemp);
            }

            File.Copy(lFileSource, lFileToBeReplaced);
        }

        private string GetCurrentModuleVersion(string libraryName)
        {
            string lFileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), libraryName);

            if (!File.Exists(lFileName))
                return VersionInfo.DefaultVersion;

            FileVersionInfo lDirectoryFileVersion = FileVersionInfo.GetVersionInfo(lFileName);

            return lDirectoryFileVersion.FileVersion;
        }

        private string GetAppConfigVersion(string fileName)
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);

            string res = VersionInfo.DefaultVersion;
            string value = ConfigurationManager.AppSettings["ConfigVersion"];
            if (File.Exists(path) && !string.IsNullOrEmpty(value))
            {
                res = value;
            }
            return res;
        }

        /// <summary>
        /// Generates temp file name in format FileName.bak.dll or FileName.bak[copy number].dll (in case FileName.bak.dll already exists)
        /// </summary>
        /// <param name="lFileTemp">first temp file name approximation</param>
        /// <returns>temp file name</returns>
        private string GenerateTempFileName(string fileName)
        {
            string lFileTemp = Path.Combine(_currentDirPath, fileName.Replace(".dll", ".bak.dll").Replace(".exe", ".bak.exe"));

            int i = 1;

            while (File.Exists(lFileTemp))
            {
                lFileTemp = lFileTemp.Replace(".bak.", ".bak" + i + ".");
                lFileTemp = lFileTemp.Replace(".bak" + i + ".", ".bak" + (i + 1) + ".");
                i++;
            }

            return lFileTemp;
        }
    }
}