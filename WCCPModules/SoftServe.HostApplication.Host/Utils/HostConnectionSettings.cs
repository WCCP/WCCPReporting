﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Internal;
using System.Data.SqlClient;
using System.Text;
using SoftServe.HostApplication.Host.Models;
using Logica.Reports.DataAccess;
using System.Xml.Serialization;
using System.IO;
using SoftServe.Core.Common.Logging;


namespace ModularWinApp
{
    public static class HostConnectionSettings
    {
        public static List<string> Servers = new List<string>();
        public static List<string> DataBases = new List<string>();

        private const string CompanyDirectory = "SoftServe";
        private const string ProgramDirectory = "WCCP Reporting";
        private const string FileName = "ConnectionSettings.xml";
        private static string _logUserNameSW = string.Empty;
        private static string _logUserNameLDB = string.Empty;

        public static ConnectionSettings ActiveConnectionSettings { get; set; }

        public static ConnectionSettings TestConnectionSettings { get; set; }

        private static string PathToSettingsFile
        {
            get
            {
                string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);
                string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);
                string lFilePath = Path.Combine(lProgramDirPath, FileName);

                return lFilePath;
            }
        }

        static HostConnectionSettings()
        {
            try//read servers from config file
            {
                Servers.AddRange(ConfigurationManager.AppSettings["Servers"].Split(';'));
            }
            catch (Exception)
            {
                Servers.Add("Cherry");
                Servers.Add("AbiAlpha");
                Servers.Add("eegdadb18.ee.interbrew.net");
                Servers.Add("eegdadb8.ee.interbrew.net");
            }

            DataBases.Add("SalesWorks");
            DataBases.Add("LDB");
        }

        public static bool TestConnection()
        {
            if (TestConnectionSettings == null || TestConnectionSettings.IsEmpty)
            {
                try
                {
                    SetConnectionStringToDefault();
                }
                catch (Exception)
                {
                    return false;
                }
            }

            SqlConnection connection = new SqlConnection(TestConnectionSettings.ConnectionString);
            bool lIsSuccessful = true;

            try
            {
                connection.Open();

                //update active string
                ActiveConnectionSettings = TestConnectionSettings.CreateCopy();
                HostConfiguration.DBSqlConnection = ActiveConnectionSettings.ConnectionString;
                DataAccessLayer.SetConnectionStringToDefault();

                //update string in user profile
                PrepareWriting();
                SaveDataToProfile();

                if (!Servers.Contains(ActiveConnectionSettings.ServerName))
                {
                    UpdateServerList();
                }

                WccpLogger.SetConnectionConfig(HostConfiguration.DBSqlConnection, _logUserNameSW, _logUserNameLDB);

            }
            catch (Exception ex)
            {
                lIsSuccessful = false;
            }
            finally
            {
                connection.Close();
            }

            return lIsSuccessful;
        }

        private static void SetConnectionStringToDefault()
        {
            try
            {
                ReadDataFromProfile();
            }
            catch (Exception)
            {
                // try to read from AppSettings
                ActiveConnectionSettings = new ConnectionSettings();
                TestConnectionSettings = new ConnectionSettings();

                ActiveConnectionSettings.ServerName = ConfigurationManager.AppSettings["DataSource"];
                ActiveConnectionSettings.DatabaseName = ConfigurationManager.AppSettings["InitialCatalog"];
                ActiveConnectionSettings.UseWidowsAuth = Boolean.Parse(ConfigurationManager.AppSettings["IntegratedSecurity"]);

                if (!ActiveConnectionSettings.UseWidowsAuth)
                {
                    ActiveConnectionSettings.UserName = ConfigurationManager.AppSettings["UserId"];
                    ActiveConnectionSettings.Password = ConfigurationManager.AppSettings["Password"];
                }

                TestConnectionSettings = ActiveConnectionSettings.CreateCopy();
            }
            try
            {
                _logUserNameSW = ConfigurationManager.AppSettings["LogUserNameSW"];
                _logUserNameLDB = ConfigurationManager.AppSettings["LogUserPWDSW"];
            }
            catch
            {
            }
        }

        public static string ConnectionString
        {
            get
            {
                return ActiveConnectionSettings == null ? null : ActiveConnectionSettings.ConnectionString;
            }
        }

        private static void ReadDataFromProfile()
        {
            XmlSerializer lSerializer = new XmlSerializer(typeof(ConnectionSettings));

            FileStream lStream = new FileStream(PathToSettingsFile, FileMode.Open, FileAccess.Read);

            try
            {
                ConnectionSettings lTemp = (ConnectionSettings)lSerializer.Deserialize(lStream);
                TestConnectionSettings = lTemp;
            }
            finally
            {
                lStream.Close();
            }
        }

        private static void SaveDataToProfile()
        {
            XmlSerializer lSerializer = new XmlSerializer(typeof(ConnectionSettings));

            FileStream lStream = new FileStream(PathToSettingsFile, FileMode.Create, FileAccess.Write);

            try
            {
                ConnectionSettings lTemp = ActiveConnectionSettings.CreateCopy();
                lSerializer.Serialize(lStream, lTemp);
            }
            finally
            {
                lStream.Close();
            }
        }

        private static void PrepareWriting()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);

            if (!Directory.Exists(lCompanyDirPath))
            {
                Directory.CreateDirectory(lCompanyDirPath);
            }

            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);

            if (!Directory.Exists(lProgramDirPath))
            {
                Directory.CreateDirectory(lProgramDirPath);
            }
        }

        private static void UpdateServerList()
        {
            Servers.Add(ActiveConnectionSettings.ServerName);

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["Servers"].Value = string.Join(";", Servers.ToArray());
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
