﻿using DevExpress.Utils.Drawing;
using DevExpress.XtraNavBar;
using DevExpress.XtraNavBar.ViewInfo;
using ModularWinApp.Models;
using ModularWinApp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ModularWinApp.VisualComponents
{
    /// <summary>
    /// Describes module tree behaviour: init/display and drawing: emulates hover/select/double click funtionality - handles links as buttons 
    /// </summary>
    [ToolboxItem(true)]
    public partial class WccpModuleTree : NavBarControl
    {
        private ModuleTree _moduleTree = new ModuleTree();
        private LoadedModules _loadedModules = new LoadedModules();

        public NavBarItemLink ActiveLink {get; set;}
        
        public event EventHandler EnterClick;

        /// <summary>
        /// Incapsulates module tree items
        /// </summary>
        public ModuleTree ModuleTree
        {
            get { return _moduleTree; }
        }

        /// <summary>
        /// An image (check) to be displayed in the link description of the loaded module 
        /// </summary>
        public Image LoadedModuleMark { get; set; }

        /// <summary>
        /// Returns the link which is currently hovered with cursor - handles links as buttons 
        /// </summary>
        public NavBarItemLink HoveredLink
        {
            get
            {
                Point pt = PointToClient(MousePosition);

                if (pt.X > Right || pt.X < Left)// cursor is not over nav control
                    return null;

                pt.X = 10;//align current hit point to the left of the nav control - to take into consideration space around the right edge of the nav control
                NavBarHitInfo hi = CalcHitInfo(pt);

                return hi.Link;
            }
        }

        public WccpModuleTree()
        {
            InitializeComponent();
            CustomDrawLink += barModuleTree_CustomDrawLink;
            Click += barModuleTree_Click;
            MouseMove += barModuleTree_MouseMove;
        }

        public WccpModuleTree(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            CustomDrawLink += barModuleTree_CustomDrawLink;
            Click += barModuleTree_Click;
            MouseMove += barModuleTree_MouseMove;
        }

        /// <summary>
        /// Inits tree with new list of modules
        /// </summary>
        public void AssignDataSource(List<ModuleTreeItem> moduleTree)
        {
            _moduleTree = new ModuleTree();
            _moduleTree.AddRange(moduleTree);
            InitModuleTreeStructure();

            _loadedModules = new LoadedModules();
        }

        /// <summary>
        /// Marks module as loaded
        /// </summary>
        /// <param name="libraryName">dll name</param>
        public void CheckModule(string libraryName)
        {
            _loadedModules.AddModule(libraryName);
        }

        /// <summary>
        /// Unchecks molule
        /// </summary>
        /// <param name="libraryName">dll n</param>
        public void UncheckModule(string libraryName)
        {
            _loadedModules.DeleteModule(libraryName);
        }

        public void Activate()
        {
            if (ActiveLink == null)
            {
                SelectLink(FindFirstItemLink());
            }
            else
            {
                SelectLink(ActiveLink);
            }
        }

        public void Deactivate()
        {
            if (ActiveLink == null) return;

            if (ActiveLink.Group.VisibleItemLinks.Count == 0 || ActiveLink.Group.VisibleItemLinks.IndexOf(ActiveLink) < 0)
                ActiveLink = null;
        }

        #region Link Selection

        private void InitModuleTreeStructure()
        {
            NavBarGroup group = new NavBarGroup("Default");

            BeginUpdate();

            Items.Clear();
            Groups.Clear();

            foreach (ModuleTreeItem module in _moduleTree)
            {
                if (module.ParentId == 0 && module.ReportId != 0)// add group
                {
                    group = new NavBarGroup(module.Name.ToUpper());
                    Groups.Add(group);
                }
                else//add link
                {
                    NavBarItem item = new NavBarItem(module.Name);
                    item.SmallImage = new Bitmap(20, 20);
                    group.ItemLinks.Add(item);
                }
            }

            EndUpdate();
        }

        private bool IsMouseOverLink(string caption)
        {
            NavBarItemLink lCurLink = HoveredLink;

            return lCurLink != null && lCurLink.Caption == caption;
        }

        /// <summary>
        /// Changes hovered link background on mouse move
        /// </summary>
        private void barModuleTree_MouseMove(object sender, MouseEventArgs e)
        {
            if (HoveredLink != null)
            {
                Refresh();
            }
        }

        /// <summary>
        /// Changes selected link background
        /// </summary>
        private void barModuleTree_Click(object sender, EventArgs e)
        {
            if (HoveredLink != null)
            {
                ActiveLink = HoveredLink;
            }
        }

        /// <summary>
        /// Handles links background changes: emulates hover/select/double click funtionality - handles links as buttons 
        /// </summary>
        private void barModuleTree_CustomDrawLink(object sender, CustomDrawNavBarElementEventArgs e)
        {
            NavBarItemLink lLink = ((NavLinkInfoArgs)e.ObjectInfo).Link;
            NavLinkInfoArgs lLinkInfo = e.ObjectInfo as NavLinkInfoArgs;

            if (lLink == null || lLinkInfo == null)
                return;

            if (_loadedModules.ContainsModule(_moduleTree.GetLibraryNameByTreeItemCaption(lLink.Caption)))
            {
                e.Appearance.Font = new Font(e.Appearance.Font.FontFamily, e.Appearance.Font.Size, FontStyle.Regular);
                e.Graphics.FillRectangle(Brushes.DarkOrange, e.RealBounds);

                if (e.Image != null)
                {
                    Rectangle lImageRect = lLinkInfo.ImageRectangle;
                    lImageRect.X = e.RealBounds.Right - lImageRect.Width + 5;
                    lImageRect.Y += (lImageRect.Height - LoadedModuleMark.Height) / 2;
                    e.Graphics.DrawImageUnscaled(LoadedModuleMark, lImageRect);
                }

                return;
            }

            if (e.ObjectInfo.State == ObjectState.Hot || IsMouseOverLink(lLink.Caption))
            {
                e.Appearance.Font = new Font(e.Appearance.Font.FontFamily, e.Appearance.Font.Size, FontStyle.Regular);
                e.Graphics.FillRectangle(Brushes.DarkGray, e.RealBounds);
            }

            if (ActiveLink != null && lLink.Caption == ActiveLink.Caption)
            {
                e.Appearance.Font = new Font(e.Appearance.Font.FontFamily, e.Appearance.Font.Size, FontStyle.Regular);
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(127, 127, 127)), e.RealBounds);
            }
        }

        #endregion

        #region Button Navigation

        private NavBarGroup NextGroup(NavBarGroup group, bool forward)
        {
            if (Groups.GetVisibleGroupCount() == 0)
                return null;

            if (group == null)
                return (forward) ? FirstVisibleGroup(null) : LastVisibleGroup(null);

            if (forward)
            {
                NavBarGroup lNextGroup = FirstVisibleGroup(group);

                if (lNextGroup != null)
                    return lNextGroup;
                else
                    return FirstVisibleGroup(null);
            }
            else
            {
                NavBarGroup lPreviousGroup = LastVisibleGroup(group);

                if (lPreviousGroup != null)
                    return lPreviousGroup;
                else
                    return LastVisibleGroup(null);
            }
        }

        private NavBarGroup FirstVisibleGroup(NavBarGroup group)
        {
            if (group == null)
            {
                return Groups.FirstOrDefault(i => i.VisibleItemLinks.Count > 0);
            }

            int curIndex = Groups.IndexOf(group);
            return Groups.FirstOrDefault(i => i.VisibleItemLinks.Count > 0 && curIndex < Groups.IndexOf(i));
        }

        private NavBarGroup LastVisibleGroup(NavBarGroup group)
        {
            if (group == null)
            {
                return Groups.LastOrDefault(i => i.VisibleItemLinks.Count > 0);
            }

            int curIndex = Groups.IndexOf(group);
            return Groups.LastOrDefault(i => i.VisibleItemLinks.Count > 0 && curIndex > Groups.IndexOf(i));
        }

        private NavBarItemLink FindNearItemLink(bool forward)
        {
            NavBarItemLink link = ActiveLink;

            NavBarGroup group;

            if (link == null)
                group = NextGroup(null, forward);
            else
                group = link.Group;

            if (group == null)
                return null;

            NavBarGroup startGroup = group;

            int i;
            if (link != null)
                i = group.VisibleItemLinks.IndexOf(link);
            else
                i = (forward) ? -1 : group.VisibleItemLinks.Count;

            bool repeat = false;

            do
            {
                if (forward)
                {
                    if (i < group.VisibleItemLinks.Count - 1)
                        return group.VisibleItemLinks[i + 1];
                    else
                    {
                        group = NextGroup(group, forward);
                        repeat = i > 0;
                        i = -1;
                    }
                }
                else
                {
                    if (i > 0 && group.VisibleItemLinks.Count > 0)
                        return group.VisibleItemLinks[i - 1];
                    else
                    {
                        group = NextGroup(group, forward);
                        repeat = i < group.VisibleItemLinks.Count - 1;
                        i = group.VisibleItemLinks.Count;
                    }
                }
            }
            while (group != startGroup || repeat);

            return null;
        }

        private NavBarItemLink FindFirstItemLink()
        {
            NavBarGroup lFirstGroup = FirstVisibleGroup(null);

            if (lFirstGroup == null)
                return null;

            return lFirstGroup.VisibleItemLinks[0];
        }

        private NavBarItemLink FindLastItemLink()
        {
            NavBarGroup lLastGroup = LastVisibleGroup(null);

            if (lLastGroup.ItemLinks.Count == 0)
                return null;

            return lLastGroup.VisibleItemLinks[lLastGroup.VisibleItemLinks.Count - 1];
        }

        private void SelectLink(NavBarItemLink link)
        {
            ActiveLink = link;

            if (ActiveLink != null && !ActiveLink.Group.Expanded)
                ActiveLink.Group.Expanded = true;

            if (ActiveLink != null)
                ViewInfo.MakeLinkVisible(ActiveLink);

            Refresh();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            Keys key = keyData & (~Keys.Modifiers);
            switch (key)
            {
                case Keys.Down:
                    SelectLink(FindNearItemLink(true));
                    break;
                case Keys.Up:
                    SelectLink(FindNearItemLink(false));
                    break;
                case Keys.Home:
                    SelectLink(FindFirstItemLink());
                    break;
                case Keys.End:
                    SelectLink(FindLastItemLink());
                    break;
                case Keys.Enter:
                    if (EnterClick != null) EnterClick(this, new EventArgs());
                    break;
                default:
                    return base.ProcessDialogKey(keyData);
            }
            return false;
        }

        #endregion
    }
}
