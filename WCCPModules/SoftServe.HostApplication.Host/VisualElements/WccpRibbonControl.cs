﻿using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.ViewInfo;
using System.ComponentModel;
using System.Drawing;

namespace ModularWinApp.VisualComponents
{
    /// <summary>
    /// Allows to hide PageGroupCaptions and allows page groups to be right aligned
    /// </summary>
    [ToolboxItem(true)]
    public partial class WccpRibbonControl : RibbonControl
    {
        bool _showCaption = true;

        public bool ShowGroupCaption
        {
            get { return _showCaption; }
            set
            {
                if (_showCaption != value)
                    _showCaption = value;
            }
        }

        protected override RibbonViewInfo CreateViewInfo()
        {
            return new WccpRibbonViewInfo(this);
        }
    }

    public class WccpRibbonViewInfo : RibbonViewInfo
    {
        public WccpRibbonViewInfo(RibbonControl ribbon) : base(ribbon) { }

        protected override RibbonPanelViewInfo CreatePanelInfo()
        {
            return new WccpRibbonPanelViewInfo(this);
        }
    }

    public class WccpRibbonPanelViewInfo : RibbonPanelViewInfo
    {
        public WccpRibbonPanelViewInfo(RibbonViewInfo viewInfo) : base(viewInfo) { }

        protected override RibbonPageGroupViewInfo CreateGroupViewInfo(RibbonPageGroup group)
        {
            return new WccpRibbonPageGroupViewInfo(ViewInfo, group);
        }

        protected override RibbonPanelLayoutCalculator CreatePanelLayoutCalculator()
        {
            return new WccpPanelComplexLayoutCalculator(this);
        }
    }

    /// <summary>
    /// Allows to hide PageGroupCaptions and allows page groups to be right aligned
    /// </summary>
    public class WccpRibbonPageGroupViewInfo : RibbonPageGroupViewInfo
    {
        public WccpRibbonPageGroupViewInfo(RibbonViewInfo viewInfo, RibbonPageGroup group) : base(viewInfo, group) { }

        protected new WccpRibbonControl Ribbon
        {
            get { return base.Ribbon as WccpRibbonControl; }
        }

        /// <summary>
        /// Allows to hide PageGroupCaptions 
        /// </summary>
        protected override int CalcCaptionHeightTabletOfficeEx()
        {
            return Ribbon.ShowGroupCaption ? base.CalcCaptionHeightTabletOfficeEx() : 0;
        }

        /// <summary>
        /// Allows page groups to be right aligned
        /// </summary>
        public override void CalcViewInfo(Rectangle bounds)
        {
            if (object.Equals(PageGroup.Tag, "AlignRight"))
            {
                int lOffset = ViewInfo.Bounds.Right - bounds.Right;
                if (lOffset > 5)
                {
                    bounds.Offset(lOffset, 0);
                }
            }

            base.CalcViewInfo(bounds);
        }
    }

    /// <summary>
    /// Allows page groups to be right aligned
    /// </summary>
    public class WccpPanelComplexLayoutCalculator : RibbonPanelComplexLayoutCalculator
    {
        public WccpPanelComplexLayoutCalculator(RibbonPanelViewInfo panelInfo)
            : base(panelInfo)
        {

        }

        /// <summary>
        /// Allows page groups to be right aligned
        /// </summary>
        public override void UpdatePanelLayout()
        {
            int lXPos = ContentBounds.Left - PanelInfo.PanelScrollOffset;

            for (int i = 0; i < PanelInfo.Groups.Count; i++)
            {
                RibbonPageGroup pageGroup = PanelInfo.Groups[i].PageGroup;

                PanelInfo.Groups[i].CalcViewInfo(new Rectangle(lXPos, ContentBounds.Top, PanelInfo.Groups[i].PrecalculatedWidth, ContentBounds.Height));

                if (object.Equals(pageGroup.Tag, "AlignRight"))
                    continue;

                lXPos += PanelInfo.Groups[i].PrecalculatedWidth + PanelInfo.DefaultIndentBetweenGroups;
            }
        }
    }
}
