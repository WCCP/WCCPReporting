﻿namespace SoftServe.ModularWinApp.DataAccess
{
    public static class SqlConstants
    {
        public static string DelimiterIdList = ",";
        public static string ItemIdFieldName = "Item_ID";
        public static string LevelFieldName = "Level";

        public static string spDwUrmTreeObjects = "spDW_URM_TreeObjects";
        public static string param_IsDesktop = "isDesktop";

        public static string spDwUrmGetObjects = "spDW_URM_GetObjects";

        public static string spDwUrmGetObjectData = "spDW_URM_GetObjectData";
        public static string param_ObjectId = "objectId";
    }
}