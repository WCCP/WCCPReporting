﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Globalization;
using System.Linq;
using BLToolkit.Data;
using ModularWinApp;
using ModularWinApp.Models;

namespace SoftServe.ModularWinApp.DataAccess
{
    public static class DataProvider
    {
        private static DbManager _db;

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null || _db.Connection.ConnectionString != HostConnectionSettings.ConnectionString)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConnectionSettings.ConnectionString);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        #endregion

        public static List<ModuleTreeItem> GetModuleTreeList(bool isDesktop)
        {
            List<ModuleTreeItem> list = Db.SetSpCommand(SqlConstants.spDwUrmTreeObjects,
                Db.Parameter(SqlConstants.param_IsDesktop, isDesktop)).ExecuteList<ModuleTreeItem>();

            return list;
        }

        public static List<ModuleObjectItem> GetModuleList()
        {
            List<ModuleObjectItem> list = Db.SetSpCommand(SqlConstants.spDwUrmGetObjects).ExecuteList<ModuleObjectItem>();

            return list;
        }

        public static byte[] GetModuleDll(int reportId)
        {
            DataTable moduleObject = Db.SetSpCommand(SqlConstants.spDwUrmGetObjectData,
                                                        Db.Parameter(SqlConstants.param_ObjectId, reportId))
                                          .ExecuteDataTable();

            return (byte[])moduleObject.Rows[0][0];
        }
    }
}