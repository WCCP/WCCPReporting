﻿using DevExpress.XtraEditors;
using ModularWinApp.Core.Interfaces;
using System;
using System.ComponentModel;

namespace ModularWinApp
{
    public partial class ReportTabForm : XtraForm
    {
        public string LibraryName { get; set; }
        public IStartupClass Module { get; set; }

        public ReportTabForm()
        {
            InitializeComponent();
            LibraryName = string.Empty;

            Closing += OnClosing;
            Closed += OnClosed;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            if (Module != null)
            {
                cancelEventArgs.Cancel = !Module.AllowClose();
            }
        }

        private void OnClosed(object sender, EventArgs eventArgs)
        {
            if (Module != null)
            {
                Module.CloseUI();
            }
        }
    }
}
