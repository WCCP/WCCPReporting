﻿using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using SoftServe.Reports.MarketProgramms.UserControls;
using Logica.Reports.Common.WaitWindow;

namespace SoftServe.Reports.MarketProgramms.Tabs
{
  /// <summary>
  /// 
  /// </summary>
  public class ActivityListTab : CommonBaseTab, IPrint
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ClientListTab"/> class.
    /// </summary>
    /// <param name="parent">The parent.</param>
    public ActivityListTab(BaseReportUserControl bruc)
    {
      if (!this.DesignMode && bruc != null)
      {
        Init(bruc, new ActivityListControl(this), Resource.ActivitytList);

        ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;

        Bruc.ExportClick += new BaseReportUserControl.ExportMenuClickHandler(parent_ExportClick);
      }
    }

    /// <summary>
    /// Gets the view.
    /// </summary>
    /// <value>The view.</value>
    public ActivityListControl Content
    {
      get
      {
        return base.UserControl as ActivityListControl;
      }
    }

    /// <summary>
    /// Loads the data.
    /// </summary>
    public override void LoadData()
    {
      WaitManager.StartWait();
      Content.LoadData();
      WaitManager.StopWait();
    }

    /// <summary>
    /// Prepare container of elements for exporting
    /// </summary>
    /// <returns></returns>
    public CompositeLink PrepareCompositeLink()
    {
      CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
      compositeLink.Links.Add(new PrintableComponentLink() { Component = Content.GridControl });
      compositeLink.CreateDocument();
      return compositeLink;
    }

    /// <summary>
    /// Parent_s the export click.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="selectedPage">The selected page.</param>
    /// <param name="type">The type.</param>
    /// <param name="fName">Name of the f.</param>
    private void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
    {
      if (this == selectedPage)
      {
        switch (type)
        {
          case ExportToType.Html:
            PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
            break;
          case ExportToType.Mht:
            PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
            break;
          case ExportToType.Pdf:
            PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions() { Compressed = true });
            break;
          case ExportToType.Rtf:
            PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
            break;
          case ExportToType.Txt:
            PrepareCompositeLink().PrintingSystem.ExportToText(fName);
            break;
          case ExportToType.Xls:
            PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = Text });
            break;
          case ExportToType.Xlsx:
            PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
            break;
          case ExportToType.Bmp:
            PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
            break;
          case ExportToType.Csv:
            PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
            break;
        }
      }
    }
  }
}
