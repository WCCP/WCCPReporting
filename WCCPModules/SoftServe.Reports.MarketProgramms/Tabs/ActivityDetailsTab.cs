﻿using System;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using DevExpress.XtraTab.ViewInfo;
using SoftServe.Reports.MarketProgramms.UserControls;

namespace SoftServe.Reports.MarketProgramms.Tabs
{
  /// <summary>
  /// 
  /// </summary>
  public class ActivityDetailsTab : CommonBaseTab, IPrint
  {
      /// <summary>
    /// Initializes a new instance of the <see cref="ClientDetailsTab"/> class.
    /// </summary>
    /// <param name="bruc">The bruc.</param>
    /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
    /// <param name="tabName">Name of the tab.</param>
    public ActivityDetailsTab(BaseReportUserControl bruc, string tabName, string actionName, int clientId, int templateId)
    {
      if (!this.DesignMode && bruc != null)
      {
        ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;

        Bruc = bruc;
       
        Init(bruc, new ActivityDetailsControl(this, clientId, templateId, actionName), tabName);

        Bruc.btnExportTo.Enabled = false;

        Bruc.ExportClick += new BaseReportUserControl.ExportMenuClickHandler(parent_ExportClick);
        Bruc.TabControl.CloseButtonClick += new EventHandler(TabControl_CloseButtonClick);
      }
    }

    /// <summary>
    /// Gets the view.
    /// </summary>
    /// <value>The view.</value>
    public ActivityDetailsControl Content
    {
      get
      {
        return base.UserControl as ActivityDetailsControl;
      }
    }

    /// <summary>
    /// Parent_s the tool button visibility check.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="selectedPage">The selected page.</param>
    private void parent_ToolButtonVisibilityCheck(object sender, XtraTabPage selectedPage)
    {
      if (this == selectedPage)
      {
        DevExpress.XtraBars.BarItem barItem = sender as DevExpress.XtraBars.BarItem;
        if (barItem != null)
        {
          barItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
        }
      }
    }

    /// <summary>
    /// Loads the data.
    /// </summary>
    public override void LoadData()
    {
      if (Content != null)
      {
        Content.LoadData();
      }
    }

    /// <summary>
    /// Prepare container of elements for exporting
    /// </summary>
    /// <returns></returns>
    public CompositeLink PrepareCompositeLink()
    {
      CompositeLink compositeLink = new CompositeLink(new PrintingSystem());

      if (Content.CurrentBaseControl is ActivityDetailsAddresses)
      {
          ActivityDetailsAddresses addr = Content.CurrentBaseControl as ActivityDetailsAddresses;

          addr.PrepareToExport();
          compositeLink.Links.Add(new PrintableComponentLink() { Component = addr.Grid });
          compositeLink.CreateDocument();
          addr.RestoreAfterExport();
      }

      if (Content.CurrentBaseControl is ActivityDetailsGoals)
      {
          Content.controlGoals.PrepareToExport();
          compositeLink.Links.Add(new PrintableComponentLink() { Component = Content.controlGoals.BandedGrid });
          compositeLink.CreateDocument();
          Content.controlGoals.RestoreAfterExport();
      }

      return compositeLink;
    }

    /// <summary>
    /// Parent_s the export click.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="selectedPage">The selected page.</param>
    /// <param name="type">The type.</param>
    /// <param name="fName">Name of the f.</param>
    private void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
    {
      if (this == selectedPage)
      {
        switch (type)
        {
          case ExportToType.Html:
            PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
            break;
          case ExportToType.Mht:
            PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
            break;
          case ExportToType.Pdf:
            PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions() { Compressed = true });
            break;
          case ExportToType.Rtf:
            PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
            break;
          case ExportToType.Txt:
            PrepareCompositeLink().PrintingSystem.ExportToText(fName);
            break;
          case ExportToType.Xls:
            PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = Text});
            break;
          case ExportToType.Xlsx:
            PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
            break;
          case ExportToType.Bmp:
            PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
            break;
          case ExportToType.Csv:
            PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
            break;
        }
      }
    }

    /// <summary>
    /// Handles the CloseButtonClick event of the TabControl control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void TabControl_CloseButtonClick(object sender, EventArgs e)
    {
      ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
      if (arg != null && arg.Page == this)
      {
        Bruc.TabControl.TabPages.Remove(this);
      }
    }
  }
}
