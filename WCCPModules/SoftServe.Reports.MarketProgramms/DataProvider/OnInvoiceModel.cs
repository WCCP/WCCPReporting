﻿using System;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MarketProgramms {
    public abstract class OnInvoiceModel : EditableObject<OnInvoiceModel> {
        [MapField("Action_Id")]
        public abstract int ActionId { get; set; }
        [MapField("Action_Name")]
        public abstract string ActionName { get; set; }
        [MapField("Aim_Id")]
        public abstract int AimId { get; set; }
        [MapField("OL_id")]
        public abstract long OutletId { get; set; }
        [MapField("Item_Id")]
        public abstract int PeriodId { get; set; }
        public abstract DateTime DateFrom { get; set; }
        public abstract DateTime DateTo { get; set; }
        public abstract decimal DiscountSum { get; set; }
        public abstract decimal DiscountPercent { get; set; }

        public override bool Equals(object obj) {
            OnInvoiceModel lModel = obj as OnInvoiceModel;
            if (null != lModel)
                return ActionId.Equals(lModel.ActionId) && AimId.Equals(lModel.AimId) && OutletId.Equals(lModel.OutletId)
                    && (lModel.PeriodId < 0 || PeriodId.Equals(lModel.PeriodId)) && DateFrom.Equals(lModel.DateFrom) && DateTo.Equals(lModel.DateTo);

            if (obj == null)
                return false;
            return ReferenceEquals(obj, this);
        }

        public override int GetHashCode() {
            return OutletId.GetHashCode();
        }
    }
}