﻿using System.Collections.Generic;

namespace SoftServe.Reports.MarketProgramms
{
    public class ConflictingPeriodsDescription
    {
        public ConflictingPeriodsDescription(long outletId, string reason, List<OnInvoiceModel> childList)
        {
            OutletId = outletId;
            Reason = reason;
            PeriodList = childList;
        }

        public long OutletId { get; set; }

        public string Reason { get; set; }

        public List<OnInvoiceModel> PeriodList { get; set; }
    }
}
