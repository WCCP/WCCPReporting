﻿using System;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    public class ImportedPeriodModel
    {
        public long OutletId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public decimal DiscountSum { get; set; }
        public decimal DiscountPercent { get; set; }

        public long RowNumber { get; set; }
        public bool Status { get; set; }
        public string StatusDescription { get; set; }


        public static explicit operator OnInvoiceModel(ImportedPeriodModel imported)
        {
            OnInvoiceModel model = OnInvoiceModel.CreateInstance();

            model.OutletId = imported.OutletId;
            model.DateFrom = imported.DateFrom;
            model.DateTo = imported.DateTo;
            model.DiscountSum = imported.DiscountSum;
            model.DiscountPercent = imported.DiscountPercent;
            model.PeriodId = -1;

            model.ActionName = "Данные из файла";

            return model;
        }


        public static explicit operator ImportedPeriodWithErrorsModel(ImportedPeriodModel imported)
        {
            ImportedPeriodWithErrorsModel model = new ImportedPeriodWithErrorsModel();

            model.OutletId = imported.OutletId.ToString();
            model.DateFrom = imported.DateFrom.ToString();
            model.DateTo = imported.DateTo.ToString();
            model.DiscountSum = imported.DiscountSum.ToString();
            model.DiscountPercent = imported.DiscountPercent.ToString();

            model.RowNumber = imported.RowNumber;
            model.Status = true;

            return model;
        }
    }
}
