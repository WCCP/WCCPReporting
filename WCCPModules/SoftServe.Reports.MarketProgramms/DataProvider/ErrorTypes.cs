﻿namespace SoftServe.Reports.MarketProgramms
{
    public enum ErrorTypes
    {
        IncorrectDataFormat = 1,
        IncorrectPeriod = 2,
        IncorrectPeriodTranscendsActionLimits = 3,
        OlDoesntBelongToPromoGroup = 4,
        ConfilctingPeriods = 5
    }
}
