﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.MarketProgramms
{
  public class ActivityExpense : ErrorableDTO
  {
    //private int id = 0;

    //public int ID { get { return id; } set { id = value; } }
    public int ExpenseID { get; set; }
    public decimal Cost { get; set; }
  }
}
