﻿using System.Collections.Generic;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    public class ImportedPeriodWithErrorsModel
    {
        public string OutletId { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string DiscountSum { get; set; }
        public string DiscountPercent { get; set; }
        public List<OnInvoiceModel> PeriodList { get; set; }

        public long RowNumber { get; set; }
        public bool Status { get; set; }
        public string StatusDescription { get; set; }
    }
}
