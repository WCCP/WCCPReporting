﻿using System.Collections.Generic;
using System.Linq;

namespace SoftServe.Reports.MarketProgramms
{
    public class ConflictingPeriodsList
    {
        private List<ConflictingPeriodsDescription> _records;

        public ConflictingPeriodsList()
        {
            _records = new List<ConflictingPeriodsDescription>();
        }

        public void Add(string reason, List<OnInvoiceModel> models)
        {
            foreach (var onInvoiceModel in models)
            {
                Add(reason, onInvoiceModel);
            }
        }

        public void Add(string reason, OnInvoiceModel model)
        {
            List<OnInvoiceModel> lPeriodList;

            ConflictingPeriodsDescription record = _records.FirstOrDefault(r => r.OutletId == model.OutletId);

            lPeriodList = record == null ? new List<OnInvoiceModel>() : record.PeriodList;
            lPeriodList.Add(model);
           
            if (record == null)
            {
                Add(model.OutletId, reason, lPeriodList);
            }
        }

        private void Add(long outletId, string reason, List<OnInvoiceModel> childList)
        {
            ConflictingPeriodsDescription lRecord = new ConflictingPeriodsDescription(outletId, reason, childList);
            _records.Add(lRecord);
        }

        public ConflictingPeriodsDescription this[int Id]
        {
            get { return _records[Id]; }
        }

        public List<ConflictingPeriodsDescription> Records
        {
            get { return _records; }
            set { _records = value; }
        }

    }
}
