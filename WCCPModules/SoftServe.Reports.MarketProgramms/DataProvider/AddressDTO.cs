﻿using System;
using Logica.Reports.BaseReportControl.Utility;
using System.Collections.Generic;

namespace SoftServe.Reports.MarketProgramms
{
  /// <summary>
    /// 
    /// </summary>
    public class AddressDTO : ErrorableDTO
    {

      public AddressesInformation Data { get; set; }

      public List<string> AddressesList 
      { 
        get { return Data.DedupedAddressesList; } 
        set { Data.AddressesList = value; } 
      }

      public string Comment 
      {
        get { return Data.Comment; }
        set { Data.Comment = value; }
      }

      public Int64 ThRFilterId
      {
        get { return Data.ThRFilterId; }
        set { Data.ThRFilterId = value; }
      }

    }
}
