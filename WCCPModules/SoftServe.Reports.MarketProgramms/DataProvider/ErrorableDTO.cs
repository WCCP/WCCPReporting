﻿using System;

namespace SoftServe.Reports.MarketProgramms
{
  /// <summary>
  /// Represents DTO which able to return error message with description
  /// </summary>
  public class ErrorableDTO
  {
    /// <summary>
    /// Gets or sets the error code.
    /// </summary>
    /// <value>The error code.</value>
    public int ErrorCode { get; set; }
    /// <summary>
    /// Gets or sets the error description.
    /// </summary>
    /// <value>The error description.</value>
    public string ErrorDescription { get; set; }
  }
}
