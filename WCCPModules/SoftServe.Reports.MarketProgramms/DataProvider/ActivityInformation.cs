﻿using System;
using System.Data;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MarketProgramms
{
    class ActivityInformation
    {
        private const string PRICE_ACTIVITY = "Ценовые активности";
        private DataTable dtActivityDetails;
        private DataTable dtTemplateDetails;

        public ActivityInformation(DataTable dtTemplateDetails, DataTable dtActivityDetails)
        {
            this.dtActivityDetails = dtActivityDetails;
            this.dtTemplateDetails = dtTemplateDetails;
        }


        private bool IsActivityLoaded
        {
            get { return dtActivityDetails != null && dtActivityDetails.Rows.Count > 0; }
        }

        private bool IsTemplateLoaded
        {
            get
            {
                return dtTemplateDetails != null
                       && dtTemplateDetails.Rows.Count > 0;
            }
        }

        public bool IsDescriptionModifiable
        {
            get { return IsTemplateLoaded && !ConvertEx.ToBool(dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_ACTION_DESCRIPTION_DISABLED]); }
        }

        public bool IsInBudget
        {
            get { return IsActivityLoaded && ConvertEx.ToBool(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_IS_BUDGET]); }
        }

        public bool IsApproved
        {
            get { return IsActivityLoaded && ConvertEx.ToBool(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_ISAPPROVED]); }
        }

        public bool IsActivityStarted
        {
            get { return IsActivityLoaded && DateTime.Now >= ConvertEx.ToDateTime(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_ACTION_START]); }
        }

        public bool IsPriceActivity
        {
            get
            {
                return IsTemplateLoaded
                       && dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_ACTIONTYPE_NAME].ToString().Equals(PRICE_ACTIVITY);
            }
        }

        #region Unused flags

        public bool IsPeriodDayFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_ACTION_START_DAY); }
        }
        public bool IsPeriodMonthFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_ACTION_START_MONTH); }
        }
        public bool IsPeriodDayToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_ACTION_END_DAY); }
        }
        public bool IsPeriodMonthToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_ACTION_END_MONTH); }
        }

        public bool IsPeriod1DayFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE1_START_DAY); }
        }
        public bool IsPeriod1MonthFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE1_START_MONTH); }
        }
        public bool IsPeriod1YearFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE1_START_YEAR_OFFSET); }
        }
        public bool IsPeriod1DayToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE1_END_DAY); }
        }
        public bool IsPeriod1MonthToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE1_END_MONTH); }
        }
        public bool IsPeriod1YearToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE1_END_YEAR_OFFSET); }
        }

        public bool IsPeriod2DayFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE2_START_DAY); }
        }
        public bool IsPeriod2MonthFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE2_START_MONTH); }
        }
        public bool IsPeriod2YearFromEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE2_START_YEAR_OFFSET); }
        }
        public bool IsPeriod2DayToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE2_END_DAY); }
        }
        public bool IsPeriod2MonthToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE2_END_MONTH); }
        }
        public bool IsPeriod2YearToEnabled
        {
            get { return GetPeriodEnabled(Constants.SP_TEMPLATE_DETAILS_BASE2_END_YEAR_OFFSET); }
        }

        #endregion

        #region Data

        public int OutletQuantity
        {
            get { return IsActivityLoaded ? ConvertEx.ToInt(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_OUTLET_QUANTITY]) : 0; }
        }

        public bool PromoGroupCascadeEdit
        {
            get { return ConvertEx.ToBool(dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_OL_EDIT_CASCADE]); }
        }

        public bool PromoGroupAllowAddAll
        {
            get { return ConvertEx.ToBool(dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_OL_ADD_MANUALLY]); }
        }

        public bool PromoGroupAllTTInActive
        {
            get { return ConvertEx.ToBool(dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_OL_CHECK_TR]); }
        }

        public bool ControlGroupCascadeEdit
        {
            get { return ConvertEx.ToBool(dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_CNT_EDIT_CASCADE]); }
        }

        public bool ControlGroupAllowAddAll
        {
            get { return ConvertEx.ToBool(dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_CNT_ADD_MANUALLY]); }
        }

        public bool ControlGroupAllTTInActive
        {
            get { return ConvertEx.ToBool(dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_CNT_CHECK_TR]); }
        }

        #endregion

        public int ControlTTCount { get { return IsActivityLoaded ? ConvertEx.ToInt(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_CNT_TT_COUNT]) : 0; } }
        public int ControlTTAvailable { get { return IsActivityLoaded ? ConvertEx.ToInt(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_CNT_TT_AVAILABLE]) : 0; } }

        public int PromoTTCount { get { return IsActivityLoaded ? ConvertEx.ToInt(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_OL_TT_COUNT]) : 0; } }
        public int PromoTTAvailable { get { return IsActivityLoaded ? ConvertEx.ToInt(dtActivityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_OL_TT_AVAILABLE]) : 0; } }


        private bool GetPeriodEnabled(string field)
        {
            return IsTemplateLoaded && (dtTemplateDetails.Rows[0].IsNull(field) || ConvertEx.ToInt(dtTemplateDetails.Rows[0][field]) == 0);
        }

    }
}
