﻿using BLToolkit.EditableObjects;
using BLToolkit.Mapping;

namespace SoftServe.Reports.MarketProgramms {
    public class PromoOutletModel : EditableObject<PromoOutletModel> {
        [MapField("OL_id")]
        public long OutletId { get; set; }
        public string OLName { get; set; }
        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
    }
}