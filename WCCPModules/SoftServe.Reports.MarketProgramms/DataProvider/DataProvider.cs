﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using BLToolkit.Data;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.MarketProgramms {
    public static class DataProvider
    {
        private static readonly Dictionary<string, bool> _positionsAccessibility = new Dictionary<string, bool>();
        private static int _userLevel;
        private static DbManager _db;

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15*60; // 15 minutes by default
                }
                return _db;
            }
        }

        #endregion

        private static void ExecuteNonQuery(string spName, SqlParameter[] parameters)
        {
            try
            {
                Db.SetSpCommand(spName, parameters).ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
        }

        private static DataTable GetData(string spName, SqlParameter[] parameters)
        {
            DataTable dataTable = Db.SetSpCommand(spName, parameters).ExecuteDataTable();
            return dataTable;
        }

        private static DataTable GetData(string spName)
        {
            return GetData(spName, new SqlParameter[0]);
        }

        #region Main lists

        /// <summary>
        /// Gets the activity list.
        /// </summary>
        /// <value>The activity list.</value>
        public static DataTable ActivityList
        {
            get
            {
                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_ACTIONS_LIST).ExecuteDataTable();
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the templates list.
        /// </summary>
        /// <value>The templates list.</value>
        public static DataTable TemplatesList
        {
            get
            {
                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_TEMPLATES_LIST).ExecuteDataTable();
                return dataTable;
            }
        }

        public static DataTable RegionsTree
        {
            get
            {
                DataTable dataTable =
                    Db.SetSpCommand(Constants.SP_GET_REGIONS_TREE, Db.Parameter(Constants.PARAM_USERLOGIN, DBNull.Value))
                        .ExecuteDataTable();
                return dataTable;
            }
        }

        #endregion

        #region Common Functionality

        private static void ExecuteErrorableVoid(string sp, SqlParameter[] parameters, ErrorableDTO dto)
        {
            try
            {
                DataTable dataTable = Db.SetSpCommand(sp, parameters).ExecuteDataTable();
                if (dataTable != null)
                    if (dataTable.Rows.Count > 0)
                    {
                        if (dataTable.Columns.Contains(Constants.SP_ERROR_CODE))
                            dto.ErrorCode = ConvertEx.ToInt(dataTable.Rows[0][Constants.SP_ERROR_CODE]);
                        if (dataTable.Columns.Contains(Constants.SP_ERROR_NUMBER))
                            dto.ErrorCode = ConvertEx.ToInt(dataTable.Rows[0][Constants.SP_ERROR_NUMBER]);
                        if (dataTable.Columns.Contains(Constants.SP_ERROR_DESCRIPTION))
                            dto.ErrorDescription = ConvertEx.ToString(dataTable.Rows[0][Constants.SP_ERROR_DESCRIPTION]);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckPosition(string position)
        {
            if (IsLDB) return false;

            if (!_positionsAccessibility.ContainsKey(position))
            {
                object result =
                    Db.SetSpCommand(Constants.SP_CHECK_POSITION, Db.Parameter(Constants.PARAM_USERPOSITION, position),
                        Db.Parameter(Constants.PARAM_USERLOGIN, DBNull.Value)).ExecuteScalar();
                _positionsAccessibility.Add(position, ConvertEx.ToBool(result));
            }
            return _positionsAccessibility[position];
        }

        public static int UserLevel
        {
            get
            {
                if (_userLevel == 0)
                {
                    _userLevel = ConvertEx.ToInt(Db.SetSpCommand(Constants.SP_GET_USER_LEVEL).ExecuteScalar());
                }
                return _userLevel;
            }
        }

        public static bool IsLDB
        {
            get { return UserLevel == 2; }
        }

        #endregion

        #region Common Tab

        public static void ReculculateOutletData(int actionId)
        {
            Db.SetSpCommand(Constants.SP_RECULC_OTLET_DATA, Db.Parameter(Constants.PARAM_ACTION_id, actionId)).ExecuteScalar();
        }

        public static DataTable GetActivityLevels(int templateId)
        {
            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_ACTIVITY_LEVELS_LIST,
                    Db.Parameter(Constants.PARAM_ACTION_ID, templateId)).ExecuteDataTable();
            return table;
        }

        public static DataTable GetActivityDetails(int activityId)
        {
            object objActivityId = DBNull.Value;
            if (activityId > 0) objActivityId = activityId;
            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_ACTION_DETAILS,
                    Db.Parameter(Constants.PARAM_ACTIONID, objActivityId)).ExecuteDataTable();
            return table;
        }

        public static DataTable GetTemplateDetails(int activityId, int templateId)
        {
            object objActivityId = DBNull.Value;
            object objTemplateId = DBNull.Value;
            if (activityId > 0)
                objActivityId = activityId;
            if (templateId > 0)
                objTemplateId = templateId;
            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_TEMPLATE_DETAILS,
                    Db.Parameter(Constants.PARAM_ACTIONID, objActivityId),
                    Db.Parameter(Constants.PARAM_TEMPLATEID, objTemplateId)).ExecuteDataTable();
            return table;
        }

        public static int InsertActivityCommon(ActivityDetailsDTO activity, bool edit)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter(Constants.PARAM_ACTIONID, activity.ActivityId),
                new SqlParameter(Constants.PARAM_TEMPLATEID, activity.TemplateId),
                new SqlParameter(Constants.PARAM_ACTION_NAME, activity.Name),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_4, activity.ActivityTypeId),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_5, activity.ActivityLevelId),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_6, activity.IsApproved),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_7, activity.ActivityStart),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_8, activity.ActivityEnd),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_9, activity.Period1Start),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_10, activity.Period1End),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_11, activity.Period2Start),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_12, activity.Period2End),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_13, activity.MonthsToTrackUpliftAfterEnd),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_14, activity.IsTakeActivePeriodsFromActiveWave),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_15, activity.Description),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_16, activity.IsShowInKPK),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_17, activity.IsConsiderConnectionDate),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_18, activity.ActivityDescriptionTemplateInKPK),
                new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_27, activity.IsEnableMoreOneAim)
            };

            if (!edit)
            {
                sqlParameters.RemoveAt(0); // remove ID param
            }
            else
            {
                sqlParameters.RemoveAt(1); // remove TemplateID param
                sqlParameters.AddRange(
                    new[]
                    {
                        new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_22, activity.ExpectedMarketTrend),
                        new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_23, activity.ExpectedUpLift),
                        new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_24, activity.AverageMACO),
                        new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_25, activity.OutletQuantity),
                        new SqlParameter(Constants.SP_UPDATE_ACTION_PARAM_26, activity.PlannedAddressesInclude)
                    }
                    );
            }

            sqlParameters.Add(new SqlParameter(Constants.SP_UPDATE_ACTION_MECHANICPRICETYPE_ID,
                activity.MechanicsPriceType));

            DataTable table =
                Db.SetSpCommand(edit ? Constants.SP_UPDATE_ACTION_DETAILS : Constants.SP_INSERT_ACTION_DETAILS,
                    sqlParameters.ToArray()).ExecuteDataTable();

            int newClientId = 0;

            if (table != null && table.Rows.Count > 0 && table.Columns.Count == 1)
            {
                newClientId = ConvertEx.ToInt(table.Rows[0][0]);
            }
            if (table != null && table.Rows.Count > 0 && table.Columns.Count == 2)
            {
                activity.ErrorCode = ConvertEx.ToInt(table.Rows[0][Constants.SP_ERROR_NUMBER]);
                activity.ErrorDescription = ConvertEx.ToString(table.Rows[0][Constants.SP_ERROR_DESCRIPTION]);
            }


            if (newClientId < 0)
            {
                activity.ErrorCode = newClientId;
            }

            return newClientId;
        }

        public static void DeleteActivityCommon(ActivityDetailsDTO activity)
        {
            DataTable table =
                Db.SetSpCommand(Constants.SP_DELETE_ACTION_DETAILS,
                    Db.Parameter(Constants.PARAM_ACTIONID, activity.ActivityId)).ExecuteDataTable();
            if (table != null && table.Rows.Count > 0 && table.Columns.Count == 2)
            {
                activity.ErrorCode = ConvertEx.ToInt(table.Rows[0][Constants.SP_ERROR_NUMBER]);
                activity.ErrorDescription = ConvertEx.ToString(table.Rows[0][Constants.SP_ERROR_DESCRIPTION]);
            }
        }

        public static DataTable GetFilesList(int id)
        {
            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_DOCS_LIST,
                    Db.Parameter(Constants.PARAM_ACTION_ID, id)).ExecuteDataTable();
            return table;
        }

        public static byte[] GetDocument(int docId)
        {
            object objFile =
                Db.SetSpCommand(Constants.SP_GET_DOCS_DATA, Db.Parameter(Constants.SP_GET_DOCS_DATA_PARAM_1, docId))
                    .ExecuteScalar();

            return ConvertEx.ToBytesArray(objFile);
        }

        public static void InsertDocument(int activityId, string filePath, byte[] fileData)
        {
            try
            {
                Db.SetSpCommand(Constants.SP_INSERT_DOCS_DATA,
                    Db.Parameter(Constants.PARAM_ACTION_ID, activityId),
                    Db.Parameter(Constants.SP_INSERT_DOCS_DATA_PARAM_2, filePath),
                    Db.Parameter(Constants.SP_INSERT_DOCS_DATA_PARAM_3, fileData))
                    .ExecuteScalar();
            }
            catch (Exception)
            {
            }
        }

        public static void DeleteDocument(int id)
        {
            try
            {
                Db.SetSpCommand(Constants.SP_DELETE_DOCS_DATA, Db.Parameter(Constants.SP_DELETE_DOCS_DATA_PARAM_1, id))
                    .ExecuteScalar();
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Config Goals Tab

        public enum MechanicsType
        {
            Discount = 0,
            Bonus
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        public static void DeleteMechanic(int ID)
        {
            Db.SetSpCommand(Constants.SP_DW_PG_DELETE_MECHANICS, Db.Parameter(Constants.PAR_ID, ID)).ExecuteNonQuery();
        }

        internal static void UpdateMechanic(int actionID, MechanicInfo mi)
        {
            string execSPName = Constants.SP_DW_PG_UPDATE_DISCOUNT_MECHANICS;

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter(Constants.PAR_ID, mi.ID));
            parameters.Add(new SqlParameter(Constants.PARAM_ACTION_ID, actionID));
            parameters.Add(new SqlParameter(Constants.PAR_PRODUCTCN_ID, mi.ProductCnID));
            if (mi.PriceTypeID > 2)
                parameters.Add(new SqlParameter(Constants.PAR_CONCURRENTCN_ID, mi.ConcurrentCnID));
            else
                parameters.Add(new SqlParameter(Constants.PAR_CONCURRENTCN_ID, DBNull.Value) {SqlDbType = SqlDbType.Int});
            parameters.Add(new SqlParameter(Constants.PAR_PRICETYPE_ID, mi.PriceTypeID));
            parameters.Add(new SqlParameter(Constants.PAR_PRICE, mi.Price));
            parameters.Add(new SqlParameter(Constants.PAR_OVERVALUE, mi.Overvalue));
            if (mi.IsBonusProduct) // bonus type
            {
                parameters.Add(new SqlParameter(Constants.PAR_BONUSPRODUCT_ID, mi.BonusProductID));
                parameters.Add(new SqlParameter(Constants.PAR_CORRECTIONRATE, mi.CorrectionVCoef));
                parameters.Add(new SqlParameter(Constants.PAR_MAXDISCOUNTPRC, mi.MaxDiscount));
                execSPName = Constants.SP_DW_PG_UPDATE_BONUS_MECHANICS;
            }

            ExecuteNonQuery(execSPName, parameters.ToArray());
        }

        /// <summary>
        /// Returns mechnics by type
        /// </summary>
        /// <param name="mechanicsType"></param>
        /// <param name="actionID">The Action ID</param>
        /// <returns></returns>
        public static DataTable GetMechanics(MechanicsType mechanicsType, int actionID)
        {
            return
                GetData(
                    (mechanicsType == MechanicsType.Discount)
                        ? Constants.SP_DW_PG_GET_DISCOUNT_MECHANICS
                        : Constants.SP_DW_PG_GET_BONUS_MECHANICS,
                    new SqlParameter[]
                    {
                        new SqlParameter(Constants.PARAM_ACTION_ID, actionID)
                    }
                    );
        }

        /// <summary>
        /// Returns price types
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPriceTypes()
        {
            return GetData(Constants.SP_DW_PG_GET_PRICE_TYPES);
        }

        /// <summary>
        /// Returns concurent combi product
        /// </summary>
        /// <returns></returns>
        public static DataTable GetConcurentCombiProducts()
        {
            return GetData(Constants.SP_DW_PG_GET_CONCURRENT_CN);
        }

        /// <summary>
        /// Returns bonus combi product
        /// </summary>
        /// <returns></returns>
        public static DataTable GetBonusCombiProducts()
        {
            return GetData(Constants.SP_DW_PG_GET_BONUS_PRODUCT);
        }

        //new 
        /// <summary>
        /// Gets the channel list.
        /// </summary>
        /// <value>the channel list.</value>
        public static DataTable GoalTypesList
        {
            get
            {
                DataTable table = Db.SetSpCommand(Constants.SP_GET_GOAL_TYPES_LIST).ExecuteDataTable();
                return table;
            }
        }

        /// <summary>
        /// Returns list of all aim in the current template
        /// </summary>
        /// <param name="templateId">template Id</param>
        /// <returns>list of all aim in the current template</returns>
        public static DataTable GetAimList(int templateId)
        {
            DataTable dataTable = null;
            object objTemplateId = DBNull.Value;
            if (templateId > 0)
            {
                objTemplateId = templateId;
            }
            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_AIM_LIST, Db.Parameter(Constants.PARAM_ACTION_ID, objTemplateId))
                    .ExecuteDataTable();
            return table;
        }

        /// <summary>
        /// Gets the SKU tree
        /// </summary>
        /// <param name="templateId">The template id.</param>
        /// <returns>the SKU tree</returns>
        public static DataTable GetSKUTree(int aimId)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_SKU_TREE_AIM,
                    Db.Parameter(Constants.SP_GET_SKU_TREE_AIM_PARAM1, objAimId))
                    .ExecuteDataTable();
            return table;
        }

        /// <summary>
        /// Gets the KPI tree
        /// </summary>
        /// <param name="templateId">template id</param>
        /// <returns>KPI tree</returns>
        public static DataTable GetKPITree(int aimId)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_KPI_TREE_AIM,
                    Db.Parameter(Constants.SP_GET_KPI_TREE_AIM_PARAM1, objAimId))
                    .ExecuteDataTable();
            return table;
        }

        /// <summary>
        /// Gets the QPQ tree
        /// </summary>
        /// <param name="templateId">template id</param>
        /// <returns>QPQ tree</returns>
        public static DataTable GetQPQTree(int aimId, int qpqType)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_QPQ_TREE_AIM,
                    Db.Parameter(Constants.SP_GET_QPQ_TREE_AIM_PARAM_ID, objAimId),
                    Db.Parameter(Constants.SP_GET_QPQ_TREE_AIM_PARAM_Type, qpqType))
                    .ExecuteDataTable();
            return table;
        }

        /// <summary>
        /// List of IPTR conditions
        /// </summary>
        /// <returns>list of IPTR conditions</returns>
        public static DataTable GetIPTRList()
        {
            DataTable table = Db.SetSpCommand(Constants.SP_GET_IPTR_LIST_AIM).ExecuteDataTable();
            return table;
        }

        /// <summary>
        /// Gets the IPTR aim
        /// </summary>
        /// <param name="aimId">aim id</param>
        /// <returns>IPTR aim</returns>
        public static DataTable GetIPTRAim(int aimId)
        {
            object objAimId = 0;

            if (aimId > 0)
            {
                objAimId = aimId;
            }

            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_IPTR_AIM,
                    Db.Parameter(Constants.SP_GET_IPTR_AIM_PARAM1, objAimId))
                    .ExecuteDataTable();
            return table;
        }

        /// <summary>
        /// Gets the MustStock SKU tree
        /// </summary>
        /// <param name="templateId">template id</param>
        /// <returns>QPQ tree</returns>
        public static DataTable GetMustStockTree(int aimId)
        {
                object objAimId = DBNull.Value;
                if (aimId > 0)
                {
                    objAimId = aimId;
                }
                DataTable table =
                               Db.SetSpCommand(Constants.SP_GET_SKU_TREE_AIM,
                                   Db.Parameter(Constants.SP_GET_SKU_TREE_AIM_PARAM1, objAimId))
                                   .ExecuteDataTable();
                return table;
        }

        /// <summary>
        /// Updates aim
        /// </summary>
        /// <param name="templateId">template Id</param>
        /// <param name="aimName">aim name</param>
        /// <param name="aimType">aim type</param>
        /// <param name="isUsedInComlpeted">whether is used in comlpeted</param>
        /// <param name="isPeriodCalcAim">whether is period calculated aim</param>
        /// <param name="aimId">aim Id</param>
        /// <returns>aim Id</returns>
        public static int UpdateAim(int templateId, string aimName, int aimType, bool isUsedInComlpeted,
            bool isPeriodCalcAim, int aimId,
            int factCalcPeriod, int factCalcWay, bool isUseAimValue, bool isInReverseExecution,
            bool isAccPerfOfTargetVal)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            Db.SetSpCommand(Constants.SP_UPDATE_AIM,
                Db.Parameter(Constants.PARAM_ACTION_ID, templateId),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM2, aimName),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM3, aimType),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM4, isUsedInComlpeted),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM5, isPeriodCalcAim),
                Db.Parameter(ParameterDirection.Output, Constants.SP_UPDATE_AIM_PARAM6, DbType.Int32),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM7, objAimId),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM8,
                    aimType == (int) GoalType.MustStock ? factCalcPeriod : (object) DBNull.Value),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM9,
                    aimType == (int) GoalType.MustStock ? factCalcWay : (object) DBNull.Value),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM10,
                    aimType == (int) GoalType.Merchandising ? isUseAimValue : (object) DBNull.Value),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM11,
                    aimType == (int) GoalType.Merchandising ? isInReverseExecution : (object) DBNull.Value),
                Db.Parameter(Constants.SP_UPDATE_AIM_PARAM12,
                    aimType == (int) GoalType.Merchandising ? isAccPerfOfTargetVal : (object) DBNull.Value)
                ).ExecuteScalar();
            object val = Db.Command.Parameters[Constants.SP_UPDATE_AIM_PARAM6];
            return ConvertEx.ToInt(((IDataParameter) val).Value);
        }

        /// <summary>
        /// Updates KPI aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="data">selected items list</param>
        /// <returns>aim Id</returns>
        public static int UpdateKPIAim(int aimId, SqlXml data)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            object result = Db.SetSpCommand(Constants.SP_UPDATE_KPI_AIM,
                Db.Parameter(Constants.SP_UPDATE_KPI_AIM_PARAM1, objAimId),
                Db.Parameter(Constants.SP_UPDATE_KPI_AIM_PARAM2, data)).ExecuteScalar();

            return ConvertEx.ToInt(result);
        }

        /// <summary>
        /// Updates SKU aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="SKUlist">selected items list</param>
        /// <returns></returns>
        public static int UpdateSKUAim(int aimId, SqlXml SKUlist)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            object result = Db.SetSpCommand(Constants.SP_UPDATE_SKU_AIM,
                Db.Parameter(Constants.SP_UPDATE_SKU_AIM_PARAM1, objAimId),
                Db.Parameter(Constants.SP_UPDATE_SKU_AIM_PARAM2, SKUlist)).ExecuteScalar();

            return ConvertEx.ToInt(result);
        }

        /// <summary>
        /// Updates IPTR aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="equipmentClass">equipment class</param>
        /// <returns>aim Id</returns>
        public static int UpdateIPTRAim(int aimId, int equipmentClass)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            object result = Db.SetSpCommand(Constants.SP_UPDATE_IPTR_AIM,
                Db.Parameter(Constants.SP_UPDATE_IPTR_AIM_PARAM1, objAimId),
                Db.Parameter(Constants.SP_UPDATE_IPTR_AIM_PARAM2, equipmentClass)).ExecuteScalar();

            return ConvertEx.ToInt(result);
        }

        /// <summary>
        /// Deletes aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        public static void DeleteAim(int aimId) {
            object objAimId = 0;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            Db.SetSpCommand(Constants.SP_DELETE_AIM,
                Db.Parameter(Constants.SP_DELETE_AIM_PARAM1, objAimId)).ExecuteDataTable();
        }

        /// <summary>
        /// Updates QPQ aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="aimType"> 1 if logic, 2 if digit</param>
        /// <param name="selectedId"> id if checked item in UI</param>>
        /// <returns>aim Id</returns>
        internal static int UpdateQPQAim(int aimId, int aimType, string selectedId)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            object result = Db.SetSpCommand(Constants.SP_UPDATE_QPQ_AIM,
                Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_ID, objAimId),
                Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_Type, aimType),
                Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_DOCITM, ConvertEx.ToGuid(selectedId),DbType.Guid)).ExecuteScalar();

            return ConvertEx.ToInt(result);
        }

        /// <summary>
        /// Updates QPQ aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="aimType"> 1 if logic, 2 if digit</param>
        /// <param name="selectedId"> id if checked item in UI</param>>
        /// <returns>aim Id</returns>
        internal static int UpdateQPQAim(int aimId, int aimType, bool isInReverseExec, string selectedId)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            object result = Db.SetSpCommand(Constants.SP_UPDATE_QPQ_AIM,
                Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_ID, objAimId),
                Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_Type, aimType),
                Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_ISIN_REVERSE_EXEC, isInReverseExec),
                Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_DOCITM, ConvertEx.ToGuid(selectedId),DbType.Guid)).ExecuteScalar();

            return ConvertEx.ToInt(result);
        }
        
        #endregion

        #region Goals Tab

        public static DataTable GetPlansFacts(int activityId, DateTime startDate, DateTime endDate, bool showFacts)
        {
            try
            {
                DataTable table = Db.SetSpCommand(Constants.SP_GET_PLANS_FACTS,
                    Db.Parameter(Constants.PARAM_ACTION_ID, activityId),
                    Db.Parameter(Constants.SP_GET_PLANS_FACTS_PARAM_2, startDate),
                    Db.Parameter(Constants.SP_GET_PLANS_FACTS_PARAM_3, endDate),
                    Db.Parameter(Constants.SP_GET_PLANS_FACTS_PARAM_4, showFacts)).ExecuteDataTable();
                return table;
            }
            catch (TimeoutException e)
            {
                XtraMessageBox.Show("Истекло время ожидания выполнения запроса");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Ошибка при выполнении запроса");
            }
            return null;
        }

        public static DataTable CheckOls(int activityId, string yearMonth, SqlXml data)
        {
            try
            {
                DataTable table = Db.SetSpCommand(Constants.SP_CHECK_OLS,
                    Db.Parameter(Constants.PARAM_ACTION_ID, activityId),
                    Db.Parameter(Constants.SP_CHECK_OLS_PARAM_2, yearMonth),
                    Db.Parameter(Constants.SP_CHECK_OLS_PARAM_3, data)).ExecuteDataTable();
                return table;
            }
            catch (Exception e)
            {
                //XtraMessageBox.Show("No data in the grid");
            }
            return null;
        }

        public static DataTable LoadPlans(int activityId, string yearMonth, SqlXml data)
        {
            DataTable table = Db.SetSpCommand(Constants.SP_LOAD_PLANS,
                   Db.Parameter(Constants.PARAM_ACTION_ID, activityId),
                   Db.Parameter(Constants.SP_LOAD_PLANS_PARAM_2, yearMonth),
                   Db.Parameter(Constants.SP_LOAD_PLANS_PARAM_3, data)).ExecuteDataTable();
            return table;
        }

        #endregion

        #region Control Group Tab

        public static DataTable GetActivityAddresses(int activityId, bool isControlGroup)
        {
            Db.Command.CommandTimeout = 30*60;
            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_ACTION_OUTLETS,
                    Db.Parameter(Constants.PARAM_ACTION_ID, activityId),
                    Db.Parameter(Constants.SP_GET_ACTION_OUTLETS_PARAM_2, isControlGroup)).ExecuteDataTable();
            Db.Command.CommandTimeout = 15*60;
            return table;
        }

        /// <summary>
        /// Gets the addresses list by OLI ds.
        /// </summary>
        /// <param name="activityId">The activity id.</param>
        /// <param name="isControlGroup">if set to <c>true</c> [is control group].</param>
        /// <param name="isCalcV">if set to <c>true</c> [is calc V].</param>
        /// <param name="strIdList">The STR id list.</param>
        /// <param name="ThRFilterId">The Thomas Research filter id.</param>
        /// <returns>DataTable of addresses</returns>
        public static DataTable GetAddressesListByOLIDs(int activityId, string strIdList)
        {
            Db.Command.CommandTimeout = 30 * 60;
            DataTable table =
                Db.SetSpCommand(Constants.SP_GET_OUTLET_DATA,
                    Db.Parameter(Constants.PARAM_ACTION_ID, activityId),
                    Db.Parameter(Constants.SP_GET_OUTLET_DATA_PARAM_2, strIdList)).ExecuteDataTable();
            Db.Command.CommandTimeout = 15 * 60;
            return table;
        }

        public static void AddAddresses(int activityId, AddressDTO data, bool isControlGroup) {
            object filterId = DBNull.Value;
            if (data.ThRFilterId > 0)
                filterId = data.ThRFilterId;

            ExecuteErrorableVoid(
                Constants.SP_ADD_OUTLET,
                new[] {
                    new SqlParameter(Constants.PARAM_ACTION_ID, activityId),
                    new SqlParameter(Constants.SP_ADD_OUTLET_PARAM_2, string.Join(Constants.DELIMITER_ID_LIST, data.AddressesList.ToArray()))
                    ,
                    new SqlParameter(Constants.SP_ADD_OUTLET_PARAM_3, isControlGroup),
                    new SqlParameter(Constants.SP_ADD_OUTLET_PARAM_4, data.Comment),
                    new SqlParameter(Constants.SP_ADD_OUTLET_PARAM_5, filterId)
                },
                data
                );
        }

        public static void DeleteAddresses(int activityId, AddressDTO data, bool isRestrictedDelete) {
            ExecuteErrorableVoid(
                Constants.SP_DELETE_OUTLET,
                new[] {
                    new SqlParameter(Constants.PARAM_ACTION_ID, activityId),
                    new SqlParameter(Constants.SP_DELETE_OUTLET_PARAM_2,
                        string.Join(Constants.DELIMITER_ID_LIST, data.AddressesList.ToArray())),
                    new SqlParameter(Constants.SP_DELETE_OUTLET_PARAM_3, isRestrictedDelete)
                },
                data
                );
        }

        public static void UpdateAddresses(int activityId, SqlXml xml, ErrorableDTO dto) {
            ExecuteErrorableVoid(
                Constants.SP_UPDATE_OUTLET,
                new[] {
                    new SqlParameter(Constants.PARAM_ACTION_ID, activityId),
                    new SqlParameter(Constants.SP_UPDATE_OUTLET_PARAM_2, xml),
                },
                dto
                );
        }

        public static DataTable GetActivationStatuses()
        {
            DataTable table = Db.SetSpCommand(Constants.SP_GET_ACTIVATION_STATUSES).ExecuteDataTable();
            return table;
        }

        public static DataTable GetDeclineReasons()
        {
            DataTable table = Db.SetSpCommand(Constants.SP_GET_DECLINE_REASONS).ExecuteDataTable();
            return table;
        }
        #endregion

        #region On-Invoice tab

        public static List<OnInvoiceModel> GetOnInvoiceAim(int activityId) {
            return Db.SetSpCommand(Constants.SP_DW_MP_GET_ONINVOICE_AIM,
                Db.Parameter(Constants.PARAM_ACTION_ID, activityId)
                ).ExecuteList<OnInvoiceModel>();
        }

        public static void DeleteOnInvoiceItem(int itemId) {
            Db.SetSpCommand(Constants.SP_DW_MP_DELETE_ONINVOICE_ITEM,
                Db.Parameter(Constants.PARAM_ITEM_ID, itemId)
                ).ExecuteNonQuery();
        }

        public static List<PromoOutletModel> GetPromoOutlets(int activityId) {
            return Db.SetSpCommand(Constants.SP_DW_MP_GET_ONINVOICE_PROMO_OUTLETS,
                Db.Parameter(Constants.PARAM_ACTION_ID, activityId)
                ).ExecuteList<PromoOutletModel>();
        }

        public static int SetOnInvoiceItem(OnInvoiceModel onInvoice) {
            return Db.SetSpCommand(Constants.SP_DW_MP_SET_ONINVOICE_ITEM,
                Db.Parameter(Constants.PARAM_ITEM_ID, onInvoice.PeriodId),
                Db.Parameter(Constants.PARAM_AIM_ID, onInvoice.AimId),
                Db.Parameter(Constants.PARAM_OL_ID, onInvoice.OutletId),
                Db.Parameter(Constants.PARAM_DATEFROM, onInvoice.DateFrom),
                Db.Parameter(Constants.PARAM_DATETO, onInvoice.DateTo),
                Db.Parameter(Constants.PARAM_DISCOUNTSUM, onInvoice.DiscountSum),
                Db.Parameter(Constants.PARAM_DISCOUNTPERCENT, onInvoice.DiscountPercent)
                ).ExecuteScalar<int>();
        }

        public static void ImportOnInvoiceItem(OnInvoiceModel onInvoice)
        {
            Db.SetSpCommand(Constants.SP_DW_MP_IMPORT_ONINVOICE_ITEM,
                Db.Parameter(Constants.PARAM_AIM_ID, onInvoice.AimId),
                Db.Parameter(Constants.PARAM_OL_ID, onInvoice.OutletId),
                Db.Parameter(Constants.PARAM_DATEFROM, onInvoice.DateFrom),
                Db.Parameter(Constants.PARAM_DATETO, onInvoice.DateTo),
                Db.Parameter(Constants.PARAM_DISCOUNTSUM, onInvoice.DiscountSum),
                Db.Parameter(Constants.PARAM_DISCOUNTPERCENT, onInvoice.DiscountPercent)
                ).ExecuteNonQuery();
        }

        public static List<OnInvoiceModel> GetOnInvoiceOutkets(int actionId) {
            return Db.SetSpCommand(Constants.SP_DW_MP_GET_ONINVOICE_OUTLETS,
                Db.Parameter(Constants.PARAM_ACTION_ID, actionId)
                ).ExecuteList<OnInvoiceModel>();
        }

        public static void ExportOnInvoiceToSW(int actionId)
        {
            SqlConnectionStringBuilder connectionForExport = new SqlConnectionStringBuilder(DataAccessLayer.ConnectionString.ConnectionString);
            connectionForExport.UserID = "osd";
            connectionForExport.Password = "M26T73984SKAVWpd$jS69opsMQ";
            connectionForExport.IntegratedSecurity = false;

            DbManager.AddConnectionString(connectionForExport.ConnectionString);
            
            DbManager exportDb = new DbManager();
            exportDb.Command.CommandTimeout = 20 * 60;
            
            exportDb.SetSpCommand(Constants.SP_DW_MP_EXPORT_TO_SW, 
                exportDb.Parameter(Constants.PARAM_INCOME_ACTION_ID, actionId)
                ).ExecuteNonQuery();
       }

        #endregion

        /// <summary>
        /// Gets chanels
        /// </summary>
        /// <returns>Table with chanels</returns>
        public static DataTable GetChanels()
        {
            DataTable table = Db.SetSpCommand(Constants.PROC_GET_CHANELS).ExecuteDataTable();
            return table;
        }
    }
}