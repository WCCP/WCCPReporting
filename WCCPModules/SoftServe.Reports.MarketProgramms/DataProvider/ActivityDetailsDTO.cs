﻿using System;
using System.Data.SqlTypes;
using System.Collections.Generic;

namespace SoftServe.Reports.MarketProgramms
{
    public class MechanicInfo
    {
        public int ID { get; set; }
        public bool IsBonusProduct { get; set; }
        public int ProductCnID { get; set; }
        public int ConcurrentCnID { get; set; }
        public int PriceTypeID { get; set; }
        public decimal? Price { get; set; }
        public decimal Overvalue { get; set; }

        // properties for bonus type
        public int BonusProductID { get; set; }
        public decimal CorrectionVCoef { get; set; }
        public decimal MaxDiscount { get; set; }

        public bool IsValid
        {
            get
            {
                bool res = (ProductCnID > 0 && PriceTypeID >= 0 && null != Price && null != Overvalue);
                if (res && (PriceTypeID > 2)/* logic for price type*/)
                    res &= (ConcurrentCnID > 0);
                if (res && IsBonusProduct)
                    res &= ((BonusProductID > 0) && (null != CorrectionVCoef) && (null != MaxDiscount));
                return res;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ActivityDetailsDTO : ErrorableDTO
    {
        public ActivityDetailsDTO()
        {
            MechanicsInfo = new List<MechanicInfo>();
            DeletedMechanics = new List<int>();
        }
        public int ActivityId { get; set; }
        public int TemplateId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ActivityTypeId { get; set; }
        public bool IsApproved { get; set; }
        public int ActivityLevelId { get; set; }
        public DateTime ActivityStart { get; set; }
        public DateTime ActivityEnd { get; set; }
        public int MonthsToTrackUpliftAfterEnd { get; set; }
        public bool IsTakeActivePeriodsFromActiveWave { get; set; }
        public DateTime Period1Start { get; set; }
        public DateTime Period1End { get; set; }
        public DateTime Period2Start { get; set; }
        public DateTime Period2End { get; set; }

        public bool ControlCascadeEdit { get; set; }
        public bool ControlAllowAddAll { get; set; }
        public bool ControlAllTTInActiveWave { get; set; }
        public bool ControlUpliftByBasePeriod { get; set; }
        public int ControlMonthsToCalculateUplift { get; set; }

        public bool PromoCascadeEdit { get; set; }
        public bool PromoAllowAddAll { get; set; }
        public bool PromoAllTTInActiveWave { get; set; }
        public bool PromoUpliftByConditions { get; set; }

        public bool IsShowInKPK { get; set; }
        public bool IsConsiderConnectionDate { get; set; }
        public string ActivityDescriptionTemplateInKPK { get; set; }


        #region Goals Config
        public bool DisableChangeSKU { get; set; }
        public bool DisableChangeKPI { get; set; }
        public bool DisableChangeType { get; set; }
        public int SplitByTime { get; set; }
        public int SplitByPOCType { get; set; }
        
        //new
        public bool IsEnableMoreOneAim { get; set; }

        // Mechanics info
        public List<int> DeletedMechanics { get; set; }
        public List<MechanicInfo> MechanicsInfo { get; set; }
        public bool IsPriceActivity { get; set; }
        public int MechanicsPriceType { get; set; }

        #endregion

        #region Goals
        public decimal AverageMACO { get; set; }
        public decimal ExpectedMarketTrend { get; set; }
        public decimal ExpectedUpLift { get; set; }
        public decimal OutletQuantity { get; set; }
        public decimal PlannedAddressesInclude { get; set; }
        public SqlXml TargetDimentionsTreeChanges { get; set; }
        #endregion

        #region Methods
        public bool Valid()
        {
            ErrorCode = 0;
            ErrorDescription = string.Empty;

            if (!(ActivityStart < ActivityEnd) || !(Period1Start < Period1End) || !(Period2Start < Period2End))
            {
                ErrorCode = 1;
                ErrorDescription = ((ErrorDescription.Length > 0) ? Environment.NewLine : string.Empty) + Resource.Common_ActivityPeriodEndCheck;
            }
            //BUG #22722. МП: закладка Общие настройки: помилка при спробі редагувати дати в існуючій акції яка ще не почалася
            //ЦА не может длится менее 7-и дней
            if (IsPriceActivity && ActivityEnd < ActivityStart.AddDays(8).Date)
            {
                ErrorCode = 1;
                ErrorDescription = ((ErrorDescription.Length > 0) ? Environment.NewLine : string.Empty) + Resource.Common_PriceActivityPeriodEndCheck;
            }
            if (!(ActivityStart > Period1End) || !(ActivityStart > Period2End))
            {
                ErrorCode = 1;
                ErrorDescription = ((ErrorDescription.Length > 0) ? Environment.NewLine : string.Empty) + Resource.Common_ActivityPeriodAfterBasePeriodCheck;
            }

            if (IsPriceActivity)
            {
                if (MechanicsInfo.Count == 1)
                {
                    List<MechanicInfo> mechanics = MechanicsInfo;
                    for (int i = 0; i < mechanics.Count; i++)
                    {
                        MechanicInfo mi = mechanics[i];
                        if (!mi.IsValid)
                        {
                            ErrorCode = 1;
                            ErrorDescription = ((ErrorDescription.Length > 0) ? Environment.NewLine : string.Empty) + Resource.Config_IncorrectMechnics;
                            break;
                        }
                    }
                }
                else
                    if (MechanicsInfo.Count > 1)
                    {
                        ErrorCode = 1;
                        ErrorDescription = ((ErrorDescription.Length > 0) ? Environment.NewLine : string.Empty) + Resource.Config_IncorrectOnlyOneMechnic;
                    }
                    else
                        if (MechanicsInfo.Count == 0)
                        {
                            ErrorCode = 1;
                            ErrorDescription = ((ErrorDescription.Length > 0) ? Environment.NewLine : string.Empty) + Resource.Config_IncorrectNoMevhanics;
                        }
            }
            return ErrorCode == 0;
        }
        #endregion

    }
}