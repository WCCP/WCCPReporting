﻿using System.Collections.Generic;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.MarketProgramms.Forms {
    public partial class AddPromoOutlets : XtraForm {
        private List<PromoOutletModel> _outlets;

        public AddPromoOutlets() {
            InitializeComponent();
        }

        public List<PromoOutletModel> DataSource {
            get { return _outlets; }
            set { 
                _outlets = value;
                gridControl.DataSource = _outlets;
            }
        }

        public List<PromoOutletModel> SelectedOutlets {
            get {
                List<PromoOutletModel> lList = new List<PromoOutletModel>();
                int[] lRowHandles = gridView.GetSelectedRows();
                for (int lI = 0; lI < gridView.SelectedRowsCount; lI++) {
                    int lIndex = gridView.GetDataSourceRowIndex(lRowHandles[lI]);
                    PromoOutletModel lOutlet = _outlets[lIndex];
                    lList.Add(lOutlet);
                }
                return lList;
            }
        }
    }
}
