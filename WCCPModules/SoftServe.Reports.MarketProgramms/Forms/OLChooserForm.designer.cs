namespace SoftServe.Reports.MarketProgramms.Forms
{
    partial class OLChooserForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.chkBoxSelectAll = new DevExpress.XtraEditors.CheckEdit();
            this.grList = new DevExpress.XtraEditors.GroupControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnSelect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumnRowNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOlId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStatusCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleOK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.grList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // chkBoxSelectAll
            // 
            this.chkBoxSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkBoxSelectAll.EditValue = null;
            this.chkBoxSelectAll.Enabled = false;
            this.chkBoxSelectAll.Location = new System.Drawing.Point(10, 278);
            this.chkBoxSelectAll.Name = "chkBoxSelectAll";
            this.chkBoxSelectAll.Properties.AllowGrayed = true;
            this.chkBoxSelectAll.Properties.Caption = "������� ���";
            this.chkBoxSelectAll.Size = new System.Drawing.Size(106, 19);
            this.chkBoxSelectAll.TabIndex = 12;
            this.chkBoxSelectAll.CheckedChanged += new System.EventHandler(this.chkBoxSelectAll_CheckedChanged);
            // 
            // grList
            // 
            this.grList.Controls.Add(this.gridControl);
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Location = new System.Drawing.Point(10, 10);
            this.grList.Name = "grList";
            this.grList.Size = new System.Drawing.Size(608, 262);
            this.grList.TabIndex = 13;
            this.grList.Text = "������:";
            // 
            // gridControl
            // 
            this.gridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(2, 21);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl.Size = new System.Drawing.Size(604, 239);
            this.gridControl.TabIndex = 3;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSelect,
            this.gridColumnRowNum,
            this.gridColumnOlId,
            this.gridColumnStatusCode,
            this.gridColumnStatus});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumnRowNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView_CustomDrawCell);
            this.gridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            this.gridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanging);
            // 
            // gridColumnSelect
            // 
            this.gridColumnSelect.Caption = " ";
            this.gridColumnSelect.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnSelect.FieldName = "olSelected";
            this.gridColumnSelect.MaxWidth = 20;
            this.gridColumnSelect.Name = "gridColumnSelect";
            this.gridColumnSelect.Visible = true;
            this.gridColumnSelect.VisibleIndex = 0;
            this.gridColumnSelect.Width = 20;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridColumnRowNum
            // 
            this.gridColumnRowNum.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnRowNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnRowNum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnRowNum.Caption = "����� ������";
            this.gridColumnRowNum.FieldName = "RowNumber";
            this.gridColumnRowNum.MaxWidth = 90;
            this.gridColumnRowNum.Name = "gridColumnRowNum";
            this.gridColumnRowNum.OptionsColumn.AllowEdit = false;
            this.gridColumnRowNum.OptionsColumn.AllowMove = false;
            this.gridColumnRowNum.OptionsColumn.ReadOnly = true;
            this.gridColumnRowNum.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnRowNum.Visible = true;
            this.gridColumnRowNum.VisibleIndex = 1;
            this.gridColumnRowNum.Width = 81;
            // 
            // gridColumnOlId
            // 
            this.gridColumnOlId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOlId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOlId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnOlId.Caption = "��� ��";
            this.gridColumnOlId.FieldName = "��� ��";
            this.gridColumnOlId.MaxWidth = 90;
            this.gridColumnOlId.Name = "gridColumnOlId";
            this.gridColumnOlId.OptionsColumn.AllowEdit = false;
            this.gridColumnOlId.OptionsColumn.AllowMove = false;
            this.gridColumnOlId.OptionsColumn.ReadOnly = true;
            this.gridColumnOlId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnOlId.Visible = true;
            this.gridColumnOlId.VisibleIndex = 2;
            this.gridColumnOlId.Width = 81;
            // 
            // gridColumnStatusCode
            // 
            this.gridColumnStatusCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnStatusCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnStatusCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnStatusCode.Caption = "������";
            this.gridColumnStatusCode.FieldName = "OL_STATUSDESCR";
            this.gridColumnStatusCode.Name = "gridColumnStatusCode";
            this.gridColumnStatusCode.OptionsColumn.AllowEdit = false;
            this.gridColumnStatusCode.OptionsColumn.AllowMove = false;
            this.gridColumnStatusCode.OptionsColumn.ReadOnly = true;
            this.gridColumnStatusCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnStatusCode.Visible = true;
            this.gridColumnStatusCode.VisibleIndex = 3;
            this.gridColumnStatusCode.Width = 81;
            // 
            // gridColumnStatus
            // 
            this.gridColumnStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnStatus.Caption = "OL_STATUS";
            this.gridColumnStatus.FieldName = "OL_STATUS";
            this.gridColumnStatus.Name = "gridColumnStatus";
            this.gridColumnStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // simpleOK
            // 
            this.simpleOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleOK.Location = new System.Drawing.Point(308, 290);
            this.simpleOK.Name = "simpleOK";
            this.simpleOK.Size = new System.Drawing.Size(210, 23);
            this.simpleOK.TabIndex = 14;
            this.simpleOK.Text = "�������� ���������� ���� � �������";
            this.simpleOK.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleCancel
            // 
            this.simpleCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleCancel.Location = new System.Drawing.Point(541, 290);
            this.simpleCancel.Name = "simpleCancel";
            this.simpleCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleCancel.TabIndex = 15;
            this.simpleCancel.Text = "������";
            this.simpleCancel.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // OLChooserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 332);
            this.Controls.Add(this.simpleCancel);
            this.Controls.Add(this.simpleOK);
            this.Controls.Add(this.grList);
            this.Controls.Add(this.chkBoxSelectAll);
            this.MinimumSize = new System.Drawing.Size(644, 370);
            this.Name = "OLChooserForm";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 60);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������� �����";
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.grList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.CheckEdit chkBoxSelectAll;
    private DevExpress.XtraEditors.GroupControl grList;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnSelect;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnRowNum;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnOlId;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatusCode;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatus;
    private DevExpress.XtraEditors.SimpleButton simpleOK;
    private DevExpress.XtraEditors.SimpleButton simpleCancel;



  }
}