using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;

namespace SoftServe.Reports.MarketProgramms.Forms
{
  public partial class TemplateSelect : XtraForm
  {
    public TemplateSelect()
    {
      InitializeComponent();
      WaitManager.StartWait();
      gridControl.DataSource = MarketProgramms.DataProvider.TemplatesList;
      WaitManager.StopWait();
    }
    public int SelectedTemplateId {
      get { return Convert.ToInt32(gridView.GetFocusedRowCellValue(gridColumnID)); }
    }

    private void gridControl_DoubleClick(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
    }
  }
}