using DevExpress.XtraEditors;

namespace SoftServe.Reports.MarketProgramms.Forms
{
    public partial class IncorrectPeriodsForm : XtraForm
    {
        public IncorrectPeriodsForm()
        {
            InitializeComponent();
        }

        public IncorrectPeriodsForm(ConflictingPeriodsList records)
        {
            InitializeComponent();
            
            gridControl.DataSource = records.Records;
            grList.Text = string.Format("�������� ��������� ��� {0} ��:", records.Records.Count);
        }

        private void simpleOK_Click(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}