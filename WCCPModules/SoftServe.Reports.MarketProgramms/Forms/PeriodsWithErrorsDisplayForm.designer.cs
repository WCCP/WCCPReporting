namespace SoftServe.Reports.MarketProgramms.Forms
{
    partial class PeriodsWithErrorsDisplayForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridViewPeriods = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionNameDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateFromDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateToDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.plansGridView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.bandFileParams = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colRowNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDecription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandData = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOutletId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDateFrom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDateTo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDiscountPercents = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDiscountSum = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gcStatus = new DevExpress.XtraEditors.GroupControl();
            this.simpleOK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPeriods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plansGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).BeginInit();
            this.gcStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridViewPeriods
            // 
            this.gridViewPeriods.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionNameDetails,
            this.colDateFromDetails,
            this.colDateToDetails});
            this.gridViewPeriods.GridControl = this.gridControl;
            this.gridViewPeriods.Name = "gridViewPeriods";
            this.gridViewPeriods.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPeriods.OptionsView.ColumnAutoWidth = false;
            this.gridViewPeriods.OptionsView.ShowGroupPanel = false;
            // 
            // colActionNameDetails
            // 
            this.colActionNameDetails.Caption = "�������� �����";
            this.colActionNameDetails.FieldName = "ActionName";
            this.colActionNameDetails.Name = "colActionNameDetails";
            this.colActionNameDetails.Visible = true;
            this.colActionNameDetails.VisibleIndex = 0;
            this.colActionNameDetails.Width = 106;
            // 
            // colDateFromDetails
            // 
            this.colDateFromDetails.Caption = " ���� �";
            this.colDateFromDetails.FieldName = "DateFrom";
            this.colDateFromDetails.Name = "colDateFromDetails";
            this.colDateFromDetails.Visible = true;
            this.colDateFromDetails.VisibleIndex = 1;
            this.colDateFromDetails.Width = 124;
            // 
            // colDateToDetails
            // 
            this.colDateToDetails.Caption = "���� ��";
            this.colDateToDetails.FieldName = "DateTo";
            this.colDateToDetails.Name = "colDateToDetails";
            this.colDateToDetails.Visible = true;
            this.colDateToDetails.VisibleIndex = 2;
            this.colDateToDetails.Width = 104;
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridViewPeriods;
            gridLevelNode1.RelationName = "PeriodList";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl.Location = new System.Drawing.Point(2, 22);
            this.gridControl.MainView = this.plansGridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(1057, 330);
            this.gridControl.TabIndex = 3;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.plansGridView,
            this.gridViewPeriods});
            // 
            // plansGridView
            // 
            this.plansGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.plansGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.plansGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.plansGridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandFileParams,
            this.bandData});
            this.plansGridView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colRowNumber,
            this.colDecription,
            this.colOutletId,
            this.colDateFrom,
            this.colDateTo,
            this.colDiscountSum,
            this.colDiscountPercents});
            this.plansGridView.CustomizationFormBounds = new System.Drawing.Rectangle(1688, 742, 223, 215);
            this.plansGridView.GridControl = this.gridControl;
            this.plansGridView.Name = "plansGridView";
            this.plansGridView.OptionsBehavior.Editable = false;
            this.plansGridView.OptionsDetail.ShowDetailTabs = false;
            this.plansGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.plansGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.plansGridView.OptionsSelection.MultiSelect = true;
            this.plansGridView.OptionsView.ColumnAutoWidth = false;
            this.plansGridView.OptionsView.ShowAutoFilterRow = true;
            this.plansGridView.OptionsView.ShowGroupPanel = false;
            this.plansGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRowNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // bandFileParams
            // 
            this.bandFileParams.AppearanceHeader.Options.UseTextOptions = true;
            this.bandFileParams.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandFileParams.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandFileParams.Caption = "��������������";
            this.bandFileParams.Columns.Add(this.colRowNumber);
            this.bandFileParams.Columns.Add(this.colDecription);
            this.bandFileParams.MinWidth = 20;
            this.bandFileParams.Name = "bandFileParams";
            this.bandFileParams.Width = 490;
            // 
            // colRowNumber
            // 
            this.colRowNumber.Caption = "������";
            this.colRowNumber.FieldName = "RowNumber";
            this.colRowNumber.Name = "colRowNumber";
            this.colRowNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRowNumber.Visible = true;
            this.colRowNumber.Width = 128;
            // 
            // colDecription
            // 
            this.colDecription.Caption = "�������";
            this.colDecription.FieldName = "StatusDescription";
            this.colDecription.Name = "colDecription";
            this.colDecription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDecription.Visible = true;
            this.colDecription.Width = 362;
            // 
            // bandData
            // 
            this.bandData.AppearanceHeader.Options.UseTextOptions = true;
            this.bandData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandData.Caption = "������ �� �����";
            this.bandData.Columns.Add(this.colOutletId);
            this.bandData.Columns.Add(this.colDateFrom);
            this.bandData.Columns.Add(this.colDateTo);
            this.bandData.Columns.Add(this.colDiscountPercents);
            this.bandData.Columns.Add(this.colDiscountSum);
            this.bandData.MinWidth = 20;
            this.bandData.Name = "bandData";
            this.bandData.Width = 540;
            // 
            // colOutletId
            // 
            this.colOutletId.Caption = "��� ��";
            this.colOutletId.FieldName = "OutletId";
            this.colOutletId.Name = "colOutletId";
            this.colOutletId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOutletId.Visible = true;
            this.colOutletId.Width = 124;
            // 
            // colDateFrom
            // 
            this.colDateFrom.Caption = "���� �";
            this.colDateFrom.FieldName = "DateFrom";
            this.colDateFrom.Name = "colDateFrom";
            this.colDateFrom.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDateFrom.Visible = true;
            this.colDateFrom.Width = 97;
            // 
            // colDateTo
            // 
            this.colDateTo.Caption = "���� ��";
            this.colDateTo.FieldName = "DateTo";
            this.colDateTo.Name = "colDateTo";
            this.colDateTo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDateTo.Visible = true;
            this.colDateTo.Width = 85;
            // 
            // colDiscountPercents
            // 
            this.colDiscountPercents.Caption = "������� ������, %";
            this.colDiscountPercents.FieldName = "DiscountPercent";
            this.colDiscountPercents.Name = "colDiscountPercents";
            this.colDiscountPercents.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDiscountPercents.Visible = true;
            this.colDiscountPercents.Width = 154;
            // 
            // colDiscountSum
            // 
            this.colDiscountSum.Caption = "����� ����� ������";
            this.colDiscountSum.FieldName = "DiscountSum";
            this.colDiscountSum.Name = "colDiscountSum";
            this.colDiscountSum.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDiscountSum.Visible = true;
            this.colDiscountSum.Width = 80;
            // 
            // gcStatus
            // 
            this.gcStatus.Controls.Add(this.gridControl);
            this.gcStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcStatus.Location = new System.Drawing.Point(10, 10);
            this.gcStatus.Name = "gcStatus";
            this.gcStatus.Size = new System.Drawing.Size(1061, 354);
            this.gcStatus.TabIndex = 13;
            this.gcStatus.Text = "������: ";
            // 
            // simpleOK
            // 
            this.simpleOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleOK.Location = new System.Drawing.Point(852, 373);
            this.simpleOK.Name = "simpleOK";
            this.simpleOK.Size = new System.Drawing.Size(136, 23);
            this.simpleOK.TabIndex = 14;
            this.simpleOK.Text = "��� ����� ���������";
            this.simpleOK.Click += new System.EventHandler(this.simpleOK_Click);
            // 
            // simpleCancel
            // 
            this.simpleCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleCancel.Location = new System.Drawing.Point(994, 373);
            this.simpleCancel.Name = "simpleCancel";
            this.simpleCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleCancel.TabIndex = 15;
            this.simpleCancel.Text = "������";
            this.simpleCancel.Click += new System.EventHandler(this.simpleCancel_Click);
            // 
            // PeriodsWithErrorsDisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 404);
            this.Controls.Add(this.simpleCancel);
            this.Controls.Add(this.simpleOK);
            this.Controls.Add(this.gcStatus);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(644, 370);
            this.Name = "PeriodsWithErrorsDisplayForm";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 40);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������� ������";
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPeriods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plansGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).EndInit();
            this.gcStatus.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl gcStatus;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraEditors.SimpleButton simpleOK;
    private DevExpress.XtraEditors.SimpleButton simpleCancel;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView plansGridView;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandFileParams;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRowNumber;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDecription;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandData;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOutletId;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDiscountPercents;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDateFrom;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDiscountSum;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDateTo;
    private DevExpress.XtraGrid.Views.Grid.GridView gridViewPeriods;
    private DevExpress.XtraGrid.Columns.GridColumn colActionNameDetails;
    private DevExpress.XtraGrid.Columns.GridColumn colDateFromDetails;
    private DevExpress.XtraGrid.Columns.GridColumn colDateToDetails;



  }
}