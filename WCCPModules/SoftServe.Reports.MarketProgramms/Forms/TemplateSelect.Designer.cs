namespace SoftServe.Reports.MarketProgramms.Forms
{
  partial class TemplateSelect
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
        this.btnSubmit = new DevExpress.XtraEditors.SimpleButton();
        this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
        this.gridControl = new DevExpress.XtraGrid.GridControl();
        this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.gridColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnTemplateType = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnTemplateName = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
        this.SuspendLayout();
        // 
        // btnCancel
        // 
        this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        this.btnCancel.Location = new System.Drawing.Point(286, 338);
        this.btnCancel.Name = "btnCancel";
        this.btnCancel.Size = new System.Drawing.Size(75, 23);
        this.btnCancel.TabIndex = 1;
        this.btnCancel.Text = "������";
        // 
        // btnSubmit
        // 
        this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.btnSubmit.DialogResult = System.Windows.Forms.DialogResult.OK;
        this.btnSubmit.Location = new System.Drawing.Point(148, 338);
        this.btnSubmit.Name = "btnSubmit";
        this.btnSubmit.Size = new System.Drawing.Size(122, 23);
        this.btnSubmit.TabIndex = 2;
        this.btnSubmit.Text = "������� ����������";
        // 
        // labelControl1
        // 
        this.labelControl1.Location = new System.Drawing.Point(12, 12);
        this.labelControl1.Name = "labelControl1";
        this.labelControl1.Size = new System.Drawing.Size(208, 13);
        this.labelControl1.TabIndex = 3;
        this.labelControl1.Text = "�������� ������ ��� ����� ����������";
        // 
        // gridControl
        // 
        this.gridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                    | System.Windows.Forms.AnchorStyles.Left)
                    | System.Windows.Forms.AnchorStyles.Right)));
        this.gridControl.Location = new System.Drawing.Point(12, 31);
        this.gridControl.MainView = this.gridView;
        this.gridControl.Name = "gridControl";
        this.gridControl.Size = new System.Drawing.Size(349, 301);
        this.gridControl.TabIndex = 4;
        this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView,
            this.gridView2});
        this.gridControl.DoubleClick += new System.EventHandler(this.gridControl_DoubleClick);
        // 
        // gridView
        // 
        this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnID,
            this.gridColumnTemplateType,
            this.gridColumnTemplateName});
        this.gridView.GridControl = this.gridControl;
        this.gridView.Name = "gridView";
        this.gridView.OptionsBehavior.Editable = false;
        this.gridView.OptionsView.ShowGroupPanel = false;
        this.gridView.OptionsView.ShowIndicator = false;
        // 
        // gridColumnID
        // 
        this.gridColumnID.Caption = "ID";
        this.gridColumnID.FieldName = "Action_ID";
        this.gridColumnID.Name = "gridColumnID";
        this.gridColumnID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        // 
        // gridColumnTemplateType
        // 
        this.gridColumnTemplateType.Caption = "��� �������";
        this.gridColumnTemplateType.FieldName = "ActionType_Name";
        this.gridColumnTemplateType.Name = "gridColumnTemplateType";
        this.gridColumnTemplateType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.gridColumnTemplateType.Visible = true;
        this.gridColumnTemplateType.VisibleIndex = 0;
        // 
        // gridColumnTemplateName
        // 
        this.gridColumnTemplateName.Caption = "��� �������";
        this.gridColumnTemplateName.FieldName = "Action_Name";
        this.gridColumnTemplateName.Name = "gridColumnTemplateName";
        this.gridColumnTemplateName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.gridColumnTemplateName.Visible = true;
        this.gridColumnTemplateName.VisibleIndex = 1;
        // 
        // gridView2
        // 
        this.gridView2.GridControl = this.gridControl;
        this.gridView2.Name = "gridView2";
        // 
        // TemplateSelect
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(373, 373);
        this.Controls.Add(this.gridControl);
        this.Controls.Add(this.labelControl1);
        this.Controls.Add(this.btnSubmit);
        this.Controls.Add(this.btnCancel);
        this.MinimumSize = new System.Drawing.Size(245, 191);
        this.Name = "TemplateSelect";
        this.ShowIcon = false;
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "�������� ����� ����������";
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private DevExpress.XtraEditors.SimpleButton btnCancel;
    private DevExpress.XtraEditors.SimpleButton btnSubmit;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnID;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnTemplateType;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnTemplateName;
  }
}