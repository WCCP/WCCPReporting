﻿using System;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using SoftServe.Reports.MarketProgramms;

namespace SoftServe.Reports.MarketProgramms
{
    public class FilterBaseForm : XtraForm
    {
        private FilterFormType filterType;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterBaseForm"/> class.
        /// </summary>
        public FilterBaseForm()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterBaseForm"/> class.
        /// </summary>
        /// <param name="filterType">Type of the filter.</param>
        public FilterBaseForm(FilterFormType filterType)
        {
            this.filterType = filterType;
        }

        /// <summary>
        /// Gets the type of the filter.
        /// </summary>
        /// <value>The type of the filter.</value>
        public FilterFormType FilterType
        {
            get
            {
                return filterType;
            }
        }

        /// <summary>
        /// Gets the poc id list.
        /// </summary>
        /// <value>The poc id list.</value>
        public virtual string POCList
        {
            set
            { 
            }
            get
            {

                /*
                  1006700660
                    111116038
                    1009007221

                 */
                return null;
            }
        }
    }
}
