using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SoftServe.Reports.MarketProgramms.Utility;

namespace SoftServe.Reports.MarketProgramms.Forms
{
    public partial class PeriodsWithErrorsDisplayForm : XtraForm
    {

        /// <summary>
        /// Create the new instance of PlansWithErrorsDisplayForm object
        /// </summary>
        public PeriodsWithErrorsDisplayForm()
        {
            InitializeComponent();
        }

        public void LoadData(List<ImportedPeriodWithErrorsModel> periods)
        {
            ChangeDateFormat(periods);

            gridControl.DataSource = periods;

            gcStatus.Text = string.Format("������: ���������� ��������� ��������� ������ ({0})", periods.Count);
        }

        private void ChangeDateFormat(List<ImportedPeriodWithErrorsModel> periods)
        {
            foreach (ImportedPeriodWithErrorsModel period in periods)
            {
               period.DateFrom = ChangeDataFormat(period.DateFrom);
               period.DateTo = ChangeDataFormat(period.DateTo);
            }
        }

        /// <summary>
        /// Finishes the dialog with OK result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }

        /// <summary>
        /// Finishes the dialog with Cancel result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private string ChangeDataFormat(object dateObject)
        {
            string lDate = string.Empty;

            try
            {
                DateTime lRowDate = Convert.ToDateTime(dateObject);
                lDate = lRowDate.Date.ToString("dd.MM.yyyy");
            }
            catch (Exception)
            {
                lDate = dateObject == DBNull.Value || dateObject == null ? string.Empty : dateObject.ToString();
            }

            return lDate;
        }
    }
}