﻿namespace SoftServe.Reports.MarketProgramms
{
    partial class FilterRegionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.groupRegion = new DevExpress.XtraEditors.GroupControl();
            this.groupStaff = new DevExpress.XtraEditors.GroupControl();
            this.groupPOCList = new DevExpress.XtraEditors.GroupControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRegion)).BeginInit();
            this.groupRegion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupStaff)).BeginInit();
            this.groupStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).BeginInit();
            this.groupPOCList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(422, 433);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            // 
            // treeList1
            // 
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList1.Location = new System.Drawing.Point(3, 19);
            this.treeList1.Name = "treeList1";
            this.treeList1.Size = new System.Drawing.Size(223, 172);
            this.treeList1.TabIndex = 1;
            // 
            // treeList2
            // 
            this.treeList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList2.Location = new System.Drawing.Point(3, 19);
            this.treeList2.Name = "treeList2";
            this.treeList2.Size = new System.Drawing.Size(244, 172);
            this.treeList2.TabIndex = 1;
            // 
            // groupRegion
            // 
            this.groupRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupRegion.Controls.Add(this.treeList1);
            this.groupRegion.Location = new System.Drawing.Point(12, 12);
            this.groupRegion.Name = "groupRegion";
            this.groupRegion.Size = new System.Drawing.Size(229, 194);
            this.groupRegion.TabIndex = 3;
            this.groupRegion.Text = "Территория:";
            // 
            // groupStaff
            // 
            this.groupStaff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupStaff.Controls.Add(this.treeList2);
            this.groupStaff.Location = new System.Drawing.Point(247, 12);
            this.groupStaff.Name = "groupStaff";
            this.groupStaff.Size = new System.Drawing.Size(250, 194);
            this.groupStaff.TabIndex = 4;
            this.groupStaff.Text = "Персонал:";
            // 
            // groupPOCList
            // 
            this.groupPOCList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPOCList.Controls.Add(this.gridControl);
            this.groupPOCList.Location = new System.Drawing.Point(12, 212);
            this.groupPOCList.Name = "groupPOCList";
            this.groupPOCList.Size = new System.Drawing.Size(485, 215);
            this.groupPOCList.TabIndex = 4;
            this.groupPOCList.Text = "Список ТТ:";
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(3, 19);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp});
            this.gridControl.Size = new System.Drawing.Size(479, 193);
            this.gridControl.TabIndex = 2;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.LightSalmon;
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "Регион";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "ТМК";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Юр. имя";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "Факт. имя";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.Caption = "Код ТТ";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.Caption = "Нас. пункт";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowMove = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "Status")});
            this.repositoryStatusLookUp.DisplayMember = "StatusName";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(341, 433);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "Вставить";
            // 
            // FilterRegionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(507, 467);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupPOCList);
            this.Controls.Add(this.groupStaff);
            this.Controls.Add(this.groupRegion);
            this.Controls.Add(this.btnCancel);
            this.MinimizeBox = false;
            this.Name = "FilterRegionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вставка ТТ";
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRegion)).EndInit();
            this.groupRegion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupStaff)).EndInit();
            this.groupStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).EndInit();
            this.groupPOCList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraEditors.GroupControl groupRegion;
        private DevExpress.XtraEditors.GroupControl groupStaff;
        private DevExpress.XtraEditors.GroupControl groupPOCList;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
        private DevExpress.XtraEditors.SimpleButton btnOk;
    }
}