﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using SoftServe.Reports.MarketProgramms;

namespace SoftServe.Reports.MarketProgramms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FilterRegionForm : FilterBaseForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterRegionForm"/> class.
        /// </summary>
        public FilterRegionForm()
            : base(FilterFormType.Region)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the poc id list.
        /// </summary>
        /// <value>The poc id list.</value>
        public override string POCList
        {
            set
            {
            }
            get
            {
                return "1006700660@1,1006700660@2,1006700660@3,6038@2,1009007221@2";
            }
        }
    }
}
