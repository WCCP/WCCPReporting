namespace SoftServe.Reports.MarketProgramms.Forms
{
    partial class ListMapping
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.mappingControl = new Logica.Reports.BaseReportControl.CommonControls.MappingControl.MappingControl();
        this.SuspendLayout();
        // 
        // mappingControl
        // 
        this.mappingControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.mappingControl.FileFieldsCaption = "���� �� �����:";
        this.mappingControl.Location = new System.Drawing.Point(10, 10);
        this.mappingControl.Name = "mappingControl";
        this.mappingControl.ReportFieldsCaption = "����:";
        this.mappingControl.Size = new System.Drawing.Size(608, 275);
        this.mappingControl.TabIndex = 0;
        this.mappingControl.OKBtnClick += new System.EventHandler(this.mappingControl_OKBtnClick);
        // 
        // ListMapping
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(628, 295);
        this.Controls.Add(this.mappingControl);
        this.MinimumSize = new System.Drawing.Size(644, 333);
        this.Name = "ListMapping";
        this.Padding = new System.Windows.Forms.Padding(10);
        this.ShowIcon = false;
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "���������� �����";
        this.ResumeLayout(false);

    }

    #endregion

    private Logica.Reports.BaseReportControl.CommonControls.MappingControl.MappingControl mappingControl;


  }
}