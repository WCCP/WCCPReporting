using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonControls.MappingControl;

namespace SoftServe.Reports.MarketProgramms.Forms
{
    /// <summary>
    /// Describes mapping form
    /// </summary>
    public partial class ListMapping : XtraForm
    {
        /// <summary>
        /// Initializes the new instance of ListMapping object
        /// </summary>
        public ListMapping()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the new instance of ListMapping object
        /// </summary>
        /// <param name="reportList">List of elements to be mapped from report.</param>
        /// <param name="fileList">List of elements to be mapped from file.</param>
        public ListMapping(List<ComboBoxItem> reportList, List<ComboBoxItem> fileList)
        {
            InitializeComponent();

            mappingControl.LoadData(reportList, fileList);
        }

        /// <summary>
        /// Gets mapping result array
        /// </summary>
        public int[] Mapping
        {
            get { return mappingControl.Mapping; }
        }

        /// <summary>
        /// Finished the Dialog with OK value
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event args.</param>
        private void mappingControl_OKBtnClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}