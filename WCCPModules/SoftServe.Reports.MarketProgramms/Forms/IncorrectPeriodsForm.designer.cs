namespace SoftServe.Reports.MarketProgramms.Forms
{
    partial class IncorrectPeriodsForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridViewDetails = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOlId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.grList = new DevExpress.XtraEditors.GroupControl();
            this.simpleOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.grList.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridViewDetails
            // 
            this.gridViewDetails.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionName,
            this.colDateFrom,
            this.colDateTo});
            this.gridViewDetails.GridControl = this.gridControl;
            this.gridViewDetails.Name = "gridViewDetails";
            this.gridViewDetails.OptionsBehavior.Editable = false;
            this.gridViewDetails.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewDetails.OptionsCustomization.AllowFilter = false;
            this.gridViewDetails.OptionsCustomization.AllowGroup = false;
            this.gridViewDetails.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewDetails.OptionsDetail.ShowDetailTabs = false;
            this.gridViewDetails.OptionsMenu.EnableColumnMenu = false;
            this.gridViewDetails.OptionsView.ColumnAutoWidth = false;
            this.gridViewDetails.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDetails.OptionsView.ShowGroupPanel = false;
            // 
            // colActionName
            // 
            this.colActionName.Caption = "�������� �����";
            this.colActionName.FieldName = "ActionName";
            this.colActionName.Name = "colActionName";
            this.colActionName.Visible = true;
            this.colActionName.VisibleIndex = 0;
            this.colActionName.Width = 194;
            // 
            // colDateFrom
            // 
            this.colDateFrom.Caption = "���� �";
            this.colDateFrom.DisplayFormat.FormatString = "d";
            this.colDateFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateFrom.FieldName = "DateFrom";
            this.colDateFrom.Name = "colDateFrom";
            this.colDateFrom.Visible = true;
            this.colDateFrom.VisibleIndex = 1;
            this.colDateFrom.Width = 80;
            // 
            // colDateTo
            // 
            this.colDateTo.Caption = "���� ��";
            this.colDateTo.DisplayFormat.FormatString = "d";
            this.colDateTo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateTo.FieldName = "DateTo";
            this.colDateTo.Name = "colDateTo";
            this.colDateTo.Visible = true;
            this.colDateTo.VisibleIndex = 2;
            this.colDateTo.Width = 80;
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.LevelTemplate = this.gridViewDetails;
            gridLevelNode2.RelationName = "PeriodList";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl.Location = new System.Drawing.Point(2, 22);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl.Size = new System.Drawing.Size(604, 340);
            this.gridControl.TabIndex = 3;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView,
            this.gridViewDetails});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOlId,
            this.colReason});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowColumnMoving = false;
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView.OptionsDetail.ShowDetailTabs = false;
            this.gridView.OptionsMenu.EnableColumnMenu = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            // 
            // colOlId
            // 
            this.colOlId.AppearanceHeader.Options.UseTextOptions = true;
            this.colOlId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOlId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOlId.Caption = "��� ��";
            this.colOlId.FieldName = "OutletId";
            this.colOlId.MaxWidth = 90;
            this.colOlId.Name = "colOlId";
            this.colOlId.OptionsColumn.AllowEdit = false;
            this.colOlId.OptionsColumn.AllowMove = false;
            this.colOlId.OptionsColumn.ReadOnly = true;
            this.colOlId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOlId.Visible = true;
            this.colOlId.VisibleIndex = 0;
            this.colOlId.Width = 81;
            // 
            // colReason
            // 
            this.colReason.Caption = "��������������";
            this.colReason.FieldName = "Reason";
            this.colReason.Name = "colReason";
            this.colReason.Visible = true;
            this.colReason.VisibleIndex = 1;
            this.colReason.Width = 236;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // grList
            // 
            this.grList.Controls.Add(this.gridControl);
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Location = new System.Drawing.Point(10, 10);
            this.grList.Name = "grList";
            this.grList.Size = new System.Drawing.Size(608, 364);
            this.grList.TabIndex = 13;
            this.grList.Text = "������:";
            // 
            // simpleOK
            // 
            this.simpleOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleOK.Location = new System.Drawing.Point(524, 394);
            this.simpleOK.Name = "simpleOK";
            this.simpleOK.Size = new System.Drawing.Size(91, 23);
            this.simpleOK.TabIndex = 14;
            this.simpleOK.Text = "OK";
            this.simpleOK.Click += new System.EventHandler(this.simpleOK_Click);
            // 
            // IncorrectPeriodsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 434);
            this.Controls.Add(this.simpleOK);
            this.Controls.Add(this.grList);
            this.MinimumSize = new System.Drawing.Size(644, 370);
            this.Name = "IncorrectPeriodsForm";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 60);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������������";
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.grList.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl grList;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private DevExpress.XtraGrid.Columns.GridColumn colOlId;
    private DevExpress.XtraEditors.SimpleButton simpleOK;
    private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetails;
    private DevExpress.XtraGrid.Columns.GridColumn colActionName;
    private DevExpress.XtraGrid.Columns.GridColumn colDateFrom;
    private DevExpress.XtraGrid.Columns.GridColumn colDateTo;
    private DevExpress.XtraGrid.Columns.GridColumn colReason;



  }
}