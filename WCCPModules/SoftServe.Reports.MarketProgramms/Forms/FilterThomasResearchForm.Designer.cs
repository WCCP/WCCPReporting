﻿namespace SoftServe.Reports.MarketProgramms
{
    partial class FilterThomasResearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupFilter = new DevExpress.XtraEditors.GroupControl();
            this.radioGroupOwner = new DevExpress.XtraEditors.RadioGroup();
            this.groupPOCList = new DevExpress.XtraEditors.GroupControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.groupFilterCriteria = new DevExpress.XtraEditors.GroupControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupFilter)).BeginInit();
            this.groupFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupOwner.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).BeginInit();
            this.groupPOCList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupFilterCriteria)).BeginInit();
            this.groupFilterCriteria.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(570, 467);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            // 
            // groupFilter
            // 
            this.groupFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupFilter.Controls.Add(this.radioGroupOwner);
            this.groupFilter.Location = new System.Drawing.Point(12, 12);
            this.groupFilter.Name = "groupFilter";
            this.groupFilter.Size = new System.Drawing.Size(633, 60);
            this.groupFilter.TabIndex = 3;
            this.groupFilter.Text = "Территория:";
            // 
            // radioGroupOwner
            // 
            this.radioGroupOwner.EditValue = 0;
            this.radioGroupOwner.Location = new System.Drawing.Point(6, 22);
            this.radioGroupOwner.Name = "radioGroupOwner";
            this.radioGroupOwner.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.radioGroupOwner.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupOwner.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroupOwner.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Все"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Свои")});
            this.radioGroupOwner.Size = new System.Drawing.Size(103, 29);
            this.radioGroupOwner.TabIndex = 5;
            this.radioGroupOwner.SelectedIndexChanged += new System.EventHandler(this.radioGroupOwner_SelectedIndexChanged);
            // 
            // groupPOCList
            // 
            this.groupPOCList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPOCList.Controls.Add(this.gridControl);
            this.groupPOCList.Location = new System.Drawing.Point(12, 78);
            this.groupPOCList.Name = "groupPOCList";
            this.groupPOCList.Size = new System.Drawing.Size(633, 296);
            this.groupPOCList.TabIndex = 4;
            this.groupPOCList.Text = "Список ТТ:";
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(3, 19);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp});
            this.gridControl.Size = new System.Drawing.Size(627, 274);
            this.gridControl.TabIndex = 2;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            styleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.LightSalmon;
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "Наименование";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Пользователь";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Дата создания";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "Критерии фильтрации";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "Status")});
            this.repositoryStatusLookUp.DisplayMember = "StatusName";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(3, 19);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(627, 59);
            this.memoEdit1.TabIndex = 5;
            // 
            // groupFilterCriteria
            // 
            this.groupFilterCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupFilterCriteria.Controls.Add(this.memoEdit1);
            this.groupFilterCriteria.Location = new System.Drawing.Point(12, 380);
            this.groupFilterCriteria.Name = "groupFilterCriteria";
            this.groupFilterCriteria.Size = new System.Drawing.Size(633, 81);
            this.groupFilterCriteria.TabIndex = 6;
            this.groupFilterCriteria.Text = "Критерии фильтрации:";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(489, 467);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Вставить";
            // 
            // FilterThomasResearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(652, 501);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupFilterCriteria);
            this.Controls.Add(this.groupPOCList);
            this.Controls.Add(this.groupFilter);
            this.Controls.Add(this.btnCancel);
            this.MinimizeBox = false;
            this.Name = "FilterThomasResearchForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вставка ТТ по фильтру Thomas Research";
            ((System.ComponentModel.ISupportInitialize)(this.groupFilter)).EndInit();
            this.groupFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupOwner.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).EndInit();
            this.groupPOCList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupFilterCriteria)).EndInit();
            this.groupFilterCriteria.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.GroupControl groupFilter;
        private DevExpress.XtraEditors.GroupControl groupPOCList;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
        private DevExpress.XtraEditors.RadioGroup radioGroupOwner;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.GroupControl groupFilterCriteria;
        private DevExpress.XtraEditors.SimpleButton btnOk;
    }
}