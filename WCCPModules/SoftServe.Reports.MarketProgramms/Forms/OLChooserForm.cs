using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;

namespace SoftServe.Reports.MarketProgramms.Forms
{
    public partial class OLChooserForm : XtraForm
    {
        private DataTable source = null;

        private bool refreshingChkBoxSelectAll = false;

        public event EventHandler POCListChanged;

        public OLChooserForm()
        {
            InitializeComponent();
        }

        public OLChooserForm(DataTable data)
        {
            InitializeComponent();

            this.source = data;

            if (data != null)
            {
                DataColumn columnSelect = new DataColumn(this.gridColumnSelect.FieldName, typeof(bool));
                columnSelect.DefaultValue = false;
                data.Columns.Add(columnSelect);
            }

            this.gridControl.DataSource = data;

            this.chkBoxSelectAll.CheckState = CheckState.Unchecked;
            SetSelectedToAll(true);
            this.chkBoxSelectAll.Enabled = this.chkBoxSelectAll.CheckState != CheckState.Unchecked;

            CalculateStatistics();
        }

        /// <summary>
        /// Gets the rows count.
        /// </summary>
        /// <value>The rows count.</value>
        public int RowsCount
        {
            get
            {
                DataTable data = this.gridControl.DataSource as DataTable;
                if (data == null) return 0;

                return data.Rows.Count;
            }
        }

        /// <summary>
        /// Gets the selected ol count.
        /// </summary>
        /// <value>The selected ol count.</value>
        public int SelectedOlCount
        {
            get
            {
                return (from DataRow row in GetDisplayedRows()
                        where ConvertEx.ToBool(row[this.gridColumnSelect.FieldName])
                        select row).Count();
            }
        }

        /// <summary>
        /// Gets the cant be added ol count.
        /// </summary>
        /// <value>The cant be added ol count.</value>
        public int CantBeAddedOlCount
        {
            get
            {
                return (from DataRow row in source.Rows
                        where !ConvertEx.ToBool(row["OL_STATUS"])
                        select row).Count();
            }
        }

        /// <summary>
        /// Gets the selected ols.
        /// </summary>
        /// <returns></returns>
        public string GetSelectedOls()
        {
            string ids = string.Empty;

            DataTable data = this.gridControl.DataSource as DataTable;

            if (data == null)
            {
                return ids;
            }

            foreach (DataRow row in GetDisplayedRows())
            {
                if (!ConvertEx.ToBool(row[this.gridColumnSelect.FieldName])
                    || !ConvertEx.ToBool(row["OL_STATUS"]))
                {
                    continue;
                }

                string id = row[this.gridColumnOlId.FieldName].ToString();

                if (!string.IsNullOrEmpty(ids))
                {
                    ids += ",";
                }

                ids += id;
            }

            return ids;
        }

        /// <summary>
        /// Handles the CellValueChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            CalculateStatistics();
        }

        /// <summary>
        /// Handles the CellValueChanging event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //CalculateStatistics();
        }

        private void chkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.refreshingChkBoxSelectAll)
            {
                this.SetSelectedToAll(this.chkBoxSelectAll.CheckState == CheckState.Checked);
            }
        }

        /// <summary>
        /// Sets the selected to all.
        /// </summary>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        private void SetSelectedToAll(bool selected)
        {
            DataTable dataTable = this.gridControl.DataSource as DataTable;
            if (dataTable == null) return;

            foreach (DataRow row in dataTable.Rows)
            {
                if (!selected || ConvertEx.ToBool(row["OL_STATUS"]))
                {
                    row[this.gridColumnSelect.FieldName] = selected;
                }
            }

            CalculateStatistics();
        }

        /// <summary>
        /// Processes the error POCs.
        /// </summary>
        private void CalculateStatistics()
        {
            grList.Text = string.Format("������: ����� ���������� ����� - {0}, ������ �������� - {1}, �������� - {2}",
                this.RowsCount, this.CantBeAddedOlCount, this.SelectedOlCount);

            RefreshChkBoxSelectAll();
        }

        private void RefreshChkBoxSelectAll()
        {
            refreshingChkBoxSelectAll = true;

            if (source.Rows.Count == 0 || this.SelectedOlCount == 0)
            {
                this.chkBoxSelectAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                this.chkBoxSelectAll.CheckState = this.SelectedOlCount == this.RowsCount - this.CantBeAddedOlCount ? CheckState.Checked : CheckState.Indeterminate;
            }

            refreshingChkBoxSelectAll = false;
        }

        private IList<DataRow> GetDisplayedRows()
        {
            IList<DataRow> rows = new List<DataRow>();

            if (this.gridView == null)
            {
                return rows;
            }

            for (int handle = 0; handle < this.gridView.DataRowCount; handle++)
            {
                rows.Add((DataRow)this.gridView.GetDataRow(handle));
            }

            return rows;
        }

        private void gridControl_DataSourceChanged(object sender, EventArgs e)
        {
            CalculateStatistics();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void gridView_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!ConvertEx.ToBool(gridView.GetRowCellValue(gridView.FocusedRowHandle, "OL_STATUS")))
            {
                e.Cancel = true;
            }
        }

        private void gridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column.FieldName == gridColumnSelect.FieldName)//e.Column.FieldName == gridColumnSelect.FieldName && !ConvertEx.ToBool(source.Rows[e.RowHandle][e.Column.FieldName])
            {
                e.Appearance.BorderColor = System.Drawing.Color.Green;
            }
        }
    }
}