﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Data.SqlTypes;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraGrid.Views.BandedGrid;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.MarketProgramms.Forms;
using System.Globalization;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraGrid;
using Logica.Reports.BaseReportControl.CommonControls.MappingControl;

namespace SoftServe.Reports.MarketProgramms.UserControls
{
    /// <summary>
    ///   Tab with Aims set to OLs in current activity
    /// </summary>
    [ToolboxItem(false)]
    public partial class ActivityDetailsGoals : CommonBaseControl
    {
        #region Constants
        
        // Band&column headers
        private string planPref = "План";
        private string targetValuePref = "Целевое значение"; //Gap 2015_5
        private string snapValuePref = "Срезанное значение";
        private string execTargetValuePref = "Выполнение по целевому значению, %";
        private string targetValuePrefPointer = "Целевое";
        private string snapValuePrefPointer = "Срезанное";
        private string execTargetValuePrefPointer = "ВыполнениеПоЦелевому";
        private string factPref = "Факт";
        private string resPref = "Выполнение";
        private string finResPref = "Итого";
        private string colAppr = "Утверждена";
        private string colIsPart = "Участвует";

        // Added columns
        private string colIsModif = "IsModified";
        private string colOLId = "Код ТТ";
        private string colMonth = "Месяц";
        private string colRowNumber = "RowNumber";
        private string colOLStatus = "OL_STATUS";
        private string colOlDescr = "OL_STATUSDESCR";
        private string colOlSelected = "olSelected";

        // for parsing headers from DB 
        private string dbMonthResPref = "ЯЯЯ";
        private string dbResPref = "ЯЯЯЯ";

        private char charDelimiter = '_';
        private string stringDelimiter = "_";

        // Format strings
        private string formatSalesVol = "{0:N3}";
        private string formatMerch = "{0:N2}%";
        private string formatIPTR = "{0:N0}";
        private string formatMonthRes = "{0:N0}";

        private GridBand merchAddPlanIndicator;

        #endregion

        #region Fields

        /// <summary>
        /// Activity's Id
        /// </summary>
        private int ActivityId { get; set; }

        /// <summary>
        /// Action start date
        /// </summary>
        private DateTime ActionStart { get; set; }

        /// <summary>
        /// Action end date
        /// </summary>
        private DateTime ActionEnd { get; set; }

        /// <summary>
        /// Contains edited plan rows
        /// </summary>
        DataTable changedValues = new DataTable();

        /// <summary>
        /// for reducing counts of DB reading during data loading 
        /// </summary>
        private bool isDateBeingSet = false;

        /// <summary>
        /// Determines whether current user can modify the tab
        /// </summary>
        private bool canModify = true;

        /// <summary>
        /// List of activity aims
        /// </summary>
        private List<DataTransferObjectAim> AimsOr = new List<DataTransferObjectAim>();

        /// <summary>
        /// Grid source
        /// </summary>
        private DataTable source;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "ActivityDetailsGoals" />
        ///   class.
        /// </summary>
        public ActivityDetailsGoals()
        {
            InitializeComponent();

            WasControlLoaded = false;
        }

        #endregion

        #region Instance Properties

        /// <summary>
        /// Main control
        /// </summary>
        public ActivityDetailsControl MainControl { get; set; }

        public GridControl  BandedGrid
        {
            get
            {
                return this.gridAims;
            }
        }
        #endregion

        #region Instance Methods

        /// <summary>
        ///   Loads the data.
        /// </summary>
        /// <param name = "activityId">The activity id.</param>
        /// <param name="activityDetails"></param>
        public void LoadData(int activityId, DataTable activityDetails)
        {
            ActivityId = activityId;

            ActionStart = MainControl.OriginalActivityDetails.ActivityStart;// ConvertEx.ToDateTime(activityDetails.Rows[0]["Action_Start"]);
            ActionEnd = MainControl.OriginalActivityDetails.ActivityEnd; //ConvertEx.ToDateTime(activityDetails.Rows[0]["Action_End"]);

            AimsOr = new List<DataTransferObjectAim>();
            LoadAllAimsFromDB(ActivityId);
            isDateBeingSet = true;
            SetPeriod();
            isDateBeingSet = false;

            DataTable dt = DataProvider.GetPlansFacts(ActivityId, dateStart.DateTime, dateEnd.DateTime, true);
            checkShowFact.Enabled = dt != null && dt.Rows.Count > 0;

            UpdateGrid(dt);
            changedValues = new DataTable();

            canModify = (MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.GoalsSetter) || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Initiator))
                        && MainControl.AccessChecker.PromoTTCount == MainControl.AccessChecker.PromoTTAvailable;

            if (!canModify)
            {
                simpleLoad.Enabled = false;
                bandedGridView1.OptionsBehavior.Editable = false;
            }

            WasControlLoaded = true;
        }

        /// <summary>
        /// Gets all data to be saved
        /// </summary>
        /// <param name="activityDetails"></param>
        public void GetAllFields(ActivityDetailsDTO activityDetails)
        { }

        /// <summary>
        /// Sets the dates
        /// </summary>
        private void SetPeriod()
        {
            dateStart.Properties.MinValue = ActionStart;
            dateEnd.Properties.MaxValue = ActionEnd;

            DateTime dateB = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime dateE = dateB.AddMonths(1).AddDays(-1);

            if (ActionStart > dateB)
            {
                dateB = ActionStart;
                dateE = dateB.AddMonths(1).AddDays(-dateB.Day);
            }

            if (ActionEnd < dateE)
            {
                dateE = ActionEnd;
                dateB = new DateTime(dateE.Year, dateE.Month, 1);
            }

            dateStart.DateTime = dateB;
            dateEnd.DateTime = dateE;
        }

        /// <summary>
        ///   Saves this instance.
        /// </summary>
        public void Save() {
            SaveChangedPlans();
        }

        /// <summary>
        /// Reads info from DB and Loads it into Aims liast
        /// </summary>
        private void LoadAllAimsFromDB(int activityId)
        {
            DataTable dt = DataProvider.GetAimList(activityId);

            DataTransferObjectAim aim = new DataTransferObjectAim();

            foreach (DataRow dr in dt.Rows)
            {
                aim = new DataTransferObjectAim();
                aim.Id = ConvertEx.ToInt(dr["Aim_Id"]);
                aim.Name = ConvertEx.ToString(dr["Aim_Name"]);
                aim.Type = ConvertEx.ToInt(dr["AimType_Id"]);

                aim.IsUsedInCalculations = ConvertEx.ToBool(dr["isUseInCompleted"]);
                aim.IsPeriodCalc = ConvertEx.ToBool(dr["isPeriodCalcAim"]);
                aim.IsInReverseExecution = ConvertEx.ToBool(dr["IsInReverseExec"]);

                aim.FactCalcWay = ConvertEx.ToInt(dr["FactCalcType"]);
                aim.FactCalcPeriod = ConvertEx.ToInt(dr["FactCalcPeriod"]);
                aim.IsUseAimValue = ConvertEx.ToBool(dr["isUseAimValue"]);
                aim.IsAccPerfOfTargetVal = ConvertEx.ToBool(dr["isAccPerfOfTargetVal"]);
                AimsOr.Add(aim);
            }
        }

        #endregion

        #region GridMethods

        /// <summary>
        /// Updates the grid according the new datasource
        /// </summary>
        /// <param name="dt">The new datasource</param>
        private void UpdateGrid(DataTable dt)
        {
            source = dt;

            gridAims.BeginUpdate();
            bandedGridView1.OptionsBehavior.Editable = true;

            gridAims.DataSource = source;
            DraWWWW(source, checkShowFact.Checked);

            gridAims.EndUpdate();

            if (source != null)
            {
                DataColumn status = source.Columns.Add(colIsModif, typeof(bool));
                status.DefaultValue = false;
            }

            if (!canModify)
            {
                bandedGridView1.OptionsBehavior.Editable = false;
            }
        }

        /// <summary>
        /// Redraws the grid according the new datasource
        /// </summary>
        /// <param name="dt">The new datasource</param>
        /// <param name="showFact">Whether show Fact</param>
        private void DraWWWW(DataTable dt, bool showFact)
        {
            while (bandedGridView1.Bands.Count > 1)
            {
                bandedGridView1.Bands.RemoveAt(1);
            }

            while (bandedGridView1.Columns.Count > 10)
            {
                bandedGridView1.Columns.RemoveAt(10);
            }

            if (dt == null || dt.Columns.Count <= 10)
            {
                return;
            }

            int index;
            string month;

            index = 9;
            month = GetMonthName(dt.Columns[index].ColumnName);

            while (month != dbResPref && index < dt.Columns.Count)
            {
                index = DrawMonthColumns(dt, month, index, showFact);
                if (index < dt.Columns.Count)
                {
                    month = GetMonthName(dt.Columns[index].ColumnName);
                }
            }

            DrawResultColumns(dt, index, showFact);

        }

        /// <summary>
        /// Draws one month
        /// </summary>
        /// <param name="dt">The datasource</param>
        /// <param name="month">Month name</param>
        /// <param name="index">Index of the datasource column</param>
        /// <param name="showFact">Whether show Fact</param>
        /// <returns>Index of the datasource column</returns>
        private int DrawMonthColumns(DataTable dt, string month, int index, bool showFact)
        {
            int i = index;

            string aimPref;

            string pref;
            GridBand monthBand;
            GridBand planBand;
            GridBand factBand;
            GridBand resBand;
            GridBand targetValueBand = new GridBand { Caption = targetValuePref };
            GridBand snapValueBand = new GridBand { Caption = snapValuePref };
            GridBand execTargetValueBand = new GridBand { Caption = execTargetValuePref };
    
            BandedGridColumn col;
            monthBand = new GridBand { Caption = month };
           
            pref = GetPlanFactPrefName(dt.Columns[i].ColumnName);

            planBand = new GridBand { Caption = planPref };
            factBand = new GridBand { Caption = factPref };

            string[] ar;
            string fieldName = string.Empty;

            // show plan and fact
            while (i < dt.Columns.Count && GetMonthName(dt.Columns[i].ColumnName) == month && GetPlanFactPrefName(dt.Columns[i].ColumnName) == planPref)
            {
                aimPref = GetAimName(dt.Columns[i].ColumnName);

                // plan
                fieldName = dt.Columns[i].ColumnName;
                col = AddColumn(fieldName, aimPref, 80, true/*string.Compare(GetYearMonth(monthBand.Caption), GetYearMonth(DateTime.Now)) >= 0*/);
                planBand.Columns.Add(col);

                //fact
                ar = fieldName.Split(charDelimiter);
                ar[1] = factPref;
                fieldName = string.Join(stringDelimiter, ar);

                col = AddColumn(fieldName, aimPref, 80, false);
                factBand.Columns.Add(col);

                i++;
            }

            i += planBand.Columns.Count;// jump over fact columns
            
            while(i< dt.Columns.Count && GetMonthName(dt.Columns[i].ColumnName) == month && GetPlanFactPrefName(dt.Columns[i].ColumnName) == targetValuePrefPointer)
            {
                aimPref = GetAimName(dt.Columns[i].ColumnName);
                fieldName = dt.Columns[i].ColumnName;
                col = AddColumn(fieldName, aimPref, 80, true);
                col.DisplayFormat.FormatString = "{0:N2}";
                targetValueBand.Columns.Add(col);
                i++;
            }

            while (i < dt.Columns.Count && GetMonthName(dt.Columns[i].ColumnName) == month && GetPlanFactPrefName(dt.Columns[i].ColumnName) == snapValuePrefPointer)
            {
                aimPref = GetAimName(dt.Columns[i].ColumnName);
                fieldName = dt.Columns[i].ColumnName;
                col = AddColumn(fieldName, aimPref, 80, false);
                col.DisplayFormat.FormatString = "{0:N2}";
                snapValueBand.Columns.Add(col);
                i++;
            }

            while (i < dt.Columns.Count && GetMonthName(dt.Columns[i].ColumnName) == month && GetPlanFactPrefName(dt.Columns[i].ColumnName) == execTargetValuePrefPointer)
            {
                aimPref = GetAimName(dt.Columns[i].ColumnName);
                fieldName = dt.Columns[i].ColumnName;
                col = AddColumn(fieldName, aimPref, 80, false);
                col.DisplayFormat.FormatString = "{0:N1}%";
                execTargetValueBand.Columns.Add(col);
                i++;
            }

            resBand = new GridBand { Caption = string.Empty };
            if (i < dt.Columns.Count && GetPlanFactPrefName(dt.Columns[i].ColumnName) == resPref)
            {
                fieldName = dt.Columns[i].ColumnName;
                col = AddColumn(fieldName, resPref, 30, false);
                resBand.Columns.Add(col);

                i++;

            }
           
            monthBand.Children.Add(planBand);
            
            if (targetValueBand.Columns.Count > 0)
            {
                monthBand.Children.Add(targetValueBand);
                merchAddPlanIndicator = targetValueBand;
            }

            if (snapValueBand.Columns.Count > 0)
            {
                monthBand.Children.Add(snapValueBand);
            }

            monthBand.Children.Add(factBand);
            
            if (execTargetValueBand.Columns.Count > 0)
            {
                monthBand.Children.Add(execTargetValueBand);
            }

            monthBand.Children.Add(resBand);
            bandedGridView1.Bands.Add(monthBand);

            factBand.Visible = showFact;

            return i;
        }

        /// <summary>
        /// Draws result columns
        /// </summary>
        /// <param name="dt">The datasource</param>
        /// <param name="index">Index of the datasource column</param>
        /// <param name="showFact">Whether show Fact</param>
        /// <returns>Index of the datasource column</returns>
        private int DrawResultColumns(DataTable dt, int index, bool showFact)
        {
            int i = index;

            BandedGridColumn col;

            string aimPref;

            GridBand planBand = new GridBand { Caption = planPref };
            GridBand factBand = new GridBand { Caption = factPref };
            GridBand resBand = new GridBand { Caption = finResPref };
           
            string[] ar;
            string fieldName = string.Empty;

            // show plan and fact
            while (i < dt.Columns.Count && GetPlanFactPrefName(dt.Columns[i].ColumnName) == planPref)
            {
                aimPref = GetAimName(dt.Columns[i].ColumnName);

                // plan
                fieldName = dt.Columns[i].ColumnName;
                col = AddColumn(fieldName, aimPref, 80, false);
                planBand.Columns.Add(col);
                
                //fact
                ar = fieldName.Split(charDelimiter);
                ar[2] = factPref;
                fieldName = string.Join(stringDelimiter, ar);

                col = AddColumn(fieldName, aimPref, 80, false);
                factBand.Columns.Add(col);

                i++;
            }

            i += planBand.Columns.Count;// jump over fact columns

            resBand.Children.Add(planBand);
            resBand.Children.Add(factBand);

            bandedGridView1.Bands.Add(resBand);
            
            factBand.Visible = showFact;

            return i;
        }

        /// <summary>
        /// Adds the column to GridViev
        /// </summary>
        /// <param name="name">Column name</param>
        /// <param name="caption">Column caption</param>
        /// <param name="width">Column width</param>
        /// <param name="allowEdit">is editable</param>
        /// <returns>Column</returns>
        private BandedGridColumn AddColumn(string name, string caption, int width, bool allowEdit)
        {
            BandedGridColumn col = bandedGridView1.Columns.AddField(name);

            col.Visible = true;
            col.Width = width;
            col.MinWidth = 30;
            col.OptionsColumn.AllowEdit = allowEdit;
            col.Caption = caption;

            SetColumnFormat(col);

            return col;
        }

        private void SetColumnFormat(BandedGridColumn col)
        {
            string name = col.FieldName;
            col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;

            if (name.Split(charDelimiter)[2] == dbMonthResPref)// exec
            {
                col.DisplayFormat.FormatString = formatMonthRes;
            }
            else
                if (GetMonthName(name) == dbResPref)// res
                {
                    col.DisplayFormat.FormatString = formatSalesVol;
                }
                else
                {
                    int aimType = AimsOr.Find(a => a.Name == GetAimName(name)).Type;

                    switch (aimType)
                    {
                        case 1:
                        case 6:
                        case 7: col.DisplayFormat.FormatString = formatSalesVol;
                            break;
                        case 2: col.DisplayFormat.FormatString = formatMerch;
                            break;
                        case 3: col.DisplayFormat.FormatString = formatIPTR;
                            break;
                        case 5:
                        case 8: col.DisplayFormat.FormatString = formatIPTR;
                            break;
                    }
                }
        }

        internal void PrepareToExport()
        {
            for (int i = 9; i < bandedGridView1.Columns.Count; i++)
            {
                bandedGridView1.Columns[i].DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
            }
        }

        internal void RestoreAfterExport()
        {
            for (int i = 9; i < bandedGridView1.Columns.Count; i++)
            {
                SetColumnFormat(bandedGridView1.Columns[i]);
            }
        }

        /// <summary>
        /// Handles the checkShowFact CheckedChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkShowFact_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridBand monthband in bandedGridView1.Bands)
            {
                foreach (GridBand band in monthband.Children)
                {
                    if (band.Caption == factPref)
                    {
                        band.Visible = checkShowFact.Checked;
                    }
                }
            }

            if (!canModify)
            {
                simpleLoad.Enabled = false;
            }
            else
            {
                simpleLoad.Enabled = !checkShowFact.Checked;
            }
        }
               
        /// <summary>
        /// Handles the bandedGridView1 CustomDrawColumnHeader event: draws vertical captions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bandedGridView1_CustomDrawColumnHeader(object sender, DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null)
                return;

            e.Appearance.BeginUpdate();

            e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            e.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            e.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;

            if (e.Column.Caption == colAppr)
            {
                e.Info.Caption = string.Empty;
                e.Painter.DrawObject(e.Info);
                e.Handled = true;
                e.Appearance.DrawVString(e.Cache, "Утверждена", e.Appearance.GetFont(), e.Appearance.GetForeBrush(e.Cache), e.Info.CaptionRect, e.Appearance.GetStringFormat(), 270);
            }
            else
                if (e.Column.Caption == colIsPart)
                {
                    e.Info.Caption = string.Empty;
                    e.Painter.DrawObject(e.Info);
                    e.Handled = true;
                    e.Appearance.DrawVString(e.Cache, "Участвует", e.Appearance.GetFont(), e.Appearance.GetForeBrush(e.Cache), e.Info.CaptionRect, e.Appearance.GetStringFormat(), 270);
                }
                else
                    if (e.Column.Caption == resPref)
                    {
                        e.Info.Caption = string.Empty;
                        e.Painter.DrawObject(e.Info);
                        e.Handled = true;
                        e.Appearance.DrawVString(e.Cache, resPref, e.Appearance.GetFont(), e.Appearance.GetForeBrush(e.Cache), e.Info.CaptionRect, e.Appearance.GetStringFormat(), 270);
                    }
                    else
                    {
                        e.Handled = false;
                    }

            e.Appearance.EndUpdate();
        }

        /// <summary>
        /// Gets aim name
        /// </summary>
        /// <param name="colName">Column name</param>
        /// <returns>Aim name</returns>
        private string GetAimName(string colName)
        {
            string name = colName.Remove(0, colName.IndexOf(charDelimiter) + 1);
            name = name.Remove(0, name.IndexOf(charDelimiter) + 1);

            if (colName.StartsWith(dbResPref))
            {
                name = name.Remove(0, name.IndexOf(charDelimiter) + 1);
            }

            return name;
        }

        private string GetAimName(string colName,bool isParam)
        {
            string targetPlanValName = colName.Remove(0, colName.IndexOf(charDelimiter));
            if (targetPlanValName.StartsWith(charDelimiter + targetValuePrefPointer + charDelimiter))
                return string.Empty;

            string name = colName.Remove(0, colName.IndexOf(charDelimiter) + 1);
            name = name.Remove(0, name.IndexOf(charDelimiter) + 1);

            if (colName.StartsWith(dbResPref))
            {
                name = name.Remove(0, name.IndexOf(charDelimiter) + 1);
            }

            return name;
        }

        /// <summary>
        /// Gets plan or fact pref name
        /// </summary>
        /// <param name="colName">Column name</param>
        /// <returns>Plan or fact pref name</returns>
        private string GetPlanFactPrefName(string colName)
        {
            string[] ar = colName.Split(charDelimiter);
            string name = string.Empty;
            if (ar.Length > 1)
            {
                name = ar[1];

                if (colName.StartsWith(dbResPref))
                {
                    name = colName.Split(charDelimiter)[2];
                }
            }

            return name;
        }

        /// <summary>
        /// Gets month name
        /// </summary>
        /// <param name="colName">Column name</param>
        /// <returns>Month name</returns>
        private string GetMonthName(string colName)
        {
            string name = colName.Split(charDelimiter)[0];

            return name;
        }

        /// <summary>
        /// Updates the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(dateStart))
            {
                dateEnd.Properties.MinValue = !string.IsNullOrEmpty(dateStart.Text) ? dateStart.DateTime : ActionStart;
            }
            if (sender.Equals(dateEnd))
            {
                dateStart.Properties.MaxValue = !string.IsNullOrEmpty(dateEnd.Text) ? dateEnd.DateTime : ActionEnd;
            }

            if (!isDateBeingSet)
            {
                WaitManager.StartWait();

                if (sender.Equals(dateStart))
                {
                    dateEnd.EditValueChanged -= dateEdit_EditValueChanged;

                    dateEnd.DateTime = dateEnd.DateTime.Month != dateStart.DateTime.Month ?
                        new DateTime(dateStart.DateTime.Year,dateStart.DateTime.Month,DateTime.DaysInMonth(dateStart.DateTime.Year,dateStart.DateTime.Month))
                        : dateEnd.DateTime;
                    dateEnd.EditValueChanged += dateEdit_EditValueChanged;
                }

                if (sender.Equals(dateEnd))
                {
                    dateStart.EditValueChanged -= dateEdit_EditValueChanged;

                    dateStart.DateTime = dateEnd.DateTime.Month != dateStart.DateTime.Month ?
                        new DateTime(dateEnd.DateTime.Year, dateEnd.DateTime.Month, 1)
                        : dateStart.DateTime;
                    dateStart.EditValueChanged += dateEdit_EditValueChanged;
                }

                DataTable dt2 = DataProvider.GetPlansFacts(ActivityId, dateStart.DateTime, dateEnd.DateTime, true);
                UpdateGrid(dt2);

                WaitManager.StopWait();
            }
        }
        #endregion

        #region PlansUploading

        private List<ComboBoxItem> GetActivityAims()
        {
            List<ComboBoxItem> Aims = new List<ComboBoxItem>();
            Aims.Add(new ComboBoxItem(-2, colOLId));
            Aims.Add(new ComboBoxItem(-1, colMonth));

            foreach (DataTransferObjectAim aim in AimsOr)
            {
                Aims.Add(new ComboBoxItem(aim.Id, aim.Name));
            }

            return Aims;
        }

        private void simpleLoad_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty;

            try
            {
                fileName = FileChosing();
            }
            catch (IOException ex)
            {
                MessageBox.Show("Файл открыт в другом приложении");
            }

            if (fileName != string.Empty)
            {
                List<ComboBoxItem> ActivityAims = GetActivityAims();

                if (bandedGridView1.Bands[1].Children.Contains(merchAddPlanIndicator))
                {
                    for (int i = 0; i < merchAddPlanIndicator.Columns.Count; i++)
                    {
                        ActivityAims.Add(new ComboBoxItem(merchAddPlanIndicator.Columns[i].Caption.GetHashCode(),
                            merchAddPlanIndicator.Caption + " " + merchAddPlanIndicator.Columns[i].Caption));
                    }
                }

                List<ComboBoxItem> FileAims = Utility.Utility.ReadFileHeader(fileName);

                if (FileAims == null)
                {
                    return;
                }

               if (FileAims.Count < ActivityAims.Count)
               {
                    MessageBox.Show("В файле недостаточно колонок");
               }

                int[] mapping = new int[0];
                using (ListMapping lm = new ListMapping(ActivityAims, FileAims))
                {
                    if (lm.ShowDialog() == DialogResult.OK)
                    {
                        mapping = lm.Mapping;

                        dateStart.Enabled = true;
                        dateEnd.Enabled = true;
                        checkShowFact.Enabled = true;

                        WaitManager.StartWait();

                        DataTable Plans = Utility.Utility.ReadFileBody(fileName, ActivityAims, FileAims, mapping, InitPlanTable(ActivityAims));

                        if (Plans.Rows.Count > 0)
                        {
                           DataView dv = Plans.DefaultView;
                           dv.Sort = colMonth;
                           Plans = dv.ToTable();
                        }  

                        //ValidatePlansDataEliminatePreviosPeriods(Plans);
                        ValidatePlansDataBiss(Plans);
                        ValidatePlansOls(Plans);
                        //ValidatePlansDataDates(Plans);

                        if (Plans.Rows.Count > 0)
                        {
                            DataView dv = Plans.DefaultView;
                            dv.Sort = colMonth;              // we need this sort for effective performance of this.LoadPlans function
                            Plans = dv.ToTable();
                        } 

                        WaitManager.StopWait();

                        using (OLChooserForm cf = new OLChooserForm(Plans))
                        {
                            if (cf.ShowDialog() == DialogResult.OK)
                            {
                                WaitManager.StartWait();

                                LoadPlans(Plans);

                                DataTable dt2 = DataProvider.GetPlansFacts(ActivityId, dateStart.DateTime, dateEnd.DateTime, true);
                                checkShowFact.Enabled = dt2 != null && dt2.Rows.Count > 0;
                                UpdateGrid(dt2);

                                WaitManager.StopWait();
                            }
                        }
                    }
                }
            }
        }

        private string FileChosing()
        {
            string fileName = string.Empty;

            string DIALOG_OPEN_FILE_FILTER = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            string DIALOG_OPEN_FILE = "Загрузить файл";

            string currentDirectory = Directory.GetCurrentDirectory();

            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = DIALOG_OPEN_FILE_FILTER;
            openFileDialog.Title = DIALOG_OPEN_FILE;
            openFileDialog.CheckFileExists = true;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;

                byte[] fileData = File.ReadAllBytes(filePath);

                if (!string.IsNullOrEmpty(filePath) && fileData != null && fileData.Length > 0)
                {
                    fileName = filePath;
                }
            }

            Directory.SetCurrentDirectory(currentDirectory);

            return fileName;
        }

        private DataTable InitPlanTable(List<ComboBoxItem> ActivityAims)
        {
            DataTransferObjectAim targetAim = null; //для того щоб виловити ціль типу 2 з використанням targetValue
            DataTable plans = new DataTable();
            DataColumn columnOlId = new DataColumn(colOLId, typeof(long));
            plans.Columns.Add(columnOlId);
            DataColumn columnMonth = new DataColumn(colMonth, typeof(DateTime));
            plans.Columns.Add(columnMonth);

            if (ActivityAims.Count > 2)
            {
                for (int i = 2; i < ActivityAims.Count; i++)
                {
                    ComboBoxItem item = ActivityAims[i];
                    Type type = typeof(string);

                    if (item.Id >= 0)
                    {
                        targetAim = AimsOr.Find(aim => aim.Id == item.Id);
                        if (targetAim != null)
                        {
                            switch (targetAim.Type)
                            {
                                case 1:
                                case 6: //числові питання
                                case 7: //Количество дверей InBev
                                    type = typeof (double);
                                    break;
                                case 2:
                                    type = typeof (float);
                                    break;
                                case 3:
                                case 5: //логічні питання
                                case 8: //Must Stock
                                    type = typeof (byte);
                                    break;
                            }
                            plans.Columns.Add(item.Text, type);
                        }
                    }
                    if (item.Text.Contains(targetValuePref))
                    {
                        plans.Columns.Add(item.Text, typeof (double));
                    }
                }
            }

            DataColumn columnRowNumber = new DataColumn(colRowNumber, typeof(int));
            plans.Columns.Add(columnRowNumber);
            DataColumn columnStatus = new DataColumn(colOLStatus);
            columnStatus.DefaultValue = true;
            plans.Columns.Add(columnStatus);
            DataColumn columnStatusDescr = new DataColumn(colOlDescr);
            plans.Columns.Add(columnStatusDescr);

            return plans;
        }

        private void ValidatePlansDataBiss(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (ConvertEx.ToBool(row[colOLStatus]))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (col.DataType == typeof(byte) && ConvertEx.ToByte(row[col]) > 1)// IPTR и Логічні питання
                        {
                            row[colOLStatus] = false;
                            row[colOlDescr] = "Неверный формат данных";

                            break;
                        }

                        if (col.DataType == typeof(double) && ConvertEx.ToDouble(row[col]) < 0)// Sales volume і числові питання.
                        {
                            row[colOLStatus] = false;
                            row[colOlDescr] = "Неверный формат данных";

                            break;
                        }

                        if (col.DataType == typeof(float) && (ConvertEx.ToFloat(row[col]) < 0 || ConvertEx.ToFloat(row[col]) > 100)) // Merchandising
                        {
                            row[colOLStatus] = false;
                            row[colOlDescr] = "Неверный формат данных";

                            break;
                        }
                    }
                }
            }
        }

        private void ValidatePlansDataEliminatePreviosPeriods(DataTable dt)
        {
            int i = 0;
            DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            while (i < dt.Rows.Count)
            {
                if (ConvertEx.ToBool(dt.Rows[i][colOLStatus]))
                {
                    DateTime date = ConvertEx.ToDateTime(dt.Rows[i][1]);
                    if (date < now)
                    {
                        dt.Rows.RemoveAt(i);
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    i++;
                }
            }
        }

        private void ValidatePlansDataDates(DataTable dt)
        {
           DateTime ActionEndMonth = new DateTime(ActionEnd.Year, ActionEnd.Month, 1);

           int i = dt.Rows.Count - 1;

           while (i >= 0)
           {
               if (ConvertEx.ToBool(dt.Rows[i][colOLStatus]))
               {
                   DateTime date = new DateTime(ConvertEx.ToDateTime(dt.Rows[i][1]).Year, ConvertEx.ToDateTime(dt.Rows[i][1]).Month, 1);
                   if (date > ActionEndMonth)
                   {
                       dt.Rows[i][colOLStatus] = false;
                       dt.Rows[i][colOlDescr] = "Дата не входит в период действия активности";
                   }
                   else
                   {
                       break;
                   }
               }

               i--;
           }
        }

        private void ValidatePlansOls(DataTable dt)
        {
            int index = 0, startIndex;
            string yearMonth;
            SqlXml data;

            while (index < dt.Rows.Count && !ConvertEx.ToBool(dt.Rows[index][colOLStatus]))
            {
                index++;
            }

            while (index < dt.Rows.Count)
            {
                yearMonth = GetYearMonth(dt.Rows[index][1]);
                startIndex = index;

                data = GetXmlSnapshotOls(dt, yearMonth, ref index);
                DataTable OlsStatus = DataProvider.CheckOls(MainControl.ActivityId, yearMonth, data);

                int olsCount = index - startIndex;

                foreach (DataRow row in OlsStatus.Rows)
                {
                    if (!ConvertEx.ToBool(row["isCorrect"]))
                    {
                        for (int i = startIndex; i < index; i++)
                        {
                            if (ConvertEx.ToBool(dt.Rows[i][colOLStatus]) && ConvertEx.ToLongInt(dt.Rows[i][colOLId]) == ConvertEx.ToLongInt(row["ol_id"]))
                            {
                                dt.Rows[i][colOLStatus] = row["isCorrect"];
                                dt.Rows[i][colOlDescr] = row["status"];

                                break;
                            }
                        }

                    }
                }

                if (olsCount != OlsStatus.Rows.Count && OlsStatus.Rows.Count > 0)
                {
                    DealWithDobleEntrances(dt, startIndex, index);
                }

            }
        }

        private void DealWithDobleEntrances(DataTable dt, int startIndex, int endIndex)
        {
            List<ComboBoxItem> olList = new List<ComboBoxItem>();

            for (int i = startIndex; i < endIndex; i++)
            {
                olList.Add(new ComboBoxItem(i, dt.Rows[i][colOLId].ToString())); 
            }

            olList.Sort((item1, item2) => string.Compare(item1.Text, item2.Text));

            for (int i = 1; i < olList.Count; i++)
            {
                if (olList[i].Text == olList[i-1].Text)
                {
                    dt.Rows[olList[i - 1].Id][colOLStatus] = false;
                    dt.Rows[olList[i - 1].Id][colOlDescr] = "План для ТТ указан в файле больше одного раза";

                    dt.Rows[olList[i].Id][colOLStatus] = false;
                    dt.Rows[olList[i].Id][colOlDescr] = "План для ТТ указан в файле больше одного раза";
                }
            }
        }

        private bool LoadPlans(DataTable dt)
        {
            int index = 0;
            string yearMonth;
            SqlXml data;

            // just omit leading rows with status=TRUE
            while (index < dt.Rows.Count && !ConvertEx.ToBool(dt.Rows[index][colOLStatus]))
            {
                index++;
            }

            // go through array only 1 time
            while (index < dt.Rows.Count)
            {
                yearMonth = GetYearMonth(dt.Rows[index][1]);

                data = GetXmlSnapshotPlans(dt, yearMonth, ref index);
                DataTable PlansLoadingResult = DataProvider.LoadPlans(MainControl.ActivityId, yearMonth, data);
            }

            return true;
        }

        private SqlXml GetXmlSnapshotOls(DataTable dataTable, string yearMonth, ref int index)
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement("rows");
                if (dataTable != null)
                    while (index < dataTable.Rows.Count && GetYearMonth(dataTable.Rows[index][1]) == yearMonth)
                    {
                        if (ConvertEx.ToBool(dataTable.Rows[index][colOLStatus]))
                        {
                            lWriter.WriteStartElement("row");

                            lWriter.WriteAttributeString("Ol_Id", dataTable.Rows[index][colOLId].ToString());

                            lWriter.WriteEndElement();
                        }

                        index++;
                    }

                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                //XmlDocument myXmlDocument = new XmlDocument();
                //myXmlDocument.Load(lStream);
                //myXmlDocument.Save("d://goals1.xml");

                return new SqlXml(lStream);
            }
        }

        private SqlXml GetXmlSnapshotPlans(DataTable dataTable, string yearMonth, ref int index)
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement("rows");
                if (dataTable != null)
                {
                    while (index < dataTable.Rows.Count && (!ConvertEx.ToBool(dataTable.Rows[index][colOlSelected]) || GetYearMonth(dataTable.Rows[index][1]) == yearMonth))
                    {
                        if (ConvertEx.ToBool(dataTable.Rows[index][colOlSelected]))
                        {
                          for (int i = 0; i < AimsOr.Count; i++)
                            {
                                lWriter.WriteStartElement("row");
                                lWriter.WriteAttributeString("Ol_Id", dataTable.Rows[index][colOLId].ToString());
                                lWriter.WriteAttributeString("Aim_Id", AimsOr[i].Id.ToString());
                                for (int j = 0; j < dataTable.Columns.Count; j++)
                                {
                                    if (dataTable.Columns[j].Caption == AimsOr[i].Name)
                                    {
                                        lWriter.WriteAttributeString("Value", ConvertEx.ToDecimal(dataTable.Rows[index][j]).ToString(CultureInfo.InvariantCulture));
                                        j = dataTable.Columns.Count;
                                    }
                                }
                                if (AimsOr[i].Type == (int) GoalType.Merchandising && AimsOr[i].IsUseAimValue)
                                {
                                    for (int j = 0; j < dataTable.Columns.Count; j++)
                                    {
                                        if (dataTable.Columns[j].Caption ==
                                            merchAddPlanIndicator.Caption + " " + AimsOr[i].Name)
                                        {
                                            lWriter.WriteAttributeString("TargetPlanValue",
                                                ConvertEx.ToDecimal(dataTable.Rows[index][j])
                                                    .ToString(CultureInfo.InvariantCulture));
                                            j = dataTable.Columns.Count;
                                        }
                                    }
                                }
                                else
                                {
                                    lWriter.WriteAttributeString("TargetPlanValue", string.Empty);
                                }
                                lWriter.WriteEndElement();
                            }
                        }

                        index++;
                    }
                }

                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                //XmlDocument myXmlDocument = new XmlDocument();
                //myXmlDocument.Load(lStream);
                //myXmlDocument.Save("d://goals2.xml");

                return new SqlXml(lStream);
            }
        }

        private string GetYearMonth(object date)
        {
            return ConvertEx.ToDateTime(date).Year.ToString() + ConvertEx.ToDateTime(date).Month.ToString("D2");
        }

        private string GetYearMonth(string date)
        {
            DateTimeFormatInfo mfi = new CultureInfo("ru-RU", false).DateTimeFormat;

            string month = (Array.IndexOf<string>(mfi.MonthNames, date.Split(' ')[0]) + 1).ToString("D2");

            return date.Split(' ')[1] + month;
        }

        #endregion PlansUploading

        #region PlansEditing

        private void ChangedTableInit()
        {
            changedValues.Columns.Add("YearMonth");
            changedValues.Columns.Add("Ol_Id");
            changedValues.Columns.Add("Aim_Id");
            changedValues.Columns.Add("Value");
            changedValues.Columns.Add("TargetPlanValue");
        }

        private List<DataRow> GetModifiedRows()
        {
            List<DataRow> changedRows = (from DataRow row in source.Rows
                                         where ConvertEx.ToBool(row[colIsModified.FieldName])
                                         select row).ToList();

            return changedRows;
        }

        private void FillChangedTable(List<DataRow> changedRows)
        {
            if (source == null || source.Columns.Count <= 9)
            {
                return;
            }

            int index;
            string yearMonth;
            int i;
            string aimName;
            GridBand month, plan;
            DataTransferObjectAim lAim = null;

            index = 1;

            while (index < bandedGridView1.Bands.Count && bandedGridView1.Bands[index].Caption != "Итого")
            {
                month = bandedGridView1.Bands[index];
                yearMonth = GetYearMonth(month.Caption);

                plan = month.Children[0];

                foreach (GridColumn col in plan.Columns)
                {
                    i = source.Columns.IndexOf(col.FieldName);
                    aimName = col.Caption;
                    lAim = AimsOr.Find(aim => aim.Name == aimName);

                    foreach (DataRow OlInfo in changedRows)
                    {
                        object[] row = new object[changedValues.Columns.Count];

                        row[0] = yearMonth;
                        row[1] = OlInfo[colOlId.FieldName];
                        row[2] = lAim.Id;
                        row[3] = OlInfo[i];
                        if (lAim.Type == (int) GoalType.Merchandising && lAim.IsUseAimValue)
                        {
                            GridBand lBand = month.Children[1];
                            GridColumn targetCol = null;
                            for (int j = 0; j < lBand.Columns.Count; j++)
                            {
                                if (lBand.Columns[j].Caption == lAim.Name)
                                {
                                    targetCol = lBand.Columns[j];
                                    j = lBand.Columns.Count;
                                }
                            }
                            int f = source.Columns.IndexOf(targetCol.FieldName);
                            row[4] = OlInfo[f];
                        }
                        else
                        {
                            row[4] = string.Empty;
                        }
                        if (row[4] != null)
                        {
                            changedValues.Rows.Add(row);
                        }
                    }
                }

                index++;
            }
        }

        private void SaveChangedPlans()
        {
            ChangedTableInit();
            FillChangedTable(GetModifiedRows());

            int index = 0;
            string yearMonth;
            SqlXml data;

            while (index < changedValues.Rows.Count)
            {
                yearMonth = ConvertEx.ToString(changedValues.Rows[index][0]);

                data = GetXmlSnapshotPlans(yearMonth, ref index);
                DataTable PlansLoadingResult = DataProvider.LoadPlans(MainControl.ActivityId, yearMonth, data);
            }
        }

        private SqlXml GetXmlSnapshotPlans(string yearMonth, ref int index)
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement("rows");
                if (changedValues != null)
                {
                    while (index < changedValues.Rows.Count && changedValues.Rows[index][0].ToString() == yearMonth)
                    {
                        lWriter.WriteStartElement("row");

                        lWriter.WriteAttributeString("Ol_Id", changedValues.Rows[index][1].ToString());
                        lWriter.WriteAttributeString("Aim_Id", changedValues.Rows[index][2].ToString());
                        lWriter.WriteAttributeString("Value", ConvertEx.ToDecimal(changedValues.Rows[index][3]).ToString(CultureInfo.InvariantCulture));
                        lWriter.WriteAttributeString("TargetPlanValue",
                            (string)changedValues.Rows[index][4] == string.Empty
                                ? string.Empty
                                : ConvertEx.ToDecimal(changedValues.Rows[index][4])
                                    .ToString(CultureInfo.InvariantCulture));

                        lWriter.WriteEndElement();

                        index++;
                    }
                }

                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                //XmlDocument myXmlDocument = new XmlDocument();
                //myXmlDocument.Load(lStream);
                //myXmlDocument.Save("d://goals2.xml");

                return new SqlXml(lStream);
            }
        }

        private void UpdateRow(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle < 0 || GetMonthName(e.Column.FieldName) == dbResPref || GetPlanFactPrefName(e.Column.FieldName) != planPref)
            {
                return;
            }

            BandedGridView view = sender as BandedGridView;

            string aimName = GetAimName(e.Column.FieldName);
            DataTransferObjectAim aim = AimsOr.Find(a => a.Name == aimName);

            if (aim.Type == 1)// sales V
            {
                //get Итого
                string resColName = "ЯЯЯЯ_Итого_План_" + GetAimName(e.Column.FieldName);
                view.SetRowCellValue(e.RowHandle, resColName, ConvertEx.ToDouble(view.GetRowCellValue(e.RowHandle, resColName)) - oldValue + ConvertEx.ToDouble(e.Value));
            }

            if (aim.IsUsedInCalculations)//update Выполнение
            {
                //update Выполнение
                bool exec = true;
                string pref = GetMonthName(e.Column.FieldName) + stringDelimiter + GetPlanFactPrefName(e.Column.FieldName);
                foreach (DataColumn col in source.Columns)
                {
                    if (col.ColumnName.StartsWith(pref) && AimsOr.Find(a => a.Name == GetAimName(col.ColumnName)).IsUsedInCalculations)
                    {
                        double plan = ConvertEx.ToDouble(view.GetRowCellValue(e.RowHandle, col.ColumnName));

                        // get fact
                        string[] ar = e.Column.FieldName.Split(charDelimiter);
                        ar[1] = factPref;
                        string factColName = string.Join(stringDelimiter, ar);

                        double fact = ConvertEx.ToDouble(view.GetRowCellValue(e.RowHandle, factColName));

                        exec = exec && plan <= fact;
                    }
                }

                //get Выполнение
                string execColName = GetMonthName(e.Column.FieldName) + "_Выполнение_ЯЯЯ";
                view.SetRowCellValue(e.RowHandle, execColName, exec ? 1 : 0);
            }
        }

        private void bandedGridView1_ValidatingEditor(object sender,
            DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            BandedGridView view = sender as BandedGridView;

            if (view.FocusedRowHandle < 0)
            {
                return;
            }

            int aimType = AimsOr.Find(a => a.Name == GetAimName(view.FocusedColumn.FieldName, true)) != null
                ? AimsOr.Find(a => a.Name == GetAimName(view.FocusedColumn.FieldName, true)).Type
                : 0;

            if (aimType == 1 || aimType == 0) // SKU aims
            {
                try
                {
                    double val = ConvertEx.ToDouble(e.Value);

                    if (val < 0 || val > 999999999999999.999)
                    {
                        e.Valid = false;
                    }

                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }

            else if (aimType == 2) // KPI aims
            {
                try
                {
                    float val = ConvertEx.ToFloat(e.Value);

                    if (val < 0 || val > 100)
                    {
                        e.Valid = false;
                    }

                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }

            else if (aimType == 3) // IPTR aims
            {
                try
                {
                    byte val = ConvertEx.ToByte(e.Value);

                    if (val > 1)
                    {
                        e.Valid = false;
                    }

                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }
            else if (aimType == 5)
            {
                try
                {
                    int val = ConvertEx.ToByte(e.Value);
                    if (val > 1 || val < 0)
                    {
                        e.Valid = false;
                    }
                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }
            else if (aimType == 6)
            {
                try
                {
                    double val = ConvertEx.ToDouble(e.Value);
                    if (val < 0 || val > 999999999999999.999)
                    {
                        e.Valid = false;
                    }
                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }
            else if (aimType == 7)
            {
                try
                {
                    double val = ConvertEx.ToDouble(e.Value);
                    if (val < 0 || val > 999999999999999.999)
                    {
                        e.Valid = false;
                    }
                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }
            else if (aimType == 8) // Must stock aims
            {
                try
                {
                    double val = ConvertEx.ToDouble(e.Value);

                    if (val < 0 || val >1)
                    {
                        e.Valid = false;
                    }

                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }

            if (!e.Valid)
            {
                e.ErrorText = "Неверный формат данных";
            }
        }

        private void bandedGridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                return;
            }

            BandedGridView view = sender as BandedGridView;

            if (ConvertEx.ToBool(view.GetRowCellValue(e.RowHandle, colIsModified.FieldName)) != true)
            {
                view.SetRowCellValue(e.RowHandle, colIsModified.FieldName, true);
            }
                        
            UpdateRow(sender, e);
        }

        private void bandedGridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle < 0 || (e.RowHandle >= 0 && (sender as BandedGridView).IsRowSelected(e.RowHandle)))
            {
                return;
            }

            BandedGridView view = sender as BandedGridView;

            if (ConvertEx.ToBool(view.GetRowCellValue(e.RowHandle, colIsModified.FieldName)))
            {
                e.Appearance.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
            }
        }

        private double oldValue;
        private void bandedGridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            BandedGridView view = sender as BandedGridView;

            if (e.RowHandle >= 0)
            {
                oldValue = ConvertEx.ToDouble(view.GetRowCellValue(e.RowHandle, e.Column.FieldName));
            }
        }

        private void bandedGridView1_ShownEditor(object sender, EventArgs e)
        {
            BandedGridView view = sender as BandedGridView;
            GridColumn col = view.FocusedColumn;

            if (view.FocusedRowHandle < 0)
            {
                return;
            }

            int aimType = AimsOr.Find(a => a.Name == GetAimName(col.FieldName, true)) != null
                ? AimsOr.Find(a => a.Name == GetAimName(col.FieldName, true)).Type
                : 0;

            col.ColumnEdit = textEdit;

            col.ColumnEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            col.ColumnEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;

            switch (aimType)
            {
                case 1:
                case 6:
                case 7:
                    col.ColumnEdit.DisplayFormat.FormatString = formatSalesVol;
                    col.ColumnEdit.EditFormat.FormatString = formatSalesVol;
                    break;
                case 2:
                    col.ColumnEdit.DisplayFormat.FormatString = formatMerch;
                    col.ColumnEdit.EditFormat.FormatString = formatMerch;
                    break;
                case 3:
                case 5:
                case 8:
                    col.ColumnEdit.DisplayFormat.FormatString = formatIPTR;
                    col.ColumnEdit.EditFormat.FormatString = formatIPTR;
                    break;
            }
            if (aimType == 0)
            {
                col.ColumnEdit.DisplayFormat.FormatString = "{0:N2}";
                col.ColumnEdit.EditFormat.FormatString = "{0:N2}";
            }
        }
        #endregion PlansEditing
    }    
}