﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.Utility;
using System.Data;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.MarketProgramms.UserControls
{
    [ToolboxItem(false)]
    public partial class RegionsTree : UserControl
    {
        #region Constants

        private const char ACTIONS_LIST_SEPARATOR = ',';

        #endregion

        #region Constructors

        public RegionsTree()
        {
            InitializeComponent();
            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
                LoadData();
        }

        #endregion

        #region Instance Properties

        public IEnumerable<int> SelectedActivityIds
        {
            get
            {
                List<string> listOfStrings = new List<string>();
                treeListRegions.NodesIterator.DoOperation(node =>
                                                              {
                                                                  if (node.Checked && !node.HasChildren)
                                                                  {
                                                                      listOfStrings.Add(node[tlcActionsList].ToString());
                                                                      if (chkShowAllActivities.Checked)
                                                                          listOfStrings.Add(node[tlcOldActionsList].ToString());
                                                                  }
                                                              });
                List<string> listOfActIds = new List<string>();
                foreach (string s in listOfStrings)
                {
                    listOfActIds.AddRange(s.Split(ACTIONS_LIST_SEPARATOR));
                }

                return listOfActIds.Where(item=>!String.IsNullOrEmpty(item.Trim())).Select(actId => ConvertEx.ToInt(actId));
            }
        }

        #endregion

        #region Instance Methods
        public void LoadData()
        {
            try
            {
                treeListRegions.BeginUpdate();
                treeListRegions.DataSource = DataProvider.RegionsTree;

                treeListRegions.ClearNodes();

                treeListRegions.ExpandAll();
                treeListRegions.CollapseAll();

                treeListRegions.EndUpdate();
            }
            catch (Exception ex)
            {}
            
            treeListRegions.NodesIterator.DoOperation(new SetNodesCheckStateOperation());
            treeListRegions.NodesIterator.DoOperation(new ExpandNodesOperation());
        }

        private void SetChildNodesStatus(TreeListNode node)
        {
            foreach (TreeListNode childNode in node.Nodes)
            {
                //if (!childNode.HasChildren && !ConvertEx.ToBool(childNode.GetValue(treeListModifyable))) continue;
                childNode.Checked = node.Checked;
                SetChildNodesStatus(childNode);
            }
        }

        private bool SetNodesState(TreeListNode topNode)
        {
            if (!topNode.HasChildren)
            {
                return topNode.Checked;
            }
            bool isChecked = true;
            foreach (TreeListNode treeListNode in topNode.Nodes)
            {
                treeListNode.Checked = SetNodesState(treeListNode);
                isChecked = isChecked && treeListNode.Checked;
            }
            return isChecked;
        }

        #endregion

        #region Event Handling

        private void btnCloseRegions_Click(object sender, EventArgs e)
        {
            if (CloseBtnClick != null)
                CloseBtnClick(this, new EventArgs());
        }

        private void treeListRegions_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            //if (!e.Node.HasChildren && e.Node.Checked && !ConvertEx.ToBool(e.Node[treeListModifyable]))
            //    e.Node.Checked = false;
            SetChildNodesStatus(e.Node);
            TreeListNode rootNode = e.Node.RootNode;
            rootNode.Checked = SetNodesState(rootNode);
        }

        #endregion

        #region Event Declarations

        public event EventHandler CloseBtnClick;

        #endregion
    }
}