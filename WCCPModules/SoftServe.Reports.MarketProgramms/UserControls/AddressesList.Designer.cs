﻿namespace SoftServe.Reports.MarketProgramms.UserControls
{
  partial class ManageAddressesList
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageAddressesList));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonFields = new DevExpress.XtraBars.BarButtonItem();
            this.addPocDdl = new DevExpress.XtraBars.BarSubItem();
            this.btnAddPocBuffer = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddPocTR = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddPocTerritory = new DevExpress.XtraBars.BarButtonItem();
            this.customAddPOCOption = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonDisplayFilter = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.barEditChanel = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.lblError = new DevExpress.XtraBars.BarStaticItem();
            this.barEditMode = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.barButtonBuffer = new DevExpress.XtraBars.BarButtonItem();
            this.gridControlSummary = new DevExpress.XtraGrid.GridControl();
            this.gridViewSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnOlId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInbevPart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnAvgV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSummaryV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFCountInbev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFCountOthers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCCountInbev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCCountOthers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFPartInbev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFPartOthers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCPartInbev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCPartOthers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridHolder = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHolder)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imCollection;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonFields,
            this.barEditChanel,
            this.barButtonBuffer,
            this.addPocDdl,
            this.btnAddPocBuffer,
            this.btnAddPocTR,
            this.btnAddPocTerritory,
            this.lblError,
            this.customAddPOCOption,
            this.barBtnDelete,
            this.barButtonDisplayFilter,
            this.barEditMode});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 14;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemRadioGroup1});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(60, 111);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonFields, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.addPocDdl, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonDisplayFilter, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnDelete, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditChanel, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblError),
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.PaintStyle | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.barEditMode, "", false, true, true, 170, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonFields
            // 
            this.barButtonFields.Caption = "Дополнительные колонки";
            this.barButtonFields.Id = 0;
            this.barButtonFields.ImageIndex = 1;
            this.barButtonFields.Name = "barButtonFields";
            this.barButtonFields.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonFields_ItemClick);
            // 
            // addPocDdl
            // 
            this.addPocDdl.Caption = "Добавить ТТ";
            this.addPocDdl.Id = 5;
            this.addPocDdl.ImageIndex = 0;
            this.addPocDdl.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAddPocBuffer),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAddPocTR),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAddPocTerritory),
            new DevExpress.XtraBars.LinkPersistInfo(this.customAddPOCOption)});
            this.addPocDdl.Name = "addPocDdl";
            this.addPocDdl.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnAddPocBuffer
            // 
            this.btnAddPocBuffer.Caption = "Из буфера обмена";
            this.btnAddPocBuffer.Id = 6;
            this.btnAddPocBuffer.ImageIndex = 0;
            this.btnAddPocBuffer.Name = "btnAddPocBuffer";
            this.btnAddPocBuffer.Tag = 0;
            this.btnAddPocBuffer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barFilterButton_Click);
            // 
            // btnAddPocTR
            // 
            this.btnAddPocTR.Caption = "Из Thomas Research";
            this.btnAddPocTR.Id = 7;
            this.btnAddPocTR.ImageIndex = 0;
            this.btnAddPocTR.Name = "btnAddPocTR";
            this.btnAddPocTR.Tag = 1;
            this.btnAddPocTR.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barFilterButton_Click);
            // 
            // btnAddPocTerritory
            // 
            this.btnAddPocTerritory.Caption = "По Территории";
            this.btnAddPocTerritory.Id = 8;
            this.btnAddPocTerritory.ImageIndex = 0;
            this.btnAddPocTerritory.Name = "btnAddPocTerritory";
            this.btnAddPocTerritory.Tag = 2;
            this.btnAddPocTerritory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barFilterButton_Click);
            // 
            // customAddPOCOption
            // 
            this.customAddPOCOption.Id = 10;
            this.customAddPOCOption.ImageIndex = 0;
            this.customAddPOCOption.Name = "customAddPOCOption";
            this.customAddPOCOption.Tag = 3;
            this.customAddPOCOption.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.customAddPOCOption.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barFilterButton_Click);
            // 
            // barButtonDisplayFilter
            // 
            this.barButtonDisplayFilter.Caption = "Фильтровать ТТ";
            this.barButtonDisplayFilter.Id = 12;
            this.barButtonDisplayFilter.ImageIndex = 3;
            this.barButtonDisplayFilter.Name = "barButtonDisplayFilter";
            this.barButtonDisplayFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonDisplayFilter_ItemClick);
            // 
            // barBtnDelete
            // 
            this.barBtnDelete.Caption = "Удалить ТТ";
            this.barBtnDelete.Id = 11;
            this.barBtnDelete.ImageIndex = 2;
            this.barBtnDelete.Name = "barBtnDelete";
            this.barBtnDelete.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barBtnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDelete_ItemClick);
            // 
            // barEditChanel
            // 
            this.barEditChanel.Caption = "Канал:";
            this.barEditChanel.Edit = this.repositoryItemComboBox1;
            this.barEditChanel.Id = 1;
            this.barEditChanel.Name = "barEditChanel";
            this.barEditChanel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditChanel.Width = 100;
            this.barEditChanel.EditValueChanged += new System.EventHandler(this.barEditChanel_EditValueChanged);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.NullText = "Все";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // lblError
            // 
            this.lblError.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblError.Appearance.Options.UseForeColor = true;
            this.lblError.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblError.Id = 9;
            this.lblError.Name = "lblError";
            this.lblError.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barEditMode
            // 
            this.barEditMode.Caption = "Режим редактирования";
            this.barEditMode.Edit = this.repositoryItemRadioGroup1;
            this.barEditMode.EditValue = false;
            this.barEditMode.Id = 13;
            this.barEditMode.Name = "barEditMode";
            this.barEditMode.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemRadioGroup1.Appearance.Options.UseBackColor = true;
            this.repositoryItemRadioGroup1.Appearance.Options.UseBorderColor = true;
            this.repositoryItemRadioGroup1.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Одиночный"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Групповой")});
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "filter.png");
            this.imCollection.Images.SetKeyName(1, "column_chooser.png");
            this.imCollection.Images.SetKeyName(2, "delete.gif");
            this.imCollection.Images.SetKeyName(3, "filter_data.png");
            // 
            // barButtonBuffer
            // 
            this.barButtonBuffer.Caption = "Из буфера обмена";
            this.barButtonBuffer.Id = 13;
            this.barButtonBuffer.ImageIndex = 3;
            this.barButtonBuffer.Name = "barButtonBuffer";
            this.barButtonBuffer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // gridControlSummary
            // 
            this.gridControlSummary.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControlSummary.Location = new System.Drawing.Point(0, 34);
            this.gridControlSummary.MainView = this.gridViewSummary;
            this.gridControlSummary.Name = "gridControlSummary";
            this.gridControlSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp});
            this.gridControlSummary.Size = new System.Drawing.Size(825, 63);
            this.gridControlSummary.TabIndex = 4;
            this.gridControlSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSummary});
            // 
            // gridViewSummary
            // 
            this.gridViewSummary.ColumnPanelRowHeight = 35;
            this.gridViewSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnOlId,
            this.gridColumnInbevPart,
            this.gridColumnAvgV,
            this.gridColumnSummaryV,
            this.gridColumnFCountInbev,
            this.gridColumnFCountOthers,
            this.gridColumnCCountInbev,
            this.gridColumnCCountOthers,
            this.gridColumnFPartInbev,
            this.gridColumnFPartOthers,
            this.gridColumnCPartInbev,
            this.gridColumnCPartOthers});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.LightSalmon;
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            this.gridViewSummary.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridViewSummary.GridControl = this.gridControlSummary;
            this.gridViewSummary.Name = "gridViewSummary";
            this.gridViewSummary.OptionsFilter.AllowFilterEditor = false;
            this.gridViewSummary.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSummary.OptionsSelection.MultiSelect = true;
            this.gridViewSummary.OptionsView.ShowFooter = true;
            this.gridViewSummary.OptionsView.ShowGroupPanel = false;
            this.gridViewSummary.RowHeight = 0;
            this.gridViewSummary.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridViewSummary.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gridViewSummary_CustomDrawFooterCell);
            this.gridViewSummary.CustomDrawFooter += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewSummary_CustomDrawFooter);
            this.gridViewSummary.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.gridViewSummary_CustomSummaryCalculate);
            this.gridViewSummary.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewSummary_CustomUnboundColumnData);
            // 
            // gridColumnOlId
            // 
            this.gridColumnOlId.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnOlId.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnOlId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOlId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOlId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnOlId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnOlId.Caption = "Кол-во ТТ";
            this.gridColumnOlId.FieldName = "ID";
            this.gridColumnOlId.Name = "gridColumnOlId";
            this.gridColumnOlId.OptionsColumn.AllowEdit = false;
            this.gridColumnOlId.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnOlId.OptionsColumn.ReadOnly = true;
            this.gridColumnOlId.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnOlId.OptionsFilter.AllowFilter = false;
            this.gridColumnOlId.SummaryItem.DisplayFormat = "{0:N0}";
            this.gridColumnOlId.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.gridColumnOlId.Visible = true;
            this.gridColumnOlId.VisibleIndex = 0;
            this.gridColumnOlId.Width = 73;
            // 
            // gridColumnInbevPart
            // 
            this.gridColumnInbevPart.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnInbevPart.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnInbevPart.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnInbevPart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnInbevPart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnInbevPart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnInbevPart.Caption = "Доля InBev %";
            this.gridColumnInbevPart.FieldName = "SHAREINBEV";
            this.gridColumnInbevPart.Name = "gridColumnInbevPart";
            this.gridColumnInbevPart.OptionsColumn.AllowEdit = false;
            this.gridColumnInbevPart.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnInbevPart.OptionsColumn.ReadOnly = true;
            this.gridColumnInbevPart.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnInbevPart.OptionsFilter.AllowFilter = false;
            this.gridColumnInbevPart.SummaryItem.DisplayFormat = "{0:P2}";
            this.gridColumnInbevPart.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Average;
            this.gridColumnInbevPart.Visible = true;
            this.gridColumnInbevPart.VisibleIndex = 1;
            this.gridColumnInbevPart.Width = 80;
            // 
            // gridColumnAvgV
            // 
            this.gridColumnAvgV.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnAvgV.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnAvgV.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnAvgV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnAvgV.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnAvgV.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnAvgV.Caption = "Общий V в базовом периоде, дал";
            this.gridColumnAvgV.FieldName = "AVERAGE_VOLUME";
            this.gridColumnAvgV.Name = "gridColumnAvgV";
            this.gridColumnAvgV.OptionsColumn.AllowEdit = false;
            this.gridColumnAvgV.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnAvgV.OptionsColumn.ReadOnly = true;
            this.gridColumnAvgV.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnAvgV.OptionsFilter.AllowFilter = false;
            this.gridColumnAvgV.SummaryItem.DisplayFormat = "{0:N2}";
            this.gridColumnAvgV.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnAvgV.Visible = true;
            this.gridColumnAvgV.VisibleIndex = 2;
            this.gridColumnAvgV.Width = 106;
            // 
            // gridColumnSummaryV
            // 
            this.gridColumnSummaryV.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnSummaryV.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnSummaryV.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnSummaryV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnSummaryV.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnSummaryV.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnSummaryV.Caption = "Общий V за М-1, дал";
            this.gridColumnSummaryV.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumnSummaryV.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnSummaryV.FieldName = "Cumulative_VOLUME";
            this.gridColumnSummaryV.Name = "gridColumnSummaryV";
            this.gridColumnSummaryV.OptionsColumn.AllowEdit = false;
            this.gridColumnSummaryV.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnSummaryV.OptionsColumn.ReadOnly = true;
            this.gridColumnSummaryV.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnSummaryV.OptionsFilter.AllowFilter = false;
            this.gridColumnSummaryV.SummaryItem.DisplayFormat = "{0:N2}";
            this.gridColumnSummaryV.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnSummaryV.Visible = true;
            this.gridColumnSummaryV.VisibleIndex = 3;
            this.gridColumnSummaryV.Width = 104;
            // 
            // gridColumnFCountInbev
            // 
            this.gridColumnFCountInbev.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnFCountInbev.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFCountInbev.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnFCountInbev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnFCountInbev.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnFCountInbev.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFCountInbev.Caption = "Кол-во ХШ InBev";
            this.gridColumnFCountInbev.FieldName = "FRIDGES_INBEV";
            this.gridColumnFCountInbev.Name = "gridColumnFCountInbev";
            this.gridColumnFCountInbev.OptionsColumn.AllowEdit = false;
            this.gridColumnFCountInbev.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnFCountInbev.OptionsColumn.ReadOnly = true;
            this.gridColumnFCountInbev.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnFCountInbev.OptionsFilter.AllowFilter = false;
            this.gridColumnFCountInbev.SummaryItem.DisplayFormat = "{0:N0}";
            this.gridColumnFCountInbev.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnFCountInbev.Visible = true;
            this.gridColumnFCountInbev.VisibleIndex = 4;
            this.gridColumnFCountInbev.Width = 87;
            // 
            // gridColumnFCountOthers
            // 
            this.gridColumnFCountOthers.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnFCountOthers.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFCountOthers.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnFCountOthers.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnFCountOthers.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnFCountOthers.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFCountOthers.Caption = "Кол-во ХШ конкурентов";
            this.gridColumnFCountOthers.FieldName = "FRIDGES_OTHER";
            this.gridColumnFCountOthers.Name = "gridColumnFCountOthers";
            this.gridColumnFCountOthers.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnFCountOthers.OptionsColumn.ReadOnly = true;
            this.gridColumnFCountOthers.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnFCountOthers.OptionsFilter.AllowFilter = false;
            this.gridColumnFCountOthers.SummaryItem.DisplayFormat = "{0:N0}";
            this.gridColumnFCountOthers.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnFCountOthers.Visible = true;
            this.gridColumnFCountOthers.VisibleIndex = 6;
            this.gridColumnFCountOthers.Width = 128;
            // 
            // gridColumnCCountInbev
            // 
            this.gridColumnCCountInbev.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnCCountInbev.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCCountInbev.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnCCountInbev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnCCountInbev.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnCCountInbev.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCCountInbev.Caption = "Кол-во РУ InBev";
            this.gridColumnCCountInbev.FieldName = "COOLERS_INBEV";
            this.gridColumnCCountInbev.Name = "gridColumnCCountInbev";
            this.gridColumnCCountInbev.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnCCountInbev.OptionsColumn.ReadOnly = true;
            this.gridColumnCCountInbev.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnCCountInbev.OptionsFilter.AllowFilter = false;
            this.gridColumnCCountInbev.SummaryItem.DisplayFormat = "{0:N0}";
            this.gridColumnCCountInbev.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnCCountInbev.Visible = true;
            this.gridColumnCCountInbev.VisibleIndex = 8;
            this.gridColumnCCountInbev.Width = 90;
            // 
            // gridColumnCCountOthers
            // 
            this.gridColumnCCountOthers.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnCCountOthers.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCCountOthers.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnCCountOthers.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnCCountOthers.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnCCountOthers.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCCountOthers.Caption = "Кол-во РУ конкурентов";
            this.gridColumnCCountOthers.FieldName = "COOLERS_OTHER";
            this.gridColumnCCountOthers.Name = "gridColumnCCountOthers";
            this.gridColumnCCountOthers.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnCCountOthers.OptionsColumn.ReadOnly = true;
            this.gridColumnCCountOthers.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnCCountOthers.OptionsFilter.AllowFilter = false;
            this.gridColumnCCountOthers.SummaryItem.DisplayFormat = "{0:N0}";
            this.gridColumnCCountOthers.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnCCountOthers.Visible = true;
            this.gridColumnCCountOthers.VisibleIndex = 10;
            // 
            // gridColumnFPartInbev
            // 
            this.gridColumnFPartInbev.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnFPartInbev.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFPartInbev.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnFPartInbev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnFPartInbev.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnFPartInbev.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFPartInbev.Caption = "% ХШ InBev";
            this.gridColumnFPartInbev.FieldName = "unbound_FPartInbev1";
            this.gridColumnFPartInbev.Name = "gridColumnFPartInbev";
            this.gridColumnFPartInbev.OptionsColumn.AllowEdit = false;
            this.gridColumnFPartInbev.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnFPartInbev.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnFPartInbev.OptionsFilter.AllowFilter = false;
            this.gridColumnFPartInbev.SummaryItem.DisplayFormat = "{0:P2}";
            this.gridColumnFPartInbev.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnFPartInbev.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnFPartInbev.Visible = true;
            this.gridColumnFPartInbev.VisibleIndex = 5;
            this.gridColumnFPartInbev.Width = 97;
            // 
            // gridColumnFPartOthers
            // 
            this.gridColumnFPartOthers.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnFPartOthers.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFPartOthers.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnFPartOthers.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnFPartOthers.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnFPartOthers.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnFPartOthers.Caption = "% ХШ конкурентов";
            this.gridColumnFPartOthers.FieldName = "unbound_FPartOthers2";
            this.gridColumnFPartOthers.Name = "gridColumnFPartOthers";
            this.gridColumnFPartOthers.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnFPartOthers.OptionsColumn.ReadOnly = true;
            this.gridColumnFPartOthers.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnFPartOthers.OptionsFilter.AllowFilter = false;
            this.gridColumnFPartOthers.SummaryItem.DisplayFormat = "{0:P2}";
            this.gridColumnFPartOthers.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnFPartOthers.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnFPartOthers.Visible = true;
            this.gridColumnFPartOthers.VisibleIndex = 7;
            this.gridColumnFPartOthers.Width = 117;
            // 
            // gridColumnCPartInbev
            // 
            this.gridColumnCPartInbev.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnCPartInbev.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCPartInbev.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnCPartInbev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnCPartInbev.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnCPartInbev.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCPartInbev.Caption = "% РУ InBev";
            this.gridColumnCPartInbev.FieldName = "unbound_CPartInbev3";
            this.gridColumnCPartInbev.Name = "gridColumnCPartInbev";
            this.gridColumnCPartInbev.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnCPartInbev.OptionsColumn.ReadOnly = true;
            this.gridColumnCPartInbev.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnCPartInbev.OptionsFilter.AllowFilter = false;
            this.gridColumnCPartInbev.SummaryItem.DisplayFormat = "{0:P2}";
            this.gridColumnCPartInbev.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnCPartInbev.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnCPartInbev.Visible = true;
            this.gridColumnCPartInbev.VisibleIndex = 9;
            this.gridColumnCPartInbev.Width = 101;
            // 
            // gridColumnCPartOthers
            // 
            this.gridColumnCPartOthers.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnCPartOthers.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCPartOthers.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnCPartOthers.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnCPartOthers.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnCPartOthers.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnCPartOthers.Caption = "% РУ конкурентов";
            this.gridColumnCPartOthers.FieldName = "unbound_CPartOthers4";
            this.gridColumnCPartOthers.Name = "gridColumnCPartOthers";
            this.gridColumnCPartOthers.OptionsColumn.AllowEdit = false;
            this.gridColumnCPartOthers.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnCPartOthers.OptionsColumn.ReadOnly = true;
            this.gridColumnCPartOthers.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnCPartOthers.OptionsFilter.AllowFilter = false;
            this.gridColumnCPartOthers.SummaryItem.DisplayFormat = "{0:P2}";
            this.gridColumnCPartOthers.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnCPartOthers.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnCPartOthers.Visible = true;
            this.gridColumnCPartOthers.VisibleIndex = 11;
            this.gridColumnCPartOthers.Width = 117;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "Status")});
            this.repositoryStatusLookUp.DisplayMember = "StatusName";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // gridHolder
            // 
            this.gridHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridHolder.Location = new System.Drawing.Point(0, 97);
            this.gridHolder.Name = "gridHolder";
            this.gridHolder.Size = new System.Drawing.Size(825, 259);
            this.gridHolder.TabIndex = 5;
            // 
            // ManageAddressesList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridHolder);
            this.Controls.Add(this.gridControlSummary);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ManageAddressesList";
            this.Size = new System.Drawing.Size(825, 356);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHolder)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager barManager1;
    private DevExpress.XtraBars.Bar bar2;
    private DevExpress.XtraBars.BarButtonItem barButtonFields;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarEditItem barEditChanel;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
    private DevExpress.Utils.ImageCollection imCollection;
    private DevExpress.XtraBars.BarButtonItem barButtonBuffer;
    private DevExpress.XtraBars.BarSubItem addPocDdl;
    private DevExpress.XtraBars.BarButtonItem btnAddPocBuffer;
    private DevExpress.XtraBars.BarButtonItem btnAddPocTR;
    private DevExpress.XtraBars.BarButtonItem btnAddPocTerritory;
    private DevExpress.XtraBars.BarStaticItem lblError;
    private DevExpress.XtraGrid.GridControl gridControlSummary;
    private DevExpress.XtraGrid.Views.Grid.GridView gridViewSummary;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnOlId;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnInbevPart;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnAvgV;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnSummaryV;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnFCountInbev;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnFPartInbev;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
    private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
    private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
    private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
    private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnFCountOthers;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnFPartOthers;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnCCountInbev;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnCPartInbev;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnCCountOthers;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnCPartOthers;
    private DevExpress.XtraBars.BarButtonItem customAddPOCOption;
    private DevExpress.XtraBars.BarButtonItem barBtnDelete;
    private DevExpress.XtraBars.BarButtonItem barButtonDisplayFilter;
    private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
    public DevExpress.XtraBars.BarEditItem barEditMode;
    private DevExpress.XtraEditors.PanelControl gridHolder;
  }
}
