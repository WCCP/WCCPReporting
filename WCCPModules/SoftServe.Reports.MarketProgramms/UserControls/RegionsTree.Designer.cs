﻿namespace SoftServe.Reports.MarketProgramms.UserControls
{
    partial class RegionsTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeListRegions = new DevExpress.XtraTreeList.TreeList();
            this.tlcRegion = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlcActionsList = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlcOldActionsList = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.btnCloseRegions = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkShowAllActivities = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.treeListRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowAllActivities.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeListRegions
            // 
            this.treeListRegions.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlcRegion,
            this.tlcActionsList,
            this.tlcOldActionsList});
            this.treeListRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListRegions.Location = new System.Drawing.Point(2, 2);
            this.treeListRegions.Name = "treeListRegions";
            this.treeListRegions.OptionsView.ShowCheckBoxes = true;
            this.treeListRegions.OptionsView.ShowColumns = false;
            this.treeListRegions.OptionsView.ShowIndicator = false;
            this.treeListRegions.ParentFieldName = "PARENT_ID";
            this.treeListRegions.Size = new System.Drawing.Size(318, 317);
            this.treeListRegions.TabIndex = 5;
            this.treeListRegions.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListRegions_AfterCheckNode);
            // 
            // tlcRegion
            // 
            this.tlcRegion.Caption = "Region";
            this.tlcRegion.FieldName = "DATA";
            this.tlcRegion.Name = "tlcRegion";
            this.tlcRegion.OptionsColumn.AllowEdit = false;
            this.tlcRegion.OptionsColumn.ReadOnly = true;
            this.tlcRegion.Visible = true;
            this.tlcRegion.VisibleIndex = 0;
            this.tlcRegion.Width = 91;
            // 
            // tlcActionsList
            // 
            this.tlcActionsList.Caption = "tlcActionsList";
            this.tlcActionsList.FieldName = "ACTIONS_LIST";
            this.tlcActionsList.Name = "tlcActionsList";
            // 
            // tlcOldActionsList
            // 
            this.tlcOldActionsList.Caption = "treeListColumn1";
            this.tlcOldActionsList.FieldName = "ACTIONS_LIST_OLD";
            this.tlcOldActionsList.Name = "tlcOldActionsList";
            // 
            // btnCloseRegions
            // 
            this.btnCloseRegions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseRegions.Location = new System.Drawing.Point(129, 31);
            this.btnCloseRegions.Name = "btnCloseRegions";
            this.btnCloseRegions.Size = new System.Drawing.Size(188, 22);
            this.btnCloseRegions.TabIndex = 4;
            this.btnCloseRegions.Text = "<< Спрятать фильтр активностей";
            this.btnCloseRegions.Click += new System.EventHandler(this.btnCloseRegions_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkShowAllActivities);
            this.panelControl1.Controls.Add(this.btnCloseRegions);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 321);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(322, 58);
            this.panelControl1.TabIndex = 6;
            // 
            // chkShowAllActivities
            // 
            this.chkShowAllActivities.Location = new System.Drawing.Point(5, 6);
            this.chkShowAllActivities.Name = "chkShowAllActivities";
            this.chkShowAllActivities.Properties.Caption = "Показывать закончившиеся активности";
            this.chkShowAllActivities.Size = new System.Drawing.Size(312, 19);
            this.chkShowAllActivities.TabIndex = 5;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.treeListRegions);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(322, 321);
            this.panelControl2.TabIndex = 7;
            // 
            // RegionsTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "RegionsTree";
            this.Size = new System.Drawing.Size(322, 379);
            ((System.ComponentModel.ISupportInitialize)(this.treeListRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkShowAllActivities.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeListRegions;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcRegion;
        private DevExpress.XtraEditors.SimpleButton btnCloseRegions;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcActionsList;
        private DevExpress.XtraEditors.CheckEdit chkShowAllActivities;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcOldActionsList;
    }
}
