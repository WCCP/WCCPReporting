﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.CommonControls.AddressesFilters;
using System.Data;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraBars;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using System.Drawing;
using DevExpress.Data;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using ComboBoxItem = Logica.Reports.BaseReportControl.CommonControls.Addresses.ComboBoxItem;

namespace SoftServe.Reports.MarketProgramms.UserControls
{
    /// <summary>
    /// 
    /// </summary>
    [Designer(typeof(AddressesListDesigner))]
    public partial class ManageAddressesList : UserControl
    {
        private readonly string noValueText = "0";
        ColumnChooserForm ccForm = new ColumnChooserForm();
        private GridControl targetGrid;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageAddressesList"/> class.
        /// </summary>
        public ManageAddressesList()
        {
            InitializeComponent();
            ChanelsDdlAllowsNull = true;

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                var table = DataProvider.GetChanels();

                if (table == null && ChanelsDdlVisible) return;

                repositoryItemComboBox1.Items.Clear();
                foreach (DataRow row in table.Rows)
                {
                    if (!ChanelsDdlAllowsNull && row[Constants.FLD_CHANEL_ID] == DBNull.Value) continue;

                    repositoryItemComboBox1.Items.Add(new ComboBoxItem(
                        row[Constants.FLD_CHANEL_ID] == DBNull.Value ? null : row[Constants.FLD_CHANEL_ID],
                        Convert.ToString(row[Constants.FLD_CHANEL_NAME])));
                }

                barEditChanel.EditValue = repositoryItemComboBox1.Items[0];
            }
        }

        #region events

        /// <summary>
        /// Occurs when filter changed.
        /// </summary>
        public event FilterChanged OnFilterChanged;

        /// <summary>
        /// Occurs when display filter changed.
        /// </summary>
        public event FilterChanged OnDisplayFilterChanged;

        /// <summary>
        /// Occurs when delete btn clicked.
        /// </summary>
        public event EventHandler DeleteBtnClick;

        /// <summary>
        /// Occurs on filter showing.
        /// </summary>
        public event CustomFilterShowingHandler CustomFilterShowing;

        /// <summary>
        /// Occurs when filter preview changing.
        /// </summary>
        public event FilterPreviewChangingHandler FilterPreviewChanging;

        #endregion

        #region public properties

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        /// <summary>
        /// This property is essential to allowing the designer to work and
        /// the DesignerSerializationVisibility Attribute (above) is essential
        /// in allowing serialization to take place at design time.
        /// </summary>
        public PanelControl WorkingArea
        {
            get
            {
                return gridHolder;
            }
        }

        /// <summary>
        /// Gets the type of the channel.
        /// </summary>
        /// <value>The type of the channel.</value>
        public object ChannelId
        {
            get
            {
                /*ComboBoxItem selectedChannel = barEditChanel.EditValue as ComboBoxItem;
                if (selectedChannel != null)
                {
                    return selectedChannel.Id;
                }

                return null; */
                return barEditChanel.EditValue;
            }
            set
            {
                //barEditChanel.EditValue = repositoryItemComboBox1.Items.Cast<ComboBoxItem>().Where(x => x.Id == value).FirstOrDefault();
                barEditChanel.EditValue = value;
            }
        }

        /// <summary>
        /// Gets or sets the editl mode.
        /// </summary>
        /// <value>The editl mode.</value>
        private AddressEditMode editMode = AddressEditMode.AllowAll;
        public AddressEditMode EditMode
        {
            get
            {
                return editMode;
            }
            set
            {
                editMode = value;
                UpdateButtonsStatus();
            }
        }

        /// <summary>
        /// Gets or sets the target grid control.
        /// </summary>
        /// <value>The target grid control.</value>
        [Description("TargetGrid")]
        public GridControl TargetGrid
        {
            get
            {
                if (targetGrid == null)
                {
                    targetGrid = FindGrid(gridHolder);
                }

                return targetGrid;
            }
            set
            {
                targetGrid = value;
                BindTargetGridEvents();
            }
        }

        [Description("InvisibleColumns")]
        public GridColumn[] InvisibleColumns
        {
            get { return ccForm.ColumnsChooserCtrl.InvisibleColumns; }
            set { ccForm.ColumnsChooserCtrl.InvisibleColumns = value; }
        }

        [Description("FrozenColumns")]
        public GridColumn[] FrozenColumns
        {
            get { return ccForm.ColumnsChooserCtrl.FrozenColumns; }
            set { ccForm.ColumnsChooserCtrl.FrozenColumns = value; }
        }

        [Description("Disable moving of frozen columns")]
        [DefaultValueAttribute(false)]
        public bool DisableFrozenColumnsMoving 
        {
            get { return ccForm.ColumnsChooserCtrl.DisableFrozenColumnsMoving; }
            set { ccForm.ColumnsChooserCtrl.DisableFrozenColumnsMoving = value; }
        }

        [Description("Chanel Id Field Name ")]
        public string ChanelIdFieldName { get; set; }

        [Description("InBev Volume Field Name")]
        public string InBevVolumeFieldName { get; set; }

        [Description("Status Field Name")]
        [DefaultValue(null)]
        public string FldNameStatus { get; set; }

        [Description("Error text which will be shown in Red near the button")]
        [DefaultValue("")]
        public string ErrorText
        {
            get { return lblError.Caption; }
            set { lblError.Caption = value; }
        }

        [Description("Chanels ddl visible")]
        [DefaultValue(true)]
        public bool ChanelsDdlVisible
        {
            get { return barEditChanel.Visibility == BarItemVisibility.Always; }
            set { barEditChanel.Visibility = value ? BarItemVisibility.Always : BarItemVisibility.Never; }
        }

        [Description("Delete btn visible")]
        [DefaultValue(true)]
        public bool DeleteBtnVisible
        {
            get { return barBtnDelete.Visibility == BarItemVisibility.Always; }
            set { barBtnDelete.Visibility = value ? BarItemVisibility.Always : BarItemVisibility.Never; }
        }

        [Description("Chanels ddl allows null")]
        [DefaultValue(true)]
        public bool ChanelsDdlAllowsNull { get; set; }

        [Description("Comments For Filters Required")]
        [DefaultValue(false)]
        public bool CommentsForFiltersRequired { get; set; }

        [Description("Add POC common options visible")]
        [DefaultValue(true)]
        public bool AddPOCCommonOptionsVisible
        {
            get
            {
                return btnAddPocBuffer.Visible() || btnAddPocTR.Visible() || btnAddPocTerritory.Visible();
            }
            set
            {
                btnAddPocBuffer.Visible(value);
                btnAddPocTR.Visible(value);
                btnAddPocTerritory.Visible(value);
            }
        }

        [Description("CustomAddPOCOptionCaption")]
        [DefaultValue("")]
        public string CustomAddPOCOptionCaption
        {
            get
            {
                return customAddPOCOption.Caption;
            }
            set
            {
                customAddPOCOption.Caption = value;
            }
        }

        [Description("CustomAddPOCOptionVisible")]
        [DefaultValue(false)]
        public bool CustomAddPOCOptionVisible
        {
            get
            {
                return customAddPOCOption.Visible();
            }
            set
            {
                customAddPOCOption.Visible(value);
            }
        }

        [Description("Text that will be displayed in the OK button on the insertion form of addresses.")]
        public string InsertButtonText { get; set;}


        #region Summary column names

        [Description("The name of the field \"Сред V/TT дал за 12 мес\"")]
        [DefaultValue("AVERAGE_VOLUME")]
        public string SumColumnAvgV
        {
            get { return gridColumnAvgV.FieldName; }
            set { gridColumnAvgV.FieldName = value; }
        }

        [Description("The name of the field \"Кол-во OX InBev\"")]
        [DefaultValue("COOLERS_INBEV")]
        public string SumColumnCCountInbev
        {
            get { return gridColumnCCountInbev.FieldName; }
            set { gridColumnCCountInbev.FieldName = value; }
        }

        [Description("The name of the field \"Кол-во OX конкурентов\"")]
        [DefaultValue("COOLERS_OTHER")]
        public string SumColumnCCountOthers
        {
            get { return gridColumnCCountOthers.FieldName; }
            set { gridColumnCCountOthers.FieldName = value; }
        }

        [Description("The name of the field \"Кол-во ХШ InBev\"")]
        [DefaultValue("FRIDGES_INBEV")]
        public string SumColumnFCountInbev
        {
            get { return gridColumnFCountInbev.FieldName; }
            set { gridColumnFCountInbev.FieldName = value; }
        }

        [Description("The name of the field \"Кол-во ХШ конкурентов\"")]
        [DefaultValue("FRIDGES_OTHER")]
        public string SumColumnFCountOthers
        {
            get { return gridColumnFCountOthers.FieldName; }
            set { gridColumnFCountOthers.FieldName = value; }
        }

        [Description("The name of the field \"Доля InBev\"")]
        [DefaultValue("SHAREINBEV")]
        public string SumColumnInbevPart
        {
            get { return gridColumnInbevPart.FieldName; }
            set { gridColumnInbevPart.FieldName = value; }
        }

        [Description("The name of the field \"Кол-во ТТ\"")]
        [DefaultValue("ID")]
        public string SumColumnOlId
        {
            get { return gridColumnOlId.FieldName; }
            set { gridColumnOlId.FieldName = value; }
        }

        [Description("The name of the field \"Общий V дал за 12 мес\"")]
        [DefaultValue("TOTAL_VOLUME")]
        public string SumColumnSummaryV
        {
            get { return gridColumnSummaryV.FieldName; }
            set { gridColumnSummaryV.FieldName = value; }
        }

        [Description("Aggregation by field \"Общий V за М-1, дал\"")]
        public double AggrColumnSummaryV
        {
            get { return double.Parse((gridColumnSummaryV.SummaryItem.SummaryValue ?? 0).ToString()); }
        }

        [Description("Aggregation by field \"Общий V в базовом периоде, дал\"")]
        public double AggrColumnAvgV
        {
            get { return double.Parse((gridColumnAvgV.SummaryItem.SummaryValue ?? 0).ToString()); }
        }

        #endregion

        #region backward compatibility

        /// <summary>
        /// Gets or sets the statistics text.
        /// </summary>
        /// <value>The statistics text.</value>
        [Obsolete]
        public String StatisticsText { get; set; }

        #endregion

        #endregion

        #region private poperties

        /// <summary>
        /// Gets the type of the channel.
        /// </summary>
        /// <value>The type of the channel.</value>
        private ChannelType ChannelType
        {
            get
            {
                ComboBoxItem selectedChannel = barEditChanel.EditValue as ComboBoxItem;
                if (selectedChannel != null && selectedChannel.Id != null)
                {
                    return (ChannelType)selectedChannel.Id;
                }
                return ChannelType.All;
            }
        }

        /// <summary>
        /// Gets the ol id FieldName in target grid.
        /// </summary>
        /// <value>The ol id FieldName in target grid.</value>
        private string OlIdFNInTargetGrid
        {
            get
            {
                return String.IsNullOrEmpty(SumColumnOlId) ? Constants.FLD_FILTER_PREVIEW_OUTLET_ID : SumColumnOlId;
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Inits this instance.
        /// </summary>
        [Obsolete("You don't need to use this method anymore. Just put the target gtid inside working area of this control.")]
        public void Init()
        {
            targetGrid.Parent.Controls.Remove(targetGrid);
            gridHolder.Controls.Add(targetGrid);

            targetGrid.Dock = DockStyle.None;
            Dock = DockStyle.Fill;
        }

        /// <summary>
        /// Filters Addresses
        /// </summary>
        public void FilterDisplayedAddresses()
        {
            var filter = String.Join(",", ParseClipboard.GetCodes().Select(it => String.Format("'{0}'", it)).ToArray());

            if (!String.IsNullOrEmpty(filter))
            {
                if (OnDisplayFilterChanged != null)
                {
                    FilterChangedArgs args = new FilterChangedArgs { PocIds = filter };
                    OnDisplayFilterChanged(args);
                    CalculateStatistics();
                }
            }
            else
            {
                MessageBox.Show(Resource.ClipboardEmptyError, Resource.Information, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void ReLoadColumnChooser()
        {
            ccForm.ColumnsChooserCtrl.ReLoadData();
        }


        #endregion

        private void gridHolder_ControlAdded(object sender, ControlEventArgs e)
        {
            BindTargetGridEvents();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.UserControl.Load"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (customAddPOCOption != null)
            {
                barManager1.Items.Add(customAddPOCOption);
                addPocDdl.LinksPersistInfo.Add(new LinkPersistInfo(customAddPOCOption));
            }

            if (TargetGrid != null)
            {
                GridView targetView = TargetGrid.DefaultView as GridView;
                if (targetView != null)
                {
                    ccForm.ColumnsChooserCtrl.AssociatedView = targetView;
                    targetView.ColumnFilterChanged += targetGrid_DataSourceChanged;
                    targetView.KeyUp += targetGrid_DataSourceChanged;
                }
            }
        }

        /// <summary>
        /// Binds the target grid events.
        /// </summary>
        private void BindTargetGridEvents()
        {
            if (TargetGrid != null)
            {
                TargetGrid.DataSourceChanged -= targetGrid_DataSourceChanged;
                TargetGrid.DataSourceChanged += targetGrid_DataSourceChanged;
            }
        }

        /// <summary>
        /// Updates the address channel.
        /// </summary>
        private void UpdateAddressChannel()
        {
            if (TargetGrid == null || String.IsNullOrEmpty(ChanelIdFieldName)) return;

            DataTable table = TargetGrid.DataSource as DataTable;
            if (table != null)
            {
                DataView dataView = table.DefaultView;
                if (ChannelType == ChannelType.All)
                {
                    dataView.RowFilter = string.Empty;
                }
                else
                {
                    dataView.RowFilter = string.Format("{0} = {1}", ChanelIdFieldName, (int)ChannelType);
                }
            }

            CalculateStatistics();
        }

        /// <summary>
        /// Handles the DataSourceChanged event of the targetGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void targetGrid_DataSourceChanged(object sender, EventArgs e)
        {
            CalculateStatistics();
        }

        /// <summary>
        /// Handles the CustomDrawFooter event of the gridViewSummary control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs"/> instance containing the event data.</param>
        private void gridViewSummary_CustomDrawFooter(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            SolidBrush brush = new SolidBrush(Color.White);
            e.Graphics.FillRectangle(brush, e.Bounds);
        }

        /// <summary>
        /// Handles the CustomDrawFooterCell event of the gridViewSummary control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs"/> instance containing the event data.</param>
        private void gridViewSummary_CustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            SolidBrush brush = new SolidBrush(Color.White);
            e.Graphics.FillRectangle(brush, e.Bounds);
        }

        /// <summary>
        /// Handles the CustomUnboundColumnData event of the gridViewSummary control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs"/> instance containing the event data.</param>
        private void gridViewSummary_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            // % will be calculated by this forlmula: % = part1/(part1+part2)
            object part1, part2;
            if (e.Column == gridColumnCPartInbev)
            {
                part1 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnCCountInbev);
                part2 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnCCountOthers);
            }
            else if (e.Column == gridColumnCPartOthers)
            {
                part1 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnCCountOthers);
                part2 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnCCountInbev);
            }
            else if (e.Column == gridColumnFPartInbev)
            {
                part1 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnFCountInbev);
                part2 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnFCountOthers);
            }
            else if (e.Column == gridColumnFPartOthers)
            {
                part1 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnFCountOthers);
                part2 = gridViewSummary.GetListSourceRowCellValue(e.ListSourceRowIndex, gridColumnFCountInbev);
            }
            else
            {
                return;
            }

            if (!(part1 is int) || !(part2 is int))
            {
                return;
            }

            int part1Int = (int)part1, part2Int = (int)part2;

            e.Value = part1Int == 0 ? 0 : 100.0 * part1Int / (part1Int + part2Int);
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonFields control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonFields_ItemClick(object sender, ItemClickEventArgs e)
        {
            ccForm.ShowDialog();
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditChanel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditChanel_EditValueChanged(object sender, EventArgs e)
        {
            UpdateAddressChannel();
        }

        /// <summary>
        /// Handles the Click event of the barFilterButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barFilterButton_Click(object sender, ItemClickEventArgs e)
        {
            FilterBaseForm filter = GetFilterByType((FilterType)(int)e.Item.Tag);
            filter.FilterPreviewChanging += args =>
            {
                args.AddedOutlets = GetAllAddresses();
                if (FilterPreviewChanging != null)
                {
                    FilterPreviewChanging(args);
                }
            };

            if (filter != null && filter.ShowDialog() == DialogResult.OK && OnFilterChanged != null)
            {
                FilterChangedArgs args = new FilterChangedArgs { FilterInfo = filter.SelectedItems, PocIds = filter.POCList };
                OnFilterChanged(args);
                CalculateStatistics();
            }
        }

        private void barBtnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DeleteBtnClick != null) 
            {
                DeleteBtnClick(this, new EventArgs());
            }

            CalculateStatistics();
        }

        private void CalculateStatistics()
        {
            DataTable source = targetGrid.DataSource as DataTable;
            GridView targetView = targetGrid.DefaultView as GridView;

            if (source == null || targetView == null)
            {
                gridControlSummary.DataSource = null;
                return;
            }

            source = source.Clone();
            for (int handle = 0; handle < targetView.DataRowCount; handle++)
            {
                DataRow row = targetView.GetDataRow(handle);
                if (String.IsNullOrEmpty(FldNameStatus) || row.Field<AddressRowStatus>(FldNameStatus) != AddressRowStatus.Removed)
                {
                    source.Rows.Add(row.ItemArray);
                }
            }
            gridControlSummary.DataSource = source;

            foreach (GridColumn column in gridViewSummary.Columns)
            {
                DataColumn targetColumn = source.Columns
                    .Cast<DataColumn>()
                    .Where(x => x.Caption == column.FieldName)
                    .FirstOrDefault();

                if (targetColumn == null && column.UnboundType == UnboundColumnType.Bound)
                {
                    column.SummaryItem.DisplayFormat = noValueText;
                }
            }
        }

        /// <summary>
        /// Gets the all addresses.
        /// </summary>
        /// <returns>List of all addresses</returns>
        private string GetAllAddresses()
        {
            if (TargetGrid == null || TargetGrid.DataSource == null) return null;

            var arr = (from d in (TargetGrid.DataSource as DataTable).AsEnumerable()
                       select d[OlIdFNInTargetGrid].ToString()).ToArray();

            return new XElement(Constants.XML_OUTLET_ROOT_NAME, arr.Select(x =>
            {
                var e = new XElement(Constants.XML_OUTLET_ELEMENT_NAME);
                e.SetAttributeValue(Constants.XML_OUTLET_ATTRIBUTE_FIELD_ID, x);
                return e;
            })).ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Gets the type of the filter by.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        private FilterBaseForm GetFilterByType(FilterType filter)
        {
            FilterBaseForm filterForm = null;

            switch (filter)
            {
                case FilterType.Clipboard:
                    {
                        filterForm = new FilterClipboardForm(ChannelId) { CommentRequired = CommentsForFiltersRequired };
                        break;
                    }
                case FilterType.Region:
                    {
                        filterForm = new FilterRegionForm(ChannelId) { CommentRequired = CommentsForFiltersRequired };
                        break;
                    }
                case FilterType.ThomasResearch:
                    {
                        filterForm = new FilterThomasResearchForm(ChannelId);
                        break;
                    }
                case FilterType.Custom:
                    {
                        if (CustomFilterShowing != null)
                        {
                            CustomFilterShowingArgs args = new CustomFilterShowingArgs(ChannelId);
                            CustomFilterShowing(args);
                            return args.FilterForm;
                        }
                        break;
                    }
            }
            if (!string.IsNullOrEmpty(InsertButtonText))
                filterForm.OKButtonText = InsertButtonText;
            return filterForm;
        }

        /// <summary>
        /// Updates the buttons status.
        /// </summary>
        private void UpdateButtonsStatus()
        {
            switch (EditMode)
            {
                case AddressEditMode.AllowAddOnly:
                    barBtnDelete.Enabled = false;
                    addPocDdl.Enabled = true;
                    break;
                case AddressEditMode.AllowRemoveOnly:
                    addPocDdl.Enabled = false;
                    barBtnDelete.Enabled = true;
                    break;
                case AddressEditMode.AllowAll:
                    barBtnDelete.Enabled = true;
                    addPocDdl.Enabled = true;
                    break;
                case AddressEditMode.ReadOnly:
                default:
                    barBtnDelete.Enabled = false;
                    addPocDdl.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Finds the grid.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        private GridControl FindGrid(Control control)
        {
            foreach (Control child in control.Controls)
            {
                if (child is GridControl)
                {
                    return child as GridControl;
                }

                GridControl grid = FindGrid(child);
                if (grid != null)
                {
                    return grid;
                }
            }

            return null;
        }

        /// <summary>
        /// The FilterChanged delegate.
        /// </summary>
        public delegate void FilterChanged(FilterChangedArgs args);

        /// <summary>
        /// The FilterShowing delegate.
        /// </summary>
        public delegate void CustomFilterShowingHandler(CustomFilterShowingArgs args);

        /// <summary>
        /// Handles the Click event of the barButtonDisplayFilter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonDisplayFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            FilterDisplayedAddresses();
        }

        private void gridViewSummary_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            GridColumnSummaryItem item = e.Item as GridColumnSummaryItem;
            if (e.SummaryProcess != CustomSummaryProcess.Finalize || !e.IsTotalSummary || item == null) 
                return;
            GridColumn column = gridViewSummary.Columns.ColumnByFieldName(item.FieldName);
            decimal val1;
            decimal val2;
            if (column == gridColumnCPartInbev)
            {
                val1 = ConvertEx.ToDecimal(gridColumnCCountInbev.SummaryItem.SummaryValue);
                val2 = ConvertEx.ToDecimal(gridColumnCCountOthers.SummaryItem.SummaryValue);
            } else if (column == gridColumnCPartOthers)
            {
                val1 = ConvertEx.ToDecimal(gridColumnCCountOthers.SummaryItem.SummaryValue);
                val2 = ConvertEx.ToDecimal(gridColumnCCountInbev.SummaryItem.SummaryValue);
            } else if (column == gridColumnFPartInbev)
            {
                val1 = ConvertEx.ToDecimal(gridColumnFCountInbev.SummaryItem.SummaryValue);
                val2 = ConvertEx.ToDecimal(gridColumnFCountOthers.SummaryItem.SummaryValue);
            }
            else if (column == gridColumnFPartOthers)
            {
                val1 = ConvertEx.ToDecimal(gridColumnFCountOthers.SummaryItem.SummaryValue);
                val2 = ConvertEx.ToDecimal(gridColumnFCountInbev.SummaryItem.SummaryValue);
            }
            else return;

            e.TotalValue = (val1 + val2) == 0 ? 0 : val1/(val1 + val2);
            e.TotalValueReady = true;

        }
    }
}
