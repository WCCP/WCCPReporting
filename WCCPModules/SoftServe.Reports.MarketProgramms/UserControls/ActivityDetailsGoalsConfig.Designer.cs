﻿namespace SoftServe.Reports.MarketProgramms.UserControls
{
    partial class ActivityDetailsGoalsConfig
    {
      /// <summary> 
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
        if (disposing && (components != null))
        {
          components.Dispose();
        }
        base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary> 
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.columnCompetitorSKUCB = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.columnBonusProductCB = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ColumnCorrectionVCoefficientTE = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnMaxDiscountProcSE = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.columnPriceTypeCB = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.columnPriceTE = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnRevaluationTE = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnShippedCE = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerKPI = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.listGoals = new DevExpress.XtraEditors.ListBoxControl();
            this.groupAims = new DevExpress.XtraEditors.GroupControl();
            this.checkShowAll = new DevExpress.XtraEditors.CheckEdit();
            this.labelEdit = new DevExpress.XtraEditors.LabelControl();
            this.textCurAimName = new DevExpress.XtraEditors.TextEdit();
            this.panelGoalConf = new DevExpress.XtraEditors.PanelControl();
            this.lcpFactsCalcWay = new DevExpress.XtraEditors.LookUpEdit();
            this.lcpPeriodCalk = new DevExpress.XtraEditors.LookUpEdit();
            this.lblFactCalcway = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodCalc = new DevExpress.XtraEditors.LabelControl();
            this.checkCannotChangeSKU = new DevExpress.XtraEditors.CheckEdit();
            this.checkCannotChangeKPI = new DevExpress.XtraEditors.CheckEdit();
            this.groupGoalsPlanning = new DevExpress.XtraEditors.GroupControl();
            this.checkUseAimValue = new DevExpress.XtraEditors.CheckEdit();
            this.checkAccPerfOfTargetVal = new DevExpress.XtraEditors.CheckEdit();
            this.checkReversExecution = new DevExpress.XtraEditors.CheckEdit();
            this.simpleEdit = new DevExpress.XtraEditors.SimpleButton();
            this.cbVisitChecked = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleDelete = new DevExpress.XtraEditors.SimpleButton();
            this.cbGoalType = new DevExpress.XtraEditors.LookUpEdit();
            this.checkUseInCalculations = new DevExpress.XtraEditors.CheckEdit();
            this.simpleAdd = new DevExpress.XtraEditors.SimpleButton();
            this.labelVisitChecked = new DevExpress.XtraEditors.LabelControl();
            this.labelGoalType = new DevExpress.XtraEditors.LabelControl();
            this.groupTragetSplit = new DevExpress.XtraEditors.GroupControl();
            this.checkAllowMultipleGoals = new DevExpress.XtraEditors.CheckEdit();
            this.checkSplitByPOCType = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbSplitByPOCType = new DevExpress.XtraEditors.LookUpEdit();
            this.cbSplitByTime = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupQPQDigit = new DevExpress.XtraEditors.GroupControl();
            this.qpqTreeDigit = new Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree();
            this.groupQPQLogic = new DevExpress.XtraEditors.GroupControl();
            this.qpqTreeLogic = new Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree();
            this.groupSKU = new DevExpress.XtraEditors.GroupControl();
            this.skuTreeCtrl = new Logica.Reports.BaseReportControl.CommonControls.SKUTree();
            this.groupConditionsList = new DevExpress.XtraEditors.GroupControl();
            this.treeKPI = new Logica.Reports.BaseReportControl.CommonControls.KPITree();
            this.groupEffectiveness = new DevExpress.XtraEditors.GroupControl();
            this.chlbIPTRList = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.MPAGridControl = new DevExpress.XtraGrid.GridControl();
            this.MPAGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnActionSKUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnActionSKUName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCompetitorSKU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repGridColumnCompetitorSKU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnBonusProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repGridColumnBonusProduct = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnCorrectionVCoefficient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMaxDiscountProc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPriceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repGridColumnPriceType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRevaluation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnShipped = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnModifiedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerMPA = new DevExpress.XtraEditors.SplitContainerControl();
            this.rbMPAType = new DevExpress.XtraEditors.RadioGroup();
            this.xtraScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            ((System.ComponentModel.ISupportInitialize)(this.columnCompetitorSKUCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnBonusProductCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnCorrectionVCoefficientTE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnMaxDiscountProcSE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnPriceTypeCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnPriceTE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnRevaluationTE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnShippedCE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerKPI)).BeginInit();
            this.splitContainerKPI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listGoals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAims)).BeginInit();
            this.groupAims.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCurAimName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGoalConf)).BeginInit();
            this.panelGoalConf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcpFactsCalcWay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcpPeriodCalk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeSKU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeKPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupGoalsPlanning)).BeginInit();
            this.groupGoalsPlanning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseAimValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccPerfOfTargetVal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkReversExecution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVisitChecked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGoalType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseInCalculations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTragetSplit)).BeginInit();
            this.groupTragetSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllowMultipleGoals.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSplitByPOCType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByPOCType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQDigit)).BeginInit();
            this.groupQPQDigit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQLogic)).BeginInit();
            this.groupQPQLogic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupSKU)).BeginInit();
            this.groupSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupConditionsList)).BeginInit();
            this.groupConditionsList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupEffectiveness)).BeginInit();
            this.groupEffectiveness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chlbIPTRList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPAGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPAGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGridColumnCompetitorSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGridColumnBonusProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGridColumnPriceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMPA)).BeginInit();
            this.splitContainerMPA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbMPAType.Properties)).BeginInit();
            this.xtraScrollableControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnCompetitorSKUCB
            // 
            this.columnCompetitorSKUCB.AutoHeight = false;
            this.columnCompetitorSKUCB.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.columnCompetitorSKUCB.Name = "columnCompetitorSKUCB";
            // 
            // columnBonusProductCB
            // 
            this.columnBonusProductCB.AutoHeight = false;
            this.columnBonusProductCB.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.columnBonusProductCB.Name = "columnBonusProductCB";
            // 
            // ColumnCorrectionVCoefficientTE
            // 
            this.ColumnCorrectionVCoefficientTE.AutoHeight = false;
            this.ColumnCorrectionVCoefficientTE.DisplayFormat.FormatString = "{0:N2}";
            this.ColumnCorrectionVCoefficientTE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ColumnCorrectionVCoefficientTE.EditFormat.FormatString = "{0:N2}";
            this.ColumnCorrectionVCoefficientTE.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ColumnCorrectionVCoefficientTE.Name = "ColumnCorrectionVCoefficientTE";
            // 
            // columnMaxDiscountProcSE
            // 
            this.columnMaxDiscountProcSE.AutoHeight = false;
            this.columnMaxDiscountProcSE.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.columnMaxDiscountProcSE.DisplayFormat.FormatString = "{0:N0} %";
            this.columnMaxDiscountProcSE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMaxDiscountProcSE.EditFormat.FormatString = "{0:N0} %";
            this.columnMaxDiscountProcSE.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMaxDiscountProcSE.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.columnMaxDiscountProcSE.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.columnMaxDiscountProcSE.Name = "columnMaxDiscountProcSE";
            // 
            // columnPriceTypeCB
            // 
            this.columnPriceTypeCB.AutoHeight = false;
            this.columnPriceTypeCB.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.columnPriceTypeCB.Name = "columnPriceTypeCB";
            // 
            // columnPriceTE
            // 
            this.columnPriceTE.AutoHeight = false;
            this.columnPriceTE.DisplayFormat.FormatString = "{0:N2}";
            this.columnPriceTE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPriceTE.EditFormat.FormatString = "{0:N2}";
            this.columnPriceTE.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPriceTE.Name = "columnPriceTE";
            // 
            // columnRevaluationTE
            // 
            this.columnRevaluationTE.AutoHeight = false;
            this.columnRevaluationTE.DisplayFormat.FormatString = "{0:N2}";
            this.columnRevaluationTE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnRevaluationTE.EditFormat.FormatString = "{0:N2}";
            this.columnRevaluationTE.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnRevaluationTE.Name = "columnRevaluationTE";
            // 
            // columnShippedCE
            // 
            this.columnShippedCE.AutoHeight = false;
            this.columnShippedCE.Name = "columnShippedCE";
            this.columnShippedCE.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // splitContainerKPI
            // 
            this.splitContainerKPI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerKPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerKPI.Location = new System.Drawing.Point(0, 0);
            this.splitContainerKPI.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainerKPI.Name = "splitContainerKPI";
            this.splitContainerKPI.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerKPI.Panel1.MinSize = 565;
            this.splitContainerKPI.Panel1.Text = "Panel1";
            this.splitContainerKPI.Panel2.Controls.Add(this.groupQPQDigit);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupQPQLogic);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupSKU);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupConditionsList);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupEffectiveness);
            this.splitContainerKPI.Panel2.MinSize = 250;
            this.splitContainerKPI.Panel2.Text = "Panel2";
            this.splitContainerKPI.Size = new System.Drawing.Size(1000, 567);
            this.splitContainerKPI.SplitterPosition = 576;
            this.splitContainerKPI.TabIndex = 19;
            this.splitContainerKPI.Text = "splitContainerKPI";
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowDrop = true;
            this.layoutControl1.AutoScroll = false;
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.groupAims);
            this.layoutControl1.Controls.Add(this.panelGoalConf);
            this.layoutControl1.Controls.Add(this.groupGoalsPlanning);
            this.layoutControl1.Controls.Add(this.groupTragetSplit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(375, 116, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(576, 563);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.listGoals);
            this.panelControl1.Location = new System.Drawing.Point(294, 116);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(280, 445);
            this.panelControl1.TabIndex = 27;
            // 
            // listGoals
            // 
            this.listGoals.DisplayMember = "Text";
            this.listGoals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listGoals.HorizontalScrollbar = true;
            this.listGoals.Location = new System.Drawing.Point(2, 2);
            this.listGoals.Margin = new System.Windows.Forms.Padding(2);
            this.listGoals.Name = "listGoals";
            this.listGoals.Size = new System.Drawing.Size(276, 441);
            this.listGoals.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listGoals.TabIndex = 0;
            this.listGoals.ValueMember = "Id";
            this.listGoals.SelectedIndexChanged += new System.EventHandler(this.listGoals_SelectedIndexChanged);
            // 
            // groupAims
            // 
            this.groupAims.Controls.Add(this.checkShowAll);
            this.groupAims.Controls.Add(this.labelEdit);
            this.groupAims.Controls.Add(this.textCurAimName);
            this.groupAims.Location = new System.Drawing.Point(294, 2);
            this.groupAims.Margin = new System.Windows.Forms.Padding(2);
            this.groupAims.MaximumSize = new System.Drawing.Size(0, 110);
            this.groupAims.MinimumSize = new System.Drawing.Size(0, 110);
            this.groupAims.Name = "groupAims";
            this.groupAims.Padding = new System.Windows.Forms.Padding(8);
            this.groupAims.Size = new System.Drawing.Size(280, 110);
            this.groupAims.TabIndex = 25;
            this.groupAims.Text = "Цели:";
            // 
            // checkShowAll
            // 
            this.checkShowAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkShowAll.Location = new System.Drawing.Point(10, 29);
            this.checkShowAll.Name = "checkShowAll";
            this.checkShowAll.Properties.Caption = "Показать все цели";
            this.checkShowAll.Size = new System.Drawing.Size(260, 19);
            this.checkShowAll.TabIndex = 29;
            this.checkShowAll.CheckedChanged += new System.EventHandler(this.checkShowAll_CheckedChanged);
            // 
            // labelEdit
            // 
            this.labelEdit.Location = new System.Drawing.Point(13, 64);
            this.labelEdit.Name = "labelEdit";
            this.labelEdit.Size = new System.Drawing.Size(79, 13);
            this.labelEdit.TabIndex = 11;
            this.labelEdit.Text = "Название цели:";
            this.labelEdit.Visible = false;
            // 
            // textCurAimName
            // 
            this.textCurAimName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textCurAimName.Location = new System.Drawing.Point(10, 80);
            this.textCurAimName.Margin = new System.Windows.Forms.Padding(2);
            this.textCurAimName.Name = "textCurAimName";
            this.textCurAimName.Size = new System.Drawing.Size(260, 20);
            this.textCurAimName.TabIndex = 1;
            this.textCurAimName.Visible = false;
            // 
            // panelGoalConf
            // 
            this.panelGoalConf.Controls.Add(this.lcpFactsCalcWay);
            this.panelGoalConf.Controls.Add(this.lcpPeriodCalk);
            this.panelGoalConf.Controls.Add(this.lblFactCalcway);
            this.panelGoalConf.Controls.Add(this.lblPeriodCalc);
            this.panelGoalConf.Controls.Add(this.checkCannotChangeSKU);
            this.panelGoalConf.Controls.Add(this.checkCannotChangeKPI);
            this.panelGoalConf.Location = new System.Drawing.Point(2, 364);
            this.panelGoalConf.Margin = new System.Windows.Forms.Padding(2);
            this.panelGoalConf.Name = "panelGoalConf";
            this.panelGoalConf.Padding = new System.Windows.Forms.Padding(8);
            this.panelGoalConf.Size = new System.Drawing.Size(288, 197);
            this.panelGoalConf.TabIndex = 24;
            // 
            // lcpFactsCalcWay
            // 
            this.lcpFactsCalcWay.Location = new System.Drawing.Point(172, 43);
            this.lcpFactsCalcWay.Name = "lcpFactsCalcWay";
            this.lcpFactsCalcWay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lcpFactsCalcWay.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "CalcType"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lcpFactsCalcWay.Properties.DisplayMember = "Text";
            this.lcpFactsCalcWay.Properties.DropDownRows = 2;
            this.lcpFactsCalcWay.Properties.PopupFormMinSize = new System.Drawing.Size(120, 0);
            this.lcpFactsCalcWay.Properties.PopupWidth = 120;
            this.lcpFactsCalcWay.Properties.ShowFooter = false;
            this.lcpFactsCalcWay.Properties.ShowHeader = false;
            this.lcpFactsCalcWay.Properties.ValueMember = "Id";
            this.lcpFactsCalcWay.Size = new System.Drawing.Size(100, 20);
            this.lcpFactsCalcWay.TabIndex = 36;
            // 
            // lcpPeriodCalk
            // 
            this.lcpPeriodCalk.Location = new System.Drawing.Point(172, 12);
            this.lcpPeriodCalk.Name = "lcpPeriodCalk";
            this.lcpPeriodCalk.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lcpPeriodCalk.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "Month", 20, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lcpPeriodCalk.Properties.DisplayMember = "Text";
            this.lcpPeriodCalk.Properties.PopupFormMinSize = new System.Drawing.Size(100, 0);
            this.lcpPeriodCalk.Properties.PopupWidth = 100;
            this.lcpPeriodCalk.Properties.ShowFooter = false;
            this.lcpPeriodCalk.Properties.ShowHeader = false;
            this.lcpPeriodCalk.Properties.ValueMember = "Id";
            this.lcpPeriodCalk.Size = new System.Drawing.Size(100, 20);
            this.lcpPeriodCalk.TabIndex = 35;
            // 
            // lblFactCalcway
            // 
            this.lblFactCalcway.Location = new System.Drawing.Point(12, 46);
            this.lblFactCalcway.Name = "lblFactCalcway";
            this.lblFactCalcway.Size = new System.Drawing.Size(132, 13);
            this.lblFactCalcway.TabIndex = 34;
            this.lblFactCalcway.Text = "Способ подсчета фактов:";
            // 
            // lblPeriodCalc
            // 
            this.lblPeriodCalc.Location = new System.Drawing.Point(12, 15);
            this.lblPeriodCalc.Name = "lblPeriodCalc";
            this.lblPeriodCalc.Size = new System.Drawing.Size(151, 13);
            this.lblPeriodCalc.TabIndex = 33;
            this.lblPeriodCalc.Text = "Расчетный период (месяцев):";
            // 
            // checkCannotChangeSKU
            // 
            this.checkCannotChangeSKU.AutoSizeInLayoutControl = true;
            this.checkCannotChangeSKU.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkCannotChangeSKU.Enabled = false;
            this.checkCannotChangeSKU.Location = new System.Drawing.Point(10, 149);
            this.checkCannotChangeSKU.Name = "checkCannotChangeSKU";
            this.checkCannotChangeSKU.Properties.Caption = "Запретить менять СКЮ в активностях";
            this.checkCannotChangeSKU.Size = new System.Drawing.Size(268, 19);
            this.checkCannotChangeSKU.TabIndex = 19;
            // 
            // checkCannotChangeKPI
            // 
            this.checkCannotChangeKPI.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkCannotChangeKPI.Enabled = false;
            this.checkCannotChangeKPI.Location = new System.Drawing.Point(10, 168);
            this.checkCannotChangeKPI.Name = "checkCannotChangeKPI";
            this.checkCannotChangeKPI.Properties.Caption = "Запретить менять условия в активностях";
            this.checkCannotChangeKPI.Size = new System.Drawing.Size(268, 19);
            this.checkCannotChangeKPI.TabIndex = 19;
            // 
            // groupGoalsPlanning
            // 
            this.groupGoalsPlanning.Controls.Add(this.checkUseAimValue);
            this.groupGoalsPlanning.Controls.Add(this.checkAccPerfOfTargetVal);
            this.groupGoalsPlanning.Controls.Add(this.checkReversExecution);
            this.groupGoalsPlanning.Controls.Add(this.simpleEdit);
            this.groupGoalsPlanning.Controls.Add(this.cbVisitChecked);
            this.groupGoalsPlanning.Controls.Add(this.simpleSave);
            this.groupGoalsPlanning.Controls.Add(this.simpleDelete);
            this.groupGoalsPlanning.Controls.Add(this.cbGoalType);
            this.groupGoalsPlanning.Controls.Add(this.checkUseInCalculations);
            this.groupGoalsPlanning.Controls.Add(this.simpleAdd);
            this.groupGoalsPlanning.Controls.Add(this.labelVisitChecked);
            this.groupGoalsPlanning.Controls.Add(this.labelGoalType);
            this.groupGoalsPlanning.Location = new System.Drawing.Point(2, 116);
            this.groupGoalsPlanning.Margin = new System.Windows.Forms.Padding(2);
            this.groupGoalsPlanning.MaximumSize = new System.Drawing.Size(0, 244);
            this.groupGoalsPlanning.MinimumSize = new System.Drawing.Size(0, 244);
            this.groupGoalsPlanning.Name = "groupGoalsPlanning";
            this.groupGoalsPlanning.Padding = new System.Windows.Forms.Padding(8);
            this.groupGoalsPlanning.Size = new System.Drawing.Size(288, 244);
            this.groupGoalsPlanning.TabIndex = 26;
            this.groupGoalsPlanning.Text = "Планирование целей:";
            // 
            // checkUseAimValue
            // 
            this.checkUseAimValue.Location = new System.Drawing.Point(10, 142);
            this.checkUseAimValue.Name = "checkUseAimValue";
            this.checkUseAimValue.Properties.Caption = "Использовать целевое значение";
            this.checkUseAimValue.Size = new System.Drawing.Size(261, 19);
            this.checkUseAimValue.TabIndex = 34;
            this.checkUseAimValue.CheckedChanged += new System.EventHandler(this.checkUseAimValue_CheckedChanged);
            // 
            // checkAccPerfOfTargetVal
            // 
            this.checkAccPerfOfTargetVal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkAccPerfOfTargetVal.Location = new System.Drawing.Point(10, 177);
            this.checkAccPerfOfTargetVal.Name = "checkAccPerfOfTargetVal";
            this.checkAccPerfOfTargetVal.Properties.Caption = "Учитывать выполнение по целевому значению";
            this.checkAccPerfOfTargetVal.Size = new System.Drawing.Size(268, 19);
            this.checkAccPerfOfTargetVal.TabIndex = 33;
            // 
            // checkReversExecution
            // 
            this.checkReversExecution.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkReversExecution.Location = new System.Drawing.Point(10, 196);
            this.checkReversExecution.Name = "checkReversExecution";
            this.checkReversExecution.Properties.Caption = "Обратное выполнение";
            this.checkReversExecution.Size = new System.Drawing.Size(268, 19);
            this.checkReversExecution.TabIndex = 31;
            // 
            // simpleEdit
            // 
            this.simpleEdit.Image = global::SoftServe.Reports.MarketProgramms.Resource.edit;
            this.simpleEdit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleEdit.Location = new System.Drawing.Point(82, 31);
            this.simpleEdit.Margin = new System.Windows.Forms.Padding(2);
            this.simpleEdit.Name = "simpleEdit";
            this.simpleEdit.Size = new System.Drawing.Size(30, 32);
            this.simpleEdit.TabIndex = 29;
            this.simpleEdit.ToolTip = "Редактировать цель";
            this.simpleEdit.Click += new System.EventHandler(this.simpleEdit_Click);
            // 
            // cbVisitChecked
            // 
            this.cbVisitChecked.Location = new System.Drawing.Point(74, 104);
            this.cbVisitChecked.Margin = new System.Windows.Forms.Padding(2);
            this.cbVisitChecked.Name = "cbVisitChecked";
            this.cbVisitChecked.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbVisitChecked.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "Период")});
            this.cbVisitChecked.Properties.DisplayMember = "Text";
            this.cbVisitChecked.Properties.ValueMember = "Id";
            this.cbVisitChecked.Size = new System.Drawing.Size(198, 20);
            this.cbVisitChecked.TabIndex = 27;
            // 
            // simpleSave
            // 
            this.simpleSave.Image = global::SoftServe.Reports.MarketProgramms.Resource.save;
            this.simpleSave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleSave.Location = new System.Drawing.Point(46, 31);
            this.simpleSave.Margin = new System.Windows.Forms.Padding(2);
            this.simpleSave.Name = "simpleSave";
            this.simpleSave.Size = new System.Drawing.Size(30, 32);
            this.simpleSave.TabIndex = 28;
            this.simpleSave.ToolTip = "Сохранить цель";
            this.simpleSave.Click += new System.EventHandler(this.simpleSaveAim_Click);
            // 
            // simpleDelete
            // 
            this.simpleDelete.Image = global::SoftServe.Reports.MarketProgramms.Resource.delete;
            this.simpleDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleDelete.Location = new System.Drawing.Point(117, 31);
            this.simpleDelete.Margin = new System.Windows.Forms.Padding(2);
            this.simpleDelete.Name = "simpleDelete";
            this.simpleDelete.Size = new System.Drawing.Size(30, 32);
            this.simpleDelete.TabIndex = 25;
            this.simpleDelete.ToolTip = "Удалить цель";
            this.simpleDelete.Click += new System.EventHandler(this.simpleDeleteGoal_Click);
            // 
            // cbGoalType
            // 
            this.cbGoalType.Location = new System.Drawing.Point(74, 76);
            this.cbGoalType.Margin = new System.Windows.Forms.Padding(2);
            this.cbGoalType.Name = "cbGoalType";
            this.cbGoalType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGoalType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AimType_Name", "Тип")});
            this.cbGoalType.Properties.DisplayMember = "AimType_Name";
            this.cbGoalType.Properties.ValueMember = "AimType_Id";
            this.cbGoalType.Size = new System.Drawing.Size(198, 20);
            this.cbGoalType.TabIndex = 26;
            this.cbGoalType.EditValueChanged += new System.EventHandler(this.cbGoalType_EditValueChanged);
            this.cbGoalType.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cbGoalType_EditValueChanging);
            // 
            // checkUseInCalculations
            // 
            this.checkUseInCalculations.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkUseInCalculations.Location = new System.Drawing.Point(10, 215);
            this.checkUseInCalculations.Name = "checkUseInCalculations";
            this.checkUseInCalculations.Properties.Caption = "Учитывать  при выполнении";
            this.checkUseInCalculations.Size = new System.Drawing.Size(268, 19);
            this.checkUseInCalculations.TabIndex = 11;
            // 
            // simpleAdd
            // 
            this.simpleAdd.Image = global::SoftServe.Reports.MarketProgramms.Resource.add;
            this.simpleAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleAdd.Location = new System.Drawing.Point(12, 31);
            this.simpleAdd.Margin = new System.Windows.Forms.Padding(2);
            this.simpleAdd.Name = "simpleAdd";
            this.simpleAdd.Size = new System.Drawing.Size(30, 32);
            this.simpleAdd.TabIndex = 0;
            this.simpleAdd.ToolTip = "Добавить цель";
            this.simpleAdd.Click += new System.EventHandler(this.simpleAddGoal_Click);
            // 
            // labelVisitChecked
            // 
            this.labelVisitChecked.Location = new System.Drawing.Point(13, 107);
            this.labelVisitChecked.Name = "labelVisitChecked";
            this.labelVisitChecked.Size = new System.Drawing.Size(41, 13);
            this.labelVisitChecked.TabIndex = 24;
            this.labelVisitChecked.Text = "Визиты:";
            // 
            // labelGoalType
            // 
            this.labelGoalType.Location = new System.Drawing.Point(12, 79);
            this.labelGoalType.Name = "labelGoalType";
            this.labelGoalType.Size = new System.Drawing.Size(49, 13);
            this.labelGoalType.TabIndex = 10;
            this.labelGoalType.Text = "Тип цели:";
            // 
            // groupTragetSplit
            // 
            this.groupTragetSplit.Controls.Add(this.checkAllowMultipleGoals);
            this.groupTragetSplit.Controls.Add(this.checkSplitByPOCType);
            this.groupTragetSplit.Controls.Add(this.labelControl1);
            this.groupTragetSplit.Controls.Add(this.labelControl2);
            this.groupTragetSplit.Controls.Add(this.cbSplitByPOCType);
            this.groupTragetSplit.Controls.Add(this.cbSplitByTime);
            this.groupTragetSplit.Location = new System.Drawing.Point(2, 2);
            this.groupTragetSplit.MaximumSize = new System.Drawing.Size(288, 110);
            this.groupTragetSplit.MinimumSize = new System.Drawing.Size(288, 110);
            this.groupTragetSplit.Name = "groupTragetSplit";
            this.groupTragetSplit.Padding = new System.Windows.Forms.Padding(8);
            this.groupTragetSplit.Size = new System.Drawing.Size(288, 110);
            this.groupTragetSplit.TabIndex = 22;
            this.groupTragetSplit.Text = "Разрезы планирования целей:";
            // 
            // checkAllowMultipleGoals
            // 
            this.checkAllowMultipleGoals.Location = new System.Drawing.Point(9, 31);
            this.checkAllowMultipleGoals.Name = "checkAllowMultipleGoals";
            this.checkAllowMultipleGoals.Properties.Caption = "Разрешить добавлять больше одной цели";
            this.checkAllowMultipleGoals.Size = new System.Drawing.Size(247, 19);
            this.checkAllowMultipleGoals.TabIndex = 10;
            this.checkAllowMultipleGoals.CheckedChanged += new System.EventHandler(this.checkAllowMultipleGoals_CheckedChanged);
            // 
            // checkSplitByPOCType
            // 
            this.checkSplitByPOCType.Location = new System.Drawing.Point(4, 98);
            this.checkSplitByPOCType.Name = "checkSplitByPOCType";
            this.checkSplitByPOCType.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.checkSplitByPOCType.Properties.Appearance.Options.UseForeColor = true;
            this.checkSplitByPOCType.Properties.Caption = "Разбивка целей по типу ТТ";
            this.checkSplitByPOCType.Size = new System.Drawing.Size(178, 19);
            this.checkSplitByPOCType.TabIndex = 9;
            this.checkSplitByPOCType.Visible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(14, 130);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(145, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "По территории и персоналу:";
            this.labelControl1.Visible = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 67);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(62, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "По времени:";
            this.labelControl2.Visible = false;
            // 
            // cbSplitByPOCType
            // 
            this.cbSplitByPOCType.Location = new System.Drawing.Point(4, 141);
            this.cbSplitByPOCType.Name = "cbSplitByPOCType";
            this.cbSplitByPOCType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSplitByPOCType.Properties.DisplayMember = "TargetRegion_Name";
            this.cbSplitByPOCType.Properties.NullText = "<Не задано>";
            this.cbSplitByPOCType.Properties.ShowFooter = false;
            this.cbSplitByPOCType.Properties.ShowHeader = false;
            this.cbSplitByPOCType.Properties.ValueMember = "TargetRegion_ID";
            this.cbSplitByPOCType.Size = new System.Drawing.Size(245, 20);
            this.cbSplitByPOCType.TabIndex = 5;
            this.cbSplitByPOCType.Visible = false;
            // 
            // cbSplitByTime
            // 
            this.cbSplitByTime.Location = new System.Drawing.Point(4, 77);
            this.cbSplitByTime.Name = "cbSplitByTime";
            this.cbSplitByTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSplitByTime.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TargetPeriod_Name", "Период")});
            this.cbSplitByTime.Properties.DisplayMember = "TargetPeriod_Name";
            this.cbSplitByTime.Properties.NullText = "<Не задано>";
            this.cbSplitByTime.Properties.ValueMember = "TargetPeriod_ID";
            this.cbSplitByTime.Size = new System.Drawing.Size(245, 20);
            this.cbSplitByTime.TabIndex = 5;
            this.cbSplitByTime.Visible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(576, 563);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.groupTragetSplit;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(292, 114);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.groupGoalsPlanning;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 114);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 248);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(24, 248);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(292, 248);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelGoalConf;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 362);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(292, 201);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.groupAims;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(292, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(284, 114);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.panelControl1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(292, 114);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(284, 449);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // groupQPQDigit
            // 
            this.groupQPQDigit.Controls.Add(this.qpqTreeDigit);
            this.groupQPQDigit.Location = new System.Drawing.Point(153, 132);
            this.groupQPQDigit.Name = "groupQPQDigit";
            this.groupQPQDigit.Size = new System.Drawing.Size(102, 112);
            this.groupQPQDigit.TabIndex = 28;
            this.groupQPQDigit.Text = "Список условий активностей:";
            // 
            // qpqTreeDigit
            // 
            this.qpqTreeDigit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qpqTreeDigit.Location = new System.Drawing.Point(2, 21);
            this.qpqTreeDigit.Margin = new System.Windows.Forms.Padding(0);
            this.qpqTreeDigit.Name = "qpqTreeDigit";
            this.qpqTreeDigit.ReadOnly = false;
            this.qpqTreeDigit.SelectedStringIdList = null;
            this.qpqTreeDigit.Size = new System.Drawing.Size(98, 89);
            this.qpqTreeDigit.TabIndex = 0;
            // 
            // groupQPQLogic
            // 
            this.groupQPQLogic.Controls.Add(this.qpqTreeLogic);
            this.groupQPQLogic.Location = new System.Drawing.Point(151, 10);
            this.groupQPQLogic.Name = "groupQPQLogic";
            this.groupQPQLogic.Size = new System.Drawing.Size(102, 112);
            this.groupQPQLogic.TabIndex = 27;
            this.groupQPQLogic.Text = "Список условий активностей:";
            // 
            // qpqTreeLogic
            // 
            this.qpqTreeLogic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qpqTreeLogic.Location = new System.Drawing.Point(2, 21);
            this.qpqTreeLogic.Margin = new System.Windows.Forms.Padding(0);
            this.qpqTreeLogic.Name = "qpqTreeLogic";
            this.qpqTreeLogic.ReadOnly = false;
            this.qpqTreeLogic.SelectedStringIdList = null;
            this.qpqTreeLogic.Size = new System.Drawing.Size(98, 89);
            this.qpqTreeLogic.TabIndex = 0;
            // 
            // groupSKU
            // 
            this.groupSKU.Controls.Add(this.skuTreeCtrl);
            this.groupSKU.Location = new System.Drawing.Point(19, 179);
            this.groupSKU.Name = "groupSKU";
            this.groupSKU.Size = new System.Drawing.Size(102, 149);
            this.groupSKU.TabIndex = 25;
            this.groupSKU.Text = "Список СКЮ для целей по V продаж:";
            // 
            // skuTreeCtrl
            // 
            this.skuTreeCtrl.AllowedLevel = 0;
            this.skuTreeCtrl.AutoScroll = true;
            this.skuTreeCtrl.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.skuTreeCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skuTreeCtrl.Location = new System.Drawing.Point(2, 21);
            this.skuTreeCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.skuTreeCtrl.Name = "skuTreeCtrl";
            this.skuTreeCtrl.ReadOnly = false;
            this.skuTreeCtrl.Size = new System.Drawing.Size(98, 126);
            this.skuTreeCtrl.TabIndex = 2;
            this.skuTreeCtrl.OnValueChanged += new Logica.Reports.BaseReportControl.CommonControls.SKUTree.ValueChanged(this.skuTreeCtrl_OnValueChanged);
            // 
            // groupConditionsList
            // 
            this.groupConditionsList.Controls.Add(this.treeKPI);
            this.groupConditionsList.Location = new System.Drawing.Point(19, 81);
            this.groupConditionsList.Name = "groupConditionsList";
            this.groupConditionsList.Size = new System.Drawing.Size(102, 93);
            this.groupConditionsList.TabIndex = 24;
            this.groupConditionsList.Text = "Список условий активностей:";
            // 
            // treeKPI
            // 
            this.treeKPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeKPI.Location = new System.Drawing.Point(2, 21);
            this.treeKPI.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeKPI.Name = "treeKPI";
            this.treeKPI.ReadOnly = false;
            this.treeKPI.Size = new System.Drawing.Size(98, 70);
            this.treeKPI.TabIndex = 19;
            // 
            // groupEffectiveness
            // 
            this.groupEffectiveness.Controls.Add(this.chlbIPTRList);
            this.groupEffectiveness.Location = new System.Drawing.Point(17, 10);
            this.groupEffectiveness.Margin = new System.Windows.Forms.Padding(2);
            this.groupEffectiveness.Name = "groupEffectiveness";
            this.groupEffectiveness.Size = new System.Drawing.Size(104, 68);
            this.groupEffectiveness.TabIndex = 22;
            this.groupEffectiveness.Text = "Эффективность:";
            this.groupEffectiveness.UseDisabledStatePainter = false;
            // 
            // chlbIPTRList
            // 
            this.chlbIPTRList.CheckOnClick = true;
            this.chlbIPTRList.DisplayMember = "ClassName";
            this.chlbIPTRList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chlbIPTRList.Location = new System.Drawing.Point(2, 21);
            this.chlbIPTRList.Margin = new System.Windows.Forms.Padding(2);
            this.chlbIPTRList.Name = "chlbIPTRList";
            this.chlbIPTRList.Size = new System.Drawing.Size(100, 45);
            this.chlbIPTRList.TabIndex = 1;
            this.chlbIPTRList.ValueMember = "EquipmentClass_ID";
            this.chlbIPTRList.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.chlbIPTRList_ItemCheck);
            // 
            // MPAGridControl
            // 
            this.MPAGridControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MPAGridControl.Location = new System.Drawing.Point(0, 98);
            this.MPAGridControl.MainView = this.MPAGridView;
            this.MPAGridControl.Name = "MPAGridControl";
            this.MPAGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repGridColumnCompetitorSKU,
            this.repGridColumnPriceType,
            this.repGridColumnBonusProduct});
            this.MPAGridControl.Size = new System.Drawing.Size(1000, 74);
            this.MPAGridControl.TabIndex = 6;
            this.MPAGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.MPAGridView});
            // 
            // MPAGridView
            // 
            this.MPAGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.MPAGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MPAGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MPAGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MPAGridView.ColumnPanelRowHeight = 40;
            this.MPAGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnID,
            this.gridColumnActionSKUID,
            this.gridColumnActionSKUName,
            this.gridColumnCompetitorSKU,
            this.gridColumnBonusProduct,
            this.gridColumnCorrectionVCoefficient,
            this.gridColumnMaxDiscountProc,
            this.gridColumnPriceType,
            this.gridColumnPrice,
            this.gridColumnRevaluation,
            this.gridColumnShipped,
            this.gridColumnStatus,
            this.gridColumnModified,
            this.gridColumnModifiedBy});
            this.MPAGridView.GridControl = this.MPAGridControl;
            this.MPAGridView.Name = "MPAGridView";
            this.MPAGridView.OptionsCustomization.AllowColumnMoving = false;
            this.MPAGridView.OptionsCustomization.AllowFilter = false;
            this.MPAGridView.OptionsCustomization.AllowGroup = false;
            this.MPAGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.MPAGridView.OptionsNavigation.AutoFocusNewRow = true;
            this.MPAGridView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.MPAGridView.OptionsView.ShowGroupPanel = false;
            this.MPAGridView.OptionsView.ShowIndicator = false;
            // 
            // gridColumnID
            // 
            this.gridColumnID.FieldName = "ID";
            this.gridColumnID.Name = "gridColumnID";
            // 
            // gridColumnActionSKUID
            // 
            this.gridColumnActionSKUID.FieldName = "ProductCn_ID";
            this.gridColumnActionSKUID.Name = "gridColumnActionSKUID";
            // 
            // gridColumnActionSKUName
            // 
            this.gridColumnActionSKUName.Caption = "Акционный комби продукт";
            this.gridColumnActionSKUName.FieldName = "ProductCnName";
            this.gridColumnActionSKUName.Name = "gridColumnActionSKUName";
            this.gridColumnActionSKUName.OptionsColumn.AllowEdit = false;
            this.gridColumnActionSKUName.Visible = true;
            this.gridColumnActionSKUName.VisibleIndex = 0;
            this.gridColumnActionSKUName.Width = 147;
            // 
            // gridColumnCompetitorSKU
            // 
            this.gridColumnCompetitorSKU.Caption = "Конкурентное СКЮ";
            this.gridColumnCompetitorSKU.ColumnEdit = this.repGridColumnCompetitorSKU;
            this.gridColumnCompetitorSKU.FieldName = "ConcurrentCn_ID";
            this.gridColumnCompetitorSKU.Name = "gridColumnCompetitorSKU";
            this.gridColumnCompetitorSKU.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumnCompetitorSKU.OptionsFilter.AllowFilter = false;
            this.gridColumnCompetitorSKU.Visible = true;
            this.gridColumnCompetitorSKU.VisibleIndex = 1;
            this.gridColumnCompetitorSKU.Width = 136;
            // 
            // repGridColumnCompetitorSKU
            // 
            this.repGridColumnCompetitorSKU.AutoHeight = false;
            this.repGridColumnCompetitorSKU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repGridColumnCompetitorSKU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ProductCn_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ProductCnName", "Конкурентное SKU")});
            this.repGridColumnCompetitorSKU.DisplayMember = "ProductCnName";
            this.repGridColumnCompetitorSKU.Name = "repGridColumnCompetitorSKU";
            this.repGridColumnCompetitorSKU.NullText = "Выберите SKU";
            this.repGridColumnCompetitorSKU.ValueMember = "ProductCn_Id";
            // 
            // gridColumnBonusProduct
            // 
            this.gridColumnBonusProduct.Caption = "Бонусный продукт";
            this.gridColumnBonusProduct.ColumnEdit = this.repGridColumnBonusProduct;
            this.gridColumnBonusProduct.FieldName = "BonusProduct_ID";
            this.gridColumnBonusProduct.Name = "gridColumnBonusProduct";
            this.gridColumnBonusProduct.Visible = true;
            this.gridColumnBonusProduct.VisibleIndex = 2;
            this.gridColumnBonusProduct.Width = 106;
            // 
            // repGridColumnBonusProduct
            // 
            this.repGridColumnBonusProduct.AutoHeight = false;
            this.repGridColumnBonusProduct.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repGridColumnBonusProduct.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Product_Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ProductName", "Бонусный продукт")});
            this.repGridColumnBonusProduct.DisplayMember = "ProductName";
            this.repGridColumnBonusProduct.Name = "repGridColumnBonusProduct";
            this.repGridColumnBonusProduct.NullText = "Виберите бонусный продукт";
            this.repGridColumnBonusProduct.ValueMember = "Product_Id";
            // 
            // gridColumnCorrectionVCoefficient
            // 
            this.gridColumnCorrectionVCoefficient.Caption = "Коррекция V коэф.";
            this.gridColumnCorrectionVCoefficient.ColumnEdit = this.ColumnCorrectionVCoefficientTE;
            this.gridColumnCorrectionVCoefficient.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumnCorrectionVCoefficient.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnCorrectionVCoefficient.FieldName = "CorrectionRate";
            this.gridColumnCorrectionVCoefficient.Name = "gridColumnCorrectionVCoefficient";
            this.gridColumnCorrectionVCoefficient.Visible = true;
            this.gridColumnCorrectionVCoefficient.VisibleIndex = 3;
            this.gridColumnCorrectionVCoefficient.Width = 73;
            // 
            // gridColumnMaxDiscountProc
            // 
            this.gridColumnMaxDiscountProc.Caption = "Max скидка %";
            this.gridColumnMaxDiscountProc.ColumnEdit = this.columnMaxDiscountProcSE;
            this.gridColumnMaxDiscountProc.DisplayFormat.FormatString = "{0:N0} %";
            this.gridColumnMaxDiscountProc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnMaxDiscountProc.FieldName = "MaxDiscountPrc";
            this.gridColumnMaxDiscountProc.Name = "gridColumnMaxDiscountProc";
            this.gridColumnMaxDiscountProc.Visible = true;
            this.gridColumnMaxDiscountProc.VisibleIndex = 4;
            this.gridColumnMaxDiscountProc.Width = 54;
            // 
            // gridColumnPriceType
            // 
            this.gridColumnPriceType.Caption = "Тип цены";
            this.gridColumnPriceType.ColumnEdit = this.repGridColumnPriceType;
            this.gridColumnPriceType.FieldName = "PriceType";
            this.gridColumnPriceType.Name = "gridColumnPriceType";
            this.gridColumnPriceType.Visible = true;
            this.gridColumnPriceType.VisibleIndex = 5;
            this.gridColumnPriceType.Width = 97;
            // 
            // repGridColumnPriceType
            // 
            this.repGridColumnPriceType.AutoHeight = false;
            this.repGridColumnPriceType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repGridColumnPriceType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PriceType_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PriceTypeName", "Тип Цены")});
            this.repGridColumnPriceType.DisplayMember = "PriceTypeName";
            this.repGridColumnPriceType.Name = "repGridColumnPriceType";
            this.repGridColumnPriceType.NullText = "Выберите тип цены";
            this.repGridColumnPriceType.ValueMember = "PriceType_ID";
            // 
            // gridColumnPrice
            // 
            this.gridColumnPrice.Caption = "Цена";
            this.gridColumnPrice.ColumnEdit = this.columnPriceTE;
            this.gridColumnPrice.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumnPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnPrice.FieldName = "Price";
            this.gridColumnPrice.Name = "gridColumnPrice";
            this.gridColumnPrice.Visible = true;
            this.gridColumnPrice.VisibleIndex = 6;
            this.gridColumnPrice.Width = 40;
            // 
            // gridColumnRevaluation
            // 
            this.gridColumnRevaluation.Caption = "Переоценка";
            this.gridColumnRevaluation.ColumnEdit = this.columnRevaluationTE;
            this.gridColumnRevaluation.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumnRevaluation.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnRevaluation.FieldName = "Overvalue";
            this.gridColumnRevaluation.Name = "gridColumnRevaluation";
            this.gridColumnRevaluation.Visible = true;
            this.gridColumnRevaluation.VisibleIndex = 7;
            this.gridColumnRevaluation.Width = 68;
            // 
            // gridColumnShipped
            // 
            this.gridColumnShipped.Caption = "Выгружено";
            this.gridColumnShipped.FieldName = "isUnload";
            this.gridColumnShipped.Name = "gridColumnShipped";
            this.gridColumnShipped.OptionsColumn.AllowEdit = false;
            this.gridColumnShipped.Visible = true;
            this.gridColumnShipped.VisibleIndex = 8;
            // 
            // gridColumnStatus
            // 
            this.gridColumnStatus.Caption = "Статус";
            this.gridColumnStatus.FieldName = "Status";
            this.gridColumnStatus.Name = "gridColumnStatus";
            this.gridColumnStatus.OptionsColumn.AllowEdit = false;
            this.gridColumnStatus.Visible = true;
            this.gridColumnStatus.VisibleIndex = 9;
            this.gridColumnStatus.Width = 77;
            // 
            // gridColumnModified
            // 
            this.gridColumnModified.Caption = "Дата редакции";
            this.gridColumnModified.DisplayFormat.FormatString = "d";
            this.gridColumnModified.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnModified.FieldName = "DLM";
            this.gridColumnModified.Name = "gridColumnModified";
            this.gridColumnModified.OptionsColumn.AllowEdit = false;
            this.gridColumnModified.Visible = true;
            this.gridColumnModified.VisibleIndex = 10;
            this.gridColumnModified.Width = 69;
            // 
            // gridColumnModifiedBy
            // 
            this.gridColumnModifiedBy.Caption = "Имя редактора";
            this.gridColumnModifiedBy.FieldName = "UserName";
            this.gridColumnModifiedBy.Name = "gridColumnModifiedBy";
            this.gridColumnModifiedBy.OptionsColumn.AllowEdit = false;
            this.gridColumnModifiedBy.Visible = true;
            this.gridColumnModifiedBy.VisibleIndex = 11;
            this.gridColumnModifiedBy.Width = 175;
            // 
            // splitContainerMPA
            // 
            this.splitContainerMPA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerMPA.Horizontal = false;
            this.splitContainerMPA.Location = new System.Drawing.Point(0, 0);
            this.splitContainerMPA.Name = "splitContainerMPA";
            this.splitContainerMPA.Panel1.Controls.Add(this.splitContainerKPI);
            this.splitContainerMPA.Panel1.Text = "Panel1";
            this.splitContainerMPA.Panel2.Controls.Add(this.rbMPAType);
            this.splitContainerMPA.Panel2.Controls.Add(this.MPAGridControl);
            this.splitContainerMPA.Panel2.MinSize = 40;
            this.splitContainerMPA.Panel2.Text = "Panel2";
            this.splitContainerMPA.Panel2.Resize += new System.EventHandler(this.splitContainerMPA_Panel2_Resize);
            this.splitContainerMPA.Size = new System.Drawing.Size(1000, 744);
            this.splitContainerMPA.SplitterPosition = 567;
            this.splitContainerMPA.TabIndex = 20;
            this.splitContainerMPA.Text = "splitContainerControl1";
            // 
            // rbMPAType
            // 
            this.rbMPAType.EditValue = 0;
            this.rbMPAType.Location = new System.Drawing.Point(0, -4);
            this.rbMPAType.Name = "rbMPAType";
            this.rbMPAType.Properties.Columns = 2;
            this.rbMPAType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Акционные механики со скидкой"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Акционные механики с бонусным продуктом")});
            this.rbMPAType.Size = new System.Drawing.Size(523, 34);
            this.rbMPAType.TabIndex = 5;
            this.rbMPAType.SelectedIndexChanged += new System.EventHandler(this.rbMPAType_SelectedIndexChanged);
            // 
            // xtraScrollableControl
            // 
            this.xtraScrollableControl.AlwaysScrollActiveControlIntoView = false;
            this.xtraScrollableControl.AutoScrollMinSize = new System.Drawing.Size(1000, 455);
            this.xtraScrollableControl.Controls.Add(this.splitContainerMPA);
            this.xtraScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl.Name = "xtraScrollableControl";
            this.xtraScrollableControl.Size = new System.Drawing.Size(1000, 744);
            this.xtraScrollableControl.TabIndex = 21;
            // 
            // ActivityDetailsGoalsConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.xtraScrollableControl);
            this.Name = "ActivityDetailsGoalsConfig";
            this.Size = new System.Drawing.Size(1000, 744);
            ((System.ComponentModel.ISupportInitialize)(this.columnCompetitorSKUCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnBonusProductCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnCorrectionVCoefficientTE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnMaxDiscountProcSE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnPriceTypeCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnPriceTE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnRevaluationTE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnShippedCE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerKPI)).EndInit();
            this.splitContainerKPI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listGoals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAims)).EndInit();
            this.groupAims.ResumeLayout(false);
            this.groupAims.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCurAimName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGoalConf)).EndInit();
            this.panelGoalConf.ResumeLayout(false);
            this.panelGoalConf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcpFactsCalcWay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcpPeriodCalk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeSKU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeKPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupGoalsPlanning)).EndInit();
            this.groupGoalsPlanning.ResumeLayout(false);
            this.groupGoalsPlanning.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseAimValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccPerfOfTargetVal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkReversExecution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVisitChecked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGoalType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseInCalculations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTragetSplit)).EndInit();
            this.groupTragetSplit.ResumeLayout(false);
            this.groupTragetSplit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllowMultipleGoals.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSplitByPOCType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByPOCType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQDigit)).EndInit();
            this.groupQPQDigit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQLogic)).EndInit();
            this.groupQPQLogic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupSKU)).EndInit();
            this.groupSKU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupConditionsList)).EndInit();
            this.groupConditionsList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupEffectiveness)).EndInit();
            this.groupEffectiveness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chlbIPTRList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPAGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPAGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGridColumnCompetitorSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGridColumnBonusProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGridColumnPriceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMPA)).EndInit();
            this.splitContainerMPA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbMPAType.Properties)).EndInit();
            this.xtraScrollableControl.ResumeLayout(false);
            this.ResumeLayout(false);

      }

      #endregion

      private DevExpress.XtraEditors.SplitContainerControl splitContainerKPI;
      private DevExpress.XtraLayout.LayoutControl layoutControl1;
      private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
      private DevExpress.XtraEditors.SplitContainerControl splitContainerMPA;
      private DevExpress.XtraEditors.RadioGroup rbMPAType;
      private DevExpress.XtraGrid.GridControl MPAGridControl;
      private DevExpress.XtraGrid.Views.Grid.GridView MPAGridView;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnActionSKUName;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnCompetitorSKU;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnBonusProduct;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnCorrectionVCoefficient;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnMaxDiscountProc;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnPriceType;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrice;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnRevaluation;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnShipped;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatus;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnModified;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnModifiedBy;
      private DevExpress.XtraEditors.Repository.RepositoryItemComboBox columnCompetitorSKUCB;
      private DevExpress.XtraEditors.Repository.RepositoryItemComboBox columnBonusProductCB;
      private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit ColumnCorrectionVCoefficientTE;
      private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit columnMaxDiscountProcSE;
      private DevExpress.XtraEditors.Repository.RepositoryItemComboBox columnPriceTypeCB;
      private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit columnPriceTE;
      private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit columnRevaluationTE;
      private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit columnShippedCE;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnActionSKUID;
      private DevExpress.XtraGrid.Columns.GridColumn gridColumnID;
      private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repGridColumnCompetitorSKU;
      private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repGridColumnPriceType;
      private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repGridColumnBonusProduct;
      private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl;
      private DevExpress.XtraEditors.GroupControl groupTragetSplit;
      private DevExpress.XtraEditors.CheckEdit checkAllowMultipleGoals;
      private DevExpress.XtraEditors.CheckEdit checkSplitByPOCType;
      private DevExpress.XtraEditors.LabelControl labelControl1;
      private DevExpress.XtraEditors.LabelControl labelControl2;
      private DevExpress.XtraEditors.LookUpEdit cbSplitByPOCType;
      private DevExpress.XtraEditors.LookUpEdit cbSplitByTime;
      private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
      private DevExpress.XtraEditors.GroupControl groupGoalsPlanning;
      private DevExpress.XtraEditors.SimpleButton simpleEdit;
      private DevExpress.XtraEditors.LookUpEdit cbVisitChecked;
      private DevExpress.XtraEditors.SimpleButton simpleSave;
      private DevExpress.XtraEditors.SimpleButton simpleDelete;
      private DevExpress.XtraEditors.LookUpEdit cbGoalType;
      private DevExpress.XtraEditors.CheckEdit checkUseInCalculations;
      private DevExpress.XtraEditors.SimpleButton simpleAdd;
      private DevExpress.XtraEditors.LabelControl labelVisitChecked;
      private DevExpress.XtraEditors.LabelControl labelGoalType;
      private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
      private DevExpress.XtraEditors.PanelControl panelGoalConf;
      private DevExpress.XtraEditors.CheckEdit checkCannotChangeSKU;
      private DevExpress.XtraEditors.CheckEdit checkCannotChangeKPI;
      private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
      private DevExpress.XtraEditors.GroupControl groupAims;
      private DevExpress.XtraEditors.CheckEdit checkShowAll;
      private DevExpress.XtraEditors.LabelControl labelEdit;
      private DevExpress.XtraEditors.TextEdit textCurAimName;
      private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
      private DevExpress.XtraEditors.PanelControl panelControl1;
      private DevExpress.XtraEditors.ListBoxControl listGoals;
      private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
      private DevExpress.XtraEditors.GroupControl groupEffectiveness;
      private DevExpress.XtraEditors.CheckedListBoxControl chlbIPTRList;
      private DevExpress.XtraEditors.GroupControl groupConditionsList;
      private Logica.Reports.BaseReportControl.CommonControls.KPITree treeKPI;
      private DevExpress.XtraEditors.GroupControl groupSKU;
      private Logica.Reports.BaseReportControl.CommonControls.SKUTree skuTreeCtrl;
      private DevExpress.XtraEditors.CheckEdit checkReversExecution;
      private DevExpress.XtraEditors.GroupControl groupQPQLogic;
      private Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree qpqTreeLogic;
      private DevExpress.XtraEditors.GroupControl groupQPQDigit;
      private Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree qpqTreeDigit;
      private DevExpress.XtraEditors.CheckEdit checkUseAimValue;
      private DevExpress.XtraEditors.CheckEdit checkAccPerfOfTargetVal;
      private DevExpress.XtraEditors.LookUpEdit lcpFactsCalcWay;
      private DevExpress.XtraEditors.LookUpEdit lcpPeriodCalk;
      private DevExpress.XtraEditors.LabelControl lblFactCalcway;
      private DevExpress.XtraEditors.LabelControl lblPeriodCalc;






    }
}
