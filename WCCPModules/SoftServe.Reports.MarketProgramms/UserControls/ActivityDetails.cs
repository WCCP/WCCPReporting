﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.MarketProgramms.Tabs;
using SoftServe.Reports.MarketProgramms.Utility;
using WccpReporting;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;

namespace SoftServe.Reports.MarketProgramms.UserControls
{
    /// <summary>
    /// Action Level changed.
    /// </summary>
    public delegate void LevelChanged(ComboBoxItem newLevel);

    [ToolboxItem(false)]
    public partial class ActivityDetailsControl : CommonBaseControl
    {
        #region Readonly & Static Fields

        internal readonly ActivityDetailsCommon controlCommon;
        internal readonly ActivityDetailsAddresses controlControlGroup;
        internal readonly ActivityDetailsGoals controlGoals;
        internal readonly ActivityDetailsGoalsConfig controlGoalsConfig;
        internal readonly ActivityDetailsAddresses controlPromoGroup;
        internal readonly ActivityDetailsOnInvoice controlOnInvoice;

        #endregion

        #region Fields

        private DataTable _dtActivityDetails;
        private DataTable _dtTemplateDetails;

        #endregion

        #region Constructors

        public ActivityDetailsControl()
        {
            controlPromoGroup = new ActivityDetailsAddresses { IsControlGroup = false, MainControl = this };
            controlGoalsConfig = new ActivityDetailsGoalsConfig { MainControl = this };
            controlGoals = new ActivityDetailsGoals { MainControl = this };
            controlOnInvoice = new ActivityDetailsOnInvoice { MainControl = this };
            controlControlGroup = new ActivityDetailsAddresses { IsControlGroup = true, MainControl = this };
            controlCommon = new ActivityDetailsCommon { MainControl = this };
            InitializeComponent();
        }

        public ActivityDetailsControl(CommonBaseTab parentTab, int activityId, int templateId, string actionName)
            : base(parentTab)
        {
            controlPromoGroup = new ActivityDetailsAddresses { IsControlGroup = false, MainControl = this };
            controlGoalsConfig = new ActivityDetailsGoalsConfig { MainControl = this };
            controlGoals = new ActivityDetailsGoals { MainControl = this };
            controlOnInvoice = new ActivityDetailsOnInvoice { MainControl = this };
            controlControlGroup = new ActivityDetailsAddresses { IsControlGroup = true, MainControl = this };
            controlCommon = new ActivityDetailsCommon { MainControl = this };
            InitializeComponent();

            ActivityId = activityId;
            TemplateId = templateId;
            ActivityName = actionName;

            controlControlGroup.TTCountChanged += group_TTCountChanged;
            controlPromoGroup.TTCountChanged += group_TTCountChanged;

            InitTabControl();
        }

        #endregion

        #region Instance Properties

        internal ActivityDetailsDTO OriginalActivityDetails;

        /// <summary>
        ///   Gets or sets the activity id.
        /// </summary>
        /// <value>The activity id.</value>
        public int ActivityId { get; set; }

        public string ActivityName { get; set; }

        public string CtrlGrOutlets
        {
            get { return controlControlGroup.GetAllAddresses(); }
        }

        public string PromoGrOutlets
        {
            get { return controlPromoGroup.GetAllAddresses(); }
        }

        /// <summary>
        ///   Gets a value indicating whether this instance is in edit mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is in edit mode; otherwise, <c>false</c>.
        /// </value>
        public bool IsEditMode
        {
            get { return ActivityId > 0; }
        }

        /// <summary>
        ///   Gets or sets the template id.
        /// </summary>
        /// <value>The template id.</value>
        public int TemplateId { get; set; }

        /// <summary>
        ///   Gets a value indicating whether this instance is level valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is level valid; otherwise, <c>false</c>.
        /// </value>
        private bool IsLevelValid
        {
            get
            {
                bool lIsCommonLevelInvalid = controlCommon.SelectedLevel == LevelType.M3 &&
                                            (controlGoalsConfig.SelectedSplitByPOCType == SplitPOCByType.ByRegion);
                //|| controlGoals.SelectedSplitByPOCType == SplitPOCByType.ByDistrict
                return !lIsCommonLevelInvalid;
            }
        }

        public bool HasOnInvoiceRecords
        {
            get { return controlOnInvoice.HasOnInvoiceRecords; }
        }

        internal AccessChecker AccessChecker { get; private set; }
        internal ActivityInformation ActivityInfo { get; private set; }

        #endregion

        #region Instance Methods

        public ActivityDetailsDTO CheckData()
        {
            if (!IsLevelValid)
            {
                XtraMessageBox.Show(Resource.ErrorM3AndRegion, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            bool lIsInvalidCommon = !controlCommon.ValidateChildren(ValidationConstraints.Selectable);
            bool lIsInvalidGoals = !controlGoals.ValidateChildren(ValidationConstraints.Selectable);
            bool lIsInvalidGoalsConfig = !controlGoalsConfig.ValidateChildren(ValidationConstraints.Selectable) || !controlGoalsConfig.ValidateData();

            if (lIsInvalidCommon || lIsInvalidGoalsConfig || lIsInvalidGoals)
            {
                int lBrokenTabsCount = (lIsInvalidGoals ? 1 : 0) + (lIsInvalidCommon ? 1 : 0) + (lIsInvalidGoalsConfig ? 1 : 0);
                string lMsg;
                if (lBrokenTabsCount > 1)
                {
                    List<string> invalidTabs = new List<string>();
                    if (lIsInvalidCommon)
                        invalidTabs.Add(Resource.TabCommon);
                    if (lIsInvalidGoalsConfig)
                        invalidTabs.Add(Resource.TabGoalsConfig);
                    if (lIsInvalidGoals)
                        invalidTabs.Add(Resource.TabGoals);
                    lMsg = string.Format(Resource.SaveInvalidDataManyTabs, invalidTabs.ToString(", "));
                }
                else
                {
                    string lInvalidTabName = (lIsInvalidCommon ? Resource.TabCommon : lIsInvalidGoalsConfig ? Resource.TabGoalsConfig : Resource.TabGoals);
                    lMsg = string.Format(Resource.SaveInvalidDataOneTab, lInvalidTabName);

                    if (lIsInvalidGoalsConfig)
                    {
                        string lAimMsg;
                        controlGoalsConfig.IsAimCurValid(out lAimMsg);
                        lMsg += "\n\r " + lAimMsg;
                    }
                }
                XtraMessageBox.Show(lMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return null;
            }

            ActivityDetailsDTO lActivityDetails = CollectActivityDetails();

            if (!lActivityDetails.Valid())
            {
                XtraMessageBox.Show(lActivityDetails.ErrorDescription, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }


            //{
            //    //BUG #22722. МП: закладка Общие настройки: помилка при спробі редагувати дати в існуючій акції яка ще не почалася
            //    if (!originalActivityDetails.IsApproved && activityDetails.ActivityStart < DateTime.Now.Date)
            //    {
            //        XtraMessageBox.Show(Resource.ActivityDetailsControl_SaveActivity_Common_StartDate_NotLess_Current, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return null;
            //    }
            //}

            ////-- 4. Можно изменять дату окончания утвержденной активности, но не менше чем на текущий мес [TR_DW_AU_Action_U]
            //if (originalActivityDetails.IsApproved)
            //{
            //    if (originalActivityDetails.ActivityEnd != activityDetails.ActivityEnd)
            //    {
            //        DialogResult dr = DialogResult.None;

            //        //МП: дозволити міняти дату закінчення активності вперед 
            //        if (activityDetails.ActivityEnd < originalActivityDetails.ActivityEnd)
            //            dr = XtraMessageBox.Show(Resource.ActivityDetailsControl_SaveActivity_Common_CantDecreaseEndDate, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            //        if (dr != DialogResult.None)
            //            return null;
            //    }
            //}
            //else
            //{
            //    DialogResult dr = DialogResult.None;
            //    //BUG #22525. МП: потрібно давати можливість збільшити дату закінчення активності. 
            //    //BUG #23133. МП: дозволити міняти дату закінчення активності вперед і назад 
            //    if (!activityDetails.IsPriceActivity && activityDetails.ActivityEnd < DateTime.Now.Date)
            //        dr = XtraMessageBox.Show(Resource.ActivityDetailsControl_SaveActivity_Common_InvalidEndDate, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    if (activityDetails.IsPriceActivity && activityDetails.ActivityEnd < DateTime.Now.AddDays(8).Date)
            //        //BUG #20742. УЦА: неправильно обчисляється дата окончания активности: сьогодні 23 число. Згідно правил дата закінчення повинна бути не менше ніж через тиждень(7 чистих днів ) тобто 23+7 = 31 а не 30 число як є на даний момент. 
            //        dr = XtraMessageBox.Show(Resource.ActivityDetailsControl_SaveActivity_Price_InvalidEndDate, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            //    if (dr != DialogResult.None)
            //        return null;
            //}

            return lActivityDetails;
        }

        /// <summary>
        ///   Loads the data.
        /// </summary>
        public void LoadData()
        {
            WaitManager.StartWait();
            try
            {
                LoadTabs();
                OriginalActivityDetails = CollectActivityDetails();
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        /// <summary>
        /// Updates the tab status.
        /// </summary>
        public void UpdateTabStatus()
        {
            bool lEnableSubTabs = IsEditMode;
            EnableSubTab(ActivityDetailsTabType.Goals, lEnableSubTabs);
            EnableSubTab(ActivityDetailsTabType.OnInvoice, lEnableSubTabs && controlGoalsConfig.IsOnInvoiceAim);
            EnableSubTab(ActivityDetailsTabType.ControlGroup, lEnableSubTabs);
            EnableSubTab(ActivityDetailsTabType.PromoGroup, lEnableSubTabs);
        }

        public void EnableOnInvoiceTab(bool enabled)
        {
            EnableSubTab(ActivityDetailsTabType.OnInvoice, enabled);
        }

        private void AddTab(CommonBaseControl content, string tabName)
        {
            if (content == null || ParentTab == null)
                return;

            CommonBaseTab lTab = new CommonBaseTab();
            lTab.Init(ParentTab.Bruc, content, tabName);
            tabControl.TabPages.Add(lTab);
            content.UpdateMainView += content_UpdateMainView;
            content.AutoScroll = true;
        }

        private ActivityDetailsDTO CollectActivityDetails()
        {
            ActivityDetailsDTO lActivityDetails = new ActivityDetailsDTO { ActivityId = ActivityId, TemplateId = TemplateId };
            controlCommon.GetAllFields(lActivityDetails);
            controlGoalsConfig.GetAllFields(lActivityDetails);
            //if (IsEditMode)
            //    controlGoals.GetAllFields(activityDetails);
            return lActivityDetails;
        }

        private void DeleteActivity()
        {
            if (ActivityId <= 0 || ParentTab == null)
                return;
            if (XtraMessageBox.Show(string.Format(Resource.ConfirmActivityDelete, ParentTab.Text),
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;

            ActivityDetailsDTO lActivityDetails = new ActivityDetailsDTO { ActivityId = ActivityId };
            DataProvider.DeleteActivityCommon(lActivityDetails);
            if (lActivityDetails.ErrorCode != 0)
                XtraMessageBox.Show(lActivityDetails.ErrorDescription, Application.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            else
                ParentTab.CloseTab();
        }

        private void EnableSubTab(ActivityDetailsTabType tabType, bool enable)
        {
            GetSubTab(tabType).PageEnabled = enable;
        }

        private CommonBaseTab GetSubTab(ActivityDetailsTabType tabType)
        {
            return tabControl.TabPages[(int)tabType] as CommonBaseTab;
        }

        /// <summary>
        /// Inits the tab control.
        /// </summary>
        private void InitTabControl()
        {
            AddTab(controlCommon, Resource.TabCommon);
            AddTab(controlGoalsConfig, Resource.TabGoalsConfig);
            AddTab(controlGoals, Resource.TabGoals);
            AddTab(controlOnInvoice, Resource.TabOnInvoice);
            AddTab(controlControlGroup, Resource.TabControlGroup);
            AddTab(controlPromoGroup, Resource.TabPromoGroup);

            tabControl.SelectedTabPageIndex = (int)ActivityDetailsTabType.Common;
            UpdateTabStatus();
        }

        /// <summary>
        ///   Loads the sub tabs.
        /// </summary>
        private void LoadTabs()
        {
            _dtTemplateDetails = DataProvider.GetTemplateDetails(ActivityId, TemplateId);
            _dtActivityDetails = (IsEditMode) ? DataProvider.GetActivityDetails(ActivityId) : null;
            AccessChecker = new AccessChecker(_dtTemplateDetails, _dtActivityDetails);
            ActivityInfo = new ActivityInformation(_dtTemplateDetails, _dtActivityDetails);

            barButtonRemove.Enabled = IsEditMode && !ActivityInfo.IsActivityStarted && AccessChecker.CheckAccess(ActivityPositionEnum.Initiator);
            barButtonSave.Enabled = AccessChecker.CheckAccess(ActivityPositionEnum.Initiator)
                                    || AccessChecker.CheckAccess(ActivityPositionEnum.Approver)
                                    || AccessChecker.CheckAccess(ActivityPositionEnum.ControlGroupSelector)
                                    || AccessChecker.CheckAccess(ActivityPositionEnum.FactController)
                                    || AccessChecker.CheckAccess(ActivityPositionEnum.GoalsSetter)
                                    || AccessChecker.CheckAccess(ActivityPositionEnum.PerformerInRetail)
                                    || ActivityInfo.PromoGroupCascadeEdit && ActivityInfo.PromoTTAvailable > 0
                                    || ActivityInfo.ControlGroupCascadeEdit && ActivityInfo.ControlTTAvailable > 0;

            controlCommon.LoadData(ActivityId, TemplateId, _dtTemplateDetails, _dtActivityDetails);
            controlGoalsConfig.LoadData(TemplateId, ActivityId, _dtTemplateDetails, _dtActivityDetails);

            if (IsEditMode)
            {
                if (controlGoals.WasControlLoaded)
                    controlGoals.LoadData(ActivityId, _dtActivityDetails);

                if (controlOnInvoice.WasControlLoaded)
                    controlOnInvoice.LoadData(ActivityId, controlGoalsConfig.OnInvoiceAimId, ActivityName);

                if (controlControlGroup.WasControlLoaded)
                    controlControlGroup.LoadData(ActivityId, _dtActivityDetails);

                if (controlPromoGroup.WasControlLoaded)
                    controlPromoGroup.LoadData(ActivityId, _dtActivityDetails);
            }

            UpdateTabStatus();
        }

        private void SaveActivity()
        {
            ActivityDetailsDTO lActivityDetails = CheckData();
            if (lActivityDetails == null)
                return;

            WaitManager.StartWait();
            try
            {
                bool lSaveResult;
                if (DataProvider.IsLDB)
                    lSaveResult = SaveLDBData(lActivityDetails);
                else
                    lSaveResult = SaveSwData(lActivityDetails);
                if (!lSaveResult)
                    return;
            }
            finally
            {
                WaitManager.StopWait();
            }

            XtraMessageBox.Show(Resource.SavedSuccessfull, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

            // Update all details
            LoadData();
        }

        private bool SaveLDBData(ActivityDetailsDTO activityDetails)
        {
            if (controlControlGroup.WasControlLoaded)
                controlControlGroup.Save();
            if (controlPromoGroup.WasControlLoaded)
                controlPromoGroup.Save();
            return true;
        }

        private bool SaveSwData(ActivityDetailsDTO activityDetails)
        {
            int lNewActivityId = DataProvider.InsertActivityCommon(activityDetails, IsEditMode);
            if (activityDetails.ErrorCode != 0)
            {
                if (activityDetails.ErrorCode == Constants.ERROR_ACTIVITY_WITH_THIS_NAME_EXISTS)
                {
                    XtraMessageBox.Show(Resource.ErrorActivityWithThisNameExists, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                XtraMessageBox.Show(string.Format(Resource.SaveError, activityDetails.ErrorCode), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (!IsEditMode)
                activityDetails.ActivityId = lNewActivityId;

            SaveDocuments(activityDetails.ActivityId);
            controlGoalsConfig.Save(activityDetails);

            if (IsEditMode)
            {
                if (controlGoals.WasControlLoaded)
                    controlGoals.Save();
                if (controlOnInvoice.WasControlLoaded)
                    controlOnInvoice.Save();
                if (controlControlGroup.WasControlLoaded)
                    controlControlGroup.Save();
                if (controlPromoGroup.WasControlLoaded)
                    controlPromoGroup.Save();
            }
            else
                ActivityId = lNewActivityId;

            // update tab name and refresh addresses appearance
            if (ParentTab != null)
            {
                string lTemplateName = ConvertEx.ToString(_dtTemplateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_ACTION_NAME]);
                ParentTab.Text = String.Format("{0} {1}", lTemplateName, activityDetails.Name);
            }
            return true;
        }

        private void SaveDocuments(int activityId)
        {
            // Save newly added documents
            IEnumerable<FileUploader.FileDataInfo> documentsAdded = controlCommon.GetDocumentChanges(DataRowState.Added);
            foreach (FileUploader.FileDataInfo doc in documentsAdded)
            {
                if (!string.IsNullOrEmpty(doc.FilePath) && doc.FileData != null && doc.FileData.Length > 0)
                {
                    WaitManager.StartWait();
                    DataProvider.InsertDocument(activityId, doc.FilePath, doc.FileData);
                    WaitManager.StopWait();
                }
            }

            // Deleted documents
            IEnumerable<FileUploader.FileDataInfo> documentsDeleted = controlCommon.GetDocumentChanges(DataRowState.Deleted);
            foreach (FileUploader.FileDataInfo doc in documentsDeleted)
            {
                if (doc.Id > 0)
                {
                    WaitManager.StartWait();
                    DataProvider.DeleteDocument(doc.Id);
                    WaitManager.StopWait();
                }
            }
        }

        #endregion

        #region Event Handling

        private void barButtonRefreshImg_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }

        /// <summary>
        ///   Handles the ItemClick event of the barButtonRemove control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "DevExpress.XtraBars.ItemClickEventArgs" /> instance containing the event data.</param>
        private void barButtonRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            DeleteActivity();
        }

        /// <summary>
        ///   Handles the ItemClick event of the barButtonSave control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "DevExpress.XtraBars.ItemClickEventArgs" /> instance containing the event data.</param>
        private void barButtonSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveActivity();
        }

        /// <summary>
        ///   Handles the UpdateMainView event of the content control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void content_UpdateMainView(object sender, EventArgs e)
        {
            UpdateTabStatus();
        }

        private void group_TTCountChanged(object sender, int ttCount)
        {
            string message = string.Empty;
            if (sender.Equals(controlPromoGroup))
            {
                int controlControlGroupCount = controlControlGroup.TTCount;
                if (controlControlGroupCount > 0 && ttCount > 0 && (1.0 * controlControlGroupCount / ttCount) < 0.5)
                    message = Resource.ControlGroup_Error;
                //controlGoals.PlannedQuantity = ActivityInfo.IsApproved ? ActivityInfo.OutletQuantity : controlPromoGroup.ApprovedTTCount;
                //controlGoals.PlannedAddressesIncludePercent = (ttCount > 0)
                //                                              	? Math.Round(Convert.ToDecimal(100.0*controlPromoGroup.ApprovedTTCount/ttCount), 1)
                //                                              	: 0;
            }
            else if (sender.Equals(controlControlGroup))
            {
                int controlPromoGroupCount = controlPromoGroup.TTCount;
                if (controlPromoGroupCount > 0 && ttCount > 0 && (1.0 * ttCount / controlPromoGroupCount) < 0.5)
                    message = Resource.ControlGroup_Error;
            }
            controlControlGroup.ErrorMessage = message;
        }

        #endregion

        /// <summary>
        /// Gets the current base control.
        /// </summary>
        /// <value>The current base control.</value>
        internal CommonBaseControl CurrentBaseControl
        {
            get
            {
                return CurrentBaseTab != null ? CurrentBaseTab.UserControl : null;
            }
        }

        /// <summary>
        /// Gets the current base tab.
        /// </summary>
        /// <value>The current base tab.</value>
        private CommonBaseTab CurrentBaseTab
        {
            get
            {
                return tabControl.SelectedTabPage as CommonBaseTab;
            }
        }

        private void tabControl_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            e.Cancel = false;
            CommonBaseTab lPrevTab = e.PrevPage as CommonBaseTab;

            // goal config verification
            if (lPrevTab != null)
            {
                if (lPrevTab.UserControl != null &&
                   (lPrevTab.UserControl is ActivityDetailsGoalsConfig) &&
                   controlGoalsConfig != null &&
                   controlGoalsConfig.IsUnsavedAim())
                {
                    DialogResult lDialogResult = XtraMessageBox.Show("На закладке Настройка целей есть несохраненная цель. Сохранить ее?",
                        "Предупреждение", MessageBoxButtons.YesNoCancel);

                    if (lDialogResult == DialogResult.Yes)
                        controlGoalsConfig.SaveToListUnsavedAim();
                    if (lDialogResult == DialogResult.No)
                        controlGoalsConfig.RenewUnsavedAim();
                    if (lDialogResult == DialogResult.Cancel)
                        e.Cancel = true;
                }
            }

            CommonBaseTab lTab = e.Page as CommonBaseTab;
            Debug.Assert(lTab != null, "Tab not identified.");

            // on the fly tabs loading
            if (lTab.UserControl != null &&
               (lTab.UserControl is ActivityDetailsGoals) &&
               controlGoals != null &&
               !controlGoals.WasControlLoaded)
            {
                WaitManager.StartWait();
                try
                {
                    controlGoals.LoadData(ActivityId, _dtActivityDetails);
                }
                finally
                {
                    WaitManager.StopWait();
                }
            }
            else
                if (lTab.UserControl != null &&
                   (lTab.UserControl is ActivityDetailsOnInvoice) &&
                   controlOnInvoice != null &&
                   !controlOnInvoice.WasControlLoaded)
                {
                    WaitManager.StartWait();
                    try
                    {
                        controlOnInvoice.LoadData(ActivityId, controlGoalsConfig.OnInvoiceAimId, ActivityName);
                    }
                    finally
                    {
                        WaitManager.StopWait();
                    }
                }
                else
                    if (lTab.UserControl != null && lTab.UserControl is ActivityDetailsAddresses)
                    {
                        ActivityDetailsAddresses lAddressesControl = lTab.UserControl as ActivityDetailsAddresses;

                        WaitManager.StartWait();
                        try
                        {
                            if (lAddressesControl.IsControlGroup && !controlControlGroup.WasControlLoaded)
                                controlControlGroup.LoadData(ActivityId, _dtActivityDetails);
                            else
                            {
                                if (!lAddressesControl.IsControlGroup && !controlPromoGroup.WasControlLoaded)
                                    controlPromoGroup.LoadData(ActivityId, _dtActivityDetails);
                            }
                        }
                        finally
                        {
                            WaitManager.StopWait();
                        }
                    }

            // export button accessibility
            if (lTab.Bruc != null && lTab.UserControl != null &&
                (lTab.UserControl is ActivityDetailsGoals ||
                 lTab.UserControl is ActivityDetailsAddresses)
               )
                lTab.Bruc.btnExportTo.Enabled = true;
            else if (lTab.Bruc != null)
                lTab.Bruc.btnExportTo.Enabled = false;
        }
    }
}