﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl.CommonControls.AddressesFilters;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.MarketProgramms.Utility;
using DevExpress.XtraGrid;
using DevExpress.Utils;

namespace SoftServe.Reports.MarketProgramms.UserControls
{
    /// <summary>
    ///   Tab with Control group of TT in activity
    /// </summary>
    [ToolboxItem(false)]
    public partial class ActivityDetailsAddresses : CommonBaseControl
    {
        #region Delegates

        public delegate void TTCountChangedEventArgs(object sender, int rowsCount);

        #endregion

        #region Constants

        private const String ACTIVITY_COLUMN_PREFIX = "EFF_";

        #endregion

        #region Fields

        private List<AddressesInformation> addedAddresses = new List<AddressesInformation>();
        private DataView clone;
        private string fieldOldStatus = "OldStatusColunm";

        /// <summary>
        ///   This flag shows whether we had already handled the resizing.
        /// </summary>
        private bool grResizeHandle;

        private bool isControlGroup;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "ActivityDetailsAddresses" />
        ///   class.
        /// </summary>
        public ActivityDetailsAddresses()
        {
            InitializeComponent();
            gridControl_SizeChanged(null, null);

            SuspendLayout();
            manageAddressesList.InvisibleColumns = new[] {colStatus, colThRFilterId};
            manageAddressesList.OnFilterChanged += manageAddressesList_OnFilterChanged;
            manageAddressesList.FilterPreviewChanging += manageAddressesList_FilterPreviewChanging;
            manageAddressesList.barEditMode.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            colTTCode.OptionsColumn.AllowMove =
                colAccepted.OptionsColumn.AllowMove = false;

            GridLocalizer.Active = new RussianGridLocalizer();
            Localizer.Active = new RussianEditorsLocalizer();

            ResumeLayout();

            manageAddressesList.OnDisplayFilterChanged += manageAddressesList_OnDisplayFilterChanged;

            WasControlLoaded = false;
        }

        #endregion

        #region Instance Properties

        public int ApprovedTTCount
        {
            get { return GetApprovedTTCount(AddressesList); }
        }

        public string ErrorMessage
        {
            get { return manageAddressesList.ErrorText; }
            set { manageAddressesList.ErrorText = value; }
        }

        public bool IsControlGroup
        {
            get { return isControlGroup; }
            set
            {
                isControlGroup = value;
                if (value)
                {
                    manageAddressesList.InvisibleColumns = new[]
                                                               {
                                                                   colStatus,
                                                                   colThRFilterId,
                                                                   colSwitchOffApproved,
                                                                   colSwitchOffDate,
                                                                   colSwitchOnApproved,
                                                                   colSwitchOnDate,
                                                                   colDeclined,
                                                                   colDeclineReason,
                                                                   colActivationStatus,
                                                                   colIsContract
                                                               };
                    manageAddressesList.FrozenColumns = new[] {colTTCode, colAccepted};
                }
                else
                {
                    manageAddressesList.InvisibleColumns = new[]
                                                               {
                                                                   colStatus,
                                                                   colThRFilterId
                                                               };
                    manageAddressesList.FrozenColumns = new[] {colTTCode, colAccepted, colSwitchOnDate, colSwitchOnApproved, colSwitchOffDate, colSwitchOffApproved};
                }

                colSwitchOnDate.OptionsColumn.AllowMove = value;
                colSwitchOnApproved.OptionsColumn.AllowMove = value;
                colSwitchOffDate.OptionsColumn.AllowMove = value;
                colSwitchOffApproved.OptionsColumn.AllowMove = value;

                colSwitchOnDate.Fixed = value ? FixedStyle.None : FixedStyle.Left;
                colSwitchOnApproved.Fixed = value ? FixedStyle.None : FixedStyle.Left;
                colSwitchOffDate.Fixed = value ? FixedStyle.None : FixedStyle.Left;
                colSwitchOffApproved.Fixed = value ? FixedStyle.None : FixedStyle.Left;

                colSwitchOffApproved.Visible = !value;
                colSwitchOffDate.Visible = !value;
                colSwitchOnApproved.Visible = !value;
                colSwitchOnDate.Visible = !value;
                colDeclined.Visible = !value;
                colDeclineReason.Visible = !value;
                colActivationStatus.Visible = !value;
            }
        }

        public ActivityDetailsControl MainControl { get; set; }

        public int TTCount
        {
            get { return GetTTCount(AddressesList); }
        }

        /// <summary>
        ///   Gets or sets the addresses list.
        /// </summary>
        /// <value>The addresses list.</value>
        private DataTable AddressesList
        {
            set
            {
                gridControl.DataSource = value;
                if (value != null)
                {
                    AddressesList.PrimaryKey = new[] {AddressesList.Columns[colTTCode.FieldName]};
                }
                FireTtChanged();
            }
            get
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable) gridControl.DataSource;
            }
        }

        public GridControl Grid 
        { 
            get
            {
                return this.gridControl;
            } 
        }

        public double AggrColumnSummaryV
        {
            get { return manageAddressesList.AggrColumnSummaryV; }
        }

        public double AggrColumnAvgV
        {
            get { return manageAddressesList.AggrColumnAvgV; }
        }

        #endregion

        #region Instance Methods
        /// <summary>
        /// Disables custom formating
        /// </summary>
        public void PrepareToExport()
        {
            colInBevPercent.DisplayFormat.FormatType = FormatType.None;
        }

        /// <summary>
        /// Restores custom formating
        /// </summary>
        public void RestoreAfterExport()
        {
            colInBevPercent.DisplayFormat.FormatType = FormatType.Numeric;
            colInBevPercent.DisplayFormat.FormatString = "P";
        }

        /// <summary>
        ///   Gets the addresses by status.
        /// </summary>
        /// <param name = "status">The status.</param>
        /// <returns>List of addresses which was added/modified/removed</returns>
        public List<AddressDTO> GetAddressesByStatus(RowStatus status)
        {
            List<AddressDTO> result = new List<AddressDTO>();

            if (status == RowStatus.New)
            {
                addedAddresses.ForEach(item => result.Add(new AddressDTO {Data = item}));
            }
            else if (status == RowStatus.Deleted)
            {
                List<string> ids =
                    (from d in AddressesList.AsEnumerable()
                     where d.Field<string>(colStatus.FieldName).Equals(status.ToString())
                     select d.Field<Int64>(colTTCode.FieldName).ToString()).ToList();
                result.Add(new AddressDTO {Data = new AddressesInformation {AddressesList = ids}});
            }
            return result;
        }

        /// <summary>
        ///   Gets the all addresses.
        /// </summary>
        /// <returns>List of all addresses</returns>
        public string GetAllAddresses()
        {
            return AddressesList == null
                ? string.Empty
                : new XElement(Constants.XML_OUTLET_ROOT_NAME,
                    AddressesList.AsEnumerable().Select(x =>
                    {
                        var e = new XElement(Constants.XML_OUTLET_ELEMENT_NAME);
                        e.SetAttributeValue(Constants.XML_OUTLET_ATTRIBUTE_FIELD_ID, x.Field<Int64>(colTTCode.FieldName));
                        return e;
                    })).ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        ///   Loads the data.
        /// </summary>
        /// <param name = "activityId">The activity id.</param>
        /// <param name="activityDetails"></param>
        public void LoadData(int activityId, DataTable activityDetails)
        {
            addedAddresses.Clear();
            DataTable dt = DataProvider.GetActivityAddresses(activityId, IsControlGroup);
            SetDefaultToColumns(dt);
            dt.Columns.Add(fieldOldStatus);
            PopulateColumn(dt, colStatus.FieldName, RowStatus.Unchanged.ToString());

            RemoveActivityColumns();
            AddActivityColumnsToGrid(dt);
            AddressesList = dt;
           
            repositoryActivationLookUpEdit.DataSource = DataProvider.GetActivationStatuses();
            repositoryDeclineLookUpEdit.DataSource = DataProvider.GetDeclineReasons();

            manageAddressesList.ChannelId = activityDetails.Rows[0][Constants.FIELD_CHANNEL_ID];

            #region Set editable for columns

            //Выбор контрольной группы выполняется пользователем с настроенной должностью, после утверждения активности в контрольную группу можно только добавлять точки, а убирать нельзя, как и в промо адреску. При этом он должен иметь доступ ко всей адреске.
            bool isControlGroupSelector = MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.ControlGroupSelector);
            bool isInitiator = MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Initiator);
            bool isFactController = MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.FactController);
            bool isPerformerInRetail = MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.PerformerInRetail);

            ActivityInformation activityInfo = MainControl.ActivityInfo;
            //Есть доолжность или разрешено каскадное редактирование и есть точки принадлежащие данному пользователю
            bool isCanAddDelOLs =
                IsControlGroup && (isControlGroupSelector || activityInfo.ControlGroupCascadeEdit && activityInfo.ControlTTAvailable > 0)
                || !IsControlGroup && (isInitiator || activityInfo.PromoGroupCascadeEdit && activityInfo.PromoTTAvailable > 0);
            manageAddressesList.EditMode = isCanAddDelOLs
                                               ? (activityInfo.IsApproved
                                                      ? AddressEditMode.AllowAddOnly
                                                      : AddressEditMode.AllowAll)
                                               : AddressEditMode.ReadOnly;

            //BUG #20756. УЦА: дозволена модифікація недозволених колонок: для Ценовой активности не должно быть возм менять поля Дата подключения ,утвердждения,отказ,причина отказа,дата откл,ее подтверждиния.
            //Исполнитель в рознице проставляет Отказ, причину отказа, может добавить комментарий, проставить дату подключения и дату отключения, а так же отметить сторчек.
            colDeclined.OptionsColumn.ReadOnly = !(!activityInfo.IsPriceActivity && isPerformerInRetail);
            colDeclineReason.OptionsColumn.ReadOnly = !(!activityInfo.IsPriceActivity && isPerformerInRetail);
            //#33546. Управление МП - Действия закрепленные за ролями
            colComment2.OptionsColumn.ReadOnly = !(isInitiator || isPerformerInRetail || isFactController);
            colSwitchOnDate.OptionsColumn.ReadOnly = !(!activityInfo.IsPriceActivity && isPerformerInRetail);
            colSwitchOffDate.OptionsColumn.ReadOnly = !(!activityInfo.IsPriceActivity && isPerformerInRetail);
            //#33546. Управление МП - Действия закрепленные за ролями
            colAccepted.OptionsColumn.ReadOnly = !isInitiator;

            //Контролер факта выполнения проставляет признак утверждения дат подключения и дат отключения, после этого исполнитель в рознице уже не может менять данную точку. Контролер так же может изменить статус активности на Реактивировать.
            colSwitchOnApproved.OptionsColumn.ReadOnly = !(!activityInfo.IsPriceActivity && isFactController);
            colSwitchOffApproved.OptionsColumn.ReadOnly = !(!activityInfo.IsPriceActivity && isFactController);
            colActivationStatus.OptionsColumn.ReadOnly = !isFactController;

            #endregion

            WasControlLoaded = true;
        }

        /// <summary>
        ///   Saves this instance.
        /// </summary>
        public void Save()
        {
            var dto = new ErrorableDTO();
            GetAddressesByStatus(RowStatus.New).ForEach(item => DataProvider.AddAddresses(MainControl.ActivityId, item, isControlGroup));

            var changes = AddressesList.Clone();
            foreach (DataRow row in AddressesList.Rows)
            {
                // added to remove recently desapproved TT
                if (row[colStatus.FieldName].ToString() == RowStatus.Updated.ToString() || row[colStatus.FieldName].ToString() == RowStatus.New.ToString() || (row[colStatus.FieldName].ToString() == RowStatus.Deleted.ToString() && row[fieldOldStatus].ToString() == RowStatus.Updated.ToString()))
                {
                    changes.ImportRow(row);
                }
            }

            if (changes.Rows.Count > 0)
            {
                bool doPortions = true;
                int portionRowsCount = 10000;
                int skipRowsCount = 0;
                DataTable portion = null;

                while (doPortions)
                {
                    portion = changes.AsEnumerable().Skip(skipRowsCount).Take(portionRowsCount).CopyToDataTable();

                    if (portion.Rows.Count == 0)
                    {
                        doPortions = false;
                        continue;
                    }

                    var xml = ConvertEx.ToSqlXml(
                        portion,
                        new[]
                        {
                            colTTCode.FieldName,
                            colAccepted.FieldName,
                            colSwitchOnDate.FieldName,
                            colSwitchOnApproved.FieldName,
                            colSwitchOffDate.FieldName,
                            colSwitchOffApproved.FieldName,
                            colDeclined.FieldName,
                            colDeclineReason.FieldName,
                            colActivationStatus.FieldName,
                            colComment2.FieldName
                        });
                    
                    DataProvider.UpdateAddresses(MainControl.ActivityId, xml, dto);

                    if (dto.ErrorCode != 0)
                    {
                        MessageBox.Show(dto.ErrorDescription, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    skipRowsCount += portionRowsCount;
                    doPortions = !(portion.Rows.Count < portionRowsCount);
                }
            }

            GetAddressesByStatus(RowStatus.Deleted).ForEach(item => DataProvider.DeleteAddresses(MainControl.ActivityId, item, false));
        }

        /// <summary>
        ///   Adds the activity columns to grid.
        /// </summary>
        /// <param name = "table">The table.</param>
        private void AddActivityColumnsToGrid(DataTable table)
        {
            var columns = gridView.Columns.Cast<GridColumn>();

            foreach (DataColumn column in table.Columns)
            {
                if (column.ColumnName.StartsWith(ACTIVITY_COLUMN_PREFIX) && !columns.Where(x => x.FieldName == column.ColumnName).Any())
                {
                    gridView.Columns.Add(NewGridColumn(
                        column.ColumnName,
                        column.ColumnName.Substring(ACTIVITY_COLUMN_PREFIX.Length)
                                             ));
                }
            }

            manageAddressesList.ReLoadColumnChooser();
        }

        private void AddPOC(AddressesInformation data, DataTable dataTable)
        {
            if (data.DedupedAddressesList != null && data.DedupedAddressesList.Count > 0)
            {
                string pocList = data.DedupedAddressesList.ToString(Constants.DELIMITER_ID_LIST);
                DataTable dt = DataProvider.GetAddressesListByOLIDs(MainControl.ActivityId, pocList);
                if (dt == null) return;

                SetDefaultToColumns(dt);

                dt.PrimaryKey = new[] {dt.Columns[colTTCode.FieldName]};

                if (dt.Rows.Count > 0)
                {
                    PopulateColumn(dt, colStatus.FieldName, RowStatus.New.ToString());
                    if (!string.IsNullOrEmpty(data.Comment))
                    {
                        PopulateColumn(dt, colComment2.FieldName, data.Comment);
                    }
                    dataTable.Merge(dt, false, MissingSchemaAction.Add);

                    AddActivityColumnsToGrid(dataTable);
                }
            }
        }

        private void FireTtChanged()
        {
            if (TTCountChanged != null)
            {
                TTCountChanged(this, GetTTCount(AddressesList));
            }
        }

        private int GetApprovedTTCount(DataTable dt)
        {
            if (dt == null)
                return 0;
            //BUG #20555. МП: Закладка "Цели": Поле "Планируемое количество ТТ" повинно показувати кількість Підтверджених ТТ Промо групи
            return (from d in dt.AsEnumerable()
                    where !d.Field<string>(colStatus.FieldName).Equals(RowStatus.Deleted.ToString())
                          && d.Field<bool>(colAccepted.FieldName)
                    select 1).Count();
        }

        private int GetTTCount(DataTable dt)
        {
            if (dt == null)
                return 0;

            return (from d in dt.AsEnumerable()
                    where !d.Field<string>(colStatus.FieldName).Equals(RowStatus.Deleted.ToString())
                    select 1).Count();
        }

        /// <summary>
        ///   Method that checks incoming column and returns value that indicates wether column type is boolean.
        /// </summary>
        /// <param name = "column">Checking column</param>
        /// <returns></returns>
        private bool IsCheckBoxEditor(GridColumn column)
        {
            return column.ColumnType == typeof (bool);
        }

        private bool IsModificationAllowed(object newValue, int rowHandle, GridColumn column)
        {
            object oldValue = gridView.GetRowCellValue(rowHandle, column);
            //BUG #22515. МП: додати перевірку на наявність дати при спробі підключити чи відлючити акцію в ТТ.
            if (column == colSwitchOnApproved
                && !ConvertEx.ToBool(oldValue)
                && ConvertEx.ToBool(newValue)
                && ConvertEx.IsNull(gridView.GetRowCellValue(rowHandle, colSwitchOnDate)))
            {
                return false;
            }
            if (column == colSwitchOffApproved
                && !ConvertEx.ToBool(oldValue)
                && ConvertEx.ToBool(newValue)
                && ConvertEx.IsNull(gridView.GetRowCellValue(rowHandle, colSwitchOffDate)))
            {
                return false;
            }

            //BUG #20757. УЦА: дозволяти міняти статус деактивованої акції в ТТ тільки на реактивовану 
            if (column == colActivationStatus
                && MainControl.ActivityInfo.IsPriceActivity
                && (ConvertEx.ToInt(newValue) != 4
                    || ConvertEx.ToInt(oldValue) != 3))
                return false;

            //BUG #20754. УЦА: некоректна робота галочки дата Подключения. Нельзя убрать галочку подтверждена если есть дата подключения и она утверждена.
            if (column == colAccepted
                && gridView.GetRowCellValue(rowHandle, colSwitchOnDate) != null
                && gridView.GetRowCellValue(rowHandle, colSwitchOnDate) != DBNull.Value
                && ConvertEx.ToBool(oldValue))
                return false;

            return true;
        }

        /// <summary>
        ///   Method that modifies given cell or multiple cells (respectively to the edit mode) with the specified value.
        /// </summary>
        /// <param name = "rowHandle">Row index to modify</param>
        /// <param name = "column">Column to modify</param>
        /// <param name = "value">Value that will be setted to the specified cell</param>
        private void ModifyCell(int rowHandle, GridColumn column, object value)
        {
            if (rowHandle >= 0 && column != colStatus)
            {
                if ((bool) (manageAddressesList.barEditMode.EditValue)) //Multiple editing
                {
                    gridView.CellValueChanging -= gridView_CellValueChanging;
                    for (int i = 0; i < gridView.RowCount; i++)
                    {
                        var status = ConvertEx.ToString(gridView.GetRowCellValue(i, colStatus));
                        if ((status.Equals(RowStatus.Unchanged.ToString()) ||
                             status.Equals(RowStatus.Updated.ToString()) ||
                             status.Equals(RowStatus.New.ToString())) &&
                            IsModificationAllowed(value, i, column))
                        {
                            gridView.SetRowCellValue(i, column, value);
                            gridView.SetRowCellValue(i, colStatus, RowStatus.Updated); //Setting cell value to "Updated" in order to highlight this cell
                            //gridView.UpdateCurrentRow();
                        }
                    }
                    gridView.CellValueChanging += gridView_CellValueChanging;
                }
                else //Single editing
                {
                    var status = ConvertEx.ToString(gridView.GetRowCellValue(rowHandle, colStatus));
                    if (!IsModificationAllowed(value, rowHandle, column))
                    {
                        gridView.CloseEditor();
                        return;
                    }
                    if (status.Equals(RowStatus.Unchanged.ToString()) || status.Equals(RowStatus.Updated.ToString())
                        || status.Equals(RowStatus.New.ToString()))
                    {
                        gridView.SetFocusedValue(value);
                        gridView.SetRowCellValue(rowHandle, colStatus, RowStatus.Updated); //Setting cell value to "Updated" in order to highlight this cell
                        //gridView.UpdateCurrentRow();
                    }
                }
                FireTtChanged();
            }
        }

        /// <summary>
        ///   News the grid column.
        /// </summary>
        /// <param name = "fieldName">Name of the field.</param>
        /// <param name = "displayName">The display name.</param>
        /// <returns></returns>
        private GridColumn NewGridColumn(String fieldName, String displayName)
        {
            GridColumn column = new GridColumn();

            column.AppearanceHeader.Options.UseTextOptions = true;
            column.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            column.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            column.Caption = displayName;
            column.ColumnEdit = repositoryActivity;
            column.FieldName = fieldName;
            column.MinWidth = 50;
            column.OptionsColumn.AllowEdit = false;
            column.OptionsColumn.AllowMove = true;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
            column.Visible = false;
            column.Width = 100;
            column.ColumnEdit = repositoryItemCheckEdit1;

            return column;
        }

        /// <summary>
        ///   Populates the column with data.
        /// </summary>
        /// <param name = "dt">The DataTable to populate.</param>
        /// <param name = "columnName">Name of the column.</param>
        /// <param name = "data">The data.</param>
        private void PopulateColumn(DataTable dt, string columnName, object data)
        {
            if (!dt.Columns.Contains(columnName))
            {
                dt.Columns.Add(columnName);
            }
            DataColumn dc = dt.Columns[dt.Columns.IndexOf(columnName)];
            foreach (DataRow row in dt.Rows)
            {
                row[dc] = data;
            }
        }

        private void RemoveActivityColumns()
        {
            List<GridColumn> additionalGridColumns = gridView.Columns.Cast<GridColumn>().Where(gc => gc.FieldName.StartsWith(ACTIVITY_COLUMN_PREFIX)).ToList();
            foreach (GridColumn column in additionalGridColumns)
            {
                gridView.Columns.Remove(column);
            }
        }

        /// <summary>
        ///   Removes the selected addresses.
        /// </summary>
        private void RemoveSelectedAddresses()
        {
            if (gridView.SelectedRowsCount > 0)
            {
                int acceptedMarketPoints = 0;
                List<int> selectedRows = gridView.GetSelectedRows().ToList();
                for (int i = selectedRows.Count; i > 0; i--)
                {
                    bool isAccepted = ConvertEx.ToBool(gridView.GetRowCellValue(selectedRows[i - 1], colAccepted));
                    if (isAccepted)
                    {
                        selectedRows.RemoveAt(i - 1);
                        acceptedMarketPoints++;
                    }
                }
                if (selectedRows.Count > 0)
                    //Old confirmation message box
                    // &&
                    //  XtraMessageBox.Show(Resource.DeleteConfirmation, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    gridView.BeginUpdate();

                    foreach (int rowHandle in selectedRows)
                    {
                        DataRowView row = gridView.GetRow(rowHandle) as DataRowView;
                        string curStatus = Convert.ToString(row[colStatus.FieldName]);
                        if (curStatus == RowStatus.Deleted.ToString())
                        {
                            row[colStatus.FieldName] = row[fieldOldStatus];
                        }
                        else
                        {
                            row[fieldOldStatus] = curStatus;
                            row[colStatus.FieldName] = RowStatus.Deleted.ToString();
                        }
                    }

                    gridView.EndUpdate();
                    FireTtChanged();
                }
                if (acceptedMarketPoints > 0)
                {
                    XtraMessageBox.Show(acceptedMarketPoints == 1 && selectedRows.Count == 0
                                            ? Resource.CantDeleteConfirmedTT
                                            : selectedRows.Count > 0 && acceptedMarketPoints > 0 ? Resource.CantDeleteSomeConfirmedTT : Resource.CantDeleteConfirmedTTMultiple,
                                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void SetDefaultToColumns(DataTable dt)
        {
            if (dt == null)
                return;
            foreach (DataColumn column in dt.Columns)
            {
                if (column.ColumnName.StartsWith(ACTIVITY_COLUMN_PREFIX))
                {
                    column.DefaultValue = false;
                    column.AllowDBNull = false;
                }
            }
        }

        #endregion

        #region Event Handling

        /// <summary>
        ///   To avoid overflow out of the bounds, we need to decrease the size of this goofy control only once.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
        private void ActivityDetailsAddresses_SizeChanged(object sender, EventArgs e)
        {
            if (!grResizeHandle && gridControl.Parent.Size.Width > 0 && gridControl.Parent.Size.Height > 0)
            {
                grResizeHandle = true;
                gridControl.Parent.Size = new Size(gridControl.Parent.Size.Width,
                                                   gridControl.Parent.Size.Height - 65);
            }
        }

        private void gridControl_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.V) && e.Control)
            {
                manageAddressesList.FilterDisplayedAddresses();
            }
        }

        private void gridControl_SizeChanged(object sender, EventArgs e)
        {
            gridView.OptionsView.ColumnAutoWidth = (from GridColumn column in gridView.Columns where column.Visible select column.Width).Sum() <= gridControl.Width;
        }

        private void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int lPos = 0;
            if (gridView.ActiveEditor is TextEdit)
                lPos = (gridView.ActiveEditor as TextEdit).SelectionStart - 1;

            ModifyCell(e.RowHandle, e.Column, e.Value);
            if (gridView.ActiveEditor is TextEdit)
                (gridView.ActiveEditor as TextEdit).SelectionStart = lPos + 1;
        }

        private void gridView_HiddenEditor(object sender, EventArgs e)
        {
            //BUG #22513. МП: неправильна робота звіту при активації ТТ для акції
            //if (!MainControl.ActivityInfo.IsPriceActivity) return;
            if (clone != null)
            {
                clone.Dispose();
                clone = null;
            }
        }

        /// <summary>
        ///   Handles the KeyDown event of the gridControl control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "System.Windows.Forms.KeyEventArgs" /> instance containing the event data.</param>
        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSelectedAddresses();
            }
            if (e.KeyCode == Keys.Space && gridView.FocusedColumn != null && IsCheckBoxEditor(gridView.FocusedColumn))
            {
                ModifyCell(gridView.FocusedRowHandle, gridView.FocusedColumn, !ConvertEx.ToBool(gridView.FocusedValue));
            }
        }

        /// <summary>
        ///   Handler of event RowCellClick, that occurs after user clicking on some cell.
        ///   This method is used in order to implement immediatly changing value on CheckBox columns (after one click).
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        private void gridView_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (!IsCheckBoxEditor(e.Column) || e.Column.ReadOnly) return;
            bool currentValue = ConvertEx.ToBool(e.CellValue);
            ModifyCell(e.RowHandle, e.Column, !currentValue); //Modify cell with reverse value.
        }

        private void gridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (gridView.FocusedColumn != null && IsCheckBoxEditor(gridView.FocusedColumn) && !gridView.FocusedColumn.ReadOnly)
            {
                e.Cancel = true;
                return;
            }
            
            var status = ConvertEx.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, colStatus));
            // BUG #20757. УЦА: дозволяти міняти статус деактивованої акції в ТТ тільки на реактивовану. Можно реактивировать только деактивированные ТТ.
            // BUG #22513. МП: неправильна робота звіту при активації ТТ для акції 
            if (gridView.FocusedColumn == colActivationStatus
                //&& MainControl.ActivityInfo.IsPriceActivity
                && (gridView.FocusedValue == null
                    || gridView.FocusedValue == DBNull.Value
                    || ConvertEx.ToInt(gridView.FocusedValue) != 3))
                e.Cancel = true;
            if (status.Equals(RowStatus.Deleted.ToString()))
                e.Cancel = true;
        }

        private void gridView_ShownEditor(object sender, EventArgs e)
        {
            //BUG #22513. МП: неправильна робота звіту при активації ТТ для акції 
            //if (!MainControl.ActivityInfo.IsPriceActivity) return;
            GridView view = (GridView)sender;
            if (view.FocusedColumn == colActivationStatus && view.ActiveEditor is LookUpEdit && (ConvertEx.ToInt(view.FocusedValue) == 3 || ConvertEx.ToInt(view.FocusedValue) == 4))
            {
                Text = view.ActiveEditor.Parent.Name;
                LookUpEdit edit = (LookUpEdit) view.ActiveEditor;

                DataTable table = edit.Properties.DataSource as DataTable;
                clone = new DataView(table);
                clone.RowFilter = String.Format("[{0}] IN (3,4) ", Constants.SP_GET_ACTIVATION_STATUSES_FLD_ID);
                edit.Properties.DataSource = clone;
            }
            if (view.FocusedColumn == colSwitchOnDate && view.ActiveEditor is DateEdit && !ConvertEx.IsNull(view.GetFocusedRowCellValue(colSwitchOffDate)))
            {
                ((DateEdit) view.ActiveEditor).Properties.MaxValue = ConvertEx.ToDateTime(view.GetFocusedRowCellValue(colSwitchOffDate)).AddDays(-1);
            }
            if (view.FocusedColumn == colSwitchOffDate && view.ActiveEditor is DateEdit && !ConvertEx.IsNull(view.GetFocusedRowCellValue(colSwitchOnDate)))
            {
                ((DateEdit) view.ActiveEditor).Properties.MinValue = ConvertEx.ToDateTime(view.GetFocusedRowCellValue(colSwitchOnDate)).AddDays(1);
            }
        }

        private void manageAddressesList_DeleteBtnClick(object sender, EventArgs e)
        {
            RemoveSelectedAddresses();
        }

        private void manageAddressesList_FilterPreviewChanging(FilterPreviewChangingArgs args)
        {
            if (MainControl == null)
            {
                return;
            }

            args.PromoGrOutlets = MainControl.PromoGrOutlets;
            args.CtrlGrOutlets = MainControl.CtrlGrOutlets;
            args.IsCtrlGroup = IsControlGroup;
            args.ModuleId = Constants.MODULE_ID;
            args.EntityId = MainControl.ActivityId;
        }

        /// <summary>
        ///   Addresseses the list CTRL_ on display filter changed.
        /// </summary>
        /// <param name = "args">The args.</param>
        private void manageAddressesList_OnDisplayFilterChanged(FilterChangedArgs args)
        {
            var filter = String.Format("[{0}] In ({1})", colTTCode.FieldName, args.PocIds);

            colTTCode.FilterInfo = new ColumnFilterInfo(ColumnFilterType.Custom, filter);
        }

        private void manageAddressesList_OnFilterChanged(FilterChangedArgs args)
        {
            AddressesInformation data = args.FilterInfo;
            if (data.AddressesList != null && data.AddressesList.Count > 0)
            {
                addedAddresses.Add(data);
                AddPOC(data, AddressesList);
                FireUpdateMainView();
                FireTtChanged();
            }
        }

        #endregion

        #region Event Declarations

        [Category("Events")]
        [Description("Fires event when TT's added/deleted")]
        public event TTCountChangedEventArgs TTCountChanged;

        #endregion
    }
}