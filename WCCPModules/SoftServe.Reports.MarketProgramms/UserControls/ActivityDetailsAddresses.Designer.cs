﻿namespace SoftServe.Reports.MarketProgramms.UserControls
{
    partial class ActivityDetailsAddresses
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTTCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccepted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDeclined = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThRFilterId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeclineReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryDeclineLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colActivationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryActivationLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colSwitchOnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSwitchOnApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSwitchOffDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSwitchOffApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTLegalName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTFactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTaffChanel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOlType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusInList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStorageArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVInBasePeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageVBefore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTPremium = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInBevPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractInBev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractBBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC1InBev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractObolon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC2InBev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC1Obolon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC2Obolon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC1BBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC2BBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC1All = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXC2All = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCNotBeer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFridgersInBev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFridgersOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoolersInbev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoolersOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusTTinSW = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPromoTTinAnotherActivity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colControlTTinAnotherActivity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCumulativeVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryActivity = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.manageAddressesList = new SoftServe.Reports.MarketProgramms.UserControls.ManageAddressesList();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.colM4 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDeclineLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActivationLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manageAddressesList.WorkingArea)).BeginInit();
            this.manageAddressesList.WorkingArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 50;
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.ReadOnly = true;
            // 
            // gridControl
            // 
            this.gridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(2, 2);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryActivity,
            this.repositoryActivationLookUpEdit,
            this.repositoryDeclineLookUpEdit,
            this.repositoryItemCheckEdit1});
            this.gridControl.Size = new System.Drawing.Size(1188, 282);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView,
            this.gridView1});
            this.gridControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControl_KeyDown);
            this.gridControl.SizeChanged += new System.EventHandler(this.gridControl_SizeChanged);
            this.gridControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControl_KeyDown);
            this.gridControl.Resize += new System.EventHandler(this.gridControl_SizeChanged);
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 35;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStatus,
            this.colTTCode,
            this.colAccepted,
            this.colDeclined,
            this.colThRFilterId,
            this.colDeclineReason,
            this.colActivationStatus,
            this.colSwitchOnDate,
            this.colSwitchOnApproved,
            this.colSwitchOffDate,
            this.colSwitchOffApproved,
            this.colComment2,
            this.colRegion,
            this.colM4,
            this.colM3,
            this.colM2,
            this.colM1,
            this.colTTLegalName,
            this.colTTFactName,
            this.gridColumnAddress,
            this.colLegalAddress,
            this.colFactAddress,
            this.colSTaffChanel,
            this.colOlType,
            this.colStatusInList,
            this.colTTArea,
            this.colStorageArea,
            this.colVInBasePeriod,
            this.colAverageVBefore,
            this.colTTChannel,
            this.colTTPremium,
            this.colInBevPercent,
            this.colContractInBev,
            this.colContractBBH,
            this.colXC1InBev,
            this.colContractObolon,
            this.colXC2InBev,
            this.colXC1Obolon,
            this.colXC2Obolon,
            this.colXC1BBH,
            this.colXC2BBH,
            this.colXC1All,
            this.colXC2All,
            this.colXCNotBeer,
            this.colFridgersInBev,
            this.colFridgersOther,
            this.colCoolersInbev,
            this.colCoolersOther,
            this.colStatusTTinSW,
            this.colPromoTTinAnotherActivity,
            this.colControlTTinAnotherActivity,
            this.colIsContract,
            this.colCurrentVolume,
            this.colCumulativeVolume});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colStatus;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "New";
            styleFormatCondition2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Strikeout);
            styleFormatCondition2.Appearance.Options.UseFont = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colStatus;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = "Deleted";
            styleFormatCondition3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            styleFormatCondition3.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            styleFormatCondition3.Appearance.Options.UseBackColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colStatus;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = "Updated";
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2,
            styleFormatCondition3});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.gridView.OptionsMenu.EnableColumnMenu = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView_RowCellClick);
            this.gridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
            this.gridView.HiddenEditor += new System.EventHandler(this.gridView_HiddenEditor);
            this.gridView.ShownEditor += new System.EventHandler(this.gridView_ShownEditor);
            this.gridView.ColumnChanged += new System.EventHandler(this.gridControl_SizeChanged);
            this.gridView.ColumnPositionChanged += new System.EventHandler(this.gridControl_SizeChanged);
            this.gridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanging);
            this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
            // 
            // colTTCode
            // 
            this.colTTCode.Caption = "Код ТТ";
            this.colTTCode.FieldName = "OL_ID";
            this.colTTCode.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTTCode.MinWidth = 50;
            this.colTTCode.Name = "colTTCode";
            this.colTTCode.OptionsColumn.ReadOnly = true;
            this.colTTCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTTCode.Visible = true;
            this.colTTCode.VisibleIndex = 0;
            this.colTTCode.Width = 98;
            // 
            // colAccepted
            // 
            this.colAccepted.Caption = "Подтверждена";
            this.colAccepted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccepted.FieldName = "ISAPPROVED";
            this.colAccepted.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAccepted.MinWidth = 30;
            this.colAccepted.Name = "colAccepted";
            this.colAccepted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAccepted.Visible = true;
            this.colAccepted.VisibleIndex = 1;
            this.colAccepted.Width = 78;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.DisplayValueChecked = "1";
            this.repositoryItemCheckEdit1.DisplayValueUnchecked = "0";
            this.repositoryItemCheckEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colDeclined
            // 
            this.colDeclined.Caption = "Отказ";
            this.colDeclined.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDeclined.FieldName = "ISDECLINED";
            this.colDeclined.MinWidth = 50;
            this.colDeclined.Name = "colDeclined";
            this.colDeclined.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDeclined.Visible = true;
            this.colDeclined.VisibleIndex = 2;
            this.colDeclined.Width = 50;
            // 
            // colThRFilterId
            // 
            this.colThRFilterId.Caption = "ThRFilterId";
            this.colThRFilterId.FieldName = "ThRFilterId";
            this.colThRFilterId.MinWidth = 50;
            this.colThRFilterId.Name = "colThRFilterId";
            this.colThRFilterId.OptionsColumn.ReadOnly = true;
            // 
            // colDeclineReason
            // 
            this.colDeclineReason.Caption = "Причина отказа";
            this.colDeclineReason.ColumnEdit = this.repositoryDeclineLookUpEdit;
            this.colDeclineReason.FieldName = "DECLINE_REASON_ID";
            this.colDeclineReason.MinWidth = 50;
            this.colDeclineReason.Name = "colDeclineReason";
            this.colDeclineReason.Visible = true;
            this.colDeclineReason.VisibleIndex = 5;
            this.colDeclineReason.Width = 60;
            // 
            // repositoryDeclineLookUpEdit
            // 
            this.repositoryDeclineLookUpEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryDeclineLookUpEdit.AutoHeight = false;
            this.repositoryDeclineLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDeclineLookUpEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("REASON", "Причина отказа")});
            this.repositoryDeclineLookUpEdit.DisplayMember = "REASON";
            this.repositoryDeclineLookUpEdit.Name = "repositoryDeclineLookUpEdit";
            this.repositoryDeclineLookUpEdit.NullText = "";
            this.repositoryDeclineLookUpEdit.ValueMember = "DECLINE_REASON_ID";
            // 
            // colActivationStatus
            // 
            this.colActivationStatus.Caption = "Статус активации";
            this.colActivationStatus.ColumnEdit = this.repositoryActivationLookUpEdit;
            this.colActivationStatus.FieldName = "ACTIVATION_STATUS_ID";
            this.colActivationStatus.MinWidth = 50;
            this.colActivationStatus.Name = "colActivationStatus";
            this.colActivationStatus.Visible = true;
            this.colActivationStatus.VisibleIndex = 6;
            this.colActivationStatus.Width = 77;
            // 
            // repositoryActivationLookUpEdit
            // 
            this.repositoryActivationLookUpEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryActivationLookUpEdit.AutoHeight = false;
            this.repositoryActivationLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActivationLookUpEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ACTIVATION_STATUS_NAME", "Статус активации")});
            this.repositoryActivationLookUpEdit.DisplayMember = "ACTIVATION_STATUS_NAME";
            this.repositoryActivationLookUpEdit.Name = "repositoryActivationLookUpEdit";
            this.repositoryActivationLookUpEdit.NullText = "";
            this.repositoryActivationLookUpEdit.ValueMember = "ACTIVATION_STATUS_ID";
            // 
            // colSwitchOnDate
            // 
            this.colSwitchOnDate.Caption = "Дата подключения";
            this.colSwitchOnDate.FieldName = "STARTDATE";
            this.colSwitchOnDate.MinWidth = 50;
            this.colSwitchOnDate.Name = "colSwitchOnDate";
            this.colSwitchOnDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSwitchOnDate.Visible = true;
            this.colSwitchOnDate.VisibleIndex = 3;
            this.colSwitchOnDate.Width = 83;
            // 
            // colSwitchOnApproved
            // 
            this.colSwitchOnApproved.Caption = "Подключение подтверждено";
            this.colSwitchOnApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSwitchOnApproved.FieldName = "ISSTARTAPPROVED";
            this.colSwitchOnApproved.MinWidth = 50;
            this.colSwitchOnApproved.Name = "colSwitchOnApproved";
            this.colSwitchOnApproved.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSwitchOnApproved.Visible = true;
            this.colSwitchOnApproved.VisibleIndex = 4;
            this.colSwitchOnApproved.Width = 96;
            // 
            // colSwitchOffDate
            // 
            this.colSwitchOffDate.Caption = "Дата отключения";
            this.colSwitchOffDate.FieldName = "ENDDATE";
            this.colSwitchOffDate.MinWidth = 50;
            this.colSwitchOffDate.Name = "colSwitchOffDate";
            this.colSwitchOffDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSwitchOffDate.Visible = true;
            this.colSwitchOffDate.VisibleIndex = 8;
            this.colSwitchOffDate.Width = 80;
            // 
            // colSwitchOffApproved
            // 
            this.colSwitchOffApproved.Caption = "Отключение подтверждено";
            this.colSwitchOffApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSwitchOffApproved.FieldName = "ISENDAPPROVED";
            this.colSwitchOffApproved.MinWidth = 50;
            this.colSwitchOffApproved.Name = "colSwitchOffApproved";
            this.colSwitchOffApproved.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSwitchOffApproved.Visible = true;
            this.colSwitchOffApproved.VisibleIndex = 9;
            this.colSwitchOffApproved.Width = 98;
            // 
            // colComment2
            // 
            this.colComment2.Caption = "Комментарий";
            this.colComment2.FieldName = "COMMENT";
            this.colComment2.MinWidth = 50;
            this.colComment2.Name = "colComment2";
            this.colComment2.Visible = true;
            this.colComment2.VisibleIndex = 7;
            this.colComment2.Width = 90;
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "REGION_NAME";
            this.colRegion.MinWidth = 50;
            this.colRegion.Name = "colRegion";
            this.colRegion.OptionsColumn.ReadOnly = true;
            this.colRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 10;
            this.colRegion.Width = 60;
            // 
            // colM3
            // 
            this.colM3.Caption = "M3";
            this.colM3.FieldName = "M3_NAME";
            this.colM3.MinWidth = 50;
            this.colM3.Name = "colM3";
            this.colM3.OptionsColumn.ReadOnly = true;
            this.colM3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3.Visible = true;
            this.colM3.VisibleIndex = 12;
            this.colM3.Width = 60;
            // 
            // colM2
            // 
            this.colM2.Caption = "M2";
            this.colM2.FieldName = "M2_NAME";
            this.colM2.MinWidth = 50;
            this.colM2.Name = "colM2";
            this.colM2.OptionsColumn.ReadOnly = true;
            this.colM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2.Visible = true;
            this.colM2.VisibleIndex = 13;
            this.colM2.Width = 60;
            // 
            // colM1
            // 
            this.colM1.Caption = "M1";
            this.colM1.FieldName = "M1_NAME";
            this.colM1.MinWidth = 50;
            this.colM1.Name = "colM1";
            this.colM1.OptionsColumn.ReadOnly = true;
            this.colM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1.Visible = true;
            this.colM1.VisibleIndex = 14;
            this.colM1.Width = 60;
            // 
            // colTTLegalName
            // 
            this.colTTLegalName.Caption = "Юр. имя ТТ";
            this.colTTLegalName.FieldName = "OLTradingName";
            this.colTTLegalName.MinWidth = 50;
            this.colTTLegalName.Name = "colTTLegalName";
            this.colTTLegalName.OptionsColumn.ReadOnly = true;
            this.colTTLegalName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTTLegalName.Visible = true;
            this.colTTLegalName.VisibleIndex = 15;
            this.colTTLegalName.Width = 60;
            // 
            // colTTFactName
            // 
            this.colTTFactName.Caption = "Факт. имя ТТ";
            this.colTTFactName.FieldName = "OLName";
            this.colTTFactName.MinWidth = 50;
            this.colTTFactName.Name = "colTTFactName";
            this.colTTFactName.OptionsColumn.ReadOnly = true;
            this.colTTFactName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTTFactName.Visible = true;
            this.colTTFactName.VisibleIndex = 16;
            this.colTTFactName.Width = 60;
            // 
            // gridColumnAddress
            // 
            this.gridColumnAddress.Caption = "Нас. пункт";
            this.gridColumnAddress.FieldName = "CITY_NAME";
            this.gridColumnAddress.MinWidth = 50;
            this.gridColumnAddress.Name = "gridColumnAddress";
            this.gridColumnAddress.OptionsColumn.ReadOnly = true;
            this.gridColumnAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnAddress.Visible = true;
            this.gridColumnAddress.VisibleIndex = 19;
            this.gridColumnAddress.Width = 60;
            // 
            // colLegalAddress
            // 
            this.colLegalAddress.Caption = "Юр. адрес";
            this.colLegalAddress.FieldName = "OLAddress";
            this.colLegalAddress.MinWidth = 50;
            this.colLegalAddress.Name = "colLegalAddress";
            this.colLegalAddress.OptionsColumn.ReadOnly = true;
            this.colLegalAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLegalAddress.Visible = true;
            this.colLegalAddress.VisibleIndex = 17;
            this.colLegalAddress.Width = 60;
            // 
            // colFactAddress
            // 
            this.colFactAddress.Caption = "Факт. Адрес";
            this.colFactAddress.FieldName = "OLDeliveryAddress";
            this.colFactAddress.MinWidth = 50;
            this.colFactAddress.Name = "colFactAddress";
            this.colFactAddress.OptionsColumn.ReadOnly = true;
            this.colFactAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colFactAddress.Visible = true;
            this.colFactAddress.VisibleIndex = 18;
            this.colFactAddress.Width = 60;
            // 
            // colSTaffChanel
            // 
            this.colSTaffChanel.Caption = "Канал персонала";
            this.colSTaffChanel.FieldName = "TRADE_CHANNEL";
            this.colSTaffChanel.MinWidth = 50;
            this.colSTaffChanel.Name = "colSTaffChanel";
            this.colSTaffChanel.OptionsColumn.ReadOnly = true;
            this.colSTaffChanel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSTaffChanel.Visible = true;
            this.colSTaffChanel.VisibleIndex = 21;
            this.colSTaffChanel.Width = 70;
            // 
            // colOlType
            // 
            this.colOlType.Caption = "Тип ТТ";
            this.colOlType.FieldName = "OL_TYPE";
            this.colOlType.MinWidth = 50;
            this.colOlType.Name = "colOlType";
            this.colOlType.OptionsColumn.ReadOnly = true;
            this.colOlType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOlType.Visible = true;
            this.colOlType.VisibleIndex = 20;
            this.colOlType.Width = 53;
            // 
            // colStatusInList
            // 
            this.colStatusInList.Caption = "Статус ТТ в адреске";
            this.colStatusInList.FieldName = "colStatusInList";
            this.colStatusInList.MinWidth = 50;
            this.colStatusInList.Name = "colStatusInList";
            this.colStatusInList.OptionsColumn.ReadOnly = true;
            this.colStatusInList.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colStatusInList.UnboundExpression = "Iif([Status] == \'New\', \'Новая\' , Iif([Status] == \'Deleted\', \'Удалена\' , \'\'))";
            this.colStatusInList.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colStatusInList.Visible = true;
            this.colStatusInList.VisibleIndex = 22;
            // 
            // colTTArea
            // 
            this.colTTArea.Caption = "Площадь ТТ";
            this.colTTArea.FieldName = "OLSize";
            this.colTTArea.MinWidth = 50;
            this.colTTArea.Name = "colTTArea";
            this.colTTArea.OptionsColumn.ReadOnly = true;
            this.colTTArea.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTTArea.Width = 20;
            // 
            // colStorageArea
            // 
            this.colStorageArea.Caption = "Площадь склада";
            this.colStorageArea.FieldName = "OLWHSize";
            this.colStorageArea.MinWidth = 50;
            this.colStorageArea.Name = "colStorageArea";
            this.colStorageArea.OptionsColumn.ReadOnly = true;
            this.colStorageArea.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colStorageArea.Width = 20;
            // 
            // colVInBasePeriod
            // 
            this.colVInBasePeriod.Caption = "V в базовом периоде дал";
            this.colVInBasePeriod.FieldName = "TOTAL_VOLUME";
            this.colVInBasePeriod.MinWidth = 50;
            this.colVInBasePeriod.Name = "colVInBasePeriod";
            this.colVInBasePeriod.OptionsColumn.ReadOnly = true;
            this.colVInBasePeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colVInBasePeriod.Width = 20;
            // 
            // colAverageVBefore
            // 
            this.colAverageVBefore.Caption = "Сред V в базовом периоде, дал";
            this.colAverageVBefore.FieldName = "AVERAGE_VOLUME";
            this.colAverageVBefore.MinWidth = 50;
            this.colAverageVBefore.Name = "colAverageVBefore";
            this.colAverageVBefore.OptionsColumn.ReadOnly = true;
            this.colAverageVBefore.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAverageVBefore.Width = 20;
            // 
            // colTTChannel
            // 
            this.colTTChannel.Caption = "Канал ТТ";
            this.colTTChannel.FieldName = "OL_CHANNEL";
            this.colTTChannel.MinWidth = 50;
            this.colTTChannel.Name = "colTTChannel";
            this.colTTChannel.OptionsColumn.ReadOnly = true;
            this.colTTChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTTChannel.Width = 20;
            // 
            // colTTPremium
            // 
            this.colTTPremium.Caption = "Премиальность ТТ";
            this.colTTPremium.FieldName = "PREMIALITY";
            this.colTTPremium.MinWidth = 50;
            this.colTTPremium.Name = "colTTPremium";
            this.colTTPremium.OptionsColumn.ReadOnly = true;
            this.colTTPremium.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTTPremium.Width = 20;
            // 
            // colInBevPercent
            // 
            this.colInBevPercent.Caption = "Доля InBev %";
            this.colInBevPercent.DisplayFormat.FormatString = "P";
            this.colInBevPercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colInBevPercent.FieldName = "SHAREINBEV";
            this.colInBevPercent.MinWidth = 50;
            this.colInBevPercent.Name = "colInBevPercent";
            this.colInBevPercent.OptionsColumn.ReadOnly = true;
            this.colInBevPercent.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInBevPercent.Width = 20;
            // 
            // colContractInBev
            // 
            this.colContractInBev.Caption = "Контракт InBev";
            this.colContractInBev.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colContractInBev.FieldName = "CONTRACT_INBEV";
            this.colContractInBev.MinWidth = 50;
            this.colContractInBev.Name = "colContractInBev";
            this.colContractInBev.OptionsColumn.ReadOnly = true;
            this.colContractInBev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colContractInBev.Width = 20;
            // 
            // colContractBBH
            // 
            this.colContractBBH.Caption = "Контракт ВВН";
            this.colContractBBH.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colContractBBH.FieldName = "CONTRACT_BBH";
            this.colContractBBH.MinWidth = 50;
            this.colContractBBH.Name = "colContractBBH";
            this.colContractBBH.OptionsColumn.ReadOnly = true;
            this.colContractBBH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colContractBBH.Width = 20;
            // 
            // colXC1InBev
            // 
            this.colXC1InBev.Caption = "ХШ 1 - дверный InBev";
            this.colXC1InBev.FieldName = "ONEDOORFRIDGES_INBEV";
            this.colXC1InBev.MinWidth = 50;
            this.colXC1InBev.Name = "colXC1InBev";
            this.colXC1InBev.OptionsColumn.ReadOnly = true;
            this.colXC1InBev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC1InBev.Width = 20;
            // 
            // colContractObolon
            // 
            this.colContractObolon.Caption = "Контракт Оболонь";
            this.colContractObolon.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colContractObolon.FieldName = "CONTRACT_OBOLON";
            this.colContractObolon.MinWidth = 50;
            this.colContractObolon.Name = "colContractObolon";
            this.colContractObolon.OptionsColumn.ReadOnly = true;
            this.colContractObolon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colContractObolon.Width = 20;
            // 
            // colXC2InBev
            // 
            this.colXC2InBev.Caption = "ХШ 2 - дверный InBev";
            this.colXC2InBev.FieldName = "TWODOORFRIDGES_INBEV";
            this.colXC2InBev.MinWidth = 50;
            this.colXC2InBev.Name = "colXC2InBev";
            this.colXC2InBev.OptionsColumn.ReadOnly = true;
            this.colXC2InBev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC2InBev.Width = 20;
            // 
            // colXC1Obolon
            // 
            this.colXC1Obolon.Caption = "ХШ 1 - дверный Оболонь";
            this.colXC1Obolon.FieldName = "ONEDOORFRIDGES_OBOLON";
            this.colXC1Obolon.MinWidth = 50;
            this.colXC1Obolon.Name = "colXC1Obolon";
            this.colXC1Obolon.OptionsColumn.ReadOnly = true;
            this.colXC1Obolon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC1Obolon.Width = 20;
            // 
            // colXC2Obolon
            // 
            this.colXC2Obolon.Caption = "ХШ 2 - дверный Оболонь";
            this.colXC2Obolon.FieldName = "TWODOORFRIDGES_OBOLON";
            this.colXC2Obolon.MinWidth = 50;
            this.colXC2Obolon.Name = "colXC2Obolon";
            this.colXC2Obolon.OptionsColumn.ReadOnly = true;
            this.colXC2Obolon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC2Obolon.Width = 20;
            // 
            // colXC1BBH
            // 
            this.colXC1BBH.Caption = "ХШ 1 - дверный ВВН";
            this.colXC1BBH.FieldName = "ONEDOORFRIDGES_BBH";
            this.colXC1BBH.MinWidth = 50;
            this.colXC1BBH.Name = "colXC1BBH";
            this.colXC1BBH.OptionsColumn.ReadOnly = true;
            this.colXC1BBH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC1BBH.Width = 20;
            // 
            // colXC2BBH
            // 
            this.colXC2BBH.Caption = "ХШ 2 - дверный ВВН";
            this.colXC2BBH.FieldName = "TWODOORFRIDGES_BBH";
            this.colXC2BBH.MinWidth = 50;
            this.colXC2BBH.Name = "colXC2BBH";
            this.colXC2BBH.OptionsColumn.ReadOnly = true;
            this.colXC2BBH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC2BBH.Width = 20;
            // 
            // colXC1All
            // 
            this.colXC1All.Caption = "ХШ 1 - дверный Всего";
            this.colXC1All.FieldName = "ONEDOORFRIDGES";
            this.colXC1All.MinWidth = 50;
            this.colXC1All.Name = "colXC1All";
            this.colXC1All.OptionsColumn.ReadOnly = true;
            this.colXC1All.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC1All.Width = 20;
            // 
            // colXC2All
            // 
            this.colXC2All.Caption = "ХШ 2 - дверный Всего";
            this.colXC2All.FieldName = "TWODOORFRIDGES";
            this.colXC2All.MinWidth = 50;
            this.colXC2All.Name = "colXC2All";
            this.colXC2All.OptionsColumn.ReadOnly = true;
            this.colXC2All.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXC2All.Width = 20;
            // 
            // colXCNotBeer
            // 
            this.colXCNotBeer.Caption = "ХШ не пивные";
            this.colXCNotBeer.FieldName = "NONBEERFRIDGES";
            this.colXCNotBeer.MinWidth = 50;
            this.colXCNotBeer.Name = "colXCNotBeer";
            this.colXCNotBeer.OptionsColumn.ReadOnly = true;
            this.colXCNotBeer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colXCNotBeer.Width = 20;
            // 
            // colFridgersInBev
            // 
            this.colFridgersInBev.Caption = "Охладители InBev";
            this.colFridgersInBev.FieldName = "FRIDGES_INBEV";
            this.colFridgersInBev.MinWidth = 50;
            this.colFridgersInBev.Name = "colFridgersInBev";
            this.colFridgersInBev.OptionsColumn.ReadOnly = true;
            this.colFridgersInBev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colFridgersOther
            // 
            this.colFridgersOther.Caption = "Охладители всех конкурентов";
            this.colFridgersOther.FieldName = "FRIDGES_OTHER";
            this.colFridgersOther.MinWidth = 50;
            this.colFridgersOther.Name = "colFridgersOther";
            this.colFridgersOther.OptionsColumn.ReadOnly = true;
            this.colFridgersOther.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colCoolersInbev
            // 
            this.colCoolersInbev.Caption = "РУ Inbev";
            this.colCoolersInbev.FieldName = "COOLERS_INBEV";
            this.colCoolersInbev.MinWidth = 50;
            this.colCoolersInbev.Name = "colCoolersInbev";
            this.colCoolersInbev.OptionsColumn.ReadOnly = true;
            this.colCoolersInbev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCoolersInbev.Width = 20;
            // 
            // colCoolersOther
            // 
            this.colCoolersOther.Caption = "РУ конкурентов";
            this.colCoolersOther.FieldName = "COOLERS_OTHER";
            this.colCoolersOther.MinWidth = 50;
            this.colCoolersOther.Name = "colCoolersOther";
            this.colCoolersOther.OptionsColumn.ReadOnly = true;
            this.colCoolersOther.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCoolersOther.Width = 20;
            // 
            // colStatusTTinSW
            // 
            this.colStatusTTinSW.Caption = "Статус ТТ в SW";
            this.colStatusTTinSW.FieldName = "SW_STATUS";
            this.colStatusTTinSW.MinWidth = 50;
            this.colStatusTTinSW.Name = "colStatusTTinSW";
            this.colStatusTTinSW.OptionsColumn.ReadOnly = true;
            this.colStatusTTinSW.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colStatusTTinSW.Width = 20;
            // 
            // colPromoTTinAnotherActivity
            // 
            this.colPromoTTinAnotherActivity.Caption = "Промо ТТ в др. активности";
            this.colPromoTTinAnotherActivity.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPromoTTinAnotherActivity.FieldName = "IS_PROMO_MP";
            this.colPromoTTinAnotherActivity.MinWidth = 50;
            this.colPromoTTinAnotherActivity.Name = "colPromoTTinAnotherActivity";
            this.colPromoTTinAnotherActivity.OptionsColumn.ReadOnly = true;
            this.colPromoTTinAnotherActivity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPromoTTinAnotherActivity.Width = 20;
            // 
            // colControlTTinAnotherActivity
            // 
            this.colControlTTinAnotherActivity.Caption = "Контрольная ТТ в другой активности";
            this.colControlTTinAnotherActivity.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colControlTTinAnotherActivity.FieldName = "IS_CONTROL_MP";
            this.colControlTTinAnotherActivity.MinWidth = 50;
            this.colControlTTinAnotherActivity.Name = "colControlTTinAnotherActivity";
            this.colControlTTinAnotherActivity.OptionsColumn.ReadOnly = true;
            this.colControlTTinAnotherActivity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colControlTTinAnotherActivity.Width = 20;
            // 
            // colIsContract
            // 
            this.colIsContract.Caption = "Промо ТТ участвует в контрактах";
            this.colIsContract.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsContract.FieldName = "IS_CONTRACT";
            this.colIsContract.Name = "colIsContract";
            this.colIsContract.OptionsColumn.ReadOnly = true;
            // 
            // colCurrentVolume
            // 
            this.colCurrentVolume.Caption = "V продаж за текущий период, дал";
            this.colCurrentVolume.FieldName = "CURRENT_VOLUME";
            this.colCurrentVolume.Name = "colCurrentVolume";
            // 
            // colCumulativeVolume
            // 
            this.colCumulativeVolume.Caption = "V продаж за М-1, дал";
            this.colCumulativeVolume.FieldName = "Cumulative_VOLUME";
            this.colCumulativeVolume.Name = "colCumulativeVolume";
            // 
            // repositoryActivity
            // 
            this.repositoryActivity.AutoHeight = false;
            this.repositoryActivity.Name = "repositoryActivity";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl;
            this.gridView1.Name = "gridView1";
            // 
            // manageAddressesList
            // 
            this.manageAddressesList.ChanelIdFieldName = null;
            this.manageAddressesList.ChanelsDdlVisible = false;
            this.manageAddressesList.ChannelId = null;
            this.manageAddressesList.CommentsForFiltersRequired = true;
            this.manageAddressesList.DisableFrozenColumnsMoving = true;
            this.manageAddressesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manageAddressesList.EditMode = Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess.AddressEditMode.AllowAll;
            this.manageAddressesList.FrozenColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.manageAddressesList.InBevVolumeFieldName = null;
            this.manageAddressesList.InsertButtonText = null;
            this.manageAddressesList.InvisibleColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.manageAddressesList.Location = new System.Drawing.Point(0, 0);
            this.manageAddressesList.Margin = new System.Windows.Forms.Padding(4);
            this.manageAddressesList.Name = "manageAddressesList";
            this.manageAddressesList.Size = new System.Drawing.Size(1192, 381);
            this.manageAddressesList.StatisticsText = "Кол-во ТТ 123, Общая доля InBev 45%, Средний V на ТТ до активности 345 дал";
            this.manageAddressesList.SumColumnOlId = "OL_ID";
            this.manageAddressesList.SumColumnSummaryV = "Cumulative_VOLUME";
            this.manageAddressesList.TabIndex = 0;
            this.manageAddressesList.TargetGrid = this.gridControl;
            // 
            // 
            // 
            this.manageAddressesList.WorkingArea.Controls.Add(this.gridControl);
            this.manageAddressesList.WorkingArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manageAddressesList.WorkingArea.Location = new System.Drawing.Point(0, 95);
            this.manageAddressesList.WorkingArea.Margin = new System.Windows.Forms.Padding(4);
            this.manageAddressesList.WorkingArea.Name = "WorkingArea";
            this.manageAddressesList.WorkingArea.Size = new System.Drawing.Size(1192, 286);
            this.manageAddressesList.WorkingArea.TabIndex = 5;
            this.manageAddressesList.DeleteBtnClick += new System.EventHandler(this.manageAddressesList_DeleteBtnClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.xtraScrollableControl1);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(1192, 407);
            this.splitContainer1.SplitterDistance = 381;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.AutoScrollMinSize = new System.Drawing.Size(700, 200);
            this.xtraScrollableControl1.Controls.Add(this.manageAddressesList);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1192, 381);
            this.xtraScrollableControl1.TabIndex = 10;
            // 
            // colM4
            // 
            this.colM4.Caption = "M4";
            this.colM4.FieldName = "M4_NAME";
            this.colM4.MinWidth = 50;
            this.colM4.Name = "colM4";
            this.colM4.OptionsColumn.ReadOnly = true;
            this.colM4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM4.Visible = true;
            this.colM4.VisibleIndex = 11;
            this.colM4.Width = 60;
            // 
            // ActivityDetailsAddresses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "ActivityDetailsAddresses";
            this.Size = new System.Drawing.Size(1192, 407);
            this.SizeChanged += new System.EventHandler(this.ActivityDetailsAddresses_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDeclineLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActivationLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manageAddressesList.WorkingArea)).EndInit();
            this.manageAddressesList.WorkingArea.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colThRFilterId;
        private DevExpress.XtraGrid.Columns.GridColumn colTTCode;
        private DevExpress.XtraGrid.Columns.GridColumn colAccepted;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraGrid.Columns.GridColumn colM2;
        private DevExpress.XtraGrid.Columns.GridColumn colM1;
        private DevExpress.XtraGrid.Columns.GridColumn colTTLegalName;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colTTFactName;
        private DevExpress.XtraGrid.Columns.GridColumn colFactAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colTTArea;
        private DevExpress.XtraGrid.Columns.GridColumn colStorageArea;
        private DevExpress.XtraGrid.Columns.GridColumn colVInBasePeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageVBefore;
        private DevExpress.XtraGrid.Columns.GridColumn colTTChannel;
        private DevExpress.XtraGrid.Columns.GridColumn colTTPremium;
        private DevExpress.XtraGrid.Columns.GridColumn colInBevPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colContractInBev;
        private DevExpress.XtraGrid.Columns.GridColumn colContractBBH;
        private DevExpress.XtraGrid.Columns.GridColumn colContractObolon;
        private DevExpress.XtraGrid.Columns.GridColumn colXC1InBev;
        private DevExpress.XtraGrid.Columns.GridColumn colXC2InBev;
        private DevExpress.XtraGrid.Columns.GridColumn colXC1Obolon;
        private DevExpress.XtraGrid.Columns.GridColumn colXC2Obolon;
        private DevExpress.XtraGrid.Columns.GridColumn colXC1BBH;
        private DevExpress.XtraGrid.Columns.GridColumn colXC2BBH;
        private DevExpress.XtraGrid.Columns.GridColumn colXC1All;
        private DevExpress.XtraGrid.Columns.GridColumn colXC2All;
        private DevExpress.XtraGrid.Columns.GridColumn colXCNotBeer;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusTTinSW;
        private DevExpress.XtraGrid.Columns.GridColumn colPromoTTinAnotherActivity;
        private DevExpress.XtraGrid.Columns.GridColumn colControlTTinAnotherActivity;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private SoftServe.Reports.MarketProgramms.UserControls.ManageAddressesList manageAddressesList;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colSTaffChanel;
        private DevExpress.XtraGrid.Columns.GridColumn colOlType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusInList;
        private DevExpress.XtraGrid.Columns.GridColumn colDeclined;
        private DevExpress.XtraGrid.Columns.GridColumn colSwitchOnDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSwitchOnApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colSwitchOffApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colSwitchOffDate;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryActivity;
        private DevExpress.XtraGrid.Columns.GridColumn colDeclineReason;
        private DevExpress.XtraGrid.Columns.GridColumn colActivationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colComment2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActivationLookUpEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryDeclineLookUpEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colCoolersInbev;
        private DevExpress.XtraGrid.Columns.GridColumn colCoolersOther;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colFridgersInBev;
        private DevExpress.XtraGrid.Columns.GridColumn colFridgersOther;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colCumulativeVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colIsContract;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colM4;


    }
}
