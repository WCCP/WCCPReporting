﻿namespace SoftServe.Reports.MarketProgramms.UserControls {
    partial class ActivityDetailsOnInvoice {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnUpload2SW = new DevExpress.XtraEditors.SimpleButton();
            this.btnExcelLoad = new DevExpress.XtraEditors.SimpleButton();
            this.btnCheck = new DevExpress.XtraEditors.SimpleButton();
            this.btnCopy = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.onInvoiceGrid = new DevExpress.XtraGrid.GridControl();
            this.onInvoiceGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOutletId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colDateTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riSumEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDiscountPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riPercentEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPeriodId = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.onInvoiceGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onInvoiceGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDateEdit.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riSumEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPercentEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnUpload2SW);
            this.panelControl1.Controls.Add(this.btnExcelLoad);
            this.panelControl1.Controls.Add(this.btnCheck);
            this.panelControl1.Controls.Add(this.btnCopy);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(894, 36);
            this.panelControl1.TabIndex = 0;
            // 
            // btnUpload2SW
            // 
            this.btnUpload2SW.Image = global::SoftServe.Reports.MarketProgramms.Resource.Upload;
            this.btnUpload2SW.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnUpload2SW.Location = new System.Drawing.Point(212, 2);
            this.btnUpload2SW.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpload2SW.Name = "btnUpload2SW";
            this.btnUpload2SW.Size = new System.Drawing.Size(32, 32);
            this.btnUpload2SW.TabIndex = 40;
            this.btnUpload2SW.ToolTip = "Выгрузить в SW";
            this.btnUpload2SW.Click += new System.EventHandler(this.btnUpload2SW_Click);
            // 
            // btnExcelLoad
            // 
            this.btnExcelLoad.Image = global::SoftServe.Reports.MarketProgramms.Resource.Excel_24;
            this.btnExcelLoad.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnExcelLoad.Location = new System.Drawing.Point(178, 2);
            this.btnExcelLoad.Margin = new System.Windows.Forms.Padding(2);
            this.btnExcelLoad.Name = "btnExcelLoad";
            this.btnExcelLoad.Size = new System.Drawing.Size(32, 32);
            this.btnExcelLoad.TabIndex = 39;
            this.btnExcelLoad.ToolTip = "Загрузить из Excel";
            this.btnExcelLoad.Click += new System.EventHandler(this.btnExcelLoad_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.Image = global::SoftServe.Reports.MarketProgramms.Resource.check;
            this.btnCheck.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCheck.Location = new System.Drawing.Point(110, 2);
            this.btnCheck.Margin = new System.Windows.Forms.Padding(2);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(32, 32);
            this.btnCheck.TabIndex = 38;
            this.btnCheck.ToolTip = "Проверить диапазоны дат";
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Image = global::SoftServe.Reports.MarketProgramms.Resource.copy;
            this.btnCopy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCopy.Location = new System.Drawing.Point(76, 2);
            this.btnCopy.Margin = new System.Windows.Forms.Padding(2);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(32, 32);
            this.btnCopy.TabIndex = 37;
            this.btnCopy.ToolTip = "Сделать копию записи";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::SoftServe.Reports.MarketProgramms.Resource.save;
            this.btnSave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSave.Location = new System.Drawing.Point(144, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(32, 32);
            this.btnSave.TabIndex = 36;
            this.btnSave.ToolTip = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::SoftServe.Reports.MarketProgramms.Resource.delete;
            this.btnDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDelete.Location = new System.Drawing.Point(42, 2);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(32, 32);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.ToolTip = "Удалить запись";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::SoftServe.Reports.MarketProgramms.Resource.add;
            this.btnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(8, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(32, 32);
            this.btnAdd.TabIndex = 34;
            this.btnAdd.ToolTip = "Добавить ТТ";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.onInvoiceGrid);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 36);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(894, 351);
            this.panelControl2.TabIndex = 1;
            // 
            // onInvoiceGrid
            // 
            this.onInvoiceGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.onInvoiceGrid.Location = new System.Drawing.Point(2, 2);
            this.onInvoiceGrid.MainView = this.onInvoiceGridView;
            this.onInvoiceGrid.Name = "onInvoiceGrid";
            this.onInvoiceGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riDateEdit,
            this.riSumEdit,
            this.riPercentEdit});
            this.onInvoiceGrid.Size = new System.Drawing.Size(890, 347);
            this.onInvoiceGrid.TabIndex = 0;
            this.onInvoiceGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.onInvoiceGridView});
            // 
            // onInvoiceGridView
            // 
            this.onInvoiceGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOutletId,
            this.colDateFrom,
            this.colDateTo,
            this.colDiscountSum,
            this.colDiscountPercent,
            this.colPeriodId});
            this.onInvoiceGridView.GridControl = this.onInvoiceGrid;
            this.onInvoiceGridView.Name = "onInvoiceGridView";
            this.onInvoiceGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.onInvoiceGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.onInvoiceGridView.OptionsBehavior.AutoPopulateColumns = false;
            this.onInvoiceGridView.OptionsCustomization.AllowGroup = false;
            this.onInvoiceGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.onInvoiceGridView.OptionsDetail.EnableMasterViewMode = false;
            this.onInvoiceGridView.OptionsDetail.ShowDetailTabs = false;
            this.onInvoiceGridView.OptionsDetail.SmartDetailExpand = false;
            this.onInvoiceGridView.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.onInvoiceGridView.OptionsMenu.EnableColumnMenu = false;
            this.onInvoiceGridView.OptionsMenu.EnableFooterMenu = false;
            this.onInvoiceGridView.OptionsMenu.EnableGroupPanelMenu = false;
            this.onInvoiceGridView.OptionsSelection.MultiSelect = true;
            this.onInvoiceGridView.OptionsView.ColumnAutoWidth = false;
            this.onInvoiceGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.onInvoiceGridView.OptionsView.EnableAppearanceOddRow = true;
            this.onInvoiceGridView.OptionsView.ShowAutoFilterRow = true;
            this.onInvoiceGridView.OptionsView.ShowDetailButtons = false;
            this.onInvoiceGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colOutletId
            // 
            this.colOutletId.Caption = "Код ТТ";
            this.colOutletId.FieldName = "OutletId";
            this.colOutletId.Name = "colOutletId";
            this.colOutletId.OptionsColumn.AllowEdit = false;
            this.colOutletId.OptionsColumn.ReadOnly = true;
            this.colOutletId.Visible = true;
            this.colOutletId.VisibleIndex = 0;
            this.colOutletId.Width = 128;
            // 
            // colDateFrom
            // 
            this.colDateFrom.Caption = "Дата с";
            this.colDateFrom.ColumnEdit = this.riDateEdit;
            this.colDateFrom.DisplayFormat.FormatString = "d";
            this.colDateFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateFrom.FieldName = "DateFrom";
            this.colDateFrom.Name = "colDateFrom";
            this.colDateFrom.Visible = true;
            this.colDateFrom.VisibleIndex = 1;
            this.colDateFrom.Width = 99;
            // 
            // riDateEdit
            // 
            this.riDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.riDateEdit.AutoHeight = false;
            this.riDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riDateEdit.Name = "riDateEdit";
            this.riDateEdit.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // colDateTo
            // 
            this.colDateTo.Caption = "Дата по";
            this.colDateTo.ColumnEdit = this.riDateEdit;
            this.colDateTo.DisplayFormat.FormatString = "d";
            this.colDateTo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateTo.FieldName = "DateTo";
            this.colDateTo.Name = "colDateTo";
            this.colDateTo.Visible = true;
            this.colDateTo.VisibleIndex = 2;
            this.colDateTo.Width = 105;
            // 
            // colDiscountSum
            // 
            this.colDiscountSum.Caption = "Лимит Суммы Скидки";
            this.colDiscountSum.ColumnEdit = this.riSumEdit;
            this.colDiscountSum.DisplayFormat.FormatString = "0.00";
            this.colDiscountSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscountSum.FieldName = "DiscountSum";
            this.colDiscountSum.Name = "colDiscountSum";
            this.colDiscountSum.Visible = true;
            this.colDiscountSum.VisibleIndex = 3;
            this.colDiscountSum.Width = 138;
            // 
            // riSumEdit
            // 
            this.riSumEdit.AutoHeight = false;
            this.riSumEdit.DisplayFormat.FormatString = "n2";
            this.riSumEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.riSumEdit.EditFormat.FormatString = "n2";
            this.riSumEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.riSumEdit.Name = "riSumEdit";
            // 
            // colDiscountPercent
            // 
            this.colDiscountPercent.Caption = "Процент Скидки, %";
            this.colDiscountPercent.ColumnEdit = this.riPercentEdit;
            this.colDiscountPercent.FieldName = "DiscountPercent";
            this.colDiscountPercent.Name = "colDiscountPercent";
            this.colDiscountPercent.Visible = true;
            this.colDiscountPercent.VisibleIndex = 4;
            this.colDiscountPercent.Width = 128;
            // 
            // riPercentEdit
            // 
            this.riPercentEdit.AutoHeight = false;
            this.riPercentEdit.DisplayFormat.FormatString = "n2";
            this.riPercentEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.riPercentEdit.EditFormat.FormatString = "n2";
            this.riPercentEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.riPercentEdit.Name = "riPercentEdit";
            // 
            // colPeriodId
            // 
            this.colPeriodId.Caption = "PeriodId";
            this.colPeriodId.FieldName = "PeriodId";
            this.colPeriodId.Name = "colPeriodId";
            this.colPeriodId.OptionsColumn.AllowEdit = false;
            this.colPeriodId.OptionsColumn.AllowMove = false;
            this.colPeriodId.OptionsColumn.AllowShowHide = false;
            // 
            // ActivityDetailsOnInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "ActivityDetailsOnInvoice";
            this.Size = new System.Drawing.Size(894, 387);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.onInvoiceGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onInvoiceGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDateEdit.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riSumEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riPercentEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl onInvoiceGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView onInvoiceGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colOutletId;
        private DevExpress.XtraGrid.Columns.GridColumn colDateFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTo;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountSum;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountPercent;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit riDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riSumEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riPercentEdit;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCopy;
        private DevExpress.XtraEditors.SimpleButton btnUpload2SW;
        private DevExpress.XtraEditors.SimpleButton btnExcelLoad;
        private DevExpress.XtraEditors.SimpleButton btnCheck;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodId;
    }
}
