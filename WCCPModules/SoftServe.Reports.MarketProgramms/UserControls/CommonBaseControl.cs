﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using SoftServe.Reports.MarketProgramms.Tabs;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;

namespace SoftServe.Reports.MarketProgramms.UserControls {
    /// <summary>
    /// 
    /// </summary>
    [ToolboxItem(false)]
    public class CommonBaseControl : UserControl {
        public event EventHandler UpdateMainView;

        /// <summary>
        /// Determines whether current controd has already been loaded 
        /// </summary>
        internal bool WasControlLoaded { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        public CommonBaseControl() {
        }

        /// <summary>
        /// Fires the update main view.
        /// </summary>
        protected void FireUpdateMainView() {
            if (UpdateMainView != null)
                UpdateMainView(this, new EventArgs());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public CommonBaseControl(CommonBaseTab parentTab) {
            ParentTab = parentTab;
        }

        /// <summary>
        /// Gets or sets the parent tab.
        /// </summary>
        /// <value>The parent tab.</value>
        public CommonBaseTab ParentTab { get; set; }
        
        /// <summary>
        /// Sets the read only mode recursively.
        /// </summary>
        /// <param name="currentControl">The current control.</param>
        internal void SetReadOnlyModeRecursively(Control currentControl) {
            if (currentControl == null)
                return;
            
            foreach (Control control in currentControl.Controls) {
                if (control == null)
                    continue;
                
                if (control is BaseEdit) {
                    (control as BaseEdit).Properties.AllowFocused = false;
                    (control as BaseEdit).Properties.ReadOnly = true;
                }
                else if (control is IControlReadOnly)
                    (control as IControlReadOnly).ReadOnly = true;
                else if (control is GridControl) {
                    GridControl gridControl = control as GridControl;
                    foreach (GridView view in gridControl.Views) {
                        gridControl.UseEmbeddedNavigator = false;
                        foreach (GridColumn column in view.Columns) {
                            column.OptionsColumn.AllowEdit = false;
                            column.OptionsColumn.ReadOnly = true;
                        }
                    }
                }
                else if (control is TreeList) {
                    TreeList treeList = control as TreeList;
                    foreach (TreeListColumn column in treeList.Columns) {
                        column.OptionsColumn.AllowEdit = false;
                        column.OptionsColumn.ReadOnly = true;
                    }
                }
                else if (control.HasChildren)
                    SetReadOnlyModeRecursively(control);
            }
        }
    }
}