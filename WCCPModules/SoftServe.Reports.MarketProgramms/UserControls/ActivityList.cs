﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SoftServe.Reports.MarketProgramms.Forms;
using SoftServe.Reports.MarketProgramms.Tabs;
using SoftServe.Reports.MarketProgramms.Utility;

namespace SoftServe.Reports.MarketProgramms.UserControls
{
    /// <summary>
    /// 
    /// </summary>
    [ToolboxItem(false)]
    public partial class ActivityListControl : CommonBaseControl
    {
        public ActivityListControl()
        {
            InitializeComponent();
        }

        public ActivityListControl(CommonBaseTab parentTab)
            : base(parentTab)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the grid control.
        /// </summary>
        /// <value>The grid control.</value>
        public GridControl GridControl
        {
            get { return gridControl; }
        }

        public void LoadData()
        {
            regionsTree.LoadData();
            barButtonNew.Enabled = !DataProvider.IsLDB;
            DataTable activityList = DataProvider.ActivityList;
            gridControl.DataSource = activityList;
            gridView.BestFitColumns();

            gridView.ActiveFilterString = columnId.FieldName + " IN (" + regionsTree.SelectedActivityIds.ToString(",") + ")";
          
            UpdateButtonsState();
        }

        /// <summary>
        /// News this instance.
        /// </summary>
        private void New()
        {
            using (TemplateSelect ts = new TemplateSelect())
            {
                if (ts.ShowDialog() == DialogResult.OK)
                {
                    LoadDetailsTab(ts.SelectedTemplateId);
                }
            }
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        private void Edit()
        {
            LoadDetailsTab(-1);
        }

        /// <summary>
        /// Loads the details tab.
        /// </summary>
        /// <param name="templateId">if set to <c>true</c> [is edit mode].</param>
        private void LoadDetailsTab(int templateId)
        {
            if (ParentTab != null)
            {
                string newDetailsTabName = Resource.NewDetailsTabName;
                string activityName = string.Empty;
                int activityId = 0;
                if (templateId <= 0)
                {
                    activityId = Convert.ToInt32(gridView.GetFocusedRowCellValue(columnId));
                    string templateName = Convert.ToString(gridView.GetFocusedRowCellValue(columnActionTemplate));
                    activityName = Convert.ToString(gridView.GetFocusedRowCellValue(columnActionName));
                    newDetailsTabName = String.Format("{0} {1}", templateName, activityName);
                }
                if (!string.IsNullOrEmpty(newDetailsTabName))
                {
                    ParentTab.LoadDetailsTab(newDetailsTabName, activityName, activityId, templateId);
                }
            }
        }

        /// <summary>
        /// Updates the state of the buttons.
        /// </summary>
        private void UpdateButtonsState()
        {
            gridView.PostEditor();
            gridView.UpdateCurrentRow();

            barButtonEdit.Enabled = gridView.FocusedRowHandle >= 0;
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            UpdateButtonsState();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonNew control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Edit();
        }

        /// <summary>
        /// Handles the DoubleClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DoubleClick(object sender, EventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                Point pt = gridView.GridControl.PointToClient(MousePosition);
                GridHitInfo info = gridView.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    Edit();
                }
            }
        }

        private bool previousPopupVisibility;
        private void popupRegions_VisibleChanged(object sender, EventArgs e)
        {
            if (!popupRegions.Visible && previousPopupVisibility)
            {
                gridView.ActiveFilterString = columnId.FieldName + " IN (" + regionsTree.SelectedActivityIds.ToString(",") + ")";
            }
            previousPopupVisibility = popupRegions.Visible;
        }

        private void regionsTree_CloseBtnClick(object sender, EventArgs e)
        {
            if (popupRegions.Visible)
            {
                popupRegions.HidePopup();
            }
        }
    }
}