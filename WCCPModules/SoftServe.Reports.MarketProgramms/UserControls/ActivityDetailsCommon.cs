﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.CommonControls.MappingControl;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MarketProgramms.UserControls
{
    [ToolboxItem(false)]
    public partial class ActivityDetailsCommon : CommonBaseControl
    {
        #region Constants

        private const int DEFAULT_ACTIVITY_PERIOD = 1;

        #endregion

        #region Fields

        private int activityTypeId = -1;

        #endregion

        #region Constructors

        public ActivityDetailsCommon()
        {
            InitializeComponent();

            PopulateDateValues(DateTime.Now.Year);
        }

        #endregion

        #region Instance Properties

        public ActivityDetailsControl MainControl { get; set; }

        public LevelType SelectedLevel
        {
            get
            {
                LevelType type = LevelType.Unknown;
                ComboBoxItem item = comboBoxActivityLevel.EditValue as ComboBoxItem;

                if (item != null)
                {
                    type = (LevelType)item.Id;
                }

                return type;
            }
        }

        private DataTable ActivityData
        {
            set
            {
                {
                    DataRow activityDetailsRow = value.Rows[0];
                    activityTypeId = ConvertEx.ToInt(activityDetailsRow[Constants.SP_ACTIVITY_DETAILS_ACTIONTYPE_ID]);

                    #region Main

                    SetMainData(activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_USERNAME, Constants.SP_ACTIVITY_DETAILS_ISAPPROVED, Constants.SP_ACTIVITY_DETAILS_IS_BUDGET,
                                Constants.SP_ACTIVITY_DETAILS_ACTIONTYPE_NAME, Constants.SP_ACTIVITY_DETAILS_CHANELTYPE,
                                Constants.SP_ACTIVITY_DETAILS_ACTION_DESCRIPTION, Constants.SP_ACTIVITY_DETAILS_ACTION_NAME,
                                ConvertEx.ToDateTime(activityDetailsRow[Constants.SP_ACTIVITY_DETAILS_CREATION_DATE]));

                    int levelId = ConvertEx.ToInt(activityDetailsRow[Constants.SP_ACTIVITY_DETAILS_ACTION_LEVEL]);
                    foreach (object obj in comboBoxActivityLevel.Properties.Items)
                    {
                        if (((ComboBoxItem)obj).Id == levelId)
                        {
                            comboBoxActivityLevel.SelectedItem = obj;
                            break;
                        }
                    }

                    #endregion

                    #region Периоды

                    InitializeDateControlsSet(datePeriodDayFrom, datePeriodMonthFrom, datePeriodYearFrom, activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_ACTION_START);
                    InitializeDateControlsSet(datePeriodDayTo, datePeriodMonthTo, datePeriodYearTo, activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_ACTION_END);

                    InitializeDateControlsSet(datePeriod1DayFrom, datePeriod1MonthFrom, datePeriod1YearFrom, activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_BASE1_START);
                    InitializeDateControlsSet(datePeriod1DayTo, datePeriod1MonthTo, datePeriod1YearTo, activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_BASE1_END);

                    InitializeDateControlsSet(datePeriod2DayFrom, datePeriod2MonthFrom, datePeriod2YearFrom, activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_BASE2_START);
                    InitializeDateControlsSet(datePeriod2DayTo, datePeriod2MonthTo, datePeriod2YearTo, activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_BASE2_END);

                    checkEditTakeBasePeriodsFromActiveWave.Checked = ConvertEx.ToBool(activityDetailsRow[Constants.SP_ACTIVITY_DETAILS_ISBASEFROMTR]);
                    checkEditTakeBasePeriodsFromActiveWave_CheckedChanged(this, new EventArgs());
                    spinEditMonthsToTrackUpliftAfterEnd.Value = ConvertEx.ToInt(activityDetailsRow[Constants.SP_ACTIVITY_DETAILS_MONITORING_AFTER_ACTION_MONTH]);

                    #endregion

                    SetPositionsData(activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_INITIATOR, Constants.SP_ACTIVITY_DETAILS_PLANNER,
                                     Constants.SP_ACTIVITY_DETAILS_CONTROLLER, Constants.SP_ACTIVITY_DETAILS_APPROVER, Constants.SP_ACTIVITY_DETAILS_EXECUTOR,
                                     Constants.SP_ACTIVITY_DETAILS_OLCHECKER);

                    SetKpkData(activityDetailsRow, Constants.SP_ACTIVITY_DETAILS_ISSHOWMOBILE, Constants.SP_ACTIVITY_DETAILS_ISCHECKDATEMOBILE,
                               Constants.SP_ACTIVITY_DETAILS_MOBILECOMMENTTEMPLATE);
                }
            }
        }

        private DataTable TemplateData
        {
            set
            {
                DataRow templateDetailsRow = value.Rows[0];
                activityTypeId = ConvertEx.ToInt(templateDetailsRow[Constants.SP_TEMPLATE_DETAILS_ACTIONTYPE_ID]);

                #region Активность

                SetMainData(templateDetailsRow, Constants.SP_ACTIVITY_DETAILS_USERNAME, string.Empty, string.Empty,
                            Constants.SP_TEMPLATE_DETAILS_ACTIONTYPE_NAME, Constants.SP_TEMPLATE_DETAILS_CHANELTYPE,
                            Constants.SP_TEMPLATE_DETAILS_ACTION_DESCRIPTION, String.Empty,
                            DateTime.Now);

                lblTemplateName.Text = ConvertEx.ToString(templateDetailsRow[Constants.SP_TEMPLATE_DETAILS_ACTION_NAME]);
                int levelId = ConvertEx.ToInt(templateDetailsRow[Constants.SP_TEMPLATE_DETAILS_ACTION_LEVEL]);

                comboBoxActivityLevel.Properties.Items.Clear();
                ComboBoxItem[] res = DataProvider.GetActivityLevels(templateDetailsRow.Field<int>(Constants.SP_TEMPLATE_DETAILS_ACTION_ID)).AsEnumerable().Select(
                    row => new ComboBoxItem(row.Field<byte>(0), row.Field<string>(1))).ToArray();
                bool isLevelFound = res.Select(item => item.Id == levelId).Count() > 0;
                foreach (ComboBoxItem item in res)
                {
                    comboBoxActivityLevel.Properties.Items.Add(item);
                    if (item.Id == levelId || !isLevelFound)
                    {
                        comboBoxActivityLevel.SelectedItem = item;
                    }
                }

                #endregion

                #region Периоды

                datePeriodYearFrom.Value = DateTime.Now.Year;
                InitializeDateControl(datePeriodMonthFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_ACTION_START_MONTH, DateTime.Now.Month);
                InitializeDateControl(datePeriodDayFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_ACTION_START_DAY, DateTime.Now.Day);

                DateTime defaultActivityEnd = new DateTime(datePeriodYearFrom.Value, datePeriodMonthFrom.Value, datePeriodDayFrom.Value).AddMonths(DEFAULT_ACTIVITY_PERIOD);
                datePeriodYearTo.Value = defaultActivityEnd.Year;
                InitializeDateControl(datePeriodMonthTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_ACTION_END_MONTH, defaultActivityEnd.Month);
                InitializeDateControl(datePeriodDayTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_ACTION_END_DAY, defaultActivityEnd.Day);

                InitializeYearControl(datePeriod1YearFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE1_START_YEAR_OFFSET, DateTime.Now.Year - 1);
                InitializeDateControl(datePeriod1MonthFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE1_START_MONTH, DateTime.Now.Month);
                InitializeDateControl(datePeriod1DayFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE1_START_DAY, DateTime.Now.Day);

                //DateTime defaultBase1End = new DateTime(datePeriod1YearFrom.Value, datePeriod1MonthFrom.Value + DEFAULT_ACTIVITY_PERIOD, datePeriod1DayFrom.Value);//.AddMonths(DEFAULT_ACTIVITY_PERIOD); Ticket#1039279
                DateTime defaultBase1End = new DateTime(datePeriod1YearFrom.Value, datePeriod1MonthFrom.Value, datePeriod1DayFrom.Value).AddMonths(DEFAULT_ACTIVITY_PERIOD);
                InitializeYearControl(datePeriod1YearTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE1_END_YEAR_OFFSET, defaultBase1End.Year);
                InitializeDateControl(datePeriod1MonthTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE1_END_MONTH, defaultBase1End.Month);
                InitializeDateControl(datePeriod1DayTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE1_END_DAY, defaultBase1End.Day);

                InitializeYearControl(datePeriod2YearFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE2_START_YEAR_OFFSET, DateTime.Now.Year - 1);
                InitializeDateControl(datePeriod2MonthFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE2_START_MONTH, DateTime.Now.Month);
                InitializeDateControl(datePeriod2DayFrom, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE2_START_DAY, DateTime.Now.Day);

                //DateTime defaultBase2End = new DateTime(datePeriod2YearFrom.Value, datePeriod2MonthFrom.Value + DEFAULT_ACTIVITY_PERIOD, datePeriod2DayFrom.Value);//.AddMonths(DEFAULT_ACTIVITY_PERIOD); Ticket#1039279
                DateTime defaultBase2End = new DateTime(datePeriod2YearFrom.Value, datePeriod2MonthFrom.Value, datePeriod2DayFrom.Value).AddMonths(DEFAULT_ACTIVITY_PERIOD);
                InitializeYearControl(datePeriod2YearTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE2_END_YEAR_OFFSET, defaultBase2End.Year);
                InitializeDateControl(datePeriod2MonthTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE2_END_MONTH, defaultBase2End.Month);
                InitializeDateControl(datePeriod2DayTo, templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_BASE2_END_DAY, defaultBase2End.Day);

                checkEditTakeBasePeriodsFromActiveWave.Enabled = templateDetailsRow[Constants.SP_TEMPLATE_DETAILS_ISBASEFROMTR] == DBNull.Value;
                checkEditTakeBasePeriodsFromActiveWave.Checked = ConvertEx.ToBool(templateDetailsRow[Constants.SP_TEMPLATE_DETAILS_ISBASEFROMTR]);
                checkEditTakeBasePeriodsFromActiveWave_CheckedChanged(this, new EventArgs());
                spinEditMonthsToTrackUpliftAfterEnd.Value = ConvertEx.ToInt(templateDetailsRow[Constants.SP_TEMPLATE_DETAILS_MONITORING_AFTER_ACTION_MONTH]);

                #endregion

                SetPositionsData(templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_INITIATOR, Constants.SP_TEMPLATE_DETAILS_PLANNER,
                                 Constants.SP_TEMPLATE_DETAILS_CONTROLLER, Constants.SP_TEMPLATE_DETAILS_APPROVER, Constants.SP_TEMPLATE_DETAILS_EXECUTOR,
                                 Constants.SP_TEMPLATE_DETAILS_OLCHECKER);


                SetKpkData(templateDetailsRow, Constants.SP_TEMPLATE_DETAILS_ISSHOWMOBILE, Constants.SP_TEMPLATE_DETAILS_ISCHECKDATEMOBILE,
                           Constants.SP_TEMPLATE_DETAILS_MOBILECOMMENTTEMPLATE);

            }
        }

        #endregion

        #region Instance Methods

        public void GetAllFields(ActivityDetailsDTO dto)
        {
            dto.ActivityTypeId = activityTypeId;

            #region Основные

            dto.Name = Convert.ToString(textEditName.EditValue);
            dto.IsApproved = checkEditIsApproved.Checked;
            ComboBoxItem itemActivityLevel = comboBoxActivityLevel.SelectedItem as ComboBoxItem;
            dto.ActivityLevelId = itemActivityLevel != null ? itemActivityLevel.Id : 0;
            dto.Description = memoEditDescription.Text;

            #endregion

            #region Периоды

            dto.ActivityStart = new DateTime(datePeriodYearFrom.Value, datePeriodMonthFrom.Value, datePeriodDayFrom.Value);
            dto.ActivityEnd = new DateTime(datePeriodYearTo.Value, datePeriodMonthTo.Value, datePeriodDayTo.Value);
            dto.MonthsToTrackUpliftAfterEnd = Convert.ToInt32(spinEditMonthsToTrackUpliftAfterEnd.Value);
            dto.IsTakeActivePeriodsFromActiveWave = checkEditTakeBasePeriodsFromActiveWave.Checked;
            dto.Period1Start = new DateTime(datePeriod1YearFrom.Value, datePeriod1MonthFrom.Value, datePeriod1DayFrom.Value);
            dto.Period1End = new DateTime(datePeriod1YearTo.Value, datePeriod1MonthTo.Value, datePeriod1DayTo.Value);
            dto.Period2Start = new DateTime(datePeriod2YearFrom.Value, datePeriod2MonthFrom.Value, datePeriod2DayFrom.Value);
            dto.Period2End = new DateTime(datePeriod2YearTo.Value, datePeriod2MonthTo.Value, datePeriod2DayTo.Value);

            #endregion

            #region Промо группа

            dto.PromoCascadeEdit = checkEditCascadeEditPromo.Checked;
            dto.PromoAllowAddAll = checkEditAllowAddAllPromo.Checked;
            dto.PromoAllTTInActiveWave = checkEditAllTTInActivePromo.Checked;

            #endregion

            #region Контрольная группа

            dto.ControlCascadeEdit = checkEditCascadeEdit.Checked;
            dto.ControlAllowAddAll = checkEditAllowAddAll.Checked;
            dto.ControlAllTTInActiveWave = checkEditAllTTInActive.Checked;

            #endregion

            #region КПК

            dto.IsShowInKPK = checkEditShowInKPK.Checked;
            dto.IsConsiderConnectionDate = checkEditWithConnectionDate.Checked;
            dto.ActivityDescriptionTemplateInKPK = Convert.ToString(memoEditKPKDescriptionTemplate.EditValue);

            #endregion
        }

        public IEnumerable<FileUploader.FileDataInfo> GetDocumentChanges(DataRowState rowStates)
        {
            return documentUploader.GetFiles(rowStates);
        }

        public void LoadData(int activityId, int templateId, DataTable templateDetails, DataTable activityDetails)
        {
            TemplateData = templateDetails;
            double uplift = 0;

            if (activityId > 0)
            {
                ActivityData = activityDetails;
                documentUploader.Data = DataProvider.GetFilesList(activityId);
                double.TryParse(activityDetails.Rows[0]["Uplift"].ToString(), out uplift);
            }
            else
            {
                documentUploader.Data = DataProvider.GetFilesList(templateId);
            }

            UpliftResultLabel.Text = uplift.ToString("P");

            checkEditCascadeEditPromo.Checked = MainControl.ActivityInfo.PromoGroupCascadeEdit;
            checkEditAllowAddAllPromo.Checked = MainControl.ActivityInfo.PromoGroupAllowAddAll;
            checkEditAllTTInActivePromo.Checked = MainControl.ActivityInfo.PromoGroupAllTTInActive;
            checkEditCascadeEdit.Checked = MainControl.ActivityInfo.ControlGroupCascadeEdit;
            checkEditAllowAddAll.Checked = MainControl.ActivityInfo.ControlGroupAllowAddAll;
            checkEditAllTTInActive.Checked = MainControl.ActivityInfo.ControlGroupAllTTInActive;


            DateTime currentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime monthActionStart = currentMonth;
            DateTime monthActionEnd = currentMonth;

            bool isApproved = false;//MainControl.ActivityInfo.IsApproved;
            bool isApprover = MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Approver);
            if (MainControl.IsEditMode)
            {

                ActivityDetailsDTO dto = new ActivityDetailsDTO();
                GetAllFields(dto);
                monthActionStart = new DateTime(dto.ActivityStart.Year, dto.ActivityStart.Month, 1);
                monthActionEnd = new DateTime(dto.ActivityEnd.Year, dto.ActivityEnd.Month, 1);

                //-- 1. Нельзя утвердить активность мес начала < текущего  [TR_DW_AU_Action_U]
                //-- 2. Нельзя разутвердить активность мес начала <= текущего  [TR_DW_AU_Action_U]
                checkEditIsApproved.Enabled = //!(!isApproved & monthActionStart < currentMonth
                    //  || isApproved & monthActionStart <= currentMonth)
                    //&& 
                                              isApprover;

            }
            else
            {
                checkEditIsApproved.Enabled = isApprover;
            }
            bool isAccesible = MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Initiator);
            //-- 3. Нельзя изменять дату начала утвержденных активностей  [TR_DW_AU_Action_U]
            datePeriodMonthFrom.Enabled = !isApproved && isAccesible;
            datePeriodDayFrom.Enabled = !isApproved && isAccesible;
            datePeriodYearFrom.Enabled = !isApproved && isAccesible;
            //BUG #20730. МП: Закладка "Обшие настройки": Якщо в активності стоїть галочка в полі "Активнсоть утверждена для использования" і активність збережена, то ми не маємо права нічого міняти в ній, окрім додавання і редагування ТТ.
            //BUG #22525. МП: потрібно давати можливість збільшити дату закінчення активності. 
            datePeriodMonthTo.Enabled = isAccesible && (/*monthActionEnd >= currentMonth || */!isApproved);
            datePeriodDayTo.Enabled = isAccesible && (/*monthActionEnd >= currentMonth || */!isApproved);
            datePeriodYearTo.Enabled = isAccesible && (/*monthActionEnd >= currentMonth || */!isApproved);

            //-- 5. Нельзя менять базовые периоды в утвержденной активности.  [TR_DW_AU_Action_U]
            datePeriod1DayFrom.Enabled = datePeriod1DayFrom.Enabled && !isApproved && isAccesible;
            datePeriod1DayTo.Enabled = datePeriod1DayTo.Enabled && !isApproved && isAccesible;
            datePeriod1MonthFrom.Enabled = datePeriod1MonthFrom.Enabled && !isApproved && isAccesible;
            datePeriod1MonthTo.Enabled = datePeriod1MonthTo.Enabled && !isApproved && isAccesible;
            datePeriod1YearFrom.Enabled = datePeriod1YearFrom.Enabled && !isApproved && isAccesible;
            datePeriod1YearTo.Enabled = datePeriod1YearTo.Enabled && !isApproved && isAccesible;
            datePeriod2DayFrom.Enabled = datePeriod2DayFrom.Enabled && !isApproved && isAccesible;
            datePeriod2DayTo.Enabled = datePeriod2DayTo.Enabled && !isApproved && isAccesible;
            datePeriod2MonthFrom.Enabled = datePeriod2MonthFrom.Enabled && !isApproved && isAccesible;
            datePeriod2MonthTo.Enabled = datePeriod2MonthTo.Enabled && !isApproved && isAccesible;
            datePeriod2YearFrom.Enabled = datePeriod2YearFrom.Enabled && !isApproved && isAccesible;
            datePeriod2YearTo.Enabled = datePeriod2YearTo.Enabled && !isApproved && isAccesible;
            //disable all active controls

            //BUG #23134. МП: разрешить менять описание активности и загружать файлы с описанием даже после ее утверждения.
            documentUploader.ReadOnly = !MainControl.ActivityInfo.IsDescriptionModifiable || !isAccesible;
            checkEditShowInKPK.Properties.ReadOnly = !MainControl.ActivityInfo.IsDescriptionModifiable || isApproved || !isAccesible;
            checkEditWithConnectionDate.Properties.ReadOnly = !MainControl.ActivityInfo.IsDescriptionModifiable || isApproved || !isAccesible;
            memoEditDescription.Properties.ReadOnly = !MainControl.ActivityInfo.IsDescriptionModifiable || !isAccesible;
            memoEditKPKDescriptionTemplate.Properties.ReadOnly = !MainControl.ActivityInfo.IsDescriptionModifiable || isApproved || !isAccesible;

            // BUG #20730. МП: Закладка "Обшие настройки": Якщо в активності стоїть галочка в полі "Активнсоть утверждена для использования" і активність збережена, то ми не маємо права нічого міняти в ній, окрім додавання і редагування ТТ.
            spinEditMonthsToTrackUpliftAfterEnd.Enabled = !isApproved && isAccesible;
            comboBoxActivityLevel.Properties.ReadOnly = isApproved || !isAccesible;
            textEditName.Properties.ReadOnly = isApproved || !isAccesible;


        }

        private void InitializeDateControl(DateCombo control, DataRow templateDetailsRow, string field, int defaultValue)
        {
            int value = ConvertEx.ToInt(templateDetailsRow[field]);
            bool isNull = templateDetailsRow[field] == DBNull.Value || value < 1;
            control.Enabled = isNull;
            control.Value = (isNull) ? defaultValue : value;
        }

        private void InitializeDateControlsSet(DateCombo ctrlDay, DateCombo ctrlMonth, DateCombo ctrlYear, DataRow dataRow, string field)
        {
            if (dataRow[field] != DBNull.Value)
            {
                DateTime actionStart = dataRow.Field<DateTime>(field);
                ctrlDay.Value = actionStart.Day;
                ctrlMonth.Value = actionStart.Month;

                ctrlYear.AddYear(actionStart.Year);
                ctrlYear.Value = actionStart.Year;
            }
        }

        private void InitializeYearControl(DateCombo control, DataRow dataRow, string field, int defaultYear)
        {
            control.Enabled = dataRow.IsNull(field);
            int value = (control.Enabled) ? defaultYear : (DateTime.Now.Year + ConvertEx.ToInt(dataRow[field]));
            control.AddYear(value);
            control.Value = value;
        }

        private void PopulateDateValues(int year)
        {
            datePeriod1YearFrom.LoadYears(year - 2, year);
            datePeriod1YearFrom.InnerControl.Properties.Columns[0].SortOrder = ColumnSortOrder.Descending;

            datePeriod1YearTo.LoadYears(year - 2, year);
            datePeriod1YearTo.InnerControl.Properties.Columns[0].SortOrder = ColumnSortOrder.Descending;

            datePeriod2YearFrom.LoadYears(year - 2, year);
            datePeriod2YearFrom.InnerControl.Properties.Columns[0].SortOrder = ColumnSortOrder.Descending;

            datePeriod2YearTo.LoadYears(year - 2, year);
            datePeriod2YearTo.InnerControl.Properties.Columns[0].SortOrder = ColumnSortOrder.Descending;

            datePeriodYearFrom.LoadYears(year, year + 2);
            datePeriodYearTo.LoadYears(year, year + 2);

            datePeriod1MonthFrom.LoadMonths();
            datePeriod1MonthTo.LoadMonths();
            datePeriod2MonthFrom.LoadMonths();
            datePeriod2MonthTo.LoadMonths();
            datePeriodMonthFrom.LoadMonths();
            datePeriodMonthTo.LoadMonths();

            datePeriod1DayFrom.LoadDays(datePeriod1YearFrom.Value, datePeriod1MonthFrom.Value);
            datePeriod1DayTo.LoadDays(datePeriod1YearTo.Value, datePeriod1MonthTo.Value);
            datePeriod2DayFrom.LoadDays(datePeriod2YearFrom.Value, datePeriod2MonthFrom.Value);
            datePeriod2DayTo.LoadDays(datePeriod2YearTo.Value, datePeriod2MonthTo.Value);
            datePeriodDayFrom.LoadDays(datePeriodYearFrom.Value, datePeriodMonthFrom.Value);
            datePeriodDayTo.LoadDays(datePeriodYearTo.Value, datePeriodMonthTo.Value);

            UpliftResultLabel.Text = 0.ToString("P");
        }

        private void PopulateDaysInMonth(DateCombo day, DateCombo month, DateCombo year)
        {
            int daysInMonth = DateTime.DaysInMonth(year.Value, month.Value);
            if (day.Value > daysInMonth)
            {
                day.Value = daysInMonth;
            }
            day.LoadDays(year.Value, month.Value);
        }

        private void SetKpkData(DataRow dataRow, String fldIsShowMobile, String fldIsCheckDateMobile, String fldMobileCommentTemplate)
        {
            checkEditShowInKPK.Checked = ConvertEx.ToBool(dataRow[fldIsShowMobile]);
            checkEditWithConnectionDate.Checked = ConvertEx.ToBool(dataRow[fldIsCheckDateMobile]);
            memoEditKPKDescriptionTemplate.Text = ConvertEx.ToString(dataRow[fldMobileCommentTemplate]);
        }

        private void SetMainData(DataRow dataRow, string fldUserName, string fldIsApproved, string fldIsBudget, string fldTypeName, string fldChanelType, string fldDescription,
                                 string fldActionName, DateTime creationDate)
        {
            checkEditIsApproved.EditValueChanging -= checkEditIsApproved_EditValueChanging;
            labelCreator.Text = string.Format(Resource.Common_Creator, ConvertEx.ToString(dataRow[fldUserName]), creationDate.ToString("dd.MM.yyyy"));
            checkEditIsApproved.Checked = String.IsNullOrEmpty(fldIsApproved) ? false : ConvertEx.ToBool(dataRow[fldIsApproved]);
            lblActivityNotInBudget.Visible = String.IsNullOrEmpty(fldIsBudget) ? true : !ConvertEx.ToBool(dataRow[fldIsBudget]);
            editControlActivityType.Text = ConvertEx.ToString(dataRow[fldTypeName]);
            textControlChannel.Text = ConvertEx.ToString(dataRow[fldChanelType]);
            memoEditDescription.Text = ConvertEx.ToString(dataRow[fldDescription]);
            textEditName.Text = String.IsNullOrEmpty(fldActionName) ? String.Empty : ConvertEx.ToString(dataRow[fldActionName]);
            checkEditIsApproved.EditValueChanging += checkEditIsApproved_EditValueChanging;
        }

        private void SetPositionsData(DataRow dataRow, String fldInitiator,
                                      String fldPlanner, String fldController, String fldApprover, String fldExecutor, String fldOlchecker)
        {
            comboBoxEditInitiator.Properties.Items.Add(dataRow[fldInitiator]);
            comboBoxEditInitiator.SelectedIndex = 0;
            comboBoxEditGoalCreator.Properties.Items.Add(dataRow[fldPlanner]);
            comboBoxEditGoalCreator.SelectedIndex = 0;
            comboBoxEditControlGroupSelector.Properties.Items.Add(dataRow[fldController]);
            comboBoxEditControlGroupSelector.SelectedIndex = 0;
            comboBoxEditActivityApprover.Properties.Items.Add(dataRow[fldApprover]);
            comboBoxEditActivityApprover.SelectedIndex = 0;
            comboBoxEditPerformerInRetail.Properties.Items.Add(dataRow[fldExecutor]);
            comboBoxEditPerformerInRetail.SelectedIndex = 0;
            comboBoxEditFactController.Properties.Items.Add(dataRow[fldOlchecker]);
            comboBoxEditFactController.SelectedIndex = 0;
        }

        #endregion

        #region Event Handling

        private void OnValidating(object sender, CancelEventArgs e)
        {
            BaseEdit baseEdit = sender as BaseEdit;
            if (baseEdit != null)
            {
                if (baseEdit.EditValue == null || string.IsNullOrEmpty(baseEdit.EditValue.ToString()))
                {
                    e.Cancel = true;
                    baseEdit.ErrorText = Resource.FieldEmptyError;
                }
            }
        }

        private void checkEditIsApproved_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (!ConvertEx.ToBool(e.OldValue) && ConvertEx.ToBool(e.NewValue) && MainControl.CheckData() == null)
                e.Cancel = true;
        }

        private void checkEditTakeBasePeriodsFromActiveWave_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditTakeBasePeriodsFromActiveWave.Checked)
            {
                datePeriod1DayFrom.Enabled =
                    datePeriod1DayTo.Enabled =
                    datePeriod1MonthFrom.Enabled =
                    datePeriod1MonthTo.Enabled =
                    datePeriod1YearFrom.Enabled =
                    datePeriod1YearTo.Enabled =
                    datePeriod2DayFrom.Enabled =
                    datePeriod2DayTo.Enabled =
                    datePeriod2MonthFrom.Enabled =
                    datePeriod2MonthTo.Enabled =
                    datePeriod2YearFrom.Enabled =
                    datePeriod2YearTo.Enabled = false;
            }
        }

        private void datePeriods_EditValueChanged(object sender, EventArgs e)
        {
            if ((sender.Equals(datePeriodYearFrom) || sender.Equals(datePeriodMonthFrom)) && datePeriodYearFrom.Value >= 1 && datePeriodMonthFrom.Value >= 1)
            {
                PopulateDaysInMonth(datePeriodDayFrom, datePeriodMonthFrom, datePeriodYearFrom);
            }
            else if ((sender.Equals(datePeriodYearTo) || sender.Equals(datePeriodMonthTo)) & datePeriodYearTo.Value >= 1 && datePeriodMonthTo.Value >= 1)
            {
                PopulateDaysInMonth(datePeriodDayTo, datePeriodMonthTo, datePeriodYearTo);
            }
            else if ((sender.Equals(datePeriod1YearFrom) || sender.Equals(datePeriod1MonthFrom)) && datePeriod1YearFrom.Value >= 1 && datePeriod1MonthFrom.Value >= 1)
            {
                PopulateDaysInMonth(datePeriod1DayFrom, datePeriod1MonthFrom, datePeriod1YearFrom);
            }
            else if ((sender.Equals(datePeriod1YearTo) || sender.Equals(datePeriod1MonthTo)) && datePeriod1YearTo.Value >= 1 && datePeriod1MonthTo.Value >= 1)
            {
                PopulateDaysInMonth(datePeriod1DayTo, datePeriod1MonthTo, datePeriod1YearTo);
            }
            else if ((sender.Equals(datePeriod2YearFrom) || sender.Equals(datePeriod2MonthFrom)) && datePeriod2YearFrom.Value >= 1 && datePeriod2MonthFrom.Value >= 1)
            {
                PopulateDaysInMonth(datePeriod2DayFrom, datePeriod2MonthFrom, datePeriod2YearFrom);
            }
            else if ((sender.Equals(datePeriod2YearTo) || sender.Equals(datePeriod2MonthTo)) && datePeriod2YearTo.Value >= 1 && datePeriod2MonthTo.Value >= 1)
            {
                PopulateDaysInMonth(datePeriod2DayTo, datePeriod2MonthTo, datePeriod2YearTo);
            }
        }

        private void documentUploader_GetFileData(object sender, FileUploader.FileDataInfo e)
        {
            e.FileData = DataProvider.GetDocument(e.Id);
        }

        private void UpliftCalculationBtn_Click(object sender, EventArgs e)
        {
            var upliftResult =
                MainControl.controlPromoGroup.AggrColumnSummaryV / MainControl.controlPromoGroup.AggrColumnAvgV -
                MainControl.controlControlGroup.AggrColumnSummaryV / MainControl.controlControlGroup.AggrColumnAvgV;

            if (Double.IsNaN(upliftResult)) return;

            UpliftResultLabel.Text = upliftResult.ToString("P");
        }

        #endregion

    }
}