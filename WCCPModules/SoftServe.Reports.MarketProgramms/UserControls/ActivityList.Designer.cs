﻿namespace SoftServe.Reports.MarketProgramms.UserControls
{
    partial class ActivityListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActivityListControl));
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnActionTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnActionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnActionTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCreator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChanelType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnActionLevelName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnActionStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnActionEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOLCountPromo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOLCountControl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOLCountConnected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUpliftPlanPrc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUpliftPlanDl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUpliftFactPrc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUpliftFactDl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barManager = new DevExpress.XtraBars.BarManager();
            this.standaloneTool = new DevExpress.XtraBars.Bar();
            this.barButtonNew = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonEdit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRegions = new DevExpress.XtraBars.BarButtonItem();
            this.popupRegions = new DevExpress.XtraBars.PopupControlContainer();
            this.regionsTree = new SoftServe.Reports.MarketProgramms.UserControls.RegionsTree();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupRegions)).BeginInit();
            this.popupRegions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 34);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp});
            this.gridControl.Size = new System.Drawing.Size(1244, 472);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.ColumnPanelRowHeight = 36;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnActionTypeName,
            this.columnActionName,
            this.columnActionTemplate,
            this.columnCreator,
            this.columnChanelType,
            this.columnActionLevelName,
            this.columnIsApproved,
            this.columnActionStart,
            this.columnActionEnd,
            this.columnOLCountPromo,
            this.columnOLCountControl,
            this.columnOLCountConnected,
            this.columnUpliftPlanPrc,
            this.columnUpliftPlanDl,
            this.columnUpliftFactPrc,
            this.columnUpliftFactDl});
            this.gridView.CustomizationFormBounds = new System.Drawing.Rectangle(1347, 437, 208, 282);
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.LightSalmon;
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsFilter.MaxCheckedListItemCount = 1000;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.columnActionTypeName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            this.gridView.DoubleClick += new System.EventHandler(this.gridView_DoubleClick);
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnId.Caption = "Код активности";
            this.columnId.FieldName = "Action_ID";
            this.columnId.MaxWidth = 80;
            this.columnId.MinWidth = 80;
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowShowHide = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 0;
            this.columnId.Width = 80;
            // 
            // columnActionTypeName
            // 
            this.columnActionTypeName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnActionTypeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnActionTypeName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnActionTypeName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnActionTypeName.Caption = "Тип активности";
            this.columnActionTypeName.FieldName = "ActionType_Name";
            this.columnActionTypeName.MinWidth = 100;
            this.columnActionTypeName.Name = "columnActionTypeName";
            this.columnActionTypeName.OptionsColumn.AllowEdit = false;
            this.columnActionTypeName.OptionsColumn.AllowShowHide = false;
            this.columnActionTypeName.OptionsColumn.ReadOnly = true;
            this.columnActionTypeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnActionTypeName.Visible = true;
            this.columnActionTypeName.VisibleIndex = 1;
            this.columnActionTypeName.Width = 100;
            // 
            // columnActionName
            // 
            this.columnActionName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnActionName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnActionName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnActionName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnActionName.Caption = "Имя активности";
            this.columnActionName.FieldName = "Action_Name";
            this.columnActionName.Name = "columnActionName";
            this.columnActionName.OptionsColumn.AllowEdit = false;
            this.columnActionName.OptionsColumn.AllowShowHide = false;
            this.columnActionName.OptionsColumn.ReadOnly = true;
            this.columnActionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnActionName.Visible = true;
            this.columnActionName.VisibleIndex = 2;
            // 
            // columnActionTemplate
            // 
            this.columnActionTemplate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnActionTemplate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnActionTemplate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnActionTemplate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnActionTemplate.Caption = "Шаблон";
            this.columnActionTemplate.FieldName = "Template";
            this.columnActionTemplate.Name = "columnActionTemplate";
            this.columnActionTemplate.OptionsColumn.AllowEdit = false;
            this.columnActionTemplate.OptionsColumn.AllowShowHide = false;
            this.columnActionTemplate.OptionsColumn.ReadOnly = true;
            this.columnActionTemplate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnActionTemplate.Visible = true;
            this.columnActionTemplate.VisibleIndex = 3;
            // 
            // columnCreator
            // 
            this.columnCreator.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCreator.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCreator.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCreator.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnCreator.Caption = "Создатель";
            this.columnCreator.FieldName = "UserName";
            this.columnCreator.Name = "columnCreator";
            this.columnCreator.OptionsColumn.AllowEdit = false;
            this.columnCreator.OptionsColumn.AllowShowHide = false;
            this.columnCreator.OptionsColumn.ReadOnly = true;
            this.columnCreator.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCreator.Visible = true;
            this.columnCreator.VisibleIndex = 4;
            // 
            // columnChanelType
            // 
            this.columnChanelType.AppearanceHeader.Options.UseTextOptions = true;
            this.columnChanelType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnChanelType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnChanelType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnChanelType.Caption = "Канал";
            this.columnChanelType.FieldName = "ChanelType";
            this.columnChanelType.Name = "columnChanelType";
            this.columnChanelType.OptionsColumn.AllowEdit = false;
            this.columnChanelType.OptionsColumn.AllowShowHide = false;
            this.columnChanelType.OptionsColumn.ReadOnly = true;
            this.columnChanelType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnChanelType.Visible = true;
            this.columnChanelType.VisibleIndex = 5;
            // 
            // columnActionLevelName
            // 
            this.columnActionLevelName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnActionLevelName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnActionLevelName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnActionLevelName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnActionLevelName.Caption = "Уровень";
            this.columnActionLevelName.FieldName = "ActionLevel_Name";
            this.columnActionLevelName.Name = "columnActionLevelName";
            this.columnActionLevelName.OptionsColumn.AllowEdit = false;
            this.columnActionLevelName.OptionsColumn.AllowShowHide = false;
            this.columnActionLevelName.OptionsColumn.ReadOnly = true;
            this.columnActionLevelName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnActionLevelName.Visible = true;
            this.columnActionLevelName.VisibleIndex = 6;
            // 
            // columnIsApproved
            // 
            this.columnIsApproved.AppearanceHeader.Options.UseTextOptions = true;
            this.columnIsApproved.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnIsApproved.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnIsApproved.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnIsApproved.Caption = "Утверждена";
            this.columnIsApproved.FieldName = "isApproved";
            this.columnIsApproved.Name = "columnIsApproved";
            this.columnIsApproved.OptionsColumn.AllowEdit = false;
            this.columnIsApproved.OptionsColumn.AllowShowHide = false;
            this.columnIsApproved.OptionsColumn.ReadOnly = true;
            this.columnIsApproved.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnIsApproved.Visible = true;
            this.columnIsApproved.VisibleIndex = 7;
            // 
            // columnActionStart
            // 
            this.columnActionStart.AppearanceHeader.Options.UseTextOptions = true;
            this.columnActionStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnActionStart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnActionStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnActionStart.Caption = "Начало";
            this.columnActionStart.FieldName = "Action_Start";
            this.columnActionStart.Name = "columnActionStart";
            this.columnActionStart.OptionsColumn.AllowEdit = false;
            this.columnActionStart.OptionsColumn.AllowShowHide = false;
            this.columnActionStart.OptionsColumn.ReadOnly = true;
            this.columnActionStart.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnActionStart.Visible = true;
            this.columnActionStart.VisibleIndex = 8;
            // 
            // columnActionEnd
            // 
            this.columnActionEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.columnActionEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnActionEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnActionEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnActionEnd.Caption = "Окончание";
            this.columnActionEnd.FieldName = "Action_End";
            this.columnActionEnd.Name = "columnActionEnd";
            this.columnActionEnd.OptionsColumn.AllowEdit = false;
            this.columnActionEnd.OptionsColumn.AllowShowHide = false;
            this.columnActionEnd.OptionsColumn.ReadOnly = true;
            this.columnActionEnd.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnActionEnd.Visible = true;
            this.columnActionEnd.VisibleIndex = 9;
            // 
            // columnOLCountPromo
            // 
            this.columnOLCountPromo.AppearanceHeader.Options.UseTextOptions = true;
            this.columnOLCountPromo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnOLCountPromo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnOLCountPromo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnOLCountPromo.Caption = "Кол-во промо ТТ";
            this.columnOLCountPromo.FieldName = "OLCountPromo";
            this.columnOLCountPromo.Name = "columnOLCountPromo";
            this.columnOLCountPromo.OptionsColumn.AllowEdit = false;
            this.columnOLCountPromo.OptionsColumn.AllowShowHide = false;
            this.columnOLCountPromo.OptionsColumn.ReadOnly = true;
            this.columnOLCountPromo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnOLCountPromo.Visible = true;
            this.columnOLCountPromo.VisibleIndex = 10;
            // 
            // columnOLCountControl
            // 
            this.columnOLCountControl.AppearanceHeader.Options.UseTextOptions = true;
            this.columnOLCountControl.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnOLCountControl.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnOLCountControl.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnOLCountControl.Caption = "Кол-во контрольн. ТТ";
            this.columnOLCountControl.FieldName = "OLCountControl";
            this.columnOLCountControl.Name = "columnOLCountControl";
            this.columnOLCountControl.OptionsColumn.AllowEdit = false;
            this.columnOLCountControl.OptionsColumn.AllowShowHide = false;
            this.columnOLCountControl.OptionsColumn.ReadOnly = true;
            this.columnOLCountControl.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnOLCountControl.Visible = true;
            this.columnOLCountControl.VisibleIndex = 11;
            // 
            // columnOLCountConnected
            // 
            this.columnOLCountConnected.AppearanceHeader.Options.UseTextOptions = true;
            this.columnOLCountConnected.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnOLCountConnected.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnOLCountConnected.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnOLCountConnected.Caption = "Кол-во подкл. ТТ";
            this.columnOLCountConnected.FieldName = "OLCountConnected";
            this.columnOLCountConnected.Name = "columnOLCountConnected";
            this.columnOLCountConnected.OptionsColumn.AllowEdit = false;
            this.columnOLCountConnected.OptionsColumn.AllowShowHide = false;
            this.columnOLCountConnected.OptionsColumn.ReadOnly = true;
            this.columnOLCountConnected.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnOLCountConnected.Visible = true;
            this.columnOLCountConnected.VisibleIndex = 12;
            // 
            // columnUpliftPlanPrc
            // 
            this.columnUpliftPlanPrc.AppearanceHeader.Options.UseTextOptions = true;
            this.columnUpliftPlanPrc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnUpliftPlanPrc.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnUpliftPlanPrc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnUpliftPlanPrc.Caption = "Uplift план %";
            this.columnUpliftPlanPrc.DisplayFormat.FormatString = "P";
            this.columnUpliftPlanPrc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnUpliftPlanPrc.FieldName = "UpliftPlanPrc";
            this.columnUpliftPlanPrc.Name = "columnUpliftPlanPrc";
            this.columnUpliftPlanPrc.OptionsColumn.AllowEdit = false;
            this.columnUpliftPlanPrc.OptionsColumn.AllowShowHide = false;
            this.columnUpliftPlanPrc.OptionsColumn.ReadOnly = true;
            this.columnUpliftPlanPrc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnUpliftPlanPrc.Visible = true;
            this.columnUpliftPlanPrc.VisibleIndex = 13;
            // 
            // columnUpliftPlanDl
            // 
            this.columnUpliftPlanDl.AppearanceHeader.Options.UseTextOptions = true;
            this.columnUpliftPlanDl.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnUpliftPlanDl.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnUpliftPlanDl.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnUpliftPlanDl.Caption = "Uplift план дал";
            this.columnUpliftPlanDl.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnUpliftPlanDl.FieldName = "UpliftPlanDl";
            this.columnUpliftPlanDl.Name = "columnUpliftPlanDl";
            this.columnUpliftPlanDl.OptionsColumn.AllowEdit = false;
            this.columnUpliftPlanDl.OptionsColumn.AllowShowHide = false;
            this.columnUpliftPlanDl.OptionsColumn.ReadOnly = true;
            this.columnUpliftPlanDl.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnUpliftPlanDl.Visible = true;
            this.columnUpliftPlanDl.VisibleIndex = 14;
            // 
            // columnUpliftFactPrc
            // 
            this.columnUpliftFactPrc.AppearanceHeader.Options.UseTextOptions = true;
            this.columnUpliftFactPrc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnUpliftFactPrc.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnUpliftFactPrc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnUpliftFactPrc.Caption = "Uplift факт %";
            this.columnUpliftFactPrc.DisplayFormat.FormatString = "P";
            this.columnUpliftFactPrc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnUpliftFactPrc.FieldName = "UpliftFactPrc";
            this.columnUpliftFactPrc.Name = "columnUpliftFactPrc";
            this.columnUpliftFactPrc.OptionsColumn.AllowEdit = false;
            this.columnUpliftFactPrc.OptionsColumn.AllowShowHide = false;
            this.columnUpliftFactPrc.OptionsColumn.ReadOnly = true;
            this.columnUpliftFactPrc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnUpliftFactPrc.Visible = true;
            this.columnUpliftFactPrc.VisibleIndex = 15;
            // 
            // columnUpliftFactDl
            // 
            this.columnUpliftFactDl.AppearanceHeader.Options.UseTextOptions = true;
            this.columnUpliftFactDl.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnUpliftFactDl.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnUpliftFactDl.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnUpliftFactDl.Caption = "Uplift факт дал";
            this.columnUpliftFactDl.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnUpliftFactDl.FieldName = "UpliftFactDl";
            this.columnUpliftFactDl.Name = "columnUpliftFactDl";
            this.columnUpliftFactDl.OptionsColumn.AllowEdit = false;
            this.columnUpliftFactDl.OptionsColumn.AllowShowHide = false;
            this.columnUpliftFactDl.OptionsColumn.ReadOnly = true;
            this.columnUpliftFactDl.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnUpliftFactDl.Visible = true;
            this.columnUpliftFactDl.VisibleIndex = 16;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "Status")});
            this.repositoryStatusLookUp.DisplayMember = "StatusName";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.standaloneTool});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonNew,
            this.barButtonEdit,
            this.barButtonRegions});
            this.barManager.MaxItemId = 24;
            // 
            // standaloneTool
            // 
            this.standaloneTool.BarName = "Tools";
            this.standaloneTool.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.standaloneTool.DockCol = 0;
            this.standaloneTool.DockRow = 0;
            this.standaloneTool.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.standaloneTool.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonRegions, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.standaloneTool.OptionsBar.AllowQuickCustomization = false;
            this.standaloneTool.OptionsBar.DisableCustomization = true;
            this.standaloneTool.OptionsBar.DrawDragBorder = false;
            this.standaloneTool.OptionsBar.UseWholeRow = true;
            this.standaloneTool.Text = "Tools";
            // 
            // barButtonNew
            // 
            this.barButtonNew.Caption = "Новая";
            this.barButtonNew.Id = 5;
            this.barButtonNew.ImageIndex = 0;
            this.barButtonNew.Name = "barButtonNew";
            this.barButtonNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonNew_ItemClick);
            // 
            // barButtonEdit
            // 
            this.barButtonEdit.Caption = "Редактировать";
            this.barButtonEdit.Id = 10;
            this.barButtonEdit.ImageIndex = 1;
            this.barButtonEdit.Name = "barButtonEdit";
            this.barButtonEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonEdit_ItemClick);
            // 
            // barButtonRegions
            // 
            this.barButtonRegions.ActAsDropDown = true;
            this.barButtonRegions.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonRegions.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonRegions.Caption = "Показать фильтр активностей >>";
            this.barButtonRegions.DropDownControl = this.popupRegions;
            this.barButtonRegions.Id = 23;
            this.barButtonRegions.ImageIndex = 4;
            this.barButtonRegions.Name = "barButtonRegions";
            // 
            // popupRegions
            // 
            this.popupRegions.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupRegions.Controls.Add(this.regionsTree);
            this.popupRegions.Location = new System.Drawing.Point(303, 143);
            this.popupRegions.Manager = this.barManager;
            this.popupRegions.Name = "popupRegions";
            this.popupRegions.ShowSizeGrip = true;
            this.popupRegions.Size = new System.Drawing.Size(266, 347);
            this.popupRegions.TabIndex = 4;
            this.popupRegions.Visible = false;
            this.popupRegions.VisibleChanged += new System.EventHandler(this.popupRegions_VisibleChanged);
            // 
            // regionsTree
            // 
            this.regionsTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.regionsTree.Location = new System.Drawing.Point(0, 0);
            this.regionsTree.Margin = new System.Windows.Forms.Padding(4);
            this.regionsTree.Name = "regionsTree";
            this.regionsTree.Size = new System.Drawing.Size(266, 347);
            this.regionsTree.TabIndex = 0;
            this.regionsTree.CloseBtnClick += new System.EventHandler(this.regionsTree_CloseBtnClick);
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "add.png");
            this.imCollection.Images.SetKeyName(1, "edit.png");
            this.imCollection.Images.SetKeyName(2, "refresh_24.png");
            this.imCollection.Images.SetKeyName(3, "close_24.png");
            this.imCollection.Images.SetKeyName(4, "filter.png");
            // 
            // ActivityListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.popupRegions);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ActivityListControl";
            this.Size = new System.Drawing.Size(1244, 506);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupRegions)).EndInit();
            this.popupRegions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnActionTypeName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar standaloneTool;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonNew;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarButtonItem barButtonEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnActionName;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreator;
        private DevExpress.XtraGrid.Columns.GridColumn columnChanelType;
        private DevExpress.XtraGrid.Columns.GridColumn columnActionLevelName;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsApproved;
        private DevExpress.XtraGrid.Columns.GridColumn columnActionStart;
        private DevExpress.XtraGrid.Columns.GridColumn columnActionEnd;
        private DevExpress.XtraGrid.Columns.GridColumn columnOLCountPromo;
        private DevExpress.XtraGrid.Columns.GridColumn columnOLCountControl;
        private DevExpress.XtraGrid.Columns.GridColumn columnOLCountConnected;
        private DevExpress.XtraGrid.Columns.GridColumn columnUpliftPlanPrc;
        private DevExpress.XtraGrid.Columns.GridColumn columnUpliftPlanDl;
        private DevExpress.XtraGrid.Columns.GridColumn columnUpliftFactPrc;
        private DevExpress.XtraGrid.Columns.GridColumn columnUpliftFactDl;
        private DevExpress.XtraGrid.Columns.GridColumn columnActionTemplate;
        private DevExpress.XtraBars.BarButtonItem barButtonRegions;
        private DevExpress.XtraBars.PopupControlContainer popupRegions;
        private RegionsTree regionsTree;


    }
}
