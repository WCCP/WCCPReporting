﻿namespace SoftServe.Reports.MarketProgramms.UserControls
{
    partial class ActivityDetailsGoals
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WorkingArea = new DevExpress.XtraEditors.PanelControl();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateStart = new DevExpress.XtraEditors.DateEdit();
            this.dateEnd = new DevExpress.XtraEditors.DateEdit();
            this.checkShowFact = new DevExpress.XtraEditors.CheckEdit();
            this.simpleLoad = new DevExpress.XtraEditors.SimpleButton();
            this.gridAims = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOlId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIsApproved = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colIsParticipator = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIsRecomended = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIsModified = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.textEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.WorkingArea)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowFact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // WorkingArea
            // 
            this.WorkingArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkingArea.Location = new System.Drawing.Point(0, 116);
            this.WorkingArea.Margin = new System.Windows.Forms.Padding(4);
            this.WorkingArea.Name = "WorkingArea";
            this.WorkingArea.Size = new System.Drawing.Size(1589, 81);
            this.WorkingArea.TabIndex = 5;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.AutoScrollMinSize = new System.Drawing.Size(750, 400);
            this.xtraScrollableControl1.Controls.Add(this.splitContainer1);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1000, 400);
            this.xtraScrollableControl1.TabIndex = 10;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dateStart);
            this.splitContainer1.Panel1.Controls.Add(this.dateEnd);
            this.splitContainer1.Panel1.Controls.Add(this.checkShowFact);
            this.splitContainer1.Panel1.Controls.Add(this.simpleLoad);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridAims);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(1000, 400);
            this.splitContainer1.SplitterDistance = 42;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(320, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "ПО";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Дата: С";
            // 
            // dateStart
            // 
            this.dateStart.EditValue = new System.DateTime(2012, 6, 1, 0, 0, 0, 0);
            this.dateStart.Location = new System.Drawing.Point(196, 11);
            this.dateStart.Margin = new System.Windows.Forms.Padding(2);
            this.dateStart.Name = "dateStart";
            this.dateStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateStart.Size = new System.Drawing.Size(104, 20);
            this.dateStart.TabIndex = 10;
            this.dateStart.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // dateEnd
            // 
            this.dateEnd.EditValue = new System.DateTime(2012, 8, 27, 14, 0, 58, 963);
            this.dateEnd.Location = new System.Drawing.Point(347, 10);
            this.dateEnd.Margin = new System.Windows.Forms.Padding(2);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEnd.Size = new System.Drawing.Size(104, 20);
            this.dateEnd.TabIndex = 11;
            this.dateEnd.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // checkShowFact
            // 
            this.checkShowFact.Location = new System.Drawing.Point(495, 12);
            this.checkShowFact.Margin = new System.Windows.Forms.Padding(2);
            this.checkShowFact.Name = "checkShowFact";
            this.checkShowFact.Properties.Caption = "Показать факт";
            this.checkShowFact.Size = new System.Drawing.Size(122, 19);
            this.checkShowFact.TabIndex = 12;
            this.checkShowFact.CheckedChanged += new System.EventHandler(this.checkShowFact_CheckedChanged);
            // 
            // simpleLoad
            // 
            this.simpleLoad.Location = new System.Drawing.Point(10, 10);
            this.simpleLoad.Margin = new System.Windows.Forms.Padding(2);
            this.simpleLoad.Name = "simpleLoad";
            this.simpleLoad.Size = new System.Drawing.Size(94, 19);
            this.simpleLoad.TabIndex = 13;
            this.simpleLoad.Text = "Загрузить цели";
            this.simpleLoad.Click += new System.EventHandler(this.simpleLoad_Click);
            // 
            // gridAims
            // 
            this.gridAims.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridAims.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAims.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridAims.Location = new System.Drawing.Point(0, 0);
            this.gridAims.MainView = this.bandedGridView1;
            this.gridAims.Margin = new System.Windows.Forms.Padding(2);
            this.gridAims.Name = "gridAims";
            this.gridAims.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.textEdit,
            this.repositoryItemCheckEdit1});
            this.gridAims.Size = new System.Drawing.Size(1000, 357);
            this.gridAims.TabIndex = 2;
            this.gridAims.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.BandPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Appearance.BandPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridView1.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.HotTrack;
            this.bandedGridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridView1.ColumnPanelRowHeight = 80;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colOlId,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn5,
            this.gridColumn4,
            this.colIsRecomended,
            this.colIsApproved,
            this.colIsParticipator,
            this.colIsModified});
            this.bandedGridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1628, 537, 218, 205);
            this.bandedGridView1.GridControl = this.gridAims;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsSelection.MultiSelect = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.bandedGridView1_CustomDrawColumnHeader);
            this.bandedGridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.bandedGridView1_CustomDrawCell);
            this.bandedGridView1.ShownEditor += new System.EventHandler(this.bandedGridView1_ShownEditor);
            this.bandedGridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.bandedGridView1_CellValueChanged);
            this.bandedGridView1.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.bandedGridView1_CellValueChanging);
            this.bandedGridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.bandedGridView1_ValidatingEditor);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand1.Columns.Add(this.colOlId);
            this.gridBand1.Columns.Add(this.colIsApproved);
            this.gridBand1.Columns.Add(this.colIsParticipator);
            this.gridBand1.Columns.Add(this.gridColumn4);
            this.gridBand1.Columns.Add(this.gridColumn5);
            this.gridBand1.Columns.Add(this.gridColumn3);
            this.gridBand1.Columns.Add(this.gridColumn2);
            this.gridBand1.Columns.Add(this.gridColumn1);
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 550;
            // 
            // colOlId
            // 
            this.colOlId.Caption = "КодТТ";
            this.colOlId.FieldName = "КодТТ";
            this.colOlId.Name = "colOlId";
            this.colOlId.OptionsColumn.AllowEdit = false;
            this.colOlId.Visible = true;
            this.colOlId.Width = 90;
            // 
            // colIsApproved
            // 
            this.colIsApproved.Caption = "Утверждена";
            this.colIsApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsApproved.FieldName = "Утверждена";
            this.colIsApproved.Name = "colIsApproved";
            this.colIsApproved.OptionsColumn.AllowEdit = false;
            this.colIsApproved.Visible = true;
            this.colIsApproved.Width = 30;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.DisplayValueChecked = "1";
            this.repositoryItemCheckEdit1.DisplayValueUnchecked = "0";
            this.repositoryItemCheckEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colIsParticipator
            // 
            this.colIsParticipator.Caption = "Участвует";
            this.colIsParticipator.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsParticipator.FieldName = "Участвует";
            this.colIsParticipator.Name = "colIsParticipator";
            this.colIsParticipator.OptionsColumn.AllowEdit = false;
            this.colIsParticipator.Visible = true;
            this.colIsParticipator.Width = 30;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Регион";
            this.gridColumn4.FieldName = "Регион";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.Width = 80;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "М4";
            this.gridColumn5.FieldName = "M4";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "М3";
            this.gridColumn3.FieldName = "M3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 80;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "М2";
            this.gridColumn2.FieldName = "M2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.Width = 80;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "М1";
            this.gridColumn1.FieldName = "M1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.Width = 80;
            // 
            // colIsRecomended
            // 
            this.colIsRecomended.Caption = "Рекомендованная";
            this.colIsRecomended.FieldName = "Рекомендованная";
            this.colIsRecomended.Name = "colIsRecomended";
            this.colIsRecomended.OptionsColumn.AllowEdit = false;
            this.colIsRecomended.Visible = true;
            this.colIsRecomended.Width = 30;
            // 
            // colIsModified
            // 
            this.colIsModified.Caption = "Редактированная";
            this.colIsModified.FieldName = "IsModified";
            this.colIsModified.Name = "colIsModified";
            // 
            // textEdit
            // 
            this.textEdit.AutoHeight = false;
            this.textEdit.Name = "textEdit";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "M4";
            this.gridColumn5.FieldName = "M4";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.Width = 80;
            // 
            // ActivityDetailsGoals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "ActivityDetailsGoals";
            this.Size = new System.Drawing.Size(1000, 400);
            ((System.ComponentModel.ISupportInitialize)(this.WorkingArea)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowFact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl WorkingArea;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.CheckEdit checkShowFact;
        private DevExpress.XtraEditors.SimpleButton simpleLoad;
        private DevExpress.XtraEditors.DateEdit dateEnd;
        private DevExpress.XtraEditors.DateEdit dateStart;
        private DevExpress.XtraGrid.GridControl gridAims;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOlId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIsRecomended;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIsApproved;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIsParticipator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIsModified;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textEdit;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn5;


    }
}
