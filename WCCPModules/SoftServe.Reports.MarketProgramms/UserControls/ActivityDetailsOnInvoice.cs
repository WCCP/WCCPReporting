﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.MarketProgramms.Forms;
using SoftServe.Reports.MarketProgramms.Utility;

namespace SoftServe.Reports.MarketProgramms.UserControls {
    public partial class ActivityDetailsOnInvoice : CommonBaseControl {
        #region Fields

        /// <summary>
        /// Determines whether current user can modify the tab
        /// </summary>
        private bool _canModify = true;

        private int _current_index = -1;

        private List<OnInvoiceModel> _onInvoiceList;
        private readonly Dictionary<long, List<OnInvoiceModel>> _outletPeriods = new Dictionary<long, List<OnInvoiceModel>>();
        List<PromoOutletModel> _promoOutlets = new List<PromoOutletModel>(); 
        
        #endregion

        public ActivityDetailsOnInvoice() {
            InitializeComponent();
            HasUnsavedChanges = false;
        }

        #region Properties

        /// <summary>
        /// Main control
        /// </summary>
        public ActivityDetailsControl MainControl { get; set; }

        public bool HasUnsavedChanges { get; set; }

        public bool HasOnInvoiceRecords {
            get { return DataProvider.GetOnInvoiceAim(ActionId).Count > 0 || _onInvoiceList != null && _onInvoiceList.Count > 0; }
        }

        /// <summary>
        /// Activity's Id
        /// </summary>
        private int ActionId { get; set; }

        /// <summary>
        /// Action start date
        /// </summary>
        private DateTime ActionStart { get; set; }

        /// <summary>
        /// Action end date
        /// </summary>
        private DateTime ActionEnd { get; set; }

        /// <summary>
        /// Aim Id
        /// </summary>
        private int AimId { get; set; }

        private int CurrentIndex { get; set; }

        private string ActionName { get; set; }

        #endregion

        public void LoadData(int activityId, int aimId, string actionName) {
            ActionId = activityId;
            AimId = aimId;
            ActionStart = MainControl.OriginalActivityDetails.ActivityStart;
            ActionEnd = MainControl.OriginalActivityDetails.ActivityEnd;
            ActionName = actionName;

            _onInvoiceList = DataProvider.GetOnInvoiceAim(ActionId);
            _promoOutlets = DataProvider.GetPromoOutlets(ActionId);
            PrepareOLPeriods();

            onInvoiceGrid.BeginUpdate();
            try {
                onInvoiceGridView.OptionsBehavior.Editable = true;
                onInvoiceGrid.DataSource = _onInvoiceList;
                riDateEdit.MinValue = ActionStart;
                riDateEdit.MaxValue = ActionEnd;
            }
            finally {
                onInvoiceGrid.EndUpdate();
            }
            
            _canModify = (MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.GoalsSetter) || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Initiator))
                        && MainControl.AccessChecker.PromoTTCount == MainControl.AccessChecker.PromoTTAvailable;

            onInvoiceGridView.OptionsBehavior.Editable = _canModify;
            //TODO: enable/disable buttons
            btnAdd.Enabled = _canModify;
            btnDelete.Enabled = _canModify;
            //simpleLoad.Enabled = false;

            HasUnsavedChanges = false;
            WasControlLoaded = true;
        }

        public void Save() {
            try {
                onInvoiceGridView.PostEditor();
                foreach (OnInvoiceModel lOnInvoiceItem in _onInvoiceList)
                {
                    if (!lOnInvoiceItem.IsDirty)
                        continue;

                    int lPeriodIdOld = lOnInvoiceItem.PeriodId;

                    lOnInvoiceItem.PeriodId = DataProvider.SetOnInvoiceItem(lOnInvoiceItem);
                    lOnInvoiceItem.AcceptChanges();

                    if (lPeriodIdOld < 0)
                    {
                        List<OnInvoiceModel> lList = _outletPeriods[lOnInvoiceItem.OutletId];
                        OnInvoiceModel lModel = lList.FirstOrDefault(m => m.PeriodId == lPeriodIdOld);
                        if (lModel != null)
                        {
                            lModel.PeriodId = lOnInvoiceItem.PeriodId;
                        }
                    }
                }

                HasUnsavedChanges = false;
            }
            catch (Exception lException) {
                XtraMessageBox.Show("Произошла ошибка при сохранении записей.\n" + lException.Message,
                    "On-Invoice",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PrepareOLPeriods() {
            _outletPeriods.Clear();
            foreach (OnInvoiceModel lItem in _onInvoiceList) {
                _outletPeriods.AddItemToDictionary(lItem);
            }
            List<OnInvoiceModel> lOnInvoiceOLs = DataProvider.GetOnInvoiceOutkets(ActionId);
            foreach (OnInvoiceModel lItem in lOnInvoiceOLs)
                _outletPeriods.AddItemToDictionary(lItem);
        }

        private void CreateOnInvoiceItem(out OnInvoiceModel newItem) {
            newItem = OnInvoiceModel.CreateInstance();
            newItem.PeriodId = _current_index--;
            newItem.ActionId = ActionId;
            newItem.AimId = AimId;
            newItem.OutletId = 0;
            newItem.DateFrom = ActionStart;
            newItem.DateTo = ActionEnd;
            newItem.DiscountSum = 0;
            newItem.DiscountPercent = 0;
            newItem.ActionName = ActionName;
            _onInvoiceList.Add(newItem);
        }
       
        private void btnAdd_Click(object sender, EventArgs e) {
            // handle changes to current row
            CurrentIndex = onInvoiceGridView.GetDataSourceRowIndex(onInvoiceGridView.FocusedRowHandle);
            if (!ValidateItem(onInvoiceGridView.FocusedRowHandle))
                return;
            //TODO: add lookup form to choose from Promo TT
            ShowPromoOutletsLookup();
            //onInvoiceGridView.RefreshData();
            //--onInvoiceGridView.MoveLastVisible();
            CurrentIndex = onInvoiceGridView.GetDataSourceRowIndex(onInvoiceGridView.FocusedRowHandle);
            HasUnsavedChanges = true;
        }

        private void ShowPromoOutletsLookup() {
            AddPromoOutlets lPromoLookupForm = new AddPromoOutlets();
            try {
                lPromoLookupForm.DataSource = _promoOutlets;
                DialogResult lDialogResult = lPromoLookupForm.ShowDialog();
                if (lDialogResult == DialogResult.Cancel)
                    return;
                List<PromoOutletModel> lSelectedList = lPromoLookupForm.SelectedOutlets;
                foreach (PromoOutletModel lItem in lSelectedList) {
                    OnInvoiceModel lOnInvoiceModel;
                    CreateOnInvoiceItem(out lOnInvoiceModel);
                    lOnInvoiceModel.OutletId = lItem.OutletId;
                    _outletPeriods.AddItemToDictionary(lOnInvoiceModel);
                }
            }
            finally {
                onInvoiceGridView.RefreshData();
            }
        }
        
        private bool ValidateItem(int rowHandle) {
            onInvoiceGridView.PostEditor();
            onInvoiceGridView.UpdateCurrentRow();
            if (rowHandle < 0 || !onInvoiceGridView.IsValidRowHandle(rowHandle))
                return true;
            if (CurrentIndex < 0 && CurrentIndex >= _onInvoiceList.Count)
                return true;
            bool lResult = ValidatePeriod(rowHandle);
//            if (lResult)
//                lResult = ValidateDiscountSum(rowHandle);
//            if (lResult)
//                lResult = ValidateDiscountPercent();
            return lResult;
        }

        private bool ValidatePeriod(int rowHandle) {
            long lOutletId = Convert.ToInt64(onInvoiceGridView.GetRowCellValue(rowHandle, colOutletId));
            List<OnInvoiceModel> lOnInvoiceList = _outletPeriods[lOutletId];
            OnInvoiceModel lCurItem = _onInvoiceList[CurrentIndex];
            if (lOnInvoiceList.Count == 1 && lOnInvoiceList.Contains(lCurItem))
                return true;
            foreach (OnInvoiceModel lModel in lOnInvoiceList) {
                if (lModel.Equals(lCurItem))
                    continue;
                if ((lCurItem.DateFrom >= lModel.DateFrom && lCurItem.DateFrom <= lModel.DateTo) ||
                    (lCurItem.DateTo >= lModel.DateFrom && lCurItem.DateTo <= lModel.DateTo))
                    return false;
            }
            return true;
        }

        // for validation on save - takes dublicates into consideration
        private List<OnInvoiceModel> FindConflictingPeriods(long outletId)
        {
            List<OnInvoiceModel> lOnInvoiceList = _outletPeriods[outletId];
            OnInvoiceModel lCurItem = _onInvoiceList[CurrentIndex];
            
            if (lOnInvoiceList.Count == 1 && lOnInvoiceList.Contains(lCurItem))
                return new List<OnInvoiceModel>();

            List<OnInvoiceModel> lConflictingModels = new List<OnInvoiceModel>();
            bool lMoreThanOneEqual = false;// ignore the first equal to current element but add all the next occurences
            
            foreach (OnInvoiceModel lModel in lOnInvoiceList)
            {
                if (lModel.Equals(lCurItem) && !lMoreThanOneEqual)
                {
                    lMoreThanOneEqual = true;
                    continue;
                }

                if ((lCurItem.DateFrom >= lModel.DateFrom && lCurItem.DateFrom <= lModel.DateTo) ||
                    (lCurItem.DateTo >= lModel.DateFrom && lCurItem.DateTo <= lModel.DateTo))
                    lConflictingModels.Add(lModel);
            }

            if(lConflictingModels.Count > 0)
                lConflictingModels.Add(lCurItem);

            return lConflictingModels;
        }

        // for validation on import - ignores dublicates - overrides values
        private List<OnInvoiceModel> FindConflictingPeriods(long outletId, OnInvoiceModel curItem)
        {
            if(!_outletPeriods.ContainsKey(outletId))
                return new List<OnInvoiceModel>();

            List<OnInvoiceModel> lOnInvoiceList = _outletPeriods[outletId];
            
            if (lOnInvoiceList.Count == 1 && lOnInvoiceList.Contains(curItem))
                return new List<OnInvoiceModel>();

            List<OnInvoiceModel> lConflictingModels = new List<OnInvoiceModel>();

            foreach (OnInvoiceModel lModel in lOnInvoiceList)
            {
                if (lModel.Equals(curItem))
                    continue;
                
                if ((curItem.DateFrom >= lModel.DateFrom && curItem.DateFrom <= lModel.DateTo) ||
                    (curItem.DateTo >= lModel.DateFrom && curItem.DateTo <= lModel.DateTo))
                    lConflictingModels.Add(lModel);
            }

            return lConflictingModels;
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            DialogResult lDialogResult = XtraMessageBox.Show("Вы действительно хотите удалить выделенные записи?", "On-Invoice",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (lDialogResult == DialogResult.No)
                return;
            try {
                //CurrentIndex = onInvoiceGridView.GetDataSourceRowIndex(onInvoiceGridView.FocusedRowHandle);
                DeleteOnInvoiceItems(onInvoiceGridView.GetSelectedRows());

                onInvoiceGridView.RefreshData();
                //int lPrevRowHandle = onInvoiceGridView.FocusedRowHandle;
                //onInvoiceGridView.DeleteSelectedRows();
                //if (lPrevRowHandle == onInvoiceGridView.FocusedRowHandle)
                //    onInvoiceGridView_FocusedRowChanged(onInvoiceGridView, new FocusedRowChangedEventArgs(GridControl.InvalidRowHandle, onInvoiceGridView.FocusedRowHandle));

                HasUnsavedChanges = true;
            }
            catch (Exception lException) {
                XtraMessageBox.Show("Произошла ошибка при удалении записей.\n" + lException.Message,
                    "On-Invoice",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DeleteOnInvoiceItems(int[] selectedRows) {
            for (int lIndex = selectedRows.Length - 1; lIndex >= 0; lIndex--)
            {
                CurrentIndex = onInvoiceGridView.GetDataSourceRowIndex(selectedRows[lIndex]);
                OnInvoiceModel lItem = _onInvoiceList[CurrentIndex];
                DataProvider.DeleteOnInvoiceItem(lItem.PeriodId);
                _onInvoiceList.RemoveAt(CurrentIndex);
                _outletPeriods.DeleteItemFromDict(lItem);
             }
        }

        private void btnSave_Click(object sender, EventArgs e) {

            if(PeriodsHasConflicts())
                return;

            Save();
            XtraMessageBox.Show("Данные по Скидкам успешно сохранены.",
                "On-Invoice",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnCopy_Click(object sender, EventArgs e) {
            onInvoiceGridView.BeginUpdate();
            try {
                CopyOnInvoiceItems(onInvoiceGridView.GetSelectedRows());
            }
            finally {
                onInvoiceGridView.EndUpdate();
                onInvoiceGridView.RefreshData();
                onInvoiceGridView.PostEditor();
                onInvoiceGridView.ClearSelection();
            }
        }

        private void CopyOnInvoiceItems(IList<int> selectedRows) {
            for (int lIndex = 0; lIndex < selectedRows.Count; lIndex++) {
                CurrentIndex = onInvoiceGridView.GetDataSourceRowIndex(selectedRows[lIndex]);
                OnInvoiceModel lItem = _onInvoiceList[CurrentIndex];
                OnInvoiceModel lNewItem;
                CreateOnInvoiceItem(out lNewItem);
                lNewItem.OutletId = lItem.OutletId;
                lNewItem.DateFrom = lItem.DateTo.AddDays(1);
                lNewItem.DateTo = lNewItem.DateFrom;
                _outletPeriods.AddItemToDictionary(lNewItem);
             }
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (!PeriodsHasConflicts())
            {
                XtraMessageBox.Show("Конфликты в диапазонах дат не обнаружены", "Проверить диапазоны дат", MessageBoxButtons.OK);
            }
        }

        private bool PeriodsHasConflicts()
        {
            PrepareOLPeriods();
            ConflictingPeriodsList lConflictingPeriodsList = new ConflictingPeriodsList();

            for (int lI = 0; lI < _onInvoiceList.Count; lI++)
            {
                CurrentIndex = lI;
                int lRowHandle = onInvoiceGridView.GetRowHandle(lI);

                long lOutletId = Convert.ToInt64(onInvoiceGridView.GetRowCellValue(lRowHandle, colOutletId));

                if (lConflictingPeriodsList.Records.FirstOrDefault(r => r.OutletId == lOutletId) != null)// our OL is already validated, no need to redo work
                    continue;

                List<OnInvoiceModel> lConflicts = FindConflictingPeriods(lOutletId);

                if (lConflicts.Count == 0)
                    continue;

                lConflictingPeriodsList.Add("Конфликт диапазона дат для ТТ", lConflicts);
            }

            if (lConflictingPeriodsList.Records.Count > 0)
            {
                IncorrectPeriodsForm lErrorForm = new IncorrectPeriodsForm(lConflictingPeriodsList);
                lErrorForm.ShowDialog(this);
            }

            return lConflictingPeriodsList.Records.Count > 0;
        }

        private void btnExcelLoad_Click(object sender, EventArgs e) {
            PeriodsUploader lUploader = new PeriodsUploader();
            lUploader.UploadPlans(PeriodIsWithinActionDuration, OutletBelongsToPromoGroup, OutletHasNotOverlapingPeriods, ActionId, AimId);

            WaitManager.StartWait();
            LoadData(ActionId, AimId, ActionName);
            WaitManager.StopWait();
        }

        private void btnUpload2SW_Click(object sender, EventArgs e) {
            try
            {
                DataProvider.ExportOnInvoiceToSW(ActionId);
                XtraMessageBox.Show(string.Format("Выгрузка в SW успешно завершена"), "Выгрузка в SW",
               MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(string.Format("Ошибка при выгрузке в SW:/n/r {0}", ex.Message), "Выгрузка в SW",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         }

        public bool OutletBelongsToPromoGroup(OnInvoiceModel model)
        {
            return _promoOutlets.FirstOrDefault(p => p.OutletId == model.OutletId) != null;
        }

        public bool PeriodIsWithinActionDuration(OnInvoiceModel model)
        {
            return model.DateFrom >= ActionStart && model.DateTo <= ActionEnd;
        }

        public List<OnInvoiceModel> OutletHasNotOverlapingPeriods(OnInvoiceModel model)
        {
            return FindConflictingPeriods(model.OutletId, model);
        }
     }
 }
