﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.CommonControls.MappingControl;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.MarketProgramms.Tabs;

namespace SoftServe.Reports.MarketProgramms.UserControls {
    /// <summary>
    /// Tab with Goals configuration for activity
    /// </summary>
    [ToolboxItem(false)]
    public partial class ActivityDetailsGoalsConfig : CommonBaseControl {
        #region Action Field Names

        private const string FLD_ID = "ID";
        private const string FLD_ITEM_ID = "Item_ID";
        private const string FLD_ITEM_NAME = "name";
        private const string FLD_PRODUCT_ID = "ProductCn_ID";
        private const string FLD_PRODUCT_NAME = "ProductCnName";
        private const string FLD_IS_UNLOAD = "isUnload";
        private const string FLD_PRICE_TYPE = "PriceType";
        private const string FLD_PRICE = "Price";
        private const string FLD_OVERVALUE = "Overvalue";
        private const string FLD_CONCURRENTCN_ID = "ConcurrentCn_ID";
        private const string FLD_STATUS = "Status";

        private const string FLD_BONUSPRODUCT_ID = "BonusProduct_ID";
        private const string FLD_CORRECTION_RATE = "CorrectionRate";
        private const string FLD_MAX_DISCOUNT_PRC = "MaxDiscountPrc";

        #endregion

        private const string DETAILS_DISABLE_CHANGE_SKU = "DisableChangeSKU";
        private const string DETAILS_DISABLE_CHANGE_KPI = "DisableChangeKPI";
        private const string DETAILS_DIV_TARGET_BY_OL_TYPE = "DivTargetByOlType";
        private const string DETAILS_TARGET_PERIOD_TYPE = "TargetPeriodType";
        private const string DETAILS_TARGET_REGION_TYPE = "TargetRegionType";
        private const string DETAILS_TARGET_PERIOD_NAME = "TargetPeriodName";
        private const string DETAILS_TARGET_REGION_NAME = "TargetRegion_Name";
        private const string DETAILS_ACTION_MECHANIC_PRICE_TYPE = "MechanicPriceType_ID";

        private const string DETAILS_ENABLE_MORE_ONE_AIM = "isEnableMoreOneAim";

        #region Not Used

        /// <summary>
        /// Gets the selected split by POC type id.
        /// </summary>
        /// <value>The selected split by POC type id.</value>
        public SplitPOCByType SelectedSplitByPOCType {
            get {
                SplitPOCByType type = SplitPOCByType.ByDistrict;
                ComboBoxItem item = cbSplitByPOCType.EditValue as ComboBoxItem;

                if (item != null)
                    type = (SplitPOCByType) item.Id;

                return type;
            }
        }

        #endregion Not Used

        #region Fields

        private bool _wasDataLoaded;
        private bool isLogicTreeFirstDraw = true;
        private bool isDigitTreeFirstDraw = true;

        /// <summary>
        /// List of all aims
        /// </summary>
        /// <summary>
        /// List of all aims
        /// </summary>
        private List<DataTransferObjectAim> _aims;

        /// <summary>
        /// SKU tree template
        /// </summary>
        private readonly DataTable _skuTreeTable;

        /// <summary>
        /// KPI tree template
        /// </summary>
        private readonly DataTable _kpiTreeTable;

        /// <summary>
        /// Questionnary logic tree template
        /// </summary>
        private DataTable _qpqLogicTreeTable;

        /// <summary>
        /// Questionnary digit tree template
        /// </summary>
        private DataTable _qpqDigitTreeTable;

        /// <summary>
        /// List of aims showen in ListGoal list
        /// </summary>
        private List<ComboBoxItem> _aimList;

        /// <summary>
        /// Current aim version
        /// </summary>
        private DataTransferObjectAim _aimCur;

        /// <summary>
        /// Original aim version
        /// </summary>
        private DataTransferObjectAim _aimOriginal;

        /// <summary>
        /// Allows to skip listGoals_SelectedIndexChanged event
        /// </summary>
        private bool _isNotChangeIndexEvent;

        /// <summary>
        /// Allows to skip cbGoalType_EditValueChanged event
        /// </summary>
        private bool _isNotComboEvent = true;

        private List<ComboBoxItem> lFactTypeSource;

        private List<ComboBoxItem> lFactCalcMonth; 

        private readonly List<int> _deletedMechanics = new List<int>();

        private int MechanicPricaType { get; set; }
        private int ActivityId { get; set; }
        private int TemplateId { get; set; }

        private bool canModify = false;

        public ActivityDetailsControl MainControl { get; set; }

        public bool IsOnInvoiceAim { 
            get {
                foreach(DataTransferObjectAim lAim in _aims)
                    if (lAim.Type == Convert.ToInt32(GoalType.OnInvoice) && lAim.State != AimRecordState.Deleted)
                        return true;
                return false;
            }
        }

        public int OnInvoiceAimId {
            get {
                foreach(DataTransferObjectAim lAim in _aims)
                    if (lAim.Type == Convert.ToInt32(GoalType.OnInvoice))
                        return lAim.Id;
                return -1;
            }
        }

        #endregion Fields

        #region Constructors

        public ActivityDetailsGoalsConfig() {
            InitializeComponent();

            chlbIPTRList.DataSource = DataProvider.GetIPTRList();
            _aims = new List<DataTransferObjectAim>();
            _skuTreeTable = DataProvider.GetSKUTree(-1);
            _kpiTreeTable = DataProvider.GetKPITree(-1);
            _qpqLogicTreeTable = DataProvider.GetQPQTree(-1, 1);
            _qpqDigitTreeTable = DataProvider.GetQPQTree(-1, 2);
            _aimList = new List<ComboBoxItem>();
            _aimCur = new DataTransferObjectAim();
            _aimOriginal = new DataTransferObjectAim();
        }

        public ActivityDetailsGoalsConfig(CommonBaseTab parentTab) : base(parentTab) {
            chlbIPTRList.DataSource = DataProvider.GetIPTRList();

            _aims = new List<DataTransferObjectAim>();
            _skuTreeTable = DataProvider.GetSKUTree(-1);
            _kpiTreeTable = DataProvider.GetKPITree(-1);
            _qpqLogicTreeTable = DataProvider.GetQPQTree(-1, 1);
            _qpqDigitTreeTable = DataProvider.GetQPQTree(-1, 2);
            _aimList = new List<ComboBoxItem>();
            _aimCur = new DataTransferObjectAim();
            _aimOriginal = new DataTransferObjectAim();
        }

        #endregion Constructors

        #region Public methods

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="templateId">The template id.</param>
        public void LoadData(int templateId, int activityId, DataTable dtTemplateDetails, DataTable dtActivityDetails) {
            if (_wasDataLoaded) {
                _isNotChangeIndexEvent = listGoals.SelectedIndex > -1;
                //isNotComboEvent = true;
            }

            TemplateId = templateId;
            ActivityId = activityId;

            canModify = (MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.GoalsSetter)
                         || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Initiator))
                        && MainControl.AccessChecker.PromoTTCount == MainControl.AccessChecker.PromoTTAvailable;

            _aims = new List<DataTransferObjectAim>();

            SetTemplateData(dtTemplateDetails);

            if (MainControl.IsEditMode) {
                SetActivityDetailsData(dtActivityDetails);
                rbMPAType.SelectedIndex = MechanicPricaType;

                LoadAllAimsFromDB(ActivityId);
            }
            else {
                LoadAllAimsFromDB(TemplateId);

                foreach (DataTransferObjectAim aim in _aims) {
                    aim.Id = -1;
                    LoadAim(aim, false);
                    GetAllAimFields();
                    SaveAimToList(_aimCur);
                }
            }

            if (!MainControl.ActivityInfo.IsPriceActivity)
                splitContainerMPA.PanelVisibility = SplitPanelVisibility.Panel1;
            else {
                InitPriceActivityControls();
                ChangeMPAGridData();
                if (!MainControl.IsEditMode)
                    FormInitialActions();
            }

            if (!_wasDataLoaded) {
                LoadDropDowns();
                checkShowAll.Checked = false;
            }

            RefreshAimList(checkShowAll.Checked, (GoalType) cbGoalType.EditValue);
            RedrawButtonAccess();

            if (MainControl.ActivityInfo.IsPriceActivity) {
                //cbGoalType.Enabled = false;
                checkShowAll.Enabled = false;
                skuTreeCtrl.AllowedLevel = 2;
            }

            LoadAim(AimRecordState.Unknown, ConvertEx.ToInt(cbGoalType.EditValue), false);

            if (!canModify) {
                SetReadOnlyModeRecursively(this);
                cbGoalType.Properties.ReadOnly = false;
                checkShowAll.Properties.ReadOnly = false;
            }

            _wasDataLoaded = true;
        }

        /// <summary>
        /// Save all aims to DB
        /// </summary>
        public void SaveAllAims(int templateId) {
            // in case there is an unsaved aim; all data is valid
            GetAllAimFields();

            if (IsModified()) {
                if (_aimCur.State == AimRecordState.New) {
                    labelEdit.Visible = false;
                    textCurAimName.Visible = false;

                    SaveAimToList(_aimCur);
                }
                else
                    SaveAimToList(_aimCur);
            }
            // end in case there is an unsaved aim

            foreach (DataTransferObjectAim aim in _aims) // new aims that have been deleled
                if (aim.State == AimRecordState.Deleted && aim.Id == -1)
                    aim.State = AimRecordState.Unknown;

            foreach (DataTransferObjectAim aim in _aims)
                if (aim.State == AimRecordState.Updated)
                    SaveAimToDB(aim, templateId);
                else {
                    if (aim.State == AimRecordState.Deleted)
                        DataProvider.DeleteAim(aim.Id);
                }

            _aimOriginal.Copy(_aimCur);
        }

        /// <summary>
        /// Determines whether there were changes on this tab
        /// </summary>
        public bool WasGoalTabModified {
            get {
                bool wasModified;
                int countAimsChanged = 0;
                foreach (DataTransferObjectAim aim in _aims)
                    if (aim.State == AimRecordState.Updated || aim.State == AimRecordState.Deleted)
                        countAimsChanged++;

                wasModified = countAimsChanged > 0;

                GetAllAimFields();

                wasModified = wasModified || IsModified();

                return wasModified;
            }
        }

        /// <summary>
        /// Determines whether current and original versions are different
        /// </summary>
        /// <returns>whether the aim was modified</returns>
        private bool IsModified() {
            GetAllAimFields();

            return !_aimOriginal.Equals(_aimCur) || _aimCur.State == AimRecordState.New;
        }

        /// <summary>
        /// Determines whether current and original versions are different
        /// </summary>
        /// <returns>whether the aim was modified</returns>
        public bool IsUnsavedAim() {
            return simpleSave.Enabled;
        }

        /// <summary>
        /// Overrides base validate method and returns metadata of validate result. 
        /// </summary>
        /// <returns>whether is valid</returns>
        internal bool ValidateData() {
            GetAllAimFields();
            string lMsg;
            bool lIsCorrect = _aimCur.State == AimRecordState.Unknown || IsAimCurValid(out lMsg);
            return lIsCorrect;
        }

        internal void SaveToListUnsavedAim() {
            GetAllAimFields();

            string lMsg;
            if (!IsAimCurValid(out lMsg)) {
                XtraMessageBox.Show(lMsg, "Предупреждение", MessageBoxButtons.OK);
                return;
            }

            if (IsModified()) {
                if (!IsAimCurValid(out lMsg)) {
                    XtraMessageBox.Show(lMsg, "Предупреждение", MessageBoxButtons.OK);
                    return;
                }

                if (_aimCur.State == AimRecordState.New) {
                    SaveAimToList(_aimCur);
                    RefreshAimList(checkShowAll.Checked, (GoalType) cbGoalType.EditValue);
                    _isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = GetIdByValue(_aims.Count - 1);
                    LoadAim(_aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);
                }
                else {
                    int lIndex = listGoals.SelectedIndex;
                    SaveAimToList(_aimCur);
                    RefreshAimList(checkShowAll.Checked, (GoalType) cbGoalType.EditValue);
                    _isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = lIndex;
                    LoadAim(_aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);
                }
            }

            labelEdit.Visible = false;
            textCurAimName.Visible = false;

            RedrawTreeWindow((GoalType) _aimCur.Type, true);
            RedrawAimControls((GoalType) _aimCur.Type, false);

            RedrawButtonAccess();
        }

        internal void RenewUnsavedAim() {
            LoadAim(_aimOriginal, true);

            labelEdit.Visible = false;
            textCurAimName.Visible = false;

            RedrawButtonAccess();
        }

        /// <summary>
        /// Gets all fields.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <param name="templateId">The template id.</param>
        public void GetAllFields(ActivityDetailsDTO dto) {
            dto.DisableChangeSKU = checkCannotChangeSKU.Checked;
            dto.DisableChangeKPI = checkCannotChangeKPI.Checked;
            dto.DisableChangeType = checkSplitByPOCType.Checked;

            //dto.SplitByTime = ((ComboBoxItem) cbSplitByTime.SelectedItem).Id;
            //dto.SplitByPOCType = ((ComboBoxItem) cbSplitByPOCType.SelectedItem).Id;

            //dto.SKUIdList = skuTreeCtrl.SelectedSKUIdList;

            //if (dto.IsKPISummaryValueValid)
            //    dto.KPIIdList = treeKPI.SelectedKPIIdList;

            // new
            dto.TemplateId = MainControl.TemplateId;
            dto.IsEnableMoreOneAim = checkAllowMultipleGoals.Checked;
            //

            dto.IsPriceActivity = MainControl.ActivityInfo.IsPriceActivity;
            GatherActivityInfo(dto);
        }

        public override bool ValidateChildren() {
            return base.ValidateChildren() && ValidateData();
        }

        #endregion Public methods

        #region IPTR methods

        /// <summary>
        /// Gets the selected IPTR id list.
        /// </summary>
        /// <value>The selected IPTR id list.</value>
        private string SelectedIPTRList {
            get {
                return chlbIPTRList.CheckedIndices.Count > 0
                           ? ConvertEx.ToString(chlbIPTRList.GetItemValue(chlbIPTRList.CheckedIndices[0]))
                           : string.Empty;
            }
        }

        /// <summary>
        /// Loads the IPTR list
        /// </summary>
        private void LoadIPTRList(string Id) {
            chlbIPTRList.UnCheckAll();

            int id = int.Parse(Id);

            for (int i = 0; i < (chlbIPTRList.DataSource as DataTable).Rows.Count; i++) {
                if (ConvertEx.ToInt(chlbIPTRList.GetItemValue(i)) == id) {
                    chlbIPTRList.SetItemChecked(i, true);
                }
            }
        }

        #endregion IPTR methods

        #region Price Activity

        private MechanicInfo GetMechanicInfo(DataRow row) {
            if (ConvertEx.ToInt(row[FLD_STATUS]) == 9) // deleted
                return null;

            MechanicInfo mi = new MechanicInfo();

            mi.ID = ConvertEx.ToInt(row[FLD_ID]);
            mi.IsBonusProduct = (rbMPAType.SelectedIndex == 1);
            mi.ProductCnID = ConvertEx.ToInt(row[FLD_PRODUCT_ID]);
            mi.ConcurrentCnID = ConvertEx.ToInt(row[FLD_CONCURRENTCN_ID]);
            mi.PriceTypeID = ConvertEx.ToInt(row[FLD_PRICE_TYPE]);
            if (row[FLD_PRICE] == DBNull.Value)
                mi.Price = null;
            else
                mi.Price = ConvertEx.ToDecimal(row[FLD_PRICE]);
            mi.Overvalue = ConvertEx.ToDecimal(row[FLD_OVERVALUE]);
            if (mi.IsBonusProduct) {
                mi.BonusProductID = ConvertEx.ToInt(row[FLD_BONUSPRODUCT_ID]);
                mi.CorrectionVCoef = ConvertEx.ToInt(row[FLD_CORRECTION_RATE]);
                mi.MaxDiscount = ConvertEx.ToDecimal(row[FLD_MAX_DISCOUNT_PRC]);
            }

            return mi;
        }

        private void SavePriceActivityInfo(ActivityDetailsDTO activityDetails) {
            for (int i = 0; i < activityDetails.DeletedMechanics.Count; i++)
                DataProvider.DeleteMechanic(activityDetails.DeletedMechanics[i]);

            _deletedMechanics.Clear();

            for (int i = 0; i < activityDetails.MechanicsInfo.Count; i++) {
                MechanicInfo mi = activityDetails.MechanicsInfo[i];
                DataProvider.UpdateMechanic(activityDetails.ActivityId, mi);
            }
        }

        private void skuTreeCtrl_OnValueChanged(object sender, EventArgs args) {
            if (!MainControl.ActivityInfo.IsPriceActivity || MainControl.ActivityInfo.IsApproved)
                return;

            // need add or remove item from list price activity
            NodeEventArgs nodeEventArgs = args as NodeEventArgs;
            DoModifyPriceActivity(nodeEventArgs);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private DataRow GetNodeDataRow(TreeListNode node) {
            DataRow result = null;

            DataTable data = node.TreeList.DataSource as DataTable;
            if (null != data) {
                DataRow row = data.Rows[node.Id];
                if (null != row)
                    result = row;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeEventArgs"></param>
        private void DoModifyPriceActivity(NodeEventArgs nodeEventArgs) {
            if (MainControl.ActivityInfo.IsPriceActivity && null != nodeEventArgs) {
                TreeListNode node = nodeEventArgs.Node;

                bool isChecked = node.Checked;

                //while (null != node && skuTreeCtrl.GetNodeLevel(node) > 2) // 2 - combi
                //{
                //    node = node.ParentNode;
                //}

                if (null != node) {
                    List<DataRow> rows = skuTreeCtrl.GetProductsInfoByLevel(node, 2); // 2 - combi
                    if (isChecked) {
                        // add new activity
                        AddProducts(rows);
                    }
                    else {
                        // remove activity
                        RemoveProducts(rows);
                    }
                }
            }
        }

        private void rbMPAType_SelectedIndexChanged(object sender, EventArgs e) {
            ChangeMPAGridData();
            if (!MainControl.IsEditMode)
                FormInitialActions();
        }

        private void ChangeMPAGridData() {
            if (!MainControl.ActivityInfo.IsPriceActivity)
                return;

            bool isBonusEnabled = false;
            SoftServe.Reports.MarketProgramms.DataProvider.MechanicsType mechanicsType = DataProvider.MechanicsType.Discount;
            if (rbMPAType.SelectedIndex == 1) {
                // load action mechanic with bonus product
                mechanicsType = DataProvider.MechanicsType.Bonus;
                isBonusEnabled = true;
            }
            else {
                // load action mechanic with discount
                // default value are initializad
            }

            DataTable gridData = DataProvider.GetMechanics(mechanicsType, MainControl.ActivityId);

            gridColumnBonusProduct.Visible = isBonusEnabled;
            gridColumnCorrectionVCoefficient.Visible = isBonusEnabled;
            gridColumnMaxDiscountProc.Visible = isBonusEnabled;

            //MPAGridView.Columns.Clear();
            MPAGridControl.DataSource = gridData;
            //MPAGridView.PopulateColumns();
            MPAGridControl.ForceInitialize();
            MPAGridView.RefreshData();
        }

        private void FormInitialActions() {
            List<DataRow> combiProducts = skuTreeCtrl.SelectedCombiProducts;
            AddProducts(combiProducts);
        }

        private void AddProducts(List<DataRow> products) {
            DataTable source = MPAGridControl.DataSource as DataTable;
            //DataTable newSource = source.Copy();

            for (int i = 0; i < products.Count; i++) {
                DataRow[] selectedRows = source.Select(String.Format("{0}={1}", FLD_PRODUCT_ID, products[i][FLD_ITEM_ID]));
                if (0 == selectedRows.Length)
                    AddAction(products[i], source);
            }
        }

        private void RemoveProducts(List<DataRow> products) {
            if (products.Count > 0) {
                DataTable source = MPAGridControl.DataSource as DataTable;
                for (int i = products.Count - 1; i >= 0; i--) {
                    try {
                        //source.DefaultView
                        // need remember deleted row
                        DataRow[] selectedRows = source.Select(String.Format("{0}={1}", FLD_PRODUCT_ID, products[i][FLD_ITEM_ID]));
                        foreach (DataRow row in selectedRows) {
                            _deletedMechanics.Add(ConvertEx.ToInt(row[0]));
                            source.Rows.Remove(row);
                        }
                    }
                    catch (Exception e) {
                    }
                }
            }
        }

        private void AddAction(DataRow sourceDataRow, DataTable destination) {
            if (null == sourceDataRow)
                return;

            if (null != destination) {
                try {
                    DataRow newRow = destination.NewRow();
                    newRow[FLD_PRODUCT_ID] = sourceDataRow[FLD_ITEM_ID];
                    newRow[FLD_PRODUCT_NAME] = sourceDataRow[FLD_ITEM_NAME];
                    newRow[FLD_IS_UNLOAD] = 0;
                    newRow[FLD_OVERVALUE] = 0;
                    newRow[FLD_PRICE] = DBNull.Value;
                    destination.Rows.Add(newRow);
                    destination.AcceptChanges();
                }
                catch (Exception e) {
                }
            }
        }

        private void InitPriceActivityControls() {
            // init price types combo
            DataTable data = DataProvider.GetPriceTypes();
            repGridColumnPriceType.DataSource = data;
            repGridColumnPriceType.EditValueChanged += new EventHandler(repGridColumnPriceType_EditValueChanged);

            // init concurent products
            data = DataProvider.GetConcurentCombiProducts();
            repGridColumnCompetitorSKU.DataSource = data;

            // init bonus product info
            data = DataProvider.GetBonusCombiProducts();
            repGridColumnBonusProduct.DataSource = data;
        }

        private void repGridColumnPriceType_EditValueChanged(object sender, EventArgs e) {
        }

        private void splitContainerMPA_Panel2_Resize(object sender, EventArgs e) {
            if (MainControl == null || MainControl.ActivityInfo == null || !MainControl.ActivityInfo.IsPriceActivity)
                return;

            int radioHeight = rbMPAType.Size.Height;
            int panelHeight = splitContainerMPA.Panel2.Height;
            if (panelHeight / 2 > radioHeight)
                MPAGridControl.Height = panelHeight - radioHeight;
        }

        #endregion Price Activity

        #region Event handling

        /// <summary>
        /// Allows to check only one node in IPTR list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chlbIPTRList_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e) {
            if (e.State != CheckState.Checked) {
                return;
            }

            CheckedListBoxControl lb = sender as CheckedListBoxControl;

            for (int i = 0; i < lb.ItemCount; i++) {
                if (i != e.Index) {
                    lb.SetItemChecked(i, false);
                }
            }
        }

        /// <summary>
        /// Change access to conrols after checking MultAims checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkAllowMultipleGoals_CheckedChanged(object sender, EventArgs e) {
            RedrawButtonAccess();
        }

        /// <summary>
        /// Determines whether all aims should be shown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkShowAll_CheckedChanged(object sender, EventArgs e) {
            int index;

            if (checkShowAll.Checked) {
                cbGoalType.Enabled = false;

                if (listGoals.SelectedIndex > -1) {
                    _isNotChangeIndexEvent = true; /////////////////////
                    index = ConvertEx.ToInt(listGoals.SelectedValue);
                    RefreshAimList(true, (GoalType) _aimCur.Type);
                    _isNotChangeIndexEvent = true; /////////////////////
                    listGoals.SelectedIndex = GetIdByValue(index);
                }
                else {
                    RefreshAimList(true, (GoalType) cbGoalType.EditValue);
                }
            }
            else {
                cbGoalType.Enabled = true;
                if (listGoals.SelectedIndex > -1) {
                    _isNotChangeIndexEvent = true; //////////////////////
                    index = ConvertEx.ToInt(listGoals.SelectedValue);
                    RefreshAimList(false, (GoalType) _aimCur.Type);
                    _isNotChangeIndexEvent = true; /////////////////////
                    listGoals.SelectedIndex = GetIdByValue(index);
                }
                else {
                    RefreshAimList(false, (GoalType) cbGoalType.EditValue);
                }
            }

            RedrawButtonAccess();
        }

        /// <summary>
        /// Implements aims reloading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbGoalType_EditValueChanged(object sender, EventArgs e) {
            if (!checkShowAll.Checked) {
                RefreshAimList(false, (GoalType) cbGoalType.EditValue);
                LoadAim(AimRecordState.Unknown, (int) cbGoalType.EditValue, false);
            }

            labelEdit.Visible = false;
            textCurAimName.Visible = false;

            RedrawButtonAccess();
        }

        /// <summary>
        /// Implements trees reloading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbGoalType_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e) {
            if (_isNotComboEvent) {
                _isNotComboEvent = false;
                return;
            }

            if (checkShowAll.Checked || !IsModified())
                return;

            if (_aimCur.State != AimRecordState.New)
                _isNotChangeIndexEvent = true;

            DialogResult lDialogResult = XtraMessageBox.Show(Resource.ConfirmSaveAim, Resource.Warning, MessageBoxButtons.YesNoCancel);
            if (lDialogResult == DialogResult.Yes) {
                string lMsg;
                if (!IsAimCurValid(out lMsg)) {
                    XtraMessageBox.Show(lMsg, Resource.Warning, MessageBoxButtons.OK);
                    e.Cancel = true;
                }
                else
                    SaveAimToList(_aimCur); // save modified aim
            }

            if (lDialogResult == DialogResult.Cancel)
                e.Cancel = true;
        }

        /// <summary>
        /// Implements trees reloading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listGoals_SelectedIndexChanged(object sender, EventArgs e) {
            if (_isNotChangeIndexEvent) {
                _isNotChangeIndexEvent = false;
                return;
            }

            bool isModified = IsModified();

            if (!isModified)
            {
                LoadAim(_aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);

                if (checkShowAll.Checked) {
                    cbGoalType.EditValue = _aimCur.Type;
                }
            }

            //in case it is modified
            if (isModified)
            {
                DialogResult result = XtraMessageBox.Show(Resource.ConfirmSaveAim, Resource.Warning, MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes) {
                    string mes;
                    if (!IsAimCurValid(out mes)) {
                        XtraMessageBox.Show(mes, Resource.Warning, MessageBoxButtons.OK);

                        _isNotChangeIndexEvent = true;
                        listGoals.SelectedIndex = GetIdByValue(_aims.FindIndex(aim => _aimOriginal.Equals(aim)));

                        return;
                    }

                    int index = ConvertEx.ToInt(listGoals.SelectedValue);

                    // save modified aim
                    SaveAimToList(_aimCur);
                    _isNotChangeIndexEvent = true;
                    RefreshAimList(checkShowAll.Checked, (GoalType) cbGoalType.EditValue);
                    // end save modified aim

                    _isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = GetIdByValue(index);

                    LoadAim(_aims[index], true);
                }

                if (result == DialogResult.No) {
                    LoadAim(_aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);

                    if (checkShowAll.Checked) {
                        cbGoalType.EditValue = _aimCur.Type;
                    }
                }

                if (result == DialogResult.Cancel) {
                    _isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = GetIdByValue(_aims.FindIndex(aim => _aimOriginal.Equals(aim)));

                    return;
                }
            }
            //end in case it is modified

            textCurAimName.Visible = false;
            labelEdit.Visible = false;

            RedrawButtonAccess();
        }

        /// <summary>
        /// Adds aim
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleAddGoal_Click(object sender, EventArgs e) {
            bool canAddGoal = checkAllowMultipleGoals.Checked ||
                             !checkAllowMultipleGoals.Checked && CurAimsCount() == 0;
            if (!canAddGoal)
            {
                XtraMessageBox.Show("Нельзя добавить более одной цели!", "Предупреждение", MessageBoxButtons.OK);
                return;
            }

            if (IsOnInvoiceAim &&
               (GoalType) ConvertEx.ToInt(cbGoalType.EditValue) == GoalType.OnInvoice) {
                XtraMessageBox.Show("Нельзя добавить 2 цели On-Invoice!", "Предупреждение", MessageBoxButtons.OK);
                return;
            }

            if (MainControl.ActivityInfo.IsPriceActivity) {
                XtraMessageBox.Show("Нельзя добавить цель в ценовую активность!", "Предупреждение", MessageBoxButtons.OK);
                return;
            }

           
            //cbGoalType.Enabled = true;

            textCurAimName.Visible = true;
            labelEdit.Visible = true;
            //simpleSave.Enabled = true;

            if (listGoals.SelectedIndex > -1) {
                _isNotChangeIndexEvent = true;
                listGoals.SelectedIndex = -1;
            }

            LoadAim(AimRecordState.New, ConvertEx.ToInt(cbGoalType.EditValue), true);

            switch ((GoalType) ConvertEx.ToInt(cbGoalType.EditValue)) {
                case GoalType.SalesVolume:
                    textCurAimName.Text = "Новая цель по объему продаж ";
                    break;
                case GoalType.Merchandising:
                    textCurAimName.Text = "Новая цель по мерчендайзингу ";
                    break;
                case GoalType.Effectiveness:
                    textCurAimName.Text = "Новая цель по эффективности оборудования ";
                    break;
                case GoalType.OnInvoice:
                    textCurAimName.Text = "On-Invoice";
                    //_isOnInvoiceGoal = true;
                    break;
                case GoalType.QuestionnaireLogic:
                    textCurAimName.Text = "Новая цель по логическому вопросу ";
                    break;
                case GoalType.QuestionnaireDigit:
                    textCurAimName.Text = "Новая цель по числовому вопросу ";
                    break;
                case GoalType.InBevDoorCount:
                    textCurAimName.Text = "Новая цель по количеству дверей InBev ";
                    break;
                case GoalType.MustStock:
                    textCurAimName.Text = "Новая цель по наличию MUST Stock ";
                    break;
            }

            textCurAimName.Text += (GoalType) ConvertEx.ToInt(cbGoalType.EditValue) != GoalType.OnInvoice
                                       ? _aimList.Count.ToString(CultureInfo.InvariantCulture)
                                       : "";
            _aimOriginal.Name = string.Copy(textCurAimName.Text);
            RedrawButtonAccess();
            AllowEdit();
        }

        /// <summary>
        /// Shows other checkboxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkUseAimValue_CheckedChanged(object sender, EventArgs e)
        {
            checkReversExecution.Visible = checkAccPerfOfTargetVal.Visible = checkUseAimValue.Checked;

            if (!checkUseAimValue.Checked)
            {
                checkReversExecution.Checked = checkAccPerfOfTargetVal.Checked = false;
            }
        }

        /// <summary>
        /// Deletes aim
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleDeleteGoal_Click(object sender, EventArgs e) {
            int aimType = ConvertEx.ToInt(cbGoalType.EditValue);

            if (aimType == (int) GoalType.OnInvoice && MainControl.HasOnInvoiceRecords)// coordinate in_invoice tab
            {
                XtraMessageBox.Show("Невозможно удалить цель: для нее есть coзданные лимиты", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (aimType == (int)GoalType.OnInvoice)// coordinate in_invoice tab
            {
                MainControl.EnableOnInvoiceTab(false);
            }

            DialogResult result = XtraMessageBox.Show("Вы уверены, что хотите удалить эту цель?", "Предупреждение", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes) {
                _aimOriginal.State = AimRecordState.Deleted;
                RefreshAimList(checkShowAll.Checked, (GoalType) aimType);
                LoadAim(AimRecordState.Unknown, aimType, false);
                if (checkShowAll.Checked)
                    cbGoalType.EditValue = aimType;
            }
            RedrawButtonAccess();
        }

        /// <summary>
        /// Saves aim
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleSaveAim_Click(object sender, EventArgs e) {
            GetAllAimFields();
            string lMsg;
            if (!IsAimCurValid(out lMsg)) {
                XtraMessageBox.Show(lMsg, "Предупреждение", MessageBoxButtons.OK);
                return;
            }

            if (IsModified()) {
                if (!IsAimCurValid(out lMsg)) {
                    XtraMessageBox.Show(lMsg, "Предупреждение", MessageBoxButtons.OK);
                    return;
                }

                if (_aimCur.State == AimRecordState.New) {
                    SaveAimToList(_aimCur);
                    RefreshAimList(checkShowAll.Checked, (GoalType) cbGoalType.EditValue);
                    _isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = GetIdByValue(_aims.Count - 1);
                    LoadAim(_aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);
                }
                else {
                    int index = listGoals.SelectedIndex;
                    SaveAimToList(_aimCur);
                    RefreshAimList(checkShowAll.Checked, (GoalType) cbGoalType.EditValue);
                    _isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = index;
                    LoadAim(_aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);
                }
            }

            labelEdit.Visible = false;
            textCurAimName.Visible = false;
            RedrawTreeWindow((GoalType) _aimCur.Type, true);
            RedrawAimControls((GoalType) _aimCur.Type, false);
            RedrawButtonAccess();
        }

        /// <summary>
        /// Alows aim editing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleEdit_Click(object sender, EventArgs e) {
            AllowEdit();
            //MainControl.barButtonSave.Enabled = false;
            //MainControl.barButtonRemove.Enabled = false;
        }

        /// <summary>
        /// Gets new version of aim list
        /// </summary>
        /// 
        /// <param name="showAll">show all aim or only of the current type</param>
        /// <param name="aimType">aim type</param>
        private void RefreshAimList(bool showAll, GoalType aimType) {
            _aimList = new List<ComboBoxItem>();
            int lIndex = 0;
            if (showAll) {
                foreach (DataTransferObjectAim aim in _aims) {
                    if (aim.State != AimRecordState.Deleted)
                        _aimList.Add(new ComboBoxItem(lIndex, aim.Name));
                    lIndex++;
                }
            }
            else {
                foreach (DataTransferObjectAim aim in _aims) {
                    if (aim.State != AimRecordState.Deleted && aim.Type == ConvertEx.ToInt(aimType))
                        _aimList.Add(new ComboBoxItem(lIndex, aim.Name));
                    lIndex++;
                }
            }

            listGoals.SelectedIndexChanged -= listGoals_SelectedIndexChanged;

            listGoals.DataSource = _aimList;

            listGoals.SelectedIndex = -1;
            listGoals.SelectedIndexChanged += listGoals_SelectedIndexChanged;
        }

        /// <summary>
        /// Change tree window according to the aim type
        /// </summary>
        /// <param name="aimType">aim type</param>
        /// <param name="visible">if the tree should be visible</param>
        private void RedrawTreeWindow(GoalType aimType, bool visible) {
            switch (aimType) {
                case GoalType.SalesVolume:
                    groupSKU.Dock = DockStyle.Fill;
                    groupSKU.Visible = true;
                    groupSKU.Text = "Список СКЮ для целей по V продаж:";
                    skuTreeCtrl.Visible = visible;
                    skuTreeCtrl.ReadOnly = true;

                    groupConditionsList.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.Merchandising:
                    groupConditionsList.Dock = DockStyle.Fill;
                    groupConditionsList.Visible = true;
                    treeKPI.Visible = visible;
                    treeKPI.ReadOnly = true;

                    groupSKU.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.Effectiveness:
                    groupEffectiveness.Dock = DockStyle.Fill;
                    groupEffectiveness.Visible = true;
                    chlbIPTRList.Visible = visible;
                    chlbIPTRList.Enabled = false;

                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.OnInvoice:
                    groupSKU.Visible = false;
                    groupConditionsList.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.QuestionnaireLogic:
                    groupQPQLogic.Dock = DockStyle.Fill;
                    groupQPQLogic.Visible = true;
                    qpqTreeLogic.Visible = visible;
                    qpqTreeLogic.ReadOnly = true;

                    groupEffectiveness.Visible = false;
                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQDigit.Visible = false;
                    break;

                case GoalType.QuestionnaireDigit:
                    groupQPQDigit.Dock = DockStyle.Fill;
                    groupQPQDigit.Visible = true;
                    qpqTreeDigit.Visible = visible;
                    qpqTreeDigit.ReadOnly = true;

                    groupEffectiveness.Visible = false;
                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;
                    
                case GoalType.InBevDoorCount:
                    groupQPQDigit.Visible = false;
                    qpqTreeDigit.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.MustStock:
                    groupSKU.Dock = DockStyle.Fill;
                    groupSKU.Visible = true;
                    groupSKU.Text = "Список СКЮ для целей по наличию MUST Stock:";
                    skuTreeCtrl.Visible = visible;
                    skuTreeCtrl.ReadOnly = true;

                    groupConditionsList.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Change aim cintrls according to the aim type
        /// </summary>
        /// <param name="aimType">aim type</param>
        /// <param name="enabled">if the tree should be enabled</param>
        private void RedrawAimControls(GoalType aimType, bool enabled) {
            checkUseInCalculations.Checked = _aimCur.IsUsedInCalculations;
            checkUseInCalculations.Enabled = enabled;

            checkReversExecution.Checked = _aimCur.IsInReverseExecution;
            checkReversExecution.Enabled = enabled;

            checkUseAimValue.Checked = _aimCur.IsUseAimValue;
            checkUseAimValue.Enabled = enabled;

            checkAccPerfOfTargetVal.Checked = _aimCur.IsAccPerfOfTargetVal;
            checkAccPerfOfTargetVal.Enabled = enabled;
            checkAccPerfOfTargetVal.Visible = checkUseAimValue.Checked;

            cbVisitChecked.EditValue = _aimCur.IsPeriodCalc ? 1 : 0;
            cbVisitChecked.Enabled = enabled;
            labelVisitChecked.Visible = enabled;

            cbVisitChecked.Visible = aimType == GoalType.Merchandising || aimType == GoalType.QuestionnaireDigit ||
                    aimType == GoalType.QuestionnaireLogic;
            checkReversExecution.Visible = aimType == GoalType.QuestionnaireDigit ||
                                           (aimType == GoalType.Merchandising && checkUseAimValue.Checked);
            labelVisitChecked.Visible = cbVisitChecked.Visible;


            lblPeriodCalc.Visible = lblFactCalcway.Visible =
            lcpPeriodCalk.Visible = lcpFactsCalcWay.Visible = aimType == GoalType.MustStock;
            lcpPeriodCalk.Enabled = lcpFactsCalcWay.Enabled
            = lblFactCalcway.Enabled = lblPeriodCalc.Enabled = enabled;
            lcpPeriodCalk.EditValue = _aimCur.FactCalcPeriod;

            lcpFactsCalcWay.EditValue = _aimCur.FactCalcWay;
            lcpPeriodCalk.EditValue = _aimCur.FactCalcPeriod;

            checkUseAimValue.Visible = aimType == GoalType.Merchandising;
        }

        /// <summary>
        /// Change state of Save and Delete buttons
        /// </summary>
        private void RedrawButtonAccess() {
            checkAllowMultipleGoals.Enabled = CurAimsCount() == 0 || CurAimsCount() == 1 && _aimCur.State != AimRecordState.New;
            checkAllowMultipleGoals.Enabled = checkAllowMultipleGoals.Enabled && canModify;

            //new
            if (MainControl.ActivityInfo.IsPriceActivity) {
                checkAllowMultipleGoals.Enabled = false;
            }

            simpleDelete.Enabled = canModify && _aimCur.State != AimRecordState.New && listGoals.SelectedIndex > -1
                                   && !textCurAimName.Visible;
            simpleSave.Enabled = canModify && _aimCur.State == AimRecordState.New || listGoals.SelectedIndex > -1 && textCurAimName.Visible;
            simpleEdit.Enabled = canModify && _aimCur.State != AimRecordState.New && listGoals.SelectedIndex > -1 && !textCurAimName.Visible;


            simpleAdd.Enabled = canModify && _aimCur.State != AimRecordState.New
                                && !checkShowAll.Checked && !textCurAimName.Visible;

            MainControl.barButtonRemove.Enabled = !simpleSave.Enabled && MainControl.IsEditMode
                                                  && !MainControl.ActivityInfo.IsActivityStarted
                                                  && MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Initiator);
            MainControl.barButtonSave.Enabled = !simpleSave.Enabled
                                                && (MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Initiator)
                                                    || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.Approver)
                                                    || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.ControlGroupSelector)
                                                    || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.FactController)
                                                    || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.GoalsSetter)
                                                    || MainControl.AccessChecker.CheckAccess(ActivityPositionEnum.PerformerInRetail)
                                                    ||
                                                    MainControl.ActivityInfo.PromoGroupCascadeEdit
                                                    && MainControl.ActivityInfo.PromoTTAvailable > 0
                                                    ||
                                                    MainControl.ActivityInfo.ControlGroupCascadeEdit
                                                    && MainControl.ActivityInfo.ControlTTAvailable > 0);
        }

        private int CurAimsCount() {
            int lCount = 0;
            foreach (DataTransferObjectAim aim in _aims)
                if (aim.State != AimRecordState.Deleted && aim.State != AimRecordState.Unknown)
                    lCount++;

            return lCount;
        }

        public void AllowEdit() {
            if (_aimCur.State != AimRecordState.Unknown) {
                switch ((GoalType) _aimCur.Type) {
                    case GoalType.SalesVolume:
                        skuTreeCtrl.ReadOnly = false;
                        break;

                    case GoalType.Merchandising:
                        treeKPI.ReadOnly = false;
                        break;

                    case GoalType.Effectiveness:
                        chlbIPTRList.Enabled = true;
                        break;

                    case GoalType.QuestionnaireLogic:
                        qpqTreeLogic.ReadOnly = false;
                        break;

                    case GoalType.QuestionnaireDigit:
                        qpqTreeDigit.ReadOnly = false;
                        break;

                    case GoalType.InBevDoorCount:
                        break;

                    case GoalType.MustStock:
                        skuTreeCtrl.ReadOnly = false;
                        break;
                }
            }

            labelEdit.Visible = true;
            textCurAimName.Visible = true;
            RedrawAimControls((GoalType) _aimCur.Type, true);
            RedrawButtonAccess();
        }

        /// <summary>
        /// Determines whether the aim is valid
        /// </summary>
        /// <param name="lMsg">error message</param>
        /// <returns>whether the aim is valid</returns>
        internal bool IsAimCurValid(out string lMsg) {
            bool lIsValid = true;
            lMsg = string.Empty;

            if (_aimCur.Name.Trim() == string.Empty) {
                lIsValid = false;
                lMsg = "Нельзя сохранить цель без названия";
            }

            if (_aims.FindIndex(aim => aim.Name.Trim() == _aimCur.Name.Trim() && aim.State != AimRecordState.Deleted) >= 0
                && _aimCur.State == AimRecordState.New) {
                lIsValid = false;
                lMsg = "Цель с заданным названием уже существует";
            }

            if (_aimCur.Name.Length >= 100) {
                lIsValid = false;
                lMsg = "Слишком длинное название цели";
            }

            if (_aimCur.SelectedIdList == string.Empty &&
                _aimCur.Type != ConvertEx.ToInt(GoalType.OnInvoice) &&
                (GoalType)_aimCur.Type != GoalType.InBevDoorCount)
            {
                lIsValid = false;
                lMsg = "Нельзя сохранить пустую цель";
            }

            lMsg = string.Format("\t\t\t-{0}", lMsg);

            return lIsValid;
        }

        #endregion Event handling

        #region Data loading

        /// <summary>
        /// Sets the template data.
        /// </summary>
        /// <param name="dtTemplateDetails">The dt template details.</param>
        private void SetTemplateData(DataTable dtTemplateDetails) {
            if (dtTemplateDetails == null || dtTemplateDetails.Rows.Count <= 0)
                return;
            
            DataRow row = dtTemplateDetails.Rows[0];
            checkCannotChangeSKU.Checked = ConvertEx.ToBool(row[DETAILS_DISABLE_CHANGE_SKU]);
            checkCannotChangeKPI.Checked = ConvertEx.ToBool(row[DETAILS_DISABLE_CHANGE_KPI]);
            checkSplitByPOCType.Checked = ConvertEx.ToBool(row[DETAILS_DIV_TARGET_BY_OL_TYPE]);

            // new
            checkAllowMultipleGoals.Checked = ConvertEx.ToBool(row[DETAILS_ENABLE_MORE_ONE_AIM]);

            //obsolete
            //cbSplitByTime.EditValue = ConvertEx.ToInt(lRow[Constants.FIELD_GOALS_TARGET_PERIOD_TYPE]);
            //cbSplitByPOCType.EditValue = ConvertEx.ToInt(lRow[Constants.FIELD_GOALS_TARGET_REGION_TYPE]);
            //cbSplitByTime.Properties.Items.Clear();
            //cbSplitByPOCType.Properties.Items.Clear();
            //cbSplitByTime.Properties.Items.Add(new ComboBoxItem(ConvertEx.ToInt(row[DETAILS_TARGET_PERIOD_TYPE]), ConvertEx.ToString(row[DETAILS_TARGET_PERIOD_NAME])));
            //cbSplitByPOCType.Properties.Items.Add(new ComboBoxItem(ConvertEx.ToInt(row[DETAILS_TARGET_REGION_TYPE]), ConvertEx.ToString(row[DETAILS_TARGET_REGION_NAME])));
            //cbSplitByPOCType.SelectedIndex = cbSplitByTime.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets the activity details data.
        /// </summary>
        /// <param name="dtActivityDetails">The dt activity details.</param>
        private void SetActivityDetailsData(DataTable dtActivityDetails) {
            if (dtActivityDetails == null || dtActivityDetails.Rows.Count <= 0)
                return;
            
            DataRow row = dtActivityDetails.Rows[0];
            checkCannotChangeSKU.Checked = ConvertEx.ToBool(row[DETAILS_DISABLE_CHANGE_SKU]);
            checkCannotChangeKPI.Checked = ConvertEx.ToBool(row[DETAILS_DISABLE_CHANGE_KPI]);
            checkSplitByPOCType.Checked = ConvertEx.ToBool(row[DETAILS_DIV_TARGET_BY_OL_TYPE]);

            // new
            checkAllowMultipleGoals.Checked = ConvertEx.ToBool(row[DETAILS_ENABLE_MORE_ONE_AIM]);

            //obsolete
            //cbSplitByTime.Properties.Items.Clear();
            //cbSplitByPOCType.Properties.Items.Clear();
            //cbSplitByTime.Properties.Items.Add(new ComboBoxItem(ConvertEx.ToInt(row[DETAILS_TARGET_PERIOD_TYPE]), ConvertEx.ToString(row[DETAILS_TARGET_PERIOD_NAME])));
            //cbSplitByPOCType.Properties.Items.Add(new ComboBoxItem(ConvertEx.ToInt(row[DETAILS_TARGET_REGION_TYPE]), ConvertEx.ToString(row[DETAILS_TARGET_REGION_NAME])));
            //cbSplitByPOCType.SelectedIndex = cbSplitByTime.SelectedIndex = 0;

            MechanicPricaType = ConvertEx.ToInt(row[DETAILS_ACTION_MECHANIC_PRICE_TYPE]);
        }

        /// <summary>
        /// Loads the drop downs.
        /// </summary>
        private void LoadDropDowns() {
            //cbSplitByTime.Properties.DataSource = DataProvider.SplitTargetsByTimeList;
            //cbSplitByTime.EditValue = (int)SplitPOCByTime.Monthly;
            //cbSplitByPOCType.Properties.DataSource = DataProvider.SplitTargetsByPocType;
            //cbSplitByPOCType.EditValue = (int)SplitPOCByType.ByM3;
            cbGoalType.EditValueChanged -= cbGoalType_EditValueChanged;
            cbGoalType.Properties.DataSource = DataProvider.GoalTypesList;
            cbGoalType.EditValue = (int) GoalType.SalesVolume;
            cbGoalType.EditValueChanged += cbGoalType_EditValueChanged;

            List<ComboBoxItem> l = new List<ComboBoxItem>();
            l.AddRange(new[] {
                new ComboBoxItem((int) CheckByVisitsType.ByLastVisit, "по последнему визиту"),
                new ComboBoxItem((int) CheckByVisitsType.ByMonth, "за месяц")
            });
            cbVisitChecked.Properties.DataSource = l;
            cbVisitChecked.EditValue = (int) CheckByVisitsType.ByMonth;

            lFactTypeSource = new List<ComboBoxItem>();
            lFactTypeSource.AddRange(new[] { new ComboBoxItem((int)FactCalcType.FactSales, "Факт из продаж"), 
                new ComboBoxItem((int)FactCalcType.FactRemainder, "Факт по остаткам в ТТ") });
            lcpFactsCalcWay.Properties.DataSource = lFactTypeSource;

            lFactCalcMonth = new List<ComboBoxItem>();
            for (int i = 1; i < 13; i++)
            {
                lFactCalcMonth.Add(new ComboBoxItem(i, i.ToString()));
            }
            lcpPeriodCalk.Properties.DataSource = lFactCalcMonth;
        }

        /// <summary>
        /// Reads info from DB and Loads it into Aims liast
        /// </summary>
        private void LoadAllAimsFromDB(int activityId) {
            DataTable dt = DataProvider.GetAimList(activityId);

            DataTransferObjectAim aim = new DataTransferObjectAim();

            foreach (DataRow dr in dt.Rows) {
                aim = new DataTransferObjectAim();

                aim.Id = ConvertEx.ToInt(dr["Aim_Id"]);
                aim.Name = ConvertEx.ToString(dr["Aim_Name"]);
                aim.Type = ConvertEx.ToInt(dr["AimType_Id"]);

                aim.IsUsedInCalculations = ConvertEx.ToBool(dr["isUseInCompleted"]);
                aim.IsPeriodCalc = ConvertEx.ToBool(dr["isPeriodCalcAim"]);
                aim.IsInReverseExecution = ConvertEx.ToBool(dr["IsInReverseExec"]);

                aim.FactCalcWay = ConvertEx.ToInt(dr["FactCalcType"]);
                aim.FactCalcPeriod = ConvertEx.ToInt(dr["FactCalcPeriod"]);
                aim.IsUseAimValue = ConvertEx.ToBool(dr["isUseAimValue"]);
                aim.IsAccPerfOfTargetVal = ConvertEx.ToBool(dr["isAccPerfOfTargetVal"]);

                aim.State = AimRecordState.Normal;
                aim.SelectedIdList = LoadAimSelectedId(aim, null);

                _aims.Add(aim);
            }
        }

        /// <summary>
        /// Loads not empty aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        private void LoadAim(DataTransferObjectAim aim, bool visible) {
            _aimOriginal = aim;
            _aimCur = new DataTransferObjectAim(_aimOriginal);
            DataTable dt = null;

            switch ((GoalType) _aimCur.Type) {
                case GoalType.SalesVolume:
                    ClearTable(_skuTreeTable);
                    dt = _skuTreeTable;
                    foreach (string Id in _aimCur.SelectedIdList.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries)) {
                        foreach (DataRow dr in dt.Rows) {
                            if (ConvertEx.ToString(dr["Id"]) == Id) {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }
                    skuTreeCtrl.Load(_skuTreeTable);
                    break;

                case GoalType.Merchandising:
                    ClearTable(_kpiTreeTable);
                    dt = _kpiTreeTable;
                    foreach (string Id in _aimCur.SelectedIdList.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries)) {
                        foreach (DataRow dr in dt.Rows) {
                            if (ConvertEx.ToString(dr["ID"]) == Id) {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }

                    treeKPI.LoadKPITree(_kpiTreeTable);
                    break;

                case GoalType.Effectiveness:
                    chlbIPTRList.UnCheckAll();
                    foreach (string Id in _aimCur.SelectedIdList.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries))
                        LoadIPTRList(Id);
                    break;

                    case GoalType.QuestionnaireLogic:
                    ClearTable(_qpqLogicTreeTable);
                    dt = _qpqLogicTreeTable;

                    foreach (
                        string Id in
                            _aimCur.SelectedIdList.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }

                    }
                    qpqTreeLogic.LoadQPQTree(_qpqLogicTreeTable);
                    if (checkShowAll.CheckState == CheckState.Checked && isLogicTreeFirstDraw)
                    {
                        isLogicTreeFirstDraw = false;
                        ClearTable(_qpqLogicTreeTable);
                        dt = _qpqLogicTreeTable;

                        foreach (
                            string Id in
                                _aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                                {
                                    dr["Checked"] = true;
                                    break;
                                }
                            }

                        }
                        qpqTreeLogic.LoadQPQTree(_qpqLogicTreeTable);
                    }
                    break;

                case GoalType.QuestionnaireDigit:
                    ClearTable(_qpqDigitTreeTable);
                    dt = _qpqDigitTreeTable;

                    foreach (
                        string Id in
                            _aimCur.SelectedIdList.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }
                    qpqTreeDigit.LoadQPQTree(_qpqDigitTreeTable);
                    if (checkShowAll.CheckState == CheckState.Checked && isDigitTreeFirstDraw)
                    {
                        isDigitTreeFirstDraw = false;
                        ClearTable(_qpqDigitTreeTable);
                        dt = _qpqDigitTreeTable;

                        foreach (
                            string Id in
                                _aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                                {
                                    dr["Checked"] = true;
                                    break;
                                }
                            }
                        }
                        qpqTreeDigit.LoadQPQTree(_qpqDigitTreeTable);
                    }
                    break;

                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:

                    ClearTable(_skuTreeTable);
                    dt = _skuTreeTable;

                    foreach (string Id in _aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToString(dr["Id"]) == Id)
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }

                    skuTreeCtrl.Load(_skuTreeTable);
                    break;
            }
            // end parse Id list
            textCurAimName.Text = string.Copy(_aimCur.Name);
            RedrawTreeWindow((GoalType) _aimCur.Type, visible);
            RedrawAimControls((GoalType) _aimCur.Type, false);
        }

        /// <summary>
        /// Loads empty aim
        /// </summary>
        /// <param name="aimId">aim id</param>
        /// <param name="aimType">aim type</param>
        private void LoadAim(AimRecordState aimState, int aimType, bool visible) {
            _aimOriginal = new DataTransferObjectAim();
            _aimOriginal.State = aimState;
            _aimOriginal.Type = aimType;
            _aimCur = new DataTransferObjectAim(_aimOriginal);
            switch ((GoalType) aimType) {
                case GoalType.SalesVolume:
                    ClearTable(_skuTreeTable);
                    skuTreeCtrl.Load(_skuTreeTable);
                    break;

                case GoalType.Merchandising:
                    ClearTable(_kpiTreeTable);
                    treeKPI.LoadKPITree(_kpiTreeTable);
                    break;

                case GoalType.Effectiveness:
                    chlbIPTRList.UnCheckAll();
                    break;

                case GoalType.QuestionnaireLogic:
                    ClearTable(_qpqLogicTreeTable);
                    qpqTreeLogic.LoadQPQTree(_qpqLogicTreeTable);
                    break;

                case GoalType.QuestionnaireDigit:
                    ClearTable(_qpqDigitTreeTable);
                    qpqTreeDigit.LoadQPQTree(_qpqDigitTreeTable);
                    break;

                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:
                    ClearTable(_skuTreeTable);
                    skuTreeCtrl.Load(_skuTreeTable);
                    break;
            }

            textCurAimName.Text = string.Copy(_aimCur.Name);
            RedrawTreeWindow((GoalType) _aimCur.Type, visible);
            RedrawAimControls((GoalType) _aimCur.Type, visible);
        }

        /// <summary>
        /// Builds Ids list
        /// </summary>
        /// <param name="aim">aim for which the list is builded</param>
        /// <param name="source">source table</param>
        /// <returns>Ids list</returns>
        private string LoadAimSelectedId(DataTransferObjectAim aim, DataTable source) {
            DataTable dt = null;
            string s = string.Empty;

            switch ((GoalType) aim.Type)
                // parse Id list
            {
                case GoalType.SalesVolume:
                    dt = source == null ? DataProvider.GetSKUTree(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows) {
                        if (ConvertEx.ToBool(dr["Checked"])) {
                            s += "," + ConvertEx.ToString(dr["Id"]);
                        }
                    }

                    break;

                case GoalType.Merchandising:
                    dt = source == null ? DataProvider.GetKPITree(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows) {
                        if (ConvertEx.ToBool(dr["Checked"])) {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }

                    break;

                case GoalType.Effectiveness:
                    dt = source == null ? DataProvider.GetIPTRAim(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows) {
                        s += "," + ConvertEx.ToString(dr["EquipmentClass_ID"]);
                    }

                    break;

                case GoalType.QuestionnaireLogic:
                    dt = source == null ? DataProvider.GetQPQTree(aim.Id, 1) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }

                    break;

                case GoalType.QuestionnaireDigit:
                    dt = source == null ? DataProvider.GetQPQTree(aim.Id, 2) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }

                    break;
                case GoalType.InBevDoorCount:
                    break;
                case GoalType.MustStock:
                    dt = source == null ? DataProvider.GetMustStockTree(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }
                    break;
            }

            s = s.TrimStart(',');
            // end parse Id list

            return s;
        }

        /// <summary>
        /// Clears trees' tables Checked column
        /// </summary>
        /// <param name="dt">table to clear</param>
        private void ClearTable(DataTable dt) {
            foreach (DataRow dr in dt.Rows) {
                dr["Checked"] = false;
            }
        }

        /// <summary>
        /// Gets Id in the list of aim by aim Id
        /// </summary>
        /// <param name="value">aim Id</param>
        /// <returns>list Id</returns>
        private int GetIdByValue(int value) {
            int index = -1, i = 0;

            foreach (ComboBoxItem item in _aimList) {
                if (item.Id == value) {
                    index = i;
                    break;
                }
                else {
                    i++;
                }
            }

            return index;
        }

        #endregion Data loading

        #region Data saving

        /// <summary>
        /// Gets current aim values
        /// </summary>
        private void GetAllAimFields() {
            //aimCur.AimId
            _aimCur.Name = string.Copy(textCurAimName.Text);
            //aimCur.AimType
            _aimCur.IsUsedInCalculations = checkUseInCalculations.Checked;
            _aimCur.IsPeriodCalc = ConvertEx.ToBool(cbVisitChecked.EditValue);

            switch ((GoalType) ConvertEx.ToInt(_aimCur.Type)) {
                case GoalType.SalesVolume:
                    _aimCur.SelectedXmlList = skuTreeCtrl.SelectedXMLIdList;
                    _aimCur.SelectedIdList = LoadAimSelectedId(_aimCur, skuTreeCtrl.DataSourcePosted);
                    break;
                case GoalType.Merchandising:
                    _aimCur.SelectedXmlList = treeKPI.SelectedXmlIdList;
                    _aimCur.SelectedIdList = LoadAimSelectedId(_aimCur, treeKPI.DataSourcePosted);
                    _aimCur.IsInReverseExecution = checkReversExecution.Checked;
                    _aimCur.IsUseAimValue = checkUseAimValue.Checked;
                    _aimCur.IsAccPerfOfTargetVal = checkAccPerfOfTargetVal.Checked;
                    break;
                case GoalType.Effectiveness:
                    _aimCur.SelectedIdList = SelectedIPTRList;
                    break;
                case GoalType.OnInvoice:
                    break;
                case GoalType.QuestionnaireLogic:
                    _aimCur.SelectedXmlList = qpqTreeLogic.SelectedXmlIdList;
                    _aimCur.SelectedIdList = LoadAimSelectedId(_aimCur, qpqTreeLogic.DataSourcePosted);
                    break;

                case GoalType.QuestionnaireDigit:
                    _aimCur.SelectedXmlList = qpqTreeDigit.SelectedXmlIdList;
                    _aimCur.SelectedIdList = LoadAimSelectedId(_aimCur, qpqTreeDigit.DataSourcePosted);
                    _aimCur.IsInReverseExecution = checkReversExecution.Checked;
                    break;
                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:
                    _aimCur.SelectedXmlList = skuTreeCtrl.SelectedXMLIdList;
                    _aimCur.SelectedIdList = LoadAimSelectedId(_aimCur, skuTreeCtrl.DataSourcePosted);
                    _aimCur.FactCalcPeriod = Convert.ToInt32(lcpPeriodCalk.EditValue);
                    _aimCur.FactCalcWay = Convert.ToInt32(lcpFactsCalcWay.EditValue);
                    break;
            }
        }

        /// <summary>
        /// Saves aim
        /// </summary>
        private void SaveAimToList(DataTransferObjectAim aim) {
            _aimOriginal.Copy(aim);
            if (aim.State == AimRecordState.New) {
                _aims.Add(aim);
                aim.State = AimRecordState.Updated;
            }
            else
                _aimOriginal.State = AimRecordState.Updated;
        }

        /// <summary>
        /// Saves aim
        /// </summary>
        private void SaveAimToDB(DataTransferObjectAim aim, int templateId) {
            aim.Id = DataProvider.UpdateAim(templateId, aim.Name, aim.Type, aim.IsUsedInCalculations, aim.IsPeriodCalc,
                aim.Id, aim.FactCalcPeriod, aim.FactCalcWay, aim.IsUseAimValue, aim.IsInReverseExecution, aim.IsAccPerfOfTargetVal);

            switch ((GoalType) aim.Type) {
                case GoalType.SalesVolume:
                    aim.Id = DataProvider.UpdateSKUAim(aim.Id, aim.SelectedXmlList);
                    break;
                case GoalType.Merchandising:
                    aim.Id = DataProvider.UpdateKPIAim(aim.Id, aim.SelectedXmlList);
                    break;
                case GoalType.Effectiveness:
                    aim.Id = DataProvider.UpdateIPTRAim(aim.Id, ConvertEx.ToInt(aim.SelectedIdList));
                    break;
                case GoalType.QuestionnaireLogic:
                    aim.Id = DataProvider.UpdateQPQAim(aim.Id, 1, aim.SelectedIdList);
                    break;
                case GoalType.QuestionnaireDigit:
                    aim.Id = DataProvider.UpdateQPQAim(aim.Id, 2, aim.IsInReverseExecution, aim.SelectedIdList);
                    break;
                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:
                    aim.Id = DataProvider.UpdateSKUAim(aim.Id, aim.SelectedXmlList);
                    break;
            }

            aim.State = AimRecordState.Normal;
        }

        private void GatherActivityInfo(ActivityDetailsDTO dto) {
            if (!MainControl.ActivityInfo.IsPriceActivity)
                return;

            if (!MainControl.ActivityInfo.IsApproved) {
                dto.DeletedMechanics = _deletedMechanics;

                DataTable actions = MPAGridControl.DataSource as DataTable;
                if (null != actions) {
                    foreach (DataRow row in actions.Rows) {
                        MechanicInfo mi = GetMechanicInfo(row);
                        if (null != mi)
                            dto.MechanicsInfo.Add(mi);
                    }
                }
            }
            else {
                DataTable gridDataSource = ((DataView) MPAGridControl.DefaultView.DataSource).Table;
                if (null != gridDataSource)
                    foreach (DataRow dr in gridDataSource.Rows) {
                        dr.AcceptChanges();
                        dto.MechanicsInfo.Add(GetMechanicInfo(dr));
                    }
            }
            dto.MechanicsPriceType = rbMPAType.SelectedIndex;
        }

        public void Save(ActivityDetailsDTO activityDetails) {
            SaveAllAims(activityDetails.ActivityId);

            if (MainControl.ActivityInfo.IsPriceActivity)
                SavePriceActivityInfo(activityDetails);
        }

        #endregion Data saving
    }
}