﻿namespace SoftServe.Reports.MarketProgramms.UserControls
{
    partial class ActivityDetailsCommon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActivityDetailsCommon));
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.UpliftResultLabel = new DevExpress.XtraEditors.LabelControl();
            this.UpliftCalculationBtn = new DevExpress.XtraEditors.SimpleButton();
            this.lblActivityNotInBudget = new DevExpress.XtraEditors.LabelControl();
            this.groupControlPromoCreation = new DevExpress.XtraEditors.GroupControl();
            this.checkEditAllTTInActivePromo = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditAllowAddAllPromo = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditCascadeEditPromo = new DevExpress.XtraEditors.CheckEdit();
            this.groupControlPeriods = new DevExpress.XtraEditors.GroupControl();
            this.datePeriod2YearTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod2MonthTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod2DayTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod2YearFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod2MonthFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod2DayFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod1YearTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod1MonthTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod1DayTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod1YearFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod1MonthFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriod1DayFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriodYearTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriodMonthTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriodDayTo = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriodYearFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriodMonthFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.datePeriodDayFrom = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.checkEditTakeBasePeriodsFromActiveWave = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditMonthsToTrackUpliftAfterEnd = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.groupControlKPK = new DevExpress.XtraEditors.GroupControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditKPKDescriptionTemplate = new DevExpress.XtraEditors.MemoEdit();
            this.checkEditWithConnectionDate = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditShowInKPK = new DevExpress.XtraEditors.CheckEdit();
            this.groupControlControlGroup = new DevExpress.XtraEditors.GroupControl();
            this.checkEditAllTTInActive = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditAllowAddAll = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditCascadeEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelCreator = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsApproved = new DevExpress.XtraEditors.CheckEdit();
            this.groupControlPositions = new DevExpress.XtraEditors.GroupControl();
            this.comboBoxEditFactController = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditPerformerInRetail = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditActivityApprover = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditControlGroupSelector = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditGoalCreator = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditInitiator = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupControlActivity = new DevExpress.XtraEditors.GroupControl();
            this.lblTemplateName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.documentUploader = new Logica.Reports.BaseReportControl.CommonControls.FileUploader();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.textControlChannel = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxActivityLevel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.editControlActivityType = new DevExpress.XtraEditors.TextEdit();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPromoCreation)).BeginInit();
            this.groupControlPromoCreation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllTTInActivePromo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllowAddAllPromo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCascadeEditPromo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPeriods)).BeginInit();
            this.groupControlPeriods.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTakeBasePeriodsFromActiveWave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMonthsToTrackUpliftAfterEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlKPK)).BeginInit();
            this.groupControlKPK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditKPKDescriptionTemplate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWithConnectionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowInKPK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlControlGroup)).BeginInit();
            this.groupControlControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllTTInActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllowAddAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCascadeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPositions)).BeginInit();
            this.groupControlPositions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditFactController.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPerformerInRetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditActivityApprover.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditControlGroupSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoalCreator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditInitiator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlActivity)).BeginInit();
            this.groupControlActivity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTemplateName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textControlChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxActivityLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editControlActivityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.groupControl1);
            this.xtraScrollableControl1.Controls.Add(this.lblActivityNotInBudget);
            this.xtraScrollableControl1.Controls.Add(this.groupControlPromoCreation);
            this.xtraScrollableControl1.Controls.Add(this.groupControlPeriods);
            this.xtraScrollableControl1.Controls.Add(this.groupControlKPK);
            this.xtraScrollableControl1.Controls.Add(this.groupControlControlGroup);
            this.xtraScrollableControl1.Controls.Add(this.labelCreator);
            this.xtraScrollableControl1.Controls.Add(this.checkEditIsApproved);
            this.xtraScrollableControl1.Controls.Add(this.groupControlPositions);
            this.xtraScrollableControl1.Controls.Add(this.groupControlActivity);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1008, 681);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // UpliftResultLabel
            // 
            this.UpliftResultLabel.Location = new System.Drawing.Point(126, 39);
            this.UpliftResultLabel.Name = "UpliftResultLabel";
            this.UpliftResultLabel.Size = new System.Drawing.Size(33, 13);
            this.UpliftResultLabel.TabIndex = 22;
            this.UpliftResultLabel.Text = "0,00%";
            // 
            // UpliftCalculationBtn
            // 
            this.UpliftCalculationBtn.Location = new System.Drawing.Point(7, 33);
            this.UpliftCalculationBtn.Name = "UpliftCalculationBtn";
            this.UpliftCalculationBtn.Size = new System.Drawing.Size(113, 23);
            this.UpliftCalculationBtn.TabIndex = 21;
            this.UpliftCalculationBtn.Text = "Расчет аплифта";
            this.UpliftCalculationBtn.Click += new System.EventHandler(this.UpliftCalculationBtn_Click);
            // 
            // lblActivityNotInBudget
            // 
            this.lblActivityNotInBudget.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblActivityNotInBudget.Appearance.Options.UseForeColor = true;
            this.lblActivityNotInBudget.Location = new System.Drawing.Point(703, 11);
            this.lblActivityNotInBudget.Name = "lblActivityNotInBudget";
            this.lblActivityNotInBudget.Size = new System.Drawing.Size(285, 13);
            this.lblActivityNotInBudget.TabIndex = 16;
            this.lblActivityNotInBudget.Text = "Активность не утверждена в бюджет и превышает его";
            // 
            // groupControlPromoCreation
            // 
            this.groupControlPromoCreation.Controls.Add(this.checkEditAllTTInActivePromo);
            this.groupControlPromoCreation.Controls.Add(this.checkEditAllowAddAllPromo);
            this.groupControlPromoCreation.Controls.Add(this.checkEditCascadeEditPromo);
            this.groupControlPromoCreation.Location = new System.Drawing.Point(628, 282);
            this.groupControlPromoCreation.Name = "groupControlPromoCreation";
            this.groupControlPromoCreation.Size = new System.Drawing.Size(373, 109);
            this.groupControlPromoCreation.TabIndex = 14;
            this.groupControlPromoCreation.Text = "Формирование промо группы ТТ";
            // 
            // checkEditAllTTInActivePromo
            // 
            this.checkEditAllTTInActivePromo.Enabled = false;
            this.checkEditAllTTInActivePromo.Location = new System.Drawing.Point(5, 75);
            this.checkEditAllTTInActivePromo.Name = "checkEditAllTTInActivePromo";
            this.checkEditAllTTInActivePromo.Properties.Caption = "Все ТТ из адрески должны быть в активной волне Th.R";
            this.checkEditAllTTInActivePromo.Size = new System.Drawing.Size(361, 19);
            this.checkEditAllTTInActivePromo.TabIndex = 2;
            // 
            // checkEditAllowAddAllPromo
            // 
            this.checkEditAllowAddAllPromo.Enabled = false;
            this.checkEditAllowAddAllPromo.Location = new System.Drawing.Point(5, 50);
            this.checkEditAllowAddAllPromo.Name = "checkEditAllowAddAllPromo";
            this.checkEditAllowAddAllPromo.Properties.Caption = "Разрешено вносить ТТ в адреску не из фильтра Th.R";
            this.checkEditAllowAddAllPromo.Size = new System.Drawing.Size(361, 19);
            this.checkEditAllowAddAllPromo.TabIndex = 1;
            // 
            // checkEditCascadeEditPromo
            // 
            this.checkEditCascadeEditPromo.Enabled = false;
            this.checkEditCascadeEditPromo.Location = new System.Drawing.Point(5, 25);
            this.checkEditCascadeEditPromo.Name = "checkEditCascadeEditPromo";
            this.checkEditCascadeEditPromo.Properties.Caption = "Каскадное редактирование адрески";
            this.checkEditCascadeEditPromo.Size = new System.Drawing.Size(361, 19);
            this.checkEditCascadeEditPromo.TabIndex = 0;
            // 
            // groupControlPeriods
            // 
            this.groupControlPeriods.Controls.Add(this.datePeriod2YearTo);
            this.groupControlPeriods.Controls.Add(this.datePeriod2MonthTo);
            this.groupControlPeriods.Controls.Add(this.datePeriod2DayTo);
            this.groupControlPeriods.Controls.Add(this.datePeriod2YearFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriod2MonthFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriod2DayFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriod1YearTo);
            this.groupControlPeriods.Controls.Add(this.datePeriod1MonthTo);
            this.groupControlPeriods.Controls.Add(this.datePeriod1DayTo);
            this.groupControlPeriods.Controls.Add(this.datePeriod1YearFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriod1MonthFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriod1DayFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriodYearTo);
            this.groupControlPeriods.Controls.Add(this.datePeriodMonthTo);
            this.groupControlPeriods.Controls.Add(this.datePeriodDayTo);
            this.groupControlPeriods.Controls.Add(this.datePeriodYearFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriodMonthFrom);
            this.groupControlPeriods.Controls.Add(this.datePeriodDayFrom);
            this.groupControlPeriods.Controls.Add(this.checkEditTakeBasePeriodsFromActiveWave);
            this.groupControlPeriods.Controls.Add(this.spinEditMonthsToTrackUpliftAfterEnd);
            this.groupControlPeriods.Controls.Add(this.labelControl23);
            this.groupControlPeriods.Controls.Add(this.labelControl22);
            this.groupControlPeriods.Controls.Add(this.labelControl21);
            this.groupControlPeriods.Controls.Add(this.labelControl20);
            this.groupControlPeriods.Controls.Add(this.labelControl19);
            this.groupControlPeriods.Controls.Add(this.labelControl18);
            this.groupControlPeriods.Controls.Add(this.labelControl16);
            this.groupControlPeriods.Controls.Add(this.labelControl15);
            this.groupControlPeriods.Controls.Add(this.labelControl14);
            this.groupControlPeriods.Controls.Add(this.labelControl13);
            this.groupControlPeriods.Location = new System.Drawing.Point(460, 30);
            this.groupControlPeriods.Name = "groupControlPeriods";
            this.groupControlPeriods.Size = new System.Drawing.Size(541, 246);
            this.groupControlPeriods.TabIndex = 13;
            this.groupControlPeriods.Text = "Периоды";
            // 
            // datePeriod2YearTo
            // 
            this.datePeriod2YearTo.Location = new System.Drawing.Point(474, 204);
            this.datePeriod2YearTo.Name = "datePeriod2YearTo";
            this.datePeriod2YearTo.Size = new System.Drawing.Size(60, 22);
            this.datePeriod2YearTo.TabIndex = 36;
            this.datePeriod2YearTo.Value = 0;
            this.datePeriod2YearTo.ValueObj = 0;
            this.datePeriod2YearTo.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod2MonthTo
            // 
            this.datePeriod2MonthTo.Location = new System.Drawing.Point(367, 204);
            this.datePeriod2MonthTo.Name = "datePeriod2MonthTo";
            this.datePeriod2MonthTo.Size = new System.Drawing.Size(101, 22);
            this.datePeriod2MonthTo.TabIndex = 35;
            this.datePeriod2MonthTo.Value = 0;
            this.datePeriod2MonthTo.ValueObj = 0;
            this.datePeriod2MonthTo.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod2DayTo
            // 
            this.datePeriod2DayTo.Location = new System.Drawing.Point(311, 204);
            this.datePeriod2DayTo.Name = "datePeriod2DayTo";
            this.datePeriod2DayTo.Size = new System.Drawing.Size(50, 22);
            this.datePeriod2DayTo.TabIndex = 34;
            this.datePeriod2DayTo.Value = 0;
            this.datePeriod2DayTo.ValueObj = 0;
            // 
            // datePeriod2YearFrom
            // 
            this.datePeriod2YearFrom.Location = new System.Drawing.Point(223, 204);
            this.datePeriod2YearFrom.Name = "datePeriod2YearFrom";
            this.datePeriod2YearFrom.Size = new System.Drawing.Size(60, 22);
            this.datePeriod2YearFrom.TabIndex = 33;
            this.datePeriod2YearFrom.Value = 0;
            this.datePeriod2YearFrom.ValueObj = 0;
            this.datePeriod2YearFrom.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod2MonthFrom
            // 
            this.datePeriod2MonthFrom.Location = new System.Drawing.Point(116, 204);
            this.datePeriod2MonthFrom.Name = "datePeriod2MonthFrom";
            this.datePeriod2MonthFrom.Size = new System.Drawing.Size(101, 22);
            this.datePeriod2MonthFrom.TabIndex = 32;
            this.datePeriod2MonthFrom.Value = 0;
            this.datePeriod2MonthFrom.ValueObj = 0;
            this.datePeriod2MonthFrom.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod2DayFrom
            // 
            this.datePeriod2DayFrom.Location = new System.Drawing.Point(60, 204);
            this.datePeriod2DayFrom.Name = "datePeriod2DayFrom";
            this.datePeriod2DayFrom.Size = new System.Drawing.Size(50, 22);
            this.datePeriod2DayFrom.TabIndex = 31;
            this.datePeriod2DayFrom.Value = 0;
            this.datePeriod2DayFrom.ValueObj = 0;
            // 
            // datePeriod1YearTo
            // 
            this.datePeriod1YearTo.Location = new System.Drawing.Point(474, 155);
            this.datePeriod1YearTo.Name = "datePeriod1YearTo";
            this.datePeriod1YearTo.Size = new System.Drawing.Size(60, 22);
            this.datePeriod1YearTo.TabIndex = 30;
            this.datePeriod1YearTo.Value = 0;
            this.datePeriod1YearTo.ValueObj = 0;
            this.datePeriod1YearTo.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod1MonthTo
            // 
            this.datePeriod1MonthTo.Location = new System.Drawing.Point(367, 155);
            this.datePeriod1MonthTo.Name = "datePeriod1MonthTo";
            this.datePeriod1MonthTo.Size = new System.Drawing.Size(101, 22);
            this.datePeriod1MonthTo.TabIndex = 29;
            this.datePeriod1MonthTo.Value = 0;
            this.datePeriod1MonthTo.ValueObj = 0;
            this.datePeriod1MonthTo.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod1DayTo
            // 
            this.datePeriod1DayTo.Location = new System.Drawing.Point(311, 155);
            this.datePeriod1DayTo.Name = "datePeriod1DayTo";
            this.datePeriod1DayTo.Size = new System.Drawing.Size(50, 22);
            this.datePeriod1DayTo.TabIndex = 28;
            this.datePeriod1DayTo.Value = 0;
            this.datePeriod1DayTo.ValueObj = 0;
            // 
            // datePeriod1YearFrom
            // 
            this.datePeriod1YearFrom.Location = new System.Drawing.Point(223, 155);
            this.datePeriod1YearFrom.Name = "datePeriod1YearFrom";
            this.datePeriod1YearFrom.Size = new System.Drawing.Size(60, 22);
            this.datePeriod1YearFrom.TabIndex = 27;
            this.datePeriod1YearFrom.Value = 0;
            this.datePeriod1YearFrom.ValueObj = 0;
            this.datePeriod1YearFrom.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod1MonthFrom
            // 
            this.datePeriod1MonthFrom.Location = new System.Drawing.Point(116, 155);
            this.datePeriod1MonthFrom.Name = "datePeriod1MonthFrom";
            this.datePeriod1MonthFrom.Size = new System.Drawing.Size(101, 22);
            this.datePeriod1MonthFrom.TabIndex = 26;
            this.datePeriod1MonthFrom.Value = 0;
            this.datePeriod1MonthFrom.ValueObj = 0;
            this.datePeriod1MonthFrom.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriod1DayFrom
            // 
            this.datePeriod1DayFrom.Location = new System.Drawing.Point(60, 155);
            this.datePeriod1DayFrom.Name = "datePeriod1DayFrom";
            this.datePeriod1DayFrom.Size = new System.Drawing.Size(50, 22);
            this.datePeriod1DayFrom.TabIndex = 25;
            this.datePeriod1DayFrom.Value = 0;
            this.datePeriod1DayFrom.ValueObj = 0;
            // 
            // datePeriodYearTo
            // 
            this.datePeriodYearTo.Location = new System.Drawing.Point(474, 53);
            this.datePeriodYearTo.Name = "datePeriodYearTo";
            this.datePeriodYearTo.Size = new System.Drawing.Size(60, 22);
            this.datePeriodYearTo.TabIndex = 24;
            this.datePeriodYearTo.Value = 0;
            this.datePeriodYearTo.ValueObj = 0;
            this.datePeriodYearTo.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriodMonthTo
            // 
            this.datePeriodMonthTo.Location = new System.Drawing.Point(367, 53);
            this.datePeriodMonthTo.Name = "datePeriodMonthTo";
            this.datePeriodMonthTo.Size = new System.Drawing.Size(101, 22);
            this.datePeriodMonthTo.TabIndex = 23;
            this.datePeriodMonthTo.Value = 0;
            this.datePeriodMonthTo.ValueObj = 0;
            this.datePeriodMonthTo.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriodDayTo
            // 
            this.datePeriodDayTo.Location = new System.Drawing.Point(311, 53);
            this.datePeriodDayTo.Name = "datePeriodDayTo";
            this.datePeriodDayTo.Size = new System.Drawing.Size(50, 22);
            this.datePeriodDayTo.TabIndex = 22;
            this.datePeriodDayTo.Value = 0;
            this.datePeriodDayTo.ValueObj = 0;
            // 
            // datePeriodYearFrom
            // 
            this.datePeriodYearFrom.Location = new System.Drawing.Point(223, 53);
            this.datePeriodYearFrom.Name = "datePeriodYearFrom";
            this.datePeriodYearFrom.Size = new System.Drawing.Size(60, 22);
            this.datePeriodYearFrom.TabIndex = 21;
            this.datePeriodYearFrom.Value = 0;
            this.datePeriodYearFrom.ValueObj = 0;
            this.datePeriodYearFrom.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriodMonthFrom
            // 
            this.datePeriodMonthFrom.Location = new System.Drawing.Point(116, 53);
            this.datePeriodMonthFrom.Name = "datePeriodMonthFrom";
            this.datePeriodMonthFrom.Size = new System.Drawing.Size(101, 22);
            this.datePeriodMonthFrom.TabIndex = 20;
            this.datePeriodMonthFrom.Value = 0;
            this.datePeriodMonthFrom.ValueObj = 0;
            this.datePeriodMonthFrom.EditValueChanged += new System.EventHandler(this.datePeriods_EditValueChanged);
            // 
            // datePeriodDayFrom
            // 
            this.datePeriodDayFrom.Location = new System.Drawing.Point(60, 53);
            this.datePeriodDayFrom.Name = "datePeriodDayFrom";
            this.datePeriodDayFrom.Size = new System.Drawing.Size(50, 22);
            this.datePeriodDayFrom.TabIndex = 19;
            this.datePeriodDayFrom.Value = 0;
            this.datePeriodDayFrom.ValueObj = 0;
            // 
            // checkEditTakeBasePeriodsFromActiveWave
            // 
            this.checkEditTakeBasePeriodsFromActiveWave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditTakeBasePeriodsFromActiveWave.Location = new System.Drawing.Point(37, 107);
            this.checkEditTakeBasePeriodsFromActiveWave.Name = "checkEditTakeBasePeriodsFromActiveWave";
            this.checkEditTakeBasePeriodsFromActiveWave.Properties.Caption = "Брать базовые периоды из активной волны Th.R";
            this.checkEditTakeBasePeriodsFromActiveWave.Size = new System.Drawing.Size(325, 19);
            this.checkEditTakeBasePeriodsFromActiveWave.TabIndex = 18;
            this.checkEditTakeBasePeriodsFromActiveWave.CheckedChanged += new System.EventHandler(this.checkEditTakeBasePeriodsFromActiveWave_CheckedChanged);
            // 
            // spinEditMonthsToTrackUpliftAfterEnd
            // 
            this.spinEditMonthsToTrackUpliftAfterEnd.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditMonthsToTrackUpliftAfterEnd.Location = new System.Drawing.Point(311, 84);
            this.spinEditMonthsToTrackUpliftAfterEnd.Name = "spinEditMonthsToTrackUpliftAfterEnd";
            this.spinEditMonthsToTrackUpliftAfterEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMonthsToTrackUpliftAfterEnd.Properties.DisplayFormat.FormatString = "N0";
            this.spinEditMonthsToTrackUpliftAfterEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditMonthsToTrackUpliftAfterEnd.Properties.IsFloatValue = false;
            this.spinEditMonthsToTrackUpliftAfterEnd.Properties.Mask.EditMask = "N0";
            this.spinEditMonthsToTrackUpliftAfterEnd.Properties.MaxValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.spinEditMonthsToTrackUpliftAfterEnd.Size = new System.Drawing.Size(50, 20);
            this.spinEditMonthsToTrackUpliftAfterEnd.TabIndex = 17;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(291, 160);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(12, 13);
            this.labelControl23.TabIndex = 11;
            this.labelControl23.Text = "по";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(49, 160);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(5, 13);
            this.labelControl22.TabIndex = 10;
            this.labelControl22.Text = "с";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(10, 185);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(122, 13);
            this.labelControl21.TabIndex = 9;
            this.labelControl21.Text = "Второй базовый период";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(291, 210);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(12, 13);
            this.labelControl20.TabIndex = 8;
            this.labelControl20.Text = "по";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(49, 210);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(5, 13);
            this.labelControl19.TabIndex = 7;
            this.labelControl19.Text = "с";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(10, 136);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(125, 13);
            this.labelControl18.TabIndex = 6;
            this.labelControl18.Text = "Первый базовый период";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(367, 88);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(17, 13);
            this.labelControl16.TabIndex = 4;
            this.labelControl16.Text = "мес";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(39, 88);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(264, 13);
            this.labelControl15.TabIndex = 3;
            this.labelControl15.Text = "Отслеживать прирост после окончания активности";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(291, 57);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(12, 13);
            this.labelControl14.TabIndex = 2;
            this.labelControl14.Text = "по";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(10, 34);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(108, 13);
            this.labelControl13.TabIndex = 1;
            this.labelControl13.Text = "Период активности с";
            // 
            // groupControlKPK
            // 
            this.groupControlKPK.Controls.Add(this.labelControl10);
            this.groupControlKPK.Controls.Add(this.memoEditKPKDescriptionTemplate);
            this.groupControlKPK.Controls.Add(this.checkEditWithConnectionDate);
            this.groupControlKPK.Controls.Add(this.checkEditShowInKPK);
            this.groupControlKPK.Location = new System.Drawing.Point(3, 422);
            this.groupControlKPK.Name = "groupControlKPK";
            this.groupControlKPK.Size = new System.Drawing.Size(454, 158);
            this.groupControlKPK.TabIndex = 12;
            this.groupControlKPK.Text = "КПК";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(5, 77);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(185, 13);
            this.labelControl10.TabIndex = 3;
            this.labelControl10.Text = "Шаблон описания активности в КПК";
            // 
            // memoEditKPKDescriptionTemplate
            // 
            this.memoEditKPKDescriptionTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditKPKDescriptionTemplate.Location = new System.Drawing.Point(5, 96);
            this.memoEditKPKDescriptionTemplate.Name = "memoEditKPKDescriptionTemplate";
            this.memoEditKPKDescriptionTemplate.Size = new System.Drawing.Size(444, 57);
            this.memoEditKPKDescriptionTemplate.TabIndex = 2;
            // 
            // checkEditWithConnectionDate
            // 
            this.checkEditWithConnectionDate.Location = new System.Drawing.Point(5, 50);
            this.checkEditWithConnectionDate.Name = "checkEditWithConnectionDate";
            this.checkEditWithConnectionDate.Properties.Caption = "С учетом даты подключения";
            this.checkEditWithConnectionDate.Size = new System.Drawing.Size(364, 19);
            this.checkEditWithConnectionDate.TabIndex = 1;
            // 
            // checkEditShowInKPK
            // 
            this.checkEditShowInKPK.Location = new System.Drawing.Point(5, 25);
            this.checkEditShowInKPK.Name = "checkEditShowInKPK";
            this.checkEditShowInKPK.Properties.Caption = "Отображать в КПК";
            this.checkEditShowInKPK.Size = new System.Drawing.Size(364, 19);
            this.checkEditShowInKPK.TabIndex = 0;
            // 
            // groupControlControlGroup
            // 
            this.groupControlControlGroup.Controls.Add(this.checkEditAllTTInActive);
            this.groupControlControlGroup.Controls.Add(this.checkEditAllowAddAll);
            this.groupControlControlGroup.Controls.Add(this.checkEditCascadeEdit);
            this.groupControlControlGroup.Location = new System.Drawing.Point(628, 397);
            this.groupControlControlGroup.Name = "groupControlControlGroup";
            this.groupControlControlGroup.Size = new System.Drawing.Size(373, 103);
            this.groupControlControlGroup.TabIndex = 15;
            this.groupControlControlGroup.Text = "Формирование контрольной группы ТТ";
            // 
            // checkEditAllTTInActive
            // 
            this.checkEditAllTTInActive.Enabled = false;
            this.checkEditAllTTInActive.Location = new System.Drawing.Point(5, 75);
            this.checkEditAllTTInActive.Name = "checkEditAllTTInActive";
            this.checkEditAllTTInActive.Properties.Caption = "Все ТТ из адрески должны быть в активной волне Th.R";
            this.checkEditAllTTInActive.Size = new System.Drawing.Size(361, 19);
            this.checkEditAllTTInActive.TabIndex = 2;
            // 
            // checkEditAllowAddAll
            // 
            this.checkEditAllowAddAll.Enabled = false;
            this.checkEditAllowAddAll.Location = new System.Drawing.Point(5, 50);
            this.checkEditAllowAddAll.Name = "checkEditAllowAddAll";
            this.checkEditAllowAddAll.Properties.Caption = "Разрешено вносить ТТ в адреску не из фильтра Th.R";
            this.checkEditAllowAddAll.Size = new System.Drawing.Size(361, 19);
            this.checkEditAllowAddAll.TabIndex = 1;
            // 
            // checkEditCascadeEdit
            // 
            this.checkEditCascadeEdit.Enabled = false;
            this.checkEditCascadeEdit.Location = new System.Drawing.Point(5, 25);
            this.checkEditCascadeEdit.Name = "checkEditCascadeEdit";
            this.checkEditCascadeEdit.Properties.Caption = "Каскадное редактирование адрески";
            this.checkEditCascadeEdit.Size = new System.Drawing.Size(361, 19);
            this.checkEditCascadeEdit.TabIndex = 0;
            // 
            // labelCreator
            // 
            this.labelCreator.Location = new System.Drawing.Point(3, 11);
            this.labelCreator.Name = "labelCreator";
            this.labelCreator.Size = new System.Drawing.Size(138, 13);
            this.labelCreator.TabIndex = 8;
            this.labelCreator.Text = "Создал: Ф.И.О. 12.12.2010";
            // 
            // checkEditIsApproved
            // 
            this.checkEditIsApproved.Location = new System.Drawing.Point(343, 8);
            this.checkEditIsApproved.Name = "checkEditIsApproved";
            this.checkEditIsApproved.Properties.Caption = "Активность утверждена для использования";
            this.checkEditIsApproved.Size = new System.Drawing.Size(252, 19);
            this.checkEditIsApproved.TabIndex = 9;
            this.checkEditIsApproved.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.checkEditIsApproved_EditValueChanging);
            // 
            // groupControlPositions
            // 
            this.groupControlPositions.Controls.Add(this.comboBoxEditFactController);
            this.groupControlPositions.Controls.Add(this.comboBoxEditPerformerInRetail);
            this.groupControlPositions.Controls.Add(this.comboBoxEditActivityApprover);
            this.groupControlPositions.Controls.Add(this.comboBoxEditControlGroupSelector);
            this.groupControlPositions.Controls.Add(this.comboBoxEditGoalCreator);
            this.groupControlPositions.Controls.Add(this.comboBoxEditInitiator);
            this.groupControlPositions.Controls.Add(this.labelControl9);
            this.groupControlPositions.Controls.Add(this.labelControl8);
            this.groupControlPositions.Controls.Add(this.labelControl7);
            this.groupControlPositions.Controls.Add(this.labelControl6);
            this.groupControlPositions.Controls.Add(this.labelControl5);
            this.groupControlPositions.Controls.Add(this.labelControl4);
            this.groupControlPositions.Location = new System.Drawing.Point(460, 282);
            this.groupControlPositions.Name = "groupControlPositions";
            this.groupControlPositions.Size = new System.Drawing.Size(162, 298);
            this.groupControlPositions.TabIndex = 11;
            this.groupControlPositions.Text = "Должности";
            // 
            // comboBoxEditFactController
            // 
            this.comboBoxEditFactController.Enabled = false;
            this.comboBoxEditFactController.Location = new System.Drawing.Point(5, 270);
            this.comboBoxEditFactController.Name = "comboBoxEditFactController";
            this.comboBoxEditFactController.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditFactController.Size = new System.Drawing.Size(150, 20);
            this.comboBoxEditFactController.TabIndex = 11;
            // 
            // comboBoxEditPerformerInRetail
            // 
            this.comboBoxEditPerformerInRetail.Enabled = false;
            this.comboBoxEditPerformerInRetail.Location = new System.Drawing.Point(5, 134);
            this.comboBoxEditPerformerInRetail.Name = "comboBoxEditPerformerInRetail";
            this.comboBoxEditPerformerInRetail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPerformerInRetail.Size = new System.Drawing.Size(151, 20);
            this.comboBoxEditPerformerInRetail.TabIndex = 10;
            // 
            // comboBoxEditActivityApprover
            // 
            this.comboBoxEditActivityApprover.Enabled = false;
            this.comboBoxEditActivityApprover.Location = new System.Drawing.Point(5, 224);
            this.comboBoxEditActivityApprover.Name = "comboBoxEditActivityApprover";
            this.comboBoxEditActivityApprover.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditActivityApprover.Size = new System.Drawing.Size(150, 20);
            this.comboBoxEditActivityApprover.TabIndex = 9;
            // 
            // comboBoxEditControlGroupSelector
            // 
            this.comboBoxEditControlGroupSelector.Enabled = false;
            this.comboBoxEditControlGroupSelector.Location = new System.Drawing.Point(5, 89);
            this.comboBoxEditControlGroupSelector.Name = "comboBoxEditControlGroupSelector";
            this.comboBoxEditControlGroupSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditControlGroupSelector.Size = new System.Drawing.Size(151, 20);
            this.comboBoxEditControlGroupSelector.TabIndex = 8;
            // 
            // comboBoxEditGoalCreator
            // 
            this.comboBoxEditGoalCreator.Enabled = false;
            this.comboBoxEditGoalCreator.Location = new System.Drawing.Point(5, 179);
            this.comboBoxEditGoalCreator.Name = "comboBoxEditGoalCreator";
            this.comboBoxEditGoalCreator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditGoalCreator.Size = new System.Drawing.Size(150, 20);
            this.comboBoxEditGoalCreator.TabIndex = 7;
            // 
            // comboBoxEditInitiator
            // 
            this.comboBoxEditInitiator.Enabled = false;
            this.comboBoxEditInitiator.Location = new System.Drawing.Point(5, 44);
            this.comboBoxEditInitiator.Name = "comboBoxEditInitiator";
            this.comboBoxEditInitiator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditInitiator.Size = new System.Drawing.Size(151, 20);
            this.comboBoxEditInitiator.TabIndex = 6;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(5, 250);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(96, 13);
            this.labelControl9.TabIndex = 5;
            this.labelControl9.Text = "Контроллер факта";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(5, 115);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(119, 13);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "Исполнитель в рознице";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(5, 205);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(126, 13);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "Утверждает активность";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(5, 70);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(121, 13);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "Выбор контрол. группы";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(5, 160);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(102, 13);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "Постановщик целей";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 25);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(117, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Инициатор активности";
            // 
            // groupControlActivity
            // 
            this.groupControlActivity.Controls.Add(this.lblTemplateName);
            this.groupControlActivity.Controls.Add(this.labelControl24);
            this.groupControlActivity.Controls.Add(this.labelControl17);
            this.groupControlActivity.Controls.Add(this.labelControl1);
            this.groupControlActivity.Controls.Add(this.documentUploader);
            this.groupControlActivity.Controls.Add(this.labelControl3);
            this.groupControlActivity.Controls.Add(this.memoEditDescription);
            this.groupControlActivity.Controls.Add(this.textControlChannel);
            this.groupControlActivity.Controls.Add(this.comboBoxActivityLevel);
            this.groupControlActivity.Controls.Add(this.labelControl2);
            this.groupControlActivity.Controls.Add(this.textEditName);
            this.groupControlActivity.Controls.Add(this.editControlActivityType);
            this.groupControlActivity.Location = new System.Drawing.Point(3, 30);
            this.groupControlActivity.Name = "groupControlActivity";
            this.groupControlActivity.Size = new System.Drawing.Size(454, 386);
            this.groupControlActivity.TabIndex = 10;
            this.groupControlActivity.Text = "Активность";
            // 
            // lblTemplateName
            // 
            this.lblTemplateName.Location = new System.Drawing.Point(5, 89);
            this.lblTemplateName.Name = "lblTemplateName";
            this.lblTemplateName.Properties.ReadOnly = true;
            this.lblTemplateName.Size = new System.Drawing.Size(104, 20);
            this.lblTemplateName.TabIndex = 22;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(5, 115);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(115, 13);
            this.labelControl24.TabIndex = 12;
            this.labelControl24.Text = "Описание активности:";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(239, 70);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(91, 13);
            this.labelControl17.TabIndex = 20;
            this.labelControl17.Text = "Канал персонала:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(239, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(47, 13);
            this.labelControl1.TabIndex = 19;
            this.labelControl1.Text = "Уровень:";
            // 
            // documentUploader
            // 
            this.documentUploader.ButtonLoadText = "Загрузить файл";
            this.documentUploader.FieldNameFileData = "Presentation_FileData";
            this.documentUploader.FieldNameFileName = "Presentation_FileName";
            this.documentUploader.FieldNameID = "Presentation_ID";
            this.documentUploader.Location = new System.Drawing.Point(5, 204);
            this.documentUploader.MaxItemsCount = 0;
            this.documentUploader.Name = "documentUploader";
            this.documentUploader.ReadOnly = true;
            this.documentUploader.Size = new System.Drawing.Size(444, 177);
            this.documentUploader.TabIndex = 10;
            this.documentUploader.GetFileData += new Logica.Reports.BaseReportControl.CommonControls.FileUploader.FileDataEventHandler(this.documentUploader_GetFileData);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(5, 25);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(22, 13);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "Тип:";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Location = new System.Drawing.Point(5, 134);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Size = new System.Drawing.Size(444, 64);
            this.memoEditDescription.TabIndex = 7;
            // 
            // textControlChannel
            // 
            this.textControlChannel.Enabled = false;
            this.textControlChannel.Location = new System.Drawing.Point(239, 89);
            this.textControlChannel.Name = "textControlChannel";
            this.textControlChannel.Size = new System.Drawing.Size(210, 20);
            this.textControlChannel.TabIndex = 4;
            // 
            // comboBoxActivityLevel
            // 
            this.comboBoxActivityLevel.Location = new System.Drawing.Point(239, 44);
            this.comboBoxActivityLevel.Name = "comboBoxActivityLevel";
            this.comboBoxActivityLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxActivityLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxActivityLevel.Size = new System.Drawing.Size(210, 20);
            this.comboBoxActivityLevel.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 70);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "Имя:";
            // 
            // textEditName
            // 
            this.textEditName.Location = new System.Drawing.Point(111, 89);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(104, 20);
            this.textEditName.TabIndex = 4;
            this.textEditName.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // editControlActivityType
            // 
            this.editControlActivityType.Enabled = false;
            this.editControlActivityType.Location = new System.Drawing.Point(5, 44);
            this.editControlActivityType.Name = "editControlActivityType";
            this.editControlActivityType.Size = new System.Drawing.Size(210, 20);
            this.editControlActivityType.TabIndex = 4;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "delete.png");
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.UpliftCalculationBtn);
            this.groupControl1.Controls.Add(this.UpliftResultLabel);
            this.groupControl1.Location = new System.Drawing.Point(628, 506);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(373, 74);
            this.groupControl1.TabIndex = 23;
            this.groupControl1.Text = "Расчет аплифта";
            // 
            // ActivityDetailsCommon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "ActivityDetailsCommon";
            this.Size = new System.Drawing.Size(1008, 681);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPromoCreation)).EndInit();
            this.groupControlPromoCreation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllTTInActivePromo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllowAddAllPromo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCascadeEditPromo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPeriods)).EndInit();
            this.groupControlPeriods.ResumeLayout(false);
            this.groupControlPeriods.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTakeBasePeriodsFromActiveWave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMonthsToTrackUpliftAfterEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlKPK)).EndInit();
            this.groupControlKPK.ResumeLayout(false);
            this.groupControlKPK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditKPKDescriptionTemplate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWithConnectionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowInKPK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlControlGroup)).EndInit();
            this.groupControlControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllTTInActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllowAddAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCascadeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPositions)).EndInit();
            this.groupControlPositions.ResumeLayout(false);
            this.groupControlPositions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditFactController.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPerformerInRetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditActivityApprover.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditControlGroupSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoalCreator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditInitiator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlActivity)).EndInit();
            this.groupControlActivity.ResumeLayout(false);
            this.groupControlActivity.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTemplateName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textControlChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxActivityLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editControlActivityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
		private DevExpress.XtraEditors.GroupControl groupControlPromoCreation;
        private DevExpress.XtraEditors.CheckEdit checkEditAllTTInActivePromo;
        private DevExpress.XtraEditors.CheckEdit checkEditAllowAddAllPromo;
        private DevExpress.XtraEditors.CheckEdit checkEditCascadeEditPromo;
        private DevExpress.XtraEditors.GroupControl groupControlKPK;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.MemoEdit memoEditKPKDescriptionTemplate;
        private DevExpress.XtraEditors.CheckEdit checkEditWithConnectionDate;
        private DevExpress.XtraEditors.CheckEdit checkEditShowInKPK;
		private DevExpress.XtraEditors.GroupControl groupControlControlGroup;
        private DevExpress.XtraEditors.CheckEdit checkEditAllTTInActive;
        private DevExpress.XtraEditors.CheckEdit checkEditAllowAddAll;
        private DevExpress.XtraEditors.CheckEdit checkEditCascadeEdit;
        private DevExpress.XtraEditors.LabelControl labelCreator;
        private DevExpress.XtraEditors.CheckEdit checkEditIsApproved;
        private DevExpress.XtraEditors.GroupControl groupControlActivity;
        private Logica.Reports.BaseReportControl.CommonControls.FileUploader documentUploader;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxActivityLevel;
        private DevExpress.XtraEditors.GroupControl groupControlPositions;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditFactController;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPerformerInRetail;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditActivityApprover;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditControlGroupSelector;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditGoalCreator;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditInitiator;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraEditors.GroupControl groupControlPeriods;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod2YearTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod2MonthTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod2DayTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod2YearFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod2MonthFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod2DayFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod1YearTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod1MonthTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod1DayTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod1YearFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod1MonthFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriod1DayFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriodYearTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriodMonthTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriodDayTo;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriodYearFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriodMonthFrom;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo datePeriodDayFrom;
        private DevExpress.XtraEditors.CheckEdit checkEditTakeBasePeriodsFromActiveWave;
        private DevExpress.XtraEditors.SpinEdit spinEditMonthsToTrackUpliftAfterEnd;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit editControlActivityType;
        private DevExpress.XtraEditors.TextEdit textControlChannel;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit lblTemplateName;
        private DevExpress.XtraEditors.LabelControl lblActivityNotInBudget;
        private DevExpress.XtraEditors.LabelControl UpliftResultLabel;
        private DevExpress.XtraEditors.SimpleButton UpliftCalculationBtn;
        private DevExpress.XtraEditors.GroupControl groupControl1;

    }
}
