﻿using DevExpress.XtraGrid.Localization;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    public class RussianGridLocalizer : GridLocalizer
    {
        public override string Language
        {
            get { return "Russian"; }
        }

        public override string GetLocalizedString(GridStringId id)
        {
            string ret = Resource.ResourceManager.GetString(id.ToString());

            if (string.IsNullOrEmpty(ret))
            {
                ret = base.GetLocalizedString(id);
            }

            return ret;
        }
    }
}