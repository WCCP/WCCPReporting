﻿using System.Collections.Generic;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    public static class DictionaryExtension
    {
        public static void AddItemToDictionary(this Dictionary<long, List<OnInvoiceModel>> dict, OnInvoiceModel item, bool distinct = false)
        {
            List<OnInvoiceModel> lOnInvoiceList;
            if (dict.ContainsKey(item.OutletId))
                lOnInvoiceList = dict[item.OutletId];
            else
            {
                lOnInvoiceList = new List<OnInvoiceModel>();
                dict.Add(item.OutletId, lOnInvoiceList);
            }

            if (!distinct || !lOnInvoiceList.Contains(item))
                lOnInvoiceList.Add(item);
        }

        public static void DeleteItemFromDict(this Dictionary<long, List<OnInvoiceModel>> dict, OnInvoiceModel item)
        {
            if (!dict.ContainsKey(item.OutletId))
                return;
            List<OnInvoiceModel> lList = dict[item.OutletId];
            if (lList.Contains(item))
                lList.Remove(item);
            if (lList.Count == 0)
                dict.Remove(item.OutletId);
        }
    }
}
