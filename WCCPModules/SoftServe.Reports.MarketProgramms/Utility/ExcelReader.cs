﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    /// <summary>
    /// Contains the Excel reading methods
    /// </summary>
    internal class ExcelReader
    {
        private System.Globalization.CultureInfo _systemCulture;
        private SpreadsheetDocument _excelFile;
        private WorkbookPart _workbook;

        private string _fileName;

        public ExcelReader(string fileName)
        {
            _fileName = fileName;
            _systemCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        }

        public void StartReading()
        {
            try
            {
                _excelFile = SpreadsheetDocument.Open(_fileName, false);
                _workbook = _excelFile.WorkbookPart;
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Ошибка при откытии файла", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Gets the worksheet by number
        /// </summary>
        private SheetData GetWorkSheetData(int pageNumber, out string pageName)
        {
            _excelFile = SpreadsheetDocument.Open(_fileName, false);
            _workbook = _excelFile.WorkbookPart;

            Sheet lSheet = _workbook.Workbook.Descendants<Sheet>().ToArray()[pageNumber - 1];
            pageName = lSheet.Name;

            WorksheetPart lWorkSheet = (WorksheetPart)(_workbook.GetPartById(lSheet.Id));
            SheetData lSheetData = lWorkSheet.Worksheet.Elements<SheetData>().First();

            return lSheetData;
        }

        public void ReadFileWorksheet(List<ImportedPeriodModel> validPlansFromFile, List<ImportedPeriodWithErrorsModel> invalidPlansFromFile,
                                      int columnsFromFileCount, int worksheetNumber)
        {
            bool wasReadingSuccsessful = true;
            SheetData lSheetData = null;

            try
            {
                string lSheetName;
                lSheetData = GetWorkSheetData(worksheetNumber, out lSheetName);
                List<Row> rows = lSheetData.Elements<Row>().ToList();

                // Read the sheet data
                if (rows.Count > 1)// to flip over the header row :)
                {
                    for (var rowNum = 1; rowNum < rows.Count; rowNum++)
                    {
                        List<string> lCurrentRow = GetRowData(rows[rowNum]);

                        if (IsRowEmpty(lCurrentRow))
                            continue;

                        HandleRowLoading(rowNum + 1, lCurrentRow, validPlansFromFile, invalidPlansFromFile);
                    }
                }
            }
            catch (Exception)
            {
                wasReadingSuccsessful = false;
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            if (!wasReadingSuccsessful)
            {
                throw new Exception();
            }
        }

        private void HandleRowLoading(int rowNumber, List<string> currentRow, List<ImportedPeriodModel> validPlansFromFile,
                                      List<ImportedPeriodWithErrorsModel> invalidPlansFromFile)
        {
            ImportedPeriodModel lPlan;
            bool isCorrect = ImportRow(currentRow, rowNumber, out lPlan);

            if (isCorrect)
            {
                validPlansFromFile.Add(lPlan);
            }
            else
            {
                invalidPlansFromFile.Add(GetInvalidRowDescription(currentRow, rowNumber, lPlan.StatusDescription));
            }
        }

        private static ImportedPeriodWithErrorsModel GetInvalidRowDescription(List<string> rowFromFile, int rowNumber, string rowErrorMessage)
        {
            ImportedPeriodWithErrorsModel period = new ImportedPeriodWithErrorsModel();

            period.OutletId = rowFromFile[0];
            period.DateFrom = rowFromFile[1];
            period.DateTo = rowFromFile[2];
            period.DiscountSum = rowFromFile[3];
            period.DiscountPercent = rowFromFile[4];

            period.RowNumber = rowNumber;
            period.StatusDescription = rowErrorMessage;

            return period;
        }

        private bool ImportRow(List<string> row, int rowNumber, out ImportedPeriodModel period)
        {
            string rowErrorMessage = string.Empty;
            string fieldErrorMessage = "Некорректный формат данных";
            period = new ImportedPeriodModel();
            bool isCorrect = true;

            try
            {
                period.OutletId = Convert.ToInt64(row[0], CultureInfo.InvariantCulture);
                period.DateFrom = DateTime.FromOADate(double.Parse(row[1]));
                period.DateTo = DateTime.FromOADate(double.Parse(row[2]));
                period.DiscountSum = Convert.ToDecimal(row[3], CultureInfo.InvariantCulture);
                period.DiscountPercent = Convert.ToDecimal(row[4], CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                rowErrorMessage += fieldErrorMessage;
                isCorrect = false;
            }

            period.RowNumber = rowNumber;
            period.StatusDescription = !string.IsNullOrEmpty(rowErrorMessage) ? "Некорректный формат данных" : string.Empty;

            return isCorrect;
        }

        private List<string> GetRowData(Row row)
        {
            var dataRow = new List<string>();

            var cellEnumerator = GetExcelCellEnumerator(row);
            while (cellEnumerator.MoveNext())
            {
                var cell = cellEnumerator.Current;
                var text = ReadExcelCell(cell, _workbook).Trim();

                dataRow.Add(text);
            }

            return dataRow;
        }

        private void FreeResourses()
        {
            if (_workbook != null)
            {
                ReleaseObject(_workbook);
            }

            _excelFile.Close();
            ReleaseObject(_excelFile);
        }

        /// <summary>
        /// Released resources
        /// </summary>
        /// <param name="obj">Object to be released</param>
        private void ReleaseObject(object obj)
        {
            if (obj != null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private bool IsRowEmpty(List<string> row)
        {
            bool isNotEmpty = row.Any(v => !string.IsNullOrEmpty(v));

            return !isNotEmpty;
        }

        private string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                // ASCII 'A' = 65
                int current = i == 0 ? letter - 65 : letter - 64;
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        /// <summary>
        /// Handles row reading cell by cell, cops with gaps in data
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)//empty cells
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }
    }
}