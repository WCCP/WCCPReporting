﻿using System.Collections.Generic;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    public delegate bool ValidateRow(OnInvoiceModel model);

    public delegate List<OnInvoiceModel> ValidatePromoGroup(OnInvoiceModel model);
}
