﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Logica.Reports.BaseReportControl.CommonControls.MappingControl;
using SoftServe.Reports.MarketProgramms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using System.Globalization;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    class Utility
    {
        /// <summary>
        /// Row number of the first non-empty cell
        /// </summary>
        private static int rFirst = -1;

        static SpreadsheetDocument _excelFile;
        static WorkbookPart _workbook;

        /// <summary>
        /// Gets the first worksheet
        /// </summary>
        /// <param name="theWorkbook">Current workbook</param>
        /// <returns></returns>
        private static SheetData GetWorkSheetData(string fileName)
        {
            _excelFile = SpreadsheetDocument.Open(fileName, false);
            _workbook = _excelFile.WorkbookPart;

            Sheet lSheet = _workbook.Workbook.Descendants<Sheet>().First();

            WorksheetPart lWorkSheet = (WorksheetPart)(_workbook.GetPartById(lSheet.Id));
            SheetData lSheetData = lWorkSheet.Worksheet.Elements<SheetData>().First();

            return lSheetData;
        }

        public static List<ComboBoxItem> ReadFileHeader(string fileName)
        {
            List<ComboBoxItem> FileAims = new List<ComboBoxItem>();

            SheetData lSheetData = null;

            try
            {
                lSheetData = GetWorkSheetData(fileName);

                List<Row> rows = lSheetData.Elements<Row>().ToList();
                List<string> headers = new List<string>();

                // Read row
                rFirst = -1;
                int rowNum = -1;

                do
                {
                    rowNum++;
                    headers = GetRowData(rows[rowNum]);
                }
                while (IsRowEmpty(headers));

                rFirst = rowNum;

                for (int i = 0; i < headers.Count; i++)
                {
                    FileAims.Add(new ComboBoxItem(i, headers[i]));
                }
            }
            catch (Exception e)
            {
                FileAims = null;
                XtraMessageBox.Show("Некорректный формат файла", "Предупреждение");
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            return FileAims;
        }

        public static DataTable ReadFileBody(string fileName, List<ComboBoxItem> ActivityAims, List<ComboBoxItem> FileAims, int[] mapping, DataTable plans)
        {
            SheetData lSheetData = null;

            try
            {
                lSheetData = GetWorkSheetData(fileName);
                List<Row> rows = lSheetData.Elements<Row>().ToList();

                int lColumnCount = rows[0].ChildElements.Count; //  it resolves crash when there are empty last columns in plans excell 
                foreach (Row  lr in rows)
                {
                    if (lr.ChildElements.Count < lColumnCount)
                    {
                        int delta = lColumnCount - lr.ChildElements.Count;

                        for (int i = 0; i < delta; i++)
                        {
                            string lCellRef = lr.Descendants<Cell>().Last().CellReference.Value;
                            char lcharNext = lCellRef[0];
                            char lChar = lcharNext++;
                            lCellRef = lCellRef.Replace(lChar, lcharNext);
                            Cell c = new Cell(){CellReference = lCellRef, CellValue = new CellValue()};
                            lr.Append(c);
                        }
                    }                                          //  it resolves crash when there are empty last columns in plans excell 
                }

                for (int rCnt = rFirst + 1; rCnt < rows.Count; rCnt++)
                {
                    object[] row = new object[ActivityAims.Count + 3];
                    List<string> lCurRow = GetRowData(rows[rCnt]);

                    if (IsRowEmpty(lCurRow))
                        continue;

                    for (int j = 0; j < ActivityAims.Count; j++)
                    {
                        row[j] = lCurRow[FileAims[mapping[j]].Id];
                    }

                    row[ActivityAims.Count] = rCnt;

                    try
                    {
                        ConvertStringDescriptionToData(row, plans);
                        plans.Rows.Add(row);
                    }
                    catch (Exception e)
                    {
                        for (int j = 1; j < ActivityAims.Count; j++) // BUG: if row has incorrect data we still show "Код ТТ"; It was "for (int j = 0; j < ActivityAims.Count; j++)"
                        {
                            row[j] = null;
                        }

                        row[ActivityAims.Count + 1] = false;
                        row[ActivityAims.Count + 2] = "Неверный формат данных";

                        plans.Rows.Add(row);
                    }
                }

            }
            catch (Exception e)
            {
                plans = null;
                XtraMessageBox.Show("Некорректный формат файла", "Предупреждение");
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            return plans;
        }

        private static void ConvertStringDescriptionToData(object[] row, DataTable plans)
        {
            row[0] = Convert.ToInt64(row[0], CultureInfo.InvariantCulture);// OlId
            row[1] = DateTime.FromOADate(double.Parse(Convert.ToString(row[1])));// date

            for (int colId = 2; colId < plans.Columns.Count - 3; colId++)
            {
                if (plans.Columns[colId].DataType == typeof(byte))// IPTR
                {
                    row[colId] = Convert.ToInt16(row[colId], CultureInfo.InvariantCulture);
                    continue;
                }

                if (plans.Columns[colId].DataType == typeof(double))// Sales volume
                {
                    row[colId] = Convert.ToDouble(row[colId], CultureInfo.InvariantCulture);
                    continue;
                }

                if (plans.Columns[colId].DataType == typeof(float)) // Merchandising
                {
                    row[colId] = Convert.ToSingle(row[colId], CultureInfo.InvariantCulture);
                    continue;
                }

                if (plans.Columns[colId].DataType == typeof(bool)) 
                {
                    row[colId] = ((string)row[colId]).Trim() == "1" || ((string)row[colId]).Trim().ToLower() == "да";
                }
            }
        }

        private static void FreeResourses()
        {
            if (_workbook != null)
            {
                ReleaseObject(_workbook);
            }

            if (_excelFile != null)
            {
                _excelFile.Close();
            }

            ReleaseObject(_excelFile);
        }

        /// <summary>
        /// Released resources
        /// </summary>
        /// <param name="obj">Object to be released</param>
        private static void ReleaseObject(object obj)
        {
            if (obj != null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private static bool IsRowEmpty(List<string> row)
        {
            bool isNotEmpty = row.Any(v => !string.IsNullOrEmpty(v));

            return !isNotEmpty;
        }

        private static string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private static int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                // ASCII 'A' = 65
                int current = i == 0 ? letter - 65 : letter - 64;
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        /// <summary>
        /// Handles row reading cell by cell, cops with gaps in data
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private static IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)//empty cells
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private static List<string> GetRowData(Row row)
        {
            var dataRow = new List<string>();

            var cellEnumerator = GetExcelCellEnumerator(row);
            while (cellEnumerator.MoveNext())
            {
                var cell = cellEnumerator.Current;
                var text = ReadExcelCell(cell, _workbook).Trim();

                dataRow.Add(text);
            }

            return dataRow;
        }

        private static string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }

    }
}
