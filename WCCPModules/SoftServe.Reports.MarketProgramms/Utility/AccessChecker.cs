﻿using System.Data;
using Logica.Reports.BaseReportControl.Utility;
using System;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    public class AccessChecker
    {
        #region Constructors

        public AccessChecker(DataTable templateDetails, DataTable activityDetails)
        {
            if (activityDetails == null || activityDetails.Rows.Count == 0)
            {
                Initiator = templateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_INITIATOR].ToString();
                GoalsSetter = templateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_PLANNER].ToString();
                Approver = templateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_APPROVER].ToString();
                ControlGroupSelector = templateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_CONTROLLER].ToString();
                PerformerInRetail = templateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_EXECUTOR].ToString();
                FactController = templateDetails.Rows[0][Constants.SP_TEMPLATE_DETAILS_OLCHECKER].ToString();
            }
            else
            {
                Initiator = activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_INITIATOR].ToString();
                GoalsSetter = activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_PLANNER].ToString();
                Approver = activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_APPROVER].ToString();
                ControlGroupSelector = activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_CONTROLLER].ToString();
                PerformerInRetail = activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_EXECUTOR].ToString();
                FactController = activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_OLCHECKER].ToString();

                _controlTTCount = Convert.ToInt32(activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_CNT_TT_COUNT]);
                _controlTTAvailable = Convert.ToInt32(activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_CNT_TT_AVAILABLE]);
                _promoTTCount = Convert.ToInt32(activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_OL_TT_COUNT]);
                _promoTTAvailable = Convert.ToInt32(activityDetails.Rows[0][Constants.SP_ACTIVITY_DETAILS_OL_TT_AVAILABLE]);
            }
        }

        #endregion

        #region Instance Properties

        /// <summary>
        /// Gets the initiator.
        /// </summary>
        /// <value>Инициатор активности - пользователь, который имеет должность, настроенную в шаблоне, как инициатор активности - может создавать активность на основе этого шаблона. Он же может изменять ее, если имеет доступ ко всей адреске активности. Этот пользователь проставляет галочки подтверждения точек в активность и может менять любую другую информацию в активности.</value>
        public string Initiator { get; set; }

        /// <summary>
        /// Gets the goals setter.
        /// </summary>
        /// <value>Постановщик целей может задавать параметры в настройках целей и на закладке целей. При этом он должен иметь доступ ко всей адреске.</value>
        public string GoalsSetter { get; set; }

        /// <summary>
        /// Gets the approver.
        /// </summary>
        /// <value>Утверждает активность - пользователь с данной должностью утверждает активность. При этом он должен иметь доступ ко всей адреске.</value>
        public string Approver { get; set; }

        /// <summary>
        /// Gets the control group selector.
        /// </summary>
        /// <value>Выбор контрольной группы выполняется пользователем с настроенной должностью, после утверждения активности в контрольную группу можно только добавлять точки, а убирать нельзя, как и в промо адреску. При этом он должен иметь доступ ко всей адреске.</value>
        public string ControlGroupSelector { get; set; }

        /// <summary>
        /// Gets the performer in retail.
        /// </summary>
        /// <value>Исполнитель в рознице проставляет Отказ, причину отказа, может добавить комментарий, проставить дату подключения и дату отключения, а так же отметить сторчек. При этом исполнитель види и может менять только точки со своей территории.</value>
        public string PerformerInRetail { get; set; }

        /// <summary>
        /// Gets the fact controller.
        /// </summary>
        /// <value>Контролер факта выполнения проставляет признак утверждения дат подключения и дат отключения, после этого исполнитель в рознице уже не может менять данную точку. Контролер так же может изменить статус активности на Реактивировать. При этом контролер видит и может менять только те точки, которые находятся на его территории.</value>
        public string FactController { get; set; }

        private int _promoTTCount = 0;
        private int _controlTTCount = 0;

        public int PromoTTCount
        {
            get { return _promoTTCount; }
            set { _promoTTCount = value; }
        }

        public int ControlTTCount
        {
            get { return _controlTTCount; }
            set { _controlTTCount = value; }
        }

        private int _promoTTAvailable = 0;
        private int _controlTTAvailable = 0;

        public int PromoTTAvailable
        {
            get { return _promoTTAvailable; }
            set { _promoTTAvailable = value; }
        }

        public int ControlTTAvailable
        {
            get { return _controlTTAvailable; }
            set { _controlTTAvailable = value; }
        }

        #endregion

        #region Instance Methods

        public bool CheckAccess(ActivityPositionEnum positionEnum)
        {
            string lPosition = string.Empty;

            switch (positionEnum)
            {
                case ActivityPositionEnum.Initiator:
                    lPosition = Initiator;
                    break;
                case ActivityPositionEnum.ControlGroupSelector:
                    lPosition = ControlGroupSelector;
                    break;
                case ActivityPositionEnum.PerformerInRetail:
                    lPosition = PerformerInRetail;
                    break;
                case ActivityPositionEnum.GoalsSetter:
                    lPosition = GoalsSetter;
                    break;
                case ActivityPositionEnum.Approver:
                    lPosition = Approver;
                    break;
                case ActivityPositionEnum.FactController:
                    lPosition = FactController;
                    break;
            }

            bool lIsPositionAvailable = DataProvider.CheckPosition(lPosition);
            //new
            //return true;
            //
            return (positionEnum == ActivityPositionEnum.PerformerInRetail || positionEnum == ActivityPositionEnum.FactController)
                       ? lIsPositionAvailable
                       : lIsPositionAvailable && PromoTTCount == PromoTTAvailable;
        }

        #endregion
    }
}