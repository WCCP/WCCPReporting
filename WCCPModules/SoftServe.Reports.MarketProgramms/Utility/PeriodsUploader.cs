﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.MarketProgramms.Forms;

namespace SoftServe.Reports.MarketProgramms.Utility
{
    /// <summary>
    /// Performs file uploading 
    /// </summary>
    class PeriodsUploader
    {
        public const int COLUMNS__FROM_FILE_COUNT = 5;

        private Dictionary<int, string> _errorCodes;

        private List<ImportedPeriodModel> _validPeriods;
        private List<ImportedPeriodWithErrorsModel> _invalidPeriods;

        private Dictionary<long, List<OnInvoiceModel>> _periodsByOutletsFromFile;


        public PeriodsUploader()
        {
            _validPeriods = new List<ImportedPeriodModel>();
            _invalidPeriods = new List<ImportedPeriodWithErrorsModel>();
            _periodsByOutletsFromFile = new Dictionary<long, List<OnInvoiceModel>>();

            InitErrorCodesList();
        }

        public void UploadPlans(ValidateRow checkPeriodLength, ValidateRow checkPromoGr, ValidatePromoGroup checkPeriods, int actionId, int aimId)
        {
            string lFilePath = GetFileName();

            if (lFilePath == string.Empty)// file wasn't chosen
                return;

            WaitManager.StartWait();

            if (!ReadData(lFilePath))
            {
                WaitManager.StopWait();
                return;
            }

            ValidateData(checkPeriodLength, checkPromoGr, checkPeriods, actionId, aimId);

            WaitManager.StopWait();

            if (HasIncorrectPlans && !AskForLoadingContinuation())
                return;

            if (_validPeriods.Count == 0)
            {
                XtraMessageBox.Show("Корректных cтрок не обнаружено", "Загрузка периодов", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            WaitManager.StartWait();
            SavePeriodsToDb(aimId);
            WaitManager.StopWait();

            XtraMessageBox.Show("Планы успешно загружены", "Загрузка периодов", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private bool HasIncorrectPlans
        {
            get { return _invalidPeriods.Count > 0; }
        }

        private void SavePeriodsToDb(int aimId)
        {
            foreach (ImportedPeriodModel period in _validPeriods)
            {
                OnInvoiceModel lModel = (OnInvoiceModel)period;
                lModel.AimId = aimId;

                DataProvider.ImportOnInvoiceItem(lModel);
            }
        }

        private void InitErrorCodesList()
        {
            _errorCodes = new Dictionary<int, string>();

            _errorCodes.Add(1, "Некорректный формат данных");
            _errorCodes.Add(2, "Некорректный период");
            _errorCodes.Add(3, "Период действия лимита превышает период действия акции");
            _errorCodes.Add(4, "ТТ не принадлежит к Промо Группе акции");
            _errorCodes.Add(5, "Обнаружены конфликты в диапазонах дат");
        }

        private bool ReadData(string filePath)
        {
            ExcelReader fileReader = new ExcelReader(filePath);
            bool wasReadingSuccsessful = true;

            fileReader.StartReading();

            try
            {
                fileReader.ReadFileWorksheet(_validPeriods, _invalidPeriods, COLUMNS__FROM_FILE_COUNT, 1);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Некорректный формат файла!", "Загрузка планов", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wasReadingSuccsessful = false;
            }
            
            return wasReadingSuccsessful;
        }

        private void ValidateData(ValidateRow checkPeriodLength, ValidateRow checkPromoGr, ValidatePromoGroup checkPeriods, int actionId, int aimId)
        {
            _periodsByOutletsFromFile.Clear();// records without errors grouped by Ols

            for (int i = _validPeriods.Count - 1; i >= 0; i--)// general validations
            {
                OnInvoiceModel lOnInvoiceModel = GetOnInvoiceModel(_validPeriods[i], actionId, aimId);
                
                if (!CheckPeriodStartEndDates(_validPeriods[i]))//"Некорректный период"
                {
                    HandleIncorrectPeriod(i, ErrorTypes.IncorrectPeriod);
                    continue;
                }

                if (!checkPeriodLength(lOnInvoiceModel))//"Период действия лимита превышает период действия акции"
                {
                    HandleIncorrectPeriod(i, ErrorTypes.IncorrectPeriodTranscendsActionLimits);
                    continue;
                }

                if (!checkPromoGr(lOnInvoiceModel))//"ТТ не принадлежит к Промо Группе акции"
                {
                    HandleIncorrectPeriod(i, ErrorTypes.OlDoesntBelongToPromoGroup);
                    continue;
                }

                _periodsByOutletsFromFile.AddItemToDictionary(lOnInvoiceModel);// records without errors grouped by Ols
            }// general validations


            for (int i = _validPeriods.Count - 1; i >= 0; i--)// conflicts in periods
            {
                OnInvoiceModel lOnInvoiceModel = GetOnInvoiceModel(_validPeriods[i], actionId, aimId);

                ImportedPeriodWithErrorsModel invalidPeriodOverlapping = (ImportedPeriodWithErrorsModel)_validPeriods[i];
                invalidPeriodOverlapping.PeriodList = checkPeriods(lOnInvoiceModel);
                invalidPeriodOverlapping.PeriodList.AddRange(FindConflictingPeriodsInFile(lOnInvoiceModel));

                if (invalidPeriodOverlapping.PeriodList.Count > 0)//"Обнаружены конфликты в периодах"
                {
                    HandleIncorrectPeriod(i, invalidPeriodOverlapping, ErrorTypes.ConfilctingPeriods);
                }
            }

        }

        private List<OnInvoiceModel> FindConflictingPeriodsInFile(OnInvoiceModel period)
        {
            List<OnInvoiceModel> lPeriodList = _periodsByOutletsFromFile[period.OutletId];
            List<OnInvoiceModel> lConflictingModels = new List<OnInvoiceModel>();

            foreach (OnInvoiceModel lModel in lPeriodList)
            {
                if (lModel.Equals(period))// isn't considered to be a conflict
                    continue;

                if ((period.DateFrom >= lModel.DateFrom && period.DateFrom <= lModel.DateTo) ||
                    (period.DateTo >= lModel.DateFrom && period.DateTo <= lModel.DateTo))
                {
                    lConflictingModels.Add(lModel);
                }
            }

            return lConflictingModels;
        }

        private bool CheckPeriodStartEndDates(ImportedPeriodModel importedPeriodModel)
        {
            return importedPeriodModel.DateFrom <= importedPeriodModel.DateTo;
        }

        private bool AskForLoadingContinuation()
        {
            PeriodsWithErrorsDisplayForm periodsWithErrorsForm = new PeriodsWithErrorsDisplayForm();
            periodsWithErrorsForm.LoadData(_invalidPeriods);

            DialogResult result = periodsWithErrorsForm.ShowDialog();

            return result == DialogResult.Yes;
        }

        private string GetFileName()
        {
            string lFilePath = string.Empty;

            try
            {
                lFilePath = ChooseFile();
            }
            catch (IOException ex)
            {
                XtraMessageBox.Show("Файл уже открыт в другом приложении", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return lFilePath;
        }

        private string ChooseFile()
        {
            string lPath = string.Empty;

            string DIALOG_OPEN_FILE_FILTER = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            string DIALOG_OPEN_FILE = "Загрузить файл";

            OpenFileDialog lFileDialog = new OpenFileDialog
                {
                    Filter = DIALOG_OPEN_FILE_FILTER,
                    Title = DIALOG_OPEN_FILE,
                    CheckFileExists = true,
                    RestoreDirectory = true
                };

            if (lFileDialog.ShowDialog() == DialogResult.OK)
            {
                lPath = lFileDialog.FileName;
            }

            return lPath;
        }

        private void HandleIncorrectPeriod(int periodId, ErrorTypes error)
        {
            ImportedPeriodWithErrorsModel lInvalidPeriod = (ImportedPeriodWithErrorsModel)_validPeriods[periodId];

            lInvalidPeriod.StatusDescription = _errorCodes[(int)error];

            _invalidPeriods.Add(lInvalidPeriod);
            _validPeriods.RemoveAt(periodId);
        }

        private void HandleIncorrectPeriod(int periodId, ImportedPeriodWithErrorsModel invalidPeriod, ErrorTypes error)
        {
            invalidPeriod.StatusDescription = _errorCodes[(int)error];

            _invalidPeriods.Add(invalidPeriod);
            _validPeriods.RemoveAt(periodId);
        }

        private OnInvoiceModel GetOnInvoiceModel(ImportedPeriodModel period, int actionId, int aimId)
        {
            OnInvoiceModel lModel = (OnInvoiceModel)period;
            lModel.ActionId = actionId;
            lModel.AimId = aimId;

            return lModel;
        }
    }
}
