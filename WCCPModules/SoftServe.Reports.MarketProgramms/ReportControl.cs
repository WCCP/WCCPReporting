﻿using System;
using System.ComponentModel;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.MarketProgramms;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraTab;
using SoftServe.Reports.MarketProgramms.Tabs;
using DevExpress.XtraPrintingLinks;
using SoftServe.Reports.MarketProgramms.UserControls;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    [ToolboxItem(false)]
    public partial class WccpUIControl : BaseReportUserControl
    {
        private ActivityListTab tabClientList;
       
        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();

            //
            SetExportType(ExportToType.Pdf, false);
            SetExportType(ExportToType.Rtf, false);
            btnExportTo.Enabled = false;
            //

            tabClientList = new ActivityListTab(this);
                        
            TabControl.ClosePageButtonShowMode = ClosePageButtonShowMode.InAllTabPageHeaders;

            TabControl.SelectedPageChanged += new TabPageChangedEventHandler(TabControl_SelectedPageChanged);
            
            UpdateAllSheets();
        }

        /// <summary>
        /// Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            SheetParamCollection parameterCollection = new SheetParamCollection { TabId = tabClientList.TabId, TableDataType = TableType.Fact };
            tabClientList.UpdateSheet(parameterCollection);
        }

        private void TabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            if (e.Page != null && e.Page is ActivityListTab)// activity list
            {
                ActivityListTab tab = e.Page as ActivityListTab;
                if (tab.Bruc != null)
                {
                    tab.Bruc.btnExportTo.Enabled = false;
                }
            }
            else
                if (e.Page != null && e.Page is ActivityDetailsTab) // activity details
                {
                    ActivityDetailsTab tab = e.Page as ActivityDetailsTab;
                    if (tab.UserControl != null)
                    {
                        ActivityDetailsControl cont = tab.UserControl as ActivityDetailsControl;
                        if (cont.tabControl.SelectedTabPage != null)
                        {
                            CommonBaseTab tabInner = cont.tabControl.SelectedTabPage as CommonBaseTab;
                            if (tabInner.Bruc != null && (tabInner.UserControl is ActivityDetailsGoals || tabInner.UserControl is ActivityDetailsAddresses))
                            {
                                tabInner.Bruc.btnExportTo.Enabled = true;
                            }
                            else
                                if (tabInner.Bruc != null)
                                {
                                    tabInner.Bruc.btnExportTo.Enabled = false;
                                }
                        }
                    }
                }
        }
    }
}
