﻿using System;
using System.Windows.Forms;
using SoftServe.Reports.MarketProgramms;
using DevExpress.XtraGrid;
using System.Data;
using System.Globalization;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace SoftServe.Reports.MarketProgramms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ProgrammListControl : CommonBaseControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListControl"/> class.
        /// </summary>
        public ProgrammListControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public ProgrammListControl(CommonBaseTab parentTab)
            : base(parentTab)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the grid control.
        /// </summary>
        /// <value>The grid control.</value>
        public GridControl GridControl
        {
            get
            {
                return gridControl;
            }
        }

        /// <summary>
        /// Gets or sets the client list.
        /// </summary>
        /// <value>The client list.</value>
        public DataTable ClientList
        {
            set
            {
                gridControl.DataSource = value;
                UpdateButtonsState();
            }
            get
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable)gridControl.DataSource;
            }
        }

        /// <summary>
        /// Handles the Load event of the ClientListControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ClientListControl_Load(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;

        }

        /// <summary>
        /// News this instance.
        /// </summary>
        private void New()
        {
            LoadDetailsTab(false);
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        private void Edit()
        {
            LoadDetailsTab(true);
        }

        /// <summary>
        /// Loads the details tab.
        /// </summary>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        private void LoadDetailsTab(bool isEditMode)
        {
            if (ParentTab != null)
            {                
                string strTabName = Resource.NewDetailsTabName;
                int clientId = 0;
                if (isEditMode)
                {
                }
            }
        }

        /// <summary>
        /// Updates the state of the buttons.
        /// </summary>
        private void UpdateButtonsState()
        {
            gridView.PostEditor();
            gridView.UpdateCurrentRow();

            //barButtonEdit.Enabled = gridView.FocusedRowHandle >= 0;
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            UpdateButtonsState();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonNew control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Edit();
        }

        /// <summary>
        /// Handles the DoubleClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DoubleClick(object sender, EventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                Point pt = gridView.GridControl.PointToClient(Control.MousePosition);
                GridHitInfo info = gridView.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    Edit();
                }
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditDateFrom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditDateFrom_EditValueChanged(object sender, EventArgs e)
        {
            UpdateRowsHighlightState();
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditDateTo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditDateTo_EditValueChanged(object sender, EventArgs e)
        {
            UpdateRowsHighlightState();
        }

        /// <summary>
        /// Updates the state of the rows highlight.
        /// </summary>
        private void UpdateRowsHighlightState()
        {

        }

        private void gridMPrograms_Click(object sender, EventArgs e)
        {

        }
    }
}
