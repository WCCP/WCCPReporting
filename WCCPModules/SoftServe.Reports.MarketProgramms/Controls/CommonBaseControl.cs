﻿using System;
using System.Windows.Forms;
using SoftServe.Reports.MarketProgramms;
using DevExpress.XtraGrid;
using System.Data;

namespace SoftServe.Reports.MarketProgramms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CommonBaseControl : UserControl
    {
        public event EventHandler UpdateMainView;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        public CommonBaseControl()
        {
 
        }

        /// <summary>
        /// Fires the update main view.
        /// </summary>
        protected void FireUpdateMainView()
        {
            if (UpdateMainView != null)
            {
                UpdateMainView(this, new EventArgs());
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public CommonBaseControl(CommonBaseTab parentTab)
        {
            ParentTab = parentTab;
        }

        /// <summary>
        /// Gets or sets the parent tab.
        /// </summary>
        /// <value>The parent tab.</value>
        public CommonBaseTab ParentTab
        {
            get;
            set;
        }
    }
}
