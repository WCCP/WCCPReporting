﻿namespace SoftServe.Reports.MarketProgramms
{
    partial class ProgrammListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgrammListControl));
          this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
          this.barManager = new DevExpress.XtraBars.BarManager(this.components);
          this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
          this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
          this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
          this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
          this.bar1 = new DevExpress.XtraBars.Bar();
          this.gridControl = new DevExpress.XtraGrid.GridControl();
          this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
          this.SuspendLayout();
          // 
          // imCollection
          // 
          this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
          this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
          this.imCollection.Images.SetKeyName(0, "New.png");
          this.imCollection.Images.SetKeyName(1, "View.png");
          this.imCollection.Images.SetKeyName(2, "Refresh.png");
          this.imCollection.Images.SetKeyName(3, "Close.png");
          // 
          // barManager
          // 
          this.barManager.AllowCustomization = false;
          this.barManager.AllowMoveBarOnToolbar = false;
          this.barManager.AllowQuickCustomization = false;
          this.barManager.AllowShowToolbarsPopup = false;
          this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
          this.barManager.DockControls.Add(this.barDockControlTop);
          this.barManager.DockControls.Add(this.barDockControlBottom);
          this.barManager.DockControls.Add(this.barDockControlLeft);
          this.barManager.DockControls.Add(this.barDockControlRight);
          this.barManager.Form = this;
          this.barManager.Images = this.imCollection;
          this.barManager.MaxItemId = 0;
          // 
          // bar1
          // 
          this.bar1.BarName = "Tools";
          this.bar1.DockCol = 0;
          this.bar1.DockRow = 0;
          this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
          this.bar1.Text = "Tools";
          // 
          // gridControl
          // 
          this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
          this.gridControl.Location = new System.Drawing.Point(0, 24);
          this.gridControl.MainView = this.gridView;
          this.gridControl.MenuManager = this.barManager;
          this.gridControl.Name = "gridControl";
          this.gridControl.Size = new System.Drawing.Size(800, 323);
          this.gridControl.TabIndex = 4;
          this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
          // 
          // gridView
          // 
          this.gridView.GridControl = this.gridControl;
          this.gridView.Name = "gridView";
          // 
          // ProgrammListControl
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.AutoScroll = true;
          this.Controls.Add(this.gridControl);
          this.Controls.Add(this.barDockControlLeft);
          this.Controls.Add(this.barDockControlRight);
          this.Controls.Add(this.barDockControlBottom);
          this.Controls.Add(this.barDockControlTop);
          this.Name = "ProgrammListControl";
          this.Size = new System.Drawing.Size(800, 347);
          this.Load += new System.EventHandler(this.ClientListControl_Load);
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;


    }
}
