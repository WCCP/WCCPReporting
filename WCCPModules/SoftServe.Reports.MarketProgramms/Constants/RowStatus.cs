using System;

namespace SoftServe.Reports.MarketProgramms
{
  public enum RowStatus
  {
    New,
    Unchanged,
    Updated,
    Deleted
  }
}
