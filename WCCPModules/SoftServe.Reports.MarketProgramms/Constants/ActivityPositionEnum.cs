namespace SoftServe.Reports.MarketProgramms {
    public enum ActivityPositionEnum {
        Initiator = 1, ControlGroupSelector, PerformerInRetail, GoalsSetter, Approver, FactController
    }
}