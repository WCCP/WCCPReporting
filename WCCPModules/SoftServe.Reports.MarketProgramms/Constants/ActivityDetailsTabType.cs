namespace SoftServe.Reports.MarketProgramms {
    /// <summary>
    /// Tab type of activity details control
    /// </summary>
    public enum ActivityDetailsTabType {
        /// <summary>
        /// Common tab
        /// </summary>
        Common = 0,

        /// <summary>
        /// Config goals tab
        /// </summary>
        GoalsConfig = 1,

        /// <summary>
        /// Goals tab
        /// </summary>
        Goals = 2,

        /// <summary>
        /// Control group tab
        /// </summary>
        OnInvoice = 3,

        /// <summary>
        /// Control group tab
        /// </summary>
        ControlGroup = 4,

        /// <summary>
        /// Promo group tab
        /// </summary>
        PromoGroup = 5
    }
}