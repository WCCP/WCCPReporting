﻿namespace SoftServe.Reports.MarketProgramms {
    /// <summary>
    /// 
    /// </summary>
    public static class Constants {
        public const int MODULE_ID = 3;

        public const int TAB_MAX_COUNT = 10;
        public const string DELIMITER_ID_LIST = ",";
        public const string M3_LEVEL = "3";
        public const string SPLIT_BY_REGION = "0";
        
        public const string PROC_GET_CHANELS = "spDW_ChanelTypeList";
        public const string FLD_FILTER_PREVIEW_OUTLET_ID = "OUTLET_CODE";
        public const string FLD_CHANEL_ID = "ChanelType_id";
        public const string FLD_CHANEL_NAME = "ChanelType";

        public const string XML_OUTLET_ROOT_NAME = "root";
        public const string XML_OUTLET_ELEMENT_NAME = "row";
        public const string XML_OUTLET_ATTRIBUTE_FIELD_ID = "field_id";

        #region SP parameters

        public const string PARAM_ACTION_id = "@ACTION_ID";
        public const string PARAM_ACTION_ID = "@Action_ID";
        public const string PARAM_ACTIONID = "@ActionId";
        public const string PARAM_TEMPLATEID = "@TemplateId";
        public const string PARAM_USERLOGIN = "@UserLogin";
        public const string PARAM_USERPOSITION = "@UserPosition";

        #endregion

        #region Common SP

        public const string SP_GET_ACTIONS_LIST = "dbo.spDW_MP_GetListActions";
        public const string SP_GET_TEMPLATES_LIST = "dbo.spDW_MP_GetListActionTemplates";
        public const string SP_GET_REGIONS_TREE = "dbo.spDW_MP_GetTerritoryFilter";
        public const string SP_CHECK_POSITION = "dbo.spDW_CheckUserPosition";
        public const string SP_GET_USER_LEVEL = "dbo.spDW_GetUserLevel";

        #endregion

        #region Common tab

        public const string SP_GET_ACTIVITY_LEVELS_LIST = "dbo.spDW_MP_GetActionLevelList";
        public const string SP_GET_TEMPLATE_DETAILS = "dbo.spDW_MP_GetActionTemplate";
        public const string SP_GET_ACTION_DETAILS = "dbo.spDW_MP_GetActionDetails";
        public const string SP_UPDATE_ACTION_DETAILS = "dbo.spDW_MP_UpdateAction";
        public const string SP_INSERT_ACTION_DETAILS = "dbo.spDW_MP_InsertAction";

        // Common tab SP params
        public const string PARAM_ACTION_NAME = "@Action_Name";
        public const string SP_UPDATE_ACTION_PARAM_4 = "ActionType_ID";
        public const string SP_UPDATE_ACTION_PARAM_5 = "Action_Level";
        public const string SP_UPDATE_ACTION_PARAM_6 = "isApproved";
        public const string SP_UPDATE_ACTION_PARAM_7 = "Action_Start";
        public const string SP_UPDATE_ACTION_PARAM_8 = "Action_End";
        public const string SP_UPDATE_ACTION_PARAM_9 = "Base1_Start";
        public const string SP_UPDATE_ACTION_PARAM_10 = "Base1_End";
        public const string SP_UPDATE_ACTION_PARAM_11 = "Base2_Start";
        public const string SP_UPDATE_ACTION_PARAM_12 = "Base2_End";
        public const string SP_UPDATE_ACTION_PARAM_13 = "Monitoring_After_Action_Month";
        public const string SP_UPDATE_ACTION_PARAM_14 = "isBaseFromTR";
        public const string SP_UPDATE_ACTION_PARAM_15 = "Action_Description";
        public const string SP_UPDATE_ACTION_PARAM_16 = "isShowMobile";
        public const string SP_UPDATE_ACTION_PARAM_17 = "isCheckDateMobile";
        public const string SP_UPDATE_ACTION_PARAM_18 = "MobileCommentTemplate";
        public const string SP_UPDATE_ACTION_PARAM_22 = "MarketTrend";
        public const string SP_UPDATE_ACTION_PARAM_23 = "Uplift_Plan";
        public const string SP_UPDATE_ACTION_PARAM_24 = "MidMACOPerDKL_Plan";
        public const string SP_UPDATE_ACTION_PARAM_25 = "Outlet_Qnt";
        public const string SP_UPDATE_ACTION_PARAM_26 = "PlannedOutletConnections";
        public const string SP_UPDATE_ACTION_PARAM_27 = "isEnableMoreOneAim";
        public const string SP_UPDATE_ACTION_MECHANICPRICETYPE_ID = "@MechanicPriceType_ID";



        public const string SP_DELETE_ACTION_DETAILS = "spDW_MP_DeleteAction";

        public const string SP_GET_DOCS_LIST = "spDW_MPT_GetPresentationList";

        public const string SP_GET_DOCS_DATA = "spDW_MPT_GetPresentationData";
        public const string SP_GET_DOCS_DATA_PARAM_1 = "Presentation_ID";

        public const string SP_INSERT_DOCS_DATA = "spDW_MPT_InsertPresentation";
        public const string SP_INSERT_DOCS_DATA_PARAM_2 = "Presentation_FileName";
        public const string SP_INSERT_DOCS_DATA_PARAM_3 = "Presentation_Data";

        public const string SP_DELETE_DOCS_DATA = "spDW_MPT_DeletePresentation";
        public const string SP_DELETE_DOCS_DATA_PARAM_1 = "Presentation_ID";

        #region Fields returned by DataProvider.GetActivityDetails method

        public const string SP_ACTIVITY_DETAILS_USERNAME = "UserName";
        public const string SP_ACTIVITY_DETAILS_CREATION_DATE = "DCR";
        public const string SP_ACTIVITY_DETAILS_ISAPPROVED = "isApproved";
        public const string SP_ACTIVITY_DETAILS_ACTION_NAME = "Action_Name";
        public const string SP_ACTIVITY_DETAILS_ACTION_LEVEL = "Action_Level";
        public const string SP_ACTIVITY_DETAILS_ACTIONTYPE_NAME = "ActionType_Name";
        public const string SP_ACTIVITY_DETAILS_CHANELTYPE = "ChanelType";
        public const string SP_ACTIVITY_DETAILS_ACTION_DESCRIPTION = "Action_Description";
        public const string SP_ACTIVITY_DETAILS_ACTION_START = "Action_Start";
        public const string SP_ACTIVITY_DETAILS_ACTION_END = "Action_End";
        public const string SP_ACTIVITY_DETAILS_BASE1_START = "Base1_Start";
        public const string SP_ACTIVITY_DETAILS_BASE1_END = "Base1_End";
        public const string SP_ACTIVITY_DETAILS_BASE2_START = "Base2_Start";
        public const string SP_ACTIVITY_DETAILS_BASE2_END = "Base2_End";
        public const string SP_ACTIVITY_DETAILS_ISBASEFROMTR = "isBaseFromTR";
        public const string SP_ACTIVITY_DETAILS_MONITORING_AFTER_ACTION_MONTH = "Monitoring_After_Action_Month";
        public const string SP_ACTIVITY_DETAILS_INITIATOR = "Initiator";
        public const string SP_ACTIVITY_DETAILS_PLANNER = "Planner";
        public const string SP_ACTIVITY_DETAILS_CONTROLLER = "Controller";
        public const string SP_ACTIVITY_DETAILS_APPROVER = "Approver";
        public const string SP_ACTIVITY_DETAILS_EXECUTOR = "Executor";
        public const string SP_ACTIVITY_DETAILS_OLCHECKER = "OlChecker";
        //public const string SP_ACTIVITY_DETAILS_OL_EDIT_CASCADE = "OL_Edit_Cascade";
        //public const string SP_ACTIVITY_DETAILS_OL_ADD_MANUALLY = "OL_Add_Manually";
        //public const string SP_ACTIVITY_DETAILS_OL_CHECK_TR = "OL_Check_TR";
        public const string SP_ACTIVITY_DETAILS_OL_TT_COUNT = "PROMO_OL_ALL_COUNT";
        public const string SP_ACTIVITY_DETAILS_OL_TT_AVAILABLE = "PROMO_OL_USER_COUNT";
        //public const string SP_ACTIVITY_DETAILS_OL_CHECK_COMPLIANCE = "OL_Check_Compliance";
        public const string SP_ACTIVITY_DETAILS_ISSHOWMOBILE = "isShowMobile";
        public const string SP_ACTIVITY_DETAILS_ISCHECKDATEMOBILE = "isCheckDateMobile";
        public const string SP_ACTIVITY_DETAILS_MOBILECOMMENTTEMPLATE = "MobileCommentTemplate";
        //public const string SP_ACTIVITY_DETAILS_CNT_EDIT_CASCADE = "Cnt_Edit_Cascade";
        //public const string SP_ACTIVITY_DETAILS_CNT_ADD_MANUALLY = "Cnt_Add_Manually";
        //public const string SP_ACTIVITY_DETAILS_CNT_CHECK_TR = "Cnt_Check_TR";
        public const string SP_ACTIVITY_DETAILS_CNT_TT_COUNT = "CONTROL_OL_ALL_COUNT";
        public const string SP_ACTIVITY_DETAILS_CNT_TT_AVAILABLE = "CONTROL_OL_USER_COUNT";
        //public const string SP_ACTIVITY_DETAILS_CNT_CHECK_SALESBL = "Cnt_Check_SalesBL";
        //public const string SP_ACTIVITY_DETAILS_CNT_CHECK_SALESPERIOD = "Cnt_Check_SalesPeriod";
        public const string SP_ACTIVITY_DETAILS_ACTIONTYPE_ID = "ActionType_ID";
        public const string SP_ACTIVITY_DETAILS_IS_BUDGET = "IS_BUDGET";
        public const string SP_ACTIVITY_DETAILS_IS_BUDGET_WORK = "IS_BUDGET_WORK";
        public const string SP_ACTIVITY_DETAILS_OUTLET_QUANTITY = "Outlet_Qnt";
        
        #endregion

        #region Fields returned by DataProvider.GetTemplateDetails method

        public const string SP_TEMPLATE_DETAILS_ACTION_ID = "Action_ID";
        public const string SP_TEMPLATE_DETAILS_ACTION_START_MONTH = "Action_Start_Month";
        public const string SP_TEMPLATE_DETAILS_ACTION_START_DAY = "Action_Start_Day";
        public const string SP_TEMPLATE_DETAILS_ACTION_END_MONTH = "Action_End_Month";
        public const string SP_TEMPLATE_DETAILS_ACTION_END_DAY = "Action_End_Day";
        public const string SP_TEMPLATE_DETAILS_BASE1_START_YEAR_OFFSET = "Base1_Start_Year_OffSet";
        public const string SP_TEMPLATE_DETAILS_BASE1_START_MONTH = "Base1_Start_Month";
        public const string SP_TEMPLATE_DETAILS_BASE1_START_DAY = "Base1_Start_Day";
        public const string SP_TEMPLATE_DETAILS_BASE1_END_YEAR_OFFSET = "Base1_End_Year_OffSet";
        public const string SP_TEMPLATE_DETAILS_BASE1_END_MONTH = "Base1_End_Month";
        public const string SP_TEMPLATE_DETAILS_BASE1_END_DAY = "Base1_End_Day";
        public const string SP_TEMPLATE_DETAILS_BASE2_START_YEAR_OFFSET = "Base2_Start_Year_OffSet";
        public const string SP_TEMPLATE_DETAILS_BASE2_START_MONTH = "Base2_Start_Month";
        public const string SP_TEMPLATE_DETAILS_BASE2_START_DAY = "Base2_Start_Day";
        public const string SP_TEMPLATE_DETAILS_BASE2_END_YEAR_OFFSET = "Base2_End_Year_OffSet";
        public const string SP_TEMPLATE_DETAILS_BASE2_END_MONTH = "Base2_End_Month";
        public const string SP_TEMPLATE_DETAILS_BASE2_END_DAY = "Base2_End_Day";
        public const string SP_TEMPLATE_DETAILS_ISBASEFROMTR = "isBaseFromTR";
        public const string SP_TEMPLATE_DETAILS_MONITORING_AFTER_ACTION_MONTH = "Monitoring_After_Action_Month";
        public const string SP_TEMPLATE_DETAILS_INITIATOR = "Initiator";
        public const string SP_TEMPLATE_DETAILS_PLANNER = "Planner";
        public const string SP_TEMPLATE_DETAILS_CONTROLLER = "Controller";
        public const string SP_TEMPLATE_DETAILS_APPROVER = "Approver";
        public const string SP_TEMPLATE_DETAILS_EXECUTOR = "Executor";
        public const string SP_TEMPLATE_DETAILS_OLCHECKER = "OlChecker";
        public const string SP_TEMPLATE_DETAILS_OL_EDIT_CASCADE = "OL_Edit_Cascade";
        public const string SP_TEMPLATE_DETAILS_OL_ADD_MANUALLY = "OL_Add_Manually";
        public const string SP_TEMPLATE_DETAILS_OL_CHECK_TR = "OL_Check_TR";
        //public const string SP_TEMPLATE_DETAILS_OL_CHECK_COMPLIANCE = "OL_Check_Compliance";
        public const string SP_TEMPLATE_DETAILS_ISSHOWMOBILE = "isShowMobile";
        public const string SP_TEMPLATE_DETAILS_ISCHECKDATEMOBILE = "isCheckDateMobile";
        public const string SP_TEMPLATE_DETAILS_MOBILECOMMENTTEMPLATE = "MobileCommentTemplate";
        public const string SP_TEMPLATE_DETAILS_CNT_EDIT_CASCADE = "Cnt_Edit_Cascade";
        public const string SP_TEMPLATE_DETAILS_CNT_ADD_MANUALLY = "Cnt_Add_Manually";
        public const string SP_TEMPLATE_DETAILS_CNT_CHECK_TR = "Cnt_Check_TR";
        //public const string SP_TEMPLATE_DETAILS_CNT_CHECK_SALESBL = "Cnt_Check_SalesBL";
        //public const string SP_TEMPLATE_DETAILS_CNT_CHECK_SALESPERIOD = "Cnt_Check_SalesPeriod";
        public const string SP_TEMPLATE_DETAILS_ACTIONTYPE_ID = "ActionType_ID";
        public const string SP_TEMPLATE_DETAILS_USERNAME = "UserName";
        public const string SP_TEMPLATE_DETAILS_ISAPPROVED = "isApproved";
        public const string SP_TEMPLATE_DETAILS_ACTION_LEVEL = "Action_Level";
        public const string SP_TEMPLATE_DETAILS_ACTIONTYPE_NAME = "ActionType_Name";
        public const string SP_TEMPLATE_DETAILS_CHANELTYPE = "ChanelType";
        public const string SP_TEMPLATE_DETAILS_ACTION_DESCRIPTION = "Action_Description";
        public const string SP_TEMPLATE_DETAILS_ACTION_DESCRIPTION_DISABLED = "DisableChangeActivityDescription";
        public const string SP_TEMPLATE_DETAILS_ACTION_NAME = "Action_Name";
        
        #endregion
        
        #endregion

        #region Config Goals tab

        public const string SP_GET_GOAL_TYPES_LIST = "spDW_GetAimTypes";

        public const string SP_GET_AIM_LIST = "spDW_MPT_ListAim";

        public const string SP_GET_SKU_TREE_AIM = "spDW_GetSKUTreeAim2";
        public const string SP_GET_SKU_TREE_AIM_PARAM1 = "Aim_ID";

        public const string SP_GET_KPI_TREE_AIM = "spDW_GetKPITreeAim";
        public const string SP_GET_KPI_TREE_AIM_PARAM1 = "Aim_ID";

        public const string SP_GET_QPQ_TREE_AIM = "spDW_QuestionnaireGetTree";
        public const string SP_GET_QPQ_TREE_AIM_PARAM_ID = "@AimID";
        public const string SP_GET_QPQ_TREE_AIM_PARAM_Type = "@AimTypeID";

        public const string SP_UPDATE_QPQ_AIM = "spDW_QuestionnaireUpdateAim";
        public const string SP_UPDATE_QPQ_AIM_PARAM_ID = "@AimID";
        public const string SP_UPDATE_QPQ_AIM_PARAM_Type = "@AimTypeID";
        public const string SP_UPDATE_QPQ_AIM_DOCITM = "@DocItm";
        public const string SP_UPDATE_QPQ_AIM_ISIN_REVERSE_EXEC = "@IsInReverseExec";

        public const string SP_GET_IPTR_LIST_AIM = "spDW_IPTR_EquipmentClassList";

        public const string SP_GET_IPTR_AIM = "spDW_MPT_GetIPTRAim";
        public const string SP_GET_IPTR_AIM_PARAM1 = "Aim_ID";

        public const string SP_UPDATE_AIM = "spDW_MPT_UpdateAim";
        public const string SP_UPDATE_AIM_PARAM2 = "Aim_Name";
        public const string SP_UPDATE_AIM_PARAM3 = "AimType_Id";
        public const string SP_UPDATE_AIM_PARAM4 = "isUseInCompleted";
        public const string SP_UPDATE_AIM_PARAM5 = "isPeriodCalcAim";
        public const string SP_UPDATE_AIM_PARAM6 = "@Aim_Id";
        public const string SP_UPDATE_AIM_PARAM7 = "Id";

        public const string SP_UPDATE_AIM_PARAM8 = "@FactCalcPeriod";
        public const string SP_UPDATE_AIM_PARAM9 = "@FactCalcType";

        public const string SP_UPDATE_AIM_PARAM10 = "@isUseAimValue";
        public const string SP_UPDATE_AIM_PARAM11 = "@IsInReverseExec";
        public const string SP_UPDATE_AIM_PARAM12 = "@isAccPerfOfTargetVal";

        public const string SP_UPDATE_KPI_AIM = "spDW_MPT_UpdateKPIAim";
        public const string SP_UPDATE_KPI_AIM_PARAM1 = "Aim_ID";
        public const string SP_UPDATE_KPI_AIM_PARAM2 = "Data";

        public const string SP_UPDATE_SKU_AIM = "spDW_MPT_UpdateSKUAim2";
        public const string SP_UPDATE_SKU_AIM_PARAM1 = "Aim_Id";
        public const string SP_UPDATE_SKU_AIM_PARAM2 = "Data";

        public const string SP_UPDATE_IPTR_AIM = "spDW_MPT_UpdateIPTRAim";
        public const string SP_UPDATE_IPTR_AIM_PARAM1 = "Aim_Id";
        public const string SP_UPDATE_IPTR_AIM_PARAM2 = "EquipmentClass_ID";

        public const string SP_DELETE_AIM = "spDW_MPT_DeleteAim";
        public const string SP_DELETE_AIM_PARAM1 = "Aim_Id";

        //public const string SP_UPDATE_SKU_TREE = "spDW_MP_UpdateSKU";
        //public const string SP_UPDATE_SKU_TREE_PARAM2 = "SKUList";

        //public const string SP_UPDATE_KPI_TREE = "spDW_MP_UpdateKPI";
        //public const string SP_UPDATE_KPI_TREE_PARAM2 = "Data";
        #endregion

        #region Goals tab
        //new
        public const string SP_GET_PLANS_FACTS = "spDW_MP_ADDR_AimMonth";
        public const string SP_GET_PLANS_FACTS_PARAM_2 = "Action_Start";
        public const string SP_GET_PLANS_FACTS_PARAM_3 = "Action_End";
        public const string SP_GET_PLANS_FACTS_PARAM_4 = "isShowFact";

        public const string SP_CHECK_OLS = "spDW_MP_CheckAim";
        public const string SP_CHECK_OLS_PARAM_2 = "YearMonth";
        public const string SP_CHECK_OLS_PARAM_3 = "Data";

        public const string SP_LOAD_PLANS = "spDW_MP_LoadAim";
        public const string SP_LOAD_PLANS_PARAM_2 = "YearMonth";
        public const string SP_LOAD_PLANS_PARAM_3 = "Data";
        //

        public const string SP_GET_PLANNED_EXPENSES_PER_OUTLET = "spDW_MP_Aim_PlannedExpensesPerOutlet";

        public const string SP_ADD_OUTLET_EXPENSE = "spDW_MP_AIM_AddOutletExpense";
        public const string SP_ADD_OUTLET_EXPENSE_PARAM_2 = "EXPENSE_ID";
        public const string SP_ADD_OUTLET_EXPENSE_PARAM_3 = "COST";

        public const string SP_UPDATE_OUTLET_EXPENSE = "spDW_MP_AIM_UpdateOutletExpense";
        public const string SP_UPDATE_OUTLET_EXPENSE_PARAM_1 = "EXPENSE_ID";
        public const string SP_UPDATE_OUTLET_EXPENSE_PARAM_2 = "COST";

        public const string SP_DELETE_OUTLET_EXPENSE = "spDW_MP_AIM_DeleteOutletExpense";
        public const string SP_DELETE_OUTLET_EXPENSE_PARAM_1 = "EXPENSE_ID";

        public const string SP_GET_TARGET_DIMENTIONS_TREE = "spDW_MP_GetTargetDimentionsTree";
        public const string SP_GET_TARGET_DIMENTIONS_TREE_PARAM_2 = "Action_Level";

        public const string SP_UPDATE_TARGET_DIMENTIONS_TREE = "spDW_MP_UpdateKPIExplicit";
        public const string SP_UPDATE_TARGET_DIMENTIONS_TREE_PARAM_2 = "data";

        // mechanics
        public const string SP_DW_PG_GET_DISCOUNT_MECHANICS = "spDW_PG_GetDiscountMechanics";
        public const string SP_DW_PG_GET_BONUS_MECHANICS = "spDW_PG_GetBonusMechanics";
        public const string SP_DW_PG_GET_PRICE_TYPES = "spDW_PG_GetPriceTypes";

        public const string SP_DW_PG_GET_CONCURRENT_CN = "spDW_PG_GetConcurrentCn";
        public const string SP_DW_PG_GET_BONUS_PRODUCT = "spDW_PG_GetBonusProduct";

        public const string SP_DW_PG_UPDATE_DISCOUNT_MECHANICS = "spDW_PG_UpdateDiscountMechanics";

        public const string PAR_ID = "@ID";
        public const string PAR_PRODUCTCN_ID = "@ProductCn_ID";
        public const string PAR_CONCURRENTCN_ID = "@ConcurrentCn_ID";
        public const string PAR_PRICETYPE_ID = "@PriceType_ID";
        public const string PAR_PRICE = "@Price";
        public const string PAR_OVERVALUE = "@Overvalue";

        public const string SP_DW_PG_UPDATE_BONUS_MECHANICS = "spDW_PG_UpdateBonusMechanics";
        public const string PAR_BONUSPRODUCT_ID = "@BonusProduct_ID";
        public const string PAR_CORRECTIONRATE = "@CorrectionRate";
        public const string PAR_MAXDISCOUNTPRC = "@MaxDiscountPrc";

        public const string SP_DW_PG_DELETE_MECHANICS = @"spDW_PG_DeleteMechanics";

        #endregion

        #region On-Invoice tab
        //SP
        public const string SP_DW_MP_GET_ONINVOICE_AIM = "dbo.spDW_MP_GetOnInvoiceAim";
        public const string SP_DW_MP_SET_ONINVOICE_ITEM = "dbo.spDW_MP_SetOnInvoiceItem";
        public const string SP_DW_MP_IMPORT_ONINVOICE_ITEM = "dbo.spDW_MP_ImportOnInvoiceItem";
        public const string SP_DW_MP_DELETE_ONINVOICE_ITEM = "dbo.spDW_MP_DeleteOnInvoiceItem";
        public const string SP_DW_MP_GET_ONINVOICE_PROMO_OUTLETS = "dbo.spDW_MP_GetOnInvoicePromoOutlets";
        public const string SP_DW_MP_GET_ONINVOICE_OUTLETS = "dbo.spDW_MP_GetOnInvoiceOutlets";
        public const string SP_DW_MP_EXPORT_TO_SW = "dbo.spDW_OnInvoice_ExportToSW";
        //params
        public const string PARAM_INCOME_ACTION_ID = "@Income_Action_ID";
        public const string PARAM_AIM_ID = "@Aim_Id";
        public const string PARAM_OL_ID = "@OL_id";
        public const string PARAM_ITEM_ID = "@Item_id";
        public const string PARAM_DATEFROM = "@DateFrom";
        public const string PARAM_DATETO = "@DateTo";
        public const string PARAM_DISCOUNTSUM = "@DiscountSum";
        public const string PARAM_DISCOUNTPERCENT = "@DiscountPercent";

        #endregion

        #region Control and Promo group tabs

        public const string SP_GET_ACTION_OUTLETS = "spDW_MP_ADDR_ListActionOutlets";
        public const string SP_GET_ACTION_OUTLETS_PARAM_2 = "ShowControlOutlets";

        public const string SP_GET_OUTLET_DATA = "spDW_MP_ADDR_GetOutletData";
        public const string SP_GET_OUTLET_DATA_PARAM_2 = "ListOutlets";

        public const string SP_RECULC_OTLET_DATA = "spDW_MP_ADDR_SetOutletVolume";
        public const string SP_ADD_OUTLET = "spDW_MP_AddOutlets";
        public const string SP_ADD_OUTLET_PARAM_2 = "listOutlets";
        public const string SP_ADD_OUTLET_PARAM_3 = "isCnt";
        public const string SP_ADD_OUTLET_PARAM_4 = "AddComment";
        public const string SP_ADD_OUTLET_PARAM_5 = "Filter_ID";

        public const string SP_DELETE_OUTLET = "spDW_MP_DeleteOutlets";
        public const string SP_DELETE_OUTLET_PARAM_2 = "OutletsList";
        public const string SP_DELETE_OUTLET_PARAM_3 = "isOwn";

        public const string SP_UPDATE_OUTLET = "spDW_MP_ADDR_SET_EDITABLE_COLUMNS";
        public const string SP_UPDATE_OUTLET_PARAM_2 = "XML_DATA";

        public const string SP_GET_ACTIVATION_STATUSES = "spDW_MP_ADDR_GetActivationStatuses";
        public const string SP_GET_ACTIVATION_STATUSES_FLD_ID = "ACTIVATION_STATUS_ID";

        public const string SP_GET_DECLINE_REASONS = "spDW_MP_ADDR_GetDeclineReasons";

        public const string FIELD_CHANNEL_ID = "ChanelType_Id";

        #endregion

        #region Error handling

        public const string SP_ERROR_NUMBER = "ErrorNumber";
        public const string SP_ERROR_CODE = "ErrorCode";
        public const string SP_ERROR_DESCRIPTION = "ErrorMessage";

        public const int ERROR_ACTIVITY_WITH_THIS_NAME_EXISTS = -1;
        
        #endregion
    }

    /// <summary>
    /// Action Level.
    /// </summary>
    public enum LevelType {
        /// <summary>
        /// Unknown action level.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// DSM action level
        /// </summary>
        M3 = 3,
        /// <summary>
        /// Region action level
        /// </summary>
        Region = 4,
        /// <summary>
        /// Country action level
        /// </summary>
        Country = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SplitPOCByType {
        /// <summary>
        /// 
        /// </summary>
        ByRegion = 0,
        /// <summary>
        /// 
        /// </summary>
        ByDistrict = 1,
        /// <summary>
        /// 
        /// </summary>
        ByM3 = 2,
        /// <summary>
        /// 
        /// </summary>
        ByM2 = 3,
        /// <summary>
        /// 
        /// </summary>
        ByPOC = 4
    }

    public enum GoalType {
        SalesVolume = 1,
        Merchandising = 2,
        Effectiveness = 3,
        OnInvoice = 4,
        QuestionnaireLogic = 5,
        QuestionnaireDigit = 6,
        InBevDoorCount = 7,
        MustStock = 8
    }

    public enum CheckByVisitsType {
        ByLastVisit = 0,
        ByMonth = 1
    }

    public enum AimRecordState {
        Normal = 0,
        Updated = 1,
        Deleted = 2,
        New = 3,
        Unknown = 4
    }

    public enum FactCalcType
    {
        FactSales = 1,
        FactRemainder = 2
    }
}
