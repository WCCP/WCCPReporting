﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace SoftServe.Reports.TasksReport.Models
{
    public class StaffItem
    {
        [MapField("ID"), PrimaryKey]
        public int Id { get; set; }

        [MapField("PARENT_ID")]
        public int ParentId { get; set; }

        [MapField("DATA")]
        public string Name { get; set; }

        [MapField("DATA_ID")]
        public int? StaffId { get; set; }

        [MapField("LEVEL")]
        public int Level { get; set; }

        [MapField("ChannelMask")]
        public int ChannelMask { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
