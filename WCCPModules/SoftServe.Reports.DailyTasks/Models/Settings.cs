﻿using System;
using System.Collections.Generic;

namespace SoftServe.Reports.TasksReport.Models
{
    public class Settings
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public List<int> StaffIds { get; set; }
        public string StaffList
        {
            get
            {
                if (StaffIds == null)
                    return string.Empty;

                return string.Join(",", StaffIds.ConvertAll(i => i.ToString()).ToArray());
            }
        }

        public bool MmTasksOnly { get; set; }
    }
}
