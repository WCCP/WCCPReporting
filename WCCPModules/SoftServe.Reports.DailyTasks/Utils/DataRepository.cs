﻿using System.Collections.Generic;
using SoftServe.Reports.TasksReport.DataAccess;
using SoftServe.Reports.TasksReport.Models;

namespace SoftServe.Reports.TasksReport.Utils
{
    public static class DataRepository
    {
        private static List<Channel> _channels;
        private static List<StaffItem> _staff;

        public static List<Channel> Channels
        {
            get
            {
                if (_channels == null)
                {
                    _channels = DataProvider.GetChannels();
                }

                return _channels;
            }
        }

        public static List<StaffItem> Staff {
            get
            {
                if (_staff == null)
                {
                    _staff = DataProvider.GetStaffTree();
                }

                return _staff;
            }
        }
    }
}
