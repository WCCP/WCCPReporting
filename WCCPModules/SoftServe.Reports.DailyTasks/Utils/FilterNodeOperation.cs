﻿using System;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace SoftServe.Reports.TasksReport.Utils
{
    public class FilterNodeOperation : TreeListOperation
    {
        private string _colLevelName;
        private string _colMaskName;

        private int _level;
        private byte _mask;

        public FilterNodeOperation(string colLevelName, int level, string colMaskName, byte mask)
        {
            _level = level;
            _mask = mask;

            _colLevelName = colLevelName;
            _colMaskName = colMaskName;
        }

        public override void Execute(TreeListNode node)
        {
            if (NodeSatisfiesCondition(node))
            {
                node.Visible = true;
                if (node.ParentNode != null)
                {node.ParentNode.Visible = true;}
            }
            else
            {
                node.Visible = false;
            }
        }

        bool NodeSatisfiesCondition(TreeListNode node)
        {
            int lLevel = Convert.ToInt32(node[_colLevelName]);
            return lLevel >= _level && (Convert.ToUInt16(node[_colMaskName]) & _mask) > 0;
        }
    }
}
