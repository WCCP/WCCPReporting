﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using SoftServe.Reports.TasksReport.Models;
using SoftServe.Reports.TasksReport.Utils;

namespace SoftServe.Reports.TasksReport
{
    public partial class SettingsForm : XtraForm
    {
        public Settings Settings { get; set; }

        public SettingsForm()
        {
            InitializeComponent();
        }

        private void GetSettings()
        {
            Settings = new Settings();

            Settings.From = dFromCreation.DateTime;
            Settings.To = dToCreation.DateTime;

            Settings.MmTasksOnly = cMmTasksOnly.Checked;

            Settings.StaffIds = GetM1Checked();
        }

        public void LoadDataSources()
        {
            LoadChannels(DataRepository.Channels);
            LoadStaff(DataRepository.Staff);

            SetDefaults();
        }

        private void SetDefaults()
        {
            cStaffAll.Checked = true;
            tStaff.CheckAll();

            foreach (CheckedListBoxItem channel in cStaffChannel.Properties.Items)
            {
                channel.CheckState = CheckState.Checked;
            }

            LoadDates();
        }

        private void LoadDates()
        {
            dToCreation.DateTime = DateTime.Now.Date;
            dFromCreation.DateTime = dToCreation.DateTime.AddDays(-1);
        }

        private void LoadStaff(List<StaffItem> list)
        {
            tStaff.DataSource = list;
            tStaff.ExpandAll();
            tStaff.CollapseAll();
        }

        private void LoadChannels(List<Channel> list)
        {
            foreach (Channel channel in list)
            {
                cStaffChannel.Properties.Items.Add(channel.Mask, channel.Name);
            }
        }

        private List<int> GetM1Checked()
        {
            CheckedNodesOperation lFirstLevelOperation = new CheckedNodesOperation(colLevel.FieldName, 1, colChannelMask.FieldName, GetChannelMask(), colStaffId.FieldName);
            tStaff.NodesIterator.DoOperation(lFirstLevelOperation);

            return lFirstLevelOperation.Ids;
        }

        private void GetStaffAllCheckState()
        {
            int lChecked = tStaff.Nodes.Count(n => n.CheckState == CheckState.Checked && n.Visible);
            int lUnhecked = tStaff.Nodes.Count(n => n.CheckState == CheckState.Unchecked && n.Visible);
            int lVisible = tStaff.Nodes.Count(n => n.Visible);

            cStaffAll.EditValueChanging -= cStaffAll_EditValueChanging;
            cStaffAll.CheckStateChanged -= cStaffAll_CheckStateChanged;

            if (lChecked == lVisible)
            {
                cStaffAll.CheckState = CheckState.Checked;
            }
            else if (lUnhecked == lVisible)
            {
                cStaffAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                cStaffAll.CheckState = CheckState.Indeterminate;
            }

            cStaffAll.EditValueChanging += cStaffAll_EditValueChanging;
            cStaffAll.CheckStateChanged += cStaffAll_CheckStateChanged;
        }

        private void FilterStaffTree()
        {
            byte lChannelMask = GetChannelMask();

            tStaff.BeginUpdate();
            FilterNodeOperation lOperation = new FilterNodeOperation(colLevel.FieldName, 1, colChannelMask.FieldName, lChannelMask);
            tStaff.NodesIterator.DoOperation(lOperation);
            tStaff.EndUpdate();

            GetStaffAllCheckState();
        }

        private byte GetChannelMask()
        {
            return Convert.ToByte(cStaffChannel.Properties.Items.GetCheckedValues().Sum(v => Convert.ToByte(v)));
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            GetSettings();
            
            if (Settings.StaffIds.Count > 0)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Нужно выбрать персонал", "Параметры", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        
        private void bCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void cStaffAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (cStaffAll.CheckState == CheckState.Checked)
            {
                tStaff.BeginUpdate();
                tStaff.CheckAll();
                tStaff.EndUpdate();
            }

            if (cStaffAll.CheckState == CheckState.Unchecked)
            {
                tStaff.BeginUpdate();
                tStaff.UncheckAll();
                tStaff.EndUpdate();
            }
        }

        private void cStaffAll_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.NewValue == null)
            {
                e.NewValue = !Convert.ToBoolean(e.OldValue);
            }
        }

        private void tStaff_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            GetStaffAllCheckState();
        }

        private void cStaffChannel_EditValueChanged(object sender, EventArgs e)
        {
            FilterStaffTree();

            cStaffAll.CheckState = CheckState.Checked;
            tStaff.CollapseAll();
        }

        private void datesCreation_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(dFromCreation))
            {
                dToCreation.Properties.MinValue = !string.IsNullOrEmpty(dFromCreation.Text) ? dFromCreation.DateTime : DateTime.MinValue;
            }

            if (sender.Equals(dToCreation))
            {
                dFromCreation.Properties.MaxValue = !string.IsNullOrEmpty(dToCreation.Text) ? dToCreation.DateTime : DateTime.MaxValue;
            }
        }
    }
}
