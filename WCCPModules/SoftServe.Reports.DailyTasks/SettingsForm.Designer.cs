﻿namespace SoftServe.Reports.TasksReport
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.lChannels = new DevExpress.XtraEditors.LabelControl();
            this.cStaffChannel = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.tStaff = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colChannelMask = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.dFromCreation = new DevExpress.XtraEditors.DateEdit();
            this.dToCreation = new DevExpress.XtraEditors.DateEdit();
            this.cMmTasksOnly = new DevExpress.XtraEditors.CheckEdit();
            this.lCreationDate = new DevExpress.XtraEditors.LabelControl();
            this.lFrom1 = new DevExpress.XtraEditors.LabelControl();
            this.lTo1 = new DevExpress.XtraEditors.LabelControl();
            this.gStaff = new DevExpress.XtraEditors.GroupControl();
            this.cStaffAll = new DevExpress.XtraEditors.CheckEdit();
            this.bOk = new DevExpress.XtraEditors.SimpleButton();
            this.bCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMmTasksOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gStaff)).BeginInit();
            this.gStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lChannels
            // 
            this.lChannels.Location = new System.Drawing.Point(35, 61);
            this.lChannels.Name = "lChannels";
            this.lChannels.Size = new System.Drawing.Size(91, 13);
            this.lChannels.TabIndex = 11;
            this.lChannels.Text = "Канал персонала:";
            // 
            // cStaffChannel
            // 
            this.cStaffChannel.Location = new System.Drawing.Point(144, 58);
            this.cStaffChannel.Name = "cStaffChannel";
            this.cStaffChannel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cStaffChannel.Properties.SelectAllItemCaption = "(Все)";
            this.cStaffChannel.Size = new System.Drawing.Size(195, 20);
            this.cStaffChannel.TabIndex = 13;
            this.cStaffChannel.EditValueChanged += new System.EventHandler(this.cStaffChannel_EditValueChanged);
            // 
            // tStaff
            // 
            this.tStaff.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colStaffId,
            this.colLevel,
            this.colChannelMask});
            this.tStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tStaff.KeyFieldName = "Id";
            this.tStaff.Location = new System.Drawing.Point(2, 21);
            this.tStaff.Name = "tStaff";
            this.tStaff.OptionsBehavior.AllowQuickHideColumns = false;
            this.tStaff.OptionsBehavior.AllowRecursiveNodeChecking = true;
            this.tStaff.OptionsBehavior.Editable = false;
            this.tStaff.OptionsBehavior.EnableFiltering = true;
            this.tStaff.OptionsBehavior.ReadOnly = true;
            this.tStaff.OptionsBehavior.ShowToolTips = false;
            this.tStaff.OptionsFilter.AllowColumnMRUFilterList = false;
            this.tStaff.OptionsFilter.AllowMRUFilterList = false;
            this.tStaff.OptionsFilter.ShowAllValuesInCheckedFilterPopup = false;
            this.tStaff.OptionsFilter.ShowAllValuesInFilterPopup = true;
            this.tStaff.OptionsMenu.EnableColumnMenu = false;
            this.tStaff.OptionsMenu.EnableFooterMenu = false;
            this.tStaff.OptionsMenu.ShowAutoFilterRowItem = false;
            this.tStaff.OptionsView.ShowCheckBoxes = true;
            this.tStaff.OptionsView.ShowColumns = false;
            this.tStaff.OptionsView.ShowHorzLines = false;
            this.tStaff.OptionsView.ShowIndicator = false;
            this.tStaff.OptionsView.ShowVertLines = false;
            this.tStaff.ParentFieldName = "ParentId";
            this.tStaff.Size = new System.Drawing.Size(328, 235);
            this.tStaff.TabIndex = 15;
            this.tStaff.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tStaff_AfterCheckNode);
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 32;
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colStaffId
            // 
            this.colStaffId.Caption = "StaffId";
            this.colStaffId.FieldName = "StaffId";
            this.colStaffId.Name = "colStaffId";
            // 
            // colLevel
            // 
            this.colLevel.Caption = "Level";
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            // 
            // colChannelMask
            // 
            this.colChannelMask.Caption = "ChannelMask";
            this.colChannelMask.FieldName = "ChannelMask";
            this.colChannelMask.Name = "colChannelMask";
            // 
            // dFromCreation
            // 
            this.dFromCreation.EditValue = null;
            this.dFromCreation.Location = new System.Drawing.Point(144, 22);
            this.dFromCreation.Name = "dFromCreation";
            this.dFromCreation.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dFromCreation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFromCreation.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFromCreation.Size = new System.Drawing.Size(86, 20);
            this.dFromCreation.TabIndex = 16;
            this.dFromCreation.EditValueChanged += new System.EventHandler(this.datesCreation_EditValueChanged);
            // 
            // dToCreation
            // 
            this.dToCreation.EditValue = null;
            this.dToCreation.Location = new System.Drawing.Point(254, 22);
            this.dToCreation.Name = "dToCreation";
            this.dToCreation.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dToCreation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dToCreation.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dToCreation.Size = new System.Drawing.Size(85, 20);
            this.dToCreation.TabIndex = 18;
            this.dToCreation.EditValueChanged += new System.EventHandler(this.datesCreation_EditValueChanged);
            // 
            // cMmTasksOnly
            // 
            this.cMmTasksOnly.Location = new System.Drawing.Point(213, 362);
            this.cMmTasksOnly.Name = "cMmTasksOnly";
            this.cMmTasksOnly.Properties.Caption = "Только задачи УМ";
            this.cMmTasksOnly.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cMmTasksOnly.Size = new System.Drawing.Size(126, 19);
            this.cMmTasksOnly.TabIndex = 21;
            // 
            // lCreationDate
            // 
            this.lCreationDate.Location = new System.Drawing.Point(7, 25);
            this.lCreationDate.Name = "lCreationDate";
            this.lCreationDate.Size = new System.Drawing.Size(119, 13);
            this.lCreationDate.TabIndex = 15;
            this.lCreationDate.Text = "Дата создания задачи:";
            // 
            // lFrom1
            // 
            this.lFrom1.Location = new System.Drawing.Point(133, 25);
            this.lFrom1.Name = "lFrom1";
            this.lFrom1.Size = new System.Drawing.Size(5, 13);
            this.lFrom1.TabIndex = 15;
            this.lFrom1.Text = "с";
            // 
            // lTo1
            // 
            this.lTo1.Location = new System.Drawing.Point(236, 25);
            this.lTo1.Name = "lTo1";
            this.lTo1.Size = new System.Drawing.Size(12, 13);
            this.lTo1.TabIndex = 29;
            this.lTo1.Text = "по";
            // 
            // gStaff
            // 
            this.gStaff.Controls.Add(this.cStaffAll);
            this.gStaff.Controls.Add(this.tStaff);
            this.gStaff.Location = new System.Drawing.Point(7, 93);
            this.gStaff.Name = "gStaff";
            this.gStaff.Size = new System.Drawing.Size(332, 258);
            this.gStaff.TabIndex = 24;
            this.gStaff.Text = "Персонал";
            // 
            // cStaffAll
            // 
            this.cStaffAll.Location = new System.Drawing.Point(282, 1);
            this.cStaffAll.Name = "cStaffAll";
            this.cStaffAll.Properties.AllowGrayed = true;
            this.cStaffAll.Properties.Caption = "Все";
            this.cStaffAll.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cStaffAll.Size = new System.Drawing.Size(42, 19);
            this.cStaffAll.TabIndex = 21;
            this.cStaffAll.TabStop = false;
            this.cStaffAll.CheckStateChanged += new System.EventHandler(this.cStaffAll_CheckStateChanged);
            this.cStaffAll.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cStaffAll_EditValueChanging);
            // 
            // bOk
            // 
            this.bOk.Image = ((System.Drawing.Image)(resources.GetObject("bOk.Image")));
            this.bOk.Location = new System.Drawing.Point(94, 402);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(146, 23);
            this.bOk.TabIndex = 26;
            this.bOk.Text = "Сгенерировать отчет";
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Image = ((System.Drawing.Image)(resources.GetObject("bCancel.Image")));
            this.bCancel.Location = new System.Drawing.Point(247, 402);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(92, 23);
            this.bCancel.TabIndex = 27;
            this.bCancel.Text = "Отменить";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(345, 433);
            this.Controls.Add(this.cMmTasksOnly);
            this.Controls.Add(this.lCreationDate);
            this.Controls.Add(this.lFrom1);
            this.Controls.Add(this.cStaffChannel);
            this.Controls.Add(this.lTo1);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.dToCreation);
            this.Controls.Add(this.dFromCreation);
            this.Controls.Add(this.lChannels);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.gStaff);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры к отчету \"Задачи дня\"";
            ((System.ComponentModel.ISupportInitialize)(this.cStaffChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMmTasksOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gStaff)).EndInit();
            this.gStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cStaffAll.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lChannels;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cStaffChannel;
        private DevExpress.XtraTreeList.TreeList tStaff;
        private DevExpress.XtraEditors.DateEdit dFromCreation;
        private DevExpress.XtraEditors.DateEdit dToCreation;
        private DevExpress.XtraEditors.CheckEdit cMmTasksOnly;
        private DevExpress.XtraEditors.GroupControl gStaff;
        private DevExpress.XtraEditors.SimpleButton bOk;
        private DevExpress.XtraEditors.SimpleButton bCancel;
        private DevExpress.XtraEditors.CheckEdit cStaffAll;
        private DevExpress.XtraEditors.LabelControl lFrom1;
        private DevExpress.XtraEditors.LabelControl lTo1;
        private DevExpress.XtraEditors.LabelControl lCreationDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colChannelMask;
    }
}