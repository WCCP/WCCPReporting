﻿using System;
using System.IO;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.TasksReport.Utils;

namespace SoftServe.Reports.TasksReport.Controls
{
    public partial class DataControl : UserControl
    {
        public object DataSource
        {
            set
            {
                gridControlData.BeginUpdate();
                gridControlData.DataSource = value;
                gridControlData.EndUpdate();
            }
        }

        public DataControl()
        {
            InitializeComponent();
        }

        internal void Export(ExportToType exportToType, string path)
        {
            switch (exportToType)
            {
                case ExportToType.Xlsx:
                    gridControlData.ExportToXlsx(path);
                    break;
                case ExportToType.Csv:
                    gridControlData.ExportToCsv(path);
                    break;
            }
        }

        internal void SaveLayout(string path)
        {
            gridControlData.MainView.SaveLayoutToXml(path);
        }

        /// <summary>
        /// Restores gridview layout for current user from file on disc
        /// </summary>
        internal void RestoreLayout(string path)
        {
            if (File.Exists(path))
            {
                gridControlData.MainView.RestoreLayoutFromXml(path);
            }
        }

        private void gridViewData_BeforeLoadLayout(object sender, DevExpress.Utils.LayoutAllowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.PreviousVersion))
            {
                e.Allow = false;
                return;
            }

            e.Allow = e.PreviousVersion == gridViewData.OptionsLayout.LayoutVersion;
        }
    }
}

