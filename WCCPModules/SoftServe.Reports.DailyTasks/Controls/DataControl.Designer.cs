﻿namespace SoftServe.Reports.TasksReport.Controls
{
    partial class DataControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlData = new DevExpress.XtraGrid.GridControl();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOLCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNameJur = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdressFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTask = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlData
            // 
            this.gridControlData.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlData.Location = new System.Drawing.Point(0, 0);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rTextEdit,
            this.rCheckEdit});
            this.gridControlData.Size = new System.Drawing.Size(1186, 423);
            this.gridControlData.TabIndex = 0;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // gridViewData
            // 
            this.gridViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewData.ColumnPanelRowHeight = 40;
            this.gridViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colM3,
            this.colM2,
            this.colM1,
            this.colOLCode,
            this.colNameJur,
            this.colNameFact,
            this.colAdressFact,
            this.colTaskId,
            this.colDateCreation,
            this.colTask,
            this.colComment,
            this.colPlan,
            this.colFact,
            this.colM1Status});
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsBehavior.Editable = false;
            this.gridViewData.OptionsBehavior.ReadOnly = true;
            this.gridViewData.OptionsLayout.LayoutVersion = "1";
            this.gridViewData.OptionsPrint.AutoWidth = false;
            this.gridViewData.OptionsSelection.MultiSelect = true;
            this.gridViewData.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridViewData.OptionsView.ColumnAutoWidth = false;
            this.gridViewData.OptionsView.ShowAutoFilterRow = true;
            this.gridViewData.OptionsView.ShowGroupPanel = false;
            this.gridViewData.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.gridViewData_BeforeLoadLayout);
            // 
            // colM3
            // 
            this.colM3.Caption = "М3";
            this.colM3.FieldName = "m3_Name";
            this.colM3.Name = "colM3";
            this.colM3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3.Visible = true;
            this.colM3.VisibleIndex = 0;
            this.colM3.Width = 155;
            // 
            // colM2
            // 
            this.colM2.Caption = "М2";
            this.colM2.FieldName = "m2_Name";
            this.colM2.Name = "colM2";
            this.colM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2.Visible = true;
            this.colM2.VisibleIndex = 1;
            this.colM2.Width = 169;
            // 
            // colM1
            // 
            this.colM1.Caption = "М1";
            this.colM1.FieldName = "m1_Name";
            this.colM1.Name = "colM1";
            this.colM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1.Visible = true;
            this.colM1.VisibleIndex = 2;
            this.colM1.Width = 192;
            // 
            // colOLCode
            // 
            this.colOLCode.Caption = "Код ТТ";
            this.colOLCode.ColumnEdit = this.rTextEdit;
            this.colOLCode.FieldName = "OL_id";
            this.colOLCode.Name = "colOLCode";
            this.colOLCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLCode.Visible = true;
            this.colOLCode.VisibleIndex = 3;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // colNameJur
            // 
            this.colNameJur.Caption = "Юридическое название";
            this.colNameJur.FieldName = "OLName";
            this.colNameJur.Name = "colNameJur";
            this.colNameJur.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colNameJur.Visible = true;
            this.colNameJur.VisibleIndex = 4;
            this.colNameJur.Width = 134;
            // 
            // colNameFact
            // 
            this.colNameFact.Caption = "Фактическое название";
            this.colNameFact.FieldName = "OLTradingName";
            this.colNameFact.Name = "colNameFact";
            this.colNameFact.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colNameFact.Visible = true;
            this.colNameFact.VisibleIndex = 5;
            this.colNameFact.Width = 140;
            // 
            // colAdressFact
            // 
            this.colAdressFact.Caption = "Фактический адрес";
            this.colAdressFact.FieldName = "OLAddress";
            this.colAdressFact.Name = "colAdressFact";
            this.colAdressFact.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAdressFact.Visible = true;
            this.colAdressFact.VisibleIndex = 6;
            this.colAdressFact.Width = 224;
            // 
            // colTaskId
            // 
            this.colTaskId.Caption = "ID задания";
            this.colTaskId.ColumnEdit = this.rTextEdit;
            this.colTaskId.FieldName = "Task_id";
            this.colTaskId.Name = "colTaskId";
            this.colTaskId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTaskId.Visible = true;
            this.colTaskId.VisibleIndex = 7;
            this.colTaskId.Width = 194;
            // 
            // colDateCreation
            // 
            this.colDateCreation.Caption = "Дата создания задачи";
            this.colDateCreation.FieldName = "CreationDate";
            this.colDateCreation.Name = "colDateCreation";
            this.colDateCreation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDateCreation.Visible = true;
            this.colDateCreation.VisibleIndex = 8;
            this.colDateCreation.Width = 90;
            // 
            // colTask
            // 
            this.colTask.Caption = "Задача";
            this.colTask.FieldName = "Task_Name";
            this.colTask.Name = "colTask";
            this.colTask.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTask.Visible = true;
            this.colTask.VisibleIndex = 9;
            this.colTask.Width = 181;
            // 
            // colComment
            // 
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Task_Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 10;
            this.colComment.Width = 262;
            // 
            // colPlan
            // 
            this.colPlan.Caption = "План";
            this.colPlan.FieldName = "PlannedAmount";
            this.colPlan.Name = "colPlan";
            this.colPlan.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPlan.Visible = true;
            this.colPlan.VisibleIndex = 11;
            // 
            // colFact
            // 
            this.colFact.Caption = "Факт";
            this.colFact.FieldName = "ActualAmount";
            this.colFact.Name = "colFact";
            this.colFact.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colFact.Visible = true;
            this.colFact.VisibleIndex = 12;
            // 
            // colM1Status
            // 
            this.colM1Status.Caption = "Статус М1";
            this.colM1Status.FieldName = "Status_Name";
            this.colM1Status.Name = "colM1Status";
            this.colM1Status.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Status.Visible = true;
            this.colM1Status.VisibleIndex = 13;
            // 
            // rCheckEdit
            // 
            this.rCheckEdit.AutoHeight = false;
            this.rCheckEdit.DisplayValueChecked = "1";
            this.rCheckEdit.DisplayValueUnchecked = "0";
            this.rCheckEdit.Name = "rCheckEdit";
            // 
            // DataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlData);
            this.Name = "DataControl";
            this.Size = new System.Drawing.Size(1186, 423);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraGrid.Columns.GridColumn colM2;
        private DevExpress.XtraGrid.Columns.GridColumn colM1;
        private DevExpress.XtraGrid.Columns.GridColumn colOLCode;
        private DevExpress.XtraGrid.Columns.GridColumn colNameJur;
        private DevExpress.XtraGrid.Columns.GridColumn colNameFact;
        private DevExpress.XtraGrid.Columns.GridColumn colAdressFact;
        private DevExpress.XtraGrid.Columns.GridColumn colTaskId;
        private DevExpress.XtraGrid.Columns.GridColumn colTask;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colFact;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreation;
        private DevExpress.XtraGrid.Columns.GridColumn colM1Status;
        private DevExpress.XtraGrid.GridControl gridControlData;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rCheckEdit;
    }
}
