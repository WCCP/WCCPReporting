﻿using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Core.Common.View;
using SoftServe.Reports.TasksReport.DataAccess;
using SoftServe.Reports.TasksReport.Models;

namespace SoftServe.Reports.TasksReport
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : SimpleControlView
    {
        private SettingsForm _settingsForm;
        private Settings _currentSettings;

        private const string CompanyDirectory = "SoftServe";
        private const string ProgramDirectory = "WCCP Reporting";
        private const string LayoutFileName = "DailyTasks";

        public WccpUIControl()
        {
            InitializeComponent();

            _settingsForm = new SettingsForm();
        }

        public int InitReport()
        {
            WaitManager.StartWait();
            _settingsForm.LoadDataSources();
            WaitManager.StopWait();


            RestoreLayout();

            return HandleSettingsForm() ? 0 : 1;
        }

        private void GenerateReport()
        {
            WaitManager.StartWait();
            dataControl.DataSource = DataProvider.GetDetails(_currentSettings);
            WaitManager.StopWait();
        }

        private bool HandleSettingsForm()
        {
            if (_settingsForm.ShowDialog(this) == DialogResult.OK)
            {
                _currentSettings = _settingsForm.Settings;

                GenerateReport();
                return true;
            }

            return false;
        }

        #region Menu Handlers

        private void bSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            HandleSettingsForm();
        }

        private void bRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            GenerateReport();
        }

        private void bExportToXlsx_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportTab(ExportToType.Xlsx);
        }

        private void bExportToCsv_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportTab(ExportToType.Csv);
        }

        private void ExportTab(ExportToType exportType)
        {
            string lPath = SelectFilePath(tabData.Text, exportType);

            if (!string.IsNullOrEmpty(lPath))
            {
                try
                {
                    dataControl.Export(exportType, lPath);
                }
                catch (Exception)
                {
                    ErrorManager.ShowErrorBox("Ошибка при экспорте");
                }
            }
        }

        private string SelectFilePath(string reportCaption, ExportToType exportType)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = reportCaption;
            sfd.Filter = String.Format("(*.{0})|*.{0}", exportType.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }

            return res;
        }

        #endregion

        #region Save/restore layout

        private string GetLayoutPath()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);

            if (!Directory.Exists(lCompanyDirPath))
            {
                Directory.CreateDirectory(lCompanyDirPath);
            }

            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);

            if (!Directory.Exists(lProgramDirPath))
            {
                Directory.CreateDirectory(lProgramDirPath);
            }

            return Path.Combine(lProgramDirPath, LayoutFileName);
        }

        internal void SaveLayout()
        {
            try
            {
                string lLayoutPath = GetLayoutPath();
                dataControl.SaveLayout(lLayoutPath);
            }
            catch (Exception) { }
        }

        private void RestoreLayout()
        {
            try
            {
                string lLayoutPath = GetLayoutPath();
                dataControl.RestoreLayout(lLayoutPath);
            }
            catch (Exception) { }
        }

        #endregion

    }
}
