﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.RouteInspector;
using SoftServe.Reports.RouteInspector.DataProvider;
using SoftServe.Reports.RouteInspector.Utility;
using Logica.Reports.Common;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;

namespace WccpReporting
{
    public partial class WccpUIControl : XtraUserControl
    {
        #region Constructors

        public WccpUIControl()
        {
            InitializeComponent();

            btnExportTable.Tag = 1;
            btnExportMap.Tag = 2;
            btnExportTableAndMap.Tag = 3;
        }

        #endregion

        #region Instance Methods

        private void CallSettingsForm()
        {
            if (SettingsForm.ShowDialog() != DialogResult.OK) return;
            ShowReport();
        }

        private void ShowReport()
        {
            WaitManager.StartWait();
            try
            {
                reportUserControl.DataSource = DataAccessProvider.GetReport(SettingsForm.ReportOptions);
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        #endregion

        #region Event Handling

        private void WccpUIControl_Load(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void bbiShowMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            var mapVisible = bbiShowMap.Down;
            reportUserControl.MapVisible =
                bbiShowTrack.Enabled = mapVisible;
            if (mapVisible)
            {
                btnExportMap.Visibility = btnExportTableAndMap.Visibility = BarItemVisibility.Always;
            }
            else
            {
                btnExportMap.Visibility = btnExportTableAndMap.Visibility = BarItemVisibility.Never;
            }
        }

        private void bbiShowTrack_ItemClick(object sender, ItemClickEventArgs e)
        {
            reportUserControl.TrackVisible = bbiShowTrack.Down;
        }

        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!(e.Item.Tag is int))
            {
                return;
            }

            var tag = (int)e.Item.Tag;

            GridView summaryView = null;
            GridView routesView = null;
            Image map = null;

            if ((tag & 1) != 0)
            {
                summaryView = reportUserControl.GetSummaryGrid();
                routesView = reportUserControl.GetDataGrid();
            }

            if ((tag & 2) != 0)
            {
                map = reportUserControl.GetMap();
            }

            string pathToFile = SelectFilePath();

            if (string.IsNullOrEmpty(pathToFile))
            {
                return;
            }

            try
            {
                CreateXslxDocument(summaryView, routesView, map, pathToFile);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox("Ошибка при экспорте файла");
            }
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowReport();
        }

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            CallSettingsForm();
        }

        #endregion

        #region Class Properties

        public static SettingsForm SettingsForm { get; set; }

        #endregion

        #region Export methods

        private void CreateXslxDocument(GridView summaryView, GridView routesView, Image map, string filePath)
        {
            CompositeLink lCompositeLink = new CompositeLink(new PrintingSystem());

            if (summaryView != null)
            {
                PrintableComponentLink lGridLink1 = new PrintableComponentLink();
                PrintableComponentLink lGridLink2 = new PrintableComponentLink();

                Link lFillerLink = new Link();
                lFillerLink.CreateDetailArea += linFiller_CreateDetailArea;

                summaryView.OptionsPrint.AutoWidth = false;
                routesView.OptionsPrint.AutoWidth = false;

                summaryView.BestFitColumns();
                routesView.BestFitColumns();

                for (int i = 0; i < summaryView.Columns.Count; i++)// align columns to avoid excel cells width
                {
                    int colWidth = Math.Max(summaryView.Columns[i].Width, routesView.Columns[i].Width);

                    routesView.Columns[i].Width = summaryView.Columns[i].Width = colWidth;
                }

                // Assign the controls to the printing links.
                lGridLink1.Component = summaryView.GridControl;
                lGridLink2.Component = routesView.GridControl;

                lCompositeLink.Links.Add(lGridLink1);
                lCompositeLink.Links.Add(lFillerLink);
                lCompositeLink.Links.Add(lGridLink2);

                if (map != null)
                {
                    lCompositeLink.Links.Add(lFillerLink);
                }
            }

            if (map != null)
            {
                Link lImageLink = new Link();
                lImageLink.CreateDetailArea += linkImage_CreateDetailArea;
                lCompositeLink.Links.Add(lImageLink);
            }

            lCompositeLink.CreateDocument();
            lCompositeLink.ExportToXlsx(filePath);
        }

        // Creates an interval between the grids and fills it with color.
        void linFiller_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            TextBrick tb = new TextBrick();
            tb.Rect = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30);
            tb.BackColor = Color.White;
            e.Graph.DrawBrick(tb);
        }

        // Creates an image
        void linkImage_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            Image headerImage = reportUserControl.GetMap();
            e.Graph.BorderWidth = 0;

            Rectangle r = new Rectangle(0, 0, headerImage.Width, headerImage.Height);
            e.Graph.DrawImage(headerImage, r);
        }

        string SelectFilePath()
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Сохранить";
            sfd.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            sfd.FileName = "Report";
            sfd.Filter = String.Format("(*.{0})|*.{0}", "xlsx");
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }
            return res;
        }

        #endregion
    }
}