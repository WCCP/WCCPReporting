﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.RouteInspector.DataProvider;
using SoftServe.Reports.RouteInspector.Properties;
using SoftServe.Reports.RouteInspector.Utility;
using SoftServe.Reports.RouteInspector.Utility.Capturing;

namespace SoftServe.Reports.RouteInspector.UserControl {
    public partial class ReportUserControl : XtraUserControl {
        #region Fields

        private DataTable _dataSource;
        private bool _disableEvents;
        private bool _isLoadBrowser;
        private bool _isLoaded;
        private bool _isMapVisible = true;
        private bool _isTrackVisible;

        #endregion

        #region Constructors

        public ReportUserControl() {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        public DataTable DataSource {
            get { return _dataSource; }
            set {
                _dataSource = value;
                gridControlData.DataSource = _dataSource;
                summaryDataBindingSource.DataSource = GetSummaryRow(_dataSource);
                if (webBrowser.ReadyState == WebBrowserReadyState.Complete) {
                    LoadWebBrowser(_dataSource);
                }
                else {
                    _isLoadBrowser = true;
                }
            }
        }

        [Browsable(true)]
        [DefaultValue(true)]
        public bool MapVisible {
            get { return _isMapVisible; }
            set {
                if (_isMapVisible == value) return;
                _isMapVisible = value;
                if (_dataSource != null) {
                    if (_isMapVisible) {
                        webBrowser.Show();
                        splitContainerControl.PanelVisibility = SplitPanelVisibility.Both;
                    }
                    else {
                        webBrowser.Hide();
                        splitContainerControl.PanelVisibility = SplitPanelVisibility.Panel1;
                    }
                }
            }
        }

        [Browsable(true)]
        [DefaultValue(false)]
        public bool TrackVisible {
            get { return _isTrackVisible; }
            set {
                if (_isTrackVisible == value) return;
                _isTrackVisible = value;

                if (_isTrackVisible) {
                    JSCreateRoute(_dataSource);
                }
                else {
                    JSDeleteRoute();
                }
            }
        }

        #endregion

        #region Instance Methods

        public void CBInitializationCompleted() {
            if (!_isLoadBrowser) return;
            LoadWebBrowser(_dataSource);
            _isLoadBrowser = false;
        }

        public void CBSelectOL(string olId) {
            var strings = olId.Split('_');
            if (strings.Length != 2)
                return;

            long iNo;
            long? vNo = null;
            if (long.TryParse(strings[0], out iNo)) {
                vNo = iNo;
            }

            long iOlId;
            long? vOlId = null;
            if (long.TryParse(strings[1], out iOlId)) {
                vOlId = iOlId;
            }

            int i = _dataSource.Rows.Cast<DataRow>().TakeWhile(
                row => !(
                            (
                                vNo == null
                                || vNo.Equals(row[gcdNo.FieldName])
                            )
                            && (
                                   vOlId == null
                                   || vOlId.Equals(row[gcdOLId.FieldName])
                               )
                        )
                ).Count();

            if (i > _dataSource.Rows.Count) return;
            _disableEvents = true;
            int lFocusedRowHandle = gridViewData.GetRowHandle(i);
            gridViewData.FocusedRowHandle = lFocusedRowHandle;
            gridViewData.TopRowIndex = gridViewData.GetVisibleIndex(lFocusedRowHandle);
            _disableEvents = false;
        }

        public GridView GetDataGrid() {
            return gridViewData;
        }

        public GridView GetSummaryGrid() {
            return gridViewSummary;
        }

        public Image GetMap() {
            return _isMapVisible ? webBrowser.DrawToImage() : null;
        }

        private MarkerColor GetMarkerColor(DataRow row) {
            if (!IsAddToMap(row))
                return MarkerColor.None;
            if (!IsVisitWithCoords(row) && !row.IsNull(gcdOLBeginTime.FieldName) ||
                row.Field<int?>(gcdDistance.FieldName) > DataAccessProvider.MinGPSDistance)
                return MarkerColor.Yellow;
            if (row.Field<int?>(gcdDistance.FieldName) <= DataAccessProvider.MinGPSDistance)
                return MarkerColor.Green;
            return MarkerColor.Red;
        }

        private SummaryData GetSummaryRow(DataTable dataTable) {
            if (dataTable == null) return null;

            var enumerable = dataTable.AsEnumerable();
            //var minDist = DataAccessProvider.MinGPSDistance;

            var summaryData = new SummaryData {
                // ТТ в маршруте
                TTInRoute =
                    enumerable.Where(r => r.Field<bool?>(gcdOLInRoute.FieldName) == true).Select(
                        r => ConvertEx.ToLongInt(r[gcdOLId.FieldName])).Distinct().Count(),
                // Визитов по маршруту
                VisitsByRoute =
                    enumerable.Count(
                        r =>
                        r.Field<bool?>(gcdOLInRoute.FieldName) == true &&
                        !r.IsNull(gcdOLBeginTime.FieldName)),
                // Визитов в ТТ по маршруту
                VisitsInTTOnRoute =
                    enumerable.Count(
                        r =>
                        r.Field<bool?>(gcdOLInRoute.FieldName) == true &&
                        r.Field<bool?>(gcdVisitInOL.FieldName) == true),
                // Визитов всего
                VisitsAll = enumerable.Count(r => !r.IsNull(gcdOLBeginTime.FieldName)),
                // ТТ без координат
                TTWithoutLocations =
                    enumerable.Where(r => !IsTTWithCoords(r)).Select(r => ConvertEx.ToLongInt(r[gcdOLId.FieldName])).
                        Distinct().Count(),
                // Визитов без координат
                VisitsWithoutLocations =
                    enumerable.Count(
                        r => !IsVisitWithCoords(r) && !r.IsNull(gcdOLBeginTime.FieldName)),
                // Заявка итого
                ClaimSummary = enumerable.Sum(r => r.Field<decimal?>(gcdClaim.FieldName)) ?? 0,
                // Отгрузка итого
                DispatchSummary =
                    enumerable.Sum(r => r.Field<decimal?>(gcdDispatch.FieldName)) ?? 0
            };
            return summaryData;
        }

        private bool IsAddToMap(DataRow row) {
            return !row.IsNull(gcdNo.FieldName) && IsTTWithCoords(row);
        }

        private bool IsTTWithCoords(DataRow row) {
            return IsLatCorrect(row.Field<double?>(gcdOLLat.FieldName)) &&
                   IsLonCorrect(row.Field<double?>(gcdOLLon.FieldName));
        }

        private bool IsVisitWithCoords(DataRow row) {
            return IsLatCorrect(row.Field<double?>(gcdVisitLat.FieldName)) &&
                   IsLonCorrect(row.Field<double?>(gcdVisitLon.FieldName));
        }

        private bool IsLatCorrect(double? lat) {
            double value = lat ?? 0;
            return value > 0.01 && Math.Abs(value) <= 90;
        }

        private bool IsLonCorrect(double? lon) {
            double value = lon ?? 0;
            return value > 0.01 && Math.Abs(value) <= 180;
        }

        private void JSAddMarker(DataRow row) {
//            if (!IsVisitWithCoords(row))
//                return;
            // function addMarker(id, name, description, color, lat, lon) {
            webBrowser.Document.InvokeScript("addMarker", new[] {
                GetPointId(row[gcdNo.FieldName], row[gcdOLId.FieldName]),
                row[gcdNo.FieldName].ToString(),
                string.Format("<strong>{0}</strong><br>{1}", row[gcdOLName.FieldName], row[gcdOLAddress.FieldName]),
                (int) GetMarkerColor(row),
                row[gcdOLLat.FieldName],
                row[gcdOLLon.FieldName],
                //row[gcdVisitLat.FieldName],
                //row[gcdVisitLon.FieldName],
                IsVisitWithCoords(row)
            });
        }

        private void JSCenterMap() {
            webBrowser.Document.InvokeScript("centerMap");
        }

        private void JSClearMap() {
            webBrowser.Document.InvokeScript("clearMap");
        }

        private void JSCreateRoute(DataTable ds) {
            object[] lCoords = ds.AsEnumerable().Where(IsVisitWithCoords)
                .SelectMany(r => new object[] {
                        r.Field<double>(gcdVisitLat.FieldName),
                        r.Field<double>(gcdVisitLon.FieldName),
                        r[gcdNo.FieldName].ToString()
                })
                .ToArray();
            webBrowser.Document.InvokeScript("createRoute", lCoords);
        }

        private void JSDeleteRoute() {
            webBrowser.Document.InvokeScript("deleteRoute");
        }

        private void JSSelectOL(object no, object olId) {
            webBrowser.Document.InvokeScript("selectOLOnMap", new object[] {GetPointId(no, olId)});
            // Convert to string because JS can't compare numbers correctly
        }

        private string GetPointId(object no, object olId) {
            string sNo = (no == null || no == DBNull.Value) ? string.Empty : no.ToString();
            string sOlId = (olId == null || olId == DBNull.Value) ? string.Empty : olId.ToString();
            return string.Format("{0}_{1}", sNo, sOlId);
        }

        private void LoadWebBrowser(DataTable dataTable) {
            JSClearMap();
            if (dataTable == null) return;

            foreach (DataRow row in dataTable.Rows) {
                if (!IsAddToMap(row)) continue;
                JSAddMarker(row);
            }
            JSCenterMap();
            if (_isTrackVisible) {
                JSCreateRoute(_dataSource);
            }
        }

        #endregion

        #region Event Handling

        private void ReportUserControl_Load(object sender, EventArgs e) {
            if (_isLoaded) return;

            webBrowser.ObjectForScripting = new ScriptManager(this);
            var googleApiKey = DataAccessProvider.GoogleApiKey.Trim();
            webBrowser.DocumentText = Resources.Map_Google_v3.Replace("<#GoogleAPI.Key#>", googleApiKey);
            _isLoaded = true;
        }

/*
    private void gridViewData_ColumnWidthChanged(object sender, ColumnEventArgs e)
    {
        int dataSum = 0;
        foreach (GridColumn c in gridViewData.VisibleColumns)
            dataSum += c.Width;
        var dataWidth = dataSum;

        int summarySum = 0;
        foreach (GridColumn c in gridViewSummary.VisibleColumns)
            summarySum += c.Width;
        var summWidth = summarySum;

        scrollableControl.AutoScrollMinSize = new Size((dataWidth >= summWidth ? dataWidth : summWidth) + 25,
                                                       145 + 20*DataSource.Rows.Count);
    }
*/

        private void gridViewData_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e) {
            if (_disableEvents) return;

            JSSelectOL(gridViewData.GetRowCellValue(e.FocusedRowHandle, gcdNo),
                gridViewData.GetRowCellValue(e.FocusedRowHandle, gcdOLId));
        }

        private void gridViewData_RowCellStyle(object sender, RowCellStyleEventArgs e) {
            if (e.Column == gcdNo) {
                var row = gridViewData.GetDataRow(e.RowHandle);
                var color = GetMarkerRGBColor(row);
                if (color.HasValue) {
                    e.Appearance.BackColor = color.Value;
                    e.Appearance.BackColor2 = color.Value;
                    e.Appearance.Options.UseBackColor = true;
                    if (color == Color.Yellow) {
                        e.Appearance.ForeColor = Color.Black;
                        e.Appearance.Options.UseForeColor = true;
                    }
                }
            }
        }

        public Color? GetMarkerRGBColor(DataRow row) {
            var markerColor = GetMarkerColor(row);
            Color? color = null;
            switch (markerColor) {
                case MarkerColor.Green:
                    color = Color.Green;
                    break;
                case MarkerColor.Yellow:
                    color = Color.Yellow;
                    break;
                case MarkerColor.Red:
                    color = Color.Red;
                    break;
            }
            return color;
        }

        #endregion

    }
}