﻿#region

using System;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.RouteInspector.DataProvider;

#endregion

namespace SoftServe.Reports.RouteInspector.UserControl
{
    public partial class SettingsFormUserControl : XtraUserControl
    {
        #region Fields

        private bool isDataLoaded;

        #endregion

        #region Constructors

        public SettingsFormUserControl()
        {
            InitializeComponent();
            btnCancel.Image = CommonResource.Close_16;
            btnSubmit.Image = CommonResource.Check_16;
        }

        #endregion

        #region Instance Properties

        public int M1Id
        {
            get { return ConvertEx.ToInt(treeList.FocusedNode.GetValue(tlcDataId)); }
        }

        public DateTime ReportDate
        {
            get { return dtReportDate.DateTime.Date; }
        }

        #endregion

        #region Instance Methods

        public ReportOptions GetReportOptions()
        {
            return new ReportOptions(ReportDate, M1Id);
        }

        #endregion

        #region Event Handling

        private void SettingsFormUserControl_Load(object sender, EventArgs e)
        {
            if (DesignMode) return;
            if (!isDataLoaded)
            {
                Enabled = false;
                WaitManager.StartWait();
                try
                {
                    var m1DataSource = DataAccessProvider.GetPersonelTree();
                    
                    dtReportDate.DateTime = DateTime.Now.Date;
                    treeList.DataSource = m1DataSource;
                    isDataLoaded = true;
                }
                finally
                {
                    Enabled = true;
                    WaitManager.StopWait();
                }
            }
        }

        private void treeList_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {
            var s = ConvertEx.ToInt(e.Node.GetValue(tlcLevel));
            btnSubmit.Enabled = s == 1;
        }

        #endregion

        #region Event Declarations

        public event EventHandler CancelClick
        {
            add { btnCancel.Click += value; }
            remove { btnCancel.Click -= value; }
        }

        public event EventHandler SubmitClick
        {
            add { btnSubmit.Click += value; }
            remove { btnSubmit.Click -= value; }
        }

        #endregion
    }
}