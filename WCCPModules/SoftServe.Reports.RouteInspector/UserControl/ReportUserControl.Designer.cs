﻿namespace SoftServe.Reports.RouteInspector.UserControl
{
    partial class ReportUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.scrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControlData = new DevExpress.XtraEditors.PanelControl();
            this.gridControlData = new SoftServe.Reports.RouteInspector.Utility.UnscrolledGrid.UnscrolledGridControl();
            this.gridViewData = new SoftServe.Reports.RouteInspector.Utility.UnscrolledGrid.UnscrolledGridView();
            this.gcdNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLInRoute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gcdVisitInOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLBeginTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdTimeInOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdVisitLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdVisitLon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLLon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdClaim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdRest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdDispatch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdOLTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdNetworkOutletTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControlSummary = new DevExpress.XtraEditors.PanelControl();
            this.gridControlSummary = new DevExpress.XtraGrid.GridControl();
            this.summaryDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTTInRoute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitsByRoute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitsInTTOnRoute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitsAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTWithoutLocations = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitsWithoutLocations = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClaimSummary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDispatchSummary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
            this.splitContainerControl.SuspendLayout();
            this.scrollableControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlData)).BeginInit();
            this.panelControlData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlSummary)).BeginInit();
            this.panelControlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl
            // 
            this.splitContainerControl.AlwaysScrollActiveControlIntoView = false;
            this.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl.Name = "splitContainerControl";
            this.splitContainerControl.Panel1.Controls.Add(this.scrollableControl);
            this.splitContainerControl.Panel1.Text = "Panel1";
            this.splitContainerControl.Panel2.Controls.Add(this.webBrowser);
            this.splitContainerControl.Panel2.MinSize = 200;
            this.splitContainerControl.Panel2.Text = "Panel2";
            this.splitContainerControl.Size = new System.Drawing.Size(1200, 600);
            this.splitContainerControl.SplitterPosition = 583;
            this.splitContainerControl.TabIndex = 6;
            // 
            // scrollableControl
            // 
            this.scrollableControl.Controls.Add(this.panelControlData);
            this.scrollableControl.Controls.Add(this.panelControlSummary);
            this.scrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollableControl.Location = new System.Drawing.Point(0, 0);
            this.scrollableControl.Name = "scrollableControl";
            this.scrollableControl.Size = new System.Drawing.Size(583, 600);
            this.scrollableControl.TabIndex = 2;
            // 
            // panelControlData
            // 
            this.panelControlData.Controls.Add(this.gridControlData);
            this.panelControlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlData.Location = new System.Drawing.Point(0, 75);
            this.panelControlData.Name = "panelControlData";
            this.panelControlData.Size = new System.Drawing.Size(583, 525);
            this.panelControlData.TabIndex = 3;
            // 
            // gridControlData
            // 
            this.gridControlData.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlData.Location = new System.Drawing.Point(2, 2);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rCheckEdit});
            this.gridControlData.Size = new System.Drawing.Size(579, 521);
            this.gridControlData.TabIndex = 1;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // gridViewData
            // 
            this.gridViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewData.ColumnPanelRowHeight = 44;
            this.gridViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcdNo,
            this.gcdOLInRoute,
            this.gcdVisitInOL,
            this.gcdOLId,
            this.gcdOLName,
            this.gcdOLAddress,
            this.gcdOLBeginTime,
            this.gcdOLEndTime,
            this.gcdTimeInOL,
            this.gcdVisitLat,
            this.gcdVisitLon,
            this.gcdOLLat,
            this.gcdOLLon,
            this.gcdClaim,
            this.gcdRest,
            this.gcdDispatch,
            this.gcdOLTypeName,
            this.gcdDistance,
            this.gcdNetworkOutletTypeName});
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.MakeFocusedColumnVisible = false;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsBehavior.Editable = false;
            this.gridViewData.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gridViewData.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewData.OptionsView.ColumnAutoWidth = false;
            this.gridViewData.OptionsView.ShowGroupPanel = false;
            this.gridViewData.OptionsView.ShowIndicator = false;
            this.gridViewData.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewData_RowCellStyle);
            this.gridViewData.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewData_FocusedRowChanged);
            // 
            // gcdNo
            // 
            this.gcdNo.Caption = "№";
            this.gcdNo.FieldName = "OL_Number";
            this.gcdNo.Name = "gcdNo";
            this.gcdNo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdNo.Visible = true;
            this.gcdNo.VisibleIndex = 0;
            this.gcdNo.Width = 40;
            // 
            // gcdOLInRoute
            // 
            this.gcdOLInRoute.Caption = "ТТ в маршруте";
            this.gcdOLInRoute.ColumnEdit = this.rCheckEdit;
            this.gcdOLInRoute.FieldName = "OLInRoute";
            this.gcdOLInRoute.Name = "gcdOLInRoute";
            this.gcdOLInRoute.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdOLInRoute.Visible = true;
            this.gcdOLInRoute.VisibleIndex = 1;
            this.gcdOLInRoute.Width = 70;
            // 
            // rCheckEdit
            // 
            this.rCheckEdit.AutoHeight = false;
            this.rCheckEdit.DisplayValueChecked = "Да";
            this.rCheckEdit.DisplayValueUnchecked = "Нет";
            this.rCheckEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rCheckEdit.Name = "rCheckEdit";
            // 
            // gcdVisitInOL
            // 
            this.gcdVisitInOL.Caption = "Визит в ТТ";
            this.gcdVisitInOL.ColumnEdit = this.rCheckEdit;
            this.gcdVisitInOL.FieldName = "VisitInOL";
            this.gcdVisitInOL.Name = "gcdVisitInOL";
            this.gcdVisitInOL.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdVisitInOL.Visible = true;
            this.gcdVisitInOL.VisibleIndex = 2;
            this.gcdVisitInOL.Width = 60;
            // 
            // gcdOLId
            // 
            this.gcdOLId.Caption = "Код ТТ";
            this.gcdOLId.FieldName = "OL_id";
            this.gcdOLId.Name = "gcdOLId";
            this.gcdOLId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdOLId.Visible = true;
            this.gcdOLId.VisibleIndex = 3;
            // 
            // gcdOLName
            // 
            this.gcdOLName.Caption = "Факт Имя";
            this.gcdOLName.FieldName = "OLName";
            this.gcdOLName.Name = "gcdOLName";
            this.gcdOLName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdOLName.Visible = true;
            this.gcdOLName.VisibleIndex = 4;
            this.gcdOLName.Width = 280;
            // 
            // gcdOLAddress
            // 
            this.gcdOLAddress.Caption = "Факт адрес";
            this.gcdOLAddress.FieldName = "OLDeliveryAddress";
            this.gcdOLAddress.Name = "gcdOLAddress";
            this.gcdOLAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdOLAddress.Visible = true;
            this.gcdOLAddress.VisibleIndex = 5;
            this.gcdOLAddress.Width = 280;
            // 
            // gcdOLBeginTime
            // 
            this.gcdOLBeginTime.Caption = "Нач. визита";
            this.gcdOLBeginTime.FieldName = "BeginTime";
            this.gcdOLBeginTime.Name = "gcdOLBeginTime";
            this.gcdOLBeginTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdOLBeginTime.Visible = true;
            this.gcdOLBeginTime.VisibleIndex = 6;
            // 
            // gcdOLEndTime
            // 
            this.gcdOLEndTime.Caption = "Конец визита";
            this.gcdOLEndTime.FieldName = "EndTime";
            this.gcdOLEndTime.Name = "gcdOLEndTime";
            this.gcdOLEndTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdOLEndTime.Visible = true;
            this.gcdOLEndTime.VisibleIndex = 7;
            // 
            // gcdTimeInOL
            // 
            this.gcdTimeInOL.Caption = "Время в ТТ";
            this.gcdTimeInOL.FieldName = "TimeInOl";
            this.gcdTimeInOL.Name = "gcdTimeInOL";
            this.gcdTimeInOL.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdTimeInOL.Visible = true;
            this.gcdTimeInOL.VisibleIndex = 8;
            // 
            // gcdVisitLat
            // 
            this.gcdVisitLat.Caption = "gridColumn10";
            this.gcdVisitLat.FieldName = "VisitLat";
            this.gcdVisitLat.Name = "gcdVisitLat";
            this.gcdVisitLat.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gcdVisitLon
            // 
            this.gcdVisitLon.Caption = "gridColumn11";
            this.gcdVisitLon.FieldName = "VisitLong";
            this.gcdVisitLon.Name = "gcdVisitLon";
            this.gcdVisitLon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gcdOLLat
            // 
            this.gcdOLLat.Caption = "gridColumn12";
            this.gcdOLLat.FieldName = "OLLat";
            this.gcdOLLat.Name = "gcdOLLat";
            this.gcdOLLat.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gcdOLLon
            // 
            this.gcdOLLon.Caption = "gridColumn13";
            this.gcdOLLon.FieldName = "OLLong";
            this.gcdOLLon.Name = "gcdOLLon";
            this.gcdOLLon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gcdClaim
            // 
            this.gcdClaim.Caption = "Заявка";
            this.gcdClaim.FieldName = "OrderVolume";
            this.gcdClaim.Name = "gcdClaim";
            this.gcdClaim.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdClaim.Visible = true;
            this.gcdClaim.VisibleIndex = 9;
            // 
            // gcdRest
            // 
            this.gcdRest.Caption = "Остатки";
            this.gcdRest.FieldName = "Remainders";
            this.gcdRest.Name = "gcdRest";
            this.gcdRest.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdRest.Visible = true;
            this.gcdRest.VisibleIndex = 10;
            // 
            // gcdDispatch
            // 
            this.gcdDispatch.Caption = "Отгрузка";
            this.gcdDispatch.FieldName = "SalOut";
            this.gcdDispatch.Name = "gcdDispatch";
            this.gcdDispatch.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdDispatch.Visible = true;
            this.gcdDispatch.VisibleIndex = 11;
            // 
            // gcdOLTypeName
            // 
            this.gcdOLTypeName.Caption = "Тип ТТ";
            this.gcdOLTypeName.FieldName = "OLtype_name";
            this.gcdOLTypeName.Name = "gcdOLTypeName";
            this.gcdOLTypeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdOLTypeName.Visible = true;
            this.gcdOLTypeName.VisibleIndex = 12;
            // 
            // gcdDistance
            // 
            this.gcdDistance.Caption = "Расстояние до ТТ";
            this.gcdDistance.FieldName = "Distance";
            this.gcdDistance.Name = "gcdDistance";
            this.gcdDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdDistance.Visible = true;
            this.gcdDistance.VisibleIndex = 13;
            this.gcdDistance.Width = 90;
            // 
            // gcdNetworkOutletTypeName
            // 
            this.gcdNetworkOutletTypeName.Caption = "Признак наличия УТТ";
            this.gcdNetworkOutletTypeName.FieldName = "NetworkOutletType_name";
            this.gcdNetworkOutletTypeName.Name = "gcdNetworkOutletTypeName";
            this.gcdNetworkOutletTypeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gcdNetworkOutletTypeName.Visible = true;
            this.gcdNetworkOutletTypeName.VisibleIndex = 14;
            this.gcdNetworkOutletTypeName.Width = 90;
            // 
            // panelControlSummary
            // 
            this.panelControlSummary.Controls.Add(this.gridControlSummary);
            this.panelControlSummary.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlSummary.Location = new System.Drawing.Point(0, 0);
            this.panelControlSummary.MaximumSize = new System.Drawing.Size(0, 75);
            this.panelControlSummary.MinimumSize = new System.Drawing.Size(0, 75);
            this.panelControlSummary.Name = "panelControlSummary";
            this.panelControlSummary.Size = new System.Drawing.Size(583, 75);
            this.panelControlSummary.TabIndex = 2;
            // 
            // gridControlSummary
            // 
            this.gridControlSummary.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlSummary.DataSource = this.summaryDataBindingSource;
            this.gridControlSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSummary.Location = new System.Drawing.Point(2, 2);
            this.gridControlSummary.MainView = this.gridViewSummary;
            this.gridControlSummary.Name = "gridControlSummary";
            this.gridControlSummary.Size = new System.Drawing.Size(579, 71);
            this.gridControlSummary.TabIndex = 0;
            this.gridControlSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSummary});
            // 
            // summaryDataBindingSource
            // 
            this.summaryDataBindingSource.AllowNew = false;
            this.summaryDataBindingSource.DataSource = typeof(SoftServe.Reports.RouteInspector.DataProvider.SummaryData);
            // 
            // gridViewSummary
            // 
            this.gridViewSummary.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridViewSummary.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewSummary.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewSummary.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewSummary.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewSummary.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewSummary.ColumnPanelRowHeight = 44;
            this.gridViewSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTTInRoute,
            this.colVisitsByRoute,
            this.colVisitsInTTOnRoute,
            this.colVisitsAll,
            this.colTTWithoutLocations,
            this.colVisitsWithoutLocations,
            this.colClaimSummary,
            this.colDispatchSummary});
            this.gridViewSummary.GridControl = this.gridControlSummary;
            this.gridViewSummary.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridViewSummary.Name = "gridViewSummary";
            this.gridViewSummary.OptionsBehavior.Editable = false;
            this.gridViewSummary.OptionsCustomization.AllowFilter = false;
            this.gridViewSummary.OptionsCustomization.AllowSort = false;
            this.gridViewSummary.OptionsFilter.AllowFilterEditor = false;
            this.gridViewSummary.OptionsMenu.EnableColumnMenu = false;
            this.gridViewSummary.OptionsMenu.EnableFooterMenu = false;
            this.gridViewSummary.OptionsPrint.PrintFooter = false;
            this.gridViewSummary.OptionsView.ColumnAutoWidth = false;
            this.gridViewSummary.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSummary.OptionsView.ShowFooter = true;
            this.gridViewSummary.OptionsView.ShowGroupPanel = false;
            this.gridViewSummary.OptionsView.ShowIndicator = false;
            this.gridViewSummary.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // colTTInRoute
            // 
            this.colTTInRoute.Caption = "ТТ в маршруте";
            this.colTTInRoute.FieldName = "TTInRoute";
            this.colTTInRoute.Name = "colTTInRoute";
            this.colTTInRoute.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTTInRoute.Visible = true;
            this.colTTInRoute.VisibleIndex = 0;
            this.colTTInRoute.Width = 64;
            // 
            // colVisitsByRoute
            // 
            this.colVisitsByRoute.Caption = "Визитов по маршруту";
            this.colVisitsByRoute.FieldName = "VisitsByRoute";
            this.colVisitsByRoute.Name = "colVisitsByRoute";
            this.colVisitsByRoute.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colVisitsByRoute.Visible = true;
            this.colVisitsByRoute.VisibleIndex = 1;
            this.colVisitsByRoute.Width = 70;
            // 
            // colVisitsInTTOnRoute
            // 
            this.colVisitsInTTOnRoute.Caption = "Визитов в ТТ по маршруту  ";
            this.colVisitsInTTOnRoute.FieldName = "VisitsInTTOnRoute";
            this.colVisitsInTTOnRoute.Name = "colVisitsInTTOnRoute";
            this.colVisitsInTTOnRoute.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colVisitsInTTOnRoute.Visible = true;
            this.colVisitsInTTOnRoute.VisibleIndex = 2;
            this.colVisitsInTTOnRoute.Width = 78;
            // 
            // colVisitsAll
            // 
            this.colVisitsAll.Caption = "Визитов всего";
            this.colVisitsAll.FieldName = "VisitsAll";
            this.colVisitsAll.Name = "colVisitsAll";
            this.colVisitsAll.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colVisitsAll.Visible = true;
            this.colVisitsAll.VisibleIndex = 3;
            this.colVisitsAll.Width = 54;
            // 
            // colTTWithoutLocations
            // 
            this.colTTWithoutLocations.Caption = "ТТ без координат";
            this.colTTWithoutLocations.FieldName = "TTWithoutLocations";
            this.colTTWithoutLocations.Name = "colTTWithoutLocations";
            this.colTTWithoutLocations.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTTWithoutLocations.Visible = true;
            this.colTTWithoutLocations.VisibleIndex = 4;
            this.colTTWithoutLocations.Width = 65;
            // 
            // colVisitsWithoutLocations
            // 
            this.colVisitsWithoutLocations.Caption = "Визиты без координат";
            this.colVisitsWithoutLocations.FieldName = "VisitsWithoutLocations";
            this.colVisitsWithoutLocations.Name = "colVisitsWithoutLocations";
            this.colVisitsWithoutLocations.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colVisitsWithoutLocations.Visible = true;
            this.colVisitsWithoutLocations.VisibleIndex = 5;
            this.colVisitsWithoutLocations.Width = 69;
            // 
            // colClaimSummary
            // 
            this.colClaimSummary.Caption = "Заявка итого";
            this.colClaimSummary.FieldName = "ClaimSummary";
            this.colClaimSummary.Name = "colClaimSummary";
            this.colClaimSummary.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colClaimSummary.Visible = true;
            this.colClaimSummary.VisibleIndex = 6;
            this.colClaimSummary.Width = 90;
            // 
            // colDispatchSummary
            // 
            this.colDispatchSummary.Caption = "Отгрузка итого";
            this.colDispatchSummary.FieldName = "DispatchSummary";
            this.colDispatchSummary.Name = "colDispatchSummary";
            this.colDispatchSummary.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colDispatchSummary.Visible = true;
            this.colDispatchSummary.VisibleIndex = 7;
            this.colDispatchSummary.Width = 90;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.ScriptErrorsSuppressed = true;
            this.webBrowser.ScrollBarsEnabled = false;
            this.webBrowser.Size = new System.Drawing.Size(612, 600);
            this.webBrowser.TabIndex = 0;
            // 
            // ReportUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl);
            this.Name = "ReportUserControl";
            this.Size = new System.Drawing.Size(1200, 600);
            this.Load += new System.EventHandler(this.ReportUserControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
            this.splitContainerControl.ResumeLayout(false);
            this.scrollableControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlData)).EndInit();
            this.panelControlData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlSummary)).EndInit();
            this.panelControlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl;
        private SoftServe.Reports.RouteInspector.Utility.UnscrolledGrid.UnscrolledGridControl gridControlData;
        private SoftServe.Reports.RouteInspector.Utility.UnscrolledGrid.UnscrolledGridView gridViewData;
        private DevExpress.XtraGrid.GridControl gridControlSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gcdNo;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLInRoute;
        private DevExpress.XtraGrid.Columns.GridColumn gcdVisitInOL;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLId;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLName;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLAddress;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLBeginTime;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn gcdTimeInOL;
        private DevExpress.XtraGrid.Columns.GridColumn gcdVisitLat;
        private DevExpress.XtraGrid.Columns.GridColumn gcdVisitLon;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLLat;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLLon;
        private DevExpress.XtraGrid.Columns.GridColumn gcdDistance;
        private DevExpress.XtraGrid.Columns.GridColumn gcdClaim;
        private DevExpress.XtraGrid.Columns.GridColumn gcdRest;
        private DevExpress.XtraGrid.Columns.GridColumn gcdDispatch;
        private DevExpress.XtraGrid.Columns.GridColumn gcdOLTypeName;
        private DevExpress.XtraEditors.XtraScrollableControl scrollableControl;
        private DevExpress.XtraEditors.PanelControl panelControlData;
        private DevExpress.XtraEditors.PanelControl panelControlSummary;
        private System.Windows.Forms.BindingSource summaryDataBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTTInRoute;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitsByRoute;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitsInTTOnRoute;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitsAll;
        private DevExpress.XtraGrid.Columns.GridColumn colTTWithoutLocations;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitsWithoutLocations;
        private DevExpress.XtraGrid.Columns.GridColumn colClaimSummary;
        private DevExpress.XtraGrid.Columns.GridColumn colDispatchSummary;
        private System.Windows.Forms.WebBrowser webBrowser;
        private DevExpress.XtraGrid.Columns.GridColumn gcdNetworkOutletTypeName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rCheckEdit;
    }
}
