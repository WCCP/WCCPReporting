﻿namespace SoftServe.Reports.RouteInspector.UserControl
{
    partial class SettingsFormUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeList = new DevExpress.XtraTreeList.TreeList();
            this.tlcM1Name = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlcLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlcDataId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.btnSubmit = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlSelectM1 = new DevExpress.XtraEditors.LabelControl();
            this.dtReportDate = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // treeList
            // 
            this.treeList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeList.Appearance.FocusedCell.BackColor = System.Drawing.Color.LightSkyBlue;
            this.treeList.Appearance.FocusedCell.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.treeList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.treeList.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSkyBlue;
            this.treeList.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.treeList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlcM1Name,
            this.tlcLevel,
            this.tlcDataId});
            this.treeList.Location = new System.Drawing.Point(3, 48);
            this.treeList.Name = "treeList";
            this.treeList.OptionsBehavior.Editable = false;
            this.treeList.OptionsView.ShowColumns = false;
            this.treeList.OptionsView.ShowIndicator = false;
            this.treeList.ParentFieldName = "PARENT_ID";
            this.treeList.Size = new System.Drawing.Size(293, 314);
            this.treeList.TabIndex = 0;
            this.treeList.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeList_FocusedNodeChanged);
            // 
            // tlcM1Name
            // 
            this.tlcM1Name.Caption = "DATA";
            this.tlcM1Name.FieldName = "DATA";
            this.tlcM1Name.Name = "tlcM1Name";
            this.tlcM1Name.Visible = true;
            this.tlcM1Name.VisibleIndex = 0;
            // 
            // tlcLevel
            // 
            this.tlcLevel.Caption = "LEVEL";
            this.tlcLevel.FieldName = "LEVEL";
            this.tlcLevel.Name = "tlcLevel";
            // 
            // tlcDataId
            // 
            this.tlcDataId.Caption = "DATA_ID";
            this.tlcDataId.FieldName = "DATA_ID";
            this.tlcDataId.Name = "tlcDataId";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Location = new System.Drawing.Point(61, 368);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(144, 23);
            this.btnSubmit.TabIndex = 1;
            this.btnSubmit.Text = "Сгенерировать отчет";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(211, 368);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отменить";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(126, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Дата для мониторинга*:";
            // 
            // labelControlSelectM1
            // 
            this.labelControlSelectM1.Location = new System.Drawing.Point(3, 29);
            this.labelControlSelectM1.Name = "labelControlSelectM1";
            this.labelControlSelectM1.Size = new System.Drawing.Size(77, 13);
            this.labelControlSelectM1.TabIndex = 4;
            this.labelControlSelectM1.Text = "Выберите М1*:";
            // 
            // dtReportDate
            // 
            this.dtReportDate.EditValue = null;
            this.dtReportDate.Location = new System.Drawing.Point(135, 3);
            this.dtReportDate.Name = "dtReportDate";
            this.dtReportDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtReportDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtReportDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtReportDate.Size = new System.Drawing.Size(100, 20);
            this.dtReportDate.TabIndex = 5;
            // 
            // SettingsFormUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dtReportDate);
            this.Controls.Add(this.labelControlSelectM1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.treeList);
            this.Name = "SettingsFormUserControl";
            this.Size = new System.Drawing.Size(299, 394);
            this.Load += new System.EventHandler(this.SettingsFormUserControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeList;
        private DevExpress.XtraEditors.SimpleButton btnSubmit;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControlSelectM1;
        private DevExpress.XtraEditors.DateEdit dtReportDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcM1Name;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcDataId;
    }
}
