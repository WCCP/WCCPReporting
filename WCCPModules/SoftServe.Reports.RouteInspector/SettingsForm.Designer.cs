namespace SoftServe.Reports.RouteInspector
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settingsFormUserControl = new SoftServe.Reports.RouteInspector.UserControl.SettingsFormUserControl();
            this.SuspendLayout();
            // 
            // settingsFormUserControl
            // 
            this.settingsFormUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsFormUserControl.Location = new System.Drawing.Point(5, 5);
            this.settingsFormUserControl.Name = "settingsFormUserControl";
            this.settingsFormUserControl.Size = new System.Drawing.Size(303, 374);
            this.settingsFormUserControl.TabIndex = 0;
            this.settingsFormUserControl.CancelClick += new System.EventHandler(this.settingsFormUserControl_CancelClick);
            this.settingsFormUserControl.SubmitClick += new System.EventHandler(this.settingsFormUserControl_SubmitClick);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 384);
            this.Controls.Add(this.settingsFormUserControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������� ������";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private RouteInspector.UserControl.SettingsFormUserControl settingsFormUserControl;

    }
}