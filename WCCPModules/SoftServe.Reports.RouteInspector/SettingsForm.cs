#region

using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SoftServe.Reports.RouteInspector.DataProvider;
using SoftServe.Reports.RouteInspector.Utility;

#endregion

namespace SoftServe.Reports.RouteInspector
{
    public partial class SettingsForm : XtraForm
    {
        #region Constructors

        public SettingsForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        public ReportOptions ReportOptions { get; private set; }

        #endregion

        #region Event Handling

        private bool isLoaded = false;
        private void SettingsForm_Load(object sender, EventArgs e)
        {
            Text = String.Format("��������� ������ {0}", Constants.ReportName);
            if (!isLoaded)
            {
                isLoaded = true;
            }
        }

        private void settingsFormUserControl_CancelClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void settingsFormUserControl_SubmitClick(object sender, EventArgs e)
        {
            ReportOptions = settingsFormUserControl.GetReportOptions();
            DialogResult = DialogResult.OK;
        }

        #endregion
    }
}