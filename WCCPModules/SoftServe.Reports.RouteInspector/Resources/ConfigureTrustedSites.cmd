@ECHO OFF
SETLOCAL

SET REG="%WINDIR%\system32\reg.exe"
SET INTRANET_DOMAINS=about:security_wccpreporting.exe
SET TRUSTED_DOMAINS=maps.google.com maps.gstatic.com security_wccpreporting.exe

IF EXIST "%USERPROFILE%\..\Default User\NTUSER.DAT" SET NTUSER="%USERPROFILE%\..\Default User\NTUSER.DAT"
IF EXIST "%USERPROFILE%\..\Default\NTUSER.DAT" SET NTUSER="%USERPROFILE%\..\Default\NTUSER.DAT"
IF DEFINED PROGRAMFILES(x86) SET X64=TRUE

ECHO Adding domains to Intranet Zone for HKEY_LOCAL_MACHINE
FOR %%D IN (%INTRANET_DOMAINS%) DO (
  ECHO -^> %%D
 %REG% add "HKLM\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\%%D" /v * /t REG_DWORD /d 1 /f
  %REG% add "HKLM\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\%%D" /v * /t REG_DWORD /d 1 /f
  IF DEFINED X64 %REG% add "HKLM\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\%%D" /v * /t REG_DWORD /d 1 /f
  IF DEFINED X64 %REG% add "HKLM\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\%%D" /v * /t REG_DWORD /d 1 /f 
 )

ECHO Adding domains to Trusted Zone for HKEY_LOCAL_MACHINE
FOR %%D IN (%TRUSTED_DOMAINS%) DO (
  ECHO -^> %%D
  %REG% add "HKLM\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\%%D" /v * /t REG_DWORD /d 2 /f
  %REG% add "HKLM\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\%%D" /v * /t REG_DWORD /d 2 /f
)

IF DEFINED NTUSER ECHO Loading registry defaults for new user from %NTUSER%
IF DEFINED NTUSER %REG% load HKU\.NTUSER %NTUSER%

FOR /f "usebackq tokens=1,2 delims=_" %%A IN (`%REG% query HKU`) DO (
  ECHO Adding domains to Intranet Zone for %%A_%%B
  FOR %%D IN (%INTRANET_DOMAINS%) DO (
   ECHO -^> %%D
   %REG% add "%%A_%%B\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\%%D" /v * /t REG_DWORD /d 1 /f
   %REG% add "%%A_%%B\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\%%D" /v * /t REG_DWORD /d 1 /f
   IF DEFINED X64 %REG% add "%%A_%%B\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\%%D" /v * /t REG_DWORD /d 1 /f
   IF DEFINED X64 %REG% add "%%A_%%B\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\%%D" /v * /t REG_DWORD /d 1 /f
  )
  ECHO Adding domains to Trusted Zone for %%A_%%B
  FOR %%D IN (%TRUSTED_DOMAINS%) DO (
   ECHO -^> %%D
   %REG% add "%%A_%%B\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\%%D" /v * /t REG_DWORD /d 2 /f
   %REG% add "%%A_%%B\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\%%D" /v * /t REG_DWORD /d 2 /f
   IF DEFINED X64 %REG% add "%%A_%%B\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\%%D" /v * /t REG_DWORD /d 2 /f
   IF DEFINED X64 %REG% add "%%A_%%B\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\%%D" /v * /t REG_DWORD /d 2 /f
  )
)

IF DEFINED NTUSER ECHO Unloading new user registry defaults
IF DEFINED NTUSER %REG% unload HKU\.NTUSER

ENDLOCAL
