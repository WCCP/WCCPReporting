using System;
using System.Collections.Generic;

namespace SoftServe.Reports.RouteInspector.DataProvider
{
    public class ReportOptions
    {
        public ReportOptions(DateTime reportDate, int m1Id)
        {
            ReportDate = reportDate;
            M1Id = m1Id;
        }

        public DateTime ReportDate { get; set; }
        public int M1Id{ get; set; }
    }
}