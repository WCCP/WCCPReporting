﻿namespace SoftServe.Reports.RouteInspector.DataProvider
{
    public class SummaryData
    {
        /// <summary>
        /// ТТ в маршруте
        /// </summary>
        public int TTInRoute { get; set; }
        /// <summary>
        /// Визитов по маршруту
        /// </summary>
        public int VisitsByRoute { get; set; }
        /// <summary>
        /// Визитов в ТТ по маршруту  
        /// </summary>
        public int VisitsInTTOnRoute { get; set; }
        /// <summary>
        /// Визитов всего
        /// </summary>
        public int VisitsAll { get; set; }
        /// <summary>
        /// ТТ без координат
        /// </summary>
        public int TTWithoutLocations { get; set; }
        /// <summary>
        /// Визиты без координат
        /// </summary>
        public int VisitsWithoutLocations { get; set; }
        /// <summary>
        /// Заявка итого
        /// </summary>
        public decimal ClaimSummary { get; set; }
        /// <summary>
        /// Отгрузка итого
        /// </summary>
        public decimal DispatchSummary { get; set; }
    }
}