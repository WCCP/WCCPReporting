﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataAccess;
using Microsoft.Win32;
using SoftServe.Reports.RouteInspector.Properties;

namespace SoftServe.Reports.RouteInspector.DataProvider
{
    public static class DataAccessProvider
    {
        private static string[] expectedKeys = new[]
                                                   {
                                                       @"SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\about:security_wccpreporting.exe",
                                                       @"SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\maps.google.com",
                                                       @"SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\maps.gstatic.com",
                                                       @"SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\security_wccpreporting.exe"
                                                   }; //HKEY_LOCAL_MACHINE\

        private const string PERSONEL_TREE = "GPS.spRouteInspector_MTreeGet";
        private const string MAIN_REPORT = "GPS.spRouteInspectorGet";
        private const string MAIN_REPORT_PRM_1 = "Date";
        private const string MAIN_REPORT_PRM_2 = "Merch_id";
        private const string MIN_GPS_DISTANCE = 
@"SELECT ISNULL(
           (
               SELECT TOP 1 CONVERT(INT, ParamValue)
               FROM   DW_Params AS par
               WHERE  ParamName = 'DistanceForGPS'
           ),
           100
       )
";
        private const string GOOGLE_API_KEY =
@"SELECT ISNULL(
           (
               SELECT TOP 1 ParamValue
               FROM   DW_Params AS par
               WHERE  ParamName = 'GoogleAPI.Key'
           ),
           'abcdefg'
       )
";

        public static DataTable GetPersonelTree()
        {
            var procResult = DataAccessLayer.ExecuteStoredProcedure(PERSONEL_TREE, new SqlParameter[] { });
            if (procResult.Tables.Count > 0)
            {
                return procResult.Tables[0];
            }
            return null;
        }

        public static DataTable GetReport(ReportOptions options)
        {
            var procResult = DataAccessLayer.ExecuteStoredProcedure(MAIN_REPORT,
                                                                    new[]
                                                                        {
                                                                            new SqlParameter(MAIN_REPORT_PRM_1,options.ReportDate),
                                                                            new SqlParameter(MAIN_REPORT_PRM_2,options.M1Id)
                                                                        });
            if (procResult.Tables.Count > 0)
            {
                return procResult.Tables[0];
            }
            return null;
        }

        private static int? minGPSDistance = null;
        public static int MinGPSDistance
        {
            get
            {
                if (!minGPSDistance.HasValue)
                {
                    minGPSDistance = ConvertEx.ToInt(DataAccessLayer.ExecuteScalarQuery(MIN_GPS_DISTANCE));
                }
                return minGPSDistance.Value;
            }
        }

        public static string GoogleApiKey
        {
            get { return ConvertEx.ToString(DataAccessLayer.ExecuteScalarQuery(GOOGLE_API_KEY)); }
        }

        public static bool IsTrustedSitesConfigured()
        {
            try
            {
                foreach (var key in expectedKeys)
                {
                    var subKey = Registry.LocalMachine.OpenSubKey(key);
                    if (subKey == null)
                        throw new Exception();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static void ConfigureTrustedSites()
        {
            var fileName = Path.Combine(Path.GetTempPath(), "wccp_add_trusted_sites.cmd");
            try
            {
                File.WriteAllText(fileName, Resources.ConfigureTrustedSites);
                var startInfo = new ProcessStartInfo
                                    {
                                        FileName = fileName, 
                                        CreateNoWindow = true, 
                                        UseShellExecute = false, 
                                        RedirectStandardOutput = true
                                    };

                using (var process = Process.Start(startInfo))
                {
                    var output = process.StandardOutput.ReadToEnd(); //Debug
                    process.WaitForExit();
                }
            } catch { }
            
            try
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            } catch { }
        }
    }
}