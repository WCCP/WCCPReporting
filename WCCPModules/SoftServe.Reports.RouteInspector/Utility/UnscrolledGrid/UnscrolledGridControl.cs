﻿// Developer Express Code Central Example:
// How to create a GridView descendant class and register it for design-time use
// 
// This is an example of a custom GridView and a custom control that inherits the
// DevExpress.XtraGrid.GridControl. Make sure to build the project prior to opening
// Form1 in the designer. Please refer to the Knowledge Base article for the
// additional information.
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E900

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Registrator;

namespace SoftServe.Reports.RouteInspector.Utility.UnscrolledGrid {
	public class UnscrolledGridControl : GridControl {
		protected override BaseView CreateDefaultView() {
			return CreateView("UnscrolledGridView");
		}
		protected override void RegisterAvailableViewsCore(InfoCollection collection) {
			base.RegisterAvailableViewsCore(collection);
			collection.Add(new UnscrolledGridViewInfoRegistrator());
		}
	}
}
