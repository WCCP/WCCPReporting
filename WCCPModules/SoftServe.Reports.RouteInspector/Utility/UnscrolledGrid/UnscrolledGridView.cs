﻿// Developer Express Code Central Example:
// How to create a GridView descendant class and register it for design-time use
// 
// This is an example of a custom GridView and a custom control that inherits the
// DevExpress.XtraGrid.GridControl. Make sure to build the project prior to opening
// Form1 in the designer. Please refer to the Knowledge Base article for the
// additional information.
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E900

using DevExpress.XtraGrid.Columns;

namespace SoftServe.Reports.RouteInspector.Utility.UnscrolledGrid {
	public class UnscrolledGridView : DevExpress.XtraGrid.Views.Grid.GridView {
		public UnscrolledGridView() : this(null) {}
        public UnscrolledGridView(DevExpress.XtraGrid.GridControl grid) : base(grid) { }
		protected override string ViewName { get { return "UnscrolledGridView"; } }

        bool lockVisibleColumns = false;

        protected override void DoAfterFocusedColumnChanged(GridColumn prevFocusedColumn, GridColumn focusedColumn) {
            lockVisibleColumns = !MakeFocusedColumnVisible;
            base.DoAfterFocusedColumnChanged(prevFocusedColumn, focusedColumn);
            lockVisibleColumns = false;
        }

        public override void ShowEditor() {
            lockVisibleColumns = !MakeFocusedColumnVisible;
            base.ShowEditor();
            lockVisibleColumns = false;
        }

        public override void MakeColumnVisible(GridColumn column) {
            if (!lockVisibleColumns) {
                base.MakeColumnVisible(column);
            }
        }
        public override void Assign(DevExpress.XtraGrid.Views.Base.BaseView v, bool copyEvents) {
            BeginUpdate();
            try {
                base.Assign(v, copyEvents);
                MakeFocusedColumnVisible = ((UnscrolledGridView)v).MakeFocusedColumnVisible;
            } finally {
                EndUpdate();
            }
        }
        private bool _MakeFocusedColumnVisible = true;
        public bool MakeFocusedColumnVisible {
            get { return _MakeFocusedColumnVisible; }
            set {
                _MakeFocusedColumnVisible = value;
            }
        }
        
	}
}
