﻿using System.Drawing;
using System.Windows.Forms;

namespace SoftServe.Reports.RouteInspector.Utility.Capturing
{
    public static class ControlExtensions
    {
        public static Image DrawToImage(this Control control)
        {
            return Utilities.CaptureWindow(control.Handle);
        }
    }
}