﻿namespace SoftServe.Reports.RouteInspector.Utility
{
    public static class Constants
    {
        /// <summary>
        /// Name of report which configured in WCCP reports tree
        /// </summary>
        public static string ReportName { get; set; }
    }
}