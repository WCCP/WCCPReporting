﻿namespace SoftServe.Reports.RouteInspector.Utility
{
    public enum MarkerColor
    {
        None = 0,
        Green = 1,
        Yellow = 2,
        Red = 3
    }
}