﻿#region

using System.Runtime.InteropServices;
using SoftServe.Reports.RouteInspector.UserControl;

#endregion

namespace SoftServe.Reports.RouteInspector.Utility
{
    /// <summary>
    ///   This class used to handle requests from webbrowser JS code to .NET code
    /// </summary>
    [ComVisible(true)]
    public class ScriptManager
    {
        #region Fields

        private ReportUserControl obj;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "ScriptManager" /> class.
        /// </summary>
        /// <param name = "form">The form.</param>
        public ScriptManager(ReportUserControl form)
        {
            obj = form;
        }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Method which called then initializaation of objects in JS is compaleted
        /// </summary>
        public void InitializationCompleted()
        {
            obj.CBInitializationCompleted();
        }

        /// <summary>
        ///   Selects the OL in table.
        /// </summary>
        /// <param name = "olId">The ol id.</param>
        public void SelectOL(string olId)
        {
            obj.CBSelectOL(olId);
        }

        #endregion
    }
}