﻿using System;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using Microsoft.Win32;
using SoftServe.Reports.RouteInspector;
using SoftServe.Reports.RouteInspector.DataProvider;
using SoftServe.Reports.RouteInspector.Utility;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpRouteInspector.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                if (!DataAccessProvider.IsTrustedSitesConfigured())
                {
                    DataAccessProvider.ConfigureTrustedSites();
                }

                Constants.ReportName = reportCaption;
                                
                DataAccessLayer.OpenConnection();

                SettingsForm setForm = new SettingsForm();

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.SettingsForm = setForm;

                    _reportControl = new WccpUIControl();
                    _reportCaption = reportCaption;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        public void CloseUI()
        {
            //throw new NotImplementedException();
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}