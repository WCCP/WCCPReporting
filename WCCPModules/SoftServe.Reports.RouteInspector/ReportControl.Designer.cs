﻿using Logica.Reports.BaseReportControl;
namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.barMenuManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnSettings = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportTable = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportMap = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportTableAndMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShowMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShowTrack = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.reportUserControl = new SoftServe.Reports.RouteInspector.UserControl.ReportUserControl();
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // barMenuManager
            // 
            this.barMenuManager.AllowCustomization = false;
            this.barMenuManager.AllowMoveBarOnToolbar = false;
            this.barMenuManager.AllowQuickCustomization = false;
            this.barMenuManager.AllowShowToolbarsPopup = false;
            this.barMenuManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barMenuManager.Controller = this.barAndDockingController1;
            this.barMenuManager.DockControls.Add(this.barDockControlTop);
            this.barMenuManager.DockControls.Add(this.barDockControlBottom);
            this.barMenuManager.DockControls.Add(this.barDockControlLeft);
            this.barMenuManager.DockControls.Add(this.barDockControlRight);
            this.barMenuManager.DockControls.Add(this.standaloneBarDockControl1);
            this.barMenuManager.Form = this;
            this.barMenuManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSettings,
            this.btnRefresh,
            this.btnExportTo,
            this.btnExportTable,
            this.btnExportMap,
            this.btnExportTableAndMap,
            this.bbiShowMap,
            this.bbiShowTrack});
            this.barMenuManager.LargeImages = this.imCollection;
            this.barMenuManager.MaxItemId = 46;
            // 
            // bar1
            // 
            this.bar1.BarName = "menuBar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(74, 165);
            this.bar1.FloatSize = new System.Drawing.Size(48, 32);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnRefresh, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSettings),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTo, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShowMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShowTrack)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "menuBar";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Обновить";
            this.btnRefresh.Hint = "Обновить";
            this.btnRefresh.Id = 6;
            this.btnRefresh.LargeImageIndex = 0;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnSettings
            // 
            this.btnSettings.Caption = "Параметры отчета";
            this.btnSettings.Hint = "Параметры отчета";
            this.btnSettings.Id = 3;
            this.btnSettings.LargeImageIndex = 6;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSettings_ItemClick);
            // 
            // btnExportTo
            // 
            this.btnExportTo.Caption = "Экспортировать в ...";
            this.btnExportTo.Hint = "Экспортировать в ...";
            this.btnExportTo.Id = 10;
            this.btnExportTo.LargeImageIndex = 10;
            this.btnExportTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTable, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportTableAndMap)});
            this.btnExportTo.Name = "btnExportTo";
            // 
            // btnExportTable
            // 
            this.btnExportTable.Caption = "Экспортировать таблицу с данными";
            this.btnExportTable.Id = 11;
            this.btnExportTable.LargeImageIndex = 23;
            this.btnExportTable.Name = "btnExportTable";
            this.btnExportTable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportMap
            // 
            this.btnExportMap.Caption = "Экспортировать карту";
            this.btnExportMap.Id = 12;
            this.btnExportMap.LargeImageIndex = 24;
            this.btnExportMap.Name = "btnExportMap";
            this.btnExportMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportTableAndMap
            // 
            this.btnExportTableAndMap.Caption = "Экспортировать и данные и карту";
            this.btnExportTableAndMap.Id = 13;
            this.btnExportTableAndMap.LargeImageIndex = 22;
            this.btnExportTableAndMap.Name = "btnExportTableAndMap";
            this.btnExportTableAndMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiShowMap.Caption = "Показать/скрыть карту";
            this.bbiShowMap.Down = true;
            this.bbiShowMap.Hint = "Показать/скрыть карту";
            this.bbiShowMap.Id = 44;
            this.bbiShowMap.LargeImageIndex = 20;
            this.bbiShowMap.Name = "bbiShowMap";
            this.bbiShowMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowMap_ItemClick);
            // 
            // bbiShowTrack
            // 
            this.bbiShowTrack.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiShowTrack.Caption = "Показать/скрыть трек";
            this.bbiShowTrack.Hint = "Показать/скрыть трек";
            this.bbiShowTrack.Id = 45;
            this.bbiShowTrack.LargeImageIndex = 21;
            this.bbiShowTrack.Name = "bbiShowTrack";
            this.bbiShowTrack.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowTrack_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(956, 32);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "refresh_24.png");
            this.imCollection.Images.SetKeyName(1, "briefcase_prev_24.png");
            this.imCollection.Images.SetKeyName(2, "del_24.png");
            this.imCollection.Images.SetKeyName(3, "Excel_42.bmp");
            this.imCollection.Images.SetKeyName(4, "ExcelAll_42.bmp");
            this.imCollection.Images.SetKeyName(5, "print_24.png");
            this.imCollection.Images.SetKeyName(6, "briefcase_ok_24.png");
            this.imCollection.Images.SetKeyName(7, "briefcase_save_24.png");
            this.imCollection.Images.SetKeyName(15, "xlsx.png");
            this.imCollection.Images.SetKeyName(16, "bmp.PNG");
            this.imCollection.Images.SetKeyName(17, "csv.PNG");
            this.imCollection.Images.SetKeyName(18, "copy_prev_24.png");
            this.imCollection.Images.SetKeyName(19, "doc_prev_24.png");
            this.imCollection.Images.SetKeyName(20, "1320671828_map.png");
            this.imCollection.Images.SetKeyName(21, "1320671891_start_here.png");
            this.imCollection.Images.SetKeyName(22, "1321536551_Layers.png");
            this.imCollection.Images.SetKeyName(23, "1321536592_table-go.png");
            this.imCollection.Images.SetKeyName(24, "1321536619_map.png");
            // 
            // reportUserControl
            // 
            this.reportUserControl.DataSource = null;
            this.reportUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportUserControl.Location = new System.Drawing.Point(0, 32);
            this.reportUserControl.Name = "reportUserControl";
            this.reportUserControl.Size = new System.Drawing.Size(956, 444);
            this.reportUserControl.TabIndex = 5;
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.reportUserControl);
            this.Controls.Add(this.standaloneBarDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(956, 476);
            this.Load += new System.EventHandler(this.WccpUIControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barMenuManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem btnSettings;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarSubItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem btnExportTable;
        private DevExpress.XtraBars.BarButtonItem btnExportMap;
        private DevExpress.XtraBars.BarButtonItem btnExportTableAndMap;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarButtonItem bbiShowMap;
        private DevExpress.XtraBars.BarButtonItem bbiShowTrack;
        private SoftServe.Reports.RouteInspector.UserControl.ReportUserControl reportUserControl;

    }
}
