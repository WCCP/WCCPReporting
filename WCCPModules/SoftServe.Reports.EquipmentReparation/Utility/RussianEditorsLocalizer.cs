﻿using DevExpress.XtraEditors.Controls;

namespace SoftServe.Reports.EquipmentReparation
{
    public class RussianEditorsLocalizer : Localizer
    {
        public override string Language
        {
            get { return "Russian"; }
        }

        public override string GetLocalizedString(StringId id)
        {
            string ret;

            switch (id)
            {
                case StringId.DateEditClear:
                    ret = "Очистить";
                    break;
                case StringId.DateEditToday:
                    ret = "Сегодня";
                    break;
                default: ret = base.GetLocalizedString(id);
                    break;
            }

            return ret;
        }
    }
}
