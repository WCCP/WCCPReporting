﻿using System.Collections.Generic;
using Logica.Reports.BaseReportControl.CommonFunctionality;

namespace SoftServe.Reports.EquipmentReparation
{
    /// <summary>
    /// Describes the report settings
    /// </summary>
    public class ReportInfo
    {
        public DateInterval ReportDates { get; set; }

        public List<string> RegionIds { get; set; }
        public List<string> StaffingsIds { get; set; }

        public List<string> ServiceCentersIds { get; set; }
        public List<string> ApplicationStatusesIds { get; set; }

        public List<string> PosIds { get; set; }
        public List<string> WarehousesIds { get; set; }
        
        public List<string> ModelsIds { get; set; }
        public List<string> ManufacturersIds { get; set; }
        public List<string> BrandsIds { get; set; }

        public DateInterval ProductionDates { get; set; }
        public DateInterval RepairDates { get; set; }

        public bool ShowRepeatedApp { get; set; }
        public bool ShowUninstalationApp { get; set; }
        public bool ShowMiscallApp { get; set; }
        
        public bool DetalizationByBreaks { get; set; }
        public bool DetalizationByBreaksType { get; set; }
        public bool DetalizationByWorks { get; set; }
        public bool DetalizationByElements { get; set; }
    }
}
