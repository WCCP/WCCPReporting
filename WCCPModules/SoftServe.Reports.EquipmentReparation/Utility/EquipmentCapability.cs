﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.EquipmentReparation.Utility
{
    public enum EquipmentCapability
    {
        Capable,
        Incapable
    }
}
