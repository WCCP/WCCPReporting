﻿using Logica.Reports.BaseReportControl.CommonControls;
using SoftServe.Reports.EquipmentReparation.UserControls;
using ComboboxExtended = Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended;
using PeriodChanged = SoftServe.Reports.EquipmentReparation.UserControls.PeriodChanged;

namespace SoftServe.Reports.EquipmentReparation
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.generateReportButton = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.cbDistrType = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.lDistrType = new DevExpress.XtraEditors.LabelControl();
            this.lDistr = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lWarehouses = new DevExpress.XtraEditors.LabelControl();
            this.lBrand = new DevExpress.XtraEditors.LabelControl();
            this.lManufacturer = new DevExpress.XtraEditors.LabelControl();
            this.tTerritory = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colTerritory = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tStaffing = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colStaffing = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffWStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cluDistr = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.gridLookUpWithMultipleChecking1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFieldID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrFieldParentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIeldCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lServiceCenter = new DevExpress.XtraEditors.LabelControl();
            this.cbPos = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.tWarehouseLocation = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tPosLocation = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colPosLocGeographyName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPosLocGeographyID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPosLocGeographyOldId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cluWarehouses = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.userControl21View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExternalCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareParentId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lApplicationStatuses = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tModels = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.treeListColumn5 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cbBrand = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.cbManufacturer = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.tRepairDates = new SoftServe.Reports.EquipmentReparation.UserControls.TimeInterval();
            this.tProductionDates = new SoftServe.Reports.EquipmentReparation.UserControls.TimeInterval();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.component1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tReportDates = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.cRepeatedApp = new DevExpress.XtraEditors.CheckEdit();
            this.cUninstalation = new DevExpress.XtraEditors.CheckEdit();
            this.cMiscall = new DevExpress.XtraEditors.CheckEdit();
            this.cDetalizationByElements = new DevExpress.XtraEditors.CheckEdit();
            this.cDetalizationByBreaksType = new DevExpress.XtraEditors.CheckEdit();
            this.cDetalizationByWorks = new DevExpress.XtraEditors.CheckEdit();
            this.cDetalizationByBreaks = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cStaffing = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.cModels = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cAppClosed = new DevExpress.XtraEditors.CheckEdit();
            this.lAppStatusesList = new DevExpress.XtraEditors.LabelControl();
            this.cAppOpen = new DevExpress.XtraEditors.CheckEdit();
            this.cApplicationStatuses = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.cPos = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.cWareHouses = new DevExpress.XtraEditors.CheckEdit();
            this.cbServiceCenter = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluDistr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpWithMultipleChecking1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tWarehouseLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPosLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluWarehouses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tModels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBrand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbManufacturer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cRepeatedApp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUninstalation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMiscall.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByElements.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByBreaksType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByWorks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByBreaks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cModels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cAppClosed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cAppOpen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cApplicationStatuses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cWareHouses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbServiceCenter.Properties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // generateReportButton
            // 
            this.generateReportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.generateReportButton.Image = global::SoftServe.Reports.EquipmentReparation.Properties.Resources.check;
            this.generateReportButton.Location = new System.Drawing.Point(743, 499);
            this.generateReportButton.Name = "generateReportButton";
            this.generateReportButton.Size = new System.Drawing.Size(146, 23);
            this.generateReportButton.TabIndex = 0;
            this.generateReportButton.Text = "Сгенерировать отчет";
            this.generateReportButton.Click += new System.EventHandler(this.generateReportButton_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Image = global::SoftServe.Reports.EquipmentReparation.Properties.Resources.close;
            this.simpleButton2.Location = new System.Drawing.Point(906, 499);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(86, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Отмена";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Appearance.Options.UseFont = true;
            this.lblUpd2.Location = new System.Drawing.Point(77, 499);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(0, 13);
            this.lblUpd2.TabIndex = 22;
            // 
            // cbDistrType
            // 
            this.cbDistrType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDistrType.Delimiter = ",";
            this.cbDistrType.Location = new System.Drawing.Point(237, 28);
            this.cbDistrType.Name = "cbDistrType";
            this.cbDistrType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDistrType.Properties.DisplayMember = "SchemaName";
            this.cbDistrType.Properties.DropDownRows = 15;
            this.cbDistrType.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbDistrType.Properties.ShowButtons = false;
            this.cbDistrType.Properties.ValueMember = "SchemaID";
            this.cbDistrType.Size = new System.Drawing.Size(240, 20);
            this.cbDistrType.TabIndex = 28;
            this.cbDistrType.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbDistrType_Closed);
            // 
            // lDistrType
            // 
            this.lDistrType.Location = new System.Drawing.Point(237, 3);
            this.lDistrType.Name = "lDistrType";
            this.lDistrType.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lDistrType.Size = new System.Drawing.Size(109, 18);
            this.lDistrType.TabIndex = 29;
            this.lDistrType.Text = "Тип дистрибьютора :";
            // 
            // lDistr
            // 
            this.lDistr.Location = new System.Drawing.Point(237, 50);
            this.lDistr.Name = "lDistr";
            this.lDistr.Size = new System.Drawing.Size(80, 13);
            this.lDistr.TabIndex = 30;
            this.lDistr.Text = "Дистрибьютор:";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(237, 87);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(17, 13);
            this.labelControl15.TabIndex = 34;
            this.labelControl15.Text = "ТС:";
            // 
            // lWarehouses
            // 
            this.lWarehouses.Location = new System.Drawing.Point(483, 166);
            this.lWarehouses.Name = "lWarehouses";
            this.lWarehouses.Size = new System.Drawing.Size(36, 13);
            this.lWarehouses.TabIndex = 36;
            this.lWarehouses.Text = "Склад:";
            // 
            // lBrand
            // 
            this.lBrand.Location = new System.Drawing.Point(237, 48);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(34, 13);
            this.lBrand.TabIndex = 42;
            this.lBrand.Text = "Брэнд:";
            // 
            // lManufacturer
            // 
            this.lManufacturer.Location = new System.Drawing.Point(237, 3);
            this.lManufacturer.Name = "lManufacturer";
            this.lManufacturer.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.lManufacturer.Size = new System.Drawing.Size(83, 20);
            this.lManufacturer.TabIndex = 44;
            this.lManufacturer.Text = "Производитель:";
            // 
            // tTerritory
            // 
            this.tTerritory.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colTerritory});
            this.tTerritory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tTerritory.KeyFieldName = "GeographyID";
            this.tTerritory.Location = new System.Drawing.Point(2, 22);
            this.tTerritory.Name = "tTerritory";
            this.tTerritory.Size = new System.Drawing.Size(237, 203);
            this.tTerritory.TabIndex = 77;
            this.tTerritory.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tTerritory_AfterNodeChecked);
            // 
            // colTerritory
            // 
            this.colTerritory.Caption = "Территория";
            this.colTerritory.CustomizationCaption = "Территория";
            this.colTerritory.FieldName = "GeographyName";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsColumn.AllowEdit = false;
            this.colTerritory.OptionsColumn.ReadOnly = true;
            this.colTerritory.Visible = true;
            this.colTerritory.VisibleIndex = 0;
            // 
            // tStaffing
            // 
            this.tStaffing.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colStaffing,
            this.colStaffParentId,
            this.colStaffWStaffId,
            this.colStaffStaffId});
            this.tStaffing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tStaffing.KeyFieldName = "wStaffID";
            this.tStaffing.Location = new System.Drawing.Point(2, 22);
            this.tStaffing.Name = "tStaffing";
            this.tStaffing.ParentFieldName = "wParentID";
            this.tStaffing.Size = new System.Drawing.Size(237, 186);
            this.tStaffing.TabIndex = 78;
            this.tStaffing.AfterEditorStateChanged += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.StateChanged(this.cStaffing_CheckedChanged);
            this.tStaffing.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tStaffing_AfterNodeChecked);
            // 
            // colStaffing
            // 
            this.colStaffing.Caption = "Персонал";
            this.colStaffing.CustomizationCaption = "Персонал";
            this.colStaffing.FieldName = "Name";
            this.colStaffing.Name = "colStaffing";
            this.colStaffing.OptionsColumn.AllowEdit = false;
            this.colStaffing.OptionsColumn.ReadOnly = true;
            this.colStaffing.Visible = true;
            this.colStaffing.VisibleIndex = 0;
            // 
            // colStaffParentId
            // 
            this.colStaffParentId.Caption = "StaffParentId";
            this.colStaffParentId.FieldName = "wParentID";
            this.colStaffParentId.Name = "colStaffParentId";
            this.colStaffParentId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffParentId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colStaffWStaffId
            // 
            this.colStaffWStaffId.Caption = "WStaffId";
            this.colStaffWStaffId.FieldName = "wStaffID";
            this.colStaffWStaffId.Name = "colStaffWStaffId";
            this.colStaffWStaffId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffWStaffId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colStaffStaffId
            // 
            this.colStaffStaffId.Caption = "StaffID";
            this.colStaffStaffId.FieldName = "StaffID";
            this.colStaffStaffId.Name = "colStaffStaffId";
            this.colStaffStaffId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffStaffId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cluDistr
            // 
            this.cluDistr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cluDistr.Delimiter = ", ";
            this.cluDistr.EditValue = "";
            this.cluDistr.Location = new System.Drawing.Point(237, 65);
            this.cluDistr.Name = "cluDistr";
            this.cluDistr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cluDistr.Properties.DisplayMember = "FieldName";
            this.cluDistr.Properties.NullText = "";
            this.cluDistr.Properties.ValueMember = "FIeldCode";
            this.cluDistr.Properties.View = this.gridLookUpWithMultipleChecking1View;
            this.cluDistr.Size = new System.Drawing.Size(240, 20);
            this.cluDistr.TabIndex = 102;
            this.cluDistr.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbDistr_Closed);
            // 
            // gridLookUpWithMultipleChecking1View
            // 
            this.gridLookUpWithMultipleChecking1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFieldID,
            this.colDistrFieldParentID,
            this.colFieldName,
            this.colLevel,
            this.colFIeldCode});
            this.gridLookUpWithMultipleChecking1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpWithMultipleChecking1View.Name = "gridLookUpWithMultipleChecking1View";
            this.gridLookUpWithMultipleChecking1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpWithMultipleChecking1View.OptionsView.ShowGroupPanel = false;
            // 
            // colFieldID
            // 
            this.colFieldID.Caption = "FieldID";
            this.colFieldID.FieldName = "FieldID";
            this.colFieldID.Name = "colFieldID";
            this.colFieldID.OptionsColumn.AllowShowHide = false;
            this.colFieldID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDistrFieldParentID
            // 
            this.colDistrFieldParentID.Caption = "FieldParentID";
            this.colDistrFieldParentID.FieldName = "FieldParentID";
            this.colDistrFieldParentID.Name = "colDistrFieldParentID";
            this.colDistrFieldParentID.OptionsColumn.AllowShowHide = false;
            this.colDistrFieldParentID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colFieldName
            // 
            this.colFieldName.Caption = "Название дистрибьютора";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.AllowShowHide = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 1;
            // 
            // colLevel
            // 
            this.colLevel.Caption = "Level";
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            this.colLevel.OptionsColumn.AllowShowHide = false;
            this.colLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colFIeldCode
            // 
            this.colFIeldCode.Caption = "Код";
            this.colFIeldCode.FieldName = "FIeldCode";
            this.colFIeldCode.MaxWidth = 100;
            this.colFIeldCode.Name = "colFIeldCode";
            this.colFIeldCode.OptionsColumn.AllowEdit = false;
            this.colFIeldCode.OptionsColumn.AllowShowHide = false;
            this.colFIeldCode.OptionsColumn.ReadOnly = true;
            this.colFIeldCode.Visible = true;
            this.colFIeldCode.VisibleIndex = 0;
            // 
            // lServiceCenter
            // 
            this.lServiceCenter.Location = new System.Drawing.Point(237, 124);
            this.lServiceCenter.Name = "lServiceCenter";
            this.lServiceCenter.Size = new System.Drawing.Size(96, 13);
            this.lServiceCenter.TabIndex = 102;
            this.lServiceCenter.Text = "Сервисный центр :";
            // 
            // cbPos
            // 
            this.cbPos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPos.Delimiter = ",";
            this.cbPos.Location = new System.Drawing.Point(237, 102);
            this.cbPos.Name = "cbPos";
            this.cbPos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPos.Properties.DisplayMember = "FieldName";
            this.cbPos.Properties.DropDownRows = 15;
            this.cbPos.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbPos.Properties.ShowButtons = false;
            this.cbPos.Properties.ValueMember = "FieldCode";
            this.cbPos.Size = new System.Drawing.Size(240, 20);
            this.cbPos.TabIndex = 99;
            this.cbPos.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbPos_Closed);
            // 
            // tWarehouseLocation
            // 
            this.tWarehouseLocation.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn3});
            this.tWarehouseLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tWarehouseLocation.KeyFieldName = "FieldID";
            this.tWarehouseLocation.Location = new System.Drawing.Point(2, 22);
            this.tWarehouseLocation.Name = "tWarehouseLocation";
            this.tWarehouseLocation.ParentFieldName = "FieldParentID";
            this.tWarehouseLocation.Size = new System.Drawing.Size(243, 133);
            this.tWarehouseLocation.TabIndex = 84;
            this.tWarehouseLocation.AfterEditorStateChanged += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.StateChanged(this.cWareHouses_CheckedChanged);
            this.tWarehouseLocation.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tWarehouseLocation_AfterCheckNode);
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Локация склада";
            this.treeListColumn3.CustomizationCaption = "Локация склада";
            this.treeListColumn3.FieldName = "FieldName";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.OptionsColumn.AllowEdit = false;
            this.treeListColumn3.OptionsColumn.ReadOnly = true;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 0;
            // 
            // tPosLocation
            // 
            this.tPosLocation.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colPosLocGeographyName,
            this.colPosLocGeographyID,
            this.colPosLocGeographyOldId});
            this.tPosLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tPosLocation.KeyFieldName = "GeographyID";
            this.tPosLocation.Location = new System.Drawing.Point(2, 22);
            this.tPosLocation.Name = "tPosLocation";
            this.tPosLocation.Size = new System.Drawing.Size(224, 173);
            this.tPosLocation.TabIndex = 79;
            this.tPosLocation.AfterEditorStateChanged += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.StateChanged(this.cPos_CheckedChanged);
            this.tPosLocation.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tPosLocation_AfterCheckNode);
            // 
            // colPosLocGeographyName
            // 
            this.colPosLocGeographyName.Caption = "Название";
            this.colPosLocGeographyName.FieldName = "GeographyName";
            this.colPosLocGeographyName.Name = "colPosLocGeographyName";
            this.colPosLocGeographyName.OptionsColumn.AllowEdit = false;
            this.colPosLocGeographyName.OptionsColumn.ReadOnly = true;
            this.colPosLocGeographyName.Visible = true;
            this.colPosLocGeographyName.VisibleIndex = 0;
            // 
            // colPosLocGeographyID
            // 
            this.colPosLocGeographyID.Caption = "GeographyID";
            this.colPosLocGeographyID.FieldName = "GeographyID";
            this.colPosLocGeographyID.Name = "colPosLocGeographyID";
            this.colPosLocGeographyID.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPosLocGeographyID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colPosLocGeographyOldId
            // 
            this.colPosLocGeographyOldId.Caption = "GeographyOldId";
            this.colPosLocGeographyOldId.FieldName = "GeographyOldId";
            this.colPosLocGeographyOldId.Name = "colPosLocGeographyOldId";
            this.colPosLocGeographyOldId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colPosLocGeographyOldId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cluWarehouses
            // 
            this.cluWarehouses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cluWarehouses.Delimiter = ", ";
            this.cluWarehouses.EditValue = "";
            this.cluWarehouses.Location = new System.Drawing.Point(483, 181);
            this.cluWarehouses.Name = "cluWarehouses";
            this.cluWarehouses.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cluWarehouses.Properties.DisplayMember = "FieldName";
            this.cluWarehouses.Properties.NullText = "";
            this.cluWarehouses.Properties.ValueMember = "FieldID";
            this.cluWarehouses.Properties.View = this.userControl21View;
            this.cluWarehouses.Size = new System.Drawing.Size(247, 20);
            this.cluWarehouses.TabIndex = 82;
            this.cluWarehouses.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cluWarehouses_Closed);
            // 
            // userControl21View
            // 
            this.userControl21View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExternalCode,
            this.colWarehouse,
            this.colWareId,
            this.colWareLevel,
            this.colWareParentId,
            this.colStaffM3});
            this.userControl21View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.userControl21View.Name = "userControl21View";
            this.userControl21View.OptionsFilter.AllowFilterEditor = false;
            this.userControl21View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.userControl21View.OptionsView.ShowGroupPanel = false;
            // 
            // colExternalCode
            // 
            this.colExternalCode.Caption = "Код";
            this.colExternalCode.CustomizationCaption = "Код";
            this.colExternalCode.FieldName = "FIeldCode";
            this.colExternalCode.MaxWidth = 100;
            this.colExternalCode.Name = "colExternalCode";
            this.colExternalCode.OptionsColumn.AllowEdit = false;
            this.colExternalCode.OptionsColumn.AllowShowHide = false;
            this.colExternalCode.OptionsColumn.ReadOnly = true;
            this.colExternalCode.Visible = true;
            this.colExternalCode.VisibleIndex = 0;
            // 
            // colWarehouse
            // 
            this.colWarehouse.Caption = "Название склада";
            this.colWarehouse.CustomizationCaption = "Название склада";
            this.colWarehouse.FieldName = "FieldName";
            this.colWarehouse.Name = "colWarehouse";
            this.colWarehouse.OptionsColumn.AllowEdit = false;
            this.colWarehouse.OptionsColumn.AllowShowHide = false;
            this.colWarehouse.OptionsColumn.ReadOnly = true;
            this.colWarehouse.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colWarehouse.Visible = true;
            this.colWarehouse.VisibleIndex = 1;
            // 
            // colWareId
            // 
            this.colWareId.Caption = "Id";
            this.colWareId.FieldName = "FieldID";
            this.colWareId.Name = "colWareId";
            this.colWareId.OptionsColumn.AllowShowHide = false;
            this.colWareId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWareLevel
            // 
            this.colWareLevel.Caption = "Level";
            this.colWareLevel.FieldName = "Level";
            this.colWareLevel.Name = "colWareLevel";
            this.colWareLevel.OptionsColumn.AllowShowHide = false;
            this.colWareLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWareParentId
            // 
            this.colWareParentId.Caption = "ParentId";
            this.colWareParentId.FieldName = "FieldParentID";
            this.colWareParentId.Name = "colWareParentId";
            this.colWareParentId.OptionsColumn.AllowShowHide = false;
            this.colWareParentId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colStaffM3
            // 
            this.colStaffM3.Caption = "DSM_ID";
            this.colStaffM3.FieldName = "DSM_ID";
            this.colStaffM3.Name = "colStaffM3";
            this.colStaffM3.OptionsColumn.AllowShowHide = false;
            this.colStaffM3.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // lApplicationStatuses
            // 
            this.lApplicationStatuses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lApplicationStatuses.Location = new System.Drawing.Point(3, 9);
            this.lApplicationStatuses.Name = "lApplicationStatuses";
            this.lApplicationStatuses.Size = new System.Drawing.Size(78, 13);
            this.lApplicationStatuses.TabIndex = 103;
            this.lApplicationStatuses.Text = "Статус заявки:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(237, 85);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.labelControl2.Size = new System.Drawing.Size(76, 23);
            this.labelControl2.TabIndex = 99;
            this.labelControl2.Text = "Дата выпуска:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(237, 143);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(115, 13);
            this.labelControl1.TabIndex = 98;
            this.labelControl1.Text = "Дата восстановления:";
            // 
            // tModels
            // 
            this.tModels.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn5});
            this.tModels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tModels.KeyFieldName = "FieldID";
            this.tModels.Location = new System.Drawing.Point(2, 22);
            this.tModels.Name = "tModels";
            this.tModels.RootValue = null;
            this.tModels.Size = new System.Drawing.Size(224, 156);
            this.tModels.TabIndex = 97;
            // 
            // treeListColumn5
            // 
            this.treeListColumn5.Caption = "Модель";
            this.treeListColumn5.CustomizationCaption = "Модель";
            this.treeListColumn5.FieldName = "FieldName";
            this.treeListColumn5.Name = "treeListColumn5";
            this.treeListColumn5.OptionsColumn.AllowEdit = false;
            this.treeListColumn5.OptionsColumn.ReadOnly = true;
            this.treeListColumn5.Visible = true;
            this.treeListColumn5.VisibleIndex = 0;
            // 
            // cbBrand
            // 
            this.cbBrand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBrand.Delimiter = ",";
            this.cbBrand.Location = new System.Drawing.Point(237, 63);
            this.cbBrand.Name = "cbBrand";
            this.cbBrand.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbBrand.Properties.DisplayMember = "POSBrand_Name";
            this.cbBrand.Properties.DropDownRows = 15;
            this.cbBrand.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbBrand.Properties.ShowButtons = false;
            this.cbBrand.Properties.ValueMember = "POSBrand_ID";
            this.cbBrand.Size = new System.Drawing.Size(235, 20);
            this.cbBrand.TabIndex = 97;
            // 
            // cbManufacturer
            // 
            this.cbManufacturer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbManufacturer.Delimiter = ",";
            this.cbManufacturer.Location = new System.Drawing.Point(237, 28);
            this.cbManufacturer.Name = "cbManufacturer";
            this.cbManufacturer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbManufacturer.Properties.DisplayMember = "ManufacturerName";
            this.cbManufacturer.Properties.DropDownRows = 15;
            this.cbManufacturer.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbManufacturer.Properties.ShowButtons = false;
            this.cbManufacturer.Properties.ValueMember = "ManufacturerId";
            this.cbManufacturer.Size = new System.Drawing.Size(235, 20);
            this.cbManufacturer.TabIndex = 96;
            // 
            // tRepairDates
            // 
            this.tRepairDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tRepairDates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tRepairDates.BackColor = System.Drawing.Color.Transparent;
            this.tRepairDates.From = new System.DateTime(((long)(0)));
            this.tRepairDates.Location = new System.Drawing.Point(237, 157);
            this.tRepairDates.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.tRepairDates.Name = "tRepairDates";
            this.tRepairDates.Size = new System.Drawing.Size(238, 24);
            this.tRepairDates.TabIndex = 83;
            this.tRepairDates.To = new System.DateTime(((long)(0)));
            // 
            // tProductionDates
            // 
            this.tProductionDates.AllowNulls = DevExpress.Utils.DefaultBoolean.True;
            this.tProductionDates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tProductionDates.BackColor = System.Drawing.Color.Transparent;
            this.tProductionDates.From = new System.DateTime(((long)(0)));
            this.tProductionDates.Location = new System.Drawing.Point(237, 113);
            this.tProductionDates.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.tProductionDates.Name = "tProductionDates";
            this.tProductionDates.Size = new System.Drawing.Size(238, 24);
            this.tProductionDates.TabIndex = 81;
            this.tProductionDates.To = new System.DateTime(((long)(0)));
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // component1View
            // 
            this.component1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.component1View.Name = "component1View";
            this.component1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.component1View.OptionsView.ShowGroupPanel = false;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Дислокация";
            this.treeListColumn2.CustomizationCaption = "Дислокация";
            this.treeListColumn2.FieldName = "GeographyName";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // tReportDates
            // 
            this.tReportDates.AllowNulls = DevExpress.Utils.DefaultBoolean.False;
            this.tReportDates.BackColor = System.Drawing.Color.Transparent;
            this.tReportDates.Description = "По заявкам за период:";
            this.tReportDates.From = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.tReportDates.Location = new System.Drawing.Point(-80, 1);
            this.tReportDates.Name = "tReportDates";
            this.tReportDates.Size = new System.Drawing.Size(558, 26);
            this.tReportDates.TabIndex = 77;
            this.tReportDates.To = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            // 
            // cRepeatedApp
            // 
            this.cRepeatedApp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cRepeatedApp.Location = new System.Drawing.Point(3, 122);
            this.cRepeatedApp.Name = "cRepeatedApp";
            this.cRepeatedApp.Properties.Caption = "Повторная заявка";
            this.cRepeatedApp.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cRepeatedApp.Size = new System.Drawing.Size(242, 19);
            this.cRepeatedApp.TabIndex = 81;
            // 
            // cUninstalation
            // 
            this.cUninstalation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cUninstalation.Location = new System.Drawing.Point(3, 142);
            this.cUninstalation.Name = "cUninstalation";
            this.cUninstalation.Properties.Caption = "Демонтаж для ремонта";
            this.cUninstalation.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cUninstalation.Size = new System.Drawing.Size(242, 19);
            this.cUninstalation.TabIndex = 82;
            // 
            // cMiscall
            // 
            this.cMiscall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cMiscall.Location = new System.Drawing.Point(3, 162);
            this.cMiscall.Name = "cMiscall";
            this.cMiscall.Properties.Caption = "Ложный вызов";
            this.cMiscall.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cMiscall.Size = new System.Drawing.Size(242, 19);
            this.cMiscall.TabIndex = 83;
            // 
            // cDetalizationByElements
            // 
            this.cDetalizationByElements.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cDetalizationByElements.Location = new System.Drawing.Point(283, 504);
            this.cDetalizationByElements.Name = "cDetalizationByElements";
            this.cDetalizationByElements.Properties.Caption = "Детализация информации по запчастям";
            this.cDetalizationByElements.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDetalizationByElements.Size = new System.Drawing.Size(257, 19);
            this.cDetalizationByElements.TabIndex = 101;
            // 
            // cDetalizationByBreaksType
            // 
            this.cDetalizationByBreaksType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cDetalizationByBreaksType.Location = new System.Drawing.Point(13, 504);
            this.cDetalizationByBreaksType.Name = "cDetalizationByBreaksType";
            this.cDetalizationByBreaksType.Properties.Caption = "Детализация информации по типам поломок";
            this.cDetalizationByBreaksType.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDetalizationByBreaksType.Size = new System.Drawing.Size(257, 19);
            this.cDetalizationByBreaksType.TabIndex = 99;
            this.cDetalizationByBreaksType.CheckedChanged += new System.EventHandler(this.cDetalizationByBreaksType_CheckedChanged);
            // 
            // cDetalizationByWorks
            // 
            this.cDetalizationByWorks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cDetalizationByWorks.Location = new System.Drawing.Point(283, 485);
            this.cDetalizationByWorks.Name = "cDetalizationByWorks";
            this.cDetalizationByWorks.Properties.Caption = "Детализация информации по работам";
            this.cDetalizationByWorks.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDetalizationByWorks.Size = new System.Drawing.Size(257, 19);
            this.cDetalizationByWorks.TabIndex = 100;
            // 
            // cDetalizationByBreaks
            // 
            this.cDetalizationByBreaks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cDetalizationByBreaks.Location = new System.Drawing.Point(13, 485);
            this.cDetalizationByBreaks.Name = "cDetalizationByBreaks";
            this.cDetalizationByBreaks.Properties.Caption = "Детализация информации по поломкам";
            this.cDetalizationByBreaks.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDetalizationByBreaks.Size = new System.Drawing.Size(257, 19);
            this.cDetalizationByBreaks.TabIndex = 98;
            this.cDetalizationByBreaks.CheckedChanged += new System.EventHandler(this.cDetalizationByBreaks_CheckedChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.tTerritory);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(241, 227);
            this.groupControl1.TabIndex = 102;
            this.groupControl1.Text = "Территория";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.cStaffing);
            this.groupControl2.Controls.Add(this.tStaffing);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(3, 236);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(241, 210);
            this.groupControl2.TabIndex = 103;
            this.groupControl2.Text = "Персонал";
            // 
            // cStaffing
            // 
            this.cStaffing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cStaffing.EditValue = true;
            this.cStaffing.Location = new System.Drawing.Point(194, 2);
            this.cStaffing.Name = "cStaffing";
            this.cStaffing.Properties.AllowGrayed = true;
            this.cStaffing.Properties.Caption = "Все";
            this.cStaffing.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cStaffing.Size = new System.Drawing.Size(43, 19);
            this.cStaffing.TabIndex = 83;
            this.cStaffing.TabStop = false;
            // 
            // groupControl5
            // 
            this.groupControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.groupControl5, 2);
            this.groupControl5.Controls.Add(this.tableLayoutPanel4);
            this.groupControl5.Location = new System.Drawing.Point(250, 236);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(479, 210);
            this.groupControl5.TabIndex = 105;
            this.groupControl5.Text = "Оборудование";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 241F));
            this.tableLayoutPanel4.Controls.Add(this.tRepairDates, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.cbManufacturer, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.labelControl1, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.lBrand, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.tProductionDates, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.labelControl2, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.cbBrand, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.groupControl8, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.lManufacturer, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(2, 22);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 8;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(475, 186);
            this.tableLayoutPanel4.TabIndex = 110;
            // 
            // groupControl8
            // 
            this.groupControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl8.Controls.Add(this.cModels);
            this.groupControl8.Controls.Add(this.tModels);
            this.groupControl8.Location = new System.Drawing.Point(3, 3);
            this.groupControl8.Name = "groupControl8";
            this.tableLayoutPanel4.SetRowSpan(this.groupControl8, 8);
            this.groupControl8.Size = new System.Drawing.Size(228, 180);
            this.groupControl8.TabIndex = 109;
            this.groupControl8.Text = "Модель оборудования";
            // 
            // cModels
            // 
            this.cModels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cModels.EditValue = true;
            this.cModels.Location = new System.Drawing.Point(183, 1);
            this.cModels.Name = "cModels";
            this.cModels.Properties.AllowGrayed = true;
            this.cModels.Properties.Caption = "Все";
            this.cModels.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cModels.Size = new System.Drawing.Size(43, 19);
            this.cModels.TabIndex = 80;
            this.cModels.TabStop = false;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.tableLayoutPanel1);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(735, 236);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(252, 210);
            this.groupControl4.TabIndex = 105;
            this.groupControl4.Text = "Заявки";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.cAppClosed, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lAppStatusesList, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cAppOpen, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cApplicationStatuses, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cUninstalation, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lApplicationStatuses, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cMiscall, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.cRepeatedApp, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 22);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(248, 186);
            this.tableLayoutPanel1.TabIndex = 105;
            // 
            // cAppClosed
            // 
            this.cAppClosed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cAppClosed.EditValue = true;
            this.cAppClosed.Location = new System.Drawing.Point(3, 48);
            this.cAppClosed.Name = "cAppClosed";
            this.cAppClosed.Properties.Caption = "Закрыто";
            this.cAppClosed.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cAppClosed.Size = new System.Drawing.Size(242, 19);
            this.cAppClosed.TabIndex = 108;
            this.cAppClosed.CheckedChanged += new System.EventHandler(this.cAppOpen_CheckedChanged);
            // 
            // lAppStatusesList
            // 
            this.lAppStatusesList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lAppStatusesList.Location = new System.Drawing.Point(3, 77);
            this.lAppStatusesList.Name = "lAppStatusesList";
            this.lAppStatusesList.Size = new System.Drawing.Size(143, 13);
            this.lAppStatusesList.TabIndex = 107;
            this.lAppStatusesList.Text = "Статус выполнения заявки:";
            // 
            // cAppOpen
            // 
            this.cAppOpen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cAppOpen.EditValue = true;
            this.cAppOpen.Location = new System.Drawing.Point(3, 28);
            this.cAppOpen.Name = "cAppOpen";
            this.cAppOpen.Properties.Caption = "Открыто";
            this.cAppOpen.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cAppOpen.Size = new System.Drawing.Size(242, 19);
            this.cAppOpen.TabIndex = 107;
            this.cAppOpen.CheckedChanged += new System.EventHandler(this.cAppOpen_CheckedChanged);
            // 
            // cApplicationStatuses
            // 
            this.cApplicationStatuses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cApplicationStatuses.Delimiter = ",";
            this.cApplicationStatuses.Location = new System.Drawing.Point(3, 96);
            this.cApplicationStatuses.Name = "cApplicationStatuses";
            this.cApplicationStatuses.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cApplicationStatuses.Properties.DisplayMember = "StatusValue";
            this.cApplicationStatuses.Properties.DropDownRows = 15;
            this.cApplicationStatuses.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cApplicationStatuses.Properties.ShowButtons = false;
            this.cApplicationStatuses.Properties.ValueMember = "StatusId";
            this.cApplicationStatuses.Size = new System.Drawing.Size(242, 20);
            this.cApplicationStatuses.TabIndex = 107;
            // 
            // groupControl3
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupControl3, 3);
            this.groupControl3.Controls.Add(this.tableLayoutPanel3);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(250, 3);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(737, 227);
            this.groupControl3.TabIndex = 104;
            this.groupControl3.Text = "По территории";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.17102F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 246F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.82898F));
            this.tableLayoutPanel3.Controls.Add(this.groupControl6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lDistrType, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cbDistrType, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lDistr, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.cluDistr, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.labelControl15, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.groupControl7, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.cluWarehouses, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.lWarehouses, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.cbPos, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.lServiceCenter, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.cbServiceCenter, 1, 7);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(2, 22);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 10;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(733, 203);
            this.tableLayoutPanel3.TabIndex = 105;
            // 
            // groupControl6
            // 
            this.groupControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl6.Controls.Add(this.cPos);
            this.groupControl6.Controls.Add(this.tPosLocation);
            this.groupControl6.Location = new System.Drawing.Point(3, 3);
            this.groupControl6.Name = "groupControl6";
            this.tableLayoutPanel3.SetRowSpan(this.groupControl6, 10);
            this.groupControl6.Size = new System.Drawing.Size(228, 197);
            this.groupControl6.TabIndex = 107;
            this.groupControl6.Text = "Дислокация ТС";
            // 
            // cPos
            // 
            this.cPos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cPos.EditValue = true;
            this.cPos.Location = new System.Drawing.Point(182, 2);
            this.cPos.Name = "cPos";
            this.cPos.Properties.AllowGrayed = true;
            this.cPos.Properties.Caption = "Все";
            this.cPos.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cPos.Size = new System.Drawing.Size(43, 19);
            this.cPos.TabIndex = 82;
            this.cPos.TabStop = false;
            // 
            // groupControl7
            // 
            this.groupControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl7.Controls.Add(this.cWareHouses);
            this.groupControl7.Controls.Add(this.tWarehouseLocation);
            this.groupControl7.Location = new System.Drawing.Point(483, 3);
            this.groupControl7.Name = "groupControl7";
            this.tableLayoutPanel3.SetRowSpan(this.groupControl7, 8);
            this.groupControl7.Size = new System.Drawing.Size(247, 157);
            this.groupControl7.TabIndex = 108;
            this.groupControl7.Text = "Дислокация склада";
            // 
            // cWareHouses
            // 
            this.cWareHouses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cWareHouses.EditValue = true;
            this.cWareHouses.Location = new System.Drawing.Point(202, 2);
            this.cWareHouses.Name = "cWareHouses";
            this.cWareHouses.Properties.AllowGrayed = true;
            this.cWareHouses.Properties.Caption = "Все";
            this.cWareHouses.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cWareHouses.Size = new System.Drawing.Size(43, 19);
            this.cWareHouses.TabIndex = 81;
            this.cWareHouses.TabStop = false;
            // 
            // cbServiceCenter
            // 
            this.cbServiceCenter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbServiceCenter.Delimiter = ",";
            this.cbServiceCenter.Location = new System.Drawing.Point(237, 139);
            this.cbServiceCenter.Name = "cbServiceCenter";
            this.cbServiceCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbServiceCenter.Properties.DisplayMember = "GeographyName";
            this.cbServiceCenter.Properties.DropDownRows = 15;
            this.cbServiceCenter.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbServiceCenter.Properties.ShowButtons = false;
            this.cbServiceCenter.Properties.ValueMember = "GeographyOldId";
            this.cbServiceCenter.Size = new System.Drawing.Size(240, 20);
            this.cbServiceCenter.TabIndex = 100;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.37811F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.67662F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.87065F));
            this.tableLayoutPanel2.Controls.Add(this.groupControl1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupControl3, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupControl4, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupControl2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupControl5, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 28);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(990, 449);
            this.tableLayoutPanel2.TabIndex = 106;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 529);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.cDetalizationByElements);
            this.Controls.Add(this.cDetalizationByBreaksType);
            this.Controls.Add(this.cDetalizationByWorks);
            this.Controls.Add(this.cDetalizationByBreaks);
            this.Controls.Add(this.tReportDates);
            this.Controls.Add(this.lblUpd2);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.generateReportButton);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(920, 550);
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры к отчету \"Ремонты оборудования\"";
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluDistr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpWithMultipleChecking1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tWarehouseLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPosLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluWarehouses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tModels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBrand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbManufacturer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cRepeatedApp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUninstalation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMiscall.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByElements.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByBreaksType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByWorks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDetalizationByBreaks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cStaffing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cModels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cAppClosed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cAppOpen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cApplicationStatuses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cWareHouses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbServiceCenter.Properties)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.SimpleButton generateReportButton;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private ComboboxExtended cbDistrType;
        private DevExpress.XtraEditors.LabelControl lDistrType;
        private DevExpress.XtraEditors.LabelControl lDistr;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lWarehouses;
        private DevExpress.XtraEditors.LabelControl lBrand;
        private DevExpress.XtraEditors.LabelControl lManufacturer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.Grid.GridView component1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking cluWarehouses;
        private DevExpress.XtraGrid.Views.Grid.GridView userControl21View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouse;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWareId;
        private DevExpress.XtraGrid.Columns.GridColumn colWareLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWareParentId;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tTerritory;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTerritory;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tStaffing;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffing;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tPosLocation;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tWarehouseLocation;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tReportDates;
        private SoftServe.Reports.EquipmentReparation.UserControls.TimeInterval tRepairDates;
        private SoftServe.Reports.EquipmentReparation.UserControls.TimeInterval tProductionDates;
        private ComboboxExtended cbManufacturer;
        private ComboboxExtended cbBrand;
        private ComboboxExtended cbPos;
        private Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental tModels;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn5;
        private DevExpress.XtraEditors.LabelControl lApplicationStatuses;
        private DevExpress.XtraEditors.LabelControl lServiceCenter;
        private DevExpress.XtraEditors.CheckEdit cRepeatedApp;
        private DevExpress.XtraEditors.CheckEdit cUninstalation;
        private DevExpress.XtraEditors.CheckEdit cMiscall;
        private DevExpress.XtraEditors.CheckEdit cDetalizationByElements;
        private DevExpress.XtraEditors.CheckEdit cDetalizationByBreaksType;
        private DevExpress.XtraEditors.CheckEdit cDetalizationByWorks;
        private DevExpress.XtraEditors.CheckEdit cDetalizationByBreaks;
        private GridLookUpWithMultipleChecking cluDistr;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpWithMultipleChecking1View;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldID;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrFieldParentID;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colFIeldCode;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPosLocGeographyName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPosLocGeographyID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPosLocGeographyOldId;
        private DevExpress.XtraEditors.CheckEdit cWareHouses;
        private DevExpress.XtraEditors.CheckEdit cPos;
        private DevExpress.XtraEditors.CheckEdit cModels;
        private DevExpress.XtraEditors.CheckEdit cStaffing;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffWStaffId;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private ComboboxExtended cbServiceCenter;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private ComboboxExtended cApplicationStatuses;
        private DevExpress.XtraEditors.LabelControl lAppStatusesList;
        private DevExpress.XtraEditors.CheckEdit cAppOpen;
        private DevExpress.XtraEditors.CheckEdit cAppClosed;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffStaffId;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffM3;
	}
}