﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.CommonControls;
using SoftServe.Reports.EquipmentReparation.Utility;
using PeriodChangedArgs = SoftServe.Reports.EquipmentReparation.UserControls.PeriodChangedArgs;

namespace SoftServe.Reports.EquipmentReparation
{
    public partial class SettingsForm : XtraForm
    {
        #region Constants

        private const string STRING_DELIMITER = ",";

        private const string DumbFilter = "-17";

        private const string FilterByLevel = "Level = {0}";
        private const string FilterByLevelLess = "Level < {0}";
        private const string AndFilterByColumn = " and {0} in ({1})";
        private const string FilterByColumn = "{0} in ({1})";

        private const string Level = "Level";

        private const string PosParentIdField = "FieldParentID";

        private const string colSCLocationGeographyID = "GeographyID";
        private const string colSCLocationGeographyOldID = "GeographyOldId";
        private const string colSCParentID = "ParentID";
        private const string DistrWorkSchema = "SchemaOfWork";

        private const string IsApplicationOpen = "isOpen";

        private int _staffM3Level = 4;

        #endregion

        #region Private Fields
        private int WarehousesLevel;

        private int DistributorsLevel;
        private int PosLevel;

        private int PosLocationLevel;

        private int SCLevel;
        private int SCParentLevel;

        private DataTable distributors;
        private DataTable posLocations;
        private DataTable warehouses;
        private DataTable scenters;

        private bool ShouldClearWarehousesSelection = false;
        private bool ShouldClearPosesSelection = false;

        #endregion

        /// <summary>
        /// Creates the new instance od <c>SettingsForm</c>/>
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();

            InitDataSources();
            SetDateIntervals();
            SetInitialSettings();

            tReportDates.EndDateChanged += tReportDates_EndDateChanged;
        }

        /// <summary>
        /// Performs gathering of settings info
        /// </summary>
        /// <returns>Current setting info</returns>
        internal ReportInfo GetSettingsData()
        {
            ReportInfo settings = new ReportInfo();

            settings.ReportDates = tReportDates.Interval;

            settings.RegionIds = tTerritory.GetCheckedNodesIds();
            settings.StaffingsIds = tStaffing.GetCheckedColumnValuesAll(colStaffStaffId.FieldName);

            settings.ServiceCentersIds = cbServiceCenter.GetComboBoxCheckedValues();

            settings.ApplicationStatusesIds = cApplicationStatuses.GetComboBoxCheckedValues();

            settings.PosIds = cbPos.GetComboBoxCheckedValues();

            settings.WarehousesIds = cluWarehouses.SelectedValues("wFieldID");

            settings.ModelsIds = tModels.GetCheckedColumnValues("NaturalID", "Level", 1);
            settings.ManufacturersIds = cbManufacturer.GetComboBoxCheckedValues();
            settings.BrandsIds = cbBrand.GetComboBoxCheckedValues();

            settings.ProductionDates = tProductionDates.Interval;
            settings.RepairDates = tRepairDates.Interval;

            settings.ShowRepeatedApp = cRepeatedApp.Checked;
            settings.ShowUninstalationApp = cUninstalation.Checked;
            settings.ShowMiscallApp = cMiscall.Checked;

            settings.DetalizationByBreaks = cDetalizationByBreaks.Checked;
            settings.DetalizationByBreaksType = cDetalizationByBreaksType.Checked;
            settings.DetalizationByWorks = cDetalizationByWorks.Checked;
            settings.DetalizationByElements = cDetalizationByElements.Checked;

            return settings;
        }

        /// <summary>
        /// Assigns datasources to contols
        /// </summary>
        private void InitDataSources()
        {
            tStaffing.LoadDataSource(DataProvider.GetStaffingTree(DateTime.Now));

            distributors = DataProvider.GetDistributorsTree(out DistributorsLevel);
            PosLevel = DistributorsLevel + 1;
            posLocations = DataProvider.GetPosesTree(out PosLocationLevel);
            warehouses = DataProvider.GetWarehouseTree(out WarehousesLevel);
            scenters = DataProvider.GetServiceCentersTree(out SCLevel);
            SCParentLevel = SCLevel - 1;

            tTerritory.LoadDataSource(DataProvider.GetTerritoryTree(2, 3));

            cbDistrType.LoadDataSource(DataProvider.GetDistrTypesList());

            DataView lDataViewWarehouses = warehouses.DefaultView;
            lDataViewWarehouses.RowFilter = string.Format(FilterByLevelLess, WarehousesLevel);
            tWarehouseLocation.LoadDataSource(lDataViewWarehouses.ToTable());

            cluWarehouses.LoadDataSource(warehouses.Copy());

            cluDistr.LoadDataSource(distributors.Copy());
            cbPos.LoadDataSource(distributors.Copy());

            tPosLocation.LoadDataSource(posLocations.Copy());

            cbServiceCenter.LoadDataSource(scenters.Copy());

            cApplicationStatuses.LoadDataSource(DataProvider.GetApplicationStatusesList());

            cbBrand.LoadDataSource(DataProvider.GetBrandList());

            tModels.LoadDataSource(DataProvider.GetModelsTree());
            HideModels();

            cbManufacturer.LoadDataSource(DataProvider.GetManufacturersList());
        }

        /// <summary>
        /// Set init settings
        /// </summary>
        private void SetInitialSettings()
        {
            tTerritory.CheckNodes(CheckState.Checked);
            FilterTreesByRegion();

            tStaffing.CheckNodes(CheckState.Checked);
            tStaffing.AssignCheckEdit(cStaffing);

            cbDistrType.SelectAll();

            tPosLocation.CheckNodes(CheckState.Checked);
            tPosLocation.AssignCheckEdit(cPos);

            tWarehouseLocation.CheckNodes(CheckState.Checked);
            tWarehouseLocation.AssignCheckEdit(cWareHouses);

            FilterDistrGeneral();
            cluDistr.SelectAll();

            FilterPosListGeneral();
            cbPos.SelectAll();

            FilterSCListGeneral();
            cbServiceCenter.SelectAll();

            FilterWarehousesListGeneral();
            cluWarehouses.SelectAll();

            tModels.CheckNodes(CheckState.Checked);
            tModels.AssignCheckEdit(cModels);

            cbManufacturer.SelectAll();
            cbBrand.SelectAll();
            cApplicationStatuses.SelectAll();
        }

        /// <summary>
        /// Sets default date intervals
        /// </summary>
        private void SetDateIntervals()
        {
            DateTime lCurrentMonthBeginning = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            tReportDates.To = DateTime.Now;
            tReportDates.From = lCurrentMonthBeginning;
        }

        /// <summary>
        /// Handles report end date changes
        /// </summary>
        private void ReloadDateDependentData()
        {
            tStaffing.LoadDataSource(DataProvider.GetStaffingTree(tReportDates.To));

            FilterStaffingTree();
        }

        /// <summary>
        /// Validates selected data
        /// </summary>
        /// <param name="reportInfo">Settings to be checked</param>
        /// <param name="message">Warning message</param>
        /// <returns><c>true</c> if settings are valid, <c>false</c> otherwise</returns>
        private bool AreSettingsValid(ReportInfo reportInfo, out string message)
        {
            message = string.Empty;
            message += reportInfo.StaffingsIds.Count > 0 ? string.Empty : "\t- не выбрано ни одного сотрудника;\r\n";

            message += reportInfo.ServiceCentersIds.Count > 0 ? string.Empty : "\t- не выбрано ни одного сервисного центра;\r\n";
            message += reportInfo.ApplicationStatusesIds.Count > 0 ? string.Empty : "\t- не выбрано ни одного статуса заявки;\r\n";

            message += reportInfo.PosIds.Count == 0
                           ? "\t- не выбрано ни одной ТС;\r\n"
                           : string.Empty;

            message += reportInfo.ModelsIds.Count > 0 ? string.Empty : "\t- не выбрано ни одной модели оборудования;\r\n";
            message += reportInfo.ManufacturersIds.Count > 0 ? string.Empty : "\t- не выбрано ни одного производителя;\r\n";
            message += reportInfo.BrandsIds.Count > 0 ? string.Empty : "\t- не выбрано ни одного бренда;\r\n";

            message += reportInfo.ProductionDates.IsValid ? string.Empty : "\t- Дата выпуска: некорректно введен период;\r\n";
            message += reportInfo.RepairDates.IsValid ? string.Empty : "\t- Дата восстановления: некорректно введен период;\r\n";

            return message.Length == 0;
        }

        #region Filtering

        /// <summary>
        /// Performs filtering of Staffing, POS and Warehouses trees by checked regions
        /// </summary>
        private void FilterTreesByRegion()
        {
            List<string> lFilter = tTerritory.GetCheckedNodesIds();

            if (cPos.CheckState == CheckState.Indeterminate && lFilter.Count == 0)
            {
                cPos.CheckState = CheckState.Unchecked;
            }

            if (cWareHouses.CheckState == CheckState.Indeterminate && lFilter.Count == 0)
            {
                cWareHouses.CheckState = CheckState.Unchecked;
            }

            if (lFilter.Count == 0)
            {
                lFilter.Add(DumbFilter);
            }

            FilterStaffingTree();

            tPosLocation.FilterTree(lFilter);
            HidePosLevel();

            tWarehouseLocation.FilterTree(lFilter);
        }

        /// <summary>
        /// Performs staffing filtering
        /// </summary>
        private void FilterStaffingTree()
        {
            List<string> lTerritoryFilter = tTerritory.GetCheckedNodesIds();

            List<string> lStaffingFilter = new List<string>();

            foreach (TreeListNode node in tStaffing.Nodes)
            {
                if (lTerritoryFilter.Contains(node[colStaffParentId.FieldName].ToString()))
                {
                    lStaffingFilter.Add(node[colStaffWStaffId.FieldName].ToString());
                }
            }

            if (lStaffingFilter.Count == 0)
            {
                lStaffingFilter.Add(DumbFilter);
            }

            List<string> values = tStaffing.GetCheckedColumnValuesAll(tStaffing.KeyFieldName);
            tStaffing.FilterTree(lStaffingFilter);

            switch (cStaffing.CheckState)
            {
                case CheckState.Checked: tStaffing.CheckNodes(CheckState.Checked);
                    break;
                case CheckState.Unchecked: tStaffing.CheckNodes(CheckState.Unchecked);
                    break;
                case CheckState.Indeterminate:
                    if (lTerritoryFilter.Count == 0)
                    {
                        cStaffing.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        tStaffing.CheckNodes(values, CheckState.Checked);
                    }
                    break;
            }
        }

        /// <summary>
        /// Performs filtering of distributors by territory and distr types
        /// </summary>
        private void FilterDistrGeneral()
        {
            string lFilter = string.Format(FilterByLevel, DistributorsLevel);

            //by region
            string regionFilter = tTerritory.GetCheckedIdsValues();

            if (!string.IsNullOrEmpty(regionFilter)) //there are selected regions
            {
                lFilter += string.Format(AndFilterByColumn, colDistrFieldParentID.FieldName, regionFilter);
            }
            else
            {
                lFilter += string.Format(AndFilterByColumn, colDistrFieldParentID.FieldName, DumbFilter);
            }

            //by distr type
            string distrTypesFilter = cbDistrType.GetComboBoxCheckedValuesList();

            if (!string.IsNullOrEmpty(distrTypesFilter)) //there are selected types
            {
                lFilter += string.Format(AndFilterByColumn, DistrWorkSchema, distrTypesFilter);
            }

            List<string> values = cluDistr.SelectedIds();
            //apply filter
            cluDistr.ClearSelection();

            DataView lDataView = distributors.DefaultView;
            lDataView.RowFilter = lFilter;
            lDataView.Sort = cluDistr.Properties.DisplayMember;
            cluDistr.Properties.DataSource = lDataView.ToTable(true, cluDistr.Properties.ValueMember,
                                                                  cluDistr.Properties.DisplayMember, DistrWorkSchema);


            cluDistr.SetComboBoxCheckedValues(values);
        }

        /// <summary>
        /// Perfoms filtering of warehouses by territory
        /// </summary>
        private void FilterWarehousesListGeneral()
        {
            string lFilter = string.Format(FilterByLevel, WarehousesLevel);

            //by warehouse location
            string lWarehouseLocationFilter = tWarehouseLocation.GetCheckedColumnValuesAllList(colWareId.FieldName);

            if (!string.IsNullOrEmpty(lWarehouseLocationFilter))//there is warehouses location selected
            {
                lFilter += string.Format(AndFilterByColumn, colWareParentId.FieldName, lWarehouseLocationFilter);
            }
            else//there is no territory selected
            {
                lWarehouseLocationFilter = tWarehouseLocation.GetCheckedColumnValuesAllList(colWareId.FieldName, CheckState.Unchecked);

                if (!string.IsNullOrEmpty(lWarehouseLocationFilter))
                {
                    lFilter += string.Format(AndFilterByColumn, colWareParentId.FieldName, lWarehouseLocationFilter);
                }
                else// for параноїків
                {
                    lFilter += string.Format(AndFilterByColumn, colWareParentId.FieldName, DumbFilter);
                }
            }

            //by stuffing M3
            string lStuffM3Filter = string.Join(",", tStaffing.GetCheckedAndIndeterminateColumnValues(colStaffStaffId.FieldName, Level, _staffM3Level).ToArray());

            if (!string.IsNullOrEmpty(lStuffM3Filter))//there is stuff selected
            {
                lFilter += string.Format(" and ({0} is null or {1})", colStaffM3.FieldName,
                                         string.Format(FilterByColumn, colStaffM3.FieldName, lStuffM3Filter));
            }
            else
            {
                lFilter += string.Format(" and ({0} is null)", colStaffM3.FieldName);
            }

            List<string> values = cluWarehouses.SelectedIds();
            //apply filter
            cluWarehouses.FilterList(lFilter);

            if (!ShouldClearWarehousesSelection)
            {
                cluWarehouses.SetComboBoxCheckedValues(values);
            }
            else
            {
                ShouldClearWarehousesSelection = false;
            }
        }

        /// <summary>
        /// Performs filtering of Pos by distributors (so as by territory)
        /// </summary>
        private void FilterPosListGeneral()
        {
            string lFilter = string.Format(FilterByLevel, PosLevel);

            //by distr
            string lDistrCheckedFilter = cluDistr.SelectedIdsString();
            if (!string.IsNullOrEmpty(lDistrCheckedFilter)) //there are distr selected
            {
                lFilter += string.Format(AndFilterByColumn, PosParentIdField, GetDistrStringDescription(lDistrCheckedFilter));
            }
            else //there are no distr selected
            {
                lDistrCheckedFilter = cluDistr.AllIdsString();
                if (!string.IsNullOrEmpty(lDistrCheckedFilter))
                {
                    lFilter += string.Format(AndFilterByColumn, PosParentIdField, GetDistrStringDescription(lDistrCheckedFilter));
                }
                else // for параноїків
                {
                    lFilter += string.Format(AndFilterByColumn, PosParentIdField, DumbFilter);
                }
            }

            string lPosLocationFilter = tPosLocation.GetInvisibleColumnValuesChildrenList(colPosLocGeographyOldId.FieldName, Level, PosLocationLevel);
            if (!string.IsNullOrEmpty(lPosLocationFilter)) //there are Poses selected
            {
                lFilter += string.Format(AndFilterByColumn, cbPos.Properties.ValueMember, lPosLocationFilter);
            }
            else//check whether there is the territory selected
            {
                List<string> lPosTerritoryFilter = tPosLocation.GetCheckedColumnValues(tPosLocation.KeyFieldName, Level, PosLocationLevel - 1);

                if (lPosTerritoryFilter.Count > 0)
                {
                    lFilter += string.Format(AndFilterByColumn, cbPos.Properties.ValueMember, DumbFilter);
                }
            }

            List<string> values = cbPos.GetComboBoxCheckedValues();
            //apply filter
            cbPos.FilterList(lFilter);

            if (!ShouldClearPosesSelection)
            {
                cbPos.SetComboBoxCheckedValues(values);
            }
            else
            {
                ShouldClearPosesSelection = false;
            }
        }

        /// <summary>
        /// Performs filtering of SC by POS (so as by territory)
        /// </summary>
        private void FilterSCListGeneral()
        {
            string lFilter = string.Format(FilterByLevel, SCLevel);
            string lCheckedPosList = cbPos.GetComboBoxCheckedValuesList();

            if (!string.IsNullOrEmpty(lCheckedPosList))
            {
                //get checked POS Ids 
                DataView lDataViewPosSCs = scenters.DefaultView;
                lDataViewPosSCs.RowFilter = string.Format(FilterByLevel, SCParentLevel) + string.Format(AndFilterByColumn, colSCLocationGeographyOldID, lCheckedPosList);
                //get checked POS Ids 

                string lPosCheckedFilter = string.Join(STRING_DELIMITER, (from DataRow row in lDataViewPosSCs.ToTable().Rows select row[colSCLocationGeographyID].ToString()).ToArray());
                if (!string.IsNullOrEmpty(lPosCheckedFilter)) //there are POSes
                {
                    lFilter += string.Format(AndFilterByColumn, colSCParentID, lPosCheckedFilter);
                }
            }
            else //there are no pos selected
            {
                lFilter += string.Format(AndFilterByColumn, colSCParentID, DumbFilter);
            }

            List<string> values = cbServiceCenter.GetComboBoxCheckedValues();
            //apply filter
            cbServiceCenter.FilterList(lFilter);

            cbServiceCenter.Properties.Items.Add(-1, "Неопределенный");

            cbServiceCenter.SetComboBoxCheckedValues(values);
        }

        /// <summary>
        /// Hides empty models 
        /// </summary>
        private void HideModels()
        {
            List<string> lMolels = tModels.GetNodesWithoutChildren(tModels.KeyFieldName, Level, 0);

            tModels.BeginUpdate();
            foreach (string nodeId in lMolels)
            {
                TreeListNode lNode = tModels.GetNodeByKeyField(nodeId);
                tModels.SetChildrensVisibility(lNode, false);
            }
            tModels.EndUpdate();
        }

        /// <summary>
        /// Hides POS level of POS location tree
        /// </summary>
        private void HidePosLevel()
        {
            tPosLocation.HideLevel(Level, PosLocationLevel);
        }

        private string GetDistrStringDescription(string filterCode)
        {
            DataView lDataView = distributors.DefaultView;
            lDataView.RowFilter = string.Format(FilterByLevel, DistributorsLevel) + string.Format(AndFilterByColumn, cluDistr.Properties.ValueMember, filterCode);// "FIeldCode in (" + filterCode + ") and Level = 4";

            List<string> lFilterIds = new List<string>();

            foreach (DataRow row in lDataView.ToTable().Rows)
            {
                lFilterIds.Add(row["FieldID"].ToString());
            }

            return string.Join(",", lFilterIds.ToArray());
        }

        #endregion

        #region Event Handling

        /// <summary>
        /// Handles Click event of generateReportButton
        /// </summary>
        /// <param name="sender">generateReportButton</param>
        /// <param name="e">EventArgs</param>
        private void generateReportButton_Click(object sender, EventArgs e)
        {
            ReportInfo lSettings = GetSettingsData();
            string lMessage;

            if (AreSettingsValid(lSettings, out lMessage))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Не все параметры введены:\n\r\n" + lMessage, "Предупреждение", MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Handles EndDateChanged event of tReportDates
        /// </summary>
        /// <param name="sender">tReportDates</param>
        /// <param name="args">PeriodChangedArgs</param>
        private void tReportDates_EndDateChanged(object sender, Logica.Reports.BaseReportControl.CommonControls.PeriodChangedArgs args)
        {
            ReloadDateDependentData();
        }

        /// <summary>
        /// Handles Closed event of cbDistr: performs dist dependent filtering
        /// </summary>
        /// <param name="sender">cbDistr</param>
        /// <param name="e">ClosedEventArgs</param>
        private void cbDistr_Closed(object sender, ClosedEventArgs e)
        {
            lDistr.Focus();

            FilterPosListGeneral();
            FilterSCListGeneral();
        }

        /// <summary>
        /// Handles Closed event of cbDistrType: perfoms dist type dependent filtering
        /// </summary>
        /// <param name="sender">cbDistrType</param>
        /// <param name="e">ClosedEventArgs</param>
        private void cbDistrType_Closed(object sender, ClosedEventArgs e)
        {
            FilterDistrGeneral();

            FilterPosListGeneral();
            FilterSCListGeneral();
        }

        /// <summary>
        /// Filters warehouses by location
        /// </summary>
        private void tWarehouseLocation_AfterCheckNode(object sender, EventArgs e)
        {
            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Filters POSs by locations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tPosLocation_AfterCheckNode(object sender, EventArgs e)
        {
            FilterPosListGeneral();
            FilterSCListGeneral();
        }

        /// <summary>
        /// Filters SCs by POSs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbPos_Closed(object sender, ClosedEventArgs e)
        {
            FilterSCListGeneral();
        }

        /// <summary>
        /// Handled CheckedChanged event of cDetalizationByBreaks
        /// </summary>
        /// <param name="sender">cDetalizationByBreaks</param>
        /// <param name="e">EventArgs</param>
        private void cDetalizationByBreaks_CheckedChanged(object sender, EventArgs e)
        {
            if (cDetalizationByBreaks.Checked)
            {
                cDetalizationByBreaksType.Checked = true;
            }
        }

        /// <summary>
        /// Handled CheckedChanged event of cDetalizationByBreaksType
        /// </summary>
        /// <param name="sender">cDetalizationByBreaksType</param>
        /// <param name="e">EventArgs</param>
        private void cDetalizationByBreaksType_CheckedChanged(object sender, EventArgs e)
        {
            bool tryingToUncheckTypeWhileBreaksIsChecked = !cDetalizationByBreaksType.Checked &&
                                                           cDetalizationByBreaks.Checked;
            if (tryingToUncheckTypeWhileBreaksIsChecked)
            {
                cDetalizationByBreaksType.Checked = true;
            }
        }
        
        /// <summary>
        /// Selects/deselects all Pos locations
        /// </summary>
        private void cPos_CheckedChanged(object sender, EventArgs e)
        {
            if (cPos.CheckState == CheckState.Unchecked)
            {
                ShouldClearPosesSelection = true;
            }

            HidePosLevel();
            FilterPosListGeneral();
        }

        /// <summary>
        /// Selects/deselects all warehouses locations
        /// </summary>
        private void cWareHouses_CheckedChanged(object sender, EventArgs e)
        {
            if (cWareHouses.CheckState == CheckState.Unchecked)
            {
                ShouldClearWarehousesSelection = true;
            }

            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Handles AfterCheckNode event of tTerritory: performs territory dependent filtering
        /// </summary>
        /// <param name="sender">tTerritory</param>
        /// <param name="e">NodeEventArgs</param>
        private void tTerritory_AfterNodeChecked(object sender, EventArgs args)
        {
            FilterTreesByRegion();

            FilterDistrGeneral();

            FilterPosListGeneral();
            HidePosLevel();

            FilterSCListGeneral();

            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Forces selected list redrawing
        /// </summary>
        private void cluWarehouses_Closed(object sender, ClosedEventArgs e)
        {
            lWarehouses.Focus();
        }

        /// <summary>
        /// Performs equipment state filtering
        /// </summary>
        private void cAppOpen_CheckedChanged(object sender, EventArgs e)
        {
            string lFilter = string.Empty;

            if (!cAppOpen.Checked && !cAppClosed.Checked)
            {
                lFilter = string.Format(FilterByColumn, IsApplicationOpen, DumbFilter);

                cApplicationStatuses.FilterList(lFilter);
            }
            else
            {
                List<string> lCheckedValues = cApplicationStatuses.GetComboBoxCheckedValues();
                List<string> lAllValues = new List<string>(cApplicationStatuses.GetComboBoxValuesList().Split(','));

                if (cAppOpen.Checked && cAppClosed.Checked)
                {
                    lFilter = string.Empty;
                }
                else
                {
                    lFilter = string.Format(FilterByColumn, IsApplicationOpen,
                                            cAppOpen.Checked ? 1 : 0);
                }

                cApplicationStatuses.FilterList(lFilter);

                for (int i = 0; i < cApplicationStatuses.Properties.Items.Count; i++)
                {
                    string lValue = cApplicationStatuses.Properties.Items[i].Value.ToString();

                    if (lAllValues.Contains(lValue))
                    {
                        cApplicationStatuses.Properties.Items[i].CheckState = lCheckedValues.Contains(lValue)
                                                                              ? CheckState.Checked
                                                                              : CheckState.Unchecked;
                    }
                    else
                    {
                        cApplicationStatuses.Properties.Items[i].CheckState = CheckState.Checked;
                    }
                }

            }
        }

        /// <summary>
        /// Performs warehouse filtering 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void tStaffing_AfterNodeChecked(object sender, EventArgs args)
        {
            FilterWarehousesListGeneral();
        }

        /// <summary>
        /// Performs warehouse filtering 
        /// </summary>
        private void cStaffing_CheckedChanged(object sender, EventArgs e)
        {
            FilterWarehousesListGeneral();
        }
        #endregion
   }
}
