﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.EquipmentReparation.Tabs;

namespace SoftServe.Reports.EquipmentReparation.UserControls
{
    [ToolboxItem(false)]
    public partial class MainControl : UserControl
    {
        private const string CompanyDirectory = "SoftServe";
        private const string ProgramDirectory = "WCCP Reporting";
        private const string FileName = "EquipmentReparation.xml";

        /// <summary>
        /// Is used for storing column width before view modification before export
        /// </summary>
        private List<int> _currentColumnsConf = new List<int>();

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl(DateInterval reportDates)
        {
            InitializeComponent();

            UpdateCaptions(reportDates);
        }

        /// <summary>
        /// Gets or sets the grid datasource
        /// </summary>
        public DataTable DataSource
        {
            get
            {
                return gridControl.DataSource as DataTable;
            }

            set
            {
                foreach (DataRow dr in value.Rows)
                {
                    dr[colRentalPrice.FieldName] = double.Parse(ConvertEx.ToDouble(dr[colRentalPrice.FieldName]).ToString("N2"));
                    dr[colInitPrice.FieldName] = double.Parse(ConvertEx.ToDouble(dr[colInitPrice.FieldName]).ToString("N2"));

                    if (dr[colWorkCost.FieldName] != DBNull.Value)
                    {
                        dr[colWorkCost.FieldName] = double.Parse(ConvertEx.ToDouble(dr[colWorkCost.FieldName]).ToString("N2"));
                    }

                    if (dr[colElementPrice.FieldName] != DBNull.Value)
                    {
                        dr[colElementPrice.FieldName] =
                            double.Parse(ConvertEx.ToDouble(dr[colElementPrice.FieldName]).ToString("N2"));
                    }

                    if (dr[colTotalPrice.FieldName] != DBNull.Value)
                    {
                        dr[colTotalPrice.FieldName] = double.Parse(ConvertEx.ToDouble(dr[colTotalPrice.FieldName]).ToString("N2"));
                    }
                }

                foreach (DataColumn col in value.Columns)
                {
                    if (col.DataType == typeof(DateTime))
                    {
                        foreach (DataRow dr in value.Rows)
                        {
                            if (dr[col] != DBNull.Value)
                            {
                                dr[col] = ConvertEx.ToDateTime(dr[col]).Date;
                            }

                        }
                    }
                }

                gridControl.BeginUpdate();
                gridControl.DataSource = value;
                gridControl.EndUpdate();
            }
        }

        /// <summary>
        /// Changes view in order to export it properly 
        /// </summary>
        internal void PrepareViewToExport()
        {
            //turn off view caption
            gridView.OptionsView.ShowViewCaption = false;

            //turn off formatting
            foreach (GridColumn col in gridView.Columns)
            {
                col.DisplayFormat.FormatString = string.Empty;
            }

            //save column conf
            _currentColumnsConf.Clear();

            foreach (GridColumn column in gridView.Columns)
            {
                _currentColumnsConf.Add(column.Width);
            }

            //tune-up columns width
            gridView.BestFitColumns();

            //for custom header display (in xls file)
            if (gridView.VisibleColumns.Count >= 2)
            {
                gridView.VisibleColumns[0].Width = MainTab.HEADER_WIDTH;
                gridView.VisibleColumns[1].Width = MainTab.HEADER_WIDTH;
            }
        }

        /// <summary>
        /// Restores view state after export 
        /// </summary>
        internal void RestoreViewAfterExport()
        {
            //turn on view caption
            gridView.OptionsView.ShowViewCaption = true;

            // turn on formatting
            foreach (GridColumn col in gridView.Columns)
            {
                if (col.DisplayFormat.FormatType == FormatType.Numeric)
                {
                    col.DisplayFormat.FormatString = "N0";
                }
            }

            //restore columns width
            for (int i = 0; i < _currentColumnsConf.Count; i++)
            {
                gridView.Columns[i].Width = _currentColumnsConf[i];
            }

        }

        /// <summary>
        /// Updates view and column captions with the chosen report interval
        /// </summary>
        /// <param name="reportDates">The report interval</param>
        private void UpdateCaptions(DateInterval reportDates)
        {
            string lStartDate = reportDates.From.ToString("d", new CultureInfo("ru-Ru"));
            string lEndDate = reportDates.To.ToString("d", new CultureInfo("ru-Ru"));

            gridView.ViewCaption = string.Format("Дата    с   {0}      по   {1}", lStartDate, lEndDate);

            foreach (GridColumn col in gridView.Columns)
            {
                col.Caption = col.Caption.Contains("(с_") ? col.Caption.Replace("(с_", string.Format("\n(c {0}", lStartDate)) : col.Caption.Replace("с_", string.Format("\nc {0}", lStartDate));
                col.Caption = col.Caption.Replace("по_", string.Format(" по {0}", lEndDate));
                col.Caption = col.Caption.Replace("на_", string.Format("на {0}", lEndDate));
            }
        }

        /// <summary>
        /// Handles ShowCustomizationForm event - is needed to show form over Report, not beneath
        /// </summary>
        /// <param name="sender">gridView</param>
        /// <param name="e">EventArgs</param>
        private void gridView_ShowCustomizationForm(object sender, EventArgs e)
        {
            gridView.CustomizationForm.TopMost = true;
        }

        /// <summary>
        /// Saves gridview layout for current user on disc
        /// </summary>
        internal void SaveLayout()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);

            if (!Directory.Exists(lCompanyDirPath))
            {
                Directory.CreateDirectory(lCompanyDirPath);
            }

            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);

            if (!Directory.Exists(lProgramDirPath))
            {
                Directory.CreateDirectory(lProgramDirPath);
            }

            string lFilePath = Path.Combine(lProgramDirPath, FileName);

            gridControl.MainView.SaveLayoutToXml(lFilePath);
        }

        /// <summary>
        /// Restores gridview layout for current user from file on disc
        /// </summary>
        internal void RestoreLayout()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);
            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);
            string lFilePath = Path.Combine(lProgramDirPath, FileName);

            if (File.Exists(lFilePath))
            {
                gridControl.MainView.RestoreLayoutFromXml(lFilePath);
            }
        }

        private void gridView_BeforeLoadLayout(object sender, DevExpress.Utils.LayoutAllowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.PreviousVersion))
            {
                e.Allow = false;
                return;
            }

            e.Allow = e.PreviousVersion == gridView.OptionsLayout.LayoutVersion;
        }
    }
}
