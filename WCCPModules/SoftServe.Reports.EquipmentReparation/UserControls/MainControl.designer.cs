﻿namespace SoftServe.Reports.EquipmentReparation.UserControls
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDocumentNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAppCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppSendingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppRecievingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppPlannedFinDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppFinDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1ConfirmationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallbackTerm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceCenter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepeatedApp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDeinstalation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiscall = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiscallReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentKind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManufacterer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConstructionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncomeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegenerationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvenNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBrand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTechnicalState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInitPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colМ4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colМ3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistributor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrSAPCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreementNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreementDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetirementDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1Telephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1Comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2Telephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoordinatorComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExecutionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElementPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreakTypeM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreaksM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreakTypeSC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreakSC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocJurName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocJurAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPocActualAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContatctPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheck)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rTextEdit,
            this.rCheck});
            this.gridControl.Size = new System.Drawing.Size(900, 500);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 45;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDocumentNumber,
            this.colAppCreationDate,
            this.colAppSendingDate,
            this.colAppRecievingDate,
            this.colAppPlannedFinDate,
            this.colAppFinDate,
            this.colM1ConfirmationDate,
            this.colCallbackTerm,
            this.colServiceCenter,
            this.colRepeatedApp,
            this.colDeinstalation,
            this.colMiscall,
            this.colMiscallReason,
            this.colEquipmentKind,
            this.colEquipmentModel,
            this.colManufacterer,
            this.colConstructionDate,
            this.colIncomeDate,
            this.colRegenerationDate,
            this.colInvenNumber,
            this.colSerialNumber,
            this.colBrand,
            this.colTechnicalState,
            this.colRentalPrice,
            this.colInitPrice,
            this.colМ4,
            this.colМ3,
            this.colRegion,
            this.colDistrType,
            this.colDistributor,
            this.colDistrSAPCode,
            this.colPos,
            this.colWarehouseName,
            this.colWarehouseLocation,
            this.colAgreementNumber,
            this.colAgreementDate,
            this.colValidTo,
            this.colRetirementDate,
            this.colM1,
            this.colM1Telephone,
            this.colM1Comment,
            this.colM2,
            this.colM2Telephone,
            this.colCoordinatorComment,
            this.colAppStatus,
            this.colExecutionStatus,
            this.colWorkCost,
            this.colElementPrice,
            this.colTotalPrice,
            this.colBreakTypeM1,
            this.colBreaksM1,
            this.colBreakTypeSC,
            this.colBreakSC,
            this.colWorks,
            this.colDetails,
            this.colPocCode,
            this.colPocType,
            this.colPocJurName,
            this.colPocJurAdress,
            this.colActualName,
            this.colPocActualAdress,
            this.colContatctPerson,
            this.colContactPersonPhone});
            this.gridView.GridControl = this.gridControl;
            this.gridView.GroupFormat = "{0}: {1}";
            this.gridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "UpliftPct", null, "{0:N2}", "2"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftDal", null, "{0:N3}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftMACO", null, "{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "AvgMACO", null, "{0:N3}", "7"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SalesDal_Promo", null, "{0:N3}")});
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsLayout.LayoutVersion = "1";
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Дата с  _  - , Дата по  _ ";
            this.gridView.ShowCustomizationForm += new System.EventHandler(this.gridView_ShowCustomizationForm);
            this.gridView.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.gridView_BeforeLoadLayout);
            // 
            // colDocumentNumber
            // 
            this.colDocumentNumber.Caption = "Номер документа";
            this.colDocumentNumber.ColumnEdit = this.rTextEdit;
            this.colDocumentNumber.FieldName = "POSRepairs_ID";
            this.colDocumentNumber.Name = "colDocumentNumber";
            this.colDocumentNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDocumentNumber.Visible = true;
            this.colDocumentNumber.VisibleIndex = 0;
            this.colDocumentNumber.Width = 100;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // colAppCreationDate
            // 
            this.colAppCreationDate.Caption = "Дата создания заявки";
            this.colAppCreationDate.FieldName = "POSRepairsDate";
            this.colAppCreationDate.Name = "colAppCreationDate";
            this.colAppCreationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAppCreationDate.Visible = true;
            this.colAppCreationDate.VisibleIndex = 1;
            this.colAppCreationDate.Width = 100;
            // 
            // colAppSendingDate
            // 
            this.colAppSendingDate.Caption = "Дата отправки заявки на СЦ";
            this.colAppSendingDate.FieldName = "FD1";
            this.colAppSendingDate.Name = "colAppSendingDate";
            this.colAppSendingDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAppSendingDate.Visible = true;
            this.colAppSendingDate.VisibleIndex = 2;
            this.colAppSendingDate.Width = 100;
            // 
            // colAppRecievingDate
            // 
            this.colAppRecievingDate.Caption = "Дата принятия заявки СЦ";
            this.colAppRecievingDate.FieldName = "FD2";
            this.colAppRecievingDate.Name = "colAppRecievingDate";
            this.colAppRecievingDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAppRecievingDate.Visible = true;
            this.colAppRecievingDate.VisibleIndex = 3;
            this.colAppRecievingDate.Width = 100;
            // 
            // colAppPlannedFinDate
            // 
            this.colAppPlannedFinDate.Caption = "Плановая дата выполнения заявки";
            this.colAppPlannedFinDate.FieldName = "CompletionDate";
            this.colAppPlannedFinDate.Name = "colAppPlannedFinDate";
            this.colAppPlannedFinDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAppPlannedFinDate.Visible = true;
            this.colAppPlannedFinDate.VisibleIndex = 4;
            this.colAppPlannedFinDate.Width = 100;
            // 
            // colAppFinDate
            // 
            this.colAppFinDate.Caption = "Дата выполнения заявки СЦ";
            this.colAppFinDate.FieldName = "FD3";
            this.colAppFinDate.Name = "colAppFinDate";
            this.colAppFinDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAppFinDate.Visible = true;
            this.colAppFinDate.VisibleIndex = 5;
            this.colAppFinDate.Width = 100;
            // 
            // colM1ConfirmationDate
            // 
            this.colM1ConfirmationDate.Caption = "Дата подтверждения M1";
            this.colM1ConfirmationDate.FieldName = "DateConfirm";
            this.colM1ConfirmationDate.Name = "colM1ConfirmationDate";
            this.colM1ConfirmationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1ConfirmationDate.Visible = true;
            this.colM1ConfirmationDate.VisibleIndex = 6;
            this.colM1ConfirmationDate.Width = 100;
            // 
            // colCallbackTerm
            // 
            this.colCallbackTerm.Caption = "Срок реагирования";
            this.colCallbackTerm.FieldName = "ReactionPeriod";
            this.colCallbackTerm.Name = "colCallbackTerm";
            this.colCallbackTerm.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCallbackTerm.Visible = true;
            this.colCallbackTerm.VisibleIndex = 7;
            this.colCallbackTerm.Width = 100;
            // 
            // colServiceCenter
            // 
            this.colServiceCenter.Caption = "Сервисный центр";
            this.colServiceCenter.FieldName = "POSSC_Name";
            this.colServiceCenter.Name = "colServiceCenter";
            this.colServiceCenter.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colServiceCenter.Visible = true;
            this.colServiceCenter.VisibleIndex = 8;
            this.colServiceCenter.Width = 100;
            // 
            // colRepeatedApp
            // 
            this.colRepeatedApp.Caption = "Повторная заявка";
            this.colRepeatedApp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRepeatedApp.FieldName = "IsRepeated";
            this.colRepeatedApp.Name = "colRepeatedApp";
            this.colRepeatedApp.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRepeatedApp.Visible = true;
            this.colRepeatedApp.VisibleIndex = 9;
            // 
            // rCheck
            // 
            this.rCheck.AutoHeight = false;
            this.rCheck.DisplayValueChecked = "Да";
            this.rCheck.DisplayValueUnchecked = "Нет";
            this.rCheck.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rCheck.Name = "rCheck";
            // 
            // colDeinstalation
            // 
            this.colDeinstalation.Caption = "Демонтаж для ремонта";
            this.colDeinstalation.ColumnEdit = this.rCheck;
            this.colDeinstalation.FieldName = "IsDiagnostic";
            this.colDeinstalation.Name = "colDeinstalation";
            this.colDeinstalation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDeinstalation.Visible = true;
            this.colDeinstalation.VisibleIndex = 10;
            // 
            // colMiscall
            // 
            this.colMiscall.Caption = "Ложный вызов";
            this.colMiscall.ColumnEdit = this.rCheck;
            this.colMiscall.FieldName = "IsFalseAlarm";
            this.colMiscall.Name = "colMiscall";
            this.colMiscall.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colMiscall.Visible = true;
            this.colMiscall.VisibleIndex = 11;
            // 
            // colMiscallReason
            // 
            this.colMiscallReason.Caption = "Причина ложного вызова";
            this.colMiscallReason.FieldName = "ReasonNotComplete";
            this.colMiscallReason.Name = "colMiscallReason";
            this.colMiscallReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colMiscallReason.Visible = true;
            this.colMiscallReason.VisibleIndex = 12;
            this.colMiscallReason.Width = 125;
            // 
            // colEquipmentKind
            // 
            this.colEquipmentKind.Caption = "Вид оборудования";
            this.colEquipmentKind.FieldName = "POSGroup_Name";
            this.colEquipmentKind.Name = "colEquipmentKind";
            this.colEquipmentKind.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEquipmentKind.Visible = true;
            this.colEquipmentKind.VisibleIndex = 13;
            this.colEquipmentKind.Width = 100;
            // 
            // colEquipmentModel
            // 
            this.colEquipmentModel.Caption = "Модель оборудования";
            this.colEquipmentModel.FieldName = "POSType_Name";
            this.colEquipmentModel.Name = "colEquipmentModel";
            this.colEquipmentModel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEquipmentModel.Visible = true;
            this.colEquipmentModel.VisibleIndex = 14;
            this.colEquipmentModel.Width = 100;
            // 
            // colManufacterer
            // 
            this.colManufacterer.Caption = "Призводитель";
            this.colManufacterer.FieldName = "Manufacturer";
            this.colManufacterer.Name = "colManufacterer";
            this.colManufacterer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colManufacterer.Visible = true;
            this.colManufacterer.VisibleIndex = 15;
            this.colManufacterer.Width = 100;
            // 
            // colConstructionDate
            // 
            this.colConstructionDate.Caption = "Дата выпуска";
            this.colConstructionDate.FieldName = "YearProduction";
            this.colConstructionDate.Name = "colConstructionDate";
            this.colConstructionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colConstructionDate.Visible = true;
            this.colConstructionDate.VisibleIndex = 16;
            this.colConstructionDate.Width = 100;
            // 
            // colIncomeDate
            // 
            this.colIncomeDate.Caption = "Дата прихода";
            this.colIncomeDate.FieldName = "ArrivalDate";
            this.colIncomeDate.Name = "colIncomeDate";
            this.colIncomeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colIncomeDate.Visible = true;
            this.colIncomeDate.VisibleIndex = 17;
            this.colIncomeDate.Width = 100;
            // 
            // colRegenerationDate
            // 
            this.colRegenerationDate.Caption = "Дата восстановления";
            this.colRegenerationDate.FieldName = "RecoveryDate";
            this.colRegenerationDate.Name = "colRegenerationDate";
            this.colRegenerationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegenerationDate.Visible = true;
            this.colRegenerationDate.VisibleIndex = 18;
            this.colRegenerationDate.Width = 125;
            // 
            // colInvenNumber
            // 
            this.colInvenNumber.Caption = "Инвентарный номер";
            this.colInvenNumber.FieldName = "Invent_No";
            this.colInvenNumber.Name = "colInvenNumber";
            this.colInvenNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInvenNumber.Visible = true;
            this.colInvenNumber.VisibleIndex = 19;
            this.colInvenNumber.Width = 100;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.Caption = "Серийный номер";
            this.colSerialNumber.FieldName = "Serial_No";
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 20;
            this.colSerialNumber.Width = 100;
            // 
            // colBrand
            // 
            this.colBrand.Caption = "Бренд";
            this.colBrand.FieldName = "POSBrand_Name";
            this.colBrand.Name = "colBrand";
            this.colBrand.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBrand.Visible = true;
            this.colBrand.VisibleIndex = 21;
            this.colBrand.Width = 100;
            // 
            // colTechnicalState
            // 
            this.colTechnicalState.Caption = "Статус оборудования";
            this.colTechnicalState.FieldName = "TechnicalCondition";
            this.colTechnicalState.Name = "colTechnicalState";
            this.colTechnicalState.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTechnicalState.Visible = true;
            this.colTechnicalState.VisibleIndex = 22;
            this.colTechnicalState.Width = 100;
            // 
            // colRentalPrice
            // 
            this.colRentalPrice.Caption = "Стоимость залоговая";
            this.colRentalPrice.DisplayFormat.FormatString = "N2";
            this.colRentalPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRentalPrice.FieldName = "CollateralValue";
            this.colRentalPrice.Name = "colRentalPrice";
            this.colRentalPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRentalPrice.Visible = true;
            this.colRentalPrice.VisibleIndex = 23;
            this.colRentalPrice.Width = 100;
            // 
            // colInitPrice
            // 
            this.colInitPrice.Caption = "Стоимость первоначальная";
            this.colInitPrice.DisplayFormat.FormatString = "N2";
            this.colInitPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colInitPrice.FieldName = "InitialCost";
            this.colInitPrice.Name = "colInitPrice";
            this.colInitPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInitPrice.Visible = true;
            this.colInitPrice.VisibleIndex = 24;
            this.colInitPrice.Width = 100;
            // 
            // colМ4
            // 
            this.colМ4.Caption = "М4";
            this.colМ4.FieldName = "RM_name";
            this.colМ4.Name = "colМ4";
            this.colМ4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colМ4.Visible = true;
            this.colМ4.VisibleIndex = 25;
            this.colМ4.Width = 100;
            // 
            // colМ3
            // 
            this.colМ3.Caption = "М3";
            this.colМ3.FieldName = "DSM_Name";
            this.colМ3.Name = "colМ3";
            this.colМ3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colМ3.Visible = true;
            this.colМ3.VisibleIndex = 26;
            this.colМ3.Width = 100;
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "OLRegion_name";
            this.colRegion.Name = "colRegion";
            this.colRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 27;
            this.colRegion.Width = 100;
            // 
            // colDistrType
            // 
            this.colDistrType.Caption = "Схема работы";
            this.colDistrType.FieldName = "WorkSchema";
            this.colDistrType.Name = "colDistrType";
            this.colDistrType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrType.Visible = true;
            this.colDistrType.VisibleIndex = 28;
            this.colDistrType.Width = 100;
            // 
            // colDistributor
            // 
            this.colDistributor.Caption = "Название дистрибьютора";
            this.colDistributor.FieldName = "Distr_name";
            this.colDistributor.Name = "colDistributor";
            this.colDistributor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistributor.Visible = true;
            this.colDistributor.VisibleIndex = 29;
            this.colDistributor.Width = 125;
            // 
            // colDistrSAPCode
            // 
            this.colDistrSAPCode.Caption = "Код дистрибьютора SAP";
            this.colDistrSAPCode.FieldName = "Distr_SAP";
            this.colDistrSAPCode.Name = "colDistrSAPCode";
            this.colDistrSAPCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrSAPCode.Visible = true;
            this.colDistrSAPCode.VisibleIndex = 30;
            this.colDistrSAPCode.Width = 125;
            // 
            // colPos
            // 
            this.colPos.Caption = "Точка синхронизации";
            this.colPos.FieldName = "Cust_name";
            this.colPos.Name = "colPos";
            this.colPos.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPos.Visible = true;
            this.colPos.VisibleIndex = 31;
            this.colPos.Width = 125;
            // 
            // colWarehouseName
            // 
            this.colWarehouseName.Caption = "Название склада";
            this.colWarehouseName.FieldName = "WarehouseName";
            this.colWarehouseName.Name = "colWarehouseName";
            this.colWarehouseName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWarehouseName.Visible = true;
            this.colWarehouseName.VisibleIndex = 32;
            this.colWarehouseName.Width = 100;
            // 
            // colWarehouseLocation
            // 
            this.colWarehouseLocation.Caption = "Адрес склада";
            this.colWarehouseLocation.FieldName = "WarehouseAddress";
            this.colWarehouseLocation.Name = "colWarehouseLocation";
            this.colWarehouseLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWarehouseLocation.Visible = true;
            this.colWarehouseLocation.VisibleIndex = 33;
            this.colWarehouseLocation.Width = 100;
            // 
            // colAgreementNumber
            // 
            this.colAgreementNumber.Caption = "№ договора";
            this.colAgreementNumber.FieldName = "OlContractId";
            this.colAgreementNumber.Name = "colAgreementNumber";
            this.colAgreementNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAgreementNumber.Visible = true;
            this.colAgreementNumber.VisibleIndex = 34;
            this.colAgreementNumber.Width = 100;
            // 
            // colAgreementDate
            // 
            this.colAgreementDate.Caption = "Дата заключения";
            this.colAgreementDate.FieldName = "StartDate";
            this.colAgreementDate.Name = "colAgreementDate";
            this.colAgreementDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAgreementDate.Visible = true;
            this.colAgreementDate.VisibleIndex = 35;
            this.colAgreementDate.Width = 100;
            // 
            // colValidTo
            // 
            this.colValidTo.Caption = "Договор действителен до";
            this.colValidTo.FieldName = "EndDate";
            this.colValidTo.Name = "colValidTo";
            this.colValidTo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colValidTo.Visible = true;
            this.colValidTo.VisibleIndex = 36;
            this.colValidTo.Width = 100;
            // 
            // colRetirementDate
            // 
            this.colRetirementDate.Caption = "Дата списания";
            this.colRetirementDate.FieldName = "DisposalDate";
            this.colRetirementDate.Name = "colRetirementDate";
            this.colRetirementDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRetirementDate.Visible = true;
            this.colRetirementDate.VisibleIndex = 37;
            this.colRetirementDate.Width = 100;
            // 
            // colM1
            // 
            this.colM1.Caption = "М1";
            this.colM1.FieldName = "MerchName";
            this.colM1.Name = "colM1";
            this.colM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1.Visible = true;
            this.colM1.VisibleIndex = 38;
            this.colM1.Width = 100;
            // 
            // colM1Telephone
            // 
            this.colM1Telephone.Caption = "Телефон М1";
            this.colM1Telephone.FieldName = "M1Phone";
            this.colM1Telephone.Name = "colM1Telephone";
            this.colM1Telephone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Telephone.Visible = true;
            this.colM1Telephone.VisibleIndex = 39;
            // 
            // colM1Comment
            // 
            this.colM1Comment.Caption = "Комментарий М1";
            this.colM1Comment.FieldName = "Comment";
            this.colM1Comment.Name = "colM1Comment";
            this.colM1Comment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Comment.Visible = true;
            this.colM1Comment.VisibleIndex = 40;
            this.colM1Comment.Width = 100;
            // 
            // colM2
            // 
            this.colM2.Caption = "M2";
            this.colM2.FieldName = "SupervisorName";
            this.colM2.Name = "colM2";
            this.colM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2.Visible = true;
            this.colM2.VisibleIndex = 41;
            this.colM2.Width = 100;
            // 
            // colM2Telephone
            // 
            this.colM2Telephone.Caption = "Телефон М2";
            this.colM2Telephone.FieldName = "M2Phone";
            this.colM2Telephone.Name = "colM2Telephone";
            this.colM2Telephone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2Telephone.Visible = true;
            this.colM2Telephone.VisibleIndex = 42;
            this.colM2Telephone.Width = 100;
            // 
            // colCoordinatorComment
            // 
            this.colCoordinatorComment.Caption = "Комментарий координатора ";
            this.colCoordinatorComment.FieldName = "FS1";
            this.colCoordinatorComment.Name = "colCoordinatorComment";
            this.colCoordinatorComment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCoordinatorComment.Visible = true;
            this.colCoordinatorComment.VisibleIndex = 43;
            this.colCoordinatorComment.Width = 100;
            // 
            // colAppStatus
            // 
            this.colAppStatus.Caption = "Статус заявки";
            this.colAppStatus.FieldName = "Status";
            this.colAppStatus.Name = "colAppStatus";
            this.colAppStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAppStatus.Visible = true;
            this.colAppStatus.VisibleIndex = 44;
            this.colAppStatus.Width = 100;
            // 
            // colExecutionStatus
            // 
            this.colExecutionStatus.Caption = "Статус выполнения";
            this.colExecutionStatus.FieldName = "StatusOfExecution";
            this.colExecutionStatus.Name = "colExecutionStatus";
            this.colExecutionStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colExecutionStatus.Visible = true;
            this.colExecutionStatus.VisibleIndex = 45;
            this.colExecutionStatus.Width = 100;
            // 
            // colWorkCost
            // 
            this.colWorkCost.Caption = "Стоимость работ";
            this.colWorkCost.DisplayFormat.FormatString = "N2";
            this.colWorkCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colWorkCost.FieldName = "RepairWorksCost";
            this.colWorkCost.Name = "colWorkCost";
            this.colWorkCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWorkCost.Visible = true;
            this.colWorkCost.VisibleIndex = 46;
            this.colWorkCost.Width = 100;
            // 
            // colElementPrice
            // 
            this.colElementPrice.Caption = "Стоимость запчастей";
            this.colElementPrice.DisplayFormat.FormatString = "N2";
            this.colElementPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colElementPrice.FieldName = "SparePartsCost";
            this.colElementPrice.Name = "colElementPrice";
            this.colElementPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colElementPrice.Visible = true;
            this.colElementPrice.VisibleIndex = 47;
            this.colElementPrice.Width = 100;
            // 
            // colTotalPrice
            // 
            this.colTotalPrice.Caption = "Общая стоимость";
            this.colTotalPrice.DisplayFormat.FormatString = "N2";
            this.colTotalPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalPrice.FieldName = "RepairCost";
            this.colTotalPrice.Name = "colTotalPrice";
            this.colTotalPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTotalPrice.Visible = true;
            this.colTotalPrice.VisibleIndex = 48;
            this.colTotalPrice.Width = 100;
            // 
            // colBreakTypeM1
            // 
            this.colBreakTypeM1.Caption = "Тип поломки (данные от М1)";
            this.colBreakTypeM1.FieldName = "M1_BreakageTypeName";
            this.colBreakTypeM1.Name = "colBreakTypeM1";
            this.colBreakTypeM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBreakTypeM1.Visible = true;
            this.colBreakTypeM1.VisibleIndex = 49;
            this.colBreakTypeM1.Width = 100;
            // 
            // colBreaksM1
            // 
            this.colBreaksM1.Caption = "Поломки (данные от М1)";
            this.colBreaksM1.FieldName = "M1_BreakageName";
            this.colBreaksM1.Name = "colBreaksM1";
            this.colBreaksM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBreaksM1.Visible = true;
            this.colBreaksM1.VisibleIndex = 50;
            this.colBreaksM1.Width = 100;
            // 
            // colBreakTypeSC
            // 
            this.colBreakTypeSC.Caption = "Тип поломки (по оценке СЦ)";
            this.colBreakTypeSC.FieldName = "SS_BreakageTypeName";
            this.colBreakTypeSC.Name = "colBreakTypeSC";
            this.colBreakTypeSC.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBreakTypeSC.Visible = true;
            this.colBreakTypeSC.VisibleIndex = 51;
            this.colBreakTypeSC.Width = 100;
            // 
            // colBreakSC
            // 
            this.colBreakSC.Caption = "Поломки (по оценке СЦ)";
            this.colBreakSC.FieldName = "SS_BreakageName";
            this.colBreakSC.Name = "colBreakSC";
            this.colBreakSC.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBreakSC.Visible = true;
            this.colBreakSC.VisibleIndex = 52;
            this.colBreakSC.Width = 100;
            // 
            // colWorks
            // 
            this.colWorks.Caption = "Работы";
            this.colWorks.FieldName = "RepairWorks";
            this.colWorks.Name = "colWorks";
            this.colWorks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWorks.Visible = true;
            this.colWorks.VisibleIndex = 53;
            // 
            // colDetails
            // 
            this.colDetails.Caption = "Запчасти";
            this.colDetails.FieldName = "SpareParts";
            this.colDetails.Name = "colDetails";
            this.colDetails.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDetails.Visible = true;
            this.colDetails.VisibleIndex = 54;
            // 
            // colPocCode
            // 
            this.colPocCode.Caption = "Код ТТ";
            this.colPocCode.FieldName = "OL_ID";
            this.colPocCode.Name = "colPocCode";
            this.colPocCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocCode.Visible = true;
            this.colPocCode.VisibleIndex = 55;
            this.colPocCode.Width = 100;
            // 
            // colPocType
            // 
            this.colPocType.Caption = "Тип ТТ";
            this.colPocType.FieldName = "OLtype_name";
            this.colPocType.Name = "colPocType";
            this.colPocType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocType.Visible = true;
            this.colPocType.VisibleIndex = 56;
            // 
            // colPocJurName
            // 
            this.colPocJurName.Caption = "Юр. название ТТ";
            this.colPocJurName.FieldName = "OLName";
            this.colPocJurName.Name = "colPocJurName";
            this.colPocJurName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocJurName.Visible = true;
            this.colPocJurName.VisibleIndex = 57;
            this.colPocJurName.Width = 100;
            // 
            // colPocJurAdress
            // 
            this.colPocJurAdress.Caption = "Юр.адрес ТТ";
            this.colPocJurAdress.FieldName = "OLAddress";
            this.colPocJurAdress.Name = "colPocJurAdress";
            this.colPocJurAdress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocJurAdress.Visible = true;
            this.colPocJurAdress.VisibleIndex = 58;
            this.colPocJurAdress.Width = 150;
            // 
            // colActualName
            // 
            this.colActualName.Caption = "Факт. имя ТТ";
            this.colActualName.FieldName = "OLTradingName";
            this.colActualName.Name = "colActualName";
            this.colActualName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActualName.Visible = true;
            this.colActualName.VisibleIndex = 59;
            this.colActualName.Width = 100;
            // 
            // colPocActualAdress
            // 
            this.colPocActualAdress.Caption = "Факт. адрес ТТ";
            this.colPocActualAdress.FieldName = "OLDeliveryAddress";
            this.colPocActualAdress.Name = "colPocActualAdress";
            this.colPocActualAdress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPocActualAdress.Visible = true;
            this.colPocActualAdress.VisibleIndex = 60;
            this.colPocActualAdress.Width = 150;
            // 
            // colContatctPerson
            // 
            this.colContatctPerson.Caption = "Контактное лицо";
            this.colContatctPerson.FieldName = "ContactPerson";
            this.colContatctPerson.Name = "colContatctPerson";
            this.colContatctPerson.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colContatctPerson.Visible = true;
            this.colContatctPerson.VisibleIndex = 61;
            this.colContatctPerson.Width = 100;
            // 
            // colContactPersonPhone
            // 
            this.colContactPersonPhone.Caption = "Телефон контактного лица";
            this.colContactPersonPhone.FieldName = "ContactPersonPhone";
            this.colContactPersonPhone.Name = "colContactPersonPhone";
            this.colContactPersonPhone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colContactPersonPhone.Visible = true;
            this.colContactPersonPhone.VisibleIndex = 62;
            this.colContactPersonPhone.Width = 100;
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(900, 500);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheck)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gridControl;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAppCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppSendingDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppRecievingDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppPlannedFinDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppFinDate;
        private DevExpress.XtraGrid.Columns.GridColumn colM1ConfirmationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCallbackTerm;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceCenter;
        private DevExpress.XtraGrid.Columns.GridColumn colRepeatedApp;
        private DevExpress.XtraGrid.Columns.GridColumn colDeinstalation;
        private DevExpress.XtraGrid.Columns.GridColumn colMiscall;
        private DevExpress.XtraGrid.Columns.GridColumn colPocCode;
        private DevExpress.XtraGrid.Columns.GridColumn colPocJurName;
        private DevExpress.XtraGrid.Columns.GridColumn colActualName;
        private DevExpress.XtraGrid.Columns.GridColumn colPocActualAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colMiscallReason;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentKind;
        private DevExpress.XtraGrid.Columns.GridColumn colBrand;
        private DevExpress.XtraGrid.Columns.GridColumn colRetirementDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentModel;
        private DevExpress.XtraGrid.Columns.GridColumn colManufacterer;
        private DevExpress.XtraGrid.Columns.GridColumn colConstructionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncomeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRegenerationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvenNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTechnicalState;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colInitPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colМ4;
        private DevExpress.XtraGrid.Columns.GridColumn colМ3;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrType;
        private DevExpress.XtraGrid.Columns.GridColumn colDistributor;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrSAPCode;
        private DevExpress.XtraGrid.Columns.GridColumn colPos;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreementNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreementDate;
        private DevExpress.XtraGrid.Columns.GridColumn colValidTo;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colM1;
        private DevExpress.XtraGrid.Columns.GridColumn colM1Telephone;
        private DevExpress.XtraGrid.Columns.GridColumn colM1Comment;
        private DevExpress.XtraGrid.Columns.GridColumn colM2;
        private DevExpress.XtraGrid.Columns.GridColumn colM2Telephone;
        private DevExpress.XtraGrid.Columns.GridColumn colCoordinatorComment;
        private DevExpress.XtraGrid.Columns.GridColumn colAppStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colExecutionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkCost;
        private DevExpress.XtraGrid.Columns.GridColumn colElementPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colBreakTypeM1;
        private DevExpress.XtraGrid.Columns.GridColumn colBreaksM1;
        private DevExpress.XtraGrid.Columns.GridColumn colBreakTypeSC;
        private DevExpress.XtraGrid.Columns.GridColumn colBreakSC;
        private DevExpress.XtraGrid.Columns.GridColumn colWorks;
        private DevExpress.XtraGrid.Columns.GridColumn colDetails;
        private DevExpress.XtraGrid.Columns.GridColumn colPocType;
        private DevExpress.XtraGrid.Columns.GridColumn colPocJurAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colContatctPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonPhone;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rCheck;

    }
}
