﻿namespace SoftServe.Reports.EquipmentReparation.UserControls
{
    partial class TimeInterval
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lFrom = new DevExpress.XtraEditors.LabelControl();
            this.dFrom = new DevExpress.XtraEditors.DateEdit();
            this.lTo = new DevExpress.XtraEditors.LabelControl();
            this.dTo = new DevExpress.XtraEditors.DateEdit();
            this.tlpDateInterval = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties)).BeginInit();
            this.tlpDateInterval.SuspendLayout();
            this.SuspendLayout();
            // 
            // lFrom
            // 
            this.lFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lFrom.Location = new System.Drawing.Point(17, 5);
            this.lFrom.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.lFrom.Name = "lFrom";
            this.lFrom.Size = new System.Drawing.Size(5, 13);
            this.lFrom.TabIndex = 7;
            this.lFrom.Text = "с";
            // 
            // dFrom
            // 
            this.dFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dFrom.EditValue = null;
            this.dFrom.Location = new System.Drawing.Point(28, 3);
            this.dFrom.Name = "dFrom";
            this.dFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFrom.Properties.NullText = " ";
            this.dFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dFrom.Size = new System.Drawing.Size(155, 20);
            this.dFrom.TabIndex = 8;
            this.dFrom.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // lTo
            // 
            this.lTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lTo.Location = new System.Drawing.Point(196, 5);
            this.lTo.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.lTo.Name = "lTo";
            this.lTo.Size = new System.Drawing.Size(12, 13);
            this.lTo.TabIndex = 9;
            this.lTo.Text = "по";
            // 
            // dTo
            // 
            this.dTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dTo.EditValue = null;
            this.dTo.Location = new System.Drawing.Point(214, 3);
            this.dTo.Name = "dTo";
            this.dTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dTo.Size = new System.Drawing.Size(158, 20);
            this.dTo.TabIndex = 10;
            this.dTo.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // tlpDateInterval
            // 
            this.tlpDateInterval.BackColor = System.Drawing.Color.Transparent;
            this.tlpDateInterval.ColumnCount = 4;
            this.tlpDateInterval.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.896552F));
            this.tlpDateInterval.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.10345F));
            this.tlpDateInterval.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.896552F));
            this.tlpDateInterval.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.10345F));
            this.tlpDateInterval.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpDateInterval.Controls.Add(this.lTo, 2, 0);
            this.tlpDateInterval.Controls.Add(this.dTo, 3, 0);
            this.tlpDateInterval.Controls.Add(this.lFrom, 0, 0);
            this.tlpDateInterval.Controls.Add(this.dFrom, 1, 0);
            this.tlpDateInterval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDateInterval.Location = new System.Drawing.Point(0, 0);
            this.tlpDateInterval.Name = "tlpDateInterval";
            this.tlpDateInterval.RowCount = 1;
            this.tlpDateInterval.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDateInterval.Size = new System.Drawing.Size(375, 26);
            this.tlpDateInterval.TabIndex = 11;
            // 
            // TimeInterval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tlpDateInterval);
            this.Name = "TimeInterval";
            this.Size = new System.Drawing.Size(375, 26);
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTo.Properties)).EndInit();
            this.tlpDateInterval.ResumeLayout(false);
            this.tlpDateInterval.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lFrom;
        private DevExpress.XtraEditors.DateEdit dFrom;
        private DevExpress.XtraEditors.LabelControl lTo;
        private DevExpress.XtraEditors.DateEdit dTo;
        private System.Windows.Forms.TableLayoutPanel tlpDateInterval;

    }
}
