﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.EquipmentReparation
{
    internal static class DataProvider
    {
        /// <summary>
        /// Gets the territory tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of territory division</param>
        /// <param name="levelTo">The lowest level of territory division</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetTerritoryTree(int? levelFrom, int? levelTo)
        {
            DataTable lDataTable = null;
            DataSet dataSet = levelFrom.HasValue && levelTo.HasValue ? DataAccessLayer.ExecuteStoredProcedure(Constants.SP_TERRITORY_TREE, new[] { 
                new SqlParameter(Constants.SP_TERRITORY_TREE_PARAM_1, levelFrom.Value),
                new SqlParameter(Constants.SP_TERRITORY_TREE_PARAM_2, levelTo.Value)
                })
            : DataAccessLayer.ExecuteStoredProcedure(Constants.SP_TERRITORY_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the distributors tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of distributors hierarchy</param>
        /// <param name="levelTo">The lowest level of distributors hierarchy</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetDistributorsTree(out int distrLevel)
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_DISTRIBUTORS_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                distrLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                distrLevel = 4;
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the staffing tree actual for the date chosen
        /// </summary>
        /// <param name="date">The date of cheching</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetStaffingTree(DateTime date)
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_STAFFING_TREE,
                new[] { new SqlParameter(Constants.SP_STAFFING_TREE_PARAM_1, date) });

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            DataView lStaffingView = lDataTable.DefaultView;
            lStaffingView.RowFilter = "Level > 0";
            lDataTable = lStaffingView.ToTable();

            return lDataTable;
        }

        /// <summary>
        /// Gets the list of warehouses
        /// </summary>
        /// <returns>The list of warehouses</returns>
        public static DataTable GetWarehouseList()
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_WAREHOUSES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the tree of warehouses interlaced with territory
        /// </summary>
        /// <returns>The list of warehouses</returns>
        public static DataTable GetWarehouseTree(out int warehouseLevel)
        {
            DataTable lDataTableDB = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_WAREHOUSES_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTableDB = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                warehouseLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                warehouseLevel = 7;
            }

            DataTable lDataTable = new DataTable();

            lDataTable.Columns.Add("FieldID");
            lDataTable.Columns.Add("FieldParentID");
            lDataTable.Columns.Add("FieldName");
            lDataTable.Columns.Add("Level");
            lDataTable.Columns.Add("FIeldCode");
            lDataTable.Columns.Add("wFieldID");
            lDataTable.Columns.Add("DSM_ID");

            foreach (DataRow row in lDataTableDB.Rows)
            {
                lDataTable.Rows.Add(row["FieldID"], row["FieldParentID"], row["FieldName"], row["Level"], row["FIeldCode"], row["wFieldID"], row["DSM_ID"]);
            }

            DataView lDataViewWarehouses = lDataTable.DefaultView;
            lDataViewWarehouses.RowFilter = string.Format("Level > 2");
            lDataTable = lDataViewWarehouses.ToTable();

            return lDataTable;
        }

        /// <summary>
        /// Gets the customers tree interlaced with territory
        /// </summary>
        /// <returns>Tree table structure</returns>
        public static DataTable GetPosesTree(out int posLevelLevel)
        {
            DataTable dataTableDB = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_CUSTOMERS_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTableDB = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                posLevelLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                posLevelLevel = 7;
            }

            DataTable lDataTable = new DataTable();

            lDataTable.Columns.Add("GeographyID");
            lDataTable.Columns.Add("ParentID");
            lDataTable.Columns.Add("GeographyCode");
            lDataTable.Columns.Add("GeographyName");
            lDataTable.Columns.Add("Level");
            lDataTable.Columns.Add("GeographyOldId");

            foreach (DataRow row in dataTableDB.Rows)
            {
                lDataTable.Rows.Add(row["GeographyID"], row["ParentID"], row["GeographyCode"], row["GeographyName"], row["Level"], row["GeographyOldId"]);
            }

            DataView lDataViewCustomers = lDataTable.DefaultView;
            lDataViewCustomers.RowFilter = string.Format("Level > 2");
            lDataTable = lDataViewCustomers.ToTable();

            return lDataTable;
        }

        /// <summary>
        /// Gets the Service Centers tree interlaced with territory
        /// </summary>
        /// <returns>Tree table structure</returns>
        public static DataTable GetServiceCentersTree(out int posSCLevel)
        {
            DataTable dataTableDB = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_SERVICE_CENTERS_TREE,
                new SqlParameter(Constants.SP_DISTRIBUTORS_TREE_PARAM_1, 7),
                new SqlParameter(Constants.SP_DISTRIBUTORS_TREE_PARAM_2, 8));

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTableDB = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTableDB = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                posSCLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                posSCLevel = 8;
            }

            return dataTableDB;
        }

        /// <summary>
        /// Gets the models tree
        /// </summary>
        /// <returns>The models tree</returns>
        public static DataTable GetModelsTree()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_MODELS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the brands list
        /// </summary>
        /// <returns>The brands list</returns>
        public static DataTable GetBrandList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_BRANDS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the list of application statuses
        /// </summary>
        /// <returns>The list of equipment application statuses</returns>
        public static DataTable GetApplicationStatusesList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_APPLICATION_STATUSES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets list of distributors
        /// </summary>
        /// <returns>The list of distributors</returns>
        public static DataTable GetDistrTypesList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_DISTR_TYPES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets list of manufacturers
        /// </summary>
        /// <returns>The list of manufacturers</returns>
        public static DataTable GetManufacturersList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_MANUFACTURERS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the main report data.
        /// </summary>
        /// <returns>The main report data.</returns>
        public static DataTable GetMainReportData(ReportInfo settings)
        {
            DataTable lDataTable = null;

            object lPosIds = DBNull.Value;
            if (settings.PosIds.Count > 0)
            {
                lPosIds = XmlHelper.GetXmlDescription(settings.PosIds);
            }

            object lWarehousesIds = DBNull.Value;
            if (settings.WarehousesIds.Count > 0)
            {
                lWarehousesIds = XmlHelper.GetXmlDescription(settings.WarehousesIds);
            }

            object lProductionDatesFrom = DBNull.Value;
            if (settings.ProductionDates.From.Date != DateTime.MinValue)
            {
                lProductionDatesFrom = settings.ProductionDates.From.Date;
            }

            object lProductionDatesTo = DBNull.Value;
            if (settings.ProductionDates.To.Date != DateTime.MinValue)
            {
                lProductionDatesTo = settings.ProductionDates.To.Date;
            }

            object lRepairDatesFrom = DBNull.Value;
            if (settings.RepairDates.From.Date != DateTime.MinValue)
            {
                lRepairDatesFrom = settings.RepairDates.From.Date;
            }

            object lRepairDatesTo = DBNull.Value;
            if (settings.RepairDates.To.Date != DateTime.MinValue)
            {
                lRepairDatesTo = settings.RepairDates.To.Date;
            }

            object lShowRepeatedApp = DBNull.Value;
            if (settings.ShowRepeatedApp)
            {
                lShowRepeatedApp = settings.ShowRepeatedApp;
            }

            object lShowUninstalationApp = DBNull.Value;
            if (settings.ShowUninstalationApp)
            {
                lShowUninstalationApp = settings.ShowUninstalationApp;
            }

            object lShowMiscallApp = DBNull.Value;
            if (settings.ShowMiscallApp)
            {
                lShowMiscallApp = settings.ShowMiscallApp;
            }
            
            DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_REPORT_DATA,
                      new SqlParameter(Constants.SP_GET_REPORT_DateStart, settings.ReportDates.From.Date),
                      new SqlParameter(Constants.SP_GET_REPORT_DateEnd, settings.ReportDates.To.Date),
                      new SqlParameter(Constants.SP_GET_REPORT_StaffList, XmlHelper.GetXmlDescription(settings.StaffingsIds)),
                      new SqlParameter(Constants.SP_GET_REPORT_Regions, XmlHelper.GetXmlDescription(settings.RegionIds)),
                      new SqlParameter(Constants.SP_GET_REPORT_Customers, lPosIds),
                      new SqlParameter(Constants.SP_GET_REPORT_Warehouses, lWarehousesIds),
                      new SqlParameter(Constants.SP_GET_REPORT_ServiceCentres, XmlHelper.GetXmlDescription(settings.ServiceCentersIds)),
                      new SqlParameter(Constants.SP_GET_REPORT_RepairStatuses, XmlHelper.GetXmlDescription(settings.ApplicationStatusesIds)),
                      new SqlParameter(Constants.SP_GET_REPORT_Models, XmlHelper.GetXmlDescription(settings.ModelsIds)),
                      new SqlParameter(Constants.SP_GET_REPORT_Brands, XmlHelper.GetXmlDescription(settings.BrandsIds)),
                      new SqlParameter(Constants.SP_GET_REPORT_Manufacturers, XmlHelper.GetXmlDescription(settings.ManufacturersIds)),
                      new SqlParameter(Constants.SP_GET_REPORT_IsRepeated, lShowRepeatedApp),
                      new SqlParameter(Constants.SP_GET_REPORT_IsDiagnostic, lShowUninstalationApp),
                      new SqlParameter(Constants.SP_GET_REPORT_IsFalseAlarm, lShowMiscallApp),
                      new SqlParameter(Constants.SP_GET_REPORT_DateProductionFrom, lProductionDatesFrom),
                      new SqlParameter(Constants.SP_GET_REPORT_DateProductionTo, lProductionDatesTo),
                      new SqlParameter(Constants.SP_GET_REPORT_RecoveryDateFrom, lRepairDatesFrom),
                      new SqlParameter(Constants.SP_GET_REPORT_RecoveryDateTo, lRepairDatesTo),
                      new SqlParameter(Constants.SP_GET_REPORT_ShowRepairWorksDetails, settings.DetalizationByWorks),
                      new SqlParameter(Constants.SP_GET_REPORT_ShowSparePartsDetails, settings.DetalizationByElements),
                      new SqlParameter(Constants.SP_GET_REPORT_ShowBreakagesDetails, settings.DetalizationByBreaks),
                      new SqlParameter(Constants.SP_GET_REPORT_ShowBreakageTypesDetails, settings.DetalizationByBreaksType)
                      );

            if (lDataSet != null)
            {
                lDataTable = lDataSet.Tables[0];
            }
            
            DataView lDataView = lDataTable.DefaultView;
            lDataView.Sort = "POSRepairs_ID ASC, RepairCost ASC";

            return lDataTable;
        }
    }
}
