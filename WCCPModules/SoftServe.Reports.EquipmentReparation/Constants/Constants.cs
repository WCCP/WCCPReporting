﻿namespace SoftServe.Reports.EquipmentReparation
{
    /// <summary>
    /// Stored procedures and their params names 
    /// </summary>
    public static class Constants
    {
        #region Settings Window

        public const string SP_TERRITORY_TREE = "spDW_GetTerritoryTree";
        public const string SP_TERRITORY_TREE_PARAM_1 = "LevelFrom";
        public const string SP_TERRITORY_TREE_PARAM_2 = "LevelTo";

        public const string SP_STAFFING_TREE = "spDW_GetHistoryStaff";
        public const string SP_STAFFING_TREE_PARAM_1 = "Date";

        public const string SP_SERVICE_CENTERS_TREE = "Pos.spDW_GetServiceCentresTree";
        public const string SP_SERVICE_CENTERS_TREE_PARAM_1 = "LevelFrom";
        public const string SP_SERVICE_CENTERS_TREE_PARAM_2 = "LevelTo";

        public const string SP_DISTR_TYPES_LIST = "Pos.spDW_GetWorkSchemas";


        public const string SP_CUSTOMERS_TREE = "spDW_GetCustomersTree";

        public const string SP_DISTRIBUTORS_TREE = "spDW_GetDistributorsTree";
        public const string SP_DISTRIBUTORS_TREE_PARAM_1 = "LevelFrom";
        public const string SP_DISTRIBUTORS_TREE_PARAM_2 = "LevelTo";
        
        public const string SP_WAREHOUSES_TREE = "Pos.spDW_GetWarehouseLocationTree";

        public const string SP_WAREHOUSES_LIST = "Pos.spDW_GetWarehousesTree";

        public const string SP_APPLICATION_STATUSES_LIST = "Pos.spDW_GetRepairStatuses";
        
        public const string SP_MODELS_LIST = "Pos.spDW_GetGroupsWithModels";
        public const string SP_BRANDS_LIST = "Pos.spDW_GetBrands";

        public const string SP_MANUFACTURERS_LIST = "Pos.spDW_GetListOfManufactures";

        #endregion Settings Window

        #region Main report tab

        public const string SP_GET_REPORT_DATA = "Pos.spDW_GetRepairs";

        public const string SP_GET_REPORT_DateStart = "DateStart";
        public const string SP_GET_REPORT_DateEnd = "DateEnd";
        public const string SP_GET_REPORT_StaffList = "Staff";
        public const string SP_GET_REPORT_Regions = "Regions";
        public const string SP_GET_REPORT_Customers = "Customer";
        public const string SP_GET_REPORT_Warehouses = "Warehouse";
        public const string SP_GET_REPORT_ServiceCentres = "ServicesCentres";
        public const string SP_GET_REPORT_RepairStatuses = "RepairStatus";
        public const string SP_GET_REPORT_Models = "Model";
        public const string SP_GET_REPORT_Brands = "Brand";
        public const string SP_GET_REPORT_Manufacturers = "Manufacturer";
        public const string SP_GET_REPORT_IsRepeated = "IsRepeated";
        public const string SP_GET_REPORT_IsDiagnostic = "IsDiagnostic";
        public const string SP_GET_REPORT_IsFalseAlarm = "IsFalseAlarm";
        public const string SP_GET_REPORT_DateProductionFrom = "DateProductionFrom";
        public const string SP_GET_REPORT_DateProductionTo = "DateProductionTo";
        public const string SP_GET_REPORT_RecoveryDateFrom = "RecoveryDateFrom";
        public const string SP_GET_REPORT_RecoveryDateTo = "RecoveryDateTo";
        public const string SP_GET_REPORT_ShowRepairWorksDetails = "ShowRepairWorksDetails";
        public const string SP_GET_REPORT_ShowSparePartsDetails = "ShowSparePartsDetails";
        public const string SP_GET_REPORT_ShowBreakagesDetails = "ShowBreakagesDetails";
        public const string SP_GET_REPORT_ShowBreakageTypesDetails = "ShowBreakageTypesDetails";

        #endregion Main report tab
    }
}
