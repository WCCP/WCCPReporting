﻿namespace POCE.Reports.M1M2.DataAccess
{
    static class SqlConstants
    {


        public const string READ_DATE =
            @"IF Not EXISTS (
        SELECT ParamValue
        FROM   DW_OffTrade_ReportSettings
        WHERE  ReportName    = 'POCE Pivot OFF'
               AND ParamName = 'LastDataUpdate'
       )
Select NULL
Else
        SELECT  ParamValue
        FROM   DW_OffTrade_ReportSettings
        WHERE  ReportName    = 'POCE Pivot OFF'
               AND ParamName = 'LastDataUpdate'";



        public const string SpRecalculateData = "dbo.spSW_ReCalculateData";
        public const string SpRecalculateDataParam1 = "RecalcDate";
        public const string SpRecalculateDataParam2 = "monthCount";

        public const string IS_REPORT_EXECUTION_ALLOWED = "[POCE].[Check_Run_POCE_Reports]";

        public const string GET_LIST_M = "spDW_M_OffTrade_OR_GetListM";
        public const string GET_LIST_M_PRM_TYPE = "MType";
        public const string GET_LIST_M_PRM_ID = "SPV_id";
        public const string GET_LIST_M_VAL_TYPE_M1 = "M1";
        public const string GET_LIST_M_VAL_TYPE_M2 = "M2";

        public const string GET_LIST_M1_ID = "Merch_id";
        public const string GET_LIST_M1_NAME = "MerchName";

        public const string GET_LIST_M2_ID = "Supervisor_id";
        public const string GET_LIST_M2_NAME = "Supervisor_name";


        //public const string GET_ALL_M1 = "select * from tblMerchandisers";

        //public const string GET_ALL_M2 = "spDW_SupervisorsGetList";
        //public const string GET_ALL_M2_PARAM_1 = "ReportId";
        //public const string GET_ALL_M2_FLD_SUPERVISOR_ID = "Supervisor_id";
        //public const string GET_ALL_M2_FLD_SUPERVISOR_NAME = "Supervisor_name";

        //public const string GET_MERCH = "spDW_M1M2_getMerch";
        //public const string GET_MERCH_PARAM_1 = "@Supervisor_id";
        //public const string GET_MERCH_PARAM_2 = "@entityType";
        //public const string GET_MERCH_FLD_MERCH_ID = "Merch_id";
        //public const string GET_MERCH_FLD_MERCH_NAME = "MerchName";

        public const string GET_WORKING_DAYS = "spDW_WorkingDays";
        public const string GET_WORKING_DAYS_PARAM_1 = "Date";
        public const string GET_WORKING_DAYS_FLD_CURRENT_DATE = "CurrentDate";
        public const string GET_WORKING_DAYS_FLD_WORK_DAYS_IN_MONTH = "WorkDaysInMonth";
        public const string GET_WORKING_DAYS_FLD_WORK_DAY = "WorkDay";
        public const string WorkDayOfTotalTextFieldName = "WorkDayOfTotalText";


        public const string GET_DISTRIBUTOR_NAME = "spDW_M_OffTrade_OR_GetCustName";
        public const string GET_DISTRIBUTOR_NAME_PRM_1 = "SPV_ID";
        public const string GET_DISTRIBUTOR_NAME_FLD_NAME = "Cust_NAME";
    }
}
