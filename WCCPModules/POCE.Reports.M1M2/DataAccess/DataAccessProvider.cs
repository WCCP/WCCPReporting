﻿using Logica.Reports.DataAccess;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization.Formatters;
using BLToolkit.Data;

namespace POCE.Reports.M1M2.DataAccess
{
    static class DataAccessProvider
    {
        private static DbManager _db;

        public static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 3600;//15 * 60;// 15 minutes by default
                }
                else if (!HostConfiguration.DBSqlConnection.Contains(_db.Connection.ConnectionString))
                {
                    _db.Close();
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 3600;//15 * 60;// 15 minutes by default
                }
                return _db;
            }
        }

        public static DataTable AllM2
        {
            get
            {
                DataTable res = Db.SetSpCommand(SqlConstants.GET_LIST_M,
                    Db.Parameter(SqlConstants.GET_LIST_M_PRM_TYPE, SqlConstants.GET_LIST_M_VAL_TYPE_M2),
                    Db.Parameter(SqlConstants.GET_LIST_M_PRM_ID, DBNull.Value)).ExecuteDataTable();

                return res;
            }
        }
 
        public static string DateDate
        {
            get
            {
                Object o = Db.SetCommand(SqlConstants.READ_DATE).ExecuteScalar();
                if (o != null && o != DBNull.Value)
                {
                    return o.ToString();
                }
                return null;
            }
        }
 
        public static void RecalculateData(DateTime date, int monthCount = 1)
        {
            try
            {
                Db.SetSpCommand(SqlConstants.SpRecalculateData,
                    Db.Parameter(SqlConstants.SpRecalculateDataParam1, date),
                    Db.Parameter(SqlConstants.SpRecalculateDataParam2, monthCount))
                    .ExecuteNonQuery();
            }
            catch (Exception lException)
            {
                if (lException.InnerException != null && lException.InnerException is SqlException)
                {
                    var lSqlException = (SqlException) lException.InnerException;

                    if (lSqlException.State == 100 && lSqlException.Class == 15)
                    {
                        throw new ApplicationException(lSqlException.Message, lSqlException);
                    }
                    throw;
                }
            }
        }

        internal static string IsExecutionAllowed()
            {
                string result = null;

                try
                {
                    Db.SetSpCommand(SqlConstants.IS_REPORT_EXECUTION_ALLOWED).ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null
                        && ex.InnerException is SqlException
                        && (((SqlException)ex.InnerException).State == 100
                            && ((SqlException)ex.InnerException).Class == 15))
                    {
                        result = ex.InnerException.Message;
                    }
                    else
                    {
                        throw;
                    }
                }
                return result;
            }
  
        public static DataTable GetM1ByM2(object supervisorId)
        {
            DataTable res = Db.SetSpCommand(SqlConstants.GET_LIST_M,
                Db.Parameter(SqlConstants.GET_LIST_M_PRM_TYPE, SqlConstants.GET_LIST_M_VAL_TYPE_M1),
                Db.Parameter(SqlConstants.GET_LIST_M_PRM_ID, supervisorId)).ExecuteDataTable();
            return res;
        }

        public static string GetDistributorName(int supervisorId)
        {
            string distributorName = (string) Db.SetSpCommand(SqlConstants.GET_DISTRIBUTOR_NAME,
                Db.Parameter(SqlConstants.GET_DISTRIBUTOR_NAME_PRM_1, supervisorId)).ExecuteScalar();

            return distributorName;
        }
        
        public static DataTable GetWorkingDays(DateTime date)
        {
                return GetTable(SqlConstants.GET_WORKING_DAYS, SqlConstants.GET_WORKING_DAYS_PARAM_1, date);
        }
        
        private static DataTable GetTable(string procName, string sqlParam, object value)
        {
            DataTable res = Db.SetSpCommand(procName, Db.Parameter(sqlParam, value)).ExecuteDataTable();
            return res;
        }

        private static DataTable GetTable(string procName, SqlParameter[] parameters)
        {
            DataTable dt = Db.SetSpCommand(procName, parameters).ExecuteDataTable();
            return dt;
        }
    }
}
