﻿namespace POCE.Reports.M1M2
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.imCollection = new DevExpress.Utils.ImageCollection();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.lbDate = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cmb1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.lblM2 = new DevExpress.XtraEditors.LabelControl();
            this.rbType = new DevExpress.XtraEditors.RadioGroup();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditM2 = new DevExpress.XtraEditors.LookUpEdit();
            this.grpUpd = new DevExpress.XtraEditors.GroupControl();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.lblUpd = new DevExpress.XtraEditors.LabelControl();
            this.btn_upd = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditM2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpUpd)).BeginInit();
            this.grpUpd.SuspendLayout();
            this.SuspendLayout();
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = null;
            this.dateEdit.Location = new System.Drawing.Point(53, 77);
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit.Size = new System.Drawing.Size(100, 20);
            this.dateEdit.TabIndex = 5;
            // 
            // lbDate
            // 
            this.lbDate.Location = new System.Drawing.Point(7, 80);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(26, 13);
            this.lbDate.TabIndex = 4;
            this.lbDate.Text = "Дата";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl2.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl2.Appearance.Options.UseBackColor = true;
            this.groupControl2.Controls.Add(this.cmb1);
            this.groupControl2.Location = new System.Drawing.Point(7, 170);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(269, 176);
            this.groupControl2.TabIndex = 16;
            this.groupControl2.Text = "Список M1, которые будут включены в отчет";
            // 
            // cmb1
            // 
            this.cmb1.CheckOnClick = true;
            this.cmb1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmb1.Location = new System.Drawing.Point(2, 21);
            this.cmb1.Name = "cmb1";
            this.cmb1.Size = new System.Drawing.Size(265, 153);
            this.cmb1.TabIndex = 0;
            // 
            // lblM2
            // 
            this.lblM2.Location = new System.Drawing.Point(7, 106);
            this.lblM2.Name = "lblM2";
            this.lblM2.Size = new System.Drawing.Size(40, 13);
            this.lblM2.TabIndex = 9;
            this.lblM2.Text = "Имя М2:";
            // 
            // rbType
            // 
            this.rbType.Location = new System.Drawing.Point(7, 129);
            this.rbType.Name = "rbType";
            this.rbType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Ввод планов"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Получение фактов")});
            this.rbType.Size = new System.Drawing.Size(269, 35);
            this.rbType.TabIndex = 10;
            this.rbType.SelectedIndexChanged += new System.EventHandler(this.rbType_SelectedIndexChanged);
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(57, 352);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(139, 25);
            this.btnYes.TabIndex = 19;
            this.btnYes.Text = "&Сгенерировать отчет";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(201, 352);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 20;
            this.btnNo.Text = "&Отмена";
            this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
            // 
            // lookUpEditM2
            // 
            this.lookUpEditM2.Location = new System.Drawing.Point(53, 103);
            this.lookUpEditM2.Name = "lookUpEditM2";
            this.lookUpEditM2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditM2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_id", "Supervisor_id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "Supervisor_name")});
            this.lookUpEditM2.Properties.DisplayMember = "Supervisor_name";
            this.lookUpEditM2.Properties.ShowFooter = false;
            this.lookUpEditM2.Properties.ShowHeader = false;
            this.lookUpEditM2.Properties.ValueMember = "Supervisor_id";
            this.lookUpEditM2.Size = new System.Drawing.Size(223, 20);
            this.lookUpEditM2.TabIndex = 21;
            this.lookUpEditM2.EditValueChanged += new System.EventHandler(this.lookUpEditM2_EditValueChanged);
            // 
            // grpUpd
            // 
            this.grpUpd.Controls.Add(this.lblUpd2);
            this.grpUpd.Controls.Add(this.lblUpd);
            this.grpUpd.Controls.Add(this.btn_upd);
            this.grpUpd.Location = new System.Drawing.Point(7, 8);
            this.grpUpd.Name = "grpUpd";
            this.grpUpd.Size = new System.Drawing.Size(269, 63);
            this.grpUpd.TabIndex = 24;
            this.grpUpd.Text = "Данные";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Location = new System.Drawing.Point(5, 45);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(113, 13);
            this.lblUpd2.TabIndex = 8;
            this.lblUpd2.Text = "12.11.2011 15:25:31";
            // 
            // lblUpd
            // 
            this.lblUpd.Location = new System.Drawing.Point(5, 25);
            this.lblUpd.Name = "lblUpd";
            this.lblUpd.Size = new System.Drawing.Size(59, 13);
            this.lblUpd.TabIndex = 7;
            this.lblUpd.Text = "Данные на:";
            // 
            // btn_upd
            // 
            this.btn_upd.ImageIndex = 0;
            this.btn_upd.ImageList = this.imCollection;
            this.btn_upd.Location = new System.Drawing.Point(164, 25);
            this.btn_upd.Name = "btn_upd";
            this.btn_upd.Size = new System.Drawing.Size(100, 33);
            this.btn_upd.TabIndex = 6;
            this.btn_upd.Text = " Обновить ";
            this.btn_upd.Click += new System.EventHandler(this.btn_upd_Click);
            // 
            // SettingsForm
            // 
            this.ClientSize = new System.Drawing.Size(281, 387);
            this.Controls.Add(this.grpUpd);
            this.Controls.Add(this.lookUpEditM2);
            this.Controls.Add(this.rbType);
            this.Controls.Add(this.dateEdit);
            this.Controls.Add(this.lbDate);
            this.Controls.Add(this.lblM2);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.groupControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "M1-M2 Отчет мерчендайзеров";
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditM2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpUpd)).EndInit();
            this.grpUpd.ResumeLayout(false);
            this.grpUpd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.LabelControl lbDate;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl lblM2;
        private DevExpress.XtraEditors.RadioGroup rbType;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.CheckedListBoxControl cmb1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditM2;
        private DevExpress.XtraEditors.GroupControl grpUpd;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraEditors.LabelControl lblUpd;
        private DevExpress.XtraEditors.SimpleButton btn_upd;
  
    }
}