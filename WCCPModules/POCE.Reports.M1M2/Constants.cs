﻿using System;

namespace POCE.Reports.M1M2
{
  static class Constants
  {
    internal static readonly Guid PlanTabId = new Guid("7e4e2e6a-f693-461f-875b-0e4759a6e046");
    internal static readonly Guid M1AnTabId = new Guid("0016c61f-3389-4a39-938c-b177cd4ea23a");
    internal static readonly Guid M1DaTabId = new Guid("ca4d6c9e-356e-420d-a9cc-5cb1e222b8bf");
    internal static readonly Guid M2Personal = new Guid("03982ef9-5a2c-4410-893e-0e62a86b67de");
    internal static readonly Guid M2WorstTT = new Guid("545db904-d2c7-4b76-a7e1-e653c73b1744");
    internal static readonly Guid M2Raiting = new Guid("7261df02-deab-4df8-bc7e-adfc88186860");

    internal const string M2_PERSONAL_FLD_ROW_NUM = "RowNum";
  }
}
