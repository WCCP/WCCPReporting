﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.GridExtension;
using Logica.Reports.ConfigXmlParser.Model;
using POCE.Reports.M1M2;
using POCE.Reports.M1M2.Properties;

#endregion

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        #region Readonly & Static Fields

        public static WccpUIControl ReportControl;

        private static List<SettingsForm> forms;

        #endregion

        #region Fields
        private List<long> worstTT = new List<long>();
        private Dictionary<Guid, BandedGridView> m1AnViews = new Dictionary<Guid, BandedGridView>();
        private Dictionary<Guid, BandedGridView> m1DaViews = new Dictionary<Guid, BandedGridView>();
        private SettingsForm setForm;

        private BandedGridView viewWithMergedCells;

        #endregion

        #region Constructors

        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            PrintingMarginOffest = 60; //set margins to 5 mm.
            Load += WccpUIControl_Load;

            RefreshClick += WccpUIControl_RefreshClick;
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            MenuCloseClick += WccpUIControl_CloseClick;
            //PrepareExport += WccpUIControl_PrepareExport;
            ReportControl = this;
        }

        #endregion

        #region Instance Properties

        private BaseTab M2WorstTab
        {
            get
            {
                return
                    TabControl.TabPages.OfType<BaseTab>().FirstOrDefault(t => t.SheetStructure.Id == Constants.M2WorstTT);
            }
        }

        #endregion

        #region Instance Methods

        public bool HasUnsavedData()
        {
            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified && page.SheetSettings.TableDataType == TableType.Plan)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsNeedFitTabToOnePage()
        {
            return true;
        }

        public bool SaveUnsavedData()
        {
            bool result = true;

            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified && page.SheetSettings.TableDataType == TableType.Plan)
                    {
                        if (!page.SaveChanges())
                        {
                            result = false;
                        }
                    }
                }
            }

            return result;
        }

        protected override void OnControlRemoved(ControlEventArgs e)
        {
            forms.Remove(setForm);
            base.OnControlRemoved(e);
        }

        private void AdjustM1AnalyzisTab(BaseTab baseTabPage)
        {
            if (baseTabPage.Length > 0)
            {
                BaseGridControl lGrid1 = baseTabPage.GetGrid(0);
                int lGridHeight1 = ((DataTable)lGrid1.GridControl.DataSource).Rows.Count * 20 + 80;
                lGrid1.MaximumSize = new Size(740, lGridHeight1);
                if (baseTabPage.Length > 1)
                {
                    BaseGridControl lGrid2 = baseTabPage.GetGrid(1);
                    int lGridHeight2 = ((DataTable)lGrid2.GridControl.DataSource).Rows.Count * 20 + 320;
                    //Sometimes control don't want to obtain new height and keep old one.
                    lGrid2.Height = lGridHeight2;
                    lGrid2.MinimumSize = new Size(0, lGridHeight2);
                    lGrid2.MaximumSize = new Size(0, lGridHeight2);
                    GroupBandedView lGbv = (GroupBandedView)lGrid2.GridControl.DefaultView;

                    var lRepositoryItemMemoEdit = new RepositoryItemMemoEdit();
                    lRepositoryItemMemoEdit.BeginInit();
                    lGrid2.GridControl.RepositoryItems.Add(lRepositoryItemMemoEdit);
                    lRepositoryItemMemoEdit.EndInit();
                    for (int lIndex = 0; lIndex < lGbv.Columns.Count; lIndex++)
                    {
                        var lColumn = lGbv.Columns[lIndex];
                        if (lColumn.Visible && (lColumn.FieldName.Equals("ФактимяТТ") || lColumn.FieldName.Equals("ФактадресТТ")))
                        {
                            lColumn.ColumnEdit = lRepositoryItemMemoEdit;
                        }
                    }
                    
                    lGbv.CustomColumnDisplayText -= lGbv.GroupBandedView_CustomColumnDisplayText;
                    lGbv.CustomColumnDisplayText += AnalysisPage_CustomColumnDisplayText;
                    lGbv.RowCellStyle += AnalysisPage_RowCellStyle;
                    m1AnViews.Add(baseTabPage.SheetStructure.Id, lGbv);
                }
            }

            baseTabPage.Refresh();
        }

        private void AdjustM1DailikTab(BaseTab baseTabPage)
        {
            if (baseTabPage.Length > 0)
            {
                var grid = baseTabPage.GetGrid(0);
                int gridHeight = ((DataTable)grid.GridControl.DataSource).Rows.Count * 20 + 435;
                //Sometimes control don't want to obtain new height and keep old one.
                grid.Height = gridHeight;
                grid.MinimumSize = new Size(0, gridHeight);
                grid.MaximumSize = new Size(0, gridHeight);
                var bgv = (BandedGridView)grid.GridControl.DefaultView;

                var repositoryItemMemoEdit = new RepositoryItemMemoEdit();
                repositoryItemMemoEdit.BeginInit();
                grid.GridControl.RepositoryItems.Add(repositoryItemMemoEdit);
                repositoryItemMemoEdit.EndInit();
                for (int i = 0; i < bgv.Columns.Count; i++)
                {
                    var column = bgv.Columns[i];
                    if (column.Visible && (column.FieldName.Equals("Название") || column.FieldName.Equals("Адрес")))
                    {
                        column.ColumnEdit = repositoryItemMemoEdit;
                    }
                }

                bgv.AppearancePrint.Lines.BackColor = Color.Black;
                bgv.AppearancePrint.Lines.BackColor2 = Color.Black;
                bgv.Appearance.HorzLine.BackColor = Color.Black;
                bgv.Appearance.HorzLine.BackColor2 = Color.Black;
                bgv.Appearance.VertLine.BackColor = Color.Black;
                bgv.Appearance.VertLine.BackColor2 = Color.Black;

                //Add additional events
                bgv.CustomColumnDisplayText += DailikPage_CustomColumnDisplayText;
                bgv.RowCellStyle += DailikPage_RowCellStyle;
                var dataTable = (DataTable)grid.GridControl.DataSource;

                var name43 = "KPI_Percent_1";
                var name44 = "KPI_Percent_2"; ;
                if (dataTable.Columns.Contains(name43) && dataTable.Columns.Contains(name44) && dataTable.Rows.Count > 0)
                {
                    var col43 = bgv.Columns.Cast<BandedGridColumn>().Where(c => c.FieldName == "43").FirstOrDefault();
                    if (col43 != null)
                    {
                        var captions = dataTable.Rows[0][name43].ToString().Split('_');
                        col43.OwnerBand.Caption = captions[0];
                        col43.OwnerBand.ParentBand.Caption = captions[1];
                    }

                    var col44 = bgv.Columns.Cast<BandedGridColumn>().Where(c => c.FieldName == "44").FirstOrDefault();
                    if (col44 != null)
                    {
                        var captions = dataTable.Rows[0][name44].ToString().Split('_');
                        col44.OwnerBand.Caption = captions[0];
                        col44.OwnerBand.ParentBand.Caption = captions[1];
                    }
                }

                m1DaViews.Add(baseTabPage.SheetStructure.Id, bgv);
                baseTabPage.Refresh();
            }
        }

        private void AdjustM2RaitingTab(BaseTab baseTab)
        {
            if (baseTab.Length <= 0) return;

            SheetParam sheetParam = baseTab.SheetSettings.FirstOrDefault(s => s.SqlParamName.Equals(Resources.DateSql));
            if (sheetParam == null) return;

            baseTab.GetGrid(0).ChartControl.Titles[0].Text =
                String.Format(baseTab.GetGrid(0).ChartControl.Titles[0].Text, sheetParam.DisplayValue);
        }

        private void AdjustM2WorstsTab(BaseTab baseTabPage)
        {
            if (baseTabPage.Length > 0)
            {
                var gridControl = baseTabPage.GetGrid(0).GridControl;
                GroupBandedView gbv = (GroupBandedView)gridControl.DefaultView;
                gbv.CustomColumnDisplayText -= gbv.GroupBandedView_CustomColumnDisplayText;
                worstTT.Clear();
                const string worstField = "Код ТТ";
                worstTT.AddRange(((DataTable)gridControl.DataSource).AsEnumerable().Select(r => ConvertEx.ToLongInt(r[worstField])));
            }
        }

        private void AdjustTabForScrolls(BaseTab baseTabPage)
        {
            int minWidth = 0;
            for (int i = 0; i < baseTabPage.Length; i++)
            {
                GridView view = (GridView)baseTabPage.GetGrid(i).GridControl.DefaultView;
                if (!view.OptionsView.ColumnAutoWidth)
                {
                    int width = 0;
                    foreach (GridColumn column in view.VisibleColumns)
                    {
                        width += column.Width;
                    }
                    if (width > minWidth)
                        minWidth = width;
                }
            }
            if (minWidth > 0)
            {
                baseTabPage.AutoScroll = true;
                baseTabPage.AutoScrollMinSize = new Size(minWidth + 4, 0);
            }
        }

        private void AttachCellMergeEvent()
        {
            XtraTabPage[] xtraTabPages =
                tabManager.TabPages.Where(
                    page => (page is BaseTab) && ((BaseTab)page).SheetStructure.Id == Constants.M2Personal).ToArray();
            if (xtraTabPages.Length == 0)
            {
                return;
            }

            BaseTab m2PersonalTab = (BaseTab)xtraTabPages[0];
            if (m2PersonalTab.Length == 1)
            {
                viewWithMergedCells = (BandedGridView)m2PersonalTab.GetGrid(0).GridControl.DefaultView;
                viewWithMergedCells.CellMerge += PersonalPage_CellMerge;
                viewWithMergedCells.RefreshData();
            }
        }

        private void CustomSettings()
        {
            cmbTab.Visible = true;
            lblTab.Visible = true;
            //btnPrintAll.ItemClick -= btnPrintAll_ItemClick;
            //btnPrintAll.ItemClick += btnPrintAll_ItemClick_M1M2;
            cmbTab.SelectedIndexChanged += SelectedIndexChanged;

            FillComboBox();
        }

        private void FillComboBox()
        {
            cmbTab.Properties.Items.Clear();
            List<XtraTabPage> tmpLst = new List<XtraTabPage>(tabManager.TabPages);
            foreach (XtraTabPage page in tmpLst)
            {
                cmbTab.Properties.Items.Add(new ComboBoxItem { Id = page, Text = page.Text });
            }
            //set first Tab name as combobox Text.
            try
            {
                cmbTab.Text = ((ComboBoxItem)cmbTab.Properties.Items[0]).Text;
            }
            catch (IndexOutOfRangeException)
            {
            }
        }

        private SettingsForm GetForm()
        {
            if (forms != null && forms.Count > 0)
                return forms[forms.Count - 1];
            return null;
        }

        private void PopulateAdditionalPages()
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
                BaseTab baseTabPage = (tabManager.TabPages[tabindex] as BaseTab);
                if (baseTabPage == null) continue;
                if (setForm.M1AnTabs.IndexOf(baseTabPage.SheetStructure.Id) > -1)
                    AdjustM1AnalyzisTab(baseTabPage);
                else if (setForm.M1DaTabs.IndexOf(baseTabPage.SheetStructure.Id) > -1)
                    AdjustM1DailikTab(baseTabPage);
                else if (baseTabPage.SheetStructure.Id == Constants.M2WorstTT)
                    AdjustM2WorstsTab(baseTabPage);
                else if (baseTabPage.SheetStructure.Id == Constants.M2Raiting)
                    AdjustM2RaitingTab(baseTabPage);
                AdjustTabForScrolls(baseTabPage);
                baseTabPage.IsNeedFitToPage = IsNeedFitTabToOnePage;
                baseTabPage.AlwaysScrollActiveControlIntoView = false;
            }
        }

        #endregion

        #region Event Handling

        private void AnalysisPage_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            BandedGridView bgv = (BandedGridView)sender;
            const string factTTName = "ФактимяТТ";
            const string fldComment = "Комментарии";
            bool isSummaryRow = bgv.GetListSourceRowCellValue(e.ListSourceRowIndex, factTTName).ToString().StartsWith("Итого по маршруту");
            bool isContainValue = e.Value != null && !String.IsNullOrEmpty(e.Value.ToString());
            bool isPercentRequired = e.Column.AbsoluteIndex > 9 &&
                                     !e.Column.FieldName.EndsWith(".Фактзапоследние4недели.Кол-вовизитов") &&
                                     !e.Column.FieldName.StartsWith("Action_");
            if (isSummaryRow && isPercentRequired)
            {
                if (isContainValue)
                {
                    e.DisplayText = string.Format("{0:N2}%", e.Value);
                }
                else
                {
                    e.DisplayText = "0.00%";
                }
            }
            if (isSummaryRow && (isContainValue && e.Column.AbsoluteIndex <= 6 && !e.Column.FieldName.Equals(factTTName)
                                 || e.Column.FieldName.StartsWith("ХО.ХО")) || e.Column.FieldName.Equals(fldComment))
            {
                e.DisplayText = String.Empty;
            }
        }

        private void AnalysisPage_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            string analysisField = "КодТТ";

            if (e.Column.FieldName != analysisField) return;
            var currentTT = ConvertEx.ToLongInt(e.CellValue);
            if (worstTT.Contains(currentTT))
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void DailikPage_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (!IsColorColumn(e.Column)) return;
            BandedGridView bandedGridView = (BandedGridView)sender;
            GridControl gridControl = bandedGridView.GridControl;
            DataRow row = ((DataTable)gridControl.DataSource).Rows[e.ListSourceRowIndex];
            if (row.IsNull(e.Column.FieldName)) row[e.Column.FieldName] = 1;
            e.DisplayText = "";
        }

        private void DailikPage_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (!IsColorColumn(e.Column)) return;
            BandedGridView bandedGridView = (BandedGridView)sender;
            GridControl gridControl = bandedGridView.GridControl;

            DataRow row = ((DataTable)gridControl.DataSource).Rows[bandedGridView.GetDataSourceRowIndex(e.RowHandle)];
            if (row.IsNull(e.Column.FieldName)) row[e.Column.FieldName] = 1;
            if (row.Field<int>(e.Column.FieldName) > 0)
            {
                e.Appearance.BackColor = Color.Gainsboro;
                e.Appearance.BackColor2 = Color.Gainsboro;
            }
        }

        private void PersonalPage_CellMerge(object sender, CellMergeEventArgs e)
        {
            BandedGridView view = (BandedGridView)sender;
            //В DX есть веселый баг. При экспорте если мы хендлим мержинг ячеек он их рисует несмерженными. в итоге нам надо хендлить те ячейки которые не должны мержится, а то что должно - то смержится само по себе.
            bool isMerge =
                view.GetDataRow(e.RowHandle1)[Constants.M2_PERSONAL_FLD_ROW_NUM].Equals(
                    view.GetDataRow(e.RowHandle2)[Constants.M2_PERSONAL_FLD_ROW_NUM]);
            e.Merge = isMerge;
            e.Handled = !isMerge;
        }

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbTab.Text.Equals(Resources.All))
                tabManager.SelectedTabPage = ((XtraTabPage)((ComboBoxItem)cmbTab.SelectedItem).Id);
        }

        private bool WccpUIControl_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage is BaseTab)
            {
                BaseTab page = selectedPage as BaseTab;
                if (page.DataModified && page.SheetSettings.TableDataType == TableType.Plan)
                {
                    DialogResult result = XtraMessageBox.Show(Resources.SaveData, Resources.DataNotSaved,
                                                              MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.OK)
                    {
                        if (page.SaveChanges())
                        {
                            XtraMessageBox.Show(Resources.DataSaved);
                        }
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void WccpUIControl_Load(object sender, EventArgs e)
        {
            if (GetForm() != null)
            {
                setForm = GetForm();
                setForm.SetParent(this);
                off_reports = true;
                UpdateSheets(setForm.SheetParamsList);
                PopulateAdditionalPages();
                AttachCellMergeEvent();
                CustomSettings();
            }
        }

        private void WccpUIControl_RefreshClick(object sender, XtraTabPage selectedpage)
        {
            BaseTab tab = selectedpage as BaseTab;
            if (tab == null) return;

            if (tab.SheetStructure.Id == Constants.M2Personal)
            {
                if (viewWithMergedCells != null)
                {
                    viewWithMergedCells.CellMerge -= PersonalPage_CellMerge;
                    viewWithMergedCells = null;
                }
                AttachCellMergeEvent();
            }
            else if (tab.SheetStructure.Id == Constants.M2WorstTT)
                AdjustM2WorstsTab(tab);
            else if (tab.SheetStructure.Id == Constants.M2Raiting)
                AdjustM2RaitingTab(tab);

            if (m1AnViews.ContainsKey(tab.SheetStructure.Id))
            {
                m1AnViews[tab.SheetStructure.Id].CustomColumnDisplayText -= AnalysisPage_CustomColumnDisplayText;
                m1AnViews[tab.SheetStructure.Id].RowCellStyle -= AnalysisPage_RowCellStyle;
                m1AnViews.Remove(tab.SheetStructure.Id);
                AdjustM1AnalyzisTab(tab);
            }
            if (m1DaViews.ContainsKey(tab.SheetStructure.Id))
            {
                m1DaViews[tab.SheetStructure.Id].CustomColumnDisplayText -= DailikPage_CustomColumnDisplayText;
                m1DaViews[tab.SheetStructure.Id].RowCellStyle -= DailikPage_RowCellStyle;
                m1DaViews.Remove(tab.SheetStructure.Id);
                AdjustM1DailikTab(tab);
            }
            AdjustTabForScrolls(tab);
            tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
        }

        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            bool isShowSettings = true;
            if (HasUnsavedData())
            {
                DialogResult dialogResult = XtraMessageBox.Show(Resources.SaveBeforeModifySettings,
                                                                Resources.DataNotSaved, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes && ReportControl.SaveUnsavedData())
                {
                    XtraMessageBox.Show(Resources.DataSaved);
                }
                if (dialogResult == DialogResult.Cancel)
                {
                    isShowSettings = false;
                }
            }

            if (isShowSettings && setForm.ShowDialog() == DialogResult.OK)
            {
                if (viewWithMergedCells != null)
                {
                    viewWithMergedCells.CellMerge -= PersonalPage_CellMerge;
                    viewWithMergedCells = null;
                }
                foreach (KeyValuePair<Guid, BandedGridView> keyValuePair in m1AnViews)
                {
                    keyValuePair.Value.CustomColumnDisplayText -= AnalysisPage_CustomColumnDisplayText;
                    keyValuePair.Value.RowCellStyle -= AnalysisPage_RowCellStyle;
                }
                m1AnViews.Clear();

                foreach (KeyValuePair<Guid, BandedGridView> keyValuePair in m1DaViews)
                {
                    keyValuePair.Value.CustomColumnDisplayText -= AnalysisPage_CustomColumnDisplayText;
                }
                m1DaViews.Clear();

                UpdateSheets(setForm.SheetParamsList);
                PopulateAdditionalPages();
                AttachCellMergeEvent();
                FillComboBox();
            }
        }

        #endregion

        #region Class Methods

        public static void AddForm(SettingsForm form)
        {
            if (forms == null) forms = new List<SettingsForm>();
            forms.Add(form);
        }

        private static bool IsColorColumn(GridColumn column)
        {
            int tryParseResult;
            return int.TryParse(column.FieldName, out tryParseResult);
        }

        #endregion
    }
}