﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.ConfigXmlParser.Model;
using POCE.Reports.M1M2.DataAccess;
using POCE.Reports.M1M2.Properties;
using Logica.Reports.DataAccess;

namespace POCE.Reports.M1M2
{
    public partial class SettingsForm : XtraForm
    {
        #region Readonly & Static Fields

        private readonly DataTable dtSuper;

        #endregion

        #region Fields

        private BaseReportUserControl bruc;
        private DataTable dataTableDate;
        private Tab[] tabs;

        #endregion

        #region Constructors

        public SettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            SetParent(parent);

            M1AnTabs = new List<Guid>();
            M1DaTabs = new List<Guid>();
            dtSuper = DataAccessProvider.AllM2;
            FillM2();
            dateEdit.DateTime = DateTime.Now;
            rbType.SelectedIndex = 1;
            GetUpdData();
            if (!DataAccessLayer.IsLDB)
            {
                grpUpd.Visible = false;
                ResizeForm();
            }
        }

        public SettingsForm()
        {
            InitializeComponent();

            M1AnTabs = new List<Guid>();
            M1DaTabs = new List<Guid>();
            dtSuper = DataAccessProvider.AllM2;
            FillM2();
            dateEdit.DateTime = DateTime.Now;
            rbType.SelectedIndex = 1;
            GetUpdData();
            if (!DataAccessLayer.IsLDB)
            {
                grpUpd.Visible = false;
                ResizeForm();
            }
        }

        #endregion

        #region Instance Properties

        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                bruc.tabManager.TabPages.Clear();
                //additionalPages = new List<DailikTabPage>();
                dataTableDate = DataAccessProvider.GetWorkingDays(dateEdit.DateTime);
                List<SheetParamCollection> list = new List<SheetParamCollection>();
                M1AnTabs.Clear();
                M1DaTabs.Clear();

                if (IsPlanSelected)
                {
                    bruc.Report.Tabs = tabs.Where(x => x.Id == Constants.PlanTabId).ToArray();
                    list.Add(FillParamCol(Constants.PlanTabId, "", 0));
                }
                else
                {
                    //Don't change order of tabs adding because in another order M2 tabs disappear
                    //add and configure M1 tabs 
                    bruc.Report.Tabs = tabs.Where(x => x.Id == Constants.M1AnTabId || x.Id == Constants.M1DaTabId).ToArray(); //&& 
                    foreach (CheckedListBoxItem item in cmb1.Items)
                    {
                        if (item.CheckState != CheckState.Checked) continue;
                        Dictionary<Guid, Guid> clonedTabs = bruc.CloneTabs(item.Description);
                        list.Add(FillParamCol(clonedTabs[Constants.M1AnTabId], item.Description, item.Value));
                        list.Add(FillParamCol(clonedTabs[Constants.M1DaTabId], item.Description, item.Value));
                        M1AnTabs.Add(clonedTabs[Constants.M1AnTabId]);
                        M1DaTabs.Add(clonedTabs[Constants.M1DaTabId]);
                    }
                    //add and configure M2 tabs
                    IEnumerable<Tab> m2Tabs = tabs.Where(x => x.Id != Constants.PlanTabId && x.Id != Constants.M1AnTabId && x.Id != Constants.M1DaTabId);
                    bruc.Report.Tabs = m2Tabs.Union(bruc.Report.Tabs.Where(x => x.Id != Constants.M1AnTabId && x.Id != Constants.M1DaTabId)).ToArray();
                    // using this expression we remove gaps in order for correct sorting
                    bruc.Report.Tabs = bruc.Report.Tabs.OrderBy(t => t.Order).Select((t, i) =>
                                                                      {
                                                                          t.Order = i + 1;
                                                                          return t;
                                                                      }).ToArray();
                    list.InsertRange(0, m2Tabs.Select(tab => FillParamCol(tab.Id, "", 0)));
                }
                return list;
            }
        }

        private bool IsPlanSelected
        {
            get { return rbType.SelectedIndex == 0 ? true : false; }
        }

        public List<Guid> M1DaTabs { get; private set; }

        public List<Guid> M1AnTabs { get; private set; }

        #endregion

        #region Instance Methods

        public void SetParent(BaseReportUserControl parent)
        {
            bruc = parent;
            tabs = parent.Report.Tabs;
        }

        private void AddM1(DataTable dtMerch)
        {
            cmb1.Items.Clear();
            foreach (DataRow dr in dtMerch.Rows)
            {
                cmb1.Items.Add(dr[SqlConstants.GET_LIST_M1_ID], dr[SqlConstants.GET_LIST_M1_NAME].ToString(), CheckState.Unchecked, true);
            }
        }

        private void CheckAllM1()
        {
            foreach (CheckedListBoxItem item in cmb1.Items)
            {
                item.CheckState = CheckState.Checked;
            }
        }

        private void FillM2()
        {
            lookUpEditM2.Properties.DataSource = dtSuper;
            if (dtSuper.Rows.Count > 0)
            {
                lookUpEditM2.EditValue = dtSuper.Rows[0][SqlConstants.GET_LIST_M2_ID];
            }
            //AddM1(DataAccessProvider.AllM1);
        }

        public SheetParamCollection FillParamCol(Guid id, string merchName, object merchId)
        {
            SheetParamCollection paramCollection = new SheetParamCollection
                                                     {
                                                         TabId = id,
                                                         TableDataType =
                                                           rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact
                                                     };
            paramCollection.Add(new SheetParam { SqlParamName = Resources.DateSql, Value = dateEdit.DateTime.Date, DisplayParamName = Resources.Date, DisplayValue = dateEdit.DateTime.Date.ToShortDateString() });
            if (id == Constants.PlanTabId)
            {

                paramCollection.MainHeader = String.Format(Resources.MainHeader_M2_Plan, lookUpEditM2.Properties.GetDisplayText(lookUpEditM2.EditValue));
                paramCollection.Add(new SheetParam { SqlParamName = Resources.SupervisorIdSql, Value = lookUpEditM2.EditValue });
                paramCollection.Add(new SheetParam { DisplayParamName = Resources.ReportMounth, DisplayValue = dateEdit.DateTime.Date.ToString("MMMM") });

                var left = ConvertEx.ToDecimal(dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_WORK_DAYS_IN_MONTH]) - ConvertEx.ToDecimal(dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_WORK_DAY]);
                string daysLeft = DaysToString(left);
                var workDays = DaysToString(ConvertEx.ToDecimal(dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_WORK_DAYS_IN_MONTH]));

                paramCollection.Add(new SheetParam
                                      {
                                          DisplayParamName = Resources.WorkDaysLeft,
                                          DisplayValue =
                                            String.Format(Resources.From,
                                                          daysLeft,
                                                          workDays)
                                      });
            }
            else
            {
                paramCollection.Add(new SheetParam
                                          {
                                              DisplayParamName = Resources.WorkDay,
                                              DisplayValue = dataTableDate.Rows[0][SqlConstants.WorkDayOfTotalTextFieldName].ToString()
                                                //String.Format(Resources.From, DaysToString(ConvertEx.ToDecimal(dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_WORK_DAY])),
                                                //              DaysToString(ConvertEx.ToDecimal(dataTableDate.Rows[0][SqlConstants.GET_WORKING_DAYS_FLD_WORK_DAYS_IN_MONTH])))
                                          });
                if (!String.IsNullOrEmpty(merchName))
                {
                    paramCollection.MainHeader = String.Format(Resources.MainHeader_M1, merchName, lookUpEditM2.Properties.GetDisplayText(lookUpEditM2.EditValue), merchName); //TODO: Последний заменить на ТП
                    paramCollection.Add(new SheetParam { SqlParamName = Resources.MerchIdSql, Value = merchId });

                }
                else
                {
                    paramCollection.MainHeader =
                        String.Format(
                            id == Constants.M2WorstTT ? Resources.MainHeader_M2_WorstTT : Resources.MainHeader_M2,
                            lookUpEditM2.Properties.GetDisplayText(lookUpEditM2.EditValue));
                    paramCollection.Add(new SheetParam { SqlParamName = Resources.SupervisorIdSql, Value = lookUpEditM2.EditValue });
                }
            }
            if (DataAccessLayer.IsLDB)
            {
                paramCollection.Add(new SheetParam
                {
                    DisplayParamName = Resources.Distributor,
                    DisplayValue = DataAccessProvider.GetDistributorName((int) lookUpEditM2.EditValue)
                });
            }
            return paramCollection;
        }

        private static string DaysToString(decimal left)
        {
            return left.ToString(left % 1 == 0 ? "N0" : "N1");
        }
        
        #endregion

        #region Event Handling

        private void btnNo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (rbType.SelectedIndex == 0 && dateEdit.DateTime < new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
            {
                XtraMessageBox.Show(Resources.Error_Date);
                return;
            }

            var executionAllowed = DataAccessProvider.IsExecutionAllowed();
            if (!string.IsNullOrEmpty(executionAllowed))
            {
                XtraMessageBox.Show(executionAllowed, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DialogResult = DialogResult.OK;
        }

        private void lookUpEditM2_EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpEditM2.EditValue != null)
            {
                AddM1(DataAccessProvider.GetM1ByM2(((int)lookUpEditM2.EditValue)));
            }
            CheckAllM1();
        }

        private void rbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmb1.Enabled = !IsPlanSelected;
            CheckAllM1();
            lbDate.Visible = dateEdit.Visible = true;
        }

        #endregion

        private void btn_upd_Click(object sender, EventArgs e) {
            WaitManager.StartWait();
            try {
                try {
                    DataAccessProvider.RecalculateData(dateEdit.DateTime.Date);
                }
                catch (Exception ex) {
                    XtraMessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } finally {
                WaitManager.StopWait();
            }
            GetUpdData();
        }

        private void GetUpdData()
        {
            lblUpd2.Text = (DataAccessProvider.DateDate ?? "нет данных");
        }

        private void ResizeForm()
        {
            lbDate.Location = new Point(lbDate.Location.X, lbDate.Location.Y - grpUpd.Height);
            dateEdit.Location = new Point(dateEdit.Location.X, dateEdit.Location.Y - grpUpd.Height);
            lblM2.Location = new Point(lblM2.Location.X, lblM2.Location.Y - grpUpd.Height);
            lookUpEditM2.Location = new Point(lookUpEditM2.Location.X, lookUpEditM2.Location.Y - grpUpd.Height);
            rbType.Location = new Point(rbType.Location.X, rbType.Location.Y - grpUpd.Height);
            groupControl2.Location = new Point(groupControl2.Location.X, groupControl2.Location.Y - grpUpd.Height);
            btnYes.Location = new Point(btnYes.Location.X, btnYes.Location.Y - grpUpd.Height);
            btnNo.Location = new Point(btnNo.Location.X, btnNo.Location.Y - grpUpd.Height);
            this.Size = new Size(this.Size.Width, this.Size.Height - grpUpd.Height);
        }
    }

    internal class ComboBoxItem
    {
        #region Instance Properties

        public object Id { get; set; }

        public string Text { get; set; }

        #endregion

        #region Instance Methods

        public override string ToString()
        {
            return Text;
        }

        #endregion
    }
}