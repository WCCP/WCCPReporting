﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using POCE.Reports.M1M2;
using POCE.Reports.M1M2.Properties;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;

namespace WccpReporting
{
    public delegate void CloseUIdelegate();

    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpPOCEM1M2Report.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                SettingsForm setForm = new SettingsForm();

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.AddForm(setForm);

                    _reportControl = new WccpUIControl(reportId);
                    _reportCaption = Resources.FSMReport;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        public void CloseUI()
        {
            if (WccpUIControl.ReportControl.HasUnsavedData()
                && XtraMessageBox.Show(Resources.SaveData, Resources.DataNotSaved, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (WccpUIControl.ReportControl.SaveUnsavedData())
                {
                    XtraMessageBox.Show(Resources.DataSaved);
                }
            }
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}
