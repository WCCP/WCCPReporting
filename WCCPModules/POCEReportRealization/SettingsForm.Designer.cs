namespace WccpReporting.POCEReportRealization
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.treeListRegions = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnRegionName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnRegionID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListDate = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnDateID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnDateName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.staffLevelTree = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEditChannelList = new DevExpress.XtraEditors.LookUpEdit();
            this.grpUpd = new DevExpress.XtraEditors.GroupControl();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.lblUpd = new DevExpress.XtraEditors.LabelControl();
            this.btn_upd = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEditStandardChannelList = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize) (this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.treeListRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.treeListDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.staffLevelTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.lookUpEditChannelList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.grpUpd)).BeginInit();
            this.grpUpd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.lookUpEditStandardChannelList.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.ImageIndex = 0;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(169, 409);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(108, 23);
            this.btnYes.TabIndex = 1;
            this.btnYes.Text = "�������������";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer) (resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "check_24.png");
            this.imCollection.Images.SetKeyName(1, "close_24.png");
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.ImageList = this.imCollection;
            this.btnCancel.Location = new System.Drawing.Point(283, 409);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "������";
            // 
            // treeListRegions
            // 
            this.treeListRegions.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnRegionName,
            this.treeListColumnRegionID});
            this.treeListRegions.Enabled = false;
            this.treeListRegions.Location = new System.Drawing.Point(2, 214);
            this.treeListRegions.Name = "treeListRegions";
            this.treeListRegions.OptionsView.ShowCheckBoxes = true;
            this.treeListRegions.OptionsView.ShowColumns = false;
            this.treeListRegions.OptionsView.ShowIndicator = false;
            this.treeListRegions.Size = new System.Drawing.Size(175, 40);
            this.treeListRegions.TabIndex = 10;
            this.treeListRegions.Visible = false;
            this.treeListRegions.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeListRegions_BeforeCheckNode);
            this.treeListRegions.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.AfterCheckNode);
            // 
            // treeListColumnRegionName
            // 
            this.treeListColumnRegionName.Caption = "Name";
            this.treeListColumnRegionName.FieldName = "Name";
            this.treeListColumnRegionName.Name = "treeListColumnRegionName";
            this.treeListColumnRegionName.OptionsColumn.AllowEdit = false;
            this.treeListColumnRegionName.Visible = true;
            this.treeListColumnRegionName.VisibleIndex = 0;
            // 
            // treeListColumnRegionID
            // 
            this.treeListColumnRegionID.Caption = "ID";
            this.treeListColumnRegionID.FieldName = "ID";
            this.treeListColumnRegionID.Name = "treeListColumnRegionID";
            this.treeListColumnRegionID.OptionsColumn.AllowEdit = false;
            // 
            // treeListDate
            // 
            this.treeListDate.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnDateID,
            this.treeListColumnDateName});
            this.treeListDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListDate.Location = new System.Drawing.Point(2, 22);
            this.treeListDate.Name = "treeListDate";
            this.treeListDate.OptionsView.ShowCheckBoxes = true;
            this.treeListDate.OptionsView.ShowColumns = false;
            this.treeListDate.OptionsView.ShowIndicator = false;
            this.treeListDate.Size = new System.Drawing.Size(160, 282);
            this.treeListDate.TabIndex = 11;
            this.treeListDate.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.AfterCheckNode);
            // 
            // treeListColumnDateID
            // 
            this.treeListColumnDateID.Caption = "ID";
            this.treeListColumnDateID.FieldName = "ID";
            this.treeListColumnDateID.Name = "treeListColumnDateID";
            this.treeListColumnDateID.OptionsColumn.AllowEdit = false;
            this.treeListColumnDateID.Width = 91;
            // 
            // treeListColumnDateName
            // 
            this.treeListColumnDateName.Caption = "Name";
            this.treeListColumnDateName.FieldName = "Name";
            this.treeListColumnDateName.Name = "treeListColumnDateName";
            this.treeListColumnDateName.OptionsColumn.AllowEdit = false;
            this.treeListColumnDateName.Visible = true;
            this.treeListColumnDateName.VisibleIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.staffLevelTree);
            this.groupControl1.Controls.Add(this.treeListRegions);
            this.groupControl1.Location = new System.Drawing.Point(12, 81);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(179, 212);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "������:";
            // 
            // staffLevelTree
            // 
            this.staffLevelTree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.staffLevelTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.staffLevelTree.Location = new System.Drawing.Point(2, 22);
            this.staffLevelTree.Name = "staffLevelTree";
            this.staffLevelTree.OptionsSelection.InvertSelection = true;
            this.staffLevelTree.OptionsSelection.MultiSelect = true;
            this.staffLevelTree.OptionsSelection.UseIndicatorForSelection = true;
            this.staffLevelTree.OptionsView.ShowColumns = false;
            this.staffLevelTree.OptionsView.ShowIndicator = false;
            this.staffLevelTree.Size = new System.Drawing.Size(175, 188);
            this.staffLevelTree.TabIndex = 11;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.treeListDate);
            this.groupControl2.Location = new System.Drawing.Point(197, 81);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(164, 306);
            this.groupControl2.TabIndex = 13;
            this.groupControl2.Text = "�����:";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lookUpEditChannelList);
            this.groupControl3.Location = new System.Drawing.Point(12, 295);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(179, 44);
            this.groupControl3.TabIndex = 14;
            this.groupControl3.Text = "����� ���������:";
            // 
            // lookUpEditChannelList
            // 
            this.lookUpEditChannelList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lookUpEditChannelList.Location = new System.Drawing.Point(2, 22);
            this.lookUpEditChannelList.Name = "lookUpEditChannelList";
            this.lookUpEditChannelList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditChannelList.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 43, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditChannelList.Properties.NullText = "<�� ������>";
            this.lookUpEditChannelList.Properties.ShowFooter = false;
            this.lookUpEditChannelList.Properties.ShowHeader = false;
            this.lookUpEditChannelList.Size = new System.Drawing.Size(175, 20);
            this.lookUpEditChannelList.TabIndex = 15;
            this.lookUpEditChannelList.EditValueChanged += new System.EventHandler(this.lookUpEditChannelList_EditValueChanged);
            // 
            // grpUpd
            // 
            this.grpUpd.Controls.Add(this.lblUpd2);
            this.grpUpd.Controls.Add(this.lblUpd);
            this.grpUpd.Controls.Add(this.btn_upd);
            this.grpUpd.Location = new System.Drawing.Point(12, 12);
            this.grpUpd.Name = "grpUpd";
            this.grpUpd.Size = new System.Drawing.Size(349, 63);
            this.grpUpd.TabIndex = 23;
            this.grpUpd.Text = "������";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Appearance.Options.UseFont = true;
            this.lblUpd2.Location = new System.Drawing.Point(77, 36);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(0, 13);
            this.lblUpd2.TabIndex = 8;
            // 
            // lblUpd
            // 
            this.lblUpd.Location = new System.Drawing.Point(12, 36);
            this.lblUpd.Name = "lblUpd";
            this.lblUpd.Size = new System.Drawing.Size(59, 13);
            this.lblUpd.TabIndex = 7;
            this.lblUpd.Text = "������ ��:";
            // 
            // btn_upd
            // 
            this.btn_upd.ImageIndex = 0;
            this.btn_upd.ImageList = this.imCollection;
            this.btn_upd.Location = new System.Drawing.Point(244, 25);
            this.btn_upd.Name = "btn_upd";
            this.btn_upd.Size = new System.Drawing.Size(100, 33);
            this.btn_upd.TabIndex = 6;
            this.btn_upd.Text = " �������� ";
            this.btn_upd.Click += new System.EventHandler(this.btn_upd_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.lookUpEditStandardChannelList);
            this.groupControl4.Location = new System.Drawing.Point(12, 343);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(179, 44);
            this.groupControl4.TabIndex = 24;
            this.groupControl4.Text = "�������� ��� ������:";
            // 
            // lookUpEditStandardChannelList
            // 
            this.lookUpEditStandardChannelList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lookUpEditStandardChannelList.Location = new System.Drawing.Point(2, 22);
            this.lookUpEditStandardChannelList.Name = "lookUpEditStandardChannelList";
            this.lookUpEditStandardChannelList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditStandardChannelList.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 43, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditStandardChannelList.Properties.NullText = "<�� ������>";
            this.lookUpEditStandardChannelList.Properties.ShowFooter = false;
            this.lookUpEditStandardChannelList.Properties.ShowHeader = false;
            this.lookUpEditStandardChannelList.Size = new System.Drawing.Size(175, 20);
            this.lookUpEditStandardChannelList.TabIndex = 15;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 444);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.grpUpd);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� �� �������������� (Pivot)";
            ((System.ComponentModel.ISupportInitialize) (this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.treeListRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.treeListDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.staffLevelTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.lookUpEditChannelList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.grpUpd)).EndInit();
            this.grpUpd.ResumeLayout(false);
            this.grpUpd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.lookUpEditStandardChannelList.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraTreeList.TreeList treeListRegions;
        private DevExpress.XtraTreeList.TreeList treeListDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnDateID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnDateName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnRegionName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnRegionID;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditChannelList;
        private DevExpress.XtraEditors.GroupControl grpUpd;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraEditors.LabelControl lblUpd;
        private DevExpress.XtraEditors.SimpleButton btn_upd;
        private DevExpress.XtraTreeList.TreeList staffLevelTree;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditStandardChannelList;
    }
}