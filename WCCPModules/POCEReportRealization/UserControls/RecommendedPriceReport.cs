﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using DevExpress.Utils;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using Logica.Reports.Common;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;

namespace WccpReporting.POCEReportRealization.UserControls
{
    [ToolboxItem(false)]
    public partial class RecommendedPriceReport : ReportUserControl
    {
        #region Constructors

        public RecommendedPriceReport()
        {
            InitializeComponent();
            pivotGridControl.OptionsView.HideAllTotals();
        }

        #endregion

        #region Instance Properties

        public override string TabCaption
        {
            get { return Resources.TAB_RecommendedPrice; }
        }

        protected override IEnumerable<PrintableComponentLink> PrintableComponentLinks
        {
            get { return new[] {new PrintableComponentLink {Component = pivotGridControl}}; }
        }

        #endregion

        #region Instance Methods

        public override void LoadReport(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal, int staffChannelType, int standardChannelType)
        {
            try
            {
                DataSet recommendedPriceData = DataAccessProvider.GetRecommendedPriceData(staffLevelId, staffId, month, typeOfPersonal);
                ClearKPIFields();
                pivotGridControl.DataSource = ParseData(recommendedPriceData);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }


        internal DataTable ParseData(DataSet ds)
        {
            if (ds.Tables.Count != 2)
                throw new Exception("Не могу выбрать данные для Рекомендованной цены");
            DataTable captions = ds.Tables[0];
            DataTable result = ds.Tables[1];
            List<string> summaryExecutionFields = new List<string>();
            foreach (DataRow caption in captions.Rows)
            {
                AddPivotField(RemoveSqlBracketsFromName(caption[SqlConstants.GET_RECOMMENDED_PRICE_FLD_REAL].ToString()), false);
                AddPivotField(RemoveSqlBracketsFromName(caption[SqlConstants.GET_RECOMMENDED_PRICE_FLD_RECOMMENDED].ToString()), false);

                string executedCaption = RemoveSqlBracketsFromName(caption[SqlConstants.GET_RECOMMENDED_PRICE_FLD_EXECUTED].ToString());
                AddPivotField(executedCaption, true);
                summaryExecutionFields.Add(string.Format("[{0}]", executedCaption));
            }


            PivotGridField field = new PivotGridField
                                       {
                                           Caption = pivotGridFieldPriceExec.Caption,
                                           Area = PivotArea.DataArea,
                                           Name = string.Format("{0}2", pivotGridFieldPriceExec.Name),
                                           FieldName = pivotGridFieldPriceExec.FieldName,
                                           Visible = true
                                       };
            field.CellFormat.FormatType = pivotGridFieldPriceExec.CellFormat.FormatType;
            field.CellFormat.FormatString = pivotGridFieldPriceExec.CellFormat.FormatString;

            pivotGridControl.Fields.Add(field);

            return result;
        }

        private void AddPivotField(string fieldName, bool isExecution)
        {
            PivotGridField field = new PivotGridField
                                       {
                                           Area = PivotArea.DataArea,
                                           Name = string.Format("pivotGridFieldPrice_{0}", fieldName.Replace(" ", "")),
                                           FieldName = fieldName,
                                           Visible = true
                                       };
            string format = string.Format("{{0:N{0}}}", isExecution ? 0 : 2);

            field.CellFormat.FormatType = FormatType.Numeric;
            field.CellFormat.FormatString = format;
            field.TotalCellFormat.FormatType = FormatType.Numeric;
            field.TotalCellFormat.FormatString = format;
            field.GrandTotalCellFormat.FormatType = FormatType.Numeric;
            field.GrandTotalCellFormat.FormatString = format;

            pivotGridControl.Fields.Add(field);
        }

        private void ClearKPIFields()
        {
            for (int i = pivotGridControl.Fields.Count; i > 0; i--)
            {
                PivotGridField field = pivotGridControl.Fields[i - 1];
                if (field.Name.StartsWith("pivotGridFieldPrice") && field != pivotGridFieldPriceExec)
                    pivotGridControl.Fields.Remove(field);
            }
        }

        private string RemoveSqlBracketsFromName(string str)
        {
            StringBuilder result = new StringBuilder(str);
            if (result[0].Equals('['))
                result.Remove(0, 1);
            if (result[result.Length - 1].Equals(']'))
                result.Remove(result.Length - 1, 1);
            return result.ToString();
        }

        #endregion
    }
}