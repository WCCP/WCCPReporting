﻿namespace WccpReporting.POCEReportRealization.UserControls
{
  partial class TTBaseReport
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.pivotGridFieldFactAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCodeTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldFactNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldWeekNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCategoryTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTypeTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFielM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDistrict = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldMerchTTBase = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSalesTTBase = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldLegalNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldLegalAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldContractOn = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOpex = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridFieldFactAddressTT
            // 
            this.pivotGridFieldFactAddressTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldFactAddressTT.AreaIndex = 5;
            this.pivotGridFieldFactAddressTT.Caption = "Адрес";
            this.pivotGridFieldFactAddressTT.FieldName = "OLAddress";
            this.pivotGridFieldFactAddressTT.Name = "pivotGridFieldFactAddressTT";
            this.pivotGridFieldFactAddressTT.Width = 200;
            // 
            // pivotGridFieldCodeTT
            // 
            this.pivotGridFieldCodeTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldCodeTT.AreaIndex = 3;
            this.pivotGridFieldCodeTT.Caption = "Код ТТ";
            this.pivotGridFieldCodeTT.FieldName = "OL_ID_Merch";
            this.pivotGridFieldCodeTT.Name = "pivotGridFieldCodeTT";
            // 
            // pivotGridFieldFactNameTT
            // 
            this.pivotGridFieldFactNameTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldFactNameTT.AreaIndex = 4;
            this.pivotGridFieldFactNameTT.Caption = "Имя";
            this.pivotGridFieldFactNameTT.FieldName = "olname";
            this.pivotGridFieldFactNameTT.Name = "pivotGridFieldFactNameTT";
            this.pivotGridFieldFactNameTT.Width = 130;
            // 
            // pivotGridFieldWeekNumber
            // 
            this.pivotGridFieldWeekNumber.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldWeekNumber.AreaIndex = 2;
            this.pivotGridFieldWeekNumber.Caption = "Маршрут";
            this.pivotGridFieldWeekNumber.FieldName = "RouteName";
            this.pivotGridFieldWeekNumber.Name = "pivotGridFieldWeekNumber";
            // 
            // pivotGridFieldCategoryTT
            // 
            this.pivotGridFieldCategoryTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldCategoryTT.AreaIndex = 1;
            this.pivotGridFieldCategoryTT.Caption = "Категория ТТ";
            this.pivotGridFieldCategoryTT.FieldName = "Category";
            this.pivotGridFieldCategoryTT.Name = "pivotGridFieldCategoryTT";
            // 
            // pivotGridFieldTypeTT
            // 
            this.pivotGridFieldTypeTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldTypeTT.AreaIndex = 0;
            this.pivotGridFieldTypeTT.Caption = "Тип ТТ";
            this.pivotGridFieldTypeTT.FieldName = "OLtype_Name";
            this.pivotGridFieldTypeTT.Name = "pivotGridFieldTypeTT";
            // 
            // pivotGridFieldM1
            // 
            this.pivotGridFieldM1.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM1.AreaIndex = 1;
            this.pivotGridFieldM1.Caption = "M1";
            this.pivotGridFieldM1.FieldName = "MerchName";
            this.pivotGridFieldM1.Name = "pivotGridFieldM1";
            // 
            // pivotGridFieldM2
            // 
            this.pivotGridFieldM2.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM2.AreaIndex = 0;
            this.pivotGridFieldM2.Caption = "M2";
            this.pivotGridFieldM2.FieldName = "Supervisor_name";
            this.pivotGridFieldM2.Name = "pivotGridFieldM2";
            // 
            // pivotGridFielM3
            // 
            this.pivotGridFielM3.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFielM3.AreaIndex = 0;
            this.pivotGridFielM3.Caption = "M3";
            this.pivotGridFielM3.FieldName = "dsm_name";
            this.pivotGridFielM3.Name = "pivotGridFielM3";
            this.pivotGridFielM3.Visible = false;
            // 
            // pivotGridFieldDistrict
            // 
            this.pivotGridFieldDistrict.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldDistrict.AreaIndex = 0;
            this.pivotGridFieldDistrict.Caption = "Область";
            this.pivotGridFieldDistrict.FieldName = "district_name";
            this.pivotGridFieldDistrict.Name = "pivotGridFieldDistrict";
            this.pivotGridFieldDistrict.Visible = false;
            // 
            // pivotGridFieldRegion
            // 
            this.pivotGridFieldRegion.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldRegion.AreaIndex = 8;
            this.pivotGridFieldRegion.Caption = "Регион";
            this.pivotGridFieldRegion.FieldName = "region_name";
            this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
            this.pivotGridFieldRegion.Visible = false;
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.BackColor = System.Drawing.SystemColors.Control;
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldRegion,
            this.pivotGridFieldDistrict,
            this.pivotGridFielM3,
            this.pivotGridFieldM2,
            this.pivotGridFieldM1,
            this.pivotGridFieldTypeTT,
            this.pivotGridFieldCategoryTT,
            this.pivotGridFieldWeekNumber,
            this.pivotGridFieldFactNameTT,
            this.pivotGridFieldCodeTT,
            this.pivotGridFieldFactAddressTT,
            this.pivotGridFieldMerchTTBase,
            this.pivotGridFieldSalesTTBase,
            this.pivotGridFieldLegalNameTT,
            this.pivotGridFieldLegalAddressTT,
            this.pivotGridFieldContractOn,
            this.pivotGridFieldOpex});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pivotGridControl.OptionsCustomization.AllowPrefilter = false;
            this.pivotGridControl.Size = new System.Drawing.Size(702, 272);
            this.pivotGridControl.TabIndex = 0;
            this.pivotGridControl.CustomCellValue += new System.EventHandler<DevExpress.XtraPivotGrid.PivotCellValueEventArgs>(this.pivotGridControl_CustomCellValue);
            // 
            // pivotGridFieldMerchTTBase
            // 
            this.pivotGridFieldMerchTTBase.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldMerchTTBase.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldMerchTTBase.AreaIndex = 0;
            this.pivotGridFieldMerchTTBase.Caption = "База ТТ Merch";
            this.pivotGridFieldMerchTTBase.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldMerchTTBase.FieldName = "OL_ID_Merch";
            this.pivotGridFieldMerchTTBase.Name = "pivotGridFieldMerchTTBase";
            // 
            // pivotGridFieldSalesTTBase
            // 
            this.pivotGridFieldSalesTTBase.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldSalesTTBase.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSalesTTBase.AreaIndex = 1;
            this.pivotGridFieldSalesTTBase.Caption = "База ТТ Sales";
            this.pivotGridFieldSalesTTBase.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSalesTTBase.FieldName = "OL_ID_Sales";
            this.pivotGridFieldSalesTTBase.Name = "pivotGridFieldSalesTTBase";
            // 
            // pivotGridFieldLegalNameTT
            // 
            this.pivotGridFieldLegalNameTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldLegalNameTT.AreaIndex = 6;
            this.pivotGridFieldLegalNameTT.Caption = "Юр. Имя ";
            this.pivotGridFieldLegalNameTT.FieldName = "OLTradingName";
            this.pivotGridFieldLegalNameTT.Name = "pivotGridFieldLegalNameTT";
            // 
            // pivotGridFieldLegalAddressTT
            // 
            this.pivotGridFieldLegalAddressTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldLegalAddressTT.AreaIndex = 7;
            this.pivotGridFieldLegalAddressTT.Caption = "Юр. Адрес";
            this.pivotGridFieldLegalAddressTT.FieldName = "OLDeliveryAddress";
            this.pivotGridFieldLegalAddressTT.Name = "pivotGridFieldLegalAddressTT";
            // 
            // pivotGridFieldContractOn
            // 
            this.pivotGridFieldContractOn.AreaIndex = 8;
            this.pivotGridFieldContractOn.Caption = "Контракт он";
            this.pivotGridFieldContractOn.FieldName = "ContractOn";
            this.pivotGridFieldContractOn.Name = "pivotGridFieldContractOn";
            // 
            // pivotGridFieldOpex
            // 
            this.pivotGridFieldOpex.AreaIndex = 9;
            this.pivotGridFieldOpex.Caption = "Летняя Программа ОРЕХ";
            this.pivotGridFieldOpex.FieldName = "OPEX";
            this.pivotGridFieldOpex.Name = "pivotGridFieldOpex";
            // 
            // TTBaseReport
            // 
            this.AutoSize = true;
            this.Controls.Add(this.pivotGridControl);
            this.Name = "TTBaseReport";
            this.Size = new System.Drawing.Size(702, 272);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldFactAddressTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCodeTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldFactNameTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldWeekNumber;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCategoryTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTypeTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFielM3;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistrict;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
    private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMerchTTBase;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSalesTTBase;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldLegalNameTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldLegalAddressTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldContractOn;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOpex;




  }
}