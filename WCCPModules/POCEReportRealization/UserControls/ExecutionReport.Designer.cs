﻿namespace WccpReporting.POCEReportRealization.UserControls
{
  partial class ExecutionReport
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.pivotGridFieldKPIName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldFactAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCodeKPK = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldStandart = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldWeekNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldData = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCategoryTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTypeTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFielM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDistrict = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldStandartShort = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldValue = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldValueProc = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldKpiId = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAchivementsOrder = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOrderNo = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMerchId = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIsLast = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIsExecution = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAverageValue = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldContractOn = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOpex = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAuditor = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridFieldKPIName
            // 
            this.pivotGridFieldKPIName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldKPIName.AreaIndex = 0;
            this.pivotGridFieldKPIName.Caption = "Показатель";
            this.pivotGridFieldKPIName.ColumnValueLineCount = 2;
            this.pivotGridFieldKPIName.FieldName = "KPIName";
            this.pivotGridFieldKPIName.Name = "pivotGridFieldKPIName";
            this.pivotGridFieldKPIName.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.pivotGridFieldKPIName.Width = 300;
            // 
            // pivotGridFieldFactAddressTT
            // 
            this.pivotGridFieldFactAddressTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldFactAddressTT.AreaIndex = 11;
            this.pivotGridFieldFactAddressTT.Caption = "Факт. Адрес ТТ";
            this.pivotGridFieldFactAddressTT.FieldName = "OLAddress";
            this.pivotGridFieldFactAddressTT.Name = "pivotGridFieldFactAddressTT";
            this.pivotGridFieldFactAddressTT.Width = 200;
            // 
            // pivotGridFieldCodeKPK
            // 
            this.pivotGridFieldCodeKPK.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldCodeKPK.AreaIndex = 10;
            this.pivotGridFieldCodeKPK.Caption = "Код КПК";
            this.pivotGridFieldCodeKPK.FieldName = "ol_id";
            this.pivotGridFieldCodeKPK.Name = "pivotGridFieldCodeKPK";
            // 
            // pivotGridFieldNameTT
            // 
            this.pivotGridFieldNameTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldNameTT.AreaIndex = 9;
            this.pivotGridFieldNameTT.Caption = "Факт. Имя ТТ";
            this.pivotGridFieldNameTT.FieldName = "OLName";
            this.pivotGridFieldNameTT.Name = "pivotGridFieldNameTT";
            this.pivotGridFieldNameTT.Width = 130;
            // 
            // pivotGridFieldStandart
            // 
            this.pivotGridFieldStandart.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldStandart.AreaIndex = 8;
            this.pivotGridFieldStandart.Caption = "Стандарт";
            this.pivotGridFieldStandart.FieldName = "Standart";
            this.pivotGridFieldStandart.Name = "pivotGridFieldStandart";
            this.pivotGridFieldStandart.Visible = false;
            // 
            // pivotGridFieldWeekNumber
            // 
            this.pivotGridFieldWeekNumber.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldWeekNumber.AreaIndex = 7;
            this.pivotGridFieldWeekNumber.Caption = "Неделя";
            this.pivotGridFieldWeekNumber.FieldName = "WeekNumber";
            this.pivotGridFieldWeekNumber.Name = "pivotGridFieldWeekNumber";
            // 
            // pivotGridFieldData
            // 
            this.pivotGridFieldData.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldData.AreaIndex = 6;
            this.pivotGridFieldData.Caption = "Дата";
            this.pivotGridFieldData.FieldName = "date";
            this.pivotGridFieldData.Name = "pivotGridFieldData";
            // 
            // pivotGridFieldCategoryTT
            // 
            this.pivotGridFieldCategoryTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldCategoryTT.AreaIndex = 5;
            this.pivotGridFieldCategoryTT.Caption = "Категория ТТ";
            this.pivotGridFieldCategoryTT.FieldName = "Category";
            this.pivotGridFieldCategoryTT.Name = "pivotGridFieldCategoryTT";
            // 
            // pivotGridFieldTypeTT
            // 
            this.pivotGridFieldTypeTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldTypeTT.AreaIndex = 4;
            this.pivotGridFieldTypeTT.Caption = "Тип ТТ";
            this.pivotGridFieldTypeTT.FieldName = "OLtype_name";
            this.pivotGridFieldTypeTT.Name = "pivotGridFieldTypeTT";
            // 
            // pivotGridFieldM1
            // 
            this.pivotGridFieldM1.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldM1.AreaIndex = 3;
            this.pivotGridFieldM1.Caption = "M1";
            this.pivotGridFieldM1.FieldName = "MerchName";
            this.pivotGridFieldM1.Name = "pivotGridFieldM1";
            // 
            // pivotGridFieldM2
            // 
            this.pivotGridFieldM2.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldM2.AreaIndex = 2;
            this.pivotGridFieldM2.Caption = "M2";
            this.pivotGridFieldM2.FieldName = "Supervisor_name";
            this.pivotGridFieldM2.Name = "pivotGridFieldM2";
            // 
            // pivotGridFielM3
            // 
            this.pivotGridFielM3.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFielM3.AreaIndex = 1;
            this.pivotGridFielM3.Caption = "M3";
            this.pivotGridFielM3.FieldName = "dsm_name";
            this.pivotGridFielM3.Name = "pivotGridFielM3";
            // 
            // pivotGridFieldDistrict
            // 
            this.pivotGridFieldDistrict.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldDistrict.AreaIndex = 0;
            this.pivotGridFieldDistrict.Caption = "Область";
            this.pivotGridFieldDistrict.FieldName = "district_name";
            this.pivotGridFieldDistrict.Name = "pivotGridFieldDistrict";
            // 
            // pivotGridFieldRegion
            // 
            this.pivotGridFieldRegion.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldRegion.AreaIndex = 12;
            this.pivotGridFieldRegion.Caption = "Регион";
            this.pivotGridFieldRegion.FieldName = "region_name";
            this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.BackColor = System.Drawing.SystemColors.Control;
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldRegion,
            this.pivotGridFieldDistrict,
            this.pivotGridFielM3,
            this.pivotGridFieldM2,
            this.pivotGridFieldM1,
            this.pivotGridFieldAuditor,
            this.pivotGridFieldTypeTT,
            this.pivotGridFieldCategoryTT,
            this.pivotGridFieldData,
            this.pivotGridFieldWeekNumber,
            this.pivotGridFieldStandart,
            this.pivotGridFieldStandartShort,
            this.pivotGridFieldNameTT,
            this.pivotGridFieldCodeKPK,
            this.pivotGridFieldFactAddressTT,
            this.pivotGridFieldValue,
            this.pivotGridFieldValueProc,
            this.pivotGridFieldKpiId,
            this.pivotGridFieldKPIName,
            this.pivotGridFieldYear,
            this.pivotGridFieldMonth,
            this.pivotGridFieldAchivementsOrder,
            this.pivotGridFieldOrderNo,
            this.pivotGridFieldMerchId,
            this.pivotGridFieldIsLast,
            this.pivotGridFieldIsExecution,
            this.pivotGridFieldAverageValue,
            this.pivotGridFieldContractOn,
            this.pivotGridFieldOpex});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pivotGridControl.OptionsCustomization.AllowPrefilter = false;
            this.pivotGridControl.Size = new System.Drawing.Size(702, 272);
            this.pivotGridControl.TabIndex = 0;
            this.pivotGridControl.CustomFieldSort += new DevExpress.XtraPivotGrid.PivotGridCustomFieldSortEventHandler(this.pivotGridControl_CustomFieldSort);
            this.pivotGridControl.CustomCellValue += new System.EventHandler<DevExpress.XtraPivotGrid.PivotCellValueEventArgs>(this.pivotGridControl_CustomCellValue);
            // 
            // pivotGridFieldStandartShort
            // 
            this.pivotGridFieldStandartShort.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldStandartShort.AreaIndex = 8;
            this.pivotGridFieldStandartShort.Caption = "Стандарт";
            this.pivotGridFieldStandartShort.FieldName = "StandartShort";
            this.pivotGridFieldStandartShort.Name = "pivotGridFieldStandartShort";
            // 
            // pivotGridFieldValue
            // 
            this.pivotGridFieldValue.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldValue.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldValue.AreaIndex = 0;
            this.pivotGridFieldValue.Caption = "Кол-во";
            this.pivotGridFieldValue.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldValue.FieldName = "VALUE";
            this.pivotGridFieldValue.Name = "pivotGridFieldValue";
            this.pivotGridFieldValue.SortBySummaryInfo.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldValueProc
            // 
            this.pivotGridFieldValueProc.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldValueProc.Appearance.Cell.Options.UseTextOptions = true;
            this.pivotGridFieldValueProc.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.pivotGridFieldValueProc.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldValueProc.AreaIndex = 1;
            this.pivotGridFieldValueProc.Caption = "Кол-во%";
            this.pivotGridFieldValueProc.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldValueProc.FieldName = "ValueProc";
            this.pivotGridFieldValueProc.Name = "pivotGridFieldValueProc";
            this.pivotGridFieldValueProc.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldValueProc.UnboundFieldName = "pivotGridFieldValueProc";
            this.pivotGridFieldValueProc.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // pivotGridFieldKpiId
            // 
            this.pivotGridFieldKpiId.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldKpiId.AreaIndex = 0;
            this.pivotGridFieldKpiId.Caption = "Kpi Id";
            this.pivotGridFieldKpiId.FieldName = "kpi_id";
            this.pivotGridFieldKpiId.Name = "pivotGridFieldKpiId";
            this.pivotGridFieldKpiId.Visible = false;
            // 
            // pivotGridFieldYear
            // 
            this.pivotGridFieldYear.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldYear.AreaIndex = 0;
            this.pivotGridFieldYear.Caption = "Год";
            this.pivotGridFieldYear.FieldName = "date";
            this.pivotGridFieldYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.pivotGridFieldYear.Name = "pivotGridFieldYear";
            this.pivotGridFieldYear.UnboundFieldName = "pivotGridField1";
            // 
            // pivotGridFieldMonth
            // 
            this.pivotGridFieldMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldMonth.AreaIndex = 1;
            this.pivotGridFieldMonth.Caption = "Месяц";
            this.pivotGridFieldMonth.FieldName = "date";
            this.pivotGridFieldMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.pivotGridFieldMonth.Name = "pivotGridFieldMonth";
            this.pivotGridFieldMonth.UnboundFieldName = "pivotGridFieldMonth";
            // 
            // pivotGridFieldAchivementsOrder
            // 
            this.pivotGridFieldAchivementsOrder.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldAchivementsOrder.AreaIndex = 2;
            this.pivotGridFieldAchivementsOrder.FieldName = "OffTradeAchivements_Field_Order";
            this.pivotGridFieldAchivementsOrder.Name = "pivotGridFieldAchivementsOrder";
            this.pivotGridFieldAchivementsOrder.Visible = false;
            // 
            // pivotGridFieldOrderNo
            // 
            this.pivotGridFieldOrderNo.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldOrderNo.AreaIndex = 2;
            this.pivotGridFieldOrderNo.FieldName = "OrderNo";
            this.pivotGridFieldOrderNo.Name = "pivotGridFieldOrderNo";
            this.pivotGridFieldOrderNo.Visible = false;
            // 
            // pivotGridFieldMerchId
            // 
            this.pivotGridFieldMerchId.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldMerchId.AreaIndex = 2;
            this.pivotGridFieldMerchId.FieldName = "MeRCh_id";
            this.pivotGridFieldMerchId.Name = "pivotGridFieldMerchId";
            this.pivotGridFieldMerchId.Visible = false;
            // 
            // pivotGridFieldIsLast
            // 
            this.pivotGridFieldIsLast.AreaIndex = 13;
            this.pivotGridFieldIsLast.Caption = "Последний визит";
            this.pivotGridFieldIsLast.FieldName = "LAST_VISIT_DESC";
            this.pivotGridFieldIsLast.Name = "pivotGridFieldIsLast";
            // 
            // pivotGridFieldIsExecution
            // 
            this.pivotGridFieldIsExecution.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldIsExecution.AreaIndex = 2;
            this.pivotGridFieldIsExecution.FieldName = "IS_Execution";
            this.pivotGridFieldIsExecution.Name = "pivotGridFieldIsExecution";
            this.pivotGridFieldIsExecution.Visible = false;
            // 
            // pivotGridFieldAverageValue
            // 
            this.pivotGridFieldAverageValue.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldAverageValue.AreaIndex = 2;
            this.pivotGridFieldAverageValue.Caption = "Срезанное значение";
            this.pivotGridFieldAverageValue.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldAverageValue.FieldName = "Converted_KPI_Value";
            this.pivotGridFieldAverageValue.Name = "pivotGridFieldAverageValue";
            this.pivotGridFieldAverageValue.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldContractOn
            // 
            this.pivotGridFieldContractOn.AreaIndex = 14;
            this.pivotGridFieldContractOn.Caption = "Контракт он";
            this.pivotGridFieldContractOn.FieldName = "ContractOn";
            this.pivotGridFieldContractOn.Name = "pivotGridFieldContractOn";
            // 
            // pivotGridFieldOpex
            // 
            this.pivotGridFieldOpex.AreaIndex = 15;
            this.pivotGridFieldOpex.Caption = "Летняя Программа ОРЕХ";
            this.pivotGridFieldOpex.FieldName = "OPEX";
            this.pivotGridFieldOpex.Name = "pivotGridFieldOpex";
            // 
            // pivotGridFieldAuditor
            // 
            this.pivotGridFieldAuditor.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldAuditor.AreaIndex = 16;
            this.pivotGridFieldAuditor.Caption = "Аудитор";
            this.pivotGridFieldAuditor.FieldName = "AuditorType_Name";
            this.pivotGridFieldAuditor.Name = "pivotGridFieldAuditor";
            // 
            // ExecutionReport
            // 
            this.AutoSize = true;
            this.Controls.Add(this.pivotGridControl);
            this.Name = "ExecutionReport";
            this.Size = new System.Drawing.Size(702, 272);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldValueProc;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldValue;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPIName;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldFactAddressTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCodeKPK;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNameTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStandart;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldWeekNumber;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldData;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCategoryTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTypeTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFielM3;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistrict;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
    private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKpiId;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMonth;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldYear;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAchivementsOrder;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOrderNo;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMerchId;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIsLast;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIsExecution;
	private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStandartShort;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAverageValue;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldContractOn;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOpex;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAuditor;




  }
}