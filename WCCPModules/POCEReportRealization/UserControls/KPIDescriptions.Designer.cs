﻿namespace WccpReporting.POCEReportRealization.UserControls
{
  partial class KPIDescriptions
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.gridControl = new DevExpress.XtraGrid.GridControl();
        this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.gridColumnNo = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnNumInDailik = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnKPIName = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnDescription = new DevExpress.XtraGrid.Columns.GridColumn();
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
        this.SuspendLayout();
        // 
        // gridControl
        // 
        this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.gridControl.Location = new System.Drawing.Point(0, 0);
        this.gridControl.MainView = this.gridView;
        this.gridControl.Name = "gridControl";
        this.gridControl.Size = new System.Drawing.Size(1011, 293);
        this.gridControl.TabIndex = 0;
        this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
        // 
        // gridView
        // 
        this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnNo,
            this.gridColumnNumInDailik,
            this.gridColumnKPIName,
            this.gridColumnDescription});
        this.gridView.GridControl = this.gridControl;
        this.gridView.Name = "gridView";
        this.gridView.OptionsBehavior.Editable = false;
        this.gridView.OptionsCustomization.AllowRowSizing = true;
        this.gridView.OptionsView.ColumnAutoWidth = false;
        this.gridView.OptionsView.RowAutoHeight = true;
        this.gridView.OptionsView.ShowGroupPanel = false;
        this.gridView.OptionsView.ShowIndicator = false;
        // 
        // gridColumnNo
        // 
        this.gridColumnNo.Caption = "№";
        this.gridColumnNo.FieldName = "OrderNo";
        this.gridColumnNo.MaxWidth = 95;
        this.gridColumnNo.MinWidth = 95;
        this.gridColumnNo.Name = "gridColumnNo";
        this.gridColumnNo.Visible = true;
        this.gridColumnNo.VisibleIndex = 0;
        this.gridColumnNo.Width = 95;
        // 
        // gridColumnNumInDailik
        // 
        this.gridColumnNumInDailik.Caption = "№ в дейлике";
        this.gridColumnNumInDailik.FieldName = "KPI_ID";
        this.gridColumnNumInDailik.MaxWidth = 90;
        this.gridColumnNumInDailik.MinWidth = 90;
        this.gridColumnNumInDailik.Name = "gridColumnNumInDailik";
        this.gridColumnNumInDailik.Visible = true;
        this.gridColumnNumInDailik.VisibleIndex = 1;
        this.gridColumnNumInDailik.Width = 90;
        // 
        // gridColumnKPIName
        // 
        this.gridColumnKPIName.AppearanceCell.Options.UseTextOptions = true;
        this.gridColumnKPIName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.gridColumnKPIName.Caption = "Название KPI";
        this.gridColumnKPIName.FieldName = "KPIName";
        this.gridColumnKPIName.Name = "gridColumnKPIName";
        this.gridColumnKPIName.Visible = true;
        this.gridColumnKPIName.VisibleIndex = 2;
        this.gridColumnKPIName.Width = 285;
        // 
        // gridColumnDescription
        // 
        this.gridColumnDescription.AppearanceCell.Options.UseTextOptions = true;
        this.gridColumnDescription.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.gridColumnDescription.Caption = "Описание";
        this.gridColumnDescription.FieldName = "KPI_Description";
        this.gridColumnDescription.MinWidth = 300;
        this.gridColumnDescription.Name = "gridColumnDescription";
        this.gridColumnDescription.Visible = true;
        this.gridColumnDescription.VisibleIndex = 3;
        this.gridColumnDescription.Width = 1000;
        // 
        // KPIDescriptions
        // 
        this.AutoSize = true;
        this.Controls.Add(this.gridControl);
        this.Name = "KPIDescriptions";
        this.Size = new System.Drawing.Size(1011, 293);
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnNumInDailik;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnKPIName;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnDescription;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnNo;





  }
}