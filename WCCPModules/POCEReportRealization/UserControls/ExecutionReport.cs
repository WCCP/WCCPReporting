﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;
using WccpReporting.POCEReportRealization.Utility;

namespace WccpReporting.POCEReportRealization.UserControls {
    [ToolboxItem(false)]
    public partial class ExecutionReport : ReportUserControl {
        #region Readonly & Static Fields

        private readonly int[] avgKPI = new[] {33, 34, 55, 201, 221, 234, 235, 236, 237, 238, 239, 240, 245};
        private readonly int[] coolerKPI = new[] {27, 50, 51, 52, 53};

        #endregion

        #region Fields
        #endregion

        #region Constructors

        public ExecutionReport() {
            InitializeComponent();

            pivotGridControl.OptionsView.HideAllTotals();
            pivotGridControl.Prefilter.Criteria = CriteriaOperator.Parse("[" + pivotGridFieldIsExecution + "]");
        }

        #endregion

        #region Instance Properties

        public override string TabCaption {
            get { return Resources.TAB_Execution; }
        }

        protected override IEnumerable<PrintableComponentLink> PrintableComponentLinks {
            get { return new[] {new PrintableComponentLink {Component = pivotGridControl}}; }
        }

        protected bool TypeOfPersonal { get; set; }

        #endregion

        #region Instance Methods

        public override void LoadReport(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal,
            int staffChannelType, int standardChannelType)
        {
            try
            {
                TypeOfPersonal = ConvertEx.ToBool(typeOfPersonal);
                DataTable dataTable = DataAccessProvider.GetExecutionData(staffLevelId, staffId, month, typeOfPersonal,
                    staffChannelType, standardChannelType);
                pivotGridControl.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }

        private int GetExecutedValueCount(PivotDrillDownDataSource ds) {
            int countNotNull = 0;

            for (int i = 0; i < ds.RowCount; i++) {
                PivotDrillDownDataRow row = ds[i];
                if (row[pivotGridFieldValue] != null
                    && !string.IsNullOrEmpty(row[pivotGridFieldValue].ToString())
                        && row[pivotGridFieldValue].GetType() == typeof (bool)
                            && (bool) row[pivotGridFieldValue])
                    countNotNull++;
            }

            return countNotNull;
        }


        public void HideColumnsForNotOnTrade()
        {
            pivotGridFieldOpex.Visible = false;
            pivotGridFieldContractOn.Visible = false;
        }
        #endregion

        #region Event Handling

        private void pivotGridControl_CustomCellValue(object sender, PivotCellValueEventArgs e) {
            if (e.DataField == pivotGridFieldValue)
            {
                if ((pivotGridFieldKPIName.Area == PivotArea.ColumnArea ||
                     pivotGridFieldKPIName.Area == PivotArea.RowArea) && e.GetFieldValue(pivotGridFieldKPIName) != null &&
                    e.GetFieldValue(pivotGridFieldKPIName).Equals(Resources.KPI_TTCount))
                {
                    PivotDrillDownDataSource dataSource = e.CreateDrillDownDataSource();
                    var fields = new List<PivotGridField>(pivotGridControl.GetFieldsByArea(PivotArea.RowArea));
                    fields.AddRange(pivotGridControl.GetFieldsByArea(PivotArea.ColumnArea));
                    if (dataSource.RowCount > 0)
                        e.Value =
                            dataSource.Cast<PivotDrillDownDataRow>()
                                .Select(row => Convert.ToInt64(row[pivotGridFieldCodeKPK]))
                                .Distinct()
                                .Count()
                                .ToString();
                }
                else
                    if (e.SummaryValue != null && e.SummaryValue.Summary != null)
                    {
                        e.Value = e.SummaryValue.Summary;
                    }
            }

            if (e.DataField == pivotGridFieldValueProc) {
                PivotDrillDownDataSource pds = e.CreateDrillDownDataSource();
                if (pds.RowCount == 0) {
                    e.Value = null;
                    return;
                }
                /* 
='27'/('27' +'50' +'51' +'52' )
='50'/('27' +'50' +'51' +'52' )
='51'/('27' +'50' +'51' +'52' )
='52'/('27' +'50' +'51' +'52' )
='53'/('27' +'50' +'51' +'52' +'53' )

27 - Холодильник AB InBev (кол-во дверей)
50 - Холодильник Оболонь (кол-во дверей)
51 - Холодильник BBH (кол-во дверей)
52 - Холодильник другие (кол-во дверей)
53 - Холодильник ТТ (кол-во дверей)
*/
                decimal lValue = Convert.ToDecimal(1.0);

                if (pds.RowCount > 0 && pds.GetValue(0, pivotGridFieldKpiId) != null &&
                    coolerKPI.Contains((int) pds.GetValue(0, pivotGridFieldKpiId))) {
                    PivotGridControl pivot = (PivotGridControl) sender;
                    DataTable dataSource = (DataTable) pivot.DataSource;

                    List<Guid> selectedDocuments =
                        pds.Cast<PivotDrillDownDataRow>().Select(row => (Guid) row[SqlConstants.GET_POC_EXECUTION_FLD_KPI_DOCUMENT_ID]).ToList();

                    int currentKPIId = (int) pds.GetValue(0, pivotGridFieldKpiId);
                    List<int> requiredKpi = new List<int>(new[] {27, 50, 51, 52});
                    if (currentKPIId == 53) {
                        requiredKpi.Add(53);
                    }

                    decimal coolersAll =
                        dataSource.Select(String.Format("{0} IN ({1})", pivotGridFieldKpiId.FieldName,
                            requiredKpi.ToString(","))).Where(
                                row =>
                                    row.Field<bool>(pivotGridFieldValue.FieldName)
                                        &&
                                        selectedDocuments.IndexOf(
                                            row.Field<Guid>(SqlConstants.GET_POC_EXECUTION_FLD_KPI_DOCUMENT_ID)) != -1).
                            Count();

                    e.Value = Math.Round(coolersAll == 0 ? 0 : (GetExecutedValueCount(pds) * lValue) / coolersAll, 2, MidpointRounding.AwayFromZero);
                }
                else if (e.GetFieldValue(pivotGridFieldKPIName) != null
                    && (e.GetFieldValue(pivotGridFieldKPIName).Equals(Resources.KPI_TTCount)
                        || e.GetFieldValue(pivotGridFieldKPIName).Equals(Resources.KPI_VisitsCount))) {
                    e.Value = String.Empty;
                }
                else {
                    e.Value = Math.Round(pds.RowCount != 0 ? (GetExecutedValueCount(pds) * lValue) / pds.RowCount : 0, 2, MidpointRounding.AwayFromZero);
                }
            }

            if (e.DataField == pivotGridFieldAverageValue) {
                if (e.SummaryValue == null)
                    return;
                PivotDrillDownDataSource pds = e.CreateDrillDownDataSource();
                if (pds.RowCount > 0 && pds.GetValue(0, pivotGridFieldKpiId) != null &&
                    avgKPI.Contains((int) pds.GetValue(0, pivotGridFieldKpiId)) && e.SummaryValue.Average != null) {
                    e.Value = Math.Round(ConvertEx.ToDecimal(e.SummaryValue.Average), 2);
                }
                else if (e.SummaryValue != null && e.SummaryValue.Summary != null &&
                    pds.Cast<PivotDrillDownDataRow>().Any(row => row[e.DataField] != null)) {
                    e.Value = e.SummaryValue.Summary;
                }
            }
        }


        private void pivotGridControl_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e)
        {
            if (e.Field != pivotGridFieldKPIName)
                return;

            var dt = pivotGridControl.DataSource as DataTable;

            if (dt != null && e.ListSourceRowIndex1 >= 0 && e.ListSourceRowIndex2 >= 0 &&
                e.ListSourceRowIndex1 < dt.Rows.Count && e.ListSourceRowIndex2 < dt.Rows.Count)
            {
                int or1 = dt.Rows[e.ListSourceRowIndex1].Field<int>(pivotGridFieldOrderNo.FieldName);
                int or2 = dt.Rows[e.ListSourceRowIndex2].Field<int>(pivotGridFieldOrderNo.FieldName);

                int result = or1 < or2 ? -1 : or1 > or2 ? 1 : 0;

                if (result != 0)
                {
                    e.Result = result;
                    e.Handled = true;
                }
            }
        }

        #endregion
    }
}