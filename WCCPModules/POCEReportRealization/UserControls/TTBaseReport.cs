﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;

#endregion

namespace WccpReporting.POCEReportRealization.UserControls
{
    [ToolboxItem(false)]
    public partial class TTBaseReport : ReportUserControl
    {
        #region Constructors

        public TTBaseReport()
        {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        public override string TabCaption
        {
            get { return Resources.TAB_TTBase; }
        }

        protected override IEnumerable<PrintableComponentLink> PrintableComponentLinks
        {
            get { return new[] {new PrintableComponentLink {Component = pivotGridControl}}; }
        }

        public override DataTable TypesOfPersonal
        {
            get
            {
                DataTable dt = new DataTable();

                dt.Columns.Add(SqlConstants.TYPES_OF_PERSONAL_FLD_ID, typeof(bool));
                dt.Columns.Add(SqlConstants.TYPES_OF_PERSONAL_FLD_NAME, typeof(string));

                dt.Rows.Add(true, Resources.MerchTT);
                dt.Rows.Add(false, Resources.SalesTT);

                return dt;
            }
        }

        private bool typeOfPersonal;
        protected bool TypeOfPersonal
        {
            get { return typeOfPersonal; }
            set 
            { 
                typeOfPersonal = value;
                if (pivotGridFieldSalesTTBase.Area != PivotArea.DataArea ||
                    pivotGridFieldMerchTTBase.Area != PivotArea.DataArea) return;
                if (typeOfPersonal)
                {
                    pivotGridFieldMerchTTBase.AreaIndex = 0;
                    pivotGridFieldSalesTTBase.AreaIndex = 1;
                    pivotGridFieldCodeTT.FieldName = pivotGridFieldMerchTTBase.FieldName;
                }
                else
                {
                    pivotGridFieldSalesTTBase.AreaIndex = 0;
                    pivotGridFieldMerchTTBase.AreaIndex = 1;
                    pivotGridFieldCodeTT.FieldName = pivotGridFieldSalesTTBase.FieldName;
                }
            }
        }

        #endregion

        #region Instance Methods

        public override void LoadReport(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal, int staffChannelType, int standardChannelType)
        {
            try
            {
                TypeOfPersonal = ConvertEx.ToBool(typeOfPersonal);
                pivotGridControl.DataSource = DataAccessProvider.GetTTBaseData(staffLevelId, staffId, month, typeOfPersonal, staffChannelType);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }

        public void HideColumnsForNotOnTrade()
        {
            pivotGridFieldOpex.Visible = false;
            pivotGridFieldContractOn.Visible = false;
        }

        #endregion

        #region Event Handling

        private void pivotGridControl_CustomCellValue(object sender, PivotCellValueEventArgs e)
        {
            if (e.DataField == pivotGridFieldSalesTTBase || e.DataField == pivotGridFieldMerchTTBase)
            {
                PivotDrillDownDataSource drillDownDataSource = e.CreateDrillDownDataSource();
                e.Value =
                    drillDownDataSource.Cast<PivotDrillDownDataRow>()
                        .Where(r => r[e.DataField] != null && r[e.DataField] != DBNull.Value)
                        .Select(r => ConvertEx.ToLongInt(r[e.DataField]))
                        .Distinct()
                        .Count();
            }
        }

        #endregion
    }
}