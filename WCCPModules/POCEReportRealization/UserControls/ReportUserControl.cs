﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using Logica.Reports.BaseReportControl;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;

namespace WccpReporting.POCEReportRealization.UserControls
{
    [ToolboxItem(false)]
	public class ReportUserControl : UserControl
	{
		#region Instance Properties

		public virtual string TabCaption
		{
			get { return string.Empty; }
		}

		protected virtual IEnumerable<PrintableComponentLink> PrintableComponentLinks
		{
			get { return new PrintableComponentLink[] {}; }
		}

        public virtual DataTable TypesOfPersonal
        {
            get
            {
                DataTable dt = new DataTable();

                dt.Columns.Add(SqlConstants.TYPES_OF_PERSONAL_FLD_ID, typeof(bool));
                dt.Columns.Add(SqlConstants.TYPES_OF_PERSONAL_FLD_NAME, typeof(string));

                dt.Rows.Add(true, Resources.Merch);
                dt.Rows.Add(false, Resources.Sales);

                return dt;
            }
        }

		#endregion

		#region Instance Methods

		public virtual void LoadReport(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal, int staffChannelType, int standardChannelType)
		{
		}

		public void ExportToFile(string fName, ExportToType type)
		{
			switch (type)
			{
				case ExportToType.Html:
					PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
					break;
				case ExportToType.Mht:
					PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
					break;
				case ExportToType.Pdf:
					PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions {Compressed = true});
					break;
				case ExportToType.Rtf:
					PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
					break;
				case ExportToType.Txt:
					PrepareCompositeLink().PrintingSystem.ExportToText(fName);
					break;
				case ExportToType.Xls:
					PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions {SheetName = Text});
					break;
				case ExportToType.Xlsx:
					PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
					break;
				case ExportToType.Bmp:
					PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
					break;
				case ExportToType.Csv:
					PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
					break;
			}
		}

		public void ShowPrintPreview()
		{
			PrepareCompositeLink().ShowPreviewDialog();
		}

		private CompositeLink PrepareCompositeLink()
		{
			CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
			foreach (PrintableComponentLink link in PrintableComponentLinks)
			{
				compositeLink.Links.Add(link);
			}
			compositeLink.CreateDocument();
			return compositeLink;
		}

		#endregion
	}
}