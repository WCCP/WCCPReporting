﻿namespace WccpReporting.POCEReportRealization.UserControls
{
  partial class RecommendedPriceReport
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.pivotGridFieldPriceExec = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldFactAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldCodeKPK = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice2_3 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice2_2 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice2_1 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice1_3 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice1_2 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice1_1 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFielM3 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldDistrict = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
        this.pivotGridFieldPrice3_1 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice3_2 = new DevExpress.XtraPivotGrid.PivotGridField();
        this.pivotGridFieldPrice3_3 = new DevExpress.XtraPivotGrid.PivotGridField();
        ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
        this.SuspendLayout();
        // 
        // pivotGridFieldPriceExec
        // 
        this.pivotGridFieldPriceExec.AreaIndex = 5;
        this.pivotGridFieldPriceExec.Caption = "Выполнение цен";
        this.pivotGridFieldPriceExec.CellFormat.FormatString = "N0";
        this.pivotGridFieldPriceExec.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPriceExec.FieldName = "EXEC_QUANTITY";
        this.pivotGridFieldPriceExec.Name = "pivotGridFieldPriceExec";
        this.pivotGridFieldPriceExec.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
        this.pivotGridFieldPriceExec.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.None;
        this.pivotGridFieldPriceExec.ValueFormat.FormatString = "N0";
        this.pivotGridFieldPriceExec.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPriceExec.Width = 300;
        // 
        // pivotGridFieldFactAddressTT
        // 
        this.pivotGridFieldFactAddressTT.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
        this.pivotGridFieldFactAddressTT.AreaIndex = 2;
        this.pivotGridFieldFactAddressTT.Caption = "Факт. Адрес ТТ";
        this.pivotGridFieldFactAddressTT.FieldName = "OL_ADDRESS";
        this.pivotGridFieldFactAddressTT.Name = "pivotGridFieldFactAddressTT";
        this.pivotGridFieldFactAddressTT.Width = 200;
        // 
        // pivotGridFieldCodeKPK
        // 
        this.pivotGridFieldCodeKPK.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
        this.pivotGridFieldCodeKPK.AreaIndex = 0;
        this.pivotGridFieldCodeKPK.Caption = "Код КПК";
        this.pivotGridFieldCodeKPK.FieldName = "OL_ID";
        this.pivotGridFieldCodeKPK.Name = "pivotGridFieldCodeKPK";
        this.pivotGridFieldCodeKPK.Width = 200;
        // 
        // pivotGridFieldNameTT
        // 
        this.pivotGridFieldNameTT.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
        this.pivotGridFieldNameTT.AreaIndex = 1;
        this.pivotGridFieldNameTT.Caption = "Факт. Имя ТТ";
        this.pivotGridFieldNameTT.FieldName = "OL_NAME";
        this.pivotGridFieldNameTT.Name = "pivotGridFieldNameTT";
        this.pivotGridFieldNameTT.Width = 150;
        // 
        // pivotGridFieldPrice2_3
        // 
        this.pivotGridFieldPrice2_3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice2_3.AreaIndex = 5;
        this.pivotGridFieldPrice2_3.Caption = "Вып. Цены ЧВТ 1л";
        this.pivotGridFieldPrice2_3.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice2_3.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice2_3.FieldName = "EXECPRICEKPI2";
        this.pivotGridFieldPrice2_3.Name = "pivotGridFieldPrice2_3";
        // 
        // pivotGridFieldPrice2_2
        // 
        this.pivotGridFieldPrice2_2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice2_2.AreaIndex = 4;
        this.pivotGridFieldPrice2_2.Caption = "Рек. Цена ЧВТ 1л";
        this.pivotGridFieldPrice2_2.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice2_2.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice2_2.FieldName = "RECPRICEKPI2";
        this.pivotGridFieldPrice2_2.Name = "pivotGridFieldPrice2_2";
        // 
        // pivotGridFieldPrice2_1
        // 
        this.pivotGridFieldPrice2_1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice2_1.AreaIndex = 3;
        this.pivotGridFieldPrice2_1.Caption = "Цена ЧВТ 1л";
        this.pivotGridFieldPrice2_1.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice2_1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice2_1.FieldName = "REALPRICEKPI2";
        this.pivotGridFieldPrice2_1.Name = "pivotGridFieldPrice2_1";
        // 
        // pivotGridFieldPrice1_3
        // 
        this.pivotGridFieldPrice1_3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice1_3.AreaIndex = 2;
        this.pivotGridFieldPrice1_3.Caption = "Вып. Цены ЧВТ 0,5л";
        this.pivotGridFieldPrice1_3.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice1_3.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice1_3.FieldName = "EXECPRICEKPI1";
        this.pivotGridFieldPrice1_3.Name = "pivotGridFieldPrice1_3";
        // 
        // pivotGridFieldPrice1_2
        // 
        this.pivotGridFieldPrice1_2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice1_2.AreaIndex = 1;
        this.pivotGridFieldPrice1_2.Caption = "Рек. Цена ЧВТ 0,5л";
        this.pivotGridFieldPrice1_2.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice1_2.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice1_2.FieldName = "RECPRICEKPI1";
        this.pivotGridFieldPrice1_2.Name = "pivotGridFieldPrice1_2";
        // 
        // pivotGridFieldPrice1_1
        // 
        this.pivotGridFieldPrice1_1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice1_1.AreaIndex = 0;
        this.pivotGridFieldPrice1_1.Caption = "Цена ЧВТ 0,5л";
        this.pivotGridFieldPrice1_1.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice1_1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice1_1.FieldName = "REALPRICEKPI1";
        this.pivotGridFieldPrice1_1.Name = "pivotGridFieldPrice1_1";
        // 
        // pivotGridFieldM1
        // 
        this.pivotGridFieldM1.AreaIndex = 4;
        this.pivotGridFieldM1.Caption = "M1";
        this.pivotGridFieldM1.FieldName = "M1_NAME";
        this.pivotGridFieldM1.Name = "pivotGridFieldM1";
        // 
        // pivotGridFieldM2
        // 
        this.pivotGridFieldM2.AreaIndex = 3;
        this.pivotGridFieldM2.Caption = "M2";
        this.pivotGridFieldM2.FieldName = "M2_NAME";
        this.pivotGridFieldM2.Name = "pivotGridFieldM2";
        // 
        // pivotGridFielM3
        // 
        this.pivotGridFielM3.AreaIndex = 2;
        this.pivotGridFielM3.Caption = "M3";
        this.pivotGridFielM3.FieldName = "M3_NAME";
        this.pivotGridFielM3.Name = "pivotGridFielM3";
        // 
        // pivotGridFieldDistrict
        // 
        this.pivotGridFieldDistrict.AreaIndex = 1;
        this.pivotGridFieldDistrict.Caption = "Область";
        this.pivotGridFieldDistrict.FieldName = "DISTRICT_NAME";
        this.pivotGridFieldDistrict.Name = "pivotGridFieldDistrict";
        // 
        // pivotGridFieldRegion
        // 
        this.pivotGridFieldRegion.AreaIndex = 0;
        this.pivotGridFieldRegion.Caption = "Регион";
        this.pivotGridFieldRegion.FieldName = "REGION_NAME";
        this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
        // 
        // pivotGridControl
        // 
        this.pivotGridControl.BackColor = System.Drawing.SystemColors.Control;
        this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
        this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldRegion,
            this.pivotGridFieldDistrict,
            this.pivotGridFielM3,
            this.pivotGridFieldM2,
            this.pivotGridFieldM1,
            this.pivotGridFieldPriceExec,
            this.pivotGridFieldCodeKPK,
            this.pivotGridFieldNameTT,
            this.pivotGridFieldFactAddressTT,
            this.pivotGridFieldPrice1_1,
            this.pivotGridFieldPrice1_2,
            this.pivotGridFieldPrice1_3,
            this.pivotGridFieldPrice2_1,
            this.pivotGridFieldPrice2_2,
            this.pivotGridFieldPrice2_3,
            this.pivotGridFieldPrice3_1,
            this.pivotGridFieldPrice3_2,
            this.pivotGridFieldPrice3_3});
        this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
        this.pivotGridControl.Name = "pivotGridControl";
        this.pivotGridControl.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
        this.pivotGridControl.Size = new System.Drawing.Size(951, 413);
        this.pivotGridControl.TabIndex = 0;
        this.pivotGridControl.Visible = false;
        // 
        // pivotGridFieldPrice3_1
        // 
        this.pivotGridFieldPrice3_1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice3_1.AreaIndex = 6;
        this.pivotGridFieldPrice3_1.Caption = "Цена Старопрамен светлое 0,5л";
        this.pivotGridFieldPrice3_1.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice3_1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice3_1.FieldName = "REALPRICEKPI3";
        this.pivotGridFieldPrice3_1.Name = "pivotGridFieldPrice3_1";
        // 
        // pivotGridFieldPrice3_2
        // 
        this.pivotGridFieldPrice3_2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice3_2.AreaIndex = 7;
        this.pivotGridFieldPrice3_2.Caption = "Рек. Цена Старопрамен светлое 0,5л";
        this.pivotGridFieldPrice3_2.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice3_2.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice3_2.FieldName = "RECPRICEKPI3";
        this.pivotGridFieldPrice3_2.Name = "pivotGridFieldPrice3_2";
        // 
        // pivotGridFieldPrice3_3
        // 
        this.pivotGridFieldPrice3_3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
        this.pivotGridFieldPrice3_3.AreaIndex = 8;
        this.pivotGridFieldPrice3_3.Caption = "Вып. Цены Старопрамен светлое 0,5л";
        this.pivotGridFieldPrice3_3.CellFormat.FormatString = "{0:N2}";
        this.pivotGridFieldPrice3_3.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.pivotGridFieldPrice3_3.FieldName = "EXECPRICEKPI3";
        this.pivotGridFieldPrice3_3.Name = "pivotGridFieldPrice3_3";
        // 
        // RecommendedPriceReport
        // 
        this.AutoSize = true;
        this.Controls.Add(this.pivotGridControl);
        this.Name = "RecommendedPriceReport";
        this.Size = new System.Drawing.Size(951, 413);
        ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPriceExec;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldFactAddressTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCodeKPK;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNameTT;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice2_3;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice2_2;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice2_1;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice1_3;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice1_2;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice1_1;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFielM3;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistrict;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
    private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice3_1;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice3_2;
    private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice3_3;




  }
}