﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrinting;
using Logica.Reports.Common;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;

namespace WccpReporting.POCEReportRealization.UserControls
{
    [ToolboxItem(false)]
	public partial class KPIDescriptions : ReportUserControl
	{
		#region Fields

		#endregion

		#region Constructors

		public KPIDescriptions()
		{
			InitializeComponent();
		}

		#endregion

		#region Instance Properties

		public override string TabCaption
		{
			get { return Resources.TAB_KPIDescriptions; }
		}

		protected override IEnumerable<PrintableComponentLink> PrintableComponentLinks
		{
			get { return new[] {new PrintableComponentLink {Component = gridControl}}; }
		}

		#endregion

		#region Instance Methods

		public override void LoadReport(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal, int staffChannelType, int standardChannelType)
		{
			try
			{
				gridControl.DataSource = DataAccessProvider.GetKPIDescriptions(staffChannelType);
				gridView.BestFitColumns();
			}
			catch (Exception ex)
			{
				ErrorManager.ShowErrorBox(ex.Message);
			}
		}

		#endregion
	}
}