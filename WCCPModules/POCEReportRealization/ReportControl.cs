﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using WccpReporting.POCEReportRealization;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;
using WccpReporting.POCEReportRealization.UserControls;
using Logica.Reports.Common.Enums;

namespace WccpReporting
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : XtraUserControl // BaseReportUserControl
    {
        #region Readonly & Static Fields

        private readonly List<ReportUserControl> reports = new List<ReportUserControl>();

        private readonly SettingsForm setForm = new SettingsForm();

        #endregion

        #region Constructors

        public WccpUIControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        private object TypeOfPersonal
        {
            get { return barEditItemTypeOfPersonal.EditValue; }
        }

        #endregion

        #region Instance Methods

        public int ReportInit()
        {
            return CallSettingsForm();
        }

        internal void ShowReport()
        {
            WaitManager.StartWait();

            ReinitTabs();

            foreach (ReportUserControl userControl in reports)
            {
                userControl.LoadReport(setForm.StaffLevelId, setForm.StaffId, setForm.Dates, TypeOfPersonal, setForm.StaffChannelId, setForm.StandardChannelId);
            }

            WaitManager.StopWait();
        }

        private void AddTabPage(ReportUserControl report)
        {
            XtraTabPage page = new XtraTabPage();
            page.Controls.Add(report);
            page.Tag = report;
            page.Text = report.TabCaption;
            page.Dock = DockStyle.Fill;
            report.Dock = DockStyle.Fill;
            tabManager.TabPages.Add(page);
        }

        private int CallSettingsForm()
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                reports.Clear();

                //reports.Add(new KPIDescriptions()); // TASK #35938. POCE: Pivot Убрать закладку "Описание"
                var executionReport = new ExecutionReport();
                reports.Add(executionReport);
                if (setForm.StandardChannelId != (int)ChannelType.OnTrade)
                {
                    reports.Add(new TTBaseReport());
                    //reports.Add(new RecommendedPriceReport());
                }
                HideColumnsForNotOnTrade();

                barEditItemTypeOfPersonal.BeginUpdate();
                try
                {
                    if (reports.Count > 0)
                    {
                        DataTable typesOfPersonal = reports[0].TypesOfPersonal;
                        repositoryItemLookUpEditTypeOfPersonal.DataSource = typesOfPersonal;
                        barEditItemTypeOfPersonal.EditValue = setForm.StandardChannelId == (int)ChannelType.OnTrade
                                                                  ? false
                                                                  : typesOfPersonal.Rows[0][
                                                                      SqlConstants.TYPES_OF_PERSONAL_FLD_ID];
                    }
                    else
                    {
                        repositoryItemLookUpEditTypeOfPersonal.DataSource = null;
                        barEditItemTypeOfPersonal.EditValue = null;
                    }
                    barEditItemTypeOfPersonal.Visibility = setForm.StandardChannelId == (int)ChannelType.OnTrade ? BarItemVisibility.Never : BarItemVisibility.Always;
                }
                finally
                {
                    barEditItemTypeOfPersonal.EndUpdate();
                }

                ShowReport();
                return 0;
            }

            return 1;
        }

        private void Export(ExportToType type)
        {
            if (null == tabManager.SelectedTabPage)
                return;
            SaveFileDialog sfd = new SaveFileDialog
                                     {
                                         InitialDirectory = Assembly.GetExecutingAssembly().Location,
                                         FileName = Resources.poceReportFileName,
                                         Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower()),
                                         FilterIndex = 1,
                                         OverwritePrompt = true,
                                         RestoreDirectory = true
                                     };

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                ExportEngine(tabManager.SelectedTabPage, sfd.FileName, type);
            }
        }

        private void ExportEngine(XtraTabPage page, string fName, ExportToType type)
        {
            ReportUserControl report = page.Tag as ReportUserControl;
            if (report != null)
            {
                try
                {
                    report.ExportToFile(fName, type);
                }
                catch (Exception)
                {
                    MessageBox.Show(Resources.ExportError + fName, Resources.Error);
                }
            }
        }

        private void ReinitTabs()
        {
            while (tabManager.TabPages.Count > 0)
            {
                tabManager.TabPages.RemoveAt(0);
            }
            foreach (ReportUserControl report in reports)
            {
                AddTabPage(report);
            }
        }

        private void HideColumnsForNotOnTrade()
        {
            if (setForm.StaffChannelId != (int)ChannelType.OnTrade)
            {
                foreach (var report in reports)
                {
                    if (report is ExecutionReport)
                    {
                        ((ExecutionReport)report).HideColumnsForNotOnTrade();
                    }
                    if (report is TTBaseReport)
                    {
                        ((TTBaseReport)report).HideColumnsForNotOnTrade();
                    }
                }
            }
        }

        #endregion

        #region Event Handling

        private void barEditItemTypeOfPersonal_EditValueChanged(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != tabManager.SelectedTabPage)
            {
                tabManager.TabPages.Remove(tabManager.SelectedTabPage);
            }
        }

        private void btnExportAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToType type = (ExportToType)e.Item.Tag;
            if (tabManager.TabPages.Count < 2)
            {
                Export(ExportToType.Xls);
                return;
            }
            if (DialogResult.Yes ==
                MessageBox.Show(Resources.ExportAllTabs + @"(" + tabManager.TabPages.Count + @" " + Resources.Things + @")", Resources.Info, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog { Description = Resources.SelectFolder };
                String path = Assembly.GetExecutingAssembly().Location;
                path = path.Substring(0, path.LastIndexOf(@"\") + 1);
                fbd.SelectedPath = path;

                if (fbd.ShowDialog() != DialogResult.Cancel)
                {
                    foreach (XtraTabPage page in tabManager.TabPages)
                    {
                        String fName = String.Format("{0}\\{1}_{2}.{3}", fbd.SelectedPath, Resources.poceReportFileName, ((ReportUserControl)page.Tag).TabCaption, type.ToString().ToLower());
                        ExportEngine(page, fName, type);
                    }
                }
            }
        }

        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e)
        {
            Export((ExportToType)e.Item.Tag);
        }

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (tabManager.SelectedTabPage != null && tabManager.SelectedTabPage.Tag is ReportUserControl)
            {
                (tabManager.SelectedTabPage.Tag as ReportUserControl).ShowPrintPreview();
            }
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowReport();
        }

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            CallSettingsForm();
        }

        private void tabManager_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            if (tabManager.SelectedTabPage == null || tabManager.SelectedTabPage.Controls.Count == 0) return;

            var reportUserControl = tabManager.SelectedTabPage.Controls[0] as ReportUserControl;
            if (reportUserControl != null)
            {
                object editValue = barEditItemTypeOfPersonal.EditValue;
                repositoryItemLookUpEditTypeOfPersonal.DataSource = reportUserControl.TypesOfPersonal;
                barEditItemTypeOfPersonal.EditValue = editValue;
            }
        }

        #endregion
    }
}