using System;
using System.Collections.Generic;
using System.Data;
using DevExpress.Utils;
using DevExpress.XtraPivotGrid;
using Logica.Reports.Common;
using WccpReporting.POCEReportRealization.Core;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;

namespace WccpReporting.POCEReportRealization.UserControls
{
  public partial class RecommendedPriceReport : ReportUserControl
  {
    private readonly List<string> kpiList = new List<string>();

    public RecommendedPriceReport()
    {
      InitializeComponent();
    }

    public override string TabCaption
    {
      get { return Resources.RecommendedPrice; }
    }

    public override void ShowReport(IEnumerable<int> regions, IEnumerable<int> month, object typeOfPersonal)
    {
      try
      {
        DataSet recommendedPriceData = DataAccessProvider.GetRecommendedPriceData(regions, month, typeOfPersonal);
        ClearKPIFields();
        pivotGridControl.DataSource = ParseData(recommendedPriceData);
      }
      catch (Exception ex)
      {
        ErrorManager.ShowErrorBox(ex.Message);
      }
    }

    private void ClearKPIFields()
    {
      for (int i = pivotGridControl.Fields.Count; i > 0; i--)
      {
        PivotGridField field = pivotGridControl.Fields[i - 1];
        if (field.Name.StartsWith("pivotGridFieldPrice"))
          pivotGridControl.Fields.Remove(field);
      }
    }

    private void AddPivotField(string fieldName)
    {
      PivotGridField field = new PivotGridField
                               {
                                 Area = PivotArea.DataArea,
                                 Name = string.Format("pivotGridFieldPrice_{0}", fieldName.Replace(" ", "")),
                                 FieldName = fieldName,
                                 Visible = true
                               };

      field.CellFormat.FormatType = FormatType.Numeric;
      field.CellFormat.FormatString = "{0:N2}";
      field.TotalCellFormat.FormatType = FormatType.Numeric;
      field.TotalCellFormat.FormatString = "{0:N2}";
      field.GrandTotalCellFormat.FormatType = FormatType.Numeric;
      field.GrandTotalCellFormat.FormatString = "{0:N2}";

      pivotGridControl.Fields.Add(field);
    }

    private string RemoveSqlBracketsFromName(string str)
    {
      return str.Remove(str.Length - 1).Remove(0, 1);
    }

    internal DataTable ParseData(DataSet ds)
    {
      if (ds.Tables.Count != 2)
        throw new Exception("�� ���� ������� ������ ��� ��������������� ����");
      DataTable captions = ds.Tables[0];
      DataTable result = ds.Tables[1];
      foreach (DataRow caption in captions.Rows)
      {
        string kpi = (string) caption[SqlConstants.GET_RECOMMENDED_PRICE_FLD_KPI];
        string realCaption = string.Format(Resources.RecommendedPriceReport_RealFormat, kpi);
        string reccomendedCaption = string.Format(Resources.RecommendedPriceReport_ReccomendedFormat, kpi);
        string executedCaption = string.Format(Resources.RecommendedPriceReport_ExecutedFormat, kpi);

        string realBefore = caption[SqlConstants.GET_RECOMMENDED_PRICE_FLD_REAL].ToString();
        string reccomendedBefore = caption[SqlConstants.GET_RECOMMENDED_PRICE_FLD_RECOMMENDED].ToString();
        string executedBefore = caption[SqlConstants.GET_RECOMMENDED_PRICE_FLD_EXECUTED].ToString();

        result.Columns[RemoveSqlBracketsFromName(realBefore)].ColumnName = realCaption;
        result.Columns[RemoveSqlBracketsFromName(reccomendedBefore)].ColumnName = reccomendedCaption;
        result.Columns[RemoveSqlBracketsFromName(executedBefore)].ColumnName = executedCaption;
        kpiList.Add(realCaption);
        kpiList.Add(reccomendedCaption);
        kpiList.Add(executedCaption);
        AddPivotField(realCaption);
        AddPivotField(reccomendedCaption);
        AddPivotField(executedCaption);
      }
      return result;
    }

    public override void ShowPrintPreview()
    {
      pivotGridControl.ShowPrintPreview();
    }

    public override void ExportToXls(string fileName)
    {
      pivotGridControl.ExportToXls(fileName);
    }
  }
}