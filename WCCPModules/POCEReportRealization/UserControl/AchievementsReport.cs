using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using WccpReporting.POCEReportRealization;
using WccpReporting.POCEReportRealization.DataProvider;
using DevExpress.XtraPivotGrid;
using System.Linq;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using System.Globalization;
using WccpReporting.POCEReportRealization.Core;
using WccpReporting.POCEReportRealization.Properties;

namespace WccpReporting.POCEReportRealization.UserControls
{
  public partial class AchievementsReport : ReportUserControl
  {
    List<string> kpiList = new List<string>();
    public AchievementsReport()
    {
      InitializeComponent();
    }

    public override string TabCaption
    {
      get { return Resources.Achievements; }
    }

    public override void ShowReport(IEnumerable<int> regions, IEnumerable<int> month, object typeOfPersonal)
    {
      try
      {
        DataSet ds = DataAccessProvider.GetAchievementsReportData(regions, month, typeOfPersonal);
        ClearKPIFields();
        pivotGridControl.DataSource = ParseData(ds);
      }
      catch (Exception ex)
      {
        ErrorManager.ShowErrorBox(ex.Message);
      }
    }

    private void ClearKPIFields()
    {
      for (int i = pivotGridControl.Fields.Count; i > 0; i--)
      {
        PivotGridField field = pivotGridControl.Fields[i - 1];
        if (field.Name.StartsWith("pivotGridFieldKPI"))
          pivotGridControl.Fields.Remove(field);
      }
    }

    private void AddPivotField(string fieldName)
    {
      PivotGridField field = new PivotGridField
      {
        Area = PivotArea.DataArea,
        Name = string.Format("pivotGridFieldKPI_{0}", fieldName.Replace(" ", "")),
        FieldName = fieldName,
        Visible = true
      };
      field.CellFormat.FormatType = FormatType.Numeric;
      field.CellFormat.FormatString = "{0:N2}";
      field.TotalCellFormat.FormatType = FormatType.Numeric;
      field.TotalCellFormat.FormatString = "{0:N2}";
      field.GrandTotalCellFormat.FormatType = FormatType.Numeric;
      field.GrandTotalCellFormat.FormatString = "{0:N2}";

      pivotGridControl.Fields.Add(field);
    }

    private string RemoveSqlBracketsFromName(string str)
    {
      return str.Remove(str.Length - 1).Remove(0, 1);
    }

    internal DataTable ParseData(DataSet ds)
    {
      if (ds.Tables.Count != 2)
        throw new Exception("�� ���� ������� ������ ��� ����������");
      DataTable captions = ds.Tables[0];
      DataTable result = ds.Tables[1];
      foreach (DataRow caption in captions.Rows)
      {
        string kpi = (string)caption[SqlConstants.GET_ACHIEVEMENTS_FLD_KPI];
        kpiList.Add(kpi);
        AddPivotField(kpi);
      }
      return result;
    }


    #region IReport Members

    public override void ShowPrintPreview()
    {
      pivotGridControl.ShowPrintPreview();
    }

    public override void ExportToXls(string fileName)
    {
      this.pivotGridControl.ExportToXls(fileName);
    }

    #endregion
  }
}
