using WccpReporting.POCEReportRealization.Core;

namespace WccpReporting.POCEReportRealization.UserControls
{
    partial class ExecutionReport : IReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.pivotGridFieldKPIName = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldFactAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldCodeKPK = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldStandart = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldWeekNumber = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldData = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldCategoryTT = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldTypeTT = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFielM3 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldDistrict = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
          this.pivotGridFieldCount = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldValue = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldValueProc = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKpiId = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldYear = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldMonth = new DevExpress.XtraPivotGrid.PivotGridField();
          ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
          this.SuspendLayout();
          // 
          // pivotGridFieldKPIName
          // 
          this.pivotGridFieldKPIName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
          this.pivotGridFieldKPIName.AreaIndex = 0;
          this.pivotGridFieldKPIName.FieldName = "KPIName";
          this.pivotGridFieldKPIName.Name = "pivotGridFieldKPIName";
          this.pivotGridFieldKPIName.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
          this.pivotGridFieldKPIName.Width = 300;
          // 
          // pivotGridFieldFactAddressTT
          // 
          this.pivotGridFieldFactAddressTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldFactAddressTT.AreaIndex = 11;
          this.pivotGridFieldFactAddressTT.Caption = "����. ����� ��";
          this.pivotGridFieldFactAddressTT.FieldName = "OLAddress";
          this.pivotGridFieldFactAddressTT.Name = "pivotGridFieldFactAddressTT";
          // 
          // pivotGridFieldCodeKPK
          // 
          this.pivotGridFieldCodeKPK.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldCodeKPK.AreaIndex = 10;
          this.pivotGridFieldCodeKPK.Caption = "��� ���";
          this.pivotGridFieldCodeKPK.FieldName = "ol_id";
          this.pivotGridFieldCodeKPK.Name = "pivotGridFieldCodeKPK";
          // 
          // pivotGridFieldNameTT
          // 
          this.pivotGridFieldNameTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldNameTT.AreaIndex = 9;
          this.pivotGridFieldNameTT.Caption = "����. ��� ��";
          this.pivotGridFieldNameTT.FieldName = "olname";
          this.pivotGridFieldNameTT.Name = "pivotGridFieldNameTT";
          // 
          // pivotGridFieldStandart
          // 
          this.pivotGridFieldStandart.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldStandart.AreaIndex = 8;
          this.pivotGridFieldStandart.Caption = "��������";
          this.pivotGridFieldStandart.FieldName = "Standart";
          this.pivotGridFieldStandart.Name = "pivotGridFieldStandart";
          // 
          // pivotGridFieldWeekNumber
          // 
          this.pivotGridFieldWeekNumber.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldWeekNumber.AreaIndex = 7;
          this.pivotGridFieldWeekNumber.Caption = "������";
          this.pivotGridFieldWeekNumber.FieldName = "WeekNumber";
          this.pivotGridFieldWeekNumber.Name = "pivotGridFieldWeekNumber";
          // 
          // pivotGridFieldData
          // 
          this.pivotGridFieldData.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldData.AreaIndex = 6;
          this.pivotGridFieldData.Caption = "����";
          this.pivotGridFieldData.FieldName = "date";
          this.pivotGridFieldData.Name = "pivotGridFieldData";
          // 
          // pivotGridFieldCategoryTT
          // 
          this.pivotGridFieldCategoryTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldCategoryTT.AreaIndex = 5;
          this.pivotGridFieldCategoryTT.Caption = "��������� ��";
          this.pivotGridFieldCategoryTT.FieldName = "Category";
          this.pivotGridFieldCategoryTT.Name = "pivotGridFieldCategoryTT";
          // 
          // pivotGridFieldTypeTT
          // 
          this.pivotGridFieldTypeTT.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldTypeTT.AreaIndex = 4;
          this.pivotGridFieldTypeTT.Caption = "��� ��";
          this.pivotGridFieldTypeTT.FieldName = "OLtype_name";
          this.pivotGridFieldTypeTT.Name = "pivotGridFieldTypeTT";
          // 
          // pivotGridFieldM1
          // 
          this.pivotGridFieldM1.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldM1.AreaIndex = 3;
          this.pivotGridFieldM1.Caption = "M1";
          this.pivotGridFieldM1.FieldName = "MerchName";
          this.pivotGridFieldM1.Name = "pivotGridFieldM1";
          // 
          // pivotGridFieldM2
          // 
          this.pivotGridFieldM2.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldM2.AreaIndex = 2;
          this.pivotGridFieldM2.Caption = "M2";
          this.pivotGridFieldM2.FieldName = "Supervisor_name";
          this.pivotGridFieldM2.Name = "pivotGridFieldM2";
          // 
          // pivotGridFielM3
          // 
          this.pivotGridFielM3.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFielM3.AreaIndex = 1;
          this.pivotGridFielM3.Caption = "M3";
          this.pivotGridFielM3.FieldName = "dsm_name";
          this.pivotGridFielM3.Name = "pivotGridFielM3";
          // 
          // pivotGridFieldDistrict
          // 
          this.pivotGridFieldDistrict.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldDistrict.AreaIndex = 0;
          this.pivotGridFieldDistrict.Caption = "�������";
          this.pivotGridFieldDistrict.FieldName = "district_name";
          this.pivotGridFieldDistrict.Name = "pivotGridFieldDistrict";
          // 
          // pivotGridFieldRegion
          // 
          this.pivotGridFieldRegion.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                      | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
          this.pivotGridFieldRegion.AreaIndex = 12;
          this.pivotGridFieldRegion.Caption = "������";
          this.pivotGridFieldRegion.FieldName = "region_name";
          this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
          // 
          // pivotGridControl
          // 
          this.pivotGridControl.BackColor = System.Drawing.SystemColors.Control;
          this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
          this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldRegion,
            this.pivotGridFieldDistrict,
            this.pivotGridFielM3,
            this.pivotGridFieldM2,
            this.pivotGridFieldM1,
            this.pivotGridFieldTypeTT,
            this.pivotGridFieldCategoryTT,
            this.pivotGridFieldData,
            this.pivotGridFieldWeekNumber,
            this.pivotGridFieldStandart,
            this.pivotGridFieldNameTT,
            this.pivotGridFieldCodeKPK,
            this.pivotGridFieldFactAddressTT,
            this.pivotGridFieldCount,
            this.pivotGridFieldValue,
            this.pivotGridFieldValueProc,
            this.pivotGridFieldKpiId,
            this.pivotGridFieldKPIName,
            this.pivotGridFieldYear,
            this.pivotGridFieldMonth});
          this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
          this.pivotGridControl.Name = "pivotGridControl";
          this.pivotGridControl.Size = new System.Drawing.Size(702, 272);
          this.pivotGridControl.TabIndex = 0;
          this.pivotGridControl.FieldAreaChanged += new DevExpress.XtraPivotGrid.PivotFieldEventHandler(this.pivotGridControl_FieldAreaChanged);
          this.pivotGridControl.CustomFieldSort += new DevExpress.XtraPivotGrid.PivotGridCustomFieldSortEventHandler(this.pivotGridControl_CustomFieldSort);
          this.pivotGridControl.FieldAreaChanging += new DevExpress.XtraPivotGrid.PivotAreaChangingEventHandler(this.pivotGridControl_FieldAreaChanging);
          this.pivotGridControl.CustomCellDisplayText += new DevExpress.XtraPivotGrid.PivotCellDisplayTextEventHandler(this.pivotGridControl_CustomCellDisplayText);
          this.pivotGridControl.CustomDrawFieldHeader += new DevExpress.XtraPivotGrid.PivotCustomDrawFieldHeaderEventHandler(this.pivotGridControl_CustomDrawFieldHeader);
          // 
          // pivotGridFieldCount
          // 
          this.pivotGridFieldCount.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
          this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldCount.AreaIndex = 0;
          this.pivotGridFieldCount.Caption = " ";
          this.pivotGridFieldCount.FieldName = "ValueIntForCount";
          this.pivotGridFieldCount.MinWidth = 1;
          this.pivotGridFieldCount.Name = "pivotGridFieldCount";
          this.pivotGridFieldCount.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.False;
          this.pivotGridFieldCount.Options.AllowEdit = false;
          this.pivotGridFieldCount.Options.ShowButtonMode = DevExpress.XtraPivotGrid.PivotShowButtonModeEnum.ShowOnlyInEditor;
          this.pivotGridFieldCount.Width = 1;
          // 
          // pivotGridFieldValue
          // 
          this.pivotGridFieldValue.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
          this.pivotGridFieldValue.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldValue.AreaIndex = 1;
          this.pivotGridFieldValue.Caption = "���-��";
          this.pivotGridFieldValue.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldValue.FieldName = "ValueInt";
          this.pivotGridFieldValue.Name = "pivotGridFieldValue";
          this.pivotGridFieldValue.SortBySummaryInfo.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
          // 
          // pivotGridFieldValueProc
          // 
          this.pivotGridFieldValueProc.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
          this.pivotGridFieldValueProc.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldValueProc.AreaIndex = 2;
          this.pivotGridFieldValueProc.Caption = "���-��%";
          this.pivotGridFieldValueProc.FieldName = "ValueProc";
          this.pivotGridFieldValueProc.Name = "pivotGridFieldValueProc";
          this.pivotGridFieldValueProc.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
          this.pivotGridFieldValueProc.UnboundFieldName = "pivotGridFieldCountProc";
          this.pivotGridFieldValueProc.UnboundType = DevExpress.Data.UnboundColumnType.String;
          // 
          // pivotGridFieldKpiId
          // 
          this.pivotGridFieldKpiId.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
          this.pivotGridFieldKpiId.AreaIndex = 0;
          this.pivotGridFieldKpiId.Caption = "Kpi Id";
          this.pivotGridFieldKpiId.FieldName = "kpi_id";
          this.pivotGridFieldKpiId.Name = "pivotGridFieldKpiId";
          this.pivotGridFieldKpiId.Visible = false;
          // 
          // pivotGridFieldYear
          // 
          this.pivotGridFieldYear.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
          this.pivotGridFieldYear.AreaIndex = 0;
          this.pivotGridFieldYear.Caption = "���";
          this.pivotGridFieldYear.FieldName = "date";
          this.pivotGridFieldYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
          this.pivotGridFieldYear.Name = "pivotGridFieldYear";
          this.pivotGridFieldYear.UnboundFieldName = "pivotGridField1";
          // 
          // pivotGridFieldMonth
          // 
          this.pivotGridFieldMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
          this.pivotGridFieldMonth.AreaIndex = 1;
          this.pivotGridFieldMonth.Caption = "�����";
          this.pivotGridFieldMonth.FieldName = "date";
          this.pivotGridFieldMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
          this.pivotGridFieldMonth.Name = "pivotGridFieldMonth";
          this.pivotGridFieldMonth.UnboundFieldName = "pivotGridFieldMonth";
          // 
          // ExecutionReport
          // 
          this.AutoSize = true;
          this.Controls.Add(this.pivotGridControl);
          this.Name = "ExecutionReport";
          this.Size = new System.Drawing.Size(702, 272);
          ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldValueProc;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldValue;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPIName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldFactAddressTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCodeKPK;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNameTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStandart;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldWeekNumber;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldData;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCategoryTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTypeTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFielM3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistrict;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKpiId;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMonth;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldYear;




    }
}
