using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using WccpReporting.POCEReportRealization;
using WccpReporting.POCEReportRealization.DataProvider;
using DevExpress.XtraPivotGrid;
using System.Linq;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using System.Globalization;
using WccpReporting.POCEReportRealization.Core;
using WccpReporting.POCEReportRealization.Properties;
using DisplayTextArgs = DevExpress.XtraPivotGrid.PivotCellDisplayTextEventArgs;

namespace WccpReporting.POCEReportRealization.UserControls
{
  public partial class ExecutionReport : ReportUserControl, IReport
  {
    public ExecutionReport()
    {
      InitializeComponent();

      pivotGridFieldCount.MinWidth = 0;
      pivotGridFieldCount.Width = 0;
    }

    public override string TabCaption
    {
      get { return Resources.Execution; }
    }

    public override void ShowReport(IEnumerable<int> regions, IEnumerable<int> month, object typeOfPersonal)
    {
      try
      {
        DataTable dataTable = DataAccessProvider.GetExecutionData(regions, month, typeOfPersonal);
        pivotGridControl.DataSource = dataTable;
      }
      catch (Exception ex)
      {
        ErrorManager.ShowErrorBox(ex.Message);
      }
    }

    private void pivotGridControl_CustomCellDisplayText(object sender, DisplayTextArgs e)
    {
      if (e.DataField == pivotGridFieldValueProc)
      {
        List<Object> filterValues = GetFilterValues(e);

        double value = HandleCoolerFields(e, filterValues);

        if (value < 0)
        {
          value = HandleInbevProductsInOthersCooler(e, filterValues);
        }

        if (value < 0)
        {
          value = HandleOtherCustomFields(e, filterValues);
        }

        if (value < 0)
        {
          PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();

          value = ds.RowCount != 0 ? (GetValueCountNotNull(ds) * 100.0) / ds.RowCount : 0;
        }

        if (value >= 0)
        {
          e.DisplayText = value.ToString("0.00", CultureInfo.InvariantCulture) + " %";
        }
      }
    }

    private static int GetValueCountNotNull(PivotDrillDownDataSource ds)
    {
      int countNotNull;

      GetValueSum(ds, out countNotNull);

      return countNotNull;
    }

    private static int GetValueSum(PivotDrillDownDataSource ds, out int countNotNull)
    {
      int sum = 0;
      countNotNull = 0;

      for (int i = 0; i < ds.RowCount; i++)
      {
        PivotDrillDownDataRow row = ds[i];

        if (row[SqlConstants.GET_POC_EXECUTION_FLD_VALUE] != null && !string.IsNullOrEmpty(row[SqlConstants.GET_POC_EXECUTION_FLD_VALUE].ToString()))
        {
          int value;
          if (int.TryParse(row[SqlConstants.GET_POC_EXECUTION_FLD_VALUE].ToString(), out value))
          {
            sum += value;

            if (value > 0)
            {
              countNotNull++;
            }
          }
        }
      }

      return sum;
    }

    private double HandleCoolerFields(DisplayTextArgs e, List<Object> filterValues)
    {
      double coolersPart = -1;
      bool coolerTt = false, coolerOther = false;

      if (filterValues.Contains(SqlConstants.KPI_COOLERS_IN_BEV))
      {
        coolersPart = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_IN_BEV);
      }
      else if (filterValues.Contains(SqlConstants.KPI_COOLERS_OBOLON))
      {
        coolersPart = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_OBOLON);
      }
      else if (filterValues.Contains(SqlConstants.KPI_COOLERS_BBH))
      {
        coolersPart = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_BBH);
      }
      else if (filterValues.Contains(SqlConstants.KPI_COOLERS_OTHERS))
      {
        coolersPart = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_OTHERS);
        coolerOther = true;
      }
      else if (filterValues.Contains(SqlConstants.KPI_COOLERS_TT))
      {
        coolersPart = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_TT);
        coolerTt = true;
      }

      if (coolersPart > -1)
      {
        double coolersAll = 0;
        coolersAll += GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_IN_BEV);
        coolersAll += GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_OBOLON);
        coolersAll += GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_BBH);
        coolersAll += GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_OTHERS);

        if (coolerTt || coolerOther)
        {
          coolersAll += GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_TT);
        }

        return GetPerc(coolersPart, coolersAll);
      }

      return -1;
    }

    private double HandleInbevProductsInOthersCooler(DisplayTextArgs e, List<Object> filterValues)
    {
      double part = -1;
      if (filterValues.Contains(SqlConstants.KPI_IN_BEV_PRODUCT_IN_OTHERS_COOLER_MIN_10))
      {
        part = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_IN_BEV_PRODUCT_IN_OTHERS_COOLER_MIN_10);
      }

      if (filterValues.Contains(SqlConstants.KPI_IN_BEV_PRODUCT_IN_OTHERS_COOLER_1_3))
      {
        part = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_IN_BEV_PRODUCT_IN_OTHERS_COOLER_1_3);
      }

      if (part >= 0)
      {
        double all = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_IN_BEV_PRODUCT_IN_OTHERS_COOLER);
        return GetPerc(part, all);
      }

      return -1;
    }

    private double HandleOtherCustomFields(DisplayTextArgs e, List<Object> filterValues)
    {
      double part = -1;
      if (filterValues.Contains(SqlConstants.KPI_PLANOGRAM))
      {
        part = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_PLANOGRAM);
      }

      if (filterValues.Contains(SqlConstants.KPI_MAT))
      {
        part = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_MAT);
      }

      if (filterValues.Contains(SqlConstants.KPI_GOLDEN_FRIDGE))
      {
        part = GetCellValueForCurrentFilter(e, pivotGridFieldValue, pivotGridFieldKPIName, SqlConstants.KPI_GOLDEN_FRIDGE);
      }

      if (part >= 0)
      {
        double all = GetCellValueForCurrentFilter(e, pivotGridFieldCount, pivotGridFieldKPIName, SqlConstants.KPI_COOLERS_TT);

        return GetPerc(part, all);
      }

      return -1;
    }

    private void pivotGridControl_FieldAreaChanged(object sender, PivotFieldEventArgs e)
    {
      if (pivotGridFieldCount.AreaIndex != 0)
      {
        pivotGridFieldCount.AreaIndex = 0;
      }
    }

    private void pivotGridControl_CustomDrawFieldHeader(object sender, PivotCustomDrawFieldHeaderEventArgs e)
    {
      if (e.Field == pivotGridFieldCount) { e.Handled = true; }
    }

    private void pivotGridControl_FieldAreaChanging(object sender, PivotAreaChangingEventArgs e)
    {
      if (e.NewArea == PivotArea.DataArea && e.NewAreaIndex == 0)
      {
        e.Allow = false;
      }
    }

    private double GetPerc(double part, double all)
    {
      return all == 0 ? 0 : part * 100 / all;
    }

    private void pivotGridControl_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e)
    {
      if (e.Field == pivotGridFieldKPIName)
      {
        var dt = pivotGridControl.DataSource as DataTable;

        if (dt != null && e.ListSourceRowIndex1 >= 0 && e.ListSourceRowIndex2 >= 0
            && e.ListSourceRowIndex1 < dt.Rows.Count && e.ListSourceRowIndex2 < dt.Rows.Count)
        {
          int or1 = dt.Rows[e.ListSourceRowIndex1].Field<int>(SqlConstants.GET_POC_EXECUTION_FLD_ORDER_NO);
          int or2 = dt.Rows[e.ListSourceRowIndex2].Field<int>(SqlConstants.GET_POC_EXECUTION_FLD_ORDER_NO);

          int result = or1 < or2 ? -1 : or1 > or2 ? 1 : 0;

          if (result != 0)
          {
            e.Result = result;
            e.Handled = true;
          }
        }
      }
    }

    private Double GetCellValueForCurrentFilter(DisplayTextArgs e, PivotGridField dataField, PivotGridField fieldToReplace, Object valueToReplace)
    {
      List<Object> columnValues = new List<Object>(), rowValues = new List<Object>();
      columnValues.AddRange(e.GetColumnFields().Select(field => GetFieldValue(e, field, fieldToReplace, valueToReplace)));
      rowValues.AddRange(e.GetRowFields().Select(field => GetFieldValue(e, field, fieldToReplace, valueToReplace)));

      object o = e.GetCellValue(columnValues.ToArray(), rowValues.ToArray(), dataField);

      if (o is int)
      {
        return (int)o;
      }

      if (o is Double)
      {
        return (Double)o;
      }

      Double d;
      if (o != null && Double.TryParse(o.ToString(), out d))
      {
        return d;
      }

      return 0;
    }

    private Object GetFieldValue(DisplayTextArgs e, PivotGridField field, PivotGridField fieldToReplace, Object valueToReplace)
    {
      if (fieldToReplace != null && field == fieldToReplace)
      {
        return valueToReplace;
      }
      return e.GetFieldValue(field);
    }

    private List<Object> GetFilterValues(DisplayTextArgs e)
    {
      List<Object> filterValues = e.GetColumnFields().Select(field => e.GetFieldValue(field)).ToList();
      filterValues.AddRange(e.GetRowFields().Select(field => e.GetFieldValue(field)));

      return filterValues;
    }

    #region IReport Members

    public override void ShowPrintPreview()
    {
      pivotGridControl.ShowPrintPreview();
    }

    public override void ExportToXls(String fileName)
    {
      pivotGridControl.ExportToXls(fileName);
    }

    #endregion
  }
}
