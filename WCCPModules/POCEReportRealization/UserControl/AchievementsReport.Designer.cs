namespace WccpReporting.POCEReportRealization.UserControls
{
    partial class AchievementsReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.pivotGridFieldFactAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldCodeKPK = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKPI3 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldM3 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldDistrict = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
          this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKPI9 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKPI8 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKPI7 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKPI6 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKPI5 = new DevExpress.XtraPivotGrid.PivotGridField();
          this.pivotGridFieldKPI4 = new DevExpress.XtraPivotGrid.PivotGridField();
          ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
          this.SuspendLayout();
          // 
          // pivotGridFieldFactAddressTT
          // 
          this.pivotGridFieldFactAddressTT.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
          this.pivotGridFieldFactAddressTT.AreaIndex = 2;
          this.pivotGridFieldFactAddressTT.Caption = "����. ����� ��";
          this.pivotGridFieldFactAddressTT.FieldName = "OLADDRESS";
          this.pivotGridFieldFactAddressTT.Name = "pivotGridFieldFactAddressTT";
          this.pivotGridFieldFactAddressTT.Width = 200;
          // 
          // pivotGridFieldCodeKPK
          // 
          this.pivotGridFieldCodeKPK.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
          this.pivotGridFieldCodeKPK.AreaIndex = 0;
          this.pivotGridFieldCodeKPK.Caption = "��� ���";
          this.pivotGridFieldCodeKPK.FieldName = "M1_DEVICESERIALNUMBER";
          this.pivotGridFieldCodeKPK.Name = "pivotGridFieldCodeKPK";
          this.pivotGridFieldCodeKPK.Width = 200;
          // 
          // pivotGridFieldNameTT
          // 
          this.pivotGridFieldNameTT.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
          this.pivotGridFieldNameTT.AreaIndex = 1;
          this.pivotGridFieldNameTT.Caption = "����. ��� ��";
          this.pivotGridFieldNameTT.FieldName = "OLNAME";
          this.pivotGridFieldNameTT.Name = "pivotGridFieldNameTT";
          this.pivotGridFieldNameTT.Width = 150;
          // 
          // pivotGridFieldKPI3
          // 
          this.pivotGridFieldKPI3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldKPI3.AreaIndex = 6;
          this.pivotGridFieldKPI3.Caption = "���. ������������ ��� ���";
          this.pivotGridFieldKPI3.CellFormat.FormatString = "{0:N2}";
          this.pivotGridFieldKPI3.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldKPI3.FieldName = "EXEC_TOPSCUDISTR";
          this.pivotGridFieldKPI3.Name = "pivotGridFieldKPI3";
          // 
          // pivotGridFieldM1
          // 
          this.pivotGridFieldM1.AreaIndex = 4;
          this.pivotGridFieldM1.Caption = "M1";
          this.pivotGridFieldM1.FieldName = "M1_NAME";
          this.pivotGridFieldM1.Name = "pivotGridFieldM1";
          // 
          // pivotGridFieldM2
          // 
          this.pivotGridFieldM2.AreaIndex = 3;
          this.pivotGridFieldM2.Caption = "M2";
          this.pivotGridFieldM2.FieldName = "M2_NAME";
          this.pivotGridFieldM2.Name = "pivotGridFieldM2";
          // 
          // pivotGridFieldM3
          // 
          this.pivotGridFieldM3.AreaIndex = 2;
          this.pivotGridFieldM3.Caption = "M3";
          this.pivotGridFieldM3.FieldName = "M3_NAME";
          this.pivotGridFieldM3.Name = "pivotGridFieldM3";
          // 
          // pivotGridFieldDistrict
          // 
          this.pivotGridFieldDistrict.AreaIndex = 1;
          this.pivotGridFieldDistrict.Caption = "�������";
          this.pivotGridFieldDistrict.FieldName = "DISTRICT_NAME";
          this.pivotGridFieldDistrict.Name = "pivotGridFieldDistrict";
          // 
          // pivotGridFieldRegion
          // 
          this.pivotGridFieldRegion.AreaIndex = 0;
          this.pivotGridFieldRegion.Caption = "������";
          this.pivotGridFieldRegion.FieldName = "REGION_NAME";
          this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
          // 
          // pivotGridControl
          // 
          this.pivotGridControl.BackColor = System.Drawing.SystemColors.Control;
          this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
          this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldRegion,
            this.pivotGridFieldDistrict,
            this.pivotGridFieldM3,
            this.pivotGridFieldM2,
            this.pivotGridFieldM1,
            this.pivotGridField1,
            this.pivotGridFieldCodeKPK,
            this.pivotGridFieldNameTT,
            this.pivotGridFieldFactAddressTT,
            this.pivotGridFieldKPI3,
            this.pivotGridFieldKPI9,
            this.pivotGridFieldKPI8,
            this.pivotGridFieldKPI7,
            this.pivotGridFieldKPI6,
            this.pivotGridFieldKPI5,
            this.pivotGridFieldKPI4});
          this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
          this.pivotGridControl.Name = "pivotGridControl";
          this.pivotGridControl.Size = new System.Drawing.Size(1011, 293);
          this.pivotGridControl.TabIndex = 0;
          // 
          // pivotGridField1
          // 
          this.pivotGridField1.AreaIndex = 5;
          this.pivotGridField1.Caption = "��� ��";
          this.pivotGridField1.FieldName = "OLTYPE_NAME";
          this.pivotGridField1.Name = "pivotGridField1";
          // 
          // pivotGridFieldKPI9
          // 
          this.pivotGridFieldKPI9.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldKPI9.AreaIndex = 0;
          this.pivotGridFieldKPI9.Caption = "���. ���������������� � ��";
          this.pivotGridFieldKPI9.CellFormat.FormatString = "{0:N2}";
          this.pivotGridFieldKPI9.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldKPI9.FieldName = "EXEC_CFPRESNTATION";
          this.pivotGridFieldKPI9.Name = "pivotGridFieldKPI9";
          // 
          // pivotGridFieldKPI8
          // 
          this.pivotGridFieldKPI8.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldKPI8.AreaIndex = 1;
          this.pivotGridFieldKPI8.Caption = "���. ����";
          this.pivotGridFieldKPI8.CellFormat.FormatString = "{0:N2}";
          this.pivotGridFieldKPI8.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldKPI8.FieldName = "EXEC_RECPRICE";
          this.pivotGridFieldKPI8.Name = "pivotGridFieldKPI8";
          // 
          // pivotGridFieldKPI7
          // 
          this.pivotGridFieldKPI7.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldKPI7.AreaIndex = 2;
          this.pivotGridFieldKPI7.Caption = "���. �������� ��������";
          this.pivotGridFieldKPI7.CellFormat.FormatString = "{0:N2}";
          this.pivotGridFieldKPI7.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldKPI7.FieldName = "EXEC_QUALITYSTANDARD";
          this.pivotGridFieldKPI7.Name = "pivotGridFieldKPI7";
          // 
          // pivotGridFieldKPI6
          // 
          this.pivotGridFieldKPI6.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldKPI6.AreaIndex = 3;
          this.pivotGridFieldKPI6.Caption = "���. ���� ��������";
          this.pivotGridFieldKPI6.CellFormat.FormatString = "{0:N2}";
          this.pivotGridFieldKPI6.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldKPI6.FieldName = "EXEC_POSMSTANDARD";
          this.pivotGridFieldKPI6.Name = "pivotGridFieldKPI6";
          // 
          // pivotGridFieldKPI5
          // 
          this.pivotGridFieldKPI5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldKPI5.AreaIndex = 4;
          this.pivotGridFieldKPI5.Caption = "���. ���������������� �� �����";
          this.pivotGridFieldKPI5.CellFormat.FormatString = "{0:N2}";
          this.pivotGridFieldKPI5.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldKPI5.FieldName = "EXEC_SHELFPRESENTATION";
          this.pivotGridFieldKPI5.Name = "pivotGridFieldKPI5";
          // 
          // pivotGridFieldKPI4
          // 
          this.pivotGridFieldKPI4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
          this.pivotGridFieldKPI4.AreaIndex = 5;
          this.pivotGridFieldKPI4.Caption = "���. ���� ����";
          this.pivotGridFieldKPI4.CellFormat.FormatString = "{0:N2}";
          this.pivotGridFieldKPI4.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
          this.pivotGridFieldKPI4.FieldName = "EXEC_MUSTSTOCK";
          this.pivotGridFieldKPI4.Name = "pivotGridFieldKPI4";
          // 
          // AchievementsReport
          // 
          this.AutoSize = true;
          this.Controls.Add(this.pivotGridControl);
          this.Name = "AchievementsReport";
          this.Size = new System.Drawing.Size(1011, 293);
          ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldFactAddressTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCodeKPK;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNameTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPI3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistrict;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPI9;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPI8;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPI7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPI6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPI5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldKPI4;




    }
}
