﻿using System.Collections.Generic;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace WccpReporting.POCEReportRealization.Utility
{
	internal class GetFinalCheckedNodesOperation : TreeListOperation
	{
		#region Readonly & Static Fields

		private readonly TreeListColumn columnId;
		private readonly List<int> selectedIds = new List<int>();

		#endregion

		#region Constructors

		public GetFinalCheckedNodesOperation(TreeListColumn columnId)
		{
			this.columnId = columnId;
		}

		#endregion

		#region Instance Properties

		public List<int> SelectedIds
		{
			get { return selectedIds; }
		}

		#endregion

		#region Instance Methods

		public override void Execute(TreeListNode node)
		{
			if (node.Checked && !node.HasChildren)
				selectedIds.Add((int) node[columnId]);
		}

		#endregion
	}
}