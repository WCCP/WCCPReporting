﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.POCEReportRealization.Utility
{
	internal static class EnumerableHelpers
	{
		public static string ToString<TSource>(this IEnumerable<TSource> source, string delimeter)
		{
			if (source.Count() == 0) return string.Empty;

			StringBuilder sb = new StringBuilder();
			foreach (TSource src in source)
			{
				sb.AppendFormat("{0}{1}", src, delimeter);
			}
			return sb.ToString(0, sb.Length - delimeter.Length);
		}

		public static IEnumerable<int> ToInt(this IEnumerable<DayOfWeek> source)
		{
			foreach (DayOfWeek dayOfWeek in source)
			{
				switch (dayOfWeek)
				{
					case DayOfWeek.Monday:
						yield return 1;
						break;
					case DayOfWeek.Tuesday:
						yield return 2;
						break;
					case DayOfWeek.Wednesday:
						yield return 3;
						break;
					case DayOfWeek.Thursday:
						yield return 4;
						break;
					case DayOfWeek.Friday:
						yield return 5;
						break;
					case DayOfWeek.Saturday:
						yield return 6;
						break;
					case DayOfWeek.Sunday:
						yield return 7;
						break;
				}
			}
		}
	}
}