﻿using System;
using System.Data;
using System.Globalization;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using WccpReporting.POCEReportRealization.Properties;
using System.Collections.Generic;
using System.Linq;
using WccpReporting.POCEReportRealization.Utility;

namespace WccpReporting.POCEReportRealization.DataProvider
{
	internal static class DataAccessProvider
	{
		private const int COUNTRY_PARENT_ID = -1;
		private static DataTable regions;
		private static DataTable dates;
		private static DataTable channels;

		#region Settings form

		internal static int UserLavel
		{
			get
			{
				DataAccessLayer.OpenConnection();

				int level = (int) DataAccessLayer.ExecuteScalarQuery(SqlConstants.GET_CURRENT_USER_LEVEL);

				DataAccessLayer.CloseConnection();

				return level;
			}
		}

		internal static DataTable Regions
		{
			get
			{
				if (regions == null)
				{
					DataTable res = null;
					DataAccessLayer.OpenConnection();
					DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GET_REGIONS);

					if (null != ds && ds.Tables.Count > 0)
					{
						res = ds.Tables[0];
						res.Columns.Add(SqlConstants.GET_REGIONS_FLD_PARENT_ID, typeof (int));
						foreach (DataRow row in res.Rows)
						{
							row[SqlConstants.GET_REGIONS_FLD_PARENT_ID] = COUNTRY_PARENT_ID;
						}
						res.Rows.Add(new object[] {COUNTRY_PARENT_ID, Resources.Settings_AllCountry, DBNull.Value});
					}
					regions = res;
					DataAccessLayer.CloseConnection();
				}
				return regions;
			}
		}

		internal static DataTable Dates
		{
			get
			{
				if (dates == null)
				{
					DataTable lDates = new DataTable();
					lDates.Columns.Add(SqlConstants.GET_DATES_TREE_FLD_ID, typeof (int));
					lDates.Columns.Add(SqlConstants.GET_DATES_TREE_FLD_PARENT_ID, typeof (int));
					lDates.Columns.Add(SqlConstants.GET_DATES_TREE_FLD_NAME, typeof (string));

					DataTable dsDates = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GET_DATES).Tables[0];


					if (dsDates.Rows.Count > 0)
					{
						IEnumerable<int> dsY = dsDates.AsEnumerable().Select(row => row.Field<int>(SqlConstants.GET_DATES_FLD_YEAR)).Distinct().OrderBy(i => -1*i);
						foreach (int currentYear in dsY)
						{
							int[] dsM =
								dsDates
									.Select(String.Format("{0} = {1}", SqlConstants.GET_DATES_FLD_YEAR, currentYear))
									.Select(row => row.Field<int>(SqlConstants.GET_DATES_FLD_MONTH))
									.Distinct()
									.OrderBy(i => i)
									.ToArray();

							if (dsM.Length <= 0) continue;

							lDates.Rows.Add(new object[] {currentYear, DBNull.Value, currentYear.ToString()});
							foreach (int currentMonth in dsM)
							{
								string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentMonth);
								lDates.Rows.Add(new object[] {currentYear*100 + currentMonth, currentYear, monthName});
							}
						}
					}
					dates = lDates;
					DataAccessLayer.CloseConnection();
				}
				return dates;
			}
		}

		internal static DataTable Channels
		{
			get
			{
				if (channels == null)
				{
					DataAccessLayer.OpenConnection();
					DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GET_CHANNELS_LIST);
					if (null != ds && ds.Tables.Count > 0)
					{
						channels = ds.Tables[0];
					}
					DataAccessLayer.CloseConnection();
				}
				return channels;
			}
		}

		internal static string DateDate
		{
			get
			{
				DataAccessLayer.OpenConnection();
                var o = DataAccessLayer.ExecuteScalarQuery(SqlConstants.READ_DATE);
                DataAccessLayer.CloseConnection();
                
                if (o != null && o != DBNull.Value)
                    return o.ToString();
                return null;
			}
		}

	    #endregion

		#region Global form

		internal static void RecalculateData(DateTime date, int monthCount = 1) {
            try {
                //DataAccessLayer.ExecuteStoredProcedure("sp_DW_Visits_With_Invoices_Update", new SqlParameter("@isFullUpdate", (byte) 1));

                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpRecalculateData, 3600,
                    new[] {
                        new SqlParameter(SqlConstants.SpRecalculateDataParam1, date),
                        new SqlParameter(SqlConstants.SpRecalculateDataParam2, monthCount)
                    });

            } catch (Exception lException) {
		        if (lException.InnerException != null && lException.InnerException is SqlException) {
		            var lSqlException = (SqlException) lException.InnerException;
                    if (lSqlException.State == 100 && lSqlException.Class == 15) {
                        throw new ApplicationException(lSqlException.Message, lSqlException);
                    }
		        }
                throw;
		    }
		}

        internal static string IsExecutionAllowed()
        {
            string result = null;
            try
            {
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.IS_REPORT_EXECUTION_ALLOWED, 3600, new SqlParameter[] { });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null
                    && ex.InnerException is SqlException
                    && (((SqlException)ex.InnerException).State == 100
                    && ((SqlException)ex.InnerException).Class == 15))
                {
                    result = ex.InnerException.Message;
                }
                else
                {
                    throw;
                }
            }
            return result;
        }

		#endregion

		internal static DataSet GetRecommendedPriceData(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal)
		{
			DataAccessLayer.OpenConnection();

			DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpPoceRecommendedPrice,
                new[]
			        {
			            new SqlParameter(SqlConstants.ParamStaffLevelId, staffLevelId),
                        new SqlParameter(SqlConstants.ParamStaffId, staffId),
			            new SqlParameter(SqlConstants.ParamMonths, month.ToString(SqlConstants.LIST_SEPARATOR)),
			            new SqlParameter(SqlConstants.ParamIsMerch, typeOfPersonal)
			        }
				);

			DataAccessLayer.CloseConnection();

			return ds;
		}

        internal static DataTable GetExecutionData(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal, int channelType, int standardChannelType)
		{
		    DataAccessLayer.OpenConnection();

			DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GET_POC_EXECUTION,
			                                                    new[]
			                                                    	{
			                                                    		new SqlParameter(SqlConstants.ParamStaffLevelId, staffLevelId),
                                                                        new SqlParameter(SqlConstants.ParamStaffId, staffId),
			                                                    		new SqlParameter(SqlConstants.ParamMonths, month.ToString(SqlConstants.LIST_SEPARATOR)),
			                                                    		new SqlParameter(SqlConstants.ParamIsMerch, typeOfPersonal),
                                                                        new SqlParameter(SqlConstants.ParamChannelTypeId, channelType),
                                                                        new SqlParameter(SqlConstants.ParamStandardChannelTypeId, standardChannelType)
			                                                    	});

			
            if (null != ds && ds.Tables.Count > 0)
            {
                DataTable res = ds.Tables[0];
                DataRow[] dataRows = res
                    .Select(string.Format("[{0}] AND [{1}] NOT LIKE [{2}]", SqlConstants.GET_POC_EXECUTION_FLD_IS_OFFTRADEACHIVEMENTS, SqlConstants.GET_POC_EXECUTION_FLD_STANDART,
                                          SqlConstants.GET_POC_EXECUTION_FLD_KPI_NAME));
                IEnumerable<IGrouping<object, DataRow>> groupBy = dataRows
                    .GroupBy(row => row[SqlConstants.GET_POC_EXECUTION_FLD_STANDART]);
                StandartKPI = groupBy
                    .ToDictionary(
                        item => item.Key.ToString(),
                        item => item.Select(row => row[SqlConstants.GET_POC_EXECUTION_FLD_KPI_NAME].ToString()).Distinct().ToList());

                return res;
			}
			DataAccessLayer.CloseConnection();

			return null;
		}

        internal static DataTable GetTTBaseData(int staffLevelId, int staffId, IEnumerable<int> month, object typeOfPersonal, int channelType)
        {
            DataAccessLayer.OpenConnection();

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpPoceGetTtBase,
                                                                new[]
			                                                    	{
			                                                    		new SqlParameter(SqlConstants.ParamStaffLevelId, staffLevelId),
                                                                        new SqlParameter(SqlConstants.ParamStaffId, staffId),
			                                                    		new SqlParameter(SqlConstants.ParamMonths, month.ToString(SqlConstants.LIST_SEPARATOR)),
                                                                        new SqlParameter(SqlConstants.ParamIsMerch, typeOfPersonal),
                                                                        new SqlParameter(SqlConstants.ParamChannelTypeId, channelType)
			                                                    	});

            DataAccessLayer.CloseConnection();

            return ds.Tables[0];
        }
        // done
	    internal static string GetLastVisitDescription(bool isLastVisit)
	    {
	        return isLastVisit
	                   ? Resources.Execution_LastVisit_true
	                   : Resources.Execution_LastVisit_false;
	    }

	    public static Dictionary<string, List<string>> StandartKPI { get; private set; }

	    internal static DataTable GetKPIDescriptions(int channelType)
		{
			DataAccessLayer.OpenConnection();
			DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.KPI_DESCRIPTIONS, new[]
			                                                                                   	{
			                                                                                   		new SqlParameter(SqlConstants.KPI_DESCRIPTIONS_PARAM_1, channelType)
			                                                                                   	});
			DataAccessLayer.CloseConnection();

			if (ds != null && ds.Tables.Count > 0)
				return ds.Tables[0];
			return null;
		}
	}
}