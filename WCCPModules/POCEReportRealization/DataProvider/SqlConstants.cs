﻿namespace WccpReporting.POCEReportRealization.DataProvider
{
	public static class SqlConstants
	{
	    public const string READ_DATE =
            @"IF Not EXISTS (
        SELECT ParamValue
        FROM   DW_OffTrade_ReportSettings
        WHERE  ReportName    = 'POCE Pivot OFF'
               AND ParamName = 'LastDataUpdate'
       )
Select NULL
Else
        SELECT  ParamValue
        FROM   DW_OffTrade_ReportSettings
        WHERE  ReportName    = 'POCE Pivot OFF'
               AND ParamName = 'LastDataUpdate'";

	    public const string LIST_SEPARATOR = ",";

		#region Settings Form

		public const string GET_CURRENT_USER_LEVEL = "SELECT dbo.fnDW_URM_GetUserLevel(SUSER_SNAME())";

		public const string GET_REGIONS = "spDW_POCE_GetRegion";
		public const string GET_REGIONS_FLD_ID = "ID";
		public const string GET_REGIONS_FLD_PARENT_ID = "Parent_Id";
		public const string GET_REGIONS_FLD_NAME = "Name";

		public const string GET_DATES = "POCE.PIVOT_MONTHS_GetList";
		public const string GET_DATES_FLD_YEAR = "Y";
		public const string GET_DATES_FLD_MONTH = "M";

		public const string GET_DATES_TREE_FLD_ID = "ID";
		public const string GET_DATES_TREE_FLD_PARENT_ID = "Parent_Id";
		public const string GET_DATES_TREE_FLD_NAME = "Name";

		public const string GET_CHANNELS_LIST = "spDW_ChannelList";
		public const string GET_CHANNELS_FLD_ID = "ChanelType_id";
		public const string GET_CHANNELS_FLD_NAME = "ChanelType";

        public const string spStaffTree = "spDW_StaffTree";
        public const string spStaffTree_Param_ChanelTypeID = "@ChanelType_ID";
        public const string spStaffTree_ParentID = "Parent_ID";
	    public const string spStaffTree_Name = "Name";
        public const string spStaffTree_ItemID = "Item_ID";
	    public const string spStaffTree_StaffLevelID = "StaffLevel_ID";
        public const string spStaffTree_ID = "ID";

		#endregion

		#region Main functions

        public const string SpRecalculateData = "dbo.spSW_ReCalculateData";
        public const string SpRecalculateDataParam1 = "RecalcDate";
        public const string SpRecalculateDataParam2 = "monthCount";
        
        public const string IS_REPORT_EXECUTION_ALLOWED = "[POCE].[Check_Run_POCE_Reports]";
		
        public const string TYPES_OF_PERSONAL_FLD_ID = "ID";
		public const string TYPES_OF_PERSONAL_FLD_NAME = "Name";

		#endregion

		#region Таб Описание

		public const string KPI_DESCRIPTIONS = "POCE.KPI_Descriptions_GetList";
		public const string KPI_DESCRIPTIONS_PARAM_1 = "ChannelType_ID";

		#endregion

        #region Stored Proc and Params

        public const string SpPoceGetTtBase = "POCE.GetTTBase";
        public const string GET_POC_EXECUTION = "POCE.Page1";
        public const string SpPoceRecommendedPrice = "POCE.RecommendedPrice";

        public const string ParamStaffLevelId = "StaffLevel_ID";
        public const string ParamStaffId = "ID";
        public const string ParamMonths = "Months";
        public const string ParamIsMerch = "Is_Merch";
        public const string ParamChannelTypeId = "ChanelType_ID";
	    public const string ParamStandardChannelTypeId = "KPIChanelType_ID";

        #endregion

        #region Таб Выполнение

        //public const string GET_POC_EXECUTION_FLD_OL_ID = "ol_id";
        //public const string GET_POC_EXECUTION_FLD_DATE = "date";
		public const string GET_POC_EXECUTION_FLD_KPI_DOCUMENT_ID = "kpidocument_id";
		public const string GET_POC_EXECUTION_FLD_POCE_KPI_ID = "POCE_KPI_ID";
		public const string GET_POC_EXECUTION_FLD_KPI_ID = "kpi_id";
		public const string GET_POC_EXECUTION_FLD_STANDART = "Standart";
		public const string GET_POC_EXECUTION_FLD_STANDART_SHORT = "StandartShort";
		public const string GET_POC_EXECUTION_FLD_KPI_NAME = "KPIName";
		public const string GET_POC_EXECUTION_FLD_VALUE = "VALUE";
		public const string GET_POC_EXECUTION_FLD_IS_OFFTRADEACHIVEMENTS = "IS_OffTradeAchivements";
		public const string GET_POC_EXECUTION_FLD_ORDER_NO = "OrderNo";
		public const string GET_POC_EXECUTION_FLD_ORDER_NO_ACHIVEMENTS = "OffTradeAchivements_Field_Order";
		public const string GET_POC_EXECUTION_FLD_IS_EXECUTION = "IS_Execution";
        //public const string GET_POC_EXECUTION_FLD_EXEC_SD = "EXEC_SD";
        //public const string GET_POC_EXECUTION_FLD_EXEC_BSM = "EXEC_BSM";
        //public const string GET_POC_EXECUTION_FLD_LAST_VISIT_DESC = "LAST_VISIT_DESC";

		#endregion

	    #region Таб Рекомендованная цена

	    public const string GET_RECOMMENDED_PRICE_PARAM_1 = "Regions";
	    public const string GET_RECOMMENDED_PRICE_PARAM_2 = "Months";
	    public const string GET_RECOMMENDED_PRICE_PARAM_3 = "Is_Merch";

	    public const string GET_RECOMMENDED_PRICE_FLD_KPI = "KPIName";
	    public const string GET_RECOMMENDED_PRICE_FLD_SKU = "SKUName";
	    public const string GET_RECOMMENDED_PRICE_FLD_REAL = "Real_FieldName";
	    public const string GET_RECOMMENDED_PRICE_FLD_RECOMMENDED = "Recommended_FieldName";
	    public const string GET_RECOMMENDED_PRICE_FLD_EXECUTED = "Executed_FieldName";


	    #endregion
	}
}