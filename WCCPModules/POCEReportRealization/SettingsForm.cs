using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;
using WccpReporting.POCEReportRealization.DataProvider;
using WccpReporting.POCEReportRealization.Properties;
using WccpReporting.POCEReportRealization.Utility;

namespace WccpReporting.POCEReportRealization
{
	/// <summary>
	/// </summary>
	public partial class SettingsForm : XtraForm
	{
		#region Constructors

		public SettingsForm()
		{
			InitializeComponent();
			BuildSettings();
		}

		#endregion

		#region Instance Properties

		public int StaffChannelId
		{
			get { return (int) lookUpEditChannelList.EditValue; }
		}

		public int StandardChannelId
		{
			get { return (int) lookUpEditStandardChannelList.EditValue; }
		}

		public int[] Dates
		{
			get
			{
				GetFinalCheckedNodesOperation op = new GetFinalCheckedNodesOperation(treeListColumnDateID);
				treeListDate.NodesIterator.DoOperation(op);
				return op.SelectedIds.ToArray();
			}
		}

		public int[] Regions
		{
			get
			{
				GetFinalCheckedNodesOperation op = new GetFinalCheckedNodesOperation(treeListColumnRegionID);
				treeListRegions.NodesIterator.DoOperation(op);
				return op.SelectedIds.ToArray();
			}
		}

	    public int StaffLevelId {
	        get { return (int) ((IList<object>) staffLevelTree.Selection[0].Tag)[1]; }
	    }

	    public int StaffId {
            get { return (int) ((IList<object>) staffLevelTree.Selection[0].Tag)[2]; }
	    }

		#endregion

		#region Instance Methods

		private void BuildSettings()
		{
//			treeListRegions.KeyFieldName = SqlConstants.GET_REGIONS_FLD_ID;
//			treeListRegions.ParentFieldName = SqlConstants.GET_REGIONS_FLD_PARENT_ID;
//			treeListColumnRegionID.FieldName = SqlConstants.GET_REGIONS_FLD_ID;
//			treeListColumnRegionName.FieldName = SqlConstants.GET_REGIONS_FLD_NAME;
//			treeListRegions.DataSource = DataAccessProvider.Regions;
//			treeListRegions.ExpandAll();
//			treeListRegions.Nodes[0].Checked = true;
//			AfterCheckNode(treeListRegions, new NodeEventArgs(treeListRegions.Nodes[0]));
			int level = DataAccessProvider.UserLavel;
			if (level <= 3 && level >= 1) {
				treeListRegions.OptionsBehavior.Editable = false;
			}

			treeListDate.KeyFieldName = SqlConstants.GET_DATES_TREE_FLD_ID;
			treeListDate.ParentFieldName = SqlConstants.GET_DATES_TREE_FLD_PARENT_ID;
			treeListColumnDateID.FieldName = SqlConstants.GET_DATES_TREE_FLD_ID;
			treeListColumnDateName.FieldName = SqlConstants.GET_DATES_TREE_FLD_NAME;
			treeListDate.DataSource = DataAccessProvider.Dates;
			treeListDate.ExpandAll();
			treeListDate.Nodes[0].Nodes.LastNode.Checked = true;

			DataTable lStaffChannels = DataAccessProvider.Channels;
			lookUpEditChannelList.Properties.ValueMember = SqlConstants.GET_CHANNELS_FLD_ID;
			lookUpEditChannelList.Properties.DisplayMember = SqlConstants.GET_CHANNELS_FLD_NAME;
			lookUpEditChannelList.Properties.Columns[0].FieldName = SqlConstants.GET_CHANNELS_FLD_ID;
			lookUpEditChannelList.Properties.Columns[1].FieldName = SqlConstants.GET_CHANNELS_FLD_NAME;
			lookUpEditChannelList.Properties.DataSource = lStaffChannels;
			lookUpEditChannelList.EditValue = lStaffChannels.Rows[1][SqlConstants.GET_CHANNELS_FLD_ID];

            DataTable lStandardChannels = DataAccessProvider.Channels;
			lookUpEditStandardChannelList.Properties.ValueMember = SqlConstants.GET_CHANNELS_FLD_ID;
			lookUpEditStandardChannelList.Properties.DisplayMember = SqlConstants.GET_CHANNELS_FLD_NAME;
			lookUpEditStandardChannelList.Properties.Columns[0].FieldName = SqlConstants.GET_CHANNELS_FLD_ID;
			lookUpEditStandardChannelList.Properties.Columns[1].FieldName = SqlConstants.GET_CHANNELS_FLD_NAME;
			lookUpEditStandardChannelList.Properties.DataSource = lStandardChannels;
			lookUpEditStandardChannelList.EditValue = lStandardChannels.Rows[1][SqlConstants.GET_CHANNELS_FLD_ID];

			GetUpdData();
		}

		private void GetUpdData()
		{
            lblUpd2.Text = (DataAccessProvider.DateDate ?? "��� ������");
		}

		private bool ValidateData()
		{
			bool isValid = true;
			string errorText = string.Empty;

//			if (Regions.Length == 0)
//				errorText = String.Format(Resources.SettingsForm_ValidateData_RegionEmpty, errorText, Environment.NewLine);

			int datesLength = Dates.Length;

		    if (datesLength == 0)
		        errorText = String.Format(Resources.SettingsForm_ValidateData_MonthsEmpty, errorText, Environment.NewLine);

		    if (datesLength > 12)
		        errorText = String.Format(Resources.SettingsForm_ValidateData_MonthsGreater12, errorText, Environment.NewLine);

		    if (null == staffLevelTree.Selection || 0 == staffLevelTree.Selection.Count || String.IsNullOrEmpty(staffLevelTree.Selection[0].ToString()))
                errorText = Resources.IncorrectStaffLevel;

			if (!string.IsNullOrEmpty(errorText)) {
				XtraMessageBox.Show(errorText, Resources.SettingsForm_ValidateData_ErrorHeader, MessageBoxButtons.OK, MessageBoxIcon.Error);
				isValid = false;
			}
			return isValid;
		}

		#endregion

		#region Event Handling

		private void AfterCheckNode(object sender, NodeEventArgs e)
		{
			SetChildNodesStatus(e.Node);
			TreeListNode rootNode = e.Node.RootNode;
			rootNode.Checked = SetNodesState(rootNode);
		}

		private void btnYes_Click(object sender, EventArgs e)
		{
		    if (!ValidateData()) return;
		    
            var executionAllowed = DataAccessProvider.IsExecutionAllowed();
		    if (!string.IsNullOrEmpty(executionAllowed))
		    {
		        XtraMessageBox.Show(executionAllowed, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
		        return;
		    }
		    
            DialogResult = DialogResult.OK;
		}

		private void btn_upd_Click(object sender, EventArgs e) {
			WaitManager.StartWait();
		    try {
		        try {
		            int lMinMonth = Dates.Min() % 100 - 1;
		            int lMonthCnt = DateTime.Today.Month > lMinMonth ? DateTime.Today.Month - lMinMonth : DateTime.Today.Month + 12 - lMinMonth;
		            DataAccessProvider.RecalculateData(DateTime.Today, lMonthCnt);
		        } catch(Exception ex) {
		            XtraMessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
		        }
		    } finally {
		        WaitManager.StopWait();
		    }
	        GetUpdData();
		}

	    private void treeListRegions_BeforeCheckNode(object sender, CheckNodeEventArgs e)
		{
			if (!treeListRegions.OptionsBehavior.Editable)
				e.State = e.PrevState;
		}

		#endregion

		#region Class Methods

		private static void SetChildNodesStatus(TreeListNode node)
		{
			foreach (TreeListNode childNode in node.Nodes)
			{
				childNode.Checked = node.Checked;
				SetChildNodesStatus(childNode);
			}
		}

		private static bool SetNodesState(TreeListNode topNode)
		{
			if (!topNode.HasChildren)
			{
				return topNode.Checked;
			}
			bool isChecked = true;
			foreach (TreeListNode treeListNode in topNode.Nodes)
			{
				treeListNode.Checked = SetNodesState(treeListNode);
				isChecked = isChecked && treeListNode.Checked;
			}
			return isChecked;
		}

		#endregion

        private void AddChildNodes(TreeListNode parentNode, DataTable table) {
            foreach (DataRow dr in table.Rows) {
                if ((null == parentNode && dr[SqlConstants.spStaffTree_ParentID].Equals(0)) ||
                    (null != parentNode && dr[SqlConstants.spStaffTree_ParentID].Equals(((IList<object>) parentNode.Tag)[0]))) {
                        TreeListNode node = staffLevelTree.AppendNode(new[] { dr[SqlConstants.spStaffTree_Name] }, parentNode);
                    node.Tag = new List<object>
                                   {
                                       dr[SqlConstants.spStaffTree_ItemID],
                                       dr[SqlConstants.spStaffTree_StaffLevelID],
                                       dr[SqlConstants.spStaffTree_ID]
                                   };
                    AddChildNodes(node, table);
                }
            }
        }

        private void BuildTree(object id) {
            staffLevelTree.ClearNodes();
            DataAccessLayer.OpenConnection();
            DataSet ds1 = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.spStaffTree,
                                                                 null != id
                                                                     ? new[] { new SqlParameter(SqlConstants.spStaffTree_Param_ChanelTypeID, id) }
                                                                     : new SqlParameter[] { });
            DataAccessLayer.CloseConnection();
            AddChildNodes(null, ds1.Tables[0]);
        }

        private void lookUpEditChannelList_EditValueChanged(object sender, EventArgs e) {
            //
            BuildTree(StaffChannelId);
            staffLevelTree.ExpandAll();
        }

	}
}