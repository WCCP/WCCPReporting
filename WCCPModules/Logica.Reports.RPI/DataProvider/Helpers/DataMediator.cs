﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;

namespace Logica.Reports.RPI.DataProvider.Helpers
{
    public class DataMediator
    {
        public static int RPTCId = 1;

        /// <summary>
        /// Returns name of region which is specified by it's Id
        /// </summary>
        /// <param name="regionId">Id of region</param>
        /// <returns>Region Name</returns>
        public static string GetRegionName(int regionId)
        {
            string regionName = string.Empty;

            if (regionId > 0)
            {
                RegionsDataSet.RegionDataTable region = new RegionsDataProvider().GetData().Region;
                region.DefaultView.RowFilter =
                    string.Format("{0} = {1}", MappedNames.RegionId, regionId);
                DataView regionView = region.DefaultView;
                foreach (DataRowView rowView in regionView)
                {
                    if (!(rowView[MappedNames.RegionName] is DBNull))
                    {
                        regionName = rowView[MappedNames.RegionName].ToString();
                        break;
                    }
                }
            }

            return regionName;
        }

        /// <summary>
        /// Returns data row for Region according to specified Region_Id
        /// </summary>
        /// <param name="regionId">Id of Region</param>
        /// <returns>Data Row</returns>
        public static DataRow GetRegionRow(int regionId)
        {
            DataRow regionRow = null;

            if (regionId > 0)
            {
                RegionsDataSet.RegionDataTable region = new RegionsDataProvider().GetData().Region;
                region.DefaultView.RowFilter =
                    string.Format("{0} = {1}", MappedNames.RegionId, regionId);
                DataView regionView = region.DefaultView;
                foreach (DataRowView rowView in regionView)
                {
                        regionRow = rowView.Row;
                        break;
                }
            }

            return regionRow;
        }

        /// <summary>
        /// Returns list of DSM's for specified RM_Id
        /// </summary>
        /// <param name="rmId">RM_Id</param>
        /// <returns>List of DSM's</returns>
        public static List<DSMEntity> GetRegionDSM(int rmId)
        {
            return rmId > 0 ? new DSMDataProvider().GetList(string.Format("{0} = {1}", MappedNames.DSMRMId, rmId)) : new DSMDataProvider().GetList();
        }

        /// <summary>
        /// Returns DSM list filtered according to Region Id
        /// </summary>
        /// <param name="regionId">Id of region</param>
        /// <returns>List of DSMEntity</returns>
        public static List<DSMEntity> GetDSMList(int regionId)
        {
            DataRow row = DataMediator.GetRegionRow(regionId);
            if (row != null)
            {
                int rmId = DataConvertorHelper.GetDBNullInt32Checked(row[MappedNames.RegionRMId]);
                return DataMediator.GetRegionDSM(rmId);
            }
            else
            {
                return new DSMDataProvider().GetList();
            }
        }

        /// <summary>
        /// Returns Supervisors list filtered according to Region Id
        /// </summary>
        /// <param name="regionId">Id of region</param>
        /// <returns>List of Supervisor Entity</returns>
        public static List<SupervisorEntity> GetSupervisorList(int regionId)
        {
            if (regionId > 0)
            {
                return new SupervisorsDataProvider().GetList(string.Format("{0} = {1}", MappedNames.SupervisorRegionId, regionId));
            }
            else 
            {
                return new SupervisorsDataProvider().GetList();
            }
        }

        /// <summary>
        /// Check Aims for having crossed periods
        /// </summary>
        /// <param name="checkedAim">Aim for check</param>
        /// <param name="errors">Errors list</param>
        public static void CheckForCrossedAimsErrors(CascadedAimEntity checkedAim, List<string> errors)
        {
            AimsDataSet aims = new CascadedAimsDataProvider().GetCrossedAims(checkedAim);
            DataView view = aims.Aim.DefaultView;

            if (checkedAim.KPITypeId == RPTCId &&
                checkedAim.DSMId == -1 && checkedAim.SupervisorId == -1
                && checkedAim.SettlementId == -1 && checkedAim.ProximityId == -1
                && checkedAim.OlGroupId == -1 && checkedAim.OlTypeId == -1 && checkedAim.OlSubTypeId == -1)
            {
                if (checkedAim.AimId == int.MinValue)
                {
                    view.RowFilter = string.Format("{0} = {1} AND {2} = {3}",
                        MappedNames.IsFocused, false.ToString(),
                        MappedNames.IsBaseSKU, true.ToString());
                }
                else if (checkedAim.AimId > 0)
                {
                    view.RowFilter = string.Format("{0} = {1} AND {2} <> {3} AND {4} = {5}",
                        MappedNames.IsFocused, false.ToString(),
                        MappedNames.AimId, checkedAim.AimId,
                        MappedNames.IsBaseSKU, true.ToString());
                }

                if (view.Count > 0)
                {
                    errors.Add(
                                string.Format(
                                "Базовое СКЮ <{0}> с KPI <{1}> и периодом действия с <{2}> по <{3}> совпадает с",
                                checkedAim.ProductCombineName,
                                checkedAim.KPITypeName,
                                DateHelper.PeriodBorderToString(checkedAim.DateStartPeriod),
                                DateHelper.PeriodBorderToString(checkedAim.DateEndPeriod)
                            )
                        );
                    foreach (DataRowView rowView in view)
                    {
                        errors.Add(
                            string.Format(
                                "       СКЮ <{0}> KPI <{1}> и периодом с <{2}> по <{3}> ",
                                rowView[MappedNames.ProductCombineName],
                                rowView[MappedNames.KPITypeName],
                                DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateStartPeriod])),
                                DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateEndPeriod]))
                            )
                            );
                    }
                }
            }
            else
            {
                if (checkedAim.AimId == int.MinValue)
                {
                    view.RowFilter = string.Format("{0} = {1} AND {2} = {3}",
                        MappedNames.IsFocused, false.ToString(),
                        MappedNames.IsBaseSKU, false.ToString());
                }
                else if (checkedAim.AimId > 0)
                {
                    view.RowFilter = string.Format("{0} = {1} AND {2} <> {3} AND {4} = {5}", 
                        MappedNames.IsFocused, false.ToString(), 
                        MappedNames.AimId, checkedAim.AimId,
                        MappedNames.IsBaseSKU, false.ToString());
                }
                //view.RowFilter = string.Format("{0} = {1}", MappedNames.IsFocused, false.ToString());

                if (view.Count > 0)
                {
                    errors.Add(
                                string.Format(
                                "Для СКЮ <{0}> с KPI <{1}> и периодом действия с <{2}> по <{3}> совпадает с",
                                checkedAim.ProductCombineName,
                                checkedAim.KPITypeName,
                                DateHelper.PeriodBorderToString(checkedAim.DateStartPeriod),
                                DateHelper.PeriodBorderToString(checkedAim.DateEndPeriod)
                            )
                        );
                    foreach (DataRowView rowView in view)
                    {
                        errors.Add(
                            string.Format(
                                "       СКЮ <{0}> KPI <{1}> и периодом с <{2}> по <{3}> ",
                                rowView[MappedNames.ProductCombineName],
                                rowView[MappedNames.KPITypeName],
                                DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateStartPeriod])),
                                DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateEndPeriod]))
                            )
                            );
                    }
                }
            }
        }

        /// <summary>
        /// Check Aims for having similar aim
        /// </summary>
        /// <param name="checkedAim">Aim for check</param>
        /// <param name="errors">Erros list</param>
        public static void CheckForSameAimsErrors(CascadedAimEntity checkedAim, List<string> errors)
        {
            AimsDataSet aims = new CascadedAimsDataProvider().GetIdenticalAims(checkedAim);
            DataView view = aims.Aim.DefaultView;

            if (checkedAim.AimId == int.MinValue)
            {
                view.RowFilter = string.Format("{0} = {1}", MappedNames.IsFocused, false.ToString());
            }
            else if (checkedAim.AimId > 0)
            {
                view.RowFilter = string.Format("{0} = {1} AND {2} <> {3}", MappedNames.IsFocused, false.ToString(), MappedNames.AimId, checkedAim.AimId);
            } 
            
            if (view.Count > 0)
            {
                errors.Add(
                            string.Format(
                            "Каскадированная цель с СКЮ <{0}> с KPI <{1}> и периодом действия с <{2}> по <{3}> совпадает с целями",
                            checkedAim.ProductCombineName,
                            checkedAim.KPITypeName,
                            DateHelper.PeriodBorderToString(checkedAim.DateStartPeriod),
                            DateHelper.PeriodBorderToString(checkedAim.DateEndPeriod)
                        )
                    );
                foreach (DataRowView rowView in view)
                {
                    errors.Add(
                        string.Format(
                            "       СКЮ <{0}> KPI <{1}> и периодом с <{2}> по <{3}> ",
                            rowView[MappedNames.ProductCombineName],
                            rowView[MappedNames.KPITypeName],
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateStartPeriod])),
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateEndPeriod]))
                        )
                        );
                }
            }
        }

    }

}
