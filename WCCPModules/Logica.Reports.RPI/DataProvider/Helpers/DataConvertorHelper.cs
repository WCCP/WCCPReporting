﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.Localization;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using DevExpress.XtraGrid;

namespace Logica.Reports.RPI.DataProvider.Helpers
{
    /// <summary>
    /// Helper class for data converting and miscellaneous helper functionality
    /// </summary>
    public class DataConvertorHelper
    {
        /// <summary>
        /// Compose TreeView base on the parent->child table relations into dataSet
        /// </summary>
        /// <param name="dataSet">DataSet which contains linked tables</param>
        /// <param name="rootTableName">Root table name</param>
        /// <param name="tableNameShowedColumnMapping">Column mapping</param>
        /// <returns></returns>
        public static TreeNode[] GetTreeNodes(DataSet dataSet, string rootTableName, IDictionary<string, string> tableNameShowedColumnMapping)
        {
            IList<TreeNode> treeNodeList = new List<TreeNode>();
            TreeNode[] nodesArray = null;

            if (dataSet.Tables.Contains(rootTableName))
            {

                foreach (DataRow dataRow in dataSet.Tables[rootTableName].Rows)
                {
                    TreeNode treeNode = new TreeNode(dataRow[tableNameShowedColumnMapping[rootTableName]].ToString());
                    treeNode.Tag = dataRow;

                    ProcessChildRows(treeNode, dataRow, tableNameShowedColumnMapping);

                    treeNodeList.Add(treeNode);
                }

            }

            if (treeNodeList.Count > 0)
            {
                nodesArray = new TreeNode[treeNodeList.Count];
                treeNodeList.CopyTo(nodesArray, 0);
            }

            return nodesArray;
        }

        /// <summary>
        /// Extract child row info from parent row in order to put them into TreeView object
        /// </summary>
        /// <param name="parentNode">Parent TreeView node</param>
        /// <param name="parentRow">Parent DataRow</param>
        /// <param name="tableNameShowedColumnMapping">Data Column mapping</param>
        private static void ProcessChildRows(TreeNode parentNode, DataRow parentRow, IDictionary<string, string> tableNameShowedColumnMapping)
        {
            foreach (DataRelation dataRelation in parentRow.Table.ChildRelations)
            {
                DataRow[] childRows = parentRow.GetChildRows(dataRelation);
                foreach (DataRow childRow in childRows)
                {
                    TreeNode childNode = new TreeNode(childRow[tableNameShowedColumnMapping[childRow.Table.TableName]].ToString());
                    childNode.Tag = childRow;

                    ProcessChildRows(childNode, childRow, tableNameShowedColumnMapping);

                    parentNode.Nodes.Add(childNode);
                }
            }


        }

        /// <summary>
        /// Returns entity id from combobox selected item
        /// </summary>
        /// <typeparam name="T">Type of object which contains data</typeparam>
        /// <param name="selectedItemIndex">Selected item index for combobox</param>
        /// <param name="selectedItem">Selected item</param>
        /// <returns>Entity Id</returns>
        public static long GetIdFromComboboxSelectedItem<T>(int selectedItemIndex, object selectedItem) where T : IBaseEntity
        {
            if (selectedItemIndex > -1)
            {
                if (selectedItem != null && selectedItem is T)
                {
                    return (selectedItem as IBaseEntity).Id;
                }
            }

            return -1;
        }

        /// <summary>
        /// Returns entity name from combobox selected item
        /// </summary>
        /// <typeparam name="T">Type of object which contains data</typeparam>
        /// <param name="selectedItemIndex">Selected item index for combobox</param>
        /// <param name="selectedItem">Selected item</param>
        /// <returns>Entity Name</returns>
        public static string GetNameFromComboboxSelectedItem<T>(int selectedItemIndex, object selectedItem) where T : IBaseEntity
        {
            if (selectedItemIndex > -1)
            {
                if (selectedItem != null && selectedItem is T)
                {
                    return (selectedItem as IBaseEntity).Name;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Populates ComboBoxItemCollection from typed collection
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="collection">ComboBoxItemCollection</param>
        /// <param name="dataList">typed collection of data entity</param>
        /// <param name="args">additional parameters </param>
        public static void PopulateList<T>(DevExpress.XtraEditors.Controls.ComboBoxItemCollection collection, IList<T> dataList, params object[] args)
        {
            collection.Clear();
            bool addFirstEmptyItem = false;

            addFirstEmptyItem = ((args == null) || (args != null && args.Length == 0));
            if (args != null && args.Length > 0)
            {
                if (args[0] is bool)
                    addFirstEmptyItem = Convert.ToBoolean(args[0]);
            }

            if (addFirstEmptyItem)
            {
                collection.Add(Resource.ComboBoxItemAnyValueText);
            }

            if (dataList != null)
            {
                foreach (T dataItem in dataList)
                {
                    collection.Add(dataItem);
                }
            }
        }

        /// <summary>
        /// Check Int32 value from DataRow Column. 
        /// If Column is DBNull it returns -1 otherwise - converts object value to Int32.
        /// </summary>
        /// <param name="value">Int32 Column from DataRow</param>
        /// <returns>Int32 value</returns>
        public static int GetDBNullInt32Checked(object value)
        {
            int result = -1;
            if (!(value is DBNull))
            {
                try
                {
                    result = Convert.ToInt32(value);
                }
                catch
                {
                    result = -1;
                }
            }
            return result;
        }

        /// <summary>
        /// Checks source string for matching the pattern of numeric value 
        /// (according to specified regular expression)
        /// </summary>
        /// <param name="sourceString">Checked string</param>
        /// <returns>Correct String representation of number or Null</returns>
        public static string GetNumberString(string sourceString)
        {
            string regexDecimalNumberPattern = @"^[0-9]*\.?[0-9]*$";

            return string.IsNullOrEmpty(sourceString)
            ? null
            : new Regex(regexDecimalNumberPattern).IsMatch(sourceString) ? sourceString : null;
        }

        /// <summary>
        /// Hides GridView columns that do not contain data
        /// </summary>
        /// <param name="gridControl">Source grid control</param>
        /// <param name="gridColumns">Set of grid columns for showing in export</param>
        public static void HideEmptyColumns(GridControl gridControl, GridColumn[] gridColumns)
        {
            if (gridControl.DataSource != null)
            {
                // we will print only columns with data
                Dictionary<string, bool> columnFieldNamesOmmitedInExport = GetColumnWithNoData(gridControl, gridColumns);

                foreach (GridColumn column in gridColumns)
                {
                    if (columnFieldNamesOmmitedInExport[column.FieldName])
                    {
                        column.OptionsColumn.Printable = DefaultBoolean.False;
                    }
                }
            }
        }

        private static Dictionary<string, bool> GetColumnWithNoData(DevExpress.XtraGrid.GridControl gridControl, GridColumn[] gridColumns)
        {
            object tblAims = gridControl.DataSource;

            Dictionary<string, bool> columnFieldNamesOmmitedInExport = new Dictionary<string, bool>();
            List<string> columnFieldNames = new List<string>();

            foreach (GridColumn column in gridColumns)
            {
                columnFieldNamesOmmitedInExport.Add(column.FieldName, true);
                columnFieldNames.Add(column.FieldName);
            }

            if (tblAims is DataTable)
            {
                foreach (DataRow row in ((DataTable)tblAims).Rows)
                {
                    foreach (string name in columnFieldNames)
                    {
                        if (row[name] != DBNull.Value && !(row[name] is System.Data.SqlTypes.SqlInt32 && Convert.ToInt32(row[name]) == -1))
                        {
                            columnFieldNamesOmmitedInExport[name] = false;
                        }
                    }
                }
            }
            else if (tblAims is DataView)
            {
                foreach (DataRowView rowView in (tblAims as DataView))
                {
                    foreach (string name in columnFieldNames)
                    {
                        if (rowView[name] != DBNull.Value && !(rowView[name] is System.Data.SqlTypes.SqlInt32 && Convert.ToInt32(rowView[name]) == -1))
                        {
                            columnFieldNamesOmmitedInExport[name] = false;
                        }
                    }
                }
            }



            return columnFieldNamesOmmitedInExport;
        }
    }

}
