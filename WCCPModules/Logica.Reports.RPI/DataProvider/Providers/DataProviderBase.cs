﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.Common;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    public abstract class DataProviderBase : IDisposable
    {
        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public abstract string ModuleName
        {
            get;
        }

        /// <summary>
        /// Gets the dictionary of mappings between the source tables and the dataset tables.
        /// </summary>
        /// <returns>Dictionary of table mapping.</returns>
        protected abstract IDictionary<string, DataTableMapping> TableMapping { get; }
        
        /// <summary>
        /// Stores table-fields mapping for DataProvider
        /// </summary>
        protected IDictionary<string, DataTableMapping> _tableMapping;
        /// <summary>
        /// Stores the current SQLConnection.
        /// </summary>
        private SqlConnection _databaseConnection;
        /// <summary>
        /// Gets the database connection.
        /// </summary>
        /// <value>The database connection.</value>
        protected SqlConnection DatabaseConnection
        {
            get
            {
                if (_databaseConnection == null)
                {
                    _databaseConnection = new SqlConnection(DataAccess.DataAccessLayer.ConnectionString.ToString() );
                }
                return _databaseConnection;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _databaseConnection.Close();
        }
    }
}

