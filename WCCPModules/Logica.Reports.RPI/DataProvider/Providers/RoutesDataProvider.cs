﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class RoutesDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "Routes";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public RoutesDataSet GetData()
        {
            RoutesDataSet dataSet = new RoutesDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.Routes, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.Text; //CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[Route.DataSetTable]);
                adapter.Fill(dataSet, Route.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Gets the report data filled with Unique Route Names.
        /// </summary>
        /// <returns></returns>
        public RoutesDataSet GetUniqueNamesData()
        {
            RoutesDataSet dataSet = new RoutesDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.UniqueRouteNames, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.Text; //CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[Route.DataSetTable]);
                adapter.Fill(dataSet, Route.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }


        /// <summary>
        /// Returns list of Routes
        /// </summary>
        /// <returns>Routes list</returns>
        public List<RouteEntity> GetList()
        {
            return GetFilledList(GetData().Route.DefaultView);
        }

        /// <summary>
        /// Returns filtered Routes list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Routes list</returns>
        public List<RouteEntity> GetList(string filterExpression)
        {
            RoutesDataSet routes = GetData();

            if (!string.IsNullOrEmpty(filterExpression))
                routes.Route.DefaultView.RowFilter = filterExpression;

            return GetFilledList(routes.Route.DefaultView);
        }

        /// <summary>
        /// Returns filtered Routes list only with Distinct route names
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Routes list</returns>
        public List<RouteEntity> GetNamesList(string filterExpression)
        {
            RoutesDataSet routes = GetData();

            if (!string.IsNullOrEmpty(filterExpression))
                routes.Route.DefaultView.RowFilter = filterExpression;

            return  GetFilledNamesList(routes.Route.DefaultView);//GetFilledList(routes.Route.DefaultView);
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(Route.SourceTable, Route.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               RouteId, RouteName, MerchId, CityId
                            }
                        );

                    _tableMapping.Add(Route.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping Route = new DataTableMapping(MappedNames.RouteSourceTable, MappedNames.RouteTable);

        protected DataColumnMapping RouteId = new DataColumnMapping("Route_id", MappedNames.RouteId);
        protected DataColumnMapping RouteName = new DataColumnMapping("RouteName", MappedNames.RouteName);
        protected DataColumnMapping MerchId = new DataColumnMapping("Merch_id", MappedNames.MerchId);
        protected DataColumnMapping CityId = new DataColumnMapping("City_id", MappedNames.CityId);
        protected DataColumnMapping IsOrdered = new DataColumnMapping("isOrdered", MappedNames.IsOrdered);
        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);        

        #endregion Protected members

        #region Private methods

        private List<RouteEntity> GetFilledList(DataView routesView)
        {
            List<RouteEntity> result = null;

            try
            {
                foreach (DataRowView route in routesView)
                {
                    if (result == null)
                    {
                        result = new List<RouteEntity>();
                    }

                    result.Add(
                                new RouteEntity
                                    (
                                        long.Parse(route[RouteId.DataSetColumn].ToString()),
                                        route[RouteName.DataSetColumn].ToString(),
                                        DataConvertorHelper.GetDBNullInt32Checked(route[MerchId.DataSetColumn]),
                                        DataConvertorHelper.GetDBNullInt32Checked(route[CityId.DataSetColumn]),
                                        false,//bool.Parse(route[IsOrdered.DataSetColumn].ToString()),
                                        -1//int.Parse(route[Status.DataSetColumn].ToString())
                                    )
                               );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }

            return result;
        }

        private List<RouteEntity> GetFilledNamesList(DataView routesView)
        {
            List<RouteEntity> result = null;
            long counter = 0;
            foreach (DataRowView route in routesView)
            {
                if (result == null)
                {
                    result = new List<RouteEntity>();
                }

                result.Add(
                            new RouteEntity
                                (
                                    ++counter,
                                    route[RouteName.DataSetColumn].ToString(),
                                    -1,
                                    -1,
                                    false,
                                    -1
                                )
                           );
            }
            return result;
        }

        #endregion Private method
    }

    public class RouteEntity : IBaseEntity
    {
        private long _id;
        private string _name;
        private int _merchId;
        private int _cityId;
        private bool _isOrdered;
        private int _status;

        public RouteEntity() { }

        public RouteEntity(long id, string name, int merchId, int cityId, bool isOrdered, int status)
            : this()
        {
            _id = id;
            _name = name;
            _merchId = merchId;
            _cityId = cityId;
            _isOrdered = isOrdered;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int MerchId
        {
            get { return _merchId; }
            set { _merchId = value; }
        }

        public int CityId
        {
            get { return _cityId; }
            set { _cityId = value; }
        }

        public bool IsOrdered
        {
            get { return _isOrdered; }
            set { _isOrdered = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }

}
