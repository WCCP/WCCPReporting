﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;


namespace Logica.Reports.RPI.DataProvider.Providers
{
    public class SettlementDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "Settlements";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public SettlementsDataSet GetData()
        {
            SettlementsDataSet dataSet = new SettlementsDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.Settlements, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[Settlement.DataSetTable]);
                adapter.Fill(dataSet, Settlement.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Settlements
        /// </summary>
        /// <returns>Settlements list</returns>
        public List<SettlementEntity> GetList()
        {
            return GetFilledList(GetData().Settlement.DefaultView);
        }

        /// <summary>
        /// Returns filtered Settlements list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Settlements list</returns>
        public List<SettlementEntity> GetList(string filterExpression)
        {
            SettlementsDataSet settlements = GetData();

            settlements.Settlement.DefaultView.RowFilter = filterExpression;

            return GetFilledList(settlements.Settlement.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping settlementMap = new DataTableMapping(Settlement.SourceTable, Settlement.DataSetTable);
                    settlementMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                                SettlementId, SettlementName, Status
                            }
                        );

                    _tableMapping.Add(Settlement.DataSetTable, settlementMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping Settlement = new DataTableMapping(MappedNames.SettlementSourceTable, MappedNames.SettlementTable);

        protected DataColumnMapping SettlementId = new DataColumnMapping("Settlement_id", MappedNames.SettlementId);
        protected DataColumnMapping SettlementName = new DataColumnMapping("Settlement", MappedNames.Settlement);

        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping ULM = new DataColumnMapping("ULM", MappedNames.ULM);

        #endregion Protected members

        #region Private methods

        private List<SettlementEntity> GetFilledList(DataView settlement)
        {
            List<SettlementEntity> result = null;

            try
            {
                foreach (DataRowView row in settlement)
                {
                    if (result == null)
                    {
                        result = new List<SettlementEntity>();
                    }

                    result.Add(
                                new SettlementEntity
                                    (
                                        long.Parse(row[SettlementId.DataSetColumn].ToString()),
                                        row[SettlementName.DataSetColumn].ToString(),
                                        DateTime.MinValue, // DateTime.Parse(row[DLM.DataSetColumn].ToString()),
                                        "",//row[ULM.DataSetColumn].ToString(),
                                        row[Status.DataSetColumn] is DBNull ? false : bool.Parse(row[Status.DataSetColumn].ToString())
                                    )
                               );
                }
            }
            catch(Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }
            return result;
        }

        #endregion Private methods
    }

    public class SettlementEntity : IBaseEntity
    {
        private long _id;
        private string _name;
        private DateTime _DLM;
        private string _ULM;
        private bool _status;

        public SettlementEntity() { }

        public SettlementEntity(long id, string name, DateTime dlm, string ulm, bool status)
            : this()
        { 
            _id = id;
            _name = name;
            _DLM = dlm;
            _ULM = ulm;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public string ULM
        {
            get { return _ULM; }
            set { _ULM = value; }
        }

        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }

}
