﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider.Entities;
using System.Windows.Forms;


namespace Logica.Reports.RPI.DataProvider.Providers
{
    public class RegionsDataProvider : DataProviderBase
    {
        #region Private fields
        
        private RegionsDataSet _dataSet;
        private TerritoryData _territoryData;

        #endregion Private fields

        #region Public properties

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override string ModuleName {
            get {
                return "Regions";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public RegionsDataSet GetData() {
            if (null != _dataSet)
                return _dataSet;

            _dataSet = new RegionsDataSet();

            try {
                SqlDataAdapter lAdapter = new SqlDataAdapter(); 
                SqlCommand lSelectCommand = new SqlCommand(ScriptProvider.RegionsCountries, DatabaseConnection);
                lSelectCommand.CommandTimeout = 30;
                lSelectCommand.CommandType = CommandType.StoredProcedure;

                lAdapter.SelectCommand = lSelectCommand;
                lAdapter.TableMappings.Add(TableMapping[Country.DataSetTable]);
                lAdapter.Fill(_dataSet, Country.SourceTable);

                lSelectCommand.CommandText = ScriptProvider.RegionsRegions;
                lAdapter.TableMappings.Add(TableMapping[Region.DataSetTable]);
                lAdapter.Fill(_dataSet, Region.SourceTable);

                lSelectCommand.CommandText = ScriptProvider.RegionsDistricts;
                lAdapter.TableMappings.Add(TableMapping[District.DataSetTable]);
                lAdapter.Fill(_dataSet, District.SourceTable);

                lSelectCommand.CommandText = ScriptProvider.RegionsCities;
                lAdapter.TableMappings.Add(TableMapping[City.DataSetTable]);
                lAdapter.Fill(_dataSet, City.SourceTable);
            }
            catch (SystemException lException) {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, lException.Message));
            }
            finally {
                DatabaseConnection.Close();
            }
            //GetTerritoryData();
            return _dataSet;
        }

        public DataTable GetTerritoryData() {
            if (null != _territoryData)
                return _territoryData.TerritoryTable;

            _territoryData = new TerritoryData();
            GetData();
            int lId;
            int lParentId = 0;
            string lName;
            int lItemId = 0;
            int lLevel = 1;
            
            //Country - Level 1
            foreach (DataRow lCountryRow in _dataSet.Country.Rows) {
                lItemId = lId = Convert.ToInt32(lCountryRow[MappedNames.CountryId]);
                if (lId <= 0)
                    continue;
                lName = lCountryRow[MappedNames.CountryName].ToString();
                _territoryData.TerritoryTable.AddTerritoryTableRow(lId, lParentId, lName, lLevel, lItemId);
            }
            _territoryData.TerritoryTable.AcceptChanges();

            //Regions - Level 2
            lLevel = 2;
            foreach (DataRow lRegionRow in _dataSet.Region.Rows) {
                if (lRegionRow[MappedNames.CountryId] is DBNull)
                    continue;
                if (Convert.ToInt32(lRegionRow[MappedNames.CountryId]) <= 0)
                    continue;
                lName = lRegionRow[MappedNames.RegionName].ToString();
                lParentId = Convert.ToInt32(lRegionRow[MappedNames.CountryId]);
                lItemId = Convert.ToInt32(lRegionRow[MappedNames.RegionId]);
                lId = 10 + lItemId;
                _territoryData.TerritoryTable.AddTerritoryTableRow(lId, lParentId, lName, lLevel, lItemId);
            }
            _territoryData.TerritoryTable.AcceptChanges();

            //Districts - Level 3
            lLevel = 3;
            foreach(DataRow lDistrictRow in _dataSet.District.Rows) {
                if (lDistrictRow[MappedNames.RegionId] is DBNull)
                    continue;
                if (Convert.ToInt32(lDistrictRow[MappedNames.RegionId]) <= 0)
                    continue;
                lName = lDistrictRow[MappedNames.DistrictName].ToString();
                lParentId = 10 + Convert.ToInt32(lDistrictRow[MappedNames.RegionId]);
                lItemId = Convert.ToInt32(lDistrictRow[MappedNames.DistrictId]);
                lId = 100 + lItemId;
                _territoryData.TerritoryTable.AddTerritoryTableRow(lId, lParentId, lName, lLevel, lItemId);
            }
            _territoryData.TerritoryTable.AcceptChanges();

            //Cities - Level 4
            lLevel = 4;
            foreach (DataRow lCityRow in _dataSet.City.Rows)
            {
                if (lCityRow[MappedNames.DistrictId] is DBNull)
                    continue;
                if (Convert.ToInt32(lCityRow[MappedNames.DistrictId]) <= 0)
                    continue;
                lName = lCityRow[MappedNames.CityName].ToString();
                lParentId = 100 + Convert.ToInt32(lCityRow[MappedNames.DistrictId]);
                lItemId = Convert.ToInt32(lCityRow[MappedNames.CityId]);
                lId = 100000 + lItemId;
                _territoryData.TerritoryTable.AddTerritoryTableRow(lId, lParentId, lName, lLevel, lItemId);
            }
            _territoryData.TerritoryTable.AcceptChanges();

            return _territoryData.TerritoryTable;
        }

        /// <summary>
        /// Returns region structure as Tree
        /// </summary>
        /// <returns>Array of Tree Nodes</returns>
        public TreeNode[] GetTree()
        {
            RegionsDataSet lDataSet = GetData();

            List<TreeNode> lNodes = new List<TreeNode>();
            //Country
            foreach (DataRow lCountryRow in lDataSet.Country.Rows) {
                TreeNode lCountryNode = new TreeNode();
                lCountryNode.Text = lCountryRow[MappedNames.CountryName] is DBNull ? " " :lCountryRow[MappedNames.CountryName].ToString();
                lCountryNode.Tag = lCountryRow;
                //Region
                foreach (DataRow lRegionRow in lDataSet.Region.Rows) {
                    if ((lRegionRow[MappedNames.CountryId] is DBNull)
                        || Convert.ToInt32(lCountryRow[MappedNames.CountryId]) != Convert.ToInt32(lRegionRow[MappedNames.CountryId]))
                        continue;

                    TreeNode lRegionNode = new TreeNode();
                    lRegionNode.Text = lRegionRow[MappedNames.RegionName] is DBNull ? " " : lRegionRow[MappedNames.RegionName].ToString();
                    lRegionNode.Tag = lRegionRow;
                    //District
                    foreach(DataRow lDistrictRow in lDataSet.District.Rows) {
                        if ((lDistrictRow[MappedNames.RegionId] is DBNull)
                            || Convert.ToInt32(lRegionRow[MappedNames.RegionId]) != Convert.ToInt32(lDistrictRow[MappedNames.RegionId]))
                            continue;

                        TreeNode lDistrictNode = new TreeNode();
                        lDistrictNode.Text = lDistrictRow[MappedNames.DistrictName] is DBNull ? " " : lDistrictRow[MappedNames.DistrictName].ToString();
                        lDistrictNode.Tag = lDistrictRow;
                        //City
                        foreach (DataRow lCityRow in lDataSet.City.Rows) {
                            if ((lCityRow[MappedNames.DistrictId] is DBNull)
                                || Convert.ToInt32(lDistrictRow[MappedNames.DistrictId]) != Convert.ToInt32(lCityRow[MappedNames.DistrictId]))
                                continue;

                            TreeNode lCityNode = new TreeNode();
                            lCityNode.Text = lCityRow[MappedNames.CityName] is DBNull ? " " : lCityRow[MappedNames.CityName].ToString();
                            lCityNode.Tag = lCityRow;

                            lDistrictNode.Nodes.Add(lCityNode);
                        }
                        lRegionNode.Nodes.Add(lDistrictNode);
                    }
                    lCountryNode.Nodes.Add(lRegionNode);
                }
                lNodes.Add(lCountryNode);
            }

            TreeNode[] lNodesArray = null;

            if (lNodes.Count > 0) {
                lNodesArray = new TreeNode[lNodes.Count];
                lNodes.CopyTo(lNodesArray, 0);
            }
            return lNodesArray;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping countryMap = new DataTableMapping(Country.SourceTable, Country.DataSetTable);
                    countryMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                        {
                            CountryId, CountryName
                        }
                        );

                    DataTableMapping regionsMap = new DataTableMapping(Region.SourceTable, Region.DataSetTable);
                    regionsMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                        {
                            RegionId, RegionName, RegionCountryId, RegionRMId
                        }
                        );

                    DataTableMapping districtsMap = new DataTableMapping(District.SourceTable, District.DataSetTable);
                    districtsMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                        {
                            DistrictId, DistrictName, DistrictRegionId
                        }
                        );

                    DataTableMapping citiesMap = new DataTableMapping(City.SourceTable, City.DataSetTable);
                    citiesMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                        {
                            CityId, CityName, CityDistrictId
                        }
                        );

                    _tableMapping.Add(Country.DataSetTable, countryMap);
                    _tableMapping.Add(Region.DataSetTable, regionsMap);
                    _tableMapping.Add(District.DataSetTable, districtsMap);
                    _tableMapping.Add(City.DataSetTable, citiesMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping Country = new DataTableMapping("tblCountry", MappedNames.CountryTable);

        protected DataColumnMapping CountryId = new DataColumnMapping("Country_id", MappedNames.CountryId);
        protected DataColumnMapping CountryName = new DataColumnMapping("Country_name", MappedNames.CountryName);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping Status = new DataColumnMapping("Status",MappedNames.Status);
        protected DataColumnMapping CurrencyId = new DataColumnMapping("CurrencyID", MappedNames.CurrencyId);
        protected DataColumnMapping VAT = new DataColumnMapping("VAT", MappedNames.VAT);
        protected DataColumnMapping CountryULM = new DataColumnMapping("ULM", MappedNames.ULM);

        protected DataTableMapping Region = new DataTableMapping("tblRegions", MappedNames.RegionTable);

        protected DataColumnMapping RegionId = new DataColumnMapping("Region_Id", MappedNames.RegionId);
        protected DataColumnMapping RegionName = new DataColumnMapping("Region_name", MappedNames.RegionName);
        protected DataColumnMapping RegionCountryId = new DataColumnMapping("Country_Id", MappedNames.CountryId);
        protected DataColumnMapping RegionULM = new DataColumnMapping("ULM", MappedNames.ULM);
        protected DataColumnMapping RegionDLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping RegionStatus = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping RegionRMId = new DataColumnMapping("RM_Id", MappedNames.RegionRMId);

        protected DataTableMapping District = new DataTableMapping("tblDistricts", MappedNames.DistrictTable);

        protected DataColumnMapping DistrictId = new DataColumnMapping("District_Id", MappedNames.DistrictId);
        protected DataColumnMapping DistrictName = new DataColumnMapping("District_name", MappedNames.DistrictName);
        protected DataColumnMapping DistrictRegionId = new DataColumnMapping("Region_Id", MappedNames.RegionId);
        protected DataColumnMapping DistrictULM = new DataColumnMapping("ULM", MappedNames.ULM);
        protected DataColumnMapping DistrictDLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping DistrictStatus = new DataColumnMapping("Status", MappedNames.Status);

        protected DataTableMapping City = new DataTableMapping("tblCities", MappedNames.CityTable);

        protected DataColumnMapping CityId = new DataColumnMapping("City_Id", MappedNames.CityId);
        protected DataColumnMapping CityName = new DataColumnMapping("City_name", MappedNames.CityName);
        protected DataColumnMapping CityDistrictId = new DataColumnMapping("District_Id", MappedNames.DistrictId);
        protected DataColumnMapping CityULM = new DataColumnMapping("ULM", MappedNames.ULM);
        protected DataColumnMapping CityDLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping CityStatus = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping CityCntAreaId = new DataColumnMapping("CntArea_id", MappedNames.CityCntAreaId);
        protected DataColumnMapping CityCityShortName = new DataColumnMapping("City_shortname", MappedNames.CityCityShortName);
        protected DataColumnMapping CitySettlementId = new DataColumnMapping( "Settlement_id", MappedNames.CitySettlementId);
        protected DataColumnMapping CityPopulationId = new DataColumnMapping( "Population_id", MappedNames.CityPopulationId);

        #endregion Protected members

    }
}
