﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;


namespace Logica.Reports.RPI.DataProvider.Providers
{
    class OutletGroupsDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "OutletGroups";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public OutletGroupsDataSet GetData()
        {
            OutletGroupsDataSet dataSet = new OutletGroupsDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.OutletGroups, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[OutletGroup.DataSetTable]);
                adapter.Fill(dataSet, OutletGroup.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Outlet Groups
        /// </summary>
        /// <returns>Outlet Groups list</returns>
        public List<OutletGroupEntity> GetList()
        {
            return GetFilledList(GetData().OutletGroup.DefaultView);
        }

        /// <summary>
        /// Returns filtered Outlet Groups list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Outlet Groups list</returns>
        public List<OutletGroupEntity> GetList(string filterExpression)
        {
            OutletGroupsDataSet outletGroups = GetData();

            outletGroups.OutletGroup.DefaultView.RowFilter = filterExpression;

            return GetFilledList(outletGroups.OutletGroup.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(OutletGroup.SourceTable, OutletGroup.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                                OLGroupId, OLGroupName, Status
                            }
                        );

                    _tableMapping.Add(OutletGroup.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping OutletGroup = new DataTableMapping(MappedNames.OutletGroupsSourceTable, MappedNames.OutletGroupTable);

        protected DataColumnMapping OLGroupId = new DataColumnMapping("OLGroup_id", MappedNames.OutletGroupId);
        protected DataColumnMapping OLGroupName = new DataColumnMapping("OLGroup_name", MappedNames.OutletGroupName);

        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);

        #endregion Protected members

        #region Private methods

        private List<OutletGroupEntity> GetFilledList(DataView outletGroup)
        {
            List<OutletGroupEntity> result = null;

            try
            {
                foreach (DataRowView group in outletGroup)
                {
                    if (result == null)
                    {
                        result = new List<OutletGroupEntity>();
                    }

                    result.Add(
                                new OutletGroupEntity
                                    (
                                        long.Parse(group[OLGroupId.DataSetColumn].ToString()),
                                        group[OLGroupName.DataSetColumn].ToString(),
                                        DateTime.MinValue, //DateTime.Parse(group[DLM.DataSetColumn].ToString()),
                                        DataConvertorHelper.GetDBNullInt32Checked(group[Status.DataSetColumn])
                                    )
                               );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }

            return result;
        }

        #endregion Private method
    }

    public class OutletGroupEntity : IBaseEntity
    {
        private long _id;
        private string _name;
        private DateTime _DLM;
        private int _status;

        public OutletGroupEntity() { }

        public OutletGroupEntity(long id, string name, DateTime dlm, int status)
            : this()
        {
            _id = id;
            _name = name;
            _DLM = dlm;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }


}