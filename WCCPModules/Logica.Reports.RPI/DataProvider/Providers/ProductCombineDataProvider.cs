﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class ProductCombineDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "ProductCombine";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns>DataSet</returns>
        public ProductCombineDataSet GetData()
        {
            ProductCombineDataSet dataSet = new ProductCombineDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.ProductCombine, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[ProductCombine.DataSetTable]);
                adapter.Fill(dataSet, ProductCombine.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Products Combine
        /// </summary>
        /// <returns>Products Combine list</returns>
        public List<ProductCombineEntity> GetList()
        {
            return GetFilledList(GetData().ProductCombine.DefaultView);
        }

        /// <summary>
        /// Returns filtered Products Combine list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Products Combine list</returns>
        public List<ProductCombineEntity> GetList(string filterExpression)
        {
            ProductCombineDataSet productsDataSet = GetData();

            productsDataSet.ProductCombine.DefaultView.RowFilter = filterExpression;

            return GetFilledList(productsDataSet.ProductCombine.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(ProductCombine.SourceTable, ProductCombine.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               ProductCnId, ProductCombineName, ProductCombineShortName//, DLM, ULM, Status
                            }
                        );

                    _tableMapping.Add(ProductCombine.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping ProductCombine = new DataTableMapping(MappedNames.ProductCombineSourceTable, MappedNames.ProductCombineTable);

        protected DataColumnMapping ProductCnId = new DataColumnMapping("ProductCn_Id", MappedNames.ProductCnId);
        protected DataColumnMapping ProductCombineName = new DataColumnMapping("ProductCnName", MappedNames.ProductCombineName);
        protected DataColumnMapping ProductCombineShortName = new DataColumnMapping("ProductCnShortName", MappedNames.ProductCombineShortName);

        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping ULM = new DataColumnMapping("ULM", MappedNames.ULM);

        #endregion Protected members

        #region Private methods

        private List<ProductCombineEntity> GetFilledList(DataView productCombineView)
        {
            List<ProductCombineEntity> result = null;
            try
            {
                foreach (DataRowView product in productCombineView)
                {
                    if (result == null)
                    {
                        result = new List<ProductCombineEntity>();
                    }

                    result.Add(
                        new ProductCombineEntity
                            (
                            long.Parse(product[ProductCnId.DataSetColumn].ToString()),
                            product[ProductCombineName.DataSetColumn].ToString(),
                            product[ProductCombineShortName.DataSetColumn].ToString(),
                            DateTime.MinValue,// DateTime.Parse(product[DLM.DataSetColumn] == DBNull.Value? DateTime.MinValue.ToString(): product[DLM.DataSetColumn].ToString()),
                            "", //product[ULM.DataSetColumn].ToString(),
                            -1 //int.Parse(product[Status.DataSetColumn].ToString())
                            )
                        );
                }
            }
            catch(Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }
            return result;
        }

        #endregion Private method
    }

    public class ProductCombineEntity: IBaseEntity
    {
        private long _id;
        private string _name;
        private string _shortName;
        private DateTime _DLM;
        private string _ULM;
        private int _status;

        public ProductCombineEntity() { }

        public ProductCombineEntity(long id, string name, string shortName, DateTime dlm, string ulm, int status)
            : this()
        {
            _id = id;
            _name = name;
            _shortName = shortName;
            _DLM = dlm;
            _ULM = ulm;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string ShortName
        {
            get { return _shortName; }
            set { _shortName = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public string ULM
        {
            get { return _ULM; }
            set { _ULM = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }


    public class ProductCombineKPI
    {
        private long _productCombineId;
        private string _productCombineName;
        private string _kpiValue;

        public ProductCombineKPI() { }

        public ProductCombineKPI(long productCombineId, string productCombineName, string kpiValue)
            : this()
        {
            _productCombineId = productCombineId;
            _productCombineName = productCombineName;
            _kpiValue = kpiValue;
        }

        public ProductCombineKPI(ProductCombineEntity productCombineEntity)
            : this()
        {
            _productCombineId = productCombineEntity.Id;
            _productCombineName = productCombineEntity.Name;
        }

        public long ProductCnIdColumn
        {
            get { return _productCombineId; }
            set { _productCombineId = value; }
        }

        public string ProductCombineNameColumn
        {
            get { return _productCombineName; }
            set { _productCombineName = value; }
        }

        public string KPIValueColumn
        {
            get { return _kpiValue; }
            set { _kpiValue = value; }
        }
    }
}