﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.DataProvider.Entities.RegionsDataSetTableAdapters;
namespace Logica.Reports.RPI.DataProvider.Providers
{
    public class CascadedAimsDataProvider : DataProviderBase
    {
        #region Private fields

        #endregion Private fields

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "CascadedAims";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns>Typed DataSet</returns>
        public AimsDataSet GetData()
        {
            AimsDataSet dataSet = new AimsDataSet();

            try
            {
                Dictionary<string, object> sqlParameters = new Dictionary<string, object>();

                SqlDataAdapter adapter = new SqlDataAdapter(); //ExecuteAdapterCommand(ScriptProvider.CascadedAims, sqlParameters);
                SqlCommand selectCommand = new SqlCommand(ScriptProvider.CascadedAims, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = selectCommand;
                
                adapter.TableMappings.Add(TableMapping["Aim"]);
                adapter.Fill(dataSet, "Aim");
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Gets set of aims with overlapped periods for specified cascaded Aim
        /// </summary>
        /// <param name="aim">Cascaded Aim class</param>
        /// <returns>Typed DataSet</returns>
        public AimsDataSet GetCrossedAims(CascadedAimEntity aim)
        {

            AimsDataSet dataSet = new AimsDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(); 

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.CascadedAimsCrossing, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType =  CommandType.StoredProcedure;

                selectCommand.Parameters.Add("@Region_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@District_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@City_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@ProductCn_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@RPI_KPI_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@DateStartPeriod", SqlDbType.DateTime);
                selectCommand.Parameters.Add("@DateEndPeriod", SqlDbType.DateTime);
                selectCommand.Parameters.Add("@isStartDateApproved", SqlDbType.Bit);
                selectCommand.Parameters.Add("@isEndDateApproved", SqlDbType.Bit);

                selectCommand.Parameters.Add("@DSM_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@Supervisor_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@Settlement_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@Proximity_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@isFocused", SqlDbType.Bit);
                selectCommand.Parameters.Add("@OLType_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@OLSubType_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@OLGroup_ID", SqlDbType.Int);

                selectCommand.Parameters["@Region_ID"].Value = aim.RegionId;
                selectCommand.Parameters["@District_ID"].Value =  aim.DistrictId;
                selectCommand.Parameters["@City_ID"].Value =  aim.CityId;
                selectCommand.Parameters["@ProductCn_ID"].Value =  aim.ProductCnId;
                selectCommand.Parameters["@RPI_KPI_ID"].Value =  aim.KPITypeId;
                selectCommand.Parameters["@DateStartPeriod"].Value = 
                    aim.DateStartPeriod == DateTime.MinValue ? new DateTime(1899, 1, 1) : aim.DateStartPeriod;
                selectCommand.Parameters["@DateEndPeriod"].Value =
                    aim.DateEndPeriod == DateTime.MinValue ? new DateTime(1899, 1, 1) : aim.DateEndPeriod; 
                selectCommand.Parameters["@isStartDateApproved"].Value = 
                    aim.IsStartDateApproved ? 1 : 0;
                selectCommand.Parameters["@isEndDateApproved"].Value =
                    aim.IsEndDateApproved ? 1 : 0;
                selectCommand.Parameters["@DSM_ID"].Value = aim.DSMId;
                selectCommand.Parameters["@Supervisor_ID"].Value = aim.SupervisorId;
                selectCommand.Parameters["@Settlement_ID"].Value = aim.SettlementId;
                selectCommand.Parameters["@Proximity_ID"].Value = aim.ProximityId;
                selectCommand.Parameters["@isFocused"].Value = aim.IsFocused ? 1 : 0;
                selectCommand.Parameters["@OLType_ID"].Value = aim.OlTypeId;
                selectCommand.Parameters["@OLSubType_ID"].Value = aim.OlSubTypeId;
                selectCommand.Parameters["@OLGroup_ID"].Value = aim.OlGroupId;

                adapter.SelectCommand = selectCommand;
                adapter.TableMappings.Add(TableMapping["Aim"]);
                adapter.Fill(dataSet, "Aim");
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Gets set of aims with same parametrs as specified cascaded Aim
        /// </summary>
        /// <param name="aim">Cascaded Aim class</param>
        /// <returns>Typed DataSet</returns>
        public AimsDataSet GetIdenticalAims(CascadedAimEntity aim)
        {

            AimsDataSet dataSet = new AimsDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.CascadedAimsIdentical, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                selectCommand.Parameters.Add("@Region_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@District_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@City_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@ProductCn_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@RPI_KPI_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@DateStartPeriod", SqlDbType.DateTime);
                selectCommand.Parameters.Add("@DateEndPeriod", SqlDbType.DateTime);
                selectCommand.Parameters.Add("@isStartDateApproved", SqlDbType.Bit);
                selectCommand.Parameters.Add("@isEndDateApproved", SqlDbType.Bit);
                selectCommand.Parameters.Add("@DSM_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@Supervisor_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@Settlement_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@Proximity_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@isFocused", SqlDbType.Bit);
                selectCommand.Parameters.Add("@OLType_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@OLSubType_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@OLGroup_ID", SqlDbType.Int);
                selectCommand.Parameters.Add("@KPIValue", SqlDbType.NVarChar);

                selectCommand.Parameters["@Region_ID"].Value = aim.RegionId;
                selectCommand.Parameters["@District_ID"].Value = aim.DistrictId;
                selectCommand.Parameters["@City_ID"].Value = aim.CityId;
                selectCommand.Parameters["@ProductCn_ID"].Value = aim.ProductCnId;
                selectCommand.Parameters["@RPI_KPI_ID"].Value = aim.KPITypeId;
                selectCommand.Parameters["@DateStartPeriod"].Value =
                    aim.DateStartPeriod == DateTime.MinValue ? new DateTime(1899, 1, 1) : aim.DateStartPeriod;
                selectCommand.Parameters["@DateEndPeriod"].Value =
                    aim.DateEndPeriod == DateTime.MinValue ? new DateTime(1899, 1, 1) : aim.DateEndPeriod;
                selectCommand.Parameters["@isStartDateApproved"].Value =
                    aim.IsStartDateApproved ? 1 : 0;
                selectCommand.Parameters["@isEndDateApproved"].Value =
                    aim.IsEndDateApproved ? 1 : 0;
                selectCommand.Parameters["@DSM_ID"].Value = aim.DSMId;
                selectCommand.Parameters["@Supervisor_ID"].Value = aim.SupervisorId;
                selectCommand.Parameters["@Settlement_ID"].Value = aim.SettlementId;
                selectCommand.Parameters["@Proximity_ID"].Value = aim.ProximityId;
                selectCommand.Parameters["@isFocused"].Value = aim.IsFocused ? 1 : 0;
                selectCommand.Parameters["@OLType_ID"].Value = aim.OlTypeId;
                selectCommand.Parameters["@OLSubType_ID"].Value = aim.OlSubTypeId;
                selectCommand.Parameters["@OLGroup_ID"].Value = aim.OlGroupId;
                selectCommand.Parameters["@KPIValue"].Value = aim.KPIValue;

                adapter.SelectCommand = selectCommand;
                adapter.TableMappings.Add(TableMapping["Aim"]);
                adapter.Fill(dataSet, "Aim");
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Gets set of aims filtered by Focused SKU
        /// </summary>
        /// <returns>Typed DataSet</returns>
        public AimsDataSet GetAimsFilteredByFocusedSKU()
        {
            AimsDataSet dataSet = new AimsDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.CascadedAimsFilteredByFocusedSKU, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;
                adapter.TableMappings.Add(TableMapping["Aim"]);
                adapter.Fill(dataSet, "Aim");
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }


        public void UpdateData(AimsDataSet.AimDataTable table)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand insertCommand = new SqlCommand(ScriptProvider.CascadedAimsInsert, DatabaseConnection);
                insertCommand.CommandTimeout = 30;
                insertCommand.CommandType =  CommandType.StoredProcedure; //CommandType.Text;
                insertCommand.Parameters.Add("@DateAdded", SqlDbType.DateTime, 0, MappedNames.DateAdded);
                insertCommand.Parameters.Add("@DateStartPeriod", SqlDbType.DateTime, 0, MappedNames.DateStartPeriod);
                insertCommand.Parameters.Add("@DateApproveStartPeriod", SqlDbType.DateTime, 0, MappedNames.DateApproveStartPeriod);
                insertCommand.Parameters.Add("@IsStartDateApproved", SqlDbType.Bit, 0, MappedNames.IsStartDateApproved);
                insertCommand.Parameters.Add("@DateEndPeriod", SqlDbType.DateTime, 0, MappedNames.DateEndPeriod);
                insertCommand.Parameters.Add("@DateApproveEndPeriod", SqlDbType.DateTime, 0, MappedNames.DateApproveEndPeriod);
                insertCommand.Parameters.Add("@IsEndDateApproved", SqlDbType.Bit, 0, MappedNames.IsEndDateApproved);
                insertCommand.Parameters.Add("@CreatedUser_Id", SqlDbType.Int, 0, MappedNames.CreatedUserId);
                insertCommand.Parameters.Add("@ApprovedUser_Id", SqlDbType.Int, 0, MappedNames.ApprovedUserId);
                insertCommand.Parameters.Add("@Region_Id", SqlDbType.Int, 0, MappedNames.RegionId);
                insertCommand.Parameters.Add("@District_Id", SqlDbType.Int, 0, MappedNames.DistrictId);
                insertCommand.Parameters.Add("@City_Id", SqlDbType.Int, 0, MappedNames.CityId);
                insertCommand.Parameters.Add("@DSM_Id", SqlDbType.Int, 0, MappedNames.DSMId);
                insertCommand.Parameters.Add("@Supervisor_Id", SqlDbType.Int, 0, MappedNames.SupervisorId);
                insertCommand.Parameters.Add("@Settlement_Id", SqlDbType.Int, 0, MappedNames.SettlementId);
                insertCommand.Parameters.Add("@Proximity_Id", SqlDbType.Int, 0, MappedNames.ProximityId);
                insertCommand.Parameters.Add("@ProductCn_Id", SqlDbType.Int, 0, MappedNames.ProductCnId);
                insertCommand.Parameters.Add("@IsFocused", SqlDbType.Bit, 0, MappedNames.IsFocused);
                insertCommand.Parameters.Add("@OLType_Id", SqlDbType.Int, 0, MappedNames.OLTypeId);
                insertCommand.Parameters.Add("@OLSubType_Id", SqlDbType.Int, 0, MappedNames.OLSubTypeId);
                insertCommand.Parameters.Add("@OLGroup_Id", SqlDbType.Int, 0, MappedNames.OLGroupId);
                insertCommand.Parameters.Add("@KPIType_Id", SqlDbType.Int, 0, MappedNames.KPITypeId);
                insertCommand.Parameters.Add("@KPIValue", SqlDbType.NVarChar, 50, MappedNames.KPIValue);
                insertCommand.Parameters.Add("@DeviationPercent", SqlDbType.Decimal, 18, MappedNames.DeviationPercent);

//                insertCommand.Parameters.Add("@Aim_id", SqlDbType.Int);
//                insertCommand.Parameters["@Aim_id"].Direction = ParameterDirection.InputOutput;

                adapter.InsertCommand = insertCommand;

                SqlCommand updateCommand = new SqlCommand(ScriptProvider.CascadedAimsUpdate, DatabaseConnection);
                updateCommand.CommandTimeout = 30;
                updateCommand.CommandType = CommandType.StoredProcedure; //CommandType.Text;
                updateCommand.Parameters.Add("@RPI_Aim_ID", SqlDbType.Int, 0, MappedNames.AimId);
                updateCommand.Parameters.Add("@DateAdded", SqlDbType.DateTime, 0, MappedNames.DateAdded);
                updateCommand.Parameters.Add("@DateStartPeriod", SqlDbType.DateTime, 0, MappedNames.DateStartPeriod);
                updateCommand.Parameters.Add("@DateApproveStartPeriod", SqlDbType.DateTime, 0, MappedNames.DateApproveStartPeriod);
                updateCommand.Parameters.Add("@IsStartDateApproved", SqlDbType.Bit, 0, MappedNames.IsStartDateApproved);
                updateCommand.Parameters.Add("@DateEndPeriod", SqlDbType.DateTime, 0, MappedNames.DateEndPeriod);
                updateCommand.Parameters.Add("@DateApproveEndPeriod", SqlDbType.DateTime, 0, MappedNames.DateApproveEndPeriod);
                updateCommand.Parameters.Add("@IsEndDateApproved", SqlDbType.Bit, 0, MappedNames.IsEndDateApproved);
                updateCommand.Parameters.Add("@CreatedUser_Id", SqlDbType.Int, 0, MappedNames.CreatedUserId);
                updateCommand.Parameters.Add("@ApprovedUser_Id", SqlDbType.Int, 0, MappedNames.ApprovedUserId);
                updateCommand.Parameters.Add("@Region_Id", SqlDbType.Int, 0, MappedNames.RegionId);
                updateCommand.Parameters.Add("@District_Id", SqlDbType.Int, 0, MappedNames.DistrictId);
                updateCommand.Parameters.Add("@City_Id", SqlDbType.Int, 0, MappedNames.CityId);
                updateCommand.Parameters.Add("@DSM_Id", SqlDbType.Int, 0, MappedNames.DSMId);
                updateCommand.Parameters.Add("@Supervisor_Id", SqlDbType.Int, 0, MappedNames.SupervisorId);
                updateCommand.Parameters.Add("@Settlement_Id", SqlDbType.Int, 0, MappedNames.SettlementId);
                updateCommand.Parameters.Add("@Proximity_Id", SqlDbType.Int, 0, MappedNames.ProximityId);
                updateCommand.Parameters.Add("@ProductCn_Id", SqlDbType.Int, 0, MappedNames.ProductCnId);
                updateCommand.Parameters.Add("@IsFocused", SqlDbType.Bit, 0, MappedNames.IsFocused);
                updateCommand.Parameters.Add("@OLType_Id", SqlDbType.Int, 0, MappedNames.OLTypeId);
                updateCommand.Parameters.Add("@OLSubType_Id", SqlDbType.Int, 0, MappedNames.OLSubTypeId);
                updateCommand.Parameters.Add("@OLGroup_Id", SqlDbType.Int, 0, MappedNames.OLGroupId);
                updateCommand.Parameters.Add("@KPIType_Id", SqlDbType.Int, 0, MappedNames.KPITypeId);
                updateCommand.Parameters.Add("@KPIValue", SqlDbType.NVarChar, 50, MappedNames.KPIValue);
                updateCommand.Parameters.Add("@DeviationPercent", SqlDbType.Decimal, 18, MappedNames.DeviationPercent);

                adapter.UpdateCommand = updateCommand;

                SqlCommand deleteCommand = new SqlCommand(ScriptProvider.CascadedAimsDelete, DatabaseConnection);
                deleteCommand.CommandTimeout = 30;
                deleteCommand.CommandType = CommandType.StoredProcedure;
                deleteCommand.Parameters.Add("@RPI_Aim_ID", SqlDbType.Int, 0, MappedNames.AimId);

                adapter.DeleteCommand = deleteCommand;

                adapter.Update(table);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
           // return dataSet;        
        
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping aimMap = new DataTableMapping(Aim.SourceTable, Aim.DataSetTable);
                    aimMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                                AimId,
                                DateAdded,
                                DateStartPeriod,
                                DateApproveStartPeriod, 
                                IsStartDateApproved,
                                DateEndPeriod,
                                DateApproveEndPeriod,
                                IsEndDateApproved, 
                                CreatedUserId, 
                                ApprovedUserId, 
                                AimRegionId, 
                                AimDistrictId, 
                                AimCityId, 

                                KPITypeName,
                                DSMId,
                                SupervisorId, 
                                SettlementId,
                                SettlementName,
                                ProximityId, 
                                ProductCnId,
                                IsFocused,
                                OLTypeId, 
                                OLSubTypeId, 
                                OLGroupId,
                                KPITypeId, 
                                KPIValue, 

                                DeviationPercent, 
                                //HLCode, 
                                ProductCombineName, 

                                ProductCombineShortName, 

                                AimStatus,
                                AimDLM, 
                                ULM,
                                OLGroupName,
                                OLTypeName,
                                OLSubTypeName,
                                DSMName,
                                SupervisorName,
                                ProximityName,
                                Deviation,
                                IsBaseSKU,
                                ComposedRegionName,
                                ComposedDistrictName,
                                ComposedCityName
                            }
                        );

                    _tableMapping.Add(Aim.SourceTable, aimMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members
        protected DataTableMapping Country = new DataTableMapping("tblCountry", MappedNames.CountryTable);

        protected DataColumnMapping CountryId = new DataColumnMapping("Country_Id", MappedNames.CountryId);
        protected DataColumnMapping CountryName = new DataColumnMapping("Country_name", MappedNames.CountryName);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping CurrencyId = new DataColumnMapping("CurrencyID", MappedNames.CurrencyId);
        protected DataColumnMapping VAT = new DataColumnMapping("VAT", MappedNames.VAT);


        protected DataTableMapping Region = new DataTableMapping("tblRegions", MappedNames.RegionTable);

        protected DataColumnMapping RegionId = new DataColumnMapping("Region_Id", MappedNames.RegionId);
        protected DataColumnMapping RegionName = new DataColumnMapping("Region_name", MappedNames.RegionName);
        protected DataColumnMapping RegionCountryId = new DataColumnMapping("Country_Id", MappedNames.CountryId);
        protected DataColumnMapping RegionDLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping RegionStatus = new DataColumnMapping("Status", MappedNames.Status);

        protected DataTableMapping District = new DataTableMapping("tblDistricts", MappedNames.DistrictTable);

        protected DataColumnMapping DistrictId = new DataColumnMapping("District_Id", MappedNames.DistrictId);
        protected DataColumnMapping DistrictName = new DataColumnMapping("District_name", MappedNames.DistrictName);
        protected DataColumnMapping DistrictRegionId = new DataColumnMapping("Region_Id", MappedNames.RegionId);
        protected DataColumnMapping DistrictDLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping DistrictStatus = new DataColumnMapping("Status", MappedNames.Status);

        protected DataTableMapping City = new DataTableMapping("tblCities", MappedNames.CityTable);

        protected DataColumnMapping CityId = new DataColumnMapping("City_Id", MappedNames.CityId);
        protected DataColumnMapping CityName = new DataColumnMapping("City_name", MappedNames.CityName);
        protected DataColumnMapping CityDistrictId = new DataColumnMapping("District_Id", MappedNames.DistrictId);
        protected DataColumnMapping CityDLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping CityStatus = new DataColumnMapping("Status", MappedNames.Status);

        protected DataTableMapping Aim = new DataTableMapping("Aim", MappedNames.AimTable);

        protected DataColumnMapping AimId = new DataColumnMapping("RPI_Aim_ID", MappedNames.AimId);
        protected DataColumnMapping DateAdded = new DataColumnMapping("DateAdded", MappedNames.DateAdded);
        protected DataColumnMapping DateStartPeriod = new DataColumnMapping("DateStartPeriod", MappedNames.DateStartPeriod);
        protected DataColumnMapping DateApproveStartPeriod = new DataColumnMapping("DateApproveStartPeriod", MappedNames.DateApproveStartPeriod);
        protected DataColumnMapping IsStartDateApproved = new DataColumnMapping("isStartDateApproved", MappedNames.IsStartDateApproved);
        protected DataColumnMapping DateEndPeriod = new DataColumnMapping("DateEndPeriod", MappedNames.DateEndPeriod);
        protected DataColumnMapping DateApproveEndPeriod = new DataColumnMapping("DateApproveEndPeriod", MappedNames.DateApproveEndPeriod);
        protected DataColumnMapping IsEndDateApproved = new DataColumnMapping("isEndDateApproved", MappedNames.IsEndDateApproved);
        protected DataColumnMapping CreatedUserId = new DataColumnMapping("CreatedUser_ID", MappedNames.CreatedUserId);
        protected DataColumnMapping ApprovedUserId = new DataColumnMapping("ApprovedUser_ID", MappedNames.ApprovedUserId);
        protected DataColumnMapping AimRegionId = new DataColumnMapping("Region_ID", MappedNames.RegionId);
        protected DataColumnMapping AimDistrictId = new DataColumnMapping("District_ID", MappedNames.DistrictId);
        protected DataColumnMapping AimCityId = new DataColumnMapping("City_ID", MappedNames.CityId);
        protected DataColumnMapping KPITypeName = new DataColumnMapping("KPIName", MappedNames.KPITypeName);

        protected DataColumnMapping DSMId = new DataColumnMapping("DSM_ID", MappedNames.DSMId);
        protected DataColumnMapping SupervisorId = new DataColumnMapping("Supervisor_ID", MappedNames.SupervisorId);
        protected DataColumnMapping SettlementId = new DataColumnMapping("Settlement_ID", MappedNames.SettlementId);
        protected DataColumnMapping SettlementName = new DataColumnMapping("Settlement", MappedNames.SettlementName);
        protected DataColumnMapping ProximityId = new DataColumnMapping("Proximity_ID", MappedNames.ProximityId);
        protected DataColumnMapping ProductCnId = new DataColumnMapping("ProductCn_ID", MappedNames.ProductCnId);
        protected DataColumnMapping IsFocused = new DataColumnMapping("isFocused", MappedNames.IsFocused);
        protected DataColumnMapping OLTypeId = new DataColumnMapping("OLType_ID", MappedNames.OLTypeId);
        protected DataColumnMapping OLSubTypeId = new DataColumnMapping("OLSubType_ID", MappedNames.OLSubTypeId);
        protected DataColumnMapping OLGroupId = new DataColumnMapping("OLGroup_ID", MappedNames.OLGroupId);
        protected DataColumnMapping KPITypeId = new DataColumnMapping("RPI_KPI_ID", MappedNames.KPITypeId);
        protected DataColumnMapping KPIValue = new DataColumnMapping("KPIValue", MappedNames.KPIValue);

        protected DataColumnMapping DeviationPercent = new DataColumnMapping("DeviationPercent", MappedNames.DeviationPercent);
        protected DataColumnMapping HLCode = new DataColumnMapping("HLCode", MappedNames.HLCode);
        protected DataColumnMapping ProductCombineName = new DataColumnMapping("ProductCnName", MappedNames.ProductCombineName);

        protected DataColumnMapping ProductCombineShortName = new DataColumnMapping("ProductCnShortName", MappedNames.ProductCombineShortName);

        protected DataColumnMapping AimStatus = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping AimDLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping ULM = new DataColumnMapping("ULM", MappedNames.ULM);

        protected DataColumnMapping OLGroupName = new DataColumnMapping("OLGroup_name", MappedNames.OLGroupName);
        protected DataColumnMapping OLTypeName = new DataColumnMapping("OLtype_name", MappedNames.OLTypeName);
        protected DataColumnMapping OLSubTypeName = new DataColumnMapping("OLSubTypeName", MappedNames.OLSubTypeName);
        protected DataColumnMapping DSMName = new DataColumnMapping("DSM_Name", MappedNames.DSMName);
        protected DataColumnMapping SupervisorName = new DataColumnMapping("Supervisor_name", MappedNames.SupervisorName);
        protected DataColumnMapping ProximityName = new DataColumnMapping("Proximity_Name", MappedNames.ProximityName);
        protected DataColumnMapping Deviation = new DataColumnMapping("Deviation", MappedNames.Deviation);
        protected DataColumnMapping IsBaseSKU = new DataColumnMapping("isBaseSKU", MappedNames.IsBaseSKU);
        protected DataColumnMapping ComposedRegionName = new DataColumnMapping("ComposedRegionName", MappedNames.ComposedRegionName);
        protected DataColumnMapping ComposedDistrictName = new DataColumnMapping("ComposedDistrictName", MappedNames.ComposedDistrictName);
        protected DataColumnMapping ComposedCityName = new DataColumnMapping("ComposedCityName", MappedNames.ComposedCityName);
        #endregion Protected members
    }

    public class CascadedAimEntity : ICloneable
    {
        private int _aimId;
        private DateTime _dateAdded;
        private DateTime _dateStartPeriod;
        private DateTime _dateApproveStartPeriod;
        private bool _isStartDateApproved;
        private DateTime _dateEndPeriod;
        private DateTime _dateApproveEndPeriod;
        private bool _isEndDateApproved;
        private int _createdUserId;
        private int _approvedUserId;
        private int _regionId;
        private int _districtId;
        private int _cityId;
        private string _KPITypeName;
        private int _DSMId;
        private int _supervisorId;
        private int _settlementId;
        private int _proximityId;
        private int _productCnId;
        private bool _isFocused;
        private int _olTypeId;

        private int _olSubTypeId;
        private int _olGroupId;
        private int _KPITypeId;
        private string _KPIValue;

        private double _deviationPercent;

        private string _HLCode;
        private string _productCombineName;
        private string _productCombineShortName;
        private int _productCombineStatus;
        private DateTime _productCombineDLM;
        private string _productCombineULM;

        private bool _isBaseSKU;

        public CascadedAimEntity() { }

        public CascadedAimEntity(
                    int aimId,
                    DateTime dateAdded,
                    DateTime dateStartPeriod,
                    DateTime dateApproveStartPeriod, 
                    bool isStartDateApproved,
                    DateTime dateEndPeriod,
                    DateTime dateApproveEndPeriod,
                    bool isEndDateApproved,
                    int createdUserId,
                    int approvedUserId,
                    int regionId,
                    int districtId,
                    int cityId, 
            
                    string KPITypeName,
                    int DSMId,
                    int supervisorId,
                    int settlementId,
                    int sroximityId,
                    int productCnId,
                    bool isFocused,
                    int olTypeId,
                    int olSubTypeId,
                    int olGroupId,
                    int KPITypeId, 
                    string KPIValue, 
            
                    double deviationPercent, 
                    string HLCode, 
                    string productCombineName, 
            
                    string productCombineShortName,
            
                    int productCombineStatus,
                    DateTime productCombineDLM,
                    string productCombineULM,
                    bool isBaseSKU
            )
            : this()
        {
                    _aimId = aimId;
                    _dateAdded = dateAdded;
                    _dateStartPeriod = dateStartPeriod;
                    _dateApproveStartPeriod = dateApproveStartPeriod; 
                    _isStartDateApproved = isStartDateApproved;
                    _dateEndPeriod = dateEndPeriod;
                    _dateApproveEndPeriod = dateApproveEndPeriod;
                    _isEndDateApproved = isEndDateApproved;
                    _createdUserId = createdUserId;
                    _approvedUserId = approvedUserId;
                    _regionId = regionId;
                    _districtId = districtId;
                    _cityId = cityId; 
            
                    _KPITypeName = KPITypeName;
                    _DSMId = DSMId;
                    _supervisorId = supervisorId;
                    _settlementId = settlementId;
                    _proximityId = sroximityId;
                    _productCnId = productCnId;
                    _isFocused = isFocused;
                    _olTypeId = olTypeId;
                    _olSubTypeId = olSubTypeId;
                    _olGroupId = olGroupId;
                    _KPITypeId = KPITypeId; 
                    _KPIValue = KPIValue; 
            
                    _deviationPercent = deviationPercent; 
                    //_HLCode = HLCode; 
                    _productCombineName = productCombineName; 
            
                    _productCombineShortName = productCombineShortName;
            
                    _productCombineStatus = productCombineStatus;
                    _productCombineDLM = productCombineDLM;
                    _productCombineULM = productCombineULM;

                    _isBaseSKU = isBaseSKU;
        }

        public CascadedAimEntity(DataRow dataRow)
            : this()
        {
            _aimId = Convert.ToInt32( dataRow[MappedNames.AimId]);

            _dateAdded = Convert.ToDateTime( dataRow[MappedNames.DateAdded]);

            _dateStartPeriod = dataRow[MappedNames.DateStartPeriod] is DBNull 
                ? DateTime.MinValue
                : Convert.ToDateTime(dataRow[MappedNames.DateStartPeriod]);

            _dateApproveStartPeriod = dataRow[MappedNames.DateApproveStartPeriod] is DBNull
                ? DateTime.MinValue
                : Convert.ToDateTime( dataRow[MappedNames.DateApproveStartPeriod]);

            _isStartDateApproved =  Convert.ToBoolean( dataRow[MappedNames.IsStartDateApproved]);

            _dateEndPeriod = dataRow[MappedNames.DateEndPeriod] is DBNull
                ? DateTime.MinValue
                : Convert.ToDateTime(dataRow[MappedNames.DateEndPeriod]);

            _dateApproveEndPeriod = dataRow[MappedNames.DateApproveEndPeriod] is DBNull
                ? DateTime.MinValue
                : Convert.ToDateTime( dataRow[MappedNames.DateApproveEndPeriod]);

            _isEndDateApproved = Convert.ToBoolean(dataRow[MappedNames.IsEndDateApproved]);
            _createdUserId =  dataRow[MappedNames.CreatedUserId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.CreatedUserId]);
            _approvedUserId =  dataRow[MappedNames.ApprovedUserId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.ApprovedUserId]);
            _regionId=  dataRow[MappedNames.RegionId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.RegionId]);
            _districtId = dataRow[MappedNames.DistrictId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.DistrictId]);
            _cityId =  dataRow[MappedNames.CityId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.CityId]);

            KPITypeName = dataRow[MappedNames.KPITypeName].ToString();
            DSMId = dataRow[MappedNames.DSMId] == DBNull.Value ? -1 : Convert.ToInt32( dataRow[MappedNames.DSMId]);
            _supervisorId = dataRow[MappedNames.SupervisorId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.SupervisorId]);
            _settlementId = dataRow[MappedNames.SettlementId]  == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.SettlementId]);
            _proximityId =  dataRow[MappedNames.ProximityId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.ProximityId]);
            _productCnId =  dataRow[MappedNames.ProductCnId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.ProductCnId]);
            _isFocused = Convert.ToBoolean( dataRow[MappedNames.IsFocused]);
            _olTypeId =  dataRow[MappedNames.OLTypeId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.OLTypeId]);
            _olSubTypeId =  dataRow[MappedNames.OLSubTypeId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.OLSubTypeId]);
            _olGroupId =  dataRow[MappedNames.OLGroupId] == DBNull.Value ? -1 : Convert.ToInt32(  dataRow[MappedNames.OLGroupId]);
            KPITypeId = dataRow[MappedNames.KPITypeId] == DBNull.Value ? -1 : Convert.ToInt32(dataRow[MappedNames.KPITypeId]);
            KPIValue = dataRow[MappedNames.KPIValue].ToString();

            _deviationPercent = dataRow[MappedNames.DeviationPercent] == DBNull.Value ? -1 : Convert.ToDouble( dataRow[MappedNames.DeviationPercent]);
            //HLCode = dataRow[MappedNames.HLCode].ToString();
            _productCombineName = dataRow[MappedNames.ProductCombineName].ToString();

            _productCombineShortName = dataRow[MappedNames.ProductCombineShortName].ToString();

            _productCombineStatus = dataRow[MappedNames.Status] == DBNull.Value ? -1 : Convert.ToInt32(dataRow[MappedNames.Status]);
            _productCombineDLM = dataRow[MappedNames.DLM]  == DBNull.Value ? DateTime.Today :Convert.ToDateTime( dataRow[MappedNames.DLM]);
            _productCombineULM = dataRow[MappedNames.ULM].ToString();

            _isBaseSKU = Convert.ToBoolean(dataRow[MappedNames.IsBaseSKU]);
        }

        public void Fill(DataRow dataRow)
        {
            dataRow[MappedNames.AimId] = AimId;

            dataRow[MappedNames.DateAdded] = _dateAdded.Year < 1900 ? new DateTime(1899, 1, 1): _dateAdded;
            dataRow[MappedNames.DateStartPeriod] = _dateStartPeriod.Year < 1900 ? new DateTime(1899, 1, 1) : _dateStartPeriod;
            dataRow[MappedNames.DateApproveStartPeriod] = _dateApproveStartPeriod.Year < 1900 ? new DateTime(1899, 1, 1) : _dateApproveStartPeriod;
            dataRow[MappedNames.IsStartDateApproved] = _isStartDateApproved;
            dataRow[MappedNames.DateEndPeriod] = _dateEndPeriod.Year < 1900 ? new DateTime(1899,1,1) : _dateEndPeriod;
            dataRow[MappedNames.DateApproveEndPeriod] = _dateApproveEndPeriod.Year < 1900 ? new DateTime(1899, 1, 1) : _dateApproveEndPeriod;
            dataRow[MappedNames.IsEndDateApproved] = _isEndDateApproved;
            dataRow[MappedNames.CreatedUserId] = _createdUserId;
            dataRow[MappedNames.ApprovedUserId] = _approvedUserId;
            dataRow[MappedNames.RegionId] = _regionId;
            dataRow[MappedNames.DistrictId] = _districtId;
            dataRow[MappedNames.CityId] = _cityId;

            dataRow[MappedNames.KPITypeName] = KPITypeName;
            dataRow[MappedNames.DSMId] = DSMId;
            dataRow[MappedNames.SupervisorId] = _supervisorId;
            dataRow[MappedNames.SettlementId] = _settlementId;
            dataRow[MappedNames.ProximityId] = _proximityId;
            dataRow[MappedNames.ProductCnId] = _productCnId;
            dataRow[MappedNames.IsFocused] = _isFocused;
            dataRow[MappedNames.OLTypeId] = _olTypeId;
            dataRow[MappedNames.OLSubTypeId] = _olSubTypeId;
            dataRow[MappedNames.OLGroupId] = _olGroupId;
            dataRow[MappedNames.KPITypeId] = KPITypeId;
            dataRow[MappedNames.KPIValue] = DataConvertorHelper.GetNumberString(KPIValue);

            dataRow[MappedNames.DeviationPercent] = _deviationPercent;
            //dataRow[MappedNames.HLCode] = HLCode;
            dataRow[MappedNames.ProductCombineName] = _productCombineName;

            dataRow[MappedNames.ProductCombineShortName] = _productCombineShortName;

            dataRow[MappedNames.Status] = _productCombineStatus;
            dataRow[MappedNames.DLM] = _productCombineDLM;
            dataRow[MappedNames.ULM] = _productCombineULM;

            dataRow[MappedNames.IsBaseSKU] = _isBaseSKU;
        }

        public override string ToString()
        {
            return _productCombineName;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public int AimId
        {
            get { return _aimId;  }
            set { _aimId = value; }
        }
        public DateTime DateAdded
        {
            get { return _dateAdded; }
            set { _dateAdded = value; }
        }
        public DateTime DateStartPeriod
        {
            get { return _dateStartPeriod; }
            set { _dateStartPeriod = value; }
        }
        public DateTime DateApproveStartPeriod
        {
            get { return _dateApproveStartPeriod; }
            set { _dateApproveStartPeriod = value; }
        }
        public bool IsStartDateApproved
        {
            get { return _isStartDateApproved; }
            set { _isStartDateApproved = value; }
        }
        public DateTime DateEndPeriod
        {
            get { return _dateEndPeriod; }
            set { _dateEndPeriod = value; }
        }
        public DateTime DateApproveEndPeriod
        {
            get { return _dateApproveEndPeriod; }
            set { _dateApproveEndPeriod = value; }
        }
        public bool IsEndDateApproved
        {
            get { return _isEndDateApproved; }
            set { _isEndDateApproved = value; }
        }
        public int CreatedUserId
        {
            get { return _createdUserId; }
            set { _createdUserId = value; }
        }
        public int ApprovedUserId
        {
            get { return _approvedUserId; }
            set { _approvedUserId = value; }
        }
        public int RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }
        public int DistrictId
        {
            get { return _districtId; }
            set { _districtId = value; }
        }
        public int CityId
        {
            get { return _cityId; }
            set { _cityId = value; }
        }
        public string KPITypeName
        {
            get { return _KPITypeName; }
            set { _KPITypeName = value; }
        }
        public int DSMId
        {
            get { return _DSMId; }
            set { _DSMId = value; }
        }
        public int SupervisorId
        {
            get { return _supervisorId; }
            set { _supervisorId = value; }
        }
        public int SettlementId
        {
            get { return _settlementId; }
            set { _settlementId = value; }
        }
        public int ProximityId
        {
            get { return _proximityId; }
            set { _proximityId = value; }
        }
        public int ProductCnId
        {
            get { return _productCnId; }
            set { _productCnId = value; }
        }
        public bool IsFocused
        {
            get { return _isFocused; }
            set { _isFocused = value; }
        }
        public int OlTypeId
        {
            get { return _olTypeId; }
            set { _olTypeId = value; }
        }
        public int OlSubTypeId
        {
            get { return _olSubTypeId; }
            set { _olSubTypeId = value; }
        }
        public int OlGroupId
        {
            get { return _olGroupId; }
            set { _olGroupId = value; }
        }
        public int KPITypeId
        {
            get { return _KPITypeId; }
            set { _KPITypeId = value; }
        }
        public string KPIValue
        {
            get { return  _KPIValue; }
            set { _KPIValue = value; }
        }
        public double DeviationPercent
        {
            get { return _deviationPercent; }
            set { _deviationPercent = value; }
        }
        public string HLCode
        {
            get { return _HLCode; }
            set { _HLCode = value; }
        }
        public string ProductCombineName
        {
            get { return _productCombineName; }
            set { _productCombineName = value; }
        }
        public string ProductCombineShortName
        {
            get { return _productCombineShortName; }
            set { _productCombineShortName = value; }
        }
        public int ProductCombineStatus
        {
            get { return _productCombineStatus; }
            set { _productCombineStatus = value; }
        }
        public DateTime ProductCombineDLM
        {
            get { return _productCombineDLM; }
            set { _productCombineDLM = value; }
        }
        public string ProductCombineULM
        {
            get { return _productCombineULM; }
            set { _productCombineULM = value; }
        }
        public bool IsBaseSKU
        {
            get { return _isBaseSKU; }
            set       {          _isBaseSKU = value;   }
        }
    }


}
