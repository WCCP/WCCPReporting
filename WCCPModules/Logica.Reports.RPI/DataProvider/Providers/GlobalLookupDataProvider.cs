﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class GlobalLookupDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "GlobalLookup";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public GlobalLookupDataSet GetData()
        {
            GlobalLookupDataSet dataSet = new GlobalLookupDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.GlobalLookup, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[GlobalLookup.DataSetTable]);
                adapter.Fill(dataSet, GlobalLookup.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Outlet Groups
        /// </summary>
        /// <returns>Outlet Groups list</returns>
        public List<GlobalLookupEntity> GetList()
        {
            return GetFilledList(GetData().GlobalLookup.DefaultView);
        }

        /// <summary>
        /// Returns filtered Outlet Groups list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Outlet Groups list</returns>
        public List<GlobalLookupEntity> GetList(string filterExpression)
        {
            GlobalLookupDataSet globalLookupDataSet = GetData();

            globalLookupDataSet.GlobalLookup.DefaultView.RowFilter = filterExpression;

            return GetFilledList(globalLookupDataSet.GlobalLookup.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(GlobalLookup.SourceTable, GlobalLookup.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               TableName, FieldName, LKey, LValue, Status
                            }
                        );

                    _tableMapping.Add(GlobalLookup.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping GlobalLookup = new DataTableMapping(MappedNames.GlobalLookupSourceTable, MappedNames.GlobalLookupTable);

        protected DataColumnMapping TableName = new DataColumnMapping("TableName", MappedNames.GlobalLookupTableName);
        protected DataColumnMapping FieldName= new DataColumnMapping("FieldName", MappedNames.GlobalLookupFieldName);
        protected DataColumnMapping LKey = new DataColumnMapping("LKey", MappedNames.GlobalLookupLKey);
        protected DataColumnMapping LValue = new DataColumnMapping("LValue", MappedNames.GlobalLookupLValue);
        protected DataColumnMapping Notes = new DataColumnMapping("Notes", MappedNames.GlobalLookupNotes);
        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);

        #endregion Protected members

        #region Private methods

        private List<GlobalLookupEntity> GetFilledList(DataView outletType)
        {
            List<GlobalLookupEntity> result = null;

            try
            {
                foreach (DataRowView lookupValue in outletType)
                {
                    if (result == null)
                    {
                        result = new List<GlobalLookupEntity>();
                    }

                    result.Add(
                                new GlobalLookupEntity
                                    (
                                        lookupValue[TableName.DataSetColumn].ToString(),
                                        lookupValue[FieldName.DataSetColumn].ToString(),
                                        long.Parse(lookupValue[LKey.DataSetColumn] == DBNull.Value ? "-1" : lookupValue[LKey.DataSetColumn].ToString()),
                                        lookupValue[LValue.DataSetColumn].ToString(),
                                        "", //lookupValue[Notes.DataSetColumn].ToString(),
                                        DateTime.MinValue, //DateTime.Parse(lookupValue[DLM.DataSetColumn].ToString()),
                                        DataConvertorHelper.GetDBNullInt32Checked(lookupValue[Status.DataSetColumn])
                                    )
                               );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }

            return result;
        }

        #endregion Private method
    }

    public class GlobalLookupEntity : IBaseEntity
    {
        private string _tableName;
        private string _fieldName;
        private long _lKey;
        private string _lValue;
        private string _notes;
        private DateTime _DLM;
        private int _status;

        public GlobalLookupEntity() { }

        public GlobalLookupEntity(string tableName, string fieldName, long lKey, string lValue, string notes, DateTime dlm, int status)
            : this()
        {
            _tableName = tableName;
            _fieldName = fieldName;
            _lKey = lKey;
            _lValue = lValue;
            _notes = notes;
            _DLM = dlm;
            _status = status;
        }

        public string TableName
        {
            get { return _tableName; }
            set { _tableName = value; }
        }

        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        public long LKey
        {
            get { return _lKey; }
            set { _lKey = value; }
        }

        public string LValue
        {
            get { return _lValue; }
            set { _lValue = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        } 

        public long Id
        {
            get {return _lKey;}
            set {_lKey = value;}
        }

        public string Name
        {
            get {return _lValue;}
            set { _lValue = value; }
        }

        public override string ToString()
        {
            return _lValue;
        }
    }

}
