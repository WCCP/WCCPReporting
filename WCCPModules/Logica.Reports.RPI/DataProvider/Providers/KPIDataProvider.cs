﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class KPIDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "KPI";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public KPIDataSet GetData()
        {

            KPIDataSet dataSet = new KPIDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.KPI, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[KPI.DataSetTable]);
                adapter.Fill(dataSet, KPI.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of KPI Types
        /// </summary>
        /// <returns>KPI Types list</returns>
        public List<KPIEntity> GetList()
        {
            return GetFilledList(GetData().KPI.DefaultView);
        }

        /// <summary>
        /// Returns filtered KPI Types list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>KPI Types list</returns>
        public List<KPIEntity> GetList(string filterExpression)
        {
            KPIDataSet kpiTypes = GetData();

            kpiTypes.KPI.DefaultView.RowFilter = filterExpression;

            return GetFilledList(kpiTypes.KPI.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(KPI.SourceTable, KPI.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               KPITypeId, KPITypeName
                            }
                        );

                    _tableMapping.Add(KPI.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping KPI = new DataTableMapping(MappedNames.DW_RPI_KPISourceTable, MappedNames.DW_RPI_KPITable);

        protected DataColumnMapping KPITypeId = new DataColumnMapping("RPI_KPI_ID", MappedNames.DW_RPI_KPITypeId);
        protected DataColumnMapping KPITypeName = new DataColumnMapping("KPIName", MappedNames.DW_RPI_KPITypeName);

        #endregion Protected members

        #region Private methods

        private List<KPIEntity> GetFilledList(DataView kpi)
        {
            List<KPIEntity> result = null;

            foreach (DataRowView kpiRow in kpi)
            {
                if (result == null)
                {
                    result = new List<KPIEntity>();
                }

                result.Add(
                            new KPIEntity
                                (
                                    long.Parse(kpiRow[KPITypeId.DataSetColumn].ToString()),
                                    kpiRow[KPITypeName.DataSetColumn].ToString()
                                )
                           );
            }
            return result;
        }

        #endregion Private method
    }

    public class KPIEntity : IBaseEntity
    {
        private long _id;
        private string _name;

        public KPIEntity() { }

        public KPIEntity(long id, string name): this()
        {
            _id = id;
            _name = name;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }

}