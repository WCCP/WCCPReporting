﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class OutletTypesDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "OutletTypes";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public OutletTypesDataSet GetData()
        {
            OutletTypesDataSet dataSet = new OutletTypesDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.OutletTypes, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[OutletType.DataSetTable]);
                adapter.Fill(dataSet, OutletType.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Outlet Groups
        /// </summary>
        /// <returns>Outlet Groups list</returns>
        public List<OutletTypeEntity> GetList()
        {
            return GetFilledList(GetData().OutletType.DefaultView);
        }

        /// <summary>
        /// Returns filtered Outlet Groups list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Outlet Groups list</returns>
        public List<OutletTypeEntity> GetList(string filterExpression)
        {
            OutletTypesDataSet outletTypes = GetData();

            outletTypes.OutletType.DefaultView.RowFilter = filterExpression;

            return GetFilledList(outletTypes.OutletType.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(OutletType.SourceTable, OutletType.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               OLTypeId, OLTypeName, OLGroupId, Status
                            }
                        );

                    _tableMapping.Add(OutletType.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping OutletType = new DataTableMapping(MappedNames.OutletTypesSourceTable, MappedNames.OutletTypeTable);

        protected DataColumnMapping OLTypeId = new DataColumnMapping("OLType_id", MappedNames.OutletTypeId);
        protected DataColumnMapping OLGroupId = new DataColumnMapping("OLGroup_id", MappedNames.OutletGroupId);
        protected DataColumnMapping OLTypeName = new DataColumnMapping("OLType_name", MappedNames.OutletTypeName);

        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);

        #endregion Protected members

        #region Private methods

        private List<OutletTypeEntity> GetFilledList(DataView outletType)
        {
            List<OutletTypeEntity> result = null;

            try
            {
                foreach (DataRowView type in outletType)
                {
                    if (result == null)
                    {
                        result = new List<OutletTypeEntity>();
                    }

                    result.Add(
                                new OutletTypeEntity
                                    (
                                        long.Parse(type[OLTypeId.DataSetColumn].ToString()),
                                        type[OLTypeName.DataSetColumn].ToString(),
                                        DataConvertorHelper.GetDBNullInt32Checked(type[OLGroupId.DataSetColumn]),
                                        DateTime.MinValue, //DateTime.Parse(type[DLM.DataSetColumn].ToString()),
                                        DataConvertorHelper.GetDBNullInt32Checked(type[Status.DataSetColumn])
                                    )
                               ); 
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }
            return result;
        }

        #endregion Private method
    }

    public class OutletTypeEntity : IBaseEntity
    {
        private long _id;
        private string _name;
        private int _groupId;
        private DateTime _DLM;
        private int _status;

        public OutletTypeEntity() { }

        public OutletTypeEntity(long id, string name, int groupId, DateTime dlm, int status)
            : this()
        {
            _id = id;
            _name = name;
            _groupId = groupId;
            _DLM = dlm;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }

}