﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class SupervisorsDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "Supervisors";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public SupervisorsDataSet GetData()
        {
            SupervisorsDataSet dataSet = new SupervisorsDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.Supervisors, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;
                //Logica.Reports.DataAccess.DataAccessLayer.QueryFillDataTable( 
                adapter.TableMappings.Add(TableMapping[Supervisor.DataSetTable]);
                adapter.Fill(dataSet, Supervisor.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Supervisors
        /// </summary>
        /// <returns>Supervisors list</returns>
        public List<SupervisorEntity> GetList()
        {
            return GetFilledList(GetData().Supervisor.DefaultView);
        }

        /// <summary>
        /// Returns filtered Supervisors list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Supervisors list</returns>
        public List<SupervisorEntity> GetList(string filterExpression)
        {
            SupervisorsDataSet supervisors = GetData();

            if (!string.IsNullOrEmpty(filterExpression) )
                supervisors.Supervisor.DefaultView.RowFilter = filterExpression;

            return GetFilledList(supervisors.Supervisor.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(Supervisor.SourceTable, Supervisor.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               SupervisorId, SupervisorName, RegionId, DSMId, RMId
                            }
                        );

                    _tableMapping.Add(Supervisor.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping Supervisor = new DataTableMapping(MappedNames.SupervisorSourceTable, MappedNames.SupervisorTable);

        protected DataColumnMapping SupervisorId = new DataColumnMapping("Supervisor_id", MappedNames.SupervisorId);
        protected DataColumnMapping SupervisorName = new DataColumnMapping("Supervisor_name", MappedNames.SupervisorName);
        protected DataColumnMapping UserTypeId = new DataColumnMapping("UserType_ID", MappedNames.SupervisorUserTypeId);
        protected DataColumnMapping RegionId = new DataColumnMapping("Region_Id", MappedNames.RegionId);
        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);
        protected DataColumnMapping DSMId = new DataColumnMapping("DSM_Id", MappedNames.SupervisorDSMId);
        protected DataColumnMapping RMId = new DataColumnMapping("RM_Id", MappedNames.SupervisorRMId);

        #endregion Protected members

        #region Private methods

        public /*private*/ List<SupervisorEntity> GetFilledList(DataView supervisorsView)
        {
            List<SupervisorEntity> result = null;

            try
            {
                foreach (DataRowView supervisor in supervisorsView)
                {
                    if (result == null)
                    {
                        result = new List<SupervisorEntity>();
                    }

                    result.Add(
                                new SupervisorEntity
                                    (
                                        long.Parse(supervisor[SupervisorId.DataSetColumn].ToString()),
                                        supervisor[SupervisorName.DataSetColumn].ToString(),
                                        -1,//int.Parse(supervisor[UserTypeId.DataSetColumn].ToString()),
                                        DataConvertorHelper.GetDBNullInt32Checked(supervisor[RegionId.DataSetColumn]),
                                        DateTime.MinValue, //DateTime.Parse(supervisor[DLM.DataSetColumn].ToString()),
                                        -1 //int.Parse(supervisor[Status.DataSetColumn].ToString())
                                    )
                               );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }

            return result;
        }

        #endregion Private method
    }

    public class SupervisorEntity : IBaseEntity
    {
        private long _id;
        private string _name;
        private int _userTypeId;
        private int _regionId;
        private DateTime _DLM;
        private int _status;

        public SupervisorEntity() { }

        public SupervisorEntity(long id, string name, int userTypeId, int regionId, DateTime dlm, int status)
            : this()
        {
            _id = id;
            _name = name;
            _userTypeId = userTypeId;
            _regionId = regionId;
            _DLM = dlm;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int UserTypeId
        {
            get { return _userTypeId; }
            set { _userTypeId = value; }
        }

        public int RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }

}