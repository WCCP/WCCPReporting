﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class OutletSubTypesDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "OutletSubTypes";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public OutletSubTypesDataSet GetData()
        {
            OutletSubTypesDataSet dataSet = new OutletSubTypesDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.OutletSubTypes, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[OutletSubType.DataSetTable]);
                adapter.Fill(dataSet, OutletSubType.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Outlet Groups
        /// </summary>
        /// <returns>Outlet Groups list</returns>
        public List<OutletSubTypeEntity> GetList()
        {
            return GetFilledList(GetData().OutletSubType.DefaultView);
        }

        /// <summary>
        /// Returns filtered Outlet Groups list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Outlet Groups list</returns>
        public List<OutletSubTypeEntity> GetList(string filterExpression)
        {
            OutletSubTypesDataSet outletSubTypes = GetData();

            outletSubTypes.OutletSubType.DefaultView.RowFilter = filterExpression;

            return GetFilledList(outletSubTypes.OutletSubType.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(OutletSubType.SourceTable, OutletSubType.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               OLSubTypeId, OLSubTypeName, OLTypeId,  Status
                            }
                        );

                    _tableMapping.Add(OutletSubType.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping OutletSubType = new DataTableMapping(MappedNames.OutletSubTypesSourceTable, MappedNames.OutletSubTypeTable);

        protected DataColumnMapping OLSubTypeId = new DataColumnMapping("OLSubType_id", MappedNames.OutletSubTypeId);
        protected DataColumnMapping OLTypeId = new DataColumnMapping("OLType_id", MappedNames.OutletTypeId);
        protected DataColumnMapping OLSubTypeName = new DataColumnMapping("OLSubTypeName", MappedNames.OutletSubTypeName);

        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);

        #endregion Protected members

        #region Private methods

        private List<OutletSubTypeEntity> GetFilledList(DataView outletType)
        {
            List<OutletSubTypeEntity> result = null;

            try
            {
                foreach (DataRowView type in outletType)
                {
                    if (result == null)
                    {
                        result = new List<OutletSubTypeEntity>();
                    }

                    result.Add(
                                new OutletSubTypeEntity
                                    (
                                        long.Parse(type[OLSubTypeId.DataSetColumn].ToString()),
                                        type[OLSubTypeName.DataSetColumn].ToString(),
                                        DataConvertorHelper.GetDBNullInt32Checked(type[OLTypeId.DataSetColumn]),
                                        DateTime.MinValue, //DateTime.Parse(type[DLM.DataSetColumn].ToString()),
                                        DataConvertorHelper.GetDBNullInt32Checked(type[Status.DataSetColumn])
                                    )
                               );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
            }

            return result;
        }

        #endregion Private method
    }

    public class OutletSubTypeEntity : IBaseEntity
    {
        private long _id;
        private string _name;
        private int _typeId;
        private DateTime _DLM;
        private int _status;

        public OutletSubTypeEntity() { }

        public OutletSubTypeEntity(long id, string name, int typeId, DateTime dlm, int status)
            : this()
        {
            _id = id;
            _name = name;
            _typeId = typeId;
            _DLM = dlm;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }

}