﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    /// <summary>
    /// Common interface for data entities which contain Id and Name fields
    /// </summary>
    public interface IBaseEntity
    {
        /// <summary>
        /// ID field
        /// </summary>
        long Id { get; set; }
        /// <summary>
        /// Name field
        /// </summary>
        string Name { get; set; }
    }


}
