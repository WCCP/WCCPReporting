﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{
    class DSMDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "DSM";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public DSMDataSet GetData()
        {
            DSMDataSet dataSet = new DSMDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.DSM, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[DSM.DataSetTable]);
                adapter.Fill(dataSet, DSM.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of DSM
        /// </summary>
        /// <returns>DSM list</returns>
        public List<DSMEntity> GetList()
        {
            return GetFilledList(GetData().DSM.DefaultView);
        }

        /// <summary>
        /// Returns filtered DSM list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>DSM list</returns>
        public List<DSMEntity> GetList(string filterExpression)
        {
            DSMDataSet dsmDataSet = GetData();

            dsmDataSet.DSM.DefaultView.RowFilter = filterExpression;

            return GetFilledList(dsmDataSet.DSM.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(DSM.SourceTable, DSM.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               DSMId, RMId, DSMName
                            }
                        );

                    _tableMapping.Add(DSM.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping DSM = new DataTableMapping(MappedNames.DSMSourceTable, MappedNames.DSMTable);

        protected DataColumnMapping DSMId = new DataColumnMapping("DSM_Id", MappedNames.DSMId);
        protected DataColumnMapping RMId = new DataColumnMapping("RM_Id", MappedNames.DSMRMId);
        protected DataColumnMapping DSMName = new DataColumnMapping("DSM_Name", MappedNames.DSMName);
        protected DataColumnMapping Pass = new DataColumnMapping("Pass", MappedNames.Pass);
        protected DataColumnMapping ULM = new DataColumnMapping("ULM", MappedNames.Pass);
        protected DataColumnMapping Email = new DataColumnMapping("Email", MappedNames.Pass);
        protected DataColumnMapping ChannelTypeId = new DataColumnMapping("ChanelType_Id", MappedNames.Pass);

        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);
        protected DataColumnMapping DLM = new DataColumnMapping("DLM", MappedNames.DLM);

        #endregion Protected members

        #region Private methods

        private List<DSMEntity> GetFilledList(DataView dsmView)
        {
            List<DSMEntity> result = null;

            foreach (DataRowView dsm in dsmView)
            {
                if (result == null)
                {
                    result = new List<DSMEntity>();
                }
                try
                {
                    DSMEntity dsmEntity = new DSMEntity
                        (
                            long.Parse(dsm[DSMId.DataSetColumn].ToString()),
                            DataConvertorHelper.GetDBNullInt32Checked(dsm[RMId.DataSetColumn]),
                            dsm[DSMName.DataSetColumn].ToString(),
                            "", //dsm[Pass.DataSetColumn].ToString(),
                            DateTime.MinValue, // DateTime.Parse(dsm[DLM.DataSetColumn].ToString()),
                            "", //dsm[ULM.DataSetColumn].ToString(),
                            "", //dsm[Email.DataSetColumn].ToString(),
                            -1, //int.Parse(dsm[ChannelTypeId.DataSetColumn].ToString()),
                            false //bool.Parse(dsm[Status.DataSetColumn].ToString())
                        );

                    result.Add(dsmEntity);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format(Resource.EntityProcessingException, GetType(), ModuleName), ex);
                }

            }
            return result;
        }

        #endregion Private method
    }

    public class DSMEntity : IBaseEntity
    {
        private long _id;
        private int _rmId;
        private string _name;
        private string _pass;
        private DateTime _DLM;
        private string _ULM;
        private string _eMail;
        private int _channelTypeId;
        private bool _status;

        public DSMEntity() { }

        public DSMEntity(long id, int rmId, string name, string pass, DateTime dlm, string ulm, string email, int channelTypeId, bool status)
            : this()
        {
            _id = id;
            _rmId = rmId;
            _name = name;
            _pass = pass;
            _DLM = dlm;
            _ULM = ulm;
            _eMail = email;
            _channelTypeId = channelTypeId;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int RmId
        {
            get { return _rmId; }
            set { _rmId = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Pass
        {
            get { return _pass; }
            set { _pass = value; }
        }

        public DateTime DLM
        {
            get { return _DLM; }
            set { _DLM = value; }
        }

        public string ULM
        {
            get { return _ULM; }
            set { _ULM = value; }
        }

        public string Email
        {
            get { return _eMail; }
            set { _eMail = value; }
        }

        public int ChannelTypeId
        {
            get { return _channelTypeId; }
            set { _channelTypeId = value; }
        }

        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }

}