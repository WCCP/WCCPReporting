﻿using System;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;
using Logica.Reports.RPI.Authentication;

namespace Logica.Reports.RPI.DataProvider.Providers {
    internal static class DataAccessProvider {
        public static string PositionCoPpmM5 = "CO_PPM_M5";
        private static string _lastPositionName = "";
        private static int _lastPositionId = int.MinValue;
        private static bool _hasUserPosition = false;
        private const string QueryGetPositionId = "select * from dbo.DW_URM_Position where PositionName = '{0}'";
        private const string SpDwUrmListPositions = "dbo.spDW_URM_ListPositions";

        public static int GetPositionIdByName(string positionName) {
            if (_lastPositionName.Equals(positionName))
                return _lastPositionId;

            DataSet lDataSet = DataAccessLayer.ExecuteQuery(string.Format(QueryGetPositionId, positionName));
            DataTable lDataTable = lDataSet.Tables[0];
            if (lDataTable.Rows.Count > 0) {
                _lastPositionName = positionName;
                return Int32.Parse(lDataTable.Rows[0][0].ToString());
            }
            return _lastPositionId;
        }

        public static bool HasUserPosition(string positionName) {
            int lPositionId = GetPositionIdByName(positionName);
            return HasUserPosition(lPositionId);
        }

        public static bool HasUserPosition(int positionId) {
            if (_lastPositionId == positionId)
                return _hasUserPosition;

            DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SpDwUrmListPositions,
                new[] { new SqlParameter("@User_ID", AuthenticationManager.Instance.CurrentUserId),
                        new SqlParameter("@Position_ID", positionId),
                        new SqlParameter("@isOFF", 0)
                      });
            DataTable lDataTable = lDataSet.Tables[0];
            _hasUserPosition = false;
            if (lDataTable.Rows.Count > 0) {
                _lastPositionId = positionId;
                _hasUserPosition = true;
            }
            return _hasUserPosition;
        }
    }
}
