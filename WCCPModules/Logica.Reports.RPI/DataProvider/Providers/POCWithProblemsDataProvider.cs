﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.DataProvider.Entities.RegionsDataSetTableAdapters;
using Logica.Reports.RPI.Authentication;
using System.Windows.Forms;
using Logica.Reports.Common;

namespace Logica.Reports.RPI.DataProvider.Providers
{

    public class POCWithProblemsDataProvider : DataProviderBase
    {
        #region Private fields

        #endregion Private fields

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "POCWithProblems";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns>Typed DataSet</returns>
        public POCWithProblemsDataSet GetData(DateTime lowerDate, DateTime upperDate, long routeId)
        {
            POCWithProblemsDataSet dataSet = new POCWithProblemsDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                //string sqlScript = AuthenticationManager.Instance.CurrentUserLevel < 3
                string sqlScript = DataAccessLayer.IsLDB ? ScriptProvider.POCLocal : ScriptProvider.POCCentral;

                SqlCommand selectCommand = new SqlCommand(sqlScript, DatabaseConnection);
                selectCommand.CommandTimeout = 600;
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.AddWithValue("@lowerDate", lowerDate.Date);
                selectCommand.Parameters.AddWithValue("@upperDate", upperDate.Date);
                if ( routeId >= 0 )
                    selectCommand.Parameters.AddWithValue("@Route_id", routeId);
                else
                    selectCommand.Parameters.AddWithValue("@Route_id", DBNull.Value);

                /*
                selectCommand.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                selectCommand.Parameters.Add("@upperDate", SqlDbType.DateTime);

                selectCommand.Parameters["@lowerDate"].Value =
                    lowerDate == DateTime.MinValue ? new DateTime(1899, 1, 1) : lowerDate;
                selectCommand.Parameters["@upperDate"].Value =
                    upperDate == DateTime.MinValue ? new DateTime(1899, 1, 1) : upperDate;
                */

                adapter.SelectCommand = selectCommand;
                adapter.TableMappings.Add(TableMapping ["POC"]);
                adapter.Fill(dataSet, "POC");
            }
            catch ( SystemException ex )
            {
                //throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
                ErrorManager.ShowErrorBox(ex.Message);
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }


        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping pocMap = new DataTableMapping(POC.SourceTable, POC.DataSetTable);
                    pocMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                                OutletId,
                                OutletName,
                                OutletSubTypeId,
                                OutletSubTypeNameColumn,
                                ProductIdColumn,
                                ProductNameColumn,
                                PTCColumn,
                                PTPColumn,
                                AreaIdColumn,
                                AreaNameColumn,
                                CityIdColumn,
                                CityNameColumn,
                                DistrictIdColumn,
                                DistrictNameColumn,
                                RegionIdColumn,
                                RegionNameColumn,
                                RPIAimIdColumn,
                                RPTCColumn,
                                PTCPreviousPeriodColumn,
                                PTPPreviousPeriodColumn,
                                HasPriceBeenCutColumn,
                                IsPriceMatchForRPTCColumn,
                                ReportMonthColumn,
                                CurrentMarkupColumn,
                                PreviousMarkupColumn,
                                TotalSalesDALColumn,
                                OutletMarkupColumn,
                                MerchIdColumn,
                                MerchandiserNameColumn,
                                SupervisorIdColumn,
                                SupervisorNameColumn,
                                DSMIdColumn,
                                DSMNameColumn,
                                RMIdColumn,
                                RMNameColumn,
                                RouteIdColumn,
                                RouteNameColumn
                            }
                        );

                    _tableMapping.Add(POC.SourceTable, pocMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping POC = new DataTableMapping("POC", MappedNames.POC);

        protected DataColumnMapping OutletId = new DataColumnMapping("OL_id", MappedNames.OutletId );
        protected DataColumnMapping OutletName = new DataColumnMapping("OLName", MappedNames.OutletName );
        protected DataColumnMapping OutletSubTypeId = new DataColumnMapping("OLSubType_Id", MappedNames.OutletSubTypeId );
        protected DataColumnMapping OutletSubTypeNameColumn = new DataColumnMapping("OLSubTypeName", MappedNames.OLSubTypeName );
        protected DataColumnMapping ProductIdColumn = new DataColumnMapping("Product_Id", MappedNames.ProductId );
        protected DataColumnMapping ProductNameColumn = new DataColumnMapping("ProductName", MappedNames.ProductName );
        protected DataColumnMapping PTCColumn = new DataColumnMapping("PTC", MappedNames.PTC );
        protected DataColumnMapping PTPColumn = new DataColumnMapping("PTP", MappedNames.PTP );
        protected DataColumnMapping AreaIdColumn = new DataColumnMapping("Area_Id", MappedNames.AreaId );
        protected DataColumnMapping AreaNameColumn = new DataColumnMapping("Area_name", MappedNames.AreaName );
        protected DataColumnMapping CityIdColumn = new DataColumnMapping("City_Id", MappedNames.CityId );
        protected DataColumnMapping CityNameColumn = new DataColumnMapping("City_name", MappedNames.CityName );
        protected DataColumnMapping DistrictIdColumn = new DataColumnMapping("District_Id", MappedNames.DistrictId );
        protected DataColumnMapping DistrictNameColumn = new DataColumnMapping("District_name", MappedNames.DistrictName );
        protected DataColumnMapping RegionIdColumn = new DataColumnMapping("Region_Id", MappedNames.RegionId );
        protected DataColumnMapping RegionNameColumn = new DataColumnMapping("Region_name", MappedNames.RegionName );
        protected DataColumnMapping RPIAimIdColumn = new DataColumnMapping("RPI_Aim_ID", MappedNames.RPIAimId );
        protected DataColumnMapping RPTCColumn = new DataColumnMapping("RPTC", MappedNames.RPTC );
        protected DataColumnMapping PTCPreviousPeriodColumn = new DataColumnMapping("PTCPreviousPeriod", MappedNames.PTCPreviousPeriod );
        protected DataColumnMapping PTPPreviousPeriodColumn = new DataColumnMapping("PTPPreviousPeriod", MappedNames.PTPPreviousPeriod );
        protected DataColumnMapping HasPriceBeenCutColumn = new DataColumnMapping("HasPriceBeenCut", MappedNames.HasPriceBeenCut );
        protected DataColumnMapping IsPriceMatchForRPTCColumn = new DataColumnMapping("IsPriceMatchForRPTC", MappedNames.IsPriceMatchForRPTC );
        protected DataColumnMapping ReportMonthColumn = new DataColumnMapping("ReportMonth", MappedNames.ReportMonth );
        protected DataColumnMapping CurrentMarkupColumn = new DataColumnMapping("CurrentMarkup", MappedNames.CurrentMarkup );
        protected DataColumnMapping PreviousMarkupColumn = new DataColumnMapping("PreviousMarkup" , MappedNames.PreviousMarkup );
        protected DataColumnMapping TotalSalesDALColumn = new DataColumnMapping("TotalSalesDAL", MappedNames.TotalSalesDAL );
        protected DataColumnMapping OutletMarkupColumn = new DataColumnMapping("OutletMarkup" , MappedNames.OutletMarkup );
        protected DataColumnMapping MerchIdColumn = new DataColumnMapping("Merch_Id", MappedNames.MerchId );
        protected DataColumnMapping MerchandiserNameColumn = new DataColumnMapping("MerchName", MappedNames.MerchandiserName );
        protected DataColumnMapping SupervisorIdColumn = new DataColumnMapping("Supervisor_Id", MappedNames.SupervisorId );
        protected DataColumnMapping SupervisorNameColumn = new DataColumnMapping("Supervisor_name" , MappedNames.SupervisorName );
        protected DataColumnMapping DSMIdColumn = new DataColumnMapping("DSM_Id", MappedNames.DSMId );
        protected DataColumnMapping DSMNameColumn = new DataColumnMapping("DSM_Name" , MappedNames.DSMName );
        protected DataColumnMapping RMIdColumn = new DataColumnMapping("RM_Id", MappedNames.RMId);
        protected DataColumnMapping RMNameColumn = new DataColumnMapping("RM_Name", MappedNames.RMName);
        protected DataColumnMapping RouteIdColumn = new DataColumnMapping("Route_Id", MappedNames.RouteId);
        protected DataColumnMapping RouteNameColumn = new DataColumnMapping("RouteName", MappedNames.RouteName );

        #endregion Protected members
    }
}
