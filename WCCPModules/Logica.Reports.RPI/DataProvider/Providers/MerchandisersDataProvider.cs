﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;

namespace Logica.Reports.RPI.DataProvider.Providers
{

    class MerchandisersDataProvider : DataProviderBase
    {

        #region Public properties

        /// <summary>
        /// Gets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string ModuleName
        {
            get
            {
                return "Merchandosers";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <returns></returns>
        public MerchandisersDataSet GetData()
        {
            MerchandisersDataSet dataSet = new MerchandisersDataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();

                SqlCommand selectCommand = new SqlCommand(ScriptProvider.Merchandisers, DatabaseConnection);
                selectCommand.CommandTimeout = 30;
                selectCommand.CommandType = CommandType.Text;// CommandType.StoredProcedure;

                adapter.SelectCommand = selectCommand;

                adapter.TableMappings.Add(TableMapping[Merchandiser.DataSetTable]);
                adapter.Fill(dataSet, Merchandiser.SourceTable);
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ModuleName, ex.Message));
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// Returns list of Merchandisers
        /// </summary>
        /// <returns>Merchandisers list</returns>
        public List<MerchandiserEntity> GetList()
        {
            return GetFilledList(GetData().Merchandiser.DefaultView);
        }

        /// <summary>
        /// Returns filtered Merchandisers list
        /// </summary>
        /// <param name="filterExpression">Expression which specifies column name and comparison</param>
        /// <returns>Merchandisers list</returns>
        public List<MerchandiserEntity> GetList(string filterExpression)
        {
            MerchandisersDataSet merchandisers = GetData();

            if (!string.IsNullOrEmpty(filterExpression))
                merchandisers.Merchandiser.DefaultView.RowFilter = filterExpression;

            return GetFilledList(merchandisers.Merchandiser.DefaultView);
        }
        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override IDictionary<string, DataTableMapping> TableMapping
        {
            get
            {
                if (_tableMapping == null)
                {
                    _tableMapping = new Dictionary<string, DataTableMapping>();

                    DataTableMapping tableMap = new DataTableMapping(Merchandiser.SourceTable, Merchandiser.DataSetTable);
                    tableMap.ColumnMappings.AddRange
                        (
                            new DataColumnMapping[]
                            {
                               MerchandiserId, MerchandiserName, SupervisorId, Status
                            }
                        );

                    _tableMapping.Add(Merchandiser.DataSetTable, tableMap);
                }

                return _tableMapping;
            }
        }

        #endregion

        #region Protected members

        protected DataTableMapping Merchandiser = new DataTableMapping(MappedNames.MerchandiserSourceTable, MappedNames.MerchandiserTable);

        protected DataColumnMapping MerchandiserId = new DataColumnMapping("Merch_id", MappedNames.MerchId);
        protected DataColumnMapping MerchandiserName = new DataColumnMapping("MerchName", MappedNames.MerchandiserName);
        protected DataColumnMapping SupervisorId = new DataColumnMapping("Supervisor_id", MappedNames.SupervisorId);
        protected DataColumnMapping Status = new DataColumnMapping("Status", MappedNames.Status);

        #endregion Protected members

        #region Private methods

        private List<MerchandiserEntity> GetFilledList(DataView merchandisersView)
        {
            List<MerchandiserEntity> result = null;

            foreach (DataRowView merchandiser in merchandisersView)
            {
                if (result == null)
                {
                    result = new List<MerchandiserEntity>();
                }

                result.Add(
                            new MerchandiserEntity
                                (
                                    long.Parse(merchandiser[MerchandiserId.DataSetColumn].ToString()),
                                    merchandiser[MerchandiserName.DataSetColumn].ToString(),
                                    DataConvertorHelper.GetDBNullInt32Checked(merchandiser[SupervisorId.DataSetColumn]),
                                    DataConvertorHelper.GetDBNullInt32Checked(merchandiser[Status.DataSetColumn])
                                )
                           );
            }
            return result;
        }

        #endregion Private method
    }

    public class MerchandiserEntity : IBaseEntity
    {
        private long _id;
        private string _name;
        private int _superviserId;
        private int _status;

        public MerchandiserEntity() { }

        public MerchandiserEntity(long id, string name, int supervisorId, int status)
            : this()
        {
            _id = id;
            _name = name;
            _superviserId = supervisorId;
            _status = status;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int SupervisorId
        {
            get { return _superviserId; }
            set { _superviserId = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
