﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.RPI.DataProvider
{
    public static class MappedNames
    {
        public const string CountrySourceTable = "tblCountry";
        public const string CountryTable = "Country";

        public const string RegionSourceTable = "tblRegions";
        public const string RegionTable = "Region";

        public const string DistrictSourceTable = "tblDistricts";
        public const string DistrictTable = "District";

        public const string CitySourceTable = "tblCities";
        public const string CityTable = "City";

        public const string CountryId = "CountryIdColumn";
        public const string CountryName = "CountryNameColumn"; 
        public const string CurrencyId = "CurrencyIdColumn"; 
        public const string VAT = "VATColumn"; 
        public const string RegionId = "RegionIdColumn"; 
        public const string RegionName = "RegionNameColumn";
        public const string RegionRMId = "RegionRMIdColumn";
        public const string DistrictId = "DistrictIdColumn";
        public const string DistrictName = "DistrictNameColumn"; 
        public const string CityId = "CityIdColumn"; 
        public const string CityName = "CityNameColumn"; 
        public const string DLM = "DLMColumn";
        public const string Status = "StatusColumn";
        public const string CityCntAreaId = "CntAreaIdColumn";
        public const string CityCityShortName = "CityShortNameColumn";
        public const string CitySettlementId = "SettlementIdColumn";
        public const string CityPopulationId = "PopulationIdColumn";


        public const string AimTable = "Aim";

        public const string AimId = "AimIdColumn";
        public const string DateAdded = "DateAddedColumn";
        public const string DateStartPeriod = "DateStartPeriodColumn";
        public const string DateApproveStartPeriod = "DateApproveStartPeriodColumn";
        public const string IsStartDateApproved = "IsStartDateApprovedColumn";
        public const string DateEndPeriod = "DateEndPeriodColumn";
        public const string DateApproveEndPeriod = "DateApproveEndPeriodColumn";
        public const string IsEndDateApproved = "IsEndDateApprovedColumn";
        public const string CreatedUserId = "CreatedUserIdColumn";
        public const string ApprovedUserId = "ApprovedUserIdColumn";
        //public const string RegionId = "RegionIdColumn";
        //public const string DistrictId = "DistrictIdColumn";
        //public const string CityId = "CityIdColumn";
        public const string KPITypeName = "KPITypeNameColumn";
        public const string DSMId = "DSMIdColumn";
        public const string RMId = "RMIdColumn";
        public const string SupervisorId = "SupervisorIdColumn";
        public const string SettlementId = "SettlementIdColumn";
        public const string SettlementName = "SettlementNameColumn";
        public const string ProximityId = "ProximityIdColumn";
        public const string ProductCnId = "ProductCnIdColumn";
        public const string IsFocused = "IsFocusedColumn";
        public const string OLTypeId = "OLTypeIdColumn";
        public const string OLSubTypeId = "OLSubTypeIdColumn";
        public const string OLGroupId = "OLGroupIdColumn";
        public const string KPITypeId = "KPITypeIdColumn";
        public const string KPIValue = "KPIValueColumn";

        public const string DeviationPercent = "DeviationPercentColumn";
        public const string HLCode = "HLCodeColumn";
        public const string ProductCombineName = "ProductCombineNameColumn";

        public const string ProductCombineShortName = "ProductCombineShortNameColumn";

        public const string OLGroupName = "OLGroupNameColumn";
        public const string OLTypeName = "OLTypeNameColumn";
        public const string OLSubTypeName = "OLSubTypeNameColumn";
        public const string ProximityName = "ProximityNameColumn";
        //public const string Status = "StatusColumn";
        //public const string DLM = "DLMColumn";
        public const string ULM = "ULMColumn";

        public const string SettlementSourceTable = "tblSettlement";
        public const string SettlementTable = "Settlement";

        public const string Settlement = "SettlementColumn";

        public const string OutletGroupsSourceTable = "tblOutletGroups";
        public const string OutletGroupTable = "OutletGroup";

        public const string OutletGroupId = "OLGroupIdColumn";
        public const string OutletGroupName = "OLGroupNameColumn";

        public const string OutletTypesSourceTable = "tblOutletTypes";
        public const string OutletTypeTable = "OutletType";

        public const string OutletTypeId = "OLTypeIdColumn";
        public const string OutletTypeName = "OLTypeNameColumn";
                
        public const string OutletSubTypesSourceTable = "tblOutletSubTypes";
        public const string OutletSubTypeTable = "OutletSubType";
        
        public const string OutletSubTypeId = "OLSubTypeIdColumn";
        public const string OutletSubTypeName = "OLSubTypeNameColumn";

        public const string DW_RPI_KPISourceTable = "DW_RPI_KPI";
        public const string DW_RPI_KPITable = "KPI";

        public const string DW_RPI_KPITypeId = "TypeIdColumn";
        public const string DW_RPI_KPITypeName = "TypeNameColumn";

        public const string GlobalLookupSourceTable = "tblGlobalLookup";
        public const string GlobalLookupTable = "GlobalLookup";

        public const string GlobalLookupTableName = "TableNameColumn";
        public const string GlobalLookupFieldName = "FieldNameColumn";
        public const string GlobalLookupLKey = "LKeyColumn";
        public const string GlobalLookupLValue = "LValueColumn";
        public const string GlobalLookupNotes = "NotesColumn";

        public const string SupervisorSourceTable = "tblSupervisors";
        public const string SupervisorTable = "Supervisor";

        //public const string SupervisorId = "SupervisorIdColumn";
        public const string SupervisorName = "SupervisorNameColumn";
        public const string SupervisorUserTypeId = "SupervisorUserTypeIdColumn";
        public const string SupervisorRegionId = "RegionIdColumn";
        public const string SupervisorDSMId = "DSMIdColumn";
        public const string SupervisorRMId = "RMIdColumn";


        public const string DSMSourceTable = "tblDSM";
        public const string DSMTable = "DSM";

        public const string DSMRMId = "RMIdColumn";
        public const string DSMName = "DSMNameColumn";
        public const string RMName = "RMNameColumn";
        public const string Pass = "PassColumn";
        public const string DSMEmail = "DSMEmailColumn";
        public const string DSMChannelTypeId = "DSMChannelTypeIdColumn";

        public const string ProductCombineSourceTable = "tblProductCombine";
        public const string ProductCombineTable = "ProductCombine";

        public const string Deviation = "DeviationColumn";
        public const string IsBaseSKU = "IsBaseSKUColumn";
        public const string ComposedRegionName = "ComposedRegionNameColumn";
        public const string ComposedDistrictName = "ComposedDistrictNameColumn";
        public const string ComposedCityName = "ComposedCityNameColumn";

        public const string POC = "POC";

        public const string OutletId = "OutletIdColumn";
        public const string OutletName = "OutletNameColumn";
        //public const string OutletSubTypeId = "OutletSubTypeIdColumn";
        //public const string OutletSubTypeName = "OutletSubTypeNameColumn";
        public const string ProductId = "ProductIdColumn";
        public const string ProductName = "ProductNameColumn";
        public const string PTC = "PTCColumn";
        public const string PTP = "PTPColumn";
        public const string AreaId = "AreaIdColumn";
        public const string AreaName = "AreaNameColumn";
        //public const string CityId = "CityIdColumn";
        //public const string CityName = "CityNameColumn";
        //public const string DistrictId = "DistrictIdColumn";
        //public const string DistrictName = "DistrictNameColumn";
        //public const string RegionId = "RegionIdColumn";
        //public const string RegionName = "RegionNameColumn";
        public const string RPIAimId = "RPIAimIdColumn";
        public const string RPTC = "RPTCColumn";
        public const string PTCPreviousPeriod = "PTCPreviousPeriodColumn";
        public const string PTPPreviousPeriod = "PTPPreviousPeriodColumn";
        public const string HasPriceBeenCut = "HasPriceBeenCutColumn";
        public const string IsPriceMatchForRPTC = "IsPriceMatchForRPTCColumn";
        public const string ReportMonth = "ReportMonthColumn";
        public const string CurrentMarkup = "CurrentMarkupColumn";
        public const string PreviousMarkup = "PreviousMarkupColumn";
        public const string TotalSalesDAL = "TotalSalesDALColumn";
        public const string OutletMarkup = "OutletMarkupColumn";
        public const string MerchId = "MerchIdColumn";
        public const string MerchandiserName = "MerchandiserNameColumn";
        //public const string SupervisorId = "SupervisorIdColumn";
        //public const string SupervisorName = "SupervisorNameColumn";
        //public const string DSMId = "DSMIdColumn";
        //public const string DSMName = "DSMNameColumn";
        public const string RouteId = "RouteIdColumn";
        public const string RouteName = "RouteNameColumn";

        public const string MerchandiserSourceTable = "tblMerchandisers";
        public const string MerchandiserTable = "Merchandiser";

        public const string RouteSourceTable = "tblRoutes";
        public const string RouteTable = "Route";

        public const string IsOrdered = "IsOrderedColumn";
    }
}
