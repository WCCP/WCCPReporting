using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.Authentication;

namespace Logica.Reports.RPI.Forms
{
    /// <summary>
    /// Form for editing parameters of Focused SKU
    /// </summary>
    public partial class EditFocusedSKUForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private members

        private CascadedAimEntity _editedAim;
        private CascadedAimEntity _oldEditedAim;

        private IList<ProductCombineKPI> _productCombineList = new List<ProductCombineKPI>();
        private IList<SettlementEntity> _settlementList;
        private IList<OutletGroupEntity> _outletGroupList;
        private IList<OutletTypeEntity> _outletTypeList;
        private IList<OutletSubTypeEntity> _outletSubTypeList;
        private IList<KPIEntity> _kpiList;
        private IList<GlobalLookupEntity> _proximityList;
        private IList<SupervisorEntity> _supervisorList;
        private IList<DSMEntity> _dsmList;
        private IList<ProductCombineEntity> _productsList;

        private int _regionId;
        private int _districtId;
        private int _cityId;

        private const string OutletTypeBaseFilter = @"OLTypeIdColumn > 0";
        private const string OutletTypeGroupIdFilter = @"OLGroupIdColumn = ";
        private const string OutletSubTypeBaseFilter = @"OlSubTypeIdColumn > 0";
        private const string OutletSubTypeTypeIdFilter = @"OLTypeIdColumn = ";
        private const string GlobalLookupProximityBaseFilter = @"TableNameColumn = 'tblOutLets' AND FieldNameColumn = 'Proximity' ";
        private const string SupervisorBaseFilter = @"SupervisorIdColumn > 0";

        #endregion Private members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public EditFocusedSKUForm()
        {
            InitializeComponent();

            LoadData();
            SetupControls();

            InitEvents();

            SetupUI();
        }

        #endregion Constructors

        #region Public properties

        /// <summary>
        /// Region ID
        /// </summary>
        public int RegionId
        {
            get
            {
                return _regionId;
            }
            set
            {
                teRegionName.Text = DataMediator.GetRegionName(value);

                if (_regionId == value)
                    return;
                _regionId = value;
            }
        }

        /// <summary>
        /// District ID
        /// </summary>
        public int DistrictId
        {
            get
            {
                return _districtId;
            }
            set
            {
                if (_districtId == value)
                    return;
                _districtId = value;
            }
        }

        /// <summary>
        /// City ID
        /// </summary>
        public int CityId
        {
            get
            {
                return _cityId;
            }
            set
            {
                if (_cityId == value)
                    return;
                _cityId = value;
            }
        }

        /// <summary>
        /// Edited Focused SKU
        /// </summary>
        public CascadedAimEntity EditedFocusedSKU
        {
            get
            {
                DateTime now = DateTime.Now;

                //_editedAim.DateAdded = now;

                _editedAim.DateStartPeriod = deStartPeriod.DateTime;
                _editedAim.IsStartDateApproved = chkDateStartPeriodApproved.Checked;
                _editedAim.DateApproveStartPeriod =
                    chkDateStartPeriodApproved.Checked != _oldEditedAim.IsStartDateApproved ? now : _editedAim.DateApproveStartPeriod;

                _editedAim.DateEndPeriod = deEndPeriod.DateTime;
                _editedAim.IsEndDateApproved = chkDateEndPeriodApproved.Checked;
                _editedAim.DateApproveEndPeriod =
                    chkDateEndPeriodApproved.Checked != _oldEditedAim.IsEndDateApproved ? now : _editedAim.DateApproveEndPeriod;

                _editedAim.CreatedUserId = AuthenticationManager.Instance.CurrentUserId;
                _editedAim.ApprovedUserId = AuthenticationManager.Instance.CurrentUserId;

                _editedAim.IsFocused = chkIsFocused.Checked; 

                _editedAim.DeviationPercent = 0f;
                _editedAim.KPITypeId = 1;
                _editedAim.ProductCombineDLM = now;
                _editedAim.ProductCombineName = cbSKU.Text;

                return _editedAim;
            }
        }

        #endregion Public properties

        #region Public methods

        /// <summary>
        /// initialize form controls by data
        /// </summary>
        /// <param name="aim">Focused SKU</param>
        public void Init(CascadedAimEntity aim)
        {
            _oldEditedAim = aim.Clone() as CascadedAimEntity;
            _editedAim = aim.Clone() as CascadedAimEntity;

            cbSKU.Text = _editedAim.ProductCnId < 0 ? Resource.ComboBoxItemAnyValueText : _editedAim.ProductCombineName;
            int itemIndex = GetIndex<ProductCombineEntity>(new List<ProductCombineEntity>(_productsList), _editedAim.ProductCnId);
            cbSKU.SelectedIndex = itemIndex == -1 ? 0 : itemIndex; 

            if (_editedAim.DateStartPeriod == DateTime.MinValue)
                deStartPeriod.Text = "";
            else
                deStartPeriod.DateTime = _editedAim.DateStartPeriod;

            chkDateStartPeriodApproved.Checked = _editedAim.IsStartDateApproved;
            if (deStartPeriod.DateTime < DateTime.Now.Date)
                chkDateStartPeriodApproved.Enabled = false;

            if (_editedAim.DateEndPeriod == DateTime.MinValue)
                deEndPeriod.Text = "";
            else
                deEndPeriod.DateTime = _editedAim.DateEndPeriod;

            chkDateEndPeriodApproved.Checked = _editedAim.IsEndDateApproved;
            chkIsFocused.Checked = aim.IsFocused;

            teRegionName.Text = DataMediator.GetRegionName(_editedAim.RegionId);

            _dsmList = DataMediator.GetDSMList(_oldEditedAim.RegionId);
            DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);
            itemIndex = GetIndex<DSMEntity>(new List<DSMEntity>(_dsmList), _oldEditedAim.DSMId);
            cbDSM.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;             
            cbDSM_SelectedIndexChanged(null, null);

            if (_supervisorList != null)
            {
                itemIndex = GetIndex<SupervisorEntity>(new List<SupervisorEntity>(_supervisorList), _editedAim.SupervisorId);
                cbSupervisor.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            }
            else
            {
                DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);
                cbSupervisor.SelectedIndex = 0;
            }

            itemIndex = GetIndex<GlobalLookupEntity>(new List<GlobalLookupEntity>(_proximityList), _editedAim.ProximityId);
            cbProximity.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;

            itemIndex = GetIndex<SettlementEntity>(new List<SettlementEntity>(_settlementList), _editedAim.SettlementId);
            cbSettlement.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;  

            itemIndex = GetIndex<OutletGroupEntity>(new List<OutletGroupEntity>(_outletGroupList), _editedAim.OlGroupId < 0 ? 0 : _editedAim.OlGroupId);
            cbOutletGroup.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            cbOutletGroup_SelectedIndexChanged(null, null);

            itemIndex = GetIndex<OutletTypeEntity>(new List<OutletTypeEntity>(_outletTypeList), _editedAim.OlTypeId < 0 ? 0 : _editedAim.OlTypeId);
            cbOutletType.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            cbOutletType_SelectedIndexChanged(null, null);

            itemIndex = GetIndex<OutletSubTypeEntity>(new List<OutletSubTypeEntity>(_outletSubTypeList), _editedAim.OlSubTypeId < 0 ? 0 : _editedAim.OlSubTypeId);
            cbOutletSubtype.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;

            chkDatePeriodApproved_CheckedChanged(null, null);
        }

        /// <summary>
        /// Localize form Caption, Buttons
        /// </summary>
        /// <param name="formCaption">Form Caption</param>
        /// <param name="btnOkText">Button Ok text</param>
        /// <param name="btnCancelText">ButtonCancel text</param>
        public void Localize(string formCaption, string btnOkText, string btnCancelText)
        {
            this.Text = formCaption;
            btnOk.Text = btnOkText;
            btnCancel.Text = btnCancelText;
        }

        #endregion Public methods

        #region Private methods

        private void SetupControls()
        {
            DataConvertorHelper.PopulateList<ProductCombineEntity>(cbSKU.Properties.Items, _productsList);
            DataConvertorHelper.PopulateList<SettlementEntity>(cbSettlement.Properties.Items, _settlementList);
            DataConvertorHelper.PopulateList<OutletGroupEntity>(cbOutletGroup.Properties.Items, _outletGroupList);
            DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);
            DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);
            DataConvertorHelper.PopulateList<GlobalLookupEntity>(cbProximity.Properties.Items, _proximityList);
            DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);
            DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);

            cbSKU.Tag = typeof(ProductCombineEntity);
            cbSettlement.Tag = typeof(SettlementEntity);
            cbOutletGroup.Tag = typeof(OutletGroupEntity);
            cbOutletType.Tag = typeof(OutletTypeEntity);
            cbOutletSubtype.Tag = typeof(OutletSubTypeEntity);
            cbProximity.Tag = typeof(GlobalLookupEntity);
            cbSupervisor.Tag = typeof(SupervisorEntity);
            cbDSM.Tag = typeof(DSMEntity);
        }

        private void SetupUI()
        {
            chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = false;
            switch (AuthenticationManager.Instance.CurrentUserLevel)
            {
                case 5:
                    chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = true;
                    break;
            }
        }

        private void LoadData()
        {
            _settlementList = new SettlementDataProvider().GetList("SettlementIdColumn > 0");

            _outletGroupList = new OutletGroupsDataProvider().GetList("OLGroupIdColumn > 0");

            _outletTypeList = new OutletTypesDataProvider().GetList(OutletTypeBaseFilter);

            _outletSubTypeList = new OutletSubTypesDataProvider().GetList(OutletSubTypeBaseFilter);

            _kpiList = new KPIDataProvider().GetList();

            _proximityList = new GlobalLookupDataProvider().GetList(GlobalLookupProximityBaseFilter);

            _supervisorList = new SupervisorsDataProvider().GetList(SupervisorBaseFilter);

            _dsmList = new DSMDataProvider().GetList();

            _productsList = new ProductCombineDataProvider().GetList();
        }

        private void InitEvents()
        {
            chkDateStartPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);
            chkDateEndPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);

            cbOutletGroup.SelectedIndexChanged += new EventHandler(cbOutletGroup_SelectedIndexChanged);
            cbOutletType.SelectedIndexChanged += new EventHandler(cbOutletType_SelectedIndexChanged);
            cbDSM.SelectedIndexChanged += new EventHandler(cbDSM_SelectedIndexChanged);

            cbDSM.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbSupervisor.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbSettlement.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbProximity.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbSKU.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbOutletType.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbOutletSubtype.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbOutletGroup.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);

            btnOk.Click += new EventHandler(btnOk_Click);
        }

        private string GetValue<T>(List<T> values, int id)
        {
            if (id < 0)
            {
                return Resource.ComboBoxItemAnyValueText;
            }
            else
            {
                T searchResult = values.Find(delegate(T item) { return (item as IBaseEntity).Id == id; });

                return searchResult != null ? (searchResult as IBaseEntity).Name : string.Empty;
            }
        }

        private int GetIndex<T>(List<T> values, int id)
        {
            int result = -1;
            int index = 0;
            foreach (T item in values)
            {
                if ((item as IBaseEntity).Id == id)
                {
                    result = index + 1;
                    break;
                }
                index++;
            }

            return result;
        }

        private void UpdateEditableControls()
        {
            deStartPeriod.Properties.ReadOnly = chkDateStartPeriodApproved.Checked;
            deEndPeriod.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;

            chkDateStartPeriodApproved.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;
            chkDateEndPeriodApproved.Properties.ReadOnly = !chkDateStartPeriodApproved.Checked;

            bool readOnly = chkDateStartPeriodApproved.Checked || chkDateEndPeriodApproved.Checked;

            chkIsFocused.Properties.ReadOnly = readOnly;

            cbSKU.Properties.ReadOnly = readOnly;

            cbDSM.Properties.ReadOnly = readOnly;
            cbSupervisor.Properties.ReadOnly = readOnly;

            cbSettlement.Properties.ReadOnly = readOnly;
            cbProximity.Properties.ReadOnly = readOnly;

            cbOutletGroup.Properties.ReadOnly = readOnly;
            cbOutletType.Properties.ReadOnly = readOnly;
            cbOutletSubtype.Properties.ReadOnly = readOnly;
        }

        private bool HasDateBeenSet(DateTime date)
        {
            return date.Year > 1900;
        }

        private bool IsValidated()
        {
            bool isValidData = false;

            DateTime now = DateTime.Now;

            if (chkDateStartPeriodApproved.Enabled)
            {
                if (!HasDateBeenSet(deStartPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
                {
                    deStartPeriod.Focus();
                    MessageBox.Show(Resource.PeriodStartDateIsNotSet);
                    return isValidData;
                }

                if (DateHelper.IsYearMonthDayLess(deStartPeriod.DateTime, now))
                {
                    deStartPeriod.Focus();
                    MessageBox.Show(Resource.PeriodStartDateLessThenCurrentDate);
                    return isValidData;
                }
            }

            if (!HasDateBeenSet(deEndPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateIsNotSet);
                return isValidData;
            }

            if (DateHelper.IsYearMonthDayLess(deEndPeriod.DateTime, now))
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateLessThenCurrentDate);
                return isValidData;
            }

            if (!DateHelper.ValidatePeriod(deStartPeriod.DateTime, deEndPeriod.DateTime))
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.IncorrectFromToDates);
                return isValidData;
            }

            if (!_oldEditedAim.IsStartDateApproved && chkDateStartPeriodApproved.Checked && deStartPeriod.DateTime != DateTime.MinValue && deStartPeriod.DateTime < now)
            {
                chkDateStartPeriodApproved.Checked = false;
                deStartPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodStartDateLessThanNow);
                return isValidData;
            }

            if (!_oldEditedAim.IsEndDateApproved && chkDateEndPeriodApproved.Checked && deEndPeriod.DateTime != DateTime.MinValue && deEndPeriod.DateTime < now)
            {
                chkDateEndPeriodApproved.Checked = false;
                deEndPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodEndDateLessThanNow);
                return isValidData;
            }

            List<string> errors = new List<string>();
            CascadedAimEntity aim = EditedFocusedSKU;

            AimsDataSet aims = new CascadedAimsDataProvider().GetCrossedAims(aim);
            DataView view = aims.Aim.DefaultView;
            view.RowFilter = string.Format
                ("{0} = {1} AND {2} <> {3}",
                    MappedNames.IsFocused,
                    true.ToString(),
                    MappedNames.AimId,
                    aim.AimId
                );

            if (view.Count > 0)
            {
                errors.Add(
                        string.Format(
                        "�������� ��� <{0}> � �������� �������� � <{1}> �� <{2}> ��������� �",
                        aim.ProductCombineName,
                        DateHelper.PeriodBorderToString(aim.DateStartPeriod),
                        DateHelper.PeriodBorderToString(aim.DateEndPeriod)
                        )
                    );
                foreach (DataRowView rowView in view)
                {
                    errors.Add(
                        string.Format(
                            "   ��� {0} � �������� � <{1}> <{2}> ",
                            rowView[MappedNames.ProductCombineName],
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateStartPeriod])),
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateEndPeriod]))
                        )
                        );
                }
            }

            if (errors.Count > 0)
            {
                deStartPeriod.Focus();
                DetailedMessageForm.Show(errors.ToArray());
                return isValidData;
            } 
            
            return true;
        }

        #endregion Private methods

        #region Event handlers

        private void chkDatePeriodApproved_CheckedChanged(object sender, EventArgs e)
        {
            UpdateEditableControls();
        }

        private void cbOutletGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletGroup.SelectedItem != null)
            {
                OutletGroupEntity currentOutletGroup = cbOutletGroup.SelectedItem as OutletGroupEntity;
                if (currentOutletGroup != null)
                {
                    cbOutletType.Properties.Items.BeginUpdate();

                    _outletTypeList.Clear();
                    string filter = string.Format("{0} AND {1} {2}", OutletTypeBaseFilter, OutletTypeGroupIdFilter, currentOutletGroup.Id.ToString());
                    _outletTypeList = new OutletTypesDataProvider().GetList(filter);
                    DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);

                    cbOutletType.Properties.Items.EndUpdate();

                    cbOutletType.SelectedIndex = -1;
                }
            }
        }

        private void cbOutletType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletType.SelectedItem != null)
            {
                OutletTypeEntity currentOutletType = cbOutletType.SelectedItem as OutletTypeEntity;
                if (currentOutletType != null)
                {
                    cbOutletSubtype.Properties.Items.BeginUpdate();

                    _outletSubTypeList.Clear();
                    string filter = string.Format("{0} AND {1} {2}", OutletSubTypeBaseFilter, OutletSubTypeTypeIdFilter, currentOutletType.Id.ToString());
                    _outletSubTypeList = new OutletSubTypesDataProvider().GetList(filter);
                    DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);

                    cbOutletSubtype.Properties.Items.EndUpdate();

                    cbOutletSubtype.SelectedIndex = -1;
                }
            }
        }

        private void cbDSM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDSM.SelectedItem != null)
            {
                cbSupervisor.Properties.Items.BeginUpdate();

                if (_supervisorList != null)
                    _supervisorList.Clear();

                string filter = string.Empty;

                if (cbDSM.SelectedItem is DSMEntity)
                {
                    DSMEntity currentDSM = (DSMEntity)cbDSM.SelectedItem;
                    filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorDSMId, currentDSM.Id);
                }
                else
                {
                    if (cbDSM.SelectedText.Contains(Resource.ComboBoxItemAnyValueText))
                    {
                        if (_oldEditedAim.RegionId > 0)
                        {
                            filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorRegionId, _oldEditedAim.RegionId);
                        }
                        else
                        {
                            filter = SupervisorBaseFilter;
                        }
                    }
                    else
                    {
                        filter = SupervisorBaseFilter;
                    }
                }

                _supervisorList = new SupervisorsDataProvider().GetList(filter);
                DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);

                cbSupervisor.Properties.Items.EndUpdate();

                cbSupervisor.SelectedIndex = -1;
            }
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxEdit combobox = sender as ComboBoxEdit;
            if (combobox.SelectedItem != null)
            {
                if (combobox.Tag == typeof(DSMEntity))
                {
                    _editedAim.DSMId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<DSMEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(SupervisorEntity))
                {
                    _editedAim.SupervisorId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SupervisorEntity>
                      (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(SettlementEntity))
                {
                    _editedAim.SettlementId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SettlementEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(GlobalLookupEntity))
                {
                    _editedAim.ProximityId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<GlobalLookupEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(ProductCombineEntity))
                {
                    _editedAim.ProductCnId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<ProductCombineEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(OutletTypeEntity))
                {
                    _editedAim.OlTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletTypeEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(OutletSubTypeEntity))
                {
                    _editedAim.OlSubTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletSubTypeEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(OutletGroupEntity))
                {
                    _editedAim.OlGroupId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletGroupEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }

            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (IsValidated())
            {
                DialogResult = DialogResult.OK;
            }
        }

        #endregion Event handlers

        private void btnOk_Click_1(object sender, EventArgs e)
        {

        }
    }
}