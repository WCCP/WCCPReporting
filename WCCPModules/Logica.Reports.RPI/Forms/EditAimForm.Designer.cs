namespace Logica.Reports.RPI.Forms
{
    partial class EditAimForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCenter = new DevExpress.XtraEditors.PanelControl();
            this.teRegionName = new DevExpress.XtraEditors.TextEdit();
            this.lblRegionName = new DevExpress.XtraEditors.LabelControl();
            this.teKPIValue = new DevExpress.XtraEditors.TextEdit();
            this.lblKPIValue = new DevExpress.XtraEditors.LabelControl();
            this.cbSKU = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblSKU = new DevExpress.XtraEditors.LabelControl();
            this.cbOutletSubtype = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbOutletType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbOutletGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbSettlement = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbProximity = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbKPI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbSupervisor = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbDSM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblOutletType = new DevExpress.XtraEditors.LabelControl();
            this.lblOutletSubType = new DevExpress.XtraEditors.LabelControl();
            this.lblOutletGroup = new DevExpress.XtraEditors.LabelControl();
            this.lblSettlement = new DevExpress.XtraEditors.LabelControl();
            this.lblProximity = new DevExpress.XtraEditors.LabelControl();
            this.lblKPI = new DevExpress.XtraEditors.LabelControl();
            this.lblSupervisor = new DevExpress.XtraEditors.LabelControl();
            this.lblDSM = new DevExpress.XtraEditors.LabelControl();
            this.chkDateEndPeriodApproved = new DevExpress.XtraEditors.CheckEdit();
            this.chkDateStartPeriodApproved = new DevExpress.XtraEditors.CheckEdit();
            this.lblDateEndPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblDateStartPeriod = new DevExpress.XtraEditors.LabelControl();
            this.deEndPeriod = new DevExpress.XtraEditors.DateEdit();
            this.deStartPeriod = new DevExpress.XtraEditors.DateEdit();
            this.pnlBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCenter)).BeginInit();
            this.pnlCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teRegionName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teKPIValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSKU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletSubtype.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSettlement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProximity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbKPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateEndPeriodApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateStartPeriodApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCenter
            // 
            this.pnlCenter.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlCenter.Appearance.Options.UseBackColor = true;
            this.pnlCenter.Controls.Add(this.teRegionName);
            this.pnlCenter.Controls.Add(this.lblRegionName);
            this.pnlCenter.Controls.Add(this.teKPIValue);
            this.pnlCenter.Controls.Add(this.lblKPIValue);
            this.pnlCenter.Controls.Add(this.cbSKU);
            this.pnlCenter.Controls.Add(this.lblSKU);
            this.pnlCenter.Controls.Add(this.cbOutletSubtype);
            this.pnlCenter.Controls.Add(this.cbOutletType);
            this.pnlCenter.Controls.Add(this.cbOutletGroup);
            this.pnlCenter.Controls.Add(this.cbSettlement);
            this.pnlCenter.Controls.Add(this.cbProximity);
            this.pnlCenter.Controls.Add(this.cbKPI);
            this.pnlCenter.Controls.Add(this.cbSupervisor);
            this.pnlCenter.Controls.Add(this.cbDSM);
            this.pnlCenter.Controls.Add(this.lblOutletType);
            this.pnlCenter.Controls.Add(this.lblOutletSubType);
            this.pnlCenter.Controls.Add(this.lblOutletGroup);
            this.pnlCenter.Controls.Add(this.lblSettlement);
            this.pnlCenter.Controls.Add(this.lblProximity);
            this.pnlCenter.Controls.Add(this.lblKPI);
            this.pnlCenter.Controls.Add(this.lblSupervisor);
            this.pnlCenter.Controls.Add(this.lblDSM);
            this.pnlCenter.Controls.Add(this.chkDateEndPeriodApproved);
            this.pnlCenter.Controls.Add(this.chkDateStartPeriodApproved);
            this.pnlCenter.Controls.Add(this.lblDateEndPeriod);
            this.pnlCenter.Controls.Add(this.lblDateStartPeriod);
            this.pnlCenter.Controls.Add(this.deEndPeriod);
            this.pnlCenter.Controls.Add(this.deStartPeriod);
            this.pnlCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCenter.Location = new System.Drawing.Point(0, 0);
            this.pnlCenter.Name = "pnlCenter";
            this.pnlCenter.Size = new System.Drawing.Size(379, 416);
            this.pnlCenter.TabIndex = 0;
            // 
            // teRegionName
            // 
            this.teRegionName.Location = new System.Drawing.Point(129, 92);
            this.teRegionName.Name = "teRegionName";
            this.teRegionName.Properties.ReadOnly = true;
            this.teRegionName.Size = new System.Drawing.Size(228, 22);
            this.teRegionName.TabIndex = 72;
            // 
            // lblRegionName
            // 
            this.lblRegionName.Location = new System.Drawing.Point(10, 97);
            this.lblRegionName.Name = "lblRegionName";
            this.lblRegionName.Size = new System.Drawing.Size(35, 13);
            this.lblRegionName.TabIndex = 71;
            this.lblRegionName.Text = "������";
            // 
            // teKPIValue
            // 
            this.teKPIValue.Location = new System.Drawing.Point(130, 199);
            this.teKPIValue.Name = "teKPIValue";
            this.teKPIValue.Size = new System.Drawing.Size(117, 22);
            this.teKPIValue.TabIndex = 70;
            // 
            // lblKPIValue
            // 
            this.lblKPIValue.Location = new System.Drawing.Point(12, 201);
            this.lblKPIValue.Name = "lblKPIValue";
            this.lblKPIValue.Size = new System.Drawing.Size(67, 13);
            this.lblKPIValue.TabIndex = 69;
            this.lblKPIValue.Text = "�������� KPI";
            // 
            // cbSKU
            // 
            this.cbSKU.Location = new System.Drawing.Point(129, 9);
            this.cbSKU.Name = "cbSKU";
            this.cbSKU.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSKU.Size = new System.Drawing.Size(228, 22);
            this.cbSKU.TabIndex = 68;
            // 
            // lblSKU
            // 
            this.lblSKU.Location = new System.Drawing.Point(12, 15);
            this.lblSKU.Name = "lblSKU";
            this.lblSKU.Size = new System.Drawing.Size(24, 13);
            this.lblSKU.TabIndex = 67;
            this.lblSKU.Text = "���";
            // 
            // cbOutletSubtype
            // 
            this.cbOutletSubtype.Location = new System.Drawing.Point(129, 341);
            this.cbOutletSubtype.Name = "cbOutletSubtype";
            this.cbOutletSubtype.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletSubtype.Size = new System.Drawing.Size(228, 22);
            this.cbOutletSubtype.TabIndex = 66;
            // 
            // cbOutletType
            // 
            this.cbOutletType.Location = new System.Drawing.Point(129, 314);
            this.cbOutletType.Name = "cbOutletType";
            this.cbOutletType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletType.Size = new System.Drawing.Size(228, 22);
            this.cbOutletType.TabIndex = 65;
            // 
            // cbOutletGroup
            // 
            this.cbOutletGroup.Location = new System.Drawing.Point(130, 286);
            this.cbOutletGroup.Name = "cbOutletGroup";
            this.cbOutletGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletGroup.Size = new System.Drawing.Size(227, 22);
            this.cbOutletGroup.TabIndex = 64;
            // 
            // cbSettlement
            // 
            this.cbSettlement.Location = new System.Drawing.Point(129, 257);
            this.cbSettlement.Name = "cbSettlement";
            this.cbSettlement.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSettlement.Size = new System.Drawing.Size(228, 22);
            this.cbSettlement.TabIndex = 63;
            // 
            // cbProximity
            // 
            this.cbProximity.Location = new System.Drawing.Point(129, 227);
            this.cbProximity.Name = "cbProximity";
            this.cbProximity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbProximity.Size = new System.Drawing.Size(228, 22);
            this.cbProximity.TabIndex = 62;
            // 
            // cbKPI
            // 
            this.cbKPI.Location = new System.Drawing.Point(129, 172);
            this.cbKPI.Name = "cbKPI";
            this.cbKPI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbKPI.Size = new System.Drawing.Size(228, 22);
            this.cbKPI.TabIndex = 61;
            // 
            // cbSupervisor
            // 
            this.cbSupervisor.Location = new System.Drawing.Point(129, 144);
            this.cbSupervisor.Name = "cbSupervisor";
            this.cbSupervisor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSupervisor.Size = new System.Drawing.Size(228, 22);
            this.cbSupervisor.TabIndex = 60;
            // 
            // cbDSM
            // 
            this.cbDSM.Location = new System.Drawing.Point(129, 118);
            this.cbDSM.Name = "cbDSM";
            this.cbDSM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDSM.Size = new System.Drawing.Size(228, 22);
            this.cbDSM.TabIndex = 59;
            // 
            // lblOutletType
            // 
            this.lblOutletType.Location = new System.Drawing.Point(12, 320);
            this.lblOutletType.Name = "lblOutletType";
            this.lblOutletType.Size = new System.Drawing.Size(33, 13);
            this.lblOutletType.TabIndex = 58;
            this.lblOutletType.Text = "��� ��";
            // 
            // lblOutletSubType
            // 
            this.lblOutletSubType.Location = new System.Drawing.Point(12, 349);
            this.lblOutletSubType.Name = "lblOutletSubType";
            this.lblOutletSubType.Size = new System.Drawing.Size(53, 13);
            this.lblOutletSubType.TabIndex = 57;
            this.lblOutletSubType.Text = "������ ��";
            // 
            // lblOutletGroup
            // 
            this.lblOutletGroup.Location = new System.Drawing.Point(12, 292);
            this.lblOutletGroup.Name = "lblOutletGroup";
            this.lblOutletGroup.Size = new System.Drawing.Size(51, 13);
            this.lblOutletGroup.TabIndex = 56;
            this.lblOutletGroup.Text = "������ ��";
            // 
            // lblSettlement
            // 
            this.lblSettlement.Location = new System.Drawing.Point(12, 260);
            this.lblSettlement.Name = "lblSettlement";
            this.lblSettlement.Size = new System.Drawing.Size(104, 13);
            this.lblSettlement.TabIndex = 55;
            this.lblSettlement.Text = "���������/��������";
            // 
            // lblProximity
            // 
            this.lblProximity.Location = new System.Drawing.Point(12, 229);
            this.lblProximity.Name = "lblProximity";
            this.lblProximity.Size = new System.Drawing.Size(87, 13);
            this.lblProximity.TabIndex = 54;
            this.lblProximity.Text = "������ ��������";
            // 
            // lblKPI
            // 
            this.lblKPI.Location = new System.Drawing.Point(12, 175);
            this.lblKPI.Name = "lblKPI";
            this.lblKPI.Size = new System.Drawing.Size(16, 13);
            this.lblKPI.TabIndex = 53;
            this.lblKPI.Text = "KPI";
            // 
            // lblSupervisor
            // 
            this.lblSupervisor.Location = new System.Drawing.Point(12, 147);
            this.lblSupervisor.Name = "lblSupervisor";
            this.lblSupervisor.Size = new System.Drawing.Size(14, 13);
            this.lblSupervisor.TabIndex = 52;
            this.lblSupervisor.Text = "M2";
            // 
            // lblDSM
            // 
            this.lblDSM.Location = new System.Drawing.Point(12, 122);
            this.lblDSM.Name = "lblDSM";
            this.lblDSM.Size = new System.Drawing.Size(14, 13);
            this.lblDSM.TabIndex = 51;
            this.lblDSM.Text = "�3";
            // 
            // chkDateEndPeriodApproved
            // 
            this.chkDateEndPeriodApproved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDateEndPeriodApproved.Location = new System.Drawing.Point(264, 66);
            this.chkDateEndPeriodApproved.Name = "chkDateEndPeriodApproved";
            this.chkDateEndPeriodApproved.Properties.Caption = "����������";
            this.chkDateEndPeriodApproved.Size = new System.Drawing.Size(99, 18);
            this.chkDateEndPeriodApproved.TabIndex = 50;
            // 
            // chkDateStartPeriodApproved
            // 
            this.chkDateStartPeriodApproved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDateStartPeriodApproved.Location = new System.Drawing.Point(264, 37);
            this.chkDateStartPeriodApproved.Name = "chkDateStartPeriodApproved";
            this.chkDateStartPeriodApproved.Properties.Caption = "����������";
            this.chkDateStartPeriodApproved.Size = new System.Drawing.Size(101, 18);
            this.chkDateStartPeriodApproved.TabIndex = 49;
            // 
            // lblDateEndPeriod
            // 
            this.lblDateEndPeriod.Location = new System.Drawing.Point(12, 72);
            this.lblDateEndPeriod.Name = "lblDateEndPeriod";
            this.lblDateEndPeriod.Size = new System.Drawing.Size(70, 13);
            this.lblDateEndPeriod.TabIndex = 48;
            this.lblDateEndPeriod.Text = "��������� ��";
            // 
            // lblDateStartPeriod
            // 
            this.lblDateStartPeriod.Location = new System.Drawing.Point(12, 38);
            this.lblDateStartPeriod.Name = "lblDateStartPeriod";
            this.lblDateStartPeriod.Size = new System.Drawing.Size(63, 13);
            this.lblDateStartPeriod.TabIndex = 47;
            this.lblDateStartPeriod.Text = "��������� �";
            // 
            // deEndPeriod
            // 
            this.deEndPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deEndPeriod.EditValue = null;
            this.deEndPeriod.Location = new System.Drawing.Point(130, 65);
            this.deEndPeriod.Name = "deEndPeriod";
            this.deEndPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndPeriod.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndPeriod.Size = new System.Drawing.Size(117, 22);
            this.deEndPeriod.TabIndex = 46;
            // 
            // deStartPeriod
            // 
            this.deStartPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deStartPeriod.EditValue = null;
            this.deStartPeriod.Location = new System.Drawing.Point(130, 36);
            this.deStartPeriod.Name = "deStartPeriod";
            this.deStartPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartPeriod.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStartPeriod.Size = new System.Drawing.Size(117, 22);
            this.deStartPeriod.TabIndex = 45;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.Appearance.Options.UseBackColor = true;
            this.pnlBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlBottom.Controls.Add(this.btnOk);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 366);
            this.pnlBottom.MinimumSize = new System.Drawing.Size(0, 50);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(379, 50);
            this.pnlBottom.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(22, 14);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(82, 25);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "�������";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(266, 14);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(91, 25);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "������";
            // 
            // EditAimForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 416);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlCenter);
            this.Name = "EditAimForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������������� ����";
            ((System.ComponentModel.ISupportInitialize)(this.pnlCenter)).EndInit();
            this.pnlCenter.ResumeLayout(false);
            this.pnlCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teRegionName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teKPIValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSKU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletSubtype.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSettlement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProximity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbKPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateEndPeriodApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateStartPeriodApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlCenter;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletSubtype;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletType;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletGroup;
        private DevExpress.XtraEditors.ComboBoxEdit cbSettlement;
        private DevExpress.XtraEditors.ComboBoxEdit cbProximity;
        private DevExpress.XtraEditors.ComboBoxEdit cbKPI;
        private DevExpress.XtraEditors.ComboBoxEdit cbSupervisor;
        private DevExpress.XtraEditors.ComboBoxEdit cbDSM;
        private DevExpress.XtraEditors.LabelControl lblOutletType;
        private DevExpress.XtraEditors.LabelControl lblOutletSubType;
        private DevExpress.XtraEditors.LabelControl lblOutletGroup;
        private DevExpress.XtraEditors.LabelControl lblSettlement;
        private DevExpress.XtraEditors.LabelControl lblProximity;
        private DevExpress.XtraEditors.LabelControl lblKPI;
        private DevExpress.XtraEditors.LabelControl lblSupervisor;
        private DevExpress.XtraEditors.LabelControl lblDSM;
        private DevExpress.XtraEditors.CheckEdit chkDateEndPeriodApproved;
        private DevExpress.XtraEditors.CheckEdit chkDateStartPeriodApproved;
        private DevExpress.XtraEditors.LabelControl lblDateEndPeriod;
        private DevExpress.XtraEditors.LabelControl lblDateStartPeriod;
        private DevExpress.XtraEditors.DateEdit deEndPeriod;
        private DevExpress.XtraEditors.DateEdit deStartPeriod;
        private DevExpress.XtraEditors.PanelControl pnlBottom;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.ComboBoxEdit cbSKU;
        private DevExpress.XtraEditors.LabelControl lblSKU;
        private DevExpress.XtraEditors.LabelControl lblKPIValue;
        private DevExpress.XtraEditors.TextEdit teKPIValue;
        private DevExpress.XtraEditors.TextEdit teRegionName;
        private DevExpress.XtraEditors.LabelControl lblRegionName;
    }

}