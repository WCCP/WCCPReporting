namespace Logica.Reports.RPI.Forms
{
    partial class NewFocusedSKUForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlContainer = new DevExpress.XtraEditors.PanelControl();
            this.teRegionName = new DevExpress.XtraEditors.TextEdit();
            this.lblRegionName = new DevExpress.XtraEditors.LabelControl();
            this.cbSKU = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblSKU = new DevExpress.XtraEditors.LabelControl();
            this.lblIsFocused = new DevExpress.XtraEditors.LabelControl();
            this.chkIsFocused = new DevExpress.XtraEditors.CheckEdit();
            this.cbOutletSubtype = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbOutletType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbOutletGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbSettlement = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbProximity = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbSupervisor = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbDSM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblOutletType = new DevExpress.XtraEditors.LabelControl();
            this.lblOutletSubType = new DevExpress.XtraEditors.LabelControl();
            this.lblOutletGroup = new DevExpress.XtraEditors.LabelControl();
            this.lblSettlement = new DevExpress.XtraEditors.LabelControl();
            this.lblProximity = new DevExpress.XtraEditors.LabelControl();
            this.lblSupervisor = new DevExpress.XtraEditors.LabelControl();
            this.lblDSM = new DevExpress.XtraEditors.LabelControl();
            this.chkDateEndPeriodApproved = new DevExpress.XtraEditors.CheckEdit();
            this.chkDateStartPeriodApproved = new DevExpress.XtraEditors.CheckEdit();
            this.lblDateEndPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblDateStartPeriod = new DevExpress.XtraEditors.LabelControl();
            this.deEndPeriod = new DevExpress.XtraEditors.DateEdit();
            this.deStartPeriod = new DevExpress.XtraEditors.DateEdit();
            this.pnlBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).BeginInit();
            this.pnlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teRegionName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSKU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsFocused.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletSubtype.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSettlement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProximity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateEndPeriodApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateStartPeriodApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContainer
            // 
            this.pnlContainer.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlContainer.Appearance.Options.UseBackColor = true;
            this.pnlContainer.Controls.Add(this.teRegionName);
            this.pnlContainer.Controls.Add(this.lblRegionName);
            this.pnlContainer.Controls.Add(this.cbSKU);
            this.pnlContainer.Controls.Add(this.lblSKU);
            this.pnlContainer.Controls.Add(this.lblIsFocused);
            this.pnlContainer.Controls.Add(this.chkIsFocused);
            this.pnlContainer.Controls.Add(this.cbOutletSubtype);
            this.pnlContainer.Controls.Add(this.cbOutletType);
            this.pnlContainer.Controls.Add(this.cbOutletGroup);
            this.pnlContainer.Controls.Add(this.cbSettlement);
            this.pnlContainer.Controls.Add(this.cbProximity);
            this.pnlContainer.Controls.Add(this.cbSupervisor);
            this.pnlContainer.Controls.Add(this.cbDSM);
            this.pnlContainer.Controls.Add(this.lblOutletType);
            this.pnlContainer.Controls.Add(this.lblOutletSubType);
            this.pnlContainer.Controls.Add(this.lblOutletGroup);
            this.pnlContainer.Controls.Add(this.lblSettlement);
            this.pnlContainer.Controls.Add(this.lblProximity);
            this.pnlContainer.Controls.Add(this.lblSupervisor);
            this.pnlContainer.Controls.Add(this.lblDSM);
            this.pnlContainer.Controls.Add(this.chkDateEndPeriodApproved);
            this.pnlContainer.Controls.Add(this.chkDateStartPeriodApproved);
            this.pnlContainer.Controls.Add(this.lblDateEndPeriod);
            this.pnlContainer.Controls.Add(this.lblDateStartPeriod);
            this.pnlContainer.Controls.Add(this.deEndPeriod);
            this.pnlContainer.Controls.Add(this.deStartPeriod);
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(410, 405);
            this.pnlContainer.TabIndex = 0;
            // 
            // teRegionName
            // 
            this.teRegionName.Location = new System.Drawing.Point(136, 101);
            this.teRegionName.Name = "teRegionName";
            this.teRegionName.Properties.ReadOnly = true;
            this.teRegionName.Size = new System.Drawing.Size(244, 22);
            this.teRegionName.TabIndex = 72;
            // 
            // lblRegionName
            // 
            this.lblRegionName.Location = new System.Drawing.Point(17, 107);
            this.lblRegionName.Name = "lblRegionName";
            this.lblRegionName.Size = new System.Drawing.Size(35, 13);
            this.lblRegionName.TabIndex = 71;
            this.lblRegionName.Text = "������";
            // 
            // cbSKU
            // 
            this.cbSKU.Location = new System.Drawing.Point(136, 12);
            this.cbSKU.Name = "cbSKU";
            this.cbSKU.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSKU.Size = new System.Drawing.Size(244, 22);
            this.cbSKU.TabIndex = 70;
            // 
            // lblSKU
            // 
            this.lblSKU.Location = new System.Drawing.Point(19, 18);
            this.lblSKU.Name = "lblSKU";
            this.lblSKU.Size = new System.Drawing.Size(24, 13);
            this.lblSKU.TabIndex = 69;
            this.lblSKU.Text = "���";
            // 
            // lblIsFocused
            // 
            this.lblIsFocused.Location = new System.Drawing.Point(18, 189);
            this.lblIsFocused.Name = "lblIsFocused";
            this.lblIsFocused.Size = new System.Drawing.Size(49, 13);
            this.lblIsFocused.TabIndex = 44;
            this.lblIsFocused.Text = "��������";
            // 
            // chkIsFocused
            // 
            this.chkIsFocused.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkIsFocused.EditValue = true;
            this.chkIsFocused.Location = new System.Drawing.Point(134, 187);
            this.chkIsFocused.Name = "chkIsFocused";
            this.chkIsFocused.Properties.Caption = "";
            this.chkIsFocused.Size = new System.Drawing.Size(23, 18);
            this.chkIsFocused.TabIndex = 43;
            // 
            // cbOutletSubtype
            // 
            this.cbOutletSubtype.Location = new System.Drawing.Point(136, 327);
            this.cbOutletSubtype.Name = "cbOutletSubtype";
            this.cbOutletSubtype.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletSubtype.Size = new System.Drawing.Size(244, 22);
            this.cbOutletSubtype.TabIndex = 42;
            // 
            // cbOutletType
            // 
            this.cbOutletType.Location = new System.Drawing.Point(136, 298);
            this.cbOutletType.Name = "cbOutletType";
            this.cbOutletType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletType.Size = new System.Drawing.Size(244, 22);
            this.cbOutletType.TabIndex = 41;
            // 
            // cbOutletGroup
            // 
            this.cbOutletGroup.Location = new System.Drawing.Point(136, 270);
            this.cbOutletGroup.Name = "cbOutletGroup";
            this.cbOutletGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletGroup.Size = new System.Drawing.Size(244, 22);
            this.cbOutletGroup.TabIndex = 40;
            // 
            // cbSettlement
            // 
            this.cbSettlement.Location = new System.Drawing.Point(136, 242);
            this.cbSettlement.Name = "cbSettlement";
            this.cbSettlement.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSettlement.Size = new System.Drawing.Size(244, 22);
            this.cbSettlement.TabIndex = 39;
            // 
            // cbProximity
            // 
            this.cbProximity.Location = new System.Drawing.Point(136, 212);
            this.cbProximity.Name = "cbProximity";
            this.cbProximity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbProximity.Size = new System.Drawing.Size(244, 22);
            this.cbProximity.TabIndex = 38;
            // 
            // cbSupervisor
            // 
            this.cbSupervisor.Location = new System.Drawing.Point(136, 159);
            this.cbSupervisor.Name = "cbSupervisor";
            this.cbSupervisor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSupervisor.Size = new System.Drawing.Size(244, 22);
            this.cbSupervisor.TabIndex = 37;
            // 
            // cbDSM
            // 
            this.cbDSM.Location = new System.Drawing.Point(136, 130);
            this.cbDSM.Name = "cbDSM";
            this.cbDSM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDSM.Size = new System.Drawing.Size(244, 22);
            this.cbDSM.TabIndex = 36;
            // 
            // lblOutletType
            // 
            this.lblOutletType.Location = new System.Drawing.Point(18, 305);
            this.lblOutletType.Name = "lblOutletType";
            this.lblOutletType.Size = new System.Drawing.Size(33, 13);
            this.lblOutletType.TabIndex = 35;
            this.lblOutletType.Text = "��� ��";
            // 
            // lblOutletSubType
            // 
            this.lblOutletSubType.Location = new System.Drawing.Point(18, 334);
            this.lblOutletSubType.Name = "lblOutletSubType";
            this.lblOutletSubType.Size = new System.Drawing.Size(53, 13);
            this.lblOutletSubType.TabIndex = 34;
            this.lblOutletSubType.Text = "������ ��";
            // 
            // lblOutletGroup
            // 
            this.lblOutletGroup.Location = new System.Drawing.Point(18, 277);
            this.lblOutletGroup.Name = "lblOutletGroup";
            this.lblOutletGroup.Size = new System.Drawing.Size(51, 13);
            this.lblOutletGroup.TabIndex = 33;
            this.lblOutletGroup.Text = "������ ��";
            // 
            // lblSettlement
            // 
            this.lblSettlement.Location = new System.Drawing.Point(18, 245);
            this.lblSettlement.Name = "lblSettlement";
            this.lblSettlement.Size = new System.Drawing.Size(104, 13);
            this.lblSettlement.TabIndex = 32;
            this.lblSettlement.Text = "���������/��������";
            // 
            // lblProximity
            // 
            this.lblProximity.Location = new System.Drawing.Point(18, 214);
            this.lblProximity.Name = "lblProximity";
            this.lblProximity.Size = new System.Drawing.Size(87, 13);
            this.lblProximity.TabIndex = 31;
            this.lblProximity.Text = "������ ��������";
            // 
            // lblSupervisor
            // 
            this.lblSupervisor.Location = new System.Drawing.Point(18, 162);
            this.lblSupervisor.Name = "lblSupervisor";
            this.lblSupervisor.Size = new System.Drawing.Size(14, 13);
            this.lblSupervisor.TabIndex = 30;
            this.lblSupervisor.Text = "M2";
            // 
            // lblDSM
            // 
            this.lblDSM.Location = new System.Drawing.Point(18, 137);
            this.lblDSM.Name = "lblDSM";
            this.lblDSM.Size = new System.Drawing.Size(14, 13);
            this.lblDSM.TabIndex = 29;
            this.lblDSM.Text = "�3";
            // 
            // chkDateEndPeriodApproved
            // 
            this.chkDateEndPeriodApproved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDateEndPeriodApproved.Location = new System.Drawing.Point(296, 73);
            this.chkDateEndPeriodApproved.Name = "chkDateEndPeriodApproved";
            this.chkDateEndPeriodApproved.Properties.Caption = "����������";
            this.chkDateEndPeriodApproved.Size = new System.Drawing.Size(99, 18);
            this.chkDateEndPeriodApproved.TabIndex = 28;
            // 
            // chkDateStartPeriodApproved
            // 
            this.chkDateStartPeriodApproved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDateStartPeriodApproved.Location = new System.Drawing.Point(296, 42);
            this.chkDateStartPeriodApproved.Name = "chkDateStartPeriodApproved";
            this.chkDateStartPeriodApproved.Properties.Caption = "����������";
            this.chkDateStartPeriodApproved.Size = new System.Drawing.Size(101, 18);
            this.chkDateStartPeriodApproved.TabIndex = 27;
            // 
            // lblDateEndPeriod
            // 
            this.lblDateEndPeriod.Location = new System.Drawing.Point(18, 75);
            this.lblDateEndPeriod.Name = "lblDateEndPeriod";
            this.lblDateEndPeriod.Size = new System.Drawing.Size(70, 13);
            this.lblDateEndPeriod.TabIndex = 26;
            this.lblDateEndPeriod.Text = "��������� ��";
            // 
            // lblDateStartPeriod
            // 
            this.lblDateStartPeriod.Location = new System.Drawing.Point(18, 44);
            this.lblDateStartPeriod.Name = "lblDateStartPeriod";
            this.lblDateStartPeriod.Size = new System.Drawing.Size(63, 13);
            this.lblDateStartPeriod.TabIndex = 25;
            this.lblDateStartPeriod.Text = "��������� �";
            // 
            // deEndPeriod
            // 
            this.deEndPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deEndPeriod.EditValue = null;
            this.deEndPeriod.Location = new System.Drawing.Point(136, 72);
            this.deEndPeriod.Name = "deEndPeriod";
            this.deEndPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndPeriod.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndPeriod.Size = new System.Drawing.Size(147, 22);
            this.deEndPeriod.TabIndex = 24;
            // 
            // deStartPeriod
            // 
            this.deStartPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deStartPeriod.EditValue = null;
            this.deStartPeriod.Location = new System.Drawing.Point(136, 41);
            this.deStartPeriod.Name = "deStartPeriod";
            this.deStartPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartPeriod.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStartPeriod.Size = new System.Drawing.Size(147, 22);
            this.deStartPeriod.TabIndex = 23;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.Appearance.Options.UseBackColor = true;
            this.pnlBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBottom.Controls.Add(this.btnOk);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 359);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(410, 46);
            this.pnlBottom.TabIndex = 2;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(11, 12);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(91, 25);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "�������";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(306, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(91, 25);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "������";
            // 
            // NewFocusedSKUForm
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 405);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlContainer);
            this.Name = "NewFocusedSKUForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������� �������� ���";
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.pnlContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teRegionName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSKU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsFocused.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletSubtype.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSettlement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProximity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateEndPeriodApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateStartPeriodApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlContainer;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletSubtype;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletType;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletGroup;
        private DevExpress.XtraEditors.ComboBoxEdit cbSettlement;
        private DevExpress.XtraEditors.ComboBoxEdit cbProximity;
        private DevExpress.XtraEditors.ComboBoxEdit cbSupervisor;
        private DevExpress.XtraEditors.ComboBoxEdit cbDSM;
        private DevExpress.XtraEditors.LabelControl lblOutletType;
        private DevExpress.XtraEditors.LabelControl lblOutletSubType;
        private DevExpress.XtraEditors.LabelControl lblOutletGroup;
        private DevExpress.XtraEditors.LabelControl lblSettlement;
        private DevExpress.XtraEditors.LabelControl lblProximity;
        private DevExpress.XtraEditors.LabelControl lblSupervisor;
        private DevExpress.XtraEditors.LabelControl lblDSM;
        private DevExpress.XtraEditors.CheckEdit chkDateEndPeriodApproved;
        private DevExpress.XtraEditors.CheckEdit chkDateStartPeriodApproved;
        private DevExpress.XtraEditors.LabelControl lblDateEndPeriod;
        private DevExpress.XtraEditors.LabelControl lblDateStartPeriod;
        private DevExpress.XtraEditors.DateEdit deEndPeriod;
        private DevExpress.XtraEditors.DateEdit deStartPeriod;
        private DevExpress.XtraEditors.PanelControl pnlBottom;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.CheckEdit chkIsFocused;
        private DevExpress.XtraEditors.LabelControl lblIsFocused;
        private DevExpress.XtraEditors.ComboBoxEdit cbSKU;
        private DevExpress.XtraEditors.LabelControl lblSKU;
        private DevExpress.XtraEditors.TextEdit teRegionName;
        private DevExpress.XtraEditors.LabelControl lblRegionName;
    }

}