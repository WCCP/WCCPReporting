using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.Authentication;

namespace Logica.Reports.RPI.Forms
{
    /// <summary>
    /// Form which contains parameters for Cascaded Aims creation
    /// </summary>
    public partial class NewAimForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private members
        private IList<ProductCombineKPI> _productCombineList = new List<ProductCombineKPI>();
        private IList<SettlementEntity> _settlementList;
        private IList<OutletGroupEntity> _outletGroupList;
        private IList<OutletTypeEntity> _outletTypeList;
        private IList<OutletSubTypeEntity> _outletSubTypeList;
        private IList<KPIEntity> _kpiList;
        private IList<GlobalLookupEntity> _proximityList;
        private IList<SupervisorEntity> _supervisorList;
        private IList<DSMEntity> _dsmList;
        private IList<ProductCombineEntity> _productsList;

        private DateTime _dateStartPeriod;
        private DateTime _dateEndPeriod;
        private bool _isDateStartPeriodApproved;
        private bool _isDateEndPeriodApproved; 
        private int _DSMId;
        private int _supervisorId;
        private int _kpiTypeId;
        private int _proximityId;
        private int _settlementId;

        private int _regionId;
        private int _districtId;
        private int _citytId;
        private string _regionName;

        private const string OutletTypeBaseFilter = @"OLTypeIdColumn > 0";
        private const string OutletTypeGroupIdFilter = @"OLGroupIdColumn = ";
        private const string OutletSubTypeBaseFilter = @"OlSubTypeIdColumn > 0";
        private const string OutletSubTypeTypeIdFilter = @"OLTypeIdColumn = ";
        private const string GlobalLookupProximityBaseFilter = @"TableNameColumn = 'tblOutLets' AND FieldNameColumn = 'Proximity' ";
        private const string SupervisorBaseFilter = @"SupervisorIdColumn > 0";

        #endregion Private members

        #region Constructors

        /// <summary>
        /// Contructor
        /// </summary>
        public NewAimForm()
        {
            InitializeComponent();
            Localize();

            SetupSKUGridColumns();
            LoadData();
            SetupControls();            

            InitEvents();

            SetupUI();
        }

        #endregion Constructors

        #region Public properties

        /// <summary>
        /// Region ID
        /// </summary>
        public int RegionId
        {
            get
            {
                return _regionId;
            }
            set
            {
                teRegionName.Text = DataMediator.GetRegionName(value);
   
                _dsmList = DataMediator.GetDSMList(value);
                DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);

                _supervisorList = DataMediator.GetSupervisorList(value);
                DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);

                cbDSM_SelectedIndexChanged(null, null);

                if (_regionId == value)
                    return;
                _regionId = value;
            }
        }

        /// <summary>
        /// District ID
        /// </summary>
        public int DistrictId
        {
            get
            {
                return _districtId;
            }
            set
            {
                if (_districtId == value)
                    return;
                _districtId = value;
            }
        }

        /// <summary>
        /// City ID
        /// </summary>
        public int CityId
        {
            get
            {
                return _citytId;
            }
            set
            {
                if (_citytId == value)
                    return;
                _citytId = value;
            }
        }

        /// <summary>
        /// List of created Cascaded Aims
        /// </summary>
        public List<CascadedAimEntity> CreatedAims
        {
            get
            {
                List<CascadedAimEntity> result = new List<CascadedAimEntity>();

                foreach (ProductCombineKPI productKPI in _productCombineList)
                {
                    if (productKPI.KPIValueColumn != null && productKPI.KPIValueColumn.Trim().Length > 0)
                    {
                        DateTime now = DateTime.Now;
                        CascadedAimEntity aim = new CascadedAimEntity();
                        aim.AimId = int.MinValue;
                        aim.DateAdded = now;

                        aim.DateStartPeriod = deStartPeriod.DateTime;
                        aim.IsStartDateApproved = chkDateStartPeriodApproved.Checked;
                        aim.DateApproveStartPeriod = chkDateStartPeriodApproved.Checked ? now : DateTime.MinValue;

                        aim.DateEndPeriod = deEndPeriod.DateTime;
                        aim.IsEndDateApproved = chkDateEndPeriodApproved.Checked;
                        aim.DateApproveEndPeriod = chkDateEndPeriodApproved.Checked ? now : DateTime.MinValue; 

                        aim.CreatedUserId = AuthenticationManager.Instance.CurrentUserId;
                        aim.ApprovedUserId = AuthenticationManager.Instance.CurrentUserId;

                        aim.RegionId = _regionId;
                        aim.DistrictId = _districtId;
                        aim.CityId = _citytId;

                        aim.KPITypeName = DataConvertorHelper.GetNameFromComboboxSelectedItem<KPIEntity>(cbKPI.SelectedIndex, cbKPI.SelectedItem);// cbKPI.SelectedIndex > -1 ? (cbKPI.SelectedItem as KPIEntity).Name : "";
                        aim.DSMId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<DSMEntity>(cbDSM.SelectedIndex, cbDSM.SelectedItem);// cbDSM.SelectedIndex > -1 ? (cbDSM.SelectedItem as DSMEntity).Id : -1;
                        aim.SupervisorId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SupervisorEntity>(cbSupervisor.SelectedIndex, cbSupervisor.SelectedItem);//).Id : -1;
                        aim.SettlementId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SettlementEntity>(cbSettlement.SelectedIndex, cbSettlement.SelectedItem);//cbSettlement.SelectedIndex > -1 ? (cbSettlement.SelectedItem as SettlementEntity).Id : -1;
                        aim.ProximityId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<GlobalLookupEntity>(cbProximity.SelectedIndex, cbProximity.SelectedItem);//cbProximity.SelectedIndex > -1 ? (cbProximity.SelectedItem as GlobalLookupEntity).LKey : -1;
                        aim.ProductCnId = (int)productKPI.ProductCnIdColumn;
                        aim.IsFocused = false;
                        aim.OlTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletTypeEntity>(cbOutletType.SelectedIndex, cbOutletType.SelectedItem);// cbOutletType.SelectedIndex > -1 ? (cbOutletType.SelectedItem as OutletTypeEntity).Id : -1; 
                        aim.OlSubTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletSubTypeEntity>(cbOutletSubtype.SelectedIndex, cbOutletSubtype.SelectedItem);//cbOutletSubtype.SelectedIndex > -1 ? (cbOutletSubtype.SelectedItem as OutletSubTypeEntity).Id : -1;
                        aim.OlGroupId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletGroupEntity>(cbOutletGroup.SelectedIndex, cbOutletGroup.SelectedItem);//cbOutletGroup.SelectedIndex > -1 ? (cbOutletGroup.SelectedItem as OutletGroupEntity).Id : -1;
                        aim.KPITypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<KPIEntity>(cbKPI.SelectedIndex, cbKPI.SelectedItem);//cbKPI.SelectedIndex > -1 ? (cbKPI.SelectedItem as KPIEntity).Id : -1;
                        aim.KPIValue = productKPI.KPIValueColumn;
                        aim.ProductCombineName = productKPI.ProductCombineNameColumn;
                        aim.DeviationPercent = 0f;

                        aim.ProductCombineDLM = now;

                        result.Add(aim);
                    }
                
                }

                return result;
            }
        }

        #endregion Public properties

        #region Event Handlers

        private void gridControlSKU_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.V && e.Control)
            {
                
                gridViewSKU.HideEditor();
                e.Handled = true;

                PasteFromClipboard();
            }
            else if (e.KeyCode == Keys.C && e.Control)
            {

            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (IsValidated())
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void cbOutletGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletGroup.SelectedItem != null && cbOutletGroup.SelectedItem is OutletGroupEntity)
            {
                OutletGroupEntity currentOutletGroup = cbOutletGroup.SelectedItem as OutletGroupEntity;

                cbOutletType.Properties.Items.BeginUpdate();

                _outletTypeList.Clear();
                string filter = string.Format("{0} AND {1} {2}", OutletTypeBaseFilter, OutletTypeGroupIdFilter, currentOutletGroup.Id.ToString());
                _outletTypeList = new OutletTypesDataProvider().GetList(filter);
                DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);

                cbOutletType.Properties.Items.EndUpdate();

                cbOutletType.SelectedIndex = -1;
            }
        }

        private void cbOutletType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletType.SelectedItem != null && cbOutletType.SelectedItem is OutletTypeEntity)
            {
                OutletTypeEntity currentOutletType = cbOutletType.SelectedItem as OutletTypeEntity;

                cbOutletSubtype.Properties.Items.BeginUpdate();

                _outletSubTypeList.Clear();
                string filter = string.Format("{0} AND {1} {2}", OutletSubTypeBaseFilter, OutletSubTypeTypeIdFilter, currentOutletType.Id.ToString());
                _outletSubTypeList = new OutletSubTypesDataProvider().GetList(filter);
                DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);

                cbOutletSubtype.Properties.Items.EndUpdate();

                cbOutletSubtype.SelectedIndex = -1;
            }
        }

        private void ActiveEditor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.V && e.Control)
            {
               
                string clipboardText = Clipboard.GetText();

                if (clipboardText.IndexOf('\t', 0) > 0 || clipboardText.IndexOf('\n', 0) > 0)
                {
                    gridViewSKU.ActiveEditor.KeyDown -= ActiveEditor_KeyDown;
                    gridViewSKU.HideEditor();
                    PasteFromClipboard();
                    
                }
            }
        }

        private void cbKPI_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbKPI.SelectedIndex > -1)
            {
                if (cbKPI.SelectedItem is KPIEntity)
                {
                    string kpiName = (cbKPI.SelectedItem as KPIEntity).Name;
                    if (!gcKPIValue.Caption.Equals(kpiName))
                        gcKPIValue.Caption = kpiName;
                }
            }
        }

        private void cbDSM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDSM.SelectedItem != null)
            {
                if (cbDSM.SelectedItem is DSMEntity)
                {
                    DSMEntity currentDSM = (DSMEntity)cbDSM.SelectedItem;

                    cbSupervisor.Properties.Items.BeginUpdate();

                    if (_supervisorList != null)
                        _supervisorList.Clear();

                    string filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorDSMId, currentDSM.Id);
                    _supervisorList = new SupervisorsDataProvider().GetList(filter);

                    DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);

                    cbSupervisor.Properties.Items.EndUpdate();

                    cbSupervisor.SelectedIndex = -1;
                }
                else
                { 
                    if (cbDSM.SelectedText.Contains(Resource.ComboBoxItemAnyValueText))
                    {
                        cbSupervisor.Properties.Items.BeginUpdate();

                        if (_supervisorList != null)
                            _supervisorList.Clear();

                        if (_regionId > 0)
                        {
                            string filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorRegionId, _regionId);
                            _supervisorList = new SupervisorsDataProvider().GetList(filter);
                        }
                        else
                        { 
                            _supervisorList = new SupervisorsDataProvider().GetList(SupervisorBaseFilter); 
                        }

                        DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);

                        cbSupervisor.Properties.Items.EndUpdate();

                        cbSupervisor.SelectedIndex = -1;
                    }
                }
            
            }
        }

        private void gridViewSKU_ShownEditor(object sender, EventArgs e)
        {
            gridViewSKU.ActiveEditor.KeyDown += new KeyEventHandler(ActiveEditor_KeyDown);
        }

        private void chkDatePeriodApproved_CheckedChanged(object sender, EventArgs e)
        {
            UpdateEditableControls();
        }

        #endregion Event Handlers

        #region Private methods

        private void Localize()
        {
            this.Text = Resource.CopiedNewAimFormCaption; 
            btnOk.Text = Resource.NewAimFormButtonOk;
            btnCancel.Text = Resource.NewAimFormButtonCancel;
        }

        private void SetupSKUGridColumns()
        {
            gridViewSKU.Columns.Clear();
            gridViewSKU.Columns.AddRange(GetGridColumns());
            //gridViewSKU.BestFitColumns();
            gridViewSKU.OptionsSelection.MultiSelect = true;
        }

        private void SetupControls()
        {
            DataConvertorHelper.PopulateList<SettlementEntity>(cbSettlement.Properties.Items, _settlementList);
            DataConvertorHelper.PopulateList<OutletGroupEntity>(cbOutletGroup.Properties.Items, _outletGroupList);
            DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);
            DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);
            DataConvertorHelper.PopulateList<KPIEntity>(cbKPI.Properties.Items, _kpiList);
            DataConvertorHelper.PopulateList<GlobalLookupEntity>(cbProximity.Properties.Items, _proximityList);
            DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);
            DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);

            gridControlSKU.BeginUpdate();
            gridControlSKU.DataSource = null;
            _productCombineList.Clear();
            foreach (ProductCombineEntity productEntity in _productsList)
                _productCombineList.Add(new ProductCombineKPI(productEntity));
            gridControlSKU.DataSource = _productCombineList;
            gridControlSKU.EndUpdate();

            teRegionName.Text = DataMediator.GetRegionName(_regionId);

            cbDSM.SelectedIndex = 0;
            cbSupervisor.SelectedIndex = 0;
            cbKPI.SelectedIndex = 0;
            cbSettlement.SelectedIndex = 0;
            cbProximity.SelectedIndex = 0;
            cbOutletGroup.SelectedIndex = 0;
            cbOutletType.SelectedIndex = 0;
            cbOutletSubtype.SelectedIndex = 0;

            deStartPeriod.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            deEndPeriod.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        private void SetupUI()
        {
            chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = false;
            switch (AuthenticationManager.Instance.CurrentUserLevel)
            {
                case 5:
                    chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = true;
                    break;
            }
        }

        private void LoadData()
        { 
            _settlementList =  new SettlementDataProvider().GetList("SettlementIdColumn > 0");

            _outletGroupList = new OutletGroupsDataProvider().GetList("OLGroupIdColumn > 0");

            _outletTypeList = new OutletTypesDataProvider().GetList(OutletTypeBaseFilter);

            _outletSubTypeList = new OutletSubTypesDataProvider().GetList(OutletSubTypeBaseFilter);

            _kpiList = new KPIDataProvider().GetList();

            _proximityList = new GlobalLookupDataProvider().GetList(GlobalLookupProximityBaseFilter);

            _supervisorList = new SupervisorsDataProvider().GetList(SupervisorBaseFilter);

            _dsmList = new DSMDataProvider().GetList();

            cbDSM_SelectedIndexChanged(null, null);

            _productsList = new ProductCombineDataProvider().GetList();

        }

        private void InitEvents()
        {
            chkDateStartPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);
            chkDateEndPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);

            cbKPI.SelectedIndexChanged += new EventHandler(cbKPI_SelectedIndexChanged);
            cbOutletGroup.SelectedIndexChanged += new EventHandler(cbOutletGroup_SelectedIndexChanged);
            cbOutletType.SelectedIndexChanged += new EventHandler(cbOutletType_SelectedIndexChanged);
            cbDSM.SelectedIndexChanged += new EventHandler(cbDSM_SelectedIndexChanged);

            gridViewSKU.ShownEditor += new EventHandler(gridViewSKU_ShownEditor);            
        }

        private void PasteFromClipboard()
        {
            List<ProductCombineKPI> clipboardKPI = new List<ProductCombineKPI>();
            string clipboardText = Clipboard.GetText();

            string[] splitted = clipboardText.Split( new char[] {'\n' }, StringSplitOptions.None);
            foreach (string s in splitted)
            {
                string[] splitByItems = s.Split(new char[] { '\t' }, StringSplitOptions.None);
                if (splitByItems.Length == gridViewSKU.Columns.Count)
                {
                    int skuId = 0;
                    if (int.TryParse(splitByItems[0], out skuId))
                    {
                        ProductCombineKPI productCombineKPI = new ProductCombineKPI
                            (
                                skuId, 
                                splitByItems[1], 
                                splitByItems[2]
                            );

                        clipboardKPI.Add(productCombineKPI);
                    }
                }
            }

            foreach(ProductCombineKPI product in _productCombineList)
            {
                ProductCombineKPI res = clipboardKPI.Find(delegate(ProductCombineKPI prod) { return prod.ProductCnIdColumn == product.ProductCnIdColumn; });
                if (res != null)
                {
                    product.KPIValueColumn = res.KPIValueColumn;
                }
            }

            gridControlSKU.DataSource = null;
            gridControlSKU.DataSource = _productCombineList;
        }

        private GridColumn[] GetGridColumns()
        {
            IList<GridColumn> columnsList = new List<GridColumn>();
            GridColumn[] columns = null;

            gcProductCombineId.Name = MappedNames.ProductCnId;
            columnsList.Add(gcProductCombineId);

            gcProductCombineName.Name = MappedNames.ProductCombineName;
            columnsList.Add(gcProductCombineName);

            gcKPIValue.Name = MappedNames.KPIValue;
            columnsList.Add(gcKPIValue);

            int i = 0;
            foreach (GridColumn column in columnsList)
            {
                column.FieldName = column.Name;
                column.Caption = LocalizationProvider.GetText(column.Name);
                column.Visible = true;
                column.OptionsColumn.ReadOnly = true;
                column.Width = 50;
                column.VisibleIndex = i++;
            }

            gcProductCombineId.Caption = Resource.ProductCnIdColumnShortName;
            gcProductCombineId.Width = 50;
            //gcProductCombineId.OptionsColumn.FixedWidth = true;
            gcProductCombineName.Width = 150;
            gcKPIValue.Width = 80;
           // gcKPIValue.OptionsColumn.FixedWidth = true;

            gcKPIValue.OptionsColumn.ReadOnly = false;

            if (columnsList.Count > 0)
            {
                columns = new GridColumn[columnsList.Count];
                columnsList.CopyTo(columns, 0);
            }

            return columns;
        }

        private bool ValidatePeriod(DateTime dateFrom, DateTime dateTo)
        {
            if (dateFrom > DateTime.MinValue && dateTo > DateTime.MinValue)
                if (dateFrom > dateTo)
                    return false;

            return true;
        }

        private void UpdateEditableControls()
        {
            deStartPeriod.Properties.ReadOnly = chkDateStartPeriodApproved.Checked;
            deEndPeriod.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;

            chkDateStartPeriodApproved.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;
            chkDateEndPeriodApproved.Properties.ReadOnly = !chkDateStartPeriodApproved.Checked;

            bool readOnly = chkDateStartPeriodApproved.Checked || chkDateEndPeriodApproved.Checked;
        
            gcKPIValue.OptionsColumn.ReadOnly = readOnly;

            cbKPI.Properties.ReadOnly = readOnly;

            cbDSM.Properties.ReadOnly = readOnly;
            cbSupervisor.Properties.ReadOnly = readOnly;

            cbSettlement.Properties.ReadOnly = readOnly;
            cbProximity.Properties.ReadOnly = readOnly;

            cbOutletGroup.Properties.ReadOnly = readOnly;
            cbOutletType.Properties.ReadOnly = readOnly;
            cbOutletSubtype.Properties.ReadOnly = readOnly;
        
        }

        private bool ContainsOnlyBaseData()
        {
            return cbDSM.SelectedIndex <= 0 && cbSupervisor.SelectedIndex <= 0
                && cbSettlement.SelectedIndex <= 0 && cbProximity.SelectedIndex <= 0
                && cbOutletGroup.SelectedIndex <= 0 && cbOutletType.SelectedIndex <= 0 && cbOutletSubtype.SelectedIndex <= 0;
        }

        private bool HasDateBeenSet(DateTime date)
        {
            return date.Year > 1900;
        }

        private bool IsValidated()
        {
            bool isValidData = false;

            DateTime now = DateTime.Now.Date;

            if (!HasDateBeenSet(deStartPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.PeriodStartDateIsNotSet);
                return isValidData;
            }

            if (deStartPeriod.DateTime.Date < now)
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.PeriodStartDateLessThenCurrentDate);
                return isValidData;
            }

            if (!HasDateBeenSet(deEndPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateIsNotSet);
                return isValidData;
            }

            if (deEndPeriod.DateTime.Date < now)
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateLessThenCurrentDate);
                return isValidData;
            }

            if (!DateHelper.ValidatePeriod(deStartPeriod.DateTime, deEndPeriod.DateTime))
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.IncorrectFromToDates);
                return isValidData;
            }

            if (chkDateStartPeriodApproved.Checked && deStartPeriod.DateTime != DateTime.MinValue && deStartPeriod.DateTime.Date < now)
            {
                chkDateStartPeriodApproved.Checked = false;
                deStartPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodStartDateLessThanNow);
                return isValidData;
            }

            if (chkDateEndPeriodApproved.Checked && deEndPeriod.DateTime != DateTime.MinValue && deEndPeriod.DateTime.Date < now)
            {
                chkDateEndPeriodApproved.Checked = false;
                deEndPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodEndDateLessThanNow);
                return isValidData;
            }

            if (cbKPI.SelectedIndex <= 0)
            {
                cbKPI.Focus();
                MessageBox.Show(Resource.KPITypeHasNotBeenDefined);
                return isValidData;
            }

            if (ContainsOnlyBaseData() && AuthenticationManager.Instance.CurrentUserLevel < 5)
            {
                cbDSM.Focus();
                MessageBox.Show(Resource.CurrentUserHasNoRightsToCreateBaseAims);
                return isValidData;
            }

            List<string> errors = new List<string>();
            foreach (CascadedAimEntity aim in CreatedAims)
            {
                DataMediator.CheckForCrossedAimsErrors(aim, errors);
            }

            foreach (CascadedAimEntity aim in CreatedAims)
            {
                DataMediator.CheckForSameAimsErrors(aim, errors);
            }

            if (errors.Count > 0)
            {
                cbKPI.Focus();
                DetailedMessageForm.Show(errors.ToArray());
                return isValidData;
            }

            return true;
        }

        #endregion Private methods

        #region Grid Columns definition
        private GridColumn gcProductCombineId = new GridColumn();
        private GridColumn gcProductCombineName = new GridColumn();
        private GridColumn gcKPIValue = new GridColumn();
        #endregion Grid Columns definition

    }

}