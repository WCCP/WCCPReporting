namespace Logica.Reports.RPI.Forms
{
    partial class NewAimForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCenter = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tpAimAttributes = new DevExpress.XtraTab.XtraTabPage();
            this.teRegionName = new DevExpress.XtraEditors.TextEdit();
            this.lblRegionName = new DevExpress.XtraEditors.LabelControl();
            this.cbOutletSubtype = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbOutletType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbOutletGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbSettlement = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbProximity = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbKPI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbSupervisor = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbDSM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblOutletType = new DevExpress.XtraEditors.LabelControl();
            this.lblOutletSubType = new DevExpress.XtraEditors.LabelControl();
            this.lblOutletGroup = new DevExpress.XtraEditors.LabelControl();
            this.lblSettlement = new DevExpress.XtraEditors.LabelControl();
            this.lblProximity = new DevExpress.XtraEditors.LabelControl();
            this.lblKPI = new DevExpress.XtraEditors.LabelControl();
            this.lblSupervisor = new DevExpress.XtraEditors.LabelControl();
            this.lblDSM = new DevExpress.XtraEditors.LabelControl();
            this.chkDateEndPeriodApproved = new DevExpress.XtraEditors.CheckEdit();
            this.chkDateStartPeriodApproved = new DevExpress.XtraEditors.CheckEdit();
            this.lblDateEndPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblDateStartPeriod = new DevExpress.XtraEditors.LabelControl();
            this.deEndPeriod = new DevExpress.XtraEditors.DateEdit();
            this.deStartPeriod = new DevExpress.XtraEditors.DateEdit();
            this.tpSKU = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSKU = new DevExpress.XtraGrid.GridControl();
            this.gridViewSKU = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pnlBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCenter)).BeginInit();
            this.pnlCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tpAimAttributes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teRegionName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletSubtype.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSettlement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProximity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbKPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateEndPeriodApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateStartPeriodApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties)).BeginInit();
            this.tpSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCenter
            // 
            this.pnlCenter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCenter.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlCenter.Appearance.Options.UseBackColor = true;
            this.pnlCenter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlCenter.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlCenter.Controls.Add(this.xtraTabControl1);
            this.pnlCenter.Location = new System.Drawing.Point(0, 0);
            this.pnlCenter.Name = "pnlCenter";
            this.pnlCenter.Size = new System.Drawing.Size(361, 357);
            this.pnlCenter.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tpAimAttributes;
            this.xtraTabControl1.Size = new System.Drawing.Size(361, 357);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage [] {
            this.tpAimAttributes,
            this.tpSKU});
            // 
            // tpAimAttributes
            // 
            this.tpAimAttributes.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpAimAttributes.Appearance.PageClient.Options.UseBackColor = true;
            this.tpAimAttributes.Controls.Add(this.teRegionName);
            this.tpAimAttributes.Controls.Add(this.lblRegionName);
            this.tpAimAttributes.Controls.Add(this.cbOutletSubtype);
            this.tpAimAttributes.Controls.Add(this.cbOutletType);
            this.tpAimAttributes.Controls.Add(this.cbOutletGroup);
            this.tpAimAttributes.Controls.Add(this.cbSettlement);
            this.tpAimAttributes.Controls.Add(this.cbProximity);
            this.tpAimAttributes.Controls.Add(this.cbKPI);
            this.tpAimAttributes.Controls.Add(this.cbSupervisor);
            this.tpAimAttributes.Controls.Add(this.cbDSM);
            this.tpAimAttributes.Controls.Add(this.lblOutletType);
            this.tpAimAttributes.Controls.Add(this.lblOutletSubType);
            this.tpAimAttributes.Controls.Add(this.lblOutletGroup);
            this.tpAimAttributes.Controls.Add(this.lblSettlement);
            this.tpAimAttributes.Controls.Add(this.lblProximity);
            this.tpAimAttributes.Controls.Add(this.lblKPI);
            this.tpAimAttributes.Controls.Add(this.lblSupervisor);
            this.tpAimAttributes.Controls.Add(this.lblDSM);
            this.tpAimAttributes.Controls.Add(this.chkDateEndPeriodApproved);
            this.tpAimAttributes.Controls.Add(this.chkDateStartPeriodApproved);
            this.tpAimAttributes.Controls.Add(this.lblDateEndPeriod);
            this.tpAimAttributes.Controls.Add(this.lblDateStartPeriod);
            this.tpAimAttributes.Controls.Add(this.deEndPeriod);
            this.tpAimAttributes.Controls.Add(this.deStartPeriod);
            this.tpAimAttributes.Name = "tpAimAttributes";
            this.tpAimAttributes.Size = new System.Drawing.Size(357, 333);
            this.tpAimAttributes.Text = "��������";
            // 
            // teRegionName
            // 
            this.teRegionName.Location = new System.Drawing.Point(127, 69);
            this.teRegionName.Name = "teRegionName";
            this.teRegionName.Properties.ReadOnly = true;
            this.teRegionName.Size = new System.Drawing.Size(207, 22);
            this.teRegionName.TabIndex = 24;
            // 
            // lblRegionName
            // 
            this.lblRegionName.Location = new System.Drawing.Point(8, 75);
            this.lblRegionName.Name = "lblRegionName";
            this.lblRegionName.Size = new System.Drawing.Size(35, 13);
            this.lblRegionName.TabIndex = 23;
            this.lblRegionName.Text = "������";
            // 
            // cbOutletSubtype
            // 
            this.cbOutletSubtype.Location = new System.Drawing.Point(127, 301);
            this.cbOutletSubtype.Name = "cbOutletSubtype";
            this.cbOutletSubtype.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletSubtype.Size = new System.Drawing.Size(207, 22);
            this.cbOutletSubtype.TabIndex = 22;
            // 
            // cbOutletType
            // 
            this.cbOutletType.Location = new System.Drawing.Point(127, 272);
            this.cbOutletType.Name = "cbOutletType";
            this.cbOutletType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletType.Size = new System.Drawing.Size(207, 22);
            this.cbOutletType.TabIndex = 21;
            // 
            // cbOutletGroup
            // 
            this.cbOutletGroup.Location = new System.Drawing.Point(127, 244);
            this.cbOutletGroup.Name = "cbOutletGroup";
            this.cbOutletGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbOutletGroup.Size = new System.Drawing.Size(207, 22);
            this.cbOutletGroup.TabIndex = 20;
            // 
            // cbSettlement
            // 
            this.cbSettlement.Location = new System.Drawing.Point(127, 214);
            this.cbSettlement.Name = "cbSettlement";
            this.cbSettlement.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSettlement.Size = new System.Drawing.Size(207, 22);
            this.cbSettlement.TabIndex = 19;
            // 
            // cbProximity
            // 
            this.cbProximity.Location = new System.Drawing.Point(127, 184);
            this.cbProximity.Name = "cbProximity";
            this.cbProximity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbProximity.Size = new System.Drawing.Size(207, 22);
            this.cbProximity.TabIndex = 18;
            // 
            // cbKPI
            // 
            this.cbKPI.Location = new System.Drawing.Point(127, 155);
            this.cbKPI.Name = "cbKPI";
            this.cbKPI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbKPI.Size = new System.Drawing.Size(207, 22);
            this.cbKPI.TabIndex = 17;
            // 
            // cbSupervisor
            // 
            this.cbSupervisor.Location = new System.Drawing.Point(127, 127);
            this.cbSupervisor.Name = "cbSupervisor";
            this.cbSupervisor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSupervisor.Size = new System.Drawing.Size(207, 22);
            this.cbSupervisor.TabIndex = 16;
            // 
            // cbDSM
            // 
            this.cbDSM.Location = new System.Drawing.Point(127, 98);
            this.cbDSM.Name = "cbDSM";
            this.cbDSM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDSM.Size = new System.Drawing.Size(207, 22);
            this.cbDSM.TabIndex = 15;
            // 
            // lblOutletType
            // 
            this.lblOutletType.Location = new System.Drawing.Point(9, 279);
            this.lblOutletType.Name = "lblOutletType";
            this.lblOutletType.Size = new System.Drawing.Size(33, 13);
            this.lblOutletType.TabIndex = 14;
            this.lblOutletType.Text = "��� ��";
            // 
            // lblOutletSubType
            // 
            this.lblOutletSubType.Location = new System.Drawing.Point(9, 308);
            this.lblOutletSubType.Name = "lblOutletSubType";
            this.lblOutletSubType.Size = new System.Drawing.Size(53, 13);
            this.lblOutletSubType.TabIndex = 13;
            this.lblOutletSubType.Text = "������ ��";
            // 
            // lblOutletGroup
            // 
            this.lblOutletGroup.Location = new System.Drawing.Point(9, 251);
            this.lblOutletGroup.Name = "lblOutletGroup";
            this.lblOutletGroup.Size = new System.Drawing.Size(51, 13);
            this.lblOutletGroup.TabIndex = 12;
            this.lblOutletGroup.Text = "������ ��";
            // 
            // lblSettlement
            // 
            this.lblSettlement.Location = new System.Drawing.Point(9, 219);
            this.lblSettlement.Name = "lblSettlement";
            this.lblSettlement.Size = new System.Drawing.Size(104, 13);
            this.lblSettlement.TabIndex = 11;
            this.lblSettlement.Text = "���������/��������";
            // 
            // lblProximity
            // 
            this.lblProximity.Location = new System.Drawing.Point(9, 188);
            this.lblProximity.Name = "lblProximity";
            this.lblProximity.Size = new System.Drawing.Size(87, 13);
            this.lblProximity.TabIndex = 10;
            this.lblProximity.Text = "������ ��������";
            // 
            // lblKPI
            // 
            this.lblKPI.Location = new System.Drawing.Point(9, 158);
            this.lblKPI.Name = "lblKPI";
            this.lblKPI.Size = new System.Drawing.Size(16, 13);
            this.lblKPI.TabIndex = 9;
            this.lblKPI.Text = "KPI";
            // 
            // lblSupervisor
            // 
            this.lblSupervisor.Location = new System.Drawing.Point(9, 130);
            this.lblSupervisor.Name = "lblSupervisor";
            this.lblSupervisor.Size = new System.Drawing.Size(14, 13);
            this.lblSupervisor.TabIndex = 8;
            this.lblSupervisor.Text = "M2";
            // 
            // lblDSM
            // 
            this.lblDSM.Location = new System.Drawing.Point(9, 105);
            this.lblDSM.Name = "lblDSM";
            this.lblDSM.Size = new System.Drawing.Size(14, 13);
            this.lblDSM.TabIndex = 7;
            this.lblDSM.Text = "�3";
            // 
            // chkDateEndPeriodApproved
            // 
            this.chkDateEndPeriodApproved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDateEndPeriodApproved.Location = new System.Drawing.Point(254, 42);
            this.chkDateEndPeriodApproved.Name = "chkDateEndPeriodApproved";
            this.chkDateEndPeriodApproved.Properties.Caption = "����������";
            this.chkDateEndPeriodApproved.Size = new System.Drawing.Size(93, 18);
            this.chkDateEndPeriodApproved.TabIndex = 6;
            // 
            // chkDateStartPeriodApproved
            // 
            this.chkDateStartPeriodApproved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDateStartPeriodApproved.Location = new System.Drawing.Point(254, 12);
            this.chkDateStartPeriodApproved.Name = "chkDateStartPeriodApproved";
            this.chkDateStartPeriodApproved.Properties.Caption = "����������";
            this.chkDateStartPeriodApproved.Size = new System.Drawing.Size(93, 18);
            this.chkDateStartPeriodApproved.TabIndex = 5;
            // 
            // lblDateEndPeriod
            // 
            this.lblDateEndPeriod.Location = new System.Drawing.Point(9, 44);
            this.lblDateEndPeriod.Name = "lblDateEndPeriod";
            this.lblDateEndPeriod.Size = new System.Drawing.Size(70, 13);
            this.lblDateEndPeriod.TabIndex = 3;
            this.lblDateEndPeriod.Text = "��������� ��";
            // 
            // lblDateStartPeriod
            // 
            this.lblDateStartPeriod.Location = new System.Drawing.Point(9, 14);
            this.lblDateStartPeriod.Name = "lblDateStartPeriod";
            this.lblDateStartPeriod.Size = new System.Drawing.Size(63, 13);
            this.lblDateStartPeriod.TabIndex = 2;
            this.lblDateStartPeriod.Text = "��������� �";
            // 
            // deEndPeriod
            // 
            this.deEndPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deEndPeriod.EditValue = null;
            this.deEndPeriod.Location = new System.Drawing.Point(127, 41);
            this.deEndPeriod.Name = "deEndPeriod";
            this.deEndPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndPeriod.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndPeriod.Size = new System.Drawing.Size(121, 22);
            this.deEndPeriod.TabIndex = 1;
            // 
            // deStartPeriod
            // 
            this.deStartPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deStartPeriod.EditValue = null;
            this.deStartPeriod.Location = new System.Drawing.Point(127, 11);
            this.deStartPeriod.Name = "deStartPeriod";
            this.deStartPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartPeriod.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton [] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStartPeriod.Size = new System.Drawing.Size(121, 22);
            this.deStartPeriod.TabIndex = 0;
            // 
            // tpSKU
            // 
            this.tpSKU.Controls.Add(this.gridControlSKU);
            this.tpSKU.Name = "tpSKU";
            this.tpSKU.Size = new System.Drawing.Size(357, 333);
            this.tpSKU.Text = "���";
            // 
            // gridControlSKU
            // 
            this.gridControlSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSKU.Location = new System.Drawing.Point(0, 0);
            this.gridControlSKU.MainView = this.gridViewSKU;
            this.gridControlSKU.Name = "gridControlSKU";
            this.gridControlSKU.Size = new System.Drawing.Size(357, 333);
            this.gridControlSKU.TabIndex = 0;
            this.gridControlSKU.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView [] {
            this.gridViewSKU});
            this.gridControlSKU.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlSKU_KeyDown);
            // 
            // gridViewSKU
            // 
            this.gridViewSKU.GridControl = this.gridControlSKU;
            this.gridViewSKU.Name = "gridViewSKU";
            // 
            // pnlBottom
            // 
            this.pnlBottom.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.Appearance.Options.UseBackColor = true;
            this.pnlBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBottom.Controls.Add(this.btnOk);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 363);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(361, 46);
            this.pnlBottom.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(11, 12);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(91, 25);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "�������";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(258, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(91, 25);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "������";
            // 
            // NewAimForm
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 409);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlCenter);
            this.MinimumSize = new System.Drawing.Size(342, 380);
            this.Name = "NewAimForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� ����";
            ((System.ComponentModel.ISupportInitialize)(this.pnlCenter)).EndInit();
            this.pnlCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tpAimAttributes.ResumeLayout(false);
            this.tpAimAttributes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teRegionName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletSubtype.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbOutletGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSettlement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProximity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbKPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateEndPeriodApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDateStartPeriodApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartPeriod.Properties)).EndInit();
            this.tpSKU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlCenter;
        private DevExpress.XtraEditors.PanelControl pnlBottom;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tpSKU;
        private DevExpress.XtraTab.XtraTabPage tpAimAttributes;
        private DevExpress.XtraGrid.GridControl gridControlSKU;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSKU;
        private DevExpress.XtraEditors.LabelControl lblDateEndPeriod;
        private DevExpress.XtraEditors.LabelControl lblDateStartPeriod;
        private DevExpress.XtraEditors.DateEdit deEndPeriod;
        private DevExpress.XtraEditors.DateEdit deStartPeriod;
        private DevExpress.XtraEditors.CheckEdit chkDateEndPeriodApproved;
        private DevExpress.XtraEditors.CheckEdit chkDateStartPeriodApproved;
        private DevExpress.XtraEditors.LabelControl lblOutletType;
        private DevExpress.XtraEditors.LabelControl lblOutletSubType;
        private DevExpress.XtraEditors.LabelControl lblOutletGroup;
        private DevExpress.XtraEditors.LabelControl lblSettlement;
        private DevExpress.XtraEditors.LabelControl lblProximity;
        private DevExpress.XtraEditors.LabelControl lblKPI;
        private DevExpress.XtraEditors.LabelControl lblSupervisor;
        private DevExpress.XtraEditors.LabelControl lblDSM;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletSubtype;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletType;
        private DevExpress.XtraEditors.ComboBoxEdit cbOutletGroup;
        private DevExpress.XtraEditors.ComboBoxEdit cbSettlement;
        private DevExpress.XtraEditors.ComboBoxEdit cbProximity;
        private DevExpress.XtraEditors.ComboBoxEdit cbKPI;
        private DevExpress.XtraEditors.ComboBoxEdit cbSupervisor;
        private DevExpress.XtraEditors.ComboBoxEdit cbDSM;
        private DevExpress.XtraEditors.LabelControl lblRegionName;
        private DevExpress.XtraEditors.TextEdit teRegionName;
    }

}