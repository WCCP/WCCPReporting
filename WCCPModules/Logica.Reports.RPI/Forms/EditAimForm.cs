using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.Authentication;

namespace Logica.Reports.RPI.Forms
{
    /// <summary>
    /// Form for editing parameters of Cascaded Aim
    /// </summary>
    public partial class EditAimForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private members

        private CascadedAimEntity _editedAim;
        private CascadedAimEntity _oldEditedAim;

        private IList<ProductCombineKPI> _productCombineList = new List<ProductCombineKPI>();
        private IList<SettlementEntity> _settlementList;
        private IList<OutletGroupEntity> _outletGroupList;
        private IList<OutletTypeEntity> _outletTypeList;
        private IList<OutletSubTypeEntity> _outletSubTypeList;
        private IList<KPIEntity> _kpiList;
        private IList<GlobalLookupEntity> _proximityList;
        private IList<SupervisorEntity> _supervisorList;
        private IList<DSMEntity> _dsmList;
        private IList<ProductCombineEntity> _productsList;

        private const string OutletTypeBaseFilter = @"OLTypeIdColumn > 0";
        private const string OutletTypeGroupIdFilter = @"OLGroupIdColumn = ";
        private const string OutletSubTypeBaseFilter = @"OlSubTypeIdColumn > 0";
        private const string OutletSubTypeTypeIdFilter = @"OLTypeIdColumn = ";
        private const string GlobalLookupProximityBaseFilter = @"TableNameColumn = 'tblOutLets' AND FieldNameColumn = 'Proximity' ";
        private const string SupervisorBaseFilter = @"SupervisorIdColumn > 0";

        #endregion Private members

        #region Constructors

        public EditAimForm()
        {
            InitializeComponent();

            LoadData();
            SetupControls();

            InitEvents();

            SetupUI();
        }

        #endregion Constructors

        #region Public properties

        /// <summary>
        /// Edited Cascaded Aim
        /// </summary>
        public CascadedAimEntity EditedAim
        {
            get
            {
                DateTime now = DateTime.Now;

                _editedAim.DateAdded = now;

                _editedAim.DateStartPeriod = deStartPeriod.DateTime;
                _editedAim.IsStartDateApproved = chkDateStartPeriodApproved.Checked;
                _editedAim.DateApproveStartPeriod =
                    chkDateStartPeriodApproved.Checked != _oldEditedAim.IsStartDateApproved ? now : _editedAim.DateApproveStartPeriod;

                _editedAim.DateEndPeriod = deEndPeriod.DateTime;
                _editedAim.IsEndDateApproved = chkDateEndPeriodApproved.Checked;
                _editedAim.DateApproveEndPeriod = 
                    chkDateEndPeriodApproved.Checked != _oldEditedAim.IsEndDateApproved ? now : _editedAim.DateApproveEndPeriod;

                _editedAim.CreatedUserId = AuthenticationManager.Instance.CurrentUserId;
                _editedAim.ApprovedUserId = AuthenticationManager.Instance.CurrentUserId;

                //_editedAim.KPITypeName = cbKPI.SelectedIndex > -1 ? (cbKPI.SelectedItem as KPIEntity).Name : "";
                _editedAim.IsFocused = false;
                _editedAim.KPIValue = teKPIValue.Text;

                _editedAim.DeviationPercent = 0f;

                _editedAim.ProductCombineDLM = now;

                return _editedAim;
            }
        }

        #endregion Public properties

        #region Public methods

        /// <summary>
        /// Initialize Form by data 
        /// </summary>
        /// <param name="aim">Class wich contains Cascaded Aims data</param>
        public void Init(CascadedAimEntity aim)
        {
            _oldEditedAim = aim.Clone() as CascadedAimEntity;
            _editedAim = aim.Clone() as CascadedAimEntity;

            cbSKU.Text = _editedAim.ProductCnId < 0 ? Resource.ComboBoxItemAnyValueText : _editedAim.ProductCombineName;

            if (_editedAim.DateStartPeriod == DateTime.MinValue)
                deStartPeriod.Text = "";
            else
                deStartPeriod.DateTime = _editedAim.DateStartPeriod;

            chkDateStartPeriodApproved.Checked = _editedAim.IsStartDateApproved;

            if (deStartPeriod.DateTime < DateTime.Now.Date)
                chkDateStartPeriodApproved.Enabled = false;

            if ( _editedAim.DateEndPeriod  == DateTime.MinValue)
                deEndPeriod.Text = "";
            else
                deEndPeriod.DateTime = _editedAim.DateEndPeriod;

            chkDateEndPeriodApproved.Checked = _editedAim.IsEndDateApproved;

            teRegionName.Text = DataMediator.GetRegionName(_oldEditedAim.RegionId);

            _dsmList = DataMediator.GetDSMList(_oldEditedAim.RegionId);
            DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);
            int itemIndex = GetIndex<DSMEntity>(new List<DSMEntity>(_dsmList), _editedAim.DSMId);
            cbDSM.SelectedIndex = itemIndex == -1 ? 0 : itemIndex; 
            //cbDSM.Text = GetValue<DSMEntity>(new List<DSMEntity>( _dsmList), _editedAim.DSMId);
            cbDSM_SelectedIndexChanged(null, null);

            cbSupervisor.Text =  GetValue<SupervisorEntity>( new SupervisorsDataProvider().GetList(), _editedAim.SupervisorId);

            itemIndex = GetIndex<KPIEntity>( new KPIDataProvider().GetList(), _editedAim.KPITypeId < 0 ? 0 : _editedAim.KPITypeId);
            cbKPI.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            //cbKPI.Text = _editedAim.KPITypeId < 0 ? Resource.ComboBoxItemAnyValueText : _editedAim.KPITypeName;

            teKPIValue.Text = _editedAim.KPIValue;

            cbProximity.Text =  GetValue<GlobalLookupEntity>( new GlobalLookupDataProvider().GetList(@"TableNameColumn = 'tblOutLets' AND FieldNameColumn = 'Proximity'"), _editedAim.ProximityId);
            cbSettlement.Text =  GetValue<SettlementEntity>( new SettlementDataProvider().GetList(), _editedAim.SettlementId);

            itemIndex = GetIndex<OutletGroupEntity>(new OutletGroupsDataProvider().GetList(), _editedAim.OlGroupId < 0 ? 0 : _editedAim.OlGroupId);
            cbOutletGroup.SelectedIndex = itemIndex == -1 ? 0 : itemIndex - 1;
            //cbOutletGroup.Text =  GetValue<OutletGroupEntity>( new OutletGroupsDataProvider().GetList(), _editedAim.OlGroupId);
            cbOutletGroup_SelectedIndexChanged(null, null);

            itemIndex = GetIndex<OutletTypeEntity>(new List<OutletTypeEntity> (_outletTypeList), _editedAim.OlTypeId < 0 ? 0 : _editedAim.OlTypeId);
            cbOutletType.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            //cbOutletType.Text = GetValue<OutletTypeEntity>(new OutletTypesDataProvider().GetList(), _editedAim.OlTypeId);
            cbOutletType_SelectedIndexChanged(null, null);

            itemIndex = GetIndex<OutletSubTypeEntity>(new List<OutletSubTypeEntity>(_outletSubTypeList), _editedAim.OlSubTypeId < 0 ? 0 : _editedAim.OlSubTypeId);
            cbOutletSubtype.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            //cbOutletSubtype.Text =  GetValue<OutletSubTypeEntity>( new OutletSubTypesDataProvider().GetList(),_editedAim.OlSubTypeId);

            chkDatePeriodApproved_CheckedChanged(null, null);

            // add check for position CO_PPM_M5
            if  (DataAccessProvider.HasUserPosition(DataAccessProvider.PositionCoPpmM5)) {
                chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = true;
                deStartPeriod.Enabled = true;
                deEndPeriod.Enabled = true;
            }
        }

        /// <summary>
        /// Localize form
        /// </summary>
        /// <param name="formCaption">Form caption</param>
        /// <param name="btnOkText">Button Ok text</param>
        /// <param name="btnCancelText">Button Cancel text</param>
        public void Localize(string formCaption, string btnOkText, string btnCancelText)
        {
            this.Text = formCaption;
            btnOk.Text = btnOkText;
            btnCancel.Text = btnCancelText;
        }

        #endregion Public methods

        #region Private methods

        private void SetupControls()
        {
            DataConvertorHelper.PopulateList<ProductCombineEntity>(cbSKU.Properties.Items, _productsList);
            DataConvertorHelper.PopulateList<SettlementEntity>(cbSettlement.Properties.Items, _settlementList);
            DataConvertorHelper.PopulateList<OutletGroupEntity>(cbOutletGroup.Properties.Items, _outletGroupList);
            DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);
            DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);
            DataConvertorHelper.PopulateList<KPIEntity>(cbKPI.Properties.Items, _kpiList);
            DataConvertorHelper.PopulateList<GlobalLookupEntity>(cbProximity.Properties.Items, _proximityList);
            DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);
            DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);

            cbSKU.Tag = typeof(ProductCombineEntity);
            cbSettlement.Tag = typeof(SettlementEntity);
            cbOutletGroup.Tag = typeof(OutletGroupEntity);
            cbOutletType.Tag = typeof(OutletTypeEntity);
            cbOutletSubtype.Tag = typeof(OutletSubTypeEntity);
            cbKPI.Tag = typeof(KPIEntity);
            cbProximity.Tag = typeof(GlobalLookupEntity);
            cbSupervisor.Tag = typeof(SupervisorEntity);
            cbDSM.Tag = typeof(DSMEntity);
        }

        private void LoadData()
        {
            _settlementList = new SettlementDataProvider().GetList("SettlementIdColumn > 0");

            _outletGroupList = new OutletGroupsDataProvider().GetList("OLGroupIdColumn > 0");

            _outletTypeList = new OutletTypesDataProvider().GetList(OutletTypeBaseFilter);

            _outletSubTypeList = new OutletSubTypesDataProvider().GetList(OutletSubTypeBaseFilter);

            _kpiList = new KPIDataProvider().GetList();

            _proximityList = new GlobalLookupDataProvider().GetList(GlobalLookupProximityBaseFilter);

            _supervisorList = new SupervisorsDataProvider().GetList(SupervisorBaseFilter);

            _dsmList = new DSMDataProvider().GetList();
            //cbDSM_SelectedIndexChanged(null, null);

            _productsList = new ProductCombineDataProvider().GetList();
        }

        private void SetupUI()
        {
            chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = false;
            switch (AuthenticationManager.Instance.CurrentUserLevel)
            {
                case 5:
                    chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = true;
                    break;
            }

            if (DataAccessProvider.HasUserPosition(DataAccessProvider.PositionCoPpmM5)) {
                chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = true;
            }
        }

        private void InitEvents()
        {
            btnOk.Click += new EventHandler(btnOk_Click);

            chkDateStartPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);
            chkDateEndPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);

            cbOutletGroup.SelectedIndexChanged += new EventHandler(cbOutletGroup_SelectedIndexChanged);
            cbOutletType.SelectedIndexChanged += new EventHandler(cbOutletType_SelectedIndexChanged);
            cbDSM.SelectedIndexChanged += new EventHandler(cbDSM_SelectedIndexChanged);

            cbKPI.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbDSM.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbSupervisor.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbSettlement.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbProximity.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbSKU.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbOutletType.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbOutletSubtype.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
            cbOutletGroup.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
        }

        private string GetValue<T>(List<T> values, int id)
        {
            if (id < 0)
                return Resource.ComboBoxItemAnyValueText;

            T searchResult = values.Find(delegate(T item) { return (item as IBaseEntity).Id == id; });

            return searchResult != null ? (searchResult as IBaseEntity).Name : string.Empty;
        }

        private int GetIndex<T>(List<T> values, int id)
        {
            int result = -1;
            int index = 0;
            foreach (T item in values)
            {
                if ((item as IBaseEntity).Id == id)
                {
                    result = index + 1;
                    break;
                }
                index++;
            }

            return result;
        }

        private void UpdateEditableControls()
        {
            deStartPeriod.Properties.ReadOnly = chkDateStartPeriodApproved.Checked;
            deEndPeriod.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;

            chkDateStartPeriodApproved.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;
            chkDateEndPeriodApproved.Properties.ReadOnly = !chkDateStartPeriodApproved.Checked;

            bool readOnly = chkDateStartPeriodApproved.Checked || chkDateEndPeriodApproved.Checked;

            teKPIValue.Properties.ReadOnly = readOnly;

            cbKPI.Properties.ReadOnly = readOnly;

            cbDSM.Properties.ReadOnly = readOnly;
            cbSupervisor.Properties.ReadOnly = readOnly;

            cbSettlement.Properties.ReadOnly = readOnly;
            cbProximity.Properties.ReadOnly = readOnly;

            cbOutletGroup.Properties.ReadOnly = readOnly;
            cbOutletType.Properties.ReadOnly = readOnly;
            cbOutletSubtype.Properties.ReadOnly = readOnly;
        }

        private bool ContainsOnlyBaseData()
        {
            return cbDSM.SelectedIndex <= 0 && cbSupervisor.SelectedIndex <= 0
                && cbSettlement.SelectedIndex <= 0 && cbProximity.SelectedIndex <= 0
                && cbOutletGroup.SelectedIndex <= 0 && cbOutletType.SelectedIndex <= 0 && cbOutletSubtype.SelectedIndex <= 0;
        }

        private bool HasDateBeenSet(DateTime date)
        {
            return date.Year > 1900;
        }

        private bool IsValidated()
        {
            bool isValidData = false;
            bool lHasPositionCoPpmM5 = DataAccessProvider.HasUserPosition(DataAccessProvider.PositionCoPpmM5);

            DateTime now = DateTime.Now.Date;
            CascadedAimEntity aim = EditedAim;

            if (chkDateStartPeriodApproved.Enabled)
            {
                if (!HasDateBeenSet(deStartPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
                {
                    deStartPeriod.Focus();
                    MessageBox.Show(Resource.PeriodStartDateIsNotSet);
                    return isValidData;
                }

                if (deStartPeriod.DateTime.Date < now && !lHasPositionCoPpmM5)
                {
                    deStartPeriod.Focus();
                    MessageBox.Show(Resource.PeriodStartDateLessThenCurrentDate);
                    return isValidData;
                }
            }

            if (!HasDateBeenSet(deEndPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateIsNotSet);
                return isValidData;
            }

            if (deEndPeriod.DateTime.Date < now && !lHasPositionCoPpmM5)
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateLessThenCurrentDate);
                return isValidData;
            }

            if (!DateHelper.ValidatePeriod(deStartPeriod.DateTime, deEndPeriod.DateTime))
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.IncorrectFromToDates);
                return isValidData;
            }

            if (!_oldEditedAim.IsStartDateApproved && chkDateStartPeriodApproved.Checked
                && deStartPeriod.DateTime != DateTime.MinValue && deStartPeriod.DateTime.Date < now
                && !lHasPositionCoPpmM5)
            {
                chkDateStartPeriodApproved.Checked = false;
                deStartPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodStartDateLessThanNow);
                return isValidData;
            }

            if (!_oldEditedAim.IsEndDateApproved && chkDateEndPeriodApproved.Checked
                && deEndPeriod.DateTime != DateTime.MinValue && deEndPeriod.DateTime.Date < now
                && !lHasPositionCoPpmM5)
            {
                chkDateEndPeriodApproved.Checked = false;
                deEndPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodEndDateLessThanNow);
                return isValidData;
            }

            if (cbKPI.SelectedIndex <= 0)
            {
                cbKPI.Focus();
                MessageBox.Show(Resource.KPITypeHasNotBeenDefined);
                return isValidData;
            }

            if (ContainsOnlyBaseData() && AuthenticationManager.Instance.CurrentUserLevel < 5)
            {
                cbDSM.Focus();
                MessageBox.Show(Resource.CurrentUserHasNoRightsToCreateBaseAims);
                return isValidData;
            }

            List<string> errors = new List<string>();

            DataMediator.CheckForCrossedAimsErrors(aim, errors);

            DataMediator.CheckForSameAimsErrors(aim, errors);

            if (errors.Count > 0)
            {
                cbKPI.Focus();
                DetailedMessageForm.Show(errors.ToArray());
                return isValidData;
            }

            return true;
        }

        #endregion Private methods

        #region Event Handlers

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (IsValidated())
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void cbOutletGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletGroup.SelectedItem != null)
            {
                OutletGroupEntity currentOutletGroup = cbOutletGroup.SelectedItem as OutletGroupEntity;
                if (currentOutletGroup != null)
                {
                    cbOutletType.Properties.Items.BeginUpdate();

                    _outletTypeList.Clear();
                    string filter = string.Format("{0} AND {1} {2}", OutletTypeBaseFilter, OutletTypeGroupIdFilter, currentOutletGroup.Id.ToString());
                    _outletTypeList = new OutletTypesDataProvider().GetList(filter);
                    DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);

                    cbOutletType.Properties.Items.EndUpdate();

                    cbOutletType.SelectedIndex = -1;
                }
            }
        }

        private void cbOutletType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletType.SelectedItem != null)
            {
                OutletTypeEntity currentOutletType = cbOutletType.SelectedItem as OutletTypeEntity;
                if (currentOutletType != null)
                {
                    cbOutletSubtype.Properties.Items.BeginUpdate();

                    _outletSubTypeList.Clear();
                    string filter = string.Format("{0} AND {1} {2}", OutletSubTypeBaseFilter, OutletSubTypeTypeIdFilter, currentOutletType.Id.ToString());
                    _outletSubTypeList = new OutletSubTypesDataProvider().GetList(filter);
                    DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);

                    cbOutletSubtype.Properties.Items.EndUpdate();

                    cbOutletSubtype.SelectedIndex = -1;
                }
            }
        }

        private void cbDSM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDSM.SelectedItem != null)
            {                    
                    cbSupervisor.Properties.Items.BeginUpdate();

                    if (_supervisorList != null)
                        _supervisorList.Clear();

                    string filter = string.Empty;

                    if (cbDSM.SelectedItem is DSMEntity)
                    {
                        DSMEntity currentDSM = (DSMEntity)cbDSM.SelectedItem;
                        filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorDSMId, currentDSM.Id);
                    }
                    else
                    {
                        if (cbDSM.SelectedText.Contains(Resource.ComboBoxItemAnyValueText))
                        {
                            if (_oldEditedAim.RegionId > 0)
                            {
                                filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorRegionId, _oldEditedAim.RegionId);
                            }
                            else
                            {
                                filter = SupervisorBaseFilter;
                            }
                        }
                        else
                        {
                            filter = SupervisorBaseFilter;
                        }
                    }

                    _supervisorList = new SupervisorsDataProvider().GetList(filter);
                    DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);

                    cbSupervisor.Properties.Items.EndUpdate();

                    cbSupervisor.SelectedIndex = -1;                
               }
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxEdit combobox = sender as ComboBoxEdit;
            if (combobox.SelectedItem != null)
            {
                if (combobox.Tag == typeof(KPIEntity))
                {
                    _editedAim.KPITypeName = DataConvertorHelper.GetNameFromComboboxSelectedItem<KPIEntity>
                        (combobox.SelectedIndex, combobox.SelectedItem);
                    _editedAim.KPITypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<KPIEntity>
                        (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(DSMEntity))
                {
                    _editedAim.DSMId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<DSMEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(SupervisorEntity))
                {
                    _editedAim.SupervisorId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SupervisorEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(SettlementEntity))
                {
                    _editedAim.SettlementId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SettlementEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(GlobalLookupEntity))
                {
                    _editedAim.ProximityId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<GlobalLookupEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(ProductCombineEntity))
                {
                    _editedAim.ProductCnId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<ProductCombineEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(OutletTypeEntity))
                {
                    _editedAim.OlTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletTypeEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(OutletSubTypeEntity))
                {
                    _editedAim.OlSubTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletSubTypeEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }
                else if (combobox.Tag == typeof(OutletGroupEntity))
                {
                    _editedAim.OlGroupId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletGroupEntity>
                       (combobox.SelectedIndex, combobox.SelectedItem);
                }

            }
        }

        private void chkDatePeriodApproved_CheckedChanged(object sender, EventArgs e)
        {
            UpdateEditableControls();
        }

        #endregion Event Handlers
    }

}