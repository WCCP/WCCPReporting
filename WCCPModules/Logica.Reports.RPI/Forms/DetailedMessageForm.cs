using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Logica.Reports.RPI.Forms
{
    /// <summary>
    /// Custom dialog form with long message text place into multiline TextBox control
    /// </summary>
    public partial class DetailedMessageForm : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors
        
        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public DetailedMessageForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor with parameter which contains long message text
        /// </summary>
        /// <param name="message">Long message</param>
        public DetailedMessageForm(string[] message)
            : this()
        {
            meErrorText.Lines = message;
        }

        /// <summary>
        /// Constructor with long message text and caption p[arameters
        /// </summary>
        /// <param name="message">Long message</param>
        /// <param name="caption">Form Caption</param>
        public DetailedMessageForm(string[] message, string caption)
            : this()
        {
            this.Text = caption;
            meErrorText.Lines = message;
        }

        #endregion Constructors

        #region Public methods

        /// <summary>
        /// Show dialog form
        /// </summary>
        /// <param name="message">long message text</param>
        /// <returns>Dialog result</returns>
        public static DialogResult Show(string[] message)
        {
            return new DetailedMessageForm(message).ShowDialog();
        }

        /// <summary>
        /// Show dialog form
        /// </summary>
        /// <param name="message">long message text</param>
        /// <param name="caption">Form Caption</param>
        /// <returns>>Dialog result</returns>
        public static DialogResult Show(string[] message, string caption)
        {
            return new DetailedMessageForm(message, caption).ShowDialog();
        }

        #endregion Public methods
    }

}