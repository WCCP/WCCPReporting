using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Logica.Reports.RPI.Forms
{
    /// <summary>
    /// Custom dialog form
    /// </summary>
    public partial class ConfirmationDialogForm : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public ConfirmationDialogForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construction with parameters
        /// </summary>
        /// <param name="message">Dialog messagae</param>
        /// <param name="caption">Dialog caption</param>
        /// <param name="buttonOkText">Button Ok text</param>
        /// <param name="buttonCancelText">Button Cancel text</param>
        public ConfirmationDialogForm(string message, string caption, string buttonOkText, string buttonCancelText)
            : this()
        {
            this.Text = caption;
            lblMessage.Text = message;
            btnOk.Text = buttonOkText;
            btnCancel.Text = buttonCancelText;
        }

        #endregion Constructors

        #region Public methods

        /// <summary>
        /// Show dialog form
        /// </summary>
        /// <param name="message">Dialog message</param>
        /// <param name="caption">Dialog Caption</param>
        /// <param name="buttonOkText">Button Ok text</param>
        /// <param name="buttonCancelText">button Cancel text</param>
        /// <returns>Dialog Result</returns>
        public static DialogResult Show(string message, string caption, string buttonOkText, string buttonCancelText)
        {
            return new ConfirmationDialogForm(message, caption, buttonOkText, buttonCancelText).ShowDialog();
        }

        #endregion Public methods  

    }
}