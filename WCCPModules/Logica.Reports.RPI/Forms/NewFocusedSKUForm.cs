using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.Authentication;

namespace Logica.Reports.RPI.Forms
{
    /// <summary>
    /// Form of setting parameters for new Focused SKU
    /// </summary>
    public partial class NewFocusedSKUForm : DevExpress.XtraEditors.XtraForm
    {
        #region Private members

        CascadedAimEntity _editedAim;

        private IList<ProductCombineKPI> _productCombineList = new List<ProductCombineKPI>();
        private IList<SettlementEntity> _settlementList;
        private IList<OutletGroupEntity> _outletGroupList;
        private IList<OutletTypeEntity> _outletTypeList;
        private IList<OutletSubTypeEntity> _outletSubTypeList;
        private IList<GlobalLookupEntity> _proximityList;
        private IList<SupervisorEntity> _supervisorList;
        private IList<DSMEntity> _dsmList;
        private IList<ProductCombineEntity> _productsList;

        private DateTime _dateStartPeriod;
        private DateTime _dateEndPeriod;
        private bool _isDateStartPeriodApproved;
        private bool _isDateEndPeriodApproved;
        private int _DSMId;
        private int _supervisorId;
        private int _proximityId;
        private int _settlementId;

        private int _regionId;
        private int _districtId;
        private int _cityId;

        private const string OutletTypeBaseFilter = @"OLTypeIdColumn > 0";
        private const string OutletTypeGroupIdFilter = @"OLGroupIdColumn = ";
        private const string OutletSubTypeBaseFilter = @"OlSubTypeIdColumn > 0";
        private const string OutletSubTypeTypeIdFilter = @"OLTypeIdColumn = ";
        private const string GlobalLookupProximityBaseFilter = @"TableNameColumn = 'tblOutLets' AND FieldNameColumn = 'Proximity' ";
        private const string SupervisorBaseFilter = @"SupervisorIdColumn > 0";

        #endregion Private members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public NewFocusedSKUForm()
        {
            InitializeComponent();
            Localize();

            LoadData();
            SetupControls();

            InitEvents();

            SetupUI();
        }

        #endregion Constructors

        #region Public properties

        /// <summary>
        /// Region ID
        /// </summary>
        public int RegionId
        {
            get
            {
                return _regionId;
            }
            set
            {
                teRegionName.Text = DataMediator.GetRegionName(value);

                _dsmList = DataMediator.GetDSMList(value);
                DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);

                _supervisorList = DataMediator.GetSupervisorList(value);
                DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);

                cbDSM_SelectedIndexChanged(null, null);

                if (_regionId == value)
                    return;
                _regionId = value;
            }
        }

        /// <summary>
        /// District ID
        /// </summary>
        public int DistrictId
        {
            get
            {
                return _districtId;
            }
            set
            {
                if (_districtId == value)
                    return;
                _districtId = value;
            }
        }

        /// <summary>
        /// City ID
        /// </summary>
        public int CityId
        {
            get
            {
                return _cityId;
            }
            set
            {
                if (_cityId == value)
                    return;
                _cityId = value;
            }
        }

        /// <summary>
        /// Created Focused SKU
        /// </summary>
        public CascadedAimEntity CreatedFocusedSKU
        {
            get
            {
                DateTime now = DateTime.Now;
                CascadedAimEntity aim = new CascadedAimEntity();

                aim.ProductCnId = (int) DataConvertorHelper.GetIdFromComboboxSelectedItem<ProductCombineEntity>(cbSKU.SelectedIndex, cbSKU.SelectedItem);

                aim.DateAdded = now;

                aim.DateStartPeriod = deStartPeriod.DateTime;
                aim.IsStartDateApproved = chkDateStartPeriodApproved.Checked;
                aim.DateApproveStartPeriod = chkDateStartPeriodApproved.Checked ? now : DateTime.MinValue;

                aim.DateEndPeriod = deEndPeriod.DateTime;
                aim.IsEndDateApproved = chkDateEndPeriodApproved.Checked;
                aim.DateApproveEndPeriod = chkDateEndPeriodApproved.Checked ? now : DateTime.MinValue; ;

                aim.CreatedUserId = AuthenticationManager.Instance.CurrentUserId;
                aim.ApprovedUserId = AuthenticationManager.Instance.CurrentUserId;
                aim.RegionId = _regionId;
                aim.DistrictId = _districtId;
                aim.CityId = _cityId;

                aim.DSMId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<DSMEntity>(cbDSM.SelectedIndex, cbDSM.SelectedItem);
                aim.SupervisorId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SupervisorEntity>(cbSupervisor.SelectedIndex, cbSupervisor.SelectedItem);
                aim.SettlementId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<SettlementEntity>(cbSettlement.SelectedIndex, cbSettlement.SelectedItem);
                aim.ProximityId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<GlobalLookupEntity>(cbProximity.SelectedIndex, cbProximity.SelectedItem);
                aim.IsFocused = chkIsFocused.Checked;
                aim.OlTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletTypeEntity>(cbOutletType.SelectedIndex, cbOutletType.SelectedItem);
                aim.OlSubTypeId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletSubTypeEntity>(cbOutletSubtype.SelectedIndex, cbOutletSubtype.SelectedItem);
                aim.OlGroupId = (int)DataConvertorHelper.GetIdFromComboboxSelectedItem<OutletGroupEntity>(cbOutletGroup.SelectedIndex, cbOutletGroup.SelectedItem);
                aim.KPITypeId = 1;
                aim.DeviationPercent = 0f;
                aim.ProductCombineName = DataConvertorHelper.GetNameFromComboboxSelectedItem<ProductCombineEntity>(cbSKU.SelectedIndex, cbSKU.SelectedItem);
                aim.ProductCombineDLM = now;

                return aim;
            }
        }

        #endregion Public properties

        #region Public methods

        /// <summary>
        /// Initialize form controls
        /// </summary>
        /// <param name="aim">Focused SKU</param>
        public void Init(CascadedAimEntity aim)
        {
            _editedAim = aim.Clone() as CascadedAimEntity;

            if (_editedAim.DateStartPeriod == DateTime.MinValue)
                deStartPeriod.Text = "";
            else
                deStartPeriod.DateTime = _editedAim.DateStartPeriod;

            chkDateStartPeriodApproved.Checked = _editedAim.IsStartDateApproved;

            if (_editedAim.DateEndPeriod == DateTime.MinValue)
                deEndPeriod.Text = "";
            else
                deEndPeriod.DateTime = _editedAim.DateEndPeriod;

            chkDateEndPeriodApproved.Checked = _editedAim.IsEndDateApproved;
            chkIsFocused.Checked = aim.IsFocused;

            teRegionName.Text = DataMediator.GetRegionName(_editedAim.RegionId);

            cbSKU.Text = _editedAim.ProductCombineName;
            int itemIndex = GetIndex<ProductCombineEntity>(new List<ProductCombineEntity>(_productsList), _editedAim.ProductCnId);
            cbSKU.SelectedIndex = itemIndex == -1 ? 0 : itemIndex; 

            _dsmList = DataMediator.GetDSMList(_editedAim.RegionId);
            itemIndex = GetIndex<DSMEntity>(new List<DSMEntity>(_dsmList), _editedAim.DSMId);
            cbDSM.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;  
            cbDSM_SelectedIndexChanged(null, null);

            if (_supervisorList != null)
            {
                itemIndex = GetIndex<SupervisorEntity>(new List<SupervisorEntity>(_supervisorList), _editedAim.SupervisorId);
                cbSupervisor.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            }
            else
            {
                DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);
            }
            
            itemIndex = GetIndex<GlobalLookupEntity>(new List<GlobalLookupEntity>(_proximityList), _editedAim.ProximityId);
            cbProximity.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;  
            
            itemIndex = GetIndex<SettlementEntity>(new List<SettlementEntity>(_settlementList), _editedAim.SettlementId);
            cbSettlement.SelectedIndex = itemIndex == -1 ? 0 : itemIndex ;  

            itemIndex = GetIndex<OutletGroupEntity>(new List<OutletGroupEntity>(_outletGroupList), _editedAim.OlGroupId < 0 ? 0 : _editedAim.OlGroupId);
            cbOutletGroup.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            cbOutletGroup_SelectedIndexChanged(null, null);

            itemIndex = GetIndex<OutletTypeEntity>(new List<OutletTypeEntity>(_outletTypeList), _editedAim.OlTypeId < 0 ? 0 : _editedAim.OlTypeId);
            cbOutletType.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;
            cbOutletType_SelectedIndexChanged(null, null);

            itemIndex = GetIndex<OutletSubTypeEntity>(new List<OutletSubTypeEntity>(_outletSubTypeList), _editedAim.OlSubTypeId < 0 ? 0 : _editedAim.OlSubTypeId);
            cbOutletSubtype.SelectedIndex = itemIndex == -1 ? 0 : itemIndex;

        }

        /// <summary>
        /// Localize form
        /// </summary>
        /// <param name="formCaption">Form Caption</param>
        /// <param name="btnOkText">Button Ok text</param>
        /// <param name="btnCancelText">Button Cancel Text</param>
        public void Localize(string formCaption, string btnOkText, string btnCancelText)
        {
            this.Text = formCaption;
            btnOk.Text = btnOkText;
            btnCancel.Text = btnCancelText;
        }

        #endregion Public methods

        #region Event Handlers

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (IsValidated())
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void cbOutletGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletGroup.SelectedItem != null && cbOutletGroup.SelectedItem is OutletGroupEntity)
            {
                OutletGroupEntity currentOutletGroup = cbOutletGroup.SelectedItem as OutletGroupEntity;

                cbOutletType.Properties.Items.BeginUpdate();

                _outletTypeList.Clear();
                string filter = string.Format("{0} AND {1} {2}", OutletTypeBaseFilter, OutletTypeGroupIdFilter, currentOutletGroup.Id.ToString());
                _outletTypeList = new OutletTypesDataProvider().GetList(filter);
                DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);

                cbOutletType.Properties.Items.EndUpdate();

                cbOutletType.SelectedIndex = -1;
            }
        }

        private void cbOutletType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutletType.SelectedItem != null && cbOutletType.SelectedItem is OutletTypeEntity)
            {
                OutletTypeEntity currentOutletType = cbOutletType.SelectedItem as OutletTypeEntity;

                cbOutletSubtype.Properties.Items.BeginUpdate();

                _outletSubTypeList.Clear();
                string filter = string.Format("{0} AND {1} {2}", OutletSubTypeBaseFilter, OutletSubTypeTypeIdFilter, currentOutletType.Id.ToString());
                _outletSubTypeList = new OutletSubTypesDataProvider().GetList(filter);
                DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);

                cbOutletSubtype.Properties.Items.EndUpdate();

                cbOutletSubtype.SelectedIndex = -1;
            }
        }

        private void cbDSM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDSM.SelectedItem != null)
            {
                cbSupervisor.Properties.Items.BeginUpdate();

                if (_supervisorList != null)
                    _supervisorList.Clear();

                string filter = string.Empty;

                if (cbDSM.SelectedItem is DSMEntity)
                {
                    DSMEntity currentDSM = (DSMEntity)cbDSM.SelectedItem;
                    filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorDSMId, currentDSM.Id);
                }
                else
                {
                    if (cbDSM.SelectedText.Contains(Resource.ComboBoxItemAnyValueText))
                    {
                        if (_regionId > 0)
                        {
                            filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.SupervisorRegionId, _regionId);
                        }
                        else
                        {
                            filter = SupervisorBaseFilter;
                        }
                    }
                    else
                    {
                        filter = SupervisorBaseFilter;
                    }
                }

                _supervisorList = new SupervisorsDataProvider().GetList(filter);
                DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);

                cbSupervisor.Properties.Items.EndUpdate();

                cbSupervisor.SelectedIndex = -1;
            }
        }

        private void chkDatePeriodApproved_CheckedChanged(object sender, EventArgs e)
        {
            UpdateEditableControls();
        }

        #endregion Event Handlers

        #region Private methods

        private void Localize()
        {
            this.Text = Resource.NewFocusedSKUFormCaption;
            btnOk.Text = Resource.NewAimFormButtonOk;
            btnCancel.Text = Resource.NewAimFormButtonCancel;
        }

        private void SetupControls()
        {
            DataConvertorHelper.PopulateList<ProductCombineEntity>(cbSKU.Properties.Items, _productsList);
            DataConvertorHelper.PopulateList<SettlementEntity>(cbSettlement.Properties.Items, _settlementList);
            DataConvertorHelper.PopulateList<OutletGroupEntity>(cbOutletGroup.Properties.Items, _outletGroupList);
            DataConvertorHelper.PopulateList<OutletTypeEntity>(cbOutletType.Properties.Items, _outletTypeList);
            DataConvertorHelper.PopulateList<OutletSubTypeEntity>(cbOutletSubtype.Properties.Items, _outletSubTypeList);
            DataConvertorHelper.PopulateList<GlobalLookupEntity>(cbProximity.Properties.Items, _proximityList);
            DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);
            DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);

            cbSKU.SelectedIndex = 0;

            cbDSM.SelectedIndex = 0;
            cbSupervisor.SelectedIndex = 0;
            cbSettlement.SelectedIndex = 0;
            cbProximity.SelectedIndex = 0;
            cbOutletGroup.SelectedIndex = 0;
            cbOutletType.SelectedIndex = 0;
            cbOutletSubtype.SelectedIndex = 0;

            deStartPeriod.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            deEndPeriod.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        private void SetupUI()
        {
            chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = false;
            switch (AuthenticationManager.Instance.CurrentUserLevel)
            {
                case 5:
                    chkDateStartPeriodApproved.Enabled = chkDateEndPeriodApproved.Enabled = true;
                    break;
            }
        }

        private void LoadData()
        {
            _settlementList = new SettlementDataProvider().GetList("SettlementIdColumn > 0");

            _outletGroupList = new OutletGroupsDataProvider().GetList("OLGroupIdColumn > 0");

            _outletTypeList = new OutletTypesDataProvider().GetList(OutletTypeBaseFilter);

            _outletSubTypeList = new OutletSubTypesDataProvider().GetList(OutletSubTypeBaseFilter);

            _proximityList = new GlobalLookupDataProvider().GetList(GlobalLookupProximityBaseFilter);

            _supervisorList = new SupervisorsDataProvider().GetList(SupervisorBaseFilter);

            _dsmList = new DSMDataProvider().GetList();

            _productsList = new ProductCombineDataProvider().GetList();
        }

        private void InitEvents()
        {
            chkDateStartPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);
            chkDateEndPeriodApproved.CheckedChanged += new EventHandler(chkDatePeriodApproved_CheckedChanged);

            cbOutletGroup.SelectedIndexChanged += new EventHandler(cbOutletGroup_SelectedIndexChanged);
            cbOutletType.SelectedIndexChanged += new EventHandler(cbOutletType_SelectedIndexChanged);
            cbDSM.SelectedIndexChanged += new EventHandler(cbDSM_SelectedIndexChanged);

            btnOk.Click += new EventHandler(btnOk_Click);
        }

        private string GetValue<T>(List<T> values, int id)
        {
            if (id < 0)
            {
                return Resource.ComboBoxItemAnyValueText;
            }
            else
            {
                T searchResult = values.Find(delegate(T item) { return (item as IBaseEntity).Id == id; });

                return searchResult != null ? (searchResult as IBaseEntity).Name : string.Empty;
            }
        }

        private int GetIndex<T>(List<T> values, int id)
        {
            int result = -1;
            int index = 0;
            foreach (T item in values)
            {
                if ((item as IBaseEntity).Id == id)
                {
                    result = index + 1;
                    break;
                }
                index++;
            }

            return result;
        }

        private void UpdateEditableControls()
        {
            deStartPeriod.Properties.ReadOnly = chkDateStartPeriodApproved.Checked;
            deEndPeriod.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;

            chkDateStartPeriodApproved.Properties.ReadOnly = chkDateEndPeriodApproved.Checked;
            chkDateEndPeriodApproved.Properties.ReadOnly = !chkDateStartPeriodApproved.Checked;

            bool readOnly = chkDateStartPeriodApproved.Checked || chkDateEndPeriodApproved.Checked;

            cbSKU.Properties.ReadOnly = readOnly;

            chkIsFocused.Properties.ReadOnly = readOnly;

            cbDSM.Properties.ReadOnly = readOnly;
            cbSupervisor.Properties.ReadOnly = readOnly;

            cbSettlement.Properties.ReadOnly = readOnly;
            cbProximity.Properties.ReadOnly = readOnly;

            cbOutletGroup.Properties.ReadOnly = readOnly;
            cbOutletType.Properties.ReadOnly = readOnly;
            cbOutletSubtype.Properties.ReadOnly = readOnly;
        }

        private bool HasDateBeenSet(DateTime date)
        {
            return date.Year > 1900;
        }

        private bool IsValidated()
        {
            bool isValidData = false;

            DateTime now = DateTime.Now;

            if (!HasDateBeenSet(deStartPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.PeriodStartDateIsNotSet);
                return isValidData;
            }

            if (DateHelper.IsYearMonthDayLess(deStartPeriod.DateTime, now))
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.PeriodStartDateLessThenCurrentDate);
                return isValidData;
            }

            if (!HasDateBeenSet(deEndPeriod.DateTime) || deStartPeriod.Text.Trim().Length == 0)
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateIsNotSet);
                return isValidData;
            }

            if (DateHelper.IsYearMonthDayLess(deEndPeriod.DateTime, now))
            {
                deEndPeriod.Focus();
                MessageBox.Show(Resource.PeriodEndDateLessThenCurrentDate);
                return isValidData;
            }

            if (!DateHelper.ValidatePeriod(deStartPeriod.DateTime, deEndPeriod.DateTime))
            {
                deStartPeriod.Focus();
                MessageBox.Show(Resource.IncorrectFromToDates);
                return isValidData;
            }

            if (chkDateStartPeriodApproved.Checked && deStartPeriod.DateTime != DateTime.MinValue && deStartPeriod.DateTime < now)
            {
                chkDateStartPeriodApproved.Checked = false;
                deStartPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodStartDateLessThanNow);
                return isValidData;
            }

            if (chkDateEndPeriodApproved.Checked && deEndPeriod.DateTime != DateTime.MinValue && deEndPeriod.DateTime < now)
            {
                chkDateEndPeriodApproved.Checked = false;
                deEndPeriod.Focus();
                MessageBox.Show(Resource.CannotApprovePeriodEndDateLessThanNow);
                return isValidData;
            }

            List<string> errors = new List<string>();
            CascadedAimEntity aim = CreatedFocusedSKU;

            AimsDataSet aims = new CascadedAimsDataProvider().GetCrossedAims(aim);
            DataView view = aims.Aim.DefaultView;
            view.RowFilter = string.Format("{0} = {1}", MappedNames.IsFocused, true.ToString());
            if (view.Count > 0)
            {
                errors.Add(
                        string.Format(
                        "�������� ��� <{0}> � �������� �������� � <{1}> �� <{2}> ��������� �",
                        aim.ProductCombineName,
                        DateHelper.PeriodBorderToString(aim.DateStartPeriod),
                        DateHelper.PeriodBorderToString(aim.DateEndPeriod)
                        )
                    );
                foreach (DataRowView rowView in view)
                {
                    DateTime endDate = DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateEndPeriod]);

                    errors.Add(
                        string.Format(
                            "   ��� <{0}> � �������� � <{1}> �� <{2}>",
                            rowView[MappedNames.ProductCombineName],
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateStartPeriod])),
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateEndPeriod]))
                        )
                        );
                }
            }

            if (errors.Count > 0)
            {
                deStartPeriod.Focus();
                DetailedMessageForm.Show(errors.ToArray());
                return isValidData;
            } 
            
            return true;
        }

        #endregion Private methods

    }

}