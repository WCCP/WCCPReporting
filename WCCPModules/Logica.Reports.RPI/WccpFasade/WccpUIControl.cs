using DevExpress.XtraTab;
using Logica.Reports.RPI;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace WccpReporting
{

    /// <summary>
    /// Container control for UI elements
    /// </summary>
    public partial class WccpUIControl : DevExpress.XtraEditors.XtraUserControl
    {
        #region Constructors

        public WccpUIControl()
        {
            InitializeComponent();
        }

        #endregion Constructors

        public int ReportInit()
        {
            int result;

            using (ReportSettings reportSettingsForm = new ReportSettings())
            {
                result = ShowSettingsForm(reportSettingsForm);
            }

            if (result == 0)
            {
                RefreshTabs();
            }

            return result;
        }

        #region Private methods

        private int ShowSettingsForm(ReportSettings reportSettingsForm)
        {
            reportSettingsForm.DateStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            reportSettingsForm.DateEnd = DateTime.Now;

            if (reportSettingsForm.ShowDialog() == DialogResult.OK)
            {
                ReportOptions.DateStart = reportSettingsForm.DateStart;
                ReportOptions.DateEnd = reportSettingsForm.DateEnd;
                ReportOptions._dsm = reportSettingsForm._dsm;
                ReportOptions._supervisor = reportSettingsForm._supervisor;
                ReportOptions._merchandiser = reportSettingsForm._merchandiser;
                ReportOptions._route = reportSettingsForm._route;

                if (reportSettingsForm._route != null)
                    ReportOptions.RouteId = reportSettingsForm._route.Id;
                else
                    ReportOptions.RouteId = -1;

                ReportOptions.CheckedReportsAndModules = reportSettingsForm.CheckedReportsAndModules;

                return 0;
            }

            return 1;
        }

        private void RefreshTabs()
        {
            IRPIModule moduleContainer;
            foreach (Type type in ReportOptions.CheckedReportsAndModules)
            {
                moduleContainer = null;

                if (tcRPIModules.TabPages != null)
                {
                    foreach (XtraTabPage tabPage in tcRPIModules.TabPages)
                    {
                        if (tabPage.Tag != null)
                        {
                            if (tabPage.Tag.GetType().Equals(type))
                            {
                                moduleContainer = tabPage.Tag as IRPIModule;
                                break;
                            }
                        }
                    }
                }

                if (moduleContainer == null)
                {
                    XtraTabPage tabPage = new XtraTabPage();

                    moduleContainer =
                        Assembly.GetExecutingAssembly().CreateInstance(type.FullName) as IRPIModule;

                    this.SuspendLayout();

                    (moduleContainer as Control).Dock = DockStyle.Fill;
                    tabPage.Controls.Add(moduleContainer as Control);
                    tabPage.Tag = moduleContainer;
                    tabPage.Text = LocalizationProvider.GetText(type.Name);
                    tcRPIModules.TabPages.Add(tabPage);

                    this.ResumeLayout(false);
                    this.PerformLayout();
                }

                moduleContainer.ReportStartDate = ReportOptions.DateStart;
                moduleContainer.ReportEndDate = ReportOptions.DateEnd;
                moduleContainer.DSM = ReportOptions._dsm;
                moduleContainer.Supervisor = ReportOptions._supervisor;
                moduleContainer.Merchandiser = ReportOptions._merchandiser;
                moduleContainer.Route = ReportOptions._route;
            }

            for (int i = tcRPIModules.TabPages.Count - 1; i >= 0; i--)
            {
                if (!ReportOptions.CheckedReportsAndModules.Contains(tcRPIModules.TabPages[i].Tag.GetType()))
                {
                    tcRPIModules.TabPages.Remove(tcRPIModules.TabPages[i]);
                }
            }
        }

        #endregion Private methods
    }
}
