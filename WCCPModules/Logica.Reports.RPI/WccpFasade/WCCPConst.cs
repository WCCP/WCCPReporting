using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.RPI;
using Logica.Reports.RPI.DataProvider.Providers;

namespace WccpReporting
{
    static class ReportOptions
    {        
        static public string ReportTitle = String.Empty;
        static public string ReportLongTitle = String.Empty;
        static public bool ReportSettingsFormIsOK = false;

        static public DateTime DateStart;
        static public DateTime DateEnd;
        static public DSMEntity _dsm;
        static public SupervisorEntity _supervisor;
        static public MerchandiserEntity _merchandiser;
        static public RouteEntity _route;
        static public long RouteId;
        static public IList<Type> CheckedReportsAndModules;
    };

    static class WCCPConst
    {
        static public string root = String.Empty;
        static public bool   IsSkin = false;
        static public string ConnectionString = String.Empty;
        static public int    CommandTimeout = 300;
        static public string Version = "2.0.2.0";
        static public string msgNoHelp = "� ������ ������ ��������� ������� �����������";
        static public string msgErrorCloseConnection = "�� ���� ������� ����������";
        static public string msgErrorSaveOptions = "�� ���� ��������� �����";
        static public string msgErrorExecProc = "�� ���� ��������� ���������";
        static public string msgErrorAddRecord = "�� ���� ������� ������";
        static public string msgErrorEditRecord = "�� ���� �������� ������";
        static public string msgQuestionDeleteItem  = "������� ������ ����� �������. \n������� ������� ������ ?";
        static public string msgQuestionDeleteAllItem = "������ ����� �������. \n������� ������ ?";
    }

}
