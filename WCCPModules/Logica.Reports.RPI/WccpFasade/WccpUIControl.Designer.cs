namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcRPIModules = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this.tcRPIModules)).BeginInit();
            this.SuspendLayout();
            // 
            // tcRPIModules
            // 
            this.tcRPIModules.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcRPIModules.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.tcRPIModules.Appearance.Options.UseBackColor = true;
            this.tcRPIModules.Cursor = System.Windows.Forms.Cursors.Default;
            this.tcRPIModules.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tcRPIModules.Location = new System.Drawing.Point(0, 2);
            this.tcRPIModules.Name = "tcRPIModules";
            this.tcRPIModules.Size = new System.Drawing.Size(396, 331);
            this.tcRPIModules.TabIndex = 0;
            // 
            // WccpUIControl
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tcRPIModules);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(399, 336);
            ((System.ComponentModel.ISupportInitialize)(this.tcRPIModules)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tcRPIModules;
    }

}
