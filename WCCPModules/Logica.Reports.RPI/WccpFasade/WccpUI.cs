﻿using System.ComponentModel.Composition;
using Logica.Reports.Common;
using Logica.Reports.RPI;
using Logica.Reports.RPI.Localization;
using ModularWinApp.Core.Interfaces;
using System;
using System.Windows.Forms;

namespace WccpReporting
{
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccprpireports.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                _reportControl = new WccpUIControl();
                _reportCaption = Resource.RPIReport;
                
                return _reportControl.ReportInit();
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
                return 1;
            }
        }

        public void CloseUI()
        {
            //throw new NotImplementedException();
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }

}

