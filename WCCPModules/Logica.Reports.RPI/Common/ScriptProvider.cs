﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.RPI.Common
{
    public static class ScriptProvider
    {
        public static string RegionsCountries
        {
            get
            {
                return @"dbo.spDW_RPI_CountryGetList";
            }
        }

        public static string RegionsRegions
        {
            get
            {
                return "dbo.spDW_RPI_RegionsGetList";
            }
        }

        public static string RegionsDistricts
        {
            get
            {
                return @"dbo.spDW_RPI_DistrictsGetList";
            }
        }

        public static string RegionsCities
        {
            get
            {
                return @"dbo.spDW_RPI_CitiesGetList";
            }
        }

        public static string CascadedAims
        {
            get
            {
                return @"dbo.spDW_RPI_AimGetList";
            }
        }

        public static string CascadedAimsUpdate
        {
            get
            {
                return @"dbo.spDW_RPI_AimPut";
            }
        }

        public static string CascadedAimsInsert 
        {
            get
            {
                return @"dbo.spDW_RPI_AimPut";
            }
        }

        public static string CascadedAimsDelete
        {
            get
            {
                return @"dbo.spDW_RPI_AimDel";
            }
        }

        public static string CascadedAimsCrossing
        {
            get
            {
                return @"dbo.spDW_RPI_CrossedAimsGetList";
            }
        }

        public static string CascadedAimsIdentical
        {
            get
            {
                return @"dbo.spDW_RPI_IdenticalAimsGetList";
            }
        }

        public static string CascadedAimsFilteredByFocusedSKU
        {
            get
            {
                return @"dbo.spDW_RPI_FilteredBySKUAimsGetList";
            }
        }

        public static string CurrentUserLevel
        {
            get { return @"SELECT dbo.fnDW_URM_GetUserLevel(SUSER_SNAME())"; }
        }

        public static string CurrentUserId
        {
            get { return @"select [User_ID] from dbo.DW_URM_User where [Login] = suser_sname()"; }
        }

        public static string Settlements
        {
            get
            {
                return @"dbo.spDW_RPI_SettlementGetList";
            }
        }

        public static string OutletGroups
        {
            get
            {
                return @"dbo.spDW_RPI_OutletGroupsGetList";
            }
        }

        public static string OutletTypes
        {
            get
            {
                return @"dbo.spDW_RPI_OutletTypesGetList";
            }
        }

        public static string OutletSubTypes
        {
            get
            {
                return @"dbo.spDW_RPI_OutletSubTypesGetList";
            }

        }

        public static string KPI
        {
            get
            {
                return @"dbo.spDW_RPI_KPIGetList";
            }

        }

        public static string GlobalLookup
        {
            get
            {
                return @"dbo.spDW_RPI_GlobalLookupGetList";
            }

        }

        public static string Supervisors
        {
            get
            {
                return @"dbo.spDW_RPI_SupervisorsGetList";
            }

        }

        public static string DSM
        {
            get
            {
                return @"dbo.spDW_RPI_DSMGetList";
            }
        }

        public static string ProductCombine
        {
            get
            {
                return @"dbo.spDW_RPI_ProductCombineGetList";
            }
        }

        public static string POC
        {
            get { return "dbo.spDW_RPI_OutletsWithProblemGetList"; }
        }

        public static string POCCentral
        {
            get { return "dbo.spDW_RPI_OutletsWithProblemCentralGetList"; }
        }

        public static string POCLocal
        {
            get { return "dbo.spDW_RPI_OutletsWithProblemLocalGetList"; }
        }

        public static string Merchandisers
        {
            get
            {
                return @"SELECT 
                            dbo.tblMerchandisers.Merch_id,
                            dbo.tblMerchandisers.MerchName,
                            dbo.tblMerchandisers.Supervisor_id,
                            dbo.tblMerchandisers.Status
                         FROM
                            dbo.tblMerchandisers
                         ORDER BY
                            dbo.tblMerchandisers.MerchName
                                        ";
            }

        }

        public static string Routes
        {
            get
            {
                return @"SELECT 
                            dbo.tblRoutes.Route_id,
                            dbo.tblRoutes.RouteName,
                            dbo.tblRoutes.Merch_id,
                            dbo.tblRoutes.City_id
                         FROM
                            dbo.tblRoutes
                                        ";
            }
        }

        public static string UniqueRouteNames
        {
            get
            {
                return @"SELECT 
                            ROW_NUMBER() OVER (ORDER BY rt.RouteName) as Route_id,
                            rt.RouteName,
                            -1 as Merch_Id,
                            -1 as City_Id,
                            0 as isOrdered,
                            -1 as Status
                        FROM
                            (SELECT	DISTINCT dbo.tblRoutes.RouteName FROM  dbo.tblRoutes) rt
                                        ";
            }

        }

    }
}
