﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.RPI.Common
{
    public class DateHelper
    {
        /// <summary>
        /// Check period for consistency: DateFrom should be less then DateTo
        /// </summary>
        /// <param name="dateFrom">Start date of period</param>
        /// <param name="dateTo">End date of period</param>
        /// <returns>True if dateFrom less then dateTo otherwise False</returns>
        public static bool ValidatePeriod(DateTime dateFrom, DateTime dateTo)
        {
            if (dateFrom > DateTime.MinValue && dateTo > DateTime.MinValue)
                if (dateFrom > dateTo)
                    return false;

            return true;
        }

        /// <summary>
        /// Check DataTime value from DataRow Column. 
        /// If Column is DBNull it returns DateTime.MinValue otherwise - converts object value to DateTime.
        /// </summary>
        /// <param name="value">DateTime Column from DataRow</param>
        /// <returns>DateTime</returns>
        public static DateTime GetDBNullDateChecked(object value)
        {
            DateTime result = DateTime.MinValue;
            if (!(value is DBNull))
            {
                try
                {
                    result = Convert.ToDateTime(value);
                }
                catch
                {
                    result = DateTime.MinValue;
                }
            }
            return result;                   
        }

        /// <summary>
        /// Converts date of start or end period to string
        /// </summary>
        /// <param name="periodBorderDate">Date start/end of period</param>
        /// <returns>String representation of date</returns>
        public static string PeriodBorderToString(DateTime periodBorderDate)
        {
            return (periodBorderDate == DateTime.MinValue || periodBorderDate.Year < 1900)
                        ? "неограниченный" 
                        : periodBorderDate.ToString();
        }

        /// <summary>
        /// Compares specified dates use only Year, Month, Day date part
        /// </summary>
        /// <param name="checkedDate">Date for check</param>
        /// <param name="currentDate">Base date</param>
        /// <returns>If checked date less then base date - True, otherwise - False </returns>
        public static bool IsYearMonthDayLess(DateTime checkedDate, DateTime baseDate)
        {
            return new DateTime(checkedDate.Year, checkedDate.Month, checkedDate.Day) < new DateTime(baseDate.Year, baseDate.Month, baseDate.Day);
        }
    }
}
