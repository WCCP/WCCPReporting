﻿using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;

namespace Logica.Reports.RPI.Common
{
    interface IRPIModule
    {
        string ModuleName { get; }
        DateTime ReportStartDate { get; set; }
        DateTime ReportEndDate { get; set; }

        DSMEntity DSM{ get; set; }
        SupervisorEntity Supervisor{ get; set; }
        MerchandiserEntity Merchandiser{ get; set; }
        RouteEntity Route{ get; set; }
    }
}
