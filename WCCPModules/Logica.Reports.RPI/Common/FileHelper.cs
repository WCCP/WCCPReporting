﻿using System;
using System.Windows.Forms;

namespace Logica.Reports.RPI.Common
{
    public class FileHelper
    {
        /// <summary>
        /// Select File Path
        /// </summary>
        /// <param name="type"></param>
        /// <returns>Path</returns>
        public static string SelectFilePath(ExportToType type)
        {
            string res = string.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Сохранить";
            sfd.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            sfd.FileName = "Report";
            sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }

            return res;
        }
    }
}
