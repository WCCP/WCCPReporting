﻿using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.RPI.Authentication;

namespace Logica.Reports.RPI.Common
{
    interface IUIConfigurator
    {
        void AdjustUI();
    }

}
