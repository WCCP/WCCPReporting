﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections.Specialized;

namespace Logica.Reports.RPI.Configuration
{
    public sealed class AuthenticationSettings 
    {
        private static int defaultUserLevel = 2;
        private static string defaultUserRole = "PRICING_DIRECTOR";
        public static string UserRole
        {
            get
            {
                string role = defaultUserRole;
                try
                {
                    //TODO:Dev3 obsolete
                    //NameValueCollection appSettings = ConfigurationManager.AppSettings;

                    //foreach (string key in appSettings.AllKeys)
                    //{
                    //    if (key.Contains("userRole"))
                    //        role = appSettings[key];
                    //}
                }
                finally
                {

                }
                return role;
            }
        }

        public static int UserLevel
        {
            get
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                foreach (string key in appSettings.AllKeys)
                {
                    if (key.Contains("userId"))
                        return int.Parse(appSettings[key]);
                }

                return defaultUserLevel;      
            }
        }
    }

}
