﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using DevExpress.LookAndFeel;
using Logica.Reports.RPI.WccpFasade;

namespace Logica.Reports.RPI.Common
{
    public class SettingProvider
    {
        static UserLookAndFeel ulf;
        public static UserLookAndFeel SkinStyle
        {
            get
            {
                if (null == ulf)
                {
                    ulf = UserLookAndFeel.Default;
                    ulf.UseDefaultLookAndFeel = false;
                    ulf.UseWindowsXPTheme = false;
                    ulf.Style = LookAndFeelStyle.Skin;
                    //style views http://www.devexpress.com/Help/?document=winconcepts/customdocument2534.htm
                    ulf.SkinName = "The Asphalt World";
                }
                return ulf;

            }
        }
        /// <summary>
        /// Converts DBConnectionString to SQL connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        public static string ConnectionString
        {
            get
            {
                try
                {
                    DbConnectionStringBuilder initialString = new DbConnectionStringBuilder();
                    DbConnectionStringBuilder resultingString = new DbConnectionStringBuilder();

                    initialString.ConnectionString = WCCPAPI.GetConnectionString();
                    string[] valuableParams = new string[] { "Data Source", "Initial Catalog", "User ID", "Password", "Integrated Security" };
                    foreach (string connectionParam in valuableParams)
                    {
                        if (initialString.ContainsKey(connectionParam))
                        {
                            resultingString.Add(connectionParam, initialString[connectionParam]);
                        }
                    }
                    return resultingString.ConnectionString;
                }
                catch
                {
                    //TODO:REMOVE (just for developing)
                    return System.Configuration.ConfigurationManager.ConnectionStrings["SQLExpress"].ConnectionString;
                }
            }
        }
    }

}
