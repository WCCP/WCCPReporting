﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica.Reports.RPI.Common
{
    public enum ExportToType
    {
        Xls,
        Pdf,
        Rtf,
        Txt,
        Html,
        Mht,
        Csv,
        Bmp
    }
}
