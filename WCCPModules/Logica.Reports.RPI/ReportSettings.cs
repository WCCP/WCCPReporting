using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.RPI.Authentication;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.Localization;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WccpReporting;


namespace Logica.Reports.RPI
{
    public partial class ReportSettings : XtraForm, IUIConfigurator
    {
        #region Private members

        private List<DSMEntity> _dsmList;
        private List<SupervisorEntity> _supervisorList;
        private List<MerchandiserEntity> _merchandiserList;
        private List<RouteEntity> _routeList;
        private long _routeId;

        public DSMEntity _dsm;
        public SupervisorEntity _supervisor;
        public MerchandiserEntity _merchandiser;
        public RouteEntity _route;

        private bool IsPreparingMode { get; set; }

        private string SupervisorBaseFilter = string.Format( @"{0} > 0", MappedNames.SupervisorId);
        //string.Format( @"{0} > 0 AND {1} <> 9", MappedNames.SupervisorId, MappedNames.Status);
        private string MerchandiserBaseFilter = string.Format(@"{0} <> 9", MappedNames.Status);
        //private string RouteBaseFilter = string.Format(@"{0} <> 9", MappedNames.Status);

        #endregion Private members

        #region Constants
        private const string RoleSplitter = "-";
        private const string RolePairsSplitter = ";";
        private const string EnabledFeature = "Enabled";

        #endregion Constants

        #region Constructors

        public ReportSettings()
        {
            InitializeComponent();

            IsPreparingMode = false;
            //LoadReportsAndModules();
            InitEvents();

            AdjustUI();

            SetCheckedItems();

            LocalizeUI();

            LoadData();
            InitComboBoxes();
        }

        #endregion Constructors

        #region Public Properties
        public IList<Type> CheckedReportsAndModules
        {
            get
            {
                IList<Type> checkedItems = new List<Type>();
                foreach (CheckedListBoxItem checkListItem in chlbReports.CheckedItems)
                {
                    checkedItems.Add((Type)checkListItem.Value);
                }
                return checkedItems;
            }
        }

        /// <summary>
        /// Return start date value from dateEdit control
        /// </summary>
        public DateTime DateStart
        {
            get { return deReportStartDate.DateTime; }
            set { deReportStartDate.DateTime = value; }
        }

        /// <summary>
        /// Return end date value from dateEdit control
        /// </summary>
        public DateTime DateEnd
        {
            get { return deReportEndDate.DateTime; }
            set { deReportEndDate.DateTime = value; }
        }

        /// <summary>
        /// Route id
        /// </summary>
        public long RouteId
        {
            get { return _routeId; }
            set { _routeId = value; }
        }

        #endregion Public Properties

        #region IUIConfigurator Implementation

        public void AdjustUI()
        {
            chlbReports.Items.Clear();

            // retrieve settings for specified ROLE from resource file 
            string roleDefinitionResourceKey = string.Format(@"{0}_{1}", typeof(ReportSettings).Name, AuthenticationManager.Instance.CurrentUserRole);
            object resourceValue = RolesDefinition.ResourceManager.GetObject(roleDefinitionResourceKey);

            if (resourceValue != null)
            {
                // fill dictionary with pairs of TypeName-TypeVisibilityForUser from ROLE settings
                IDictionary<string, string> roleTypesVisibility = new Dictionary<string, string>();

                string roleSettingsString = (string)resourceValue;

                string[] splitted = roleSettingsString.Split(new string[] { RolePairsSplitter }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string rolePair in splitted)
                {
                    string[] pair = rolePair.Split(new string[] { RoleSplitter }, StringSplitOptions.RemoveEmptyEntries);
                    if (pair.Length == 2)
                    {
                        roleTypesVisibility.Add(pair[0], pair[1]);
                    }
                }


                // get Modules are used and add them to checkListbox
                Type[] implementedTypes = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();
                foreach (Type type in implementedTypes)
                {
                    if (type.GetInterface(typeof(IRPIModule).FullName) != null)
                    {
                        if (roleTypesVisibility.ContainsKey(type.Name))
                            if (roleTypesVisibility[type.Name].Contains(EnabledFeature))
                            {
                                chlbReports.Items.Add(type, LocalizationProvider.GetText(type.Name), CheckState.Unchecked, true);
                            }
                    }
                }
            }
        }

        #endregion IUIConfigurator Implementation

        #region Private Methods

        private void InitEvents()
        {
            cbDSM.SelectedIndexChanged += new EventHandler(cbDSM_SelectedIndexChanged);
            cbSupervisor.SelectedIndexChanged += new EventHandler(cbSupervisor_SelectedIndexChanged);
            cbMerchandisers.SelectedIndexChanged += new EventHandler(cbMerchandisers_SelectedIndexChanged);
            cbRoutes.SelectedIndexChanged += new EventHandler(cbRoutes_SelectedIndexChanged);
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(WCCPConst.msgNoHelp, "����������", MessageBoxButtons.OK);
        }

        private void cbDSM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPreparingMode)
                return;

            SetSelection<DSMEntity>(cbDSM, ref _dsm);
            if (_dsm == null)
            {
                _supervisorList = new SupervisorsDataProvider().GetList(SupervisorBaseFilter);
            }
            else 
            {
                string filter = string.Format("{0} AND {1} = {2}", SupervisorBaseFilter, MappedNames.DSMId, _dsm.Id);
                _supervisorList = new SupervisorsDataProvider().GetList(filter);
            }
            DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);            

            //DataConvertorHelper.PopulateList<MerchandiserEntity>(cbMerchandisers.Properties.Items, _merchandiserList);
            //DataConvertorHelper.PopulateList<RouteEntity>(cbRoutes.Properties.Items, _routeList);

            //_merchandiserList = new MerchandisersDataProvider().GetList();
            //_routeList = new RoutesDataProvider().GetList();

            IsPreparingMode = true;
            if (cbRoutes.Properties.Items.Count > 0)
                cbRoutes.SelectedIndex = 0;
            if (cbMerchandisers.Properties.Items.Count > 0)
                cbMerchandisers.SelectedIndex = 0;
            IsPreparingMode = false;

            cbSupervisor.SelectedIndex = 0;

            //cbMerchandisers.SelectedIndex = 0;
            //cbRoutes.SelectedIndex = 0;

        }

        private void cbSupervisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPreparingMode)
                return;

            SetSelection<SupervisorEntity>(cbSupervisor, ref _supervisor);
            if (_supervisor == null)
            {
                _merchandiserList = new MerchandisersDataProvider().GetList(MerchandiserBaseFilter);
            }
            else
            {
                string filter = string.Format("{0} AND {1} = {2}", MerchandiserBaseFilter, MappedNames.SupervisorId, _supervisor.Id);
                _merchandiserList = new MerchandisersDataProvider().GetList(filter);
            }
            DataConvertorHelper.PopulateList<MerchandiserEntity>(cbMerchandisers.Properties.Items, _merchandiserList);

            //DataConvertorHelper.PopulateList<RouteEntity>(cbRoutes.Properties.Items, _routeList);

            //_routeList = new RoutesDataProvider().GetList();

            IsPreparingMode = true;
            if (cbRoutes.Properties.Items.Count > 0)
                cbRoutes.SelectedIndex = 0;
            IsPreparingMode = false;

            cbMerchandisers.SelectedIndex = 0;

            //cbRoutes.SelectedIndex = 0;
        }

        private void cbMerchandisers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPreparingMode)
                return;

            SetSelection<MerchandiserEntity>(cbMerchandisers, ref _merchandiser);

            if (_merchandiser == null)
            {
                _routeList = new RoutesDataProvider().GetList();
            }
            else
            {
                string filter = string.Format("{0} = {1}", MappedNames.MerchId, _merchandiser.Id);
                _routeList = new RoutesDataProvider().GetList(filter);
            }
            DataConvertorHelper.PopulateList<RouteEntity>(cbRoutes.Properties.Items, _routeList);

            cbRoutes.SelectedIndex = 0;
        }

        private void cbRoutes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPreparingMode)
                return;

            SetSelection<RouteEntity>(cbRoutes, ref _route);
        }

        private void LoadReportsAndModules()
        {
            this.chlbReports.Items.Clear();

            Type[] implementedTypes = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();
            foreach (Type type in implementedTypes)
            {
                if (type.GetInterface(typeof(IRPIModule).FullName) != null)
                {
                    chlbReports.Items.Add( type, LocalizationProvider.GetText(type.Name), CheckState.Unchecked, true);
                }
            }
        }

        private void LocalizeUI()
        {
            Text = Resource.Reports;
            tpOptions.Text = Resource.Action;
            btnYes.Text = Resource.Yes;
            btnNo.Text = Resource.No;
            btnHelp.Text = Resource.Help;
            chkCheckAllReports.Text = Resource.CheckAllReports;
            lblStartPeriod.Text = Resource.LabelStartPeriodText;
            lblEndPeriod.Text = Resource.LabelEndPeriodText;
            lblDSM.Text = Resource.DSMLabelText;
            lblSupervisor.Text = Resource.SupervisorLabelText;
            lblMerchandisers.Text = Resource.MerchandiserLabelText;
            lblRoute.Text = Resource.RouteLabelText;
        }

        private void SetCheckedItems()
        {
            foreach (CheckedListBoxItem item in chlbReports.Items)
            {
                if (item.Value is Type)
                    if ((item.Value as Type).Name.Contains("AimsAndSKUModule"))
                        item.CheckState = CheckState.Checked;
            }
        }

        private void InitComboBoxes()
        {
            cbDSM.SelectedIndex = 0;
            /*
            cbSupervisor.SelectedIndex = 0;
            cbMerchandisers.SelectedIndex = 0;
            cbRoutes.SelectedIndex = 0;        
            */
        }

        private void LoadData()
        {
            _dsmList = new DSMDataProvider().GetList();
            /*
            _supervisorList = new SupervisorsDataProvider().GetList(SupervisorBaseFilter);
            _merchandiserList = new MerchandisersDataProvider().GetList(MerchandiserBaseFilter);
            _routeList = new RoutesDataProvider().GetList();
            */

            DataConvertorHelper.PopulateList<DSMEntity>(cbDSM.Properties.Items, _dsmList);
            /*
            DataConvertorHelper.PopulateList<SupervisorEntity>(cbSupervisor.Properties.Items, _supervisorList);
            DataConvertorHelper.PopulateList<MerchandiserEntity>(cbMerchandisers.Properties.Items, _merchandiserList);
            DataConvertorHelper.PopulateList<RouteEntity>(cbRoutes.Properties.Items, _routeList);
            */
        }

        private void SetSelection<T> (ComboBoxEdit combo, ref T value) where T : IBaseEntity
        {
            value = default(T);

            if (combo.Properties.Items.Count > 0)
            {
                if (combo.SelectedItem != null && combo.SelectedIndex > 0)
                {
                    value = (T)combo.SelectedItem;
                }
            }
        }

        private SupervisorsDataSet SupervisorsDS { get; set; }

        private List<SupervisorEntity> GetSuperVisorsList(string filter)
        {
            SupervisorsDataProvider sdp = new SupervisorsDataProvider();
            if (null == SupervisorsDS)
                SupervisorsDS = sdp.GetData();

            SupervisorsDS.Supervisor.DefaultView.RowFilter = filter;

            List<SupervisorEntity> supervisorList = sdp.GetFilledList(SupervisorsDS.Supervisor.DefaultView);
            
            return supervisorList;
        }
        
        #endregion Private Methods

        #region Event Handlers

        private void chkCheckAllReports_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCheckAllReports.CheckState == CheckState.Checked)
                foreach (CheckedListBoxItem item in chlbReports.Items)
                    item.CheckState = CheckState.Checked;
            else if (chkCheckAllReports.CheckState == CheckState.Unchecked)
                foreach (CheckedListBoxItem item in chlbReports.Items)
                    item.CheckState = CheckState.Unchecked;
        }

        #endregion Event Handlers

    }
}