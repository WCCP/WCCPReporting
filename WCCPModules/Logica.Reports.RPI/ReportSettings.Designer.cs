namespace Logica.Reports.RPI
{
    partial class ReportSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportSettings));
            this.tcRPIOptions = new DevExpress.XtraTab.XtraTabControl();
            this.tpOptions = new DevExpress.XtraTab.XtraTabPage();
            this.pnlContainer = new DevExpress.XtraEditors.PanelControl();
            this.cbRoutes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblRoute = new DevExpress.XtraEditors.LabelControl();
            this.cbMerchandisers = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblMerchandisers = new DevExpress.XtraEditors.LabelControl();
            this.cbSupervisor = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbDSM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblSupervisor = new DevExpress.XtraEditors.LabelControl();
            this.lblDSM = new DevExpress.XtraEditors.LabelControl();
            this.lblEndPeriod = new DevExpress.XtraEditors.LabelControl();
            this.lblStartPeriod = new DevExpress.XtraEditors.LabelControl();
            this.deReportEndDate = new DevExpress.XtraEditors.DateEdit();
            this.deReportStartDate = new DevExpress.XtraEditors.DateEdit();
            this.chkCheckAllReports = new DevExpress.XtraEditors.CheckEdit();
            this.chlbReports = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.pnlBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.icRPIOptions = new DevExpress.Utils.ImageCollection();
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.tcRPIOptions)).BeginInit();
            this.tcRPIOptions.SuspendLayout();
            this.tpOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).BeginInit();
            this.pnlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbRoutes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMerchandisers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAllReports.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chlbReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).BeginInit();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icRPIOptions)).BeginInit();
            this.SuspendLayout();
            // 
            // tcRPIOptions
            // 
            this.tcRPIOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcRPIOptions.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.tcRPIOptions.Appearance.BackColor2 = System.Drawing.SystemColors.Control;
            this.tcRPIOptions.Appearance.BorderColor = System.Drawing.SystemColors.Control;
            this.tcRPIOptions.Appearance.Options.UseBackColor = true;
            this.tcRPIOptions.Appearance.Options.UseBorderColor = true;
            this.tcRPIOptions.AppearancePage.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tcRPIOptions.AppearancePage.PageClient.BackColor2 = System.Drawing.SystemColors.Control;
            this.tcRPIOptions.AppearancePage.PageClient.Options.UseBackColor = true;
            this.tcRPIOptions.Location = new System.Drawing.Point(0, 0);
            this.tcRPIOptions.Name = "tcRPIOptions";
            this.tcRPIOptions.SelectedTabPage = this.tpOptions;
            this.tcRPIOptions.Size = new System.Drawing.Size(365, 404);
            this.tcRPIOptions.TabIndex = 0;
            this.tcRPIOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpOptions});
            // 
            // tpOptions
            // 
            this.tpOptions.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpOptions.Appearance.PageClient.Options.UseBackColor = true;
            this.tpOptions.Controls.Add(this.pnlContainer);
            this.tpOptions.Name = "tpOptions";
            this.tpOptions.Size = new System.Drawing.Size(361, 380);
            this.tpOptions.Text = "Options";
            // 
            // pnlContainer
            // 
            this.pnlContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlContainer.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlContainer.Appearance.BackColor2 = System.Drawing.SystemColors.Control;
            this.pnlContainer.Appearance.BorderColor = System.Drawing.SystemColors.Control;
            this.pnlContainer.Appearance.Options.UseBackColor = true;
            this.pnlContainer.Appearance.Options.UseBorderColor = true;
            this.pnlContainer.Controls.Add(this.cbRoutes);
            this.pnlContainer.Controls.Add(this.lblRoute);
            this.pnlContainer.Controls.Add(this.cbMerchandisers);
            this.pnlContainer.Controls.Add(this.lblMerchandisers);
            this.pnlContainer.Controls.Add(this.cbSupervisor);
            this.pnlContainer.Controls.Add(this.cbDSM);
            this.pnlContainer.Controls.Add(this.lblSupervisor);
            this.pnlContainer.Controls.Add(this.lblDSM);
            this.pnlContainer.Controls.Add(this.lblEndPeriod);
            this.pnlContainer.Controls.Add(this.lblStartPeriod);
            this.pnlContainer.Controls.Add(this.deReportEndDate);
            this.pnlContainer.Controls.Add(this.deReportStartDate);
            this.pnlContainer.Controls.Add(this.chkCheckAllReports);
            this.pnlContainer.Controls.Add(this.chlbReports);
            this.pnlContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(358, 380);
            this.pnlContainer.TabIndex = 0;
            // 
            // cbRoutes
            // 
            this.cbRoutes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbRoutes.Location = new System.Drawing.Point(87, 318);
            this.cbRoutes.Name = "cbRoutes";
            this.cbRoutes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbRoutes.Size = new System.Drawing.Size(266, 22);
            this.cbRoutes.TabIndex = 24;
            // 
            // lblRoute
            // 
            this.lblRoute.Location = new System.Drawing.Point(10, 323);
            this.lblRoute.Name = "lblRoute";
            this.lblRoute.Size = new System.Drawing.Size(39, 13);
            this.lblRoute.TabIndex = 23;
            this.lblRoute.Text = "lblRoute";
            // 
            // cbMerchandisers
            // 
            this.cbMerchandisers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMerchandisers.Location = new System.Drawing.Point(39, 286);
            this.cbMerchandisers.Name = "cbMerchandisers";
            this.cbMerchandisers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbMerchandisers.Size = new System.Drawing.Size(314, 22);
            this.cbMerchandisers.TabIndex = 22;
            // 
            // lblMerchandisers
            // 
            this.lblMerchandisers.Location = new System.Drawing.Point(10, 290);
            this.lblMerchandisers.Name = "lblMerchandisers";
            this.lblMerchandisers.Size = new System.Drawing.Size(79, 13);
            this.lblMerchandisers.TabIndex = 21;
            this.lblMerchandisers.Text = "lblMerchandisers";
            // 
            // cbSupervisor
            // 
            this.cbSupervisor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSupervisor.Location = new System.Drawing.Point(39, 257);
            this.cbSupervisor.Name = "cbSupervisor";
            this.cbSupervisor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSupervisor.Size = new System.Drawing.Size(314, 22);
            this.cbSupervisor.TabIndex = 20;
            // 
            // cbDSM
            // 
            this.cbDSM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDSM.Location = new System.Drawing.Point(39, 228);
            this.cbDSM.Name = "cbDSM";
            this.cbDSM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDSM.Size = new System.Drawing.Size(314, 22);
            this.cbDSM.TabIndex = 19;
            // 
            // lblSupervisor
            // 
            this.lblSupervisor.Location = new System.Drawing.Point(10, 260);
            this.lblSupervisor.Name = "lblSupervisor";
            this.lblSupervisor.Size = new System.Drawing.Size(61, 13);
            this.lblSupervisor.TabIndex = 18;
            this.lblSupervisor.Text = "lblSupervisor";
            // 
            // lblDSM
            // 
            this.lblDSM.Location = new System.Drawing.Point(10, 233);
            this.lblDSM.Name = "lblDSM";
            this.lblDSM.Size = new System.Drawing.Size(31, 13);
            this.lblDSM.TabIndex = 17;
            this.lblDSM.Text = "lblDSM";
            // 
            // lblEndPeriod
            // 
            this.lblEndPeriod.Location = new System.Drawing.Point(166, 20);
            this.lblEndPeriod.Name = "lblEndPeriod";
            this.lblEndPeriod.Size = new System.Drawing.Size(58, 13);
            this.lblEndPeriod.TabIndex = 5;
            this.lblEndPeriod.Text = "lblEndPeriod";
            // 
            // lblStartPeriod
            // 
            this.lblStartPeriod.Location = new System.Drawing.Point(8, 20);
            this.lblStartPeriod.Name = "lblStartPeriod";
            this.lblStartPeriod.Size = new System.Drawing.Size(64, 13);
            this.lblStartPeriod.TabIndex = 4;
            this.lblStartPeriod.Text = "lblStartPeriod";
            // 
            // deReportEndDate
            // 
            this.deReportEndDate.EditValue = null;
            this.deReportEndDate.Location = new System.Drawing.Point(202, 17);
            this.deReportEndDate.Name = "deReportEndDate";
            this.deReportEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deReportEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deReportEndDate.Size = new System.Drawing.Size(100, 22);
            this.deReportEndDate.TabIndex = 3;
            // 
            // deReportStartDate
            // 
            this.deReportStartDate.EditValue = null;
            this.deReportStartDate.Location = new System.Drawing.Point(37, 17);
            this.deReportStartDate.Name = "deReportStartDate";
            this.deReportStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deReportStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deReportStartDate.Size = new System.Drawing.Size(100, 22);
            this.deReportStartDate.TabIndex = 2;
            // 
            // chkCheckAllReports
            // 
            this.chkCheckAllReports.Location = new System.Drawing.Point(5, 47);
            this.chkCheckAllReports.Name = "chkCheckAllReports";
            this.chkCheckAllReports.Properties.Caption = "chkCheckAllReports";
            this.chkCheckAllReports.Size = new System.Drawing.Size(75, 17);
            this.chkCheckAllReports.TabIndex = 1;
            this.chkCheckAllReports.CheckedChanged += new System.EventHandler(this.chkCheckAllReports_CheckedChanged);
            // 
            // chlbReports
            // 
            this.chlbReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chlbReports.Location = new System.Drawing.Point(5, 72);
            this.chlbReports.Name = "chlbReports";
            this.chlbReports.Size = new System.Drawing.Size(348, 141);
            this.chlbReports.TabIndex = 0;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.Appearance.BackColor2 = System.Drawing.SystemColors.Control;
            this.pnlBottom.Appearance.BorderColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.pnlBottom.Appearance.Options.UseBackColor = true;
            this.pnlBottom.Appearance.Options.UseBorderColor = true;
            this.pnlBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBottom.Controls.Add(this.btnHelp);
            this.pnlBottom.Controls.Add(this.btnNo);
            this.pnlBottom.Controls.Add(this.btnYes);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 405);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(365, 41);
            this.pnlBottom.TabIndex = 4;
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.icRPIOptions;
            this.btnHelp.Location = new System.Drawing.Point(281, 8);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 6;
            this.btnHelp.Text = "Help";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // icRPIOptions
            // 
            this.icRPIOptions.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icRPIOptions.ImageStream")));
            this.icRPIOptions.Images.SetKeyName(0, "help_16.png");
            // 
            // btnNo
            // 
            this.btnNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageList = this.icRPIOptions;
            this.btnNo.Location = new System.Drawing.Point(200, 8);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 5;
            this.btnNo.Text = "Cancel";
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnYes.Appearance.Options.UseForeColor = true;
            this.btnYes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnYes.Image = global::Logica.Reports.RPI.Properties.Resources.ok;
            this.btnYes.ImageList = this.icRPIOptions;
            this.btnYes.Location = new System.Drawing.Point(119, 8);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 4;
            this.btnYes.Text = "Ok";
            // 
            // ReportSettings
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 446);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.tcRPIOptions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ReportSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RPI";
            ((System.ComponentModel.ISupportInitialize)(this.tcRPIOptions)).EndInit();
            this.tcRPIOptions.ResumeLayout(false);
            this.tpOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.pnlContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbRoutes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMerchandisers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSupervisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDSM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deReportStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAllReports.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chlbReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottom)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.icRPIOptions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tcRPIOptions;
        private DevExpress.XtraTab.XtraTabPage tpOptions;
        private DevExpress.XtraEditors.PanelControl pnlContainer;
        private DevExpress.XtraEditors.PanelControl pnlBottom;
        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.Utils.ImageCollection icRPIOptions;
        private DevExpress.XtraEditors.CheckedListBoxControl chlbReports;
        private DevExpress.XtraEditors.DateEdit deReportEndDate;
        private DevExpress.XtraEditors.DateEdit deReportStartDate;
        private DevExpress.XtraEditors.CheckEdit chkCheckAllReports;
        private DevExpress.XtraEditors.LabelControl lblEndPeriod;
        private DevExpress.XtraEditors.LabelControl lblStartPeriod;
        private DevExpress.XtraEditors.ComboBoxEdit cbRoutes;
        private DevExpress.XtraEditors.LabelControl lblRoute;
        private DevExpress.XtraEditors.ComboBoxEdit cbMerchandisers;
        private DevExpress.XtraEditors.LabelControl lblMerchandisers;
        private DevExpress.XtraEditors.ComboBoxEdit cbSupervisor;
        private DevExpress.XtraEditors.ComboBoxEdit cbDSM;
        private DevExpress.XtraEditors.LabelControl lblSupervisor;
        private DevExpress.XtraEditors.LabelControl lblDSM;
    }
}
