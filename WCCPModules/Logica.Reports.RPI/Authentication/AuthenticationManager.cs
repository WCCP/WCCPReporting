﻿using System;
using Logica.Reports.RPI.Configuration;
using Logica.Reports.RPI.Common;
using Logica.Reports.DataAccess;

namespace Logica.Reports.RPI.Authentication
{
    /// <summary>
    /// Authentication manager. Thread-safe implementation
    /// </summary>
    public sealed class AuthenticationManager
    {
        private static volatile AuthenticationManager _instance;
        private static object _syncRoot = new object();

        private int _currentUserLevel = int.MinValue;

        private int _currentUserId = int.MinValue;

        private AuthenticationManager() { }

        public static AuthenticationManager Instance {
            get {
                if (_instance == null) {
                    lock (_syncRoot) {
                        if (_instance == null)
                            _instance = new AuthenticationManager();
                    }
                }
                return _instance;
            }
        }

        public string CurrentUserRole {
            get {
                return AuthenticationSettings.UserRole;
            }
        }

        public int CurrentUserLevel {
            get {
                if (_currentUserLevel == int.MinValue)
                    _currentUserLevel = Int32.Parse(DataAccessLayer.ExecuteScalarQuery(ScriptProvider.CurrentUserLevel).ToString());

                if (_currentUserLevel == int.MinValue)
                    _currentUserLevel = AuthenticationSettings.UserLevel;

                return _currentUserLevel;
            }
        }

        public int CurrentUserId 
        {
            get {
                if (_currentUserId == int.MinValue)
                {
                    try
                    {
                        _currentUserId = Int32.Parse(DataAccessLayer.ExecuteScalarQuery(ScriptProvider.CurrentUserId).ToString());
                    }
                    catch (NullReferenceException ex)
                    {
                        _currentUserId = -1;
                    }
                }

                return _currentUserId;
            }
        }
    }

}
