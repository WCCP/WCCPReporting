﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataAccess;
using Logica.Reports.RPI.Authentication;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.Forms;
using Logica.Reports.RPI.Localization;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraGrid.Export;
using DevExpress.XtraExport;

namespace Logica.Reports.RPI.UserControls
{
    /// <summary>
    /// User control which contains functionality for Cascaded Aims processing
    /// </summary>
    public partial class CascadedAimsModule : UserControl, IRPIModule
    {
        #region Private members

        AimsDataSet.AimDataTable aimsData = null;
        AimsDataSet.AimDataTable aimsDataFilteredByFocusedSKU = null;

        private DateTime _reportStartDate;
        private DateTime _reportEndDate;

        private IDictionary<string, string> tableNameShowedColumnMapping = new Dictionary<string, string>();
        Dictionary<int, string> KPIlist = new Dictionary<int, string>();
        private int _regionId = -1;
        private int _districtId = -1;
        private int _cityId = -1;
        private string regionsFilter = string.Empty;
        private string notFocusedSKUFilter;

        private bool isInit = false;
        private object prevValue;
        private bool checkingMode = false;
        private Color _tooltipColor = Color.FromArgb(255, 255, 231);

        #endregion Private members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public CascadedAimsModule()
        {
            InitializeComponent();

            InitEvents();

            Localize();

            InitializeMapping();

            SetKPIList();

            SetupColumns();

            SetupUI();
        }

        #endregion Constructors

        #region Public properties

        /// <summary>
        ///  Returns name of the module
        /// </summary>
        public string ModuleName
        {
            get { return Resource.CascadedAimsModule; }
        }

        /// <summary>
        /// Start Date of Report
        /// </summary>
        public DateTime ReportStartDate
        {
            get { return _reportStartDate; }
            set { _reportStartDate = value; }
        }

        /// <summary>
        /// End Date of Report
        /// </summary>
        public DateTime ReportEndDate
        {
            get { return _reportEndDate; }
            set { _reportEndDate = value; }
        }

        /// <summary>
        /// Selected DSM from Report parameters
        /// </summary>
        public DSMEntity DSM { get; set; }

        /// <summary>
        /// Selected Supervisor from Report parameters
        /// </summary>
        public SupervisorEntity Supervisor { get; set; }

        /// <summary>
        /// Selected Merchandiser from Report parameters
        /// </summary>
        public MerchandiserEntity Merchandiser { get; set; }

        /// <summary>
        /// Selected Route from Report parameters
        /// </summary>
        public RouteEntity Route { get; set; }

        /// <summary>
        /// Returns TreeView control which represents hierarchy of Country > Region > District > City
        /// </summary>
        public TreeView RegionsTree
        {
            get { return tvRegions; }
        }

        public TreeList TerritoryTree
        {
            get { return treeRegions; }
        }

        /// <summary>
        /// Returns filter expression for current state of Filter panel
        /// </summary>
        public string CurrentFilter
        {
            get
            {
                string lFilter = notFocusedSKUFilter;

                if (!string.IsNullOrEmpty(regionsFilter))
                    lFilter = string.Format("{0} AND {1}", lFilter, regionsFilter);

                string lDatesFilter = GetDatesFilter();
                if (!string.IsNullOrEmpty(lDatesFilter))
                    if (string.IsNullOrEmpty(lFilter))
                        lFilter = lDatesFilter;
                    else
                        lFilter = string.Format("{0} AND {1}", lFilter, lDatesFilter);

                return lFilter;
            }
        }

        #endregion Public properties

        #region Public methods

        /// <summary>
        ///  Setup columns for Grid which represents Cascaded Aims data
        /// </summary>
        public void SetupColumns()
        {
            gridView.Columns.Clear();

            gridView.Columns.AddRange(GetGridColumns());
            gridView.OptionsView.ColumnAutoWidth = false;
            gridView.OptionsSelection.MultiSelect = true;
        }

        /// <summary>
        /// Loads data from RegionDataSet like array of TreeNode elements
        /// </summary>
        public void LoadRegionsTree()
        {
            RegionsDataProvider lRegionsDataProvider = new RegionsDataProvider();

            DataTable territory = lRegionsDataProvider.GetTerritoryData();

            treeRegions.BeginUpdate();
            try
            {
                treeRegions.DataSource = territory;
                treeRegions.PopulateColumns();
                RestoreSettingsForColumn(columnLevel, false);
                RestoreSettingsForColumn(columnItemId, false);
                RestoreSettingsForColumn(columnName, true);
            }
            finally
            {
                treeRegions.EndUpdate();
            }
        }

        private void RestoreSettingsForColumn(TreeListColumn column, bool visible)
        {
            treeRegions.Columns[column.FieldName].OptionsColumn.ReadOnly = true;
            treeRegions.Columns[column.FieldName].OptionsColumn.AllowEdit = false;
            treeRegions.Columns[column.FieldName].Caption = column.Caption;
            treeRegions.Columns[column.FieldName].Visible = visible;
        }

        /// <summary>
        /// Reload cascaded aims
        /// </summary>
        public void LoadCascadedAims()
        {
            RefreshData();
        }

        /// <summary>
        /// Load data for Regions and Cascaded Aims at the first time
        /// </summary>
        public void InitialLoadData()
        {
            if (isInit)
                return;
            AttachTreeEvents(false);

            notFocusedSKUFilter = string.Format("{0} = {1}", MappedNames.IsFocused, false);
            LoadRegionsTree();
            LoadCascadedAims();
            AttachTreeEvents(true);

            tvRegions.HideSelection = false;
            if (tvRegions.Nodes.Count > 0)
                tvRegions.SelectedNode = tvRegions.Nodes[0];
            //tvRegions.Select();
            if (treeRegions.Nodes.Count > 0)
                treeRegions.FocusedNode = treeRegions.Nodes.FirstNode;
            //treeRegions.Select();
            isInit = true;
        }

        #endregion Public methods

        #region Event Handlers

        private void btnNewAim_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NewAimForm frmNewAim = new NewAimForm();
            frmNewAim.RegionId = _regionId;
            frmNewAim.DistrictId = _districtId;
            frmNewAim.CityId = _cityId;

            if (frmNewAim.ShowDialog() == DialogResult.OK)
            {
                if (frmNewAim.CreatedAims.Count > 0)
                {
                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    int fakeIdentity = int.MaxValue;
                    foreach (CascadedAimEntity entity in frmNewAim.CreatedAims)
                    {
                        entity.IsFocused = false;

                        fakeIdentity--;
                        DataRow dr = InsertAim(entity, aimsData);
                        dr[MappedNames.AimId] = fakeIdentity;
                    }

                    dataProvider.UpdateData(aimsData);

                    RefreshData(true);
                }
            }
        }

        private DataRow InsertAim(CascadedAimEntity cascadedAim, AimsDataSet.AimDataTable table)
        {
            DataRow insertedRow = table.NewRow();

            cascadedAim.Fill(insertedRow);
            table.Rows.Add(insertedRow);

            return insertedRow;
        }

        private void btnCopyAim_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                EditAimForm frmEditAim = new EditAimForm();

                CascadedAimEntity aim = new CascadedAimEntity(focusedRow);
                aim.AimId = int.MinValue; // flag about copying the aim
                aim.RegionId = _regionId;
                aim.DistrictId = _districtId;
                aim.CityId = _cityId;

                // in order to prevent copying Approve parameters
                aim.IsStartDateApproved = false;
                aim.IsEndDateApproved = false;

                frmEditAim.Init(aim);
                frmEditAim.Localize(Resource.CopiedNewAimFormCaption, Resource.NewAimFormButtonOk, Resource.NewAimFormButtonCancel);

                if (frmEditAim.ShowDialog() == DialogResult.OK)
                {
                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    DataRow dr = InsertAim(frmEditAim.EditedAim, aimsData);

                    dataProvider.UpdateData(aimsData);

                    RefreshData(true);
                }
            }
        }

        private void btnEditAim_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditAim();
        }

        private void EditAim()
        {
            DataRow lFocusedRow = gridView.GetFocusedDataRow();
            if (lFocusedRow != null)
            {
                EditAimForm lFrmEditAim = new EditAimForm();

                int lFocusedRowHandle = gridView.FocusedRowHandle;
                CascadedAimEntity lAim = new CascadedAimEntity(lFocusedRow);
                lFrmEditAim.Init(lAim);
                lFrmEditAim.Localize(Resource.EditAimFormCaption, Resource.EditAimFormButtonOk, Resource.EditAimFormButtonCancel);

                if (lFrmEditAim.ShowDialog() == DialogResult.OK)
                {
                    CascadedAimsDataProvider lDataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable lAimsData;

                    if (gctlCascadedAims.DataSource is DataView)
                        lAimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        lAimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (lAimsData == null)
                        lAimsData = lDataProvider.GetData().Aim;

                    if (lFrmEditAim.EditedAim.RegionId == -1)
                        lFrmEditAim.EditedAim.RegionId = _regionId;
                    if (lFrmEditAim.EditedAim.DistrictId == -1)
                        lFrmEditAim.EditedAim.DistrictId = _districtId;
                    if (lFrmEditAim.EditedAim.CityId == -1)
                        lFrmEditAim.EditedAim.CityId = _cityId;

                    lFocusedRow.BeginEdit();
                    lFrmEditAim.EditedAim.Fill(lFocusedRow);
                    lFocusedRow.EndEdit();

                    lDataProvider.UpdateData(lAimsData);

                    RefreshData();

                    gridView.FocusedRowHandle = lFocusedRowHandle;
                }
            }
        }

        private void btnApproveStartDate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                DateTime currentDate = DateTime.Now;
                bool isCurrentDateApproved = Convert.ToBoolean(focusedRow[MappedNames.IsStartDateApproved]);

                int focusedRowHandle = gridView.FocusedRowHandle;
                int[] selectedRowHandles = gridView.GetSelectedRows();

                if (ConfirmationDialogForm.Show
                    (
                        string.Format(
                            @"{0} {1}?",
                            "Вы уверены в том что хотите ",
                            GetApprovePhrase(isCurrentDateApproved, selectedRowHandles.Length)),
                        isCurrentDateApproved ? "Cнятие утверждение" : "Утверждение",
                        isCurrentDateApproved ? "Cнять утверждение" : "Утвердить",
                        "Отмена"
                    ) == DialogResult.OK
                    )
                {

                    foreach (int handle in selectedRowHandles)
                    {
                        DataRow row = gridView.GetRow(handle) is DataRow
                            ? gridView.GetRow(handle) as DataRow
                            : (gridView.GetRow(handle) as DataRowView).Row;

                        bool isStartdateApproved = Convert.ToBoolean(row[MappedNames.IsStartDateApproved]);
                        bool isEnddateApproved = Convert.ToBoolean(row[MappedNames.IsEndDateApproved]);

                        DateTime currentRowDateStartPeriod = DateHelper.GetDBNullDateChecked(row[MappedNames.DateStartPeriod]);
                        if (isCurrentDateApproved)
                        {
                            if (isEnddateApproved)
                            {
                                MessageBox.Show(
                                                   string.Format(@"{0} ({1}, {2})",
                                                       Resource.CannotDisapproveStartDateBecauseEndDateStillApprovedErrorMsg,
                                                       row[MappedNames.ProductCombineName],
                                                       currentRowDateStartPeriod
                                                       ),
                                                   Resource.DisapproveErrorFormCaption
                                               );
                                continue;
                            }

                            if (currentRowDateStartPeriod != DateTime.MinValue && currentDate > currentRowDateStartPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.DisapproveStartDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateStartPeriod
                                        ),
                                    Resource.DisapproveErrorFormCaption);
                                continue;
                            }


                        }
                        else
                        {
                            if (currentRowDateStartPeriod != DateTime.MinValue && currentDate > currentRowDateStartPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.ApproveStartDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateStartPeriod
                                        ),
                                    Resource.ApproveErrorFormCaption);
                                continue;
                            }
                        }

                        CascadedAimEntity approvedAim = new CascadedAimEntity(row);
                        approvedAim.IsStartDateApproved = !isCurrentDateApproved;
                        List<string> errors = new List<string>();
                        DataMediator.CheckForCrossedAimsErrors(approvedAim, errors);
                        if (errors.Count > 0)
                        {
                            DetailedMessageForm.Show(errors.ToArray());
                            continue;
                        }

                        row.BeginEdit();
                        row[MappedNames.IsStartDateApproved] = !isCurrentDateApproved;
                        row[MappedNames.DateApproveStartPeriod] = currentDate;
                        row.EndEdit();
                    }

                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    dataProvider.UpdateData(aimsData);
                }
                gridView.FocusedRowHandle = focusedRowHandle;
            }
        }

        private void btnApproveEndDate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                DateTime currentDate = DateTime.Now;
                bool isCurrentDateApproved = Convert.ToBoolean(focusedRow[MappedNames.IsEndDateApproved]);

                int focusedRowHandle = gridView.FocusedRowHandle;
                int[] selectedRowHandles = gridView.GetSelectedRows();

                if (ConfirmationDialogForm.Show
                    (
                        string.Format(
                            @"{0} {1}?",
                            "Вы уверены в том что хотите ",
                            GetApprovePhrase(isCurrentDateApproved, selectedRowHandles.Length)),
                        isCurrentDateApproved ? "Cнятие утверждение" : "Утверждение",
                        isCurrentDateApproved ? "Cнять утверждение" : "Утвердить",
                        "Отмена"
                    ) == DialogResult.OK
                    )
                {

                    foreach (int handle in selectedRowHandles)
                    {
                        DataRow row = gridView.GetRow(handle) is DataRow
                            ? gridView.GetRow(handle) as DataRow
                            : (gridView.GetRow(handle) as DataRowView).Row;

                        bool isStartdateApproved = Convert.ToBoolean(row[MappedNames.IsStartDateApproved]);
                        bool isEnddateApproved = Convert.ToBoolean(row[MappedNames.IsEndDateApproved]);

                        DateTime currentRowDateEndPeriod = Convert.ToDateTime(row[MappedNames.DateEndPeriod]);

                        if (isCurrentDateApproved)
                        {
                            if (currentRowDateEndPeriod != DateTime.MinValue && currentDate > currentRowDateEndPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.DisapproveEndDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateEndPeriod
                                        ),
                                    Resource.DisapproveErrorFormCaption);
                                continue;
                            }
                        }
                        else
                        {
                            if (!isStartdateApproved)
                            {
                                MessageBox.Show(
                                                   string.Format(@"{0} ({1}, {2})",
                                                       Resource.CannotApproveEndDateBecauseStartDateStillNotApprovedErrorMsg,
                                                       row[MappedNames.ProductCombineName],
                                                       currentRowDateEndPeriod
                                                       ),
                                                   Resource.ApproveErrorFormCaption
                                               );
                                continue;
                            }

                            if (currentRowDateEndPeriod != DateTime.MinValue && currentDate > currentRowDateEndPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.ApproveEndDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateEndPeriod
                                        ),
                                    Resource.ApproveErrorFormCaption);
                                continue;
                            }
                        }

                        CascadedAimEntity approvedAim = new CascadedAimEntity(row);
                        approvedAim.IsEndDateApproved = !isCurrentDateApproved;
                        List<string> errors = new List<string>();
                        DataMediator.CheckForCrossedAimsErrors(approvedAim, errors);
                        if (errors.Count > 0)
                        {
                            DetailedMessageForm.Show(errors.ToArray());
                            continue;
                        }

                        row.BeginEdit();
                        row[MappedNames.IsEndDateApproved] = !isCurrentDateApproved;
                        row[MappedNames.DateApproveEndPeriod] = currentDate;
                        row.EndEdit();
                    }

                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    dataProvider.UpdateData(aimsData);
                }
                gridView.FocusedRowHandle = focusedRowHandle;
            }
        }

        private void btnDeleteAim_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                // check if Aim is in approved state - so it cann't be deleted
                if (Convert.ToBoolean(focusedRow[MappedNames.IsStartDateApproved]))
                {
                    MessageBox.Show(Resource.CannotDeleteApprovedAimMsg, Resource.ErrorDeleteAimFormCaption);
                    return;
                }

                if (ConfirmationDialogForm.Show(
                        Resource.DeleteAimConfirmationMsg,
                        Resource.DeleteAimConfirmationFormCaption,
                        Resource.DeleteAimConfirmationButtonOkText,
                        Resource.DeleteAimConfirmationButtonCancelText) == DialogResult.OK
                    )
                {
                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    focusedRow.Delete();

                    dataProvider.UpdateData(aimsData);

                    RefreshData();
                }
            }
        }

        private void btnRefreshData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void btnExportTo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string lPathToFile = FileHelper.SelectFilePath(ExportToType.Xls);
            ExportEngine(lPathToFile, gctlCascadedAims, GetGridColumns());
        }

        private void btnPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //gctlCascadedAims.ShowPrintPreview();
            PrintEngine(gctlCascadedAims, GetGridColumns());
        }

        private void CascadedAimsModule_Invalidated(object sender, InvalidateEventArgs e)
        {
            InitialLoadData();
        }

        private void tvRegions_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode node = e.Node;

            _regionId = -1;
            _districtId = -1;
            _cityId = -1;

            //            CheckNodes(node);
            //            ComposeRegionsFilter();
            //
            //            SetupUI(_regionId);
            //
            //            RefreshData();
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void deFilter_DateTimeChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C && e.Control)
            {
                try
                {
                    CopyToClipboard(gridView.GetSelectedRows());
                }
                catch { }
            }
        }

        private void gridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (ExistInFieldList(e.Column.FieldName))
            {
                if (e.Value == null || e.Value == DBNull.Value)
                {
                    e.DisplayText = Resource.ComboBoxItemAnyValueText;

                }
            }
        }

        void gridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            DataRow row = gridView.GetDataRow(e.RowHandle);
            if (row != null && Convert.ToBoolean(row[MappedNames.IsBaseSKU])
                && e.Column.FieldName != MappedNames.IsStartDateApproved
                && e.Column.FieldName != MappedNames.IsEndDateApproved)
            {

                Rectangle r = e.Bounds;

                e.Appearance.DrawString(e.Cache, e.DisplayText, r, new SolidBrush(Color.Green));
                //Set e.Handled to true to prevent default painting
                e.Handled = true;

            }
        }

        private void gctlCascadedAims_DoubleClick(object sender, EventArgs e)
        {
            EditAim();
        }

        private void gridView_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;

            if (view.FocusedRowHandle < 0)
            {
                return;
            }

            if (view.FocusedColumn.FieldName == MappedNames.KPIValue)
            {
                try
                {
                    double KPIvalue = Convert.ToDouble(e.Value, CultureInfo.InvariantCulture);

                    if (KPIvalue < 0)
                    {
                        e.Valid = false;
                    }
                }
                catch (FormatException ex)
                {
                    e.Valid = false;
                }
            }

            if (!e.Valid)
            {
                e.ErrorText = Resource.WrongKPIValue;
            }
        }

        private void gridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            string mess;
            e.Cancel = !CouldValueBeEdited(gridView.FocusedColumn.FieldName, out mess);
        }

        private void gridView_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (!checkingMode && e.RowHandle < gridView.RowCount)
            {
                string errorMess;

                e.Valid = IsRowValid(e.RowHandle, out errorMess);
                e.ErrorText = errorMess;

                if (e.Valid)
                {
                    UpdateRow(GetAimFromRow(e.RowHandle), gridView.FocusedRowHandle);
                }
            }
        }

        private void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                return;
            }

            checkingMode = true;

            if (gridView.FocusedColumn.FieldName != MappedNames.KPIValue && Convert.ToBoolean(rbEditType.EditValue))
            {
                WaitManager.StartWait();

                DataRow firstRow = gridView.GetDataRow(e.RowHandle);

                List<DataRow> rows = new List<DataRow>();
                List<int> rowsNums = new List<int>();
                for (int i = 0; i < gridView.RowCount; i++)
                {
                    rows.Add(gridView.GetDataRow(i));
                }

                string curFilter = gridView.ActiveFilterString;
                gridView.ActiveFilterString = string.Empty;
                DevExpress.Data.ColumnSortOrder sorting = e.Column.SortOrder;
                e.Column.SortOrder = DevExpress.Data.ColumnSortOrder.None;

                int rowHandle = FindRowHandleByDataRow(firstRow);

                foreach (DataRow row in rows)
                {
                    rowsNums.Add(FindRowHandleByDataRow(row));
                }

                string errorMess = string.Empty;

                WaitManager.StopWait();

                if (CouldAllRowsBeEdited(rowsNums, e.Column.FieldName, ref rowHandle, e.Value, out errorMess))
                {
                    WaitManager.StartWait();
                    gridView.CellValueChanging -= gridView_CellValueChanging;

                    foreach (int row in rowsNums)
                    {
                        gridView.SetRowCellValue(row, e.Column.FieldName, e.Value);
                        UpdateRow(GetAimFromRow(row), row);
                    }

                    gridView.CellValueChanging += gridView_CellValueChanging;

                    DataRow focusedRow = gridView.GetDataRow(rowHandle);

                    gridView.BeginUpdate();
                    gridView.ActiveFilterString = curFilter;
                    e.Column.SortOrder = sorting;
                    gridView.EndUpdate();

                    gridView.FocusedRowHandle = FindRowHandleByDataRow(focusedRow);
                    gridView.SelectRow(FindRowHandleByDataRow(focusedRow));

                    WaitManager.StopWait();
                }
                else
                {
                    RefreshData();

                    foreach (int row in gridView.GetSelectedRows())
                    {
                        gridView.UnselectRow(row);
                    }

                    DataRow focusedRow = gridView.GetDataRow(rowHandle);

                    gridView.BeginUpdate();
                    gridView.ActiveFilterString = curFilter;
                    e.Column.SortOrder = sorting;
                    gridView.EndUpdate();

                    gridView.FocusedRowHandle = FindRowHandleByDataRow(focusedRow);
                    gridView.SelectRow(FindRowHandleByDataRow(focusedRow));

                    XtraMessageBox.Show("Невозможно редактировать все записи: " + errorMess);
                }
            }
            checkingMode = false;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                return;
            }

            checkingMode = true;

            if (Convert.ToBoolean(rbEditType.EditValue) && gridView.FocusedColumn.FieldName == MappedNames.KPIValue)
            {
                WaitManager.StartWait();

                DataRow firstRow = gridView.GetDataRow(e.RowHandle);

                List<DataRow> rows = new List<DataRow>();
                List<int> rowsNums = new List<int>();
                for (int i = 0; i < gridView.RowCount; i++)
                {
                    rows.Add(gridView.GetDataRow(i));
                }

                string curFilter = gridView.ActiveFilterString;
                gridView.ActiveFilterString = string.Empty;
                DevExpress.Data.ColumnSortOrder sorting = e.Column.SortOrder;
                e.Column.SortOrder = DevExpress.Data.ColumnSortOrder.None;

                int rowHandle = FindRowHandleByDataRow(firstRow);

                foreach (DataRow row in rows)
                {
                    rowsNums.Add(FindRowHandleByDataRow(row));
                }

                string errorMess = string.Empty;
                WaitManager.StopWait();

                if (CouldAllRowsBeEdited(rowsNums, e.Column.FieldName, ref rowHandle, e.Value, out errorMess))
                {
                    WaitManager.StartWait();

                    gridView.CellValueChanged -= gridView_CellValueChanged;

                    foreach (int row in rowsNums)
                    {
                        gridView.SetRowCellValue(row, e.Column.FieldName, e.Value);
                        UpdateRow(GetAimFromRow(row), row);
                    }

                    gridView.CellValueChanged += gridView_CellValueChanged;

                    DataRow focusedRow = gridView.GetDataRow(rowHandle);

                    gridView.BeginUpdate();
                    gridView.ActiveFilterString = curFilter;
                    e.Column.SortOrder = sorting;
                    gridView.EndUpdate();

                    gridView.FocusedRowHandle = FindRowHandleByDataRow(focusedRow);
                    gridView.SelectRow(FindRowHandleByDataRow(focusedRow));

                    WaitManager.StopWait();
                }
                else
                {
                    RefreshData(true);

                    foreach (int row in gridView.GetSelectedRows())
                    {
                        gridView.UnselectRow(row);
                    }

                    DataRow focusedRow = gridView.GetDataRow(rowHandle);

                    gridView.ActiveFilterString = curFilter;
                    e.Column.SortOrder = sorting;

                    gridView.FocusedRowHandle = FindRowHandleByDataRow(focusedRow);
                    gridView.SelectRow(FindRowHandleByDataRow(focusedRow));

                    XtraMessageBox.Show("Невозможно редактировать все записи: " + errorMess);
                }
            }

            checkingMode = false;
        }
        #endregion Event Handlers

        #region Private methods

        private void SetupUI()
        {
            DataAccessLayer.OpenConnection();
            btnNewAim.Enabled = btnCopyAim.Enabled = false;
            btnEditAim.Enabled = btnDeleteAim.Enabled = false;
            btnApproveStartDate.Enabled = btnApproveEndDate.Enabled = false;
            switch (AuthenticationManager.Instance.CurrentUserLevel)
            {
                case 5:
                    btnNewAim.Enabled = btnCopyAim.Enabled = btnEditAim.Enabled =
                        btnApproveStartDate.Enabled = btnApproveEndDate.Enabled = btnDeleteAim.Enabled = true;
                    break;
                case 4:
                    btnEditAim.Enabled = btnDeleteAim.Enabled = true;
                    break;
            }
            DataAccessLayer.CloseConnection();
        }

        private void SetupUI(int regionId)
        {
            if (regionId > 0)
            {
                switch (AuthenticationManager.Instance.CurrentUserLevel)
                {
                    case 4:
                        btnNewAim.Enabled = btnCopyAim.Enabled = btnEditAim.Enabled = btnDeleteAim.Enabled = true;
                        break;
                }
            }
            else
            {
                switch (AuthenticationManager.Instance.CurrentUserLevel)
                {
                    case 4:
                        btnNewAim.Enabled = btnCopyAim.Enabled = btnEditAim.Enabled = btnDeleteAim.Enabled = false;
                        break;
                }
            }
        }

        private void InitEvents()
        {
            //tvRegions.AfterSelect += new TreeViewEventHandler(tvRegions_AfterSelect);

            gridView.KeyDown += new KeyEventHandler(gridView_KeyDown);
            gridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(gridView_CustomColumnDisplayText);
            gridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(gridView_CustomDrawCell);

            btnNewAim.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnNewAim_ItemClick);
            btnCopyAim.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnCopyAim_ItemClick);
            btnEditAim.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnEditAim_ItemClick);
            btnApproveStartDate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnApproveStartDate_ItemClick);
            btnApproveEndDate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnApproveEndDate_ItemClick);
            btnDeleteAim.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnDeleteAim_ItemClick);
            btnRefreshAims.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnRefreshData_ItemClick);

            btnExportTo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnExportTo_ItemClick);
            btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnPrint_ItemClick);

            chkFilterOnlyApprovedAims.CheckedChanged += new EventHandler(chkFilter_CheckedChanged);
            chkFilterValidAimsForCurrentDate.CheckedChanged += new EventHandler(chkFilter_CheckedChanged);
            chkFilterByFocusedSKU.CheckedChanged += new EventHandler(chkFilter_CheckedChanged);

            deFilterAimsFrom.DateTimeChanged += new EventHandler(deFilter_DateTimeChanged);
            deFilterAimsTo.DateTimeChanged += new EventHandler(deFilter_DateTimeChanged);

            this.Invalidated += new InvalidateEventHandler(CascadedAimsModule_Invalidated);
        }

        private void RefreshData(bool fullRefresh = false)
        {
            AimsDataSet.AimDataTable lAimsData;

            if (!fullRefresh)
            {
                lAimsData = AimsData;
            }
            else
            {
                if (chkFilterByFocusedSKU.Checked)
                {
                    lAimsData = new CascadedAimsDataProvider().GetAimsFilteredByFocusedSKU().Aim;
                    aimsDataFilteredByFocusedSKU = lAimsData;
                }
                else
                {
                    lAimsData = new CascadedAimsDataProvider().GetData().Aim;
                    aimsData = lAimsData;
                }
            }

            gctlCascadedAims.BeginUpdate();

            if (string.IsNullOrEmpty(CurrentFilter))
            {
                lAimsData.DefaultView.RowFilter = string.Empty;
                gctlCascadedAims.DataSource = lAimsData.DefaultView;
                //gctlCascadedAims.DataSource = aimsData;
            }
            else
            {
                lAimsData.DefaultView.RowFilter = CurrentFilter;
                gctlCascadedAims.DataSource = lAimsData.DefaultView;
            }

            gctlCascadedAims.EndUpdate();

            //gridView.RefreshData();
        }

        private AimsDataSet.AimDataTable AimsData
        {
            get
            {
                AimsDataSet.AimDataTable result = null;

                if (chkFilterByFocusedSKU.Checked)
                {
                    if (null == aimsDataFilteredByFocusedSKU)
                        aimsDataFilteredByFocusedSKU = new CascadedAimsDataProvider().GetAimsFilteredByFocusedSKU().Aim;
                    result = aimsDataFilteredByFocusedSKU;
                }
                else
                {
                    if (null == aimsData)
                        aimsData = new CascadedAimsDataProvider().GetData().Aim;
                    result = aimsData;
                }
                return result;
            }
        }

        private void Localize()
        {
            SuperToolTip sttNewAim = new SuperToolTip();
            ToolTipItem ttiNewAim = new ToolTipItem();
            ttiNewAim.Text = Resource.NewAimButton;
            sttNewAim.Items.Add(ttiNewAim);
            sttNewAim.Appearance.Options.UseBackColor = true;
            sttNewAim.Appearance.BackColor = _tooltipColor;
            btnNewAim.SuperTip = sttNewAim;//new SuperToolTip();
            //btnNewAim.SuperTip.Items.Add(Resource.NewAimButton);

            SuperToolTip sttCopyAim = new SuperToolTip();
            ToolTipItem ttiCopyAim = new ToolTipItem();
            ttiCopyAim.Text = Resource.CopyAimButton;
            sttCopyAim.Items.Add(ttiCopyAim);
            sttCopyAim.Appearance.Options.UseBackColor = true;
            sttCopyAim.Appearance.BackColor = _tooltipColor;
            btnCopyAim.SuperTip = sttCopyAim;// new SuperToolTip();
            //btnCopyAim.SuperTip.Items.Add(Resource.CopyAimButton);

            SuperToolTip sttEditAim = new SuperToolTip();
            ToolTipItem ttiEditAim = new ToolTipItem();
            ttiEditAim.Text = Resource.EditAimButton;
            sttEditAim.Items.Add(ttiEditAim);
            sttEditAim.Appearance.Options.UseBackColor = true;
            sttEditAim.Appearance.BackColor = _tooltipColor;
            btnEditAim.SuperTip = sttEditAim;// new SuperToolTip();
            //btnEditAim.SuperTip.Items.Add(Resource.EditAimButton);


            SuperToolTip sttSetAimStartDate = new SuperToolTip();
            ToolTipItem ttiSetAimStartDate = new ToolTipItem();
            ttiSetAimStartDate.Text = Resource.SetAimStartDateButton;
            sttSetAimStartDate.Items.Add(ttiSetAimStartDate);
            sttSetAimStartDate.Appearance.Options.UseBackColor = true;
            sttSetAimStartDate.Appearance.BackColor = _tooltipColor;
            btnApproveStartDate.SuperTip = sttSetAimStartDate;// new SuperToolTip();
            //btnApproveStartDate.SuperTip.Items.Add(Resource.SetAimStartDateButton);

            SuperToolTip sttSetAimEndDate = new SuperToolTip();
            ToolTipItem ttiSetAimEndDate = new ToolTipItem();
            ttiSetAimEndDate.Text = Resource.SetAimEndDateButton;
            sttSetAimEndDate.Items.Add(ttiSetAimEndDate);
            sttSetAimEndDate.Appearance.Options.UseBackColor = true;
            sttSetAimEndDate.Appearance.BackColor = _tooltipColor;
            btnApproveEndDate.SuperTip = sttSetAimEndDate; // new SuperToolTip();
            //btnApproveEndDate.SuperTip.Items.Add(Resource.SetAimEndDateButton);

            SuperToolTip sttDeleteAim = new SuperToolTip();
            ToolTipItem ttiDeleteAim = new ToolTipItem();
            ttiDeleteAim.Text = Resource.DeleteAimButton;
            sttDeleteAim.Items.Add(ttiDeleteAim);
            sttDeleteAim.Appearance.Options.UseBackColor = true;
            sttDeleteAim.Appearance.BackColor = _tooltipColor;
            btnDeleteAim.SuperTip = sttDeleteAim;// new SuperToolTip();
            //btnDeleteAim.SuperTip.Items.Add(Resource.DeleteAimButton);

            SuperToolTip sttRefreshAims = new SuperToolTip();
            ToolTipItem ttiRefreshAims = new ToolTipItem();
            ttiRefreshAims.Text = Resource.RefreshAimButton;
            sttRefreshAims.Items.Add(ttiRefreshAims);
            sttRefreshAims.Appearance.Options.UseBackColor = true;
            sttRefreshAims.Appearance.BackColor = _tooltipColor;
            btnRefreshAims.SuperTip = sttRefreshAims;//new SuperToolTip();
            //btnRefreshAims.SuperTip.Items.Add(Resource.RefreshAimButton);

            SuperToolTip sttExport = new SuperToolTip();
            ToolTipItem ttiExport = new ToolTipItem();
            ttiExport.Text = Resource.ExportToExcelButton;
            sttExport.Items.Add(ttiExport);
            sttExport.Appearance.Options.UseBackColor = true;
            sttExport.Appearance.BackColor = _tooltipColor;
            btnExportTo.SuperTip = sttExport;

            SuperToolTip sttPrint = new SuperToolTip();
            ToolTipItem ttiPrint = new ToolTipItem();
            ttiPrint.Text = Resource.PrintButton;
            sttPrint.Items.Add(ttiPrint);
            sttPrint.Appearance.Options.UseBackColor = true;
            sttPrint.Appearance.BackColor = _tooltipColor;
            btnPrint.SuperTip = sttPrint;
        }

        private void InitializeMapping()
        {
            tableNameShowedColumnMapping.Clear();
            tableNameShowedColumnMapping.Add(MappedNames.CountryTable, MappedNames.CountryName);
            tableNameShowedColumnMapping.Add(MappedNames.RegionTable, MappedNames.RegionName);
            tableNameShowedColumnMapping.Add(MappedNames.DistrictTable, MappedNames.DistrictName);
            tableNameShowedColumnMapping.Add(MappedNames.CityTable, MappedNames.CityName);
        }

        private GridColumn[] GetGridColumns()
        {
            IList<GridColumn> columnsList = new List<GridColumn>();
            GridColumn[] columns = null;

            //gcAimId.Name = MappedNames.AimId;
            //columnsList.Add(gcAimId);

            gcProductCombineName.Name = MappedNames.ProductCombineName;
            gcProductCombineName.Width = 200;
            columnsList.Add(gcProductCombineName);

            gcDateStartPeriod.Name = MappedNames.DateStartPeriod;
            gcDateStartPeriod.Width = 90;
            gcDateStartPeriod.ColumnEdit = repositoryDateEdit;
            columnsList.Add(gcDateStartPeriod);

            gcIsStartDateApproved.Name = MappedNames.IsStartDateApproved;
            gcIsStartDateApproved.Width = 90;
            gcIsStartDateApproved.ColumnEdit = repositoryCheckEdit;
            columnsList.Add(gcIsStartDateApproved);

            gcDateEndPeriod.Name = MappedNames.DateEndPeriod;
            gcDateEndPeriod.Width = 90;
            gcDateEndPeriod.ColumnEdit = repositoryDateEdit;
            columnsList.Add(gcDateEndPeriod);

            gcIsEndDateApproved.Name = MappedNames.IsEndDateApproved;
            gcIsEndDateApproved.Width = 90;
            gcIsEndDateApproved.ColumnEdit = repositoryCheckEdit;
            columnsList.Add(gcIsEndDateApproved);

            gcKPI.Name = MappedNames.KPITypeName;
            gcKPI.Width = 200;
            gcKPI.ColumnEdit = repositoryComboBox;
            columnsList.Add(gcKPI);

            gcKPIValue.Name = MappedNames.KPIValue;
            gcKPIValue.Width = 90;
            columnsList.Add(gcKPIValue);

            gcDeviation.Name = MappedNames.Deviation;
            gcDeviation.Width = 90;
            columnsList.Add(gcDeviation);

            gcProximity.Name = MappedNames.ProximityName;
            gcProximity.Width = 150;
            columnsList.Add(gcProximity);

            gcSettlement.Name = MappedNames.SettlementName;
            gcSettlement.Width = 150;
            columnsList.Add(gcSettlement);

            gcComposedRegionName.Name = MappedNames.ComposedRegionName;
            gcComposedRegionName.Width = 120;
            columnsList.Add(gcComposedRegionName);

            gcComposedDistrictName.Name = MappedNames.ComposedDistrictName;
            gcComposedDistrictName.Width = 120;
            columnsList.Add(gcComposedDistrictName);

            gcComposedCityName.Name = MappedNames.ComposedCityName;
            gcComposedCityName.Width = 120;
            columnsList.Add(gcComposedCityName);

            gcDSM.Name = MappedNames.DSMName;
            gcDSM.Width = 150;
            columnsList.Add(gcDSM);

            gcSupervisor.Name = MappedNames.SupervisorName;
            gcSupervisor.Width = 150;
            columnsList.Add(gcSupervisor);

            gcOutletGroup.Name = MappedNames.OLGroupName;
            gcOutletGroup.Width = 100;
            columnsList.Add(gcOutletGroup);

            gcOutletType.Name = MappedNames.OLTypeName;
            gcOutletType.Width = 200;
            columnsList.Add(gcOutletType);

            gcOutletSubType.Name = MappedNames.OLSubTypeName;
            gcOutletSubType.Width = 200;
            columnsList.Add(gcOutletSubType);

            int i = 0;
            foreach (GridColumn column in columnsList)
            {
                column.OptionsColumn.AllowEdit = false;

                column.FieldName = column.Name;
                column.Caption = LocalizationProvider.GetText(column.Name);
                column.Visible = true;
                //column.Width = 50;
                column.VisibleIndex = i++;
            }

            gcDateStartPeriod.OptionsColumn.AllowEdit = true;
            gcIsStartDateApproved.OptionsColumn.AllowEdit = true;
            gcDateEndPeriod.OptionsColumn.AllowEdit = true;
            gcIsEndDateApproved.OptionsColumn.AllowEdit = true;
            gcKPI.OptionsColumn.AllowEdit = true;
            gcKPIValue.OptionsColumn.AllowEdit = true;

            if (AuthenticationManager.Instance.CurrentUserRole.Contains("PRICING_DIRECTOR"))
            {
                //gcIsStartDateApproved.OptionsColumn.ReadOnly = false;
                //gcIsEndDateApproved.OptionsColumn.ReadOnly = false;
            }

            if (columnsList.Count > 0)
            {
                columns = new GridColumn[columnsList.Count];
                columnsList.CopyTo(columns, 0);
            }

            return columns;
        }

        private string GetDatesFilter()
        {
            string filter = string.Empty;

            if (deFilterAimsFrom.Text.Length > 0)
            {
                filter = string.Format(
                    @"'{0}' <= {1} AND '{0}' <= {2}",
                    deFilterAimsFrom.Text,
                    MappedNames.DateStartPeriod,
                    MappedNames.DateEndPeriod
                    );
            }

            if (deFilterAimsTo.Text.Length > 0)
            {
                string filterTo = string.Format(
                       @"'{0}' >= {1} AND '{0}' >= {2}",
                       deFilterAimsTo.Text,
                       MappedNames.DateStartPeriod,
                       MappedNames.DateEndPeriod
                       );
                if (string.IsNullOrEmpty(filter))
                {
                    filter = filterTo;
                }
                else
                {
                    filter = string.Format("{0} AND {1}", filter, filterTo);
                }
            }

            if (chkFilterOnlyApprovedAims.Checked)
            {
                string filterApproved = string.Format(
                       @"'{0}' = {1} AND '{0}'= {2}",
                       "true",
                       MappedNames.IsStartDateApproved,
                       MappedNames.IsEndDateApproved
                       );
                if (string.IsNullOrEmpty(filter))
                {
                    filter = filterApproved;
                }
                else
                {
                    filter = string.Format("{0} AND {1}", filter, filterApproved);
                }
            }

            if (chkFilterValidAimsForCurrentDate.Checked)
            {
                string filterValid = string.Format(
                       @"'{0}' >= {1} AND '{0}' <= {2}",
                       DateTime.Now.ToString("d"),
                       MappedNames.DateStartPeriod,
                       MappedNames.DateEndPeriod
                       );
                if (string.IsNullOrEmpty(filter))
                {
                    filter = filterValid;
                }
                else
                {
                    filter = string.Format("{0} AND {1}", filter, filterValid);
                }
            }

            return filter;
        }

        private void ComposeRegionsFilter()
        {
            regionsFilter = string.Empty;
            if (_regionId != -1)
                regionsFilter = string.Format("{0} = {1}", MappedNames.RegionId, _regionId);
            if (_districtId != -1)
                regionsFilter += string.Format(" AND {0} = {1}", MappedNames.DistrictId, _districtId);
            if (_cityId != -1)
                regionsFilter += string.Format(" AND {0} = {1}", MappedNames.CityId, _cityId);
        }

        private void CheckNodes(TreeNode node)
        {
            if (node.Tag != null)
            {
                if (node.Tag is RegionsDataSet.CityRow)
                    _cityId = (node.Tag as RegionsDataSet.CityRow).CityIdColumn;
                else if (node.Tag is RegionsDataSet.DistrictRow)
                    _districtId = (node.Tag as RegionsDataSet.DistrictRow).DistrictIdColumn;
                else if (node.Tag is RegionsDataSet.RegionRow)
                    _regionId = (node.Tag as RegionsDataSet.RegionRow).RegionIdColumn;

                if (node.Parent != null)
                    CheckNodes(node.Parent);
            }
        }

        private void CopyToClipboard(int[] rowHandles)
        {
            if (rowHandles.Length > 0)
            {
                StringBuilder builder = new StringBuilder();
                foreach (int handle in rowHandles)
                {
                    object row = gridView.GetRow(handle);
                    string line = string.Empty;
                    string separator = '\t'.ToString();
                    foreach (GridColumn gridColumn in gridView.Columns)
                    {
                        string value = row is DataRow
                                        ? (row as DataRow)[gridColumn.FieldName].ToString()
                                        : row is DataRowView
                                            ? (row as DataRowView)[gridColumn.FieldName].ToString()
                                            : " ";
                        line = string.IsNullOrEmpty(line)
                            ? line = value
                            : string.Join(separator, new string[] { line, value });
                    }
                    builder.AppendLine(line);
                }

                Clipboard.SetText(builder.ToString());
            }
        }

        private string GetApprovePhrase(bool isCurrentDateApproved, int aimCount)
        {
            return isCurrentDateApproved
                ? "снять утверждение с " + (aimCount == 1 ? "цели" : "группы целей")
                : "утвердить " + (aimCount == 1 ? "цель" : "группу целей");
        }

        private bool ExistInFieldList(string fieldName)
        {
            try
            {
                return GetFieldsWithNotEmptyNames().Contains(fieldName); ;
            }
            catch
            {
                return false;
            }
        }

        private List<string> GetFieldsWithNotEmptyNames()
        {
            return new List<string>
                (
                    new string[] 
                    { 
                        MappedNames.ProductCombineName,
                        MappedNames.KPITypeName,
                        MappedNames.ProximityName,
                        MappedNames.SettlementName,
                        MappedNames.ComposedRegionName,
                        MappedNames.ComposedDistrictName,
                        MappedNames.ComposedCityName,
                        MappedNames.DSMName,
                        MappedNames.SupervisorName,
                        MappedNames.OutletGroupName,
                        MappedNames.OutletSubTypeName,
                        MappedNames.OutletTypeName
                    }
                );
        }

        private void ExportEngine(string fileName, DevExpress.XtraGrid.GridControl gridControl, GridColumn[] gridColumns)
        {
            if (gctlCascadedAims.DataSource != null && !string.IsNullOrEmpty(fileName))
            {
                DataConvertorHelper.HideEmptyColumns(gridControl, gridColumns);

                gridView.OptionsPrint.AutoWidth = false;
                gridControl.ExportToXls(fileName);
            }
        }

        private void PrintEngine(DevExpress.XtraGrid.GridControl gridControl, GridColumn[] gridColumns)
        {
            if (gctlCascadedAims.DataSource != null)
            {
                DataConvertorHelper.HideEmptyColumns(gridControl, gridColumns);

                gridView.OptionsPrint.AutoWidth = true;
                gridControl.ShowPrintPreview();
            }
        }

        private void UpdateRow(CascadedAimEntity aim, int rowHandle)
        {
            CascadedAimsDataProvider lDataProvider = new CascadedAimsDataProvider();
            AimsDataSet.AimDataTable lAimsData;
            DataRow lModifiedRow = gridView.GetDataRow(rowHandle);

            if (gctlCascadedAims.DataSource is DataView)
                lAimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
            else
                lAimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

            if (lAimsData == null)
                lAimsData = lDataProvider.GetData().Aim;

            DateTime now = DateTime.Now;

            aim.DateAdded = now;

            aim.DateApproveStartPeriod =
                aim.IsStartDateApproved != Convert.ToBoolean(lAimsData.Rows[rowHandle][MappedNames.IsStartDateApproved]) ? now : aim.DateApproveStartPeriod;

            aim.DateApproveEndPeriod =
                aim.IsEndDateApproved != Convert.ToBoolean(lAimsData.Rows[rowHandle][MappedNames.IsEndDateApproved]) ? now : aim.DateApproveEndPeriod;

            //aim.CreatedUserId = AuthenticationManager.Instance.CurrentUserId;
            //aim.ApprovedUserId = AuthenticationManager.Instance.CurrentUserId;

            aim.IsFocused = false;

            foreach (int i in KPIlist.Keys)
            {
                if (KPIlist[i] == aim.KPITypeName)
                {
                    aim.KPITypeId = i;
                }
            }

            //if (aim.RegionId == -1)
            //    aim.RegionId = _regionId;
            //if (aim.DistrictId == -1)
            //    aim.DistrictId = _districtId;
            //if (aim.CityId == -1)
            //    aim.CityId = _cityId;

            lModifiedRow.BeginEdit();
            aim.Fill(lModifiedRow);
            lModifiedRow.EndEdit();

            lDataProvider.UpdateData(lAimsData);

            RefreshData();

            if (!checkingMode)
            {
                gridView.FocusedRowHandle = rowHandle;
            }
        }

        private bool IsRowValid(int rowHandle, out string errorMess)
        {
            bool lHasPositionCoPpmM5 = false;//DataAccessProvider.HasUserPosition(DataAccessProvider.PositionCoPpmM5);
            GridView view = gridView;
            bool isRowVal = false;

            DateTime now = DateTime.Now.Date;
            CascadedAimEntity aim = GetAimFromRow(rowHandle);

            //if (Convert.ToDateTime(view.GetRowCellValue(rowHandle, MappedNames.DateStartPeriod)).Date < now && !lHasPositionCoPpmM5 && Convert.ToBoolean(view.GetRowCellValue(rowHandle, MappedNames.IsEndDateApproved)))
            //{
            //    errorMess = Resource.PeriodStartDateLessThenCurrentDate;
            //    return isRowVal;
            //}

            if (Convert.ToDateTime(view.GetRowCellValue(rowHandle, MappedNames.DateEndPeriod)).Date < now && !lHasPositionCoPpmM5 && Convert.ToBoolean(view.GetRowCellValue(rowHandle, MappedNames.IsEndDateApproved)))
            {
                errorMess = Resource.PeriodEndDateLessThenCurrentDate;
                return isRowVal;
            }

            if (!DateHelper.ValidatePeriod(Convert.ToDateTime(view.GetRowCellValue(rowHandle, MappedNames.DateStartPeriod)).Date, Convert.ToDateTime(view.GetRowCellValue(rowHandle, MappedNames.DateEndPeriod)).Date))
            {
                errorMess = Resource.IncorrectFromToDates;
                return isRowVal;
            }

            if (view.FocusedColumn.FieldName == MappedNames.KPIValue)
            {
                try
                {
                    double KPIvalue = Convert.ToDouble(view.GetRowCellValue(rowHandle, view.FocusedColumn), CultureInfo.InvariantCulture);

                    if (KPIvalue < 0)
                    {
                        errorMess = Resource.WrongKPIValue;
                        isRowVal = false;
                        return isRowVal;
                    }
                }
                catch (FormatException ex)
                {
                    errorMess = Resource.WrongKPIValue;
                    isRowVal = false;
                    return isRowVal;
                }
            }

            List<string> errors = new List<string>();

            DataMediator.CheckForCrossedAimsErrors(aim, errors);

            DataMediator.CheckForSameAimsErrors(aim, errors);

            if (errors.Count > 0)
            {
                string s = string.Empty;

                foreach (string str in errors)
                {
                    s += str + ";\n";
                }

                errorMess = s;
                return isRowVal;
            }

            errorMess = string.Empty;
            isRowVal = true;

            return isRowVal;
        }

        private CascadedAimEntity GetAimFromRow(int rowHandle)
        {
            CascadedAimEntity lAim = null;
            DataRow lFocusedRow = gridView.GetDataRow(rowHandle);
            if (lFocusedRow != null)
            {
                lAim = new CascadedAimEntity(lFocusedRow);
            }

            return lAim;
        }

        private bool CouldValueBeEdited(string fieldName, out string mess)
        {
            bool cancel = true;
            mess = "Нельзя редактировать поле " + LocalizationProvider.GetText(fieldName);

            GridView view = gridView as GridView;
            bool lHasPositionCoPpmM5 = false;//DataAccessProvider.HasUserPosition(DataAccessProvider.PositionCoPpmM5);

            //DateStartPeriod
            if (fieldName == MappedNames.DateStartPeriod && Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.IsStartDateApproved)))
            {
                cancel = false;
            }

            //DateEndPeriod
            if (fieldName == MappedNames.DateEndPeriod && Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            //IsStartDateApproved
            if (!lHasPositionCoPpmM5 && fieldName == MappedNames.IsStartDateApproved && Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.DateStartPeriod)) < DateTime.Now.Date)
            {
                cancel = false;
            }

            if (fieldName == MappedNames.IsStartDateApproved && Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            //IsEndDateApproved
            if (!lHasPositionCoPpmM5 && fieldName == MappedNames.IsEndDateApproved && Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.DateEndPeriod)) < DateTime.Now.Date && !Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            if (fieldName == MappedNames.IsEndDateApproved && Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.DateEndPeriod)) < Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.DateStartPeriod)))
            {
                cancel = false;
            }

            if (fieldName == MappedNames.IsEndDateApproved && !Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.IsStartDateApproved)))
            {
                cancel = false;
            }

            //KPIValue
            if (fieldName == MappedNames.KPIValue && Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            //KPIName
            if (fieldName == MappedNames.KPITypeName && Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            if (cancel)
            {
                mess = string.Empty;
            }
            return cancel;
        }

        private bool CouldAllRowsBeEdited(List<int> rows, string fieldName, ref int firstRowNum, object value, out string errorMess)
        {
            checkingMode = true;

            errorMess = string.Empty;
            bool incorRow = false;

            if (Convert.ToBoolean(rbEditType.EditValue))
            {
                gridView.CellValueChanging -= gridView_CellValueChanging;
                gridView.CellValueChanged -= gridView_CellValueChanged;

                prevValue = gridView.GetRowCellValue(firstRowNum, fieldName);
                gridView.SetRowCellValue(firstRowNum, fieldName, value);

                if (IsRowValid(firstRowNum, out errorMess))
                {
                    WaitManager.StartWait();
                    gridView.SetRowCellValue(firstRowNum, fieldName, prevValue);

                    foreach (int i in rows)
                    {
                        if (i != firstRowNum)
                        {
                            gridView.FocusedRowHandle = i;

                            if (CouldValueBeEdited(fieldName, out errorMess))
                            {
                                prevValue = gridView.GetRowCellValue(i, fieldName);
                                gridView.SetRowCellValue(i, fieldName, value);
                            }
                            else
                            {
                                incorRow = true;
                                firstRowNum = i;
                                break;
                            }

                            if (!IsRowValid(i, out errorMess))
                            {
                                gridView.SetRowCellValue(i, fieldName, prevValue);
                                incorRow = true;
                                firstRowNum = i;
                                break;
                            }

                            gridView.SetRowCellValue(i, fieldName, prevValue);
                        }
                    }

                    WaitManager.StopWait();
                }
                else
                {
                    gridView.SetRowCellValue(firstRowNum, fieldName, prevValue);
                    incorRow = true;
                }

                gridView.CellValueChanging += gridView_CellValueChanging;
                gridView.CellValueChanged += gridView_CellValueChanged;
            }

            checkingMode = false;

            return !incorRow;
        }

        private void SetKPIList()
        {
            //setup Combo Editor
            //KPIlist[0] = Resource.ComboBoxItemAnyValueText;

            foreach (KPIEntity kpi in new KPIDataProvider().GetList())
            {
                KPIlist.Add((int)kpi.Id, kpi.Name);
            }

            foreach (int key in KPIlist.Keys)
            {
                repositoryComboBox.Items.Add(KPIlist[key]);
            }
        }

        private int FindRowHandleByDataRow(DataRow row)
        {

            if (row != null)

                for (int i = 0; i < gridView.DataRowCount; i++)

                    if (gridView.GetDataRow(i) == row)

                        return i;

            return DevExpress.XtraGrid.GridControl.InvalidRowHandle;
        }
        #endregion Private methods

        #region Grid Columns definition
        private GridColumn gcAimId = new GridColumn();
        private GridColumn gcProductCombineName = new GridColumn();
        private GridColumn gcDateStartPeriod = new GridColumn();
        private GridColumn gcIsStartDateApproved = new GridColumn();
        private GridColumn gcDateEndPeriod = new GridColumn();
        private GridColumn gcIsEndDateApproved = new GridColumn();
        private GridColumn gcKPI = new GridColumn();
        private GridColumn gcKPIValue = new GridColumn();
        private GridColumn gcDeviation = new GridColumn();
        private GridColumn gcProximity = new GridColumn();
        private GridColumn gcSettlement = new GridColumn();
        private GridColumn gcDSM = new GridColumn();
        private GridColumn gcSupervisor = new GridColumn();
        private GridColumn gcOutletGroup = new GridColumn();
        private GridColumn gcOutletType = new GridColumn();
        private GridColumn gcOutletSubType = new GridColumn();
        private GridColumn gcComposedRegionName = new GridColumn();
        private GridColumn gcComposedDistrictName = new GridColumn();
        private GridColumn gcComposedCityName = new GridColumn();

        #endregion Grid Columns definition

        private void AttachTreeEvents(bool attach)
        {
            if (attach)
            {
                treeRegions.FocusedNodeChanged += treeRegions_FocusedNodeChanged;
            }
            else
            {
                treeRegions.FocusedNodeChanged -= treeRegions_FocusedNodeChanged;
            }
        }

        private void treeRegions_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            _regionId = -1;
            _districtId = -1;
            _cityId = -1;

            int lLevel = ConvertEx.ToInt(e.Node.GetValue(columnLevel));

            if (lLevel == 4)
            {
                if (e.Node.ParentNode == null || e.Node.ParentNode.ParentNode == null)
                    return;

                _cityId = ConvertEx.ToInt(e.Node.GetValue(columnItemId));
                _districtId = ConvertEx.ToInt(e.Node.ParentNode.GetValue(columnItemId));
                _regionId = ConvertEx.ToInt(e.Node.ParentNode.ParentNode.GetValue(columnItemId));
            }
            else if (lLevel == 3)
            {
                if (e.Node.ParentNode == null)
                    return;

                _districtId = ConvertEx.ToInt(e.Node.GetValue(columnItemId));
                _regionId = ConvertEx.ToInt(e.Node.ParentNode.GetValue(columnItemId));
            }
            else if (lLevel == 2)
                _regionId = ConvertEx.ToInt(e.Node.GetValue(columnItemId));

            ComposeRegionsFilter();
            SetupUI(_regionId);
            RefreshData();
        }
    }
}
