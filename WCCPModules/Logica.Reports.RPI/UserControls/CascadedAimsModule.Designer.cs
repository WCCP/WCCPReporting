﻿namespace Logica.Reports.RPI.UserControls
{
    partial class CascadedAimsModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CascadedAimsModule));
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.aimsBar = new DevExpress.XtraBars.Bar();
            this.btnNewAim = new DevExpress.XtraBars.BarButtonItem();
            this.btnCopyAim = new DevExpress.XtraBars.BarButtonItem();
            this.btnEditAim = new DevExpress.XtraBars.BarButtonItem();
            this.btnApproveStartDate = new DevExpress.XtraBars.BarButtonItem();
            this.btnApproveEndDate = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteAim = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefreshAims = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCollectionAims = new DevExpress.Utils.ImageCollection();
            this.cascadedAimsToolTipController = new DevExpress.Utils.ToolTipController();
            this.pnlTop = new DevExpress.XtraEditors.PanelControl();
            this.pnlMiddle = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerMiddle = new DevExpress.XtraEditors.SplitContainerControl();
            this.pnlRegionsTree = new DevExpress.XtraEditors.PanelControl();
            this.treeRegions = new DevExpress.XtraTreeList.TreeList();
            this.columnId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnItemId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tvRegions = new System.Windows.Forms.TreeView();
            this.pnlDataGrid = new DevExpress.XtraEditors.PanelControl();
            this.pnlDataGridBottom = new DevExpress.XtraEditors.PanelControl();
            this.tcFilters = new DevExpress.XtraTab.XtraTabControl();
            this.tpFilters = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.rbEditType = new DevExpress.XtraEditors.RadioGroup();
            this.chkFilterByFocusedSKU = new DevExpress.XtraEditors.CheckEdit();
            this.chkFilterValidAimsForCurrentDate = new DevExpress.XtraEditors.CheckEdit();
            this.chkFilterOnlyApprovedAims = new DevExpress.XtraEditors.CheckEdit();
            this.deFilterAimsTo = new DevExpress.XtraEditors.DateEdit();
            this.deFilterAimsFrom = new DevExpress.XtraEditors.DateEdit();
            this.lblFilterAimsTo = new DevExpress.XtraEditors.LabelControl();
            this.lblFilterAimsFrom = new DevExpress.XtraEditors.LabelControl();
            this.pnlDataGridCenter = new DevExpress.XtraEditors.PanelControl();
            this.gctlCascadedAims = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).BeginInit();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMiddle)).BeginInit();
            this.pnlMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMiddle)).BeginInit();
            this.splitContainerMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegionsTree)).BeginInit();
            this.pnlRegionsTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGrid)).BeginInit();
            this.pnlDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridBottom)).BeginInit();
            this.pnlDataGridBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcFilters)).BeginInit();
            this.tcFilters.SuspendLayout();
            this.tpFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterByFocusedSKU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterValidAimsForCurrentDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterOnlyApprovedAims.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridCenter)).BeginInit();
            this.pnlDataGridCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gctlCascadedAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryComboBox)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.aimsBar});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageCollectionAims;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnNewAim,
            this.btnCopyAim,
            this.btnEditAim,
            this.btnApproveStartDate,
            this.btnApproveEndDate,
            this.btnDeleteAim,
            this.btnRefreshAims,
            this.btnExportTo,
            this.btnPrint});
            this.barManager1.LargeImages = this.imageCollectionAims;
            this.barManager1.MaxItemId = 26;
            this.barManager1.ToolTipController = this.cascadedAimsToolTipController;
            // 
            // aimsBar
            // 
            this.aimsBar.BarAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.aimsBar.BarAppearance.Normal.Options.UseFont = true;
            this.aimsBar.BarName = "Tools";
            this.aimsBar.DockCol = 0;
            this.aimsBar.DockRow = 0;
            this.aimsBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.aimsBar.FloatLocation = new System.Drawing.Point(650, 209);
            this.aimsBar.FloatSize = new System.Drawing.Size(46, 300);
            this.aimsBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNewAim),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCopyAim, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEditAim),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnApproveStartDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnApproveEndDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDeleteAim, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefreshAims),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportTo),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint)});
            this.aimsBar.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.aimsBar.Text = "Tools";
            // 
            // btnNewAim
            // 
            this.btnNewAim.Id = 0;
            this.btnNewAim.ImageIndex = 0;
            this.btnNewAim.ImageIndexDisabled = 7;
            this.btnNewAim.LargeImageIndex = 0;
            this.btnNewAim.LargeImageIndexDisabled = 7;
            this.btnNewAim.Name = "btnNewAim";
            this.btnNewAim.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnCopyAim
            // 
            this.btnCopyAim.Id = 1;
            this.btnCopyAim.ImageIndex = 1;
            this.btnCopyAim.ImageIndexDisabled = 8;
            this.btnCopyAim.LargeImageIndex = 1;
            this.btnCopyAim.LargeImageIndexDisabled = 8;
            this.btnCopyAim.Name = "btnCopyAim";
            this.btnCopyAim.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnEditAim
            // 
            this.btnEditAim.Id = 2;
            this.btnEditAim.ImageIndex = 2;
            this.btnEditAim.ImageIndexDisabled = 9;
            this.btnEditAim.LargeImageIndex = 2;
            this.btnEditAim.LargeImageIndexDisabled = 9;
            this.btnEditAim.Name = "btnEditAim";
            this.btnEditAim.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnApproveStartDate
            // 
            this.btnApproveStartDate.Id = 3;
            this.btnApproveStartDate.ImageIndex = 3;
            this.btnApproveStartDate.ImageIndexDisabled = 10;
            this.btnApproveStartDate.LargeImageIndex = 3;
            this.btnApproveStartDate.LargeImageIndexDisabled = 10;
            this.btnApproveStartDate.Name = "btnApproveStartDate";
            this.btnApproveStartDate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnApproveEndDate
            // 
            this.btnApproveEndDate.Id = 4;
            this.btnApproveEndDate.ImageIndex = 4;
            this.btnApproveEndDate.ImageIndexDisabled = 11;
            this.btnApproveEndDate.LargeImageIndex = 4;
            this.btnApproveEndDate.LargeImageIndexDisabled = 11;
            this.btnApproveEndDate.Name = "btnApproveEndDate";
            this.btnApproveEndDate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnDeleteAim
            // 
            this.btnDeleteAim.Id = 5;
            this.btnDeleteAim.ImageIndex = 5;
            this.btnDeleteAim.ImageIndexDisabled = 12;
            this.btnDeleteAim.LargeImageIndex = 5;
            this.btnDeleteAim.LargeImageIndexDisabled = 12;
            this.btnDeleteAim.Name = "btnDeleteAim";
            this.btnDeleteAim.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnRefreshAims
            // 
            this.btnRefreshAims.Id = 6;
            this.btnRefreshAims.ImageIndex = 6;
            this.btnRefreshAims.ImageIndexDisabled = 13;
            this.btnRefreshAims.LargeImageIndex = 6;
            this.btnRefreshAims.LargeImageIndexDisabled = 13;
            this.btnRefreshAims.Name = "btnRefreshAims";
            // 
            // btnExportTo
            // 
            this.btnExportTo.Id = 7;
            this.btnExportTo.LargeImageIndex = 14;
            this.btnExportTo.Name = "btnExportTo";
            // 
            // btnPrint
            // 
            this.btnPrint.Id = 25;
            this.btnPrint.LargeImageIndex = 15;
            this.btnPrint.Name = "btnPrint";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.MinimumSize = new System.Drawing.Size(431, 14);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(890, 35);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(890, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 419);
            this.barDockControlBottom.Size = new System.Drawing.Size(890, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 419);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(890, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 419);
            // 
            // imageCollectionAims
            // 
            this.imageCollectionAims.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollectionAims.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionAims.ImageStream")));
            this.imageCollectionAims.Images.SetKeyName(0, "doc_24.png");
            this.imageCollectionAims.Images.SetKeyName(1, "doc_add_24.png");
            this.imageCollectionAims.Images.SetKeyName(2, "doc_edit_24.png");
            this.imageCollectionAims.Images.SetKeyName(3, "doc_back_24.png");
            this.imageCollectionAims.Images.SetKeyName(4, "doc_next_24.png");
            this.imageCollectionAims.Images.SetKeyName(5, "del_24.png");
            this.imageCollectionAims.Images.SetKeyName(6, "refresh_24.png");
            this.imageCollectionAims.Images.SetKeyName(7, "doc_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(8, "doc_add_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(9, "doc_edit_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(10, "doc_back_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(11, "doc_next_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(12, "del_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(13, "refresh_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(14, "Excel_24.png");
            this.imageCollectionAims.Images.SetKeyName(15, "print_24.png");
            // 
            // cascadedAimsToolTipController
            // 
            this.cascadedAimsToolTipController.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.cascadedAimsToolTipController.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cascadedAimsToolTipController.Appearance.Options.UseBackColor = true;
            this.cascadedAimsToolTipController.ToolTipType = DevExpress.Utils.ToolTipType.Standard;
            // 
            // pnlTop
            // 
            this.pnlTop.AutoSize = true;
            this.pnlTop.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTop.Controls.Add(this.standaloneBarDockControl1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.MinimumSize = new System.Drawing.Size(0, 35);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(890, 35);
            this.pnlTop.TabIndex = 4;
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMiddle.Controls.Add(this.splitContainerMiddle);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Location = new System.Drawing.Point(0, 35);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(890, 384);
            this.pnlMiddle.TabIndex = 5;
            // 
            // splitContainerMiddle
            // 
            this.splitContainerMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerMiddle.Location = new System.Drawing.Point(0, 0);
            this.splitContainerMiddle.Name = "splitContainerMiddle";
            this.splitContainerMiddle.Panel1.Controls.Add(this.pnlRegionsTree);
            this.splitContainerMiddle.Panel1.Text = "Panel1";
            this.splitContainerMiddle.Panel2.Controls.Add(this.pnlDataGrid);
            this.splitContainerMiddle.Panel2.Text = "Panel2";
            this.splitContainerMiddle.Size = new System.Drawing.Size(890, 384);
            this.splitContainerMiddle.SplitterPosition = 209;
            this.splitContainerMiddle.TabIndex = 0;
            this.splitContainerMiddle.Text = "splitContainerControl1";
            // 
            // pnlRegionsTree
            // 
            this.pnlRegionsTree.AutoSize = true;
            this.pnlRegionsTree.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlRegionsTree.Controls.Add(this.treeRegions);
            this.pnlRegionsTree.Controls.Add(this.tvRegions);
            this.pnlRegionsTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRegionsTree.Location = new System.Drawing.Point(0, 0);
            this.pnlRegionsTree.Name = "pnlRegionsTree";
            this.pnlRegionsTree.Size = new System.Drawing.Size(209, 384);
            this.pnlRegionsTree.TabIndex = 2;
            // 
            // treeRegions
            // 
            this.treeRegions.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Highlight;
            this.treeRegions.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.treeRegions.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeRegions.Appearance.FocusedRow.Options.UseForeColor = true;
            this.treeRegions.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.treeRegions.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnId,
            this.columnParentId,
            this.columnName,
            this.columnLevel,
            this.columnItemId});
            this.treeRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeRegions.Location = new System.Drawing.Point(2, 2);
            this.treeRegions.Name = "treeRegions";
            this.treeRegions.OptionsBehavior.AutoPopulateColumns = false;
            this.treeRegions.OptionsBehavior.Editable = false;
            this.treeRegions.OptionsMenu.EnableColumnMenu = false;
            this.treeRegions.OptionsMenu.EnableFooterMenu = false;
            this.treeRegions.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeRegions.OptionsView.ShowColumns = false;
            this.treeRegions.OptionsView.ShowHorzLines = false;
            this.treeRegions.OptionsView.ShowIndicator = false;
            this.treeRegions.OptionsView.ShowVertLines = false;
            this.treeRegions.RootValue = null;
            this.treeRegions.Size = new System.Drawing.Size(205, 380);
            this.treeRegions.TabIndex = 1;
            this.treeRegions.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeRegions_FocusedNodeChanged);
            // 
            // columnId
            // 
            this.columnId.AllowIncrementalSearch = false;
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            // 
            // columnParentId
            // 
            this.columnParentId.Caption = "Parent ID";
            this.columnParentId.FieldName = "ParentID";
            this.columnParentId.Name = "columnParentId";
            this.columnParentId.OptionsColumn.AllowEdit = false;
            // 
            // columnName
            // 
            this.columnName.Caption = "Name";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            // 
            // columnLevel
            // 
            this.columnLevel.Caption = "Level";
            this.columnLevel.FieldName = "Level";
            this.columnLevel.Name = "columnLevel";
            this.columnLevel.OptionsColumn.AllowEdit = false;
            // 
            // columnItemId
            // 
            this.columnItemId.Caption = "Item ID";
            this.columnItemId.FieldName = "ItemID";
            this.columnItemId.Name = "columnItemId";
            this.columnItemId.OptionsColumn.AllowEdit = false;
            // 
            // tvRegions
            // 
            this.tvRegions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvRegions.Enabled = false;
            this.tvRegions.Location = new System.Drawing.Point(3, 3);
            this.tvRegions.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.tvRegions.Name = "tvRegions";
            this.tvRegions.Size = new System.Drawing.Size(203, 174);
            this.tvRegions.TabIndex = 0;
            this.tvRegions.Visible = false;
            // 
            // pnlDataGrid
            // 
            this.pnlDataGrid.AutoSize = true;
            this.pnlDataGrid.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlDataGrid.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDataGrid.Controls.Add(this.pnlDataGridBottom);
            this.pnlDataGrid.Controls.Add(this.pnlDataGridCenter);
            this.pnlDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDataGrid.Location = new System.Drawing.Point(0, 0);
            this.pnlDataGrid.MinimumSize = new System.Drawing.Size(150, 200);
            this.pnlDataGrid.Name = "pnlDataGrid";
            this.pnlDataGrid.Size = new System.Drawing.Size(676, 384);
            this.pnlDataGrid.TabIndex = 2;
            // 
            // pnlDataGridBottom
            // 
            this.pnlDataGridBottom.Controls.Add(this.tcFilters);
            this.pnlDataGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDataGridBottom.Location = new System.Drawing.Point(0, 269);
            this.pnlDataGridBottom.Name = "pnlDataGridBottom";
            this.pnlDataGridBottom.Size = new System.Drawing.Size(676, 115);
            this.pnlDataGridBottom.TabIndex = 7;
            // 
            // tcFilters
            // 
            this.tcFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcFilters.Location = new System.Drawing.Point(2, 2);
            this.tcFilters.MinimumSize = new System.Drawing.Size(422, 112);
            this.tcFilters.Name = "tcFilters";
            this.tcFilters.SelectedTabPage = this.tpFilters;
            this.tcFilters.Size = new System.Drawing.Size(672, 112);
            this.tcFilters.TabIndex = 7;
            this.tcFilters.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpFilters});
            // 
            // tpFilters
            // 
            this.tpFilters.Controls.Add(this.labelControl1);
            this.tpFilters.Controls.Add(this.rbEditType);
            this.tpFilters.Controls.Add(this.chkFilterByFocusedSKU);
            this.tpFilters.Controls.Add(this.chkFilterValidAimsForCurrentDate);
            this.tpFilters.Controls.Add(this.chkFilterOnlyApprovedAims);
            this.tpFilters.Controls.Add(this.deFilterAimsTo);
            this.tpFilters.Controls.Add(this.deFilterAimsFrom);
            this.tpFilters.Controls.Add(this.lblFilterAimsTo);
            this.tpFilters.Controls.Add(this.lblFilterAimsFrom);
            this.tpFilters.Name = "tpFilters";
            this.tpFilters.Size = new System.Drawing.Size(666, 84);
            this.tpFilters.Text = "Фильтры";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(478, 11);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "Режим редактирования";
            // 
            // rbEditType
            // 
            this.rbEditType.EditValue = false;
            this.rbEditType.Location = new System.Drawing.Point(478, 30);
            this.rbEditType.Name = "rbEditType";
            this.rbEditType.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.rbEditType.Properties.Appearance.Options.UseBackColor = true;
            this.rbEditType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Одиночный"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Груповой")});
            this.rbEditType.Size = new System.Drawing.Size(165, 23);
            this.rbEditType.TabIndex = 11;
            // 
            // chkFilterByFocusedSKU
            // 
            this.chkFilterByFocusedSKU.Location = new System.Drawing.Point(293, 35);
            this.chkFilterByFocusedSKU.MenuManager = this.barManager1;
            this.chkFilterByFocusedSKU.Name = "chkFilterByFocusedSKU";
            this.chkFilterByFocusedSKU.Properties.Caption = "Цели по фокусным СКЮ";
            this.chkFilterByFocusedSKU.Size = new System.Drawing.Size(151, 19);
            this.chkFilterByFocusedSKU.TabIndex = 6;
            // 
            // chkFilterValidAimsForCurrentDate
            // 
            this.chkFilterValidAimsForCurrentDate.Location = new System.Drawing.Point(7, 60);
            this.chkFilterValidAimsForCurrentDate.MenuManager = this.barManager1;
            this.chkFilterValidAimsForCurrentDate.Name = "chkFilterValidAimsForCurrentDate";
            this.chkFilterValidAimsForCurrentDate.Properties.Caption = "Показывать цели действующие на текущую дату";
            this.chkFilterValidAimsForCurrentDate.Size = new System.Drawing.Size(388, 19);
            this.chkFilterValidAimsForCurrentDate.TabIndex = 5;
            // 
            // chkFilterOnlyApprovedAims
            // 
            this.chkFilterOnlyApprovedAims.Location = new System.Drawing.Point(7, 35);
            this.chkFilterOnlyApprovedAims.MenuManager = this.barManager1;
            this.chkFilterOnlyApprovedAims.Name = "chkFilterOnlyApprovedAims";
            this.chkFilterOnlyApprovedAims.Properties.Caption = "Показывать только утвержденные цели";
            this.chkFilterOnlyApprovedAims.Size = new System.Drawing.Size(247, 19);
            this.chkFilterOnlyApprovedAims.TabIndex = 4;
            // 
            // deFilterAimsTo
            // 
            this.deFilterAimsTo.EditValue = null;
            this.deFilterAimsTo.Location = new System.Drawing.Point(295, 9);
            this.deFilterAimsTo.MenuManager = this.barManager1;
            this.deFilterAimsTo.Name = "deFilterAimsTo";
            this.deFilterAimsTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFilterAimsTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFilterAimsTo.Size = new System.Drawing.Size(100, 20);
            this.deFilterAimsTo.TabIndex = 3;
            // 
            // deFilterAimsFrom
            // 
            this.deFilterAimsFrom.EditValue = null;
            this.deFilterAimsFrom.Location = new System.Drawing.Point(138, 9);
            this.deFilterAimsFrom.MenuManager = this.barManager1;
            this.deFilterAimsFrom.Name = "deFilterAimsFrom";
            this.deFilterAimsFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFilterAimsFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFilterAimsFrom.Size = new System.Drawing.Size(100, 20);
            this.deFilterAimsFrom.TabIndex = 2;
            // 
            // lblFilterAimsTo
            // 
            this.lblFilterAimsTo.Location = new System.Drawing.Point(262, 12);
            this.lblFilterAimsTo.Name = "lblFilterAimsTo";
            this.lblFilterAimsTo.Size = new System.Drawing.Size(12, 13);
            this.lblFilterAimsTo.TabIndex = 1;
            this.lblFilterAimsTo.Text = "по";
            // 
            // lblFilterAimsFrom
            // 
            this.lblFilterAimsFrom.Location = new System.Drawing.Point(9, 12);
            this.lblFilterAimsFrom.Name = "lblFilterAimsFrom";
            this.lblFilterAimsFrom.Size = new System.Drawing.Size(109, 13);
            this.lblFilterAimsFrom.TabIndex = 0;
            this.lblFilterAimsFrom.Text = "Цели действующие с";
            // 
            // pnlDataGridCenter
            // 
            this.pnlDataGridCenter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDataGridCenter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlDataGridCenter.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDataGridCenter.Controls.Add(this.gctlCascadedAims);
            this.pnlDataGridCenter.Location = new System.Drawing.Point(2, 3);
            this.pnlDataGridCenter.Name = "pnlDataGridCenter";
            this.pnlDataGridCenter.Size = new System.Drawing.Size(673, 263);
            this.pnlDataGridCenter.TabIndex = 3;
            // 
            // gctlCascadedAims
            // 
            this.gctlCascadedAims.Cursor = System.Windows.Forms.Cursors.Default;
            this.gctlCascadedAims.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gctlCascadedAims.Location = new System.Drawing.Point(0, 0);
            this.gctlCascadedAims.MainView = this.gridView;
            this.gctlCascadedAims.MinimumSize = new System.Drawing.Size(414, 84);
            this.gctlCascadedAims.Name = "gctlCascadedAims";
            this.gctlCascadedAims.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryDateEdit,
            this.repositoryCheckEdit,
            this.repositoryComboBox});
            this.gctlCascadedAims.Size = new System.Drawing.Size(673, 263);
            this.gctlCascadedAims.TabIndex = 2;
            this.gctlCascadedAims.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            this.gctlCascadedAims.DoubleClick += new System.EventHandler(this.gctlCascadedAims_DoubleClick);
            // 
            // gridView
            // 
            this.gridView.GridControl = this.gctlCascadedAims;
            this.gridView.Name = "gridView";
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            this.gridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanging);
            this.gridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView_ValidateRow);
            this.gridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView_ValidatingEditor);
            // 
            // repositoryDateEdit
            // 
            this.repositoryDateEdit.AutoHeight = false;
            this.repositoryDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryDateEdit.Name = "repositoryDateEdit";
            // 
            // repositoryCheckEdit
            // 
            this.repositoryCheckEdit.AutoHeight = false;
            this.repositoryCheckEdit.DisplayValueChecked = "Да";
            this.repositoryCheckEdit.DisplayValueUnchecked = "Нет";
            this.repositoryCheckEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryCheckEdit.Name = "repositoryCheckEdit";
            // 
            // repositoryComboBox
            // 
            this.repositoryComboBox.AutoHeight = false;
            this.repositoryComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryComboBox.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.Value;
            this.repositoryComboBox.Name = "repositoryComboBox";
            // 
            // CascadedAimsModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.pnlMiddle);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "CascadedAimsModule";
            this.Size = new System.Drawing.Size(890, 419);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).EndInit();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMiddle)).EndInit();
            this.pnlMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMiddle)).EndInit();
            this.splitContainerMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegionsTree)).EndInit();
            this.pnlRegionsTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGrid)).EndInit();
            this.pnlDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridBottom)).EndInit();
            this.pnlDataGridBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcFilters)).EndInit();
            this.tcFilters.ResumeLayout(false);
            this.tpFilters.ResumeLayout(false);
            this.tpFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterByFocusedSKU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterValidAimsForCurrentDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterOnlyApprovedAims.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridCenter)).EndInit();
            this.pnlDataGridCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gctlCascadedAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryComboBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar aimsBar;
        private DevExpress.XtraBars.BarButtonItem btnNewAim;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnCopyAim;
        private DevExpress.XtraBars.BarButtonItem btnEditAim;
        private DevExpress.XtraBars.BarButtonItem btnApproveStartDate;
        private DevExpress.XtraBars.BarButtonItem btnApproveEndDate;
        private DevExpress.XtraBars.BarButtonItem btnDeleteAim;
        private DevExpress.Utils.ImageCollection imageCollectionAims;
        private DevExpress.XtraBars.BarButtonItem btnRefreshAims;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraEditors.PanelControl pnlMiddle;
        private DevExpress.XtraEditors.PanelControl pnlTop;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerMiddle;
        private DevExpress.XtraEditors.PanelControl pnlRegionsTree;
        private System.Windows.Forms.TreeView tvRegions;
        private DevExpress.XtraEditors.PanelControl pnlDataGrid;
        private DevExpress.XtraEditors.PanelControl pnlDataGridCenter;
        private DevExpress.XtraGrid.GridControl gctlCascadedAims;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.PanelControl pnlDataGridBottom;
        private DevExpress.XtraTab.XtraTabControl tcFilters;
        private DevExpress.XtraTab.XtraTabPage tpFilters;
        private DevExpress.XtraEditors.CheckEdit chkFilterValidAimsForCurrentDate;
        private DevExpress.XtraEditors.CheckEdit chkFilterOnlyApprovedAims;
        private DevExpress.XtraEditors.DateEdit deFilterAimsTo;
        private DevExpress.XtraEditors.DateEdit deFilterAimsFrom;
        private DevExpress.XtraEditors.LabelControl lblFilterAimsTo;
        private DevExpress.XtraEditors.LabelControl lblFilterAimsFrom;
        private DevExpress.XtraEditors.CheckEdit chkFilterByFocusedSKU;
        private DevExpress.Utils.ToolTipController cascadedAimsToolTipController;
        private DevExpress.XtraBars.BarButtonItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryCheckEdit;
        private DevExpress.XtraEditors.RadioGroup rbEditType;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryComboBox;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTreeList.TreeList treeRegions;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnItemId;
        
    }


}
