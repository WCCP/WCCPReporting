﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using Logica.Reports.RPI.Forms;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.Authentication;
using DevExpress.XtraEditors.Repository;
using WccpReporting;
using Logica.Reports.Common.WaitWindow;

namespace Logica.Reports.RPI.UserControls
{
    /// <summary>
    /// User control which contains data for report "List of Outlets with Problems"
    /// </summary>
    public partial class ListOfPOCWithProblemsReport : UserControl, IRPIModule
    {
        #region Private members

        private bool _isInitForTheFirstTime = true;

        private DateTime _reportStartDate;
        private DateTime _reportEndDate;
        private long _routeId;

        public DSMEntity _dsm;
        public SupervisorEntity _supervisor;
        public MerchandiserEntity _merchandiser;
        public RouteEntity _route;

        private string _currentFilter = string.Empty;
        private Color _tooltipColor = Color.FromArgb(255, 255, 231);

        #endregion Private members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public ListOfPOCWithProblemsReport()
        {
            InitializeComponent();

            InitEvents();

            Localize();

            SetupColumns();
        }

        #endregion Constructors

        #region Public Properties

        /// <summary>
        ///  Returns name of the module
        /// </summary>
        public string ModuleName
        {
            get { return Resource.ListOfPOCWithProblemsReport; }
        }

        /// <summary>
        /// Start Date of Report
        /// </summary>
        public DateTime ReportStartDate
        {
            get { return _reportStartDate; }
            set { _reportStartDate = value; }
        }

        /// <summary>
        /// End Date of Report
        /// </summary>
        public DateTime ReportEndDate
        {
            get { return _reportEndDate; }
            set { _reportEndDate = value; }
        }

        /// <summary>
        /// Route id
        /// </summary>
        public long RouteId
        {
            get { return _routeId; }
            set { _routeId = value; }
        }

        public DSMEntity DSM { get { return _dsm; } set { _dsm = value; } }
        public SupervisorEntity Supervisor { get { return _supervisor; } set { _supervisor = value; } }
        public MerchandiserEntity Merchandiser { get { return _merchandiser; } set { _merchandiser = value; } }
        public RouteEntity Route { get { return _route; } set { _route = value; ; } }

        public string CurrentFilter
        {
            get 
            {
                _currentFilter = string.Empty;

                if (_dsm != null)
                {
                    _currentFilter = GetFilterWithCondition(_currentFilter, string.Format("{0} = {1}", MappedNames.DSMId, _dsm.Id), "AND");
                }
                if (_supervisor != null)
                {
                    _currentFilter = GetFilterWithCondition(_currentFilter, string.Format("{0} = {1}", MappedNames.SupervisorId, _supervisor.Id), "AND");
                }
                if (_merchandiser != null)
                {
                    _currentFilter = GetFilterWithCondition(_currentFilter, string.Format("{0} = {1}", MappedNames.MerchId, _merchandiser.Id), "AND");
                }
                if (_route != null)
                {
                    _currentFilter = GetFilterWithCondition(_currentFilter, string.Format("{0} = {1}", MappedNames.RouteId, _route.Id), "AND");
                }

                return _currentFilter; 
            }
            set { _currentFilter = value; }
        }

        #endregion Public Properties

        #region Public Methods

        #endregion Public Methods

        #region Private Methods

        private void SetupUI()
        {

        }

        private void InitEvents()
        {
            btnRefreshAims.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnRefreshData_ItemClick);

            btnExportTo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnExportTo_ItemClick);
            btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnPrint_ItemClick);

            gridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(gridView_CustomColumnDisplayText);

            this.Paint += new PaintEventHandler(ListOfPOCWithProblemsReport_Paint);
        }

        private void RefreshData()
        {
            WaitManager.StartWait();

            try
            {
                POCWithProblemsDataProvider dataProvider = new POCWithProblemsDataProvider();

                POCWithProblemsDataSet.POCDataTable pocData = dataProvider.GetData(ReportStartDate, ReportEndDate, ReportOptions.RouteId).POC;

                gctlPOCwithProblems.BeginUpdate();

                gctlPOCwithProblems.DataSource = pocData;

                if (string.IsNullOrEmpty(CurrentFilter))
                    gctlPOCwithProblems.DataSource = pocData;
                else
                {
                    pocData.DefaultView.RowFilter = CurrentFilter;
                    gctlPOCwithProblems.DataSource = pocData.DefaultView;
                }
                
                gctlPOCwithProblems.EndUpdate();
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        private void Localize()
        {
            SuperToolTip sttRefreshAims = new SuperToolTip();
            ToolTipItem ttiRefreshAims = new ToolTipItem();
            ttiRefreshAims.Text = Resource.RefreshDataButton;
            sttRefreshAims.Items.Add(ttiRefreshAims);
            sttRefreshAims.Appearance.Options.UseBackColor = true;
            sttRefreshAims.Appearance.BackColor = _tooltipColor;
            btnRefreshAims.SuperTip = sttRefreshAims;

            SuperToolTip sttReportSetting = new SuperToolTip();
            ToolTipItem ttiReportSetting = new ToolTipItem();
            ttiReportSetting.Text = Resource.ReportParametersButton;
            sttReportSetting.Items.Add(ttiReportSetting);
            sttReportSetting.Appearance.Options.UseBackColor = true;
            sttReportSetting.Appearance.BackColor = _tooltipColor;
            btnShowSettings.SuperTip = sttReportSetting;

            SuperToolTip sttExport = new SuperToolTip();
            ToolTipItem ttiExport = new ToolTipItem();
            ttiExport.Text = Resource.ExportToButton;
            sttExport.Items.Add(ttiExport);
            sttExport.Appearance.Options.UseBackColor = true;
            sttExport.Appearance.BackColor = _tooltipColor;
            btnExportTo.SuperTip = sttExport;

            SuperToolTip sttPrint = new SuperToolTip();
            ToolTipItem ttiPrint = new ToolTipItem();
            ttiPrint.Text = Resource.PrintButton;
            sttPrint.Items.Add(ttiPrint);
            sttPrint.Appearance.Options.UseBackColor = true;
            sttPrint.Appearance.BackColor = _tooltipColor;
            btnPrint.SuperTip = sttPrint;        
        }

        private void SetupColumns()
        {
            gridView.Columns.Clear();

            gridView.Columns.AddRange(GetGridColumns());

            gridView.OptionsSelection.MultiSelect = true;

            RepositoryItemTextEdit itemTextEditIsApproved = new RepositoryItemTextEdit();
            gctlPOCwithProblems.RepositoryItems.Add(itemTextEditIsApproved);

            foreach (GridColumn column in gridView.Columns)
            {
                if (column.FieldName == MappedNames.HasPriceBeenCut ||
                    column.FieldName == MappedNames.IsPriceMatchForRPTC ||
                    column.FieldName == MappedNames.OutletMarkup
                    )
                {
                    column.ColumnEdit = itemTextEditIsApproved;
                }

                if (column.FieldName == MappedNames.PTC ||
                    column.FieldName == MappedNames.RPTC ||
                    column.FieldName == MappedNames.TotalSalesDAL
                    )
                {
                    column.DisplayFormat.FormatType = FormatType.Custom;
                    column.DisplayFormat.FormatString = "#0.00";
                }

                if (column.FieldName == MappedNames.CurrentMarkup ||
                        column.FieldName == MappedNames.PreviousMarkup
                        )
                {
                    column.DisplayFormat.FormatType = FormatType.Custom;
                    column.DisplayFormat.FormatString = "#0.00";
                }
            }
        }

        private GridColumn[] GetGridColumns()
        {
            IList<GridColumn> columnsList = new List<GridColumn>();
            GridColumn[] columns = null;

            gcRMName.Name = MappedNames.RMName;
            gcRMName.Width = 100;
            columnsList.Add(gcRMName);

            gcDSMName.Name = MappedNames.DSMName;
            gcDSMName.Width = 100;
            columnsList.Add(gcDSMName);

            gcSupervisorName.Name = MappedNames.SupervisorName;
            gcSupervisorName.Width = 100;
            columnsList.Add(gcSupervisorName);

            gcMerchandiserName.Name = MappedNames.MerchandiserName;
            gcMerchandiserName.Width = 100;
            columnsList.Add(gcMerchandiserName);

            //gcRouteName.Name = MappedNames.RouteName;
            //gcRouteName.Width = 130;
            //columnsList.Add(gcRouteName);

            gcOutletNameID.Name = MappedNames.OutletId;
            gcOutletNameID.Width = 100;
            columnsList.Add(gcOutletNameID);

            gcOutletName.Name = MappedNames.OutletName;
            gcOutletName.Width = 130;
            columnsList.Add(gcOutletName);

            gcProductName.Name = MappedNames.ProductName;
            gcProductName.Width = 170;
            columnsList.Add(gcProductName);

            gcHasPriceBeenCut.Name = MappedNames.HasPriceBeenCut;
            gcHasPriceBeenCut.Width = 75;
            columnsList.Add(gcHasPriceBeenCut);

            gcIsPriceMatchForRPTC.Name = MappedNames.IsPriceMatchForRPTC;
            gcIsPriceMatchForRPTC.Width = 75;
            columnsList.Add(gcIsPriceMatchForRPTC);

            gcDoesOutletHaveMarkup.Name = MappedNames.OutletMarkup;
            gcDoesOutletHaveMarkup.Width = 75;
            columnsList.Add(gcDoesOutletHaveMarkup);

            gcPTC.Name = MappedNames.PTC;
            gcPTC.Width = 45;
            columnsList.Add(gcPTC);

            gcRPTC.Name = MappedNames.RPTC;
            gcRPTC.Width = 45;
            columnsList.Add(gcRPTC);

            gcCurrentMonthMarkup.Name = MappedNames.CurrentMarkup;
            gcCurrentMonthMarkup.Width = 85;
            columnsList.Add(gcCurrentMonthMarkup);

            gcPreviousMonthMarkup.Name = MappedNames.PreviousMarkup;
            gcPreviousMonthMarkup.Width = 100;
            columnsList.Add(gcPreviousMonthMarkup);

            gcCurrentMonthSelling.Name = MappedNames.TotalSalesDAL;
            gcCurrentMonthSelling.Width = 100;
            columnsList.Add(gcCurrentMonthSelling);

            int i = 0;
            foreach (GridColumn column in columnsList)
            {
                column.OptionsColumn.ReadOnly = true;

                column.FieldName = column.Name;
                column.Caption = LocalizationProvider.GetText(column.Name);
                column.Visible = true;
                //column.Width = 50;
                column.VisibleIndex = i++;
            }

            if (AuthenticationManager.Instance.CurrentUserRole.Contains("PRICING_DIRECTOR"))
            {
                //gcIsStartDateApproved.OptionsColumn.ReadOnly = false;
                //gcIsEndDateApproved.OptionsColumn.ReadOnly = false;
            }

            if (columnsList.Count > 0)
            {
                columns = new GridColumn[columnsList.Count];
                columnsList.CopyTo(columns, 0);
            }

            return columns;
        }

        private string GetApprovePhrase(bool isCurrentDateApproved, int aimCount)
        {
            return isCurrentDateApproved
                ? "снять утверждение с " + (aimCount == 1 ? "цели" : "группы целей")
                : "утвердить " + (aimCount == 1 ? "цель" : "группу целей");
        }

        private bool ExistInFieldList(string fieldName)
        {
            try
            {
                return GetFieldsWithNotEmptyNames().Contains(fieldName); ;
            }
            catch
            {
                return false;
            }
        }

        private List<string> GetFieldsWithNotEmptyNames()
        {
            return new List<string>
                (
                    new string[] 
                    { 
                        MappedNames.ProductCombineName,
                        MappedNames.KPITypeName,
                        MappedNames.ProximityName,
                        MappedNames.SettlementName,
                        MappedNames.ComposedRegionName,
                        MappedNames.ComposedDistrictName,
                        MappedNames.ComposedCityName,
                        MappedNames.DSMName,
                        MappedNames.RMName,
                        MappedNames.SupervisorName,
                        MappedNames.OutletGroupName,
                        MappedNames.OutletSubTypeName,
                        MappedNames.OutletTypeName
                    }
                );
        }

        private void ExportEngine(string fileName, DevExpress.XtraGrid.GridControl gridControl, GridColumn[] gridColumns, ExportToType exportType)
        {
            if (gctlPOCwithProblems.DataSource != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView gridViewForExport = gridView;

                if (gridViewForExport != null)
                {
                    DevExpress.XtraGrid.Export.GridViewExportLink gvlink;

                    switch (exportType)
                    {
                        case ExportToType.Bmp:                     
                        case ExportToType.Csv:
                            MessageBox.Show(Resource.ExportFormatNotSupported, Resource.Information);
                            break;
                        case ExportToType.Html:
                            gridViewForExport.ExportToHtml(fileName);
                            break;
                        case ExportToType.Mht:
                            gridViewForExport.ExportToMht(fileName);
                            break;
                        case ExportToType.Pdf:
                            gridViewForExport.ExportToRtf(fileName);
                            break;
                        case ExportToType.Rtf:
                            gridViewForExport.ExportToRtf(fileName);
                            break;
                        case ExportToType.Txt:
                                gvlink = (DevExpress.XtraGrid.Export.GridViewExportLink)gridViewForExport.CreateExportLink( new DevExpress.XtraExport.ExportTxtProvider(fileName));

                                gvlink.ExportCellsAsDisplayText = true;
                                gvlink.ExportAll = true;
                                gvlink.ExpandAll = true;
                                gvlink.ExportDetails = true;
                                gvlink.ExportTo(true);
                            break;
                        case ExportToType.Xls:
                                gvlink = (DevExpress.XtraGrid.Export.GridViewExportLink)gridViewForExport.CreateExportLink( new DevExpress.XtraExport.ExportXlsProvider(fileName));

                                gvlink.ExportCellsAsDisplayText = true;
                                gvlink.ExportAll = true;
                                gvlink.ExpandAll = true;
                                gvlink.ExportDetails = true;
                                gvlink.ExportTo(true);
                            break;
                    }

                }

            }
        }

        private void PrintEngine(DevExpress.XtraGrid.GridControl gridControl, GridColumn[] gridColumns)
        {
            if (gctlPOCwithProblems.DataSource != null)
            {
                //DevExpress.XtraGrid.Views.Grid.GridView gridViewMain = ((DevExpress.XtraGrid.Views.Grid.GridView)gridControl.MainView);
                DevExpress.XtraGrid.Views.Grid.GridView gridViewForPrint = gridView; // DataConvertorHelper.GetViewForExport(gridControl, gridColumns);

                if (gridViewForPrint != null)
                {
                    gridControl.ShowPrintPreview();

                    //gridControl.MainView = gridViewMain;

                    //gridControl.ViewCollection.Remove(gridViewForPrint);
                }
            }
        }

        private string GetFilterWithCondition(string filter, string condition, string operand)
        {
            string result = string.Empty;

            if (string.IsNullOrEmpty(condition))
            {
                return filter;
            }
            else
            {
                if (string.IsNullOrEmpty(filter))
                {
                    result = condition;
                }
                else
                {
                    result = string.Format("{0} {1} {2}", filter, operand, condition);
                }
            }

            return result;
        }

        #endregion Private Methods

        #region Event Handlers

        private void btnExportAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportToType exportType = ExportToType.Xls;

            if (e.Item != null && e.Item is DevExpress.XtraBars.BarButtonItem)
            { 
                if (e.Item.Name.Contains(ExportToType.Xls.ToString()))
                    exportType = ExportToType.Xls;
                else if (e.Item.Name.ToLower().Contains("text"))
                    exportType = ExportToType.Txt;
                else if (e.Item.Name.Contains(ExportToType.Rtf.ToString()))
                    exportType = ExportToType.Rtf;
                else if (e.Item.Name.Contains(ExportToType.Pdf.ToString()))
                    exportType = ExportToType.Pdf;
                else if (e.Item.Name.Contains(ExportToType.Mht.ToString()))
                    exportType = ExportToType.Mht;
                else if (e.Item.Name.Contains(ExportToType.Html.ToString()))
                    exportType = ExportToType.Html;
                else if (e.Item.Name.Contains(ExportToType.Csv.ToString()))
                    exportType = ExportToType.Csv;
                else if (e.Item.Name.Contains(ExportToType.Bmp.ToString()))
                    exportType = ExportToType.Bmp;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Title = Resource.ExportSaveFileDialogCaption;
            saveFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveFileDialog.FileName = "Report";
            saveFileDialog.Filter = String.Format("(*.{0})|*.{0}", exportType.ToString().ToLower());
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                ExportEngine(saveFileDialog.FileName, gctlPOCwithProblems, GetGridColumns(), exportType);
            }
        }


        private void btnExportTo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //SaveFileDialog saveFileDialog = new SaveFileDialog();

            //saveFileDialog.Title = Resource.ExportSaveFileDialogCaption;
            //saveFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //saveFileDialog.FileName = "Report";
            //saveFileDialog.Filter = String.Format("(*.{0})|*.{0}", "xls");
            //saveFileDialog.FilterIndex = 1;
            //saveFileDialog.OverwritePrompt = true;

            //if (saveFileDialog.ShowDialog() != DialogResult.Cancel)
            //{
            //    ExportEngine(saveFileDialog.FileName, gctlCascadedAims, GetGridColumns());
            //}
        }

        private void btnPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintEngine(gctlPOCwithProblems, GetGridColumns());
        }

        private void btnRefreshData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void ListOfPOCWithProblemsReport_Paint(object sender, PaintEventArgs e)
        {
            if (_isInitForTheFirstTime)
            {
                _isInitForTheFirstTime = false;
                RefreshData();            
            }
        }

        private void gridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if(e.Value == null || e.Value == DBNull.Value)
                return;

            if (e.Column.FieldName == MappedNames.HasPriceBeenCut ||
                e.Column.FieldName == MappedNames.IsPriceMatchForRPTC ||
                e.Column.FieldName == MappedNames.OutletMarkup)
            {
                e.DisplayText = Convert.ToBoolean(e.Value) ? "Да" : "Нет";
            }
        }
        #endregion Event Handlers

        #region Grid Columns definition

        private GridColumn gcRMName = new GridColumn();
        private GridColumn gcDSMName = new GridColumn();
        private GridColumn gcSupervisorName = new GridColumn();
        private GridColumn gcMerchandiserName = new GridColumn();
        ///private GridColumn gcRouteName = new GridColumn();
        private GridColumn gcOutletNameID = new GridColumn();
        private GridColumn gcOutletName = new GridColumn();
        private GridColumn gcProductName = new GridColumn();
        private GridColumn gcHasPriceBeenCut = new GridColumn();
        private GridColumn gcIsPriceMatchForRPTC = new GridColumn();
        private GridColumn gcDoesOutletHaveMarkup = new GridColumn();
        private GridColumn gcPTC = new GridColumn();
        private GridColumn gcRPTC = new GridColumn();
        private GridColumn gcCurrentMonthMarkup = new GridColumn();
        private GridColumn gcPreviousMonthMarkup = new GridColumn();
        private GridColumn gcCurrentMonthSelling = new GridColumn();

        #endregion Grid Columns definition

    }

}
