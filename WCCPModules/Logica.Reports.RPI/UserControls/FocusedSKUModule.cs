﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using Logica.Reports.RPI.Forms;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.Authentication;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Export;
using DevExpress.XtraExport;

namespace Logica.Reports.RPI.UserControls
{
    /// <summary>
    /// User control which contains functionality for focused SKU processing
    /// </summary>
    public partial class FocusedSKUModule : UserControl, IRPIModule
    {
        #region Private members

        AimsDataSet.AimDataTable aimsData = null;

        private DateTime _reportStartDate;
        private DateTime _reportEndDate;

        private IDictionary<string, string> tableNameShowedColumnMapping = new Dictionary<string, string>();
        private int _regionId = -1;
        private int _districtId = -1;
        private int _cityId = -1;
        private string regionsFilter = string.Empty;
        private string focusedSKUFilter;

        private bool isInit = false;
        private bool checkingMode = false;
        private object prevValue;

        private Color _tooltipColor = Color.FromArgb(255, 255, 231);
        #endregion Private members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public FocusedSKUModule()
        {
            InitializeComponent();

            InitEvents();

            Localize();

            InitializeMapping();

            SetupColumns();

            SetupUI();
        }

        #endregion Constructors

        #region Public properties

        /// <summary>
        ///  Returns name of the module
        /// </summary>
        public string ModuleName
        {
            get { return Resource.CascadedAimsModule; }
        }

        /// <summary>
        /// Start Date of Report
        /// </summary>
        public DateTime ReportStartDate
        {
            get { return _reportStartDate; }
            set { _reportStartDate = value; }
        }

        /// <summary>
        /// End Date of Report
        /// </summary>
        public DateTime ReportEndDate
        {
            get { return _reportEndDate; }
            set { _reportEndDate = value; }
        }

        /// <summary>
        /// Selected DSM from Report parameters
        /// </summary>
        public DSMEntity DSM { get; set; }

        /// <summary>
        /// Selected Supervisor from Report parameters
        /// </summary>
        public SupervisorEntity Supervisor { get; set; }

        /// <summary>
        /// Selected Merchandiser from Report parameters
        /// </summary>
        public MerchandiserEntity Merchandiser { get; set; }

        /// <summary>
        /// Selected Route from Report parameters
        /// </summary>
        public RouteEntity Route { get; set; }

        /// <summary>
        /// Current filter expression which is used for focused SKU data filtering
        /// </summary>
        public string CurrentFilter
        {
            get
            {
                string filter = focusedSKUFilter;

                if (!string.IsNullOrEmpty(regionsFilter))
                {
                    filter = string.Format("{0} AND {1}", filter, regionsFilter);
                }

                string datesFilter = GetDatesFilter();
                if (!string.IsNullOrEmpty(datesFilter))
                {
                    if (string.IsNullOrEmpty(filter))
                        filter = datesFilter;
                    else
                        filter = string.Format("{0} AND {1}", filter, datesFilter);
                }

                return filter;
            }
        }

        #endregion Public properties

        #region Public methods

        /// <summary>
        ///  Setup columns for Grid which represents Cascaded Aims data
        /// </summary>
        public void SetupColumns()
        {
            gridView.Columns.Clear();

            gridView.Columns.AddRange(GetGridColumns());
            gridView.OptionsView.ColumnAutoWidth = false;
            gridView.OptionsSelection.MultiSelect = true;
        }

        /// <summary>
        /// Loads data from RegionDataSet like array of TreeNode elements
        /// </summary>
        public void LoadRegionsTree()
        {
            //RegionsDataSet _regionsDataSet = new RegionsDataProvider().GetData();

            TreeNode[] lNodes = new RegionsDataProvider().GetTree(); //DataConvertorHelper.GetTreeNodes(_regionsDataSet, _regionsDataSet.Country.TableName, tableNameShowedColumnMapping);
            tvRegions.BeginUpdate();
            try
            {
                tvRegions.Nodes.Clear();
                if (lNodes != null)
                {
                    foreach (TreeNode lNode in lNodes)
                    {
                        // hide Fake Country
                        if (lNode.Tag != null && lNode.Tag is RegionsDataSet.CountryRow)
                        {
                            RegionsDataSet.CountryRow row = lNode.Tag as RegionsDataSet.CountryRow;
                            if (Convert.ToInt32(row.CountryIdColumn.Trim()) == 0)
                                continue;
                        }
                        tvRegions.Nodes.Add(lNode);
                    }
                }
            }
            finally
            {
                tvRegions.EndUpdate();
                tvRegions.Invalidate();
            }
        }

        /// <summary>
        /// Reloads focused SKUs data
        /// </summary>
        public void LoadFocusedSKUs()
        {
            RefreshData();
        }

        /// <summary>
        /// Load data for Regions and Focused SKU's at the first time
        /// </summary>
        public void InitialLoadData()
        {
            if (isInit)
                return;

            focusedSKUFilter = string.Format("{0} = {1}", MappedNames.IsFocused, true);
            LoadRegionsTree();
            LoadFocusedSKUs();
            tvRegions.HideSelection = false;
            if (tvRegions.Nodes.Count > 0)
                tvRegions.SelectedNode = tvRegions.Nodes[0];
            tvRegions.Select();
            isInit = true;
        }

        #endregion Public methods

        #region Event Handlers

        private void btnNewFocusedSKU_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NewFocusedSKUForm frmNewFocusedSKU = new NewFocusedSKUForm();
            frmNewFocusedSKU.RegionId = _regionId;
            frmNewFocusedSKU.DistrictId = _districtId;
            frmNewFocusedSKU.CityId = _cityId;

            if (frmNewFocusedSKU.ShowDialog() == DialogResult.OK)
            {
                CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                AimsDataSet.AimDataTable aimsData = null;

                if (gctlCascadedAims.DataSource is DataView)
                    aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                else
                    aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                if (aimsData == null)
                    aimsData = dataProvider.GetData().Aim;

                int fakeIdentity = int.MaxValue;
                CascadedAimEntity entity = frmNewFocusedSKU.CreatedFocusedSKU;

                entity.KPITypeId = 1;

                fakeIdentity--;
                DataRow dr = InsertAim(entity, aimsData);
                dr[MappedNames.AimId] = fakeIdentity;

                dataProvider.UpdateData(aimsData);

                RefreshData();

            }
        }

        private DataRow InsertAim(CascadedAimEntity cascadedAim, AimsDataSet.AimDataTable table)
        {
            DataRow insertedRow = table.NewRow();

            cascadedAim.Fill(insertedRow);
            table.Rows.Add(insertedRow);

            return insertedRow;
        }

        private void btnCopyFocusedSKU_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                NewFocusedSKUForm frmNewFocusedSKU = new NewFocusedSKUForm();

                frmNewFocusedSKU.RegionId = _regionId;
                frmNewFocusedSKU.DistrictId = _districtId;
                frmNewFocusedSKU.CityId = _cityId;

                CascadedAimEntity aim = new CascadedAimEntity(focusedRow);
                // in order to prevent copying Approve parameters
                aim.IsStartDateApproved = false;
                aim.IsEndDateApproved = false;

                frmNewFocusedSKU.Init(aim);
                frmNewFocusedSKU.Localize(Resource.CopiedNewFocusedSKUFormCaption, Resource.NewAimFormButtonOk, Resource.NewAimFormButtonCancel);

                if (frmNewFocusedSKU.ShowDialog() == DialogResult.OK)
                {
                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    int fakeIdentity = int.MaxValue;
                    CascadedAimEntity entity = frmNewFocusedSKU.CreatedFocusedSKU;
                    entity.ProductCnId = (entity.ProductCnId == -1) ? aim.ProductCnId : entity.ProductCnId;
                    entity.KPITypeId = 1;

                    fakeIdentity--;
                    DataRow dr = InsertAim(entity, aimsData);
                    dr[MappedNames.AimId] = fakeIdentity;

                    dataProvider.UpdateData(aimsData);

                    RefreshData();

                }

            }
        }

        private void btnEditFocusedSKU_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditFocusedSKU();
        }

        private void EditFocusedSKU()
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                EditFocusedSKUForm frmEditFocusedSKU = new EditFocusedSKUForm();

                int focusedRowHandle = gridView.FocusedRowHandle;
                CascadedAimEntity aim = new CascadedAimEntity(focusedRow);
                frmEditFocusedSKU.Init(aim);
                frmEditFocusedSKU.Localize(Resource.EditFocusedSKUCaption, Resource.EditAimFormButtonOk, Resource.EditAimFormButtonCancel);

                if (frmEditFocusedSKU.ShowDialog() == DialogResult.OK)
                {
                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    if (frmEditFocusedSKU.EditedFocusedSKU.RegionId == -1)
                        frmEditFocusedSKU.EditedFocusedSKU.RegionId = _regionId;
                    if (frmEditFocusedSKU.EditedFocusedSKU.DistrictId == -1)
                        frmEditFocusedSKU.EditedFocusedSKU.DistrictId = _districtId;
                    if (frmEditFocusedSKU.EditedFocusedSKU.CityId == -1)
                        frmEditFocusedSKU.EditedFocusedSKU.CityId = _cityId;

                    focusedRow.BeginEdit();
                    frmEditFocusedSKU.EditedFocusedSKU.Fill(focusedRow);
                    focusedRow.EndEdit();

                    dataProvider.UpdateData(aimsData);

                    RefreshData();

                    gridView.FocusedRowHandle = focusedRowHandle;
                }
            }
        }

        private void btnApproveSKUStartDate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                DateTime currentDate = DateTime.Now;
                bool isCurrentDateApproved = Convert.ToBoolean(focusedRow[MappedNames.IsStartDateApproved]);

                int focusedRowHandle = gridView.FocusedRowHandle;
                int[] selectedRowHandles = gridView.GetSelectedRows();

                if (ConfirmationDialogForm.Show
                    (
                        string.Format(
                            @"{0} {1}?",
                            "Вы уверены в том что хотите ",
                            GetApprovePhrase(isCurrentDateApproved, selectedRowHandles.Length)),
                        isCurrentDateApproved ? "Cнятие утверждение" : "Утверждение",
                        isCurrentDateApproved ? "Cнять утверждение" : "Утвердить",
                        "Отмена"
                    ) == DialogResult.OK
                    )
                {

                    foreach (int handle in selectedRowHandles)
                    {
                        DataRow row = gridView.GetRow(handle) is DataRow
                            ? gridView.GetRow(handle) as DataRow
                            : (gridView.GetRow(handle) as DataRowView).Row;

                        DateTime currentRowDateStartPeriod = Convert.ToDateTime(row[MappedNames.DateStartPeriod]);
                        if (isCurrentDateApproved)
                        {
                            if (currentRowDateStartPeriod != DateTime.MinValue && currentDate > currentRowDateStartPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.DisapproveStartDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateStartPeriod
                                        ),
                                    Resource.DisapproveErrorFormCaption);
                                continue;
                            }
                        }
                        else
                        {
                            if (currentRowDateStartPeriod != DateTime.MinValue && currentDate > currentRowDateStartPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.ApproveStartDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateStartPeriod
                                        ),
                                    Resource.ApproveErrorFormCaption);
                                continue;
                            }
                        }

                        row.BeginEdit();
                        row[MappedNames.IsStartDateApproved] = !isCurrentDateApproved;
                        row[MappedNames.DateApproveStartPeriod] = currentDate;
                        row.EndEdit();

                    }

                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    dataProvider.UpdateData(aimsData);
                }
                gridView.FocusedRowHandle = focusedRowHandle;
            }
        }

        private void btnApproveSKUEndDate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                DateTime currentDate = DateTime.Now;
                bool isCurrentDateApproved = Convert.ToBoolean(focusedRow[MappedNames.IsEndDateApproved]);

                int focusedRowHandle = gridView.FocusedRowHandle;
                int[] selectedRowHandles = gridView.GetSelectedRows();

                if (ConfirmationDialogForm.Show
                    (
                        string.Format(
                            @"{0} {1}?",
                            "Вы уверены в том что хотите ",
                            GetApprovePhrase(isCurrentDateApproved, selectedRowHandles.Length)),
                        isCurrentDateApproved ? "Cнятие утверждение" : "Утверждение",
                        isCurrentDateApproved ? "Cнять утверждение" : "Утвердить",
                        "Отмена"
                    ) == DialogResult.OK
                    )
                {

                    foreach (int handle in selectedRowHandles)
                    {
                        DataRow row = gridView.GetRow(handle) is DataRow
                            ? gridView.GetRow(handle) as DataRow
                            : (gridView.GetRow(handle) as DataRowView).Row;

                        DateTime currentRowDateEndPeriod = Convert.ToDateTime(row[MappedNames.DateEndPeriod]);
                        if (isCurrentDateApproved)
                        {
                            if (currentRowDateEndPeriod != DateTime.MinValue && currentDate > currentRowDateEndPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.DisapproveEndDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateEndPeriod
                                        ),
                                    Resource.DisapproveErrorFormCaption);
                                continue;
                            }
                        }
                        else
                        {
                            if (currentRowDateEndPeriod != DateTime.MinValue && currentDate > currentRowDateEndPeriod)
                            {
                                MessageBox.Show(
                                    string.Format(@"{0} ({1}, {2})",
                                        Resource.ApproveEndDateErrorMsg,
                                        row[MappedNames.ProductCombineName],
                                        currentRowDateEndPeriod
                                        ),
                                    Resource.ApproveErrorFormCaption);
                                continue;
                            }
                        }
                        row.BeginEdit();
                        row[MappedNames.IsEndDateApproved] = !isCurrentDateApproved;
                        row[MappedNames.DateApproveEndPeriod] = currentDate;
                        row.EndEdit();

                    }

                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    dataProvider.UpdateData(aimsData);
                }
                gridView.FocusedRowHandle = focusedRowHandle;
            }

        }

        private void btnDeleteFocusedSKU_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRow focusedRow = gridView.GetFocusedDataRow();
            if (focusedRow != null)
            {
                // check if Focused SKU is in approved state - so it cann't be deleted
                if (Convert.ToBoolean(focusedRow[MappedNames.IsStartDateApproved]))
                {
                    MessageBox.Show(Resource.CannotDeleteApprovedSKUMsg, Resource.ErrorDeleteSKUFormCaption);
                    return;
                }


                if (ConfirmationDialogForm.Show(
                        Resource.DeleteSKUConfirmationMsg,
                        Resource.DeleteSKUConfirmationFormCaption,
                        Resource.DeleteSKUConfirmationButtonOkText,
                        Resource.DeleteSKUConfirmationButtonCancelText) == DialogResult.OK
                    )
                {
                    CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                    AimsDataSet.AimDataTable aimsData = null;

                    if (gctlCascadedAims.DataSource is DataView)
                        aimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
                    else
                        aimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

                    if (aimsData == null)
                        aimsData = dataProvider.GetData().Aim;

                    focusedRow.Delete();

                    dataProvider.UpdateData(aimsData);

                    RefreshData();
                }
            }
        }

        private void btnRefreshData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void btnExportToExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string lPathToFile = FileHelper.SelectFilePath(ExportToType.Xls);
            ExportEngine(lPathToFile, gctlCascadedAims, GetGridColumns());
        }

        private void btnPrintPreview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintEngine(gctlCascadedAims, GetGridColumns());
        }

        private void CascadedAimsModule_Invalidated(object sender, InvalidateEventArgs e)
        {
            InitialLoadData();
        }

        private void tvRegions_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode node = e.Node;

            _regionId = -1;
            _districtId = -1;
            _cityId = -1;

            CheckNodes(node);
            ComposeRegionsFilter();

            SetupUI(_regionId);

            RefreshData();
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void deFilter_DateTimeChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C && e.Control)
            {
                try
                {
                    CopyToClipboard(gridView.GetSelectedRows());
                }
                catch { }
            }
        }

        private void gridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (ExistInFieldList(e.Column.FieldName))
            {
                if (e.Value == null || e.Value == DBNull.Value)
                {
                    e.DisplayText = Resource.ComboBoxItemAnyValueText;
                }
            }
        }

        private void gctlCascadedAims_DoubleClick(object sender, EventArgs e)
        {
            EditFocusedSKU();
        }

        private void gridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            string mess;
            e.Cancel = !CouldValueBeEdited(gridView.FocusedColumn.FieldName, out mess);
        }

        private void gridView_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (!checkingMode && e.RowHandle < gridView.RowCount)
            {
                string errorMess;

                e.Valid = IsRowValid(e.RowHandle, out errorMess);
                e.ErrorText = errorMess;

                if (e.Valid)
                {
                    UpdateRow(GetAimFromRow(e.RowHandle), gridView.FocusedRowHandle);
                }
            }
        }

        private void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                return;
            }

            checkingMode = true;

            if (Convert.ToBoolean(rbEditType.EditValue))
            {
                WaitManager.StartWait();

                DataRow firstRow = gridView.GetDataRow(e.RowHandle);

                List<DataRow> rows = new List<DataRow>();
                List<int> rowsNums = new List<int>();
                for (int i = 0; i < gridView.RowCount; i++)
                {
                    rows.Add(gridView.GetDataRow(i));
                }

                string curFilter = gridView.ActiveFilterString;
                gridView.ActiveFilterString = string.Empty;
                DevExpress.Data.ColumnSortOrder sorting = e.Column.SortOrder;
                e.Column.SortOrder = DevExpress.Data.ColumnSortOrder.None;

                int rowHandle = FindRowHandleByDataRow(firstRow);

                foreach (DataRow row in rows)
                {
                    rowsNums.Add(FindRowHandleByDataRow(row));
                }

                string errorMess = string.Empty;

                WaitManager.StopWait();

                if (CouldAllRowsBeEdited(rowsNums, e.Column.FieldName, ref rowHandle, e.Value, out errorMess))
                {
                    WaitManager.StartWait();
                    gridView.CellValueChanging -= gridView_CellValueChanging;

                    foreach (int row in rowsNums)
                    {
                        gridView.SetRowCellValue(row, e.Column.FieldName, e.Value);
                        UpdateRow(GetAimFromRow(row), row);
                    }

                    gridView.CellValueChanging += gridView_CellValueChanging;

                    DataRow focusedRow = gridView.GetDataRow(rowHandle);

                    gridView.BeginUpdate();
                    gridView.ActiveFilterString = curFilter;
                    e.Column.SortOrder = sorting;
                    gridView.EndUpdate();

                    gridView.FocusedRowHandle = FindRowHandleByDataRow(focusedRow);
                    gridView.SelectRow(FindRowHandleByDataRow(focusedRow));

                    WaitManager.StopWait();
                }
                else
                {
                    RefreshData();

                    foreach (int row in gridView.GetSelectedRows())
                    {
                        gridView.UnselectRow(row);
                    }

                    DataRow focusedRow = gridView.GetDataRow(rowHandle);

                    gridView.BeginUpdate();
                    gridView.ActiveFilterString = curFilter;
                    e.Column.SortOrder = sorting;
                    gridView.EndUpdate();

                    gridView.FocusedRowHandle = FindRowHandleByDataRow(focusedRow);
                    gridView.SelectRow(FindRowHandleByDataRow(focusedRow));

                    XtraMessageBox.Show("Невозможно редактировать все записи: " + errorMess);
                }
            }
            checkingMode = false;
        }

        #endregion Event Handlers

        #region Private methods

        private void InitEvents()
        {
            tvRegions.AfterSelect += new TreeViewEventHandler(tvRegions_AfterSelect);

            gridView.KeyDown += new KeyEventHandler(gridView_KeyDown);
            gridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(gridView_CustomColumnDisplayText);

            btnNewFocusedSKU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnNewFocusedSKU_ItemClick);
            btnCopyFocusedSKU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnCopyFocusedSKU_ItemClick);
            btnEditFocusedSKU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnEditFocusedSKU_ItemClick);
            btnApproveSKUStartDate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnApproveSKUStartDate_ItemClick);
            btnApproveSKUEndDate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnApproveSKUEndDate_ItemClick);
            btnDeleteFocusedSKU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnDeleteFocusedSKU_ItemClick);
            btnRefreshFocusedSKUList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnRefreshData_ItemClick);

            btnExportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnExportToExcel_ItemClick);
            btnPrintPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnPrintPreview_ItemClick);

            chkFilterOnlyApprovedAims.CheckedChanged += new EventHandler(chkFilter_CheckedChanged);
            chkFilterValidAimsForCurrentDate.CheckedChanged += new EventHandler(chkFilter_CheckedChanged);

            deFilterAimsFrom.DateTimeChanged += new EventHandler(deFilter_DateTimeChanged);
            deFilterAimsTo.DateTimeChanged += new EventHandler(deFilter_DateTimeChanged);

            this.Invalidated += new InvalidateEventHandler(CascadedAimsModule_Invalidated);
        }

        private void RefreshData()
        {
            if (null == aimsData)
            {
                CascadedAimsDataProvider dataProvider = new CascadedAimsDataProvider();
                aimsData = dataProvider.GetData().Aim;
            }

            gctlCascadedAims.BeginUpdate();

            //gctlCascadedAims.DataSource = null;

            if (string.IsNullOrEmpty(CurrentFilter))
            {
                aimsData.DefaultView.RowFilter = string.Empty;
                gctlCascadedAims.DataSource = aimsData.DefaultView;
                //gctlCascadedAims.DataSource = aimsData;
            }
            else
            {
                aimsData.DefaultView.RowFilter = CurrentFilter;
                gctlCascadedAims.DataSource = aimsData.DefaultView;
            }

            gctlCascadedAims.EndUpdate();
        }

        private void Localize()
        {
            SuperToolTip sttNewFocusedSKU = new SuperToolTip();
            ToolTipItem ttiNewFocusedSKU = new ToolTipItem();
            ttiNewFocusedSKU.Text = Resource.NewFocusedSKUButton;
            sttNewFocusedSKU.Items.Add(ttiNewFocusedSKU);
            sttNewFocusedSKU.Appearance.Options.UseBackColor = true;
            sttNewFocusedSKU.Appearance.BackColor = _tooltipColor;
            btnNewFocusedSKU.SuperTip = sttNewFocusedSKU;

            SuperToolTip sttCopyFocusedSKU = new SuperToolTip();
            ToolTipItem ttiCopyFocusedSKU = new ToolTipItem();
            ttiCopyFocusedSKU.Text = Resource.CreateNewFocusedSKUFromExistedButton;
            sttCopyFocusedSKU.Items.Add(ttiCopyFocusedSKU);
            sttCopyFocusedSKU.Appearance.Options.UseBackColor = true;
            sttCopyFocusedSKU.Appearance.BackColor = _tooltipColor;
            btnCopyFocusedSKU.SuperTip = sttCopyFocusedSKU;

            SuperToolTip sttEditFocusedSKU = new SuperToolTip();
            ToolTipItem ttiEditFocusedSKU = new ToolTipItem();
            ttiEditFocusedSKU.Text = Resource.EditFocusedSKUButton;
            sttEditFocusedSKU.Items.Add(ttiEditFocusedSKU);
            sttEditFocusedSKU.Appearance.Options.UseBackColor = true;
            sttEditFocusedSKU.Appearance.BackColor = _tooltipColor;
            btnEditFocusedSKU.SuperTip = sttEditFocusedSKU;


            SuperToolTip sttSetFocusedSKUStartDate = new SuperToolTip();
            ToolTipItem ttiSetFocusedSKUStartDate = new ToolTipItem();
            ttiSetFocusedSKUStartDate.Text = Resource.SetFocusedSKUStartDateButton;
            sttSetFocusedSKUStartDate.Items.Add(ttiSetFocusedSKUStartDate);
            sttSetFocusedSKUStartDate.Appearance.Options.UseBackColor = true;
            sttSetFocusedSKUStartDate.Appearance.BackColor = _tooltipColor;
            btnApproveSKUStartDate.SuperTip = sttSetFocusedSKUStartDate;

            SuperToolTip sttSetFocusedSKUEndDate = new SuperToolTip();
            ToolTipItem ttiSetFocusedSKUEndDate = new ToolTipItem();
            ttiSetFocusedSKUEndDate.Text = Resource.SetFocusedSKUEndDateButton;
            sttSetFocusedSKUEndDate.Items.Add(ttiSetFocusedSKUEndDate);
            sttSetFocusedSKUEndDate.Appearance.Options.UseBackColor = true;
            sttSetFocusedSKUEndDate.Appearance.BackColor = _tooltipColor;
            btnApproveSKUEndDate.SuperTip = sttSetFocusedSKUEndDate;

            SuperToolTip sttDeleteFocusedSKU = new SuperToolTip();
            ToolTipItem ttiDeleteFocusedSKU = new ToolTipItem();
            ttiDeleteFocusedSKU.Text = Resource.DeleteFocusedSKUButton;
            sttDeleteFocusedSKU.Items.Add(ttiDeleteFocusedSKU);
            sttDeleteFocusedSKU.Appearance.Options.UseBackColor = true;
            sttDeleteFocusedSKU.Appearance.BackColor = _tooltipColor;
            btnDeleteFocusedSKU.SuperTip = sttDeleteFocusedSKU;

            SuperToolTip sttRefreshFocusedSKUList = new SuperToolTip();
            ToolTipItem ttiRefreshFocusedSKUList = new ToolTipItem();
            ttiRefreshFocusedSKUList.Text = Resource.RefreshFocusedSKUListButton;
            sttRefreshFocusedSKUList.Items.Add(ttiRefreshFocusedSKUList);
            sttRefreshFocusedSKUList.Appearance.Options.UseBackColor = true;
            sttRefreshFocusedSKUList.Appearance.BackColor = _tooltipColor;
            btnRefreshFocusedSKUList.SuperTip = sttRefreshFocusedSKUList;

        }

        private void SetupUI()
        {
            btnNewFocusedSKU.Enabled = btnCopyFocusedSKU.Enabled = false;
            btnEditFocusedSKU.Enabled = btnDeleteFocusedSKU.Enabled = false;
            btnApproveSKUStartDate.Enabled = btnApproveSKUEndDate.Enabled = false;
            switch (AuthenticationManager.Instance.CurrentUserLevel)
            {
                case 5:
                    btnNewFocusedSKU.Enabled = btnCopyFocusedSKU.Enabled = btnEditFocusedSKU.Enabled =
                        btnApproveSKUStartDate.Enabled = btnApproveSKUEndDate.Enabled = btnDeleteFocusedSKU.Enabled = true;
                    break;
                case 4:
                    btnEditFocusedSKU.Enabled = btnDeleteFocusedSKU.Enabled = true;
                    break;
            }
        }

        private void SetupUI(int regionId)
        {
            if (regionId > 0)
            {
                switch (AuthenticationManager.Instance.CurrentUserLevel)
                {
                    case 4:
                        btnNewFocusedSKU.Enabled = btnCopyFocusedSKU.Enabled = btnEditFocusedSKU.Enabled = btnDeleteFocusedSKU.Enabled = true;
                        break;
                }
            }
            else
            {
                switch (AuthenticationManager.Instance.CurrentUserLevel)
                {
                    case 4:
                        btnNewFocusedSKU.Enabled = btnCopyFocusedSKU.Enabled = btnEditFocusedSKU.Enabled = btnDeleteFocusedSKU.Enabled = false;
                        break;
                }
            }
        }

        private void InitializeMapping()
        {
            tableNameShowedColumnMapping.Clear();
            tableNameShowedColumnMapping.Add(MappedNames.CountryTable, MappedNames.CountryName);
            tableNameShowedColumnMapping.Add(MappedNames.RegionTable, MappedNames.RegionName);
            tableNameShowedColumnMapping.Add(MappedNames.DistrictTable, MappedNames.DistrictName);
            tableNameShowedColumnMapping.Add(MappedNames.CityTable, MappedNames.CityName);
        }

        private GridColumn[] GetGridColumns()
        {
            IList<GridColumn> columnsList = new List<GridColumn>();
            GridColumn[] columns = null;

            //gcAimId.Name = MappedNames.AimId;
            //columnsList.Add(gcAimId);

            gcProductCombineName.Name = MappedNames.ProductCombineName;
            gcProductCombineName.Width = 200;
            columnsList.Add(gcProductCombineName);

            gcDateStartPeriod.Name = MappedNames.DateStartPeriod;
            gcDateStartPeriod.Width = 90;
            gcDateStartPeriod.ColumnEdit = repositoryDateEdit;
            columnsList.Add(gcDateStartPeriod);

            gcIsStartDateApproved.Name = MappedNames.IsStartDateApproved;
            gcIsStartDateApproved.Width = 90;
            gcIsStartDateApproved.ColumnEdit = repositoryCheckEdit;
            columnsList.Add(gcIsStartDateApproved);

            gcDateEndPeriod.Name = MappedNames.DateEndPeriod;
            gcDateEndPeriod.Width = 90;
            gcDateEndPeriod.ColumnEdit = repositoryDateEdit;
            columnsList.Add(gcDateEndPeriod);

            gcIsEndDateApproved.Name = MappedNames.IsEndDateApproved;
            gcIsEndDateApproved.Width = 90;
            gcIsEndDateApproved.ColumnEdit = repositoryCheckEdit;
            columnsList.Add(gcIsEndDateApproved);

            gcIsFocused.Name = MappedNames.IsFocused;
            gcIsFocused.Width = 70;
            gcIsFocused.ColumnEdit = repositoryCheckEdit;
            columnsList.Add(gcIsFocused);

            gcProximity.Name = MappedNames.ProximityName;
            gcProximity.Width = 150;
            columnsList.Add(gcProximity);

            gcSettlement.Name = MappedNames.SettlementName;
            gcSettlement.Width = 150;
            columnsList.Add(gcSettlement);

            gcComposedRegionName.Name = MappedNames.ComposedRegionName;
            gcComposedRegionName.Width = 150;
            columnsList.Add(gcComposedRegionName);

            gcComposedDistrictName.Name = MappedNames.ComposedDistrictName;
            gcComposedDistrictName.Width = 120;
            columnsList.Add(gcComposedDistrictName);

            gcComposedCityName.Name = MappedNames.ComposedCityName;
            gcComposedCityName.Width = 120;
            columnsList.Add(gcComposedCityName);

            gcDSM.Name = MappedNames.DSMName;
            gcDSM.Width = 150;
            columnsList.Add(gcDSM);

            gcSupervisor.Name = MappedNames.SupervisorName;
            gcSupervisor.Width = 150;
            columnsList.Add(gcSupervisor);

            gcOutletGroup.Name = MappedNames.OLGroupName;
            gcOutletGroup.Width = 100;
            columnsList.Add(gcOutletGroup);

            gcOutletType.Name = MappedNames.OLTypeName;
            gcOutletType.Width = 200;
            columnsList.Add(gcOutletType);

            gcOutletSubType.Name = MappedNames.OLSubTypeName;
            gcOutletSubType.Width = 200;
            columnsList.Add(gcOutletSubType);

            int i = 0;
            foreach (GridColumn column in columnsList)
            {
                column.OptionsColumn.AllowEdit = false;

                column.FieldName = column.Name;
                column.Caption = LocalizationProvider.GetText(column.Name);
                column.Visible = true;
                //column.Width = 50;
                column.VisibleIndex = i++;
            }

            gcDateStartPeriod.OptionsColumn.AllowEdit = true;
            gcIsStartDateApproved.OptionsColumn.AllowEdit = true;
            gcDateEndPeriod.OptionsColumn.AllowEdit = true;
            gcIsEndDateApproved.OptionsColumn.AllowEdit = true;
            //gcIsFocused.OptionsColumn.AllowEdit = true;

            if (AuthenticationManager.Instance.CurrentUserRole.Contains("PRICING_DIRECTOR"))
            {
                //gcIsStartDateApproved.OptionsColumn.ReadOnly = false;
                //gcIsEndDateApproved.OptionsColumn.ReadOnly = false;
            }

            if (columnsList.Count > 0)
            {
                columns = new GridColumn[columnsList.Count];
                columnsList.CopyTo(columns, 0);
            }

            return columns;
        }

        private string GetDatesFilter()
        {
            string filter = string.Empty;

            if (deFilterAimsFrom.Text.Length > 0)
            {
                filter = string.Format(
                    @"'{0}' <= {1} AND '{0}' <= {2}",
                    deFilterAimsFrom.Text,
                    MappedNames.DateStartPeriod,
                    MappedNames.DateEndPeriod
                    );
            }

            if (deFilterAimsTo.Text.Length > 0)
            {
                string filterTo = string.Format(
                       @"'{0}' >= {1} AND '{0}' >= {2}",
                       deFilterAimsTo.Text,
                       MappedNames.DateStartPeriod,
                       MappedNames.DateEndPeriod
                       );
                if (string.IsNullOrEmpty(filter))
                {
                    filter = filterTo;
                }
                else
                {
                    filter = string.Format("{0} AND {1}", filter, filterTo);
                }
            }

            if (chkFilterOnlyApprovedAims.Checked)
            {
                string filterApproved = string.Format(
                       @"'{0}' = {1} AND '{0}'= {2}",
                       "true",
                       MappedNames.IsStartDateApproved,
                       MappedNames.IsEndDateApproved
                       );
                if (string.IsNullOrEmpty(filter))
                {
                    filter = filterApproved;
                }
                else
                {
                    filter = string.Format("{0} AND {1}", filter, filterApproved);
                }
            }

            if (chkFilterValidAimsForCurrentDate.Checked)
            {
                string filterValid = string.Format(
                       @"'{0}' >= {1} AND '{0}' <= {2}",
                       DateTime.Now.ToString("d"),
                       MappedNames.DateStartPeriod,
                       MappedNames.DateEndPeriod
                       );
                if (string.IsNullOrEmpty(filter))
                {
                    filter = filterValid;
                }
                else
                {
                    filter = string.Format("{0} AND {1}", filter, filterValid);
                }
            }

            return filter;
        }

        private void ComposeRegionsFilter()
        {
            regionsFilter = string.Empty;
            if (_regionId != -1)
                regionsFilter = string.Format("{0} = {1}", MappedNames.RegionId, _regionId);
            if (_districtId != -1)
                regionsFilter += string.Format(" AND {0} = {1}", MappedNames.DistrictId, _districtId);
            if (_cityId != -1)
                regionsFilter += string.Format(" AND {0} = {1}", MappedNames.CityId, _cityId);
        }

        private void CheckNodes(TreeNode node)
        {
            if (node.Tag != null)
            {
                if (node.Tag is RegionsDataSet.CityRow)
                    _cityId = (node.Tag as RegionsDataSet.CityRow).CityIdColumn;
                else if (node.Tag is RegionsDataSet.DistrictRow)
                    _districtId = (node.Tag as RegionsDataSet.DistrictRow).DistrictIdColumn;
                else if (node.Tag is RegionsDataSet.RegionRow)
                    _regionId = (node.Tag as RegionsDataSet.RegionRow).RegionIdColumn;

                if (node.Parent != null)
                    CheckNodes(node.Parent);
            }
        }

        private void CopyToClipboard(int[] rowHandles)
        {
            if (rowHandles.Length > 0)
            {
                StringBuilder builder = new StringBuilder();
                foreach (int handle in rowHandles)
                {
                    object row = gridView.GetRow(handle);
                    string line = string.Empty;
                    string separator = '\t'.ToString();
                    foreach (GridColumn gridColumn in gridView.Columns)
                    {
                        string value = row is DataRow
                                        ? (row as DataRow)[gridColumn.FieldName].ToString()
                                        : row is DataRowView
                                            ? (row as DataRowView)[gridColumn.FieldName].ToString()
                                            : " ";
                        line = string.IsNullOrEmpty(line)
                            ? line = value
                            : string.Join(separator, new string[] { line, value });
                    }
                    builder.AppendLine(line);
                }

                Clipboard.SetText(builder.ToString());
            }
        }

        private string GetApprovePhrase(bool isCurrentDateApproved, int aimCount)
        {
            return isCurrentDateApproved
                ? "снять утверждение с " + (aimCount == 1 ? "СКЮ" : "группы СКЮ")
                : "утвердить " + (aimCount == 1 ? "текущую СКЮ" : "группу СКЮ");
        }

        private bool ExistInFieldList(string fieldName)
        {
            try
            {
                return GetFieldsWithNotEmptyNames().Contains(fieldName); ;
            }
            catch
            {
                return false;
            }
        }

        private List<string> GetFieldsWithNotEmptyNames()
        {
            return new List<string>
                (
                    new string[] 
                    { 
                        MappedNames.ProductCombineName,
                        MappedNames.KPITypeName,
                        MappedNames.ProximityName,
                        MappedNames.SettlementName,
                        MappedNames.ComposedRegionName,
                        MappedNames.ComposedDistrictName,
                        MappedNames.ComposedCityName,
                        MappedNames.DSMName,
                        MappedNames.SupervisorName,
                        MappedNames.OutletGroupName,
                        MappedNames.OutletSubTypeName,
                        MappedNames.OutletTypeName
                    }
                );
        }

        private void ExportEngine(string fileName, DevExpress.XtraGrid.GridControl gridControl, GridColumn[] gridColumns)
        {
            if (gctlCascadedAims.DataSource != null && !string.IsNullOrEmpty(fileName))
            {
                DataConvertorHelper.HideEmptyColumns(gridControl, gridColumns);

                gridView.OptionsPrint.AutoWidth = false;
                gridControl.ExportToXls(fileName);
            }
        }

        private void PrintEngine(DevExpress.XtraGrid.GridControl gridControl, GridColumn[] gridColumns)
        {
            if (gctlCascadedAims.DataSource != null)
            {
                DataConvertorHelper.HideEmptyColumns(gridControl, gridColumns);

                gridView.OptionsPrint.AutoWidth = true;
                gridControl.ShowPrintPreview();
            }
        }

        private CascadedAimEntity GetAimFromRow(int rowHandle)
        {
            CascadedAimEntity lAim = null;
            DataRow lFocusedRow = gridView.GetDataRow(rowHandle);
            if (lFocusedRow != null)
            {
                lAim = new CascadedAimEntity(lFocusedRow);
            }

            return lAim;
        }

        private void UpdateRow(CascadedAimEntity aim, int rowHandle)
        {
            CascadedAimsDataProvider lDataProvider = new CascadedAimsDataProvider();
            AimsDataSet.AimDataTable lAimsData;
            DataRow lModifiedRow = gridView.GetDataRow(rowHandle);

            if (gctlCascadedAims.DataSource is DataView)
                lAimsData = (gctlCascadedAims.DataSource as DataView).Table as AimsDataSet.AimDataTable;
            else
                lAimsData = gctlCascadedAims.DataSource as AimsDataSet.AimDataTable;

            if (lAimsData == null)
                lAimsData = lDataProvider.GetData().Aim;

            DateTime now = DateTime.Now;

            aim.DateAdded = now;

            aim.DateApproveStartPeriod =
                aim.IsStartDateApproved != Convert.ToBoolean(lAimsData.Rows[rowHandle][MappedNames.IsStartDateApproved]) ? now : aim.DateApproveStartPeriod;

            aim.DateApproveEndPeriod =
                aim.IsEndDateApproved != Convert.ToBoolean(lAimsData.Rows[rowHandle][MappedNames.IsEndDateApproved]) ? now : aim.DateApproveEndPeriod;

            //aim.CreatedUserId = AuthenticationManager.Instance.CurrentUserId;
            //aim.ApprovedUserId = AuthenticationManager.Instance.CurrentUserId;

            //if (aim.RegionId == -1)
            //    aim.RegionId = _regionId;
            //if (aim.DistrictId == -1)
            //    aim.DistrictId = _districtId;
            //if (aim.CityId == -1)
            //    aim.CityId = _cityId;

            lModifiedRow.BeginEdit();
            aim.Fill(lModifiedRow);
            lModifiedRow.EndEdit();

            lDataProvider.UpdateData(lAimsData);

            RefreshData();

            if (!checkingMode)
            {
                gridView.FocusedRowHandle = rowHandle;
            }
        }

        private bool IsRowValid(int rowHandle, out string errorMess)
        {
            bool lHasPositionCoPpmM5 = false;//DataAccessProvider.HasUserPosition(DataAccessProvider.PositionCoPpmM5);
            bool isRowVal = false;

            DateTime now = DateTime.Now.Date;
            CascadedAimEntity aim = GetAimFromRow(rowHandle);

            if (Convert.ToDateTime(gridView.GetRowCellValue(rowHandle, MappedNames.DateStartPeriod)).Date < now && !lHasPositionCoPpmM5 && Convert.ToBoolean(gridView.GetRowCellValue(rowHandle, MappedNames.IsEndDateApproved)))
            {
                errorMess = Resource.PeriodStartDateLessThenCurrentDate;
                return isRowVal;
            }

            if (Convert.ToDateTime(gridView.GetRowCellValue(rowHandle, MappedNames.DateEndPeriod)).Date < now && !lHasPositionCoPpmM5 && Convert.ToBoolean(gridView.GetRowCellValue(rowHandle, MappedNames.IsEndDateApproved)))
            {
                errorMess = Resource.PeriodEndDateLessThenCurrentDate;
                return isRowVal;
            }

            if (!DateHelper.ValidatePeriod(Convert.ToDateTime(gridView.GetRowCellValue(rowHandle, MappedNames.DateStartPeriod)).Date, Convert.ToDateTime(gridView.GetRowCellValue(rowHandle, MappedNames.DateEndPeriod)).Date))
            {
                errorMess = Resource.IncorrectFromToDates;
                return isRowVal;
            }

            List<string> errors = new List<string>();

            AimsDataSet aims = new CascadedAimsDataProvider().GetCrossedAims(aim);
            DataView view = aims.Aim.DefaultView;
            view.RowFilter = string.Format
                ("{0} = {1} AND {2} <> {3}",
                    MappedNames.IsFocused,
                    true.ToString(),
                    MappedNames.AimId,
                    aim.AimId
                );

            if (view.Count > 0)
            {
                errors.Add(
                        string.Format(
                        "Фокусное СКЮ <{0}> с периодом действия с <{1}> по <{2}> совпадает с",
                        aim.ProductCombineName,
                        DateHelper.PeriodBorderToString(aim.DateStartPeriod),
                        DateHelper.PeriodBorderToString(aim.DateEndPeriod)
                        )
                    );
                foreach (DataRowView rowView in view)
                {
                    errors.Add(
                        string.Format(
                            "   СКЮ {0} с периодом с <{1}> <{2}> ",
                            rowView[MappedNames.ProductCombineName],
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateStartPeriod])),
                            DateHelper.PeriodBorderToString(DateHelper.GetDBNullDateChecked(rowView[MappedNames.DateEndPeriod]))
                        )
                        );
                }
            }

            if (errors.Count > 0)
            {
                string s = string.Empty;

                foreach (string str in errors)
                {
                    s += str + ";\n";
                }

                errorMess = s;
                return isRowVal;
            }

            errorMess = string.Empty;
            isRowVal = true;

            return isRowVal;
        }

        private bool CouldValueBeEdited(string fieldName, out string mess)
        {
            bool cancel = true;
            mess = "Нельзя редактировать поле " + LocalizationProvider.GetText(fieldName);

            bool lHasPositionCoPpmM5 = false;//DataAccessProvider.HasUserPosition(DataAccessProvider.PositionCoPpmM5);

            //DateStartPeriod
            if (fieldName == MappedNames.DateStartPeriod && Convert.ToBoolean(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.IsStartDateApproved)))
            {
                cancel = false;
            }

            //DateEndPeriod
            if (fieldName == MappedNames.DateEndPeriod && Convert.ToBoolean(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            //IsStartDateApproved
            if (!lHasPositionCoPpmM5 && fieldName == MappedNames.IsStartDateApproved && Convert.ToDateTime(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.DateStartPeriod)) < DateTime.Now.Date)
            {
                cancel = false;
            }

            if (fieldName == MappedNames.IsStartDateApproved && Convert.ToBoolean(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            //IsEndDateApproved
            if (!lHasPositionCoPpmM5 && fieldName == MappedNames.IsEndDateApproved && Convert.ToDateTime(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.DateEndPeriod)) < DateTime.Now.Date && !Convert.ToBoolean(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            if (fieldName == MappedNames.IsEndDateApproved && Convert.ToDateTime(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.DateEndPeriod)) < Convert.ToDateTime(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.DateStartPeriod)))
            {
                cancel = false;
            }

            if (fieldName == MappedNames.IsEndDateApproved && !Convert.ToBoolean(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.IsStartDateApproved)))
            {
                cancel = false;
            }

            //KPIValue
            if (fieldName == MappedNames.IsFocused && Convert.ToBoolean(gridView.GetRowCellValue(gridView.FocusedRowHandle, MappedNames.IsEndDateApproved)))
            {
                cancel = false;
            }

            if (cancel)
            {
                mess = string.Empty;
            }
            return cancel;
        }

        private bool CouldAllRowsBeEdited(List<int> rows, string fieldName, ref int firstRowNum, object value, out string errorMess)
        {
            checkingMode = true;

            errorMess = string.Empty;
            bool incorRow = false;

            if (Convert.ToBoolean(rbEditType.EditValue))
            {
                gridView.CellValueChanging -= gridView_CellValueChanging;

                prevValue = gridView.GetRowCellValue(firstRowNum, fieldName);
                gridView.SetRowCellValue(firstRowNum, fieldName, value);

                if (IsRowValid(firstRowNum, out errorMess))
                {
                    WaitManager.StartWait();
                    gridView.SetRowCellValue(firstRowNum, fieldName, prevValue);

                    foreach (int i in rows)
                    {
                        if (i != firstRowNum)
                        {
                            gridView.FocusedRowHandle = i;

                            if (CouldValueBeEdited(fieldName, out errorMess))
                            {
                                prevValue = gridView.GetRowCellValue(i, fieldName);
                                gridView.SetRowCellValue(i, fieldName, value);
                            }
                            else
                            {
                                incorRow = true;
                                firstRowNum = i;
                                break;
                            }

                            if (!IsRowValid(i, out errorMess))
                            {
                                gridView.SetRowCellValue(i, fieldName, prevValue);
                                incorRow = true;
                                firstRowNum = i;
                                break;
                            }

                            gridView.SetRowCellValue(i, fieldName, prevValue);
                        }
                    }

                    WaitManager.StopWait();
                }
                else
                {
                    gridView.SetRowCellValue(firstRowNum, fieldName, prevValue);
                    incorRow = true;
                }

                gridView.CellValueChanging += gridView_CellValueChanging;
            }

            checkingMode = false;

            return !incorRow;
        }

        private int FindRowHandleByDataRow(DataRow row)
        {

            if (row != null)

                for (int i = 0; i < gridView.DataRowCount; i++)

                    if (gridView.GetDataRow(i) == row)

                        return i;

            return DevExpress.XtraGrid.GridControl.InvalidRowHandle;
        }
        #endregion Private methods

        #region Grid Columns definition
        private GridColumn gcAimId = new GridColumn();
        private GridColumn gcProductCombineName = new GridColumn();
        private GridColumn gcDateStartPeriod = new GridColumn();
        private GridColumn gcIsStartDateApproved = new GridColumn();
        private GridColumn gcDateEndPeriod = new GridColumn();
        private GridColumn gcIsEndDateApproved = new GridColumn();
        private GridColumn gcKPI = new GridColumn();
        private GridColumn gcKPIValue = new GridColumn();
        private GridColumn gcProximity = new GridColumn();
        private GridColumn gcSettlement = new GridColumn();
        private GridColumn gcDSM = new GridColumn();
        private GridColumn gcSupervisor = new GridColumn();
        private GridColumn gcOutletGroup = new GridColumn();
        private GridColumn gcOutletType = new GridColumn();
        private GridColumn gcOutletSubType = new GridColumn();
        private GridColumn gcIsFocused = new GridColumn();
        private GridColumn gcComposedRegionName = new GridColumn();
        private GridColumn gcComposedDistrictName = new GridColumn();
        private GridColumn gcComposedCityName = new GridColumn();
        #endregion Grid Columns definition
    }
}
