﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;

namespace Logica.Reports.RPI.UserControls
{
    public partial class AnalysisOfEfficacyInControllingRetailPricesM2Report : UserControl, IRPIModule
    {
        private DateTime _reportStartDate;
        private DateTime _reportEndDate;

        public AnalysisOfEfficacyInControllingRetailPricesM2Report()
        {
            InitializeComponent();
        }

        public string ModuleName
        {
            get { return Resource.AnalysisOfEfficacyInControllingRetailPricesM2Report; }
        }

        /// <summary>
        /// Start Date of Report
        /// </summary>
        public DateTime ReportStartDate
        {
            get { return _reportStartDate; }
            set { _reportStartDate = value; }
        }

        /// <summary>
        /// End Date of Report
        /// </summary>
        public DateTime ReportEndDate
        {
            get { return _reportEndDate; }
            set { _reportEndDate = value; }
        }

        public DSMEntity DSM { get; set; }
        public SupervisorEntity Supervisor { get; set; }
        public MerchandiserEntity Merchandiser { get; set; }
        public RouteEntity Route { get; set; }
    }

}
