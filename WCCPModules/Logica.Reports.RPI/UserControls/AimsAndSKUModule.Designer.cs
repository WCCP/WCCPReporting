﻿namespace Logica.Reports.RPI.UserControls
{
    partial class AimsAndSKUModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcAimAndSKU = new System.Windows.Forms.TabControl();
            this.tpAim = new System.Windows.Forms.TabPage();
            this.cascadedAimsModule1 = new Logica.Reports.RPI.UserControls.CascadedAimsModule();
            this.tpSKU = new System.Windows.Forms.TabPage();
            this.focusedSKUModule1 = new Logica.Reports.RPI.UserControls.FocusedSKUModule();
            this.tcAimAndSKU.SuspendLayout();
            this.tpAim.SuspendLayout();
            this.tpSKU.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcAimAndSKU
            // 
            this.tcAimAndSKU.Controls.Add(this.tpAim);
            this.tcAimAndSKU.Controls.Add(this.tpSKU);
            this.tcAimAndSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcAimAndSKU.Location = new System.Drawing.Point(0, 0);
            this.tcAimAndSKU.Name = "tcAimAndSKU";
            this.tcAimAndSKU.SelectedIndex = 0;
            this.tcAimAndSKU.Size = new System.Drawing.Size(751, 448);
            this.tcAimAndSKU.TabIndex = 0;
            // 
            // tpAim
            // 
            this.tpAim.Controls.Add(this.cascadedAimsModule1);
            this.tpAim.Location = new System.Drawing.Point(4, 22);
            this.tpAim.Name = "tpAim";
            this.tpAim.Padding = new System.Windows.Forms.Padding(3);
            this.tpAim.Size = new System.Drawing.Size(743, 422);
            this.tpAim.TabIndex = 0;
            this.tpAim.Text = "Цели";
            this.tpAim.UseVisualStyleBackColor = true;
            // 
            // cascadedAimsModule1
            // 
            this.cascadedAimsModule1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cascadedAimsModule1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cascadedAimsModule1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cascadedAimsModule1.Location = new System.Drawing.Point(3, 3);
            this.cascadedAimsModule1.Name = "cascadedAimsModule1";
            this.cascadedAimsModule1.Size = new System.Drawing.Size(737, 416);
            this.cascadedAimsModule1.TabIndex = 0;
            // 
            // tpSKU
            // 
            this.tpSKU.Controls.Add(this.focusedSKUModule1);
            this.tpSKU.Location = new System.Drawing.Point(4, 22);
            this.tpSKU.Name = "tpSKU";
            this.tpSKU.Padding = new System.Windows.Forms.Padding(3);
            this.tpSKU.Size = new System.Drawing.Size(743, 422);
            this.tpSKU.TabIndex = 1;
            this.tpSKU.Text = "Фокусные СКЮ";
            this.tpSKU.UseVisualStyleBackColor = true;
            // 
            // focusedSKUModule1
            // 
            this.focusedSKUModule1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.focusedSKUModule1.Location = new System.Drawing.Point(3, 3);
            this.focusedSKUModule1.Name = "focusedSKUModule1";
            this.focusedSKUModule1.Size = new System.Drawing.Size(737, 416);
            this.focusedSKUModule1.TabIndex = 0;
            // 
            // AimsAndSKUModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tcAimAndSKU);
            this.Name = "AimsAndSKUModule";
            this.Size = new System.Drawing.Size(751, 448);
            this.tcAimAndSKU.ResumeLayout(false);
            this.tpAim.ResumeLayout(false);
            this.tpSKU.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcAimAndSKU;
        private System.Windows.Forms.TabPage tpAim;
        private System.Windows.Forms.TabPage tpSKU;
        private CascadedAimsModule cascadedAimsModule1;
        private FocusedSKUModule focusedSKUModule1;
    }


}
