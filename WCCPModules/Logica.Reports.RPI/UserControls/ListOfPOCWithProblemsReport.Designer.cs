﻿namespace Logica.Reports.RPI.UserControls
{
    partial class ListOfPOCWithProblemsReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListOfPOCWithProblemsReport));
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.aimsBar = new DevExpress.XtraBars.Bar();
            this.btnRefreshAims = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportAllToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToHtml = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToMht = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToRtf = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToText = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToImage = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCollectionAims = new DevExpress.Utils.ImageCollection(this.components);
            this.btnShowSettings = new DevExpress.XtraBars.BarButtonItem();
            this.cascadedAimsToolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.gctlPOCwithProblems = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gctlPOCwithProblems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar [] {
            this.aimsBar});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageCollectionAims;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem [] {
            this.btnRefreshAims,
            this.btnShowSettings,
            this.btnExportTo,
            this.btnExportAllToExcel,
            this.btnExportAllToPdf,
            this.btnExportAllToHtml,
            this.btnExportAllToMht,
            this.btnExportAllToRtf,
            this.btnExportAllToText,
            this.btnExportAllToImage,
            this.btnExportAllToCsv,
            this.btnPrint});
            this.barManager1.LargeImages = this.imageCollectionAims;
            this.barManager1.MaxItemId = 30;
            this.barManager1.ToolTipController = this.cascadedAimsToolTipController;
            // 
            // aimsBar
            // 
            this.aimsBar.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.aimsBar.Appearance.Options.UseFont = true;
            this.aimsBar.BarName = "Tools";
            this.aimsBar.DockCol = 0;
            this.aimsBar.DockRow = 0;
            this.aimsBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.aimsBar.FloatLocation = new System.Drawing.Point(650, 209);
            this.aimsBar.FloatSize = new System.Drawing.Size(46, 300);
            this.aimsBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo [] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefreshAims),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTo, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint)});
            this.aimsBar.Text = "Tools";
            // 
            // btnRefreshAims
            // 
            this.btnRefreshAims.Id = 5;
            this.btnRefreshAims.ImageIndex = 6;
            this.btnRefreshAims.ImageIndexDisabled = 13;
            this.btnRefreshAims.LargeImageIndex = 6;
            this.btnRefreshAims.LargeImageIndexDisabled = 13;
            this.btnRefreshAims.Name = "btnRefreshAims";
            // 
            // btnExportTo
            // 
            this.btnExportTo.Caption = "Экспортировать в...";
            this.btnExportTo.Id = 7;
            this.btnExportTo.LargeImageIndex = 23;
            this.btnExportTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo [] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToHtml),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToMht),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToRtf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToText),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToImage),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToCsv)});
            this.btnExportTo.Name = "btnExportTo";
            // 
            // btnExportAllToExcel
            // 
            this.btnExportAllToExcel.Caption = "Xls";
            this.btnExportAllToExcel.Id = 9;
            this.btnExportAllToExcel.LargeImageIndex = 14;
            this.btnExportAllToExcel.Name = "btnExportAllToExcel";
            this.btnExportAllToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportAllToPdf
            // 
            this.btnExportAllToPdf.Caption = "Pdf";
            this.btnExportAllToPdf.Id = 10;
            this.btnExportAllToPdf.LargeImageIndex = 21;
            this.btnExportAllToPdf.Name = "btnExportAllToPdf";
            this.btnExportAllToPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportAllToHtml
            // 
            this.btnExportAllToHtml.Caption = "Html";
            this.btnExportAllToHtml.Id = 11;
            this.btnExportAllToHtml.LargeImageIndex = 18;
            this.btnExportAllToHtml.Name = "btnExportAllToHtml";
            this.btnExportAllToHtml.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportAllToMht
            // 
            this.btnExportAllToMht.Caption = "Mht";
            this.btnExportAllToMht.Id = 12;
            this.btnExportAllToMht.LargeImageIndex = 19;
            this.btnExportAllToMht.Name = "btnExportAllToMht";
            this.btnExportAllToMht.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportAllToRtf
            // 
            this.btnExportAllToRtf.Caption = "Rtf";
            this.btnExportAllToRtf.Id = 13;
            this.btnExportAllToRtf.LargeImageIndex = 22;
            this.btnExportAllToRtf.Name = "btnExportAllToRtf";
            this.btnExportAllToRtf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportAllToText
            // 
            this.btnExportAllToText.Caption = "Text";
            this.btnExportAllToText.Id = 14;
            this.btnExportAllToText.LargeImageIndex = 20;
            this.btnExportAllToText.Name = "btnExportAllToText";
            this.btnExportAllToText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportAllToImage
            // 
            this.btnExportAllToImage.Caption = "Image";
            this.btnExportAllToImage.Id = 15;
            this.btnExportAllToImage.LargeImageIndex = 24;
            this.btnExportAllToImage.Name = "btnExportAllToImage";
            this.btnExportAllToImage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnExportAllToImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportAllToCsv
            // 
            this.btnExportAllToCsv.Caption = "Csv";
            this.btnExportAllToCsv.Id = 16;
            this.btnExportAllToCsv.LargeImageIndex = 17;
            this.btnExportAllToCsv.Name = "btnExportAllToCsv";
            this.btnExportAllToCsv.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnExportAllToCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAll_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Id = 8;
            this.btnPrint.LargeImageIndex = 15;
            this.btnPrint.Name = "btnPrint";
            // 
            // imageCollectionAims
            // 
            this.imageCollectionAims.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollectionAims.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionAims.ImageStream")));
            this.imageCollectionAims.Images.SetKeyName(0, "doc_24.png");
            this.imageCollectionAims.Images.SetKeyName(1, "doc_add_24.png");
            this.imageCollectionAims.Images.SetKeyName(2, "doc_edit_24.png");
            this.imageCollectionAims.Images.SetKeyName(3, "doc_back_24.png");
            this.imageCollectionAims.Images.SetKeyName(4, "doc_next_24.png");
            this.imageCollectionAims.Images.SetKeyName(5, "del_24.png");
            this.imageCollectionAims.Images.SetKeyName(6, "refresh_24.png");
            this.imageCollectionAims.Images.SetKeyName(7, "doc_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(8, "doc_add_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(9, "doc_edit_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(10, "doc_back_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(11, "doc_next_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(12, "del_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(13, "refresh_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(14, "Excel_24.png");
            this.imageCollectionAims.Images.SetKeyName(15, "print_24.png");
            this.imageCollectionAims.Images.SetKeyName(16, "briefcase_ok_24.png");
            this.imageCollectionAims.Images.SetKeyName(17, "csv.PNG");
            this.imageCollectionAims.Images.SetKeyName(18, "html.PNG");
            this.imageCollectionAims.Images.SetKeyName(19, "mhtml.PNG");
            this.imageCollectionAims.Images.SetKeyName(20, "notepad.PNG");
            this.imageCollectionAims.Images.SetKeyName(21, "pdf.PNG");
            this.imageCollectionAims.Images.SetKeyName(22, "rft.PNG");
            this.imageCollectionAims.Images.SetKeyName(23, "image_refresh_24.png");
            this.imageCollectionAims.Images.SetKeyName(24, "bmp.PNG");
            this.imageCollectionAims.Images.SetKeyName(25, "briefcase_ok_24.png");
            // 
            // btnShowSettings
            // 
            this.btnShowSettings.Enabled = false;
            this.btnShowSettings.Id = 6;
            this.btnShowSettings.ImageIndex = 25;
            this.btnShowSettings.ImageIndexDisabled = 25;
            this.btnShowSettings.LargeImageIndex = 25;
            this.btnShowSettings.LargeImageIndexDisabled = 25;
            this.btnShowSettings.Name = "btnShowSettings";
            // 
            // cascadedAimsToolTipController
            // 
            this.cascadedAimsToolTipController.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.cascadedAimsToolTipController.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cascadedAimsToolTipController.Appearance.Options.UseBackColor = true;
            // 
            // gctlPOCwithProblems
            // 
            this.gctlPOCwithProblems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gctlPOCwithProblems.Location = new System.Drawing.Point(0, 34);
            this.gctlPOCwithProblems.MainView = this.gridView;
            this.gctlPOCwithProblems.MinimumSize = new System.Drawing.Size(414, 84);
            this.gctlPOCwithProblems.Name = "gctlPOCwithProblems";
            this.gctlPOCwithProblems.Size = new System.Drawing.Size(485, 259);
            this.gctlPOCwithProblems.TabIndex = 5;
            this.gctlPOCwithProblems.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView [] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 31;
            this.gridView.GridControl = this.gctlPOCwithProblems;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowIndicator = false;
            // 
            // ListOfPOCWithProblemsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gctlPOCwithProblems);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ListOfPOCWithProblemsReport";
            this.Size = new System.Drawing.Size(485, 293);
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gctlPOCwithProblems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar aimsBar;
        private DevExpress.XtraBars.BarButtonItem btnRefreshAims;
        private DevExpress.XtraBars.BarButtonItem btnShowSettings;
        private DevExpress.XtraBars.BarSubItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToExcel;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToPdf;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToHtml;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToMht;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToRtf;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToText;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToImage;
        private DevExpress.XtraBars.BarButtonItem btnExportAllToCsv;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imageCollectionAims;
        private DevExpress.Utils.ToolTipController cascadedAimsToolTipController;
        private DevExpress.XtraGrid.GridControl gctlPOCwithProblems;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    }

}
