﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using Logica.Reports.RPI.Forms;
using Logica.Reports.RPI.Common;
using Logica.Reports.RPI.Localization;
using Logica.Reports.RPI.DataProvider;
using Logica.Reports.RPI.DataProvider.Providers;
using Logica.Reports.RPI.DataProvider.Entities;
using Logica.Reports.RPI.DataProvider.Helpers;
using Logica.Reports.RPI.Authentication;

namespace Logica.Reports.RPI.UserControls
{

    /// <summary>
    /// User control which contains 2 controls: 1 - for Cascaded Aims peocessing, 2 - for FOcused SKU processing
    /// </summary>
    public partial class AimsAndSKUModule : UserControl, IRPIModule
    {
        #region Private members

        private DateTime _reportStartDate;
        private DateTime _reportEndDate;

        private bool isInitForTheFirstTime = false;

        #endregion Private members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public AimsAndSKUModule()
        {
            InitializeComponent();
            this.Invalidated += new InvalidateEventHandler(AimsAndSKUModule_Invalidated);
        }

        #endregion Constructors

        #region Public properties

        /// <summary>
        ///  Returns name of the module
        /// </summary>
        public string ModuleName
        {
            get { return "Comm"; }
        }

        /// <summary>
        /// Start Date of Report
        /// </summary>
        public DateTime ReportStartDate
        {
            get { return _reportStartDate; }
            set { _reportStartDate = value; }
        }

        /// <summary>
        /// End Date of Report
        /// </summary>
        public DateTime ReportEndDate
        {
            get { return _reportEndDate; }
            set { _reportEndDate = value; }
        }

        /// <summary>
        /// Selected DSM from Report parameters
        /// </summary>
        public DSMEntity DSM { get; set; }

        /// <summary>
        /// Selected Supervisor from Report parameters
        /// </summary>
        public SupervisorEntity Supervisor { get; set; }

        /// <summary>
        /// Selected Merchandiser from Report parameters
        /// </summary>
        public MerchandiserEntity Merchandiser { get; set; }

        /// <summary>
        /// Selected Route from Report parameters
        /// </summary>
        public RouteEntity Route { get; set; }

        #endregion Public properties

        #region Event Handlers

        private void AimsAndSKUModule_Invalidated(object sender, InvalidateEventArgs e) {
            if (isInitForTheFirstTime)
                return;

            cascadedAimsModule1.InitialLoadData();
            focusedSKUModule1.InitialLoadData();

            cascadedAimsModule1.Focus();
//            cascadedAimsModule1.RegionsTree.HideSelection = false;
//            cascadedAimsModule1.RegionsTree.Select();
            cascadedAimsModule1.TerritoryTree.Select();

            isInitForTheFirstTime = true;
        }

        #endregion Event Handlers
    }

}
