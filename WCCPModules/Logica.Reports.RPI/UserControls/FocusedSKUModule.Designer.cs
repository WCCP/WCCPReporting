﻿namespace Logica.Reports.RPI.UserControls
{
    partial class FocusedSKUModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FocusedSKUModule));
            this.pnlTop = new DevExpress.XtraEditors.PanelControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.aimsBar = new DevExpress.XtraBars.Bar();
            this.btnNewFocusedSKU = new DevExpress.XtraBars.BarButtonItem();
            this.btnCopyFocusedSKU = new DevExpress.XtraBars.BarButtonItem();
            this.btnEditFocusedSKU = new DevExpress.XtraBars.BarButtonItem();
            this.btnApproveSKUStartDate = new DevExpress.XtraBars.BarButtonItem();
            this.btnApproveSKUEndDate = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteFocusedSKU = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefreshFocusedSKUList = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrintPreview = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCollectionAims = new DevExpress.Utils.ImageCollection();
            this.pnlMiddle = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerMiddle = new DevExpress.XtraEditors.SplitContainerControl();
            this.pnlRegionsTree = new DevExpress.XtraEditors.PanelControl();
            this.tvRegions = new System.Windows.Forms.TreeView();
            this.pnlDataGrid = new DevExpress.XtraEditors.PanelControl();
            this.pnlDataGridBottom = new DevExpress.XtraEditors.PanelControl();
            this.tcFilters = new DevExpress.XtraTab.XtraTabControl();
            this.tpFilters = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.rbEditType = new DevExpress.XtraEditors.RadioGroup();
            this.chkFilterValidAimsForCurrentDate = new DevExpress.XtraEditors.CheckEdit();
            this.chkFilterOnlyApprovedAims = new DevExpress.XtraEditors.CheckEdit();
            this.deFilterAimsTo = new DevExpress.XtraEditors.DateEdit();
            this.deFilterAimsFrom = new DevExpress.XtraEditors.DateEdit();
            this.lblFilterAimsTo = new DevExpress.XtraEditors.LabelControl();
            this.lblFilterAimsFrom = new DevExpress.XtraEditors.LabelControl();
            this.pnlDataGridCenter = new DevExpress.XtraEditors.PanelControl();
            this.gctlCascadedAims = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).BeginInit();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMiddle)).BeginInit();
            this.pnlMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMiddle)).BeginInit();
            this.splitContainerMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegionsTree)).BeginInit();
            this.pnlRegionsTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGrid)).BeginInit();
            this.pnlDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridBottom)).BeginInit();
            this.pnlDataGridBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcFilters)).BeginInit();
            this.tcFilters.SuspendLayout();
            this.tpFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterValidAimsForCurrentDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterOnlyApprovedAims.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridCenter)).BeginInit();
            this.pnlDataGridCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gctlCascadedAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCheckEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTop.Controls.Add(this.standaloneBarDockControl1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(863, 35);
            this.pnlTop.TabIndex = 5;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.MinimumSize = new System.Drawing.Size(431, 14);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(863, 35);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.aimsBar,
            this.bar3});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageCollectionAims;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnNewFocusedSKU,
            this.btnCopyFocusedSKU,
            this.btnEditFocusedSKU,
            this.btnApproveSKUStartDate,
            this.btnApproveSKUEndDate,
            this.btnDeleteFocusedSKU,
            this.btnRefreshFocusedSKUList,
            this.btnExportToExcel,
            this.btnPrintPreview});
            this.barManager1.LargeImages = this.imageCollectionAims;
            this.barManager1.MaxItemId = 27;
            this.barManager1.StatusBar = this.bar3;
            // 
            // aimsBar
            // 
            this.aimsBar.BarAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.aimsBar.BarAppearance.Normal.Options.UseFont = true;
            this.aimsBar.BarName = "Tools";
            this.aimsBar.DockCol = 0;
            this.aimsBar.DockRow = 0;
            this.aimsBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.aimsBar.FloatLocation = new System.Drawing.Point(650, 209);
            this.aimsBar.FloatSize = new System.Drawing.Size(46, 300);
            this.aimsBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNewFocusedSKU),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCopyFocusedSKU, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEditFocusedSKU),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnApproveSKUStartDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnApproveSKUEndDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDeleteFocusedSKU, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefreshFocusedSKUList),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrintPreview)});
            this.aimsBar.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.aimsBar.Text = "Tools";
            // 
            // btnNewFocusedSKU
            // 
            this.btnNewFocusedSKU.Id = 0;
            this.btnNewFocusedSKU.ImageIndex = 0;
            this.btnNewFocusedSKU.ImageIndexDisabled = 7;
            this.btnNewFocusedSKU.LargeImageIndex = 0;
            this.btnNewFocusedSKU.LargeImageIndexDisabled = 7;
            this.btnNewFocusedSKU.Name = "btnNewFocusedSKU";
            this.btnNewFocusedSKU.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnCopyFocusedSKU
            // 
            this.btnCopyFocusedSKU.Id = 1;
            this.btnCopyFocusedSKU.ImageIndex = 1;
            this.btnCopyFocusedSKU.ImageIndexDisabled = 8;
            this.btnCopyFocusedSKU.LargeImageIndex = 1;
            this.btnCopyFocusedSKU.LargeImageIndexDisabled = 8;
            this.btnCopyFocusedSKU.Name = "btnCopyFocusedSKU";
            this.btnCopyFocusedSKU.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnEditFocusedSKU
            // 
            this.btnEditFocusedSKU.Id = 2;
            this.btnEditFocusedSKU.ImageIndex = 2;
            this.btnEditFocusedSKU.ImageIndexDisabled = 9;
            this.btnEditFocusedSKU.LargeImageIndex = 2;
            this.btnEditFocusedSKU.LargeImageIndexDisabled = 9;
            this.btnEditFocusedSKU.Name = "btnEditFocusedSKU";
            this.btnEditFocusedSKU.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnApproveSKUStartDate
            // 
            this.btnApproveSKUStartDate.Id = 3;
            this.btnApproveSKUStartDate.ImageIndex = 3;
            this.btnApproveSKUStartDate.ImageIndexDisabled = 10;
            this.btnApproveSKUStartDate.LargeImageIndex = 3;
            this.btnApproveSKUStartDate.LargeImageIndexDisabled = 10;
            this.btnApproveSKUStartDate.Name = "btnApproveSKUStartDate";
            this.btnApproveSKUStartDate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnApproveSKUEndDate
            // 
            this.btnApproveSKUEndDate.Id = 4;
            this.btnApproveSKUEndDate.ImageIndex = 4;
            this.btnApproveSKUEndDate.ImageIndexDisabled = 11;
            this.btnApproveSKUEndDate.LargeImageIndex = 4;
            this.btnApproveSKUEndDate.LargeImageIndexDisabled = 11;
            this.btnApproveSKUEndDate.Name = "btnApproveSKUEndDate";
            this.btnApproveSKUEndDate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnDeleteFocusedSKU
            // 
            this.btnDeleteFocusedSKU.Id = 5;
            this.btnDeleteFocusedSKU.ImageIndex = 5;
            this.btnDeleteFocusedSKU.ImageIndexDisabled = 12;
            this.btnDeleteFocusedSKU.LargeImageIndex = 5;
            this.btnDeleteFocusedSKU.LargeImageIndexDisabled = 12;
            this.btnDeleteFocusedSKU.Name = "btnDeleteFocusedSKU";
            this.btnDeleteFocusedSKU.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnRefreshFocusedSKUList
            // 
            this.btnRefreshFocusedSKUList.Id = 15;
            this.btnRefreshFocusedSKUList.ImageIndex = 6;
            this.btnRefreshFocusedSKUList.ImageIndexDisabled = 13;
            this.btnRefreshFocusedSKUList.LargeImageIndex = 6;
            this.btnRefreshFocusedSKUList.LargeImageIndexDisabled = 13;
            this.btnRefreshFocusedSKUList.Name = "btnRefreshFocusedSKUList";
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Id = 25;
            this.btnExportToExcel.LargeImageIndex = 14;
            this.btnExportToExcel.Name = "btnExportToExcel";
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.Id = 26;
            this.btnPrintPreview.LargeImageIndex = 15;
            this.btnPrintPreview.Name = "btnPrintPreview";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(863, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 324);
            this.barDockControlBottom.Size = new System.Drawing.Size(863, 20);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 324);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(863, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 324);
            // 
            // imageCollectionAims
            // 
            this.imageCollectionAims.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollectionAims.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionAims.ImageStream")));
            this.imageCollectionAims.Images.SetKeyName(0, "doc_24.png");
            this.imageCollectionAims.Images.SetKeyName(1, "doc_add_24.png");
            this.imageCollectionAims.Images.SetKeyName(2, "doc_edit_24.png");
            this.imageCollectionAims.Images.SetKeyName(3, "doc_back_24.png");
            this.imageCollectionAims.Images.SetKeyName(4, "doc_next_24.png");
            this.imageCollectionAims.Images.SetKeyName(5, "del_24.png");
            this.imageCollectionAims.Images.SetKeyName(6, "refresh_24.png");
            this.imageCollectionAims.Images.SetKeyName(7, "doc_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(8, "doc_add_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(9, "doc_edit_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(10, "doc_back_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(11, "doc_next_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(12, "del_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(13, "refresh_24_dis.png");
            this.imageCollectionAims.Images.SetKeyName(14, "Excel_24.png");
            this.imageCollectionAims.Images.SetKeyName(15, "print_24.png");
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMiddle.Controls.Add(this.splitContainerMiddle);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Location = new System.Drawing.Point(0, 35);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(863, 289);
            this.pnlMiddle.TabIndex = 10;
            // 
            // splitContainerMiddle
            // 
            this.splitContainerMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerMiddle.Location = new System.Drawing.Point(0, 0);
            this.splitContainerMiddle.Name = "splitContainerMiddle";
            this.splitContainerMiddle.Panel1.Controls.Add(this.pnlRegionsTree);
            this.splitContainerMiddle.Panel1.Text = "Panel1";
            this.splitContainerMiddle.Panel2.Controls.Add(this.pnlDataGrid);
            this.splitContainerMiddle.Panel2.Text = "Panel2";
            this.splitContainerMiddle.Size = new System.Drawing.Size(863, 289);
            this.splitContainerMiddle.SplitterPosition = 209;
            this.splitContainerMiddle.TabIndex = 0;
            this.splitContainerMiddle.Text = "splitContainerControl1";
            // 
            // pnlRegionsTree
            // 
            this.pnlRegionsTree.AutoSize = true;
            this.pnlRegionsTree.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlRegionsTree.Controls.Add(this.tvRegions);
            this.pnlRegionsTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRegionsTree.Location = new System.Drawing.Point(0, 0);
            this.pnlRegionsTree.Name = "pnlRegionsTree";
            this.pnlRegionsTree.Size = new System.Drawing.Size(209, 289);
            this.pnlRegionsTree.TabIndex = 2;
            // 
            // tvRegions
            // 
            this.tvRegions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvRegions.Location = new System.Drawing.Point(3, 3);
            this.tvRegions.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.tvRegions.Name = "tvRegions";
            this.tvRegions.Size = new System.Drawing.Size(203, 283);
            this.tvRegions.TabIndex = 0;
            // 
            // pnlDataGrid
            // 
            this.pnlDataGrid.AutoSize = true;
            this.pnlDataGrid.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlDataGrid.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDataGrid.Controls.Add(this.pnlDataGridBottom);
            this.pnlDataGrid.Controls.Add(this.pnlDataGridCenter);
            this.pnlDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDataGrid.Location = new System.Drawing.Point(0, 0);
            this.pnlDataGrid.MinimumSize = new System.Drawing.Size(150, 200);
            this.pnlDataGrid.Name = "pnlDataGrid";
            this.pnlDataGrid.Size = new System.Drawing.Size(649, 289);
            this.pnlDataGrid.TabIndex = 2;
            // 
            // pnlDataGridBottom
            // 
            this.pnlDataGridBottom.Controls.Add(this.tcFilters);
            this.pnlDataGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDataGridBottom.Location = new System.Drawing.Point(0, 174);
            this.pnlDataGridBottom.Name = "pnlDataGridBottom";
            this.pnlDataGridBottom.Size = new System.Drawing.Size(649, 115);
            this.pnlDataGridBottom.TabIndex = 7;
            // 
            // tcFilters
            // 
            this.tcFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcFilters.Location = new System.Drawing.Point(2, 2);
            this.tcFilters.MinimumSize = new System.Drawing.Size(422, 112);
            this.tcFilters.Name = "tcFilters";
            this.tcFilters.SelectedTabPage = this.tpFilters;
            this.tcFilters.Size = new System.Drawing.Size(645, 112);
            this.tcFilters.TabIndex = 7;
            this.tcFilters.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpFilters});
            // 
            // tpFilters
            // 
            this.tpFilters.Controls.Add(this.labelControl1);
            this.tpFilters.Controls.Add(this.rbEditType);
            this.tpFilters.Controls.Add(this.chkFilterValidAimsForCurrentDate);
            this.tpFilters.Controls.Add(this.chkFilterOnlyApprovedAims);
            this.tpFilters.Controls.Add(this.deFilterAimsTo);
            this.tpFilters.Controls.Add(this.deFilterAimsFrom);
            this.tpFilters.Controls.Add(this.lblFilterAimsTo);
            this.tpFilters.Controls.Add(this.lblFilterAimsFrom);
            this.tpFilters.Name = "tpFilters";
            this.tpFilters.Size = new System.Drawing.Size(639, 84);
            this.tpFilters.Text = "Фильтры";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(442, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 13);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Режим редактирования";
            // 
            // rbEditType
            // 
            this.rbEditType.EditValue = false;
            this.rbEditType.Location = new System.Drawing.Point(442, 30);
            this.rbEditType.Name = "rbEditType";
            this.rbEditType.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.rbEditType.Properties.Appearance.Options.UseBackColor = true;
            this.rbEditType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Одиночный"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Груповой")});
            this.rbEditType.Size = new System.Drawing.Size(165, 23);
            this.rbEditType.TabIndex = 12;
            // 
            // chkFilterValidAimsForCurrentDate
            // 
            this.chkFilterValidAimsForCurrentDate.Location = new System.Drawing.Point(7, 60);
            this.chkFilterValidAimsForCurrentDate.MenuManager = this.barManager1;
            this.chkFilterValidAimsForCurrentDate.Name = "chkFilterValidAimsForCurrentDate";
            this.chkFilterValidAimsForCurrentDate.Properties.Caption = "Показывать СКЮ действующие на текущую дату";
            this.chkFilterValidAimsForCurrentDate.Size = new System.Drawing.Size(388, 19);
            this.chkFilterValidAimsForCurrentDate.TabIndex = 5;
            // 
            // chkFilterOnlyApprovedAims
            // 
            this.chkFilterOnlyApprovedAims.Location = new System.Drawing.Point(7, 35);
            this.chkFilterOnlyApprovedAims.MenuManager = this.barManager1;
            this.chkFilterOnlyApprovedAims.Name = "chkFilterOnlyApprovedAims";
            this.chkFilterOnlyApprovedAims.Properties.Caption = "Показывать только утвержденные СКЮ";
            this.chkFilterOnlyApprovedAims.Size = new System.Drawing.Size(388, 19);
            this.chkFilterOnlyApprovedAims.TabIndex = 4;
            // 
            // deFilterAimsTo
            // 
            this.deFilterAimsTo.EditValue = null;
            this.deFilterAimsTo.Location = new System.Drawing.Point(295, 9);
            this.deFilterAimsTo.MenuManager = this.barManager1;
            this.deFilterAimsTo.Name = "deFilterAimsTo";
            this.deFilterAimsTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFilterAimsTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFilterAimsTo.Size = new System.Drawing.Size(100, 20);
            this.deFilterAimsTo.TabIndex = 3;
            // 
            // deFilterAimsFrom
            // 
            this.deFilterAimsFrom.EditValue = null;
            this.deFilterAimsFrom.Location = new System.Drawing.Point(138, 9);
            this.deFilterAimsFrom.MenuManager = this.barManager1;
            this.deFilterAimsFrom.Name = "deFilterAimsFrom";
            this.deFilterAimsFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFilterAimsFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFilterAimsFrom.Size = new System.Drawing.Size(100, 20);
            this.deFilterAimsFrom.TabIndex = 2;
            // 
            // lblFilterAimsTo
            // 
            this.lblFilterAimsTo.Location = new System.Drawing.Point(262, 12);
            this.lblFilterAimsTo.Name = "lblFilterAimsTo";
            this.lblFilterAimsTo.Size = new System.Drawing.Size(12, 13);
            this.lblFilterAimsTo.TabIndex = 1;
            this.lblFilterAimsTo.Text = "по";
            // 
            // lblFilterAimsFrom
            // 
            this.lblFilterAimsFrom.Location = new System.Drawing.Point(9, 12);
            this.lblFilterAimsFrom.Name = "lblFilterAimsFrom";
            this.lblFilterAimsFrom.Size = new System.Drawing.Size(107, 13);
            this.lblFilterAimsFrom.TabIndex = 0;
            this.lblFilterAimsFrom.Text = "СКЮ действующие с";
            // 
            // pnlDataGridCenter
            // 
            this.pnlDataGridCenter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDataGridCenter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlDataGridCenter.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDataGridCenter.Controls.Add(this.gctlCascadedAims);
            this.pnlDataGridCenter.Location = new System.Drawing.Point(2, 3);
            this.pnlDataGridCenter.Name = "pnlDataGridCenter";
            this.pnlDataGridCenter.Size = new System.Drawing.Size(646, 168);
            this.pnlDataGridCenter.TabIndex = 3;
            // 
            // gctlCascadedAims
            // 
            this.gctlCascadedAims.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gctlCascadedAims.Location = new System.Drawing.Point(0, 0);
            this.gctlCascadedAims.MainView = this.gridView;
            this.gctlCascadedAims.MinimumSize = new System.Drawing.Size(414, 84);
            this.gctlCascadedAims.Name = "gctlCascadedAims";
            this.gctlCascadedAims.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryDateEdit,
            this.repositoryCheckEdit});
            this.gctlCascadedAims.Size = new System.Drawing.Size(646, 168);
            this.gctlCascadedAims.TabIndex = 2;
            this.gctlCascadedAims.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            this.gctlCascadedAims.DoubleClick += new System.EventHandler(this.gctlCascadedAims_DoubleClick);
            // 
            // gridView
            // 
            this.gridView.GridControl = this.gctlCascadedAims;
            this.gridView.Name = "gridView";
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
            this.gridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanging);
            this.gridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView_ValidateRow);
            // 
            // repositoryDateEdit
            // 
            this.repositoryDateEdit.AutoHeight = false;
            this.repositoryDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryDateEdit.Name = "repositoryDateEdit";
            // 
            // repositoryCheckEdit
            // 
            this.repositoryCheckEdit.AutoHeight = false;
            this.repositoryCheckEdit.Name = "repositoryCheckEdit";
            // 
            // FocusedSKUModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMiddle);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FocusedSKUModule";
            this.Size = new System.Drawing.Size(863, 344);
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).EndInit();
            this.pnlTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMiddle)).EndInit();
            this.pnlMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMiddle)).EndInit();
            this.splitContainerMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegionsTree)).EndInit();
            this.pnlRegionsTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGrid)).EndInit();
            this.pnlDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridBottom)).EndInit();
            this.pnlDataGridBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcFilters)).EndInit();
            this.tcFilters.ResumeLayout(false);
            this.tpFilters.ResumeLayout(false);
            this.tpFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterValidAimsForCurrentDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFilterOnlyApprovedAims.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFilterAimsFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDataGridCenter)).EndInit();
            this.pnlDataGridCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gctlCascadedAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCheckEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlTop;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar aimsBar;
        private DevExpress.XtraBars.BarButtonItem btnNewFocusedSKU;
        private DevExpress.XtraBars.BarButtonItem btnCopyFocusedSKU;
        private DevExpress.XtraBars.BarButtonItem btnEditFocusedSKU;
        private DevExpress.XtraBars.BarButtonItem btnApproveSKUStartDate;
        private DevExpress.XtraBars.BarButtonItem btnApproveSKUEndDate;
        private DevExpress.XtraBars.BarButtonItem btnDeleteFocusedSKU;
        private DevExpress.XtraBars.BarButtonItem btnRefreshFocusedSKUList;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imageCollectionAims;
        private DevExpress.XtraEditors.PanelControl pnlMiddle;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerMiddle;
        private DevExpress.XtraEditors.PanelControl pnlRegionsTree;
        private System.Windows.Forms.TreeView tvRegions;
        private DevExpress.XtraEditors.PanelControl pnlDataGrid;
        private DevExpress.XtraEditors.PanelControl pnlDataGridBottom;
        private DevExpress.XtraTab.XtraTabControl tcFilters;
        private DevExpress.XtraTab.XtraTabPage tpFilters;
        private DevExpress.XtraEditors.CheckEdit chkFilterValidAimsForCurrentDate;
        private DevExpress.XtraEditors.CheckEdit chkFilterOnlyApprovedAims;
        private DevExpress.XtraEditors.DateEdit deFilterAimsTo;
        private DevExpress.XtraEditors.DateEdit deFilterAimsFrom;
        private DevExpress.XtraEditors.LabelControl lblFilterAimsTo;
        private DevExpress.XtraEditors.LabelControl lblFilterAimsFrom;
        private DevExpress.XtraEditors.PanelControl pnlDataGridCenter;
        private DevExpress.XtraGrid.GridControl gctlCascadedAims;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraBars.BarButtonItem btnExportToExcel;
        private DevExpress.XtraBars.BarButtonItem btnPrintPreview;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryCheckEdit;
        private DevExpress.XtraEditors.RadioGroup rbEditType;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }


}
