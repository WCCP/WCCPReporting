﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SoftServe.Reports.RouteManagement.WcfRouterService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RouteParams", Namespace="http://schemas.datacontract.org/2004/07/WcfRouterCommon")]
    [System.SerializableAttribute()]
    public partial class RouteParams : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsRoundField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsRuField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool OptimizeItField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[] PointsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsRound {
            get {
                return this.IsRoundField;
            }
            set {
                if ((this.IsRoundField.Equals(value) != true)) {
                    this.IsRoundField = value;
                    this.RaisePropertyChanged("IsRound");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsRu {
            get {
                return this.IsRuField;
            }
            set {
                if ((this.IsRuField.Equals(value) != true)) {
                    this.IsRuField = value;
                    this.RaisePropertyChanged("IsRu");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool OptimizeIt {
            get {
                return this.OptimizeItField;
            }
            set {
                if ((this.OptimizeItField.Equals(value) != true)) {
                    this.OptimizeItField = value;
                    this.RaisePropertyChanged("OptimizeIt");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[] Points {
            get {
                return this.PointsField;
            }
            set {
                if ((object.ReferenceEquals(this.PointsField, value) != true)) {
                    this.PointsField = value;
                    this.RaisePropertyChanged("Points");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GeoPoint", Namespace="http://schemas.datacontract.org/2004/07/WcfRouterCommon")]
    [System.SerializableAttribute()]
    public partial class GeoPoint : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double LatField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double LonField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Lat {
            get {
                return this.LatField;
            }
            set {
                if ((this.LatField.Equals(value) != true)) {
                    this.LatField = value;
                    this.RaisePropertyChanged("Lat");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Lon {
            get {
                return this.LonField;
            }
            set {
                if ((this.LonField.Equals(value) != true)) {
                    this.LonField = value;
                    this.RaisePropertyChanged("Lon");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RouteResult", Namespace="http://schemas.datacontract.org/2004/07/WcfRouterCommon")]
    [System.SerializableAttribute()]
    public partial class RouteResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private short ErrorCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ErrorMessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string[] LabelsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[][] OptRouteSegmentsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double OptimizedDistanceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int[] OptimizedOrderField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[] OptimizedRouteField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int[] RejectedPointsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double SourceDistanceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[] SourcePointsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[][] SourceRouteField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public short ErrorCode {
            get {
                return this.ErrorCodeField;
            }
            set {
                if ((this.ErrorCodeField.Equals(value) != true)) {
                    this.ErrorCodeField = value;
                    this.RaisePropertyChanged("ErrorCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ErrorMessage {
            get {
                return this.ErrorMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.ErrorMessageField, value) != true)) {
                    this.ErrorMessageField = value;
                    this.RaisePropertyChanged("ErrorMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string[] Labels {
            get {
                return this.LabelsField;
            }
            set {
                if ((object.ReferenceEquals(this.LabelsField, value) != true)) {
                    this.LabelsField = value;
                    this.RaisePropertyChanged("Labels");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[][] OptRouteSegments {
            get {
                return this.OptRouteSegmentsField;
            }
            set {
                if ((object.ReferenceEquals(this.OptRouteSegmentsField, value) != true)) {
                    this.OptRouteSegmentsField = value;
                    this.RaisePropertyChanged("OptRouteSegments");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double OptimizedDistance {
            get {
                return this.OptimizedDistanceField;
            }
            set {
                if ((this.OptimizedDistanceField.Equals(value) != true)) {
                    this.OptimizedDistanceField = value;
                    this.RaisePropertyChanged("OptimizedDistance");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int[] OptimizedOrder {
            get {
                return this.OptimizedOrderField;
            }
            set {
                if ((object.ReferenceEquals(this.OptimizedOrderField, value) != true)) {
                    this.OptimizedOrderField = value;
                    this.RaisePropertyChanged("OptimizedOrder");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[] OptimizedRoute {
            get {
                return this.OptimizedRouteField;
            }
            set {
                if ((object.ReferenceEquals(this.OptimizedRouteField, value) != true)) {
                    this.OptimizedRouteField = value;
                    this.RaisePropertyChanged("OptimizedRoute");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int[] RejectedPoints {
            get {
                return this.RejectedPointsField;
            }
            set {
                if ((object.ReferenceEquals(this.RejectedPointsField, value) != true)) {
                    this.RejectedPointsField = value;
                    this.RaisePropertyChanged("RejectedPoints");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double SourceDistance {
            get {
                return this.SourceDistanceField;
            }
            set {
                if ((this.SourceDistanceField.Equals(value) != true)) {
                    this.SourceDistanceField = value;
                    this.RaisePropertyChanged("SourceDistance");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[] SourcePoints {
            get {
                return this.SourcePointsField;
            }
            set {
                if ((object.ReferenceEquals(this.SourcePointsField, value) != true)) {
                    this.SourcePointsField = value;
                    this.RaisePropertyChanged("SourcePoints");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public SoftServe.Reports.RouteManagement.WcfRouterService.GeoPoint[][] SourceRoute {
            get {
                return this.SourceRouteField;
            }
            set {
                if ((object.ReferenceEquals(this.SourceRouteField, value) != true)) {
                    this.SourceRouteField = value;
                    this.RaisePropertyChanged("SourceRoute");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MerchRouteParams", Namespace="http://schemas.datacontract.org/2004/07/WcfRouterCommon")]
    [System.SerializableAttribute()]
    public partial class MerchRouteParams : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsRuField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long Merch_IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime RouteDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private short RouteTypeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsRu {
            get {
                return this.IsRuField;
            }
            set {
                if ((this.IsRuField.Equals(value) != true)) {
                    this.IsRuField = value;
                    this.RaisePropertyChanged("IsRu");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long Merch_ID {
            get {
                return this.Merch_IDField;
            }
            set {
                if ((this.Merch_IDField.Equals(value) != true)) {
                    this.Merch_IDField = value;
                    this.RaisePropertyChanged("Merch_ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime RouteDate {
            get {
                return this.RouteDateField;
            }
            set {
                if ((this.RouteDateField.Equals(value) != true)) {
                    this.RouteDateField = value;
                    this.RaisePropertyChanged("RouteDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public short RouteType {
            get {
                return this.RouteTypeField;
            }
            set {
                if ((this.RouteTypeField.Equals(value) != true)) {
                    this.RouteTypeField = value;
                    this.RaisePropertyChanged("RouteType");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WcfRouterService.IRouterService")]
    public interface IRouterService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRouterService/BuildRoute", ReplyAction="http://tempuri.org/IRouterService/BuildRouteResponse")]
        SoftServe.Reports.RouteManagement.WcfRouterService.RouteResult BuildRoute(SoftServe.Reports.RouteManagement.WcfRouterService.RouteParams param);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRouterService/BuildMerchRoute", ReplyAction="http://tempuri.org/IRouterService/BuildMerchRouteResponse")]
        SoftServe.Reports.RouteManagement.WcfRouterService.RouteResult BuildMerchRoute(SoftServe.Reports.RouteManagement.WcfRouterService.MerchRouteParams param);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IRouterServiceChannel : SoftServe.Reports.RouteManagement.WcfRouterService.IRouterService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RouterServiceClient : System.ServiceModel.ClientBase<SoftServe.Reports.RouteManagement.WcfRouterService.IRouterService>, SoftServe.Reports.RouteManagement.WcfRouterService.IRouterService {
        
        public RouterServiceClient() {
        }
        
        public RouterServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public RouterServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RouterServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RouterServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public SoftServe.Reports.RouteManagement.WcfRouterService.RouteResult BuildRoute(SoftServe.Reports.RouteManagement.WcfRouterService.RouteParams param) {
            return base.Channel.BuildRoute(param);
        }
        
        public SoftServe.Reports.RouteManagement.WcfRouterService.RouteResult BuildMerchRoute(SoftServe.Reports.RouteManagement.WcfRouterService.MerchRouteParams param) {
            return base.Channel.BuildMerchRoute(param);
        }
    }
}
