﻿namespace SoftServe.Reports.RouteManagement.DataAccess {
    public class SqlConstants {
        // SP names
        public const string SpDwRmtCustomersGetList = "spDW_RMT_CustomersGetList";
        public const string SpDwRmtSupervisorsGetList = "spDW_RMT_SupervisorsGetList";
        public const string SpDwRmtMerchandisersGetList = "spDW_RMT_MerchandisersGetList";
        public const string SpDwRmtRoutesGetList = "spDW_RMT_RoutesGetList";
        public const string SpDwRmtRouteOutletsGetList = "spDW_RMT_RouteOutletsGetList";
        public const string SpDwRmtRouteView = "spDW_RMT_RouteView";
        public const string SpDwRmtRouteSet = "spDW_RMT_RouteSet";
        public const string SpDwRmtRouteSetDetail = "spDW_RMT_RouteSetDetail";
        public const string SpDwRmtRouteList = "spDW_RMT_RouteList";
        public const string SpDwRmtWorkWithRoute = "spDW_RMT_WorkWithRoute";
        public const string SpDwRmtInit = "spDW_RMT_Init";
        public const string SpDwRmtRouteSetDel = "spDW_RMT_RouteSetDel";
        public const string SpDwRmtOutletInfo = "spDW_RMT_OutletInfo";
        public const string SpDwRmtGetSaveSWStatus = "spDW_getSaveSWStatus";

        // Field Names
        public const string DistancFieldName = "Distance";
        public const string OptDistancFieldName = "OptDistance";
        public const string IdFieldName = "Id";
        public const string SupervisorIdFieldName = "Supervisor_id";
        public const string SupervisorNameFieldName = "Supervisor_name";
        public const string CustIdFieldName = "Cust_id";
        public const string CountryIdFieldName = "country_id";
        public const string CustNameFieldName = "Cust_Name";
        public const string MerchIdFieldName = "Merch_id";
        public const string MerchNameFieldName = "MerchName";
        public const string RouteIdFieldName = "Route_id";
        public const string RouteNameFieldName = "RouteName";
        public const string OlIdFieldName = "OL_id";
        public const string OlNameFieldName = "OLName";
        public const string OlTraidingNameFieldName = "OLTradingName";
        public const string OlAddressFieldName = "OLAddress";
        public const string OlDeliveryAddressFieldName = "OLDeliveryAddress";
        public const string OlNumberFieldName = "OL_Number";
        public const string LatitudeFieldName = "Latitude";
        public const string LongitudeFieldName = "Longitude";
        public const string RouteSetIdFieldName = "RouteSet_ID";
        public const string RouteSetDateFieldName = "RouteSetDate";
        public const string CommentFieldName = "Comment";
        public const string IsLoadFieldName = "isLoad";
        public const string CategoryFieldName = "Category";
        public const string OLtypeNameFieldName = "OLtype_name";
        public const string SaleSoldVolumeCurFieldName = "SALE_SoldVolumeCur";
        public const string MiddleCountSalesFieldName = "MiddleCountSales";
        public const string MiddleCountVisitsFieldName = "MiddleCountVisits";
        public const string MiddleSkuAttFieldName = "MiddleSkuAtt";
        public const string StrikeRateFieldName = "StrikeRate";

        // SP Params
        //public const string Param = "";
        public const string ParamId = "@ID";
        public const string ParamSupervisorId = "@SupervisorId";
        public const string ParamMerchId = "@MerchId";
        public const string ParamRouteId = "@RouteId";
        
        
        //spDW_RMT_RouteView params
        public const string ParamTypeOfRoute = "@TypeOfRoute";
        public const string ParamDate = "@Date";
        public const string ParamCust_Id = "@cust_id";
        public const string ParamEntityId = "@entityId";
        public const string ParamEntityType = "@entityType";
        
        //spDW_RMT_RouteSet params
        public const string ParamSupervisor_id = "@Supervisor_id";
        //spDW_RMT_RouteSetDetail params
        public const string ParamRouteSet_ID = "@RouteSet_ID";
        //spDW_RMT_WorkWithRoute params
        public const string ParamRouteSetDate = "@RouteSetDate";
        public const string ParamComment = "@Comment";
        public const string ParamIsLoad = "@isLoad";
        public const string ParamData = "@Data";
        public const string ParamDataRoute = "@DataRoute";

        //spDW_RMT_OutletInfo params
        public const string ParamOl_id = "@ol_id";
        public const string ParamType = "@type";

        //spDW_RMT_WorkWithRoute params


    }
}