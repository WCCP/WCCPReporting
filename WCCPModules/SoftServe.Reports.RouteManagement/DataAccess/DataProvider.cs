﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text;
using BLToolkit.Data;
using Logica.Reports.DataAccess;
using CreateConnection;
using SoftServe.Core.Common.Logging;
using WccpReporting;

namespace SoftServe.Reports.RouteManagement.DataAccess
{
    public static class DataProvider 
    {
        private static DbManager _db;

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManagerInit();
                }
                else if (CheckDbWasChanged())
                {
                    _db.Close();
                    DbManagerInit();
                }
                return _db;
            }
        }

        private static bool CheckDbWasChanged()
        {
            return !HostConfiguration.DBSqlConnection.Contains(_db.Connection.ConnectionString);
        }

        private static void DbManagerInit()
        {
            DbManager.DefaultConfiguration = ""; //to reset possible previous changes
            DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
            _db = new DbManager();
            _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
        }

        // Log Done
        public static DataTable GetCustomers()
        {
            Db.SetSpCommand(SqlConstants.SpDwRmtCustomersGetList);
            
            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.DataTable);
            
            if (result != null)
                return (DataTable) result;
            
            return Db.ExecuteDataTable();
        }

        // Log Done
        public static DataTable GetSupervisors(int custId = 0)
        {
            object customerId = DBNull.Value;
            if (custId != 0)
            {
                customerId = custId;
            }
            
                Db.SetSpCommand(SqlConstants.SpDwRmtSupervisorsGetList,
                    Db.Parameter(SqlConstants.ParamCust_Id, customerId));
            
                TimeParameter tp = WccpLogger.InitTimeParameter();
                object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                    SQLQueryType.DataTable);

                if (result != null)
                    return (DataTable)result;

            return Db.ExecuteDataTable();
        }

        // Log Done
        public static DataTable GetMerchandisers(int svId, int custId = 0)
        {
            object customerId = DBNull.Value;
            if (custId != 0)
            {
                customerId = custId;
            }
            
                Db.SetSpCommand(SqlConstants.SpDwRmtMerchandisersGetList,
                    Db.Parameter(SqlConstants.ParamSupervisorId, svId),
                    Db.Parameter(SqlConstants.ParamCust_Id, customerId));
                
            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.DataTable);
            
            if (result != null)
                return (DataTable)result;

            return Db.ExecuteDataTable();
        }

        [Obsolete("Use GetRouteView instead.")]
        public static DataTable GetRoutes(int merchId) {
            DataTable lDataTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpDwRmtRoutesGetList,
                    new SqlParameter(SqlConstants.ParamMerchId, merchId));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            
            return lDataTable;
        }

        [Obsolete("Use GetRouteView instead.")]
        public static DataTable GetRouteOutlets(long routeId) {
            DataTable lDataTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpDwRmtRouteOutletsGetList,
                    new SqlParameter(SqlConstants.ParamRouteId, routeId));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            
            return lDataTable;
        }

        // Log Done
        public static DataTable GetRouteView(int typeOfRoute, DateTime date, int custId, int entityId, int entityType)
        {
            Db.SetSpCommand(SqlConstants.SpDwRmtRouteView, Db.Parameter(SqlConstants.ParamTypeOfRoute, typeOfRoute),
                Db.Parameter(SqlConstants.ParamDate, date), Db.Parameter(SqlConstants.ParamCust_Id, custId),
                Db.Parameter(SqlConstants.ParamEntityId, entityId),
                Db.Parameter(SqlConstants.ParamEntityType, entityType));
            

            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.DataTable);

            if (result != null)
                return (DataTable)result;

            return Db.ExecuteDataTable();
        }

        // Log Done
        public static DataTable GetRouteSet(int svId)
        {
            Db.SetSpCommand(SqlConstants.SpDwRmtRouteSet, Db.Parameter(SqlConstants.ParamSupervisor_id, svId));
                  
            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.DataTable);

            if (result != null)
                return (DataTable)result;

            return  Db.ExecuteDataTable();
        }

        // Log Done
        public static DataTable GetRouteSetDetail(int routeSetId, short typeOfRoute)
        {
            Db.SetSpCommand(SqlConstants.SpDwRmtRouteSetDetail,
                Db.Parameter(SqlConstants.ParamRouteSet_ID, routeSetId),
                Db.Parameter(SqlConstants.ParamTypeOfRoute, typeOfRoute));

            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.DataTable);

            if (result != null)
                return (DataTable)result;

            return Db.ExecuteDataTable();
        }
       
        // Log Done
        public static void SaveRouteSet(int routeSetId, int svId, DateTime date, string nameOfRouteSet, int custId,
            bool isLoad, SqlXml xmlData, SqlXml xmlDataRoute)
        {
            if (DataAccessLayer.IsLDB)
            {
                try
                {
                    try
                    {
                        ConnectionToSWDB_V35 ConToSW_DDB = new ConnectionToSWDB_V35();
                        SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(ConToSW_DDB.Connection.ConnectionString);
                        sb.InitialCatalog = "LDB";
                        DataAccessLayer.SetConnectionString(sb.ConnectionString);
                        DataAccessLayer.OpenConnection();
                    }
                    catch (Exception)
                    {
                        throw new Exception("Ошибка подключения к SW базе: проверьте пароль SA.");
                    }
                    object lRsId = DBNull.Value;
                    if (routeSetId != 0)
                        lRsId = routeSetId;

                    DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpDwRmtWorkWithRoute,
                        new SqlParameter(SqlConstants.ParamRouteSet_ID, lRsId),
                        new SqlParameter(SqlConstants.ParamSupervisor_id, svId),
                        new SqlParameter(SqlConstants.ParamRouteSetDate, date),
                        new SqlParameter(SqlConstants.ParamComment, nameOfRouteSet),
                        new SqlParameter(SqlConstants.ParamCust_Id, custId),
                        new SqlParameter(SqlConstants.ParamIsLoad, isLoad),
                        new SqlParameter(SqlConstants.ParamData, xmlData),
                        new SqlParameter(SqlConstants.ParamDataRoute, xmlDataRoute));
                }
                finally
                {
                    DataAccessLayer.CloseConnection();
                    DataAccessLayer.SetConnectionStringToDefault();
                }
            }
            else
            {
                object rsId = DBNull.Value;
                if (routeSetId != 0)
                {
                    rsId = routeSetId;
                }

                
                Db.SetSpCommand(SqlConstants.SpDwRmtWorkWithRoute, Db.Parameter(SqlConstants.ParamRouteSet_ID, rsId),
                    Db.Parameter(SqlConstants.ParamSupervisor_id, svId),
                    Db.Parameter(SqlConstants.ParamRouteSetDate, date),
                    Db.Parameter(SqlConstants.ParamComment, nameOfRouteSet),
                    Db.Parameter(SqlConstants.ParamCust_Id, custId),
                    Db.Parameter(SqlConstants.ParamIsLoad, isLoad), Db.Parameter(SqlConstants.ParamData, xmlData),
                    Db.Parameter(SqlConstants.ParamDataRoute, xmlDataRoute));

                TimeParameter tp = WccpLogger.InitTimeParameter();
                object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                    SQLQueryType.NonQuery);

                if (result == null)
                    Db.ExecuteNonQuery();
            }
        }

        // Log Done
        public static void InitRmt()
        {
            Db.SetSpCommand(SqlConstants.SpDwRmtInit);
  
            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.NonQuery);

            if (result == null)
                Db.ExecuteNonQuery();
        }

        // Log Done
        public static void DeleteRouteSet(int routeSetId)
        {
            Db.SetSpCommand(SqlConstants.SpDwRmtRouteSetDel, Db.Parameter(SqlConstants.ParamRouteSet_ID, routeSetId));
            //Db.ExecuteNonQuery();

            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.NonQuery);

            if (result == null)
                Db.ExecuteNonQuery();
        }

        // Log Done
        public static DataTable GetFullInfo(long outletId)
        {
                Db.SetSpCommand(SqlConstants.SpDwRmtOutletInfo, Db.Parameter(SqlConstants.ParamOl_id, outletId),
                    Db.Parameter(SqlConstants.ParamType, 1));
                //return Db.ExecuteDataTable();
                TimeParameter tp = WccpLogger.InitTimeParameter();
                object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                    SQLQueryType.DataTable);

                if (result != null)
                    return (DataTable)result;

                return Db.ExecuteDataTable();
        }

        // Log Done
        public static DataTable GetRouteList(int entityType, int entityId)
        {
            Db.SetSpCommand(SqlConstants.SpDwRmtRouteList, Db.Parameter(SqlConstants.ParamEntityId, entityId),
                Db.Parameter(SqlConstants.ParamEntityType, entityType));
            //return Db.ExecuteDataTable();

            TimeParameter tp = WccpLogger.InitTimeParameter();
            object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                SQLQueryType.DataTable);

            if (result != null)
                    return (DataTable)result;

                return Db.ExecuteDataTable();
        }

        // Log Done
        public static bool GetSaveSWStatus(int supervisorId)
        {
                Db.SetSpCommand(SqlConstants.SpDwRmtGetSaveSWStatus,
                    Db.Parameter(SqlConstants.ParamSupervisor_id, supervisorId));
            
                TimeParameter tp = WccpLogger.InitTimeParameter();
                object result = WccpLogger.DoSafeDbTraceLoge(WccpUI._rpRMT, tp, Db,
                    SQLQueryType.Scalar);

            if (result != null)
                return Convert.ToInt32(result) == 1;

                return Db.ExecuteScalar<bool>();
        }
    }
}