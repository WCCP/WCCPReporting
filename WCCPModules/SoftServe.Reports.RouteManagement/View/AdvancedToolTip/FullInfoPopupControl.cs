﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using SoftServe.Reports.RouteManagement.Model.Outlet;

namespace SoftServe.Reports.RouteManagement.View.AdvancedToolTip {
    public partial class FullInfoPopupControl : PopupControl {
//        private Dictionary<long, OutletModel> _outletsToDisplay;
        private OutletModel _outletToDisplay;
        private List<OutletModel> _outletList; //= new List<OutletModel>();
        private int _currentIndex;

        public FullInfoPopupControl() {
            InitializeComponent();
            _currentIndex = 0;
        }

        public event OnClosePopupHandler OnClosePopup;

        public bool AllowHtmlText {
            get { return labelText.AllowHtmlString; }
            set { labelText.AllowHtmlString = value; }
        }

        private int CurrentIndex {
            get { return _currentIndex; }
            set {
                if (value < 0)
                    value = _outletList.Count - 1;
                else if (value >= _outletList.Count)
                    value = 0;

                _currentIndex = value;
                SetOutlet(_outletList[_currentIndex]);
            }
        }

//        public void SetOutlets(Dictionary<long, OutletModel> outletDict) {
//            _outletsToDisplay = outletDict;
//            SortOutlets();
//        }

        public void SetOutlets(List<OutletModel> outletList) {
            if (!AutoSize)
                AutoSize = true;
            _outletList = outletList;
            SortOutlets();
            // Show the first outlet
            btnPrev.Enabled = btnNext.Enabled = (_outletList.Count != 1);
            CurrentIndex = 0; //_outletList.Count - 1;
        }

        private void SortOutlets() {
            // Sort the list of outlets if you want to
            // Exclude this if you dont care about order
//            _outletList.Sort(
//                delegate(OutletModel o1, OutletModel o2) {
//                    // Do your sorting method for ordering the markers
//                    return 0;
//                }
//            );
        }

        private void SetOutlet(OutletModel outlet) {
            _outletToDisplay = outlet;
            pictureMarker.Image = _outletToDisplay.MarkerImage;
            labelTitle.Text = _outletToDisplay.ToolTipTitle;
            labelCounter.Text = string.Format("{0} из {1}", _currentIndex + 1, _outletList.Count);
            labelText.Text = _outletToDisplay.ToolTipText + _outletToDisplay.FullInfo;

            Width = labelText.Width + Padding.Left + Padding.Right;
            Height = labelText.Height + Padding.Top + Padding.Bottom;
            AlignControls();
        }

        private void AlignControls() {
            btnX.Left = Width - btnX.Width - Padding.Right;
            //btnNext.Left = btnX.Left - btnNext.Width - Padding.Right;
            //btnPrev.Left = btnNext.Left - btnPrev.Width - Padding.Right;
            //labelCounter.Left = btnPrev.Left - labelCounter.Width - Padding.Right;
        }

        private void btnX_Click(object sender, System.EventArgs e) {
            Parent.Hide();
        }

        public override void Closed() {
            if (null != OnClosePopup)
                OnClosePopup(this, new EventArgs());
            base.Closed();
        }

        private void btnPrev_Click(object sender, EventArgs e) {
            CurrentIndex--;
        }

        private void btnNext_Click(object sender, EventArgs e) {
            CurrentIndex++;
        }

        private void FullInfoPopupControl_Resize(object sender, EventArgs e) {
            AlignControls();
        }

    }
}