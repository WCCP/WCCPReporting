﻿using System.Windows.Forms;

namespace SoftServe.Reports.RouteManagement.View.AdvancedToolTip {
    public partial class PopupControl : UserControl {
        public PopupControl() {
            InitializeComponent();
        }

        protected override bool ProcessDialogKey(Keys keyData) {
            // Alt+F4 is to closing
            if ((keyData & Keys.Alt) == Keys.Alt)
                if ((keyData & Keys.F4) == Keys.F4) {
                    this.Parent.Hide();
                    return true;
                }

            if ((keyData & Keys.Enter) == Keys.Enter) {
                if (ActiveControl is Button) {
                    (ActiveControl as Button).PerformClick();
                    return true;
                }
            }

            return base.ProcessDialogKey(keyData);
        }

        public virtual void Closed() {
        }
    }
}