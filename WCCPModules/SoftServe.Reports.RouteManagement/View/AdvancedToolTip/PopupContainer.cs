﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SoftServe.Reports.RouteManagement.View.AdvancedToolTip {
    public partial class PopupContainer : ToolStripDropDown {
        private PopupControl _popupControl;
        private ToolStripControlHost _stripControlHost;
        private readonly bool _fade = true;
        private string _text;

        private const int Frames = 5;
        private const int TotalDuration = 100;
        private const int FrameDuration = TotalDuration / Frames;

        public PopupContainer(): this(null) {
        }

        public PopupContainer(PopupControl popupControl) {
            InitializeComponent();

            _popupControl = popupControl ?? new PopupControl();

            _fade = SystemInformation.IsMenuAnimationEnabled && SystemInformation.IsMenuFadeEnabled;

            _stripControlHost = new ToolStripControlHost(_popupControl);
            _stripControlHost.AutoSize = true;
            Padding = Margin = _stripControlHost.Padding = _stripControlHost.Margin = Padding.Empty;
            _popupControl.Location = Point.Empty;
            Items.Add(_stripControlHost);
            
            // this popup container will be disposed immediately after disposion of the contained control
            _popupControl.Disposed += delegate {
                                          _popupControl = null;
                                          Dispose(true);
                                      };
        }

        public void SetDisplayObject(object obj) {
            
        }

        public void Show(Control control) {
            if (control == null)
                throw new ArgumentNullException("control");

            Show(control, control.ClientRectangle);
        }

        public new void Show(Control control, Point location) {
            Show(control, new Rectangle(location, new Size(2, 2)));
        }

        public void Show(Form form, Point location) {
            Show(form, new Rectangle(location, new Size(0, 0)));
        }

        private void Show(Control control, Rectangle area) {
            if (control == null)
                throw new ArgumentNullException("control");

            Point lPosition = new Point(area.Left, area.Top + area.Height);
            Point lCorrectedPosition = control.PointToScreen(lPosition);
            Rectangle lWorkingArea = Screen.FromControl(control).WorkingArea;

            if (lCorrectedPosition.X + Size.Width > (lWorkingArea.Left + lWorkingArea.Width))
                lCorrectedPosition.X = (lWorkingArea.Left + lWorkingArea.Width) - Size.Width;

            if (lCorrectedPosition.Y + Size.Height > (lWorkingArea.Top + lWorkingArea.Height))
                lCorrectedPosition.Y -= Size.Height + area.Height;
            lCorrectedPosition = control.PointToClient(lCorrectedPosition);

            _popupControl.Text = _text;
            Show(control, lCorrectedPosition, ToolStripDropDownDirection.BelowRight);
        }

        protected override bool ProcessDialogKey(Keys keyData) {
            //prevent alt from closing it and allow alt+menumonic to work
            if ((keyData & Keys.Alt) == Keys.Alt)
                return false;

            return base.ProcessDialogKey(keyData);
        }

        protected override void SetVisibleCore(bool visible) {
            double lOpacity = Opacity;
            if (visible && _fade)
                Opacity = 0;
            base.SetVisibleCore(visible);

            if (!visible || !_fade)
                return;
            for (int lI = 1; lI <= Frames; lI++) {
                if (lI > 1)
                    System.Threading.Thread.Sleep(FrameDuration);
                Opacity = lOpacity * lI / Frames;
            }
            Opacity = lOpacity;
        }

        protected override void OnOpening(CancelEventArgs e) {
            if (_popupControl.IsDisposed || _popupControl.Disposing) {
                e.Cancel = true;
                return;
            }
            base.OnOpening(e);
        }

        protected override void OnOpened(EventArgs e) {
            _popupControl.Focus();
            base.OnOpened(e);
        }

        protected override void OnClosed(ToolStripDropDownClosedEventArgs e) {
            _popupControl.Closed();
            base.OnClosed(e);
        }
    }
}