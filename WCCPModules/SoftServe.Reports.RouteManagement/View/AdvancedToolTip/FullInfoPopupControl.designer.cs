﻿namespace SoftServe.Reports.RouteManagement.View.AdvancedToolTip
{
    partial class FullInfoPopupControl
    {

        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        private void InitializeComponent()
        {
            this.labelText = new DevExpress.XtraEditors.LabelControl();
            this.btnX = new DevExpress.XtraEditors.SimpleButton();
            this.labelTitle = new DevExpress.XtraEditors.LabelControl();
            this.pictureMarker = new DevExpress.XtraEditors.PictureEdit();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrev = new DevExpress.XtraEditors.SimpleButton();
            this.labelCounter = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureMarker.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelText
            // 
            this.labelText.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.False;
            this.labelText.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelText.Appearance.ForeColor = System.Drawing.SystemColors.InfoText;
            this.labelText.Appearance.Options.UseFont = true;
            this.labelText.Appearance.Options.UseForeColor = true;
            this.labelText.Appearance.Options.UseTextOptions = true;
            this.labelText.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelText.Location = new System.Drawing.Point(8, 52);
            this.labelText.Name = "labelText";
            this.labelText.ShowToolTips = false;
            this.labelText.Size = new System.Drawing.Size(73, 18);
            this.labelText.TabIndex = 1;
            this.labelText.Text = "Main Text";
            // 
            // btnX
            // 
            this.btnX.AllowFocus = false;
            this.btnX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnX.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.btnX.Appearance.BackColor2 = System.Drawing.SystemColors.Info;
            this.btnX.Appearance.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnX.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnX.Appearance.Options.UseBackColor = true;
            this.btnX.Appearance.Options.UseFont = true;
            this.btnX.Appearance.Options.UseForeColor = true;
            this.btnX.Appearance.Options.UseTextOptions = true;
            this.btnX.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnX.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnX.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnX.Location = new System.Drawing.Point(178, 4);
            this.btnX.Margin = new System.Windows.Forms.Padding(2);
            this.btnX.Name = "btnX";
            this.btnX.ShowToolTips = false;
            this.btnX.Size = new System.Drawing.Size(20, 20);
            this.btnX.TabIndex = 2;
            this.btnX.Text = "X";
            this.btnX.Click += new System.EventHandler(this.btnX_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTitle.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.labelTitle.Appearance.Options.UseFont = true;
            this.labelTitle.Appearance.Options.UseForeColor = true;
            this.labelTitle.Location = new System.Drawing.Point(40, 28);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(98, 18);
            this.labelTitle.TabIndex = 3;
            this.labelTitle.Text = "Title 100199";
            // 
            // pictureMarker
            // 
            this.pictureMarker.Location = new System.Drawing.Point(8, 8);
            this.pictureMarker.Name = "pictureMarker";
            this.pictureMarker.Properties.AllowFocused = false;
            this.pictureMarker.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.pictureMarker.Properties.Appearance.Options.UseBackColor = true;
            this.pictureMarker.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureMarker.Properties.ReadOnly = true;
            this.pictureMarker.Properties.ShowMenu = false;
            this.pictureMarker.ShowToolTips = false;
            this.pictureMarker.Size = new System.Drawing.Size(28, 34);
            this.pictureMarker.TabIndex = 4;
            // 
            // btnNext
            // 
            this.btnNext.AllowFocus = false;
            this.btnNext.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.btnNext.Appearance.BackColor2 = System.Drawing.SystemColors.Info;
            this.btnNext.Appearance.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNext.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnNext.Appearance.Options.UseBackColor = true;
            this.btnNext.Appearance.Options.UseFont = true;
            this.btnNext.Appearance.Options.UseForeColor = true;
            this.btnNext.Appearance.Options.UseTextOptions = true;
            this.btnNext.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnNext.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnNext.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnNext.Location = new System.Drawing.Point(64, 4);
            this.btnNext.Margin = new System.Windows.Forms.Padding(2);
            this.btnNext.Name = "btnNext";
            this.btnNext.ShowToolTips = false;
            this.btnNext.Size = new System.Drawing.Size(20, 20);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = ">";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.AllowFocus = false;
            this.btnPrev.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.btnPrev.Appearance.BackColor2 = System.Drawing.SystemColors.Info;
            this.btnPrev.Appearance.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrev.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnPrev.Appearance.Options.UseBackColor = true;
            this.btnPrev.Appearance.Options.UseFont = true;
            this.btnPrev.Appearance.Options.UseForeColor = true;
            this.btnPrev.Appearance.Options.UseTextOptions = true;
            this.btnPrev.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnPrev.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnPrev.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnPrev.Location = new System.Drawing.Point(40, 4);
            this.btnPrev.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.ShowToolTips = false;
            this.btnPrev.Size = new System.Drawing.Size(20, 20);
            this.btnPrev.TabIndex = 6;
            this.btnPrev.Text = "<";
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // labelCounter
            // 
            this.labelCounter.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.False;
            this.labelCounter.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCounter.Appearance.ForeColor = System.Drawing.SystemColors.InfoText;
            this.labelCounter.Appearance.Options.UseFont = true;
            this.labelCounter.Appearance.Options.UseForeColor = true;
            this.labelCounter.Appearance.Options.UseTextOptions = true;
            this.labelCounter.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelCounter.Location = new System.Drawing.Point(88, 8);
            this.labelCounter.Name = "labelCounter";
            this.labelCounter.ShowToolTips = false;
            this.labelCounter.Size = new System.Drawing.Size(48, 14);
            this.labelCounter.TabIndex = 7;
            this.labelCounter.Text = "12 из 20";
            // 
            // FullInfoPopupControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = false;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.Controls.Add(this.labelCounter);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.pictureMarker);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.btnX);
            this.Controls.Add(this.labelText);
            this.ForeColor = System.Drawing.SystemColors.InfoText;
            this.Name = "FullInfoPopupControl";
            this.Padding = new System.Windows.Forms.Padding(8, 8, 4, 8);
            this.Size = new System.Drawing.Size(203, 171);
            this.Resize += new System.EventHandler(this.FullInfoPopupControl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureMarker.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelText;
        private DevExpress.XtraEditors.SimpleButton btnX;
        private DevExpress.XtraEditors.LabelControl labelTitle;
        private DevExpress.XtraEditors.PictureEdit pictureMarker;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private DevExpress.XtraEditors.SimpleButton btnPrev;
        private DevExpress.XtraEditors.LabelControl labelCounter;

    }
}
