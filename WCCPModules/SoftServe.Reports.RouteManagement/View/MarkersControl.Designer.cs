﻿namespace SoftServe.Reports.RouteManagement.View {
    partial class MarkersControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarkersControl));
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.vGrid = new DevExpress.XtraVerticalGrid.VGridControl();
            this.outletModelBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.rowId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTraidingName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDeliveryAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLat = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLon = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRecVisits = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPlanVisits = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowVisitsDelta = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowOutletInfo = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowOutletToolTip = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMarkerToolTip = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowImageIndex = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMarkerImage = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowIsMarker = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMarker = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.toolTipCtrlHint = new DevExpress.Utils.ToolTipController(this.components);
            this.imageListBox = new DevExpress.XtraEditors.ImageListBoxControl();
            this.outletModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imgMarkerList = new DevExpress.Utils.ImageCollection(this.components);
            this.toolTipCtrlFullInfo = new DevExpress.Utils.ToolTipController(this.components);
            this.mapControl = new GMap.NET.WindowsForms.GMapControl();
            this.timer2click = new System.Windows.Forms.Timer(this.components);
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanel.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outletModelBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outletModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarkerList)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel
            // 
            this.dockPanel.Controls.Add(this.dockPanel1_Container);
            this.dockPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanel.ID = new System.Guid("47a3dca7-5da4-40c7-a605-ee433d8a9269");
            this.dockPanel.Location = new System.Drawing.Point(0, 306);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Options.AllowDockFill = false;
            this.dockPanel.Options.AllowDockLeft = false;
            this.dockPanel.Options.AllowDockRight = false;
            this.dockPanel.Options.AllowDockTop = false;
            this.dockPanel.Options.AllowFloating = false;
            this.dockPanel.Options.FloatOnDblClick = false;
            this.dockPanel.Options.ShowCloseButton = false;
            this.dockPanel.Options.ShowMaximizeButton = false;
            this.dockPanel.OriginalSize = new System.Drawing.Size(200, 100);
            this.dockPanel.Size = new System.Drawing.Size(648, 100);
            this.dockPanel.Text = "ТТ без координат";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.vGrid);
            this.dockPanel1_Container.Controls.Add(this.imageListBox);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 25);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(642, 72);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // vGrid
            // 
            this.vGrid.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.vGrid.Appearance.Empty.Options.UseBackColor = true;
            this.vGrid.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.vGrid.DataSource = this.outletModelBindingSource1;
            this.vGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGrid.Location = new System.Drawing.Point(0, 0);
            this.vGrid.Name = "vGrid";
            this.vGrid.OptionsBehavior.Editable = false;
            this.vGrid.OptionsBehavior.ResizeRowValues = false;
            this.vGrid.OptionsView.ShowHorzLines = false;
            this.vGrid.OptionsView.ShowRows = false;
            this.vGrid.OptionsView.ShowVertLines = false;
            this.vGrid.RecordWidth = 32;
            this.vGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemPictureEdit1});
            this.vGrid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowId,
            this.rowName,
            this.rowTraidingName,
            this.rowAddress,
            this.rowDeliveryAddress,
            this.rowLat,
            this.rowLon,
            this.rowRecVisits,
            this.rowPlanVisits,
            this.rowVisitsDelta,
            this.rowOutletInfo,
            this.rowOutletToolTip,
            this.rowMarkerToolTip,
            this.rowImageIndex,
            this.rowMarkerImage,
            this.rowIsMarker,
            this.rowMarker});
            this.vGrid.Size = new System.Drawing.Size(642, 72);
            this.vGrid.TabIndex = 1;
            this.vGrid.ToolTipController = this.toolTipCtrlHint;
            this.vGrid.DragOver += new System.Windows.Forms.DragEventHandler(this.vGrid_DragOver);
            this.vGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.vGrid_MouseDown);
            this.vGrid.MouseLeave += new System.EventHandler(this.vGrid_MouseLeave);
            this.vGrid.MouseMove += new System.Windows.Forms.MouseEventHandler(this.vGrid_MouseMove);
            // 
            // outletModelBindingSource1
            // 
            this.outletModelBindingSource1.DataSource = typeof(SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel);
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            this.repositoryItemImageEdit1.ReadOnly = true;
            this.repositoryItemImageEdit1.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ReadOnly = true;
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // rowId
            // 
            this.rowId.Name = "rowId";
            this.rowId.Properties.Caption = "Id";
            this.rowId.Properties.FieldName = "Id";
            this.rowId.Visible = false;
            // 
            // rowName
            // 
            this.rowName.Name = "rowName";
            this.rowName.OptionsRow.AllowMove = false;
            this.rowName.OptionsRow.AllowMoveToCustomizationForm = false;
            this.rowName.Properties.Caption = "Name";
            this.rowName.Properties.FieldName = "Name";
            this.rowName.Visible = false;
            // 
            // rowTraidingName
            // 
            this.rowTraidingName.Name = "rowTraidingName";
            this.rowTraidingName.Properties.Caption = "Traiding Name";
            this.rowTraidingName.Properties.FieldName = "TraidingName";
            this.rowTraidingName.Visible = false;
            // 
            // rowAddress
            // 
            this.rowAddress.Name = "rowAddress";
            this.rowAddress.Properties.Caption = "Address";
            this.rowAddress.Properties.FieldName = "Address";
            this.rowAddress.Visible = false;
            // 
            // rowDeliveryAddress
            // 
            this.rowDeliveryAddress.Name = "rowDeliveryAddress";
            this.rowDeliveryAddress.Properties.Caption = "Delivery Address";
            this.rowDeliveryAddress.Properties.FieldName = "DeliveryAddress";
            this.rowDeliveryAddress.Visible = false;
            // 
            // rowLat
            // 
            this.rowLat.Name = "rowLat";
            this.rowLat.Properties.Caption = "Lat";
            this.rowLat.Properties.FieldName = "Lat";
            this.rowLat.Visible = false;
            // 
            // rowLon
            // 
            this.rowLon.Name = "rowLon";
            this.rowLon.Properties.Caption = "Lon";
            this.rowLon.Properties.FieldName = "Lon";
            this.rowLon.Visible = false;
            // 
            // rowRecVisits
            // 
            this.rowRecVisits.Name = "rowRecVisits";
            this.rowRecVisits.Properties.Caption = "Rec Visits";
            this.rowRecVisits.Properties.FieldName = "RecVisits";
            this.rowRecVisits.Visible = false;
            // 
            // rowPlanVisits
            // 
            this.rowPlanVisits.Name = "rowPlanVisits";
            this.rowPlanVisits.Properties.Caption = "Plan Visits";
            this.rowPlanVisits.Properties.FieldName = "PlanVisits";
            this.rowPlanVisits.Visible = false;
            // 
            // rowVisitsDelta
            // 
            this.rowVisitsDelta.Name = "rowVisitsDelta";
            this.rowVisitsDelta.Properties.Caption = "Visits Delta";
            this.rowVisitsDelta.Properties.FieldName = "VisitsDelta";
            this.rowVisitsDelta.Properties.ReadOnly = true;
            this.rowVisitsDelta.Visible = false;
            // 
            // rowOutletInfo
            // 
            this.rowOutletInfo.Name = "rowOutletInfo";
            this.rowOutletInfo.Properties.Caption = "Outlet Info";
            this.rowOutletInfo.Properties.FieldName = "OutletInfo";
            this.rowOutletInfo.Properties.ReadOnly = true;
            this.rowOutletInfo.Visible = false;
            // 
            // rowOutletToolTip
            // 
            this.rowOutletToolTip.Name = "rowOutletToolTip";
            this.rowOutletToolTip.Properties.Caption = "Outlet Tool Tip";
            this.rowOutletToolTip.Properties.FieldName = "OutletToolTip";
            this.rowOutletToolTip.Properties.ReadOnly = true;
            this.rowOutletToolTip.Visible = false;
            // 
            // rowMarkerToolTip
            // 
            this.rowMarkerToolTip.Name = "rowMarkerToolTip";
            this.rowMarkerToolTip.Properties.Caption = "Marker Tool Tip";
            this.rowMarkerToolTip.Properties.FieldName = "MarkerToolTip";
            this.rowMarkerToolTip.Visible = false;
            // 
            // rowImageIndex
            // 
            this.rowImageIndex.Name = "rowImageIndex";
            this.rowImageIndex.Properties.Caption = "Image Index";
            this.rowImageIndex.Properties.FieldName = "ImageIndex";
            this.rowImageIndex.Visible = false;
            // 
            // rowMarkerImage
            // 
            this.rowMarkerImage.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.rowMarkerImage.Appearance.Options.UseBackColor = true;
            this.rowMarkerImage.Height = 36;
            this.rowMarkerImage.MaxCaptionLineCount = 4;
            this.rowMarkerImage.Name = "rowMarkerImage";
            this.rowMarkerImage.OptionsRow.AllowMove = false;
            this.rowMarkerImage.OptionsRow.AllowMoveToCustomizationForm = false;
            this.rowMarkerImage.OptionsRow.AllowSize = false;
            this.rowMarkerImage.OptionsRow.DblClickExpanding = false;
            this.rowMarkerImage.OptionsRow.ShowInCustomizationForm = false;
            this.rowMarkerImage.Properties.Caption = "Marker Image";
            this.rowMarkerImage.Properties.FieldName = "MarkerImage";
            this.rowMarkerImage.Properties.ReadOnly = true;
            this.rowMarkerImage.Properties.RowEdit = this.repositoryItemPictureEdit1;
            // 
            // rowIsMarker
            // 
            this.rowIsMarker.Name = "rowIsMarker";
            this.rowIsMarker.Properties.Caption = "Is Marker";
            this.rowIsMarker.Properties.FieldName = "IsMarker";
            this.rowIsMarker.Properties.ReadOnly = true;
            this.rowIsMarker.Visible = false;
            // 
            // rowMarker
            // 
            this.rowMarker.Name = "rowMarker";
            this.rowMarker.Properties.Caption = "Marker";
            this.rowMarker.Properties.FieldName = "Marker";
            this.rowMarker.Properties.ReadOnly = true;
            this.rowMarker.Visible = false;
            // 
            // toolTipCtrlHint
            // 
            this.toolTipCtrlHint.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolTipCtrlHint.Appearance.Options.UseFont = true;
            this.toolTipCtrlHint.Appearance.Options.UseTextOptions = true;
            this.toolTipCtrlHint.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.toolTipCtrlHint.Rounded = true;
            this.toolTipCtrlHint.ShowBeak = true;
            this.toolTipCtrlHint.ToolTipLocation = DevExpress.Utils.ToolTipLocation.TopRight;
            this.toolTipCtrlHint.ToolTipType = DevExpress.Utils.ToolTipType.Standard;
            // 
            // imageListBox
            // 
            this.imageListBox.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.imageListBox.Appearance.Options.UseBackColor = true;
            this.imageListBox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.imageListBox.ColumnWidth = 32;
            this.imageListBox.DataSource = this.outletModelBindingSource;
            this.imageListBox.Enabled = false;
            this.imageListBox.HotTrackSelectMode = DevExpress.XtraEditors.HotTrackSelectMode.SelectItemOnClick;
            this.imageListBox.ImageIndexMember = "ImageIndex";
            this.imageListBox.ImageList = this.imgMarkerList;
            this.imageListBox.ItemHeight = 36;
            this.imageListBox.Location = new System.Drawing.Point(356, 0);
            this.imageListBox.MultiColumn = true;
            this.imageListBox.Name = "imageListBox";
            this.imageListBox.Size = new System.Drawing.Size(284, 112);
            this.imageListBox.TabIndex = 0;
            this.imageListBox.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.imageListBox.ValueMember = "Id";
            this.imageListBox.Visible = false;
            // 
            // outletModelBindingSource
            // 
            this.outletModelBindingSource.DataSource = typeof(SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel);
            // 
            // imgMarkerList
            // 
            this.imgMarkerList.ImageSize = new System.Drawing.Size(28, 34);
            this.imgMarkerList.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgMarkerList.ImageStream")));
            // 
            // toolTipCtrlFullInfo
            // 
            this.toolTipCtrlFullInfo.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolTipCtrlFullInfo.Appearance.Options.UseFont = true;
            this.toolTipCtrlFullInfo.AppearanceTitle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolTipCtrlFullInfo.AppearanceTitle.Options.UseFont = true;
            this.toolTipCtrlFullInfo.AppearanceTitle.Options.UseTextOptions = true;
            this.toolTipCtrlFullInfo.AppearanceTitle.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.toolTipCtrlFullInfo.AutoPopDelay = 3600000;
            this.toolTipCtrlFullInfo.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            this.toolTipCtrlFullInfo.ReshowDelay = 200;
            this.toolTipCtrlFullInfo.Rounded = true;
            this.toolTipCtrlFullInfo.ShowBeak = true;
            this.toolTipCtrlFullInfo.ToolTipType = DevExpress.Utils.ToolTipType.Standard;
            // 
            // mapControl
            // 
            this.mapControl.AllowDrop = true;
            this.mapControl.AutoSize = true;
            this.mapControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mapControl.Bearing = 0F;
            this.mapControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapControl.CanDragMap = true;
            this.mapControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl.EmptyTileColor = System.Drawing.Color.Navy;
            this.mapControl.GrayScaleMode = false;
            this.mapControl.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.mapControl.LevelsKeepInMemmory = 5;
            this.mapControl.Location = new System.Drawing.Point(0, 0);
            this.mapControl.MarkersEnabled = true;
            this.mapControl.MaxZoom = 24;
            this.mapControl.MinZoom = 1;
            this.mapControl.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.ViewCenter;
            this.mapControl.Name = "mapControl";
            this.mapControl.NegativeMode = false;
            this.mapControl.PolygonsEnabled = true;
            this.mapControl.RetryLoadTile = 0;
            this.mapControl.RoutesEnabled = true;
            this.mapControl.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Fractional;
            this.mapControl.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.mapControl.ShowTileGridLines = false;
            this.mapControl.Size = new System.Drawing.Size(648, 306);
            this.mapControl.TabIndex = 1;
            this.mapControl.Zoom = 0D;
            this.mapControl.OnMarkerClick += new GMap.NET.WindowsForms.MarkerClick(this.mapControl_OnMarkerClick);
            this.mapControl.OnMarkerEnter += new GMap.NET.WindowsForms.MarkerEnter(this.mapControl_OnMarkerEnter);
            this.mapControl.OnMarkerLeave += new GMap.NET.WindowsForms.MarkerLeave(this.mapControl_OnMarkerLeave);
            this.mapControl.OnMapZoomChanged += new GMap.NET.MapZoomChanged(this.mapControl_OnMapZoomChanged);
            this.mapControl.DragDrop += new System.Windows.Forms.DragEventHandler(this.mapControl_DragDrop);
            this.mapControl.DragOver += new System.Windows.Forms.DragEventHandler(this.mapControl_DragOver);
            this.mapControl.DoubleClick += new System.EventHandler(this.mapControl_DoubleClick);
            this.mapControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mapControl_MouseDown);
            this.mapControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapControl_MouseMove);
            this.mapControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mapControl_MouseUp);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MarkersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mapControl);
            this.Controls.Add(this.dockPanel);
            this.Name = "MarkersControl";
            this.Size = new System.Drawing.Size(648, 406);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanel.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outletModelBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outletModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarkerList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private GMap.NET.WindowsForms.GMapControl mapControl;
        private DevExpress.XtraEditors.ImageListBoxControl imageListBox;
        private DevExpress.Utils.ImageCollection imgMarkerList;
        private System.Windows.Forms.BindingSource outletModelBindingSource;
        private DevExpress.XtraVerticalGrid.VGridControl vGrid;
        private System.Windows.Forms.BindingSource outletModelBindingSource1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowId;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTraidingName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDeliveryAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLon;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRecVisits;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPlanVisits;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowVisitsDelta;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowOutletInfo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowOutletToolTip;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMarkerToolTip;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowImageIndex;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMarkerImage;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowIsMarker;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMarker;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.Utils.ToolTipController toolTipCtrlFullInfo;
        private System.Windows.Forms.Timer timer2click;
        private DevExpress.Utils.ToolTipController toolTipCtrlHint;
        private System.Windows.Forms.Timer timer;
    }
}
