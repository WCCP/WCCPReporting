﻿using System;
using DevExpress.Utils;
using SoftServe.Core.Common.View;

namespace SoftServe.Reports.RouteManagement.View {
    public delegate void OnClosePopupHandler(object obj, EventArgs e);

    public interface IRMTView : ISimpleControlView {
        ImageCollection Images { get; }
        void ProcessEvents();
        void UpdateButtonsStatus();
        void RefreshRouteProperties();
        void GetSourceRoute();
        void RefreshZoom();
    }
}