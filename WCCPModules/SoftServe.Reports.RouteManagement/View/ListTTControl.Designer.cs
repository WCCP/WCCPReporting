﻿using SoftServe.Reports.RouteManagement.Model.Outlet;

namespace SoftServe.Reports.RouteManagement.View {
    partial class ListTTControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblTimeHm = new DevExpress.XtraEditors.LabelControl();
            this.lblHm = new DevExpress.XtraEditors.LabelControl();
            this.lblKm = new DevExpress.XtraEditors.LabelControl();
            this.lblTTCount = new DevExpress.XtraEditors.LabelControl();
            this.lblLengthKm = new DevExpress.XtraEditors.LabelControl();
            this.lblTtOnRoute = new DevExpress.XtraEditors.LabelControl();
            this.listTT = new DevExpress.XtraEditors.ListBoxControl();
            this.outletInRouteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outletInRouteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblTimeHm);
            this.panelControl1.Controls.Add(this.lblHm);
            this.panelControl1.Controls.Add(this.lblKm);
            this.panelControl1.Controls.Add(this.lblTTCount);
            this.panelControl1.Controls.Add(this.lblLengthKm);
            this.panelControl1.Controls.Add(this.lblTtOnRoute);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(321, 61);
            this.panelControl1.TabIndex = 2;
            // 
            // lblTimeHm
            // 
            this.lblTimeHm.Appearance.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.lblTimeHm.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblTimeHm.Appearance.Options.UseFont = true;
            this.lblTimeHm.Appearance.Options.UseForeColor = true;
            this.lblTimeHm.AutoEllipsis = true;
            this.lblTimeHm.Location = new System.Drawing.Point(208, 32);
            this.lblTimeHm.Name = "lblTimeHm";
            this.lblTimeHm.Size = new System.Drawing.Size(89, 18);
            this.lblTimeHm.TabIndex = 8;
            this.lblTimeHm.Text = "Время, чч:мм";
            // 
            // lblHm
            // 
            this.lblHm.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblHm.Appearance.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblHm.Appearance.Options.UseFont = true;
            this.lblHm.Appearance.Options.UseForeColor = true;
            this.lblHm.Appearance.Options.UseTextOptions = true;
            this.lblHm.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblHm.AutoEllipsis = true;
            this.lblHm.Location = new System.Drawing.Point(224, 4);
            this.lblHm.Name = "lblHm";
            this.lblHm.Size = new System.Drawing.Size(55, 23);
            this.lblHm.TabIndex = 7;
            this.lblHm.Text = "10:26";
            // 
            // lblKm
            // 
            this.lblKm.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblKm.Appearance.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblKm.Appearance.Options.UseFont = true;
            this.lblKm.Appearance.Options.UseForeColor = true;
            this.lblKm.Appearance.Options.UseTextOptions = true;
            this.lblKm.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblKm.AutoEllipsis = true;
            this.lblKm.Location = new System.Drawing.Point(130, 3);
            this.lblKm.Name = "lblKm";
            this.lblKm.Size = new System.Drawing.Size(54, 23);
            this.lblKm.TabIndex = 6;
            this.lblKm.Text = "256.8";
            // 
            // lblTTCount
            // 
            this.lblTTCount.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblTTCount.Appearance.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblTTCount.Appearance.Options.UseFont = true;
            this.lblTTCount.Appearance.Options.UseForeColor = true;
            this.lblTTCount.Appearance.Options.UseTextOptions = true;
            this.lblTTCount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTTCount.AutoEllipsis = true;
            this.lblTTCount.Location = new System.Drawing.Point(41, 3);
            this.lblTTCount.Name = "lblTTCount";
            this.lblTTCount.Size = new System.Drawing.Size(24, 23);
            this.lblTTCount.TabIndex = 5;
            this.lblTTCount.Text = "20";
            // 
            // lblLengthKm
            // 
            this.lblLengthKm.Appearance.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.lblLengthKm.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblLengthKm.Appearance.Options.UseFont = true;
            this.lblLengthKm.Appearance.Options.UseForeColor = true;
            this.lblLengthKm.AutoEllipsis = true;
            this.lblLengthKm.Location = new System.Drawing.Point(120, 32);
            this.lblLengthKm.Name = "lblLengthKm";
            this.lblLengthKm.Size = new System.Drawing.Size(66, 18);
            this.lblLengthKm.TabIndex = 4;
            this.lblLengthKm.Text = "Длина, км";
            // 
            // lblTtOnRoute
            // 
            this.lblTtOnRoute.Appearance.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.lblTtOnRoute.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblTtOnRoute.Appearance.Options.UseFont = true;
            this.lblTtOnRoute.Appearance.Options.UseForeColor = true;
            this.lblTtOnRoute.AutoEllipsis = true;
            this.lblTtOnRoute.Location = new System.Drawing.Point(3, 32);
            this.lblTtOnRoute.Name = "lblTtOnRoute";
            this.lblTtOnRoute.Size = new System.Drawing.Size(101, 18);
            this.lblTtOnRoute.TabIndex = 3;
            this.lblTtOnRoute.Text = "ТТ на маршруте";
            // 
            // listTT
            // 
            this.listTT.AllowDrop = true;
            this.listTT.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.listTT.Appearance.BackColor2 = System.Drawing.SystemColors.Window;
            this.listTT.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listTT.Appearance.Options.UseBackColor = true;
            this.listTT.Appearance.Options.UseFont = true;
            this.listTT.Appearance.Options.UseTextOptions = true;
            this.listTT.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.listTT.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.listTT.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.listTT.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.listTT.DataSource = this.outletInRouteBindingSource;
            this.listTT.DisplayMember = "OutletInfo";
            this.listTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTT.ItemHeight = 24;
            this.listTT.Location = new System.Drawing.Point(0, 61);
            this.listTT.Name = "listTT";
            this.listTT.Size = new System.Drawing.Size(321, 399);
            this.listTT.TabIndex = 3;
            this.listTT.ValueMember = "Id";
            this.listTT.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.listTT_DrawItem);
            // 
            // outletInRouteBindingSource
            // 
            this.outletInRouteBindingSource.AllowNew = true;
            this.outletInRouteBindingSource.DataSource = typeof(SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute);
            // 
            // ListTTControl
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.listTT);
            this.Controls.Add(this.panelControl1);
            this.MinimumSize = new System.Drawing.Size(300, 0);
            this.Name = "ListTTControl";
            this.Size = new System.Drawing.Size(321, 460);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outletInRouteBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblKm;
        private DevExpress.XtraEditors.LabelControl lblTTCount;
        private DevExpress.XtraEditors.LabelControl lblLengthKm;
        private DevExpress.XtraEditors.LabelControl lblTtOnRoute;
        private DevExpress.XtraEditors.ListBoxControl listTT;
        private DevExpress.XtraEditors.LabelControl lblTimeHm;
        private DevExpress.XtraEditors.LabelControl lblHm;
        private System.Windows.Forms.BindingSource outletInRouteBindingSource;
    }
}
