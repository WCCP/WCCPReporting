﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraVerticalGrid;
using GMap.NET;
using GMap.NET.WindowsForms;
using SoftServe.Core.Common.Logging;
using SoftServe.Reports.RouteManagement.Controller;
using SoftServe.Reports.RouteManagement.Model.Merch;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using SoftServe.Reports.RouteManagement.Model.Route;
using GMap.NET.MapProviders;
using SoftServe.Reports.RouteManagement.Model.Supervisor;
using SoftServe.Reports.RouteManagement.View.AdvancedToolTip;

namespace SoftServe.Reports.RouteManagement.View {
    public partial class MarkersControl : XtraUserControl {
        #region Fields

        private IRouteManagementController _controller;
        private IRMTView _rmtView;
        //private SimpleLogger _logger = new SimpleLogger("zRMT");

        private OutletModel _outlet;
        private ShowTTEnum _showTT;
        private List<OutletModel> _ttWithoutCoord = new List<OutletModel>();
        private FullInfoPopupControl _fullInfoControl;
        private PopupContainer _popupFullInfo;

        // Map fields
        private GMapMarker _currentMarker;
        private GMapMarker _tipMarker;
        private GMapMarker _clickMarker;
        private readonly GMapOverlay _objects = new GMapOverlay("objects");
        private readonly GMapOverlay _routes = new GMapOverlay("routes");
//        private readonly Dictionary<long, GMapMarker> _ttMarkers = new Dictionary<long, GMapMarker>();
        private List<PointLatLng> _routePoints = new List<PointLatLng>();
        private List<OutletModel> _mouseOveredOutlets = new List<OutletModel>();
        private List<OutletModel> _vGridOutlets = new List<OutletModel>();

        private bool _isRouteCentered;
        private bool _isDraggingMarker;
        private VGridHitInfo _captureHitInfo;
        private int _visitFrequency;
        private bool _isFullInfoShowed = false;
        private Point _location = Point.Empty;
        private MouseEventArgs _mouseEventArgs;
        private bool _isMarkerClicked;
        private bool _isPopupInfoShowed;
        private bool _debug;
        private int _zoomPercent;

        private const double ZoomPrcDelta = 0.05;
        private const int ZoomDelta = 1;

        #endregion


        public MarkersControl() {
            _debug = FiltersHelper.IsDebug;
            InitializeComponent();
            InitMap();
            _showTT = ShowTTEnum.M2;
            _visitFrequency = FiltersHelper.AnyFrequency;

            imageListBox.DataSource = _ttWithoutCoord;
            vGrid.DataSource = _ttWithoutCoord;

            toolTipCtrlHint.AllowHtmlText = FiltersHelper.AllowHtmlText;

            _fullInfoControl = new FullInfoPopupControl();
            _fullInfoControl.OnClosePopup += ClosePopup;
            _fullInfoControl.AllowHtmlText = FiltersHelper.AllowHtmlText;
            _popupFullInfo = new PopupContainer(_fullInfoControl);
        }

        private void InitMap() {
            GMapProvider.WebProxy = WebRequest.DefaultWebProxy;
            Random random = new Random();
            OpenStreetMapProvider.UserAgent = string.Format("Mozilla/5.0 (Windows NT {1}.0; {2}rv:{0}.0) Gecko/20100101 Firefox/{0}.0",
            random.Next(DateTime.Today.Year - 1969 - 5, DateTime.Today.Year - 1969),
            random.Next(0, 10) % 2 == 0 ? 10 : 6,
                random.Next(0, 10) % 2 == 1 ? string.Empty : "WOW64; ");

            mapControl.MapProvider = GMapProviders.OpenStreetMap;
            mapControl.Manager.Mode = AccessMode.ServerAndCache;
            mapControl.DragButton = MouseButtons.Left;
            mapControl.MinZoom = 1;
            mapControl.MaxZoom = 24;
            mapControl.Zoom = 12;

            // stops immediate marker/route/polygon invalidations;
            // call Refresh to perform single refresh and reset invalidation state
            //mapControl.HoldInvalidation = true;
            mapControl.Overlays.Add(_routes);
            mapControl.Overlays.Add(_objects);
        }

        #region Properties

        public List<PointLatLng> RoutePoints {
            get { return _routePoints; }
        }

        public ShowTTEnum ShowTT {
            get { return _showTT; }
            set { _showTT = value; }
        }

        public int VisitFrequency {
            get { return _visitFrequency; }
            set { _visitFrequency = value; }
        }

        public bool EnableCentering { get; set; }

        public ImageListBoxControl ImageListBox {
            get { return imageListBox; }
        }

        public VGridControl VGrid {
            get { return vGrid; }
        }

        public string ZoomPercentStr {
            get { return _zoomPercent.ToString(CultureInfo.InvariantCulture); }
        }

        #endregion

        public void SetRMTView(IRMTView view) {
            _rmtView = view;
        }

        public void SetController(IRouteManagementController controller) {
            _controller = controller;
        }

        #region Map manipulating

//        private void RemoveTtFromMap(OutletInRoute outletInRoute) {
//            if (!outletInRoute.Outlet.IsMarker)
//                return;
//            int lIndex = _objects.Markers.IndexOf(outletInRoute.Outlet.Marker);
//            if (lIndex < 0)
//                return;
//            _objects.Markers.RemoveAt(lIndex);
//            mapControl.Refresh();
//        }

        public void UpdateMapMarkers() {
            Cursor.Current = Cursors.WaitCursor;
            mapControl.HoldInvalidation = true;
//            imageListBox.BeginUpdate();
            try {
                _objects.Markers.Clear();
                _ttWithoutCoord.Clear();
                imgMarkerList.Images.Clear();

                if (!_controller.CustomerHasNoRoutes)
                {
                    switch (_showTT)
                    {
                        case ShowTTEnum.Route:
                            UpdateMarkersForRoute(_controller.CurrentRoute);
                            break;
                        case ShowTTEnum.M1:
                            UpdateMarkersForM1(_controller.CurrentM1);
                            break;
                        case ShowTTEnum.M2:
                            for (int lI = 0; lI < _controller.CurrentM2.M1List.Count; lI++)
                                UpdateMarkersForM1(_controller.CurrentM2.M1List[lI]);
                            break;
                        case ShowTTEnum.Cust:
                        List<SupervisorModel> lSvList = _controller.CustIdSupervisors[_controller.CurrentSupervisor.CustId];
                            for (int lI = 0; lI < lSvList.Count; lI++)
                            {
                                if (lSvList[lI].Id == _controller.CurrentM2.Id)
                                    continue;
                                M2Model lM2 = _controller.M2Cache[lSvList[lI].Id];
                                if (lM2 == null)
                                    continue;
                                for (int lJ = 0; lJ < lM2.M1List.Count; lJ++)
                                {
                                    UpdateMarkersForM1(lM2.M1List[lJ]);
                                }
                            }
                            for (int lI = 0; lI < _controller.CurrentM2.M1List.Count; lI++)
                            {
                                UpdateMarkersForM1(_controller.CurrentM2.M1List[lI]);
                            }
                            break;
                    }
                }

                UpdateMapRoute();

                if (!EnableCentering)
                    return;

                if (_isRouteCentered)
                    return;
                if (_objects.Markers.Count > 0) {
                    mapControl.Position = _objects.Markers[0].Position;
                }
                mapControl.ZoomAndCenterMarkers("objects");
            }
            finally {
                Cursor.Current = Cursors.Default;
                mapControl.Refresh();
                vGrid.Refresh();
//                imageListBox.EndUpdate();
//                imageListBox.Refresh();
            }
        }

        /// <summary>
        /// Updates marker image and tooltip.
        /// </summary>
        /// <param name="outlet"> object of OutletModel type. Persists info for marker.</param>
        /// <param name="number"> <c>int</c> if >0 - represents Outlet ordinal position in route. if == -1 - outlet isn't on route. </param>
        public void UpdateMarkerForOutlet(OutletModel outlet, int number) {
            outlet.ToolTipTitle = (number == -1 ? string.Empty : string.Format("№ {0} - ", number.ToString(CultureInfo.InvariantCulture)))
                + outlet.Id.ToString(CultureInfo.InvariantCulture);
            outlet.MarkerToolTip = outlet.OutletToolTip;

            if (outlet.IsMarker) {
                GMapMarker lMarker = outlet.Marker;
                int lMarkerIndex = _objects.Markers.IndexOf(lMarker);
                if (lMarkerIndex < 0)
                    _objects.Markers.Add(lMarker);
                //lMarker.ToolTipText = outlet.MarkerToolTip;
                if (_showTT == ShowTTEnum.Route && number == -1)
                    _objects.Markers.Remove(lMarker);
                //Filter "С частотой посещения"
                if (_visitFrequency != FiltersHelper.AnyFrequency) {
                    if (_visitFrequency != outlet.VisitsDelta)
                        if (_objects.Markers.Contains(lMarker))
                            _objects.Markers.Remove(lMarker);
                }
            }
            else {
                int lIndex = _ttWithoutCoord.IndexOf(outlet);
                if (lIndex < 0) {
//                    imgMarkerList.AddImage(outletInRoute.Outlet.MarkerImage, outletInRoute.Id.ToString());
//                    outletInRoute.Outlet.ImageIndex = imgMarkerList.Images.Count - 1;
                    _ttWithoutCoord.Add(outlet);
                }
//                else {
//                    imgMarkerList.Images[lIndex] = outletInRoute.Outlet.MarkerImage;
//                    outletInRoute.Outlet.ImageIndex = -1;
//                    outletInRoute.Outlet.ImageIndex = lIndex;
//                }
                if (_showTT == ShowTTEnum.Route && number == -1) {
                    _ttWithoutCoord.Remove(outlet);
                }
                //Filter "С частотой посещения"
                if (_visitFrequency != FiltersHelper.AnyFrequency) {
                    if (_visitFrequency != outlet.VisitsDelta)
                        if (_ttWithoutCoord.Contains(outlet))
                            _ttWithoutCoord.Remove(outlet);
                }
            }
        }

        public void UpdateMarkersForRoute(long routeId) {
            RouteWithOutletsModel lRoute = _controller.GetRouteById(routeId);
            if (null == lRoute)
                return;
            UpdateMarkersForRoute(lRoute);
        }

        public void UpdateMarkersForRoute(RouteWithOutletsModel route) {
            List<OutletInRoute> lOutletList = route.Outlets;
            bool lIsCurrentRoute = route == _controller.CurrentRoute;
            lock (lOutletList) {
                if (lIsCurrentRoute)
                    if (route.IsStartMarker) {
                        GMapMarker lMarker = route.StartMarker;
                        int lMarkerIndex = _objects.Markers.IndexOf(lMarker);
                        if (lMarkerIndex < 0)
                            _objects.Markers.Add(lMarker);
                    }
                for (int lIndex = 0; lIndex < lOutletList.Count; lIndex++)
                    UpdateMarkerForOutlet(lOutletList[lIndex].Outlet, lIsCurrentRoute ? lOutletList[lIndex].OlNumber : -1);
            }
        }

        public void ClearOutletsCache()
        {
            _mouseOveredOutlets.Clear();
        }

        private void UpdateMarkersForM1(M1Model m1) {
            for (int lI = 0; lI < m1.Routes.Count; lI++)
                if (m1.Routes[lI] != _controller.CurrentRoute)
                    UpdateMarkersForRoute(m1.Routes[lI]);
            UpdateMarkersForRoute(_controller.CurrentRoute);
        }

        /// <summary>
        /// Use own Router service based on OSMsharp library.
        /// </summary>
        public void UpdateMapRoute() {
            _routes.Routes.Clear();
            vGrid.Refresh();
            _isRouteCentered = false;
            if (_routePoints.Count <= 0)
                return;
            try {
                // add route
                GMapRoute lGMapRoute = new GMapRoute(_routePoints, _controller.CurrentRoute.GetRouteName());
                lGMapRoute.Stroke.Width = 4;
                lGMapRoute.Stroke.Color = _controller.CurrentRoute.IsOptimized ? Color.Purple : Color.Red;
                lGMapRoute.Stroke.DashStyle = DashStyle.Solid;
                lGMapRoute.Stroke.StartCap = LineCap.DiamondAnchor;
                //r.Stroke.CustomStartCap = new CustomLineCap(GraphicsPath.);
                lGMapRoute.Stroke.EndCap = LineCap.ArrowAnchor;
                lGMapRoute.IsHitTestVisible = true;
                _routes.Routes.Add(lGMapRoute);
                if (!EnableCentering)
                    return;
                //mapControl.Position = _routePoints[0];
                mapControl.ZoomAndCenterRoutes("routes");
                _isRouteCentered = true;
            }
            finally {
                mapControl.Refresh();
            }
        }

        /// <summary>
        /// Use standard RoutingProvider
        /// </summary>
//        private void UpdateMapRoute1() {
//            _routes.Routes.Clear();
//            try {
//                RoutingProvider lRoutingProvider = mapControl.MapProvider as RoutingProvider ?? GMapProviders.OpenStreetMap;
//
//                List<OutletInRoute> lOutletList = _controller.GetRouteOutlets();
//                for (int lIndex = 0; lIndex < lOutletList.Count; lIndex++) {
//                    if (lIndex == lOutletList.Count - 1)
//                        break;
//                    OutletModel lOutlet = lOutletList[lIndex].Outlet;
//                    OutletModel lOutletNext = lOutletList[lIndex + 1].Outlet;
//                    PointLatLng lStart = lOutlet.Marker.Position;
//                    PointLatLng lEnd = lOutletNext.Marker.Position;
//
//                    MapRoute lRoute = lRoutingProvider.GetRoute(lStart, lEnd, false, true, 15); // 15 is good
//                    if (lRoute != null) {
//                        // add route
//                        GMapRoute r = new GMapRoute(lRoute.Points, lRoute.Name);
//                        r.Stroke.Width = 4;
//                        r.Stroke.Color = Color.Purple;
//                        r.Stroke.DashStyle = DashStyle.Solid;
//                        r.Stroke.StartCap = LineCap.DiamondAnchor;
//                        //r.Stroke.CustomStartCap = new CustomLineCap(GraphicsPath.);
//                        r.Stroke.EndCap = LineCap.ArrowAnchor;
//                        r.IsHitTestVisible = true;
//                        _routes.Routes.Add(r);
//                    }
//                }
//            }
//            finally {
//                mapControl.Position = _objects.Markers[0].Position;
//                mapControl.ZoomAndCenterRoutes("routes");
//                mapControl.Refresh();
//            }
//        }

        public void CenterMapByTt(OutletModel outlet) {
            if (outlet.IsMarker) {
                mapControl.Position = outlet.Marker.Position;
                mapControl.Refresh();
            }
        }

        #endregion

        #region Map events

        private void mapControl_DragDrop(object sender, DragEventArgs e) {
            if (_controller.CurrentRoute.RouteId == 0)
                return;
            if (!e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute"))
                return;
            OutletInRoute lOutletInRoute = e.Data.GetData("SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute") as OutletInRoute;
            if (null == lOutletInRoute)
                return;
            _controller.RemoveOutletFromRoute(lOutletInRoute);
            _rmtView.UpdateButtonsStatus();
            EnableCentering = false;
            UpdateMarkerForOutlet(lOutletInRoute.Outlet, -1);
            _rmtView.GetSourceRoute();
            _rmtView.RefreshRouteProperties();
            UpdateMarkersForRoute(_controller.CurrentRoute);
            UpdateMapRoute();
        }

        private void mapControl_DragOver(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Copy;
            if (e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute"))
                e.Effect = DragDropEffects.Move;
        }

        private void mapControl_MouseDown(object sender, MouseEventArgs e) {
//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader(string.Format("mapControl_MouseDown - Clicks: {0}", e.Clicks));

            //if (!_popupFullInfo.Visible)
            //    _isPopupInfoShowed = false;

            if (e.Button == MouseButtons.Left)
                if (_currentMarker != null && _currentMarker.IsMouseOver) {
                    _isDraggingMarker = true;
                    long lKey = Convert.ToInt64(_currentMarker.Tag);
                    if (_controller.OLCache.ContainsKey(lKey))
                        _outlet = _controller.OLCache[lKey];
                    else {
                        _outlet = null;
                        _isDraggingMarker = false;
                    }
                }
        }

        private void mapControl_MouseMove(object sender, MouseEventArgs e) {
//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader("mapControl_MouseMove");

            if (e.Button == MouseButtons.Left && _isDraggingMarker && _currentMarker != null) {
                if (null == _outlet) {
                    long lKey = Convert.ToInt64(_currentMarker.Tag);
                    if (_controller.OLCache.ContainsKey(lKey))
                        _outlet = _controller.OLCache[lKey];
                }
                if (null != _outlet) {
                    mapControl.DoDragDrop(_outlet, DragDropEffects.Copy);
                    _isDraggingMarker = false;
                    _currentMarker = null;
                }
            }
//            else if (e.Button == MouseButtons.None && _currentMarker != null) {
//                if (_isFullInfoShowed)
//                    return;
//                if (_mouseOveredOutlets.Count == 0)
//                    return;
//
//                OutletModel lOutlet = _mouseOveredOutlets[_mouseOveredOutlets.Count - 1];
//                toolTipCtrlHint.HideHint();
//                toolTipCtrlHint.ShowHint(lOutlet.MarkerToolTip, MousePosition);
//            }
        }

        private void mapControl_MouseUp(object sender, MouseEventArgs e) {
//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader("mapControl_MouseUp");
            
            _isDraggingMarker = false;
            _currentMarker = null;
//            if (_mouseOveredOutlets.Count == 0)
//                _isPopupInfoShowed = false;
        }

        private void mapControl_OnMarkerEnter(GMapMarker item) {
            _tipMarker = item;
            if (!_isDraggingMarker)
                _currentMarker = item;

            long lOutletId = Convert.ToInt64(item.Tag);
            if (_controller.OLCache.ContainsKey(lOutletId))
                if (!_mouseOveredOutlets.Contains(_controller.OLCache[lOutletId]))
                    _mouseOveredOutlets.Add(_controller.OLCache[lOutletId]);

//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader(string.Format("mapControl_OnMarkerEnter - {0} - OL add: {1}", _mouseOveredOutlets.Count, lOutletId));
        }

        private void mapControl_OnMarkerLeave(GMapMarker item) {

            long lOutletId = Convert.ToInt64(item.Tag);
            if (_controller.OLCache.ContainsKey(lOutletId))
                _mouseOveredOutlets.Remove(_controller.OLCache[lOutletId]);

            if (_mouseOveredOutlets.Count == 0)
                _isPopupInfoShowed = false;

//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader(string.Format("mapControl_OnMarkerLeave - {0} - OL remove: {1}", _mouseOveredOutlets.Count, lOutletId));
        }

        private void mapControl_OnMarkerClick(GMapMarker item, MouseEventArgs e) {
//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader(string.Format("mapControl_OnMarkerClick - {0} - Clicks: {1}", _mouseOveredOutlets.Count, e.Clicks));

            if (e.Button == MouseButtons.Left) {
                if (_popupFullInfo.Visible || _isPopupInfoShowed) {
                    _popupFullInfo.Close();
                    _isPopupInfoShowed = false;
                    return;
                }
                
                if (_mouseOveredOutlets.Count == 0)
                    return;

                _fullInfoControl.SetOutlets(_mouseOveredOutlets);
                _isFullInfoShowed = true;
                _isMarkerClicked = true;
                _isPopupInfoShowed = true;
                
                //GPoint lPosition = mapControl.FromLatLngToLocal(item.Position);
                //lPosition.Offset(0, (_fullInfoControl.Height) + 34);
                //_popupFullInfo.Show(mapControl, new Point((int) lPosition.X, (int) lPosition.Y));
                _popupFullInfo.Show(mapControl, e.Location);
            }

            if (e.Button == MouseButtons.Right) {
                long lOutletId = Convert.ToInt64(item.Tag);
                //e.Location
                //TODO: show menu in correct position
                //pmAddRemoveTt.ShowPopup(e.Location);
            }
        }

        private void mapControl_DoubleClick(object sender, EventArgs e) {
//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader(string.Format("mapControl_DoubleClick - {0}", _mouseOveredOutlets.Count));

            if (_isPopupInfoShowed)
                return;
            if (_mouseOveredOutlets.Count == 0)
                return;
            Point lPosition = mapControl.PointToClient(MousePosition);
            foreach (OutletModel lOutlet in _mouseOveredOutlets)
                FillFullInfo(lOutlet);

            //show tooltip
            //_tipMarker = null;
            _fullInfoControl.SetOutlets(_mouseOveredOutlets);
            _isFullInfoShowed = true;
            _isPopupInfoShowed = true;
            GPoint lGPosition = mapControl.FromLatLngToLocal(_tipMarker.Position);
            //_popupFullInfo.Show(mapControl, new Point((int) lGPosition.X, (int) lGPosition.Y));
            _popupFullInfo.Show(mapControl, lPosition);
        }

        #endregion

        private void vGrid_MouseDown(object sender, MouseEventArgs e) {
            VGridControl lVGrid = sender as VGridControl;
            if (lVGrid == null)
                return;
            _captureHitInfo = lVGrid.CalcHitInfo(new Point(e.X, e.Y));
            if (e.Clicks == 2 && e.Button == MouseButtons.Left) {
//                toolTipCtrlFullInfo.HideHint();
                if (_captureHitInfo.HitInfoType == HitInfoTypeEnum.ValueCell) {
                    OutletModel lOutlet = lVGrid.GetRecordObject(_captureHitInfo.RecordIndex) as OutletModel;
                    if (lOutlet == null)
                        return;
                    FillFullInfo(lOutlet);

                    _vGridOutlets.Clear();
                    _vGridOutlets.Add(lOutlet);
                    _fullInfoControl.SetOutlets(_vGridOutlets);
                    _isFullInfoShowed = true;
                    _popupFullInfo.Show(vGrid, _captureHitInfo.PtMouse);
//                    toolTipCtrlFullInfo.HideHint();
//                    toolTipCtrlFullInfo.ShowHint(lOutlet.MarkerToolTip + lOutlet.FullInfo, MousePosition);
                }
            }
        }

        private void vGrid_MouseMove(object sender, MouseEventArgs e) {
            VGridControl lVGrid = sender as VGridControl;
            if (lVGrid == null)
                return;
            
            if (e.Button == MouseButtons.Left && _captureHitInfo != null) {
                if (_captureHitInfo.HitInfoType == HitInfoTypeEnum.ValueCell) {
                    OutletModel lOutlet = (OutletModel) lVGrid.GetRecordObject(_captureHitInfo.RecordIndex);
                    _captureHitInfo = null;
                    lVGrid.DoDragDrop(lOutlet, DragDropEffects.Copy);
                }
            }

            if (e.Button == MouseButtons.None && e.Clicks == 0) {
                if (_isFullInfoShowed)
                    return;
                VGridHitInfo lHitInfo = lVGrid.CalcHitInfo(e.Location);
                if (lHitInfo != null)
                    if (lHitInfo.HitInfoType == HitInfoTypeEnum.ValueCell) {
                        OutletModel lOutlet = lVGrid.GetRecordObject(lHitInfo.RecordIndex) as OutletModel;
                        if (lOutlet == null)
                            return;
                        toolTipCtrlHint.ShowHint(lOutlet.MarkerToolTip, MousePosition);
                    }
                    else
                        toolTipCtrlHint.HideHint();
            }
//            else
//                toolTipCtrlHint.HideHint();
        }

        private void vGrid_MouseLeave(object sender, EventArgs e) {
            toolTipCtrlHint.HideHint();
        }

        private void vGrid_DragOver(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Copy;
        }

        private void hideContainerBottom_Click(object sender, EventArgs e) {
            //
        }

        private void FillFullInfo(OutletModel outlet) {
            if (outlet.FullInfoOutlet == null) {
                Cursor.Current = Cursors.WaitCursor;
                try {
                    _controller.GetFullInfo(outlet);
                }
                finally {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void ClosePopup(object obj, EventArgs e) {
//            if (_debug)
//                lock (_logger)
//                    _logger.LogMsgHeader(string.Format("ClosePopup - {0}", _mouseOveredOutlets.Count));

            _isFullInfoShowed = false;
            //Thread.Sleep(100);
            //_isPopupInfoShowed = false;
        }

        private void timer_Tick(object sender, EventArgs e) {
            timer.Stop();
            if (_clickMarker != null) {
                mapControl_OnMarkerEnter(_clickMarker);
                _isMarkerClicked = false;
                mapControl_OnMarkerClick(_clickMarker, _mouseEventArgs);
            }
        }

        #region Map Zoom

        public void ZoomIn() {
            UpdateZoomByDelta(false);
            //UpdateZoomByPercent(false);
        }

        private void UpdateZoomByPercent(bool isZoomOut) {
            int lZoom100 = mapControl.MaxZoom - mapControl.MinZoom;
            double lZoomDelta = lZoom100 * ZoomPrcDelta;
            mapControl.Zoom += (isZoomOut ? 1 : -1) * lZoomDelta;
            _zoomPercent = Convert.ToInt32(mapControl.Zoom);
        }

        private void UpdateZoomByDelta(bool isZoomOut) {
            mapControl.Zoom += (isZoomOut ? 1 : -1) * ZoomDelta;
            _rmtView.RefreshZoom();
        }

        public void ZoomOut() {
            UpdateZoomByDelta(true);
            //UpdateZoomByPercent(true);
        }

        #endregion

        private void mapControl_OnMapZoomChanged() {
            //_zoomValue = Convert.ToInt32(mapControl.Zoom);
            //_rmtView.RefreshZoom();
        }
    }
}
