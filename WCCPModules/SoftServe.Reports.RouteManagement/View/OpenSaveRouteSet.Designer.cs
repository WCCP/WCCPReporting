﻿namespace SoftServe.Reports.RouteManagement.View {
    partial class OpenSaveRouteSet {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelButtons = new DevExpress.XtraEditors.PanelControl();
            this.btnLoadActual = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.checkIsLoad = new DevExpress.XtraEditors.CheckEdit();
            this.editName = new DevExpress.XtraEditors.TextEdit();
            this.labelName = new DevExpress.XtraEditors.LabelControl();
            this.panelGrid = new DevExpress.XtraEditors.PanelControl();
            this.gridRouteSet = new DevExpress.XtraGrid.GridControl();
            this.routeSetBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRouteSetId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupervisorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupervisorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRouteSetDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsLoad = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelButtons)).BeginInit();
            this.panelButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkIsLoad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).BeginInit();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRouteSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.routeSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panelButtons
            // 
            this.panelButtons.Controls.Add(this.btnLoadActual);
            this.panelButtons.Controls.Add(this.btnOk);
            this.panelButtons.Controls.Add(this.btnCancel);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelButtons.Location = new System.Drawing.Point(0, 285);
            this.panelButtons.Margin = new System.Windows.Forms.Padding(2);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(552, 48);
            this.panelButtons.TabIndex = 0;
            // 
            // btnLoadActual
            // 
            this.btnLoadActual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadActual.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.btnLoadActual.Location = new System.Drawing.Point(16, 12);
            this.btnLoadActual.Name = "btnLoadActual";
            this.btnLoadActual.Size = new System.Drawing.Size(144, 23);
            this.btnLoadActual.TabIndex = 2;
            this.btnLoadActual.Text = "Загрузить актуальные";
            this.btnLoadActual.ToolTip = "Загрузить актуальные маршруты";
            this.btnLoadActual.Click += new System.EventHandler(this.btnLoadActual_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(356, 12);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Загрузить";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(453, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.btnDelete);
            this.panelTop.Controls.Add(this.checkIsLoad);
            this.panelTop.Controls.Add(this.editName);
            this.panelTop.Controls.Add(this.labelName);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(552, 40);
            this.panelTop.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = global::SoftServe.Reports.RouteManagement.Properties.Resources.Erase_16x16;
            this.btnDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDelete.Location = new System.Drawing.Point(520, 8);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(24, 24);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.ToolTip = "Удалить набор маршрутов";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // checkIsLoad
            // 
            this.checkIsLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkIsLoad.Location = new System.Drawing.Point(404, 12);
            this.checkIsLoad.Name = "checkIsLoad";
            this.checkIsLoad.Properties.AutoWidth = true;
            this.checkIsLoad.Properties.Caption = "Выгрузить в SW";
            this.checkIsLoad.Size = new System.Drawing.Size(105, 19);
            this.checkIsLoad.TabIndex = 8;
            // 
            // editName
            // 
            this.editName.Location = new System.Drawing.Point(68, 12);
            this.editName.Name = "editName";
            this.editName.Size = new System.Drawing.Size(320, 20);
            this.editName.TabIndex = 7;
            this.editName.EditValueChanged += new System.EventHandler(this.editName_EditValueChanged);
            // 
            // labelName
            // 
            this.labelName.Location = new System.Drawing.Point(12, 16);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(52, 13);
            this.labelName.TabIndex = 6;
            this.labelName.Text = "Название:";
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gridRouteSet);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 40);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(552, 245);
            this.panelGrid.TabIndex = 2;
            // 
            // gridRouteSet
            // 
            this.gridRouteSet.DataSource = this.routeSetBindingSource;
            this.gridRouteSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRouteSet.Location = new System.Drawing.Point(2, 2);
            this.gridRouteSet.MainView = this.gridView;
            this.gridRouteSet.Name = "gridRouteSet";
            this.gridRouteSet.Size = new System.Drawing.Size(548, 241);
            this.gridRouteSet.TabIndex = 0;
            this.gridRouteSet.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // routeSetBindingSource
            // 
            this.routeSetBindingSource.DataSource = typeof(SoftServe.Reports.RouteManagement.Model.Route.RouteSet);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRouteSetId,
            this.colComment,
            this.colCustId,
            this.colSupervisorId,
            this.colSupervisorName,
            this.colRouteSetDate,
            this.colIsLoad});
            this.gridView.GridControl = this.gridRouteSet;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsMenu.EnableFooterMenu = false;
            this.gridView.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            // 
            // colRouteSetId
            // 
            this.colRouteSetId.FieldName = "RouteSetId";
            this.colRouteSetId.Name = "colRouteSetId";
            this.colRouteSetId.Width = 85;
            // 
            // colComment
            // 
            this.colComment.Caption = "Название";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 0;
            this.colComment.Width = 176;
            // 
            // colCustId
            // 
            this.colCustId.FieldName = "CustId";
            this.colCustId.Name = "colCustId";
            this.colCustId.Width = 69;
            // 
            // colSupervisorId
            // 
            this.colSupervisorId.FieldName = "SupervisorId";
            this.colSupervisorId.Name = "colSupervisorId";
            this.colSupervisorId.Width = 69;
            // 
            // colSupervisorName
            // 
            this.colSupervisorName.Caption = "Супервайзер";
            this.colSupervisorName.FieldName = "SupervisorName";
            this.colSupervisorName.Name = "colSupervisorName";
            this.colSupervisorName.Visible = true;
            this.colSupervisorName.VisibleIndex = 1;
            this.colSupervisorName.Width = 112;
            // 
            // colRouteSetDate
            // 
            this.colRouteSetDate.Caption = "Дата";
            this.colRouteSetDate.DisplayFormat.FormatString = "g";
            this.colRouteSetDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colRouteSetDate.FieldName = "RouteSetDate";
            this.colRouteSetDate.Name = "colRouteSetDate";
            this.colRouteSetDate.Visible = true;
            this.colRouteSetDate.VisibleIndex = 2;
            this.colRouteSetDate.Width = 97;
            // 
            // colIsLoad
            // 
            this.colIsLoad.Caption = "Выгружено в SW";
            this.colIsLoad.FieldName = "IsLoad";
            this.colIsLoad.Name = "colIsLoad";
            this.colIsLoad.Visible = true;
            this.colIsLoad.VisibleIndex = 3;
            this.colIsLoad.Width = 110;
            // 
            // OpenSaveRouteSet
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(552, 333);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelButtons);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(380, 188);
            this.Name = "OpenSaveRouteSet";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Сохранить/Загрузить набор маршрутов";
            ((System.ComponentModel.ISupportInitialize)(this.panelButtons)).EndInit();
            this.panelButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkIsLoad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGrid)).EndInit();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridRouteSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.routeSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelButtons;
        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.PanelControl panelGrid;
        private DevExpress.XtraGrid.GridControl gridRouteSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteSetId;
        private DevExpress.XtraGrid.Columns.GridColumn colSupervisorId;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteSetDate;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colCustId;
        private DevExpress.XtraGrid.Columns.GridColumn colIsLoad;
        private DevExpress.XtraGrid.Columns.GridColumn colSupervisorName;
        private System.Windows.Forms.BindingSource routeSetBindingSource;
        private DevExpress.XtraEditors.SimpleButton btnLoadActual;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.CheckEdit checkIsLoad;
        private DevExpress.XtraEditors.TextEdit editName;
        private DevExpress.XtraEditors.LabelControl labelName;
    }
}