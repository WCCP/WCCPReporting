﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using System.Drawing.Drawing2D;

namespace SoftServe.Reports.RouteManagement.View
{
    public partial class ListTTControl : XtraUserControl
    {

        private int _countTt;
        private decimal _distanceKm;
        private long _durationMim;

        public ListTTControl()
        {
            InitializeComponent();
            _countTt = -1;
        }

        #region Properties

        public ListBoxControl ListTT
        {
            get { return listTT; }
        }

        public XtraTabPage Page { get; set; }

        public int CountTt
        {
            get { return _countTt; }
            set
            {
                if (_countTt == value)
                    return;
                _countTt = value;
                lblTTCount.Text = _countTt.ToString(CultureInfo.InvariantCulture);
                //RecalcDuration();
            }
        }

        public decimal Distance
        {
            get { return _distanceKm; }
            set
            {
                if (_distanceKm == value)
                    return;
                _distanceKm = value;
                lblKm.Text = (_distanceKm <= 0) ? @"N/A" : string.Format("{0:N1}", _distanceKm);
                //RecalcDuration();
            }
        }

        public long DurationMinutes
        {
            get { return _durationMim; }
            set
            {
                if (_durationMim == value)
                    return;
                _durationMim = value;
                lblHm.Text = (_durationMim <= 0) ? @"N/A" : string.Format("{0}:{1:D2}", _durationMim / 60, _durationMim % 60);
            }
        }

        #endregion

        private void listTT_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            List<OutletInRoute> lDataSource = ((sender as ListBoxControl).DataSource as List<OutletInRoute>);
            if (e.Index >= lDataSource.Count || e.Index < 0)
                return;
            OutletInRoute lOutletInRoute = lDataSource[e.Index];
            if (null == lOutletInRoute)
                return;

            
            if ((e.State & DrawItemState.Selected) != 0)
            {
                e.Appearance.BackColor = SystemColors.Highlight;
                e.Appearance.BackColor2 = Color.FromArgb(e.Appearance.BackColor.A / 2, e.Appearance.BackColor.R, e.Appearance.BackColor.G, e.Appearance.BackColor.B);
                e.Appearance.ForeColor = lOutletInRoute.Outlet.IsMarker ? SystemColors.HighlightText : SystemColors.InactiveCaptionText;
            }
            else
            {
                e.Appearance.BackColor = lOutletInRoute.Outlet.IsMarker ? SystemColors.Window : SystemColors.ButtonFace;
                e.Appearance.BackColor2 = lOutletInRoute.Outlet.IsMarker ? SystemColors.Window : Color.FromArgb(SystemColors.ButtonFace.A / 2, SystemColors.ButtonFace.R, SystemColors.ButtonFace.G, SystemColors.ButtonFace.B);
                e.Appearance.ForeColor = SystemColors.WindowText;
            }

            Brush backBrush = new LinearGradientBrush(e.Bounds, e.Appearance.BackColor, e.Appearance.BackColor2, LinearGradientMode.Horizontal);
                
            e.Cache.FillRectangle(backBrush, e.Bounds);
            ControlPaint.DrawBorder(e.Graphics, e.Bounds, e.Appearance.BorderColor, ButtonBorderStyle.Solid);

            string itemText = listTT.GetItemText(e.Index);
            e.Cache.DrawString(itemText, new Font(e.Appearance.Font.Name,
              e.Appearance.Font.Size, FontStyle.Regular), new SolidBrush(e.Appearance.ForeColor),
              e.Bounds, e.Appearance.GetStringFormat());

            e.Handled = true;
        }

    }
}
