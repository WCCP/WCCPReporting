﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SoftServe.Core.Common.Logging;
using SoftServe.Reports.RouteManagement.DataAccess;
using SoftServe.Reports.RouteManagement.Model.Route;
using SoftServe.Reports.RouteManagement.Properties;
using WccpReporting;

namespace SoftServe.Reports.RouteManagement.View {
    public partial class OpenSaveRouteSet : Form {
        private string _comment;
        private bool _isLoad;
        private int _currentIndex;
        private int _routeSetId;
        private List<RouteSet> _routeSetList;
        private int _dupRouteSetId;
        private bool _dupIsLoad;
        private bool _isSave;

        public OpenSaveRouteSet() {
            InitializeComponent();
            _currentIndex = -1;
        }

        public List<RouteSet> RouteSetList {
            get { return _routeSetList; }
            set { 
                _routeSetList = value;
                gridRouteSet.DataSource = _routeSetList;
                if (_routeSetList.Count == 0) {
                    btnDelete.Enabled = false;
                    if (_isSave) {
                        editName.EditValue = _comment = "Новый набор маршрутов";
                        btnOk.Enabled = true;
                    }
                    else
                        btnOk.Enabled = false;
                    _currentIndex = -1;
                    _routeSetId = 0;
                }
            }
        }

        public string Comment {
            get { return _comment; }
        }

        public bool IsLoad {
            get { return _isLoad; }
        }

        public int CurrentIndex {
            get { return _currentIndex; }
        }

        public int RouteSetId {
            get { return _routeSetId; }
        }

        public void SetOptions(bool isSave, int supervisorId) {
            _isSave = isSave;
            if (isSave) {
                Text = Resources.StrSaveRouteSetCaption;
                btnOk.Text = Resources.StrSave;
                btnLoadActual.Enabled = false;
                btnLoadActual.Visible = false;
                checkIsLoad.Checked = false;
                checkIsLoad.Visible = DataProvider.GetSaveSWStatus(supervisorId);
            }
            else {
                Text = Resources.StrOpenRouteSetCaption;
                btnOk.Text = Resources.StrLoad;
                btnLoadActual.Enabled = true;
                btnLoadActual.Visible = true;
                checkIsLoad.Enabled = false;
                checkIsLoad.Visible = false;
            }
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) {
            if (_routeSetList.Count == 0) {
                _routeSetId = 0;
                _currentIndex = -1;
                btnDelete.Enabled = false;
                btnOk.Enabled = false;
                editName.EditValue = string.Empty;
                return;
            }
            if (e.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                return;
            _currentIndex = gridView.GetDataSourceRowIndex(e.FocusedRowHandle);
            editName.EditValue = _routeSetList[_currentIndex].Comment;
            //checkIsLoad.Checked = _isLoad = _routeSetList[_currentIndex].IsLoad;
            if (_routeSetList[_currentIndex].IsLoad) {
                _routeSetId = 0;
                btnDelete.Enabled = false;
            }
            else {
                _routeSetId = _routeSetList[_currentIndex].RouteSetId;
                btnDelete.Enabled = true;
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {
            _comment = (string) editName.EditValue;
            _isLoad = checkIsLoad.Checked;
            _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
            if (!_isSave)
                return;

            if (IsDuplicateRouteSet()) {
                DialogResult lDialogResult;
                if (_dupIsLoad) {
                    MessageBox.Show(string.Format("Вы не можете заменить набор маршрутов \"{0}\", который уже был выгружен в SW.", _comment),
                        Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    DialogResult = DialogResult.None;
                }
                else {
                    lDialogResult = MessageBox.Show(string.Format("Набор маршрутов с именем \"{0}\" уже существует.\nХотите заменить его?", _comment),
                        Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (lDialogResult == DialogResult.Yes)
                        _routeSetId = _dupRouteSetId;
                    else
                        DialogResult = DialogResult.None;
                }
            }
        }

        private bool IsDuplicateRouteSet() {
            bool lIsDuplicate = false;
            foreach (RouteSet lRouteSet in _routeSetList) {
                if (lRouteSet.Comment == (string) editName.EditValue) {
                    lIsDuplicate = true;
                    _dupRouteSetId = lRouteSet.RouteSetId;
                    _dupIsLoad = lRouteSet.IsLoad;
                    break;
                }
            }
            return lIsDuplicate;
        }

        private void editName_EditValueChanged(object sender, EventArgs e) {
            _routeSetId = 0;
            if ((string) editName.EditValue == string.Empty && _isSave)
                btnOk.Enabled = false;
            else
                btnOk.Enabled = true;
            if (_routeSetList.Count == 0 && !_isSave)
                btnOk.Enabled = false;
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            if (_routeSetList.Count == 0)
                return;
            if (_routeSetList[_currentIndex].IsLoad)
                return;

            DialogResult lDialogResult = MessageBox.Show(string.Format("Вы действительно хотите удалить набор маршрутов \"{0}\"",
                _routeSetList[_currentIndex].Comment),
                Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (lDialogResult == DialogResult.Cancel)
                return;

            #region Logging
            WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "btnDelete_Click()",
                        String.Format("delete routeSetId mode = {0}", _routeSetList[_currentIndex].RouteSetId),
                        LayerType.UI, null); 
            #endregion

            bool lResult = false;
            try {
                DataProvider.DeleteRouteSet(_routeSetList[_currentIndex].RouteSetId);
                lResult = true;
            }
            catch (Exception lException) {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "btnDelete_Click()", "OpenSaveRouteSet", LayerType.UI, null, lException.Message);
                MessageBox.Show(string.Format("Возникла ошибка при удалении набора маршрутов \"{0}\".\n{1}",
                    _routeSetList[_currentIndex].Comment, lException.Message),
                    Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (lResult) {
                int lPrevRowHandle = gridView.FocusedRowHandle;
                gridView.DeleteRow(gridView.FocusedRowHandle);
                //TODO: call gridView_FocusedRowChanged
                if (lPrevRowHandle == gridView.FocusedRowHandle)
                    gridView_FocusedRowChanged(gridView, new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(lPrevRowHandle, gridView.FocusedRowHandle));
//                if (gridView.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) {
//                    _routeSetId = 0;
//                    _currentIndex = -1;
//                    return;
//                }
//                
//                _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
//                editName.EditValue = _routeSetList[_currentIndex].Comment;
//                //checkIsLoad.Checked = _isLoad = _routeSetList[_currentIndex].IsLoad;
//                if (_routeSetList[_currentIndex].IsLoad) {
//                    _routeSetId = 0;
//                    btnDelete.Enabled = false;
//                }
//                else {
//                    _routeSetId = _routeSetList[_currentIndex].RouteSetId;
//                    btnDelete.Enabled = true;
//                }
            }
        }

        private void btnLoadActual_Click(object sender, EventArgs e) {
            _comment = string.Empty;
            _isLoad = false;
            _currentIndex = -2;
        }

    }
}
