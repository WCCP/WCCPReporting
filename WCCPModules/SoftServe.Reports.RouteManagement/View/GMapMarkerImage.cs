﻿using System.Drawing;
using System.Runtime.Serialization;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.ToolTips;

namespace SoftServe.Reports.RouteManagement.View {
    public class GMapMarkerImage : GMapMarker {
        private Image _image;

        public GMapMarkerImage(PointLatLng pos) : base(pos) {
        }

        public GMapMarkerImage(PointLatLng pos, Image image) : base(pos) {
            _image = image;
            Size = _image.Size;
            Offset = new Point(-Size.Width / 2, -Size.Height);
            //Offset = new Point(0, 0);
            ToolTip = new GMapBaloonToolTip(this);
        }

        public GMapMarkerImage(SerializationInfo info, StreamingContext context) : base(info, context) {
        }

        public Image MarkerImage {
            get { return _image; }
            set { _image = value; }
        }

        public override void OnRender(Graphics g) {
            g.DrawImage(_image, LocalPosition.X, LocalPosition.Y, Size.Width, Size.Height);
        }

    }
}