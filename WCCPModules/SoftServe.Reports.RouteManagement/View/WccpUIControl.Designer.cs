﻿//namespace SoftServe.Reports.RouteManagement.View {

using SoftServe.Reports.RouteManagement.Model.Outlet;

namespace WccpReporting {
    partial class WccpUIControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageRoutes = new DevExpress.XtraTab.XtraTabPage();
            this.panelMain = new DevExpress.XtraEditors.PanelControl();
            this.splitContainer = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabControlRoutes = new DevExpress.XtraTab.XtraTabControl();
            this.tabPage = new DevExpress.XtraTab.XtraTabPage();
            this.listTTControl1 = new SoftServe.Reports.RouteManagement.View.ListTTControl();
            this.barDockRouteTools = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.markersControl = new SoftServe.Reports.RouteManagement.View.MarkersControl();
            this.barDockMapTools = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.statusPanel = new DevExpress.XtraEditors.PanelControl();
            this.barDockMapFilter = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.toolTipContrListTT = new DevExpress.Utils.ToolTipController(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barRouteTools = new DevExpress.XtraBars.Bar();
            this.btnOptimizeRoute = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnReverseRoute = new DevExpress.XtraBars.BarButtonItem();
            this.btnCopyRoute = new DevExpress.XtraBars.BarSubItem();
            this.btnDeleteRoute = new DevExpress.XtraBars.BarButtonItem();
            this.barFilter = new DevExpress.XtraBars.Bar();
            this.cbFilterTS = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxTS = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbFilterM2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxM2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbFilterM1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxM1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbShowTT = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxShowTT = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbFrequency = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxFrequency = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.btnZoomIn = new DevExpress.XtraBars.BarButtonItem();
            this.btnZoomOut = new DevExpress.XtraBars.BarButtonItem();
            this.barMapTools = new DevExpress.XtraBars.Bar();
            this.btnOpenRoutes = new DevExpress.XtraBars.BarButtonItem();
            this.btnSaveRoutes = new DevExpress.XtraBars.BarButtonItem();
            this.barAndDockingController = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.pmiAddToRoute = new DevExpress.XtraBars.BarButtonItem();
            this.pmiDeleteFromRoute = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticText = new DevExpress.XtraBars.BarStaticItem();
            this.imgCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.pmAddRemoveTt = new DevExpress.XtraBars.PopupMenu(this.components);
            this.imgMarkers = new DevExpress.Utils.ImageCollection(this.components);
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageRoutes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).BeginInit();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlRoutes)).BeginInit();
            this.tabControlRoutes.SuspendLayout();
            this.tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxTS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxShowTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAddRemoveTt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarkers)).BeginInit();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderButtons = ((DevExpress.XtraTab.TabButtons)((((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next) 
            | DevExpress.XtraTab.TabButtons.Close) 
            | DevExpress.XtraTab.TabButtons.Default)));
            this.tabControl.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.Never;
            this.tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabPageRoutes;
            this.tabControl.Size = new System.Drawing.Size(1187, 637);
            this.tabControl.TabIndex = 0;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageRoutes});
            // 
            // tabPageRoutes
            // 
            this.tabPageRoutes.Controls.Add(this.panelMain);
            this.tabPageRoutes.Controls.Add(this.statusPanel);
            this.tabPageRoutes.Controls.Add(this.barDockMapFilter);
            this.tabPageRoutes.Name = "tabPageRoutes";
            this.tabPageRoutes.Size = new System.Drawing.Size(1181, 609);
            this.tabPageRoutes.Text = "Управление Маршрутами";
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.splitContainer);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 35);
            this.panelMain.Name = "panelMain";
            this.panelMain.Padding = new System.Windows.Forms.Padding(1);
            this.panelMain.Size = new System.Drawing.Size(1181, 546);
            this.panelMain.TabIndex = 2;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(3, 3);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Panel1.Controls.Add(this.tabControlRoutes);
            this.splitContainer.Panel1.Controls.Add(this.barDockRouteTools);
            this.splitContainer.Panel1.Text = "Panel1";
            this.splitContainer.Panel2.Controls.Add(this.markersControl);
            this.splitContainer.Panel2.Controls.Add(this.barDockMapTools);
            this.splitContainer.Panel2.Text = "Panel2";
            this.splitContainer.Size = new System.Drawing.Size(1175, 540);
            this.splitContainer.SplitterPosition = 323;
            this.splitContainer.TabIndex = 1;
            this.splitContainer.Text = "splitContainerControl1";
            // 
            // tabControlRoutes
            // 
            this.tabControlRoutes.AllowDrop = true;
            this.tabControlRoutes.Appearance.Options.UseTextOptions = true;
            this.tabControlRoutes.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.tabControlRoutes.AppearancePage.Header.Options.UseTextOptions = true;
            this.tabControlRoutes.AppearancePage.Header.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.tabControlRoutes.AppearancePage.Header.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.tabControlRoutes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlRoutes.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.False;
            this.tabControlRoutes.Location = new System.Drawing.Point(0, 0);
            this.tabControlRoutes.MultiLine = DevExpress.Utils.DefaultBoolean.True;
            this.tabControlRoutes.Name = "tabControlRoutes";
            this.tabControlRoutes.SelectedTabPage = this.tabPage;
            this.tabControlRoutes.ShowHeaderFocus = DevExpress.Utils.DefaultBoolean.True;
            this.tabControlRoutes.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tabControlRoutes.ShowToolTips = DevExpress.Utils.DefaultBoolean.True;
            this.tabControlRoutes.Size = new System.Drawing.Size(323, 505);
            this.tabControlRoutes.TabIndex = 2;
            this.tabControlRoutes.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPage});
            this.tabControlRoutes.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControlRoutes_SelectedPageChanged);
            this.tabControlRoutes.DragDrop += new System.Windows.Forms.DragEventHandler(this.tabPage_DragDrop);
            this.tabControlRoutes.DragOver += new System.Windows.Forms.DragEventHandler(this.tabPage_DragOver);
            // 
            // tabPage
            // 
            this.tabPage.AllowDrop = true;
            this.tabPage.Controls.Add(this.listTTControl1);
            this.tabPage.Name = "tabPage";
            this.tabPage.Size = new System.Drawing.Size(317, 477);
            this.tabPage.Text = "xtraTabPage1";
            this.tabPage.DragDrop += new System.Windows.Forms.DragEventHandler(this.tabPage_DragDrop);
            this.tabPage.DragOver += new System.Windows.Forms.DragEventHandler(this.tabPage_DragOver);
            // 
            // listTTControl1
            // 
            this.listTTControl1.AllowDrop = true;
            this.listTTControl1.AutoSize = true;
            this.listTTControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.listTTControl1.CountTt = 0;
            this.listTTControl1.Distance = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.listTTControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTTControl1.DurationMinutes = ((long)(0));
            this.listTTControl1.Location = new System.Drawing.Point(0, 0);
            this.listTTControl1.MinimumSize = new System.Drawing.Size(300, 0);
            this.listTTControl1.Name = "listTTControl1";
            this.listTTControl1.Page = null;
            this.listTTControl1.Size = new System.Drawing.Size(317, 477);
            this.listTTControl1.TabIndex = 0;
            this.listTTControl1.DragOver += new System.Windows.Forms.DragEventHandler(this.tabPage_DragOver);
            // 
            // barDockRouteTools
            // 
            this.barDockRouteTools.CausesValidation = false;
            this.barDockRouteTools.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockRouteTools.Location = new System.Drawing.Point(0, 505);
            this.barDockRouteTools.Name = "barDockRouteTools";
            this.barDockRouteTools.Size = new System.Drawing.Size(323, 35);
            this.barDockRouteTools.Text = "standaloneBarDockControl2";
            // 
            // markersControl
            // 
            this.markersControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.markersControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.markersControl.EnableCentering = false;
            this.markersControl.Location = new System.Drawing.Point(0, 0);
            this.markersControl.Name = "markersControl";
            this.markersControl.ShowTT = SoftServe.Reports.RouteManagement.Controller.ShowTTEnum.M2;
            this.markersControl.Size = new System.Drawing.Size(847, 505);
            this.markersControl.TabIndex = 2;
            this.markersControl.VisitFrequency = 2147483647;
            // 
            // barDockMapTools
            // 
            this.barDockMapTools.CausesValidation = false;
            this.barDockMapTools.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockMapTools.Location = new System.Drawing.Point(0, 505);
            this.barDockMapTools.Name = "barDockMapTools";
            this.barDockMapTools.Size = new System.Drawing.Size(847, 35);
            this.barDockMapTools.Text = "standaloneBarDockControl1";
            // 
            // statusPanel
            // 
            this.statusPanel.AlwaysScrollActiveControlIntoView = false;
            this.statusPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.statusPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statusPanel.Location = new System.Drawing.Point(0, 581);
            this.statusPanel.Name = "statusPanel";
            this.statusPanel.Size = new System.Drawing.Size(1181, 28);
            this.statusPanel.TabIndex = 1;
            // 
            // barDockMapFilter
            // 
            this.barDockMapFilter.CausesValidation = false;
            this.barDockMapFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockMapFilter.Location = new System.Drawing.Point(0, 0);
            this.barDockMapFilter.Name = "barDockMapFilter";
            this.barDockMapFilter.Size = new System.Drawing.Size(1181, 35);
            this.barDockMapFilter.Text = "standaloneBarDockControl1";
            // 
            // toolTipContrListTT
            // 
            this.toolTipContrListTT.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolTipContrListTT.Appearance.Options.UseFont = true;
            this.toolTipContrListTT.Appearance.Options.UseTextOptions = true;
            this.toolTipContrListTT.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.toolTipContrListTT.Rounded = true;
            this.toolTipContrListTT.ShowBeak = true;
            this.toolTipContrListTT.ToolTipLocation = DevExpress.Utils.ToolTipLocation.TopRight;
            this.toolTipContrListTT.ToolTipType = DevExpress.Utils.ToolTipType.Standard;
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barRouteTools,
            this.barFilter,
            this.barMapTools});
            this.barManager.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Popup", new System.Guid("8ba0e6dd-3395-4a8b-92a3-fedd1989fc81"), false),
            new DevExpress.XtraBars.BarManagerCategory("ListActions", new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23")),
            new DevExpress.XtraBars.BarManagerCategory("MapActions", new System.Guid("875cce61-b209-4530-80cd-39c516d30907"))});
            this.barManager.Controller = this.barAndDockingController;
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.DockControls.Add(this.barDockRouteTools);
            this.barManager.DockControls.Add(this.barDockMapFilter);
            this.barManager.DockControls.Add(this.barDockMapTools);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.cbFilterM2,
            this.cbFilterM1,
            this.btnOptimizeRoute,
            this.btnReverseRoute,
            this.btnCopyRoute,
            this.btnDeleteRoute,
            this.btnOpenRoutes,
            this.btnSaveRoutes,
            this.cbShowTT,
            this.cbFrequency,
            this.pmiAddToRoute,
            this.pmiDeleteFromRoute,
            this.btnZoomIn,
            this.btnZoomOut,
            this.barStaticText,
            this.cbFilterTS});
            this.barManager.LargeImages = this.imgCollection;
            this.barManager.MaxItemId = 25;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBoxM2,
            this.repositoryItemComboBoxM1,
            this.repositoryItemComboBoxShowTT,
            this.repositoryItemComboBoxFrequency,
            this.repositoryItemImageEdit1,
            this.repositoryItemComboBoxTS,
            this.repositoryItemComboBox1});
            // 
            // barRouteTools
            // 
            this.barRouteTools.BarName = "RouteTools";
            this.barRouteTools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.barRouteTools.DockCol = 0;
            this.barRouteTools.DockRow = 0;
            this.barRouteTools.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barRouteTools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnOptimizeRoute),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnReverseRoute),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnCopyRoute, "", true, false, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDeleteRoute)});
            this.barRouteTools.OptionsBar.AllowQuickCustomization = false;
            this.barRouteTools.OptionsBar.DisableClose = true;
            this.barRouteTools.OptionsBar.DisableCustomization = true;
            this.barRouteTools.OptionsBar.DrawDragBorder = false;
            this.barRouteTools.StandaloneBarDockControl = this.barDockRouteTools;
            this.barRouteTools.Text = "RouteTools";
            // 
            // btnOptimizeRoute
            // 
            this.btnOptimizeRoute.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.btnOptimizeRoute.Hint = "Оптимизировать маршрут";
            this.btnOptimizeRoute.Id = 2;
            this.btnOptimizeRoute.LargeImageIndex = 0;
            this.btnOptimizeRoute.Name = "btnOptimizeRoute";
            this.btnOptimizeRoute.ShowCaptionOnBar = false;
            this.btnOptimizeRoute.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnOptimizeRoute_ItemClick);
            // 
            // btnReverseRoute
            // 
            this.btnReverseRoute.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.btnReverseRoute.Hint = "Развернуть маршрут";
            this.btnReverseRoute.Id = 3;
            this.btnReverseRoute.LargeImageIndex = 1;
            this.btnReverseRoute.Name = "btnReverseRoute";
            this.btnReverseRoute.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReverseRoute_ItemClick);
            // 
            // btnCopyRoute
            // 
            this.btnCopyRoute.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.btnCopyRoute.Hint = "Копировать маршрут";
            this.btnCopyRoute.Id = 15;
            this.btnCopyRoute.ItemClickFireMode = DevExpress.XtraBars.BarItemEventFireMode.Immediate;
            this.btnCopyRoute.LargeImageIndex = 2;
            this.btnCopyRoute.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.SmallImagesText;
            this.btnCopyRoute.Name = "btnCopyRoute";
            // 
            // btnDeleteRoute
            // 
            this.btnDeleteRoute.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.btnDeleteRoute.Hint = "Очистить маршрут";
            this.btnDeleteRoute.Id = 5;
            this.btnDeleteRoute.LargeImageIndex = 3;
            this.btnDeleteRoute.Name = "btnDeleteRoute";
            this.btnDeleteRoute.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteRoute_ItemClick);
            // 
            // barFilter
            // 
            this.barFilter.BarName = "RouteFilter";
            this.barFilter.DockCol = 0;
            this.barFilter.DockRow = 0;
            this.barFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barFilter.FloatLocation = new System.Drawing.Point(413, 162);
            this.barFilter.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.cbFilterTS, "", false, true, true, 154),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.cbFilterM2, "", false, true, true, 128),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.cbFilterM1, "", false, true, true, 125),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.cbShowTT, "", true, true, true, 107),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.cbFrequency, "", false, true, true, 63),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnZoomIn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnZoomOut)});
            this.barFilter.OptionsBar.AllowQuickCustomization = false;
            this.barFilter.OptionsBar.DisableClose = true;
            this.barFilter.OptionsBar.DisableCustomization = true;
            this.barFilter.OptionsBar.DrawDragBorder = false;
            this.barFilter.StandaloneBarDockControl = this.barDockMapFilter;
            this.barFilter.Text = "Route Filter";
            // 
            // cbFilterTS
            // 
            this.cbFilterTS.Caption = "ТС:";
            this.cbFilterTS.Edit = this.repositoryItemComboBoxTS;
            this.cbFilterTS.Id = 24;
            this.cbFilterTS.Name = "cbFilterTS";
            this.cbFilterTS.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.cbFilterTS.EditValueChanged += new System.EventHandler(this.cbFilterTS_EditValueChanged);
            this.cbFilterTS.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cbFilter_ItemClick);
            // 
            // repositoryItemComboBoxTS
            // 
            this.repositoryItemComboBoxTS.AutoHeight = false;
            this.repositoryItemComboBoxTS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxTS.Name = "repositoryItemComboBoxTS";
            this.repositoryItemComboBoxTS.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cbFilterM2
            // 
            this.cbFilterM2.Caption = "M2:";
            this.cbFilterM2.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.cbFilterM2.Edit = this.repositoryItemComboBoxM2;
            this.cbFilterM2.Id = 0;
            this.cbFilterM2.Name = "cbFilterM2";
            this.cbFilterM2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.cbFilterM2.EditValueChanged += new System.EventHandler(this.cbFilterM2_EditValueChanged);
            this.cbFilterM2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cbFilter_ItemClick);
            // 
            // repositoryItemComboBoxM2
            // 
            this.repositoryItemComboBoxM2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxM2.DropDownRows = 8;
            this.repositoryItemComboBoxM2.Name = "repositoryItemComboBoxM2";
            this.repositoryItemComboBoxM2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cbFilterM1
            // 
            this.cbFilterM1.Caption = "M1:";
            this.cbFilterM1.CaptionAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.cbFilterM1.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.cbFilterM1.Edit = this.repositoryItemComboBoxM1;
            this.cbFilterM1.Id = 1;
            this.cbFilterM1.Name = "cbFilterM1";
            this.cbFilterM1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.cbFilterM1.EditValueChanged += new System.EventHandler(this.cbFilterM1_EditValueChanged);
            this.cbFilterM1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cbFilter_ItemClick);
            // 
            // repositoryItemComboBoxM1
            // 
            this.repositoryItemComboBoxM1.AutoHeight = false;
            this.repositoryItemComboBoxM1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxM1.Name = "repositoryItemComboBoxM1";
            this.repositoryItemComboBoxM1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cbShowTT
            // 
            this.cbShowTT.Caption = "Показать ТТ:";
            this.cbShowTT.CategoryGuid = new System.Guid("875cce61-b209-4530-80cd-39c516d30907");
            this.cbShowTT.Edit = this.repositoryItemComboBoxShowTT;
            this.cbShowTT.EditValue = "М2";
            this.cbShowTT.Id = 10;
            this.cbShowTT.Name = "cbShowTT";
            this.cbShowTT.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.cbShowTT.EditValueChanged += new System.EventHandler(this.cbShowTT_EditValueChanged);
            this.cbShowTT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cbFilter_ItemClick);
            // 
            // repositoryItemComboBoxShowTT
            // 
            this.repositoryItemComboBoxShowTT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxShowTT.DropDownRows = 4;
            this.repositoryItemComboBoxShowTT.Items.AddRange(new object[] {
            "ТС",
            "М2",
            "М1",
            "Маршрут"});
            this.repositoryItemComboBoxShowTT.Name = "repositoryItemComboBoxShowTT";
            this.repositoryItemComboBoxShowTT.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cbFrequency
            // 
            this.cbFrequency.Caption = "С частотой посещения:";
            this.cbFrequency.CategoryGuid = new System.Guid("875cce61-b209-4530-80cd-39c516d30907");
            this.cbFrequency.Edit = this.repositoryItemComboBoxFrequency;
            this.cbFrequency.EditValue = "Любой";
            this.cbFrequency.Id = 11;
            this.cbFrequency.Name = "cbFrequency";
            this.cbFrequency.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.cbFrequency.EditValueChanged += new System.EventHandler(this.cbFrequency_EditValueChanged);
            this.cbFrequency.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cbFilter_ItemClick);
            // 
            // repositoryItemComboBoxFrequency
            // 
            this.repositoryItemComboBoxFrequency.AutoHeight = false;
            this.repositoryItemComboBoxFrequency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxFrequency.DropDownRows = 16;
            this.repositoryItemComboBoxFrequency.Items.AddRange(new object[] {
            "-7",
            "-6",
            "-5",
            "-4",
            "-3",
            "-2",
            "-1",
            "0",
            "Любой",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.repositoryItemComboBoxFrequency.Name = "repositoryItemComboBoxFrequency";
            this.repositoryItemComboBoxFrequency.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.Hint = "Уменьшить масштаб";
            this.btnZoomIn.Id = 17;
            this.btnZoomIn.LargeImageIndex = 10;
            this.btnZoomIn.Name = "btnZoomIn";
            this.btnZoomIn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnZoomIn_ItemClick);
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.Hint = "Увеличить масштаб";
            this.btnZoomOut.Id = 19;
            this.btnZoomOut.LargeImageIndex = 9;
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnZoomOut_ItemClick);
            // 
            // barMapTools
            // 
            this.barMapTools.BarName = "MapTools";
            this.barMapTools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.barMapTools.DockCol = 0;
            this.barMapTools.DockRow = 0;
            this.barMapTools.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barMapTools.FloatLocation = new System.Drawing.Point(477, 659);
            this.barMapTools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnOpenRoutes),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSaveRoutes)});
            this.barMapTools.OptionsBar.AllowQuickCustomization = false;
            this.barMapTools.OptionsBar.DisableClose = true;
            this.barMapTools.OptionsBar.DisableCustomization = true;
            this.barMapTools.OptionsBar.DrawDragBorder = false;
            this.barMapTools.StandaloneBarDockControl = this.barDockMapTools;
            this.barMapTools.Text = "MapTools";
            // 
            // btnOpenRoutes
            // 
            this.btnOpenRoutes.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.btnOpenRoutes.Hint = "Загрузить маршруты";
            this.btnOpenRoutes.Id = 6;
            this.btnOpenRoutes.LargeImageIndex = 4;
            this.btnOpenRoutes.Name = "btnOpenRoutes";
            this.btnOpenRoutes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnOpenRoutes_ItemClick);
            // 
            // btnSaveRoutes
            // 
            this.btnSaveRoutes.CategoryGuid = new System.Guid("7d915f26-add4-4d55-be84-e786b5e2ff23");
            this.btnSaveRoutes.Hint = "Сохранить маршруты";
            this.btnSaveRoutes.Id = 7;
            this.btnSaveRoutes.LargeImageIndex = 5;
            this.btnSaveRoutes.Name = "btnSaveRoutes";
            this.btnSaveRoutes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSaveRoutes_ItemClick);
            // 
            // barAndDockingController
            // 
            this.barAndDockingController.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController.PropertiesBar.LargeIcons = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1187, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 637);
            this.barDockControlBottom.Size = new System.Drawing.Size(1187, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 637);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1187, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 637);
            // 
            // pmiAddToRoute
            // 
            this.pmiAddToRoute.Caption = "Добавить в маршрут";
            this.pmiAddToRoute.CategoryGuid = new System.Guid("8ba0e6dd-3395-4a8b-92a3-fedd1989fc81");
            this.pmiAddToRoute.Id = 12;
            this.pmiAddToRoute.Name = "pmiAddToRoute";
            // 
            // pmiDeleteFromRoute
            // 
            this.pmiDeleteFromRoute.Caption = "Удалить из маршрута";
            this.pmiDeleteFromRoute.CategoryGuid = new System.Guid("8ba0e6dd-3395-4a8b-92a3-fedd1989fc81");
            this.pmiDeleteFromRoute.Id = 13;
            this.pmiDeleteFromRoute.Name = "pmiDeleteFromRoute";
            // 
            // barStaticText
            // 
            this.barStaticText.Caption = "100%";
            this.barStaticText.Id = 23;
            this.barStaticText.Name = "barStaticText";
            this.barStaticText.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // imgCollection
            // 
            this.imgCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imgCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgCollection.ImageStream")));
            this.imgCollection.Images.SetKeyName(0, "roadmap24x24.png");
            this.imgCollection.Images.SetKeyName(1, "vise_versa_24x24.png");
            this.imgCollection.Images.SetKeyName(2, "Copy_24x24.png");
            this.imgCollection.Images.SetKeyName(3, "del24x24.png");
            this.imgCollection.Images.SetKeyName(4, "open24x24.png");
            this.imgCollection.Images.SetKeyName(5, "save24x24.png");
            this.imgCollection.Images.SetKeyName(6, "saveAll24x24.png");
            this.imgCollection.Images.SetKeyName(7, "upload24x24.png");
            this.imgCollection.Images.SetKeyName(8, "Yellow_star.png");
            this.imgCollection.Images.SetKeyName(9, "Zoom in.png");
            this.imgCollection.Images.SetKeyName(10, "Zoom out.png");
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(149, 167);
            this.bar1.FloatSize = new System.Drawing.Size(46, 24);
            this.bar1.Text = "Tools";
            // 
            // pmAddRemoveTt
            // 
            this.pmAddRemoveTt.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pmiAddToRoute),
            new DevExpress.XtraBars.LinkPersistInfo(this.pmiDeleteFromRoute)});
            this.pmAddRemoveTt.Manager = this.barManager;
            this.pmAddRemoveTt.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.SmallImagesText;
            this.pmAddRemoveTt.Name = "pmAddRemoveTt";
            // 
            // imgMarkers
            // 
            this.imgMarkers.ImageSize = new System.Drawing.Size(32, 37);
            this.imgMarkers.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgMarkers.ImageStream")));
            this.imgMarkers.Images.SetKeyName(0, "number_empty_32_37.png");
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.toolStripContainer1.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tabControl);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.barDockControlLeft);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.barDockControlRight);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.barDockControlBottom);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.barDockControlTop);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1187, 637);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(1187, 637);
            this.toolStripContainer1.TabIndex = 4;
            this.toolStripContainer1.Text = "toolStripContainer1";
            this.toolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(1187, 637);
            this.Resize += new System.EventHandler(this.WccpUIControl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageRoutes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).EndInit();
            this.panelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControlRoutes)).EndInit();
            this.tabControlRoutes.ResumeLayout(false);
            this.tabPage.ResumeLayout(false);
            this.tabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxTS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxShowTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAddRemoveTt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarkers)).EndInit();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPageRoutes;
        private DevExpress.XtraEditors.PanelControl panelMain;
        private DevExpress.XtraEditors.SplitContainerControl splitContainer;
        private DevExpress.XtraEditors.PanelControl statusPanel;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarEditItem cbFilterM2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxM2;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem cbFilterM1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxM1;
        private DevExpress.XtraBars.StandaloneBarDockControl barDockRouteTools;
        private DevExpress.XtraBars.Bar barRouteTools;
        private DevExpress.XtraBars.BarLargeButtonItem btnOptimizeRoute;
        private DevExpress.Utils.ImageCollection imgCollection;
        private DevExpress.XtraTab.XtraTabControl tabControlRoutes;
        private DevExpress.XtraBars.BarButtonItem btnReverseRoute;
        private DevExpress.XtraBars.BarButtonItem btnDeleteRoute;
        private DevExpress.XtraBars.StandaloneBarDockControl barDockMapTools;
        private DevExpress.XtraBars.StandaloneBarDockControl barDockMapFilter;
        private DevExpress.XtraBars.Bar barFilter;
        private DevExpress.XtraBars.Bar barMapTools;
        private DevExpress.XtraBars.BarButtonItem btnOpenRoutes;
        private DevExpress.XtraBars.BarButtonItem btnSaveRoutes;
        private DevExpress.XtraBars.BarEditItem cbShowTT;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxShowTT;
        private DevExpress.XtraBars.BarEditItem cbFrequency;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxFrequency;
        private DevExpress.XtraTab.XtraTabPage tabPage;
        private DevExpress.Utils.ToolTipController toolTipContrListTT;
        private SoftServe.Reports.RouteManagement.View.ListTTControl listTTControl1;
        private DevExpress.XtraBars.BarButtonItem pmiAddToRoute;
        private DevExpress.XtraBars.BarButtonItem pmiDeleteFromRoute;
        private DevExpress.XtraBars.PopupMenu pmAddRemoveTt;
        private DevExpress.Utils.ImageCollection imgMarkers;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private SoftServe.Reports.RouteManagement.View.MarkersControl markersControl;
        private DevExpress.XtraBars.BarSubItem btnCopyRoute;
        private DevExpress.XtraBars.BarButtonItem btnZoomIn;
        private DevExpress.XtraBars.BarButtonItem btnZoomOut;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraBars.BarStaticItem barStaticText;
        private DevExpress.XtraBars.BarEditItem cbFilterTS;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxTS;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
    }
}
