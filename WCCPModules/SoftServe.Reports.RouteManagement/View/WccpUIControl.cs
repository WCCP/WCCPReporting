﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using System.Linq;
using GMap.NET;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.Logging;
using SoftServe.Core.Common.View;
using SoftServe.Reports.RouteManagement;
using SoftServe.Reports.RouteManagement.Controller;
using SoftServe.Reports.RouteManagement.Model.Merch;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using SoftServe.Reports.RouteManagement.Model.Route;
using SoftServe.Reports.RouteManagement.Model.Supervisor;
using SoftServe.Reports.RouteManagement.Properties;
using SoftServe.Reports.RouteManagement.View;
using SoftServe.Reports.RouteManagement.WcfRouterService;

namespace WccpReporting {
    [ToolboxItem(false)]
    public partial class WccpUIControl : SimpleControlView, IRMTView
    {
        #region Fields

        private IRouteManagementController _controller;

        private readonly Dictionary<XtraTabPage, ListTTControl> _routeTabs =
            new Dictionary<XtraTabPage, ListTTControl>();

        private Point _point = Point.Empty;
        private OutletInRoute _outletInRoute;
        private List<OutletInRoute> _sourceOutletList;
        private bool _isDataLoading = true;
        private readonly object[] _comboBoxShowTtDataSource = {"ТС", "М2", "М1", "Маршрут"};
        private bool _wasClick = false;
        #endregion

        public WccpUIControl()
        {
            InitializeComponent();

            TimeParameter pt = WccpLogger.InitTimeParameter();
            Init();
            WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "Init()", "WccpUIControl", LayerType.UI, pt);
            //InitMap();
        }

        #region Implementation of IRMTView

        public ImageCollection Images
        {
            get { return imgCollection; }
        }

        public void ProcessEvents()
        {
            //TODO: Refresh status
        }

        #endregion


        private void Init()
        {
            //Need this because WccpUIControl instanciated from Delphi ManagedVCL library
            if (_controller == null)
            {
                _controller = new RouteManagementController(this);
            }
            markersControl.SetController(_controller);
            markersControl.SetRMTView(this);
            toolTipContrListTT.AllowHtmlText = FiltersHelper.AllowHtmlText;

            //init DB for RMT
            _controller.InitDBObjects();

            //Clear tab pages
            _routeTabs.Clear();
            tabControlRoutes.TabPages.Clear();

            //Assign events
//            markersControl.ImageListBox.MouseMove += imageListBox_MouseMove;
//            markersControl.ImageListBox.MouseLeave += listTT_MouseLeave;
            //markersControl.VGrid.ToolTipController = toolTipContrListTT;

            _controller.LoadData();
            DataToView();
            _isDataLoading = false;
        }

        private void DataToView()
        {
            // Fill filters
            repositoryItemComboBoxTS.Items.AddRange(_controller.GetCustomerList());
            cbFilterTS.EditValue = _controller.CurrentCustomer;

            //repositoryItemComboBoxM2.Items.AddRange(_controller.GetSupervisorList());
            cbFilterM2.EditValue = _controller.GetCurrentSupervisor();

            //repositoryItemComboBoxM1.Items.AddRange(_controller.GetMerchList());
            cbFilterM1.EditValue = _controller.GetCurrentMerch();
        }

        private XtraTabPage CreateTabPage(RouteWithOutletsModel route)
        {
            XtraTabPage lTabPage = new XtraTabPage();
            lTabPage.Text = route.GetRouteName(); // + '\n' + route.RouteId;
            lTabPage.Tag = route.GetRouteId();
            lTabPage.AllowDrop = true;
            lTabPage.Size = new Size(300, 400);
            lTabPage.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            ListTTControl lListTT = new ListTTControl();
            _routeTabs.Add(lTabPage, lListTT);
            lTabPage.Controls.Add(lListTT);

            lListTT.Dock = DockStyle.Fill;
            lListTT.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            lListTT.Location = new Point(0, 0);
            //lListTT.MinimumSize = new Size(300, 0);
            //lListTT.Size = new Size(360, 491);
            lListTT.TabIndex = 0;
            lListTT.ListTT.DataSource = route.Outlets;

            lListTT.CountTt = route.CountTt;
            lListTT.Distance = -1;
            lListTT.DurationMinutes = -1;

            // events
            ReassignEvents(route, lTabPage);

            // ListTT events
            lListTT.ListTT.MouseDown += listTT_MouseDown;
            lListTT.ListTT.MouseMove += listTT_MouseMove;
            lListTT.ListTT.DragOver += listTT_DragOver;
            lListTT.ListTT.MouseDoubleClick += listTT_MouseDoubleClick;
            lListTT.ListTT.MouseLeave += listTT_MouseLeave;

            return lTabPage;
        }

        private void ReassignEvents(RouteWithOutletsModel route, XtraTabPage page)
        {
            ListTTControl lListTT = _routeTabs[page];
            // clear events
            page.DragOver -= tabPage_DragOver;
            page.DragDrop -= tabPage_DragDrop;
            lListTT.ListTT.DragDrop -= listTT_DragDrop;

            //assign events
            if (route.RouteId == 0)
            {
                page.DragOver -= tabPage_DragOver;
                page.DragDrop -= tabPage_DragDrop;
                lListTT.ListTT.DragDrop -= listTT_DragDrop;
            }
            else
            {
                page.DragOver += tabPage_DragOver;
                page.DragDrop += tabPage_DragDrop;
                lListTT.ListTT.DragDrop += listTT_DragDrop;
            }
        }

        public override void SetController(IViewController viewController)
        {
            _controller = (IRouteManagementController) viewController;
        }

        public override IViewController GetController()
        {
            return _controller;
        }

        public override void RefreshData()
        {
            if (_isDataLoading)
                return;
            SupervisorModel sv = (SupervisorModel) cbFilterM2.EditValue;
            if (sv == null || sv.Id != _controller.CurrentSupervisor.Id)
                cbFilterM2.EditValue = _controller.CurrentSupervisor;
            MerchModel merch = (MerchModel) cbFilterM1.EditValue;
            if (merch == null || merch.Id != _controller.CurrentMerch.Id)

                cbFilterM1.EditValue = _controller.CurrentMerch;
            UpdateRoutes();
            UpdateButtonsStatus();
            //GetSourceRoute();                                                 //----------------Ticket#1041279 Redundant request to wcf
            RefreshRouteProperties();
            markersControl.UpdateMapMarkers();
        }


        #region Filter actions

        private void cbFilterTS_EditValueChanged(object sender, EventArgs e)
        {
            var cust = (CustomerSimpleModel) cbFilterTS.EditValue;

            #region Logging
            if (_wasClick)
            {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "cbFilterTS_EditValueChanged",
                    String.Format("From CustId = {0} to CustId = {1}", _controller.CurrentCustomer.Id, cust.Id),
                    LayerType.UI, null);
                _wasClick = !_wasClick;
            } 
            #endregion

            UpdateCbShowTtItems(cust.Id, _controller.CurrentCustomer.Id);
            _controller.ChangeCurrentCustomer(cust.Id);
            repositoryItemComboBoxM2.Items.Clear();
            repositoryItemComboBoxM2.Items.AddRange(_controller.GetSupervisorList());
            cbFilterM2.EditValue = _controller.GetCurrentSupervisor();
        }

        private void UpdateCbShowTtItems(int custIdNewValue, int custIdOldValue)
        {
            if (custIdNewValue != Constants.CUST_ID_ALL && custIdOldValue != Constants.CUST_ID_ALL)
            {
                return;
            }
            repositoryItemComboBoxShowTT.Items.Clear();
            bool showItemTs = custIdNewValue != Constants.CUST_ID_ALL;
            if (showItemTs)
            {
                repositoryItemComboBoxShowTT.Items.AddRange(_comboBoxShowTtDataSource);
            }
            else
            {
                repositoryItemComboBoxShowTT.Items.AddRange(_comboBoxShowTtDataSource.ToList()
                    .GetRange(1, _comboBoxShowTtDataSource.Length - 1));
                cbShowTT.EditValue = _comboBoxShowTtDataSource[1];
            }

        }

        private void cbFilterM2_EditValueChanged(object sender, EventArgs e)
        {
            SupervisorModel sv = (SupervisorModel) cbFilterM2.EditValue;
            int svId = (sv == null) ? Constants.EMPTY_OBJECT_ID : sv.Id;

            #region Logging
            if (_wasClick)
            {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "cbFilterM2_EditValueChanged",
                    String.Format("From SvId = {0} to SvId = {1}", _controller.CurrentSupervisor.Id, svId),
                    LayerType.UI, null);
                _wasClick = !_wasClick;
            } 
            #endregion
           
            _controller.ChangeCurrentSupervisor(svId);
            repositoryItemComboBoxM1.Items.Clear();
            repositoryItemComboBoxM1.Items.AddRange(_controller.GetMerchList());
            cbFilterM1.EditValue = _controller.GetCurrentMerch();
        }

        private void cbFilterM1_EditValueChanged(object sender, EventArgs e)
        {
            MerchModel merch = (MerchModel) cbFilterM1.EditValue;
            int merchId = (merch == null) ? Constants.EMPTY_OBJECT_ID : merch.Id;

            #region Logging
            if (_wasClick)
            {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "cbFilterM1_EditValueChanged",
                    String.Format("From MerchId = {0} to MerchId = {1}", _controller.CurrentMerch.Id, merchId),
                    LayerType.UI, null, null);
                _wasClick = !_wasClick;
            } 
            #endregion

            _controller.ChangeCurrentMerch(merchId);
            UpdateRoutes();
            UpdateButtonsStatus();
            GetSourceRoute();
            RefreshRouteProperties();
            markersControl.EnableCentering = true;
            markersControl.UpdateMapMarkers();
        }

        private void cbShowTT_EditValueChanged(object sender, EventArgs e)
        {
            var prevValue = FiltersHelper.LastShowTT;
            markersControl.ShowTT = FiltersHelper.GetShowTT((string) cbShowTT.EditValue);

            #region Logging
            if (_wasClick)
            {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "cbShowTT_EditValueChanged",
                    String.Format("From mode = {0} to mode = {1}", prevValue, markersControl.ShowTT),
                    LayerType.UI, null);
                _wasClick = !_wasClick;
            } 
            #endregion
            
            bool hidingTs = prevValue == ShowTTEnum.Cust &&
                            repositoryItemComboBoxShowTT.Items.Count < _comboBoxShowTtDataSource.Length;
            if (hidingTs)
            {
                return;
            }
            markersControl.EnableCentering = false;
            if (markersControl.ShowTT == ShowTTEnum.Cust)
            {
                _controller.CompleteM2Cashe();
            }
            markersControl.UpdateMapMarkers();
        }

        private void cbFrequency_EditValueChanged(object sender, EventArgs e)
        {
            int fromFrequency = markersControl.VisitFrequency;
          
            markersControl.VisitFrequency = FiltersHelper.GetVisitFrequency((string) cbFrequency.EditValue);

            #region Logging
            if (_wasClick)
            {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "cbFrequency_EditValueChanged",
                    String.Format("From frequency = {0} to frequency = {1}", fromFrequency,
                        markersControl.VisitFrequency),
                    LayerType.UI, null);
                _wasClick = !_wasClick;
            } 
            #endregion

            markersControl.EnableCentering = false;
            markersControl.UpdateMapMarkers();
        }

        #endregion

        private void UpdateRoutes()
        {
            if (_controller.CustomerHasNoRoutes)
            {
                tabControlRoutes.TabPages.Clear();
            }
            List<RouteWithOutletsModel> lRoutesList = _controller.GetRoutesForView();
            int lLastTabIndex = 0;
            for (int lIndex = 0; lIndex < lRoutesList.Count; lIndex++)
            {
                RouteWithOutletsModel lRoute = lRoutesList[lIndex];
                XtraTabPage lTabPage;
                if (lIndex > tabControlRoutes.TabPages.Count - 1)
                {
                    lTabPage = CreateTabPage(lRoute);
                    tabControlRoutes.TabPages.Insert(lIndex, lTabPage);
                }
                else
                {
                    lTabPage = tabControlRoutes.TabPages[lIndex];
                    lTabPage.Text = lRoute.GetRouteName(); // + '\n' + route.RouteId;
                    lTabPage.Tag = lRoute.GetRouteId();
                    _routeTabs[lTabPage].ListTT.DataSource = lRoute.Outlets;
                    _routeTabs[lTabPage].CountTt = lRoute.Outlets.Count;
                    _routeTabs[lTabPage].Distance = lRoute.Distance;
                    _routeTabs[lTabPage].DurationMinutes = lRoute.Duration;
                    ReassignEvents(lRoute, lTabPage);
                }
                lLastTabIndex = lIndex;
            }
            for (int lIndex = tabControlRoutes.TabPages.Count - 1; lIndex > lLastTabIndex; lIndex--)
            {
                XtraTabPage lTab = tabControlRoutes.TabPages[lIndex];
                _routeTabs[lTab].ListTT.DataSource = null;
                _routeTabs.Remove(lTab);
                tabControlRoutes.TabPages.Remove(lTab);
            }
            tabControlRoutes.SelectedTabPage = (tabControlRoutes.TabPages.Count == 0)
                ? null
                : tabControlRoutes.TabPages[0];
            PopulateMenuSubItems();
        }


        #region List TT actions: Drag and Drop, Double click etc.

        private void listTT_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;
            ListBoxControl lListBox = sender as ListBoxControl;
            _outletInRoute = null;
            _sourceOutletList = null;
            if (null == lListBox)
                return;
            _point = new Point(e.X, e.Y);
            int lSelectedIndex = lListBox.IndexFromPoint(_point);
            if (lSelectedIndex == -1)
            {
                _point = Point.Empty;
                return;
            }
            _sourceOutletList = lListBox.DataSource as List<OutletInRoute>;
            Debug.Assert(_sourceOutletList != null, "_sourceOutletList can not be null");
            _outletInRoute = _sourceOutletList[lSelectedIndex];
        }

        private void listTT_MouseMove(object sender, MouseEventArgs e)
        {
            ListBoxControl lListBox = sender as ListBoxControl;
            if (null == lListBox)
                return;
            if (e.Button == MouseButtons.Left)
                if ((_point != Point.Empty) &&
                    ((Math.Abs(e.X - _point.X) > SystemInformation.DragSize.Width) ||
                     (Math.Abs(e.Y - _point.Y) > SystemInformation.DragSize.Height)))
                    lListBox.DoDragDrop(_outletInRoute, DragDropEffects.Move | DragDropEffects.Copy);

            if (e.Button != MouseButtons.None)
                return;

            int lIndex = lListBox.IndexFromPoint(new Point(e.X, e.Y));
            if (lIndex != -1)
            {
                string lTip = (lListBox.GetItem(lIndex) as OutletInRoute).Outlet.OutletToolTip;
                toolTipContrListTT.ShowHint(lTip, lListBox.PointToScreen(new Point(e.X, e.Y)));
            }
            else
                toolTipContrListTT.HideHint();
        }

        private void listTT_MouseLeave(object sender, EventArgs e)
        {
            toolTipContrListTT.HideHint();
        }

        private void listTT_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
            if (e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel")
                || ((e.KeyState & (1 + 8)) == (1 + 8))) // KeyState 1 + 8 = Left mouse button + CTL
                e.Effect = DragDropEffects.Copy;
        }

        private void listTT_DragDrop(object sender, DragEventArgs e)
        {
            ListBoxControl lListBox = sender as ListBoxControl;
            if (null == lListBox)
                return;
            Point lNewPoint = new Point(e.X, e.Y);
            lNewPoint = lListBox.PointToClient(lNewPoint);
            int lSelectedIndex = lListBox.IndexFromPoint(lNewPoint);
            List<OutletInRoute> lOutletList = lListBox.DataSource as List<OutletInRoute>;
            if (lSelectedIndex > lOutletList.Count - 1)
                return;

            if (e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel"))
            {
                OutletModel lOutlet =
                    e.Data.GetData("SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel") as OutletModel;
                if (!_controller.InsertOutletToRoute(lSelectedIndex, lOutlet))
                    MessageBox.Show(Resources.StrOutletAlreadyOnThisRoute,
                        Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    UpdateButtonsStatus();
                    GetSourceRoute();
                    RefreshRouteProperties();
                    markersControl.EnableCentering = false;
                    markersControl.UpdateMarkersForRoute(_controller.CurrentRoute);
                    markersControl.UpdateMapRoute();
                }
            }
            else if (e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute"))
            {
                OutletInRoute lOutletInRoute =
                    e.Data.GetData("SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute") as OutletInRoute;
                int lIndex = lOutletList.IndexOf(lOutletInRoute);
                if (lIndex == lSelectedIndex) // nothing to do
                    return;
                if (_sourceOutletList != null)
                    _sourceOutletList.Remove(lOutletInRoute);
                if (lSelectedIndex < 0)
                {
                    lOutletList.Add(lOutletInRoute);
                    lSelectedIndex = lOutletList.Count - 1;
                }
                else
                    lOutletList.Insert(lSelectedIndex, lOutletInRoute);
                lListBox.SelectedIndex = lSelectedIndex;
                lOutletInRoute.MakeModified();
                _controller.CurrentRoute.RenumberOutlets();

                _controller.CurrentRoute.HasPreDistance = false; // Optimization RMT

                UpdateButtonsStatus();
                GetSourceRoute();
                RefreshRouteProperties();
                markersControl.EnableCentering = false;
                markersControl.UpdateMarkersForRoute(_controller.CurrentRoute);
                markersControl.UpdateMapRoute();
            }

            //clear after drop
            _sourceOutletList = null;
            _outletInRoute = null;
        }

        private void listTT_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListBoxControl lListBox = sender as ListBoxControl;
            if (null == lListBox)
                return;
            //Point lNewPoint = lListBox.PointToClient(e.Location);
            int lSelectedIndex = lListBox.IndexFromPoint(e.Location);
            List<OutletInRoute> lOutletList = lListBox.DataSource as List<OutletInRoute>;
            Debug.Assert(lOutletList != null, "OutletList can not be null. (Source: ListBoxControl.DataSource)");
            if (lSelectedIndex > lOutletList.Count - 1)
                return;
            if (lSelectedIndex < 0)
                return;
            OutletModel lOutlet = lOutletList[lSelectedIndex].Outlet;
            markersControl.CenterMapByTt(lOutlet);
        }

        #endregion

        private void imageListBox_MouseMove(object sender, MouseEventArgs e)
        {
            ImageListBoxControl lListBox = sender as ImageListBoxControl;
            if (null == lListBox)
                return;

            if (e.Button == MouseButtons.None)
            {
                int lIndex = lListBox.IndexFromPoint(new Point(e.X, e.Y));
                if (lIndex != -1)
                {
                    string lTip = (lListBox.GetItem(lIndex) as OutletModel).MarkerToolTip;
                    toolTipContrListTT.ShowHint(lTip, lListBox.PointToScreen(new Point(e.X, e.Y)));
                }
                else
                    toolTipContrListTT.HideHint();
            }
        }


        private void tabPage_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
            if (e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel")
                || ((e.KeyState & (1 + 8)) == (1 + 8))) // KeyState 1 + 8 = Left mouse button + CTL
                e.Effect = DragDropEffects.Copy;
        }

        private void tabPage_DragDrop(object sender, DragEventArgs e)
        {
            XtraTabControl lTabControl = sender as XtraTabControl;
            if (lTabControl == null)
                return;

            XtraTabHitInfo lHitInfo = lTabControl.CalcHitInfo(lTabControl.PointToClient(new Point(e.X, e.Y)));
            if (lHitInfo.Page == null)
                return;
            long lRouteId = Convert.ToInt64(lHitInfo.Page.Tag);
            if (lRouteId == 0)
                return;
            ListTTControl lListTT = _routeTabs[lHitInfo.Page];
//            List<OutletInRoute> lDestOutletsList = lListTT.ListTT.DataSource as List<OutletInRoute>;
//            if (lDestOutletsList == null)
//                return;
            if (e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute"))
            {
                // Cases: drop Outlet from list of TT on Tab: add Outlet to route
                OutletInRoute lOutletInRoute =
                    e.Data.GetData("SoftServe.Reports.RouteManagement.Model.Outlet.OutletInRoute") as OutletInRoute;
                Debug.Assert(lOutletInRoute != null, "OutletInRoute cannot be null");
                if (!_controller.AddOutletToRoute(lRouteId, lOutletInRoute.Outlet))
                {
                    MessageBox.Show(Resources.StrOutletAlreadyOnThisRoute, Resources.StrMsgBoxTitleRouteManagement,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if ((e.Effect & DragDropEffects.Move) == DragDropEffects.Move)
                    {
                        //if (e.KeyState & )
                        // move Outlet on Tab: need to delete from current route
                        lListTT.ListTT.BeginUpdate();
                        try
                        {
                            _controller.RemoveOutletFromRoute(lOutletInRoute);
                        }
                        finally
                        {
                            lListTT.ListTT.EndUpdate();
                        }

                        UpdateButtonsStatus();
                        markersControl.EnableCentering = false;
                        markersControl.UpdateMarkerForOutlet(lOutletInRoute.Outlet, -1);
                        GetSourceRoute();
                        RefreshRouteProperties();
                        markersControl.UpdateMarkersForRoute(_controller.CurrentRoute);
                        markersControl.UpdateMapRoute();
                    }
                    else
                    {
                        // copy Outlet on Tab: just refresh marker
                        markersControl.EnableCentering = false;
                        markersControl.UpdateMarkerForOutlet(lOutletInRoute.Outlet, lOutletInRoute.OlNumber);
                        //markersControl.UpdateMarkersForRoute(_controller.CurrentRoute);
                        markersControl.UpdateMapRoute();
                    }
                }
            }
            else if (e.Data.GetDataPresent("SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel"))
            {
                // Cases: drop Outlet from Map or Panel: copy Outlet to route
                OutletModel lOutlet =
                    e.Data.GetData("SoftServe.Reports.RouteManagement.Model.Outlet.OutletModel") as OutletModel;
                if (!_controller.AddOutletToRoute(lRouteId, lOutlet))
                    MessageBox.Show(Resources.StrOutletAlreadyOnThisRoute, Resources.StrMsgBoxTitleRouteManagement,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    //UpdateButtonsStatus();
                    //GetSourceRoute();
                    //RefreshRouteProperties();
                    markersControl.EnableCentering = false;
                    int lNumber = _controller.CurrentRoute.GetOutletNumber(lOutlet);
                    markersControl.UpdateMarkerForOutlet(lOutlet, lNumber);
                    //markersControl.UpdateMarkersForRoute(lRouteId);
                    markersControl.UpdateMapRoute();
                }
            }
        }

        private void tabControlRoutes_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {

            if (null == tabControlRoutes.SelectedTabPage)
                return;
            long lRouteId = (long) tabControlRoutes.SelectedTabPage.Tag;
            if (_controller.CurrentRoute.RouteId == lRouteId)
                return;

            #region Logging
            
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "tabControlRoutes_SelectedPageChanged",
                    String.Format("From routeId = {0} to routeId = {1}", _controller.CurrentRoute.RouteId, lRouteId),
                    LayerType.UI, null);
               
            #endregion

            markersControl.EnableCentering = true;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                _controller.ChangeCurrentRoute(lRouteId);
                PopulateMenuSubItems();
                UpdateButtonsStatus();
                GetSourceRoute();
                RefreshRouteProperties();
                markersControl.UpdateMapMarkers();
                markersControl.ClearOutletsCache();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void PopulateMenuSubItems()
        {
            btnCopyRoute.ClearLinks();
            btnCopyRoute.ItemLinks.Clear();
            if (_controller.CustomerHasNoRoutes)
            {
                return;
            }
            for (int lI = 0; lI < _controller.CurrentM1.Routes.Count; lI++)
            {
                RouteWithOutletsModel lRouteWithOutlets = _controller.CurrentM1.Routes[lI];
                if (lRouteWithOutlets.RouteId == _controller.CurrentRoute.RouteId ||
                    lRouteWithOutlets.RouteId == 0)
                    continue;
                BarButtonItem lBtnRouteItem = new BarButtonItem(barManager, lRouteWithOutlets.RouteName);
                lBtnRouteItem.Tag = lRouteWithOutlets.RouteId;
                lBtnRouteItem.ButtonStyle = BarButtonStyle.Default;
                lBtnRouteItem.PaintStyle = BarItemPaintStyle.CaptionInMenu;
                lBtnRouteItem.ItemClick += barButtonItem_ItemClick;
                btnCopyRoute.AddItem(lBtnRouteItem);
            }
        }

        public void RefreshRouteProperties()
        {
            if (_controller.CustomerHasNoRoutes)
            {
                return;
            }
            ListTTControl lListTT = _routeTabs[tabControlRoutes.SelectedTabPage];
            lListTT.CountTt = _controller.CurrentRoute.CountTt;
            if (_controller.CurrentRoute.GetRouteId() <= 0 || _controller.CurrentRoute.CountTt <= 0)
            {
                lListTT.Distance = -1;
                lListTT.DurationMinutes = -1;
            }
            else
            {
                lListTT.Distance = _controller.CurrentRoute.Distance;
                lListTT.DurationMinutes = _controller.CurrentRoute.Duration;
            }
            lListTT.Refresh();
            lListTT.ListTT.Refresh();
            //lListTT.Invalidate(true);
        }

        public void GetSourceRoute()
        {
            markersControl.RoutePoints.Clear();
            if (_controller.CustomerHasNoRoutes || _controller.CurrentRoute.RouteId <= 0 ||
                _controller.CurrentRoute.CountTt <= 0)
                return;

            RouteResult lRouteResult = _controller.GetSourceRoute(true);
            if (null == lRouteResult || lRouteResult.SourceRoute == null)
                return;

            for (int lI = 0; lI < lRouteResult.SourceRoute.Length; lI++)
            {
                GeoPoint[] lPointArray = lRouteResult.SourceRoute[lI];
                if (lPointArray == null)
                    continue;
                for (int lJ = 0; lJ < lPointArray.Length; lJ++)
                {
                    GeoPoint lPoint = lPointArray[lJ];
                    markersControl.RoutePoints.Add(new PointLatLng(lPoint.Lat, lPoint.Lon));
                }
            }
        }

        public void RefreshZoom()
        {
            //barStaticText.Caption = markersControl.ZoomPercentStr;
        }

        #region Route toolbar actions

        private void btnDeleteRoute_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult lDialogResult = MessageBox.Show("Вы действительно хотите очистить маршрут?",
                "Route Management", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (lDialogResult == DialogResult.Cancel)
            {
                return;
            }

            #region Logging
            WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "btnDeleteRoute_ItemClick",
                String.Format("RouteId = {0}; RouteName = {1}; MerchId = {2}",
                _controller.CurrentRoute.RouteId, _controller.CurrentRoute.RouteName, _controller.CurrentRoute.MerchId),
                LayerType.UI, null);
            # endregion

            XtraTabPage lTabPage = tabControlRoutes.SelectedTabPage;
            if (!_routeTabs.ContainsKey(lTabPage))
                return;
            ListTTControl lListTT = _routeTabs[lTabPage];
            lListTT.ListTT.BeginUpdate();
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                _controller.ClearRoute();
                GetSourceRoute();
                RefreshRouteProperties();
                markersControl.EnableCentering = false;
                markersControl.UpdateMapMarkers();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                lListTT.ListTT.EndUpdate();
                UpdateButtonsStatus();
            }
        }

        private void btnReverseRoute_ItemClick(object sender, ItemClickEventArgs e)
        {
            #region Logging
            WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "btnReverseRoute_ItemClick",
                String.Format("RouteId = {0}; RouteName = {1}; MerchId = {2}",
                _controller.CurrentRoute.RouteId, _controller.CurrentRoute.RouteName, _controller.CurrentRoute.MerchId),
                LayerType.UI, null);
            # endregion

            XtraTabPage lTabPage = tabControlRoutes.SelectedTabPage;
            if (!_routeTabs.ContainsKey(lTabPage))
                return;
            ListTTControl lListTT = _routeTabs[lTabPage];
            lListTT.ListTT.BeginUpdate();
            try
            {
                _controller.CurrentRoute.HasPreDistance = false; //Optimization RMT
                _controller.CurrentRoute.HasPreOptDistance = false; //Optimization RMT
                _controller.CurrentRoute.ReverseRoute();
                GetSourceRoute();
                RefreshRouteProperties();
                markersControl.EnableCentering = false;
                markersControl.UpdateMarkersForRoute(_controller.CurrentRoute);
                markersControl.UpdateMapRoute();
            }
            finally
            {
                lListTT.ListTT.EndUpdate();
            }
        }

        private void btnOptimizeRoute_ItemClick(object sender, ItemClickEventArgs e)
        {
                #region Logging
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "btnOptimizeRoute_ItemClick",
                    String.Format("RouteId = {0}; RouteName = {1}; MerchId = {2}",
                    _controller.CurrentRoute.RouteId, _controller.CurrentRoute.RouteName, _controller.CurrentRoute.MerchId),
                    LayerType.UI, null);
                # endregion

                XtraTabPage lTabPage = tabControlRoutes.SelectedTabPage;
            if (!_routeTabs.ContainsKey(lTabPage))
                return;
            ListTTControl lListTT = _routeTabs[lTabPage];
            lListTT.ListTT.BeginUpdate();
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                RouteResult lRouteResult = null;
                lRouteResult = _controller.GetRouteResultsFromService(_controller.CurrentRoute, true, false);

                if (lRouteResult == null)
                    return;
                if (lRouteResult.ErrorCode < 0)
                    return;

                RefreshRouteProperties();

                // fill route points
                markersControl.RoutePoints.Clear();
                foreach (GeoPoint lPoint in lRouteResult.OptimizedRoute)
                {
                    markersControl.RoutePoints.Add(new PointLatLng(lPoint.Lat, lPoint.Lon));
                }
                markersControl.EnableCentering = false;
                markersControl.UpdateMarkersForRoute(_controller.CurrentRoute);
                markersControl.UpdateMapRoute();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                lListTT.ListTT.EndUpdate();
            }
        }

        private void btnSaveRoutes_ItemClick(object sender, ItemClickEventArgs e)
        {
            //ViewToData();
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                #region Logging
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "btnSaveRoutes_ItemClick",
                    String.Format("Sv_Id={0}",
                    _controller.CurrentSupervisor.Id),
                    LayerType.UI, null);
                # endregion

                _controller.SaveData();
                _controller.CurrentM2.Apply();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnOpenRoutes_ItemClick(object sender, ItemClickEventArgs e)
        {
            #region Logging
            WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "btnOpenRoutes_ItemClick",
                String.Format("Sv_Id={0}",
                _controller.CurrentSupervisor.Id),
                LayerType.UI, null);
            # endregion

            _controller.LoadRouteSet();
        }

        private void barButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null == e.Item.Tag)
                return;
            long lRouteId = (long) e.Item.Tag;
            CopyConfirmDialog lDialog = new CopyConfirmDialog();
            DialogResult lDialogResult = lDialog.ShowDialog();
            if (lDialogResult == DialogResult.Cancel)
                return;

            bool lOverwrite = lDialogResult == DialogResult.OK;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                _controller.CopyRoute(lOverwrite, lRouteId);
                markersControl.EnableCentering = false;
                markersControl.UpdateMarkersForRoute(_controller.CurrentRoute);
                markersControl.UpdateMapRoute();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        public void UpdateButtonsStatus()
        {
            bool lEnabled = !_controller.CustomerHasNoRoutes && _controller.CurrentRoute.RouteId > 0 &&
                            _controller.CurrentRoute.CountTt > 0;
            Boolean lEnabledCopy = !_controller.CustomerHasNoRoutes && _controller.CurrentRoute.RouteId == 0 &&
                                   _controller.CurrentRoute.CountTt > 0;

            btnOptimizeRoute.Enabled = lEnabled;
            btnReverseRoute.Enabled = lEnabled;
            btnCopyRoute.Enabled = lEnabledCopy || lEnabled;
            btnDeleteRoute.Enabled = lEnabled;
            btnSaveRoutes.Enabled = btnOpenRoutes.Enabled = !_controller.CustomerHasNoRoutes;
        }

        private void WccpUIControl_Resize(object sender, EventArgs e)
        {
            // workaround for stupid DevEx ListBoxControl, bars and GMap
            //int lSplitPos = splitContainer.SplitterPosition;
            splitContainer.SplitterPosition += 2;
            splitContainer.SplitterPosition -= 2;

            markersControl.Refresh();
            barDockMapFilter.Refresh();
            barMapTools.Invalidate();
//            barMapTools.Reset();
            barFilter.Invalidate();
//            barMapFilter.Reset();
        }

        private void btnZoomIn_ItemClick(object sender, ItemClickEventArgs e)
        {
            markersControl.ZoomIn();
            //barStaticText.Caption = markersControl.ZoomPercentStr;
        }

        private void btnZoomOut_ItemClick(object sender, ItemClickEventArgs e)
        {
            markersControl.ZoomOut();
            //barStaticText.Caption = markersControl.ZoomPercentStr;
        }

        private void cbFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            _wasClick = true;
        }
    }
}

