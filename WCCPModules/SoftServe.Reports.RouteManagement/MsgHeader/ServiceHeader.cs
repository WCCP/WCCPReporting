﻿using System.Runtime.Serialization;

namespace SoftServe.Reports.RouteManagement.MsgHeader
{
    [DataContract]
    public class ServiceHeader
    {
        [DataMember]
        public string PCName { get; set; }

        [DataMember]
        public string PCUserLogin { get; set; }

        [DataMember]
        public string SQLUserLogin { get; set; }

        [DataMember]
        public string HostName { get; set; }

        [DataMember]
        public long RouteId { get; set; }
    }
}
