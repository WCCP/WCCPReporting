﻿namespace SoftServe.Reports.RouteManagement.Controller {
    public enum ShowTTEnum {
        Unknown = -1,
        Route = 0,
        M1 = 1,
        M2 = 2,
        Cust = 3
    }
}