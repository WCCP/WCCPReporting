﻿using System;
using System.Linq;
using SoftServe.Reports.RouteManagement.Model.Merch;
using SoftServe.Reports.RouteManagement.Model.Route;

namespace SoftServe.Reports.RouteManagement.Controller {
    public static class FiltersHelper {
        private const string AnyFrequencyStr = "Любой";
        public static int AnyFrequency = int.MaxValue;

        // Filter settings
        public static ShowTTEnum LastShowTT = ShowTTEnum.M2;
        public static int LastVisitFrequency = AnyFrequency;
        public static M1Model LastM1;
        public static RouteWithOutletsModel LastRoute;

        // Global settings
        public static bool AllowHtmlText = true;
        public static string ValueColor = "#FF8C00"; // DarkOrange;
        public static bool IsDebug = false;

        public static ShowTTEnum GetShowTT(string value) {
            LastShowTT = ShowTTEnum.Unknown;
            if (value == "ТС")
                LastShowTT = ShowTTEnum.Cust;
            if (value == "М2")
                LastShowTT = ShowTTEnum.M2;
            if (value == "М1")
                LastShowTT = ShowTTEnum.M1;
            if (value == "Маршрут")
                LastShowTT = ShowTTEnum.Route;
            
            return LastShowTT;
        }

        public static int GetVisitFrequency(string value) {
            LastVisitFrequency = (value == AnyFrequencyStr) ? AnyFrequency : Convert.ToInt32(value);
            return LastVisitFrequency;
        }

        public static RouteWithOutletsModel GetOtherTTRoute()
        {
            return LastM1 == null ? null : LastM1.Routes.FirstOrDefault(r => r.RouteId == 0);
        }
    }
}