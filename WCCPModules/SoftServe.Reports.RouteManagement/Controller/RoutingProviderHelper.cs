﻿using System;
using System.Collections.Generic;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using SoftServe.Reports.RouteManagement.Model.Route;
using SoftServe.Reports.RouteManagement.WcfRouterService;
using SoftServe.Reports.RouteManagement.MsgHeader;
using System.Configuration;
using System.Data.SqlClient;
using System.ServiceModel;
using Logica.Reports.DataAccess;
using SoftServe.Core.Common.Logging;
using WccpReporting;

namespace SoftServe.Reports.RouteManagement.Controller {
    public static class RoutingProviderHelper {
        private static string _lastMsg = String.Empty;
        private static int _lastErrorCode = 0;
        private static EndpointAddress serviceAddress_Ru;
        private static EndpointAddress serviceAddress_Ua;
       
        public static int LastErrorCode {
            get { return _lastErrorCode; }
        }

        public static string LastErrorMsg {
            get { return _lastMsg; }
        }
        
        public static string GetLastErrorMsg() {
            return _lastMsg;
        }

        static RoutingProviderHelper()
        {
            if (serviceAddress_Ru == null)
            {
                serviceAddress_Ru = new EndpointAddress(ConfigurationManager.AppSettings["RUServiceRMT"]);
            }
            if (serviceAddress_Ua == null)
            {
                serviceAddress_Ua = new EndpointAddress(ConfigurationManager.AppSettings["UAServiceRMT"]);
            }
            SqlConnectionStringBuilder conInfo = new SqlConnectionStringBuilder(HostConfiguration.DBSqlConnection);
            ClientContext.HostName = conInfo.DataSource;
            ClientContext.PCName = Environment.MachineName;
            ClientContext.PCUserLogin = Environment.UserName;
            ClientContext.SQLUserLogin = conInfo.UserID;
             


        }

        public static RouteResult BuildRoute(RouteParams param, long routeId) {
            _lastMsg = String.Empty;
            _lastErrorCode = 0;
            RouteResult lRouteResult = null;

            ClientContext.RouteId = routeId;

            try
            {
                RouterServiceClient lServiceClient = new RouterServiceClient();
                lServiceClient.ChannelFactory.Endpoint.Behaviors.Add(new CustomBehavior());
                
                // If there is no specified endpoint address in App.Config, then address will be specifying in runtime according to the country.
                if (lServiceClient.Endpoint.Address == null)
                {
                    lServiceClient.Endpoint.Address = param.IsRu
                        ? serviceAddress_Ru
                        : serviceAddress_Ua;
                }

                try
                {
                    lRouteResult = lServiceClient.BuildRoute(param);
                }
                finally
                {
                    lServiceClient.Close();
                }
                if (lRouteResult == null)
                    return null;

                _lastErrorCode = lRouteResult.ErrorCode;
                _lastMsg = String.Empty;
                if (lRouteResult.ErrorCode < 0)
                {
                    _lastMsg = String.Format("Оптимизация завершена с ошибкой.\nКод: {0}\n{1}.", lRouteResult.ErrorCode,
                        lRouteResult.ErrorMessage);
                    WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "BuildRoute()", "RoutingProviderHelper", LayerType.UI, null, _lastMsg);
                }
        }
            catch (Exception lException) {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "BuildRoute()", "RoutingProviderHelper", LayerType.UI, null, lException.Message);
                _lastMsg = lException.Message;
                _lastErrorCode = -1;
                throw;
            }
            return lRouteResult;
        }

        public static void BuildRouteParams(RouteWithOutletsModel route, out RouteParams routeParams,
                                             out List<OutletInRoute> sourceOutlets, out List<OutletInRoute> withoutCoordList) {
            routeParams = new RouteParams();
            routeParams.IsRound = false;
            int lCount = route.Outlets.Count;
            if (route.IsStartMarker) {
                lCount++;
                routeParams.IsRound = true;
            }

            List<GeoPoint> lPointList = new List<GeoPoint>(lCount);
            if (route.IsStartMarker) {
                GeoPoint lGeoPoint = new GeoPoint();
                lGeoPoint.Lat = route.StartMarker.Position.Lat;
                lGeoPoint.Lon = route.StartMarker.Position.Lng;
                lPointList.Add(lGeoPoint);
            }

            sourceOutlets = new List<OutletInRoute>(route.Outlets.Count);
            withoutCoordList = new List<OutletInRoute>();
            for (int lI = 0; lI < route.Outlets.Count; lI++) {
                OutletInRoute lOutletInRoute = route.Outlets[lI];
                if (!lOutletInRoute.Outlet.IsMarker) {
                    withoutCoordList.Add(lOutletInRoute);
                    continue;
                }
                GeoPoint lGeoPoint = new GeoPoint();
                lGeoPoint.Lat = (double) lOutletInRoute.Outlet.Lat;
                lGeoPoint.Lon = (double) lOutletInRoute.Outlet.Lon;
                lPointList.Add(lGeoPoint);
                sourceOutlets.Add(lOutletInRoute);
            }
            routeParams.Points = lPointList.ToArray();
        }
    }
}