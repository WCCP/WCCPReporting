﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using SoftServe.Core.Common.Controller;
using SoftServe.Core.Common.Logging;
using SoftServe.Core.Common.View;
using SoftServe.Reports.RouteManagement.DataAccess;
using SoftServe.Reports.RouteManagement.Model.Merch;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using SoftServe.Reports.RouteManagement.Model.Route;
using SoftServe.Reports.RouteManagement.Model.Supervisor;
using SoftServe.Reports.RouteManagement.Properties;
using SoftServe.Reports.RouteManagement.View;
using SoftServe.Reports.RouteManagement.WcfRouterService;
using WccpReporting;

namespace SoftServe.Reports.RouteManagement.Controller {
    public class RouteManagementController : BaseViewController, IRouteManagementController {
        #region Fields

        private IRMTView _view;

        private Dictionary<long, OutletModel> _OLCache = new Dictionary<long, OutletModel>(1000);
        private Dictionary<int, M1Model> _m1Cache = new Dictionary<int, M1Model>(25);
        private Dictionary<int, M2Model> _m2Cache = new Dictionary<int, M2Model>(5);
        private Dictionary<int, List<SupervisorModel>> _custIdSupervisors = new Dictionary<int, List<SupervisorModel>>();

        private List<CustomerSimpleModel> _customersList = new List<CustomerSimpleModel>();
        private List<SupervisorModel> _supervisorsList = new List<SupervisorModel>();
        private List<MerchModel> _merchList = new List<MerchModel>();
        private List<RouteModel> _routeList = new List<RouteModel>();
        private List<RouteWithOutletsModel> _routeWithOutletsList = new List<RouteWithOutletsModel>(4);
        private List<RouteSet> _routeSetList = new List<RouteSet>();

        private CustomerSimpleModel _currentCustomer;
        private SupervisorModel _currentSupervisor;
        private MerchModel _currentMerch;
        private M2Model _currentM2;
        private M1Model _currentM1;
        private RouteWithOutletsModel _currentRoute;

        #endregion

        #region Implementation of IViewController

        public RouteManagementController(IRMTView view) : base(view) {
            _view = view;
        }

        protected override IView CreateView() {
            return _view ?? new WccpUIControl();
        }

        public override bool Validate() {
            return true;
        }

        public override void LoadData(bool reload = false) {
            LoadCustomers();
            LoadSupervisors();
            LoadRouteView(false);
        }

        public override void SaveData() {
            LoadRouteSetList(_currentM2.Id);

            OpenSaveRouteSet lSaveDialog = new OpenSaveRouteSet();
            //set save properties
            lSaveDialog.SetOptions(true, _currentM2.Id);
            lSaveDialog.RouteSetList = _routeSetList;
            DialogResult lDialogResult = lSaveDialog.ShowDialog();
            if (lDialogResult == DialogResult.Cancel)
                return;

            Cursor.Current = Cursors.WaitCursor;
            try {
                string lNameOfRouteSet = lSaveDialog.Comment;
                bool lIsLoad = lSaveDialog.IsLoad;
                int lRouteSetId = lSaveDialog.RouteSetId;

                TimeParameter tp = WccpLogger.InitTimeParameter();

                MemoryStream lStream = new MemoryStream(1024 * 10);
                XmlWriterSettings lSettings = new XmlWriterSettings();
                lSettings.OmitXmlDeclaration = true;
                XmlWriter lWriter = XmlWriter.Create(lStream, lSettings);
                lWriter.WriteStartElement("rows");

                MemoryStream lStreamRoute = new MemoryStream(1024 * 10);
                XmlWriter lWriterRoute = XmlWriter.Create(lStreamRoute, lSettings);
                lWriterRoute.WriteStartElement("rows");

                int lRoutesOutletsCount = 0;
                int lRoutesCount = 0;

                for (int lI = 0; lI < _currentM2.M1List.Count; lI++) {
                    M1Model lM1 = _currentM2.M1List[lI];
                    for (int lJ = 0; lJ < lM1.Routes.Count; lJ++) {
                        RouteWithOutletsModel lRoute = lM1.Routes[lJ];
                        int lTypeOfRoute = lRoute.GetRouteId() > 0 ? 1 : lRoute.RouteId == 0 ? 2 : 3;
                        if (lTypeOfRoute != 1)
                            continue;
                        if (lRoute.RouteId > 0 && lRoute.CountTt > 0 && !lRoute.IsOptimized)
                            GetRouteDistances(lRoute);
                        lWriterRoute.WriteStartElement("row");
                        lWriterRoute.WriteAttributeString("Route_ID", lRoute.GetRouteId().ToString(CultureInfo.InvariantCulture));
                        lWriterRoute.WriteAttributeString("Distance", lRoute.Distance.ToString(CultureInfo.InvariantCulture));
                        lWriterRoute.WriteAttributeString("OptDistance", lRoute.OptDistance.ToString(CultureInfo.InvariantCulture));
                        lWriterRoute.WriteEndElement();
                        for (int lK = 0; lK < lRoute.Outlets.Count; lK++) {
                            OutletInRoute lOutlet = lRoute.Outlets[lK];
                            lWriter.WriteStartElement("row");
                            lWriter.WriteAttributeString("Route_ID", lRoute.GetRouteId().ToString(CultureInfo.InvariantCulture));
                            lWriter.WriteAttributeString("Merch_ID", lM1.GetId().ToString(CultureInfo.InvariantCulture));
                            lWriter.WriteAttributeString("Ol_Id", lOutlet.Outlet.Id.ToString(CultureInfo.InvariantCulture));
                            lWriter.WriteAttributeString("Ol_Number", lK.ToString(CultureInfo.InvariantCulture));
                            //lWriter.WriteAttributeString("TypeOfRoute", lTypeOfRoute.ToString(CultureInfo.InvariantCulture));
                            lWriter.WriteEndElement();
                            lRoutesOutletsCount++;
                        }
                        lRoutesCount++;
                        //TODO: calc progress
                        _view.ProcessEvents();
                        //_view.UpdateProgress();
                    }
                }

                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                lWriterRoute.WriteFullEndElement();
                lWriterRoute.Flush();
                lStreamRoute.Position = 0;

                SqlXml lXmlData = new SqlXml(lStream);
                SqlXml lXmlDataRoute = new SqlXml(lStreamRoute);

                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "SaveData()", string.Format("RouteSetId={0}; Sv_id={1}; IsLoud={2}; RoutesCount={3}; RoutOutlCount = {4}",
                    lRouteSetId,_currentM2.Id,lIsLoad,lRoutesCount, lRoutesOutletsCount), LayerType.UI, tp);
               
                try
                {
                    DataProvider.SaveRouteSet(lRouteSetId, _currentM2.Id, DateTime.Now, lNameOfRouteSet, _currentM2.CustId, lIsLoad, lXmlData, lXmlDataRoute);
                }
                catch (Exception lException)
                {
                    WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "SaveData()", string.Format("RouteSetId={0}; Sv_id={1}; IsLoud={2}; RoutesCount={3}; RoutOutlCount = {4}",
                    lRouteSetId, _currentM2.Id, lIsLoad, lRoutesCount, lRoutesOutletsCount), LayerType.UI, tp, lException.Message);

                    MessageBox.Show("Ошибка при сохранении маршрутов!\n" + lException.Message,
                        Resources.StrMsgBoxTitleRouteManagement,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }

        public void LoadRouteSetList(int svId) {
            DataTable lDataTable = DataProvider.GetRouteSet(svId);
            _routeSetList.Clear();
            foreach (DataRow lRow in lDataTable.Rows) {
                RouteSet lRouteSet = new RouteSet((int) lRow[SqlConstants.RouteSetIdFieldName]);
                lRouteSet.SetSupervisorId((int) lRow[SqlConstants.SupervisorIdFieldName]);
                lRouteSet.SetSupervisorName((string) lRow[SqlConstants.SupervisorNameFieldName]);
                lRouteSet.SetRouteSetDate((DateTime) lRow[SqlConstants.RouteSetDateFieldName]);
                lRouteSet.SetComment((string) lRow[SqlConstants.CommentFieldName]);
                lRouteSet.SetIsLoad((bool) lRow[SqlConstants.IsLoadFieldName]);
                lRouteSet.Apply();
                _routeSetList.Add(lRouteSet);
            }
        }

        #endregion

        #region Load methods

        private void LoadCustomers() {
            DataTable lDataTable = DataProvider.GetCustomers();
            _customersList.Clear();
            _currentCustomer = null;
            _custIdSupervisors.Clear();
            foreach (DataRow lRow in lDataTable.Rows) {
                CustomerSimpleModel lCustomer = new CustomerSimpleModel((int)lRow[SqlConstants.CustIdFieldName]);
                lCustomer.SetName((string)lRow[SqlConstants.CustNameFieldName]);
                _customersList.Add(lCustomer);
            }
            if (_customersList.Count <= 0)
                return;
            _currentCustomer = _customersList[0];
            FiltersHelper.LastShowTT = ShowTTEnum.M2;
        }

        private void LoadSupervisors() {
            _m2Cache.Clear();
            _supervisorsList.Clear();
            _currentSupervisor = null;
            if (_currentCustomer == null)
                return;
            DataTable dataTable = DataProvider.GetSupervisors(_currentCustomer.Id);
            CheckCustomerHasNoRoutes(dataTable);

            foreach (DataRow lRow in dataTable.Rows)
            {
                var supervisorModel = CreateSupervisorModel(lRow);
                _supervisorsList.Add(supervisorModel);
            }
            if (_supervisorsList.Count <= 0)
                return;
            if (_currentCustomer.Id == Constants.CUST_ID_ALL)
            {
                _supervisorsList = _supervisorsList.Distinct().ToList();
            }
            _currentSupervisor = _supervisorsList[0];
            CreateOrGetFromCacheM2(_currentSupervisor, out _currentM2);

            //fill Cust_id and Supervisors dictionary
            _custIdSupervisors[_currentCustomer.Id] = _supervisorsList;
        }

        private void CheckCustomerHasNoRoutes(DataTable supervisorsDataTable)
        {
            if (supervisorsDataTable.Rows.Count == 0)
            {
                MessageBox.Show(Resources.CustomerWithoutRoutes, Resources.StrMsgBoxTitleRouteManagement,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoadRouteView(bool loading) {
            try {
                if (_currentSupervisor == null)
                    return;
                if (loading)
                    ClearM2(_currentM2);

                if (_currentM2.IsLoaded) {
                    SetCurrentValues();
                    return;
                }

                List<SupervisorModel> supervisors;
                if (loading || FiltersHelper.LastShowTT != ShowTTEnum.Cust)
                {
                    supervisors = new List<SupervisorModel> { _currentSupervisor };
                }
                else
                {
                    supervisors = _custIdSupervisors[_currentSupervisor.CustId];
                }

                foreach (var sm in supervisors)
                {
                    FillM2Model(sm);
                }

                if (_currentM2.IsLoaded)
                    SetCurrentValues();
            }
            catch (Exception lException) {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "LoadRouteView()", "RouteManagementController",LayerType.UI, null,lException.Message);
                MessageBox.Show("Ошибка при загрузке текущих маршрутов!\n" + lException.Message,
                    Resources.StrMsgBoxTitleRouteManagement,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FillM2Model(SupervisorModel sm)
        {
            M2Model m2 = null;

            // TypeOfRoute = 1 - regular routes
            int typeOfRoute = 1;
            DataTable regularRoutes = DataProvider.GetRouteView(typeOfRoute, DateTime.Today, _currentSupervisor.CustId,
                sm.Id, 2);
            // TypeOfRoute = 2 - not on the route
            typeOfRoute = 2;
            DataTable notOnTheRoute = DataProvider.GetRouteView(typeOfRoute, DateTime.Today, _currentSupervisor.CustId,
                sm.Id, 2);

            typeOfRoute = 1;
            DataSetToM2Model(sm, regularRoutes, typeOfRoute, out m2);

            typeOfRoute = 2;
            DataSetToM2Model(sm, notOnTheRoute, typeOfRoute, out m2);

            CreateEmptyRoutes(sm, out m2);

            m2.Apply();
            m2.IsLoaded = true;
        }

        private void LoadRouteSetDetail(M2Model m2, int routeSetId) {
            try {
                // clear cache and models for M2
                ClearM2(m2);

                // TypeOfRoute = 1 - regular routes
                short lTypeOfRoute = 1;
                DataTable lDataTable = DataProvider.GetRouteSetDetail(routeSetId, lTypeOfRoute);
                DataSetToM2Model(_currentSupervisor, lDataTable, lTypeOfRoute, out m2);
    
                // TypeOfRoute = 2 - not on the route
                lTypeOfRoute = 2;
                lDataTable = DataProvider.GetRouteSetDetail(routeSetId, lTypeOfRoute);
                DataSetToM2Model(_currentSupervisor, lDataTable, lTypeOfRoute, out m2);
    
                CreateEmptyRoutes(_currentSupervisor, out m2);
    
                m2.M1List.Sort();
                m2.Apply();
                m2.IsLoaded = true;
    
                if (_currentM2.IsLoaded)
                    SetCurrentValues();
            }
            catch (Exception lException) {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "LoadRouteSetDetail()", "RouteManagementController", LayerType.UI, null, lException.Message);
                MessageBox.Show("Ошибка при загрузке сохранённых маршрутов!\n" + lException.Message,
                    Resources.StrMsgBoxTitleRouteManagement,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearM2(M2Model m2) {
            m2.IsLoaded = false;
            for (int lI = 0; lI < m2.M1List.Count; lI++) {
                M1Model lM1Model = m2.M1List[lI];
                if (_m1Cache.ContainsKey(lM1Model.Id))
                    _m1Cache.Remove(lM1Model.Id);
                ClearOLCache(lM1Model);
            }
            m2.M1List.Clear();
        }

        private void ClearOLCache(M1Model m1) {
            for (int lI = 0; lI < m1.Routes.Count; lI++) {
                RouteWithOutletsModel lRoute = m1.Routes[lI];
                for (int lJ = 0; lJ < lRoute.Outlets.Count; lJ++) {
                    OutletInRoute lOutletInRoute = lRoute.Outlets[lJ];
                    if (_OLCache.ContainsKey(lOutletInRoute.Id))
                        _OLCache.Remove(lOutletInRoute.Id);
                }
            }
        }

        private void CreateEmptyRoutes(SupervisorModel supervisor, out M2Model m2) {
            CreateOrGetFromCacheM2(supervisor, out m2);
            if (m2.IsLoaded)
                return;

            LoadMerchList(supervisor);

            foreach (MerchModel lMerch in _merchList) {
                M1Model lM1;
                if (_m1Cache.ContainsKey(lMerch.Id))
                    lM1 = _m1Cache[lMerch.Id];
                else {
                    //CreateM1(lRow, lMerch.Id, out lM1);
                    lM1 = new M1Model(lMerch.Id);
                    lM1.SetName(lMerch.Name);
                    lM1.SetSupervisorId(lMerch.SupervisorId);
                    lM1.SetLatitude(lMerch.Lat);
                    lM1.SetLongitude(lMerch.Lon);

                    lM1.SetParentModel(m2);
                    _m1Cache.Add(lMerch.Id, lM1);
                }

                //check for empty routes for merch
                LoadRouteList(1, lM1.Id);
                RouteWithOutletsModel lRouteWithOutlets;
                foreach (RouteModel lRoute in _routeList) {
                    if (lM1.RouteExists(lRoute.RouteId))
                        continue;
                    lRouteWithOutlets = new RouteWithOutletsModel(lRoute.RouteId);
                    lRouteWithOutlets.SetMerchId(lM1.Id);
                    lRouteWithOutlets.SetRouteName(lRoute.RouteName);
                    lRouteWithOutlets.SetParentModel(lM1);
                    lRouteWithOutlets.Apply();
                    lM1.AddRoute(lRouteWithOutlets);
                }

                //TypeOfRoute = 2 - Другие ТТ
                lRouteWithOutlets = null;
                foreach (RouteWithOutletsModel lRoute in lM1.Routes)
                    if (lRoute.RouteId == 0) {
                        lRouteWithOutlets = lRoute;
                        break;
                    }
                if (lRouteWithOutlets == null) {
                    lRouteWithOutlets = new RouteWithOutletsModel(0);
                    lRouteWithOutlets.SetMerchId(lMerch.Id);
                    lRouteWithOutlets.SetRouteName("Другие ТТ");
                    lRouteWithOutlets.SetParentModel(lM1);
                    lRouteWithOutlets.Apply();
                    lM1.AddRoute(lRouteWithOutlets);
                }

                //sort routes by name
                lM1.Routes.Sort();
                lM1.Apply();
            }
        }

        /// <summary>
        /// Fills models from DataSet
        /// </summary>
        /// <param name="supervisor"></param>
        /// <param name="dataTable">Unified DataSet with </param>
        /// <param name="typeOfRoute">Type of Route: 1 - regular route, 2 - Other (not on routes)</param>
        /// <param name="m2"></param>
        private void DataSetToM2Model(SupervisorModel supervisor, DataTable dataTable, int typeOfRoute, out M2Model m2) {
            RouteWithOutletsModel lRoute = null;
            RouteModel lRouteModel = null;
            MerchModel lMerch = null;
            M1Model lM1 = null;

            CreateOrGetFromCacheM2(supervisor, out m2);

            if (m2.IsLoaded)
                return;

            LoadMerchList(supervisor);
            long lCurRouteId = -100;
            int lCurMerchId = -1;
            int lOlNumber = 1;

            foreach (DataRow lRow in dataTable.Rows) {
                long lOLid = (long) lRow[SqlConstants.OlIdFieldName];
                long lRouteId = Convert.ToInt64(lRow[SqlConstants.RouteIdFieldName]);
                int lMerchId = (int) lRow[SqlConstants.MerchIdFieldName];

                //Merch
                if (lCurMerchId != lMerchId) {
                    lCurRouteId = -100;
                    lMerch = GetMerchById(lMerchId);

                    if(lMerch == null)
                        continue;

                    if (_m1Cache.ContainsKey(lMerchId))
                        lM1 = _m1Cache[lMerchId];
                    else {
                        CreateM1(lRow, lMerch, out lM1);
                        lM1.SetParentModel(m2);
                        _m1Cache.Add(lMerchId, lM1);
                    }
                    lCurMerchId = lMerchId;
                    if (!m2.M1List.Contains(lM1))
                        m2.AddM1(lM1);
                }

                // Route
                Debug.Assert(lM1 != null, "M1 can not be null");
                if (lRouteId != lCurRouteId) {
                    lOlNumber = 1;
                    lRoute = new RouteWithOutletsModel(lRouteId);
                    lRoute.SetRouteName((string) lRow[SqlConstants.RouteNameFieldName]);
                    lRoute.SetMerchId((int) lRow[SqlConstants.MerchIdFieldName]);
                    lRoute.SetPreDistance(typeOfRoute == 1 ? lRow[SqlConstants.DistancFieldName] : DBNull.Value);
                    lRoute.SetPreOptDistance(typeOfRoute == 1 ? lRow[SqlConstants.OptDistancFieldName] : DBNull.Value);
                    lRoute.SetParentModel(lM1);
                    lRoute.StartMarker = lM1.Marker;
                    lRoute.Apply();
                    lM1.AddRoute(lRoute);
                    lCurRouteId = lRouteId;

                    lRouteModel = new RouteModel(lRouteId);
                    lRouteModel.Assign(lRoute);
                }

                // Outlet
                OutletModel lOutlet;
                if (_OLCache.ContainsKey(lOLid))
                    lOutlet = _OLCache[lOLid];
                else {
                    lOutlet = CreateOutlet(lRow);
                    _OLCache.Add(lOLid, lOutlet);
                }

                // MerchRoutes
                if (typeOfRoute == 1)
                {
                    lOutlet.AddMerchRoute(lMerch, lRouteModel);
                    lOutlet.UpdateRecVisits();
                }
                if (typeOfRoute == 2) {
                    lOutlet.OwnerMerch = lMerch;
                }

                // OutletInRoute
                OutletInRoute lOutletInRoute = new OutletInRoute(lOutlet);
                lOutletInRoute.SetOlNumber(lOlNumber++);
                lOutletInRoute.Apply();
                lOutletInRoute.SetParentModel(lRoute);
                Debug.Assert(lRoute != null, "Route can not be null");
                lRoute.AddOutlet(lOutletInRoute);
            }
        }

        /// <summary>
        /// Sets current values for M2, M1, Route, OutletList
        /// </summary>
        private void SetCurrentValues()
        {
            if (CustomerHasNoRoutes)
            {
                CurrentM1 = null;
                _merchList.Clear();
                _currentMerch = null;
            }
            else
            {
                CurrentM1 = (_currentM2.M1List.Count == 0) ? null : _currentM2.M1List[0];
                //current Merch
                UpdateCurrentMerchList();
                _currentMerch = (_merchList.Count == 0) ? null : _merchList[0];
            }

            // current Route
            CurrentRoute = null;
            if (CurrentM1 != null)
                _routeWithOutletsList = CurrentM1.Routes;
            else
                _routeWithOutletsList = new List<RouteWithOutletsModel>(4);
            if (_routeWithOutletsList.Count <= 0)
                return;
            CurrentRoute = _routeWithOutletsList[0];
            ChangeOtherRoute();
            _view.RefreshData();
        }

        private void UpdateCurrentMerchList()
        {
            _merchList.Clear();
            for (int lI = 0; lI < _currentM2.M1List.Count; lI++)
            {
                MerchModel lMerch = new MerchModel();
                lMerch.Assign(_currentM2.M1List[lI]);
                _merchList.Add(lMerch);
            }
        }

        private void LoadMerchList(SupervisorModel supervisor) {
            DataTable lDataTable = DataProvider.GetMerchandisers(supervisor.Id, supervisor.CustId);
            _merchList.Clear();
            //_currentMerch = null;
            foreach (DataRow lRow in lDataTable.Rows) {
                MerchModel lMerch = new MerchModel((int) lRow[SqlConstants.MerchIdFieldName]);
                lMerch.SetName((string) lRow[SqlConstants.MerchNameFieldName]);
                lMerch.SetSupervisorId((int) lRow[SqlConstants.SupervisorIdFieldName]);
                if (DBNull.Value == lRow[SqlConstants.LatitudeFieldName])
                    lMerch.SetLatitude(null);
                else
                    lMerch.SetLatitude(Convert.ToDouble(lRow[SqlConstants.LatitudeFieldName]));
                if (DBNull.Value == lRow[SqlConstants.LongitudeFieldName])
                    lMerch.SetLongitude(null);
                else
                    lMerch.SetLongitude(Convert.ToDouble(lRow[SqlConstants.LongitudeFieldName]));

                _merchList.Add(lMerch);
            }
        }

        private void LoadRouteList(int entityType, int entityId) {
            DataTable lDataTable = DataProvider.GetRouteList(entityType, entityId);
            _routeList.Clear();
            //_currentMerch = null;
            foreach (DataRow lRow in lDataTable.Rows) {
                RouteModel lRouteModel = new RouteModel((long) lRow[SqlConstants.RouteIdFieldName]);
                lRouteModel.SetRouteName((string) lRow[SqlConstants.RouteNameFieldName]);
                lRouteModel.SetMerchId(Convert.ToInt32(lRow[SqlConstants.MerchIdFieldName]));

                _routeList.Add(lRouteModel);
            }
        }

        private MerchModel GetMerchById(int merchId) {
            foreach (MerchModel lMerch in _merchList) {
                if (merchId == lMerch.Id)
                    return lMerch;
            }
            return null;
        }

        public RouteWithOutletsModel GetRouteById(long routeId) {
            if (null == CurrentM1)
                return null;
            foreach (RouteWithOutletsModel lRoute in CurrentM1.Routes) {
                if (lRoute.RouteId == routeId)
                    return lRoute;
            }
            return null;
        }

        #endregion

        #region Object creation methods

        private OutletModel CreateOutlet(DataRow dataRow) {
            OutletModel lOutlet = new OutletModel((long) dataRow[SqlConstants.OlIdFieldName]);
            lOutlet.SetName((string) dataRow[SqlConstants.OlNameFieldName]);
            lOutlet.SetTraidingName((string) dataRow[SqlConstants.OlTraidingNameFieldName]);
            lOutlet.SetAddress((string) dataRow[SqlConstants.OlAddressFieldName]);
            lOutlet.SetDeliveryAddress((string) dataRow[SqlConstants.OlDeliveryAddressFieldName]);
            if (DBNull.Value == dataRow[SqlConstants.LatitudeFieldName])
                lOutlet.SetLat(null);
            else
                lOutlet.SetLat(Convert.ToDouble(dataRow[SqlConstants.LatitudeFieldName]));
            if (DBNull.Value == dataRow[SqlConstants.LongitudeFieldName])
                lOutlet.SetLon(null);
            else
                lOutlet.SetLon(Convert.ToDouble(dataRow[SqlConstants.LongitudeFieldName]));
            lOutlet.AllowHtmlText = FiltersHelper.AllowHtmlText;
            lOutlet.ColorForText = FiltersHelper.ValueColor; //"#FF8C00"; // DarkOrange
            lOutlet.CreateMarker();
            lOutlet.Apply();
            return lOutlet;
        }

        private void CreateM1(DataRow dataRow, MerchModel merch, out M1Model m1) {
            m1 = new M1Model((int) dataRow[SqlConstants.MerchIdFieldName]);
            m1.SetName((string) dataRow[SqlConstants.MerchNameFieldName]);
            m1.SetSupervisorId((int) dataRow[SqlConstants.SupervisorIdFieldName]);
            if (null != merch) {
                m1.SetLatitude(merch.Lat);
                m1.SetLongitude(merch.Lon);
            }
            m1.CreateMarker(_view.Images.Images[8]);
            m1.Apply();
        }

        private void CreateOrGetFromCacheM2(SupervisorModel supervisor, out M2Model m2) {
            if (supervisor == null)
            {
                m2 = null;
                return;
            }
            if (_m2Cache.ContainsKey(supervisor.Id)) {
                m2 = _m2Cache[supervisor.Id];
                return;
            }
            m2 = new M2Model(supervisor.Id);
            m2.SetName(supervisor.Name);
            m2.SetCustId(supervisor.CustId);
            m2.Apply();
            _m2Cache.Add(m2.Id, m2);
        }

        private SupervisorModel CreateSupervisorModel(DataRow dataRow)
        {
            var sm = new SupervisorModel((int)dataRow[SqlConstants.SupervisorIdFieldName]);
            sm.SetName((string)dataRow[SqlConstants.SupervisorNameFieldName]);
            sm.SetCustId((int)dataRow[SqlConstants.CustIdFieldName]);
            sm.SetCountry((Country)dataRow[SqlConstants.CountryIdFieldName]);
            return sm;
        }

        #endregion

        #region Implementation of IRouteManagementController

        public M1Model CurrentM1 {
            get { return _currentM1; }
            private set {
                _currentM1 = value;
                FiltersHelper.LastM1 = _currentM1;
            }
        }

        public M2Model CurrentM2 {
            get { return _currentM2; }
        }

        public RouteWithOutletsModel CurrentRoute {
            get { return _currentRoute; }
            private set {
                _currentRoute = value;
                FiltersHelper.LastRoute = _currentRoute;
            }
        }

        private RouteWithOutletsModel _otherRoute;

        public RouteWithOutletsModel OtherRoute {
            get { return _otherRoute; }
            private set {
                _otherRoute = value;
                //FiltersHelper.LastOtherRoute = _otherRoute;
            }
        }

        public Dictionary<int, M1Model> M1Cache {
            get { return _m1Cache; }
        }

        public Dictionary<int, M2Model> M2Cache {
            get { return _m2Cache; }
        }

        public Dictionary<long, OutletModel> OLCache {
            get { return _OLCache; }
        }

        public CustomerSimpleModel CurrentCustomer {
            get { return _currentCustomer; }
            set { _currentCustomer = value; }
        }

        public SupervisorModel CurrentSupervisor {
            get { return GetCurrentSupervisor(); }
            set { _currentSupervisor = value; }
        }

        public MerchModel CurrentMerch {
            get { return GetCurrentMerch(); }
            set { _currentMerch = value; }
        }

        public Dictionary<int, List<SupervisorModel>> CustIdSupervisors {
            get { return _custIdSupervisors; }
        }

        public List<CustomerSimpleModel> GetCustomerList() {
            return _customersList;
        }

        public List<SupervisorModel> GetSupervisorList() {
            return _supervisorsList;
        }

        public List<MerchModel> GetMerchList() {
            return _merchList;
        }

        public SupervisorModel GetCurrentSupervisor() {
            return _currentSupervisor;
        }

        public MerchModel GetCurrentMerch() {
            return _currentMerch;
        }

        public List<RouteWithOutletsModel> GetRoutesForView() {
            List<RouteWithOutletsModel> lRouteList = new List<RouteWithOutletsModel>();
            if (null == CurrentM1)
                return lRouteList;
            foreach (RouteWithOutletsModel lRoute in CurrentM1.Routes) {
                if (lRoute.GetRouteId() >= 0)
                    lRouteList.Add(lRoute);
            }
            return lRouteList;
        }

        public void ChangeCurrentMerch(int merchId) {
            if (!CustomerHasNoRoutes && merchId == _currentMerch.Id)
                return;
            if (!_m1Cache.ContainsKey(merchId))
                return;
            CurrentM1 = _m1Cache[merchId];
            _routeWithOutletsList = CurrentM1.Routes;
            ChangeCurrentRoute(_routeWithOutletsList[0].RouteId);
            _currentMerch = GetMerchById(merchId);
            ChangeOtherRoute();
        }

        private void ChangeOtherRoute() {
            OtherRoute = null;
            if (null == CurrentM1)
                return;
            foreach (RouteWithOutletsModel lRoute in CurrentM1.Routes) {
                if (lRoute.RouteId == 0) {
                    OtherRoute = lRoute;
                    break;
                }
            }
        }

        public void ChangeCurrentRoute(long routeId) {
            CurrentRoute = null;
            if (null == CurrentM1)
                return;
            for (int lIndex = 0; lIndex < CurrentM1.Routes.Count; lIndex++) {
                if (CurrentM1.Routes[lIndex].RouteId == routeId) {
                    CurrentRoute = CurrentM1.Routes[lIndex];
                    break;
                }
            }
        }

        public void ChangeCurrentSupervisor(int svId) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                if (!CustomerHasNoRoutes && svId == _currentSupervisor.Id)
                    return;
                if (!CheckModificationAndSave())
                    return;

                foreach (SupervisorModel lSv in _supervisorsList)
                    if (lSv.Id == svId) {
                        _currentSupervisor = lSv;
                        break;
                    }
                CreateOrGetFromCacheM2(_currentSupervisor, out _currentM2);
                if (!CustomerHasNoRoutes && !_currentM2.IsLoaded)
                    LoadRouteView(false);
                else                                                          //----------------Ticket#1041279 It was without  ELSE 
                SetCurrentValues();
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ChangeCurrentCustomer(int custId) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                if (custId == _currentCustomer.Id)
                    return;
                if (!CheckModificationAndSave())
                    return;

                foreach (CustomerSimpleModel lCust in _customersList)
                    if (lCust.Id == custId) {
                        _currentCustomer = lCust;
                        break;
                    }
                
                //TODO - ???
                _OLCache.Clear();
                _m1Cache.Clear();
                _m2Cache.Clear();
                if (_custIdSupervisors.ContainsKey(custId))
                    _supervisorsList = _custIdSupervisors[custId];
                else
                    _supervisorsList = new List<SupervisorModel>();
                LoadSupervisors();
                LoadRouteView(false);
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }

        //|TODO: Refactor: move to View object
        private bool CheckModificationAndSave() {
            try {
                if (_currentM2 != null &&_currentM2.IsModified())
                {
                    DialogResult lDialogResult = MessageBox.Show(string.Format("Маршруты для персонала были изменены.\nСохранить?"),
                        Resources.StrSave, MessageBoxButtons.OKCancel);
                    if (lDialogResult == DialogResult.OK) {
                        Cursor.Current = Cursors.WaitCursor;
                        try {
                            SaveData();
                        }
                        finally {
                            Cursor.Current = Cursors.Default;
                        }
                    }
                }
            }
            catch (Exception lException) {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "CheckModificationAndSave()", "RouteManagementController", LayerType.UI, null, lException.Message);
                MessageBox.Show("Ошибка при сохранении маршрутов!\n" + lException.Message,
                    Resources.StrMsgBoxTitleRouteManagement,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public void ClearRoute(RouteWithOutletsModel route = null) {
            if (null == route)
                route = CurrentRoute;
            for (int lI = route.Outlets.Count - 1; lI >= 0; lI--)
                RemoveOutletFromRoute(route.Outlets[lI], route);
        }

        public RouteResult GetRouteResultsFromService(RouteWithOutletsModel route, bool optimize, bool silentMode) {
            if (route.Outlets.Count <= 0)
                return null;
            RouteParams lRouteParams;
            List<OutletInRoute> lWithoutCoordList;
            List<OutletInRoute> lSourceOutletsList;

            RoutingProviderHelper.BuildRouteParams(route, out lRouteParams, out lSourceOutletsList, out lWithoutCoordList);
            lRouteParams.OptimizeIt = optimize;
            lRouteParams.IsRu = _currentSupervisor.GetCountry() == Country.Russia;

            // call route service
            RouteResult lRouteResult = BuildRouteUsingService(lRouteParams, silentMode, CurrentRoute.RouteId);

            if (null == lRouteResult) {
                route.Distance = 0;
                route.OptDistance = 0;
                return null;
            }

            //set Distance and Optimized distance
            route.Distance = Convert.ToDecimal(Math.Round(lRouteResult.SourceDistance / 1000, 1, MidpointRounding.AwayFromZero));
            route.OptDistance = Convert.ToDecimal(Math.Round(lRouteResult.OptimizedDistance / 1000, 1, MidpointRounding.AwayFromZero));

            if (optimize && lRouteResult.OptimizedOrder != null) {
                route.IsOptimized = true;
                route.Distance = Convert.ToDecimal(Math.Round(lRouteResult.OptimizedDistance / 1000, 1, MidpointRounding.AwayFromZero));

                List<OutletInRoute> lNewOutletsList = new List<OutletInRoute>(route.Outlets.Count);
                int lIndex = route.IsStartMarker ? 1 : 0;
                // reorder outlets
                for (int lI = lIndex; lI < lRouteResult.OptimizedOrder.Length; lI++)
                    lNewOutletsList.Add(lSourceOutletsList[lRouteResult.OptimizedOrder[lI] - lIndex]);
                // add points rejected by service to the end
                if (lRouteResult.RejectedPoints != null)
                    for (int lI = 0; lI < lRouteResult.RejectedPoints.Length; lI++)
                        lNewOutletsList.Add(lSourceOutletsList[lRouteResult.RejectedPoints[lI]]);
                // add outlets without coords to the end
                foreach (OutletInRoute lOutletInRoute in lWithoutCoordList) {
                    lNewOutletsList.Add(lOutletInRoute);
                }

                route.Outlets.Clear();
                route.Outlets.AddRange(lNewOutletsList);
                route.RenumberOutlets();
                route.IsOptimized = true;
            }
            return lRouteResult;
        }

        public RouteResult GetSourceRoute(bool silentMode) {
            return GetRouteResultsFromService(CurrentRoute, false, silentMode);
        }

        private void GetRouteDistances(RouteWithOutletsModel route) {
            if (route.Outlets.Count <= 0)
                return;
            if (route.HasPreDistance && route.HasPreOptDistance)
            {
                route.Distance = Math.Round(route.PreDistance/1000, 1, MidpointRounding.AwayFromZero);
                route.OptDistance = Math.Round(route.PreOptDistance/1000, 1, MidpointRounding.AwayFromZero);
                return;
            }           
            RouteParams lRouteParams;
            List<OutletInRoute> lWithoutCoordList;
            List<OutletInRoute> lSourceOutletsList;

            RoutingProviderHelper.BuildRouteParams(route, out lRouteParams, out lSourceOutletsList, out lWithoutCoordList);
            lRouteParams.IsRu = _currentSupervisor.GetCountry() == Country.Russia;

            lRouteParams.OptimizeIt = !(route.HasPreOptDistance && !route.HasPreDistance);

            // call route service
            RouteResult lRouteResult = BuildRouteUsingService(lRouteParams, true, route.RouteId);

            if (null == lRouteResult) {
                route.Distance = 0;
                route.OptDistance = 0;
                return;
            }

            //set Distance and Optimized distance
            route.Distance = Convert.ToDecimal(Math.Round(lRouteResult.SourceDistance / 1000, 1, MidpointRounding.AwayFromZero));
            route.OptDistance = (route.HasPreOptDistance && !route.HasPreDistance)? Math.Round(route.PreOptDistance/1000,1,MidpointRounding.AwayFromZero):
            Convert.ToDecimal(Math.Round(lRouteResult.OptimizedDistance / 1000, 1, MidpointRounding.AwayFromZero));
        }

        public void LoadRouteSet() {
            if (!CheckModificationAndSave())
                return;

            LoadRouteSetList(_currentM2.Id);

            OpenSaveRouteSet lOpenDialog = new OpenSaveRouteSet();
            //set properties
            lOpenDialog.SetOptions(false, _currentM2.Id);
            lOpenDialog.RouteSetList = _routeSetList;
            DialogResult lDialogResult = lOpenDialog.ShowDialog();
            if (lDialogResult == DialogResult.Cancel)
                return;

            if (lDialogResult == DialogResult.Retry) {
                LoadRouteView(true);
                return;
            }

            if (lOpenDialog.CurrentIndex < 0)
                return;
            int lRouteSetId = _routeSetList[lOpenDialog.CurrentIndex].RouteSetId;
            LoadRouteSetDetail(_currentM2, lRouteSetId);
        }

        public void InitDBObjects() {
            DataProvider.InitRmt();
        }

        public void CopyRoute(bool overwrite, long destRouteId) {
            //find route
            RouteWithOutletsModel lDestRoute = GetRouteById(destRouteId);
            if (lDestRoute == null)
                return;

            //clone source route
            List<OutletInRoute> lSourceOutlets = new List<OutletInRoute>(CurrentRoute.CountTt);
            foreach (OutletInRoute lOutletInRoute in CurrentRoute.Outlets)
                lSourceOutlets.Add(lOutletInRoute);

            //clear destination route
            if (overwrite)
                ClearRoute(lDestRoute);

            //add TT to route
            //TODO: add overload method AddOutletToRoute(lDestRoute, lOutletInRoute.Outlet);
            foreach (OutletInRoute lOutletInRoute in lSourceOutlets)
                //AddOutletToRoute(lDestRoute, lOutletInRoute);
                AddOutletToRoute(lDestRoute, lOutletInRoute.Outlet);
        }

        public void GetFullInfo(OutletModel outlet) {
            DataTable lDataTable = DataProvider.GetFullInfo(outlet.Id);
            if (lDataTable == null)
                return;
            OutletFullModel lFullInfo = new OutletFullModel(outlet.Id);
            DataRow lRow = lDataTable.Rows[0];
            lFullInfo.SetTypeName((string) lRow[SqlConstants.OLtypeNameFieldName]);
            lFullInfo.SetCategory((string) lRow[SqlConstants.CategoryFieldName]);
            if (lRow[SqlConstants.SaleSoldVolumeCurFieldName] == DBNull.Value)
                lFullInfo.SetSaleVolume(null);
            else
                lFullInfo.SetSaleVolume((decimal?) lRow[SqlConstants.SaleSoldVolumeCurFieldName]);
            if (lRow[SqlConstants.MiddleCountSalesFieldName] == DBNull.Value)
                lFullInfo.SetAvgShipmentCount(null);
            else
                lFullInfo.SetAvgShipmentCount((decimal?) lRow[SqlConstants.MiddleCountSalesFieldName]);
            if (lRow[SqlConstants.MiddleCountVisitsFieldName] == DBNull.Value)
                lFullInfo.SetAvgVisitCount(null);
            else
                lFullInfo.SetAvgVisitCount((decimal?) lRow[SqlConstants.MiddleCountVisitsFieldName]);
            if (lRow[SqlConstants.MiddleSkuAttFieldName] == DBNull.Value)
                lFullInfo.SetAverageSku(null);
            else
                lFullInfo.SetAverageSku((int?) lRow[SqlConstants.MiddleSkuAttFieldName]);
            if (lRow[SqlConstants.StrikeRateFieldName] == DBNull.Value)
                lFullInfo.SetStrikeRate(null);
            else
                lFullInfo.SetStrikeRate((decimal?) lRow[SqlConstants.StrikeRateFieldName]);

            outlet.FullInfoOutlet = lFullInfo;
        }

        // Adding and Removing outlet in route
        //--------------------------------------

        public bool AddOutletToRoute(OutletModel outlet) {
            return InsertOutletToRoute(-1, outlet);
        }

        public bool InsertOutletToRoute(int index, OutletModel outlet) {
            bool lOutletExists = CurrentRoute.OutletExists(outlet);

            CurrentRoute.HasPreDistance = false;        // Optimization RMT
            CurrentRoute.HasPreOptDistance = false;     // Optimization RMT

            RouteModel lRoute = new RouteModel(CurrentRoute.RouteId);
            lRoute.Assign(CurrentRoute);
            outlet.AddMerchRoute(_currentMerch, lRoute);

            if (lOutletExists)
                return false;
            OutletInRoute lOutletInRoute = new OutletInRoute(outlet);
            CurrentRoute.InsertOutlet(index, lOutletInRoute);
            RemoveOutletFromRoute(lOutletInRoute, OtherRoute);
            return true;
        }

        public bool AddOutletToRoute(long routeId, OutletModel outlet) {
            RouteWithOutletsModel lRoute = GetRouteById(routeId);
            if (null == lRoute)
                return false;
            return AddOutletToRoute(lRoute, outlet);
        }

        public bool AddOutletToRoute(RouteWithOutletsModel route, OutletModel outlet) {
            bool lOutletExists = route.OutletExists(outlet);

            if (lOutletExists)
                return false;

            route.HasPreDistance = false;        // Optimization RMT
            route.HasPreOptDistance = false;     // Optimization RMT

            OutletInRoute lOutletInRoute = new OutletInRoute(outlet);
            bool lResult = route.AddOutlet(lOutletInRoute);
            if (!route.Outlets.Contains(lOutletInRoute))
                return false;

            if (route.RouteId != 0) {
                RouteModel lRoute = new RouteModel(route.RouteId);
                lRoute.Assign(route);
                outlet.AddMerchRoute(_currentMerch, lRoute);
                RemoveOutletFromRoute(lOutletInRoute, OtherRoute);
                return true;
            }
            return lResult;
        }

        public bool AddOutletToRoute(long routeId, OutletInRoute outletInRoute) {
            RouteWithOutletsModel lRoute = GetRouteById(routeId);
            if (null == lRoute)
                return false;
            return AddOutletToRoute(lRoute, outletInRoute);
        }

        public bool AddOutletToRoute(RouteWithOutletsModel route, OutletInRoute outletInRoute) {
           
            route.HasPreDistance = route.HasPreOptDistance = false;  //optimization RMT

            bool lResult = route.AddOutlet(outletInRoute);
            if (!route.Outlets.Contains(outletInRoute))
                return false;
            if (route.RouteId != 0) {
                RouteModel lRouteModel = new RouteModel(CurrentRoute.RouteId);
                lRouteModel.Assign(route);
                outletInRoute.Outlet.AddMerchRoute(_currentMerch, lRouteModel);
                RemoveOutletFromRoute(outletInRoute, OtherRoute);
            }
            return lResult;
        }

        public bool RemoveOutletFromRoute(OutletInRoute outletInRoute, RouteWithOutletsModel route) {
            
            route.HasPreDistance = route.HasPreOptDistance = false;  //optimization RMT
            
            bool lResult = route.RemoveOutlet(outletInRoute);
            // add TT to Other TT route if no route present
            if (!outletInRoute.Outlet.IsOnRoutes(_currentMerch) && route.RouteId > 0)
                AddOutletToRoute(OtherRoute, outletInRoute);
            return lResult;
        }

        public bool RemoveOutletFromRoute(OutletInRoute outletInRoute) {
            return RemoveOutletFromRoute(outletInRoute, CurrentRoute);
        }

        public void CompleteM2Cashe()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var smToLoad = FindM2NotLoadedToCache();
                if (smToLoad.Count > 0)
                {
                    foreach (var sm in smToLoad)
                    {
                        FillM2Model(sm);
                    }
                    UpdateCurrentMerchList();
                }
            }
            catch (Exception ex)
            {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "CompleteM2Cashe()", "RouteManagementController", LayerType.UI, null, ex.Message);
                MessageBox.Show("Ошибка при загрузке текущих маршрутов!\n" + ex.Message,
                    Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private List<SupervisorModel> FindM2NotLoadedToCache()
        {
            var smToLoad =
                _supervisorsList.Where(sm => !_m2Cache.Keys.Contains(sm.Id) || !_m2Cache[sm.Id].IsLoaded).ToList();
            return smToLoad;
        }

        #endregion

        private RouteResult BuildRouteUsingService(RouteParams param, bool silentMode, long routeId) {
            RouteResult lRouteResult = null;
            try {
                lRouteResult = RoutingProviderHelper.BuildRoute(param, routeId);
                //string lMsg = RoutingProviderHelper.GetLastErrorMsg();

                if (lRouteResult == null)
                    return null;

                if (lRouteResult.ErrorCode < -1 && !silentMode) {
                    MessageBox.Show(string.Format("{0}.\nКод: {1}.", lRouteResult.ErrorMessage, lRouteResult.ErrorCode),
                        Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception lException) {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "BuildRouteUsingService()", "RouteManagementController", LayerType.UI, null, lException.Message);
                MessageBox.Show(string.Format("Ошибка при обращении к Geo-сервису.\nМерчендайзер: {0} ({1}), Маршрут: {2} ({3})\nКод: {4}. {5}.",
                    _currentMerch.Name, _currentMerch.Id, CurrentRoute.RouteName, CurrentRoute.RouteId, RoutingProviderHelper.LastErrorCode, lException.Message),
                    Resources.StrMsgBoxTitleRouteManagement, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return lRouteResult;
        }

        public bool CustomerHasNoRoutes
        {
            get { return _currentSupervisor == null; }
        }
    }
}