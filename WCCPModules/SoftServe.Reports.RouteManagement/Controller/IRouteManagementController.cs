﻿using System.Collections.Generic;
using SoftServe.Core.Common.Controller;
using SoftServe.Reports.RouteManagement.Model.Merch;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using SoftServe.Reports.RouteManagement.Model.Route;
using SoftServe.Reports.RouteManagement.Model.Supervisor;
using SoftServe.Reports.RouteManagement.WcfRouterService;

namespace SoftServe.Reports.RouteManagement.Controller {
    public interface IRouteManagementController : IViewController {
        #region Properties

        CustomerSimpleModel CurrentCustomer { get; set; }
        SupervisorModel CurrentSupervisor { get; set; }
        MerchModel CurrentMerch { get; set; }
        M1Model CurrentM1 { get; }
        M2Model CurrentM2 { get; }
        RouteWithOutletsModel CurrentRoute { get; }
        RouteWithOutletsModel OtherRoute { get; }

        Dictionary<int, M2Model> M2Cache { get; }
        Dictionary<int, M1Model> M1Cache { get; }
        Dictionary<long, OutletModel> OLCache { get; }
        Dictionary<int, List<SupervisorModel>> CustIdSupervisors { get; }

        bool CustomerHasNoRoutes { get; }

        #endregion

        #region Lists

        List<CustomerSimpleModel> GetCustomerList();
        List<SupervisorModel> GetSupervisorList();
        List<MerchModel> GetMerchList();
        List<RouteWithOutletsModel> GetRoutesForView();

        #endregion

        SupervisorModel GetCurrentSupervisor();
        MerchModel GetCurrentMerch();
        RouteWithOutletsModel GetRouteById(long routeId);

        void ChangeCurrentMerch(int merchId);
        void ChangeCurrentRoute(long routeId);
        void ChangeCurrentSupervisor(int svId);
        void ChangeCurrentCustomer(int custId);
        
        bool AddOutletToRoute(OutletModel outlet);
        bool InsertOutletToRoute(int index, OutletModel outlet);
        bool AddOutletToRoute(long routeId, OutletModel outlet);
        bool AddOutletToRoute(RouteWithOutletsModel route, OutletModel outlet);
        bool AddOutletToRoute(long routeId, OutletInRoute outletInRoute);
        bool AddOutletToRoute(RouteWithOutletsModel route, OutletInRoute outletInRoute);
        bool RemoveOutletFromRoute(OutletInRoute outletInRoute, RouteWithOutletsModel route);
        bool RemoveOutletFromRoute(OutletInRoute outletInRoute);
        void ClearRoute(RouteWithOutletsModel route = null);

        RouteResult GetRouteResultsFromService(RouteWithOutletsModel route, bool optimize, bool silentMode);
        RouteResult GetSourceRoute(bool silentMode);

        void LoadRouteSetList(int svId);
        void LoadRouteSet();
        void InitDBObjects();
        void CopyRoute(bool overwrite, long destRouteId);
        void GetFullInfo(OutletModel outlet);
        void CompleteM2Cashe();
    }
}