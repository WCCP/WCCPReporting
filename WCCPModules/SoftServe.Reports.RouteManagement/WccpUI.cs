﻿using System;
using Logica.Reports.Common;
using System.Windows.Forms;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;
using BLToolkit.Data.Linq;
using SoftServe.Core.Common.Logging;
using SoftServe.Reports.RouteManagement;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpRouteManagement.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;
        private int _reportId;

        public static ReportParameter _rpRMT = new ReportParameter();
        

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Shows the UI.
        /// </summary>
        /// <param name="isSkined">The is skined.</param>
        /// <param name="reportCaption">The report caption.</param>
        /// <returns></returns>
        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
				//for debug
                //Util.SetCertificatePolicy();
                _reportCaption = reportCaption;
                _reportId = reportId;

                _rpRMT.Id = _reportId;
                _rpRMT.Name = _reportCaption;
                WccpLogger.WccpOpentLog(_rpRMT);

                _reportControl = new WccpUIControl();
                
                return 0;
            }
            catch (Exception ex)
            {
                WccpLogger.DoReportTraceLog(WccpUI._rpRMT, "ShowUI()", "WccpUI", LayerType.UI, null, ex.Message);
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        /// <summary>
        /// Closes the UI.
        /// </summary>
        public void CloseUI()
        {
            WccpLogger.WccpExitLog(_rpRMT);
        }

        public bool AllowClose()
        {
            return true;
        }
    }
}