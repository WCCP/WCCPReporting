﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using GMap.NET;
using GMap.NET.WindowsForms;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Controller;
using SoftServe.Reports.RouteManagement.Model.Merch;
using SoftServe.Reports.RouteManagement.Model.Route;
using SoftServe.Reports.RouteManagement.Properties;
using SoftServe.Reports.RouteManagement.View;

namespace SoftServe.Reports.RouteManagement.Model.Outlet {
    [Serializable]
    public class OutletModel : UpdatableParentModel, IOutletModel {
        #region Fields

        private long _id;
        private string _name;
        private string _traidingName;
        private string _address;
        private string _deliveryAddress;
        private double? _lat;
        private double? _lon;
        private readonly List<MerchRoutesModel> _merchRoutesList = new List<MerchRoutesModel>();
        private string _merchesRoutesInfo = string.Empty;
        private int _recVisits;
        private int _planVisits;
        private Image _image;
        private GMapMarkerImage _marker;
        private OutletFullModel _outletFull;
        private MerchModel _ttOwner;
        private bool _allowHtmlText = false;
        private string _colorStr = "red";
        private string _toolTipText;
        private Font _font;
        private Pen _pen;

        private const int NumberOffset = 4;

        #endregion

        #region Constructors

        public OutletModel() : this(0, string.Empty) {
        }

        public OutletModel(long id) : this(id, string.Empty) {
        }

        public OutletModel(long id, string name) {
            _id = id;
            _name = name ?? string.Empty;
            _traidingName = string.Empty;
            _address = string.Empty;
            _deliveryAddress = string.Empty;
//            _olNumber = 0;
            _lat = 0.0;
            _lon = 0.0;
            _font = new Font("Tahoma", 11, FontStyle.Bold, GraphicsUnit.Point, 204);
            _pen = new Pen(SystemColors.InfoText);
        }

        public OutletModel(IUpdatableParentModel parentModel) : this(0, string.Empty) {
            SetParentModel(parentModel);
        }

        #endregion

        #region Properties

        private string LineBreak {
            get { return AllowHtmlText ? "<br>" : "\n"; }
        }

        private string ColorTag {
            get { return "<color=" + ColorForText + ">"; }
        }

        private string ColorTagEnd {
            get { return "</color>"; }
        }

        public bool AllowHtmlText {
            get { return _allowHtmlText; }
            set {
                if (value == _allowHtmlText)
                    return;
                _allowHtmlText = value;
                if (_outletFull != null)
                    _outletFull.AllowHtmlText = value;
                UpdateMerchRoutesInfo();
            }
        }

        public string ColorForText {
            get { return _colorStr; }
            set {
                if (value == _colorStr)
                    return;
                _colorStr = value;
                if (_outletFull != null)
                    _outletFull.ColorForText = value;
                UpdateMerchRoutesInfo();
            }
        }

        public string ToolTipTitle { get; set; }

        public string ToolTipText {
            get { return _traidingName + LineBreak
                    + _deliveryAddress //+ "\n"
                    + _merchesRoutesInfo; }
        }

        public string OutletToolTip {
            get { 
                if (string.IsNullOrEmpty(ToolTipTitle))
                    ToolTipTitle = _id.ToString(CultureInfo.InvariantCulture);

                return ToolTipTitle + LineBreak + ToolTipText;
            }
        }

        public OutletFullModel FullInfoOutlet {
            get { return _outletFull; }
            set { 
                _outletFull = value;
                _outletFull.AllowHtmlText = AllowHtmlText;
                _outletFull.ColorForText = ColorForText;
            }
        }

        public string FullInfo {
            get { return _outletFull != null ? _outletFull.FullInfo : string.Empty; }
        }

        public long Id {
            get { return GetId(); }
            set { SetId(value); }
        }

        public string Name {
            get { return GetName(); }
            set { SetName(value); }
        }

        public string TraidingName {
            get { return GetTraidingName(); }
            set { SetTraidingName(value); }
        }

        public string Address {
            get { return GetAddress(); }
            set { SetAddress(value); }
        }

        public string DeliveryAddress {
            get { return GetDeliveryAddress(); }
            set { SetDeliveryAddress(value); }
        }

        public double? Lat {
            get { return GetLat(); }
            set { SetLat(value); }
        }

        public double? Lon {
            get { return GetLon(); }
            set { SetLon(value); }
        }

        public int RecVisits {
            get { return GetRecVisits(); }
            set { SetRecVisits(value); }
        }

        public int PlanVisits {
            get { return GetPlanVisits(); }
            set { SetPlanVisits(value); }
        }

        public int VisitsDelta {
            get { return _recVisits - _planVisits; }
        }

        public string OutletInfo {
            get { return /*_id.ToString(CultureInfo.InvariantCulture) + ", " +*/ _traidingName + ", " + _deliveryAddress; }
        }

        public string MarkerToolTip { get; set; }

        public int ImageIndex { get; set; }

        public List<MerchRoutesModel> MerchRoutes {
            get { return _merchRoutesList; }
        }

        public MerchModel OwnerMerch {
            get { return _ttOwner; }
            set { _ttOwner = value; }
        }

        public Image MarkerImage {
            get { return _image; }
            set { 
                _image = value;
                if (IsMarker)
                    _marker.MarkerImage = _image;
            }
        }

        public bool IsMarker {
            get { return _marker != null; }
        }

        public GMapMarkerImage Marker {
            get { return _marker; }
            //set { _marker = value; }
        }

        #endregion

        #region Implementation of IOutletModel

        public long GetId() {
            return _id;
        }

        public void SetId(long id) {
            if (_id == id)
                return;
            _id = id;
            MakeModified();
        }

        public string GetName() {
            return _name;
        }

        public void SetName(string name) {
            if (_name.Equals(name))
                return;
            _name = name;
            MakeModified();
        }

        public string GetTraidingName() {
            return _traidingName;
        }

        public void SetTraidingName(string traidingName) {
            if (_traidingName.Equals(traidingName))
                return;
            _traidingName = traidingName;
            MakeModified();
        }

        public string GetAddress() {
            return _address;
        }

        public void SetAddress(string address) {
            if (_address.Equals(address))
                return;
            _address = address;
            MakeModified();
        }

        public string GetDeliveryAddress() {
            return _deliveryAddress;
        }

        public void SetDeliveryAddress(string deliveryAddress) {
            if (_deliveryAddress.Equals(deliveryAddress))
                return;
            _deliveryAddress = deliveryAddress;
            MakeModified();
        }

        public double? GetLat() {
            return _lat;
        }

        public void SetLat(double? lat) {
            if (_lat == null && lat == null)
                return;
            if (_lat != null && _lat.Equals(lat))
                return;
            _lat = lat;
            MakeModified();
        }

        public double? GetLon() {
            return _lon;
        }

        public void SetLon(double? lon) {
            if (_lon == null && lon == null)
                return;
            if (_lon != null && _lon.Equals(lon))
                return;
            _lon = lon;
            MakeModified();
        }

        public int GetRecVisits() {
            return _recVisits;
        }

        public void SetRecVisits(int recVisits) {
            if (_recVisits.Equals(recVisits))
                return;
            _recVisits = recVisits;
            MakeModified();
        }

        public int GetPlanVisits() {
            return _planVisits;
        }

        public void SetPlanVisits(int planVisits) {
            if (_planVisits.Equals(planVisits))
                return;
            _planVisits = planVisits;
            MakeModified();
        }

        public void UpdateMarkerToolTip(int position) {
            if (!IsMarker)
                return;
            if (position == 0)
                //_marker.ToolTipText = OutletToolTip;
                ToolTipTitle = _id.ToString(CultureInfo.InvariantCulture);
            else
                //_marker.ToolTipText = "№ " + position + " - " + OutletToolTip;
                ToolTipTitle = "№ " + position + " - " + _id.ToString(CultureInfo.InvariantCulture);
        }

        public void AddMerchRoute(MerchModel merch, RouteModel route) {
            bool lMerchExists = false;
            foreach (MerchRoutesModel lMerchRoutes in _merchRoutesList) {
                if (lMerchRoutes.Id == merch.Id) {
                    lMerchExists = true;
                    lMerchRoutes.AddRoute(route);
                    break;
                }
            }
            if (!lMerchExists) {
                MerchRoutesModel lMerchRoutes = new MerchRoutesModel(merch, route);
                _merchRoutesList.Add(lMerchRoutes);
            }
            UpdateMerchRoutesInfo();
        }

        public void AddMerchRoute(MerchRoutesModel merchRoute) {
            bool lMerchExists = false;
            foreach (MerchRoutesModel lMerchRoutes in _merchRoutesList) {
                if (lMerchRoutes.Id == merchRoute.Id) {
                    lMerchExists = true;
                    foreach (RouteModel lRoute in merchRoute.Routes)
                        lMerchRoutes.AddRoute(lRoute);
                }
            }
            if (!lMerchExists)
                _merchRoutesList.Add(merchRoute);
            UpdateMerchRoutesInfo();
        }

        public void RemoveRoute(long routeId) {
            foreach (MerchRoutesModel lMerch in _merchRoutesList)
                lMerch.RemoveRoute(routeId);
            CheckAndRemoveMerch();
            UpdateMerchRoutesInfo();
        }

        public void RemoveRoute(RouteModel route) {
            foreach (MerchRoutesModel lMerch in _merchRoutesList)
                lMerch.RemoveRoute(route);
            CheckAndRemoveMerch();
            UpdateMerchRoutesInfo();
        }

        private void UpdateMerchRoutesInfo() {
            _merchesRoutesInfo = string.Empty;
            _planVisits = 0;
            foreach (MerchRoutesModel lMerch in _merchRoutesList) {
                _merchesRoutesInfo += LineBreak + lMerch.MerchRoutesInfo;
                _planVisits += lMerch.Routes.Count;
            }
            RenderImage();
        }

        private void RenderImage() {
            float lX;
            float lY;
            Bitmap lBitmap = null;
            if (VisitsDelta > 0)
                lBitmap = new Bitmap(Resources.red_empty_28x34);
            else if (VisitsDelta == 0)
                lBitmap = new Bitmap(Resources.green_empty_28_34);
            else
                lBitmap = new Bitmap(Resources.gray_empty_28x34);
            //TODO: Blue marker for other TT 
            if (_merchRoutesList.Count == 0)
            {
                // display TT not on routes
                lBitmap = new Bitmap(Resources.blue_empty_28x34);
            }
            else if ((FiltersHelper.LastShowTT == ShowTTEnum.M1 || FiltersHelper.LastShowTT == ShowTTEnum.Route) && CheckIsOnOtherRoute()) 
            {
                // display TT from OtherRoute
                lBitmap = new Bitmap(Resources.blue_empty_28x34);
            }
            else if (FiltersHelper.LastShowTT == ShowTTEnum.Route && FiltersHelper.LastRoute.RouteId == 0 &&
                FiltersHelper.LastRoute.OutletExists(this))
            {
                // display TT only if OtherRoute is active
                lBitmap = new Bitmap(Resources.blue_empty_28x34);
            }

            Graphics lGraphics = Graphics.FromImage(lBitmap);
            Font lDrawFont = new Font("Tahoma", 11, FontStyle.Bold);
            SolidBrush lDrawBrush = new SolidBrush(Color.White);
            if (VisitsDelta < 0) {
                lX = NumberOffset;
                lY = NumberOffset;
            }
            else {
                lX = 2 * NumberOffset;
                lY = NumberOffset;
            }
            lGraphics.DrawString(VisitsDelta.ToString(CultureInfo.InvariantCulture), lDrawFont, lDrawBrush, lX, lY);
            _image = lBitmap;
            if (IsMarker)
                _marker.MarkerImage = _image;
        }

        private bool CheckIsOnOtherRoute()
        {
            var otheTTRoute = FiltersHelper.GetOtherTTRoute();
            return (otheTTRoute != null && otheTTRoute.OutletExists(this) ||
                    (_merchRoutesList.Count > 0 && !IsOnRoutes(_merchRoutesList[0])));
        }

        public void CreateMarker() {
            RenderImage();
            CreateMarker(_image);
        }

        public void CreateMarker(Image image) {
            if (_lat == null || _lon == null)
                return;
            double lLat = (double) _lat;
            double lLon = (double) _lon;

            _marker = new GMapMarkerImage(new PointLatLng(lLat, lLon), image);
            _marker.Tag = Id;
            _marker.ToolTipMode = MarkerTooltipMode.Never;
//            _marker.ToolTip.Fill = SystemBrushes.Info;
//            _marker.ToolTip.Foreground = SystemBrushes.InfoText;
//            _marker.ToolTip.Font = _font;
//            _marker.ToolTip.Stroke = _pen;
            _marker.ToolTipText = OutletToolTip;
        }

        #endregion

        #region BaseModel implementation

        public override void ClearKey() {
            base.ClearKey();
            SetId(0);
        }

        public override object GetKey() {
            return _id;
        }

        public override IBaseModel Clone() {
            OutletModel lModel = new OutletModel();
            lModel.Assign(this);
            return lModel;
        }

        public override bool Assign(IBaseModel model) {
            OutletModel lModel = model as OutletModel;
            if (null == lModel)
                return false;

            SetId(lModel.GetId());
            SetName(lModel.GetName());
            SetTraidingName(lModel.GetTraidingName());
            SetAddress(lModel.GetAddress());
            SetDeliveryAddress(lModel.GetDeliveryAddress());
            SetLat(lModel.GetLat());
            SetLon(lModel.GetLon());
            _merchRoutesList.Clear();
            foreach (MerchRoutesModel lMerchRoute in lModel.MerchRoutes) {
                _merchRoutesList.Add(lMerchRoute);
            }

            return true;
        }

        public override bool Equals(object obj) {
            Debug.Assert(obj != null, "Object should not be null");
            OutletModel lModel = obj as OutletModel;
            if (null == lModel)
                return false;

            return _id.Equals(lModel.GetId());
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }

        public override string ToString() {
 	        return _id.ToString(CultureInfo.InvariantCulture) + "\n" + _traidingName + "\n" + _deliveryAddress;
        }

        #endregion

        private void CheckAndRemoveMerch() {
            for (int lI = 0; lI < _merchRoutesList.Count; lI++)
                if (_merchRoutesList[lI].Routes.Count <= 0)
                    _merchRoutesList.RemoveAt(lI);
        }

        public void UpdateRecVisits() {
            _recVisits = 0;
            for (int lI = 0; lI < _merchRoutesList.Count; lI++)
                _recVisits += _merchRoutesList[lI].Routes.Count;
            RenderImage();
        }

        public bool IsOnRoutes(MerchModel merch) {
            if (_merchRoutesList.Count == 0)
                return false;
            if (null == merch)
                return false;
            for (int lI = _merchRoutesList.Count - 1; lI >= 0; lI--)
                if (_merchRoutesList[lI].Id == merch.Id)
                    return true;
            return false;
        }
    }
}