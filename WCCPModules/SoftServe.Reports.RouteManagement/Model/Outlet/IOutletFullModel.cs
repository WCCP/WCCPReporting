﻿using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Outlet {
    public interface IOutletFullModel : IUpdatableParentModel {
        long Id { get; set; }

        string GetTypeName();
        void SetTypeName(string typeName);
        string GetCategory();
        void SetCategory(string category);

        decimal? GetSaleVolume();
        void SetSaleVolume(decimal? saleVolume);
        decimal? GetAvgVisitCount();
        void SetAvgVisitCount(decimal? avgVisitCount);
        decimal? GetAvgShipmentCount();
        void SetAvgShipmentCount(decimal? avgShipmentCount);
        decimal? GetAverageSku();
        void SetAverageSku(decimal? averageSku);
        decimal? GetStrikeRate();
        void SetStrikeRate(decimal? strikeRate);
    }
}