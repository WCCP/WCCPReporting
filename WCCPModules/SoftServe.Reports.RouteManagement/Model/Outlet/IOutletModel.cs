﻿using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Merch;
using SoftServe.Reports.RouteManagement.Model.Route;

namespace SoftServe.Reports.RouteManagement.Model.Outlet {
    public interface IOutletModel : IUpdatableParentModel {
        long GetId();
        void SetId(long id);
        string GetName();
        void SetName(string name);
        string GetTraidingName();
        void SetTraidingName(string traidingName);

        string GetAddress();
        void SetAddress(string address);
        string GetDeliveryAddress();
        void SetDeliveryAddress(string deliveryAddress);

        double? GetLat();
        void SetLat(double? lat);
        double? GetLon();
        void SetLon(double? lon);

        int GetRecVisits();
        void SetRecVisits(int recVisits);
        int GetPlanVisits();
        void SetPlanVisits(int planVisits);

        void UpdateMarkerToolTip(int position);

        void AddMerchRoute(MerchModel merch, RouteModel route);
        void AddMerchRoute(MerchRoutesModel merchRoutes);
        void RemoveRoute(long routeId);
        void RemoveRoute(RouteModel route);
    }
}