﻿using System;
using System.Diagnostics;
using System.Globalization;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Outlet {
    [Serializable]
    public class OutletInRoute : UpdatableParentModel, IOutletInRoute {
        private int _olNumber;
        private OutletModel _outlet;

        #region Constructors

        public OutletInRoute(OutletModel outlet) {
            _outlet = outlet;
            _olNumber = 0;
        }

        public OutletInRoute(IUpdatableParentModel parentModel) : this(null) {
            SetParentModel(parentModel);
        }

        #endregion

        #region Properties

        public long Id {
            get { return _outlet.GetId(); }
        }

        public string OutletInfo {
            get { return _olNumber.ToString(CultureInfo.InvariantCulture) + " " + _outlet.OutletInfo; }
        }

        public int OlNumber {
            get { return GetOlNumber(); }
            set { SetOlNumber(value); }
        }

        #endregion

        #region Implementation of IOutletInRoute

        public OutletModel Outlet {
            get { return _outlet; }
            set { 
                if (_outlet.Equals(value))
                    return;
                _outlet = value;
            }
        }

        public int GetOlNumber() {
            return _olNumber;
        }

        public void SetOlNumber(int olNumber) {
            if (_olNumber.Equals(olNumber))
                return;
            _olNumber = olNumber;
            MakeModified();
        }

        #endregion

        public override bool Equals(object obj) {
            Debug.Assert(obj != null, "Object should not be null");
            OutletInRoute lModel = obj as OutletInRoute;
            if (null == lModel)
                return false;

            return _outlet.Id == lModel.Id;
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }

        public override string ToString() {
            return "№ " + _olNumber.ToString(CultureInfo.InvariantCulture) + " - " + _outlet;
        }
    }
}