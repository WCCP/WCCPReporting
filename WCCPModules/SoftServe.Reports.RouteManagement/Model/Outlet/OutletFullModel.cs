﻿using System;
using System.Globalization;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Outlet {
    [Serializable]
    public class OutletFullModel : UpdatableParentModel, IOutletFullModel {
        #region Fields

        private long _id;
        private string _typeName;
        private string _category;
        private decimal? _saleVolume;
        private decimal? _avgVisitCount;
        private decimal? _avgShipmentCount;
        private decimal? _averageSku;
        private decimal? _strikeRate;
        //private OutletModel _outlet;

        private string _fullInfo;
        private bool _allowHtmlText;
        private string _colorStr;

        #endregion

        #region Constructors

        public OutletFullModel(long id) {
            _id = id;
            _typeName = string.Empty;
            _category = string.Empty;
            _saleVolume = 0;
            _avgVisitCount = 0;
            _avgShipmentCount = 0;
            _averageSku = 0;
            _strikeRate = 0;
            _fullInfo = string.Empty;
        }

        public OutletFullModel(IUpdatableParentModel parentModel) : this(0) {
            SetParentModel(parentModel);
        }

        #endregion

        public bool AllowHtmlText {
            get { return _allowHtmlText; }
            set {
                if (value == _allowHtmlText)
                    return;
                _allowHtmlText = value;
                UpdateFullInfo();
            }
        }

        public string ColorForText {
            get { return _colorStr; }
            set {
                if (value == _colorStr)
                    return;
                _colorStr = value;
                UpdateFullInfo();
            }
        }

        private string ColorTag {
            get { return "<color=" + ColorForText + ">"; }
        }

        private string ColorTagEnd {
            get { return "</color>"; }
        }

        public string FullInfo {
            get { return _fullInfo; }
        }

        private void UpdateFullInfo() {
            _fullInfo = LineBreak + "Тип ТТ: " + (AllowHtmlText ? ColorTag + _typeName + ColorTagEnd : _typeName)
                + LineBreak + "Категория ТТ: " + (AllowHtmlText ? ColorTag + _category + ColorTagEnd : _typeName)
                + LineBreak + "Объем продаж: "
                + (_saleVolume == null ? "" : (AllowHtmlText ? ColorTag + string.Format("{0:n3} дал", _saleVolume) + ColorTagEnd : string.Format("{0:n3} дал", _saleVolume)))
                + LineBreak + "Среднее к-во отгрузок: "
                + (_avgShipmentCount == null ? "" : (AllowHtmlText ? ColorTag + string.Format("{0:n2}", _avgShipmentCount) + ColorTagEnd : string.Format("{0:n2}", _avgShipmentCount)))
                + LineBreak + "Среднее к-во визитов: "
                + (_avgVisitCount == null ? "" : (AllowHtmlText ? ColorTag + string.Format("{0:n2}", _avgVisitCount) + ColorTagEnd : string.Format("{0:n2}", _avgVisitCount)))
                + LineBreak + "Среднее СКЮ: "
                + (_averageSku == null ? null : (AllowHtmlText ? ColorTag + string.Format("{0}", _averageSku) + ColorTagEnd : string.Format("{0}", _averageSku)))
                + LineBreak + "StrikeRate (4 нед.): "
                + (_strikeRate == null ? "" : (AllowHtmlText ? ColorTag + string.Format("{0:n2} %", _strikeRate) + ColorTagEnd : string.Format("{0:n2} %", _strikeRate)));            
        }

        private string LineBreak {
            get { return AllowHtmlText ? "<br>" : "\n"; }
        }

        #region Implementation of IOutletFullModel

        public long Id {
            get { return _id; }
            set {
                if (_id.Equals(value))
                    return;
                _id = value;
            }
        }

        public string GetTypeName() {
            return _typeName;
        }

        public void SetTypeName(string typeName) {
            if (_typeName.Equals(typeName))
                return;
            _typeName = typeName;
            UpdateFullInfo();
        }

        public string GetCategory() {
            return _category;
        }

        public void SetCategory(string category) {
            if (_category.Equals(category))
                return;
            _category = category;
            UpdateFullInfo();
        }

        public decimal? GetSaleVolume() {
            return _saleVolume;
        }

        public void SetSaleVolume(decimal? saleVolume) {
            if (_saleVolume.Equals(saleVolume))
                return;
            if (saleVolume != null)
                _saleVolume = Math.Round((decimal) saleVolume, 3, MidpointRounding.AwayFromZero);
            else
                _saleVolume = null;
            UpdateFullInfo();
        }

        public decimal? GetAvgVisitCount() {
            return _avgVisitCount;
        }

        public void SetAvgVisitCount(decimal? avgVisitCount) {
            if (_avgVisitCount.Equals(avgVisitCount))
                return;
            if (avgVisitCount != null)
                _avgVisitCount = Math.Round((decimal) avgVisitCount, 2, MidpointRounding.AwayFromZero);
            else
                _avgVisitCount = null;
            UpdateFullInfo();
        }

        public decimal? GetAvgShipmentCount() {
            return _avgShipmentCount;
        }

        public void SetAvgShipmentCount(decimal? avgShipmentCount) {
            if (_avgShipmentCount.Equals(avgShipmentCount))
                return;
            if (avgShipmentCount != null)
                _avgShipmentCount = Math.Round((decimal) avgShipmentCount, 2, MidpointRounding.AwayFromZero);
            else
                _avgShipmentCount = null;
            UpdateFullInfo();
        }

        public decimal? GetAverageSku() {
            return _averageSku;
        }

        public void SetAverageSku(decimal? averageSku) {
            if (_averageSku.Equals(averageSku))
                return;
            _averageSku = averageSku;
            UpdateFullInfo();
        }

        public decimal? GetStrikeRate() {
            return _strikeRate;
        }

        public void SetStrikeRate(decimal? strikeRate) {
            if (_strikeRate.Equals(strikeRate))
                return;
            if (strikeRate != null)
                _strikeRate = Math.Round((decimal) strikeRate, 2, MidpointRounding.AwayFromZero);
            else
                _strikeRate = null;
            UpdateFullInfo();
        }

        #endregion
    }
}