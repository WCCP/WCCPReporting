﻿using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Outlet {
    public interface IOutletInRoute : IUpdatableParentModel {
        OutletModel Outlet { get; set; }

        int GetOlNumber();
        void SetOlNumber(int olNumber);
         
    }
}