﻿using System;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Supervisor {
    [Serializable]
    public class CustomerSimpleModel : SimpleModel {
        #region Constructors

        public CustomerSimpleModel() : this(0, string.Empty) {
        }

        public CustomerSimpleModel(int id) : base(id) {
        }

        public CustomerSimpleModel(int id, string name) : base(id, name) {
        }

        #endregion

        #region Properties

        public int Id {
            get { return GetId(); }
            set { SetId(value); }
        }

        public string Name {
            get { return GetName(); }
            set { SetName(value); }
        }

        #endregion
    }
}