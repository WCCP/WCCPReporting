﻿using System;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Supervisor {
    [Serializable]
    public class SupervisorModel : SimpleModel, ISupervisorModel {
        private int _custId;
        private Country _country;

        #region Constructors

        public SupervisorModel() {
        }

        public SupervisorModel(int id, string name) : base(id, name) {
        }

        public SupervisorModel(int id) : base(id) {
        }

        #endregion

        public int Id {
            get { return GetId(); }
            set { SetId(value); }
        }

        public string Name {
            get { return GetName(); }
            set { SetName(value); }
        }

        public int CustId {
            get { return GetCustId(); }
            set { SetCustId(value); }
        }
        
        #region Implementation of ISupervisorModel

        public int GetCustId() {
            return _custId;
        }

        public void SetCustId(int custId) {
            _custId = custId;
        }

        #endregion

        public Country GetCountry()
        {
            return _country;
        }

        public void SetCountry(Country country)
        {
            _country = country;
        }

        public override bool Equals(object obj)
        {
            var sm = obj as SupervisorModel;
            if (null == sm)
                return false;

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}