﻿namespace SoftServe.Reports.RouteManagement.Model.Supervisor
{
    public enum Country
    {
        Ukraine = 1,
        Russia = 2
    }
}