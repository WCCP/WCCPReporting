﻿using System.Collections.Generic;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Merch;

namespace SoftServe.Reports.RouteManagement.Model.Supervisor {
    public interface IM2Model : ISupervisorModel, IUpdatableParentModel {
        List<M1Model> GetM1List();
        void AddM1(M1Model m1);
    }
}