using System;
using System.Collections.Generic;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Merch;

namespace SoftServe.Reports.RouteManagement.Model.Supervisor {
    [Serializable]
    public class M2Model : UpdatableParentModel, IM2Model {
        #region Fields

        private bool _isLoaded;
        private int _id;
        private string _name;
        private int _custId;
        private List<M1Model> _m1List = new List<M1Model>(5);

        #endregion

        #region Constructors

        public M2Model() : this(0, string.Empty) {
        }

        public M2Model(int id) : this(id, string.Empty) {
        }

        public M2Model(int id, string name) {
            _isLoaded = false;
            _id = id;
            _name = name;
        }

        #endregion

        #region Properties

        public bool IsLoaded {
            get { return _isLoaded; }
            set { _isLoaded = value; }
        }

        public int Id {
            get { return GetId(); }
            set { SetId(value); }
        }

        public string Name {
            get { return GetName(); }
            set { SetName(value); }
        }

        public int CustId {
            get { return GetCustId(); }
            set { SetCustId(value); }
        }

        public List<M1Model> M1List {
            get { return _m1List; }
        }

        #endregion

        #region Implementation of ISimpleModel

        public int GetId() {
            return _id;
        }

        public void SetId(int id) {
            if (_id.Equals(id))
                return;
            _id = id;
            MakeModified();
        }

        public string GetName() {
            return _name;
        }

        public void SetName(string name) {
            if (_name.Equals(name))
                return;
            _name = name;
            MakeModified();
        }

        #endregion

        #region Implementation of IM2Model

        public List<M1Model> GetM1List() {
            return _m1List;
        }

        public void AddM1(M1Model m1) {
            if (_m1List.Contains(m1))
                return;
            m1.SetParentModel(this);
            _m1List.Add(m1);
            MakeModified();
        }

        #endregion

        #region Implementation of ISupervisorModel

        public int GetCustId() {
            return _custId;
        }

        public void SetCustId(int custId) {
            if (_custId.Equals(custId))
                return;
            _custId = custId;
            MakeModified();
        }

        #endregion
    }
}