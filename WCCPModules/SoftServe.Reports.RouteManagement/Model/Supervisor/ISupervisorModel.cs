﻿using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Supervisor {
    public interface ISupervisorModel : ISimpleModel {
        int GetCustId();
        void SetCustId(int custId);
    }
}