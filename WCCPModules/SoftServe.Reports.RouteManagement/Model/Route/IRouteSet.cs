﻿using System;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Route {
    public interface IRouteSet : IUpdatableModel {
        int RouteSetId { get; set; }
        int SupervisorId { get; set; }
        string SupervisorName { get; set; }
        DateTime RouteSetDate { get; set; }
        string Comment { get; set; }
        int CustId { get; set; }
        bool IsLoad { get; set; }

        int GetRouteSetId();
        void SetRouteSetId(int routeSetId);
        int GetSupervisorId();
        void SetSupervisorId(int supervisorId);
        string GetSupervisorName();
        void SetSupervisorName(string supervisorName);
        DateTime GetRouteSetDate();
        void SetRouteSetDate(DateTime date);
        string GetComment();
        void SetComment(string comment);
        int GetCustId();
        void SetCustId(int custId);
        bool GetIsLoad();
        void SetIsLoad(bool isLoad);
    }
}