﻿using System;
using System.Diagnostics;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Route {
    [Serializable]
    public class RouteModel : BaseModel, IRouteModel {
        #region Fields

        private long _routeId;
        private string _routeName;
        private int _merchId;

        #endregion

        #region Constructors

        public RouteModel() : this(0, string.Empty) {
        }

        public RouteModel(long routeId) : this(routeId, string.Empty) {
        }

        public RouteModel(long routeId, string routeName) {
            _routeId = routeId;
            _routeName = routeName;
        }

        #endregion

        public long RouteId {
            get { return GetRouteId(); }
            set { SetRouteId(value); }
        }

        public string RouteName {
            get { return GetRouteName(); }
            set { SetRouteName(value); }
        }

        public int MerchId {
            get { return GetMerchId(); }
            set { SetMerchId(value); }
        }

        #region Implementation of IRouteModel

        public long GetRouteId() {
            return _routeId;
        }

        public void SetRouteId(long routeId) {
            _routeId = routeId;
        }

        public string GetRouteName() {
            return _routeName;
        }

        public void SetRouteName(string routeName) {
            _routeName = routeName;
        }

        public int GetMerchId() {
            return _merchId;
        }

        public void SetMerchId(int merchId) {
            _merchId = merchId;
        }

        #endregion

        public override bool Assign(IBaseModel model) {
            IRouteModel lModel = model as IRouteModel;
            if (null == lModel)
                return false;
            SetRouteId(lModel.GetRouteId());
            SetRouteName(lModel.GetRouteName());
            SetMerchId(lModel.GetMerchId());
            return true;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof (RouteModel))
                return false;
            return Equals((RouteModel) obj);
        }

        public bool Equals(RouteModel other) {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return other._routeId == _routeId && Equals(other._routeName, _routeName) && other._merchId == _merchId;
        }

        public override int GetHashCode() {
            unchecked {
                int lResult = _routeId.GetHashCode();
                lResult = (lResult * 397) ^ (_routeName != null ? _routeName.GetHashCode() : 0);
                lResult = (lResult * 397) ^ _merchId;
                return lResult;
            }
        }
    }
}