using System;
using System.Collections.Generic;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using SoftServe.Reports.RouteManagement.View;

namespace SoftServe.Reports.RouteManagement.Model.Route {
    [Serializable]
    public class RouteWithOutletsModel : UpdatableParentModel, IRouteWithOutletsModel, IComparable {
        #region Fields

        private long _routeId;
        private string _routeName;
        private int _merchId;
        private readonly List<OutletInRoute> _outlets = new List<OutletInRoute>(10);
        private readonly List<OutletInRoute> _deletedOutlets = new List<OutletInRoute>();
        private decimal _distance;
        private decimal _optDistance;
        private decimal _preDistance;
        private decimal _preOptDistance;
        private long _durationMin;
        private bool _isOptimized;
        private bool _hasPreDistance;
        private bool _hasPreOptDistance;
        private GMapMarkerImage _startMarker;

        #endregion

        #region Constructors

        public RouteWithOutletsModel() : this(0, string.Empty) {
        }

        public RouteWithOutletsModel(long routeId) : this(routeId, string.Empty) {
        }

        public RouteWithOutletsModel(long routeId, string routeName) {
            _routeId = routeId;
            _routeName = routeName;
            _merchId = 0;
            _distance = 0;
            _optDistance = 0;
            _durationMin = 0;
            _isOptimized = false;
        }

        #endregion

        public List<OutletInRoute> Outlets {
            get { return _outlets; }
        }

        private void RecalcDuration() {
            // [�����������������] = 60 * [�������������] / 45 + [�-�� ��] * 7   (������)
            // if route is optimized use OptDistance else use Distance
            //decimal lDistance = _isOptimized ? _optDistance : _distance;
            // if route is optimized OptDistance = Distance
            decimal lDistance = _distance;
            decimal lMinutes = lDistance <= 0 ? -1 : 60m * lDistance / 45m + 12 * CountTt; //Ticket#1027404 � Route Management ���������� ��������� ����� ������ � �� � 7 �� 12 �����
            _durationMin = Convert.ToInt64(Math.Round(lMinutes, MidpointRounding.AwayFromZero));
        }

        #region Implementation of IRouteWithOutletsModel

        public decimal PreDistance
        {
            get { return _preDistance; }
            set { SetPreDistance(value); }
        }

        public decimal PreOptDistance
        {
            get { return _preOptDistance; }
            set { SetPreOptDistance(value); }
        }

        public bool HasPreDistance
        {
            get { return _hasPreDistance; }
            set { _hasPreDistance = value; }
        }

        public bool HasPreOptDistance
        {
            get { return _hasPreOptDistance; }
            set { _hasPreOptDistance = value; }
        }

        public int CountTt {
            get { return _outlets.Count; }
        }

        public decimal Distance {
            get { return GetDistance(); }
            set { SetDistance(value); }
        }

        public decimal OptDistance {
            get { return GetOptDistance(); }
            set { SetOptDistance(value); }
        }

        public long Duration {
            get { return _durationMin; }
        }

        public bool IsOptimized {
            get { return GetIsOptimized(); }
            set { SetIsOptimized(value); }
        }

        public GMapMarkerImage StartMarker {
            get { return _startMarker; }
            set { _startMarker = value; }
        }

        public bool IsStartMarker {
            get { return _startMarker != null; }
        }

        public decimal GetDistance() {
            return _distance;
        }

        public void SetDistance(decimal distance) {
            if (_distance.Equals(distance))
                return;
            _distance = distance;
            RecalcDuration();
            //MakeModified();
        }

        public decimal GetOptDistance() {
            return _optDistance;
        }

        public void SetOptDistance(decimal distance) {
            if (_optDistance.Equals(distance))
                return;
            if (distance <= 0 && _isOptimized)
                _optDistance = _distance;
            else
                _optDistance = distance;
            RecalcDuration();
            //MakeModified();
        }

        public bool GetIsOptimized() {
            return _isOptimized;
        }

        public void SetIsOptimized(bool isOptimized) {
            if (_isOptimized.Equals(isOptimized))
                return;
            _isOptimized = isOptimized;
            RecalcDuration();
            MakeModified();
        }

        public bool AddOutlet(OutletInRoute outletInRoute) {
            return InsertOutlet(-1, outletInRoute);
        }

        public bool InsertOutlet(int index, OutletInRoute outletInRoute) {
            outletInRoute.UnDelete();
            if (_outlets.Contains(outletInRoute))
                return false;

            if (index < 0)
                _outlets.Add(outletInRoute);
            else
                _outlets.Insert(index, outletInRoute);
            outletInRoute.SetParentModel(this);
            MakeModified();
            SetIsOptimized(false);
            RenumberOutlets();
            return true;
        }

        public bool RemoveOutlet(OutletInRoute outletInRoute) {
            if (!_outlets.Contains(outletInRoute))
                return false;

            outletInRoute.Delete();
            MakeModified();
            SetIsOptimized(false);
            _outlets.Remove(outletInRoute);
            RenumberOutlets();
            outletInRoute.Outlet.RemoveRoute(_routeId);
            outletInRoute.Outlet.UpdateMarkerToolTip(0);
            return true;

//            if (_deletedOutlets.Contains(outletInRoute))
//                return;
//            _deletedOutlets.Add(outletInRoute);
        }

        public void RenumberOutlets() {
            _isOptimized = false;
            for (int lI = 0; lI < _outlets.Count; lI++) {
                OutletInRoute lOutletInRoute = _outlets[lI];
                lOutletInRoute.SetOlNumber(lI + 1);
                lOutletInRoute.Outlet.UpdateMarkerToolTip(lI + 1);
            }
        }

        public void SetPreDistance(object preDistance)
        {
            if (preDistance != DBNull.Value)
            {
                _preDistance = Convert.ToDecimal(preDistance);
                _hasPreDistance = true;
            }
            else
            {
                _hasPreDistance = false;
            }
        }

        public void SetPreOptDistance(object preOptDistance)
        {
            if (preOptDistance != DBNull.Value)
            {
                _preOptDistance = Convert.ToDecimal(preOptDistance);
                _hasPreOptDistance = true;
            }
            else
            {
                _hasPreOptDistance = false;
            }
        }

        /// <summary>
        /// Used for initial load data. Omits set parent for models.
        /// </summary>
        /// <param name="outletList"> </param>
        public void AddOutletRange(List<OutletInRoute> outletList) {
            _outlets.AddRange(outletList);
            MakeModified();
            SetIsOptimized(false);
            RenumberOutlets();
        }

        #endregion

        #region Implementation of IRouteModel

        public long RouteId {
            get { return GetRouteId(); }
            set { SetRouteId(value); }
        }

        public string RouteName {
            get { return GetRouteName(); }
            set { SetRouteName(value); }
        }

        public int MerchId {
            get { return GetMerchId(); }
            set { SetMerchId(value); }
        }

        public long GetRouteId() {
            return _routeId;
        }

        public void SetRouteId(long routeId) {
            if (_routeId.Equals(routeId))
                return;
            _routeId = routeId;
            MakeModified();
        }

        public string GetRouteName() {
            return _routeName;
        }

        public void SetRouteName(string routeName) {
            if (_routeName.Equals(routeName))
                return;
            _routeName = routeName;
            MakeModified();
        }

        public int GetMerchId() {
            return _merchId;
        }

        public void SetMerchId(int merchId) {
            if (_merchId.Equals(merchId))
                return;
            _merchId = merchId;
            MakeModified();
        }

        #endregion

        public bool OutletExists(OutletModel outlet) {
            return -1 != GetOutletNumber(outlet);
        }

        public int GetOutletNumber(OutletModel outlet) {
            int lNumber = -1;
            foreach (OutletInRoute lOutletInRoute in _outlets)
                if (lOutletInRoute.Id == outlet.Id) {
                    lNumber = lOutletInRoute.OlNumber;
                    break;
                }
            return lNumber;
        }

        public void ReverseRoute() {
            List<OutletInRoute> lNewOutletsList = new List<OutletInRoute>(_outlets.Count);
            int lIndex = 0;
            OutletInRoute lOutletInRoute;
            for (int lI = _outlets.Count - 1; lI >= 0; lI--) {
                lOutletInRoute = _outlets[lI];
                lNewOutletsList.Insert(lIndex, lOutletInRoute);
                lOutletInRoute.SetOlNumber(lIndex + 1);
                lIndex++;
            }
            _outlets.Clear();
            _outlets.AddRange(lNewOutletsList);
            MakeModified();
            SetIsOptimized(false);
        }

        #region Implementation of IComparable

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer
        /// that indicates whether the current instance precedes, follows, or occurs in the same position
        /// in the sort order as the other object.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared.
        /// The return value has these meanings: 
        ///     Value - Meaning 
        ///     Less than zero - This instance is less than <paramref name="obj"/>. 
        ///     Zero - This instance is equal to <paramref name="obj"/>. 
        ///     Greater than zero - This instance is greater than <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. 
        ///                 </param><exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. 
        ///                 </exception><filterpriority>2</filterpriority>
        public int CompareTo(object obj) {
            RouteWithOutletsModel lRoute = obj as RouteWithOutletsModel;
            if (null == lRoute)
                return 1;
            if (lRoute.RouteId == 0 && _routeId == 0)
                return 0;
            if (_routeId == 0)
                return 1;
            if (lRoute.RouteId == 0)
                return -1;
            return string.CompareOrdinal(_routeName, lRoute.RouteName);
        }

        #endregion
    }
}