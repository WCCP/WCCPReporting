﻿using System.Collections.Generic;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Outlet;
using SoftServe.Reports.RouteManagement.View;

namespace SoftServe.Reports.RouteManagement.Model.Route {
    public interface IRouteWithOutletsModel : IRouteModel, IUpdatableParentModel {
        int CountTt { get; }
        decimal Distance { get; set; }
        decimal OptDistance { get; set; }
        decimal PreDistance { get; set; }
        decimal PreOptDistance { get; set; }
        bool HasPreDistance { get; }
        bool HasPreOptDistance { get; }
        long Duration { get; }
        bool IsOptimized { get; set; }
        GMapMarkerImage StartMarker { get; set; }
        bool IsStartMarker { get; }

        decimal GetDistance();
        void SetDistance(decimal distance);
        decimal GetOptDistance();
        void SetOptDistance(decimal distance);
        void SetPreDistance(object preDistance);
        void SetPreOptDistance(object preOptDistance);
        bool GetIsOptimized();
        void SetIsOptimized(bool isOptimized);

        bool AddOutlet(OutletInRoute outletInRoute);
        bool InsertOutlet(int index, OutletInRoute outletInRoute);
        void AddOutletRange(List<OutletInRoute> outletList);
    }
}