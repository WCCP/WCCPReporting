﻿using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Route {
    public interface IRouteModel : IBaseModel {
        long RouteId { get; set; }
        string RouteName { get; set; }
        int MerchId { get; set; }

        long GetRouteId();
        void SetRouteId(long routeId);
        string GetRouteName();
        void SetRouteName(string routeName);
        int GetMerchId();
        void SetMerchId(int merchId);
    }
}