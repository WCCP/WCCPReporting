using System;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Route {
    [Serializable]
    public class RouteSet : UpdatableModel, IRouteSet {
        #region Fields

        private int _routeSetId;
        private int _supervisorId;
        private string _supervisorName;
        private int _custId;
        private DateTime _routeSetDate;
        private string _comment;
        private bool _isLoad;

        #endregion

        public RouteSet() : this(0) {
        }

        public RouteSet(int routeSetId) {
            _routeSetId = routeSetId;
            _supervisorId = 0;
            _supervisorName = string.Empty;
            _custId = 0;
            _routeSetDate = DateTime.Now;
            _comment = string.Empty;
            _isLoad = false;
        }

        #region Implementation of IRouteSet

        public int RouteSetId {
            get { return GetRouteSetId(); }
            set { SetRouteSetId(value); }
        }

        public int SupervisorId {
            get { return GetSupervisorId(); }
            set { SetSupervisorId(value); }
        }

        public string SupervisorName {
            get { return GetSupervisorName(); }
            set { SetSupervisorName(value); }
        }

        public DateTime RouteSetDate {
            get { return GetRouteSetDate(); }
            set { SetRouteSetDate(value); }
        }

        public string Comment {
            get { return GetComment(); }
            set { SetComment(value); }
        }

        public int CustId {
            get { return GetCustId(); }
            set { SetCustId(value); }
        }

        public bool IsLoad {
            get { return GetIsLoad(); }
            set { SetIsLoad(value); }
        }

        public int GetRouteSetId() {
            return _routeSetId;
        }

        public void SetRouteSetId(int routeSetId) {
            if (_routeSetId == routeSetId)
                return;
            _routeSetId = routeSetId;
            MakeModified();
        }

        public int GetSupervisorId() {
            return _supervisorId;
        }

        public void SetSupervisorId(int supervisorId) {
            if (_supervisorId == supervisorId)
                return;
            _supervisorId = supervisorId;
            MakeModified();
        }

        public string GetSupervisorName() {
            return _supervisorName;
        }

        public void SetSupervisorName(string supervisorName) {
            if (_supervisorName == supervisorName)
                return;
            _supervisorName = supervisorName;
            MakeModified();
        }

        public DateTime GetRouteSetDate() {
            return _routeSetDate;
        }

        public void SetRouteSetDate(DateTime date) {
            if (_routeSetDate.Equals(date))
                return;
            _routeSetDate = date;
            MakeModified();
        }

        public string GetComment() {
            return _comment;
        }

        public void SetComment(string comment) {
            if (_comment.Equals(comment))
                return;
            _comment = comment;
            MakeModified();
        }

        public int GetCustId() {
            return _custId;
        }

        public void SetCustId(int custId) {
            if (_custId.Equals(custId))
                return;
            _custId = custId;
            MakeModified();
        }

        public bool GetIsLoad() {
            return _isLoad;
        }

        public void SetIsLoad(bool isLoad) {
            if (_isLoad.Equals(isLoad))
                return;
            _isLoad = isLoad;
            MakeModified();
        }

        #endregion
    }
}