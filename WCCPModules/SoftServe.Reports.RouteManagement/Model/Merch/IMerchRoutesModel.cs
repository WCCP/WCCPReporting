﻿using System.Collections.Generic;
using SoftServe.Reports.RouteManagement.Model.Route;

namespace SoftServe.Reports.RouteManagement.Model.Merch {
    public interface IMerchRoutesModel : IMerchModel {
        List<RouteModel> Routes { get; }
        string MerchRoutesInfo { get; }

        void AddRoute(RouteModel route);
        void RemoveRoute(RouteModel route);

        //void AddRoute(long routeId);
        void RemoveRoute(long routeId);
    }
}