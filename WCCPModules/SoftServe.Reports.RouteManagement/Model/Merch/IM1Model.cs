using System.Collections.Generic;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Route;

namespace SoftServe.Reports.RouteManagement.Model.Merch {
    public interface IM1Model : IMerchModel, IUpdatableParentModel {
        int Id { get; set; }
        string Name { get; set; }
        List<RouteWithOutletsModel> Routes { get; }

        List<RouteWithOutletsModel> GetRouteList();
        void AddRoute(RouteWithOutletsModel routeWithOutlets);
    }
}