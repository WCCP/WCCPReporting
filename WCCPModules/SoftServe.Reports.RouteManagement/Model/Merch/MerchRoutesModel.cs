using System.Collections.Generic;
using System.Text;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Route;

namespace SoftServe.Reports.RouteManagement.Model.Merch {
    public class MerchRoutesModel : MerchModel, IMerchRoutesModel {
        private readonly List<RouteModel> _routes = new List<RouteModel>();
        private string _merchRoutesInfo = string.Empty;

        private const string DELIMITER_ROUTES_LIST = ", ";

        public MerchRoutesModel() {
        }

        public MerchRoutesModel(int id) : base(id) {
        }

        public MerchRoutesModel(int id, string name) : base(id, name) {
        }

        public MerchRoutesModel(MerchModel merch, RouteModel route) {
            SetId(merch.Id);
            SetName(merch.Name);
            SetSupervisorId(merch.SupervisorId);
            SetLatitude(merch.Lat);
            SetLongitude(merch.Lon);
            AddRoute(route);
        }

        #region Implementation of IBaseModel

        public override bool Assign(IBaseModel model) {
            IMerchRoutesModel lModel = model as IMerchRoutesModel;
            if (null == lModel)
                return false;
            
            SetId(lModel.GetId());
            SetName(lModel.GetName());
            SetSupervisorId(lModel.GetSupervisorId());
            SetLatitude(lModel.GetLatitude());
            SetLongitude(lModel.GetLongitude());
            _routes.Clear();
            for (int lI = 0; lI < lModel.Routes.Count; lI++) {
                _routes.Add(lModel.Routes[lI]);
            }
            return true;
        }

        public override IBaseModel Clone() {
            MerchRoutesModel lModel = new MerchRoutesModel(GetId());
            lModel.Assign(this);
            return lModel;
        }

        #endregion

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            return Equals(obj as MerchRoutesModel);
        }

        #region Implementation of IMerchRoutesModel

        public List<RouteModel> Routes {
            get { return _routes; }
        }

        public string MerchRoutesInfo {
            get { return Name + ": " + _merchRoutesInfo; }
        }

        private void UpdateRouteInfo() {
            StringBuilder lListBuilder = new StringBuilder(10);
            for (int lI = 0; lI < _routes.Count; lI++)
                lListBuilder.AppendFormat("{0}{1}", _routes[lI].RouteName, DELIMITER_ROUTES_LIST);
            _merchRoutesInfo = lListBuilder.ToString().TrimEnd(DELIMITER_ROUTES_LIST.ToCharArray());
        }

        public void AddRoute(RouteModel route) {
            if (_routes.Contains(route))
                return;
            _routes.Add(route);
            UpdateRouteInfo();
        }

        public void RemoveRoute(RouteModel route) {
            if (!_routes.Contains(route))
                return;
            _routes.Remove(route);
            UpdateRouteInfo();
        }

        public void RemoveRoute(long routeId) {
            foreach (RouteModel lRoute in _routes) {
                if (lRoute.RouteId == routeId) {
                    _routes.Remove(lRoute);
                    UpdateRouteInfo();
                    break;
                }
            }
        }

        #endregion

        public bool Equals(MerchRoutesModel other) {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return base.Equals(other) && Equals(other._routes, _routes) && Equals(other._merchRoutesInfo, _merchRoutesInfo);
        }

        public override int GetHashCode() {
            unchecked {
                int lResult = base.GetHashCode();
                lResult = (lResult * 397) ^ (_routes != null ? _routes.GetHashCode() : 0);
                lResult = (lResult * 397) ^ (_merchRoutesInfo != null ? _merchRoutesInfo.GetHashCode() : 0);
                return lResult;
            }
        }
    }
}