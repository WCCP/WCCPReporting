﻿using System;
using System.Diagnostics;
using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Merch {
    [Serializable]
    public class MerchModel : SimpleModel, IMerchModel {
        private int _supervisorId;
        private double? _lat;
        private double? _lon;

        public MerchModel() {
        }

        public MerchModel(int id) : base(id) {
        }

        public MerchModel(int id, string name) : base(id, name) {
        }

        #region Properties

        public int SupervisorId {
            get { return _supervisorId; }
            set { _supervisorId = value; }
        }

        public int Id {
            get { return GetId(); }
            set { SetId(value); }
        }

        public string Name {
            get { return GetName(); }
            set { SetName(value); }
        }

        public double? Lat {
            get { return GetLatitude(); }
            set { SetLatitude(value); }
        }

        public double? Lon {
            get { return GetLongitude(); }
            set { SetLongitude(value); }
        }

        #endregion

        #region Implementation of IMerchModel

        public int GetSupervisorId() {
            return _supervisorId;
        }

        public void SetSupervisorId(int svId) {
            _supervisorId = svId;
        }

        public double? GetLatitude() {
            return _lat;
        }

        public void SetLatitude(double? lat) {
            _lat = lat;
        }

        public double? GetLongitude() {
            return _lon;
        }

        public void SetLongitude(double? lon) {
            _lon = lon;
        }

        #endregion

        public override bool Assign(IBaseModel model) {
            IMerchModel lModel = model as IMerchModel;
            if (null == lModel)
                return false;
            
            SetId(lModel.GetId());
            SetName(lModel.GetName());
            SetSupervisorId(lModel.GetSupervisorId());
            SetLatitude(lModel.GetLatitude());
            SetLongitude(lModel.GetLongitude());
            return true;
        }

        public override bool Equals(object obj) {
            Debug.Assert(obj != null, "Object should not be null");
            MerchModel lModel = obj as MerchModel;
            if (null == lModel)
                return false;
            
            return base.Equals(obj) &&
                   _supervisorId.Equals(lModel.GetSupervisorId()) &&
                   _lat.Equals(lModel.GetLatitude()) &&
                   _lon.Equals(lModel.GetLongitude());
        }

        public override IBaseModel Clone() {
            MerchModel lModel = new MerchModel(GetId());
            lModel.Assign(this);
            return lModel;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}