﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using SoftServe.Core.Common.Model;
using SoftServe.Reports.RouteManagement.Model.Route;
using SoftServe.Reports.RouteManagement.View;

namespace SoftServe.Reports.RouteManagement.Model.Merch {
    [Serializable]
    public class M1Model : UpdatableParentModel, IM1Model, IComparable {
        #region Fields

        private int _id;
        private string _name;
        private int _supervisorId;
        private double? _lat;
        private double? _lon;
        private GMapMarkerImage _marker;

        private readonly List<RouteWithOutletsModel> _routes = new List<RouteWithOutletsModel>();

        #endregion

        #region Constructors

        public M1Model() : this(0, string.Empty) {
        }

        public M1Model(int id) : this(id, string.Empty) {
        }

        public M1Model(int id, string name) {
            _id = id;
            _name = name;
            _supervisorId = 0;
        }

        #endregion

        #region Properties

        public int Id {
            get { return _id; }
            set { _id = value; }
        }

        public string Name {
            get { return _name; }
            set { _name = value; }
        }

        public double? Latitude {
            get { return GetLatitude(); }
            set { SetLatitude(value); }
        }

        public double? Longitude {
            get { return GetLongitude(); }
            set { SetLongitude(value); }
        }

        public List<RouteWithOutletsModel> Routes {
            get { return _routes; }
        }

        public GMapMarkerImage Marker {
            get { return _marker; }
        }

        public bool IsMarker {
            get { return _marker != null; }
        }

        #endregion

        #region Implementation of ISimpleModel

        public int GetId() {
            return _id;
        }

        public void SetId(int id) {
            if (_id.Equals(id))
                return;
            _id = id;
            MakeModified();
        }

        public string GetName() {
            return _name;
        }

        public void SetName(string name) {
            if (_name.Equals(name))
                return;
            _name = name;
            MakeModified();
        }

        #endregion

        #region Implementation of IMerchModel

        public int SupervisorId {
            get { return GetSupervisorId(); }
            set { SetSupervisorId(value); }
        }

        public double? Lat {
            get { return GetLatitude(); }
            set { SetLatitude(value); }
        }

        public double? Lon {
            get { return GetLongitude(); }
            set { SetLongitude(value); }
        }

        public int GetSupervisorId() {
            return _supervisorId;
        }

        public void SetSupervisorId(int svId) {
            if (_supervisorId.Equals(svId))
                return;
            _supervisorId = svId;
            MakeModified();
        }

        public double? GetLatitude() {
            return _lat;
        }

        public void SetLatitude(double? lat) {
            if (_lat == null && lat == null)
                return;
            if (_lat != null && _lat.Equals(lat))
                return;
            _lat = lat;
            MakeModified();
        }

        public double? GetLongitude() {
            return _lon;
        }

        public void SetLongitude(double? lon) {
            if (_lon == null && lon == null)
                return;
            if (_lon != null && _lon.Equals(lon))
                return;
            _lon = lon;
            MakeModified();
        }

        #endregion

        #region Implementation of IM1Model

        public List<RouteWithOutletsModel> GetRouteList() {
            return _routes;
        }

        public void AddRoute(RouteWithOutletsModel routeWithOutlets) {
            routeWithOutlets.SetParentModel(this);
            _routes.Add(routeWithOutlets);
            MakeModified();
        }

        #endregion

        public override bool Assign(IBaseModel model) {
            IM1Model lModel = model as IM1Model;
            if (null == lModel)
                return false;
            
            SetId(lModel.GetId());
            SetName(lModel.GetName());
            SetSupervisorId(lModel.GetSupervisorId());
            SetLatitude(lModel.GetLatitude());
            SetLongitude(lModel.GetLongitude());
            _routes.Clear();
            for (int lI = 0; lI < lModel.Routes.Count; lI++) {
                _routes.Add(lModel.Routes[lI]);
            }
            return true;
        }

        public void CreateMarker(Image image) {
            if (_lat == null || _lon == null)
                return;
            double lLat = (double) _lat;
            double lLon = (double) _lon;

            _marker = new GMapMarkerImage(new PointLatLng(lLat, lLon), image);
            _marker.Tag = Id;
            _marker.ToolTipMode = MarkerTooltipMode.OnMouseOver;
            _marker.ToolTipText = Name;
        }

        public bool RouteExists(long routeId) {
            foreach (RouteWithOutletsModel lRoute in _routes)
                if (lRoute.RouteId == routeId)
                    return true;
            return false;
        }

        #region Implementation of IComparable

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: 
        ///                     Value 
        ///                     Meaning 
        ///                     Less than zero 
        ///                     This instance is less than <paramref name="obj"/>. 
        ///                     Zero 
        ///                     This instance is equal to <paramref name="obj"/>. 
        ///                     Greater than zero 
        ///                     This instance is greater than <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. 
        ///                 </param><exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. 
        ///                 </exception><filterpriority>2</filterpriority>
        public int CompareTo(object obj) {
            M1Model lM1Model = obj as M1Model;
            if (null == lM1Model)
                return 1;
            //return string.CompareOrdinal(_name, lM1Model.Name);
            return string.Compare(_name, lM1Model.Name, true, new CultureInfo(0x0419));
        }

        #endregion
    }
}