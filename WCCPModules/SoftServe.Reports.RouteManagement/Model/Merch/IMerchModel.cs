﻿using SoftServe.Core.Common.Model;

namespace SoftServe.Reports.RouteManagement.Model.Merch {
    public interface IMerchModel : ISimpleModel {
        int SupervisorId { get; set; }
        double? Lat { get; set; }
        double? Lon { get; set; }

        int GetSupervisorId();
        void SetSupervisorId(int svId);
        double? GetLatitude();
        void SetLatitude(double? lat);
        double? GetLongitude();
        void SetLongitude(double? lon);
    }
}