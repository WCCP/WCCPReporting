﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.DataAccess;
using Logica.Reports.wccpM3OnTrade.DataProvider;
using Logica.Reports.wccpM3OnTrade.Properties;

namespace Logica.Reports.wccpM3OnTrade
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SettingsForm : XtraForm
    {
        public Int32 DsmId { get { return (int)lookUpEditDSM.EditValue; } }
        public String DsmName { get { return lookUpEditDSM.Properties.GetDisplayText(lookUpEditDSM.EditValue); } }
        public DateTime ReportDate { get { return dateEdit.DateTime; } }
        private IDictionary<string, string> dict = new Dictionary<string, string>();

        private Report Report { get; set; }

        private int ReportId { get; set; }

        public SettingsForm(Report report)
        {
            InitializeComponent();
            Report = report;
        }

        public SettingsForm(int reportId)
        {
            InitializeComponent();
            ReportId = reportId;
        }
        public void SetParent(Report parent)
        {
            Report = parent;
        }

        public bool IsRatingsRequired
        {
            get
            {
                return rbType.SelectedIndex == 1 
                    && dateEdit.DateTime.DayOfWeek != DayOfWeek.Saturday 
                    && dateEdit.DateTime.DayOfWeek != DayOfWeek.Sunday;
            }
        }


        private void M3FSMSettingsForm_Load(object sender, EventArgs e)
        {
            lookUpEditDSM.Properties.DisplayMember = Resources.SQL_DSM_FLD_NAME;
            lookUpEditDSM.Properties.ValueMember = Resources.SQL_DSM_FLD_ID;
            lookUpEditDSM.Properties.Columns[0].FieldName = Resources.SQL_DSM_FLD_ID;
            lookUpEditDSM.Properties.Columns[0].Visible = false;
            lookUpEditDSM.Properties.Columns[1].FieldName = Resources.SQL_DSM_FLD_NAME;
            lookUpEditDSM.Properties.Columns[1].Visible = true;
            rbType.SelectedIndex = 1;
            UpdateControls();
        }

        private void UpdateControls()
        {
            if (string.IsNullOrEmpty(dateEdit.Text))
            {
                dateEdit.DateTime = DateTime.Now;
            }
            FillM3();
        }

        private void FillM3()
        {
            DataTable m3Users = DataAccessProvider.M3Users;

            lookUpEditDSM.Properties.DataSource = m3Users;
            if (m3Users.Rows.Count > 0)
                lookUpEditDSM.EditValue = m3Users.Rows[0][Resources.SQL_DSM_FLD_ID];
            GetDefaultSettings();
        }

        public List<SheetParamCollection> SheetParamsList
        {

            get
            {
                List<SheetParamCollection> listParams = new List<SheetParamCollection>();
                foreach (Tab tab in Report.Tabs)
                {
                    if (tab.Tables.Any(i => i.Type == (rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact)))
                    {
                        SheetParamCollection param = SheetParamCollection;
                        param.TabId = tab.Id;

                        listParams.Add(param);
                    }
                }

                return listParams;
            }
        }

        public SheetParamCollection SheetParamCollection
        {
            get
            {
                DataTable dataTableDate = DataAccessProvider.GetDataInformation(dateEdit.DateTime);
                SheetParamCollection param = new SheetParamCollection
                {
                    MainHeader = Resources.MainHeader,
                    TableDataType = rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact
                };
                param.Add(new SheetParam { DisplayParamName = Resources.ReportDate, SqlParamName = Resources.SQL_Sheet_Date, Value = ReportDate.Date, DisplayValue = ReportDate.ToShortDateString() });
                param.Add(new SheetParam { DisplayParamName = Resources.M3, SqlParamName = Resources.SQL_Sheet_DSM, Value = DsmId, DisplayValue = DsmName });
                param.Add(new SheetParam { DisplayParamName = Resources.ReportMounth, DisplayValue = dateEdit.DateTime.Date.ToString("MMMM, yyyy") });
                param.Add(new SheetParam { DisplayParamName = Resources.WorkDay, DisplayValue = DaysToString(ConvertEx.ToDecimal(dataTableDate.Rows[0][Resources.SQL_DateInfo_FLD_WorkDay])) });
                param.Add(new SheetParam { DisplayParamName = Resources.WorkDays, DisplayValue = DaysToString(ConvertEx.ToDecimal(dataTableDate.Rows[0][Resources.SQL_DateInfo_FLD_WorkDaysInMonth])) });

                return param;
            }
        }

        private static string DaysToString(decimal value)
        {
            return value.ToString(value % 1 == 0 ? "N0" : "N1");
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (rbType.SelectedIndex == 0 && (dateEdit.DateTime.Month < DateTime.Now.Month
                || dateEdit.DateTime.Year < DateTime.Now.Year))
            {
                XtraMessageBox.Show(Resources.Error_Date);
                //DialogResult = DialogResult.Retry;
            }
            else if (lookUpEditDSM == null)
            {
                XtraMessageBox.Show(Resources.M3_error);
            }
            else
            {
                dict.Clear();
                dict.Add(Resources.Settings_DSM, DsmId.ToString());
                dict.Add(Resources.Settings_PlanOrFact, rbType.SelectedIndex.ToString());
                dict.Add(Resources.Settings_Date, ReportDate.Date.ToShortDateString());
                BaseReportControl.Utility.SettingsAccessor.SaveSettings(dict, ReportId);
                DialogResult = DialogResult.OK;
            }
        }

        private void GetDefaultSettings()
        {
            dict = BaseReportControl.Utility.SettingsAccessor.GetSettings(ReportId);
            string strId = (dict.ContainsKey(Resources.Settings_DSM)) ? dict[Resources.Settings_DSM] : "";
            string strPlanOrFact = (dict.ContainsKey(Resources.Settings_PlanOrFact)) ? dict[Resources.Settings_PlanOrFact] : "";
            string strDate = (dict.ContainsKey(Resources.Settings_Date)) ? dict[Resources.Settings_Date] : "";
            int id;
            int planOrFact;
            DateTime date;

            if (!string.IsNullOrEmpty(strId) && Int32.TryParse(strId, out id) && lookUpEditDSM.Properties.GetDataSourceRowByKeyValue(id) != null)
            {
                lookUpEditDSM.EditValue = id;
            }
            if (!string.IsNullOrEmpty(strPlanOrFact) && Int32.TryParse(strPlanOrFact, out planOrFact) && (planOrFact == 0 || planOrFact == 1))
            {
                rbType.SelectedIndex = planOrFact;
            }
            if (!string.IsNullOrEmpty(strDate) && DateTime.TryParse(strDate, out date))
            {
                dateEdit.DateTime = date;
            }
        }

    }

    /// <summary>
    /// ComboBoxItem
    /// </summary>
    internal class ComboBoxItem
    {
        public object Id { get; set; }

        public string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
