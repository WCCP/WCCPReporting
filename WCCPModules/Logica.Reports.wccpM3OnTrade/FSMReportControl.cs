﻿#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common;
using Logica.Reports.wccpM3OnTrade;
using Logica.Reports.wccpM3OnTrade.Properties;
using Logica.Reports.wccpM3OnTrade.UserControls;

#endregion

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        #region Readonly & Static Fields

        public static WccpUIControl ReportControl;
        private static List<SettingsForm> forms;
        private readonly SettingsForm setForm;

        #endregion

        #region Fields

        private bool isRatingsRequired;
        private RaitingsTabPage ratingsReportPage = null;

        #endregion

        #region Constructors

        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            if (GetForm() != null)
            {
                setForm = GetForm();
                setForm.SetParent(Report);
                off_reports = true;
                isRatingsRequired = setForm.IsRatingsRequired;
                if (isRatingsRequired)
                {
                    ratingsReportPage = new RaitingsTabPage();
                }

                UpdateTabs();
                foreach (XtraTabPage page in tabManager.TabPages)
                {
                    BaseTab tab = page as BaseTab;
                    if (null != tab)
                    {
                        tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
                    }
                }

                UpdateEventHandlers();
                CustomSettings();
            }
            //this.ExportClick += new ExportMenuClickHandler(WccpUIControl_ExportClick);
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            MenuCloseClick += WccpUIControl_CloseClick;
            RefreshClick += WccpUIControl_RefreshClick;
            PrepareExport += WccpUIControl_PrepareExport;
            ReportControl = this;
        }

        CompositeLink WccpUIControl_PrepareExport(object sender, XtraTabPage selectedPage, ExportToType type)
        {
            CompositeLink cl = null;
            if (selectedPage == ratingsReportPage)
            {
                cl = ratingsReportPage.PrepareCompositeLink(type);
            }
            return cl;
        }

        #endregion

        #region Instance Methods

        //public int ReportInit

        public bool HasUnsavedData()
        {
            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool SaveUnsavedData()
        {
            bool result = true;

            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        if (!page.SaveChanges())
                        {
                            result = false;
                        }
                    }
                }
            }

            return result;
        }

        protected override void OnControlRemoved(ControlEventArgs e)
        {
            forms.Remove(setForm);
            base.OnControlRemoved(e);
        }

        void CustomSettings()
        {
            cmbTab.Visible = true;
            lblTab.Visible = true;
            cmbTab.SelectedIndexChanged += SelectedIndexChanged;
            FillComboBox();
        }

        /// <summary>
        /// Fill combobox with data for M1M2 report
        /// </summary>
        private void FillComboBox()
        {
            cmbTab.Properties.Items.Clear();
            List<XtraTabPage> tmpLst = new List<XtraTabPage>(tabManager.TabPages);
            foreach (XtraTabPage page in tmpLst)
            {
                cmbTab.Properties.Items.Add(new ComboBoxItem { Id = page, Text = page.Text });
            }
            //set first Tab name as combobox Text.
            if (cmbTab.Properties.Items.Count > 0)
                cmbTab.Text = ((ComboBoxItem)cmbTab.Properties.Items[0]).Text;
        }

        private SettingsForm GetForm()
        {
            if (forms != null && forms.Count > 0)
                return forms[forms.Count - 1];
            return null;
        }

        private bool IsNeedFitTabToOnePage()
        {
            return true;
        }

        private void SortPages()
        {
            XtraTabPage pg;
            List<XtraTabPage> lst = new List<XtraTabPage>();
            foreach (XtraTabPage page in tabManager.TabPages)
                lst.Add(page);
            for (int i = 0; i < lst.Count; i++)
            {
                pg = lst[i];
                for (int j = i; j < lst.Count; j++)
                {
                    if (pg.Text.CompareTo(lst[j].Text) >= 0)
                        pg = lst[j];
                }
                lst.Remove(pg);
                lst.Insert(i, pg);
            }
            lst.ForEach(page => tabManager.TabPages.Move(150 + lst.IndexOf(page), page));
        }

        private void UpdateTabs()
        {
            UpdateSheets(setForm.SheetParamsList);
            foreach (XtraTabPage page in tabManager.TabPages)
            {
                BaseTab tab = page as BaseTab;
                if (null != tab)
                {
                    tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
                }
            }
            if (isRatingsRequired)
            {
                if (!TabControl.TabPages.Contains(ratingsReportPage))
                {
                    TabControl.TabPages.Add(ratingsReportPage);
                }
                ratingsReportPage.LoadData(setForm.DsmId, setForm.ReportDate.Date);
                ratingsReportPage.SheetSettings = setForm.SheetParamCollection;
            }

        }

        private void UpdateEventHandlers()
        {
            foreach (XtraTabPage page in TabControl.TabPages)
            {
                if (page is BaseTab)
                    UpdateEventHandler((BaseTab) page);
            }
        }

        private void UpdateEventHandler(BaseTab page)
        {
            if (page.SheetStructure.Id == Constants.TAB_M33 && page.Length >= 2)
            {
                CustomGridControl customGridControl = page.GetGrid(1).GridControl;
                ((BandedGridView)customGridControl.DefaultView).CustomColumnDisplayText += WccpUIControl_CustomColumnDisplayText;
                object dataSource = customGridControl.DataSource;
                customGridControl.DataSource = null;
                customGridControl.DataSource = dataSource;
            }
        }

        void WccpUIControl_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.DisplayText == Resources.ALL_ValueText)
                e.DisplayText = Resources.ALL_DisplayText.ToUpper();
        }

        #endregion

        #region Event Handling

        /// <summary>
        /// Occurs when index is changed
        /// </summary>
        /// <param name="sender">cmbTab</param>
        /// <param name="e">Event arguments</param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            tabManager.SelectedTabPage = ((XtraTabPage)((ComboBoxItem)cmbTab.SelectedItem).Id);
        }

        private bool WccpUIControl_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage is BaseTab)
            {
                BaseTab page = selectedPage as BaseTab;

                if (page.DataModified)
                {
                    DialogResult result = MessageBox.Show(Resources.SaveData, Resources.DataNotSaved, MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.OK)
                    {
                        if (page.SaveChanges())
                        {
                            XtraMessageBox.Show(Resources.DataSaved);
                        }
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        void WccpUIControl_RefreshClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage == ratingsReportPage)
            {
                ratingsReportPage.RefreshData();
            } 
            else if (selectedPage is BaseTab)
            {
                UpdateEventHandler((BaseTab)selectedPage);
            }
        }

        /// <summary>
        /// WCCPs the UI control_ settings form click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                isRatingsRequired = setForm.IsRatingsRequired;

                if (isRatingsRequired)
                {
                    if (!TabControl.TabPages.Contains(ratingsReportPage))
                    {
                        ratingsReportPage = new RaitingsTabPage();
                    }
                }
                else if (TabControl.TabPages.Contains(ratingsReportPage))
                    TabControl.TabPages.Remove(ratingsReportPage);

                UpdateTabs();
                SortPages();
                UpdateEventHandlers();
            }
        }

        #endregion

        #region Class Properties

        //private static int ReportID { get { return WCCPAPI.GetReportId(); } }

        #endregion

        #region Class Methods

        public static void AddForm(SettingsForm form)
        {
            if (forms == null)
                forms = new List<SettingsForm>();
            forms.Add(form);
        }

        #endregion
    }
}
