﻿using Logica.Reports.Common;
using Logica.Reports.wccpM3OnTrade;
using Logica.Reports.wccpM3OnTrade.Properties;
using ModularWinApp.Core.Interfaces;
using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    /// 
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpM3OnTrade.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        #region IStartupClass Members

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Shows the UI.
        /// </summary>
        /// <param name="isSkined">The is skined.</param>
        /// <param name="reportCaption">The report caption.</param>
        /// <returns></returns>
        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                SettingsForm setForm = new SettingsForm(reportId);

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.AddForm(setForm);

                    _reportControl = new WccpUIControl(reportId);
                    _reportCaption = reportCaption;
                    
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        /// <summary>
        /// Closes the UI.
        /// </summary>
        public void CloseUI()
        {
            DialogResult result;
            if (WccpUIControl.ReportControl.HasUnsavedData())
            {
                result = MessageBox.Show(Resources.SaveData, Resources.DataNotSaved, MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    if (WccpUIControl.ReportControl.SaveUnsavedData())
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(Resources.DataSaved);
                    }
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }
        }

        #endregion

        public bool AllowClose()
        {
            return true;
        }
    }
}
