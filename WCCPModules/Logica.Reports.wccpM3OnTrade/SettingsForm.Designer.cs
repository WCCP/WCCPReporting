﻿namespace Logica.Reports.wccpM3OnTrade
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.rbType = new DevExpress.XtraEditors.RadioGroup();
            this.groupData1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.lbDate = new DevExpress.XtraEditors.LabelControl();
            this.lblReport = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEditDSM = new DevExpress.XtraEditors.LookUpEdit();
            this.lblM2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).BeginInit();
            this.groupData1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditDSM.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.CausesValidation = false;
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(221, 216);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 9;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(141, 216);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 8;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.rbType);
            this.groupControl3.Location = new System.Drawing.Point(0, 153);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(298, 59);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "Тип отчета:";
            // 
            // rbType
            // 
            this.rbType.Location = new System.Drawing.Point(8, 22);
            this.rbType.Name = "rbType";
            this.rbType.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rbType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Ввод планов"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Получение фактов")});
            this.rbType.Size = new System.Drawing.Size(276, 35);
            this.rbType.TabIndex = 7;
            // 
            // groupData1
            // 
            this.groupData1.Controls.Add(this.dateEdit);
            this.groupData1.Controls.Add(this.lbDate);
            this.groupData1.Location = new System.Drawing.Point(0, 41);
            this.groupData1.Name = "groupData1";
            this.groupData1.Size = new System.Drawing.Size(298, 50);
            this.groupData1.TabIndex = 19;
            this.groupData1.Text = "Период";
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = null;
            this.dateEdit.Location = new System.Drawing.Point(48, 25);
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit.Size = new System.Drawing.Size(83, 20);
            this.dateEdit.TabIndex = 5;
            // 
            // lbDate
            // 
            this.lbDate.Location = new System.Drawing.Point(13, 28);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(30, 13);
            this.lbDate.TabIndex = 4;
            this.lbDate.Text = "Дата:";
            // 
            // lblReport
            // 
            this.lblReport.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.lblReport.Appearance.Options.UseFont = true;
            this.lblReport.Location = new System.Drawing.Point(98, 12);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(93, 21);
            this.lblReport.TabIndex = 20;
            this.lblReport.Text = "M3 On-trade";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl2.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl2.Appearance.Options.UseBackColor = true;
            this.groupControl2.Controls.Add(this.lookUpEditDSM);
            this.groupControl2.Controls.Add(this.lblM2);
            this.groupControl2.Location = new System.Drawing.Point(0, 96);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(298, 52);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Пользователь";
            // 
            // lookUpEditDSM
            // 
            this.lookUpEditDSM.Location = new System.Drawing.Point(48, 27);
            this.lookUpEditDSM.Name = "lookUpEditDSM";
            this.lookUpEditDSM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditDSM.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DSM_ID", "DSM_ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DSM_NAME", "DSM_NAME")});
            this.lookUpEditDSM.Properties.DisplayMember = "DSM_NAME";
            this.lookUpEditDSM.Properties.ShowFooter = false;
            this.lookUpEditDSM.Properties.ShowHeader = false;
            this.lookUpEditDSM.Properties.ValueMember = "DSM_ID";
            this.lookUpEditDSM.Size = new System.Drawing.Size(179, 20);
            this.lookUpEditDSM.TabIndex = 10;
            // 
            // lblM2
            // 
            this.lblM2.Location = new System.Drawing.Point(13, 30);
            this.lblM2.Name = "lblM2";
            this.lblM2.Size = new System.Drawing.Size(18, 13);
            this.lblM2.TabIndex = 9;
            this.lblM2.Text = "М3:";
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(300, 245);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.lblReport);
            this.Controls.Add(this.groupData1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры отчёта";
            this.Load += new System.EventHandler(this.M3FSMSettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).EndInit();
            this.groupData1.ResumeLayout(false);
            this.groupData1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditDSM.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.RadioGroup rbType;
        private DevExpress.XtraEditors.GroupControl groupData1;
        private DevExpress.XtraEditors.LabelControl lbDate;
        private DevExpress.XtraEditors.LabelControl lblReport;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl lblM2;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditDSM;
    }
}