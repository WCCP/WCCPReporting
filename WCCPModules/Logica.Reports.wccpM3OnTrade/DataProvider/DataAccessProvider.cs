﻿using System;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using Logica.Reports.wccpM3OnTrade.Properties;

namespace Logica.Reports.wccpM3OnTrade.DataProvider
{
  internal class DataAccessProvider
  {
    #region Class Properties

    internal static DataTable M3Users
    {
      get
      {
        DataTable dataTableM3Users = null;
        try
        {
          DataAccessLayer.OpenConnection();
          dataTableM3Users = DataAccessLayer.ExecuteStoredProcedure(Resources.SQL_DSM).Tables[0];
        }
        catch (Exception e)
        {
          ErrorManager.ShowErrorBox(e.Message);
        }
        finally
        {
          DataAccessLayer.CloseConnection();
        }
        return dataTableM3Users;
      }
    }

    #endregion

    #region Class Methods

    internal static DataTable GetDataInformation(DateTime date)
    {
      DataTable dataInformation;
      try
      {
        DataAccessLayer.OpenConnection();
        dataInformation = DataAccessLayer.ExecuteStoredProcedure(Resources.SQL_DateInfo, new[] {new SqlParameter(Resources.SQL_DateInfo_PARAM_Date, date)}).Tables[0];
      }
      finally
      {
        DataAccessLayer.CloseConnection();
      }
      return dataInformation;
    }


    internal static DataTable GetRating(int dsmId, DateTime reportDate)
    {
        DataTable res = null;

        DataAccessLayer.OpenConnection();

        DataSet ds = DataAccessLayer.ExecuteStoredProcedure(Resources.SQL_RAITING,
            new SqlParameter(Resources.SQL_RAITING_PRM_DSM_ID, dsmId),
            new SqlParameter(Resources.SQL_RAITING_PRM_REPORT_DATE, reportDate));

        if (null != ds && ds.Tables.Count > 0)
            res = ds.Tables[0];
        DataAccessLayer.CloseConnection();

        return res;
    }


    #endregion
  }
}