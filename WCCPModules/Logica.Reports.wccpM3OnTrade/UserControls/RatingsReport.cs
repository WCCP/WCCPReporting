#region

using System;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.wccpM3OnTrade.DataProvider;

#endregion

namespace Logica.Reports.wccpM3OnTrade.UserControls
{
    public partial class RatingsReport : UserControl
    {
        #region Fields

        private int dsmID;
        private DateTime reportDate;

        #endregion

        #region Constructors

        public RatingsReport()
        {
            InitializeComponent();   
        }

        #endregion

        #region Instance Methods

        public void RefreshData()
        {
            LoadData();
        }

        public void UpdateReport(int dsmID, DateTime date)
        {
                this.dsmID = dsmID;
                reportDate = date;

                LoadData();
        }

        private void LoadData()
        {
            try
            {
                pivotGridRatingByM2.DataSource = DataAccessProvider.GetRating(dsmID, reportDate);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

        }

        #endregion
    }
}