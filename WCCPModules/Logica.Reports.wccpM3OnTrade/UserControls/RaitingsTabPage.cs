﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.wccpM3OnTrade.Properties;

#endregion

namespace Logica.Reports.wccpM3OnTrade.UserControls
{
    class RaitingsTabPage : XtraTabPage, IPrint
    {
        #region Fields

        private GridControl headerGrid;
        private GridView headerGridView;
        private RatingsReport ratingsReport = new RatingsReport();
        private SheetParamCollection sheetSettings;

        #endregion

        #region Constructors

        public RaitingsTabPage()
        {
            Controls.Add(ratingsReport);
            Tag = ratingsReport;
            Text = Resources.Raitings;
            //ratingsReportPage.Dock = DockStyle.Fill;
            ratingsReport.Dock = DockStyle.Fill;
        }

        #endregion

        #region Instance Properties

        public SheetParamCollection SheetSettings
        {
            get { return sheetSettings; }
            set
            {
                sheetSettings = value;
                UpdateRateHeader();
            }
        }

        #endregion

        #region Instance Methods

        public void LoadData(int dsmID, DateTime reportDate)
        {
            ratingsReport.UpdateReport(dsmID, reportDate);
        }

        public CompositeLink PrepareCompositeLink(ExportToType type)
        {
            List<IPrintable> lst = new List<IPrintable> {headerGrid};
            lst.AddRange(from Control ratingsGp in Controls
                         from Control panel in ratingsGp.Controls
                         from Control c in panel.Controls
                         where c is IPrintable
                         select c as IPrintable);
            return TabOperationHelper.PrepareCompositeLink(type, "", lst, true);
        }

        public void RefreshData()
        {
            ratingsReport.RefreshData();
        }

        private void UpdateRateHeader()
        {
            TabOperationHelper.CreateSettingSection(this, SheetSettings, ref headerGrid, ref headerGridView);
        }

        #endregion

        #region IPrint Members

        public CompositeLink PrepareCompositeLink()
        {
            return PrepareCompositeLink(ExportToType.Pdf);
        }

        #endregion
    }
}
