namespace Logica.Reports.wccpM3OnTrade.UserControls
{
    partial class RatingsReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotGridRatingByM2 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridRatingByM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pivotGridRatingByM2
            // 
            this.pivotGridRatingByM2.Appearance.FieldHeader.Options.UseTextOptions = true;
            this.pivotGridRatingByM2.Appearance.FieldHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByM2.Appearance.FieldValue.Options.UseTextOptions = true;
            this.pivotGridRatingByM2.Appearance.FieldValue.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByM2.Appearance.FieldValueTotal.Options.UseTextOptions = true;
            this.pivotGridRatingByM2.Appearance.FieldValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByM2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridRatingByM2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridRatingByM2.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField1,
            this.pivotGridField2,
            this.pivotGridField3,
            this.pivotGridField4});
            this.pivotGridRatingByM2.Location = new System.Drawing.Point(2, 2);
            this.pivotGridRatingByM2.Name = "pivotGridRatingByM2";
            this.pivotGridRatingByM2.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pivotGridRatingByM2.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.ColumnArea;
            this.pivotGridRatingByM2.OptionsDataField.AreaIndex = 1;
            this.pivotGridRatingByM2.OptionsDataField.RowHeaderWidth = 170;
            this.pivotGridRatingByM2.OptionsView.ShowColumnGrandTotals = false;
            this.pivotGridRatingByM2.OptionsView.ShowColumnTotals = false;
            this.pivotGridRatingByM2.OptionsView.ShowRowGrandTotals = false;
            this.pivotGridRatingByM2.OptionsView.ShowRowTotals = false;
            this.pivotGridRatingByM2.Size = new System.Drawing.Size(975, 497);
            this.pivotGridRatingByM2.TabIndex = 3;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "���� �";
            this.pivotGridField1.EmptyCellText = "0";
            this.pivotGridField1.FieldName = "DayNumber";
            this.pivotGridField1.Name = "pivotGridField1";
            this.pivotGridField1.Options.AllowEdit = false;
            this.pivotGridField1.Options.ReadOnly = true;
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField2.AreaIndex = 0;
            this.pivotGridField2.Caption = "�2";
            this.pivotGridField2.EmptyCellText = "0";
            this.pivotGridField2.FieldName = "supervisor_name";
            this.pivotGridField2.Name = "pivotGridField2";
            this.pivotGridField2.Options.AllowEdit = false;
            this.pivotGridField2.Options.ReadOnly = true;
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField3.AreaIndex = 0;
            this.pivotGridField3.Caption = "������� �� ����";
            this.pivotGridField3.CellFormat.FormatString = "{0:N2}";
            this.pivotGridField3.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField3.EmptyCellText = "0";
            this.pivotGridField3.FieldName = "rating";
            this.pivotGridField3.Name = "pivotGridField3";
            this.pivotGridField3.Options.AllowEdit = false;
            this.pivotGridField3.Options.ReadOnly = true;
            this.pivotGridField3.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField4.AreaIndex = 1;
            this.pivotGridField4.Caption = "������������";
            this.pivotGridField4.CellFormat.FormatString = "{0:N2}";
            this.pivotGridField4.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField4.EmptyCellText = "0";
            this.pivotGridField4.FieldName = "comraiting";
            this.pivotGridField4.Name = "pivotGridField4";
            this.pivotGridField4.Options.AllowEdit = false;
            this.pivotGridField4.Options.ReadOnly = true;
            this.pivotGridField4.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.pivotGridField4.Width = 142;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pivotGridRatingByM2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(979, 501);
            this.panelControl2.TabIndex = 5;
            // 
            // RatingsReport
            // 
            this.Controls.Add(this.panelControl2);
            this.Name = "RatingsReport";
            this.Size = new System.Drawing.Size(979, 501);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridRatingByM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        public DevExpress.XtraPivotGrid.PivotGridControl pivotGridRatingByM2;





    }
}
