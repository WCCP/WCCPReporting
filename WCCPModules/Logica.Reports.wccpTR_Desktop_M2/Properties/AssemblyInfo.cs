﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("wccpTR_Desktop_M2")]
[assembly: AssemblyDescription("Thomas Research Desktop M2 Report")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logica, SoftServe Inc.")]
[assembly: AssemblyProduct("WCCP Reporting")]
[assembly: AssemblyCopyright("Copyright (c) 2009 Logica, 2010 - 2014 SoftServe Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("812bcb27-8aff-4489-8d61-1ad113d768bb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2.1.1.64999")]
[assembly: AssemblyFileVersion("2.1.1.64999")]
