﻿using System;
using System.Data.SqlClient;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;

namespace WccpReporting
{
    public partial class WccpUIControl : XtraUserControl
    {
        #region Fields

        private const string SpDwTrCalcFl = "spDW_TR_Calc_FL";
        private const string SpDwTrCalcInconsistency = "spDW_TR_Calc_Inconsistency";
        private const string SpDwTrCalcSr = "spDW_TR_Calc_SR";

        private bool isPrepare;

        private string reportTitle;

        private string supervisorName;

        #endregion

        #region Constructors

        public WccpUIControl()
        {
            InitializeComponent();
            Disposed += ReportControl_Disposed;
        }

        #endregion

        #region Instance Properties

        public int DayOfWeek { get; set; }

        public string ReportTitle
        {
            get { return reportTitle; }
            set
            {
                reportTitle = value;
                lbReportTitle.Text = value;
            }
        }

        public string SupervisorName
        {
            get { return supervisorName; }
            set
            {
                supervisorName = value;
                edSupervisorText.Text = value;
            }
        }

        private SqlParameter ParDW
        {
            get
            {
                var ParDW = new SqlParameter("@ParDW", Convert.ToString(cboxDayOfWeek.SelectedIndex + 1));
                return ParDW;
            }
        }

        private SqlParameter ParSv
        {
            get
            {
                SqlParameter parSv;
                if (SupervisorName == "Все ОФФ" || SupervisorName == "Все ОН")
                {
                    //sql = sql.Replace("<#pParSVp#>", "NULL");
                    parSv = new SqlParameter("@ParSV", DBNull.Value);
                }
                else
                {
                    //sql = sql.Replace("<#pParSVp#>", "'" + ReportOptions.SupervisorName + "'");
                    parSv = new SqlParameter("@ParSV", ReportOptions.SupervisorName);
                }
                return parSv;
            }
        }

        #endregion

        #region Instance Methods

        private void LoadInconsystencyDataSet()
        {
            var dsInconsistency = DataAccessLayer.ExecuteStoredProcedure("spDW_TR_IncReportOutput", new[] { ParChannel });
            if (dsInconsistency != null && dsInconsistency.Tables.Count > 0)
            {
                reportDataSet.DW_TomasResearch_Inconsistency.Clear();
                reportDataSet.DW_TomasResearch_Inconsistency.Load(dsInconsistency.Tables[0].CreateDataReader());
                gvInconsistency.BestFitColumns();
            }
        }

        private void LoadProgressDataSet()
        {
            var dsProgress = DataAccessLayer.ExecuteStoredProcedure("spDW_Tr_ProgressReport_AllDays",
                new[] { ParChannel, ParSv });
            if (dsProgress != null && dsProgress.Tables.Count > 0)
            {
                reportDataSet.DW_TomasResearch_ProgressReport.Clear();
                reportDataSet.DW_TomasResearch_ProgressReport.Load(dsProgress.Tables[0].CreateDataReader());
            }
        }

        //сформировать предварительный набор данных
        private bool Prepare()
        {
            try
            {
                // TR reengineering
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SpDwTrCalcFl);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SpDwTrCalcInconsistency);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SpDwTrCalcSr);

                isPrepare = true;
                return true;
            }
            catch (Exception lException)
            {
                MessageBox.Show("Ошибка: " + WCCPConst.msgErrorPrepare + "\n" + lException.Message, "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private string SelectFilePath(string reportCaption)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = reportCaption;
            sfd.Filter = String.Format("(*.{0})|*.{0}", "xlsx");
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }

            return res;
        }

        #endregion

        #region Event Handling

        private void ReportControl_Disposed(object sender, EventArgs e)
        {
        }

        private void ReportControl_Load(object sender, EventArgs e)
        {
            ReportTitle = ReportOptions.ReportTitle;
            SupervisorName = ReportOptions.SupervisorName;
            DayOfWeek = (int)DateTime.Today.DayOfWeek;
            cboxDayOfWeek.SelectedIndex = (DayOfWeek - 1);

            WaitManager.StartWait();
            try
            {
                if (Prepare())
                {
                    LoadInconsystencyDataSet();
                    LoadProgressDataSet();
                }
                WaitManager.StopWait();
            }
            catch (Exception ex)
            {
                isPrepare = false;
                WaitManager.StopWait();
                MessageBox.Show("Ошибка: " + "\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Обновить данные для "Покрытие на день"
        private void btnRefreshCoveringOfDay_Click(object sender, EventArgs e)
        {
            string sql = String.Empty;
            WaitManager.StartWait();
            try
            {
                if (isPrepare)
                {
                    //WCCPAPI.GetDesktopReportScript("wccpTR_Desktop_M2.fdb", 4, ref sql, ref isErrorScript);
                    //sql = sql.Replace("<#pParDWp#>", "'" + Convert.ToString(cboxDayOfWeek.SelectedIndex + 1) + "'");
                    var ds = DataAccessLayer.ExecuteStoredProcedure("spDW_Tr_ProgressReport_WeekDays",
                        new[] { ParSv, ParChannel, ParDW });
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        gridControl2.DataSource = ds.Tables[0].DefaultView;
                    }
                }
                WaitManager.StopWait();
            }
            catch (Exception ex)
            {
                WaitManager.StopWait();
                MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbtnReportRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (InputReportDataForm fmInputReportDataForm = new InputReportDataForm())
            {
                fmInputReportDataForm.ReportName = ReportTitle;
                if (fmInputReportDataForm.ShowDialog() == DialogResult.OK)
                {
                    System.Windows.Forms.Application.DoEvents();

                    SupervisorName = ReportOptions.SupervisorName;

                    WaitManager.StartWait();
                    try
                    {
                        if (Prepare())
                        {
                            LoadInconsystencyDataSet();
                            LoadProgressDataSet();
                        }
                        WaitManager.StopWait();
                    }
                    catch (Exception ex)
                    {
                        WaitManager.StopWait();
                        MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void tbtnReportToExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            string lPath = string.Empty;

            try
            {
                switch (Convert.ToInt32(tcReports.SelectedTabPage.Tag))
                {
                    case 1:
                        {
                            lPath = SelectFilePath(WCCPConst.frptexlTPReport);

                            if (!string.IsNullOrEmpty(lPath))
                                pivotGridControl1.ExportToXlsx(lPath);
                            break;
                        }
                    case 2:
                        {
                            lPath = SelectFilePath(WCCPConst.frptexlTTReport);

                            if (!string.IsNullOrEmpty(lPath))
                                pivotGridControl2.ExportToXlsx(lPath);
                            break;
                        }
                    case 3:
                        {
                            lPath = SelectFilePath(WCCPConst.frptexlIncReport);

                            if (!string.IsNullOrEmpty(lPath))
                                gridControl1.ExportToXlsx(lPath);
                            break;
                        }
                    case 4:
                        {
                            lPath = SelectFilePath(WCCPConst.frptexlDayOfWeekReport);

                            if (!string.IsNullOrEmpty(lPath))
                                gridControl2.ExportToXlsx(lPath);
                            break;
                        }
                }

                if (!string.IsNullOrEmpty(lPath))
                    System.Diagnostics.Process.Start(lPath);

            }
            catch (Exception)
            {
                ErrorManager.ShowErrorBox("Ошибка при экспорте: закройте файл и попробуйте еще раз");
            }
        }

        #endregion

        #region Class Properties

        private static SqlParameter ParChannel
        {
            get
            {
                SqlParameter parChanel = new SqlParameter("@ParChanel", ReportOptions.ChanelName);
                return parChanel;
            }
        }

        #endregion
    }
}