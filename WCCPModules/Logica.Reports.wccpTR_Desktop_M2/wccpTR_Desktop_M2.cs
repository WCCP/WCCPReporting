﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpTR_Desktop_M2.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        //Возвращает версию модуля
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        //Точка входа модуля
        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                DataAccessLayer.OpenConnection();

                ReportOptions.ReportTitle = reportCaption;

                using (InputReportDataForm fmInputReportDataForm = new InputReportDataForm())
                {
                    fmInputReportDataForm.ReportName = reportCaption;
                    if (fmInputReportDataForm.ShowDialog() == DialogResult.OK)
                    {
                        _reportControl = new WccpUIControl();
                        _reportCaption = reportCaption;

                        return 0;
                    }

                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка:" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
        }

        //Точка выхода модуля
        public void CloseUI()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка:" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool AllowClose()
        {
            return true;
        }
    }
}
