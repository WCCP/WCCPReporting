﻿namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.pnReportSheet = new DevExpress.XtraEditors.PanelControl();
            this.tcReports = new DevExpress.XtraTab.XtraTabControl();
            this.tpTP = new DevExpress.XtraTab.XtraTabPage();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.bsProgressReport = new System.Windows.Forms.BindingSource(this.components);
            this.reportDataSet = new WccpReporting.ReportDataSet();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField8 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField9 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField10 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField24 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.tpTT = new DevExpress.XtraTab.XtraTabPage();
            this.pivotGridControl2 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField12 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField13 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField14 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField15 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField16 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField17 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField18 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField19 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField20 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField22 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField23 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.tpInconsistency = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bsInconsistency = new System.Windows.Forms.BindingSource(this.components);
            this.gvInconsistency = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colКритичность = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colДатаВизита = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colСВ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colТП = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colКодTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colТТ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colЮрИмяТТ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colАдресТТ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colЮрАдресТТ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colКаналТТ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSKU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colТипНесоотв = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colЗначение = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colМинЗнач = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colМаксЗнач = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПодтверждено = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManagerReport = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.tbtnReportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.tbtnReportRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imgNormal = new DevExpress.Utils.ImageCollection(this.components);
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.imgLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.tpCoveringOfDay = new DevExpress.XtraTab.XtraTabPage();
            this.pnClientCoveringOfDay = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnOptionsCoveringOfDay = new DevExpress.XtraEditors.PanelControl();
            this.btnRefreshCoveringOfDay = new DevExpress.XtraEditors.SimpleButton();
            this.cboxDayOfWeek = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pnReportHeader = new DevExpress.XtraEditors.PanelControl();
            this.edSupervisorText = new DevExpress.XtraEditors.TextEdit();
            this.lbSupervisor = new DevExpress.XtraEditors.LabelControl();
            this.lbReportTitle = new System.Windows.Forms.Label();
            this.pnReportToolBar = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).BeginInit();
            this.pnReportSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcReports)).BeginInit();
            this.tcReports.SuspendLayout();
            this.tpTP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProgressReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).BeginInit();
            this.tpTT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).BeginInit();
            this.tpInconsistency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInconsistency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvInconsistency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.tpCoveringOfDay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnClientCoveringOfDay)).BeginInit();
            this.pnClientCoveringOfDay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnOptionsCoveringOfDay)).BeginInit();
            this.pnOptionsCoveringOfDay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboxDayOfWeek.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).BeginInit();
            this.pnReportHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edSupervisorText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).BeginInit();
            this.pnReportToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnReportSheet
            // 
            this.pnReportSheet.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnReportSheet.Controls.Add(this.tcReports);
            this.pnReportSheet.Controls.Add(this.pnReportHeader);
            this.pnReportSheet.Controls.Add(this.pnReportToolBar);
            this.pnReportSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnReportSheet.Location = new System.Drawing.Point(0, 0);
            this.pnReportSheet.Name = "pnReportSheet";
            this.pnReportSheet.Size = new System.Drawing.Size(640, 480);
            this.pnReportSheet.TabIndex = 0;
            // 
            // tcReports
            // 
            this.tcReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReports.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tcReports.Location = new System.Drawing.Point(0, 102);
            this.tcReports.Name = "tcReports";
            this.tcReports.SelectedTabPage = this.tpTP;
            this.tcReports.Size = new System.Drawing.Size(640, 378);
            this.tcReports.TabIndex = 2;
            this.tcReports.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpTP,
            this.tpTT,
            this.tpInconsistency,
            this.tpCoveringOfDay});
            // 
            // tpTP
            // 
            this.tpTP.Controls.Add(this.pivotGridControl1);
            this.tpTP.Name = "tpTP";
            this.tpTP.Size = new System.Drawing.Size(634, 350);
            this.tpTP.Tag = "1";
            this.tpTP.Text = "Покрытие - ТП";
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Appearance.HeaderArea.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pivotGridControl1.Appearance.HeaderArea.Options.UseFont = true;
            this.pivotGridControl1.Appearance.RowHeaderArea.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pivotGridControl1.Appearance.RowHeaderArea.Options.UseFont = true;
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.bsProgressReport;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField3,
            this.pivotGridField4,
            this.pivotGridField5,
            this.pivotGridField6,
            this.pivotGridField7,
            this.pivotGridField8,
            this.pivotGridField9,
            this.pivotGridField10,
            this.pivotGridField11,
            this.pivotGridField1,
            this.pivotGridField2,
            this.pivotGridField24});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.AllowEdit = false;
            this.pivotGridControl1.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.RowArea;
            this.pivotGridControl1.OptionsDataField.AreaIndex = 2;
            this.pivotGridControl1.OptionsDataField.Caption = "Данные";
            this.pivotGridControl1.OptionsMenu.EnableHeaderAreaMenu = false;
            this.pivotGridControl1.OptionsSelection.MultiSelect = false;
            this.pivotGridControl1.Size = new System.Drawing.Size(634, 350);
            this.pivotGridControl1.TabIndex = 3;
            // 
            // bsProgressReport
            // 
            this.bsProgressReport.DataMember = "DW_TomasResearch_ProgressReport";
            this.bsProgressReport.DataSource = this.reportDataSet;
            // 
            // reportDataSet
            // 
            this.reportDataSet.DataSetName = "ReportDataSet";
            this.reportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pivotGridField3.Appearance.Header.Options.UseFont = true;
            this.pivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField3.AreaIndex = 0;
            this.pivotGridField3.Caption = "СВ";
            this.pivotGridField3.FieldName = "СВ";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pivotGridField4.Appearance.Header.Options.UseFont = true;
            this.pivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField4.AreaIndex = 1;
            this.pivotGridField4.Caption = "ТП";
            this.pivotGridField4.FieldName = "ТП";
            this.pivotGridField4.Name = "pivotGridField4";
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.AreaIndex = 0;
            this.pivotGridField5.Caption = "КаналТТ";
            this.pivotGridField5.FieldName = "КаналТТ";
            this.pivotGridField5.Name = "pivotGridField5";
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.AreaIndex = 1;
            this.pivotGridField6.Caption = "ДатаВизита";
            this.pivotGridField6.FieldName = "ДатаВизита";
            this.pivotGridField6.Name = "pivotGridField6";
            // 
            // pivotGridField7
            // 
            this.pivotGridField7.AreaIndex = 2;
            this.pivotGridField7.Caption = "ТС";
            this.pivotGridField7.FieldName = "ТС";
            this.pivotGridField7.Name = "pivotGridField7";
            // 
            // pivotGridField8
            // 
            this.pivotGridField8.AreaIndex = 3;
            this.pivotGridField8.Caption = "Канал(анкета)";
            this.pivotGridField8.FieldName = "Канал(анкета)";
            this.pivotGridField8.Name = "pivotGridField8";
            // 
            // pivotGridField9
            // 
            this.pivotGridField9.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField9.AreaIndex = 0;
            this.pivotGridField9.Caption = "Заполнение";
            this.pivotGridField9.FieldName = "Заполнение";
            this.pivotGridField9.Name = "pivotGridField9";
            // 
            // pivotGridField10
            // 
            this.pivotGridField10.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField10.AreaIndex = 0;
            this.pivotGridField10.Caption = "Кол-во ТТ";
            this.pivotGridField10.FieldName = "КодТТ";
            this.pivotGridField10.Name = "pivotGridField10";
            this.pivotGridField10.Options.AllowEdit = false;
            this.pivotGridField10.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            // 
            // pivotGridField11
            // 
            this.pivotGridField11.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField11.AreaIndex = 1;
            this.pivotGridField11.Caption = "% ТТ";
            this.pivotGridField11.CellFormat.FormatString = "p0";
            this.pivotGridField11.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.FieldName = "КодТТ";
            this.pivotGridField11.GrandTotalCellFormat.FormatString = "p0";
            this.pivotGridField11.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.GrandTotalText = "Всего";
            this.pivotGridField11.Name = "pivotGridField11";
            this.pivotGridField11.Options.AllowEdit = false;
            this.pivotGridField11.SummaryDisplayType = DevExpress.Data.PivotGrid.PivotSummaryDisplayType.PercentOfRow;
            this.pivotGridField11.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            this.pivotGridField11.TotalCellFormat.FormatString = "p0";
            this.pivotGridField11.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.TotalValueFormat.FormatString = "p0";
            this.pivotGridField11.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.ValueFormat.FormatString = "p0";
            this.pivotGridField11.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "КодТТ";
            this.pivotGridField1.FieldName = "КодТТ";
            this.pivotGridField1.Name = "pivotGridField1";
            this.pivotGridField1.Visible = false;
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.AreaIndex = 0;
            this.pivotGridField2.Caption = "ЮрИмяТТ";
            this.pivotGridField2.FieldName = "ЮрИмяТТ";
            this.pivotGridField2.Name = "pivotGridField2";
            this.pivotGridField2.Visible = false;
            // 
            // pivotGridField24
            // 
            this.pivotGridField24.AreaIndex = 0;
            this.pivotGridField24.Caption = "ЮрАдресТТ";
            this.pivotGridField24.FieldName = "ЮрАдресТТ";
            this.pivotGridField24.Name = "pivotGridField24";
            this.pivotGridField24.Visible = false;
            // 
            // tpTT
            // 
            this.tpTT.Controls.Add(this.pivotGridControl2);
            this.tpTT.Name = "tpTT";
            this.tpTT.Size = new System.Drawing.Size(634, 350);
            this.tpTT.Tag = "2";
            this.tpTT.Text = "Покрытие - ТТ";
            // 
            // pivotGridControl2
            // 
            this.pivotGridControl2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl2.DataSource = this.bsProgressReport;
            this.pivotGridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl2.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField12,
            this.pivotGridField13,
            this.pivotGridField14,
            this.pivotGridField15,
            this.pivotGridField16,
            this.pivotGridField17,
            this.pivotGridField18,
            this.pivotGridField19,
            this.pivotGridField20,
            this.pivotGridField21,
            this.pivotGridField22,
            this.pivotGridField23});
            this.pivotGridControl2.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl2.Name = "pivotGridControl2";
            this.pivotGridControl2.OptionsCustomization.AllowEdit = false;
            this.pivotGridControl2.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.RowArea;
            this.pivotGridControl2.OptionsDataField.Caption = "Данные";
            this.pivotGridControl2.OptionsSelection.MultiSelect = false;
            this.pivotGridControl2.Size = new System.Drawing.Size(634, 350);
            this.pivotGridControl2.TabIndex = 0;
            // 
            // pivotGridField12
            // 
            this.pivotGridField12.AreaIndex = 0;
            this.pivotGridField12.Caption = "КаналТТ";
            this.pivotGridField12.FieldName = "КаналТТ";
            this.pivotGridField12.Name = "pivotGridField12";
            // 
            // pivotGridField13
            // 
            this.pivotGridField13.AreaIndex = 1;
            this.pivotGridField13.Caption = "ДатаВизита";
            this.pivotGridField13.FieldName = "ДатаВизита";
            this.pivotGridField13.Name = "pivotGridField13";
            // 
            // pivotGridField14
            // 
            this.pivotGridField14.AreaIndex = 2;
            this.pivotGridField14.Caption = "Канал(анкета)";
            this.pivotGridField14.FieldName = "Канал(анкета)";
            this.pivotGridField14.Name = "pivotGridField14";
            // 
            // pivotGridField15
            // 
            this.pivotGridField15.AreaIndex = 3;
            this.pivotGridField15.Caption = "АдресТТ";
            this.pivotGridField15.FieldName = "АдресТТ";
            this.pivotGridField15.Name = "pivotGridField15";
            // 
            // pivotGridField16
            // 
            this.pivotGridField16.AreaIndex = 5;
            this.pivotGridField16.Caption = "ЮрИмяТТ";
            this.pivotGridField16.FieldName = "ЮрИмяТТ";
            this.pivotGridField16.Name = "pivotGridField16";
            // 
            // pivotGridField17
            // 
            this.pivotGridField17.AreaIndex = 4;
            this.pivotGridField17.Caption = "ЮрАдресТТ";
            this.pivotGridField17.FieldName = "ЮрАдресТТ";
            this.pivotGridField17.Name = "pivotGridField17";
            // 
            // pivotGridField18
            // 
            this.pivotGridField18.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pivotGridField18.Appearance.Header.Options.UseFont = true;
            this.pivotGridField18.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField18.AreaIndex = 0;
            this.pivotGridField18.Caption = "СВ";
            this.pivotGridField18.FieldName = "СВ";
            this.pivotGridField18.Name = "pivotGridField18";
            // 
            // pivotGridField19
            // 
            this.pivotGridField19.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pivotGridField19.Appearance.Header.Options.UseFont = true;
            this.pivotGridField19.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField19.AreaIndex = 1;
            this.pivotGridField19.Caption = "ТП";
            this.pivotGridField19.FieldName = "ТП";
            this.pivotGridField19.Name = "pivotGridField19";
            // 
            // pivotGridField20
            // 
            this.pivotGridField20.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pivotGridField20.Appearance.Header.Options.UseFont = true;
            this.pivotGridField20.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField20.AreaIndex = 2;
            this.pivotGridField20.Caption = "КодТТ";
            this.pivotGridField20.FieldName = "КодТТ";
            this.pivotGridField20.Name = "pivotGridField20";
            // 
            // pivotGridField21
            // 
            this.pivotGridField21.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pivotGridField21.Appearance.Header.Options.UseFont = true;
            this.pivotGridField21.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField21.AreaIndex = 3;
            this.pivotGridField21.Caption = "ТТ";
            this.pivotGridField21.FieldName = "ТТ";
            this.pivotGridField21.Name = "pivotGridField21";
            // 
            // pivotGridField22
            // 
            this.pivotGridField22.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField22.AreaIndex = 0;
            this.pivotGridField22.Caption = "Заполнение";
            this.pivotGridField22.FieldName = "Заполнение";
            this.pivotGridField22.Name = "pivotGridField22";
            // 
            // pivotGridField23
            // 
            this.pivotGridField23.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField23.AreaIndex = 0;
            this.pivotGridField23.Caption = "Кол-во (КодТТ)";
            this.pivotGridField23.FieldName = "КодТТ";
            this.pivotGridField23.Name = "pivotGridField23";
            this.pivotGridField23.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            // 
            // tpInconsistency
            // 
            this.tpInconsistency.Controls.Add(this.gridControl1);
            this.tpInconsistency.Name = "tpInconsistency";
            this.tpInconsistency.Size = new System.Drawing.Size(634, 350);
            this.tpInconsistency.Tag = "3";
            this.tpInconsistency.Text = "Несоответствия";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.bsInconsistency;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gvInconsistency;
            this.gridControl1.MenuManager = this.barManagerReport;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(634, 350);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvInconsistency});
            // 
            // bsInconsistency
            // 
            this.bsInconsistency.DataMember = "DW_TomasResearch_Inconsistency";
            this.bsInconsistency.DataSource = this.reportDataSet;
            // 
            // gvInconsistency
            // 
            this.gvInconsistency.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gvInconsistency.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvInconsistency.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colКритичность,
            this.colДатаВизита,
            this.colСВ,
            this.colТП,
            this.colКодTT,
            this.colТТ,
            this.colЮрИмяТТ,
            this.colАдресТТ,
            this.colЮрАдресТТ,
            this.colКаналТТ,
            this.gridColumn1,
            this.colSKU,
            this.colТипНесоотв,
            this.colЗначение,
            this.colМинЗнач,
            this.colМаксЗнач,
            this.colПодтверждено});
            this.gvInconsistency.GridControl = this.gridControl1;
            this.gvInconsistency.Name = "gvInconsistency";
            this.gvInconsistency.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvInconsistency.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvInconsistency.OptionsBehavior.CacheValuesOnRowUpdating = DevExpress.Data.CacheRowValuesMode.Disabled;
            this.gvInconsistency.OptionsBehavior.Editable = false;
            this.gvInconsistency.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvInconsistency.OptionsPrint.AutoWidth = false;
            this.gvInconsistency.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.NeverAnimate;
            this.gvInconsistency.OptionsView.ColumnAutoWidth = false;
            this.gvInconsistency.OptionsView.ShowIndicator = false;
            // 
            // colКритичность
            // 
            this.colКритичность.FieldName = "Критичность";
            this.colКритичность.Name = "colКритичность";
            this.colКритичность.Visible = true;
            this.colКритичность.VisibleIndex = 0;
            // 
            // colДатаВизита
            // 
            this.colДатаВизита.FieldName = "ДатаВизита";
            this.colДатаВизита.Name = "colДатаВизита";
            this.colДатаВизита.Visible = true;
            this.colДатаВизита.VisibleIndex = 1;
            // 
            // colСВ
            // 
            this.colСВ.FieldName = "СВ";
            this.colСВ.Name = "colСВ";
            this.colСВ.Visible = true;
            this.colСВ.VisibleIndex = 2;
            // 
            // colТП
            // 
            this.colТП.FieldName = "ТП";
            this.colТП.Name = "colТП";
            this.colТП.Visible = true;
            this.colТП.VisibleIndex = 3;
            // 
            // colКодTT
            // 
            this.colКодTT.FieldName = "КодTT";
            this.colКодTT.Name = "colКодTT";
            this.colКодTT.Visible = true;
            this.colКодTT.VisibleIndex = 4;
            // 
            // colТТ
            // 
            this.colТТ.FieldName = "ТТ";
            this.colТТ.Name = "colТТ";
            this.colТТ.Visible = true;
            this.colТТ.VisibleIndex = 5;
            // 
            // colЮрИмяТТ
            // 
            this.colЮрИмяТТ.FieldName = "ЮрИмяТТ";
            this.colЮрИмяТТ.Name = "colЮрИмяТТ";
            this.colЮрИмяТТ.Visible = true;
            this.colЮрИмяТТ.VisibleIndex = 6;
            // 
            // colАдресТТ
            // 
            this.colАдресТТ.FieldName = "АдресТТ";
            this.colАдресТТ.Name = "colАдресТТ";
            this.colАдресТТ.Visible = true;
            this.colАдресТТ.VisibleIndex = 7;
            // 
            // colЮрАдресТТ
            // 
            this.colЮрАдресТТ.FieldName = "ЮрАдресТТ";
            this.colЮрАдресТТ.Name = "colЮрАдресТТ";
            this.colЮрАдресТТ.Visible = true;
            this.colЮрАдресТТ.VisibleIndex = 8;
            // 
            // colКаналТТ
            // 
            this.colКаналТТ.FieldName = "КаналТТ";
            this.colКаналТТ.Name = "colКаналТТ";
            this.colКаналТТ.Visible = true;
            this.colКаналТТ.VisibleIndex = 9;
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "Канал(анкета)";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            // 
            // colSKU
            // 
            this.colSKU.FieldName = "SKU";
            this.colSKU.Name = "colSKU";
            this.colSKU.Visible = true;
            this.colSKU.VisibleIndex = 11;
            // 
            // colТипНесоотв
            // 
            this.colТипНесоотв.FieldName = "ТипНесоотв";
            this.colТипНесоотв.Name = "colТипНесоотв";
            this.colТипНесоотв.Visible = true;
            this.colТипНесоотв.VisibleIndex = 12;
            // 
            // colЗначение
            // 
            this.colЗначение.FieldName = "Значение";
            this.colЗначение.Name = "colЗначение";
            this.colЗначение.Visible = true;
            this.colЗначение.VisibleIndex = 13;
            // 
            // colМинЗнач
            // 
            this.colМинЗнач.FieldName = "МинЗнач";
            this.colМинЗнач.Name = "colМинЗнач";
            this.colМинЗнач.Visible = true;
            this.colМинЗнач.VisibleIndex = 14;
            // 
            // colМаксЗнач
            // 
            this.colМаксЗнач.FieldName = "МаксЗнач";
            this.colМаксЗнач.Name = "colМаксЗнач";
            this.colМаксЗнач.Visible = true;
            this.colМаксЗнач.VisibleIndex = 15;
            // 
            // colПодтверждено
            // 
            this.colПодтверждено.FieldName = "Подтверждено";
            this.colПодтверждено.Name = "colПодтверждено";
            this.colПодтверждено.Visible = true;
            this.colПодтверждено.VisibleIndex = 16;
            // 
            // barManagerReport
            // 
            this.barManagerReport.AllowCustomization = false;
            this.barManagerReport.AllowMoveBarOnToolbar = false;
            this.barManagerReport.AllowQuickCustomization = false;
            this.barManagerReport.AllowShowToolbarsPopup = false;
            this.barManagerReport.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManagerReport.Controller = this.barAndDockingController1;
            this.barManagerReport.DockControls.Add(this.barDockControlTop);
            this.barManagerReport.DockControls.Add(this.barDockControlBottom);
            this.barManagerReport.DockControls.Add(this.barDockControlLeft);
            this.barManagerReport.DockControls.Add(this.barDockControlRight);
            this.barManagerReport.DockControls.Add(this.standaloneBarDockControl1);
            this.barManagerReport.Form = this;
            this.barManagerReport.Images = this.imgNormal;
            this.barManagerReport.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.tbtnReportToExcel,
            this.tbtnReportRefresh,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManagerReport.LargeImages = this.imgLarge;
            this.barManagerReport.MainMenu = this.bar2;
            this.barManagerReport.MaxItemId = 6;
            this.barManagerReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            // 
            // bar2
            // 
            this.bar2.BarName = "menuReport";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(217, 125);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this.tbtnReportToExcel, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this.tbtnReportRefresh, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "menuReport";
            // 
            // tbtnReportToExcel
            // 
            this.tbtnReportToExcel.Caption = "Отчет в Excel";
            this.tbtnReportToExcel.Hint = "Отчет в Excel";
            this.tbtnReportToExcel.Id = 0;
            this.tbtnReportToExcel.LargeImageIndex = 1;
            this.tbtnReportToExcel.Name = "tbtnReportToExcel";
            this.tbtnReportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportToExcel_ItemClick);
            // 
            // tbtnReportRefresh
            // 
            this.tbtnReportRefresh.Caption = "Обновить отчет";
            this.tbtnReportRefresh.Hint = "Обновить отчет";
            this.tbtnReportRefresh.Id = 2;
            this.tbtnReportRefresh.LargeImageIndex = 0;
            this.tbtnReportRefresh.Name = "tbtnReportRefresh";
            this.tbtnReportRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportRefresh_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(2, 2);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(636, 36);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(640, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 480);
            this.barDockControlBottom.Size = new System.Drawing.Size(640, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 480);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(640, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 480);
            // 
            // imgNormal
            // 
            this.imgNormal.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgNormal.ImageStream")));
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 4;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 5;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // imgLarge
            // 
            this.imgLarge.ImageSize = new System.Drawing.Size(24, 24);
            this.imgLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgLarge.ImageStream")));
            this.imgLarge.Images.SetKeyName(0, "briefcase_ok_24.png");
            this.imgLarge.Images.SetKeyName(1, "Excel_32.bmp");
            this.imgLarge.Images.SetKeyName(2, "briefcase_prev_24.png");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // tpCoveringOfDay
            // 
            this.tpCoveringOfDay.Controls.Add(this.pnClientCoveringOfDay);
            this.tpCoveringOfDay.Controls.Add(this.pnOptionsCoveringOfDay);
            this.tpCoveringOfDay.Name = "tpCoveringOfDay";
            this.tpCoveringOfDay.Size = new System.Drawing.Size(634, 350);
            this.tpCoveringOfDay.Tag = "4";
            this.tpCoveringOfDay.Text = "Покрытие на день";
            // 
            // pnClientCoveringOfDay
            // 
            this.pnClientCoveringOfDay.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnClientCoveringOfDay.Controls.Add(this.gridControl2);
            this.pnClientCoveringOfDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnClientCoveringOfDay.Location = new System.Drawing.Point(0, 32);
            this.pnClientCoveringOfDay.Name = "pnClientCoveringOfDay";
            this.pnClientCoveringOfDay.Size = new System.Drawing.Size(634, 318);
            this.pnClientCoveringOfDay.TabIndex = 1;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.bsProgressReport;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManagerReport;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(634, 318);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn5,
            this.gridColumn7,
            this.gridColumn6});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsBehavior.CacheValuesOnRowUpdating = DevExpress.Data.CacheRowValuesMode.Disabled;
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView2.OptionsPrint.AutoWidth = false;
            this.gridView2.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.NeverAnimate;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "СВ";
            this.gridColumn2.FieldName = "СВ";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 150;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ТП";
            this.gridColumn3.FieldName = "ТП";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 150;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "КодТТ";
            this.gridColumn4.FieldName = "КодТТ";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            this.gridColumn4.Width = 95;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "ТТ";
            this.gridColumn8.FieldName = "ТТ";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 150;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "АдресТТ";
            this.gridColumn9.FieldName = "АдресТТ";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            this.gridColumn9.Width = 150;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Канал(анкета)";
            this.gridColumn5.FieldName = "Канал(анкета)";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            this.gridColumn5.Width = 95;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Заполнение_детально";
            this.gridColumn7.FieldName = "Заполнение_детально";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Заполнение";
            this.gridColumn6.FieldName = "Заполнение";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 7;
            this.gridColumn6.Width = 150;
            // 
            // pnOptionsCoveringOfDay
            // 
            this.pnOptionsCoveringOfDay.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnOptionsCoveringOfDay.Controls.Add(this.btnRefreshCoveringOfDay);
            this.pnOptionsCoveringOfDay.Controls.Add(this.cboxDayOfWeek);
            this.pnOptionsCoveringOfDay.Controls.Add(this.labelControl2);
            this.pnOptionsCoveringOfDay.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnOptionsCoveringOfDay.Location = new System.Drawing.Point(0, 0);
            this.pnOptionsCoveringOfDay.Name = "pnOptionsCoveringOfDay";
            this.pnOptionsCoveringOfDay.Size = new System.Drawing.Size(634, 32);
            this.pnOptionsCoveringOfDay.TabIndex = 0;
            // 
            // btnRefreshCoveringOfDay
            // 
            this.btnRefreshCoveringOfDay.Image = global::Logica.Reports.wccpTR_Desktop_M2.Properties.Resources.refresh_24;
            this.btnRefreshCoveringOfDay.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRefreshCoveringOfDay.Location = new System.Drawing.Point(312, 1);
            this.btnRefreshCoveringOfDay.Name = "btnRefreshCoveringOfDay";
            this.btnRefreshCoveringOfDay.Size = new System.Drawing.Size(32, 31);
            this.btnRefreshCoveringOfDay.TabIndex = 2;
            this.btnRefreshCoveringOfDay.TabStop = false;
            this.btnRefreshCoveringOfDay.ToolTip = "Пересчитать";
            this.btnRefreshCoveringOfDay.Click += new System.EventHandler(this.btnRefreshCoveringOfDay_Click);
            // 
            // cboxDayOfWeek
            // 
            this.cboxDayOfWeek.EditValue = "Понедельник";
            this.cboxDayOfWeek.Location = new System.Drawing.Point(200, 7);
            this.cboxDayOfWeek.MenuManager = this.barManagerReport;
            this.cboxDayOfWeek.Name = "cboxDayOfWeek";
            this.cboxDayOfWeek.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cboxDayOfWeek.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cboxDayOfWeek.Properties.Appearance.Options.UseBackColor = true;
            this.cboxDayOfWeek.Properties.Appearance.Options.UseFont = true;
            this.cboxDayOfWeek.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cboxDayOfWeek.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboxDayOfWeek.Properties.Items.AddRange(new object[] {
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            "Пятница",
            "Суббота",
            "Воскресенье"});
            this.cboxDayOfWeek.Properties.ShowPopupShadow = false;
            this.cboxDayOfWeek.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboxDayOfWeek.Size = new System.Drawing.Size(104, 18);
            this.cboxDayOfWeek.TabIndex = 1;
            this.cboxDayOfWeek.ToolTip = "Выбрать день недели";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.labelControl2.Location = new System.Drawing.Point(8, 9);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(184, 14);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Покрытие анкетами на день:";
            // 
            // pnReportHeader
            // 
            this.pnReportHeader.Controls.Add(this.edSupervisorText);
            this.pnReportHeader.Controls.Add(this.lbSupervisor);
            this.pnReportHeader.Controls.Add(this.lbReportTitle);
            this.pnReportHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportHeader.Location = new System.Drawing.Point(0, 40);
            this.pnReportHeader.Name = "pnReportHeader";
            this.pnReportHeader.Size = new System.Drawing.Size(640, 62);
            this.pnReportHeader.TabIndex = 1;
            // 
            // edSupervisorText
            // 
            this.edSupervisorText.Location = new System.Drawing.Point(88, 32);
            this.edSupervisorText.MenuManager = this.barManagerReport;
            this.edSupervisorText.Name = "edSupervisorText";
            this.edSupervisorText.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edSupervisorText.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edSupervisorText.Properties.Appearance.Options.UseBackColor = true;
            this.edSupervisorText.Properties.Appearance.Options.UseFont = true;
            this.edSupervisorText.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edSupervisorText.Properties.ReadOnly = true;
            this.edSupervisorText.Size = new System.Drawing.Size(200, 20);
            this.edSupervisorText.TabIndex = 2;
            this.edSupervisorText.TabStop = false;
            // 
            // lbSupervisor
            // 
            this.lbSupervisor.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbSupervisor.Location = new System.Drawing.Point(4, 35);
            this.lbSupervisor.Name = "lbSupervisor";
            this.lbSupervisor.Size = new System.Drawing.Size(78, 13);
            this.lbSupervisor.TabIndex = 1;
            this.lbSupervisor.Text = "Супервайзер:";
            // 
            // lbReportTitle
            // 
            this.lbReportTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbReportTitle.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbReportTitle.Location = new System.Drawing.Point(2, 2);
            this.lbReportTitle.Name = "lbReportTitle";
            this.lbReportTitle.Size = new System.Drawing.Size(636, 18);
            this.lbReportTitle.TabIndex = 0;
            this.lbReportTitle.Text = "Title";
            this.lbReportTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnReportToolBar
            // 
            this.pnReportToolBar.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pnReportToolBar.Controls.Add(this.standaloneBarDockControl1);
            this.pnReportToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportToolBar.Location = new System.Drawing.Point(0, 0);
            this.pnReportToolBar.Name = "pnReportToolBar";
            this.pnReportToolBar.Size = new System.Drawing.Size(640, 40);
            this.pnReportToolBar.TabIndex = 0;
            // 
            // WccpUIControl
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnReportSheet);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(640, 480);
            this.Load += new System.EventHandler(this.ReportControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).EndInit();
            this.pnReportSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcReports)).EndInit();
            this.tcReports.ResumeLayout(false);
            this.tpTP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProgressReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).EndInit();
            this.tpTT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).EndInit();
            this.tpInconsistency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInconsistency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvInconsistency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.tpCoveringOfDay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnClientCoveringOfDay)).EndInit();
            this.pnClientCoveringOfDay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnOptionsCoveringOfDay)).EndInit();
            this.pnOptionsCoveringOfDay.ResumeLayout(false);
            this.pnOptionsCoveringOfDay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboxDayOfWeek.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).EndInit();
            this.pnReportHeader.ResumeLayout(false);
            this.pnReportHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edSupervisorText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).EndInit();
            this.pnReportToolBar.ResumeLayout(false);
            this.pnReportToolBar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnReportSheet;
        private DevExpress.XtraEditors.PanelControl pnReportToolBar;
        private DevExpress.XtraBars.BarManager barManagerReport;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem tbtnReportToExcel;
        private DevExpress.Utils.ImageCollection imgLarge;
        private DevExpress.XtraEditors.PanelControl pnReportHeader;
        private System.Windows.Forms.Label lbReportTitle;
        private DevExpress.XtraEditors.TextEdit edSupervisorText;
        private DevExpress.XtraEditors.LabelControl lbSupervisor;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem tbtnReportRefresh;
        private ReportDataSet reportDataSet;
        private System.Windows.Forms.BindingSource bsProgressReport;
        private DevExpress.XtraTab.XtraTabControl tcReports;
        private DevExpress.XtraTab.XtraTabPage tpTP;
        private DevExpress.XtraTab.XtraTabPage tpTT;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField8;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField9;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField10;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField11;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField12;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField13;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField14;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField15;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField16;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField17;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField18;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField19;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField20;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField21;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField22;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField23;
        private DevExpress.XtraTab.XtraTabPage tpInconsistency;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gvInconsistency;
        private System.Windows.Forms.BindingSource bsInconsistency;
        private DevExpress.XtraTab.XtraTabPage tpCoveringOfDay;
        private DevExpress.XtraEditors.PanelControl pnClientCoveringOfDay;
        private DevExpress.XtraEditors.PanelControl pnOptionsCoveringOfDay;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cboxDayOfWeek;
        private DevExpress.XtraEditors.SimpleButton btnRefreshCoveringOfDay;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField24;
        private DevExpress.Utils.ImageCollection imgNormal;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraGrid.Columns.GridColumn colКритичность;
        private DevExpress.XtraGrid.Columns.GridColumn colДатаВизита;
        private DevExpress.XtraGrid.Columns.GridColumn colСВ;
        private DevExpress.XtraGrid.Columns.GridColumn colТП;
        private DevExpress.XtraGrid.Columns.GridColumn colКодTT;
        private DevExpress.XtraGrid.Columns.GridColumn colТТ;
        private DevExpress.XtraGrid.Columns.GridColumn colЮрИмяТТ;
        private DevExpress.XtraGrid.Columns.GridColumn colАдресТТ;
        private DevExpress.XtraGrid.Columns.GridColumn colЮрАдресТТ;
        private DevExpress.XtraGrid.Columns.GridColumn colКаналТТ;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colSKU;
        private DevExpress.XtraGrid.Columns.GridColumn colТипНесоотв;
        private DevExpress.XtraGrid.Columns.GridColumn colЗначение;
        private DevExpress.XtraGrid.Columns.GridColumn colМинЗнач;
        private DevExpress.XtraGrid.Columns.GridColumn colМаксЗнач;
        private DevExpress.XtraGrid.Columns.GridColumn colПодтверждено;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
    }
}
