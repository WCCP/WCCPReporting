﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WccpReporting
{
    public partial class fmWait : Form
    {
        public TimeSpan startTime;
        public TimeSpan elapsedTime;
        public TimeSpan elapsedTimeValue;
        public Timer    timerWait;

        public fmWait()
        {
            InitializeComponent();
            timerWait = new Timer();
            timerWait.Interval = 1000;
            timerWait.Enabled = true;
            timerWait.Tick += new EventHandler(timerWait_Tick);
            startTime = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }

         private void timerWait_Tick(object sender, EventArgs e)
        {
            elapsedTime = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            elapsedTimeValue = elapsedTime; //startTime - elapsedTime;
            lbElapsedTimeValue.Text = "123"; // Convert.ToString(elapsedTimeValue.Hours) + ":" + Convert.ToString(elapsedTimeValue.Minutes) + ":" + Convert.ToString(elapsedTimeValue.Seconds);
            lbElapsedTimeValue.Update();
            Application.DoEvents();
        }
    }

    public static class Wait
    {
        static fmWait _Wait = null;
        static public void StartWait()
        {
            if (_Wait == null)
            {
                _Wait = new fmWait();
                _Wait.Show();
                //_Wait.timerWait.Start();
                Application.DoEvents();
                Cursor.Current = Cursors.WaitCursor;
            };

        }
        static public void StopWait()
        {
            if (_Wait != null)
            {
                //_Wait.timerWait.Stop();
                _Wait.Close();
                _Wait.Dispose();
                _Wait = null;
                Cursor.Current = Cursors.Default;
                Application.DoEvents();
            };

        }
    }

}