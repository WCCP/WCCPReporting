﻿namespace WccpReporting
{
    partial class InputReportDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControlBaseDialog = new DevExpress.XtraLayout.LayoutControl();
            this.PanelBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.PanelClient = new DevExpress.XtraEditors.PanelControl();
            this.PageControlOptions = new DevExpress.XtraTab.XtraTabControl();
            this.OptionsTabSheet = new DevExpress.XtraTab.XtraTabPage();
            this.OptionsPanel = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.grpOptionsReport = new DevExpress.XtraEditors.GroupControl();
            this.cboxSupervisor = new DevExpress.XtraEditors.LookUpEdit();
            this.bsSupervisor = new System.Windows.Forms.BindingSource(this.components);
            this.reportDataSet = new WccpReporting.ReportDataSet();
            this.lbWave = new System.Windows.Forms.Label();
            this.edReportName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBaseDialog)).BeginInit();
            this.layoutControlBaseDialog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelBottom)).BeginInit();
            this.PanelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).BeginInit();
            this.PanelClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).BeginInit();
            this.PageControlOptions.SuspendLayout();
            this.OptionsTabSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).BeginInit();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).BeginInit();
            this.grpOptionsReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboxSupervisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSupervisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlBaseDialog
            // 
            this.layoutControlBaseDialog.Controls.Add(this.PanelBottom);
            this.layoutControlBaseDialog.Controls.Add(this.PanelClient);
            this.layoutControlBaseDialog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlBaseDialog.Location = new System.Drawing.Point(0, 0);
            this.layoutControlBaseDialog.Name = "layoutControlBaseDialog";
            this.layoutControlBaseDialog.Root = this.layoutControlGroup1;
            this.layoutControlBaseDialog.Size = new System.Drawing.Size(369, 450);
            this.layoutControlBaseDialog.TabIndex = 0;
            // 
            // PanelBottom
            // 
            this.PanelBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PanelBottom.Controls.Add(this.btnHelp);
            this.PanelBottom.Controls.Add(this.btnNo);
            this.PanelBottom.Controls.Add(this.btnYes);
            this.PanelBottom.Location = new System.Drawing.Point(0, 419);
            this.PanelBottom.Name = "PanelBottom";
            this.PanelBottom.Size = new System.Drawing.Size(369, 31);
            this.PanelBottom.TabIndex = 1;
            // 
            // btnHelp
            // 
            this.btnHelp.Image = global::Logica.Reports.wccpTR_Desktop_M2.Properties.Resources.help_16;
            this.btnHelp.Location = new System.Drawing.Point(287, 3);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 2;
            this.btnHelp.Text = "&Справка";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.Location = new System.Drawing.Point(206, 3);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 1;
            this.btnNo.Text = "Нет";
            // 
            // btnYes
            // 
            this.btnYes.Image = global::Logica.Reports.wccpTR_Desktop_M2.Properties.Resources.ok;
            this.btnYes.Location = new System.Drawing.Point(125, 3);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 0;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // PanelClient
            // 
            this.PanelClient.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PanelClient.Controls.Add(this.PageControlOptions);
            this.PanelClient.Location = new System.Drawing.Point(0, 0);
            this.PanelClient.Name = "PanelClient";
            this.PanelClient.Size = new System.Drawing.Size(369, 419);
            this.PanelClient.TabIndex = 0;
            // 
            // PageControlOptions
            // 
            this.PageControlOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageControlOptions.Location = new System.Drawing.Point(0, 0);
            this.PageControlOptions.Name = "PageControlOptions";
            this.PageControlOptions.SelectedTabPage = this.OptionsTabSheet;
            this.PageControlOptions.Size = new System.Drawing.Size(369, 419);
            this.PageControlOptions.TabIndex = 1;
            this.PageControlOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.OptionsTabSheet});
            // 
            // OptionsTabSheet
            // 
            this.OptionsTabSheet.Controls.Add(this.OptionsPanel);
            this.OptionsTabSheet.Name = "OptionsTabSheet";
            this.OptionsTabSheet.Size = new System.Drawing.Size(363, 391);
            this.OptionsTabSheet.Text = "Действие";
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Controls.Add(this.label1);
            this.OptionsPanel.Controls.Add(this.grpOptionsReport);
            this.OptionsPanel.Controls.Add(this.edReportName);
            this.OptionsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionsPanel.Location = new System.Drawing.Point(0, 0);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(363, 391);
            this.OptionsPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Отчет:";
            // 
            // grpOptionsReport
            // 
            this.grpOptionsReport.Controls.Add(this.cboxSupervisor);
            this.grpOptionsReport.Controls.Add(this.lbWave);
            this.grpOptionsReport.Location = new System.Drawing.Point(8, 121);
            this.grpOptionsReport.Name = "grpOptionsReport";
            this.grpOptionsReport.Size = new System.Drawing.Size(345, 255);
            this.grpOptionsReport.TabIndex = 1;
            this.grpOptionsReport.Text = " Опции отчета: ";
            // 
            // cboxSupervisor
            // 
            this.cboxSupervisor.EnterMoveNextControl = true;
            this.cboxSupervisor.Location = new System.Drawing.Point(7, 42);
            this.cboxSupervisor.Name = "cboxSupervisor";
            this.cboxSupervisor.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFit;
            this.cboxSupervisor.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.cboxSupervisor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboxSupervisor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", 100, "Супервайзер")});
            this.cboxSupervisor.Properties.DataSource = this.bsSupervisor;
            this.cboxSupervisor.Properties.DisplayMember = "Supervisor_name";
            this.cboxSupervisor.Properties.DropDownRows = 20;
            this.cboxSupervisor.Properties.NullText = "Не выбран";
            this.cboxSupervisor.Properties.ValueMember = "Supervisor_id";
            this.cboxSupervisor.Size = new System.Drawing.Size(330, 22);
            this.cboxSupervisor.TabIndex = 3;
            // 
            // bsSupervisor
            // 
            this.bsSupervisor.AllowNew = true;
            this.bsSupervisor.DataMember = "tblSupervisors";
            this.bsSupervisor.DataSource = this.reportDataSet;
            // 
            // reportDataSet
            // 
            this.reportDataSet.DataSetName = "ReportDataSet";
            this.reportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lbWave
            // 
            this.lbWave.AutoSize = true;
            this.lbWave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbWave.Location = new System.Drawing.Point(7, 26);
            this.lbWave.Name = "lbWave";
            this.lbWave.Size = new System.Drawing.Size(77, 13);
            this.lbWave.TabIndex = 2;
            this.lbWave.Text = "Супервайзер:";
            // 
            // edReportName
            // 
            this.edReportName.Location = new System.Drawing.Point(8, 20);
            this.edReportName.Name = "edReportName";
            this.edReportName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.edReportName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edReportName.Properties.Appearance.Options.UseBackColor = true;
            this.edReportName.Properties.Appearance.Options.UseFont = true;
            this.edReportName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.edReportName.Properties.ReadOnly = true;
            this.edReportName.Size = new System.Drawing.Size(344, 22);
            this.edReportName.TabIndex = 0;
            this.edReportName.TabStop = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.ShowInCustomizationForm = false;
            this.layoutControlGroup1.Size = new System.Drawing.Size(369, 450);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.PanelClient;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(369, 419);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.PanelBottom;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 419);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(369, 31);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // InputReportDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(369, 450);
            this.Controls.Add(this.layoutControlBaseDialog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputReportDataForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отчет";
            this.Load += new System.EventHandler(this.InputProgressReportDataForm_Load);
            this.Shown += new System.EventHandler(this.InputProgressReportDataForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBaseDialog)).EndInit();
            this.layoutControlBaseDialog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelBottom)).EndInit();
            this.PanelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelClient)).EndInit();
            this.PanelClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PageControlOptions)).EndInit();
            this.PageControlOptions.ResumeLayout(false);
            this.OptionsTabSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).EndInit();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOptionsReport)).EndInit();
            this.grpOptionsReport.ResumeLayout(false);
            this.grpOptionsReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboxSupervisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSupervisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edReportName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlBaseDialog;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        public DevExpress.XtraEditors.PanelControl PanelClient;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.PanelControl PanelBottom;
        public DevExpress.XtraEditors.SimpleButton btnHelp;
        public DevExpress.XtraEditors.SimpleButton btnYes;
        public DevExpress.XtraTab.XtraTabControl PageControlOptions;
        public DevExpress.XtraTab.XtraTabPage OptionsTabSheet;
        public DevExpress.XtraEditors.PanelControl OptionsPanel;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.GroupControl grpOptionsReport;
        protected DevExpress.XtraEditors.TextEdit edReportName;
        private DevExpress.XtraEditors.LookUpEdit cboxSupervisor;
        private System.Windows.Forms.Label lbWave;
        private ReportDataSet reportDataSet;
        private System.Windows.Forms.BindingSource bsSupervisor;

    }
}