﻿using System;
using System.Data;
using System.Windows.Forms;
using Logica.Reports.DataAccess;
using Logica.Reports.wccpTR_Desktop_M2.Properties;

namespace WccpReporting
{
    public partial class InputReportDataForm : Form
    {
        private const string SQL_GET_SUPERVISERS =
@"SELECT distinct tblSupervisors.Supervisor_name, tblSupervisors.Supervisor_id, tblRegions.Region_Name 
FROM tblSupervisors 
INNER JOIN tblMerchandisers ON tblSupervisors.Supervisor_id = tblMerchandisers.Supervisor_id 
INNER JOIN tblRegions ON tblRegions.Region_ID=tblSupervisors.Region_ID 
INNER JOIN tblMerchandiserUserTypes tmut ON tmut.Merch_id = tblMerchandisers.Merch_id
Where tblRegions.Region_ID>0 
  and tblSupervisors.Supervisor_id>0 
  AND tmut.Status<>9
  AND tmut.UserType_id IN (2,5)";

        private string _reportName;
        public string ReportName
        {
            get { return _reportName; }
            set
            {
                _reportName = value;
                edReportName.Text = value;
            }
        }

        private string _RegionName;
        public string RegionName
        {
            get { return _RegionName; }
            set { _RegionName = value; }
        }

        private string _ChanelName;
        public string ChanelName
        {
            get { return _ChanelName; }
            set { _ChanelName = value; }
        }

        private int _SupervisorId;
        public int SupervisorId
        {
            get { return _SupervisorId; }
            set { _SupervisorId = value; }
        }

        private string _SupervisorName;
        public string SupervisorName
        {
            get { return _SupervisorName; }
            set { _SupervisorName = value; }
        }

        public InputReportDataForm()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboxSupervisor.EditValue == null)
                {
                    throw new Exception(WCCPConst.msgErrorSupervisorIsNull);
                }

                SupervisorId = (int)cboxSupervisor.EditValue;
                SupervisorName = cboxSupervisor.Text;

                if (reportDataSet.tblSupervisors.Rows.Count > 2)
                    RegionName = (string)reportDataSet.tblSupervisors[2]["Region_Name"];
                else
                    RegionName = String.Empty;

                //string s = SupervisorName.Remove(0, SupervisorName.IndexOf("."));

                SupervisorName = SupervisorName.Trim();
                string s = SupervisorName.Substring(SupervisorName.Length - 2, 2);

                if (s.IndexOf("он") >= 0 || s.IndexOf("ОН") >= 0)
                    ChanelName = "On-trade";
                else
                    ChanelName = "Off-trade";

                Settings.Default.SupervisorId = SupervisorId;
                Settings.Default.Save();
                //
                ReportOptions.SupervisorId = SupervisorId;
                ReportOptions.SupervisorName = SupervisorName;
                ReportOptions.RegionName = RegionName;
                ReportOptions.ChanelName = ChanelName;
                //
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                cboxSupervisor.Focus();
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(WCCPConst.msgNoHelp, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void InputProgressReportDataForm_Shown(object sender, EventArgs e)
        {
            if (Settings.Default.SupervisorId > 0)
            {
                cboxSupervisor.EditValue = Settings.Default.SupervisorId;
            }
            else
            {
                if (Settings.Default.SupervisorId == -1)
                {
                    cboxSupervisor.EditValue = -1;
                }
                else
                    if (Settings.Default.SupervisorId == -2)
                    {
                        cboxSupervisor.EditValue = -2;
                    }
            }

            cboxSupervisor.Focus();
        }

        //Загрузить форму
        private void InputProgressReportDataForm_Load(object sender, EventArgs e)
        {
            try
            {
                DataRow newRow;
                newRow = reportDataSet.tblSupervisors.NewRow();
                newRow["Supervisor_name"] = "Все ОФФ";
                newRow["Supervisor_id"] = -1;
                newRow["Region_Name"] = String.Empty;
                reportDataSet.tblSupervisors.Rows.Add(newRow);

                newRow = reportDataSet.tblSupervisors.NewRow();
                newRow["Supervisor_name"] = "Все ОН";
                newRow["Supervisor_id"] = -2;
                newRow["Region_Name"] = String.Empty;
                reportDataSet.tblSupervisors.Rows.Add(newRow);

                var supervisorsDs = DataAccessLayer.ExecuteQuery(SQL_GET_SUPERVISERS);
                if (supervisorsDs.Tables.Count > 0)
                {
                    foreach (DataRow row in supervisorsDs.Tables[0].Rows)
                    {
                        newRow = reportDataSet.tblSupervisors.NewRow();
                        newRow["Supervisor_name"] = row["Supervisor_name"];
                        newRow["Supervisor_id"] = row["Supervisor_id"];
                        newRow["Region_Name"] = row["Region_Name"];
                        reportDataSet.tblSupervisors.Rows.Add(newRow);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateSupervisorList + "\n(" + ex.Message + ")", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }
    }
}