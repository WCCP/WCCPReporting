﻿namespace WccpReporting {
    public static class ReportOptions {
        public static string ReportTitle;
        public static int SupervisorId;
        public static string SupervisorName;
        public static string RegionName;
        public static string ChanelName;
    }

    public static class WCCPConst {
        public static string frptexlTPReport = "TR_TPReport.xlsx";
        public static string frptexlTTReport = "TR_TTReport.xlsx";
        public static string frptexlIncReport = "TR_IncReport.xlsx";
        public static string frptexlDayOfWeekReport = "TR_DayOfWeekReport.xlsx";

        public static string msgNoHelp = "В данной версии программы справка отсутствует";
        public static string msgErrorCreateWaveList = "Не могу создать список волн";
        public static string msgErrorCreateSupervisorList = "Не могу создать список супервайзеров";
        public static string msgErrorSupervisorIsNull = "Супервайзер должен быть задан";
        public static string msgErrorPrepare = "Не могу сформировать предварительный набор данных";
        public static string msgErrorCloseConnection = "Не могу закрыть соединение";
    }
}