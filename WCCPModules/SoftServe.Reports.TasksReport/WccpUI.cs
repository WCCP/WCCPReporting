﻿using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Forms;
using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;
using SoftServe.Reports.TasksReport;

namespace WccpReporting
{
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpTasksReport.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }


        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                _reportControl = new WccpUIControl();
                _reportCaption = reportCaption;

                return _reportControl.InitReport();
            }
            catch (Exception lException)
            {
                ErrorManager.ShowErrorBox(lException.Message);
            }

            return 1;
        }

        /// <summary>
        /// Closes the UI.
        /// </summary>
        public void CloseUI()
        {
            if (_reportControl != null)
            {
                _reportControl.SaveLayout();
            }
        }

        public bool AllowClose()
        {
            return true;
        }
    }
}