﻿using System.ComponentModel;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using SoftServe.Reports.TasksReport.Controls;

namespace SoftServe.Reports.TasksReport
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this._tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabStaffExec = new DevExpress.XtraTab.XtraTabPage();
            this.resultsByStaffControl = new SoftServe.Reports.TasksReport.Controls.ResultsByStaffControl();
            this.tabNonChecked = new DevExpress.XtraTab.XtraTabPage();
            this.notCheckedControl = new SoftServe.Reports.TasksReport.Controls.NotCheckedControl();
            this.tabDetails = new DevExpress.XtraTab.XtraTabPage();
            this.detailsControl = new SoftServe.Reports.TasksReport.Controls.DataControl();
            this.tabData = new DevExpress.XtraTab.XtraTabPage();
            this.dataControl = new SoftServe.Reports.TasksReport.Controls.DataControl();
            this.tabImages = new DevExpress.XtraTab.XtraTabPage();
            this.imagesControl = new SoftServe.Reports.TasksReport.Controls.ImagesControl();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.menuBar = new DevExpress.XtraBars.Bar();
            this.bRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bSettings = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bExport = new DevExpress.XtraBars.BarSubItem();
            this.bExportToXlsx = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bExportToCsv = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bClearFilter = new DevExpress.XtraBars.BarButtonItem();
            this.bEditFilter = new DevExpress.XtraBars.BarButtonItem();
            this.cShowTotals = new DevExpress.XtraBars.BarCheckItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barToggleSwitchItem1 = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).BeginInit();
            this._tabControl.SuspendLayout();
            this.tabStaffExec.SuspendLayout();
            this.tabNonChecked.SuspendLayout();
            this.tabDetails.SuspendLayout();
            this.tabData.SuspendLayout();
            this.tabImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this._tabControl.Location = new System.Drawing.Point(0, 59);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedTabPage = this.tabStaffExec;
            this._tabControl.Size = new System.Drawing.Size(1184, 537);
            this._tabControl.TabIndex = 0;
            this._tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabStaffExec,
            this.tabNonChecked,
            this.tabDetails,
            this.tabData,
            this.tabImages});
            this._tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this._tabControl_SelectedPageChanged);
            // 
            // tabStaffExec
            // 
            this.tabStaffExec.Controls.Add(this.resultsByStaffControl);
            this.tabStaffExec.Name = "tabStaffExec";
            this.tabStaffExec.Size = new System.Drawing.Size(1178, 509);
            this.tabStaffExec.Text = "Персонал-выполнение задач";
            // 
            // resultsByStaffControl
            // 
            this.resultsByStaffControl.DataSource = null;
            this.resultsByStaffControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsByStaffControl.Location = new System.Drawing.Point(0, 0);
            this.resultsByStaffControl.Name = "resultsByStaffControl";
            this.resultsByStaffControl.Size = new System.Drawing.Size(1178, 509);
            this.resultsByStaffControl.TabIndex = 0;
            // 
            // tabNonChecked
            // 
            this.tabNonChecked.Controls.Add(this.notCheckedControl);
            this.tabNonChecked.Name = "tabNonChecked";
            this.tabNonChecked.Size = new System.Drawing.Size(1178, 509);
            this.tabNonChecked.Text = "Не проверено руководителем";
            // 
            // notCheckedControl
            // 
            this.notCheckedControl.DataSource = null;
            this.notCheckedControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notCheckedControl.Location = new System.Drawing.Point(0, 0);
            this.notCheckedControl.Name = "notCheckedControl";
            this.notCheckedControl.Size = new System.Drawing.Size(1178, 509);
            this.notCheckedControl.TabIndex = 0;
            // 
            // tabDetails
            // 
            this.tabDetails.AllowTouchScroll = true;
            this.tabDetails.Controls.Add(this.detailsControl);
            this.tabDetails.Name = "tabDetails";
            this.tabDetails.Size = new System.Drawing.Size(1178, 509);
            this.tabDetails.Text = "Подробно";
            // 
            // detailsControl
            // 
            this.detailsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsControl.Location = new System.Drawing.Point(0, 0);
            this.detailsControl.Name = "detailsControl";
            this.detailsControl.Size = new System.Drawing.Size(1178, 509);
            this.detailsControl.TabIndex = 0;
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.dataControl);
            this.tabData.Name = "tabData";
            this.tabData.Size = new System.Drawing.Size(1178, 509);
            this.tabData.Text = "Данные";
            // 
            // dataControl
            // 
            this.dataControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataControl.Location = new System.Drawing.Point(0, 0);
            this.dataControl.Name = "dataControl";
            this.dataControl.Size = new System.Drawing.Size(1178, 509);
            this.dataControl.TabIndex = 0;
            // 
            // tabImages
            // 
            this.tabImages.Controls.Add(this.imagesControl);
            this.tabImages.Name = "tabImages";
            this.tabImages.Size = new System.Drawing.Size(1178, 509);
            this.tabImages.Text = "Фотографии";
            // 
            // imagesControl
            // 
            this.imagesControl.AutoScroll = true;
            this.imagesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imagesControl.Location = new System.Drawing.Point(0, 0);
            this.imagesControl.Name = "imagesControl";
            this.imagesControl.Size = new System.Drawing.Size(1178, 509);
            this.imagesControl.TabIndex = 0;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.menuBar,
            this.bar2,
            this.bar3});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bSettings,
            this.bRefresh,
            this.bExport,
            this.bExportToXlsx,
            this.bExportToCsv,
            this.barSubItem2,
            this.barButtonItem1,
            this.bClearFilter,
            this.bEditFilter,
            this.barToggleSwitchItem1,
            this.cShowTotals});
            this.barManager.LargeImages = this.imageCollection;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 11;
            this.barManager.StatusBar = this.bar3;
            // 
            // menuBar
            // 
            this.menuBar.BarName = "Tools";
            this.menuBar.DockCol = 0;
            this.menuBar.DockRow = 0;
            this.menuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.menuBar.FloatLocation = new System.Drawing.Point(730, 120);
            this.menuBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bSettings),
            new DevExpress.XtraBars.LinkPersistInfo(this.bExport),
            new DevExpress.XtraBars.LinkPersistInfo(this.bClearFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bEditFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.cShowTotals)});
            this.menuBar.OptionsBar.AllowQuickCustomization = false;
            this.menuBar.OptionsBar.DisableClose = true;
            this.menuBar.OptionsBar.DisableCustomization = true;
            this.menuBar.OptionsBar.UseWholeRow = true;
            this.menuBar.Text = "Tools";
            // 
            // bRefresh
            // 
            this.bRefresh.Caption = "Обновить";
            this.bRefresh.Id = 1;
            this.bRefresh.LargeImageIndex = 4;
            this.bRefresh.Name = "bRefresh";
            this.bRefresh.ShowCaptionOnBar = false;
            this.bRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bRefresh_ItemClick);
            // 
            // bSettings
            // 
            this.bSettings.Caption = "Настройка параметров";
            this.bSettings.Id = 0;
            this.bSettings.LargeImageIndex = 0;
            this.bSettings.Name = "bSettings";
            this.bSettings.ShowCaptionOnBar = false;
            this.bSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bSettings_ItemClick);
            // 
            // bExport
            // 
            this.bExport.Glyph = ((System.Drawing.Image)(resources.GetObject("bExport.Glyph")));
            this.bExport.Id = 2;
            this.bExport.LargeImageIndex = 3;
            this.bExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bExportToXlsx),
            new DevExpress.XtraBars.LinkPersistInfo(this.bExportToCsv)});
            this.bExport.Name = "bExport";
            this.bExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bExportToXlsx
            // 
            this.bExportToXlsx.Caption = "Xlsx";
            this.bExportToXlsx.Glyph = ((System.Drawing.Image)(resources.GetObject("bExportToXlsx.Glyph")));
            this.bExportToXlsx.Id = 3;
            this.bExportToXlsx.LargeImageIndex = 2;
            this.bExportToXlsx.Name = "bExportToXlsx";
            this.bExportToXlsx.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bExportToXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bExportToXlsx_ItemClick);
            // 
            // bExportToCsv
            // 
            this.bExportToCsv.Caption = "Csv";
            this.bExportToCsv.Glyph = ((System.Drawing.Image)(resources.GetObject("bExportToCsv.Glyph")));
            this.bExportToCsv.Id = 4;
            this.bExportToCsv.LargeImageIndex = 1;
            this.bExportToCsv.Name = "bExportToCsv";
            this.bExportToCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bExportToCsv_ItemClick);
            // 
            // bClearFilter
            // 
            this.bClearFilter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bClearFilter.Caption = "Очистить фильтр";
            this.bClearFilter.Id = 7;
            this.bClearFilter.Name = "bClearFilter";
            this.bClearFilter.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bClearFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bClearFilter_ItemClick);
            // 
            // bEditFilter
            // 
            this.bEditFilter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bEditFilter.Caption = "Редактировать фильтр";
            this.bEditFilter.Id = 8;
            this.bEditFilter.Name = "bEditFilter";
            this.bEditFilter.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bEditFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bEditFilter_ItemClick);
            // 
            // cShowTotals
            // 
            this.cShowTotals.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.cShowTotals.Caption = "Отобразить Totals";
            this.cShowTotals.Id = 10;
            this.cShowTotals.Name = "cShowTotals";
            this.cShowTotals.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.cShowTotals_CheckedChanged);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            this.bar2.Visible = false;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1184, 59);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 596);
            this.barDockControlBottom.Size = new System.Drawing.Size(1184, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 59);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 537);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1184, 59);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 537);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "barSubItem2";
            this.barSubItem2.Id = 5;
            this.barSubItem2.LargeImageIndex = 3;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barToggleSwitchItem1
            // 
            this.barToggleSwitchItem1.Caption = "barToggleSwitchItem1";
            this.barToggleSwitchItem1.Id = 9;
            this.barToggleSwitchItem1.Name = "barToggleSwitchItem1";
            // 
            // imageCollection
            // 
            this.imageCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "briefcase_ok_24.png");
            this.imageCollection.Images.SetKeyName(1, "csv.PNG");
            this.imageCollection.Images.SetKeyName(2, "Excel_42.bmp");
            this.imageCollection.Images.SetKeyName(3, "image_refresh_24.png");
            this.imageCollection.Images.SetKeyName(4, "refresh_24.png");
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(1184, 619);
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).EndInit();
            this._tabControl.ResumeLayout(false);
            this.tabStaffExec.ResumeLayout(false);
            this.tabNonChecked.ResumeLayout(false);
            this.tabDetails.ResumeLayout(false);
            this.tabData.ResumeLayout(false);
            this.tabImages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private XtraTabControl _tabControl;
        private XtraTabPage tabStaffExec;
        private XtraTabPage tabNonChecked;
        private XtraTabPage tabDetails;
        private XtraTabPage tabData;
        private BarManager barManager;
        private Bar menuBar;
        private Bar bar2;
        private Bar bar3;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private BarLargeButtonItem bSettings;
        private BarLargeButtonItem bRefresh;
        private BarSubItem bExport;
        private BarLargeButtonItem bExportToXlsx;
        private BarLargeButtonItem bExportToCsv;
        private ImageCollection imageCollection;
        private BarSubItem barSubItem2;
        private DataControl dataControl;
        private DataControl detailsControl;
        private ResultsByStaffControl resultsByStaffControl;
        private NotCheckedControl notCheckedControl;
        private BarButtonItem bClearFilter;
        private BarButtonItem bEditFilter;
        private BarToggleSwitchItem barToggleSwitchItem1;
        private BarButtonItem barButtonItem1;
        private BarCheckItem cShowTotals;
        private XtraTabPage tabImages;
        private ImagesControl imagesControl;
    }
}
