﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;

namespace SoftServe.Reports.TasksReport
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.rUserLevel = new DevExpress.XtraEditors.RadioGroup();
            this.lUserLevel = new DevExpress.XtraEditors.LabelControl();
            this.lChannels = new DevExpress.XtraEditors.LabelControl();
            this.cStaffChannel = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.tStaff = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colChannelMask = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.dFromCreation = new DevExpress.XtraEditors.DateEdit();
            this.dFromCompletion = new DevExpress.XtraEditors.DateEdit();
            this.dToCreation = new DevExpress.XtraEditors.DateEdit();
            this.dToCompletion = new DevExpress.XtraEditors.DateEdit();
            this.cDateCreation = new DevExpress.XtraEditors.CheckEdit();
            this.cDateCompletion = new DevExpress.XtraEditors.CheckEdit();
            this.gDates = new DevExpress.XtraEditors.GroupControl();
            this.lUse = new DevExpress.XtraEditors.LabelControl();
            this.lFrom2 = new DevExpress.XtraEditors.LabelControl();
            this.lTo2 = new DevExpress.XtraEditors.LabelControl();
            this.lFrom1 = new DevExpress.XtraEditors.LabelControl();
            this.lTo1 = new DevExpress.XtraEditors.LabelControl();
            this.gStaff = new DevExpress.XtraEditors.GroupControl();
            this.cStaffAll = new DevExpress.XtraEditors.CheckEdit();
            this.gStaffConditions = new DevExpress.XtraEditors.GroupControl();
            this.bOk = new DevExpress.XtraEditors.SimpleButton();
            this.bCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.rUserLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCompletion.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCompletion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCompletion.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCompletion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDateCreation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDateCompletion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gDates)).BeginInit();
            this.gDates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gStaff)).BeginInit();
            this.gStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gStaffConditions)).BeginInit();
            this.gStaffConditions.SuspendLayout();
            this.SuspendLayout();
            // 
            // rUserLevel
            // 
            this.rUserLevel.EditValue = 1D;
            this.rUserLevel.Location = new System.Drawing.Point(44, 49);
            this.rUserLevel.Name = "rUserLevel";
            this.rUserLevel.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rUserLevel.Properties.Appearance.Options.UseBackColor = true;
            this.rUserLevel.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rUserLevel.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(4D, "M4"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3D, "M3"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2D, "M2"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1D, "M1")});
            this.rUserLevel.Size = new System.Drawing.Size(219, 23);
            this.rUserLevel.TabIndex = 10;
            this.rUserLevel.SelectedIndexChanged += new System.EventHandler(this.rUserLevel_SelectedIndexChanged);
            // 
            // lUserLevel
            // 
            this.lUserLevel.Location = new System.Drawing.Point(12, 30);
            this.lUserLevel.Name = "lUserLevel";
            this.lUserLevel.Size = new System.Drawing.Size(192, 13);
            this.lUserLevel.TabIndex = 9;
            this.lUserLevel.Text = "Нижний уровень персонала в отчете:";
            // 
            // lChannels
            // 
            this.lChannels.Location = new System.Drawing.Point(12, 91);
            this.lChannels.Name = "lChannels";
            this.lChannels.Size = new System.Drawing.Size(91, 13);
            this.lChannels.TabIndex = 11;
            this.lChannels.Text = "Канал персонала:";
            // 
            // cStaffChannel
            // 
            this.cStaffChannel.Location = new System.Drawing.Point(111, 88);
            this.cStaffChannel.Name = "cStaffChannel";
            this.cStaffChannel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cStaffChannel.Properties.SelectAllItemCaption = "(Все)";
            this.cStaffChannel.Size = new System.Drawing.Size(128, 20);
            this.cStaffChannel.TabIndex = 13;
            this.cStaffChannel.EditValueChanged += new System.EventHandler(this.cStaffChannel_EditValueChanged);
            // 
            // tStaff
            // 
            this.tStaff.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colStaffId,
            this.colLevel,
            this.colChannelMask});
            this.tStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tStaff.KeyFieldName = "Id";
            this.tStaff.Location = new System.Drawing.Point(2, 21);
            this.tStaff.Name = "tStaff";
            this.tStaff.OptionsBehavior.AllowQuickHideColumns = false;
            this.tStaff.OptionsBehavior.AllowRecursiveNodeChecking = true;
            this.tStaff.OptionsBehavior.Editable = false;
            this.tStaff.OptionsBehavior.EnableFiltering = true;
            this.tStaff.OptionsBehavior.ReadOnly = true;
            this.tStaff.OptionsBehavior.ShowToolTips = false;
            this.tStaff.OptionsFilter.AllowColumnMRUFilterList = false;
            this.tStaff.OptionsFilter.AllowMRUFilterList = false;
            this.tStaff.OptionsFilter.ShowAllValuesInCheckedFilterPopup = false;
            this.tStaff.OptionsFilter.ShowAllValuesInFilterPopup = true;
            this.tStaff.OptionsMenu.EnableColumnMenu = false;
            this.tStaff.OptionsMenu.EnableFooterMenu = false;
            this.tStaff.OptionsMenu.ShowAutoFilterRowItem = false;
            this.tStaff.OptionsView.ShowCheckBoxes = true;
            this.tStaff.OptionsView.ShowColumns = false;
            this.tStaff.OptionsView.ShowHorzLines = false;
            this.tStaff.OptionsView.ShowIndicator = false;
            this.tStaff.OptionsView.ShowVertLines = false;
            this.tStaff.ParentFieldName = "ParentId";
            this.tStaff.Size = new System.Drawing.Size(324, 265);
            this.tStaff.TabIndex = 15;
            this.tStaff.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tStaff_AfterCheckNode);
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 32;
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colStaffId
            // 
            this.colStaffId.Caption = "StaffId";
            this.colStaffId.FieldName = "StaffId";
            this.colStaffId.Name = "colStaffId";
            // 
            // colLevel
            // 
            this.colLevel.Caption = "Level";
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            // 
            // colChannelMask
            // 
            this.colChannelMask.Caption = "ChannelMask";
            this.colChannelMask.FieldName = "ChannelMask";
            this.colChannelMask.Name = "colChannelMask";
            // 
            // dFromCreation
            // 
            this.dFromCreation.EditValue = null;
            this.dFromCreation.Location = new System.Drawing.Point(44, 68);
            this.dFromCreation.Name = "dFromCreation";
            this.dFromCreation.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dFromCreation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFromCreation.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFromCreation.Size = new System.Drawing.Size(86, 20);
            this.dFromCreation.TabIndex = 16;
            this.dFromCreation.EditValueChanged += new System.EventHandler(this.datesCreation_EditValueChanged);
            // 
            // dFromCompletion
            // 
            this.dFromCompletion.EditValue = null;
            this.dFromCompletion.Location = new System.Drawing.Point(44, 124);
            this.dFromCompletion.Name = "dFromCompletion";
            this.dFromCompletion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dFromCompletion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFromCompletion.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dFromCompletion.Size = new System.Drawing.Size(86, 20);
            this.dFromCompletion.TabIndex = 17;
            this.dFromCompletion.EditValueChanged += new System.EventHandler(this.datesCompletion_EditValueChanged);
            // 
            // dToCreation
            // 
            this.dToCreation.EditValue = null;
            this.dToCreation.Location = new System.Drawing.Point(154, 68);
            this.dToCreation.Name = "dToCreation";
            this.dToCreation.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dToCreation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dToCreation.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dToCreation.Size = new System.Drawing.Size(85, 20);
            this.dToCreation.TabIndex = 18;
            this.dToCreation.EditValueChanged += new System.EventHandler(this.datesCreation_EditValueChanged);
            // 
            // dToCompletion
            // 
            this.dToCompletion.EditValue = null;
            this.dToCompletion.Location = new System.Drawing.Point(154, 124);
            this.dToCompletion.Name = "dToCompletion";
            this.dToCompletion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dToCompletion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dToCompletion.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dToCompletion.Size = new System.Drawing.Size(85, 20);
            this.dToCompletion.TabIndex = 19;
            this.dToCompletion.EditValueChanged += new System.EventHandler(this.datesCompletion_EditValueChanged);
            // 
            // cDateCreation
            // 
            this.cDateCreation.EditValue = true;
            this.cDateCreation.Location = new System.Drawing.Point(13, 47);
            this.cDateCreation.Name = "cDateCreation";
            this.cDateCreation.Properties.Caption = "Дата создания задачи:";
            this.cDateCreation.Size = new System.Drawing.Size(139, 19);
            this.cDateCreation.TabIndex = 20;
            this.cDateCreation.CheckedChanged += new System.EventHandler(this.cDateCreation_CheckedChanged);
            // 
            // cDateCompletion
            // 
            this.cDateCompletion.EditValue = true;
            this.cDateCompletion.Location = new System.Drawing.Point(14, 101);
            this.cDateCompletion.Name = "cDateCompletion";
            this.cDateCompletion.Properties.Caption = "Дата выполнения задачи:";
            this.cDateCompletion.Size = new System.Drawing.Size(159, 19);
            this.cDateCompletion.TabIndex = 21;
            this.cDateCompletion.CheckedChanged += new System.EventHandler(this.cDateCompletion_CheckedChanged);
            // 
            // gDates
            // 
            this.gDates.Controls.Add(this.lUse);
            this.gDates.Controls.Add(this.lFrom2);
            this.gDates.Controls.Add(this.cDateCompletion);
            this.gDates.Controls.Add(this.lTo2);
            this.gDates.Controls.Add(this.lFrom1);
            this.gDates.Controls.Add(this.dFromCompletion);
            this.gDates.Controls.Add(this.lTo1);
            this.gDates.Controls.Add(this.dToCreation);
            this.gDates.Controls.Add(this.cDateCreation);
            this.gDates.Controls.Add(this.dFromCreation);
            this.gDates.Controls.Add(this.dToCompletion);
            this.gDates.Location = new System.Drawing.Point(6, 5);
            this.gDates.Name = "gDates";
            this.gDates.Size = new System.Drawing.Size(251, 159);
            this.gDates.TabIndex = 22;
            this.gDates.Text = "Период генерации отчета";
            // 
            // lUse
            // 
            this.lUse.Location = new System.Drawing.Point(12, 26);
            this.lUse.Name = "lUse";
            this.lUse.Size = new System.Drawing.Size(75, 13);
            this.lUse.TabIndex = 15;
            this.lUse.Text = "Использовать:";
            // 
            // lFrom2
            // 
            this.lFrom2.Location = new System.Drawing.Point(33, 128);
            this.lFrom2.Name = "lFrom2";
            this.lFrom2.Size = new System.Drawing.Size(5, 13);
            this.lFrom2.TabIndex = 14;
            this.lFrom2.Text = "с";
            // 
            // lTo2
            // 
            this.lTo2.Location = new System.Drawing.Point(136, 128);
            this.lTo2.Name = "lTo2";
            this.lTo2.Size = new System.Drawing.Size(12, 13);
            this.lTo2.TabIndex = 28;
            this.lTo2.Text = "по";
            // 
            // lFrom1
            // 
            this.lFrom1.Location = new System.Drawing.Point(33, 71);
            this.lFrom1.Name = "lFrom1";
            this.lFrom1.Size = new System.Drawing.Size(5, 13);
            this.lFrom1.TabIndex = 15;
            this.lFrom1.Text = "с";
            // 
            // lTo1
            // 
            this.lTo1.Location = new System.Drawing.Point(136, 71);
            this.lTo1.Name = "lTo1";
            this.lTo1.Size = new System.Drawing.Size(12, 13);
            this.lTo1.TabIndex = 29;
            this.lTo1.Text = "по";
            // 
            // gStaff
            // 
            this.gStaff.Controls.Add(this.cStaffAll);
            this.gStaff.Controls.Add(this.tStaff);
            this.gStaff.Location = new System.Drawing.Point(263, 5);
            this.gStaff.Name = "gStaff";
            this.gStaff.Size = new System.Drawing.Size(328, 288);
            this.gStaff.TabIndex = 24;
            this.gStaff.Text = "Персонал";
            // 
            // cStaffAll
            // 
            this.cStaffAll.Location = new System.Drawing.Point(282, 1);
            this.cStaffAll.Name = "cStaffAll";
            this.cStaffAll.Properties.AllowGrayed = true;
            this.cStaffAll.Properties.Caption = "Все";
            this.cStaffAll.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cStaffAll.Size = new System.Drawing.Size(42, 19);
            this.cStaffAll.TabIndex = 21;
            this.cStaffAll.TabStop = false;
            this.cStaffAll.CheckStateChanged += new System.EventHandler(this.cStaffAll_CheckStateChanged);
            this.cStaffAll.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cStaffAll_EditValueChanging);
            // 
            // gStaffConditions
            // 
            this.gStaffConditions.Controls.Add(this.rUserLevel);
            this.gStaffConditions.Controls.Add(this.lUserLevel);
            this.gStaffConditions.Controls.Add(this.cStaffChannel);
            this.gStaffConditions.Controls.Add(this.lChannels);
            this.gStaffConditions.Location = new System.Drawing.Point(6, 170);
            this.gStaffConditions.Name = "gStaffConditions";
            this.gStaffConditions.Size = new System.Drawing.Size(251, 123);
            this.gStaffConditions.TabIndex = 25;
            this.gStaffConditions.Text = "Настройки персонала";
            // 
            // bOk
            // 
            this.bOk.Image = ((System.Drawing.Image)(resources.GetObject("bOk.Image")));
            this.bOk.Location = new System.Drawing.Point(345, 299);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(146, 23);
            this.bOk.TabIndex = 26;
            this.bOk.Text = "Сгенерировать отчет";
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Image = ((System.Drawing.Image)(resources.GetObject("bCancel.Image")));
            this.bCancel.Location = new System.Drawing.Point(497, 299);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(92, 23);
            this.bCancel.TabIndex = 27;
            this.bCancel.Text = "Отменить";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(596, 329);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.gStaffConditions);
            this.Controls.Add(this.gStaff);
            this.Controls.Add(this.gDates);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры к Отчету по задачам";
            ((System.ComponentModel.ISupportInitialize)(this.rUserLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCreation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCompletion.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFromCompletion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCreation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCompletion.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dToCompletion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDateCreation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDateCompletion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gDates)).EndInit();
            this.gDates.ResumeLayout(false);
            this.gDates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gStaff)).EndInit();
            this.gStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cStaffAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gStaffConditions)).EndInit();
            this.gStaffConditions.ResumeLayout(false);
            this.gStaffConditions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private RadioGroup rUserLevel;
        private LabelControl lUserLevel;
        private LabelControl lChannels;
        private CheckedComboBoxEdit cStaffChannel;
        private TreeList tStaff;
        private DateEdit dFromCreation;
        private DateEdit dFromCompletion;
        private DateEdit dToCreation;
        private DateEdit dToCompletion;
        private CheckEdit cDateCreation;
        private CheckEdit cDateCompletion;
        private GroupControl gDates;
        private GroupControl gStaff;
        private GroupControl gStaffConditions;
        private SimpleButton bOk;
        private SimpleButton bCancel;
        private CheckEdit cStaffAll;
        private LabelControl lFrom2;
        private LabelControl lFrom1;
        private LabelControl lTo2;
        private LabelControl lTo1;
        private LabelControl lUse;
        private TreeListColumn colName;
        private TreeListColumn colStaffId;
        private TreeListColumn colLevel;
        private TreeListColumn colChannelMask;
    }
}