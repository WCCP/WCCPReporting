﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.Common;
using SoftServe.Reports.TasksReport.Properties;
using SoftServe.Reports.TasksReport.Utils;

namespace SoftServe.Reports.TasksReport.Controls
{
    public partial class ImagesControl : UserControl
    {
        public ImagesControl()
        {
            InitializeComponent();
        }

        public void LoadPhotos(List<string> photos, ImageSourceEnum source)
        {
            Clear();

            switch (source)
            {
                case ImageSourceEnum.RemoteFile: GetRemoteImages(photos);
                    break;
                case ImageSourceEnum.Service: GetServiceImage(photos);
                    break;
            }

        }

        private void GetServiceImage(List<string> photos)
        {
            int lYPosition = 15;

            for (int i = 0; i < photos.Count; i++)
            {
                try
                {
                    Stream lPhotoStream = GetFileContent(photos[i]);
                    Image lImage = Image.FromStream(lPhotoStream);

                    PictureEdit lPictureEdit = AddPictureEdit(i, lImage.Size, lYPosition);
                    lPictureEdit.Image = lImage;

                    lYPosition = 15 + lPictureEdit.Bottom;
                }
                catch (Exception ex)
                {
                    ErrorManager.ShowErrorBox(string.Format("Не получилось отобразить фотографию {0}: {1}", photos[i], ex.Message));
                }
            }
        }

        private static Stream GetFileContent(string photoName)
        {
            Stream result = null;

            string lUriString = string.Format(Settings.Default.ContentServiceRequest, photoName);
            HttpWebRequest lRequest = (HttpWebRequest)HttpWebRequest.Create(lUriString);

            int statusCode = 0;
            string message = "";
            WebResponse response = null;

            response = lRequest.GetResponse();

            if (response is HttpWebResponse)
            {
                var webResponse = (HttpWebResponse)response;
                statusCode = (int)webResponse.StatusCode;

                result = webResponse.GetResponseStream();
            }

            if (statusCode != 200)
            {
                throw new Exception("Ошибка в работе сервиса");
            }

            return result;
        }

        private void GetRemoteImages(List<string> photos)
        {
            int lYPosition = 15;

            for (int i = 0; i < photos.Count; i++)
            {
                try
                {

                    string filePath = Path.Combine(Settings.Default.PhotosDirectoryPath, photos[i]);
                    Image lImage = Image.FromFile(filePath);

                    PictureEdit lPictureEdit = AddPictureEdit(i, lImage.Size, lYPosition);
                    lPictureEdit.Image = lImage;

                    lYPosition = 15 + lPictureEdit.Bottom;
                }
                catch (Exception ex)
                {
                    ErrorManager.ShowErrorBox(string.Format("Не получилось отобразить фотографию {0}: {1}", photos[i], ex.Message));
                }
            }
        }

        private void Clear()
        {
            for (int i = Controls.Count - 1; i >= 0; i--)
            {
                if (Controls[i] is PictureEdit)
                {
                    Controls.RemoveAt(i);
                }
            }
        }

        private PictureEdit AddPictureEdit(int index, Size imageSize, int yPosition)
        {
            Size lEditorSize = DefineEditorSize(imageSize);

            this.SuspendLayout();
            PictureEdit pictureEdit = new PictureEdit();
            ((ISupportInitialize)(pictureEdit.Properties)).BeginInit();
            pictureEdit.Location = new Point(15, yPosition);
            pictureEdit.Name = "pictureEdit" + index;
            pictureEdit.Properties.Appearance.BackColor = Color.Transparent;
            pictureEdit.Properties.Appearance.Options.UseBackColor = true;
            pictureEdit.Properties.AllowFocused = false;
            pictureEdit.Properties.BorderStyle = BorderStyles.NoBorder;
            pictureEdit.Properties.PictureAlignment = ContentAlignment.TopLeft;
            pictureEdit.Properties.ShowMenu = true;
            pictureEdit.Properties.SizeMode = PictureSizeMode.Zoom;
            pictureEdit.Properties.ReadOnly = true;
            pictureEdit.Properties.ShowZoomSubMenu = DefaultBoolean.True;
            pictureEdit.Properties.ShowScrollBars = AutoScroll;
            pictureEdit.Size = new Size(lEditorSize.Width, lEditorSize.Height);

            Controls.Add(pictureEdit);
            ((ISupportInitialize)(pictureEdit.Properties)).EndInit();
            ResumeLayout(false);

            return pictureEdit;
        }

        //we display photo with fixed max size - 1026 for width and height; this method calulates the size required
        private Size DefineEditorSize(Size imageSize)
        {
            Size editorSize = new Size();

            if (imageSize.Width > imageSize.Height)
            {
                editorSize.Width = Math.Min(imageSize.Width, 1026);
                double lHeightCoeff = editorSize.Width * 1.0 / imageSize.Width;

                editorSize.Height = (int)Math.Round(imageSize.Height * lHeightCoeff);
            }
            else
            {
                editorSize.Height = Math.Min(imageSize.Height, 1026);
                double lWidthCoeff = editorSize.Height * 1.0 / imageSize.Height;

                editorSize.Width = (int)Math.Round(imageSize.Width * lWidthCoeff);
            }

            return editorSize;
        }
    }
}
