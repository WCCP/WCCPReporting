﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.TasksReport.Utils;

namespace SoftServe.Reports.TasksReport.Controls
{
    public partial class DataControl : UserControl
    {
        public EventHandler<FilteringArgs> FilterUpdateHandler = delegate { };
        public EventHandler<ImageArgs> ImageHandler = delegate { };

        public object DataSource
        {
            set
            {
                gridControlData.BeginUpdate();
                gridControlData.DataSource = value;
                gridControlData.EndUpdate();
            }
        }

        public bool IsFilterEmpty
        {
            get { return gridViewData.ActiveFilter.IsEmpty; }
        }

        public DataControl()
        {
            InitializeComponent();
        }

        internal void HideColumnsForDetailsTab()
        {
            List<GridColumn> lDataColumnsOnly = new List<GridColumn>(new[] { colM4Id, colM4, colM3Id, colM3, colM2Id, colM2, colM1Id, colOLCodeExternal, colNameFact, colTaskId, colTaskSource, colTaskInitiator, colTaskForOneDay });

            foreach (GridColumn gridColumn in lDataColumnsOnly)
            {
                gridColumn.Visible = false;
                gridColumn.OptionsColumn.ShowInCustomizationForm = false;
                gridColumn.OptionsColumn.ShowInExpressionEditor = false;
            }
        }

        internal void Export(ExportToType exportToType, string path)
        {
            switch (exportToType)
            {
                case ExportToType.Xlsx:
                    gridControlData.ExportToXlsx(path);
                    break;
                case ExportToType.Csv:
                    gridControlData.ExportToCsv(path);
                    break;
            }
        }

        internal void SaveLayout(string path)
        {
            gridControlData.MainView.SaveLayoutToXml(path);
        }

        internal void ClearFilter()
        {
            gridViewData.ActiveFilter.Clear();
        }

        internal void EditFilter()
        {
            gridViewData.ShowFilterEditor(gridViewData.Columns[0]);
        }

        /// <summary>
        /// Restores gridview layout for current user from file on disc
        /// </summary>
        internal void RestoreLayout(string path)
        {
            if (File.Exists(path))
            {
                gridControlData.MainView.RestoreLayoutFromXml(path);
            }
        }

        private void gridViewData_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;

            bool lDrawRed = gridViewData.GetRowCellValue(e.RowHandle, colColour) != DBNull.Value && gridViewData.GetRowCellValue(e.RowHandle, colColour) != null;

            if (lDrawRed)
            {
                e.Appearance.BackColor = Color.Red;
                e.Appearance.BackColor2 = Color.Red;
            }
        }

        private void gridViewData_BeforeLoadLayout(object sender, LayoutAllowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.PreviousVersion))
            {
                e.Allow = false;
                return;
            }

            e.Allow = e.PreviousVersion == gridViewData.OptionsLayout.LayoutVersion;
        }

        private void gridViewData_ColumnFilterChanged(object sender, EventArgs e)
        {
            FilterUpdateHandler(this, new FilteringArgs(IsFilterEmpty));
        }

        private void gridViewData_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column == colPhotoOther)
            {
                object lLink = gridViewData.GetListSourceRowCellValue(e.ListSourceRowIndex, colPhotoOtherReal);
                if (lLink != null && lLink != DBNull.Value)
                {
                    e.Value = "Просмотр фото";
                    return;
                }
            }

            if (e.Column == colM1Photo)
            {
                object lLink = gridViewData.GetListSourceRowCellValue(e.ListSourceRowIndex, colPhotoM1Real);
                if (lLink != null && lLink != DBNull.Value)
                {
                    e.Value = "Просмотр фото";
                }
            }
        }

        private void rHyperLinkEdit_Click(object sender, EventArgs e)
        {
            WaitManager.StartWait();

            List<string> lPhotos = new List<string>();

            if (gridViewData.FocusedColumn == colM1Photo)
            {
                object lLink = gridViewData.GetFocusedRowCellValue(colPhotoM1Real);
                string lNames = Convert.ToString(lLink);

                lPhotos.AddRange(lNames.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries));

                ImageHandler(this, new ImageArgs(lPhotos, ImageSourceEnum.Service));
            }

            if (gridViewData.FocusedColumn == colPhotoOther)
            {
                object lLink = gridViewData.GetFocusedRowCellValue(colPhotoOtherReal);
                string lNames = Convert.ToString(lLink);

                lPhotos.AddRange(lNames.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries));

                ImageHandler(this, new ImageArgs(lPhotos, ImageSourceEnum.RemoteFile));
            }

            //if (gridViewData.FocusedColumn == colPhotoOther)
            //{
            //    lPhotos.Add("gggg.jpg");
            //    lPhotos.Add("7285fqght6asu8arne5akk0jl354bf75d62334b.jpg");
            //    lPhotos.Add("2078305-R3L8T8D-650-10.png.jpg");
            //    lPhotos.Add("3846403_d54cc691.jpg");
            //    lPhotos.Add("hSlIrPr-640x426.jpg");
            //    lPhotos.Add("38a86289256aab5c798129dbfb6830e9.jpg");

            //    ImageHandler(this, new ImageArgs(lPhotos, ImageSourceEnum.RemoteFile));
            //}
            //else
            //{
            //    lPhotos.Add("Радуга-6741336f-6334-4f2c-a577-fbfe1016c897.jpg");

            //    ImageHandler(this, new ImageArgs(lPhotos, ImageSourceEnum.Service));
            //}

            WaitManager.StopWait();
        }

        private void gridViewData_ShowFilterPopupCheckedListBox(object sender, FilterPopupCheckedListBoxEventArgs e)
        {
            if (e.Column == colTaskForOneDay)
            {
                foreach (CheckedListBoxItem item in e.CheckedComboBox.Items)
                {
                    if (((FilterItem)item.Value).Text == "0")
                    {
                        item.Description = "Нет";
                    }
                    else
                    {
                        item.Description = "Да";
                    }
                }
            }           
        }

        private void gridViewData_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            gridViewData.OptionsBehavior.CopyToClipboardWithColumnHeaders = gridViewData.GetSelectedCells().Count() > 1;
        }
    }
}

