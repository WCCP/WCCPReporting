﻿using System.ComponentModel;
using DevExpress.XtraPivotGrid;

namespace SoftServe.Reports.TasksReport.Controls
{
    partial class ResultsByStaffControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotData = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.fM4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM1Status = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fTaskSource = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fData = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fCompleted = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fUnknown = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotData)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotData
            // 
            this.pivotData.Appearance.ColumnHeaderArea.Options.UseTextOptions = true;
            this.pivotData.Appearance.ColumnHeaderArea.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotData.Appearance.ColumnHeaderArea.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotData.Appearance.ColumnHeaderArea.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotData.Appearance.FieldValueTotal.Options.UseTextOptions = true;
            this.pivotData.Appearance.FieldValueTotal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotData.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fM4,
            this.fM3,
            this.fM2,
            this.fM1,
            this.fM1Status,
            this.fTaskSource,
            this.fData,
            this.fCompleted,
            this.fUnknown});
            this.pivotData.Location = new System.Drawing.Point(0, 0);
            this.pivotData.Name = "pivotData";
            this.pivotData.OptionsBehavior.CopyToClipboardWithFieldValues = true;
            this.pivotData.OptionsLayout.LayoutVersion = "1";
            this.pivotData.OptionsPrint.MergeColumnFieldValues = false;
            this.pivotData.OptionsPrint.MergeRowFieldValues = false;
            this.pivotData.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotData.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotData.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotData.OptionsView.ColumnTotalsLocation = DevExpress.XtraPivotGrid.PivotTotalsLocation.Near;
            this.pivotData.OptionsView.ShowColumnTotals = false;
            this.pivotData.OptionsView.ShowDataHeaders = false;
            this.pivotData.OptionsView.ShowGrandTotalsForSingleValues = true;
            this.pivotData.Size = new System.Drawing.Size(783, 504);
            this.pivotData.TabIndex = 0;
            this.pivotData.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotData_CustomSummary);
            this.pivotData.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.pivotData_BeforeLoadLayout);
            this.pivotData.FieldValueDisplayText += new DevExpress.XtraPivotGrid.PivotFieldDisplayTextEventHandler(this.pivotData_FieldValueDisplayText);
            this.pivotData.CellDoubleClick += new DevExpress.XtraPivotGrid.PivotCellEventHandler(this.pivotData_CellDoubleClick);
            // 
            // fM4
            // 
            this.fM4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM4.AreaIndex = 0;
            this.fM4.Caption = "M4";
            this.fM4.FieldName = "m4_Name";
            this.fM4.Name = "fM4";
            this.fM4.Width = 120;
            // 
            // fM3
            // 
            this.fM3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM3.AreaIndex = 1;
            this.fM3.Caption = "M3";
            this.fM3.FieldName = "m3_Name";
            this.fM3.Name = "fM3";
            this.fM3.Width = 120;
            // 
            // fM2
            // 
            this.fM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM2.AreaIndex = 2;
            this.fM2.Caption = "M2";
            this.fM2.FieldName = "m2_Name";
            this.fM2.Name = "fM2";
            this.fM2.Width = 120;
            // 
            // fM1
            // 
            this.fM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM1.AreaIndex = 3;
            this.fM1.Caption = "M1";
            this.fM1.FieldName = "m1_Name";
            this.fM1.Name = "fM1";
            this.fM1.Width = 120;
            // 
            // fM1Status
            // 
            this.fM1Status.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fM1Status.AreaIndex = 1;
            this.fM1Status.Caption = "Статус М1";
            this.fM1Status.FieldName = "Status_Name";
            this.fM1Status.Name = "fM1Status";
            // 
            // fTaskSource
            // 
            this.fTaskSource.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fTaskSource.AreaIndex = 0;
            this.fTaskSource.Caption = "Источник задачи";
            this.fTaskSource.FieldName = "Destination_Name";
            this.fTaskSource.Name = "fTaskSource";
            // 
            // fData
            // 
            this.fData.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
            this.fData.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fData.AreaIndex = 0;
            this.fData.Caption = "Задачи";
            this.fData.ExpandedInFieldsGroup = false;
            this.fData.FieldName = "value";
            this.fData.Name = "fData";
            this.fData.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.False;
            this.fData.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.fData.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.fData.Options.ShowGrandTotal = false;
            this.fData.Options.ShowTotals = false;
            // 
            // fCompleted
            // 
            this.fCompleted.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
            this.fCompleted.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fCompleted.AreaIndex = 1;
            this.fCompleted.Caption = "Выполненный";
            this.fCompleted.FieldName = "value";
            this.fCompleted.GrandTotalText = "Выполненный";
            this.fCompleted.Name = "fCompleted";
            this.fCompleted.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.False;
            this.fCompleted.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.fCompleted.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.fCompleted.Options.ShowTotals = false;
            this.fCompleted.Options.ShowValues = false;
            this.fCompleted.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.fCompleted.TotalCellFormat.FormatString = "p2";
            this.fCompleted.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fCompleted.TotalValueFormat.FormatString = "p2";
            this.fCompleted.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fCompleted.ValueFormat.FormatString = "p2";
            this.fCompleted.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fUnknown
            // 
            this.fUnknown.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
            this.fUnknown.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fUnknown.AreaIndex = 2;
            this.fUnknown.Caption = "Нет статуса";
            this.fUnknown.FieldName = "value";
            this.fUnknown.GrandTotalText = "Нет статуса";
            this.fUnknown.Name = "fUnknown";
            this.fUnknown.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.False;
            this.fUnknown.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.fUnknown.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.fUnknown.Options.ShowTotals = false;
            this.fUnknown.Options.ShowValues = false;
            this.fUnknown.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.fUnknown.TotalCellFormat.FormatString = "p2";
            this.fUnknown.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fUnknown.TotalValueFormat.FormatString = "p2";
            this.fUnknown.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fUnknown.ValueFormat.FormatString = "p2";
            this.fUnknown.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // ResultsByStaffControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pivotData);
            this.Name = "ResultsByStaffControl";
            this.Size = new System.Drawing.Size(783, 504);
            ((System.ComponentModel.ISupportInitialize)(this.pivotData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PivotGridControl pivotData;
        private PivotGridField fM4;
        private PivotGridField fM3;
        private PivotGridField fM2;
        private PivotGridField fM1;
        private PivotGridField fM1Status;
        private PivotGridField fTaskSource;
        private PivotGridField fData;
        private PivotGridField fCompleted;
        private PivotGridField fUnknown;
    }
}
