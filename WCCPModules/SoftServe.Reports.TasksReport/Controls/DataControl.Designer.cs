﻿using System.ComponentModel;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace SoftServe.Reports.TasksReport.Controls
{
    partial class DataControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlData = new DevExpress.XtraGrid.GridControl();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colM4Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOLCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOLCodeExternal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameJur = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdressFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTask = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompletion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM1Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBoseStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskInitiator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskForOneDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colM1Photo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colPhotoM1Real = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhotoOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhotoOtherReal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOLVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColour = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rHyperLinkEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlData
            // 
            this.gridControlData.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlData.Location = new System.Drawing.Point(0, 0);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rTextEdit,
            this.rCheckEdit,
            this.rHyperLinkEdit});
            this.gridControlData.Size = new System.Drawing.Size(1186, 423);
            this.gridControlData.TabIndex = 0;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // gridViewData
            // 
            this.gridViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewData.ColumnPanelRowHeight = 40;
            this.gridViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colM4Id,
            this.colM4,
            this.colM3Id,
            this.colM3,
            this.colM2Id,
            this.colM2,
            this.colM1Id,
            this.colM1,
            this.colOLCode,
            this.colOLCodeExternal,
            this.colNameJur,
            this.colNameFact,
            this.colAdressFact,
            this.colTaskId,
            this.colTask,
            this.colComment,
            this.colPlan,
            this.colFact,
            this.colDateCreation,
            this.colDateCompletion,
            this.colM1Status,
            this.colBoseStatus,
            this.colTaskSource,
            this.colTaskInitiator,
            this.colTaskForOneDay,
            this.colM1Photo,
            this.colPhotoM1Real,
            this.colPhotoOther,
            this.colPhotoOtherReal,
            this.colWasVisit,
            this.colOLVisit,
            this.colColour});
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsBehavior.ReadOnly = true;
            this.gridViewData.OptionsLayout.LayoutVersion = "1";
            this.gridViewData.OptionsPrint.AutoWidth = false;
            this.gridViewData.OptionsSelection.MultiSelect = true;
            this.gridViewData.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridViewData.OptionsView.ColumnAutoWidth = false;
            this.gridViewData.OptionsView.ShowAutoFilterRow = true;
            this.gridViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewData.OptionsView.ShowGroupPanel = false;
            this.gridViewData.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewData_RowStyle);
            this.gridViewData.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewData_SelectionChanged);
            this.gridViewData.ColumnFilterChanged += new System.EventHandler(this.gridViewData_ColumnFilterChanged);
            this.gridViewData.ShowFilterPopupCheckedListBox += new DevExpress.XtraGrid.Views.Grid.FilterPopupCheckedListBoxEventHandler(this.gridViewData_ShowFilterPopupCheckedListBox);
            this.gridViewData.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewData_CustomUnboundColumnData);
            this.gridViewData.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.gridViewData_BeforeLoadLayout);
            // 
            // colM4Id
            // 
            this.colM4Id.Caption = "М4 ID";
            this.colM4Id.FieldName = "m4_id";
            this.colM4Id.Name = "colM4Id";
            this.colM4Id.OptionsColumn.AllowEdit = false;
            this.colM4Id.OptionsColumn.ReadOnly = true;
            this.colM4Id.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM4Id.Visible = true;
            this.colM4Id.VisibleIndex = 0;
            // 
            // colM4
            // 
            this.colM4.Caption = "М4";
            this.colM4.FieldName = "m4_Name";
            this.colM4.Name = "colM4";
            this.colM4.OptionsColumn.AllowEdit = false;
            this.colM4.OptionsColumn.ReadOnly = true;
            this.colM4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM4.Visible = true;
            this.colM4.VisibleIndex = 1;
            this.colM4.Width = 160;
            // 
            // colM3Id
            // 
            this.colM3Id.Caption = "М3 ID";
            this.colM3Id.FieldName = "m3_id";
            this.colM3Id.Name = "colM3Id";
            this.colM3Id.OptionsColumn.AllowEdit = false;
            this.colM3Id.OptionsColumn.ReadOnly = true;
            this.colM3Id.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3Id.Visible = true;
            this.colM3Id.VisibleIndex = 2;
            // 
            // colM3
            // 
            this.colM3.Caption = "М3";
            this.colM3.FieldName = "m3_Name";
            this.colM3.Name = "colM3";
            this.colM3.OptionsColumn.AllowEdit = false;
            this.colM3.OptionsColumn.ReadOnly = true;
            this.colM3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3.Visible = true;
            this.colM3.VisibleIndex = 3;
            this.colM3.Width = 155;
            // 
            // colM2Id
            // 
            this.colM2Id.Caption = "М2 ID";
            this.colM2Id.FieldName = "m2_id";
            this.colM2Id.Name = "colM2Id";
            this.colM2Id.OptionsColumn.AllowEdit = false;
            this.colM2Id.OptionsColumn.ReadOnly = true;
            this.colM2Id.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2Id.Visible = true;
            this.colM2Id.VisibleIndex = 4;
            // 
            // colM2
            // 
            this.colM2.Caption = "М2";
            this.colM2.FieldName = "m2_Name";
            this.colM2.Name = "colM2";
            this.colM2.OptionsColumn.AllowEdit = false;
            this.colM2.OptionsColumn.ReadOnly = true;
            this.colM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2.Visible = true;
            this.colM2.VisibleIndex = 5;
            this.colM2.Width = 169;
            // 
            // colM1Id
            // 
            this.colM1Id.Caption = "М1 ID";
            this.colM1Id.FieldName = "m1_id";
            this.colM1Id.Name = "colM1Id";
            this.colM1Id.OptionsColumn.AllowEdit = false;
            this.colM1Id.OptionsColumn.ReadOnly = true;
            this.colM1Id.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Id.Visible = true;
            this.colM1Id.VisibleIndex = 6;
            // 
            // colM1
            // 
            this.colM1.Caption = "М1";
            this.colM1.FieldName = "m1_Name";
            this.colM1.Name = "colM1";
            this.colM1.OptionsColumn.AllowEdit = false;
            this.colM1.OptionsColumn.ReadOnly = true;
            this.colM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1.Visible = true;
            this.colM1.VisibleIndex = 7;
            this.colM1.Width = 192;
            // 
            // colOLCode
            // 
            this.colOLCode.Caption = "Код ТТ";
            this.colOLCode.ColumnEdit = this.rTextEdit;
            this.colOLCode.FieldName = "OL_id";
            this.colOLCode.Name = "colOLCode";
            this.colOLCode.OptionsColumn.AllowEdit = false;
            this.colOLCode.OptionsColumn.ReadOnly = true;
            this.colOLCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLCode.Visible = true;
            this.colOLCode.VisibleIndex = 8;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // colOLCodeExternal
            // 
            this.colOLCodeExternal.Caption = "Внешний код ТТ";
            this.colOLCodeExternal.ColumnEdit = this.rTextEdit;
            this.colOLCodeExternal.FieldName = "OL_Code";
            this.colOLCodeExternal.Name = "colOLCodeExternal";
            this.colOLCodeExternal.OptionsColumn.AllowEdit = false;
            this.colOLCodeExternal.OptionsColumn.ReadOnly = true;
            this.colOLCodeExternal.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLCodeExternal.Visible = true;
            this.colOLCodeExternal.VisibleIndex = 9;
            this.colOLCodeExternal.Width = 134;
            // 
            // colNameJur
            // 
            this.colNameJur.Caption = "Юридическое название";
            this.colNameJur.FieldName = "OLName";
            this.colNameJur.Name = "colNameJur";
            this.colNameJur.OptionsColumn.AllowEdit = false;
            this.colNameJur.OptionsColumn.ReadOnly = true;
            this.colNameJur.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colNameJur.Visible = true;
            this.colNameJur.VisibleIndex = 10;
            this.colNameJur.Width = 134;
            // 
            // colNameFact
            // 
            this.colNameFact.Caption = "Фактическое название";
            this.colNameFact.FieldName = "OLTradingName";
            this.colNameFact.Name = "colNameFact";
            this.colNameFact.OptionsColumn.AllowEdit = false;
            this.colNameFact.OptionsColumn.ReadOnly = true;
            this.colNameFact.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colNameFact.Visible = true;
            this.colNameFact.VisibleIndex = 11;
            this.colNameFact.Width = 140;
            // 
            // colAdressFact
            // 
            this.colAdressFact.Caption = "Фактический адрес";
            this.colAdressFact.FieldName = "OLAddress";
            this.colAdressFact.Name = "colAdressFact";
            this.colAdressFact.OptionsColumn.AllowEdit = false;
            this.colAdressFact.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colAdressFact.Visible = true;
            this.colAdressFact.VisibleIndex = 12;
            this.colAdressFact.Width = 224;
            // 
            // colTaskId
            // 
            this.colTaskId.Caption = "ID задания";
            this.colTaskId.ColumnEdit = this.rTextEdit;
            this.colTaskId.FieldName = "Task_id";
            this.colTaskId.Name = "colTaskId";
            this.colTaskId.OptionsColumn.AllowEdit = false;
            this.colTaskId.OptionsColumn.ReadOnly = true;
            this.colTaskId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTaskId.Visible = true;
            this.colTaskId.VisibleIndex = 13;
            this.colTaskId.Width = 194;
            // 
            // colTask
            // 
            this.colTask.Caption = "Задача";
            this.colTask.FieldName = "Task_Name";
            this.colTask.Name = "colTask";
            this.colTask.OptionsColumn.AllowEdit = false;
            this.colTask.OptionsColumn.ReadOnly = true;
            this.colTask.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTask.Visible = true;
            this.colTask.VisibleIndex = 14;
            this.colTask.Width = 181;
            // 
            // colComment
            // 
            this.colComment.Caption = "Комментарий";
            this.colComment.FieldName = "Task_Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            this.colComment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 15;
            this.colComment.Width = 262;
            // 
            // colPlan
            // 
            this.colPlan.Caption = "План";
            this.colPlan.FieldName = "PlannedAmount";
            this.colPlan.Name = "colPlan";
            this.colPlan.OptionsColumn.AllowEdit = false;
            this.colPlan.OptionsColumn.ReadOnly = true;
            this.colPlan.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPlan.Visible = true;
            this.colPlan.VisibleIndex = 16;
            // 
            // colFact
            // 
            this.colFact.Caption = "Факт";
            this.colFact.FieldName = "ActualAmount";
            this.colFact.Name = "colFact";
            this.colFact.OptionsColumn.AllowEdit = false;
            this.colFact.OptionsColumn.ReadOnly = true;
            this.colFact.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colFact.Visible = true;
            this.colFact.VisibleIndex = 17;
            // 
            // colDateCreation
            // 
            this.colDateCreation.Caption = "Дата создания задачи";
            this.colDateCreation.FieldName = "CreationDate";
            this.colDateCreation.Name = "colDateCreation";
            this.colDateCreation.OptionsColumn.AllowEdit = false;
            this.colDateCreation.OptionsColumn.ReadOnly = true;
            this.colDateCreation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDateCreation.Visible = true;
            this.colDateCreation.VisibleIndex = 18;
            this.colDateCreation.Width = 90;
            // 
            // colDateCompletion
            // 
            this.colDateCompletion.Caption = "Дата выполнения задачи";
            this.colDateCompletion.FieldName = "CompletedDate";
            this.colDateCompletion.Name = "colDateCompletion";
            this.colDateCompletion.OptionsColumn.AllowEdit = false;
            this.colDateCompletion.OptionsColumn.ReadOnly = true;
            this.colDateCompletion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDateCompletion.Visible = true;
            this.colDateCompletion.VisibleIndex = 19;
            this.colDateCompletion.Width = 103;
            // 
            // colM1Status
            // 
            this.colM1Status.Caption = "Статус М1";
            this.colM1Status.FieldName = "Status_Name";
            this.colM1Status.Name = "colM1Status";
            this.colM1Status.OptionsColumn.AllowEdit = false;
            this.colM1Status.OptionsColumn.ReadOnly = true;
            this.colM1Status.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Status.Visible = true;
            this.colM1Status.VisibleIndex = 20;
            // 
            // colBoseStatus
            // 
            this.colBoseStatus.Caption = "Статус руководителя/other";
            this.colBoseStatus.FieldName = "ManagerStatus";
            this.colBoseStatus.Name = "colBoseStatus";
            this.colBoseStatus.OptionsColumn.AllowEdit = false;
            this.colBoseStatus.OptionsColumn.ReadOnly = true;
            this.colBoseStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBoseStatus.Visible = true;
            this.colBoseStatus.VisibleIndex = 21;
            this.colBoseStatus.Width = 118;
            // 
            // colTaskSource
            // 
            this.colTaskSource.Caption = "Источник задачи";
            this.colTaskSource.FieldName = "Destination_Name";
            this.colTaskSource.Name = "colTaskSource";
            this.colTaskSource.OptionsColumn.AllowEdit = false;
            this.colTaskSource.OptionsColumn.ReadOnly = true;
            this.colTaskSource.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTaskSource.Visible = true;
            this.colTaskSource.VisibleIndex = 22;
            // 
            // colTaskInitiator
            // 
            this.colTaskInitiator.Caption = "Инициатор задачи";
            this.colTaskInitiator.FieldName = "TaskInitiator";
            this.colTaskInitiator.Name = "colTaskInitiator";
            this.colTaskInitiator.OptionsColumn.AllowEdit = false;
            this.colTaskInitiator.OptionsColumn.ReadOnly = true;
            this.colTaskInitiator.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTaskInitiator.Visible = true;
            this.colTaskInitiator.VisibleIndex = 23;
            // 
            // colTaskForOneDay
            // 
            this.colTaskForOneDay.Caption = "Задание на один день";
            this.colTaskForOneDay.ColumnEdit = this.rCheckEdit;
            this.colTaskForOneDay.FieldName = "CurrentDayTask";
            this.colTaskForOneDay.Name = "colTaskForOneDay";
            this.colTaskForOneDay.OptionsColumn.AllowEdit = false;
            this.colTaskForOneDay.OptionsColumn.ReadOnly = true;
            this.colTaskForOneDay.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTaskForOneDay.Visible = true;
            this.colTaskForOneDay.VisibleIndex = 24;
            // 
            // rCheckEdit
            // 
            this.rCheckEdit.AutoHeight = false;
            this.rCheckEdit.DisplayValueChecked = "1";
            this.rCheckEdit.DisplayValueUnchecked = "0";
            this.rCheckEdit.Name = "rCheckEdit";
            // 
            // colM1Photo
            // 
            this.colM1Photo.Caption = "Фотография М1";
            this.colM1Photo.ColumnEdit = this.rHyperLinkEdit;
            this.colM1Photo.FieldName = "M1_Photo_Custom";
            this.colM1Photo.Name = "colM1Photo";
            this.colM1Photo.OptionsColumn.ReadOnly = true;
            this.colM1Photo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Photo.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colM1Photo.Visible = true;
            this.colM1Photo.VisibleIndex = 25;
            // 
            // rHyperLinkEdit
            // 
            this.rHyperLinkEdit.AutoHeight = false;
            this.rHyperLinkEdit.Name = "rHyperLinkEdit";
            this.rHyperLinkEdit.SingleClick = true;
            this.rHyperLinkEdit.Click += new System.EventHandler(this.rHyperLinkEdit_Click);
            // 
            // colPhotoM1Real
            // 
            this.colPhotoM1Real.Caption = "colPhotoReal";
            this.colPhotoM1Real.FieldName = "M1_Photo";
            this.colPhotoM1Real.Name = "colPhotoM1Real";
            this.colPhotoM1Real.OptionsColumn.ShowInCustomizationForm = false;
            this.colPhotoM1Real.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colPhotoOther
            // 
            this.colPhotoOther.Caption = "Фотография М2/M3/Other";
            this.colPhotoOther.ColumnEdit = this.rHyperLinkEdit;
            this.colPhotoOther.FieldName = "M2_M3_Photo_Custom";
            this.colPhotoOther.Name = "colPhotoOther";
            this.colPhotoOther.OptionsColumn.ReadOnly = true;
            this.colPhotoOther.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPhotoOther.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colPhotoOther.Visible = true;
            this.colPhotoOther.VisibleIndex = 26;
            // 
            // colPhotoOtherReal
            // 
            this.colPhotoOtherReal.Caption = "colPhotoOtherReal";
            this.colPhotoOtherReal.FieldName = "M2_M3_Photo";
            this.colPhotoOtherReal.Name = "colPhotoOtherReal";
            this.colPhotoOtherReal.OptionsColumn.ShowInCustomizationForm = false;
            this.colPhotoOtherReal.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colWasVisit
            // 
            this.colWasVisit.Caption = "Был визит";
            this.colWasVisit.ColumnEdit = this.rCheckEdit;
            this.colWasVisit.FieldName = "Visit";
            this.colWasVisit.Name = "colWasVisit";
            this.colWasVisit.OptionsColumn.AllowEdit = false;
            this.colWasVisit.OptionsColumn.ReadOnly = true;
            this.colWasVisit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWasVisit.Visible = true;
            this.colWasVisit.VisibleIndex = 27;
            // 
            // colOLVisit
            // 
            this.colOLVisit.Caption = "Визит в ТТ";
            this.colOLVisit.ColumnEdit = this.rCheckEdit;
            this.colOLVisit.FieldName = "inTTVisit";
            this.colOLVisit.Name = "colOLVisit";
            this.colOLVisit.OptionsColumn.AllowEdit = false;
            this.colOLVisit.OptionsColumn.ReadOnly = true;
            this.colOLVisit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLVisit.Visible = true;
            this.colOLVisit.VisibleIndex = 28;
            // 
            // colColour
            // 
            this.colColour.Caption = "Color";
            this.colColour.FieldName = "Color";
            this.colColour.Name = "colColour";
            this.colColour.OptionsColumn.AllowEdit = false;
            this.colColour.OptionsColumn.ReadOnly = true;
            this.colColour.OptionsColumn.ShowInCustomizationForm = false;
            this.colColour.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // DataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlData);
            this.Name = "DataControl";
            this.Size = new System.Drawing.Size(1186, 423);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rHyperLinkEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridColumn colM4Id;
        private GridColumn colM4;
        private GridColumn colM3Id;
        private GridColumn colM3;
        private GridColumn colM2Id;
        private GridColumn colM2;
        private GridColumn colM1Id;
        private GridColumn colM1;
        private GridColumn colOLCode;
        private GridColumn colOLCodeExternal;
        private GridColumn colNameJur;
        private GridColumn colNameFact;
        private GridColumn colAdressFact;
        private GridColumn colTaskId;
        private GridColumn colTask;
        private GridColumn colComment;
        private GridColumn colPlan;
        private GridColumn colFact;
        private GridColumn colDateCreation;
        private GridColumn colDateCompletion;
        private GridColumn colM1Status;
        private GridColumn colBoseStatus;
        private GridColumn colTaskSource;
        private GridColumn colTaskInitiator;
        private GridColumn colTaskForOneDay;
        private GridColumn colM1Photo;
        private GridColumn colPhotoOther;
        private GridColumn colWasVisit;
        private GridColumn colOLVisit;
        private GridControl gridControlData;
        internal GridView gridViewData;
        private GridColumn colColour;
        private RepositoryItemTextEdit rTextEdit;
        private RepositoryItemCheckEdit rCheckEdit;
        private RepositoryItemHyperLinkEdit rHyperLinkEdit;
        private GridColumn colPhotoM1Real;
        private GridColumn colPhotoOtherReal;
    }
}
