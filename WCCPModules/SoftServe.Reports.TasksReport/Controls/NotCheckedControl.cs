﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraPivotGrid;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.TasksReport.Models;
using SoftServe.Reports.TasksReport.Utils;
using System.ComponentModel;
using System.Collections.Generic;

namespace SoftServe.Reports.TasksReport.Controls
{
    public partial class NotCheckedControl : UserControl
    {
        public EventHandler<FilterArgs> FilterDetails = delegate { };
        
        public DataTable DataSource
        {
            get { return pivotData.DataSource as DataTable; }
            set
            {
                pivotData.BeginUpdate();
                pivotData.DataSource = value;
                pivotData.EndUpdate();
            }
        }

        public bool AreTotalsVisible
        {
            get { return pivotData.OptionsView.ShowRowTotals; }
        }

        public NotCheckedControl()
        {
            InitializeComponent();
        }


        public void LoadSettings(Settings settings)
        {
            fM1.Visible = fM1.Options.ShowInCustomizationForm = settings.UserLevel <= (int)StaffLevels.M1;
            fM2.Visible = fM2.Options.ShowInCustomizationForm = settings.UserLevel <= (int)StaffLevels.M2;
            fM3.Visible = fM3.Options.ShowInCustomizationForm = settings.UserLevel <= (int)StaffLevels.M3;
            fM4.Visible = fM4.Options.ShowInCustomizationForm = settings.UserLevel <= (int)StaffLevels.M4;
        }

        internal void Export(ExportToType exportToType, string path)
        {
            switch (exportToType)
            {
                case ExportToType.Xlsx:
                    pivotData.ExportToXlsx(path);
                    break;
                case ExportToType.Csv:
                    pivotData.ExportToCsv(path);
                    break;
            }
        }

        internal void SaveLayout(string path)
        {
            pivotData.SaveLayoutToXml(path);
        }

        internal void ShowTotals(bool show)
        {
            pivotData.OptionsView.ShowColumnTotals = pivotData.OptionsView.ShowRowTotals = show;
            //fM4.Options.ShowTotals = fM3.Options.ShowTotals = fM2.Options.ShowTotals =
            //fM4.Options.ShowTotals = fM1.Options.ShowTotals = fTaskSource.Options.ShowTotals =
            //fM1Status.Options.ShowTotals = fDateCreation.Options.ShowTotals = fDateCompletion.Options.ShowTotals =
            //fBossStatus.Options.ShowTotals = show;
        }

        /// <summary>
        /// Restores gridview layout for current user from file on disc
        /// </summary>
        internal void RestoreLayout(string path)
        {
            if (File.Exists(path))
            {
                pivotData.RestoreLayoutFromXml(path);
            }
        }

        private void pivotData_BeforeLoadLayout(object sender, LayoutAllowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.PreviousVersion))
            {
                e.Allow = false;
                return;
            }

            e.Allow = e.PreviousVersion == pivotData.OptionsLayout.LayoutVersion;
        }

        private void pivotData_CellDoubleClick(object sender, PivotCellEventArgs e)
        {
            WaitManager.StartWait();
            FilterDetails(this, new FilterArgs(ConvertPivotDrillDownDataSourceToDataTable(e.CreateDrillDownDataSource())));
            WaitManager.StopWait();
        }

        private DataTable ConvertPivotDrillDownDataSourceToDataTable(PivotDrillDownDataSource source)
        {
            DataTable result = new DataTable();
            ITypedList dataProperties = source as ITypedList;
            if (dataProperties == null) return result;
            foreach (PropertyDescriptor prop in dataProperties.GetItemProperties(null))
                result.Columns.Add(prop.Name, prop.PropertyType);
            for (int row = 0; row < source.RowCount; row++)
            {
                List<object> values = new List<object>();
                foreach (DataColumn col in result.Columns)
                    values.Add(source.GetValue(row, col.ColumnName));
                result.Rows.Add(values.ToArray());
            }
            return result;
        }
    }
}
