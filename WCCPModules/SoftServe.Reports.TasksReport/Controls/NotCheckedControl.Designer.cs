﻿using System.ComponentModel;
using DevExpress.XtraPivotGrid;

namespace SoftServe.Reports.TasksReport.Controls
{
    partial class NotCheckedControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotData = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.fM4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fDateCreation = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fDateCompletion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fTaskSource = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fBossStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fM1Status = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fData = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotData)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotData
            // 
            this.pivotData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotData.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fM4,
            this.fM3,
            this.fM2,
            this.fM1,
            this.fDateCreation,
            this.fDateCompletion,
            this.fTaskSource,
            this.fBossStatus,
            this.fM1Status,
            this.fData});
            this.pivotData.Location = new System.Drawing.Point(0, 0);
            this.pivotData.Name = "pivotData";
            this.pivotData.OptionsBehavior.CopyToClipboardWithFieldValues = true;
            this.pivotData.OptionsLayout.LayoutVersion = "1";
            this.pivotData.OptionsPrint.MergeColumnFieldValues = false;
            this.pivotData.OptionsPrint.MergeRowFieldValues = false;
            this.pivotData.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotData.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotData.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotData.OptionsView.ShowColumnTotals = false;
            this.pivotData.OptionsView.ShowDataHeaders = false;
            this.pivotData.OptionsView.ShowGrandTotalsForSingleValues = true;
            this.pivotData.Size = new System.Drawing.Size(828, 383);
            this.pivotData.TabIndex = 0;
            this.pivotData.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.pivotData_BeforeLoadLayout);
            this.pivotData.CellDoubleClick += new DevExpress.XtraPivotGrid.PivotCellEventHandler(this.pivotData_CellDoubleClick);
            // 
            // fM4
            // 
            this.fM4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM4.AreaIndex = 0;
            this.fM4.Caption = "M4";
            this.fM4.FieldName = "m4_Name";
            this.fM4.Name = "fM4";
            this.fM4.Width = 120;
            // 
            // fM3
            // 
            this.fM3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM3.AreaIndex = 1;
            this.fM3.Caption = "M3";
            this.fM3.FieldName = "m3_Name";
            this.fM3.Name = "fM3";
            this.fM3.Width = 120;
            // 
            // fM2
            // 
            this.fM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM2.AreaIndex = 2;
            this.fM2.Caption = "M2";
            this.fM2.FieldName = "m2_Name";
            this.fM2.Name = "fM2";
            this.fM2.Width = 120;
            // 
            // fM1
            // 
            this.fM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fM1.AreaIndex = 3;
            this.fM1.Caption = "M1";
            this.fM1.FieldName = "m1_Name";
            this.fM1.Name = "fM1";
            this.fM1.Width = 120;
            // 
            // fDateCreation
            // 
            this.fDateCreation.AreaIndex = 0;
            this.fDateCreation.Caption = "Дата создания задачи";
            this.fDateCreation.FieldName = "CreationDate";
            this.fDateCreation.Name = "fDateCreation";
            this.fDateCreation.ValueFormat.FormatString = "d";
            this.fDateCreation.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fDateCompletion
            // 
            this.fDateCompletion.AreaIndex = 1;
            this.fDateCompletion.Caption = "Дата выполнения задачи";
            this.fDateCompletion.FieldName = "CompletedDate";
            this.fDateCompletion.Name = "fDateCompletion";
            this.fDateCompletion.ValueFormat.FormatString = "d";
            this.fDateCompletion.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fTaskSource
            // 
            this.fTaskSource.AreaIndex = 2;
            this.fTaskSource.Caption = "Источник задачи";
            this.fTaskSource.FieldName = "Destination_Name";
            this.fTaskSource.Name = "fTaskSource";
            // 
            // fBossStatus
            // 
            this.fBossStatus.AreaIndex = 3;
            this.fBossStatus.Caption = "Статус руководителя/other";
            this.fBossStatus.FieldName = "ManagerStatus";
            this.fBossStatus.Name = "fBossStatus";
            // 
            // fM1Status
            // 
            this.fM1Status.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fM1Status.AreaIndex = 0;
            this.fM1Status.Caption = "Статус М1";
            this.fM1Status.FieldName = "Status_Name";
            this.fM1Status.Name = "fM1Status";
            this.fM1Status.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            // 
            // fData
            // 
            this.fData.AllowedAreas = DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea;
            this.fData.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fData.AreaIndex = 0;
            this.fData.Caption = "Задачи";
            this.fData.FieldName = "value";
            this.fData.Name = "fData";
            // 
            // NotCheckedControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pivotData);
            this.Name = "NotCheckedControl";
            this.Size = new System.Drawing.Size(828, 383);
            ((System.ComponentModel.ISupportInitialize)(this.pivotData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PivotGridControl pivotData;
        private PivotGridField fM4;
        private PivotGridField fM3;
        private PivotGridField fM2;
        private PivotGridField fM1;
        private PivotGridField fDateCreation;
        private PivotGridField fDateCompletion;
        private PivotGridField fTaskSource;
        private PivotGridField fBossStatus;
        private PivotGridField fM1Status;
        private PivotGridField fData;
    }
}
