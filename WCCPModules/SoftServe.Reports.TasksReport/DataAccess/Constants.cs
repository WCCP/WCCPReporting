﻿namespace SoftServe.Reports.TasksReport.DataAccess
{
    public static class Constants
    {
        #region Settings Form

        public const string spGetChannelList = "MM.spDW_GetChannelList";
        
        public const string spGetStaffTree = "spDW_TaskPerform_GET_Personal";
        public const string spGetStaffTree_Login = "UserLogin";

        #endregion

        #region Main Report

        public const string spGetReportData = "spDW_TasksPerformance";
        public const string spGetReportData_CreationDateFrom = "StartDateFrom";
        public const string spGetReportData_CreationDateTo = "StartDateTo";
        public const string spGetReportData_CompletionDateFrom = "EndDateFrom";
        public const string spGetReportData_CompletionDateTo = "EndDateTo";
        public const string spGetReportData_StaffIdList = "StaffIdList";
        public const string spGetReportData_GreedNumber = "Greed";

        #endregion
    }
}
