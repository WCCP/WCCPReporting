﻿using System;
using System.Collections.Generic;
using System.Data;
using BLToolkit.Data;
using Logica.Reports.DataAccess;
using SoftServe.Reports.TasksReport.Models;

namespace SoftServe.Reports.TasksReport.DataAccess
{
    public static class DataProvider
    {
        private static DbManager _db;

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        #endregion

        #region Settings Form

        public static List<StaffItem> GetStaffTree()
        {
            return Db.SetSpCommand(Constants.spGetStaffTree,
                Db.Parameter(Constants.spGetStaffTree_Login, null)
                ).ExecuteList<StaffItem>();
        }

        public static List<Channel> GetChannels()
        {
            return Db.SetSpCommand(Constants.spGetChannelList).ExecuteList<Channel>();
        }

        #endregion

        #region Main Report

        public static DataTable GetDetails(Settings settings)
        {
            return Db.SetSpCommand(Constants.spGetReportData,
                Db.Parameter(Constants.spGetReportData_CreationDateFrom, settings.DateCreation.Use ? settings.DateCreation.From : default(DateTime?)),
                Db.Parameter(Constants.spGetReportData_CreationDateTo, settings.DateCreation.Use ? settings.DateCreation.To : default(DateTime?)),
                Db.Parameter(Constants.spGetReportData_CompletionDateFrom, settings.DateCompletion.Use ? settings.DateCompletion.From : default(DateTime?)),
                Db.Parameter(Constants.spGetReportData_CompletionDateTo, settings.DateCompletion.Use ? settings.DateCompletion.To : default(DateTime?)),
                Db.Parameter(Constants.spGetReportData_StaffIdList, settings.StaffList),
                Db.Parameter(Constants.spGetReportData_GreedNumber, 1)
                ).ExecuteDataTable();
        }

        #endregion
    }
}
