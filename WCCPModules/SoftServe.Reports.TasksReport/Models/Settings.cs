﻿using System.Collections.Generic;
using SoftServe.Reports.TasksReport.Utils;

namespace SoftServe.Reports.TasksReport.Models
{
    public class Settings
    {
        public DateUsage DateCreation { get; set; }
        public DateUsage DateCompletion { get; set; }

        public int UserLevel { get; set; }

        public List<int> StaffIds { get; set; }
        public string StaffList
        {
            get
            {
                if (StaffIds == null)
                    return string.Empty;

                return string.Join(",", StaffIds.ConvertAll(i => i.ToString()).ToArray());
            }
        }
    }
}
