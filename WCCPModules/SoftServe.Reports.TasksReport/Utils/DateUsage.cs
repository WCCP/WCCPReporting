﻿using System;

namespace SoftServe.Reports.TasksReport.Utils
{
    public struct DateUsage
    {
        public bool Use { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
