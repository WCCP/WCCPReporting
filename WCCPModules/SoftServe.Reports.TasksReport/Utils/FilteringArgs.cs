﻿using System;

namespace SoftServe.Reports.TasksReport.Utils
{
    public class FilteringArgs: EventArgs
    {
        public bool IsNotFiltered { get; set; }

        public FilteringArgs(bool value)
        {
            IsNotFiltered = value;
        }
    }
}
