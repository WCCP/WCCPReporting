﻿using System;
using System.Data;

namespace SoftServe.Reports.TasksReport.Utils
{
    public class FilterArgs : EventArgs
    {
        public DataTable Source { get; set; }

        public FilterArgs(DataTable source)
        {
            Source = source;
        }
    }
}
