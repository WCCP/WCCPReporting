﻿using System;
using System.Collections.Generic;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace SoftServe.Reports.TasksReport.Utils
{
    class CheckedNodesOperation : TreeListOperation
    {
        private string _colLevelName;
        private string _colMaskName;
        private string _colIdName;

        private int _level;
        private byte _mask;

        public List<int> Ids { get; set; }

        public CheckedNodesOperation(string colLevelName, int level, string colMaskName, byte mask, string colIdName)
        {
            _level = level;
            _mask = mask;

            _colLevelName = colLevelName;
            _colMaskName = colMaskName;
            _colIdName = colIdName;

            Ids = new List<int>();
        }

        public override void Execute(TreeListNode node)
        {
            if (NodeSatisfiesCondition(node))
            {
                Ids.Add(Convert.ToInt32(node[_colIdName]));
            }
        }

        bool NodeSatisfiesCondition(TreeListNode node)
        {
            return node.Visible && node.Checked && _level == Convert.ToInt32(node[_colLevelName]) && (Convert.ToUInt16(node[_colMaskName]) & _mask) > 0;
        }
    }
}
