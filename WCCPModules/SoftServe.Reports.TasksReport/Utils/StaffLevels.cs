﻿namespace SoftServe.Reports.TasksReport.Utils
{
    enum StaffLevels: int
    {
        M1 = 1,
        M2,
        M3,
        M4
    }
}
