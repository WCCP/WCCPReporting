﻿using System;
using System.Collections.Generic;

namespace SoftServe.Reports.TasksReport.Utils
{
    public class ImageArgs : EventArgs
    {
        public List<string> ImagesPathes { get; set; }

        public ImageSourceEnum Source { get; set; }

        public ImageArgs(List<string> pathes, ImageSourceEnum source)
        {
            ImagesPathes = pathes;
            Source = source;
        }
    }
}
