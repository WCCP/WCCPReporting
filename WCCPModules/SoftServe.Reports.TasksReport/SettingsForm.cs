﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList;
using SoftServe.Reports.TasksReport.Models;
using SoftServe.Reports.TasksReport.Utils;

namespace SoftServe.Reports.TasksReport
{
    public partial class SettingsForm : XtraForm
    {
        public Settings Settings { get; set; }

        public SettingsForm()
        {
            InitializeComponent();
        }

        private void GetSettings()
        {
            Settings = new Settings();

            Settings.DateCreation = new DateUsage
            {
                Use = cDateCreation.Checked,
                From = dFromCreation.DateTime,
                To = dToCreation.DateTime
            };

            Settings.DateCompletion = new DateUsage
            {
                Use = cDateCompletion.Checked,
                From = dFromCompletion.DateTime,
                To = dToCompletion.DateTime
            };

            Settings.UserLevel = StaffLevel;

            Settings.StaffIds = GetM1Checked();
        }

        public void LoadDataSources()
        {
            LoadChannels(DataRepository.Channels);
            LoadStaff(DataRepository.Staff);

            SetDefaults();
        }

        private void SetDefaults()
        {
            cStaffAll.Checked = true;

            //foreach (TreeListNode node in tStaff.Nodes)
            //{
            //    node.CheckState = CheckState.Checked;
            //}

            tStaff.CheckAll();

            foreach (CheckedListBoxItem channel in cStaffChannel.Properties.Items)
            {
                channel.CheckState = CheckState.Checked;
            }

            LoadDates();
        }

        private void LoadDates()
        {
            DateTime lMonthBeginning = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dFromCreation.DateTime = dFromCompletion.DateTime = lMonthBeginning;
            dToCreation.DateTime = dToCompletion.DateTime = DateTime.Now;
        }

        private void LoadStaff(List<StaffItem> list)
        {
            tStaff.DataSource = list;
            tStaff.ExpandAll();
            tStaff.CollapseAll();
        }

        private void LoadChannels(List<Channel> list)
        {
            foreach (Channel channel in list)
            {
                cStaffChannel.Properties.Items.Add(channel.Mask, channel.Name);
            }
        }

        private List<int> GetM1Checked()
        {
            int lCurStaffLevel = StaffLevel;

            if (lCurStaffLevel == 1)
            {
                CheckedNodesOperation lFirstLevelOperation = new CheckedNodesOperation(colLevel.FieldName, lCurStaffLevel, colChannelMask.FieldName, GetChannelMask(), colStaffId.FieldName);
                tStaff.NodesIterator.DoOperation(lFirstLevelOperation);

                return lFirstLevelOperation.Ids;
            }

            CheckedNodesOperation lCheckedOperation = new CheckedNodesOperation(colLevel.FieldName, lCurStaffLevel, colChannelMask.FieldName, GetChannelMask(), tStaff.KeyFieldName);
            tStaff.NodesIterator.DoOperation(lCheckedOperation);

            List<int> lCurrLevelIds = lCheckedOperation.Ids;

            while (lCurStaffLevel > 2)
            {
                lCurStaffLevel--;

                NodesByParentsOperation lInterimOperation = new NodesByParentsOperation(colLevel.FieldName, lCurStaffLevel, colChannelMask.FieldName, GetChannelMask(), tStaff.ParentFieldName, lCurrLevelIds, tStaff.KeyFieldName);
                tStaff.NodesIterator.DoOperation(lInterimOperation);

                lCurrLevelIds = lInterimOperation.Ids;
            }

            NodesByParentsOperation lM1Operation = new NodesByParentsOperation(colLevel.FieldName, 1, colChannelMask.FieldName, GetChannelMask(), tStaff.ParentFieldName, lCurrLevelIds, colStaffId.FieldName);
            tStaff.NodesIterator.DoOperation(lM1Operation);
            lCurrLevelIds = lM1Operation.Ids;

            return lCurrLevelIds;
        }

        private void GetStaffAllCheckState()
        {
            int lChecked = tStaff.Nodes.Count(n => n.CheckState == CheckState.Checked && n.Visible);
            int lUnhecked = tStaff.Nodes.Count(n => n.CheckState == CheckState.Unchecked && n.Visible);
            int lVisible = tStaff.Nodes.Count(n => n.Visible);

            cStaffAll.EditValueChanging -= cStaffAll_EditValueChanging;
            cStaffAll.CheckStateChanged -= cStaffAll_CheckStateChanged;

            if (lChecked == lVisible)
            {
                cStaffAll.CheckState = CheckState.Checked;
            }
            else if (lUnhecked == lVisible)
            {
                cStaffAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                cStaffAll.CheckState = CheckState.Indeterminate;
            }

            cStaffAll.EditValueChanging += cStaffAll_EditValueChanging;
            cStaffAll.CheckStateChanged += cStaffAll_CheckStateChanged;
        }

        private void FilterStaffTree()
        {
            int lStaffLevel = StaffLevel;
            byte lChannelMask = GetChannelMask();

            tStaff.BeginUpdate();

            FilterNodeOperation lOperation = new FilterNodeOperation(colLevel.FieldName, lStaffLevel, colChannelMask.FieldName, lChannelMask);
            tStaff.NodesIterator.DoOperation(lOperation);

            tStaff.EndUpdate();

            GetStaffAllCheckState();
        }

        private int StaffLevel
        {
            get { return Convert.ToInt32(rUserLevel.EditValue); }
        }

        private byte GetChannelMask()
        {
            return Convert.ToByte(cStaffChannel.Properties.Items.GetCheckedValues().Sum(v => Convert.ToByte(v)));
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            GetSettings();
            string lWarningMessage;

            if (AreSettingsCorrect(out lWarningMessage))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Некорректный набор параметров:" + Environment.NewLine + lWarningMessage, "Параметры", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool AreSettingsCorrect(out string lWarningMessage)
        {
            string lMessage = string.Empty;

            if (!Settings.DateCreation.Use && !Settings.DateCompletion.Use)
            {
                lMessage += "- Нужно выбрать хотя бы одну дату: Дата создания задачи или Дата выполнения задачи;" +
                            Environment.NewLine;
            }

            if (Settings.StaffIds.Count == 0)
            {
                lMessage += "- Нужно выбрать персонал;" +
                            Environment.NewLine;
            }

            lWarningMessage = lMessage;

            return string.IsNullOrEmpty(lMessage);
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void cStaffAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (cStaffAll.CheckState == CheckState.Checked)
            {
                tStaff.BeginUpdate();
                tStaff.CheckAll();
                tStaff.EndUpdate();
            }

            if (cStaffAll.CheckState == CheckState.Unchecked)
            {
                tStaff.BeginUpdate();
                tStaff.UncheckAll();
                tStaff.EndUpdate();
            }
        }

        private void cStaffAll_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.NewValue == null)
            {
                e.NewValue = !Convert.ToBoolean(e.OldValue);
            }
        }

        private void tStaff_AfterCheckNode(object sender, NodeEventArgs e)
        {
            GetStaffAllCheckState();
        }



        private void rUserLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterStaffTree();

            cStaffAll.CheckState = CheckState.Checked;
            tStaff.CollapseAll();
        }

        private void cStaffChannel_EditValueChanged(object sender, EventArgs e)
        {
            FilterStaffTree();

            cStaffAll.CheckState = CheckState.Checked;
            tStaff.CollapseAll();
        }

        private void cDateCreation_CheckedChanged(object sender, EventArgs e)
        {
            lFrom1.Enabled = lTo1.Enabled = dFromCreation.Enabled = dToCreation.Enabled = cDateCreation.Checked;
        }

        private void cDateCompletion_CheckedChanged(object sender, EventArgs e)
        {
            lFrom2.Enabled = lTo2.Enabled = dFromCompletion.Enabled = dToCompletion.Enabled = cDateCompletion.Checked;
        }

        private void datesCreation_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(dFromCreation))
            {
                dToCreation.Properties.MinValue = !string.IsNullOrEmpty(dFromCreation.Text) ? dFromCreation.DateTime : DateTime.MinValue;
            }

            if (sender.Equals(dToCreation))
            {
                dFromCreation.Properties.MaxValue = !string.IsNullOrEmpty(dToCreation.Text) ? dToCreation.DateTime : DateTime.MaxValue;
            }
        }

        private void datesCompletion_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(dFromCompletion))
            {
                dToCompletion.Properties.MinValue = !string.IsNullOrEmpty(dFromCompletion.Text) ? dFromCompletion.DateTime : DateTime.MinValue;
            }

            if (sender.Equals(dToCompletion))
            {
                dFromCompletion.Properties.MaxValue = !string.IsNullOrEmpty(dToCompletion.Text) ? dToCompletion.DateTime : DateTime.MaxValue;
            }
        }
    }
}
