﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Core.Common.View;
using SoftServe.Reports.TasksReport.DataAccess;
using SoftServe.Reports.TasksReport.Models;
using SoftServe.Reports.TasksReport.Utils;

namespace SoftServe.Reports.TasksReport
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : SimpleControlView
    {
        private SettingsForm _settingsForm;
        private Settings _currentSettings;

        private const string CompanyDirectory = "SoftServe";
        private const string ProgramDirectory = "WCCP Reporting";

        public WccpUIControl()
        {
            InitializeComponent();
            detailsControl.HideColumnsForDetailsTab();

            _settingsForm = new SettingsForm();
        }

        public int InitReport()
        {
            WaitManager.StartWait();
            _settingsForm.LoadDataSources();
            WaitManager.StopWait();

            resultsByStaffControl.FilterDetails += ShowDetails;
            notCheckedControl.FilterDetails += ShowDetails;
            dataControl.FilterUpdateHandler += UpdateClearFilterVisibility;
            detailsControl.FilterUpdateHandler += UpdateClearFilterVisibility;
            dataControl.ImageHandler += ShowPhotos;
            detailsControl.ImageHandler += ShowPhotos;

            RestoreLayout();
            UpdateShowTotalsState(resultsByStaffControl.AreTotalsVisible);

            return HandleSettingsForm() ? 0 : 1;
        }

       

        private void GenerateReport()
        {
            WaitManager.StartWait();

            DataTable lPlainData = DataProvider.GetDetails(_currentSettings);

            dataControl.DataSource = lPlainData;

            resultsByStaffControl.LoadSettings(_currentSettings);
            resultsByStaffControl.DataSource = GetStaffData(lPlainData);

            notCheckedControl.LoadSettings(_currentSettings);
            notCheckedControl.DataSource = GetNonCheckedInfo(lPlainData);
            WaitManager.StopWait();
        }

        private bool HandleSettingsForm()
        {
            if (_settingsForm.ShowDialog(this) == DialogResult.OK)
            {
                _currentSettings = _settingsForm.Settings;

                GenerateReport();
                return true;
            }

            return false;
        }

        #region Menu Handlers
        private void bSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            HandleSettingsForm();
        }

        private void bRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            GenerateReport();
        }

        private void bExportToXlsx_ItemClick(object sender, ItemClickEventArgs e)
        {
            string lPath = SelectFilePath(_tabControl.SelectedTabPage.Text, ExportToType.Xlsx);

            if (!string.IsNullOrEmpty(lPath))
            {
                try
                {
                    if (_tabControl.SelectedTabPage == tabData)
                    {
                        dataControl.Export(ExportToType.Xlsx, lPath);
                    }
                    if (_tabControl.SelectedTabPage == tabDetails)
                    {
                        detailsControl.Export(ExportToType.Xlsx, lPath);
                    }
                    if (_tabControl.SelectedTabPage == tabStaffExec)
                    {
                        resultsByStaffControl.Export(ExportToType.Xlsx, lPath);
                    }
                    if (_tabControl.SelectedTabPage == tabNonChecked)
                    {
                        notCheckedControl.Export(ExportToType.Xlsx, lPath);
                    }
                }
                catch (Exception)
                {
                    ErrorManager.ShowErrorBox("Ошибка при экспорте");
                }
            }
        }

        private void bExportToCsv_ItemClick(object sender, ItemClickEventArgs e)
        {
            string lPath = SelectFilePath(_tabControl.SelectedTabPage.Text, ExportToType.Csv);

            if (!string.IsNullOrEmpty(lPath))
            {
                try
                {

                    if (_tabControl.SelectedTabPage == tabData)
                    {
                        dataControl.Export(ExportToType.Csv, lPath);
                    }
                    if (_tabControl.SelectedTabPage == tabDetails)
                    {
                        detailsControl.Export(ExportToType.Csv, lPath);
                    }
                    if (_tabControl.SelectedTabPage == tabStaffExec)
                    {
                        resultsByStaffControl.Export(ExportToType.Csv, lPath);
                    }
                    if (_tabControl.SelectedTabPage == tabNonChecked)
                    {
                        notCheckedControl.Export(ExportToType.Csv, lPath);
                    }
                }
                catch (Exception)
                {
                    ErrorManager.ShowErrorBox("Ошибка при экспорте");
                }
            }
        }

        private string SelectFilePath(string reportCaption, ExportToType type)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = reportCaption;
            sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }
            return res;
        }

        private void bClearFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_tabControl.SelectedTabPage == tabDetails)
            {
                detailsControl.ClearFilter();
            }

            if (_tabControl.SelectedTabPage == tabData)
            {
                dataControl.ClearFilter();
            }
        }

        private void bEditFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_tabControl.SelectedTabPage == tabDetails)
            {
                detailsControl.EditFilter();
            }

            if (_tabControl.SelectedTabPage == tabData)
            {
                dataControl.EditFilter();
            }
        }

        private void cShowTotals_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_tabControl.SelectedTabPage == tabStaffExec)
            {
                resultsByStaffControl.ShowTotals(cShowTotals.Checked);
            }

            if (_tabControl.SelectedTabPage == tabNonChecked)
            {
                notCheckedControl.ShowTotals(cShowTotals.Checked);
            }
        }

        #endregion

        #region Form tabs datasources
        /// <summary>
        /// Условия отбора задач в эту вкладку: В данной таблице отображаются задачи с источником = 7 - М2/M3/Other и Статус руководителя(Confirmed) = 0 
        /// </summary>
        private DataTable GetNonCheckedInfo(DataTable plainData)
        {
            DataView lView = plainData.Copy().DefaultView;
            lView.RowFilter = "Destination = 7 and Confirmed = 0";

            return lView.ToTable();
        }

        /// <summary>
        /// В данной таблице не отображаются задачи с источником = 6 - Дневное планирование (ММ).
        /// </summary>
        private DataTable GetStaffData(DataTable plainData)
        {
            DataView lView = plainData.Copy().DefaultView;
            lView.RowFilter = "Destination in (1, 4, 5, 7)";

            return lView.ToTable();
        }

        private void ShowDetails(object source, FilterArgs args)
        {
            detailsControl.DataSource = args.Source;
            _tabControl.SelectedTabPage = tabDetails;
        }

        #endregion

        #region Save/restore layout

        private string GetLayoutDirectory()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);

            if (!Directory.Exists(lCompanyDirPath))
            {
                Directory.CreateDirectory(lCompanyDirPath);
            }

            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);

            if (!Directory.Exists(lProgramDirPath))
            {
                Directory.CreateDirectory(lProgramDirPath);
            }

            return lProgramDirPath;
        }

        internal void SaveLayout()
        {
            try
            {
                string lDirPath = GetLayoutDirectory();

                dataControl.SaveLayout(Path.Combine(lDirPath, "TaskReportDataTab"));
                detailsControl.SaveLayout(Path.Combine(lDirPath, "TaskReportDetailsTab"));
                resultsByStaffControl.SaveLayout(Path.Combine(lDirPath, "TaskReportStaffTab"));
                notCheckedControl.SaveLayout(Path.Combine(lDirPath, "TaskReportNotCheckedTab"));
            }
            catch (Exception) { }
        }

        private void RestoreLayout()
        {
            try
            {
                string lDirPath = GetLayoutDirectory();

                dataControl.RestoreLayout(Path.Combine(lDirPath, "TaskReportDataTab"));
                detailsControl.RestoreLayout(Path.Combine(lDirPath, "TaskReportDetailsTab"));
                resultsByStaffControl.RestoreLayout(Path.Combine(lDirPath, "TaskReportStaffTab"));
                notCheckedControl.RestoreLayout(Path.Combine(lDirPath, "TaskReportNotCheckedTab"));
            }
            catch (Exception) { }
        }

        #endregion

        #region Page selection

        private void _tabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            bClearFilter.Visibility = e.Page == tabData && !dataControl.IsFilterEmpty || e.Page == tabDetails && !detailsControl.IsFilterEmpty ? BarItemVisibility.Always : BarItemVisibility.Never;
            bEditFilter.Visibility = e.Page == tabData || e.Page == tabDetails ? BarItemVisibility.Always : BarItemVisibility.Never;

            cShowTotals.Visibility = e.Page == tabNonChecked || e.Page == tabStaffExec ? BarItemVisibility.Always : BarItemVisibility.Never;

            if (_tabControl.SelectedTabPage == tabStaffExec)
            {
                UpdateShowTotalsState(resultsByStaffControl.AreTotalsVisible);
            }

            if (_tabControl.SelectedTabPage == tabNonChecked)
            {
                UpdateShowTotalsState(notCheckedControl.AreTotalsVisible);
            }
        }

        private void UpdateShowTotalsState(bool areTotalsVisible)
        {
            if (_tabControl.SelectedTabPage == tabData || _tabControl.SelectedTabPage == tabDetails)
                return;

            cShowTotals.CheckedChanged -= cShowTotals_CheckedChanged;
            cShowTotals.Checked = areTotalsVisible;
            cShowTotals.CheckedChanged += cShowTotals_CheckedChanged;
        }

        private void UpdateClearFilterVisibility(object source, FilteringArgs args)
        {
            if (_tabControl.SelectedTabPage == tabNonChecked || _tabControl.SelectedTabPage == tabStaffExec)
                return;

            bClearFilter.Visibility = args.IsNotFiltered ? BarItemVisibility.Never : BarItemVisibility.Always;
        }

        private void ShowPhotos(object sender, ImageArgs e)
        {
            imagesControl.LoadPhotos(e.ImagesPathes, e.Source);
            _tabControl.SelectedTabPage = tabImages;
        }

        #endregion
    }
}
