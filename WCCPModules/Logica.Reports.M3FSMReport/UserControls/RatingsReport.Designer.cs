namespace Logica.Reports.M3FSMReport.UserControls
{
    partial class RatingsReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotGridRatingByProd = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridRatingByM1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridRatingByProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridRatingByM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pivotGridRatingByProd
            // 
            this.pivotGridRatingByProd.Appearance.FieldHeader.Options.UseTextOptions = true;
            this.pivotGridRatingByProd.Appearance.FieldHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByProd.Appearance.FieldValue.Options.UseTextOptions = true;
            this.pivotGridRatingByProd.Appearance.FieldValue.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByProd.Appearance.FieldValueTotal.Options.UseTextOptions = true;
            this.pivotGridRatingByProd.Appearance.FieldValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByProd.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridRatingByProd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridRatingByProd.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotField1,
            this.pivotField2,
            this.pivotField3,
            this.pivotField4});
            this.pivotGridRatingByProd.Location = new System.Drawing.Point(2, 2);
            this.pivotGridRatingByProd.Name = "pivotGridRatingByProd";
            this.pivotGridRatingByProd.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.ColumnArea;
            this.pivotGridRatingByProd.OptionsDataField.AreaIndex = 1;
            this.pivotGridRatingByProd.OptionsDataField.RowHeaderWidth = 170;
            this.pivotGridRatingByProd.Size = new System.Drawing.Size(975, 252);
            this.pivotGridRatingByProd.TabIndex = 2;
            // 
            // pivotField1
            // 
            this.pivotField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotField1.AreaIndex = 0;
            this.pivotField1.Caption = "���� �";
            this.pivotField1.EmptyCellText = "0";
            this.pivotField1.FieldName = "DayNumber";
            this.pivotField1.Name = "pivotField1";
            this.pivotField1.Options.AllowEdit = false;
            this.pivotField1.Options.ReadOnly = true;
            // 
            // pivotField2
            // 
            this.pivotField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotField2.AreaIndex = 0;
            this.pivotField2.Caption = "�������";
            this.pivotField2.EmptyCellText = "0";
            this.pivotField2.FieldName = "ProductCnShortName";
            this.pivotField2.Name = "pivotField2";
            this.pivotField2.Options.AllowEdit = false;
            this.pivotField2.Options.ReadOnly = true;
            // 
            // pivotField3
            // 
            this.pivotField3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotField3.AreaIndex = 0;
            this.pivotField3.Caption = "������� �� ����";
            this.pivotField3.CellFormat.FormatString = "{0:N2}";
            this.pivotField3.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotField3.EmptyCellText = "0";
            this.pivotField3.FieldName = "Rating";
            this.pivotField3.Name = "pivotField3";
            this.pivotField3.Options.AllowEdit = false;
            this.pivotField3.Options.ReadOnly = true;
            // 
            // pivotField4
            // 
            this.pivotField4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotField4.AreaIndex = 1;
            this.pivotField4.Caption = "������������";
            this.pivotField4.CellFormat.FormatString = "{0:N2}";
            this.pivotField4.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotField4.EmptyCellText = "0";
            this.pivotField4.FieldName = "ComRaiting";
            this.pivotField4.Name = "pivotField4";
            this.pivotField4.Options.AllowEdit = false;
            this.pivotField4.Options.ReadOnly = true;
            // 
            // pivotGridRatingByM1
            // 
            this.pivotGridRatingByM1.Appearance.FieldHeader.Options.UseTextOptions = true;
            this.pivotGridRatingByM1.Appearance.FieldHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByM1.Appearance.FieldValue.Options.UseTextOptions = true;
            this.pivotGridRatingByM1.Appearance.FieldValue.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByM1.Appearance.FieldValueTotal.Options.UseTextOptions = true;
            this.pivotGridRatingByM1.Appearance.FieldValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridRatingByM1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridRatingByM1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridRatingByM1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField1,
            this.pivotGridField2,
            this.pivotGridField3,
            this.pivotGridField4});
            this.pivotGridRatingByM1.Location = new System.Drawing.Point(2, 2);
            this.pivotGridRatingByM1.Name = "pivotGridRatingByM1";
            this.pivotGridRatingByM1.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pivotGridRatingByM1.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.ColumnArea;
            this.pivotGridRatingByM1.OptionsDataField.AreaIndex = 1;
            this.pivotGridRatingByM1.OptionsDataField.RowHeaderWidth = 170;
            this.pivotGridRatingByM1.OptionsView.ShowColumnGrandTotals = false;
            this.pivotGridRatingByM1.OptionsView.ShowColumnTotals = false;
            this.pivotGridRatingByM1.OptionsView.ShowRowGrandTotals = false;
            this.pivotGridRatingByM1.OptionsView.ShowRowTotals = false;
            this.pivotGridRatingByM1.Size = new System.Drawing.Size(975, 241);
            this.pivotGridRatingByM1.TabIndex = 3;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "���� �";
            this.pivotGridField1.EmptyCellText = "0";
            this.pivotGridField1.FieldName = "DayNumber";
            this.pivotGridField1.Name = "pivotGridField1";
            this.pivotGridField1.Options.AllowEdit = false;
            this.pivotGridField1.Options.ReadOnly = true;
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField2.AreaIndex = 0;
            this.pivotGridField2.Caption = "�1";
            this.pivotGridField2.EmptyCellText = "0";
            this.pivotGridField2.FieldName = "supervisor_name";
            this.pivotGridField2.Name = "pivotGridField2";
            this.pivotGridField2.Options.AllowEdit = false;
            this.pivotGridField2.Options.ReadOnly = true;
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField3.AreaIndex = 0;
            this.pivotGridField3.Caption = "������� �� ����";
            this.pivotGridField3.CellFormat.FormatString = "{0:N2}";
            this.pivotGridField3.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField3.EmptyCellText = "0";
            this.pivotGridField3.FieldName = "rating";
            this.pivotGridField3.Name = "pivotGridField3";
            this.pivotGridField3.Options.AllowEdit = false;
            this.pivotGridField3.Options.ReadOnly = true;
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField4.AreaIndex = 1;
            this.pivotGridField4.Caption = "������������";
            this.pivotGridField4.CellFormat.FormatString = "{0:N2}";
            this.pivotGridField4.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField4.EmptyCellText = "0";
            this.pivotGridField4.FieldName = "comraiting";
            this.pivotGridField4.Name = "pivotGridField4";
            this.pivotGridField4.Options.AllowEdit = false;
            this.pivotGridField4.Options.ReadOnly = true;
            this.pivotGridField4.Width = 142;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pivotGridRatingByProd);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(979, 256);
            this.panelControl1.TabIndex = 4;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pivotGridRatingByM1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 256);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(979, 245);
            this.panelControl2.TabIndex = 5;
            // 
            // RatingsReport
            // 
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "RatingsReport";
            this.Size = new System.Drawing.Size(979, 501);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridRatingByProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridRatingByM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridRatingByProd;
        private DevExpress.XtraPivotGrid.PivotGridField pivotField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotField4;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridRatingByM1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;





    }
}
