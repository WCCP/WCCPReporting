using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using DevExpress.XtraPivotGrid;
using System.Linq;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using System.Globalization;
using Logica.Reports.M3FSMReport;
using Logica.Reports.M3FSMReport.DataProvider;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraGrid.Views.Base;

namespace Logica.Reports.M3FSMReport.UserControls
{
    public partial class RatingsReport : UserControl
    {
        M3FSMSettingsForm setForm = null;
     
        public RatingsReport()
        {
            InitializeComponent();   
        }

        public void UpdateReport(M3FSMSettingsForm setForm)
        {
            this.setForm = setForm;
            try
            {
                pivotGridRatingByProd.DataSource = DataAccessProvider.GetRating(true, setForm.DsmId, setForm.ReportDate.Date);
                pivotGridRatingByProd.Height = (DataAccessProvider.GetRating(true, setForm.DsmId, setForm.ReportDate.Date)).Rows.Count * 20 + 150;
                panelControl1.Height = pivotGridRatingByProd.Height + 10;
                pivotGridRatingByM1.DataSource = DataAccessProvider.GetRating(false, setForm.DsmId, setForm.ReportDate.Date);
                pivotGridRatingByM1.Height = (DataAccessProvider.GetRating(false, setForm.DsmId, setForm.ReportDate.Date)).Rows.Count * 20 + 150;
                panelControl2.Height = pivotGridRatingByM1.Height + 10;
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }
    }
}