﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.M3FSMReport;
using Logica.Reports.M3FSMReport.UserControls;
using DevExpress.XtraTab;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using Logica.Reports.Common;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataAccess;
using Logica.Reports.ConfigXmlParser.Model;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using Logica.Reports.BaseReportControl.Utility;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        private static bool use_Ratings = false;
        private RatingsReport ratingsReport = new RatingsReport();
        private XtraTabPage ratingsReportPage = null;
        public static WccpUIControl ReportControl = null;
        public static List<M3FSMSettingsForm> Forms = null;
        private M3FSMSettingsForm setForm = null;
        private GridControl headerGrid = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            if (getForm() != null)
            {
                setForm = getForm();
                setForm.setParent(this.Report);
                this.off_reports = true;
                use_Ratings = setForm.AddRatings();
                if (use_Ratings)
                {
                    ratingsReportPage = new XtraTabPage();
                    ratingsReportPage.Controls.Add(ratingsReport as Control);
                    ratingsReportPage.Tag = ratingsReport;
                    ratingsReportPage.Text = Logica.Reports.Resource.Ratings;
                    (ratingsReportPage as Control).Dock = DockStyle.Fill;
                    (ratingsReport as Control).Dock = DockStyle.Fill;
                }
                UpdateTabs();
                foreach (XtraTabPage page in tabManager.TabPages)
                {
                    BaseTab tab = page as BaseTab;
                    if (null != tab)
                    {
                        tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
                    }
                    if (page.Equals(ratingsReportPage))
                        getRateHeader(ratingsReportPage);
                }
                customSettings();
            }
            //this.ExportClick += new ExportMenuClickHandler(WccpUIControl_ExportClick);
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
            this.MenuCloseClick += WccpUIControl_CloseClick;
            this.PrepareExport += new PrepareExportMenuClickHandler(WccpUIControl_PrepareExport);
            ReportControl = this; 
        }

        CompositeLink WccpUIControl_PrepareExport(object sender, XtraTabPage selectedPage, ExportToType type)
        {
            CompositeLink cl = null;
            if (selectedPage == ratingsReportPage)
            {
                cl = new CompositeLink();
                List<IPrintable> lst = new List<IPrintable>();
                lst.Add(headerGrid as IPrintable);
                foreach (Control ratings_gp in selectedPage.Controls)
                {
                    foreach (Control panel in ratings_gp.Controls)
                    {
                        foreach (Control c in panel.Controls)
                        {
                            lst.Add(c as IPrintable);
                        }
                    }
                }
                cl = Logica.Reports.BaseReportControl.Utility.TabOperationHelper.PrepareCompositeLink(type, "", lst, true); //(type, "", lst, IsNeedFitTabToOnePage);
            }
            return cl;
        }

        public static void AddForm(M3FSMSettingsForm form)
        {
            if (Forms == null) 
                Forms = new List<M3FSMSettingsForm>();
            Forms.Add(form);
        }

        private void UpdateTabs()
        {
            UpdateSheets(setForm.SheetParamsList);
            foreach (XtraTabPage page in tabManager.TabPages)
            {
                BaseTab tab = page as BaseTab;
                if (null != tab)
                {
                    tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
                }
            }
            if (use_Ratings)
            {
                if (!this.TabControl.TabPages.Contains(ratingsReportPage))
                {
                    this.TabControl.TabPages.Add(ratingsReportPage);
                }
                ratingsReport.UpdateReport(setForm);
            }
        }

        public bool IsNeedFitTabToOnePage()
        {
            return true;
        }

        /// <summary>
        /// WCCPs the UI control_ settings form click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                use_Ratings = setForm.AddRatings();

                if (use_Ratings)
                {
                    if (!this.TabControl.TabPages.Contains(ratingsReportPage))
                    {
                        ratingsReportPage = new XtraTabPage();
                        ratingsReportPage.Controls.Add(ratingsReport as Control);
                        ratingsReportPage.Tag = ratingsReport;
                    }
                    ratingsReportPage.Text = Logica.Reports.Resource.Ratings;
                    (ratingsReportPage as Control).Dock = DockStyle.Fill;
                    (ratingsReport as Control).Dock = DockStyle.Fill;
                }
                else if (this.TabControl.TabPages.Contains(ratingsReportPage))
                    this.TabControl.TabPages.Remove(ratingsReportPage);
                UpdateTabs();
                SortPages();
            }
        }

        public bool HasUnsavedData()
        {
            foreach (XtraTabPage tab in this.tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool SaveUnsavedData()
        {
            bool result = true;

            foreach (XtraTabPage tab in this.tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        if (!page.SaveChanges())
                        {
                            result = false;
                        }
                    }
                }
            }

            return result;
        }

        private bool WccpUIControl_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage is BaseTab)
            {
                BaseTab page = selectedPage as BaseTab; 
                
                if (page.DataModified)
                {
                    DialogResult result = MessageBox.Show(Logica.Reports.Resource.SaveData, Logica.Reports.Resource.DataNotSaved, MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.OK)
                    {
                        if (page.SaveChanges())
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show(Logica.Reports.Resource.DataSaved);
                        }
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private M3FSMSettingsForm getForm()
        {
            if (Forms != null && Forms.Count > 0)
                return Forms[Forms.Count - 1];
            else
                return null;
        }

        protected override void OnControlRemoved(ControlEventArgs e)
        {
            Forms.Remove(setForm);
            base.OnControlRemoved(e);
        }

        void customSettings()
        {
            cmbTab.Visible = true;
            lblTab.Visible = true;
            cmbTab.SelectedIndexChanged += new EventHandler(SelectedIndexChanged);
            FillComboBox();
        }

        /// <summary>
        /// Fill combobox with data for M1M2 report
        /// </summary>
        private void FillComboBox()
        {
            cmbTab.Properties.Items.Clear();
            List<XtraTabPage> tmpLst = new List<XtraTabPage>(tabManager.TabPages);
            foreach (XtraTabPage page in tmpLst)
            {
                cmbTab.Properties.Items.Add(new ComboBoxItem() { Id = page, Text = page.Text.ToString() });
            }
            //set first Tab name as combobox Text.
            if (cmbTab.Properties.Items.Count > 0)
                cmbTab.Text = ((ComboBoxItem)cmbTab.Properties.Items[0]).Text;
        }

        /// <summary>
        /// Occurs when index is changed
        /// </summary>
        /// <param name="sender">cmbTab</param>
        /// <param name="e">Event arguments</param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            base.tabManager.SelectedTabPage = ((XtraTabPage)((ComboBoxItem)cmbTab.SelectedItem).Id);
        }

        private void SortPages()
        {
            XtraTabPage pg = new XtraTabPage();
            List<XtraTabPage> lst_ = new List<XtraTabPage>();
            foreach (XtraTabPage page in tabManager.TabPages)
              lst_.Add(page);
            for (int i = 0; i < lst_.Count; i++)
            {
                pg = lst_[i];
                for (int j = i; j < lst_.Count; j++)
                {
                    if (pg.Text.CompareTo(lst_[j].Text) >= 0)
                        pg = lst_[j];                   
                }
                lst_.Remove(pg);
                lst_.Insert(i, pg);
            }
            lst_.ForEach(pg_ => { tabManager.TabPages.Move(150 + lst_.IndexOf(pg_), pg_); });
        }

        private Control getRateHeader(XtraTabPage pg)
        {
            GridView headerGridView = null;
            SheetParamCollection SheetSettings = new SheetParamCollection();
            SheetSettings.MainHeader = Logica.Reports.Resource.MainHeader;
            SheetSettings.Add(new SheetParam() { DisplayParamName = Logica.Reports.Resource.M3, DisplayValue = setForm.DsmName });
            SheetSettings.Add(new SheetParam() { DisplayParamName = Logica.Reports.Resource.ReportDay, DisplayValue = setForm.CurrentDate.ToShortDateString() });
            SheetSettings.Add(new SheetParam() { DisplayParamName = Logica.Reports.Resource.ReportMounth, DisplayValue = setForm.CurrentDate.ToString("MM yyyy") });
            SheetSettings.Add(new SheetParam() { DisplayParamName = Logica.Reports.Resource.WorkDays, DisplayValue = setForm.WorkDays });
            SheetSettings.Add(new SheetParam() { DisplayParamName = Logica.Reports.Resource.WorkDay, DisplayValue = setForm.WorkDay });
            SheetSettings.Add(new SheetParam() { DisplayParamName = Logica.Reports.Resource.ReportDate, DisplayValue = setForm.ReportDate.ToShortDateString()});

            TabOperationHelper.CreateSettingSection(pg, SheetSettings, ref headerGrid, ref headerGridView);
            return headerGrid;
        }
    }
}
