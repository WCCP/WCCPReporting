﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataAccess;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.ConfigXmlParser.Model;
using System.Data.SqlClient;
using Logica.Reports.Common;
using System.Globalization;
using System.Collections;

namespace Logica.Reports.M3FSMReport
{
    /// <summary>
    /// 
    /// </summary>
    public partial class M3FSMSettingsForm : XtraForm
    {
        public Int32 DsmId { get; set; }
        public String DsmName { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime CurrentDate { get; set; }
        public Object WorkDay { get; set; }
        public Object WorkDays { get; set; }
        private IDictionary<string,string> dict = new Dictionary<string, string>();

        private Report Report { get; set; }
        private int _reportId { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="M3FSMSettingsForm"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public M3FSMSettingsForm(Report report, int reportId)
        {
            InitializeComponent();

            _reportId = reportId;
            Report = report;
            //Bruc.Load += new EventHandler(Bruc_Load);
        }

        public M3FSMSettingsForm(int reportId)
        {
            InitializeComponent();

            _reportId = reportId;
        }

        public void setParent(Report parent)
        {
            Report = parent;
        }

        /// <summary>
        /// Handles the Load event of the M3FSMSettingsForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void M3FSMSettingsForm_Load(object sender, EventArgs e)
        {
            UpdateControls();
        }

        /// <summary>
        /// Updates the controls.
        /// </summary>
        public void UpdateControls()
        {
            if(string.IsNullOrEmpty(dateEdit.Text))
            {
                dateEdit.DateTime = DateTime.Now;
            }
            FillM3();
        }

        /// <summary>
        /// Fills the m3.
        /// </summary>
        private void FillM3()
        {
            DataTable dataTableM3Users = null;
            try
            {
                DataAccessLayer.OpenConnection();
                dataTableM3Users = DataAccessLayer.ExecuteStoredProcedure("spDW_M3_ListDSM").Tables[0];
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }
            finally
            {
                DataAccessLayer.CloseConnection();
            }
            cbM3.Properties.Items.Clear();
            foreach (DataRow dataRow in dataTableM3Users.Rows)
            {
                cbM3.Properties.Items.Add(new ComboBoxItem { Id = dataRow["DSM_ID"], Text = dataRow["DSM_NAME"].ToString() });
            }
            cbM3.SelectedIndex = 0;
            try
            {
                GetDefaultSettings();
            } catch (Exception) {}
        }

        /// <summary>
        /// Gets the sheet params list.
        /// </summary>
        /// <value>The sheet params list.</value>
        public List<SheetParamCollection> SheetParamsList
        {
            
            get
            {
                DataTable dataTableDate = DataAccessLayer.ExecuteStoredProcedure("spDW_OnWorkingDays", new[] { new SqlParameter("@Date", dateEdit.DateTime) }).Tables[0];
                List<SheetParamCollection> listParams = new List<SheetParamCollection>();
                foreach (Tab tab in Report.Tabs)
                {
                    foreach (Table tbl in tab.Tables)
                    {
                        if (tbl.Type == (rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact))
                        {
                            DsmId = (int)((ComboBoxItem)cbM3.SelectedItem).Id;
                            DsmName = ((ComboBoxItem)cbM3.SelectedItem).Text;
                            ReportDate = dateEdit.DateTime;
                            CurrentDate = Convert.ToDateTime(dataTableDate.Rows[0]["CurrentDate"]);
                            WorkDay = DaysToString(ConvertEx.ToDecimal(dataTableDate.Rows[0]["WorkDay"]));
                            WorkDays = DaysToString(ConvertEx.ToDecimal(dataTableDate.Rows[0]["WorkDaysInMonth"]));

                            SheetParamCollection param = new SheetParamCollection();
                            param.MainHeader = Resource.MainHeader;
                            param.TabId = tab.Id;
                            param.TableDataType = rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact; //TableType.Fact;
                            param.Add(new SheetParam { DisplayParamName = Resource.M3, SqlParamName = "@DSM_ID", Value = DsmId, DisplayValue = DsmName });
                            param.Add(new SheetParam { DisplayParamName = Resource.ReportDay, DisplayValue = CurrentDate.ToShortDateString() });
                            param.Add(new SheetParam { DisplayParamName = Resource.ReportMounth, DisplayValue = CurrentDate.ToString("MM yyyy") });
                            param.Add(new SheetParam { DisplayParamName = Resource.WorkDays, DisplayValue = WorkDays });
                            param.Add(new SheetParam { DisplayParamName = Resource.WorkDay, DisplayValue = WorkDay });
                            param.Add(new SheetParam { DisplayParamName = Resource.ReportDate, SqlParamName = "@ReportDate", Value = ReportDate.Date, DisplayValue = ReportDate.ToShortDateString() });

                            listParams.Add(param);

                            break;
                        }
                    }
                }
                return listParams;
            }
        }
        
        private static string DaysToString(decimal value)
        {
            return value.ToString(value % 1 == 0 ? "N0" : "N1");
        }
        
        public bool AddRatings()
        {
            if (rbType.SelectedIndex == 1 && dateEdit.DateTime.DayOfWeek != DayOfWeek.Saturday &&
                dateEdit.DateTime.DayOfWeek != DayOfWeek.Sunday)
                return true;
            return false;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (rbType.SelectedIndex == 0 && (dateEdit.DateTime.Month < DateTime.Now.Month 
                || dateEdit.DateTime.Year < DateTime.Now.Year))
            {
                XtraMessageBox.Show(Resource.Error_Date);
                //DialogResult = DialogResult.Retry;
            }
            else if (cbM3.SelectedIndex == -1)
            {
                XtraMessageBox.Show(Resource.M3_error);
            }
            else
            {

                try
                {
                    dict.Clear();
                    dict.Add("M3_ID", ((ComboBoxItem) cbM3.SelectedItem).Id.ToString());
                    SettingsAccessor.SaveSettings(dict, _reportId);
                } catch (Exception) {}
                DialogResult = DialogResult.OK;
            }
        }

        private DateTime DateParse(DateTime input_date)
        {
            string result_str = input_date.ToString("yyyy-MM-dd");
            DateTime parsed = DateTime.ParseExact(result_str, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            return parsed;
        }

        private void GetDefaultSettings()
        {
            string id = "";
            dict = SettingsAccessor.GetSettings(_reportId);
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                if (kvp.Key.Equals("M3_ID"))
                {
                    id = kvp.Value;
                    break;
                }
            }
            if (!id.Equals(""))
            {
                foreach (ComboBoxItem item in cbM3.Properties.Items)
                {
                    if (item.Id.ToString() == id)
                    {
                        cbM3.SelectedIndex = cbM3.Properties.Items.IndexOf(item);
                        break;
                    }

                }
            }
        }
    }

    

    /// <summary>
    /// ComboBoxItem
    /// </summary>
    internal class ComboBoxItem
    {
        object id;
        public object Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        public override string ToString()
        {
            return Text;
        }
    }
}
