﻿using System;

namespace Logica.Reports.M3FSMReport.DataProvider
{
    public class SqlConstants
    {
        // SP names
        public const string ProcRatingByProduct = "spDW_M3_OffTrade_Page6_4";
        public const string ProcRatingByM1 = "spDW_M3_OffTrade_Page6_3";

        // SP Params
        public const string ParamDsmId = "@DSM_ID";
        public const string ParamReportDate = "@ReportDate";
    }
}
