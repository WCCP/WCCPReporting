﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;

namespace Logica.Reports.M3FSMReport.DataProvider
{
    class DataAccessProvider
    {
        internal static DataTable GetRating(bool byProd, int dsmId, DateTime reportDate)
        {
            DataTable res = null;

            DataAccessLayer.OpenConnection();

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(byProd ? SqlConstants.ProcRatingByProduct : SqlConstants.ProcRatingByM1,
                new SqlParameter(SqlConstants.ParamDsmId, dsmId),
                new SqlParameter(SqlConstants.ParamReportDate, reportDate));

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];
            DataAccessLayer.CloseConnection();

            return res;
        }
    }
}
