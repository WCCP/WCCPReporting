using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MI.Forms
{
    public partial class ChooserForm : XtraForm
    {
        private string colSelected = "Selected";
             
        /// <summary>
        /// Determines whether this is Status form
        /// </summary>
        private bool isStatusWindow;

        /// <summary>
        /// Grid source
        /// </summary>
        private DataTable source = null;

        /// <summary>
        /// Determines whether chkBoxSelectAll is being refreshed
        /// </summary>
        private bool refreshingChkBoxSelectAll = false;

        /// <summary>
        /// Create the new instance of ChooserForm object
        /// </summary>
        public ChooserForm()
        {
            InitializeComponent();

            isStatusWindow = true;
        }

        /// <summary>
        /// Create the new instance of ChooserForm object
        /// </summary>
        /// <param name="data">Grid datatable</param>
        /// <param name="isError">Determines whether this is Status form</param>
        public ChooserForm(DataTable data, bool isError)
        {
            InitializeComponent();

            this.isStatusWindow = isError;
            this.source = data;

            this.gridControl.DataSource = data;
            
            if (isStatusWindow)
            {
                ShowStatusForm();
            }
            else
            {
                ShowChooserForm();
            }
                        
            CalculateStatistics();
        }

        /// <summary>
        /// Adds status column 
        /// </summary>
        private void AddSelectedColumn()
        {
            if (this.source != null && !this.source.Columns.Contains(colSelected))
            {
                DataColumn columnSelect = new DataColumn(colSelected, typeof(bool));
                columnSelect.DefaultValue = false;
                this.source.Columns.Add(columnSelect);
            }
        }

        /// <summary>
        /// Show view of Chooser form
        /// </summary>
        private void ShowChooserForm()
        {
            AddSelectedColumn();

            //this.gridView.ActiveFilterString = string.Format("{0} = {1}", colStatus, true);
            chkBoxSelectAll.Visible = true;
            simpleOK.Text = Resource.textChooserFormButton;
            this.Text = Resource.textChooserFormCaption;

            gridColumnSelect.Visible = true;
            gridColumnMonth.Visible = true;
            gridColumnRegion.Visible = true;
            gridColumnSKU.Visible = true;
            gridColumnMACO.Visible = true;
            gridColumnERP.Visible = true;

            gridColumnRowNum.Visible = false;
            gridColumnStatusDescr.Visible = false;

            this.chkBoxSelectAll.CheckState = CheckState.Unchecked;
            SetSelectedToAll(true);
            this.chkBoxSelectAll.Enabled = this.chkBoxSelectAll.CheckState != CheckState.Unchecked;
        }

        /// <summary>
        /// Shows view of Status form
        /// </summary>
        private void ShowStatusForm()
        {
            //this.gridView.ActiveFilterString = string.Format("{0} = {1}", colStatus, false);

            chkBoxSelectAll.Visible = false;
            simpleOK.Text = Resource.textStatusFormButton;
            this.Text = Resource.textStatusFormCaption;

            gridColumnSelect.Visible = false;
            gridColumnMonth.Visible = false;
            gridColumnRegion.Visible = false;
            gridColumnSKU.Visible = false;
            gridColumnMACO.Visible = false;
            gridColumnERP.Visible = false;

            gridColumnRowNum.Visible = true;
            gridColumnStatusDescr.Visible = true;
        }

        /// <summary>
        /// Gets the rows count.
        /// </summary>
        /// <value>The rows count.</value>
        public int RowsCount
        {
            get
            {
                DataTable data = this.gridControl.DataSource as DataTable;
                if (data == null) return 0;

                return data.Rows.Count;
            }
        }

        /// <summary>
        /// Gets the selected rows count.
        /// </summary>
        /// <value>The selected rows count.</value>
        public int SelectedCount
        {
            get
            {
                return (from DataRow row in this.source.Rows
                        where ConvertEx.ToBool(row[colSelected])
                        select row).Count();
            }
        }

        /// <summary>
        /// Handles the CellValueChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            CalculateStatistics();
        }

        /// <summary>
        /// Handles the CheckedChanged event of the chkBoxSelectAll control. 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.EventArgs"/> instance containing the event data.</param>
        private void chkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.refreshingChkBoxSelectAll)
            {
                this.SetSelectedToAll(this.chkBoxSelectAll.CheckState == CheckState.Checked);
            }
        }

        /// <summary>
        /// Sets the selected to all.
        /// </summary>
        /// <param name="selected">If set to <c>true</c> [selected].</param>
        private void SetSelectedToAll(bool selected)
        {
            DataTable dataTable = this.gridControl.DataSource as DataTable;
            if (dataTable == null) return;

            foreach (DataRow row in dataTable.Rows)
            {
                row[this.gridColumnSelect.FieldName] = selected;
            }

            CalculateStatistics();
        }

        /// <summary>
        /// Processes statistics.
        /// </summary>
        private void CalculateStatistics()
        {
            if (isStatusWindow)
            {
                grList.Text = string.Format("������: �� ������� ��������� ��������� ������:  {0}", this.RowsCount);
            }
            else
            {
                grList.Text = string.Format("������: ����� ���������� ����� - {0}, �������� - {1}",
                this.RowsCount, this.SelectedCount);

                RefreshChkBoxSelectAll();
            }
        }

        /// <summary>
        /// Refreshes chkBoxSelectAll state according to changes made
        /// </summary>
        private void RefreshChkBoxSelectAll()
        {
            refreshingChkBoxSelectAll = true;

            if (source.Rows.Count == 0 || this.SelectedCount == 0)
            {
                this.chkBoxSelectAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                this.chkBoxSelectAll.CheckState = this.SelectedCount == this.RowsCount? CheckState.Checked : CheckState.Indeterminate;
            }

            refreshingChkBoxSelectAll = false;
        }

        /// <summary>
        /// Gets the list of currently dislayed rows
        /// </summary>
        /// <returns>List of currently dislayed rows.</returns>
        private IList<DataRow> GetDisplayedRows()
        {
            IList<DataRow> rows = new List<DataRow>();

            if (this.gridView == null)
            {
                return rows;
            }

            for (int handle = 0; handle < this.gridView.DataRowCount; handle++)
            {
                rows.Add((DataRow)this.gridView.GetDataRow(handle));
            }

            return rows;
        }

        /// <summary>
        /// Handles the DataSourceChanged event of the gridControl control. 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The instance containing the event data.</param>
        private void gridControl_DataSourceChanged(object sender, EventArgs e)
        {
            CalculateStatistics();
        }

        /// <summary>
        /// Finishes the dialog with OK result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// Finishes the dialog with Cancel result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}