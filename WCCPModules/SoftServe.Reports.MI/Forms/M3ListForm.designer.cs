namespace SoftServe.Reports.MI.Forms
{
    partial class M3ListForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.grList = new DevExpress.XtraEditors.GroupControl();
        this.gridControl = new DevExpress.XtraGrid.GridControl();
        this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.gridColumnDSM_ID = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnDSM_Name = new DevExpress.XtraGrid.Columns.GridColumn();
        this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
        this.simpleOK = new DevExpress.XtraEditors.SimpleButton();
        ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
        this.grList.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
        this.SuspendLayout();
        // 
        // grList
        // 
        this.grList.Controls.Add(this.gridControl);
        this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
        this.grList.Location = new System.Drawing.Point(10, 10);
        this.grList.Name = "grList";
        this.grList.Size = new System.Drawing.Size(275, 262);
        this.grList.TabIndex = 13;
        this.grList.Text = "�3, ��� ������� ��� ����������� ������:";
        // 
        // gridControl
        // 
        this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.gridControl.Location = new System.Drawing.Point(2, 22);
        this.gridControl.MainView = this.gridView;
        this.gridControl.Name = "gridControl";
        this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
        this.gridControl.Size = new System.Drawing.Size(271, 238);
        this.gridControl.TabIndex = 3;
        this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
        // 
        // gridView
        // 
        this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
        this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
        this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnDSM_ID,
            this.gridColumnDSM_Name});
        this.gridView.GridControl = this.gridControl;
        this.gridView.Name = "gridView";
        this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
        this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
        this.gridView.OptionsView.ShowAutoFilterRow = true;
        this.gridView.OptionsView.ShowGroupPanel = false;
        // 
        // gridColumnDSM_ID
        // 
        this.gridColumnDSM_ID.AppearanceHeader.Options.UseTextOptions = true;
        this.gridColumnDSM_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gridColumnDSM_ID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gridColumnDSM_ID.Caption = "��� �3";
        this.gridColumnDSM_ID.FieldName = "DSM_ID";
        this.gridColumnDSM_ID.MaxWidth = 300;
        this.gridColumnDSM_ID.Name = "gridColumnDSM_ID";
        this.gridColumnDSM_ID.OptionsColumn.AllowEdit = false;
        this.gridColumnDSM_ID.OptionsColumn.AllowMove = false;
        this.gridColumnDSM_ID.OptionsColumn.ReadOnly = true;
        this.gridColumnDSM_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.gridColumnDSM_ID.Visible = true;
        this.gridColumnDSM_ID.VisibleIndex = 0;
        this.gridColumnDSM_ID.Width = 70;
        // 
        // gridColumnDSM_Name
        // 
        this.gridColumnDSM_Name.AppearanceHeader.Options.UseTextOptions = true;
        this.gridColumnDSM_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gridColumnDSM_Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gridColumnDSM_Name.Caption = "��� �3";
        this.gridColumnDSM_Name.FieldName = "DSM_Name";
        this.gridColumnDSM_Name.Name = "gridColumnDSM_Name";
        this.gridColumnDSM_Name.OptionsColumn.AllowEdit = false;
        this.gridColumnDSM_Name.OptionsColumn.AllowMove = false;
        this.gridColumnDSM_Name.OptionsColumn.ReadOnly = true;
        this.gridColumnDSM_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.gridColumnDSM_Name.Visible = true;
        this.gridColumnDSM_Name.VisibleIndex = 1;
        this.gridColumnDSM_Name.Width = 169;
        // 
        // repositoryItemCheckEdit1
        // 
        this.repositoryItemCheckEdit1.AutoHeight = false;
        this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
        // 
        // simpleOK
        // 
        this.simpleOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
        this.simpleOK.Location = new System.Drawing.Point(82, 292);
        this.simpleOK.Name = "simpleOK";
        this.simpleOK.Size = new System.Drawing.Size(116, 23);
        this.simpleOK.TabIndex = 14;
        this.simpleOK.Text = "OK";
        this.simpleOK.Click += new System.EventHandler(this.simpleOK_Click);
        // 
        // M3ListForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(295, 332);
        this.Controls.Add(this.simpleOK);
        this.Controls.Add(this.grList);
        this.MinimumSize = new System.Drawing.Size(300, 370);
        this.Name = "M3ListForm";
        this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 60);
        this.ShowIcon = false;
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "��������������";
        ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
        this.grList.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl grList;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnDSM_ID;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnDSM_Name;
    private DevExpress.XtraEditors.SimpleButton simpleOK;



  }
}