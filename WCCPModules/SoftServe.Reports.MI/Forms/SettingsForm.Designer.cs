﻿namespace Softserve.Reports.MI
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.lblWarning = new DevExpress.XtraEditors.LabelControl();
            this.grReportType = new DevExpress.XtraEditors.GroupControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelMain = new DevExpress.XtraEditors.PanelControl();
            this.panelMP = new DevExpress.XtraEditors.PanelControl();
            this.grAims = new DevExpress.XtraEditors.GroupControl();
            this.cbAims = new DevExpress.XtraEditors.LookUpEdit();
            this.grActivity = new DevExpress.XtraEditors.GroupControl();
            this.listActivity = new DevExpress.XtraEditors.ListBoxControl();
            this.panelCM = new DevExpress.XtraEditors.PanelControl();
            this.grBudgetOwner = new DevExpress.XtraEditors.GroupControl();
            this.chContractOwner = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.dateBaseEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateBaseStart = new DevExpress.XtraEditors.DateEdit();
            this.datePromo = new DevExpress.XtraEditors.DateEdit();
            this.cbStaffChanel = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cbReportType = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleMACO = new DevExpress.XtraEditors.SimpleButton();
            this.simpleLoadReport = new DevExpress.XtraEditors.SimpleButton();
            this.simpleCancel = new DevExpress.XtraEditors.SimpleButton();
            this.cDontUseBasketsForUpliftCalc = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grReportType)).BeginInit();
            this.grReportType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).BeginInit();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMP)).BeginInit();
            this.panelMP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grAims)).BeginInit();
            this.grAims.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbAims.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grActivity)).BeginInit();
            this.grActivity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCM)).BeginInit();
            this.panelCM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grBudgetOwner)).BeginInit();
            this.grBudgetOwner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chContractOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePromo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePromo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStaffChanel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbReportType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDontUseBasketsForUpliftCalc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // lblWarning
            // 
            this.lblWarning.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Appearance.Options.UseForeColor = true;
            this.lblWarning.Location = new System.Drawing.Point(332, 4);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(315, 13);
            this.lblWarning.TabIndex = 2;
            this.lblWarning.Text = "При текущих параметрах отчета есть недоступные колонки!";
            this.lblWarning.Visible = false;
            // 
            // grReportType
            // 
            this.grReportType.Controls.Add(this.labelControl5);
            this.grReportType.Controls.Add(this.labelControl4);
            this.grReportType.Controls.Add(this.labelControl3);
            this.grReportType.Controls.Add(this.labelControl2);
            this.grReportType.Controls.Add(this.labelControl1);
            this.grReportType.Controls.Add(this.panelMain);
            this.grReportType.Controls.Add(this.dateBaseEnd);
            this.grReportType.Controls.Add(this.dateBaseStart);
            this.grReportType.Controls.Add(this.datePromo);
            this.grReportType.Controls.Add(this.cbStaffChanel);
            this.grReportType.Location = new System.Drawing.Point(7, 38);
            this.grReportType.Name = "grReportType";
            this.grReportType.Size = new System.Drawing.Size(409, 316);
            this.grReportType.TabIndex = 5;
            this.grReportType.Text = "Инициативы:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(99, 93);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(8, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "с ";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(251, 93);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(15, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "по ";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 93);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(49, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Base Line:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 67);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(56, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Promo Line:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 35);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(94, 13);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Канал персонала: ";
            // 
            // panelMain
            // 
            this.panelMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelMain.Controls.Add(this.panelMP);
            this.panelMain.Controls.Add(this.panelCM);
            this.panelMain.Location = new System.Drawing.Point(5, 119);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(399, 189);
            this.panelMain.TabIndex = 8;
            // 
            // panelMP
            // 
            this.panelMP.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelMP.Controls.Add(this.grAims);
            this.panelMP.Controls.Add(this.grActivity);
            this.panelMP.Location = new System.Drawing.Point(3, 6);
            this.panelMP.Name = "panelMP";
            this.panelMP.Size = new System.Drawing.Size(182, 180);
            this.panelMP.TabIndex = 1;
            // 
            // grAims
            // 
            this.grAims.Controls.Add(this.cbAims);
            this.grAims.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grAims.Location = new System.Drawing.Point(0, 112);
            this.grAims.Name = "grAims";
            this.grAims.Padding = new System.Windows.Forms.Padding(10);
            this.grAims.Size = new System.Drawing.Size(182, 68);
            this.grAims.TabIndex = 3;
            this.grAims.Text = "Цели:";
            // 
            // cbAims
            // 
            this.cbAims.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbAims.EditValue = " ";
            this.cbAims.Location = new System.Drawing.Point(12, 32);
            this.cbAims.Name = "cbAims";
            this.cbAims.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbAims.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Aim_Name", "Цель")});
            this.cbAims.Properties.DisplayMember = "Aim_Name";
            this.cbAims.Properties.ValueMember = "Aim_Id";
            this.cbAims.Size = new System.Drawing.Size(158, 20);
            this.cbAims.TabIndex = 0;
            // 
            // grActivity
            // 
            this.grActivity.Controls.Add(this.listActivity);
            this.grActivity.Dock = System.Windows.Forms.DockStyle.Top;
            this.grActivity.Location = new System.Drawing.Point(0, 0);
            this.grActivity.Name = "grActivity";
            this.grActivity.Size = new System.Drawing.Size(182, 111);
            this.grActivity.TabIndex = 3;
            this.grActivity.Text = "ТМ активности:";
            // 
            // listActivity
            // 
            this.listActivity.DisplayMember = "Action_Name";
            this.listActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listActivity.Location = new System.Drawing.Point(2, 22);
            this.listActivity.Name = "listActivity";
            this.listActivity.Size = new System.Drawing.Size(178, 87);
            this.listActivity.TabIndex = 0;
            this.listActivity.ValueMember = "Action_ID";
            this.listActivity.SelectedValueChanged += new System.EventHandler(this.listActivity_SelectedValueChanged);
            // 
            // panelCM
            // 
            this.panelCM.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCM.Controls.Add(this.grBudgetOwner);
            this.panelCM.Location = new System.Drawing.Point(216, 6);
            this.panelCM.Name = "panelCM";
            this.panelCM.Size = new System.Drawing.Size(180, 180);
            this.panelCM.TabIndex = 0;
            // 
            // grBudgetOwner
            // 
            this.grBudgetOwner.Controls.Add(this.chContractOwner);
            this.grBudgetOwner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grBudgetOwner.Location = new System.Drawing.Point(0, 0);
            this.grBudgetOwner.Name = "grBudgetOwner";
            this.grBudgetOwner.Size = new System.Drawing.Size(180, 180);
            this.grBudgetOwner.TabIndex = 2;
            this.grBudgetOwner.Text = "Владелец бюджета:";
            // 
            // chContractOwner
            // 
            this.chContractOwner.CheckOnClick = true;
            this.chContractOwner.DisplayMember = "BudgetOwner_Name";
            this.chContractOwner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chContractOwner.Location = new System.Drawing.Point(2, 22);
            this.chContractOwner.Name = "chContractOwner";
            this.chContractOwner.Size = new System.Drawing.Size(176, 156);
            this.chContractOwner.TabIndex = 0;
            this.chContractOwner.ValueMember = "BudgetOwner_ID";
            // 
            // dateBaseEnd
            // 
            this.dateBaseEnd.EditValue = null;
            this.dateBaseEnd.Location = new System.Drawing.Point(272, 90);
            this.dateBaseEnd.Name = "dateBaseEnd";
            this.dateBaseEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateBaseEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateBaseEnd.Size = new System.Drawing.Size(100, 20);
            this.dateBaseEnd.TabIndex = 7;
            this.dateBaseEnd.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // dateBaseStart
            // 
            this.dateBaseStart.EditValue = null;
            this.dateBaseStart.Location = new System.Drawing.Point(124, 90);
            this.dateBaseStart.Name = "dateBaseStart";
            this.dateBaseStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateBaseStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateBaseStart.Size = new System.Drawing.Size(100, 20);
            this.dateBaseStart.TabIndex = 6;
            this.dateBaseStart.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // datePromo
            // 
            this.datePromo.EditValue = null;
            this.datePromo.Location = new System.Drawing.Point(124, 64);
            this.datePromo.Name = "datePromo";
            this.datePromo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datePromo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.datePromo.Size = new System.Drawing.Size(100, 20);
            this.datePromo.TabIndex = 5;
            this.datePromo.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // cbStaffChanel
            // 
            this.cbStaffChanel.Location = new System.Drawing.Point(124, 32);
            this.cbStaffChanel.Name = "cbStaffChanel";
            this.cbStaffChanel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbStaffChanel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "Канал")});
            this.cbStaffChanel.Properties.DisplayMember = "ChanelType";
            this.cbStaffChanel.Properties.ValueMember = "ChanelType_id";
            this.cbStaffChanel.Size = new System.Drawing.Size(155, 20);
            this.cbStaffChanel.TabIndex = 4;
            this.cbStaffChanel.EditValueChanged += new System.EventHandler(this.cbStaffChanel_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(20, 12);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(140, 13);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Выберите тип инициативы:";
            // 
            // cbReportType
            // 
            this.cbReportType.Location = new System.Drawing.Point(182, 9);
            this.cbReportType.Name = "cbReportType";
            this.cbReportType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbReportType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "Тип")});
            this.cbReportType.Properties.DisplayMember = "Text";
            this.cbReportType.Properties.ValueMember = "Id";
            this.cbReportType.Size = new System.Drawing.Size(171, 20);
            this.cbReportType.TabIndex = 16;
            this.cbReportType.EditValueChanged += new System.EventHandler(this.cbReportType_EditValueChanged);
            // 
            // simpleMACO
            // 
            this.simpleMACO.Location = new System.Drawing.Point(7, 393);
            this.simpleMACO.Name = "simpleMACO";
            this.simpleMACO.Size = new System.Drawing.Size(117, 25);
            this.simpleMACO.TabIndex = 17;
            this.simpleMACO.Text = "Загрузить MACO";
            this.simpleMACO.Click += new System.EventHandler(this.simpleMACO_Click);
            // 
            // simpleLoadReport
            // 
            this.simpleLoadReport.Image = global::SoftServe.Reports.MI.Resource.check_24;
            this.simpleLoadReport.Location = new System.Drawing.Point(154, 393);
            this.simpleLoadReport.Name = "simpleLoadReport";
            this.simpleLoadReport.Size = new System.Drawing.Size(162, 25);
            this.simpleLoadReport.TabIndex = 18;
            this.simpleLoadReport.Text = "Сгенерировать отчет";
            this.simpleLoadReport.Click += new System.EventHandler(this.simpleShowReport_Click);
            // 
            // simpleCancel
            // 
            this.simpleCancel.Image = global::SoftServe.Reports.MI.Resource.close_24;
            this.simpleCancel.Location = new System.Drawing.Point(322, 393);
            this.simpleCancel.Name = "simpleCancel";
            this.simpleCancel.Size = new System.Drawing.Size(94, 25);
            this.simpleCancel.TabIndex = 19;
            this.simpleCancel.Text = "Отмена";
            this.simpleCancel.Click += new System.EventHandler(this.simpleCancel_Click);
            // 
            // cDontUseBasketsForUpliftCalc
            // 
            this.cDontUseBasketsForUpliftCalc.Location = new System.Drawing.Point(18, 360);
            this.cDontUseBasketsForUpliftCalc.Name = "cDontUseBasketsForUpliftCalc";
            this.cDontUseBasketsForUpliftCalc.Properties.Caption = "Не использовать разбивку по корзинам при расчете Volume Uplift";
            this.cDontUseBasketsForUpliftCalc.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cDontUseBasketsForUpliftCalc.Size = new System.Drawing.Size(361, 19);
            this.cDontUseBasketsForUpliftCalc.TabIndex = 20;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 427);
            this.Controls.Add(this.cDontUseBasketsForUpliftCalc);
            this.Controls.Add(this.simpleCancel);
            this.Controls.Add(this.simpleLoadReport);
            this.Controls.Add(this.simpleMACO);
            this.Controls.Add(this.cbReportType);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.grReportType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры отчёта - Мониторинг инициатив";
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grReportType)).EndInit();
            this.grReportType.ResumeLayout(false);
            this.grReportType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).EndInit();
            this.panelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelMP)).EndInit();
            this.panelMP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grAims)).EndInit();
            this.grAims.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbAims.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grActivity)).EndInit();
            this.grActivity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCM)).EndInit();
            this.panelCM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grBudgetOwner)).EndInit();
            this.grBudgetOwner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chContractOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBaseStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePromo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePromo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStaffChanel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbReportType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cDontUseBasketsForUpliftCalc.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.LabelControl lblWarning;
        private DevExpress.XtraEditors.GroupControl grReportType;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelMain;
        private DevExpress.XtraEditors.PanelControl panelMP;
        private DevExpress.XtraEditors.GroupControl grAims;
        private DevExpress.XtraEditors.LookUpEdit cbAims;
        private DevExpress.XtraEditors.GroupControl grActivity;
        private DevExpress.XtraEditors.ListBoxControl listActivity;
        private DevExpress.XtraEditors.PanelControl panelCM;
        private DevExpress.XtraEditors.GroupControl grBudgetOwner;
        private DevExpress.XtraEditors.CheckedListBoxControl chContractOwner;
        private DevExpress.XtraEditors.DateEdit dateBaseEnd;
        private DevExpress.XtraEditors.DateEdit dateBaseStart;
        private DevExpress.XtraEditors.DateEdit datePromo;
        private DevExpress.XtraEditors.LookUpEdit cbStaffChanel;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LookUpEdit cbReportType;
        private DevExpress.XtraEditors.SimpleButton simpleMACO;
        private DevExpress.XtraEditors.SimpleButton simpleLoadReport;
        private DevExpress.XtraEditors.SimpleButton simpleCancel;
        private DevExpress.XtraEditors.CheckEdit cDontUseBasketsForUpliftCalc;
    }
}