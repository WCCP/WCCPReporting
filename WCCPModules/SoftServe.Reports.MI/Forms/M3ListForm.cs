using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MI.Forms
{
    public partial class M3ListForm : XtraForm
    {
        /// <summary>
        /// Create the new instance of M3ListForm object
        /// </summary>
        public M3ListForm()
        {
            InitializeComponent();

            grList.Text = string.Format("�3, ��� ������� ��� ����������� ������:  {0}", this.RowsCount);
        }

        /// <summary>
        /// Create the new instance of M3ListForm object
        /// </summary>
        public M3ListForm(DataTable source)
        {
            InitializeComponent();

            gridControl.BeginUpdate();
            gridControl.DataSource = source;
            gridControl.EndUpdate();

            grList.Text = string.Format("�3, ��� ������� ��� ����������� ������:  {0}", this.RowsCount);
        }
               
        /// <summary>
        /// Gets the rows count.
        /// </summary>
        /// <value>The rows count.</value>
        public int RowsCount
        {
            get
            {
                DataTable data = this.gridControl.DataSource as DataTable;
                if (data == null) return 0;

                return data.Rows.Count;
            }
        }
       
        /// <summary>
        /// Finishes the dialog with OK result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}