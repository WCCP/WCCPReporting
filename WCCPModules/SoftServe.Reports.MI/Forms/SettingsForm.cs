﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.MI;
using SoftServe.Reports.MI.Utility;

namespace Softserve.Reports.MI
{
    /// <summary>
    /// Describes the settings form
    /// </summary>
    internal partial class SettingsForm : XtraForm
    {
        /// <summary>
        /// Initializes the new instance of SettingsForm object
        /// </summary>
        /// <param name="parent"></param>
        public SettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();

            LoadDBData();
        }

        /// <summary>
        /// Loads settings info.
        /// </summary>
        /// <param name="info">Settings params.</param>
        public void LoadSettingsInfo(ReportInfo info)
        {
            cbReportType.EditValue = ConvertEx.ToInt(info.Type);

            datePromo.DateTime = info.PromoDate;

            dateBaseStart.DateTime = info.BaseDateStart;
            dateBaseEnd.DateTime = info.BaseDateEnd;

            cbStaffChanel.EditValue = ConvertEx.ToInt(info.Chanel);

            cDontUseBasketsForUpliftCalc.Checked = !info.UseBasketsInUpliftCalc;

            listActivity.SelectedValue = info.ActivityId;

            if (info.AimId > -1)
            {
                cbAims.EditValue = info.AimId;
            }

            chContractOwner.UnCheckAll();
            for (int i = 0; i < DBSettingsData.Owners.Rows.Count; i++)
            {
                if (info.ContractOwnerIds.Contains(ConvertEx.ToInt(DBSettingsData.Owners.Rows[i][0])))
                {
                    chContractOwner.SetItemChecked(i, true);
                }
            }
        }

        /// <summary>
        /// Gets settings params.
        /// </summary>
        /// <returns>Settings params.</returns>
        public ReportInfo GetSettingsInfo()
        {
            ReportInfo newInfo = new ReportInfo();

            newInfo.PromoDate = datePromo.DateTime.Date;

            newInfo.BaseDateStart = dateBaseStart.DateTime.Date;
            newInfo.BaseDateEnd = dateBaseEnd.DateTime.Date;

            newInfo.Type = (ReportType)cbReportType.EditValue;

            newInfo.Chanel = (StaffChanel)cbStaffChanel.EditValue;

            newInfo.UseBasketsInUpliftCalc = !cDontUseBasketsForUpliftCalc.Checked;

            if (listActivity.SelectedIndex > -1)
            {
                newInfo.ActivityId = ConvertEx.ToInt(listActivity.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ConvertEx.ToString(cbAims.EditValue).Trim()))
            {
                newInfo.AimId = ConvertEx.ToInt(cbAims.EditValue);
            }

            foreach (int index in chContractOwner.CheckedIndices)
            {
                newInfo.ContractOwnerIds.Add(ConvertEx.ToInt(chContractOwner.GetItemValue(index)));
            }

            return newInfo;
        }

        /// <summary>
        /// Loads data from DB
        /// </summary>
        private void LoadDBData()
        {
            List<ComboBoxItem> list = new List<ComboBoxItem>();
            list.Add(new ComboBoxItem(ConvertEx.ToInt(ReportType.MarketProgram), Resource.ReportTypeMP));
            list.Add(new ComboBoxItem(ConvertEx.ToInt(ReportType.ContractManagement), Resource.ReportTypeCM));

            cbReportType.Properties.DataSource = list;
            cbStaffChanel.Properties.DataSource = DataProvider.ChannelList;
            chContractOwner.DataSource = DBSettingsData.Owners;
            cDontUseBasketsForUpliftCalc.Checked = DataProvider.Country == Countries.Russia;
        }

        /// <summary>
        /// Configures form view as Market Progs
        /// </summary>
        private void ShowFormMP()
        {
            grReportType.Text = Resource.PanelHeaderMP;

            panelCM.Visible = false;

            panelMP.Dock = DockStyle.Fill;
            panelMP.Visible = true;
        }

        /// <summary>
        /// Configures form view as Contract Management
        /// </summary>
        private void ShowFormCM()
        {
            grReportType.Text = Resource.PanelHeaderCM;

            panelMP.Visible = false;

            panelCM.Dock = DockStyle.Fill;
            panelCM.Visible = true;
        }

        /// <summary>
        /// Checks whether all fields are filled properly.
        /// </summary>
        /// <returns>True if all fields are filled properly.</returns>
        private bool CheckFields()
        {
            List<string> errors = new List<string>();

            if (string.IsNullOrEmpty(datePromo.Text))
            {
                errors.Add(Resource.errorPromoDateNotFilled);
            }

            if (string.IsNullOrEmpty(dateBaseStart.Text))
            {
                errors.Add(Resource.errorBaseDateStartNotFilled);
            }

            if (string.IsNullOrEmpty(dateBaseEnd.Text))
            {
                errors.Add(Resource.errorBaseDateEndNotFilled);
            }

            switch ((ReportType)cbReportType.EditValue)
            {
                case ReportType.MarketProgram:
                    if (listActivity.SelectedIndex == -1)
                    {
                        errors.Add(Resource.errorNoActivityChosen);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(ConvertEx.ToString(cbAims.EditValue).Trim()))
                        {
                            errors.Add(Resource.errorNoAimChosen);
                        }
                    }
                    break;

                case ReportType.ContractManagement:
                    if (chContractOwner.CheckedIndices.Count == 0)
                    {
                        errors.Add(Resource.errorNoOwnerChosen);
                    }
                    break;
            }

            if (errors.Count > 0)
            {
                string mess = "\t" + Resource.errorWrongFields + "\n\n";

                foreach (string error in errors)
                {
                    mess += string.Format("\t\t\t- {0};\n", error);
                }

                mess += "\n";

                XtraMessageBox.Show(mess, Resource.Warning);
            }

            return errors.Count == 0;
        }

        /// <summary>
        /// Filters Activities by Staff Chanel
        /// </summary>
        private void FilterActivitiesByStaffChanel()
        {
            DataView dv = DBSettingsData.Activities(datePromo.DateTime.Date).DefaultView;

            StaffChanel chanel = cbStaffChanel.EditValue != null ? (StaffChanel)cbStaffChanel.EditValue : StaffChanel.OnOff;

            string chanelFormatString = string.Format("{0}", ConvertEx.ToInt(chanel));

            dv.RowFilter = string.Format("ChanelType_ID in ({0})", chanelFormatString);
            listActivity.DataSource = dv.ToTable();
        }

        /// <summary>
        /// Handles Click event of simpleShowReport
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void simpleShowReport_Click(object sender, EventArgs e)
        {
            if (CheckFields())
            {
                DialogResult = DialogResult.OK;
            }
        }

        /// <summary>
        /// Handles Click event of simpleCancel
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void simpleCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Handles Click event of simpleMACO
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void simpleMACO_Click(object sender, EventArgs e)
        {
            ExcelUploader uploader = new ExcelUploader();
            uploader.StartUploading();
        }

        /// <summary>
        /// Handles EditValueChanged event of cbReportType
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void cbReportType_EditValueChanged(object sender, EventArgs e)
        {
            switch ((ReportType)cbReportType.EditValue)
            {
                case ReportType.MarketProgram:
                    ShowFormMP();
                    break;
                case ReportType.ContractManagement:
                    ShowFormCM();
                    break;
            }
        }

        /// <summary>
        /// Handles EditValueChanged event of cbStaffChanel
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void cbStaffChanel_EditValueChanged(object sender, EventArgs e)
        {
            FilterActivitiesByStaffChanel();
        }

        /// <summary>
        /// Handles EditValueChanged event of dateEdit
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(dateBaseStart))
            {
                dateBaseEnd.Properties.MinValue = !string.IsNullOrEmpty(dateBaseStart.Text) ? dateBaseStart.DateTime : DateTime.MinValue;
            }
            if (sender.Equals(dateBaseEnd))
            {
                dateBaseStart.Properties.MaxValue = !string.IsNullOrEmpty(dateBaseEnd.Text) ? dateBaseEnd.DateTime : DateTime.MaxValue;
                datePromo.Properties.MinValue = !string.IsNullOrEmpty(dateBaseEnd.Text) ? dateBaseEnd.DateTime : DateTime.MinValue;
            }
            if (sender.Equals(datePromo))
            {
                listActivity.DataSource = DBSettingsData.Activities(datePromo.DateTime.Date, true);
                FilterActivitiesByStaffChanel();

                dateBaseEnd.Properties.MaxValue = !string.IsNullOrEmpty(datePromo.Text) ? datePromo.DateTime : DateTime.MaxValue;
            }
        }

        /// <summary>
        /// Handles SelectedValueChanged event of listActivity
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void listActivity_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbReportType.EditValue != null)
            {
                DataView dv = DBSettingsData.Aims.DefaultView;
                dv.RowFilter = string.Format("Action_Id = {0}", ConvertEx.ToInt(listActivity.SelectedValue));

                DataTable dt = dv.ToTable();
                cbAims.Properties.DataSource = dt;

                if (dt.Rows.Count > 0)
                {
                    cbAims.EditValue = ConvertEx.ToInt(dt.Rows[0][1]);
                }
            }
        }
    }
}
