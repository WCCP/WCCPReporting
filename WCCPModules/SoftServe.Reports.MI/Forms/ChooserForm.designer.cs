namespace SoftServe.Reports.MI.Forms
{
    partial class ChooserForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.chkBoxSelectAll = new DevExpress.XtraEditors.CheckEdit();
        this.grList = new DevExpress.XtraEditors.GroupControl();
        this.gridControl = new DevExpress.XtraGrid.GridControl();
        this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.gridColumnSelect = new DevExpress.XtraGrid.Columns.GridColumn();
        this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
        this.gridColumnRowNum = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnRegion = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnStatusDescr = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnSKU = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnMonth = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnERP = new DevExpress.XtraGrid.Columns.GridColumn();
        this.gridColumnMACO = new DevExpress.XtraGrid.Columns.GridColumn();
        this.simpleOK = new DevExpress.XtraEditors.SimpleButton();
        this.simpleCancel = new DevExpress.XtraEditors.SimpleButton();
        ((System.ComponentModel.ISupportInitialize)(this.chkBoxSelectAll.Properties)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
        this.grList.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
        this.SuspendLayout();
        // 
        // chkBoxSelectAll
        // 
        this.chkBoxSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
        this.chkBoxSelectAll.EditValue = null;
        this.chkBoxSelectAll.Enabled = false;
        this.chkBoxSelectAll.Location = new System.Drawing.Point(10, 278);
        this.chkBoxSelectAll.Name = "chkBoxSelectAll";
        this.chkBoxSelectAll.Properties.AllowGrayed = true;
        this.chkBoxSelectAll.Properties.Caption = "������� ���";
        this.chkBoxSelectAll.Size = new System.Drawing.Size(106, 19);
        this.chkBoxSelectAll.TabIndex = 12;
        this.chkBoxSelectAll.CheckedChanged += new System.EventHandler(this.chkBoxSelectAll_CheckedChanged);
        // 
        // grList
        // 
        this.grList.Controls.Add(this.gridControl);
        this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
        this.grList.Location = new System.Drawing.Point(10, 10);
        this.grList.Name = "grList";
        this.grList.Size = new System.Drawing.Size(608, 262);
        this.grList.TabIndex = 13;
        this.grList.Text = "������:";
        // 
        // gridControl
        // 
        this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.gridControl.Location = new System.Drawing.Point(2, 22);
        this.gridControl.MainView = this.gridView;
        this.gridControl.Name = "gridControl";
        this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
        this.gridControl.Size = new System.Drawing.Size(604, 238);
        this.gridControl.TabIndex = 3;
        this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
        // 
        // gridView
        // 
        this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
        this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
        this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSelect,
            this.gridColumnRowNum,
            this.gridColumnRegion,
            this.gridColumnStatusDescr,
            this.gridColumnStatus,
            this.gridColumnSKU,
            this.gridColumnMonth,
            this.gridColumnERP,
            this.gridColumnMACO});
        this.gridView.GridControl = this.gridControl;
        this.gridView.Name = "gridView";
        this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
        this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
        this.gridView.OptionsView.ShowAutoFilterRow = true;
        this.gridView.OptionsView.ShowGroupPanel = false;
        this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
        // 
        // gridColumnSelect
        // 
        this.gridColumnSelect.Caption = " ";
        this.gridColumnSelect.ColumnEdit = this.repositoryItemCheckEdit1;
        this.gridColumnSelect.FieldName = "Selected";
        this.gridColumnSelect.MaxWidth = 30;
        this.gridColumnSelect.Name = "gridColumnSelect";
        this.gridColumnSelect.Visible = true;
        this.gridColumnSelect.VisibleIndex = 0;
        this.gridColumnSelect.Width = 26;
        // 
        // repositoryItemCheckEdit1
        // 
        this.repositoryItemCheckEdit1.AutoHeight = false;
        this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
        // 
        // gridColumnRowNum
        // 
        this.gridColumnRowNum.AppearanceHeader.Options.UseTextOptions = true;
        this.gridColumnRowNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gridColumnRowNum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gridColumnRowNum.Caption = "����� ������";
        this.gridColumnRowNum.FieldName = "RowNumber";
        this.gridColumnRowNum.MaxWidth = 100;
        this.gridColumnRowNum.Name = "gridColumnRowNum";
        this.gridColumnRowNum.OptionsColumn.AllowEdit = false;
        this.gridColumnRowNum.OptionsColumn.AllowMove = false;
        this.gridColumnRowNum.OptionsColumn.ReadOnly = true;
        this.gridColumnRowNum.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.gridColumnRowNum.Width = 95;
        // 
        // gridColumnRegion
        // 
        this.gridColumnRegion.AppearanceHeader.Options.UseTextOptions = true;
        this.gridColumnRegion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gridColumnRegion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gridColumnRegion.Caption = "�������";
        this.gridColumnRegion.FieldName = "Region";
        this.gridColumnRegion.Name = "gridColumnRegion";
        this.gridColumnRegion.OptionsColumn.AllowEdit = false;
        this.gridColumnRegion.OptionsColumn.AllowMove = false;
        this.gridColumnRegion.OptionsColumn.ReadOnly = true;
        this.gridColumnRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.gridColumnRegion.Visible = true;
        this.gridColumnRegion.VisibleIndex = 2;
        this.gridColumnRegion.Width = 100;
        // 
        // gridColumnStatusDescr
        // 
        this.gridColumnStatusDescr.AppearanceHeader.Options.UseTextOptions = true;
        this.gridColumnStatusDescr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gridColumnStatusDescr.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gridColumnStatusDescr.Caption = "�������";
        this.gridColumnStatusDescr.FieldName = "StatusDescr";
        this.gridColumnStatusDescr.Name = "gridColumnStatusDescr";
        this.gridColumnStatusDescr.OptionsColumn.AllowEdit = false;
        this.gridColumnStatusDescr.OptionsColumn.AllowMove = false;
        this.gridColumnStatusDescr.OptionsColumn.ReadOnly = true;
        this.gridColumnStatusDescr.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.gridColumnStatusDescr.Width = 81;
        // 
        // gridColumnStatus
        // 
        this.gridColumnStatus.AppearanceHeader.Options.UseTextOptions = true;
        this.gridColumnStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gridColumnStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gridColumnStatus.Caption = "Status";
        this.gridColumnStatus.FieldName = "Status";
        this.gridColumnStatus.Name = "gridColumnStatus";
        this.gridColumnStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        // 
        // gridColumnSKU
        // 
        this.gridColumnSKU.Caption = "���";
        this.gridColumnSKU.FieldName = "SKU";
        this.gridColumnSKU.Name = "gridColumnSKU";
        this.gridColumnSKU.OptionsColumn.AllowEdit = false;
        this.gridColumnSKU.Visible = true;
        this.gridColumnSKU.VisibleIndex = 4;
        this.gridColumnSKU.Width = 210;
        // 
        // gridColumnMonth
        // 
        this.gridColumnMonth.Caption = "�����";
        this.gridColumnMonth.FieldName = "Month";
        this.gridColumnMonth.Name = "gridColumnMonth";
        this.gridColumnMonth.OptionsColumn.AllowEdit = false;
        this.gridColumnMonth.Visible = true;
        this.gridColumnMonth.VisibleIndex = 1;
        this.gridColumnMonth.Width = 67;
        // 
        // gridColumnERP
        // 
        this.gridColumnERP.Caption = " ��� ERP";
        this.gridColumnERP.FieldName = "ERP";
        this.gridColumnERP.Name = "gridColumnERP";
        this.gridColumnERP.Visible = true;
        this.gridColumnERP.VisibleIndex = 3;
        this.gridColumnERP.Width = 67;
        // 
        // gridColumnMACO
        // 
        this.gridColumnMACO.Caption = "MACO/HL, UAH";
        this.gridColumnMACO.FieldName = "MACO";
        this.gridColumnMACO.Name = "gridColumnMACO";
        this.gridColumnMACO.OptionsColumn.AllowEdit = false;
        this.gridColumnMACO.Visible = true;
        this.gridColumnMACO.VisibleIndex = 5;
        this.gridColumnMACO.Width = 113;
        // 
        // simpleOK
        // 
        this.simpleOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.simpleOK.Location = new System.Drawing.Point(354, 290);
        this.simpleOK.Name = "simpleOK";
        this.simpleOK.Size = new System.Drawing.Size(181, 23);
        this.simpleOK.TabIndex = 14;
        this.simpleOK.Text = "��������� ������ �������";
        this.simpleOK.Click += new System.EventHandler(this.simpleOK_Click);
        // 
        // simpleCancel
        // 
        this.simpleCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.simpleCancel.Location = new System.Drawing.Point(541, 290);
        this.simpleCancel.Name = "simpleCancel";
        this.simpleCancel.Size = new System.Drawing.Size(75, 23);
        this.simpleCancel.TabIndex = 15;
        this.simpleCancel.Text = "������";
        this.simpleCancel.Click += new System.EventHandler(this.simpleCancel_Click);
        // 
        // ChooserForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(628, 332);
        this.Controls.Add(this.simpleCancel);
        this.Controls.Add(this.simpleOK);
        this.Controls.Add(this.grList);
        this.Controls.Add(this.chkBoxSelectAll);
        this.MinimumSize = new System.Drawing.Size(644, 370);
        this.Name = "ChooserForm";
        this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 60);
        this.ShowIcon = false;
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "�������� MACO";
        ((System.ComponentModel.ISupportInitialize)(this.chkBoxSelectAll.Properties)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
        this.grList.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.CheckEdit chkBoxSelectAll;
    private DevExpress.XtraEditors.GroupControl grList;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnSelect;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnRowNum;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnRegion;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatusDescr;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatus;
    private DevExpress.XtraEditors.SimpleButton simpleOK;
    private DevExpress.XtraEditors.SimpleButton simpleCancel;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnSKU;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnMonth;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnMACO;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumnERP;



  }
}