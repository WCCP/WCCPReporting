﻿using System;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;
using Logica.Reports.BaseReportControl.Utility;
using System.Globalization;

namespace SoftServe.Reports.MI
{
    internal static class DataProvider
    {
        /// <summary>
        /// Gets the channel list.
        /// </summary>
        /// <value>The channel list.</value>
        public static DataTable ChannelList
        {
            get
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_CHANNEL_LIST);

                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the actions list.
        /// </summary>
        /// <value>The actions list.</value>
        public static DataTable ActivitiesList(DateTime promoLine)
        {
            DataTable dataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_ACTIVITIES_LIST,
                new SqlParameter[] { 
                new SqlParameter(Constants.SP_GET_ACTIVITIES_LIST_PARAM_1, promoLine)
                });

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTable = dataSet.Tables[0];
            }
            return dataTable;
        }

        /// <summary>
        /// Gets the aims list.
        /// </summary>
        /// <value>The aims list.</value>
        public static DataTable AimsList
        {
            get
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_AIMS_LIST);

                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the budget owners list.
        /// </summary>
        /// <value>The budget owners list.</value>
        public static DataTable OwnersList
        {
            get
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_OWNERS_LIST);

                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the main report data.
        /// </summary>
        /// <param name="baseLineStart">Base line start date</param>
        /// <param name="baseLineEnd">Base line end date</param>
        /// <param name="promoLine">Promo line date</param>
        /// <param name="chanelId">Staff chanel id</param>
        /// <param name="filter">Id list</param>
        /// <returns>The main report data.</returns>
        public static DataSet GetMainReportData(int reportType, DateTime baseLineStart, DateTime baseLineEnd, DateTime promoLine, int chanelId, int aimId, string filter, bool useBasketsForUpliftCalc)
        {
            DataTable dataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_REPORT_DATA,
                    new[]{ 
                new SqlParameter(Constants.SP_GET_REPORT_DATA_MI_Type, reportType),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Channel_ID, chanelId),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_BaseLine_Begin, baseLineStart),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_BaseLine_End, baseLineEnd),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_PromoLine_Month, promoLine),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Aim_ID, aimId),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Filter_ID, filter),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_UseUpliftBasket, useBasketsForUpliftCalc)
            });

            if (dataSet != null && dataSet.Tables.Count == 3)
            {
                dataTable = dataSet.Tables[0];
            }

            if (dataTable != null)
            {
                foreach (DataRow dr in dataTable.Rows)
                {
                    dr[9] = double.Parse((100 * ConvertEx.ToDouble(dr[9])).ToString("N2"));
                    dr[10] = double.Parse(ConvertEx.ToDouble(dr[10]).ToString("N3"));
                    dr[11] = double.Parse(ConvertEx.ToDouble(dr[11]).ToString("N2"));
                    dr[12] = double.Parse(ConvertEx.ToDouble(dr[12]).ToString("N3"));
                }
            }

            return dataSet;
        }

        /// <summary>
        /// Loads MACO to DB
        /// </summary>
        /// <param name="tblMACO">MACO table</param>
        public static void LoadMaco(DataTable MACO)
        {
            DataAccessLayer.ExecuteQuery(@"DELETE FROM tmpDW_MACO");

            foreach (DataRow row in MACO.Rows)
            {
                DateTime date = ConvertEx.ToDateTime(row[0]);
                string dateStr = date.Year.ToString("D4") + date.Month.ToString("D2") + date.Day.ToString("D2");
                string script = string.Format(@"INSERT INTO tmpDW_MACO 
                                               (MACO_MONTH, District, Product_WH, Product_Name, MACO) 
                                                VALUES('{0}', '{1}', {2}, '{3}', {4})",
                                              dateStr, row[1], row[4], row[2], ConvertEx.ToDecimal(row[3]).ToString(CultureInfo.InvariantCulture));

                DataAccessLayer.ExecuteQuery(script);
            }

            DataAccessLayer.ExecuteStoredProcedure(Constants.SP_SET_MACO);
        }

        /// <summary>
        /// Gets current user's country
        /// </summary>
        /// <value>Current user's country</value>
        public static Countries Country
        {
            get
            {
                object countryObj = DataAccessLayer.ExecuteScalarStoredProcedure(Constants.SP_GET_COUNTRY_FOR_CURRENT_USER);

                return (Countries)countryObj;
            }
        }
    }
}
