﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MI.UserControls
{
    [ToolboxItem(false)]
    public partial class MainControl : UserControl
    {
        /// <summary>
        /// Sales volume format
        /// </summary>
        private string formatDal = "{0:N3}";
        
        /// <summary>
        /// Currency and percents format
        /// </summary>
        private string formatHrnPr = "{0:N2}";

        /// <summary>
        /// SumProduct of colUpliftPct and colSalesVolBase
        /// </summary>
        private double sumProdUpliftPer;

        /// <summary>
        /// SumProduct of colAvgMACO and colSalesVol
        /// </summary>
        private double sumProdAvgMACO;
        
        /// <summary>
        /// Sum of colSalesVolBase
        /// </summary>
        private double sumUpliftPer;

        /// <summary>
        /// Sum of colSalesVol
        /// </summary>
        private double sumAvgMACO;

        /// <summary>
        /// The group level name 
        /// </summary>
        private string groupName;

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the grid datasource
        /// </summary>
        public DataTable DataSource
        {
            get
            {
                return gridControl.DataSource as DataTable;
            }

            set
            {
                gridControl.BeginUpdate();

                gridControl.DataSource = value;
                gridView.ExpandAllGroups();
                
                gridControl.EndUpdate();
            }
        }

        /// <summary>
        /// Changes view in order to export it properly 
        /// </summary>
        internal void PrepareViewToExport()
        {
            colSalesVol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
            colUpliftPer.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
            colUpliftDal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
            colUpliftMACO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
            colAvgMACO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;

            foreach (GridColumn col in gridView.Columns)
            {
                col.Caption = col.Caption.Replace(" ", "\n");
            }

            gridView.LevelIndent = 0;
        }

        /// <summary>
        /// Restores view state after export 
        /// </summary>
        internal void RestoreViewAfterExport()
        {
            colSalesVol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            colSalesVol.DisplayFormat.FormatString = formatDal;

            colUpliftPer.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            colUpliftPer.DisplayFormat.FormatString = formatHrnPr;

            colUpliftDal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            colUpliftDal.DisplayFormat.FormatString = formatDal;

            colUpliftMACO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            colUpliftMACO.DisplayFormat.FormatString = formatHrnPr;

            colAvgMACO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            colAvgMACO.DisplayFormat.FormatString = formatDal;

            foreach (GridColumn col in gridView.Columns)
            {
                col.Caption = col.Caption.Replace("\n", " ");
            }

            gridView.LevelIndent = -1;
        }

        /// <summary>
        /// Handles the CustomDrawColumnHeader event of gridView1
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="e">The event args</param>
        private void gridView1_CustomDrawColumnHeader(object sender, DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null) return;

            e.Column.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
         
            Rectangle rect = e.Bounds;
            Brush brush =
                e.Cache.GetGradientBrush(rect, e.Column.AppearanceHeader.BackColor,
                e.Column.AppearanceHeader.BackColor2,  e.Column.AppearanceHeader.GradientMode);
            rect.Inflate(-1, -1);

            // Fill column headers with the specified colors.
            e.Graphics.FillRectangle(brush, rect);

            ControlPaint.DrawBorder3D(e.Graphics, e.Bounds);

            e.Appearance.DrawString(e.Cache, e.Info.Caption, e.Info.CaptionRect);

            // Draw the filter and sort buttons.
            foreach (DevExpress.Utils.Drawing.DrawElementInfo info in e.Info.InnerElements)
            {
                DevExpress.Utils.Drawing.ObjectPainter.DrawObject(e.Cache, info.ElementPainter,
                    info.ElementInfo);
            }

            e.Handled = true;
        }

        /// <summary>
        /// Handles the CustomDrawGroupRow event of gridView1
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="e">The event args</param>
        private void gridView1_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;
            DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;

            string value = string.Empty;
            string caption = info.Column.Caption + ": " + info.EditValue.ToString();

            GridGroupRowInfo groupRowInfo = e.Info as GridGroupRowInfo;
            Rectangle groupRowBounds = groupRowInfo.DataBounds;
            Rectangle expandButtonBounds = groupRowInfo.ButtonBounds;
            Rectangle textBounds = e.Bounds;
            textBounds.X = expandButtonBounds.Right + 4;

            //A brush for the group row.
            Brush brush = e.Appearance.GetBackBrush(e.Cache, groupRowBounds);

            //A brush for the region containing the expand button.
            Brush brushImage = e.Appearance.GetBackBrush(e.Cache, expandButtonBounds);

            //Brushes to draw the text in the group row
            Brush brushText = e.Appearance.GetForeBrush(e.Cache),
            brushTextShadow = e.Appearance.GetForeBrush(e.Cache);
            if (e.RowHandle == view.FocusedRowHandle)
            {
                brush = brushTextShadow = Brushes.DarkBlue;
                brushText = Brushes.White;
            }

            //Fill the rectangle of the group row without the expand button
            e.Graphics.FillRectangle(brush, groupRowBounds);

            //Draw a custom expand button
            DetailButtonObjectInfoArgs args = new DetailButtonObjectInfoArgs(groupRowInfo.ButtonBounds, groupRowInfo.GroupExpanded, true);
            args.Graphics = e.Graphics;
            SkinGridDetailButtonPainter p = new SkinGridDetailButtonPainter(view);
            p.DrawObject(args);

            //Draw the group row text
            e.Appearance.DrawString(e.Cache, caption, textBounds, brushText);

            //Draw values
            if (!view.GetRowExpanded(e.RowHandle))
            {
                foreach (DevExpress.XtraGrid.GridGroupSummaryItem g in view.GroupSummary)
                {
                    if (g.ShowInGroupColumnFooter == null)
                    {
                        value = view.GetGroupSummaryDisplayText(e.RowHandle, g);

                        GridColumn col = view.Columns[g.FieldName];

                        textBounds.X = viewInfo.ColumnsInfo[col].Bounds.X;
                        textBounds.Width = viewInfo.ColumnsInfo[col].Bounds.Width - 4;
                        e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        e.Appearance.DrawString(e.Cache, value, textBounds, brushText);
                    }
                }
            }

            //Prevent default painting
            e.Handled = true;
        }

        /// <summary>
        /// Handles the CustomSummaryCalculate event of gridView1
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="e">The event args</param>
        private void gridView1_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
            // Get the summary ID. 
            int summaryID = ConvertEx.ToInt((e.Item as GridSummaryItem).Tag);
            GridView View = sender as GridView;

            // Initialization 
            if (e.SummaryProcess == CustomSummaryProcess.Start)
            {
                sumProdUpliftPer = 0;
                sumUpliftPer = 0;
                sumProdAvgMACO = 0;
                sumAvgMACO = 0;
                groupName = string.Empty;
            }

            // Calculation 
            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                switch (summaryID)
                {
                    case 1: // The total summary Uplift, %
                        sumProdUpliftPer += ConvertEx.ToDouble(e.FieldValue) * ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVolBase));
                        sumUpliftPer += ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVolBase));
                        break;
                    case 2: // The group summary Uplift, % 
                        sumProdUpliftPer += ConvertEx.ToDouble(e.FieldValue) * ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVolBase));
                        sumUpliftPer += ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVolBase));
                        break;
                    case 4: // The group summary Region name. 
                        string region = (string)View.GetRowCellValue(e.RowHandle, colRegion);
                        if ( string.Compare(groupName, region) < 0) groupName = region;
                        break;
                    case 5: // The group summary M3 name. 
                        string name = (string)View.GetRowCellValue(e.RowHandle, colM3);
                        if ( string.Compare(groupName, name) < 0) groupName = name;
                        break;
                    case 6: // The total summary AvgMACO
                        sumProdAvgMACO += ConvertEx.ToDouble(e.FieldValue) * ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVol));
                        sumAvgMACO += ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVol));
                        break;
                    case 7: // The group summary AvgMACO
                        sumProdAvgMACO += ConvertEx.ToDouble(e.FieldValue) * ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVol));
                        sumAvgMACO += ConvertEx.ToDouble(View.GetRowCellValue(e.RowHandle, colSalesVol));
                        break;
                }
            }

            // Finalization 
            if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            {
                switch (summaryID)
                {
                    case 1: // The total summary Uplift, %
                        e.TotalValue = sumProdUpliftPer / sumUpliftPer;
                        break;
                    case 2: // The group summary Uplift, % 
                        e.TotalValue = sumProdUpliftPer / sumUpliftPer;
                        break;
                    case 3: // The group summary Region / M3 name. 
                        if (e.GroupLevel == 1)
                        {
                            e.TotalValue = "M3";
                        }
                        else
                        {
                            e.TotalValue = "Регион";
                        }
                        break;
                    case 4:  // The group summary Region name. 
                        if (e.GroupLevel == 1)
                        {
                            e.TotalValue = string.Empty;
                        }
                        else
                        {
                            e.TotalValue = groupName;
                        }
                        break;
                    case 5: // The group summary M3 name.
                        if (e.GroupLevel == 1)
                        {
                            e.TotalValue = groupName;
                        }
                        else
                        {
                            e.TotalValue = string.Empty;
                        }
                        break;
                    case 6: // The total summary AvgMACO
                        e.TotalValue = sumProdAvgMACO / sumAvgMACO;
                        break;
                    case 7: // The group summary AvgMACO
                        e.TotalValue = sumProdAvgMACO / sumAvgMACO;
                        break;
                }
            }      
        }
     }
}
