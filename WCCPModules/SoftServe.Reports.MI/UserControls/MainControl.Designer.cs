﻿namespace SoftServe.Reports.MI.UserControls
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInitGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffChanel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalesVol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpliftPer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpliftDal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpliftMACO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgMACO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalesVolBase = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(900, 500);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 60;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInitGroup,
            this.colInit,
            this.colRegion,
            this.colM3,
            this.colStaffChanel,
            this.colYear,
            this.colMonth,
            this.colSalesVol,
            this.colUpliftPer,
            this.colUpliftDal,
            this.colUpliftMACO,
            this.colAvgMACO,
            this.colSalesVolBase});
            this.gridView.GridControl = this.gridControl;
            this.gridView.GroupCount = 2;
            this.gridView.GroupFormat = "{0}: {1}";
            this.gridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SalesDal_Promo", this.colSalesVol, "{0:N3}", ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftDal", this.colUpliftDal, "{0:N3}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftMACO", this.colUpliftMACO, "{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "AvgMACO", this.colAvgMACO, "{0:N3}", "7"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Max, "Year", this.colYear, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Max, "Month", this.colMonth, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Max, "ChanelType", this.colStaffChanel, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "", this.colInitGroup, "Subtotal {0}:", "3"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "UpliftPct", null, "{0:N2}", "2"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftDal", null, "{0:N3}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftMACO", null, "{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "AvgMACO", null, "{0:N3}", "7"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SalesDal_Promo", null, "{0:N3}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "UpliftPct", this.colUpliftPer, "{0:N2}", "2"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Region_name", this.colRegion, "{0}", "4"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "DSM_Name", this.colM3, "{0}", "5")});
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowFooter = true;
            this.gridView.OptionsView.ShowGroupedColumns = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegion, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colM3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.gridView1_CustomDrawColumnHeader);
            this.gridView.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridView1_CustomDrawGroupRow);
            this.gridView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.gridView1_CustomSummaryCalculate);
            // 
            // colInitGroup
            // 
            this.colInitGroup.AppearanceHeader.BackColor = System.Drawing.Color.Khaki;
            this.colInitGroup.AppearanceHeader.BackColor2 = System.Drawing.Color.Goldenrod;
            this.colInitGroup.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colInitGroup.AppearanceHeader.Options.UseBackColor = true;
            this.colInitGroup.Caption = "Группа инициативы";
            this.colInitGroup.FieldName = "InitiativeGroup";
            this.colInitGroup.MinWidth = 75;
            this.colInitGroup.Name = "colInitGroup";
            this.colInitGroup.SummaryItem.DisplayFormat = "Total country:";
            this.colInitGroup.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.colInitGroup.Visible = true;
            this.colInitGroup.VisibleIndex = 0;
            this.colInitGroup.Width = 134;
            // 
            // colInit
            // 
            this.colInit.AppearanceHeader.BackColor = System.Drawing.Color.Khaki;
            this.colInit.AppearanceHeader.BackColor2 = System.Drawing.Color.Goldenrod;
            this.colInit.AppearanceHeader.Options.UseBackColor = true;
            this.colInit.Caption = "Инициатива";
            this.colInit.FieldName = "Initiative";
            this.colInit.MinWidth = 75;
            this.colInit.Name = "colInit";
            this.colInit.Visible = true;
            this.colInit.VisibleIndex = 1;
            this.colInit.Width = 85;
            // 
            // colRegion
            // 
            this.colRegion.AppearanceHeader.BackColor = System.Drawing.Color.Khaki;
            this.colRegion.AppearanceHeader.BackColor2 = System.Drawing.Color.Goldenrod;
            this.colRegion.AppearanceHeader.Options.UseBackColor = true;
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "Region_name";
            this.colRegion.MinWidth = 75;
            this.colRegion.Name = "colRegion";
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 2;
            this.colRegion.Width = 80;
            // 
            // colM3
            // 
            this.colM3.AppearanceHeader.BackColor = System.Drawing.Color.Khaki;
            this.colM3.AppearanceHeader.BackColor2 = System.Drawing.Color.Goldenrod;
            this.colM3.AppearanceHeader.Options.UseBackColor = true;
            this.colM3.Caption = "M3";
            this.colM3.FieldName = "DSM_Name";
            this.colM3.MinWidth = 75;
            this.colM3.Name = "colM3";
            this.colM3.Visible = true;
            this.colM3.VisibleIndex = 3;
            this.colM3.Width = 80;
            // 
            // colStaffChanel
            // 
            this.colStaffChanel.AppearanceHeader.BackColor = System.Drawing.Color.Khaki;
            this.colStaffChanel.AppearanceHeader.BackColor2 = System.Drawing.Color.Goldenrod;
            this.colStaffChanel.AppearanceHeader.Options.UseBackColor = true;
            this.colStaffChanel.Caption = "Канал персонала M3";
            this.colStaffChanel.FieldName = "ChanelType";
            this.colStaffChanel.MinWidth = 40;
            this.colStaffChanel.Name = "colStaffChanel";
            this.colStaffChanel.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Max;
            this.colStaffChanel.Visible = true;
            this.colStaffChanel.VisibleIndex = 4;
            // 
            // colYear
            // 
            this.colYear.AppearanceHeader.BackColor = System.Drawing.Color.Khaki;
            this.colYear.AppearanceHeader.BackColor2 = System.Drawing.Color.Goldenrod;
            this.colYear.AppearanceHeader.Options.UseBackColor = true;
            this.colYear.Caption = "Год";
            this.colYear.FieldName = "Year";
            this.colYear.MinWidth = 40;
            this.colYear.Name = "colYear";
            this.colYear.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Max;
            this.colYear.Visible = true;
            this.colYear.VisibleIndex = 5;
            // 
            // colMonth
            // 
            this.colMonth.AppearanceHeader.BackColor = System.Drawing.Color.Khaki;
            this.colMonth.AppearanceHeader.BackColor2 = System.Drawing.Color.Goldenrod;
            this.colMonth.AppearanceHeader.Options.UseBackColor = true;
            this.colMonth.Caption = "Месяц";
            this.colMonth.FieldName = "Month";
            this.colMonth.MinWidth = 40;
            this.colMonth.Name = "colMonth";
            this.colMonth.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Max;
            this.colMonth.Visible = true;
            this.colMonth.VisibleIndex = 6;
            // 
            // colSalesVol
            // 
            this.colSalesVol.AppearanceHeader.BackColor = System.Drawing.Color.LightBlue;
            this.colSalesVol.AppearanceHeader.BackColor2 = System.Drawing.Color.SteelBlue;
            this.colSalesVol.AppearanceHeader.Options.UseBackColor = true;
            this.colSalesVol.Caption = "Обьем продаж промо группы";
            this.colSalesVol.DisplayFormat.FormatString = "{0:N3}";
            this.colSalesVol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSalesVol.FieldName = "SalesDal_Promo";
            this.colSalesVol.MinWidth = 40;
            this.colSalesVol.Name = "colSalesVol";
            this.colSalesVol.SummaryItem.DisplayFormat = "{0:N3}";
            this.colSalesVol.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colSalesVol.Visible = true;
            this.colSalesVol.VisibleIndex = 7;
            this.colSalesVol.Width = 80;
            // 
            // colUpliftPer
            // 
            this.colUpliftPer.AppearanceHeader.BackColor = System.Drawing.Color.LightBlue;
            this.colUpliftPer.AppearanceHeader.BackColor2 = System.Drawing.Color.SteelBlue;
            this.colUpliftPer.AppearanceHeader.Options.UseBackColor = true;
            this.colUpliftPer.Caption = "Uplift, %";
            this.colUpliftPer.DisplayFormat.FormatString = "{0:N2}";
            this.colUpliftPer.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpliftPer.FieldName = "UpliftPct";
            this.colUpliftPer.MinWidth = 40;
            this.colUpliftPer.Name = "colUpliftPer";
            this.colUpliftPer.SummaryItem.DisplayFormat = "{0:N2}";
            this.colUpliftPer.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.colUpliftPer.SummaryItem.Tag = "1";
            this.colUpliftPer.Visible = true;
            this.colUpliftPer.VisibleIndex = 8;
            // 
            // colUpliftDal
            // 
            this.colUpliftDal.AppearanceHeader.BackColor = System.Drawing.Color.LightBlue;
            this.colUpliftDal.AppearanceHeader.BackColor2 = System.Drawing.Color.SteelBlue;
            this.colUpliftDal.AppearanceHeader.Options.UseBackColor = true;
            this.colUpliftDal.Caption = "Uplift, dal";
            this.colUpliftDal.DisplayFormat.FormatString = "{0:N3}";
            this.colUpliftDal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpliftDal.FieldName = "UpliftDal";
            this.colUpliftDal.MinWidth = 40;
            this.colUpliftDal.Name = "colUpliftDal";
            this.colUpliftDal.SummaryItem.DisplayFormat = "{0:N3}";
            this.colUpliftDal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colUpliftDal.Visible = true;
            this.colUpliftDal.VisibleIndex = 9;
            // 
            // colUpliftMACO
            // 
            this.colUpliftMACO.AppearanceHeader.BackColor = System.Drawing.Color.LightGreen;
            this.colUpliftMACO.AppearanceHeader.BackColor2 = System.Drawing.Color.SeaGreen;
            this.colUpliftMACO.AppearanceHeader.Options.UseBackColor = true;
            this.colUpliftMACO.Caption = "Uplift MACO, грн";
            this.colUpliftMACO.DisplayFormat.FormatString = "{0:N2}";
            this.colUpliftMACO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpliftMACO.FieldName = "UpliftMACO";
            this.colUpliftMACO.MinWidth = 40;
            this.colUpliftMACO.Name = "colUpliftMACO";
            this.colUpliftMACO.SummaryItem.DisplayFormat = "{0:N2}";
            this.colUpliftMACO.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colUpliftMACO.Visible = true;
            this.colUpliftMACO.VisibleIndex = 10;
            // 
            // colAvgMACO
            // 
            this.colAvgMACO.AppearanceHeader.BackColor = System.Drawing.Color.LightGreen;
            this.colAvgMACO.AppearanceHeader.BackColor2 = System.Drawing.Color.SeaGreen;
            this.colAvgMACO.AppearanceHeader.Options.UseBackColor = true;
            this.colAvgMACO.Caption = "Avg MACO/dal, грн";
            this.colAvgMACO.DisplayFormat.FormatString = "{0:N3}";
            this.colAvgMACO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgMACO.FieldName = "AvgMACO";
            this.colAvgMACO.MinWidth = 40;
            this.colAvgMACO.Name = "colAvgMACO";
            this.colAvgMACO.SummaryItem.DisplayFormat = "{0:N3}";
            this.colAvgMACO.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.colAvgMACO.SummaryItem.Tag = "6";
            this.colAvgMACO.Visible = true;
            this.colAvgMACO.VisibleIndex = 11;
            // 
            // colSalesVolBase
            // 
            this.colSalesVolBase.FieldName = "SalesDal_Base";
            this.colSalesVolBase.Name = "colSalesVolBase";
            this.colSalesVolBase.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.AutoScrollMinSize = new System.Drawing.Size(900, 500);
            this.xtraScrollableControl1.Controls.Add(this.gridControl);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(900, 500);
            this.xtraScrollableControl1.TabIndex = 1;
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(900, 500);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gridControl;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colInitGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colInit;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffChanel;
        private DevExpress.XtraGrid.Columns.GridColumn colYear;
        private DevExpress.XtraGrid.Columns.GridColumn colMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colSalesVol;
        private DevExpress.XtraGrid.Columns.GridColumn colUpliftPer;
        private DevExpress.XtraGrid.Columns.GridColumn colUpliftDal;
        private DevExpress.XtraGrid.Columns.GridColumn colUpliftMACO;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgMACO;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colSalesVolBase;

    }
}
