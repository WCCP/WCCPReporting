﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MI.UserControls
{
    [ToolboxItem(false)]
    public partial class RejectedPointsControl : UserControl
    {
        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public RejectedPointsControl(bool isColInitiativeVisible)
        {
            InitializeComponent();

            colInitiative.Visible = isColInitiativeVisible;
        }

        /// <summary>
        /// Gets or sets the grid datasource
        /// </summary>
        public DataTable DataSource
        {
            get
            {
                return gridControl.DataSource as DataTable;
            }

            set
            {
                gridControl.BeginUpdate();
                gridControl.DataSource = value;
                gridControl.EndUpdate();
            }
        }

        /// <summary>
        /// Changes view in order to export it properly 
        /// </summary>
        internal void PrepareViewToExport()
        {
            foreach (GridColumn col in gridView.Columns)
            {
                col.Caption = col.Caption.Replace(" ", "\n");
            }
        }

        /// <summary>
        /// Restores view state after export 
        /// </summary>
        internal void RestoreViewAfterExport()
        {
            foreach (GridColumn col in gridView.Columns)
            {
                col.Caption = col.Caption.Replace("\n", " ");
            }
        }

        /// <summary>
        /// Handles the CustomDisplayText event of repositoryTextEdit
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="e">The event args</param>
        private void repositoryTextEdit_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            e.DisplayText = ConvertEx.ToBool(e.Value) ? Resource.controlGroupTT : Resource.promomGroupTT;   
        }
     }
}
