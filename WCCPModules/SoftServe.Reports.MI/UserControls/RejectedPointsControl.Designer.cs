﻿namespace SoftServe.Reports.MI.UserControls
{
    partial class RejectedPointsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOLId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsControl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInitiative = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOLTradingName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSkipReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTextEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.AutoScrollMinSize = new System.Drawing.Size(900, 500);
            this.xtraScrollableControl1.Controls.Add(this.gridControl);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(900, 500);
            this.xtraScrollableControl1.TabIndex = 1;
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTextEdit});
            this.gridControl.Size = new System.Drawing.Size(900, 500);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 40;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOLId,
            this.colIsControl,
            this.colRegion,
            this.colM3,
            this.colInitiative,
            this.colOLTradingName,
            this.colSkipReason});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowFooter = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            // 
            // colOLId
            // 
            this.colOLId.Caption = "Код ТТ";
            this.colOLId.FieldName = "OL_ID";
            this.colOLId.MaxWidth = 150;
            this.colOLId.MinWidth = 50;
            this.colOLId.Name = "colOLId";
            this.colOLId.SummaryItem.DisplayFormat = "Количество ТТ:";
            this.colOLId.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.colOLId.Visible = true;
            this.colOLId.VisibleIndex = 0;
            this.colOLId.Width = 90;
            // 
            // colIsControl
            // 
            this.colIsControl.Caption = "Промо/Контрольная";
            this.colIsControl.ColumnEdit = this.repositoryTextEdit;
            this.colIsControl.FieldName = "isCnt";
            this.colIsControl.MaxWidth = 150;
            this.colIsControl.MinWidth = 50;
            this.colIsControl.Name = "colIsControl";
            this.colIsControl.Visible = true;
            this.colIsControl.VisibleIndex = 1;
            this.colIsControl.Width = 130;
            // 
            // repositoryTextEdit
            // 
            this.repositoryTextEdit.AutoHeight = false;
            this.repositoryTextEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryTextEdit.Name = "repositoryTextEdit";
            this.repositoryTextEdit.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.repositoryTextEdit_CustomDisplayText);
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "Region_name";
            this.colRegion.MaxWidth = 250;
            this.colRegion.MinWidth = 50;
            this.colRegion.Name = "colRegion";
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 2;
            this.colRegion.Width = 130;
            // 
            // colM3
            // 
            this.colM3.Caption = "M3";
            this.colM3.FieldName = "DSM_Name";
            this.colM3.MaxWidth = 300;
            this.colM3.MinWidth = 90;
            this.colM3.Name = "colM3";
            this.colM3.Visible = true;
            this.colM3.VisibleIndex = 3;
            this.colM3.Width = 130;
            // 
            // colInitiative
            // 
            this.colInitiative.Caption = "Владелец бюджета";
            this.colInitiative.FieldName = "Initiative";
            this.colInitiative.MaxWidth = 250;
            this.colInitiative.MinWidth = 80;
            this.colInitiative.Name = "colInitiative";
            this.colInitiative.Visible = true;
            this.colInitiative.VisibleIndex = 5;
            this.colInitiative.Width = 130;
            // 
            // colOLTradingName
            // 
            this.colOLTradingName.Caption = "Фактическое название точки";
            this.colOLTradingName.FieldName = "OLTradingName";
            this.colOLTradingName.MaxWidth = 400;
            this.colOLTradingName.MinWidth = 100;
            this.colOLTradingName.Name = "colOLTradingName";
            this.colOLTradingName.Visible = true;
            this.colOLTradingName.VisibleIndex = 4;
            this.colOLTradingName.Width = 130;
            // 
            // colSkipReason
            // 
            this.colSkipReason.Caption = "Причина удаления ТТ из расчета";
            this.colSkipReason.FieldName = "SkipReason";
            this.colSkipReason.MinWidth = 80;
            this.colSkipReason.Name = "colSkipReason";
            this.colSkipReason.SummaryItem.FieldName = "OL_ID";
            this.colSkipReason.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.colSkipReason.Visible = true;
            this.colSkipReason.VisibleIndex = 6;
            this.colSkipReason.Width = 139;
            // 
            // RejectedPointsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "RejectedPointsControl";
            this.Size = new System.Drawing.Size(900, 500);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTextEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colOLId;
        private DevExpress.XtraGrid.Columns.GridColumn colIsControl;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraGrid.Columns.GridColumn colInitiative;
        private DevExpress.XtraGrid.Columns.GridColumn colOLTradingName;
        private DevExpress.XtraGrid.Columns.GridColumn colSkipReason;
        internal DevExpress.XtraGrid.GridControl gridControl;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTextEdit;

    }
}
