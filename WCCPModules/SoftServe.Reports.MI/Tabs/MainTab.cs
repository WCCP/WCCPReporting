﻿using System.Collections.Generic;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.MI.UserControls;
using System.Data;

namespace Softserve.Reports.MI.Tabs
{
    public class MainTab : XtraTabPage, IPrint
    {
        
        #region Private Properties
        private BaseReportUserControl ReportUserControl { get; set; }
        #endregion Private Properties

        #region Members
        private MainControl cont = null;
       
        #endregion

        private GridControl headerGrid = null;
        private GridView headerGridView = null;

        public MainTab(BaseReportUserControl bruc)
        {
            ReportUserControl = bruc;

            ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            Text = "Основной отчет";
        }

        #region IPrint Members

        public CompositeLink PrepareCompositeLink()
        {
            System.Windows.Forms.MessageBox.Show("Sorry. Not implemented yet");
            return new CompositeLink();
        }

        #endregion

        internal void SetSourceData(DataTable sourceData)
        {
            SetupInfo();
            cont.DataSource = sourceData;

            headerGrid = cont.gridControl;
            headerGridView = cont.gridView;
        }

        private void SetupInfo()
        {
            cont = new MainControl();
            Controls.Add(cont);
            cont.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            if (null != headerGridView)
                headerGridView.BestFitColumns();
        }

        internal CompositeLink PrepareExport(object sender, XtraTabPage selectedPage, ExportToType type)
        {
            List<IPrintable> components = new List<IPrintable>();
            CompositeLink res = null;

            if (null != headerGrid)
            {
                cont.PrepareViewToExport();
                components.Add(headerGrid);

                res = TabOperationHelper.PrepareCompositeLink(type, "", components, true);

                cont.RestoreViewAfterExport();
            }

            return res;
        }
    
    }
}
