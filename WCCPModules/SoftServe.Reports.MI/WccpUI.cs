﻿using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;
using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccp_mi.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Shows the UI.
        /// </summary>
        /// <param name="isSkined">The is skined.</param>
        /// <param name="reportCaption">The report caption.</param>
        /// <returns></returns>
        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                _reportCaption = reportCaption;
                _reportControl = new WccpUIControl(reportId);

                return _reportControl.ReportInit();
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        /// <summary>
        /// Closes the UI.
        /// </summary>
        public void CloseUI()
        {
        }

        public bool AllowClose()
        {
            return true;
        }
    }
}
