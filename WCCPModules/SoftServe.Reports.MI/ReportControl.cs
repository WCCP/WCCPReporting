﻿using System.Data;
using System.Windows.Forms;
using DevExpress.XtraPrintingLinks;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;
using Softserve.Reports.MI;
using Softserve.Reports.MI.Tabs;
using SoftServe.Reports.MI;
using SoftServe.Reports.MI.Forms;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraEditors;
using System;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        SettingsForm settingsForm = null;

        ReportInfo info;

        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            SetExportType(ExportToType.Pdf, true);
            SetExportType(ExportToType.Rtf, false);
            this.MenuButtonsRendering += new MenuButtonsRenderingHandler(WccpUIControl_MenuButtonsRendering);

            info = new ReportInfo();
            settingsForm = new SettingsForm(this);
            settingsForm.LoadSettingsInfo(info);
            
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
            this.PrepareExport += new PrepareExportMenuClickHandler(WccpUIControl_PrepareExport);
            this.RefreshClick += new MenuClickHandler(WccpUIControl_RefreshClick);
        }

        public int ReportInit()
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                info = settingsForm.GetSettingsInfo();
                ShowReport(true);

                return 0;
            }

            return 1;
        }

        void WccpUIControl_MenuButtonsRendering(object sender, BaseReportUserControl.MenuButtonsRenderingEventArgs eventArgs)
        {
            eventArgs.ShowExportAllBtn = false;
            eventArgs.ShowPrintAllBtn = false;
            eventArgs.ShowPrintBtn = false;

            if (eventArgs.SelectedPage is RejectedPointsTab)
            {
                eventArgs.ShowExportBtn = false;
            }
        }

        void WccpUIControl_RefreshClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
             ShowReport(true);
        }

        CompositeLink WccpUIControl_PrepareExport(object sender, DevExpress.XtraTab.XtraTabPage selectedPage, ExportToType type)
        {
            CompositeLink result = null;

            if (selectedPage is MainTab)
            {
                MainTab tab = selectedPage as MainTab;
                if (null != tab)
                    result = tab.PrepareExport(sender, selectedPage, type);
            }

            if (selectedPage is RejectedPointsTab)
            {
                RejectedPointsTab tab = selectedPage as RejectedPointsTab;
                if (null != tab)
                    result = tab.PrepareExport(sender, selectedPage, type);
            }

            return result;
        }

        void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            settingsForm.LoadSettingsInfo(info);

            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                info = settingsForm.GetSettingsInfo();
                ShowReport(true);
            }
        }
        
        private void ShowReport(bool refresh)
        {
           while (tabManager.TabPages.Count > 0)
                tabManager.TabPages.RemoveAt(0);
        
            if (refresh)
            {
                DataAccessLayer.OpenConnection();

                try
                {
                    WaitManager.StartWait();

                    DataSet sourceData = DataProvider.GetMainReportData(ConvertEx.ToInt(info.Type), info.BaseDateStart, info.BaseDateEnd, info.PromoDate, ConvertEx.ToInt(info.Chanel), info.AimId, info.IdList, info.UseBasketsInUpliftCalc);

                    if (sourceData.Tables.Count == 1)// there is no control group, we show warning
                    {
                        DataAccessLayer.CloseConnection();
                        WaitManager.StopWait();

                        string mess = ConvertEx.ToString(sourceData.Tables[0].Rows[0][0]);
                        XtraMessageBox.Show(mess, Resource.Warning, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        if (settingsForm.ShowDialog() == DialogResult.OK)
                        {
                            info = settingsForm.GetSettingsInfo();
                            ShowReport(true);
                        }

                        return;
                    }

                    MainTab mainTab = new MainTab(this);
                    tabManager.TabPages.Add(mainTab);

                    RejectedPointsTab rejectedTab = new RejectedPointsTab(this, info.Type);
                    tabManager.TabPages.Add(rejectedTab);

                    if (sourceData.Tables.Count == 0)// no output on the currrent input info - just in case ;)
                    {
                        mainTab.SetSourceData(null);
                        rejectedTab.SetSourceData(null);
                    }

                    if(sourceData.Tables.Count == 3)// there is an apropriate output
                    {
                        mainTab.SetSourceData(sourceData.Tables[0]);
                        rejectedTab.SetSourceData(sourceData.Tables[1]);

                        if (sourceData.Tables[2].Rows != null && sourceData.Tables[2].Rows.Count > 0)
                        {
                            M3ListForm M3list = new M3ListForm(sourceData.Tables[2]);
                            M3list.ShowDialog();
                        }
                    }
                }
                finally
                {
                    DataAccessLayer.CloseConnection();
                    WaitManager.StopWait();
                }
            }
            else
            {
                MainTab tab = new MainTab(this);
                tabManager.TabPages.Add(tab);
            }
        }
    }
}
