﻿namespace SoftServe.Reports.MI
{
    /// <summary>
    /// Stored procedures and their params names 
    /// </summary>
    public static class Constants
    {
        #region Settings Window

        public const string SP_GET_CHANNEL_LIST = "spDW_ChannelList";

        public const string SP_GET_ACTIVITIES_LIST = "spDW_MI_GetTMActivities";
        public const string SP_GET_ACTIVITIES_LIST_PARAM_1 = "PromoDate";

        public const string SP_GET_AIMS_LIST = "spDW_MI_GetListOfAims";

        public const string SP_GET_OWNERS_LIST = "spDW_CM_BudgetOwners";

        public const string SP_SET_MACO = "spDW_MI_LoadMACO";

        public const string SP_GET_COUNTRY_FOR_CURRENT_USER = "spDW_GetCountryByUser";

        #endregion Settings Window

        #region Main report tab

        public const string SP_GET_REPORT_DATA = "spDW_MI_FlatReport";

        public const string SP_GET_REPORT_DATA_MI_Type = "MI_Type";
        public const string SP_GET_REPORT_DATA_Channel_ID = "Channel_ID";
        public const string SP_GET_REPORT_DATA_BaseLine_Begin = "BaseLine_Begin";
        public const string SP_GET_REPORT_DATA_BaseLine_End = "BaseLine_End";
        public const string SP_GET_REPORT_DATA_PromoLine_Month = "PromoLine_Month";
        public const string SP_GET_REPORT_DATA_Aim_ID = "Aim_ID";
        public const string SP_GET_REPORT_DATA_Filter_ID = "Filter_ID";
        public const string SP_GET_REPORT_DATA_UseUpliftBasket = "UseUpliftBasket";

        #endregion Main report tab
    }

    /// <summary>
    /// Report types
    /// </summary>
    public enum ReportType
    {
        /// <summary>
        /// Market program
        /// </summary>
        MarketProgram = 2,

        /// <summary>
        /// Contract management
        /// </summary>
        ContractManagement = 1
    }

    /// <summary>
    /// Staff chanels
    /// </summary>
    public enum StaffChanel
    {
        /// <summary>
        /// On-trade chanel
        /// </summary>
        On = 1,

        /// <summary>
        /// Off-trade chanel
        /// </summary>
        Off = 2,

        /// <summary>
        /// K-trade chanel
        /// </summary>
        KA = 3,

        /// <summary>
        /// On/Off-trade chanel
        /// </summary>
        OnOff = 4
    }

    /// <summary>
    /// Countries
    /// </summary>
    public enum Countries
    {
        Ukraine = 1,
        Russia = 2
    }
}
