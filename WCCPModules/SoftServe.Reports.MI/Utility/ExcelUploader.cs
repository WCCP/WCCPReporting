﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logica.Reports.BaseReportControl.CommonControls.MappingControl;
using System.IO;
using DevExpress.XtraEditors;
using SoftServe.Reports.MI.Forms;
using System.Windows.Forms;
using Logica.Reports.Common.WaitWindow;
using System.Data;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MI.Utility
{
    /// <summary>
    /// Performs file uploading 
    /// </summary>
    class ExcelUploader
    {
        #region Colunm Names
        private string colMonth = "Month";
        private string colRegion = "Region";
        private string colSKU = "SKU";
        private string colMACO = "MACO";
        private string colERP = "ERP";

        private string colRowNumber = "RowNumber";
        private string colStatus = "Status";
        private string colStatusDescr = "StatusDescr";

        private string colSelected = "Selected";
        #endregion

        /// <summary>
        /// Performs Master file uploader functions
        /// </summary>
        public void StartUploading()
        {
            string fileName = string.Empty;

            // file choosing
            try
            {
                fileName = FileChosing();
            }
            catch (IOException ex)
            {
                XtraMessageBox.Show(Resource.errorFileIsAlreadyOpen, Resource.Error);
                return;
            }

            if (fileName == string.Empty)
            {
                return;
            }

            // header reading
            Utility fileReader = new Utility();

            List<ComboBoxItem> reportFields = GetReportFields();

            List<ComboBoxItem> fileFields = fileReader.ReadFileHeader(fileName);

            if (fileFields == null)// error while file reading
            {
                return;
            }

            if (fileFields.Count < reportFields.Count)
            {
                XtraMessageBox.Show(Resource.errorNotEnoughColumnsInTheFile, Resource.Error);
                return;
            }

            // mapping
            int[] mapping = new int[0];
            using (ListMapping lm = new ListMapping(reportFields, fileFields))
            {
                if (lm.ShowDialog() == DialogResult.OK)
                {
                    mapping = lm.Mapping;
                }
                else
                {
                    return;
                }
            }

            // data reading
            WaitManager.StartWait();

            DataTable MACO = fileReader.ReadFileBody(fileName, reportFields, fileFields, mapping, InitMACOTable(reportFields));
            ValidateDataBiss(MACO);

            WaitManager.StopWait();

            DataView dv = MACO.DefaultView;
            dv.RowFilter = string.Format("{0} = {1}", colStatus, false);
            DataTable MACOerrors = dv.ToTable();

            if (MACOerrors.Rows.Count > 0)
            {
                using (ChooserForm sf = new ChooserForm(MACOerrors, true))
                {
                    if (sf.ShowDialog() == DialogResult.Cancel)
                    {
                        return;
                    }
                }
            }

            dv.RowFilter = string.Format("{0} = {1}", colStatus, true);
            MACO = dv.ToTable();

            using (ChooserForm cf = new ChooserForm(MACO, false))
            {
                if (cf.ShowDialog() == DialogResult.OK)
                {
                    WaitManager.StartWait();

                    dv = MACO.DefaultView;
                    dv.RowFilter = string.Format("{0} = {1}", colSelected, true);
                    DataTable MACOselected = dv.ToTable();

                    DataProvider.LoadMaco(MACOselected);

                    WaitManager.StopWait();
                }
            }
        }

        /// <summary>
        /// Performs file choosing
        /// </summary>
        /// <returns>File name</returns>
        private string FileChosing()
        {
            string fileName = string.Empty;

            string DIALOG_OPEN_FILE_FILTER = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            string DIALOG_OPEN_FILE = "Загрузить файл";

            string currentDirectory = Directory.GetCurrentDirectory();

            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = DIALOG_OPEN_FILE_FILTER;
            openFileDialog.Title = DIALOG_OPEN_FILE;
            openFileDialog.CheckFileExists = true;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;

                byte[] fileData = File.ReadAllBytes(filePath);

                if (!string.IsNullOrEmpty(filePath) && fileData != null && fileData.Length > 0)
                {
                    fileName = filePath;
                }
            }

            Directory.SetCurrentDirectory(currentDirectory);

            return fileName;
        }

        /// <summary>
        /// Collects reports fields
        /// </summary>
        /// <returns>List of the report fields</returns>
        private List<ComboBoxItem> GetReportFields()
        {
            List<ComboBoxItem> reportFields = new List<ComboBoxItem>();
            reportFields.Add(new ComboBoxItem(0, Resource.fieldMonth));
            reportFields.Add(new ComboBoxItem(1, Resource.fieldRegion));
            reportFields.Add(new ComboBoxItem(2, Resource.fieldSKU));
            reportFields.Add(new ComboBoxItem(3, Resource.fieldMACO));
            reportFields.Add(new ComboBoxItem(4, Resource.fieldERP));

            return reportFields;
        }

        /// <summary>
        /// Inits MACO table
        /// </summary>
        /// <param name="reportFields">List of the report fields</param>
        /// <returns>Inial MACO table</returns>
        private DataTable InitMACOTable(List<ComboBoxItem> reportFields)
        {
            DataTable MACO = new DataTable();

            DataColumn columnMonth = new DataColumn(colMonth, typeof(DateTime));
            MACO.Columns.Add(columnMonth);

            DataColumn columnRegion = new DataColumn(colRegion, typeof(string));
            MACO.Columns.Add(columnRegion);

            DataColumn columnSKU = new DataColumn(colSKU, typeof(string));
            MACO.Columns.Add(columnSKU);

            DataColumn columnMACO = new DataColumn(colMACO, typeof(decimal));
            MACO.Columns.Add(columnMACO);

            DataColumn columnERP = new DataColumn(colERP, typeof(uint));
            MACO.Columns.Add(columnERP);


            DataColumn columnRowNumber = new DataColumn(colRowNumber, typeof(int));
            MACO.Columns.Add(columnRowNumber);

            DataColumn columnStatus = new DataColumn(colStatus, typeof(bool));
            columnStatus.DefaultValue = true;
            MACO.Columns.Add(columnStatus);

            DataColumn columnStatusDescr = new DataColumn(colStatusDescr, typeof(string));
            MACO.Columns.Add(columnStatusDescr);

            return MACO;
        }

        /// <summary>
        /// Performs business validation
        /// </summary>
        /// <param name="dt">Table to be validated</param>
        private void ValidateDataBiss(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (ConvertEx.ToBool(row[colStatus]) && (ConvertEx.ToDecimal(row[colMACO]) < -99999999 || ConvertEx.ToDecimal(row[colMACO]) > 99999999))
                {
                    row[colStatus] = false;
                    row[colStatusDescr] = Resource.errorInCorrectDataFormat;
                }

                DateTime month = ConvertEx.ToDateTime(row[colMonth]);
                row[colMonth] = new DateTime(month.Year, month.Month, 1);
            }
        }
    }
}
