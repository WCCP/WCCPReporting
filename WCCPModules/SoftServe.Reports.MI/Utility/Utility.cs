﻿using DevExpress.XtraEditors;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Logica.Reports.BaseReportControl.CommonControls.MappingControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using System.Globalization;

namespace SoftServe.Reports.MI.Utility
{
    /// <summary>
    /// Describes the Excel reading methods
    /// </summary>
    class Utility
    {
        /// <summary>
        /// Row number of the first non-empty cell
        /// </summary>
        private int rFirst = -1;

        SpreadsheetDocument _excelFile;
        WorkbookPart _workbook;

        /// <summary>
        /// Gets the first worksheet
        /// </summary>
        /// <param name="theWorkbook">Current workbook</param>
        /// <returns></returns>
        private SheetData GetWorkSheetData(string fileName)
        {
            _excelFile = SpreadsheetDocument.Open(fileName, false);
            _workbook = _excelFile.WorkbookPart;

            Sheet lSheet = _workbook.Workbook.Descendants<Sheet>().First();

            WorksheetPart lWorkSheet = (WorksheetPart)(_workbook.GetPartById(lSheet.Id));
            SheetData lSheetData = lWorkSheet.Worksheet.Elements<SheetData>().First();

            return lSheetData;
        }

        /// <summary>
        /// Reads the file header
        /// </summary>
        /// <param name="fileName">The file name</param>
        /// <returns>List of column headers</returns>
        public List<ComboBoxItem> ReadFileHeader(string fileName)
        {
            List<ComboBoxItem> fileColumns = new List<ComboBoxItem>();
            SheetData lSheetData = null;

            try
            {
                lSheetData = GetWorkSheetData(fileName);

                List<Row> rows = lSheetData.Elements<Row>().ToList();
                List<string> headers = new List<string>();

                // Read row
                int rowNum = -1;

                do
                {
                    rowNum++;
                    headers = GetRowData(rows[rowNum]);
                }
                while (IsRowEmpty(headers));

                rFirst = rowNum;

                for (int i = 0; i < headers.Count; i++)
                {
                    fileColumns.Add(new ComboBoxItem(i, headers[i]));
                }
            }
            catch (Exception e)
            {
                fileColumns = null;
                XtraMessageBox.Show(Resource.errorInCorrectFileFormat, Resource.Warning);
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            return fileColumns;
        }

        /// <summary>
        /// Reads the file 
        /// </summary>
        /// <param name="fileName">The file name</param>
        /// <param name="reportColumns">List of columns from the report</param>
        /// <param name="fileColumns">List of columns from the file</param>
        /// <param name="mapping">The mapping key</param>
        /// <param name="fileData">Data from the file</param>
        /// <returns></returns>
        public DataTable ReadFileBody(string fileName, List<ComboBoxItem> reportColumns, List<ComboBoxItem> fileColumns, int[] mapping, DataTable fileData)
        {
            SheetData lSheetData = null;

            try
            {
                lSheetData = GetWorkSheetData(fileName);
                List<Row> rows = lSheetData.Elements<Row>().ToList();

                int listCount = reportColumns.Count;
                for (int rCnt = rFirst + 1; rCnt < rows.Count; rCnt++)
                {
                    object[] row = new object[listCount + 3];
                    List<string> lCurRow = GetRowData(rows[rCnt]);

                    if (IsRowEmpty(lCurRow))
                        continue;

                    for (int j = 0; j < listCount; j++)
                    {
                        row[j] = lCurRow[fileColumns[mapping[j]].Id];
                    }

                    row[listCount] = rCnt;

                    try
                    {
                        row[0] = DateTime.FromOADate(double.Parse(Convert.ToString(row[0])));// date
                        row[3] = Convert.ToDecimal(row[3], CultureInfo.InvariantCulture);// Maco
                        row[4] = uint.Parse(Convert.ToString(row[4]));// ERP

                        fileData.Rows.Add(row);
                    }
                    catch (Exception e)
                    {
                        for (int j = 0; j < listCount; j++)
                        {
                            row[j] = null;
                        }

                        row[listCount + 1] = false;
                        row[listCount + 2] = Resource.errorInCorrectDataFormat;

                        fileData.Rows.Add(row);
                    }
                }

            }
            catch (Exception e)
            {
                fileColumns = null;
                XtraMessageBox.Show(Resource.errorInCorrectFileFormat, Resource.Warning);
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            return fileData;
        }

        private void FreeResourses()
        {            
                if (_workbook != null)
                {
                    ReleaseObject(_workbook);
                }

                _excelFile.Close();
                ReleaseObject(_excelFile);
        }

        /// <summary>
        /// Released resources
        /// </summary>
        /// <param name="obj">Object to be released</param>
        private static void ReleaseObject(object obj)
        {
            if (obj != null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private bool IsRowEmpty(List<string> row)
        {
            bool isNotEmpty = row.Any(v => !string.IsNullOrEmpty(v));

            return !isNotEmpty;
        }

        private string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                // ASCII 'A' = 65
                int current = i == 0 ? letter - 65 : letter - 64;
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        /// <summary>
        /// Handles row reading cell by cell, cops with gaps in data
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)//empty cells
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private List<string> GetRowData(Row row)
        {
            var dataRow = new List<string>();

            var cellEnumerator = GetExcelCellEnumerator(row);
            while (cellEnumerator.MoveNext())
            {
                var cell = cellEnumerator.Current;
                var text = ReadExcelCell(cell, _workbook).Trim();

                dataRow.Add(text);
            }

            return dataRow;
        }

        private string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }
    }
}
