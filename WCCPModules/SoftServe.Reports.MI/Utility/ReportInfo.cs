﻿using System;
using System.Collections.Generic;

namespace SoftServe.Reports.MI
{
    /// <summary>
    /// Describes the report settings
    /// </summary>
    internal class ReportInfo
    {
        /// <summary>
        /// Gets or sets the type of the report
        /// </summary>
        public ReportType Type { get; set; }

        /// <summary>
        /// Gets or sets the staff chanel
        /// </summary>
        public StaffChanel Chanel { get; set; }

        /// <summary>
        /// Gets or sets the promo date
        /// </summary>
        public DateTime PromoDate { get; set; }

        /// <summary>
        /// Gets or sets the base start date
        /// </summary>
        public DateTime BaseDateStart { get; set; }

        /// <summary>
        /// Gets or sets the base end date
        /// </summary>
        public DateTime BaseDateEnd { get; set; }

        /// <summary>
        /// Gets or sets the activity Id
        /// </summary>
        public int ActivityId { get; set; }

        /// <summary>
        /// Gets or sets the aim Id
        /// </summary>
        public int AimId { get; set; }

        /// <summary>
        /// Gets or sets the contract owners Ids
        /// </summary>
        public List<int> ContractOwnerIds { get; set; }

        /// <summary>
        /// Gets or sets whether baskets should patricipate in calculations 
        /// </summary>
        public bool UseBasketsInUpliftCalc { get; set; }

        /// <summary>
        /// Initializes the new item of ReportInfo class
        /// </summary>
        public ReportInfo()
        {
            Type = ReportType.MarketProgram;
            Chanel = StaffChanel.OnOff;
            
            PromoDate = DateTime.Now;

            BaseDateStart = new DateTime(DateTime.Now.Year - 1, DateTime.Now.Month, 1);
            BaseDateEnd = BaseDateStart.AddMonths(1).AddDays(-1); 

            ActivityId = -1;
            AimId = -1;
            UseBasketsInUpliftCalc = false;

            ContractOwnerIds = new List<int>();
        }

        /// <summary>
        /// Gets the string with Id list, denending of report type
        /// </summary>
        public string IdList
        {
            get 
            {
                string ids = string.Empty;

                switch (Type)
                {
                    case ReportType.MarketProgram:

                        ids = ActivityId.ToString();
                        break;

                    case ReportType.ContractManagement:

                        foreach (int id in ContractOwnerIds)
                        {
                            ids += id.ToString() + ",";
                        }

                        ids = ids.TrimEnd(',');
                        break;
                }

                return ids;
            }
        }
    }
}
