﻿using System.Data;
using System;

namespace SoftServe.Reports.MI.Utility
{
    /// <summary>
    /// Holds DB settings data
    /// </summary>
    internal class DBSettingsData
    {
        /// <summary>
        /// Activities list
        /// </summary>
        private static DataTable activities;
        
        /// <summary>
        /// Aims list
        /// </summary>
        private static DataTable aims;

        /// <summary>
        /// Contract owners list
        /// </summary>
        private static DataTable owners;

        /// <summary>
        /// Initializes the new item of DBSettingsData class
        /// </summary>
        private DBSettingsData() { }

        /// <summary>
        /// Gets the activities list
        /// </summary>
        public static DataTable Activities(DateTime promoDate, bool fullRefresh = false)
        {
            if (fullRefresh)
                {
                    activities = DataProvider.ActivitiesList(promoDate);
                }
                return activities;
        }

        /// <summary>
        /// Gets the aims list
        /// </summary>
        public static DataTable Aims
        {
            get
            {
                if (aims == null)
                {
                    aims = DataProvider.AimsList;
                }
                return aims;
            }
        }

        /// <summary>
        /// Gets the contract owners list
        /// </summary>
        public static DataTable Owners
        {
            get
            {
                if (owners == null)
                {
                    owners = DataProvider.OwnersList;
                }
                return owners;
            }
        }
    }
}
