﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Nodes;

namespace SoftServe.Reports.EquipmentInventoryTool
{
    public partial class SettingsForm : XtraForm
    {
        #region Constants

        private const string FilterByLevel = "Level = {0}";
        private const string FilterByIdList = " and {0} in ({1})";

        private const string FakeFilter = "-5";
        private const string ActiveWaveFilter = "1";

        private int _staffM3Level;// = 4;

        private int _territoryRegionLevel;// = 3;

        private int _wavesLevel;// = 6;
        private int _warehousesLevel;// = 7;

        #endregion

        private DataTable _waves;

        /// <summary>
        /// Creates the new instance od <c>SettingsForm</c>/>
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Performs gathering of settings info
        /// </summary>
        /// <returns>Current setting info</returns>
        internal ReportInfo GetSettingsData()
        {
            ReportInfo settings = new ReportInfo();

            settings.CitiesIds = tTerritory.GetCheckedAndIndeterminateColumnValues(colTerGeographyId.FieldName, "Level", 5);

            settings.StaffingIdsM3 = tStaffing.GetCheckedColumnValuesChildren(colStaffId.FieldName, colStaffLevel.FieldName, _staffM3Level);

            settings.WarehousesLocationIds = cluWarehouses.SelectedValues(colWFieldId.FieldName);

            settings.WaveId = string.IsNullOrEmpty(gluWaves.Text) ? default(int?) : (int)gluWaves.EditValue;

            settings.ShowNonInvent = cShowUnInvent.Checked;

            return settings;
        }

        /// <summary>
        /// Assigns datasources to contols
        /// </summary>
        private void InitDataSources()
        {
            tTerritory.LoadDataSource(DataProvider.GetTerritoryTree(out _territoryRegionLevel));

            cluWarehouses.LoadDataSource(DataProvider.GetWarehouseTree(out _warehousesLevel));

            _waves = DataProvider.GetWavesList(out _wavesLevel);
            gluWaves.Properties.DataSource = _waves.Copy();

            cbInventType.LoadDataSource(DataProvider.GetWaveTypesList());
        }

        /// <summary>
        /// Sets initial settings
        /// </summary>
        private void SetInitialSettings()
        {
            tTerritory.CheckNodes(CheckState.Checked);

            FilterWarehousesByTerritory();
            cluWarehouses.SelectAll();

            cbInventType.SelectAll();

            FilterWaves();
            SelectInitWave();
        }

        /// <summary>
        /// Performs active wave selection
        /// </summary>
        private void SelectInitWave()
        {
            DataTable lWaves = gluWaves.Properties.DataSource as DataTable;

            if (lWaves != null)
            {
                int lWaveId = -1;
                foreach (DataRow row in lWaves.Rows)
                {
                    if ((DateTime)row[colDateFrom.FieldName] <= DateTime.Now && (DateTime)row[colDateTo.FieldName] >= DateTime.Now)
                    {
                        lWaveId = (int)row[gluWaves.Properties.ValueMember];
                        break;
                    }
                }

                if (lWaveId != -1)
                {
                    gluWaves.EditValue = Convert.ToInt32(lWaveId);
                    tStaffing.CheckNodes(CheckState.Checked);
                }
                else
                {
                    tStaffing.LoadDataSource(DataProvider.GetStaffingTree(DateTime.Now, out _staffM3Level));

                    FilterStuffingByTerritory();
                    tStaffing.CheckNodes(CheckState.Checked);
                }
            }
        }

        /// <summary>
        /// Performs filtering of Staffing, POS and Warehouses trees by checked regions
        /// </summary>
        private void FilterStuffingByTerritory(List<string> selectedStuff = null)
        {
            List<string> lTerritoryFilter = tTerritory.GetCheckedAndIndeterminateColumnValues(tTerritory.KeyFieldName, colTerLevel.FieldName, _territoryRegionLevel);

            List<string> lStaffingFilter = new List<string>();

            foreach (TreeListNode node in tStaffing.Nodes)
            {
                if (lTerritoryFilter.Contains(node[colStaffParentId.FieldName].ToString()))//colStaffId  colStaffParentId
                {
                    lStaffingFilter.Add(node[colStaffWStaffId.FieldName].ToString());
                }
            }

            if (lStaffingFilter.Count == 0)
            {
                lStaffingFilter.Add(FakeFilter);
            }

            if (selectedStuff == null)
            {
                List<string> lSelectedStaff = tStaffing.GetCheckedColumnValuesAll(tStaffing.KeyFieldName);

                tStaffing.FilterTree(lStaffingFilter);

                tStaffing.CheckNodes(lSelectedStaff, CheckState.Checked);
            }
            else
            {
                tStaffing.FilterTree(lStaffingFilter);
                tStaffing.CheckNodes(selectedStuff, CheckState.Checked);
            }
        }

        /// <summary>
        /// Perfoms filtering of warehouses by territory
        /// </summary>
        private void FilterWarehousesByTerritory()
        {
            string lFilter = string.Format(FilterByLevel, _warehousesLevel);

            //by territory
            string regionFilter = tTerritory.GetCheckedColumnValuesAllList(colTerGeographyId.FieldName);

            if (!string.IsNullOrEmpty(regionFilter))//there is selected territory
            {
                lFilter += string.Format(FilterByIdList, colWaveParentId.FieldName, regionFilter);
            }

            List<string> lWarehouses = cluWarehouses.SelectedIds();
            //apply filter
            cluWarehouses.FilterList(lFilter);

            cluWarehouses.SetComboBoxCheckedValues(lWarehouses);
        }

        /// <summary>
        /// Perfoms filtering of warehouses by territory
        /// </summary>
        private void FilterWaves()
        {
            string lFilter = string.Format(FilterByLevel, _wavesLevel);

            //by territory
            string regionFilter = tTerritory.GetCheckedColumnValuesAllList(colTerGeographyId.FieldName);

            if (!string.IsNullOrEmpty(regionFilter))//there are selected regions
            {
                lFilter += string.Format(FilterByIdList, colWaveParentId.FieldName, regionFilter);
            }

            //by invent type
            string inventTypeFilter = cbInventType.GetComboBoxCheckedValuesList();

            if (!string.IsNullOrEmpty(inventTypeFilter))//there are selected invent type
            {
                lFilter += string.Format(FilterByIdList, colInvType, inventTypeFilter);
            }

            //by active
            string activeFilter = cActive.Checked ? ActiveWaveFilter : string.Empty;

            if (!string.IsNullOrEmpty(activeFilter))//is checked
            {
                lFilter += string.Format(FilterByIdList, colIsActive, activeFilter);
            }

            //apply filter
            if (_waves != null)
            {
                DataView dataViewWaves = _waves.DefaultView;
                dataViewWaves.RowFilter = lFilter;
                gluWaves.Properties.DataSource = dataViewWaves.ToTable(true, colWInvId.FieldName, colName.FieldName,
                                                                       colDateFrom.FieldName, colDateTo.FieldName);
            }
        }

        /// <summary>
        /// Validates selected data
        /// </summary>
        /// <param name="reportInfo">Settings to be checked</param>
        /// <param name="message">Warning message</param>
        /// <returns><c>true</c> if settings are valid, <c>false</c> otherwise</returns>
        private bool AreSettingsValid(ReportInfo reportInfo, out string message)
        {
            message = string.Empty;

            message += reportInfo.CitiesIds.Count > 0 ? string.Empty : "\t- не выбрано территорию;\r\n";

            message += reportInfo.StaffingIdsM3.Count > 0 ? string.Empty : "\t- не выбрано персонал;\r\n";

            message += reportInfo.WaveId.HasValue ? string.Empty : "\t- не выбрано ни одной волны;\r\n";

            return message.Length == 0;
        }

        /// <summary>
        /// Handles Click event of generateReportButton
        /// </summary>
        /// <param name="sender">generateReportButton</param>
        /// <param name="e">EventArgs</param>
        private void generateReportButton_Click(object sender, EventArgs e)
        {
            ReportInfo settings = GetSettingsData();
            string message;

            if (AreSettingsValid(settings, out message))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Не все параметры введены:\n\r\n" + message, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Guarantees pop-up resizing on form resizing
        /// </summary>
        private void PopUp_Resize(object sender, EventArgs e)
        {
            cluWarehouses.Properties.PopupFormSize = new Size(cluWarehouses.Size.Width, 0);
        }

        /// <summary>
        /// Handles EditValueChanged event of cbInventType
        /// </summary>
        /// <param name="sender">cbInventType</param>
        /// <param name="e">Event args</param>
        private void cbInventType_EditValueChanged(object sender, EventArgs e)
        {
            FilterWaves();
        }

        /// <summary>
        /// Handles CheckedChanged event of cbInventType
        /// </summary>
        /// <param name="sender">cActive</param>
        /// <param name="e">Event args</param>
        private void cActive_CheckedChanged(object sender, EventArgs e)
        {
            FilterWaves();
        }

        /// <summary>
        /// Handles CustomDisplayText event of gluWaves
        /// </summary>
        /// <param name="sender">gluWaves</param>
        /// <param name="e">Event args</param>
        private void gluWaves_CustomDisplayText(object sender, CustomDisplayTextEventArgs e)
        {
            if (e.Value != null && !string.IsNullOrEmpty(e.Value.ToString()) && !string.IsNullOrEmpty(e.DisplayText))
            {
                string template = "{0} ({1} - {2})";

                int lCurWaveRowHandle = GetCurWaveRowHandle();

                string name = _waves.Rows[lCurWaveRowHandle].Field<string>(colName.FieldName).ToString();
                string dateFrom = _waves.Rows[lCurWaveRowHandle].Field<DateTime>(colDateFrom.FieldName).ToShortDateString();
                string dateTo = _waves.Rows[lCurWaveRowHandle].Field<DateTime>(colDateTo.FieldName).ToShortDateString();

                e.DisplayText = string.Format(template, name, dateFrom, dateTo);
            }
        }

        /// <summary>
        /// Handles EditValueChanged event of gluWaves
        /// </summary>
        /// <param name="sender">gluWaves</param>
        /// <param name="e">Event args</param>
        private void gluWaves_EditValueChanged(object sender, EventArgs e)
        {
            OnWaveChanged();
        }

        /// <summary>
        /// Handles wave selection changes
        /// </summary>
        private void OnWaveChanged()
        {
            if (!string.IsNullOrEmpty(gluWaves.Text))
            {
                DateTime waveEnd = _waves.Rows[GetCurWaveRowHandle()].Field<DateTime>(colDateTo.FieldName);

                List<string> lSelectedStaff = tStaffing.GetCheckedColumnValuesAll(tStaffing.KeyFieldName);

                tStaffing.LoadDataSource(DataProvider.GetStaffingTree(DateTime.Now > waveEnd ? waveEnd : DateTime.Now, out _staffM3Level));

                FilterStuffingByTerritory(lSelectedStaff);
            }
        }

        /// <summary>
        /// Handles AfterCheckNode event of tTerritory: performs territory dependent filtering
        /// </summary>
        private void tTerritory_AfterNodeChecked(object sender, EventArgs args)
        {
            FilterWarehousesByTerritory();
            FilterWaves();
            FilterStuffingByTerritory();
        }

        /// <summary>
        /// Forces selected list redrawing
        /// </summary>
        private void cluWarehouses_Closed(object sender, ClosedEventArgs e)
        {
            gcWarehouse.Focus();
        }

        /// <summary>
        /// Gets row handle of current wave
        /// </summary>
        /// <returns>The row handle of current wave</returns>
        private int GetCurWaveRowHandle()
        {
            int lRowHandle = -5;
            int lCurrentWaveId = Convert.ToInt32(gluWaves.EditValue);

            for (int i = 0; i < _waves.Rows.Count; i++)
            {
                if (_waves.Rows[i].Field<int>(colWInvId.FieldName) == lCurrentWaveId)
                {
                    lRowHandle = i;
                    break;
                }
            }

            return lRowHandle;
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            InitDataSources();
            SetInitialSettings();
        }
    }
}
