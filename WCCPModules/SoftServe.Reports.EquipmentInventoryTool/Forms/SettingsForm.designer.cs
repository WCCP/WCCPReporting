﻿using Logica.Reports.BaseReportControl.CommonControls;
using SoftServe.Reports.EquipmentInventoryTool.UserControls;

namespace SoftServe.Reports.EquipmentInventoryTool
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.generateReportButton = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.tTerritory = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colTerritory = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTerLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTerGeographyId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tStaffing = new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental();
            this.colStaffing = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffWStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cluWarehouses = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.userControl21View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExternalCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWareParentId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWFieldId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcWave = new DevExpress.XtraEditors.GroupControl();
            this.gluWaves = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaveParentId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWInvId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cActive = new DevExpress.XtraEditors.CheckEdit();
            this.cbInventType = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.cShowUnInvent = new DevExpress.XtraEditors.CheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.component1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlpGeneral = new System.Windows.Forms.TableLayoutPanel();
            this.gcStuffing = new DevExpress.XtraEditors.GroupControl();
            this.gcTerritory = new DevExpress.XtraEditors.GroupControl();
            this.gcWarehouse = new DevExpress.XtraEditors.GroupControl();
            this.gcInvType = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluWarehouses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWave)).BeginInit();
            this.gcWave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gluWaves.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbInventType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cShowUnInvent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).BeginInit();
            this.tlpGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcStuffing)).BeginInit();
            this.gcStuffing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerritory)).BeginInit();
            this.gcTerritory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcWarehouse)).BeginInit();
            this.gcWarehouse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcInvType)).BeginInit();
            this.gcInvType.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // generateReportButton
            // 
            this.generateReportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.generateReportButton.Image = global::SoftServe.Reports.EquipmentInventoryTool.Properties.Resources.check;
            this.generateReportButton.Location = new System.Drawing.Point(449, 499);
            this.generateReportButton.Name = "generateReportButton";
            this.generateReportButton.Size = new System.Drawing.Size(146, 23);
            this.generateReportButton.TabIndex = 0;
            this.generateReportButton.Text = "Сгенерировать отчет";
            this.generateReportButton.Click += new System.EventHandler(this.generateReportButton_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Image = global::SoftServe.Reports.EquipmentInventoryTool.Properties.Resources.close;
            this.simpleButton2.Location = new System.Drawing.Point(601, 499);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(86, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Отмена";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Location = new System.Drawing.Point(68, 37);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(0, 13);
            this.lblUpd2.TabIndex = 22;
            // 
            // tTerritory
            // 
            this.tTerritory.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colTerritory,
            this.colTerLevel,
            this.colTerGeographyId});
            this.tTerritory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tTerritory.KeyFieldName = "GeographyID";
            this.tTerritory.Location = new System.Drawing.Point(2, 21);
            this.tTerritory.Name = "tTerritory";
            this.tTerritory.OptionsBehavior.Editable = false;
            this.tTerritory.Size = new System.Drawing.Size(325, 363);
            this.tTerritory.TabIndex = 77;
            this.tTerritory.AfterNodeChecked += new Logica.Reports.BaseReportControl.CommonControls.TreeListIncremental.NodeChecked(this.tTerritory_AfterNodeChecked);
            // 
            // colTerritory
            // 
            this.colTerritory.Caption = "Территория";
            this.colTerritory.CustomizationCaption = "Территория";
            this.colTerritory.FieldName = "GeographyName";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsColumn.AllowEdit = false;
            this.colTerritory.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colTerritory.OptionsColumn.ReadOnly = true;
            this.colTerritory.Visible = true;
            this.colTerritory.VisibleIndex = 0;
            // 
            // colTerLevel
            // 
            this.colTerLevel.Caption = "Level";
            this.colTerLevel.FieldName = "Level";
            this.colTerLevel.Name = "colTerLevel";
            this.colTerLevel.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colTerLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colTerGeographyId
            // 
            this.colTerGeographyId.Caption = "GeographyID";
            this.colTerGeographyId.FieldName = "GeographyID";
            this.colTerGeographyId.Name = "colTerGeographyId";
            this.colTerGeographyId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colTerGeographyId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // tStaffing
            // 
            this.tStaffing.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colStaffing,
            this.colStaffId,
            this.colStaffLevel,
            this.colStaffParentId,
            this.colStaffWStaffId});
            this.tStaffing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tStaffing.KeyFieldName = "wStaffID";
            this.tStaffing.Location = new System.Drawing.Point(2, 21);
            this.tStaffing.Name = "tStaffing";
            this.tStaffing.OptionsBehavior.Editable = false;
            this.tStaffing.ParentFieldName = "wParentID";
            this.tStaffing.Size = new System.Drawing.Size(339, 303);
            this.tStaffing.TabIndex = 78;
            // 
            // colStaffing
            // 
            this.colStaffing.Caption = "Персонал";
            this.colStaffing.CustomizationCaption = "Персонал";
            this.colStaffing.FieldName = "Name";
            this.colStaffing.Name = "colStaffing";
            this.colStaffing.OptionsColumn.AllowEdit = false;
            this.colStaffing.OptionsColumn.AllowMove = false;
            this.colStaffing.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffing.OptionsColumn.ReadOnly = true;
            this.colStaffing.Visible = true;
            this.colStaffing.VisibleIndex = 0;
            // 
            // colStaffId
            // 
            this.colStaffId.Caption = "StaffId";
            this.colStaffId.FieldName = "StaffID";
            this.colStaffId.Name = "colStaffId";
            this.colStaffId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colStaffLevel
            // 
            this.colStaffLevel.Caption = "Level";
            this.colStaffLevel.FieldName = "Level";
            this.colStaffLevel.Name = "colStaffLevel";
            this.colStaffLevel.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colStaffParentId
            // 
            this.colStaffParentId.Caption = "ParentId";
            this.colStaffParentId.FieldName = "wParentID";
            this.colStaffParentId.Name = "colStaffParentId";
            this.colStaffParentId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffParentId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colStaffWStaffId
            // 
            this.colStaffWStaffId.Caption = "WStaffId";
            this.colStaffWStaffId.FieldName = "wStaffID";
            this.colStaffWStaffId.Name = "colStaffWStaffId";
            this.colStaffWStaffId.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffWStaffId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cluWarehouses
            // 
            this.cluWarehouses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cluWarehouses.Delimiter = ", ";
            this.cluWarehouses.EditValue = "";
            this.cluWarehouses.Location = new System.Drawing.Point(6, 28);
            this.cluWarehouses.Name = "cluWarehouses";
            this.cluWarehouses.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cluWarehouses.Properties.DisplayMember = "FieldName";
            this.cluWarehouses.Properties.NullText = "";
            this.cluWarehouses.Properties.PopupFormSize = new System.Drawing.Size(325, 0);
            this.cluWarehouses.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.cluWarehouses.Properties.ValueMember = "FieldID";
            this.cluWarehouses.Properties.View = this.userControl21View;
            this.cluWarehouses.Size = new System.Drawing.Size(318, 20);
            this.cluWarehouses.TabIndex = 82;
            this.cluWarehouses.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cluWarehouses_Closed);
            this.cluWarehouses.Resize += new System.EventHandler(this.PopUp_Resize);
            // 
            // userControl21View
            // 
            this.userControl21View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExternalCode,
            this.colWarehouse,
            this.colWareId,
            this.colWareLevel,
            this.colWareParentId,
            this.colWFieldId});
            this.userControl21View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.userControl21View.Name = "userControl21View";
            this.userControl21View.OptionsFilter.AllowFilterEditor = false;
            this.userControl21View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.userControl21View.OptionsView.ShowGroupPanel = false;
            // 
            // colExternalCode
            // 
            this.colExternalCode.Caption = "Код";
            this.colExternalCode.CustomizationCaption = "Код";
            this.colExternalCode.FieldName = "FIeldCode";
            this.colExternalCode.MaxWidth = 100;
            this.colExternalCode.Name = "colExternalCode";
            this.colExternalCode.OptionsColumn.AllowEdit = false;
            this.colExternalCode.OptionsColumn.AllowShowHide = false;
            this.colExternalCode.OptionsColumn.ReadOnly = true;
            this.colExternalCode.Visible = true;
            this.colExternalCode.VisibleIndex = 0;
            // 
            // colWarehouse
            // 
            this.colWarehouse.Caption = "Название склада";
            this.colWarehouse.CustomizationCaption = "Название склада";
            this.colWarehouse.FieldName = "FieldName";
            this.colWarehouse.Name = "colWarehouse";
            this.colWarehouse.OptionsColumn.AllowEdit = false;
            this.colWarehouse.OptionsColumn.AllowShowHide = false;
            this.colWarehouse.OptionsColumn.ReadOnly = true;
            this.colWarehouse.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colWarehouse.Visible = true;
            this.colWarehouse.VisibleIndex = 1;
            // 
            // colWareId
            // 
            this.colWareId.Caption = "Id";
            this.colWareId.FieldName = "FieldID";
            this.colWareId.Name = "colWareId";
            this.colWareId.OptionsColumn.AllowShowHide = false;
            this.colWareId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWareLevel
            // 
            this.colWareLevel.Caption = "Level";
            this.colWareLevel.FieldName = "Level";
            this.colWareLevel.Name = "colWareLevel";
            this.colWareLevel.OptionsColumn.AllowShowHide = false;
            this.colWareLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWareParentId
            // 
            this.colWareParentId.Caption = "ParentId";
            this.colWareParentId.FieldName = "FieldParentID";
            this.colWareParentId.Name = "colWareParentId";
            this.colWareParentId.OptionsColumn.AllowShowHide = false;
            this.colWareParentId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWFieldId
            // 
            this.colWFieldId.Caption = "WFieldId";
            this.colWFieldId.FieldName = "wFieldID";
            this.colWFieldId.Name = "colWFieldId";
            this.colWFieldId.OptionsColumn.AllowShowHide = false;
            this.colWFieldId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gcWave
            // 
            this.gcWave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcWave.Controls.Add(this.gluWaves);
            this.gcWave.Controls.Add(this.cActive);
            this.gcWave.Location = new System.Drawing.Point(338, 63);
            this.gcWave.Name = "gcWave";
            this.gcWave.Size = new System.Drawing.Size(343, 54);
            this.gcWave.TabIndex = 105;
            this.gcWave.Text = "Волна";
            // 
            // gluWaves
            // 
            this.gluWaves.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gluWaves.EditValue = "";
            this.gluWaves.Location = new System.Drawing.Point(5, 27);
            this.gluWaves.Name = "gluWaves";
            this.gluWaves.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gluWaves.Properties.DisplayMember = "FieldName";
            this.gluWaves.Properties.NullText = "";
            this.gluWaves.Properties.PopupFormSize = new System.Drawing.Size(335, 0);
            this.gluWaves.Properties.ValueMember = "wInvPeriodId";
            this.gluWaves.Properties.View = this.gridLookUpEdit1View;
            this.gluWaves.Size = new System.Drawing.Size(333, 20);
            this.gluWaves.TabIndex = 105;
            this.gluWaves.EditValueChanged += new System.EventHandler(this.gluWaves_EditValueChanged);
            this.gluWaves.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.gluWaves_CustomDisplayText);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colWaveParentId,
            this.colName,
            this.colLevel,
            this.colDateFrom,
            this.colDateTo,
            this.colInvType,
            this.colIsActive,
            this.colWInvId});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "FieldID";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowShowHide = false;
            this.colId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWaveParentId
            // 
            this.colWaveParentId.Caption = "ParentId";
            this.colWaveParentId.FieldName = "FieldParentID";
            this.colWaveParentId.Name = "colWaveParentId";
            this.colWaveParentId.OptionsColumn.AllowShowHide = false;
            this.colWaveParentId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colName
            // 
            this.colName.Caption = "Название Инвентаризации";
            this.colName.FieldName = "FieldName";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowShowHide = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 150;
            // 
            // colLevel
            // 
            this.colLevel.Caption = "Level";
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            this.colLevel.OptionsColumn.AllowShowHide = false;
            this.colLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDateFrom
            // 
            this.colDateFrom.Caption = "Дата С";
            this.colDateFrom.FieldName = "StartDate";
            this.colDateFrom.Name = "colDateFrom";
            this.colDateFrom.OptionsColumn.AllowEdit = false;
            this.colDateFrom.OptionsColumn.AllowShowHide = false;
            this.colDateFrom.OptionsColumn.ReadOnly = true;
            this.colDateFrom.Visible = true;
            this.colDateFrom.VisibleIndex = 1;
            this.colDateFrom.Width = 115;
            // 
            // colDateTo
            // 
            this.colDateTo.Caption = "Дата ПО";
            this.colDateTo.FieldName = "EndDate";
            this.colDateTo.Name = "colDateTo";
            this.colDateTo.OptionsColumn.AllowEdit = false;
            this.colDateTo.OptionsColumn.AllowShowHide = false;
            this.colDateTo.OptionsColumn.ReadOnly = true;
            this.colDateTo.Visible = true;
            this.colDateTo.VisibleIndex = 2;
            this.colDateTo.Width = 116;
            // 
            // colInvType
            // 
            this.colInvType.Caption = "InvType";
            this.colInvType.FieldName = "InvType";
            this.colInvType.Name = "colInvType";
            this.colInvType.OptionsColumn.AllowShowHide = false;
            this.colInvType.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colIsActive
            // 
            this.colIsActive.Caption = "IsActive";
            this.colIsActive.FieldName = "IsActive";
            this.colIsActive.Name = "colIsActive";
            this.colIsActive.OptionsColumn.AllowShowHide = false;
            this.colIsActive.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colWInvId
            // 
            this.colWInvId.Caption = "WInvId";
            this.colWInvId.FieldName = "wInvPeriodId";
            this.colWInvId.Name = "colWInvId";
            this.colWInvId.OptionsColumn.AllowShowHide = false;
            this.colWInvId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cActive
            // 
            this.cActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cActive.EditValue = true;
            this.cActive.Location = new System.Drawing.Point(265, 2);
            this.cActive.Name = "cActive";
            this.cActive.Properties.Caption = "Активная";
            this.cActive.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cActive.Size = new System.Drawing.Size(73, 19);
            this.cActive.TabIndex = 78;
            this.cActive.TabStop = false;
            this.cActive.CheckedChanged += new System.EventHandler(this.cActive_CheckedChanged);
            // 
            // cbInventType
            // 
            this.cbInventType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbInventType.Delimiter = ",";
            this.cbInventType.Location = new System.Drawing.Point(5, 27);
            this.cbInventType.Name = "cbInventType";
            this.cbInventType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbInventType.Properties.DisplayMember = "TypeValue";
            this.cbInventType.Properties.DropDownRows = 3;
            this.cbInventType.Properties.SelectAllItemCaption = "(Все)";
            this.cbInventType.Properties.ShowButtons = false;
            this.cbInventType.Properties.ValueMember = "TypeId";
            this.cbInventType.Size = new System.Drawing.Size(333, 20);
            this.cbInventType.TabIndex = 99;
            this.cbInventType.EditValueChanged += new System.EventHandler(this.cbInventType_EditValueChanged);
            // 
            // cShowUnInvent
            // 
            this.cShowUnInvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cShowUnInvent.EditValue = true;
            this.cShowUnInvent.Location = new System.Drawing.Point(346, 467);
            this.cShowUnInvent.Name = "cShowUnInvent";
            this.cShowUnInvent.Properties.Caption = "Отображать записи со статусом «НЕ проинвентаризировано»";
            this.cShowUnInvent.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cShowUnInvent.Size = new System.Drawing.Size(337, 19);
            this.cShowUnInvent.TabIndex = 103;
            this.cShowUnInvent.TabStop = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // component1View
            // 
            this.component1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.component1View.Name = "component1View";
            this.component1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.component1View.OptionsView.ShowGroupPanel = false;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Дислокация";
            this.treeListColumn2.CustomizationCaption = "Дислокация";
            this.treeListColumn2.FieldName = "GeographyName";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // tlpGeneral
            // 
            this.tlpGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpGeneral.ColumnCount = 2;
            this.tlpGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49F));
            this.tlpGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51F));
            this.tlpGeneral.Controls.Add(this.gcStuffing, 1, 2);
            this.tlpGeneral.Controls.Add(this.gcTerritory, 0, 0);
            this.tlpGeneral.Controls.Add(this.gcWarehouse, 0, 3);
            this.tlpGeneral.Controls.Add(this.gcInvType, 1, 0);
            this.tlpGeneral.Controls.Add(this.gcWave, 1, 1);
            this.tlpGeneral.Location = new System.Drawing.Point(7, 8);
            this.tlpGeneral.Name = "tlpGeneral";
            this.tlpGeneral.RowCount = 4;
            this.tlpGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpGeneral.Size = new System.Drawing.Size(684, 452);
            this.tlpGeneral.TabIndex = 104;
            // 
            // gcStuffing
            // 
            this.gcStuffing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcStuffing.Controls.Add(this.tStaffing);
            this.gcStuffing.Location = new System.Drawing.Point(338, 123);
            this.gcStuffing.Name = "gcStuffing";
            this.tlpGeneral.SetRowSpan(this.gcStuffing, 2);
            this.gcStuffing.Size = new System.Drawing.Size(343, 326);
            this.gcStuffing.TabIndex = 105;
            this.gcStuffing.Text = "Персонал";
            // 
            // gcTerritory
            // 
            this.gcTerritory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcTerritory.Controls.Add(this.tTerritory);
            this.gcTerritory.Location = new System.Drawing.Point(3, 3);
            this.gcTerritory.Name = "gcTerritory";
            this.tlpGeneral.SetRowSpan(this.gcTerritory, 3);
            this.gcTerritory.Size = new System.Drawing.Size(329, 386);
            this.gcTerritory.TabIndex = 105;
            this.gcTerritory.Text = "Территория";
            // 
            // gcWarehouse
            // 
            this.gcWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcWarehouse.Controls.Add(this.cluWarehouses);
            this.gcWarehouse.Controls.Add(this.lblUpd2);
            this.gcWarehouse.Location = new System.Drawing.Point(3, 395);
            this.gcWarehouse.Name = "gcWarehouse";
            this.gcWarehouse.Size = new System.Drawing.Size(329, 54);
            this.gcWarehouse.TabIndex = 105;
            this.gcWarehouse.Text = "Склад";
            // 
            // gcInvType
            // 
            this.gcInvType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcInvType.Controls.Add(this.cbInventType);
            this.gcInvType.Location = new System.Drawing.Point(338, 3);
            this.gcInvType.Name = "gcInvType";
            this.gcInvType.Size = new System.Drawing.Size(343, 54);
            this.gcInvType.TabIndex = 105;
            this.gcInvType.Text = "Тип инвентаризации";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 534);
            this.Controls.Add(this.tlpGeneral);
            this.Controls.Add(this.cShowUnInvent);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.generateReportButton);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(630, 400);
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры к \"Inventory Tool\"";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTerritory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStaffing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cluWarehouses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWave)).EndInit();
            this.gcWave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gluWaves.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbInventType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cShowUnInvent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.component1View)).EndInit();
            this.tlpGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcStuffing)).EndInit();
            this.gcStuffing.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTerritory)).EndInit();
            this.gcTerritory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcWarehouse)).EndInit();
            this.gcWarehouse.ResumeLayout(false);
            this.gcWarehouse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcInvType)).EndInit();
            this.gcInvType.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraEditors.SimpleButton generateReportButton;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.Grid.GridView component1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private GridLookUpWithMultipleChecking cluWarehouses;
        private DevExpress.XtraGrid.Views.Grid.GridView userControl21View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouse;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWareId;
        private DevExpress.XtraGrid.Columns.GridColumn colWareLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWareParentId;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private TreeListIncremental tTerritory;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTerritory;
        private TreeListIncremental tStaffing;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffing;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.CheckEdit cShowUnInvent;
        private ComboboxExtended cbInventType;
        private DevExpress.XtraEditors.CheckEdit cActive;
        private System.Windows.Forms.TableLayoutPanel tlpGeneral;
        private DevExpress.XtraEditors.GroupControl gcWave;
        private DevExpress.XtraEditors.GroupControl gcStuffing;
        private DevExpress.XtraEditors.GroupControl gcTerritory;
        private DevExpress.XtraEditors.GroupControl gcWarehouse;
        private DevExpress.XtraEditors.GroupControl gcInvType;
        private DevExpress.XtraEditors.GridLookUpEdit gluWaves;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTo;
        private DevExpress.XtraGrid.Columns.GridColumn colInvType;
        private DevExpress.XtraGrid.Columns.GridColumn colIsActive;
        private DevExpress.XtraGrid.Columns.GridColumn colWaveParentId;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWInvId;
        private DevExpress.XtraGrid.Columns.GridColumn colWFieldId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffWStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTerLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTerGeographyId;
	}
}