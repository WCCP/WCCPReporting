﻿namespace SoftServe.Reports.EquipmentInventoryTool.Utility
{
    class InventoryRecord
    {
        public long Id { get; set; }
        public string CorrectSerialNumber { get; set; }
        public byte InventStatus { get; set; }
        public string Comment { get; set; }
        
        public InventoryRecord() {}

        public InventoryRecord(long id, string corSerNum, byte inventStatus, string comment)
        {
            Id = id;
            CorrectSerialNumber = corSerNum;
            InventStatus = inventStatus;
            Comment = comment;
        }
    }
}
