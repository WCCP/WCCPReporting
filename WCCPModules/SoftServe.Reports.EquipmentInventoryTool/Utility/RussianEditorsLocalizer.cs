﻿using DevExpress.XtraEditors.Controls;

namespace SoftServe.Reports.EquipmentInventoryTool
{
    public class RussianEditorsLocalizer : Localizer
    {
        public override string Language
        {
            get { return "Russian"; }
        }

        public override string GetLocalizedString(StringId id)
        {
            string ret = Resource.ResourceManager.GetString(id.ToString());

            if (string.IsNullOrEmpty(ret))
            {
                ret = base.GetLocalizedString(id);
            }

            return ret;
        }
    }
}
