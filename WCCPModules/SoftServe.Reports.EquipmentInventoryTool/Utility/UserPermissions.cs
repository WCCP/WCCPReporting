﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.EquipmentInventoryTool.Utility
{
    public enum UserPermissions
    {
        Read = 0,
        ReadWrite = 1,
        SuperMan = 2
    }
}
