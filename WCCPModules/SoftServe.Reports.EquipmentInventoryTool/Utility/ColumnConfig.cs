﻿namespace SoftServe.Reports.EquipmentInventoryTool.Utility
{
    /// <summary>
    /// Describes column configuration params needed for layout restoring
    /// </summary>
    internal class ColumnConfig
    {
        public int Width { get; set; }
        public int ColIndex { get; set; }
        public int RowIndex { get; set; }

        public  ColumnConfig()
        {
        }

        public ColumnConfig(int width, int colInd, int rowInd)
        {
            Width = width;
            ColIndex = colInd;
            RowIndex = rowInd;
        }
    }
}
