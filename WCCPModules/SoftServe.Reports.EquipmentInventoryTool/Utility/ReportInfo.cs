﻿using System.Collections.Generic;

namespace SoftServe.Reports.EquipmentInventoryTool
{
    /// <summary>
    /// Describes the report settings
    /// </summary>
    public class ReportInfo
    {
        public List<string> CitiesIds { get; set; }
        public List<string> StaffingIdsM3 { get; set; }
        public List<string> WarehousesLocationIds { get; set; }

        public int? WaveId { get; set; }
        public bool ShowNonInvent { get; set; }
    }
}
