﻿namespace SoftServe.Reports.EquipmentInventoryTool.Utility
{
    /// <summary>
    /// Represents Inventory statuses
    /// </summary>
    enum InventStatuses:byte
    {
        NonInvent = 0,
        Invented,
        InventWithDiffs,
        PartiallyConfirmed,
        NonConfirmed,
        Found,
        Lost,
        EliminatedWithErrors
    }
}
