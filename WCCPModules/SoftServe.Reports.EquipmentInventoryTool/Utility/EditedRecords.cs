﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.EquipmentInventoryTool.Utility
{
    class EditedRecords
    {
        public Dictionary<long, InventoryRecord> Records { get; set; }

        public EditedRecords()
        {
            Records = new Dictionary<long, InventoryRecord>();
        }

        public void Add(InventoryRecord record)
        {
            if(Records.ContainsKey(record.Id))
            {
                Records[record.Id] = record;
            }
            else
            {
                Records.Add(record.Id, record);
            }
        }
    }
}
