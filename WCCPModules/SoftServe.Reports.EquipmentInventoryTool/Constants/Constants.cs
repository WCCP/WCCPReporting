﻿namespace SoftServe.Reports.EquipmentInventoryTool
{
    /// <summary>
    /// Stored procedures and their params names 
    /// </summary>
    public static class Constants
    {
        public const string FN_USER_PREMISSION = "Pos.fnGetInventoryToolRightForCurrentUser";
        public const string FN_USER_PREMISSION_RETURN = "value";

        #region Settings Window

        public const string SP_TERRITORY_TREE = "spDW_GetTerritoryTree";
        public const string SP_TERRITORY_TREE_LevelFrom_PARAM = "LevelFrom";
        public const string SP_TERRITORY_TREE_LevelTo_PARAM = "LevelTo";

        public const string SP_STAFFING_TREE = "spDW_GetHistoryStaff";
        public const string SP_STAFFING_TREE_Date_PARAM = "Date";

        public const string SP_WAREHOUSES_TREE = "Pos.spDW_GetWarehouseLocationTree";

        public const string SP_WAVE_TYPES_LIST = "Pos.spDW_GetInventoryTypes";

        public const string SP_WAVES_LIST = "Pos.spDW_GetInventoryWaves";

        public const string SP_INV_STATUSES_LIST = "Pos.spDW_GetInventoryStatuses";
        #endregion Settings Window

        #region Main report tab

        public const string SP_GET_REPORT_DATA = "Pos.spDW_GetInventoryResults";

        public const string SP_GET_REPORT_DATA_Cities_PARAM = "Cities";
        public const string SP_GET_REPORT_DATA_Staff_M3_PARAM = "M3";
        public const string SP_GET_REPORT_DATA_WarehouseLocations_PARAM = "Warehouses";
        public const string SP_GET_REPORT_DATA_Waves_PARAM = "InventoryPeriod";
        public const string SP_GET_REPORT_DATA_ShowNonInvent_PARAM = "ShowNotInventoried";

        public const string SP_SET_INVENT_INFO = "Pos.spDW_SetNewInventoryStatus";

        public const string SP_PERFOM_MAPPING_FOR_SAVED_1 = "Pos.spDW_InventoryMappingForCustomers";
        public const string SP_PERFOM_MAPPING_FOR_SAVED_1_PARAM = "OL_ID";

        public const string SP_PERFOM_MAPPING_FOR_SAVED_2 = "Pos.spDW_InventoryMappingForWarehouses";

        public const string SP_PERFOM_MAPPING_FOR_NEWLY_LOADED = "Pos.spDW_StartPOSRefreshProcess";

        public const string SP_SET_INVENT_INFO_Id = "ID";
        public const string SP_SET_INVENT_INFO_Status = "InventoryStatus";
        public const string SP_SET_INVENT_INFO_SerialNumber = "Serial_No";
        public const string SP_SET_INVENT_INFO_Comment = "Comment";

        #endregion Main report tab
    }
}
