﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataAccess;
using SoftServe.Reports.EquipmentInventoryTool.Utility;

namespace SoftServe.Reports.EquipmentInventoryTool
{
    internal static class DataProvider
    {
        /// <summary>
        /// Gets the territory tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of territory division</param>
        /// <param name="levelTo">The lowest level of territory division</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetTerritoryTree(out int regionLevel)
        {
            int lTerritoryLevel = 2;
            int lRegionOfCityLevel = 6;

            regionLevel = lTerritoryLevel + 1;

            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_TERRITORY_TREE, new[]
                    {
                        new SqlParameter (Constants.SP_TERRITORY_TREE_LevelFrom_PARAM, lTerritoryLevel),
                        new SqlParameter (Constants.SP_TERRITORY_TREE_LevelTo_PARAM, lRegionOfCityLevel)
                    });

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the staffing tree actual for the date chosen
        /// </summary>
        /// <param name="date">The date of cheching</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetStaffingTree(DateTime date, out int staffM3Level)
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_STAFFING_TREE,
                new[] { new SqlParameter(Constants.SP_STAFFING_TREE_Date_PARAM, date) });

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            staffM3Level = 4;//2

            DataView lStaffingView = lDataTable.DefaultView;
            lStaffingView.RowFilter = "Level > 0 and Level <= " + staffM3Level;
            lDataTable = lStaffingView.ToTable();

            return lDataTable;
        }

        /// <summary>
        /// Gets the tree of warehouses interlaced with territory
        /// </summary>
        /// <returns>The list of warehouses</returns>
        public static DataTable GetWarehouseTree(out int warehouseLevel)
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_WAREHOUSES_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }
            
            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                warehouseLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                warehouseLevel = 7;
            }

            DataView lDataViewWarehouses = lDataTable.DefaultView;
            lDataViewWarehouses.RowFilter = "Level = " + warehouseLevel.ToString();
            lDataTable = lDataViewWarehouses.ToTable();

            //lDataTable.Rows.Add(30730, 20948, "Warehouse 2 ХАРКІВ Київський", 7, 053457, 30730);//ХАРКІВ Київський
            //lDataTable.Rows.Add(30731, 23396, "Warehouse 3 СУМИ - ", 7, 1785354, 30731);//СУМИ - 
            //lDataTable.Rows.Add(30732, 24968, "Warehouse 4 ВІННИЦЯ -", 7, 035667, 30732);//ВІННИЦЯ -
            //lDataTable.Rows.Add(30733, 24968, "Warehouse 5 ВІННИЦЯ -", 7, 135674, 30733);//ВІННИЦЯ - 
            //lDataTable.Rows.Add(30734, 24968, "Warehouse 6 ВІННИЦЯ -", 7, 367480, 30734);//ВІННИЦЯ -
            //lDataTable.Rows.Add(30735, 27343, "Warehouse 1 ПОЛТАВА Київський", 7, 14524, 30735);//ПОЛТАВА Київський
            
            return lDataTable;
        }

        /// <summary>
        /// Gets the list of equipment statuses
        /// </summary>
        /// <returns>The list of equipment statuses</returns>
        public static DataTable GetInventoryStatusesList()
        {
            DataTable lDataTable = new DataTable();

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_INV_STATUSES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the list of wave types
        /// </summary>
        /// <returns>The list of wave types</returns>
        public static DataTable GetWaveTypesList()
        {
            DataTable lDataTable = new DataTable();

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_WAVE_TYPES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }
            
            //lDataTable.Rows.Add(0, "Годовая");
            //lDataTable.Rows.Add(1, "Текущая");
            
            return lDataTable;
        }

        /// <summary>
        /// Gets the list of waves
        /// </summary>
        /// <returns>The list of waves</returns>
        public static DataTable GetWavesList(out int waveLevel)
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_WAVES_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            if (dataSet != null && dataSet.Tables.Count > 1)
            {
                waveLevel = (int)dataSet.Tables[1].Rows[0][0];
            }
            else
            {
                waveLevel = 6;
            }
            
            DataView lDataViewWaves = lDataTable.DefaultView;
            lDataViewWaves.RowFilter = "Level = " + waveLevel.ToString();
            lDataTable = lDataViewWaves.ToTable();

            //lDataTable.Rows.Add(15666, 13425, "Wave 2 Годовая ХАРКІВ", 6, new DateTime(2012, 5, 12), new DateTime(2013, 12, 27), 0, 0, 15666);//ХАРКІВ
            //lDataTable.Rows.Add(15665, 14190, "Wave 1 Текущая ПОЛТАВА Акт", 6, new DateTime(2012, 12, 12), new DateTime(2014, 12, 13), 1, 1, 15665);//ПОЛТАВА
            //lDataTable.Rows.Add(15667, 12633, "Wave 3 Текущая СУМИ", 6, new DateTime(2012, 7, 12), new DateTime(2013, 12, 24), 0, 1, 15667);//СУМИ
            //lDataTable.Rows.Add(15668, 11252, "Wave 4 Годовая ВІННИЦЯ", 6, new DateTime(2012, 8, 12), new DateTime(2013, 12, 23), 0, 0, 15668);//ВІННИЦЯ
            //lDataTable.Rows.Add(15669, 11252, "Wave 5 Текущая ВІННИЦЯ Акт", 6, new DateTime(2012, 9, 12), new DateTime(2014, 4, 13), 1, 1, 15669);//ВІННИЦЯ
            //lDataTable.Rows.Add(15670, 11252, "Wave 6 Годовая ВІННИЦЯ Акт", 6, new DateTime(2012, 10, 12), new DateTime(2014, 4, 12), 1, 0, 15670);//ВІННИЦЯ

            return lDataTable;
        }

        /// <summary>
        /// Gets user permission descriotion
        /// </summary>
        /// <returns>The user permission descriotion</returns>
        public static UserPermissions GetUserPermissions()
        {
            SqlParameter lReturnParam = new SqlParameter(Constants.FN_USER_PREMISSION_RETURN, DbType.String);
            lReturnParam.Direction = ParameterDirection.ReturnValue;

            SqlParameterCollection result = DataAccessLayer.ExecuteNonQueryStoredProcedureWithOutput(Constants.FN_USER_PREMISSION, lReturnParam);

            string lDescription = ConvertEx.ToString(result[Constants.FN_USER_PREMISSION_RETURN].Value);

            UserPermissions lPermissions;

            switch (lDescription.ToUpper())
            {
                case "R": lPermissions = UserPermissions.Read;
                    break;
                case "RW": lPermissions = UserPermissions.ReadWrite;
                    break;
                case "SA": lPermissions = UserPermissions.SuperMan;
                    break;
                default: lPermissions = UserPermissions.Read;
                    break;
            }

           return lPermissions;
        }

        /// <summary>
        /// Updates the inventory data.
        /// </summary>
        public static void UpdateInventInfo(InventoryRecord record)
        {
            DataAccessLayer.ExecuteStoredProcedure(Constants.SP_SET_INVENT_INFO,
                new SqlParameter(Constants.SP_SET_INVENT_INFO_Id, record.Id),
                new SqlParameter(Constants.SP_SET_INVENT_INFO_Status, record.InventStatus),
                new SqlParameter(Constants.SP_SET_INVENT_INFO_SerialNumber, record.CorrectSerialNumber),
                new SqlParameter(Constants.SP_SET_INVENT_INFO_Comment, record.Comment)
                );
        }

        /// <summary>
        /// Updates the inventory data.
        /// </summary>
        public static void PerformMappingForNewlySavedData()
        {
            DataAccessLayer.ExecuteStoredProcedure(Constants.SP_PERFOM_MAPPING_FOR_SAVED_1,
                new SqlParameter(Constants.SP_PERFOM_MAPPING_FOR_SAVED_1_PARAM, DBNull.Value));

            DataAccessLayer.ExecuteStoredProcedure(Constants.SP_PERFOM_MAPPING_FOR_SAVED_2);
        }

        /// <summary>
        /// Updates the inventory data.
        /// </summary>
        public static void PerformMappingOfNewlyLoadedData()
        {
            try
            {
                DataAccessLayer.ExecuteStoredProcedure(Constants.SP_PERFOM_MAPPING_FOR_NEWLY_LOADED);
            }
            catch (Exception)
            {}
         }

        /// <summary>
        /// Gets the main report data.
        /// </summary>
        /// <returns>The main report data.</returns>
        public static DataTable GetMainReportData(ReportInfo settings)
        {
            DataTable lDataTable = new DataTable();
            
            object lWarehouses = DBNull.Value;
            if (settings.WarehousesLocationIds.Count > 0)
            {
                lWarehouses = XmlHelper.GetXmlDescription(settings.WarehousesLocationIds);
            }

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_REPORT_DATA,
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Cities_PARAM, XmlHelper.GetXmlDescription(settings.CitiesIds)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Staff_M3_PARAM, XmlHelper.GetXmlDescription(settings.StaffingIdsM3)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_WarehouseLocations_PARAM, lWarehouses),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Waves_PARAM, settings.WaveId.Value),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_ShowNonInvent_PARAM, settings.ShowNonInvent)
                );

            //dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_REPORT_DATA, new SqlParameter(Constants.SP_GET_REPORT_DATA_Waves_PARAM, 9));

            if (dataSet != null)
            {
                lDataTable = dataSet.Tables[0];
            }


            //List<object> ll = new List<object>();

            //foreach (DataColumn col in lDataTable.Columns)
            //{
            //    if (col.DataType == typeof(int))
            //    {
            //        ll.Add(4);
            //    }
            //    else

            //        if (col.DataType == typeof(bool))
            //        {
            //            ll.Add(true);
            //        }
            //        else

            //            if (col.DataType == typeof(long))
            //            {
            //                ll.Add(1234);
            //            }
            //            else

            //                if (col.DataType == typeof(byte))
            //                {
            //                    ll.Add(3);
            //                }
            //                else

            //                    if (col.DataType == typeof(DateTime))
            //                    {
            //                        ll.Add(new DateTime(2014, 2, 11));
            //                    }

            //                    else ll.Add("lalala");
            //}

            //List<object> ll3 = new List<object>();

            //foreach (DataColumn col in lDataTable.Columns)
            //{
            //    if (col.DataType == typeof(int))
            //    {
            //        ll3.Add(4);
            //    }
            //    else

            //        if (col.DataType == typeof(bool))
            //        {
            //            ll3.Add(false);
            //        }
            //        else

            //            if (col.DataType == typeof(long))
            //            {
            //                ll3.Add(12345);
            //            }
            //            else

            //                if (col.DataType == typeof(byte))
            //                {
            //                    ll3.Add(3);
            //                }
            //                else

            //                    if (col.DataType == typeof(DateTime))
            //                    {
            //                        ll3.Add(new DateTime(2014, 2, 11));
            //                    }

            //                    else ll3.Add("lalala");
            //}


            //List<object> ll1 = new List<object>();

            //foreach (DataColumn col in lDataTable.Columns)
            //{
            //    if (col.DataType == typeof(int))
            //    {
            //        ll1.Add(4);
            //    }
            //    else

            //        if (col.DataType == typeof(bool))
            //        {
            //            ll1.Add(DBNull.Value);
            //        }
            //        else

            //            if (col.DataType == typeof(long))
            //            {
            //                ll1.Add(1234);
            //            }
            //            else

            //                if (col.DataType == typeof(byte))
            //                {
            //                    ll1.Add(7);
            //                }
            //                else

            //                    if (col.DataType == typeof(DateTime))
            //                    {
            //                        ll1.Add(new DateTime(2014, 2, 11));
            //                    }

            //                    else ll1.Add("lalala");
            //}

            //lDataTable.Rows.Add(ll.ToArray());
            //lDataTable.Rows.Add(ll3.ToArray());
            //lDataTable.Rows.Add(ll1.ToArray());
            //lDataTable.Rows.Add(ll3.ToArray());

            
            DataColumn lCol = new DataColumn("InvStatusInitial", typeof(byte));
            lDataTable.Columns.Add(lCol);

            foreach (DataRow row in lDataTable.Rows)
            {
                row[lCol] = row["InventoryStatus"];
            }

            return lDataTable;
        }
    }
}
