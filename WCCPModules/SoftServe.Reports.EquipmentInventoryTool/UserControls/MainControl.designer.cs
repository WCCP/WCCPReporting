﻿namespace SoftServe.Reports.EquipmentInventoryTool.UserControls
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.bandKPKData = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandKPKData1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colInventoryWave = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTerritory = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRegion_name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCust_NAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colW_Code = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colW_Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colW_Location = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOL_id = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOLname = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOLDeliveryAddress = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandKPKDataOpt = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOLTradingName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOLAddress = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDistrict_Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCntArea_name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCity_Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandKPKPOS = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colModelName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSerial_No = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPOSBrand_Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYearProduction = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTechStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCorrectSerialNo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandKPKTech = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.coltech_IsSideWallsOk = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_IsScreenBrandOk = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_IsLowerGrateFront = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_IsDoorsOk = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_NumShelf = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_IsShelfOk = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_NumShelfSupport = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_IsWetClean = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.coltech_IsPackLabel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandSysPOS = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSysClassName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSysModelName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSysSerialNo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSysInvNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSysPOSBrand_Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSysYearProduction = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSysTechStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colM2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM2Phone = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM1Phone = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colInvStatusId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.riInvStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colComment = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLoginModifiedBy = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLastModifiedDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLastInvDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLastApproveDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colInvStatusInit = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();

            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riInvStatusLookUp)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riInvStatusLookUp,
            this.rTextEdit});
            this.gridControl.Size = new System.Drawing.Size(900, 500);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.BandPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.BandPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandKPKData,
            this.bandSysPOS,
            this.gridBand1,
            this.gridBand2});
            this.gridView.ColumnPanelRowHeight = 37;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colInventoryWave,
            this.colTerritory,
            this.colRegion_name,
            this.colCust_NAME,
            this.colW_Code,
            this.colW_Name,
            this.colW_Location,
            this.colOL_id,
            this.colOLname,
            this.colOLDeliveryAddress,
            this.colOLTradingName,
            this.colOLAddress,
            this.colDistrict_Name,
            this.colCntArea_name,
            this.colCity_Name,
            this.colModelName,
            this.colSerial_No,
            this.colPOSBrand_Name,
            this.colYearProduction,
            this.colCorrectSerialNo,
            this.colTechStatus,
            this.coltech_IsSideWallsOk,
            this.coltech_IsScreenBrandOk,
            this.coltech_IsLowerGrateFront,
            this.coltech_IsDoorsOk,
            this.coltech_NumShelf,
            this.coltech_IsShelfOk,
            this.coltech_NumShelfSupport,
            this.coltech_IsWetClean,
            this.coltech_IsPackLabel,
            this.colSysClassName,
            this.colSysModelName,
            this.colSysSerialNo,
            this.colSysPOSBrand_Name,
            this.colSysYearProduction,
            this.colSysTechStatus,
            this.colInvStatusId,
            this.colComment,
            this.colLoginModifiedBy,
            this.colLastModifiedDate,
            this.colLastInvDate,
            this.colLastApproveDate,
            this.colM2,
            this.colM1,
            this.colM2Phone,
            this.colM1Phone,
            this.colInvStatusInit,
            this.colID,
            this.colSysInvNumber});
            this.gridView.CustomizationFormBounds = new System.Drawing.Rectangle(998, 200, 223, 215);
            this.gridView.GridControl = this.gridControl;
            this.gridView.GroupCount = 1;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.gridView.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gridView.OptionsLayout.LayoutVersion = "1";
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsPrint.UsePrintStyles = true;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.EnableAppearanceOddRow = true;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInvStatusId, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colW_Code, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOL_id, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.gridView_ColumnWidthChanged);
            this.gridView.ShowCustomizationForm += new System.EventHandler(this.gridView_ShowCustomizationForm);
            this.gridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
            this.gridView.ShownEditor += new System.EventHandler(this.gridView_ShownEditor);
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            this.gridView.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.gridView_BeforeLoadLayout);
            // 
            // bandKPKData
            // 
            this.bandKPKData.AppearanceHeader.Options.UseTextOptions = true;
            this.bandKPKData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandKPKData.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandKPKData.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandKPKData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandKPKData.Caption = "Данные из КПК М1/М2";
            this.bandKPKData.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandKPKData1,
            this.bandKPKDataOpt,
            this.bandKPKPOS,
            this.bandKPKTech});
            this.bandKPKData.MinWidth = 20;
            this.bandKPKData.Name = "bandKPKData";
            this.bandKPKData.OptionsBand.AllowMove = false;
            this.bandKPKData.Width = 2521;
            // 
            // bandKPKData1
            // 
            this.bandKPKData1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandKPKData1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandKPKData1.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandKPKData1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandKPKData1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandKPKData1.Caption = "Осн. данные о ТТ";
            this.bandKPKData1.Columns.Add(this.colInventoryWave);
            this.bandKPKData1.Columns.Add(this.colTerritory);
            this.bandKPKData1.Columns.Add(this.colRegion_name);
            this.bandKPKData1.Columns.Add(this.colCust_NAME);
            this.bandKPKData1.Columns.Add(this.colW_Code);
            this.bandKPKData1.Columns.Add(this.colW_Name);
            this.bandKPKData1.Columns.Add(this.colW_Location);
            this.bandKPKData1.Columns.Add(this.colOL_id);
            this.bandKPKData1.Columns.Add(this.colOLname);
            this.bandKPKData1.Columns.Add(this.colOLDeliveryAddress);
            this.bandKPKData1.Name = "bandKPKData1";
            this.bandKPKData1.OptionsBand.AllowMove = false;
            this.bandKPKData1.Width = 1155;
            // 
            // colInventoryWave
            // 
            this.colInventoryWave.AppearanceHeader.Options.UseTextOptions = true;
            this.colInventoryWave.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInventoryWave.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colInventoryWave.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInventoryWave.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInventoryWave.AutoFillDown = true;
            this.colInventoryWave.Caption = "Дата проведения инвентаризации (волна)";
            this.colInventoryWave.FieldName = "WaveDate";
            this.colInventoryWave.Name = "colInventoryWave";
            this.colInventoryWave.OptionsColumn.AllowEdit = false;
            this.colInventoryWave.OptionsColumn.ReadOnly = true;
            this.colInventoryWave.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInventoryWave.Visible = true;
            this.colInventoryWave.Width = 103;
            // 
            // colTerritory
            // 
            this.colTerritory.AutoFillDown = true;
            this.colTerritory.Caption = "Территория";
            this.colTerritory.FieldName = "Territory";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsColumn.AllowEdit = false;
            this.colTerritory.OptionsColumn.ReadOnly = true;
            this.colTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTerritory.Visible = true;
            this.colTerritory.Width = 98;
            // 
            // colRegion_name
            // 
            this.colRegion_name.AppearanceHeader.Options.UseTextOptions = true;
            this.colRegion_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRegion_name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colRegion_name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colRegion_name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRegion_name.AutoFillDown = true;
            this.colRegion_name.Caption = "Регион ";
            this.colRegion_name.FieldName = "Region";
            this.colRegion_name.Name = "colRegion_name";
            this.colRegion_name.OptionsColumn.AllowEdit = false;
            this.colRegion_name.OptionsColumn.ReadOnly = true;
            this.colRegion_name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegion_name.Visible = true;
            this.colRegion_name.Width = 98;
            // 
            // colCust_NAME
            // 
            this.colCust_NAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colCust_NAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCust_NAME.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colCust_NAME.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCust_NAME.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCust_NAME.AutoFillDown = true;
            this.colCust_NAME.Caption = "Точка синхронизации";
            this.colCust_NAME.FieldName = "Cust_NAME";
            this.colCust_NAME.Name = "colCust_NAME";
            this.colCust_NAME.OptionsColumn.AllowEdit = false;
            this.colCust_NAME.OptionsColumn.ReadOnly = true;
            this.colCust_NAME.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCust_NAME.Visible = true;
            this.colCust_NAME.Width = 134;
            // 
            // colW_Code
            // 
            this.colW_Code.AppearanceHeader.Options.UseTextOptions = true;
            this.colW_Code.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colW_Code.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colW_Code.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colW_Code.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colW_Code.AutoFillDown = true;
            this.colW_Code.Caption = "Код склада";
            this.colW_Code.FieldName = "ExternalCode";
            this.colW_Code.Name = "colW_Code";
            this.colW_Code.OptionsColumn.AllowEdit = false;
            this.colW_Code.OptionsColumn.ReadOnly = true;
            this.colW_Code.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colW_Code.Visible = true;
            // 
            // colW_Name
            // 
            this.colW_Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colW_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colW_Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colW_Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colW_Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colW_Name.AutoFillDown = true;
            this.colW_Name.Caption = "Название склада";
            this.colW_Name.FieldName = "Name";
            this.colW_Name.Name = "colW_Name";
            this.colW_Name.OptionsColumn.AllowEdit = false;
            this.colW_Name.OptionsColumn.ReadOnly = true;
            this.colW_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colW_Name.Visible = true;
            this.colW_Name.Width = 129;
            // 
            // colW_Location
            // 
            this.colW_Location.AppearanceHeader.Options.UseTextOptions = true;
            this.colW_Location.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colW_Location.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colW_Location.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colW_Location.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colW_Location.AutoFillDown = true;
            this.colW_Location.Caption = "Место локации  склада";
            this.colW_Location.FieldName = "WLocation";
            this.colW_Location.Name = "colW_Location";
            this.colW_Location.OptionsColumn.AllowEdit = false;
            this.colW_Location.OptionsColumn.ReadOnly = true;
            this.colW_Location.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colW_Location.Visible = true;
            this.colW_Location.Width = 137;
            // 
            // colOL_id
            // 
            this.colOL_id.AppearanceHeader.Options.UseTextOptions = true;
            this.colOL_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOL_id.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colOL_id.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOL_id.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOL_id.AutoFillDown = true;
            this.colOL_id.Caption = "Код ТТ";
            this.colOL_id.ColumnEdit = this.rTextEdit;
            this.colOL_id.FieldName = "OL_ID";
            this.colOL_id.Name = "colOL_id";
            this.colOL_id.OptionsColumn.AllowEdit = false;
            this.colOL_id.OptionsColumn.ReadOnly = true;
            this.colOL_id.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOL_id.Visible = true;
            this.colOL_id.Width = 101;
            // 
            // rTextEdit
            // 
            this.rTextEdit.AutoHeight = false;
            this.rTextEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.rTextEdit.Name = "rTextEdit";
            // 
            // colOLname
            // 
            this.colOLname.AppearanceHeader.Options.UseTextOptions = true;
            this.colOLname.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOLname.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colOLname.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOLname.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOLname.AutoFillDown = true;
            this.colOLname.Caption = "Юр. название ТТ ";
            this.colOLname.FieldName = "OLName";
            this.colOLname.Name = "colOLname";
            this.colOLname.OptionsColumn.AllowEdit = false;
            this.colOLname.OptionsColumn.ReadOnly = true;
            this.colOLname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLname.Visible = true;
            this.colOLname.Width = 134;
            // 
            // colOLDeliveryAddress
            // 
            this.colOLDeliveryAddress.AppearanceHeader.Options.UseTextOptions = true;
            this.colOLDeliveryAddress.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOLDeliveryAddress.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colOLDeliveryAddress.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOLDeliveryAddress.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOLDeliveryAddress.AutoFillDown = true;
            this.colOLDeliveryAddress.Caption = "Факт. адрес ТТ";
            this.colOLDeliveryAddress.FieldName = "OLDeliveryAddress";
            this.colOLDeliveryAddress.Name = "colOLDeliveryAddress";
            this.colOLDeliveryAddress.OptionsColumn.AllowEdit = false;
            this.colOLDeliveryAddress.OptionsColumn.ReadOnly = true;
            this.colOLDeliveryAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLDeliveryAddress.Visible = true;
            this.colOLDeliveryAddress.Width = 146;
            // 
            // bandKPKDataOpt
            // 
            this.bandKPKDataOpt.AppearanceHeader.Options.UseTextOptions = true;
            this.bandKPKDataOpt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandKPKDataOpt.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandKPKDataOpt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandKPKDataOpt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandKPKDataOpt.Caption = "Доп. данные о ТТ";
            this.bandKPKDataOpt.Columns.Add(this.colOLTradingName);
            this.bandKPKDataOpt.Columns.Add(this.colOLAddress);
            this.bandKPKDataOpt.Columns.Add(this.colDistrict_Name);
            this.bandKPKDataOpt.Columns.Add(this.colCntArea_name);
            this.bandKPKDataOpt.Columns.Add(this.colCity_Name);
            this.bandKPKDataOpt.Name = "bandKPKDataOpt";
            this.bandKPKDataOpt.Width = 697;
            // 
            // colOLTradingName
            // 
            this.colOLTradingName.AppearanceHeader.Options.UseTextOptions = true;
            this.colOLTradingName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOLTradingName.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colOLTradingName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOLTradingName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOLTradingName.AutoFillDown = true;
            this.colOLTradingName.Caption = "Факт. название ТТ";
            this.colOLTradingName.FieldName = "OLTradingName";
            this.colOLTradingName.Name = "colOLTradingName";
            this.colOLTradingName.OptionsColumn.AllowEdit = false;
            this.colOLTradingName.OptionsColumn.ReadOnly = true;
            this.colOLTradingName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLTradingName.Visible = true;
            this.colOLTradingName.Width = 130;
            // 
            // colOLAddress
            // 
            this.colOLAddress.AppearanceHeader.Options.UseTextOptions = true;
            this.colOLAddress.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOLAddress.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colOLAddress.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOLAddress.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOLAddress.AutoFillDown = true;
            this.colOLAddress.Caption = "Юр. адрес ТТ";
            this.colOLAddress.FieldName = "OLAddress";
            this.colOLAddress.Name = "colOLAddress";
            this.colOLAddress.OptionsColumn.AllowEdit = false;
            this.colOLAddress.OptionsColumn.ReadOnly = true;
            this.colOLAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOLAddress.Visible = true;
            this.colOLAddress.Width = 201;
            // 
            // colDistrict_Name
            // 
            this.colDistrict_Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistrict_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistrict_Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colDistrict_Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistrict_Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistrict_Name.AutoFillDown = true;
            this.colDistrict_Name.Caption = "Область";
            this.colDistrict_Name.FieldName = "District";
            this.colDistrict_Name.Name = "colDistrict_Name";
            this.colDistrict_Name.OptionsColumn.AllowEdit = false;
            this.colDistrict_Name.OptionsColumn.ReadOnly = true;
            this.colDistrict_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrict_Name.Visible = true;
            this.colDistrict_Name.Width = 144;
            // 
            // colCntArea_name
            // 
            this.colCntArea_name.AppearanceHeader.Options.UseTextOptions = true;
            this.colCntArea_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCntArea_name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colCntArea_name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCntArea_name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCntArea_name.AutoFillDown = true;
            this.colCntArea_name.Caption = "Район";
            this.colCntArea_name.FieldName = "Area";
            this.colCntArea_name.Name = "colCntArea_name";
            this.colCntArea_name.OptionsColumn.AllowEdit = false;
            this.colCntArea_name.OptionsColumn.ReadOnly = true;
            this.colCntArea_name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCntArea_name.Visible = true;
            this.colCntArea_name.Width = 127;
            // 
            // colCity_Name
            // 
            this.colCity_Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colCity_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCity_Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colCity_Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCity_Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCity_Name.AutoFillDown = true;
            this.colCity_Name.Caption = "Город";
            this.colCity_Name.FieldName = "City";
            this.colCity_Name.Name = "colCity_Name";
            this.colCity_Name.OptionsColumn.AllowEdit = false;
            this.colCity_Name.OptionsColumn.ReadOnly = true;
            this.colCity_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCity_Name.Visible = true;
            this.colCity_Name.Width = 95;
            // 
            // bandKPKPOS
            // 
            this.bandKPKPOS.AppearanceHeader.Options.UseTextOptions = true;
            this.bandKPKPOS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandKPKPOS.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandKPKPOS.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandKPKPOS.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandKPKPOS.Caption = "Оборудование";
            this.bandKPKPOS.Columns.Add(this.colModelName);
            this.bandKPKPOS.Columns.Add(this.colSerial_No);
            this.bandKPKPOS.Columns.Add(this.colPOSBrand_Name);
            this.bandKPKPOS.Columns.Add(this.colYearProduction);
            this.bandKPKPOS.Columns.Add(this.colTechStatus);
            this.bandKPKPOS.Columns.Add(this.colCorrectSerialNo);
            this.bandKPKPOS.Name = "bandKPKPOS";
            this.bandKPKPOS.OptionsBand.AllowMove = false;
            this.bandKPKPOS.Width = 669;
            // 
            // colModelName
            // 
            this.colModelName.AppearanceHeader.Options.UseTextOptions = true;
            this.colModelName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colModelName.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colModelName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colModelName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colModelName.AutoFillDown = true;
            this.colModelName.Caption = "Модель";
            this.colModelName.FieldName = "F_POSType_Name";
            this.colModelName.Name = "colModelName";
            this.colModelName.OptionsColumn.AllowEdit = false;
            this.colModelName.OptionsColumn.ReadOnly = true;
            this.colModelName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colModelName.Visible = true;
            this.colModelName.Width = 99;
            // 
            // colSerial_No
            // 
            this.colSerial_No.AppearanceHeader.Options.UseTextOptions = true;
            this.colSerial_No.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSerial_No.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSerial_No.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSerial_No.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSerial_No.AutoFillDown = true;
            this.colSerial_No.Caption = "Серийный номер";
            this.colSerial_No.FieldName = "F_Serial_No";
            this.colSerial_No.Name = "colSerial_No";
            this.colSerial_No.OptionsColumn.AllowEdit = false;
            this.colSerial_No.OptionsColumn.ReadOnly = true;
            this.colSerial_No.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSerial_No.Visible = true;
            this.colSerial_No.Width = 102;
            // 
            // colPOSBrand_Name
            // 
            this.colPOSBrand_Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colPOSBrand_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPOSBrand_Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPOSBrand_Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPOSBrand_Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPOSBrand_Name.AutoFillDown = true;
            this.colPOSBrand_Name.Caption = "Бренд";
            this.colPOSBrand_Name.FieldName = "F_POSBrand_Name";
            this.colPOSBrand_Name.Name = "colPOSBrand_Name";
            this.colPOSBrand_Name.OptionsColumn.AllowEdit = false;
            this.colPOSBrand_Name.OptionsColumn.ReadOnly = true;
            this.colPOSBrand_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSBrand_Name.Visible = true;
            this.colPOSBrand_Name.Width = 95;
            // 
            // colYearProduction
            // 
            this.colYearProduction.AppearanceHeader.Options.UseTextOptions = true;
            this.colYearProduction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYearProduction.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colYearProduction.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colYearProduction.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colYearProduction.AutoFillDown = true;
            this.colYearProduction.Caption = "Дата изготовления";
            this.colYearProduction.FieldName = "F_YearProduction";
            this.colYearProduction.Name = "colYearProduction";
            this.colYearProduction.OptionsColumn.AllowEdit = false;
            this.colYearProduction.OptionsColumn.ReadOnly = true;
            this.colYearProduction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colYearProduction.Visible = true;
            this.colYearProduction.Width = 83;
            // 
            // colTechStatus
            // 
            this.colTechStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colTechStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTechStatus.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colTechStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTechStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTechStatus.AutoFillDown = true;
            this.colTechStatus.Caption = "Техническое состояние";
            this.colTechStatus.FieldName = "F_TechnicalCondition";
            this.colTechStatus.Name = "colTechStatus";
            this.colTechStatus.OptionsColumn.AllowEdit = false;
            this.colTechStatus.OptionsColumn.ReadOnly = true;
            this.colTechStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTechStatus.Visible = true;
            this.colTechStatus.Width = 130;
            // 
            // colCorrectSerialNo
            // 
            this.colCorrectSerialNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colCorrectSerialNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCorrectSerialNo.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colCorrectSerialNo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCorrectSerialNo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCorrectSerialNo.AutoFillDown = true;
            this.colCorrectSerialNo.Caption = "Корректный серийный номер";
            this.colCorrectSerialNo.FieldName = "Serial_No_Actual";
            this.colCorrectSerialNo.Name = "colCorrectSerialNo";
            this.colCorrectSerialNo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCorrectSerialNo.Visible = true;
            this.colCorrectSerialNo.Width = 160;
            // 
            // bandKPKTech
            // 
            this.bandKPKTech.AppearanceHeader.Options.UseTextOptions = true;
            this.bandKPKTech.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandKPKTech.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandKPKTech.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandKPKTech.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandKPKTech.Caption = "Тех.состояние";
            this.bandKPKTech.Columns.Add(this.coltech_IsSideWallsOk);
            this.bandKPKTech.Columns.Add(this.coltech_IsScreenBrandOk);
            this.bandKPKTech.Columns.Add(this.coltech_IsLowerGrateFront);
            this.bandKPKTech.Columns.Add(this.coltech_IsDoorsOk);
            this.bandKPKTech.Columns.Add(this.coltech_NumShelf);
            this.bandKPKTech.Columns.Add(this.coltech_IsShelfOk);
            this.bandKPKTech.Columns.Add(this.coltech_NumShelfSupport);
            this.bandKPKTech.Columns.Add(this.coltech_IsWetClean);
            this.bandKPKTech.Columns.Add(this.coltech_IsPackLabel);
            this.bandKPKTech.Name = "bandKPKTech";
            this.bandKPKTech.Visible = false;
            this.bandKPKTech.Width = 75;
            // 
            // coltech_IsSideWallsOk
            // 
            this.coltech_IsSideWallsOk.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_IsSideWallsOk.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_IsSideWallsOk.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_IsSideWallsOk.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_IsSideWallsOk.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_IsSideWallsOk.AutoFillDown = true;
            this.coltech_IsSideWallsOk.Caption = "Боковые стенки без вмятин и царапин";
            this.coltech_IsSideWallsOk.FieldName = "F1-";
            this.coltech_IsSideWallsOk.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.coltech_IsSideWallsOk.Name = "coltech_IsSideWallsOk";
            this.coltech_IsSideWallsOk.OptionsColumn.AllowEdit = false;
            this.coltech_IsSideWallsOk.OptionsColumn.ReadOnly = true;
            this.coltech_IsSideWallsOk.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.coltech_IsSideWallsOk.UnboundExpression = "Iif(IsNull([F1]), \' \' , Iif([F1] == True, \'Да\' , \'Нет\') )";
            this.coltech_IsSideWallsOk.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // coltech_IsScreenBrandOk
            // 
            this.coltech_IsScreenBrandOk.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_IsScreenBrandOk.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_IsScreenBrandOk.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_IsScreenBrandOk.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_IsScreenBrandOk.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_IsScreenBrandOk.AutoFillDown = true;
            this.coltech_IsScreenBrandOk.Caption = "Экран целый, бренд совпадает";
            this.coltech_IsScreenBrandOk.FieldName = "F2-";
            this.coltech_IsScreenBrandOk.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.coltech_IsScreenBrandOk.Name = "coltech_IsScreenBrandOk";
            this.coltech_IsScreenBrandOk.OptionsColumn.AllowEdit = false;
            this.coltech_IsScreenBrandOk.OptionsColumn.ReadOnly = true;
            this.coltech_IsScreenBrandOk.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.coltech_IsScreenBrandOk.UnboundExpression = "Iif(IsNull([F2]), \' \' , Iif([F2] == True, \'Да\' , \'Нет\') )";
            this.coltech_IsScreenBrandOk.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // coltech_IsLowerGrateFront
            // 
            this.coltech_IsLowerGrateFront.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_IsLowerGrateFront.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_IsLowerGrateFront.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_IsLowerGrateFront.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_IsLowerGrateFront.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_IsLowerGrateFront.AutoFillDown = true;
            this.coltech_IsLowerGrateFront.Caption = "Нижняя решетка спереди";
            this.coltech_IsLowerGrateFront.FieldName = "F3-";
            this.coltech_IsLowerGrateFront.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.coltech_IsLowerGrateFront.Name = "coltech_IsLowerGrateFront";
            this.coltech_IsLowerGrateFront.OptionsColumn.AllowEdit = false;
            this.coltech_IsLowerGrateFront.OptionsColumn.ReadOnly = true;
            this.coltech_IsLowerGrateFront.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.coltech_IsLowerGrateFront.UnboundExpression = "Iif(IsNull([F3]), \' \' , Iif([F3] == True, \'Да\' , \'Нет\') )";
            this.coltech_IsLowerGrateFront.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // coltech_IsDoorsOk
            // 
            this.coltech_IsDoorsOk.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_IsDoorsOk.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_IsDoorsOk.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_IsDoorsOk.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_IsDoorsOk.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_IsDoorsOk.AutoFillDown = true;
            this.coltech_IsDoorsOk.Caption = "Исправность двери/дверей";
            this.coltech_IsDoorsOk.FieldName = "F4-";
            this.coltech_IsDoorsOk.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.coltech_IsDoorsOk.Name = "coltech_IsDoorsOk";
            this.coltech_IsDoorsOk.OptionsColumn.AllowEdit = false;
            this.coltech_IsDoorsOk.OptionsColumn.ReadOnly = true;
            this.coltech_IsDoorsOk.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.coltech_IsDoorsOk.UnboundExpression = "Iif(IsNull([F4]), \' \' , Iif([F4] == True, \'Да\' , \'Нет\') )";
            this.coltech_IsDoorsOk.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // coltech_NumShelf
            // 
            this.coltech_NumShelf.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_NumShelf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_NumShelf.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_NumShelf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_NumShelf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_NumShelf.AutoFillDown = true;
            this.coltech_NumShelf.Caption = "Количество полок";
            this.coltech_NumShelf.FieldName = "F5";
            this.coltech_NumShelf.Name = "coltech_NumShelf";
            this.coltech_NumShelf.OptionsColumn.AllowEdit = false;
            this.coltech_NumShelf.OptionsColumn.ReadOnly = true;
            this.coltech_NumShelf.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // coltech_IsShelfOk
            // 
            this.coltech_IsShelfOk.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_IsShelfOk.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_IsShelfOk.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_IsShelfOk.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_IsShelfOk.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_IsShelfOk.AutoFillDown = true;
            this.coltech_IsShelfOk.Caption = "Состояние полок удовдетворительное";
            this.coltech_IsShelfOk.FieldName = "F6-";
            this.coltech_IsShelfOk.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.coltech_IsShelfOk.Name = "coltech_IsShelfOk";
            this.coltech_IsShelfOk.OptionsColumn.AllowEdit = false;
            this.coltech_IsShelfOk.OptionsColumn.ReadOnly = true;
            this.coltech_IsShelfOk.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.coltech_IsShelfOk.UnboundExpression = "Iif(IsNull([F6]), \' \' , Iif([F6] == True, \'Да\' , \'Нет\') )";
            this.coltech_IsShelfOk.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // coltech_NumShelfSupport
            // 
            this.coltech_NumShelfSupport.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_NumShelfSupport.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_NumShelfSupport.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_NumShelfSupport.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_NumShelfSupport.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_NumShelfSupport.AutoFillDown = true;
            this.coltech_NumShelfSupport.Caption = "Количество полкодержателей";
            this.coltech_NumShelfSupport.FieldName = "F7";
            this.coltech_NumShelfSupport.Name = "coltech_NumShelfSupport";
            this.coltech_NumShelfSupport.OptionsColumn.AllowEdit = false;
            this.coltech_NumShelfSupport.OptionsColumn.ReadOnly = true;
            this.coltech_NumShelfSupport.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // coltech_IsWetClean
            // 
            this.coltech_IsWetClean.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_IsWetClean.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_IsWetClean.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_IsWetClean.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_IsWetClean.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_IsWetClean.AutoFillDown = true;
            this.coltech_IsWetClean.Caption = "Произведена влажная уборка шкафа";
            this.coltech_IsWetClean.FieldName = "F8-";
            this.coltech_IsWetClean.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.coltech_IsWetClean.Name = "coltech_IsWetClean";
            this.coltech_IsWetClean.OptionsColumn.AllowEdit = false;
            this.coltech_IsWetClean.OptionsColumn.ReadOnly = true;
            this.coltech_IsWetClean.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.coltech_IsWetClean.UnboundExpression = "Iif(IsNull([F8]), \' \' , Iif([F8] == True, \'Да\' , \'Нет\') )";
            this.coltech_IsWetClean.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // coltech_IsPackLabel
            // 
            this.coltech_IsPackLabel.AppearanceHeader.Options.UseTextOptions = true;
            this.coltech_IsPackLabel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltech_IsPackLabel.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.coltech_IsPackLabel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltech_IsPackLabel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltech_IsPackLabel.AutoFillDown = true;
            this.coltech_IsPackLabel.Caption = "Произведена упаковка, маркировка";
            this.coltech_IsPackLabel.FieldName = "F9-";
            this.coltech_IsPackLabel.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.coltech_IsPackLabel.Name = "coltech_IsPackLabel";
            this.coltech_IsPackLabel.OptionsColumn.AllowEdit = false;
            this.coltech_IsPackLabel.OptionsColumn.ReadOnly = true;
            this.coltech_IsPackLabel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.coltech_IsPackLabel.UnboundExpression = "Iif(IsNull([F9]), \' \' , Iif([F9] == True, \'Да\' , \'Нет\') )";
            this.coltech_IsPackLabel.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // bandSysPOS
            // 
            this.bandSysPOS.AppearanceHeader.Options.UseTextOptions = true;
            this.bandSysPOS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandSysPOS.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandSysPOS.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandSysPOS.Caption = "Системные данные";
            this.bandSysPOS.Columns.Add(this.colSysClassName);
            this.bandSysPOS.Columns.Add(this.colSysModelName);
            this.bandSysPOS.Columns.Add(this.colSysSerialNo);
            this.bandSysPOS.Columns.Add(this.colSysInvNumber);
            this.bandSysPOS.Columns.Add(this.colSysPOSBrand_Name);
            this.bandSysPOS.Columns.Add(this.colSysYearProduction);
            this.bandSysPOS.Columns.Add(this.colSysTechStatus);
            this.bandSysPOS.MinWidth = 20;
            this.bandSysPOS.Name = "bandSysPOS";
            this.bandSysPOS.OptionsBand.AllowMove = false;
            this.bandSysPOS.Width = 675;
            // 
            // colSysClassName
            // 
            this.colSysClassName.AppearanceHeader.Options.UseTextOptions = true;
            this.colSysClassName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSysClassName.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSysClassName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSysClassName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSysClassName.AutoFillDown = true;
            this.colSysClassName.Caption = "Тип оборудования";
            this.colSysClassName.FieldName = "POSGroup_Name";
            this.colSysClassName.Name = "colSysClassName";
            this.colSysClassName.OptionsColumn.AllowEdit = false;
            this.colSysClassName.OptionsColumn.ReadOnly = true;
            this.colSysClassName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSysClassName.Visible = true;
            this.colSysClassName.Width = 84;
            // 
            // colSysModelName
            // 
            this.colSysModelName.AppearanceHeader.Options.UseTextOptions = true;
            this.colSysModelName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSysModelName.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSysModelName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSysModelName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSysModelName.AutoFillDown = true;
            this.colSysModelName.Caption = "Модель";
            this.colSysModelName.FieldName = "POSType_Name";
            this.colSysModelName.Name = "colSysModelName";
            this.colSysModelName.OptionsColumn.AllowEdit = false;
            this.colSysModelName.OptionsColumn.ReadOnly = true;
            this.colSysModelName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSysModelName.Visible = true;
            // 
            // colSysSerialNo
            // 
            this.colSysSerialNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colSysSerialNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSysSerialNo.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSysSerialNo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSysSerialNo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSysSerialNo.AutoFillDown = true;
            this.colSysSerialNo.Caption = "Серийный номер";
            this.colSysSerialNo.FieldName = "SYS_Serial_No";
            this.colSysSerialNo.Name = "colSysSerialNo";
            this.colSysSerialNo.OptionsColumn.AllowEdit = false;
            this.colSysSerialNo.OptionsColumn.ReadOnly = true;
            this.colSysSerialNo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSysSerialNo.Visible = true;
            this.colSysSerialNo.Width = 89;
            // 
            // colSysInvNumber
            // 
            this.colSysInvNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colSysInvNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSysInvNumber.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSysInvNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSysInvNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSysInvNumber.AutoFillDown = true;
            this.colSysInvNumber.Caption = "Инвентарный номер";
            this.colSysInvNumber.FieldName = "Invent_no";
            this.colSysInvNumber.Name = "colSysInvNumber";
            this.colSysInvNumber.OptionsColumn.AllowEdit = false;
            this.colSysInvNumber.OptionsColumn.ReadOnly = true;
            this.colSysInvNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSysInvNumber.Visible = true;
            this.colSysInvNumber.Width = 89;
            // 
            // colSysPOSBrand_Name
            // 
            this.colSysPOSBrand_Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colSysPOSBrand_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSysPOSBrand_Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSysPOSBrand_Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSysPOSBrand_Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSysPOSBrand_Name.AutoFillDown = true;
            this.colSysPOSBrand_Name.Caption = "Бренд";
            this.colSysPOSBrand_Name.FieldName = "POSBrand_Name";
            this.colSysPOSBrand_Name.Name = "colSysPOSBrand_Name";
            this.colSysPOSBrand_Name.OptionsColumn.AllowEdit = false;
            this.colSysPOSBrand_Name.OptionsColumn.ReadOnly = true;
            this.colSysPOSBrand_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSysPOSBrand_Name.Visible = true;
            this.colSysPOSBrand_Name.Width = 114;
            // 
            // colSysYearProduction
            // 
            this.colSysYearProduction.AppearanceHeader.Options.UseTextOptions = true;
            this.colSysYearProduction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSysYearProduction.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSysYearProduction.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSysYearProduction.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSysYearProduction.AutoFillDown = true;
            this.colSysYearProduction.Caption = "Дата изготовления";
            this.colSysYearProduction.FieldName = "SYS_YearProduction";
            this.colSysYearProduction.Name = "colSysYearProduction";
            this.colSysYearProduction.OptionsColumn.AllowEdit = false;
            this.colSysYearProduction.OptionsColumn.ReadOnly = true;
            this.colSysYearProduction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSysYearProduction.Visible = true;
            this.colSysYearProduction.Width = 87;
            // 
            // colSysTechStatus
            // 
            this.colSysTechStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colSysTechStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSysTechStatus.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colSysTechStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSysTechStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSysTechStatus.AutoFillDown = true;
            this.colSysTechStatus.Caption = "Техническое состояние";
            this.colSysTechStatus.FieldName = "SYS_TechnicalCondition";
            this.colSysTechStatus.Name = "colSysTechStatus";
            this.colSysTechStatus.OptionsColumn.AllowEdit = false;
            this.colSysTechStatus.OptionsColumn.ReadOnly = true;
            this.colSysTechStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSysTechStatus.Visible = true;
            this.colSysTechStatus.Width = 137;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.gridBand1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand1.Caption = "Данные по М1/М2";
            this.gridBand1.Columns.Add(this.colM2);
            this.gridBand1.Columns.Add(this.colM1);
            this.gridBand1.Columns.Add(this.colM2Phone);
            this.gridBand1.Columns.Add(this.colM1Phone);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 447;
            // 
            // colM2
            // 
            this.colM2.AppearanceHeader.Options.UseTextOptions = true;
            this.colM2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM2.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM2.AutoFillDown = true;
            this.colM2.Caption = "М2";
            this.colM2.FieldName = "M2Name";
            this.colM2.Name = "colM2";
            this.colM2.OptionsColumn.AllowEdit = false;
            this.colM2.OptionsColumn.ReadOnly = true;
            this.colM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2.Visible = true;
            this.colM2.Width = 106;
            // 
            // colM1
            // 
            this.colM1.AppearanceHeader.Options.UseTextOptions = true;
            this.colM1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM1.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM1.AutoFillDown = true;
            this.colM1.Caption = "М1";
            this.colM1.FieldName = "M1Name";
            this.colM1.Name = "colM1";
            this.colM1.OptionsColumn.AllowEdit = false;
            this.colM1.OptionsColumn.ReadOnly = true;
            this.colM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1.Visible = true;
            this.colM1.Width = 95;
            // 
            // colM2Phone
            // 
            this.colM2Phone.AppearanceHeader.Options.UseTextOptions = true;
            this.colM2Phone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM2Phone.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM2Phone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM2Phone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM2Phone.AutoFillDown = true;
            this.colM2Phone.Caption = "Контактный телефон М2 ";
            this.colM2Phone.FieldName = "M2PhoneNumber";
            this.colM2Phone.Name = "colM2Phone";
            this.colM2Phone.OptionsColumn.AllowEdit = false;
            this.colM2Phone.OptionsColumn.ReadOnly = true;
            this.colM2Phone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2Phone.Visible = true;
            this.colM2Phone.Width = 124;
            // 
            // colM1Phone
            // 
            this.colM1Phone.AppearanceHeader.Options.UseTextOptions = true;
            this.colM1Phone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM1Phone.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM1Phone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM1Phone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM1Phone.AutoFillDown = true;
            this.colM1Phone.Caption = "Контактный телефон М1";
            this.colM1Phone.FieldName = "M1PhoneNumber";
            this.colM1Phone.Name = "colM1Phone";
            this.colM1Phone.OptionsColumn.AllowEdit = false;
            this.colM1Phone.OptionsColumn.ReadOnly = true;
            this.colM1Phone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Phone.Visible = true;
            this.colM1Phone.Width = 122;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.gridBand2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand2.Caption = "Данные, внесенные ответственным сотрудником, после обработки инвентаризации";
            this.gridBand2.Columns.Add(this.colInvStatusId);
            this.gridBand2.Columns.Add(this.colComment);
            this.gridBand2.Columns.Add(this.colLoginModifiedBy);
            this.gridBand2.Columns.Add(this.colLastModifiedDate);
            this.gridBand2.Columns.Add(this.colLastInvDate);
            this.gridBand2.Columns.Add(this.colLastApproveDate);
            this.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.FixedWidth = true;
            this.gridBand2.RowCount = 2;
            this.gridBand2.Width = 641;
            // 
            // colInvStatusId
            // 
            this.colInvStatusId.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvStatusId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvStatusId.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colInvStatusId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInvStatusId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInvStatusId.AutoFillDown = true;
            this.colInvStatusId.Caption = "Статус инвентаризации";
            this.colInvStatusId.ColumnEdit = this.riInvStatusLookUp;
            this.colInvStatusId.FieldName = "InventoryStatus";
            this.colInvStatusId.Name = "colInvStatusId";
            this.colInvStatusId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colInvStatusId.Visible = true;
            this.colInvStatusId.Width = 160;
            // 
            // riInvStatusLookUp
            // 
            this.riInvStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riInvStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("InventoryStatus", "Статус", 110, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.riInvStatusLookUp.DisplayMember = "InventoryStatus";
            this.riInvStatusLookUp.DropDownRows = 8;
            this.riInvStatusLookUp.Name = "riInvStatusLookUp";
            this.riInvStatusLookUp.NullText = "";
            this.riInvStatusLookUp.ShowFooter = false;
            this.riInvStatusLookUp.ShowHeader = false;
            this.riInvStatusLookUp.ValueMember = "InventoryStatus_ID";
            // 
            // colComment
            // 
            this.colComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComment.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colComment.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colComment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComment.AutoFillDown = true;
            this.colComment.Caption = "Комментарии";
            this.colComment.FieldName = "Comments";
            this.colComment.Name = "colComment";
            this.colComment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colComment.Visible = true;
            this.colComment.Width = 158;
            // 
            // colLoginModifiedBy
            // 
            this.colLoginModifiedBy.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoginModifiedBy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoginModifiedBy.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colLoginModifiedBy.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLoginModifiedBy.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLoginModifiedBy.AutoFillDown = true;
            this.colLoginModifiedBy.Caption = "Обработал, внес изменения";
            this.colLoginModifiedBy.FieldName = "ULM";
            this.colLoginModifiedBy.Name = "colLoginModifiedBy";
            this.colLoginModifiedBy.OptionsColumn.AllowEdit = false;
            this.colLoginModifiedBy.OptionsColumn.ReadOnly = true;
            this.colLoginModifiedBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLoginModifiedBy.Visible = true;
            this.colLoginModifiedBy.Width = 86;
            // 
            // colLastModifiedDate
            // 
            this.colLastModifiedDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colLastModifiedDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLastModifiedDate.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colLastModifiedDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLastModifiedDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLastModifiedDate.AutoFillDown = true;
            this.colLastModifiedDate.Caption = "Последняя дата внесения изменений по результатам инвентаризации";
            this.colLastModifiedDate.FieldName = "DLM";
            this.colLastModifiedDate.Name = "colLastModifiedDate";
            this.colLastModifiedDate.OptionsColumn.AllowEdit = false;
            this.colLastModifiedDate.OptionsColumn.FixedWidth = true;
            this.colLastModifiedDate.OptionsColumn.ReadOnly = true;
            this.colLastModifiedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLastModifiedDate.Visible = true;
            this.colLastModifiedDate.Width = 79;
            // 
            // colLastInvDate
            // 
            this.colLastInvDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colLastInvDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLastInvDate.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colLastInvDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLastInvDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLastInvDate.AutoFillDown = true;
            this.colLastInvDate.Caption = "Последняя дата инвентаризации";
            this.colLastInvDate.FieldName = "LastInventoryDate";
            this.colLastInvDate.Name = "colLastInvDate";
            this.colLastInvDate.OptionsColumn.AllowEdit = false;
            this.colLastInvDate.OptionsColumn.FixedWidth = true;
            this.colLastInvDate.OptionsColumn.ReadOnly = true;
            this.colLastInvDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLastInvDate.Visible = true;
            this.colLastInvDate.Width = 79;
            // 
            // colLastApproveDate
            // 
            this.colLastApproveDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colLastApproveDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLastApproveDate.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colLastApproveDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLastApproveDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLastApproveDate.AutoFillDown = true;
            this.colLastApproveDate.Caption = "Последняя дата подтверждения";
            this.colLastApproveDate.FieldName = "LastApproveDate";
            this.colLastApproveDate.Name = "colLastApproveDate";
            this.colLastApproveDate.OptionsColumn.AllowEdit = false;
            this.colLastApproveDate.OptionsColumn.FixedWidth = true;
            this.colLastApproveDate.OptionsColumn.ReadOnly = true;
            this.colLastApproveDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLastApproveDate.Visible = true;
            this.colLastApproveDate.Width = 79;
            // 
            // colInvStatusInit
            // 
            this.colInvStatusInit.Caption = "InvStatusInit";
            this.colInvStatusInit.FieldName = "InvStatusInitial";
            this.colInvStatusInit.Name = "colInvStatusInit";
            this.colInvStatusInit.OptionsColumn.AllowShowHide = false;
            this.colInvStatusInit.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colID
            // 
            this.colID.Caption = "IT_ID";
            this.colID.FieldName = "IT_ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowShowHide = false;
            this.colID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(900, 500);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riInvStatusLookUp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView gridView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInventoryWave;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRegion_name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCust_NAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colW_Code;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colW_Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colW_Location;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOL_id;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOLname;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOLDeliveryAddress;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOLTradingName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOLAddress;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDistrict_Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCntArea_name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCity_Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colModelName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSerial_No;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPOSBrand_Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYearProduction;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCorrectSerialNo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTechStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_IsSideWallsOk;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_IsDoorsOk;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_IsPackLabel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_IsScreenBrandOk;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_IsShelfOk;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_IsWetClean;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_NumShelf;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_NumShelfSupport;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn coltech_IsLowerGrateFront;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSysClassName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSysModelName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSysSerialNo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSysPOSBrand_Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSysYearProduction;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSysTechStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInvStatusId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit riInvStatusLookUp;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colComment;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLoginModifiedBy;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLastInvDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLastApproveDate;
        internal DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTerritory;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM2Phone;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM1Phone;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLastModifiedDate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandKPKData;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandKPKData1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandKPKDataOpt;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandKPKPOS;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandKPKTech;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandSysPOS;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInvStatusInit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rTextEdit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSysInvNumber;


    }
}
