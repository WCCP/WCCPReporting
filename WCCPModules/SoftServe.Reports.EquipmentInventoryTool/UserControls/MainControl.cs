﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Layout.Modes;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.EquipmentInventoryTool.Utility;

namespace SoftServe.Reports.EquipmentInventoryTool.UserControls
{
    [ToolboxItem(false)]
    public partial class MainControl : UserControl
    {
        private const string CompanyDirectory = "SoftServe";
        private const string ProgramDirectory = "WCCP Reporting";
        private const string FileName = "InventoryToolLayout.xml";

        /// <summary>
        /// Is used for storing column width before view modification before export
        /// </summary>
        private Dictionary<string, int> _currentColumnsWidth = new Dictionary<string, int>();

        /// <summary>
        /// Is used for storing column width before view modification before export
        /// </summary>
        private List<ColumnConfig> _currentColumnsConf = new List<ColumnConfig>();

        /// <summary>
        /// Is used for storing edited records
        /// </summary>
        private EditedRecords _editedRecords = new EditedRecords();

        private UserPermissions _userPermissions = DataProvider.GetUserPermissions();

        private DataTable _invStatusesList;

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl(UserPermissions permissions)
        {
            InitializeComponent();

            _userPermissions = permissions;
            _invStatusesList = DataProvider.GetInventoryStatusesList();

            riInvStatusLookUp.DataSource = _invStatusesList.Copy();

            foreach (BandedGridColumn column in gridView.Columns)
            {
                _currentColumnsWidth.Add(column.Name, column.Width);
            }
        }

        /// <summary>
        /// Gets <code>true</code> if there is no unsaved data
        /// </summary>
        public bool IsSaved
        {
            get
            {
                gridView.PostEditor();

                return _editedRecords.Records.Count == 0;
            }
        }

        /// <summary>
        /// Gets or sets the grid datasource
        /// </summary>
        public DataTable DataSource
        {
            get
            {
                return gridControl.DataSource as DataTable;
            }

            set
            {
                foreach (DataColumn col in value.Columns)
                {
                    if (col.DataType == typeof(DateTime))
                    {
                        foreach (DataRow dr in value.Rows)
                        {
                            if (dr[col] != DBNull.Value)
                            {
                                dr[col] = ConvertEx.ToDateTime(dr[col]).Date;
                            }

                        }
                    }
                }

                gridControl.BeginUpdate();
                gridControl.DataSource = value;
                gridControl.EndUpdate();
            }
        }

        /// <summary>
        /// Loads report data from database
        /// </summary>
        /// <param name="settings">Report settings</param>
        internal void LoadReportData(ReportInfo settings)
        {
            DataSource = DataProvider.GetMainReportData(settings);
        }

        /// <summary>
        /// Saves changes to database
        /// </summary>
        internal void SaveChanges()
        {
            gridView.PostEditor();

            foreach (InventoryRecord record in _editedRecords.Records.Values)
            {
                DataProvider.UpdateInventInfo(record);
            }

            if (_editedRecords.Records.Count > 0)
            {
                DataProvider.PerformMappingForNewlySavedData();
            }

            _editedRecords = new EditedRecords();
        }

        /// <summary>
        /// Changes view in order to export it properly 
        /// </summary>
        internal void PrepareViewToExport()
        {
            gridView.PostEditor();
           
            //save column conf
            _currentColumnsConf.Clear();

            foreach (BandedGridColumn column in gridView.Columns)
            {
                _currentColumnsConf.Add(new ColumnConfig(column.Width, column.ColIndex, column.RowIndex));
            }

            for (int i = 0; i < _currentColumnsConf.Count; i++)
            {
                gridView.SetColumnPosition(gridView.Columns[i], 0, i);
            }

            gridView.BestFitColumns();

            colLoginModifiedBy.BestFit();
            colLastModifiedDate.Width = 160;
            colLastInvDate.Width = 90;
            colLastApproveDate.Width = 90;
            colComment.BestFit();
            colInvStatusId.BestFit();

            colID.Visible = false;
        }

        /// <summary>
        /// Saves gridview layout for current user on disc
        /// </summary>
        internal void SaveLayout()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);

            if (!Directory.Exists(lCompanyDirPath))
            {
                Directory.CreateDirectory(lCompanyDirPath);
            }

            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);

            if (!Directory.Exists(lProgramDirPath))
            {
                Directory.CreateDirectory(lProgramDirPath);
            }

            string lFilePath = Path.Combine(lProgramDirPath, FileName);

            gridControl.MainView.SaveLayoutToXml(lFilePath);
        }

        /// <summary>
        /// Restores gridview layout for current user from file on disc
        /// </summary>
        internal void RestoreLayout()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);
            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);
            string lFilePath = Path.Combine(lProgramDirPath, FileName);

            if (File.Exists(lFilePath))
            {
                gridControl.MainView.RestoreLayoutFromXml(lFilePath);
            }
        }

        /// <summary>
        /// Handles ShowCustomizationForm event - is needed to show form over Report, not beneath
        /// </summary>
        /// <param name="sender">gridView</param>
        /// <param name="e">EventArgs</param>
        private void gridView_ShowCustomizationForm(object sender, EventArgs e)
        {
            gridView.CustomizationForm.TopMost = true;
        }

        /// <summary>
        /// Handles CellValueChanged of gridView
        /// </summary>
        /// <param name="sender">gridView</param>
        /// <param name="e">event args</param>
        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            long lId = (long)gridView.GetFocusedRowCellValue(colID);
            string lSerNum = gridView.GetFocusedRowCellValue(colCorrectSerialNo).ToString();

            object gh = gridView.GetFocusedRowCellValue(colInvStatusId);

            byte lInventStatus = (byte)gridView.GetFocusedRowCellValue(colInvStatusId);
            string lComment = gridView.GetFocusedRowCellValue(colComment).ToString();

            _editedRecords.Add(new InventoryRecord(lId, lSerNum, lInventStatus, lComment));
        }

        /// <summary>
        /// Handles ShowingEditor event of gridView
        /// </summary>
        /// <param name="sender">gridView</param>
        /// <param name="e">event args</param>
        private void gridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (_userPermissions == UserPermissions.Read)
            {
                e.Cancel = true;
                riInvStatusLookUp.Buttons[0].Visible = !e.Cancel;

                return;
            }

            if (gridView.FocusedColumn == colComment)// we can always edit comment
            {
                return;
            }

            if (gridView.FocusedColumn == colCorrectSerialNo)
            {
                if (!IsInventoryDataPresent(gridView.FocusedRowHandle) && !IsWarehouseInventarization(gridView.FocusedRowHandle))// SN could be edited only on warehouse if there is no InvInfo
                {
                    e.Cancel = true;
                    return;
                }
            }

            InventStatuses lRecordStatus = (InventStatuses)gridView.GetFocusedRowCellValue(colInvStatusInit);

            bool lCanRWEdit = lRecordStatus == InventStatuses.NonConfirmed
                             || lRecordStatus == InventStatuses.PartiallyConfirmed
                             || (lRecordStatus == InventStatuses.NonInvent && IsWarehouseInventarization(gridView.FocusedRowHandle));

            if (_userPermissions == UserPermissions.ReadWrite)
            {
                e.Cancel = !lCanRWEdit;
                riInvStatusLookUp.Buttons[0].Visible = !e.Cancel;
                return;
            }

            if (_userPermissions == UserPermissions.SuperMan)
            {
                bool lCanSMOnlyEdit = lRecordStatus == InventStatuses.EliminatedWithErrors
                                 || lRecordStatus == InventStatuses.Lost
                                 || lRecordStatus == InventStatuses.Found;

                e.Cancel = !lCanRWEdit && !lCanSMOnlyEdit;
                riInvStatusLookUp.Buttons[0].Visible = !e.Cancel;
            }
        }

        /// <summary>
        /// Filters inventory statuses list based on current status
        /// </summary>
        /// <param name="initStatus">Current status</param>
        /// <returns>The list of availeble statuses</returns>
        private DataTable FormStatusesDataSource(InventStatuses initStatus)
        {
            DataView lView = _invStatusesList.DefaultView;

            List<InventStatuses> lStatusList = new List<InventStatuses>();

            switch (initStatus)
            {
                case InventStatuses.NonConfirmed:
                    lStatusList.AddRange(new[]
                                       {
                                           InventStatuses.NonConfirmed, InventStatuses.Found,
                                           InventStatuses.InventWithDiffs, InventStatuses.EliminatedWithErrors, InventStatuses.Lost
                                       });
                    break;
                case InventStatuses.PartiallyConfirmed:
                    lStatusList.AddRange(new[] { InventStatuses.PartiallyConfirmed, InventStatuses.InventWithDiffs });
                    break;
                case InventStatuses.NonInvent://warehouse only
                    lStatusList.AddRange(new[] { InventStatuses.NonInvent, InventStatuses.InventWithDiffs, InventStatuses.Lost });
                    break;
                case InventStatuses.EliminatedWithErrors://SM only
                case InventStatuses.Lost://SM only
                case InventStatuses.Found://SM only
                    lStatusList.AddRange(new[] { InventStatuses.InventWithDiffs, InventStatuses.EliminatedWithErrors, InventStatuses.Lost, InventStatuses.Found });
                    break;
            }

            string lFilter = string.Join(",", (from InventStatuses status in lStatusList select ((byte)status).ToString()).ToArray());

            lView.RowFilter = string.Format("{0} in ({1})", riInvStatusLookUp.ValueMember, lFilter);

            return lView.ToTable();
        }

        /// <summary>
        /// Handles ShownEditor event of gridView
        /// </summary>
        /// <param name="sender">gridView</param>
        /// <param name="e">Event args</param>
        private void gridView_ShownEditor(object sender, EventArgs e)
        {
            if (gridView.FocusedColumn == colInvStatusId)
            {
                LookUpEdit editor = (LookUpEdit)gridView.ActiveEditor;

                if (!gridView.IsFilterRow(gridView.FocusedRowHandle))
                {
                    editor.Properties.DataSource = FormStatusesDataSource((InventStatuses)gridView.GetFocusedRowCellValue(colInvStatusInit));

                    int lCurStatusInt = Convert.ToInt32(gridView.GetFocusedRowCellValue(colInvStatusId));

                    editor.EditValue = lCurStatusInt;
                }
            }
        }

        /// <summary>
        /// Checks whether there is inventory data present
        /// </summary>
        private bool IsInventoryDataPresent(int rowHandle)//not OL part
        {
            return gridView.GetRowCellValue(rowHandle, colSerial_No) != DBNull.Value;
        }

        /// <summary>
        /// Checks whether this is warehouse inventarization
        /// </summary>
        private bool IsWarehouseInventarization(int rowHandle)
        {
            return gridView.GetRowCellValue(rowHandle, colOL_id) == DBNull.Value;
        }

        /// <summary>
        /// Handles drop down buttons visibility
        /// </summary>
        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridView.FocusedColumn == colInvStatusId && e.FocusedRowHandle < 0)
            {
                riInvStatusLookUp.Buttons[0].Visible = true;
            }
        }

        /// <summary>
        /// Handles columns resizing, especialy prevents for hidden ones - workaroud for DevEx bug
        /// </summary>
        private void gridView_ColumnWidthChanged(object sender, DevExpress.XtraGrid.Views.Base.ColumnEventArgs e)
        {
            Point clickPoint = gridControl.PointToClient(MousePosition);
            var hitInfo = gridView.CalcHitInfo(clickPoint);

            GridViewInfo gridViewInfo = (GridViewInfo)gridView.GetViewInfo();
            BandedGridViewInfo bandedGridViewInfo = (BandedGridViewInfo)gridView.GetViewInfo();

            GridColumnInfoArgs columnInfo = gridViewInfo.ColumnsInfo[e.Column];
            GridBandInfoArgs bandInfo = bandedGridViewInfo.BandsInfo[gridBand2];

            bool isColHiddenBeneathBand = hitInfo.Column == null || bandInfo.TopLeft.X < columnInfo.TopLeft.X + columnInfo.Column.Width;//hitInfo.Column.Width;
            
            if (isColHiddenBeneathBand && ((BandedGridColumn)e.Column).OwnerBand != gridBand2)
            {
                bool isColAjacentToBand = Math.Abs(bandInfo.TopLeft.X - (columnInfo.TopLeft.X + _currentColumnsWidth[e.Column.Name])) < 5;

                if (isColAjacentToBand)
                {
                    _currentColumnsWidth[e.Column.Name] = e.Column.Width;
                }
                else// cancel resizing if col is hidden & not ajacent to the last band
                {
                    e.Column.Width = _currentColumnsWidth[e.Column.Name];
                }
               
                return;
            }

            //handle resizing of columns in the last band
            if (isColHiddenBeneathBand && ((BandedGridColumn)e.Column).OwnerBand == gridBand2)
            {
                if (e.Column == colComment && e.Column.VisibleWidth > 300)
                {
                    e.Column.Width = 300;
                }

                if (e.Column == colLoginModifiedBy && e.Column.VisibleWidth > 100)
                {
                    e.Column.Width = 100;
                }

                if (e.Column == colInvStatusId && e.Column.VisibleWidth > 200)
                {
                    e.Column.Width = 200;
                }

                if ((e.Column == colLastModifiedDate || e.Column == colLastInvDate || e.Column == colLastApproveDate) && e.Column.VisibleWidth > 80)
                {
                    e.Column.Width = 100;
                }
            }

            //update current columns width
            _currentColumnsWidth[e.Column.Name] = e.Column.Width;
        }

        private void gridView_BeforeLoadLayout(object sender, DevExpress.Utils.LayoutAllowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.PreviousVersion))
            {
                e.Allow = false;
                return;
            }

            e.Allow = e.PreviousVersion == gridView.OptionsLayout.LayoutVersion;
        }
    }
}
