﻿using DevExpress.XtraPrinting;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.EquipmentInventoryTool.UserControls;
using SoftServe.Reports.EquipmentInventoryTool.Utility;

namespace SoftServe.Reports.EquipmentInventoryTool.Tabs
{
    public class MainTab : XtraTabPage
    {
        private MainControl _mainControl;

        internal MainControl MainControl
        {
            get { return _mainControl; }
        }

        private ReportInfo Settings { get; set; }

        /// <summary>
        /// Gets <code>true</code> if there is no unsaved data
        /// </summary>
        public bool IsDataSaved
        {
            get
            {
                return _mainControl == null || _mainControl.IsSaved;
            }
        }

        public MainTab()
        {
            ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            Text = "Основной отчет";
        }

        public MainTab(ReportInfo settings, UserPermissions permissions)
            : this()
        {
            Settings = settings;

            _mainControl = new MainControl(permissions);
            _mainControl.Dock = System.Windows.Forms.DockStyle.Fill;

            Controls.Add(_mainControl);
        }

        /// <summary>
        /// Loads report
        /// </summary>
        /// <param name="settings">Report settings</param>
        internal void LoadReport()
        {
            _mainControl.RestoreLayout();
            _mainControl.LoadReportData(Settings);
        }

        /// <summary>
        /// Saves changes to database
        /// </summary>
        internal void SaveChanges()
        {
            if (_mainControl != null)
            {
                _mainControl.SaveChanges();
            }
        }

        /// <summary>
        /// Performs export to xls
        /// </summary>
        /// <param name="filePath">Path to file</param>
        /// <param name="type">Output file format</param>
        internal void ExportToXls(string filePath, ExportToType type)
        {
            PrintingSystem lPrintingSystem = new PrintingSystem();
            PrintableComponentLink lPrintableComponentLink = new PrintableComponentLink();

            lPrintingSystem.Links.AddRange(new object[] { lPrintableComponentLink });

            _mainControl.SaveLayout();
            _mainControl.PrepareViewToExport();

            lPrintableComponentLink.Component = _mainControl.gridControl;
            lPrintableComponentLink.CreateDocument();

            _mainControl.RestoreLayout();

            switch (type)
            {
                case ExportToType.Xls: lPrintableComponentLink.PrintingSystem.ExportToXls(filePath, new XlsExportOptions { SheetName = "Inventory Tool" });
                    break;
                case ExportToType.Xlsx: lPrintableComponentLink.PrintingSystem.ExportToXlsx(filePath, new XlsxExportOptions { SheetName = "Inventory Tool" });
                    break;
            }
        }

        /// <summary>
        /// Performs disposing of the tab
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_mainControl != null)
            {
                _mainControl.SaveLayout();
            }

            base.Dispose(disposing);
        }
    }
}
