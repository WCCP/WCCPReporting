﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.EquipmentInventoryTool;
using SoftServe.Reports.EquipmentInventoryTool.Tabs;
using SoftServe.Reports.EquipmentInventoryTool.Utility;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        /// <summary>
        /// Report's settings form 
        /// </summary>
        readonly SettingsForm _settingsForm = new SettingsForm();

        private MainTab _mainTab;

        private UserPermissions _userPermissions;

        /// <summary>
        /// Initializes the new instance of <see cref="WccpUIControl"/> class
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();

            _userPermissions = DataProvider.GetUserPermissions();

            SetExportType(ExportToType.Pdf, false);
            SetExportType(ExportToType.Rtf, false);

            MenuButtonsRendering += WccpUIControl_MenuButtonsRendering;
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            CustomExport += OnCustomExport;
            RefreshClick += WccpUIControl_RefreshClick;
            SaveClick += WccpUIControl_SaveClick;

            GridLocalizer.Active = new RussianGridLocalizer();
            Localizer.Active = new RussianEditorsLocalizer();
        }

        public int ReportInit()
        {
            if (_settingsForm.ShowDialog() == DialogResult.OK)
            {
                LoadReport();
                return 0;
            }

            return 1;
        }

        /// <summary>
        /// Drives export activity
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="selectedPage">Page to be exported</param>
        /// <param name="type">Export type</param>
        /// <returns>Composite link to be exported</returns>
        private void OnCustomExport(object sender, CustomExportEventArgs customExportEventArgs)
        {
            if (!WasReportLoaded)
            {
                customExportEventArgs.Handled = true;
                return;
            }

            string lFilePath = SelectFilePath(customExportEventArgs.ExportType);

            if (!string.IsNullOrEmpty(lFilePath) && customExportEventArgs.ExportPage != null)
            {
                _mainTab.ExportToXls(lFilePath, customExportEventArgs.ExportType);
            }

            customExportEventArgs.Handled = true;
        }

        /// <summary>
        /// Handles Menu button visibilyty/accessibility
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="eventArgs">Event args</param>
        private void WccpUIControl_MenuButtonsRendering(object sender, MenuButtonsRenderingEventArgs eventArgs)
        {
            eventArgs.ShowExportAllBtn = false;
            eventArgs.ShowPrintAllBtn = false;
            eventArgs.ShowPrintBtn = false;
            eventArgs.ShowExportBtn = true;
            eventArgs.ShowSaveBtn = _userPermissions != UserPermissions.Read;
        }

        /// <summary>
        /// Handles Settings Form button click
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="selectedPage">Active page</param>
        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (_settingsForm.ShowDialog() == DialogResult.OK)
            {
                if (WasReportLoaded)
                {
                    _mainTab.MainControl.SaveLayout();
                }

                ReloadReport();
            }
        }

        /// <summary>
        /// Hanldes Refresh button click
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="selectedPage">Page to be refreshed</param>
        private void WccpUIControl_RefreshClick(object sender, XtraTabPage selectedPage)
        {
            if (WasReportLoaded)
            {
                _mainTab.MainControl.SaveLayout();
                ReloadReport();
            }
        }

        /// <summary>
        /// Hanldes Save button click
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="selectedPage">Page to be refreshed</param>
        private void WccpUIControl_SaveClick(object sender, XtraTabPage selectedPage)
        {
            if (WasReportLoaded && !_mainTab.IsDataSaved)
            {
                _mainTab.MainControl.SaveLayout();
                SaveChanges();
                LoadReport();
            }
        }

        /// <summary>
        /// Loads data from DB and displays it
        /// </summary>
        private void SaveChanges()
        {
            _mainTab.SaveChanges();
        }

        /// <summary>
        /// Loads data from DB and displays it
        /// </summary>
        private void LoadReport()
        {
            tabManager.TabPages.Clear();

            WaitManager.StartWait();
            try
            {
                ReportInfo settings = _settingsForm.GetSettingsData();

                _mainTab = new MainTab(settings, _userPermissions);
                tabManager.TabPages.Add(_mainTab);

                _mainTab.LoadReport();
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        /// <summary>
        /// Reloads data from DB and displays it
        /// </summary>
        private void ReloadReport()
        {
            if (_mainTab.IsDataSaved)
            {
                DataProvider.PerformMappingOfNewlyLoadedData();
                LoadReport();
            }
            else
            {
                DialogResult lResult = XtraMessageBox.Show("Некоторые изменения не сохранены и могут быть утеряны. Сохранить изменения?",
                                                       "Внимание", MessageBoxButtons.YesNoCancel);
                switch (lResult)
                {
                    case DialogResult.Yes:
                        SaveChanges();

                        DataProvider.PerformMappingOfNewlyLoadedData();
                        LoadReport();
                        break;
                    case DialogResult.No:

                        DataProvider.PerformMappingOfNewlyLoadedData();
                        LoadReport();
                        break;
                }
            }
        }

        /// <summary>
        /// Returns <c>true</c> if report has already been loaded
        /// </summary>
        private bool WasReportLoaded
        {
            get { return _mainTab.MainControl != null; }
        }
    }
}
