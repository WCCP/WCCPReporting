﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.BaseReportControl
{
    /// <summary>
    /// Represent class that holds validation infromation.
    /// </summary>
    public class ValidateMetadata
    {
        /// <summary>
        /// Gets or sets value that indicates whether validation is correct.
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// Gets or sets message that is used to describe validation problem.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Creates instance of ValidateMetadata class.
        /// </summary>
        public ValidateMetadata() { }

        /// <summary>
        /// Creates instance of ValidateMetadata class.
        /// </summary>
        /// <param name="result">Result of the validation</param>
        /// <param name="message">Validation message</param>
        public ValidateMetadata(bool result, string message)
        {
            this.IsCorrect = result;
            this.Message = message;
        }
    }
}
