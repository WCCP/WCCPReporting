﻿using System;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Base.Handler;
using DevExpress.XtraGrid.Views.Base.ViewInfo;
using DevExpress.XtraGrid.Registrator;


namespace Logica.Reports.BaseReportControl
{
    public class CustomGridViewInfoRegistrator : BandedGridInfoRegistrator
    {
        public override string ViewName { get { return "CustomGridView"; } }
        public override BaseView CreateView(GridControl grid)
        {
            return new CustomGridView(grid as
                GridControl);
        }
        public override BaseViewInfo CreateViewInfo(BaseView view)
        {
            return new CustomGridViewInfo(view as
                CustomGridView);
        }
        public override BaseViewHandler CreateHandler(BaseView view)
        {
            return new CustomGridHandler(view as
                CustomGridView);
        }
    }

}
