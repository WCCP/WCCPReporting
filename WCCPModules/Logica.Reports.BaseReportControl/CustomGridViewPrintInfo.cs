﻿using System;
using System.Drawing;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Printing;
using DevExpress.XtraPrinting;
using Logica.Reports.ConfigXmlParser.GridExtension;
using DevExpress.Data;


namespace Logica.Reports.BaseReportControl
{
	public class CustomGridViewPrintInfo : BandedGridViewPrintInfo
	{
		#region Constructors

		public CustomGridViewPrintInfo(PrintInfoArgs args)
			: base(args)
		{
		}

		#endregion

		#region Instance Methods
        public int FooterPanelHeight
        {
            get
            {
                return CalcStyleHeight(AppearancePrint.FooterPanel) + 4;
            }
        }

		public override void PrintHeader(IBrickGraphics graph)
		{
			PrintBandHeader(graph);
			CustomDrawHeaderCells(graph);
		}

        public override void PrintFooterPanel(IBrickGraphics graph)
        {
            Bricks["FooterPanel"].Font = new Font("Verdana", 12);
            SetDefaultBrickStyle(graph, Bricks["FooterPanel"]);
            base.PrintFooterPanel(graph);
            CustomDrawFooterCells(graph);


        }

        private void CustomDrawFooterCells(IBrickGraphics graph)
        {
            if (!View.OptionsPrint.PrintFooter) return;
            foreach (PrintColumnInfo colInfo in Columns)
            {
                if (colInfo.Column.SummaryItem.SummaryType == SummaryItemType.None) continue;
                Rectangle r = Rectangle.Empty;
                r.X = colInfo.Bounds.X + Indent;
                r.Y = colInfo.RowIndex * FooterPanelHeight + 2 + Y;
                r.Width = colInfo.Bounds.Width;
                r.Height = FooterPanelHeight * colInfo.RowCount;
                r.X -= Indent;
                r.Y -= r.Height;
                string text = string.Empty;
                TextBrick ib = GetTextBrick(colInfo, r, out text);
                if (ib != null)
                    graph.DrawBrick(ib, ib.Rect);
            }
        }

         private TextBrick GetTextBrick(PrintColumnInfo colInfo, Rectangle rect, out string displayText)
        {
            Bitmap bmp = new Bitmap(rect.Width, rect.Height);
            GraphicsCache cache = new GraphicsCache(Graphics.FromImage(bmp));
            FooterCellCustomDrawEventArgs args = (View as CustomGridView).GetCustomDrawCellArgs(cache, rect,
            colInfo.Column);
            displayText = args.Info.DisplayText;
            if (!args.Handled)
                return null;
            BorderSide border = args.Appearance.Options.UseBorderColor? BorderSide.All: BorderSide.None;
            
            TextBrick tb = new TextBrick(border, 1, args.Appearance.BorderColor, args.Appearance.BackColor, args.Appearance.ForeColor);
            tb.Rect = rect;
            //ib.Image = bmp;
            return tb;
        }
    


		protected override void PrintBandHeader(IBrickGraphics graph)
		{
			if (!View.OptionsPrint.PrintBandHeader || !View.OptionsView.ShowBands) return;

			Rectangle empty = Rectangle.Empty;
			Point pos = new Point(Indent, 0);
			bool usePrintStyles = View.OptionsPrint.UsePrintStyles;
            //Bricks["BandPanel"].Font = new Font("Verdana", 12);
            
			SetDefaultBrickStyle(graph, Bricks["BandPanel"]);
			foreach (PrintBandInfo info in Bands)
			{
				GroupBand groupBand = info.Band as GroupBand;
				if (groupBand == null || !groupBand.VerticalCaption)
				{
					empty = info.Bounds;
					empty.Offset(pos);
					AppearanceObject target = new AppearanceObject();
					AppearanceHelper.Combine(target, new[] {info.Band.AppearanceHeader, AppearancePrint.BandPanel});
					SetDefaultBrickStyle(graph, Bricks.Create(target, BorderSide.All, target.BorderColor, 1));
					DrawTextBrick(graph, info.Band.GetTextCaption(), empty);
				}
				else
				{
					empty = info.Bounds;
					empty.Offset(pos);
					string text;
					ImageBrick ib;
					GetVerticalImageBrick(info, empty, out text, out ib);
					if (ib != null)
					{
						graph.DrawBrick(ib, ib.Rect);
					}
				}
			}
		}

		private Rectangle CreateVerticalHeader(Rectangle rect, string displayText, Bitmap bmp, AppearanceObject appearanceObject)
		{
			string formattedText = displayText;
			using (Graphics g = Graphics.FromImage(bmp))
			{
				g.TranslateTransform((float) rect.Width, (float) rect.Height);
				//rotate
				g.RotateTransform(270);
				//move image back
				g.TranslateTransform(-(float) rect.Width, -(float) rect.Height);

				g.DrawImage(bmp, new Point(rect.X, rect.Y));

				Rectangle r = new Rectangle(rect.Width + 1, rect.Height - rect.Width + 1, rect.Height - 1, rect.Width - 1);
				g.DrawString(formattedText, appearanceObject.GetFont(), new SolidBrush(appearanceObject.GetForeColor()), r, appearanceObject.GetStringFormat());
			}
			return rect;
		}

		private void CustomDrawHeaderCells(IBrickGraphics graph)
		{
			if (!View.OptionsPrint.PrintHeader || !View.OptionsView.ShowColumnHeaders) return;
			Point pos = new Point(Indent, HeaderY);
			Rectangle empty = Rectangle.Empty;
			SetDefaultBrickStyle(graph, Bricks["HeaderPanel"]);
			foreach (PrintColumnInfo info in Columns)
			{
				DynamicColumn dynamicColumn = info.Column as DynamicColumn;
				empty = info.Bounds;
				empty.Offset(pos);
				if (dynamicColumn == null || !dynamicColumn.VerticalCaption)
				{
					AppearanceObject target = new AppearanceObject();
					AppearanceHelper.Combine(target, new[] {info.Column.AppearanceHeader, AppearancePrint.HeaderPanel});
					SetDefaultBrickStyle(graph, Bricks.Create(target, BorderSide.All, target.BorderColor, 1));
					string textCaption = info.Column.GetTextCaption();
					if (!ColumnsInfo[Columns.IndexOf(info)].Column.OptionsColumn.ShowCaption)
					{
						textCaption = string.Empty;
					}
					DrawTextBrick(graph, textCaption, empty);
				}
				else
				{
					string text;
					ImageBrick ib;
					GetVerticalImageBrick(info, empty, out text, out ib);
					if (ib != null)
					{
						graph.DrawBrick(ib, ib.Rect);
					}
				}
			}
		}

		private void GetVerticalImageBrick(PrintBandInfo colInfo, Rectangle rect,
		                                   out string displayText, out ImageBrick imageBrick /*, out TextBrick textBrick*/)
		{
			try
			{
				Bitmap bmp = new Bitmap(rect.Width, rect.Height);
				GraphicsCache cache = new GraphicsCache(Graphics.FromImage(bmp));
				BandHeaderCustomDrawEventArgs args = (View as CustomGridView).GetCustomDrawBandArgs(cache, rect, colInfo.Band);

				displayText = String.Empty;

				/*if (!args.Handled)
            return null;*/

				GroupBand groupBand = args.Band as GroupBand;
				displayText = groupBand.CustomizationCaption;
				if (groupBand.CustomizationCaption == String.Empty)
				{
					displayText = groupBand.Caption;
				}
				displayText = displayText.Replace("\\n", "replace").Replace("replace", "\n");
				BorderSide border = BorderSide.All;
				imageBrick = new ImageBrick(border, 1, Color.Gray, Color.White);
				//textBrick = new TextBrick(border, 0, Color.Gray, Color.White, Color.White);
				rect = CreateVerticalHeader(rect, displayText, bmp, colInfo.Band.AppearanceHeader);
				imageBrick.Rect = rect;
				imageBrick.Image = bmp;
			}
			catch (Exception ex)
			{
				//System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
				displayText = "";
				imageBrick = null;
				//textBrick = null;
			}
		}

		private void GetVerticalImageBrick(PrintColumnInfo colInfo, Rectangle rect,
		                                   out string displayText, out ImageBrick imageBrick /*, out TextBrick textBrick*/)
		{
			try
			{
				Bitmap bmp = new Bitmap(rect.Width, rect.Height);
				GraphicsCache cache = new GraphicsCache(Graphics.FromImage(bmp));
				ColumnHeaderCustomDrawEventArgs args = (View as CustomGridView).GetCustomDrawHeaderArgs(cache, rect, colInfo.Column);

				displayText = String.Empty;

				/*if (!args.Handled)
            return null;*/

				DynamicColumn dynamicColumn = args.Column as DynamicColumn;
				displayText = dynamicColumn.CustomizationCaption;
				if (dynamicColumn.CustomizationCaption == String.Empty)
				{
					displayText = dynamicColumn.Caption;
				}
				displayText = displayText.Replace("\\n", "replace").Replace("replace", "\n");
				BorderSide border = BorderSide.All;
				imageBrick = new ImageBrick(border, 1, Color.Gray, Color.White);
				//textBrick = new TextBrick(border, 0, Color.Gray, Color.White, Color.White);
				rect = CreateVerticalHeader(rect, displayText, bmp, dynamicColumn.AppearanceHeader);
				imageBrick.Rect = rect;
				imageBrick.Image = bmp;
			}
			catch (Exception ex)
			{
				//System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
				displayText = "";
				imageBrick = null;
				//textBrick = null;
			}
		}

		#endregion
	}
}