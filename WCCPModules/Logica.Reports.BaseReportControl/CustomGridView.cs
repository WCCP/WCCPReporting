﻿using System;
using DevExpress.XtraGrid.Views.Base;
using System.Drawing;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils.Drawing;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.BandedGrid;


namespace Logica.Reports.BaseReportControl
{
    [System.ComponentModel.DesignerCategory("")]
    public class CustomGridView : Logica.Reports.ConfigXmlParser.GridExtension.GroupBandedView
    {
        public CustomGridView() : this(null) { }
        public CustomGridView(DevExpress.XtraGrid.GridControl grid)
            : base(grid)
        {
            // put your initialization code here
        }
        protected override string ViewName { get { return "CustomGridView"; } }

        protected override DevExpress.XtraGrid.Views.Printing.BaseViewPrintInfo CreatePrintInfoInstance(DevExpress.XtraGrid.Views.Printing.PrintInfoArgs args)
        {
            return new CustomGridViewPrintInfo(args);
        }


        public ColumnHeaderCustomDrawEventArgs GetCustomDrawHeaderArgs(GraphicsCache cache, Rectangle rect, GridColumn col)
        {
            GridColumnInfoArgs styleArgs = new GridColumnInfoArgs(cache, col);
            styleArgs.Bounds = new Rectangle(new Point(0, 0), rect.Size);
            ColumnHeaderCustomDrawEventArgs args = new ColumnHeaderCustomDrawEventArgs(cache, null, styleArgs);

            //RaiseCustomDrawColumnHeader(args);
            args.Handled = true;
            return args;
        }
        public BandHeaderCustomDrawEventArgs GetCustomDrawBandArgs(GraphicsCache cache, Rectangle rect, GridBand col)
        {
            GridBandInfoArgs styleArgs = new GridBandInfoArgs(col, cache);
            styleArgs.Bounds = new Rectangle(new Point(0, 0), rect.Size);
            BandHeaderCustomDrawEventArgs args = new BandHeaderCustomDrawEventArgs(cache, null, styleArgs);

            //RaiseCustomDrawColumnHeader(args);
            args.Handled = true;
            return args;
        }

        public FooterCellCustomDrawEventArgs GetCustomDrawCellArgs(GraphicsCache cache, Rectangle rect,
        GridColumn col)
        {
            GridFooterCellInfoArgs styleArgs = new GridFooterCellInfoArgs(cache);
            styleArgs.Bounds = new Rectangle(new Point(0, 0), rect.Size);
            FooterCellCustomDrawEventArgs args = new FooterCellCustomDrawEventArgs(cache, -1, col, null, styleArgs);
            args.Handled = true;
            RaiseCustomDrawFooterCell(args);
            return args;
        }

    }

}
