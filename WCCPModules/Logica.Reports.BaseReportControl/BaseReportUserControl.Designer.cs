﻿namespace Logica.Reports.BaseReportControl
{
    partial class BaseReportUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseReportUserControl));
            this.barMenuManager = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnSettings = new DevExpress.XtraBars.BarButtonItem();
            this.btnSave = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportAllToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToHtml = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToMht = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToRtf = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToText = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToImage = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportAllToXlsx = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarSubItem();
            this.btnExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToHtml = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToMht = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToRtf = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToText = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToImage = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportToXlsx = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrintAll = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.btnSaveForPDA = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.imCollection = new DevExpress.Utils.ImageCollection();
            this.tabManager = new DevExpress.XtraTab.XtraTabControl();
            this.lblTab = new DevExpress.XtraEditors.LabelControl();
            this.cmbTab = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTab.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barMenuManager
            // 
            this.barMenuManager.AllowCustomization = false;
            this.barMenuManager.AllowMoveBarOnToolbar = false;
            this.barMenuManager.AllowQuickCustomization = false;
            this.barMenuManager.AllowShowToolbarsPopup = false;
            this.barMenuManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barMenuManager.Controller = this.barAndDockingController1;
            this.barMenuManager.DockControls.Add(this.barDockControlTop);
            this.barMenuManager.DockControls.Add(this.barDockControlBottom);
            this.barMenuManager.DockControls.Add(this.barDockControlLeft);
            this.barMenuManager.DockControls.Add(this.barDockControlRight);
            this.barMenuManager.DockControls.Add(this.standaloneBarDockControl1);
            this.barMenuManager.Form = this;
            this.barMenuManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSettings,
            this.btnClose,
            this.btnPrint,
            this.btnRefresh,
            this.btnExportTo,
            this.btnExportToExcel,
            this.btnExportToPdf,
            this.btnExportToHtml,
            this.btnExportToMht,
            this.btnExportToRtf,
            this.btnExportToText,
            this.btnExportToImage,
            this.btnExportToCsv,
            this.btnExportToXlsx,
            this.btnExportAllTo,
            this.btnExportAllToExcel,
            this.btnExportAllToPdf,
            this.btnExportAllToHtml,
            this.btnExportAllToMht,
            this.btnExportAllToRtf,
            this.btnExportAllToText,
            this.btnExportAllToImage,
            this.btnExportAllToCsv,
            this.btnExportAllToXlsx,
            this.btnSave,
            this.btnPrintAll,
            this.barButtonItem1,
            this.btnSaveForPDA});
            this.barMenuManager.LargeImages = this.imCollection;
            this.barMenuManager.MaxItemId = 43;
            // 
            // bar1
            // 
            this.bar1.BarName = "menuBar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(74, 165);
            this.bar1.FloatSize = new System.Drawing.Size(48, 32);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSettings),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSave),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportAllTo, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTo, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrintAll),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSaveForPDA)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "menuBar";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Обновить";
            this.btnRefresh.Id = 6;
            this.btnRefresh.LargeImageIndex = 0;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnSettings
            // 
            this.btnSettings.Caption = "Параметры отчета";
            this.btnSettings.Id = 3;
            this.btnSettings.LargeImageIndex = 6;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSettings_ItemClick);
            // 
            // btnSave
            // 
            this.btnSave.Caption = "Сохранить";
            this.btnSave.Id = 35;
            this.btnSave.LargeImageIndex = 7;
            this.btnSave.Name = "btnSave";
            this.btnSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_ItemClick);
            // 
            // btnExportAllTo
            // 
            this.btnExportAllTo.Caption = "Экспортировать все в...";
            this.btnExportAllTo.Id = 19;
            this.btnExportAllTo.LargeImageIndex = 14;
            this.btnExportAllTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToHtml),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToMht),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToRtf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToText),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToImage),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportAllToXlsx)});
            this.btnExportAllTo.Name = "btnExportAllTo";
            // 
            // btnExportAllToExcel
            // 
            this.btnExportAllToExcel.Caption = "Xls";
            this.btnExportAllToExcel.Id = 20;
            this.btnExportAllToExcel.LargeImageIndex = 3;
            this.btnExportAllToExcel.Name = "btnExportAllToExcel";
            this.btnExportAllToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToPdf
            // 
            this.btnExportAllToPdf.Caption = "Pdf";
            this.btnExportAllToPdf.Id = 22;
            this.btnExportAllToPdf.LargeImageIndex = 13;
            this.btnExportAllToPdf.Name = "btnExportAllToPdf";
            this.btnExportAllToPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToHtml
            // 
            this.btnExportAllToHtml.Caption = "Html";
            this.btnExportAllToHtml.Id = 23;
            this.btnExportAllToHtml.LargeImageIndex = 9;
            this.btnExportAllToHtml.Name = "btnExportAllToHtml";
            this.btnExportAllToHtml.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToMht
            // 
            this.btnExportAllToMht.Caption = "Mht";
            this.btnExportAllToMht.Id = 24;
            this.btnExportAllToMht.LargeImageIndex = 11;
            this.btnExportAllToMht.Name = "btnExportAllToMht";
            this.btnExportAllToMht.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToRtf
            // 
            this.btnExportAllToRtf.Caption = "Rtf";
            this.btnExportAllToRtf.Id = 25;
            this.btnExportAllToRtf.LargeImageIndex = 8;
            this.btnExportAllToRtf.Name = "btnExportAllToRtf";
            this.btnExportAllToRtf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToText
            // 
            this.btnExportAllToText.Caption = "Text";
            this.btnExportAllToText.Id = 26;
            this.btnExportAllToText.LargeImageIndex = 12;
            this.btnExportAllToText.Name = "btnExportAllToText";
            this.btnExportAllToText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToImage
            // 
            this.btnExportAllToImage.Caption = "Image";
            this.btnExportAllToImage.Id = 26;
            this.btnExportAllToImage.LargeImageIndex = 16;
            this.btnExportAllToImage.Name = "btnExportAllToImage";
            this.btnExportAllToImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToCsv
            // 
            this.btnExportAllToCsv.Caption = "Csv";
            this.btnExportAllToCsv.Id = 26;
            this.btnExportAllToCsv.LargeImageIndex = 17;
            this.btnExportAllToCsv.Name = "btnExportAllToCsv";
            this.btnExportAllToCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportAllToXlsx
            // 
            this.btnExportAllToXlsx.Caption = "Xlsx";
            this.btnExportAllToXlsx.Id = 26;
            this.btnExportAllToXlsx.LargeImageIndex = 15;
            this.btnExportAllToXlsx.Name = "btnExportAllToXlsx";
            this.btnExportAllToXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportAllTo_ItemClick);
            // 
            // btnExportTo
            // 
            this.btnExportTo.Caption = "Экспортировать в ...";
            this.btnExportTo.Id = 10;
            this.btnExportTo.LargeImageIndex = 10;
            this.btnExportTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportToExcel, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToHtml),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToMht),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToRtf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToText),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToImage),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToXlsx)});
            this.btnExportTo.Name = "btnExportTo";
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Caption = "Xml";
            this.btnExportToExcel.Id = 11;
            this.btnExportToExcel.LargeImageIndex = 3;
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToPdf
            // 
            this.btnExportToPdf.Caption = "Pdf";
            this.btnExportToPdf.Id = 12;
            this.btnExportToPdf.LargeImageIndex = 13;
            this.btnExportToPdf.Name = "btnExportToPdf";
            this.btnExportToPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToHtml
            // 
            this.btnExportToHtml.Caption = "Html";
            this.btnExportToHtml.Id = 13;
            this.btnExportToHtml.LargeImageIndex = 9;
            this.btnExportToHtml.Name = "btnExportToHtml";
            this.btnExportToHtml.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToMht
            // 
            this.btnExportToMht.Caption = "Mht";
            this.btnExportToMht.Id = 14;
            this.btnExportToMht.LargeImageIndex = 11;
            this.btnExportToMht.Name = "btnExportToMht";
            this.btnExportToMht.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToRtf
            // 
            this.btnExportToRtf.Caption = "Rtf";
            this.btnExportToRtf.Id = 15;
            this.btnExportToRtf.LargeImageIndex = 8;
            this.btnExportToRtf.Name = "btnExportToRtf";
            this.btnExportToRtf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToText
            // 
            this.btnExportToText.Caption = "Text";
            this.btnExportToText.Id = 16;
            this.btnExportToText.LargeImageIndex = 12;
            this.btnExportToText.Name = "btnExportToText";
            this.btnExportToText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToImage
            // 
            this.btnExportToImage.Caption = "Image";
            this.btnExportToImage.Id = 16;
            this.btnExportToImage.LargeImageIndex = 16;
            this.btnExportToImage.Name = "btnExportToImage";
            this.btnExportToImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToCsv
            // 
            this.btnExportToCsv.Caption = "Csv";
            this.btnExportToCsv.Id = 16;
            this.btnExportToCsv.LargeImageIndex = 17;
            this.btnExportToCsv.Name = "btnExportToCsv";
            this.btnExportToCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnExportToXlsx
            // 
            this.btnExportToXlsx.Caption = "Xlsx";
            this.btnExportToXlsx.Id = 16;
            this.btnExportToXlsx.LargeImageIndex = 15;
            this.btnExportToXlsx.Name = "btnExportToXlsx";
            this.btnExportToXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
            // 
            // btnPrintAll
            // 
            this.btnPrintAll.Caption = "Печать все";
            this.btnPrintAll.Id = 39;
            this.btnPrintAll.LargeImageIndex = 18;
            this.btnPrintAll.Name = "btnPrintAll";
            this.btnPrintAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrintAll_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "Печать";
            this.btnPrint.Id = 5;
            this.btnPrint.LargeImageIndex = 19;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
            // 
            // btnSaveForPDA
            // 
            this.btnSaveForPDA.Caption = "Планы для КПК";
            this.btnSaveForPDA.Id = 42;
            this.btnSaveForPDA.LargeImageIndex = 20;
            this.btnSaveForPDA.Name = "btnSaveForPDA";
            this.btnSaveForPDA.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnSaveForPDA.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSaveForPDA_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(648, 32);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(648, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 480);
            this.barDockControlBottom.Size = new System.Drawing.Size(648, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 480);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(648, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 480);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "Закрыть лист";
            this.btnClose.Id = 4;
            this.btnClose.LargeImageIndex = 2;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "d";
            this.barButtonItem1.Id = 41;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "refresh_24.png");
            this.imCollection.Images.SetKeyName(1, "briefcase_prev_24.png");
            this.imCollection.Images.SetKeyName(2, "del_24.png");
            this.imCollection.Images.SetKeyName(3, "Excel_42.bmp");
            this.imCollection.Images.SetKeyName(4, "ExcelAll_42.bmp");
            this.imCollection.Images.SetKeyName(5, "print_24.png");
            this.imCollection.Images.SetKeyName(6, "briefcase_ok_24.png");
            this.imCollection.Images.SetKeyName(7, "briefcase_save_24.png");
            this.imCollection.Images.SetKeyName(15, "xlsx.png");
            this.imCollection.Images.SetKeyName(16, "bmp.PNG");
            this.imCollection.Images.SetKeyName(17, "csv.PNG");
            this.imCollection.Images.SetKeyName(18, "copy_prev_24.png");
            this.imCollection.Images.SetKeyName(19, "doc_prev_24.png");
            this.imCollection.Images.SetKeyName(20, "pda (2).png");
            // 
            // tabManager
            // 
            this.tabManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabManager.HeaderButtons = ((DevExpress.XtraTab.TabButtons)((DevExpress.XtraTab.TabButtons.Close | DevExpress.XtraTab.TabButtons.Default)));
            this.tabManager.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tabManager.Location = new System.Drawing.Point(0, 32);
            this.tabManager.Name = "tabManager";
            this.tabManager.Size = new System.Drawing.Size(648, 448);
            this.tabManager.TabIndex = 9;
            this.tabManager.CloseButtonClick += new System.EventHandler(this.tabManager_CloseButtonClick);
            // 
            // lblTab
            // 
            this.lblTab.Location = new System.Drawing.Point(346, 10);
            this.lblTab.Name = "lblTab";
            this.lblTab.Size = new System.Drawing.Size(87, 13);
            this.lblTab.TabIndex = 11;
            this.lblTab.Text = "Выбор закладки:";
            this.lblTab.Visible = false;
            // 
            // cmbTab
            // 
            this.cmbTab.Location = new System.Drawing.Point(439, 7);
            this.cmbTab.MenuManager = this.barMenuManager;
            this.cmbTab.Name = "cmbTab";
            this.cmbTab.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTab.Size = new System.Drawing.Size(200, 20);
            this.cmbTab.TabIndex = 12;
            this.cmbTab.Visible = false;
            // 
            // BaseReportUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmbTab);
            this.Controls.Add(this.lblTab);
            this.Controls.Add(this.tabManager);
            this.Controls.Add(this.standaloneBarDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "BaseReportUserControl";
            this.Size = new System.Drawing.Size(648, 480);
            this.SizeChanged += new System.EventHandler(this.BaseReportUserControl_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTab.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected DevExpress.XtraBars.BarManager barMenuManager;
        protected DevExpress.XtraBars.BarDockControl barDockControlTop;
        protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
        protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
        protected DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected DevExpress.Utils.ImageCollection imCollection;
        protected DevExpress.XtraBars.Bar bar1;
        protected DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        protected DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        protected DevExpress.XtraBars.BarButtonItem btnSettings;
        protected DevExpress.XtraBars.BarButtonItem btnClose;
        protected DevExpress.XtraBars.BarButtonItem btnRefresh;
        protected DevExpress.XtraBars.BarButtonItem btnExportToExcel;
        protected DevExpress.XtraBars.BarButtonItem btnExportToPdf;
        protected DevExpress.XtraBars.BarButtonItem btnExportToHtml;
        protected DevExpress.XtraBars.BarButtonItem btnExportToMht;
        protected DevExpress.XtraBars.BarButtonItem btnExportToRtf;
        protected DevExpress.XtraBars.BarButtonItem btnExportToText;
        protected DevExpress.XtraBars.BarButtonItem btnExportToImage;
        protected DevExpress.XtraBars.BarButtonItem btnExportToCsv;
        protected DevExpress.XtraBars.BarButtonItem btnExportToXlsx;
        public DevExpress.XtraBars.BarSubItem btnExportAllTo;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToExcel;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToPdf;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToHtml;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToMht;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToRtf;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToText;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToImage;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToCsv;
        protected DevExpress.XtraBars.BarButtonItem btnExportAllToXlsx;
        protected DevExpress.XtraBars.BarButtonItem btnSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        public DevExpress.XtraTab.XtraTabControl tabManager;
        public DevExpress.XtraEditors.LabelControl lblTab;
        public DevExpress.XtraEditors.ComboBoxEdit cmbTab;
        public DevExpress.XtraBars.BarButtonItem btnPrint;
        public DevExpress.XtraBars.BarButtonItem btnPrintAll;
        public DevExpress.XtraBars.BarButtonItem btnSaveForPDA;
        public DevExpress.XtraBars.BarSubItem btnExportTo;
    }
}
