﻿using System;
using System.Windows.Forms;
using System.Data;
using System.ComponentModel;
using DevExpress.XtraEditors;
using System.Globalization;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DateCombo : UserControl
    {
        private const string COLUMN_DAYS = "День";
        private const string COLUMN_MONTHS = "Месяц";
        private const string COLUMN_YEARS = "Год";
        private const string COLUMN_VALUE = "Значение";

        private const int MIN_YEAR = 2000;
        private const int MAX_YEAR = 2030;

        /// <summary>
        /// 
        /// </summary>
        public enum YearFlag
        {
            /// <summary>
            /// 
            /// </summary>
            None = 0,
            /// <summary>
            /// 
            /// </summary>
            Previous = 1,
            /// <summary>
            /// 
            /// </summary>
            Current = 2
        }

        //
        // Summary:
        //     Fires immediately after the edit value has been changed.
        [Category("Events")]
        [Description("Fires immediately after the edit value has been changed.")]
        public event EventHandler EditValueChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="DateComboDay"/> class.
        /// </summary>
        public DateCombo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the drop down control.
        /// </summary>
        /// <value>The drop down control.</value>
        public LookUpEdit InnerControl
        {
            get
            {
                return comboBox;
            }
        }

        /// <summary>
        /// Gets or sets the edit value.
        /// </summary>
        /// <value>The edit value.</value>
        public int Value
        {
            get { return ConvertEx.ToInt(comboBox.EditValue); }
            set { comboBox.EditValue = value; }
        }

        /// <summary>
        /// Gets or sets the edit value.
        /// </summary>
        /// <value>The edit value.</value>
        public object ValueObj
        {
            get { return comboBox.EditValue; }
            set { comboBox.EditValue = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether date combo allows null.
        /// </summary>
        /// <value><c>true</c> if whether date combo allows null; otherwise, <c>false</c>.</value>
        [DefaultValue(false)]
        public bool AllowsNull { get; set; }

        /// <summary>
        /// Loads the days.
        /// </summary>
        public void LoadDays(int year, int month)
        {
            if (year < MIN_YEAR || year > MAX_YEAR)
            {
                year = DateTime.Today.Year;
            }
            if (month <= 0 || month > 12)
            {
                month = 1;
            }

            DataTable dataTable = CreateDataTable(COLUMN_DAYS);

            int daysCount = DateTime.DaysInMonth(year, month);

            for (int day = 1; day <= daysCount; day++)
            {
                dataTable.Rows.Add(new object[] { day, day });
            }
            comboBox.Properties.DataSource = dataTable;
        }

        /// <summary>
        /// Loads the months.
        /// </summary>
        public void LoadMonths()
        {
            DateTimeFormatInfo mfi = new CultureInfo("ru-RU", false).DateTimeFormat;

            DataTable dataTable = CreateDataTable(COLUMN_MONTHS);
            for (int month = 1; month <= 12; month++)
            {
                dataTable.Rows.Add(new object[] { month, mfi.GetMonthName(month) });
            }
            comboBox.Properties.DataSource = dataTable;
        }

        /// <summary>
        /// Loads the year flags.
        /// </summary>
        public void LoadYearFlags()
        {
            DataTable dataTable = CreateDataTable(COLUMN_YEARS);

            dataTable.Rows.Add(new object[] { (int)YearFlag.Previous, "прошлый" });
            dataTable.Rows.Add(new object[] { (int)YearFlag.Current, "текущий" });

            comboBox.Properties.DataSource = dataTable;
        }

        /// <summary>
        /// Loads the years.
        /// </summary>
        public void LoadYears()
        {
            LoadYears(2000, 2030);
        }

        /// <summary>
        /// Loads the years.
        /// </summary>
        public void LoadYears(int yearFrom, int yearTo)
        {
            DataTable dataTable = CreateDataTable(COLUMN_YEARS);
            for (int year = yearFrom; year <= yearTo; year++)
            {
                dataTable.Rows.Add(new object[] { year, year });
            }
            comboBox.Properties.DataSource = dataTable;
        }

        public void AddYear(int year)
        {
            DataTable dat = (DataTable) comboBox.Properties.DataSource;
            if (dat.Select(string.Format("{0}={1}", COLUMN_VALUE, year)).Length > 0)
                return;
            dat.Rows.Add(year, year.ToString());
        }

        /// <summary>
        /// Handles the EditValueChanged event of the comboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void comboBox_EditValueChanged(object sender, EventArgs e)
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(this, e);
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the comboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
        private void comboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                comboBox.EditValue = null;
                comboBox.ResetText();
                if (comboBox.IsEditorActive)
                {
                    comboBox.ClosePopup();
                }
            }
        }

        /// <summary>
        /// Creates the data table.
        /// </summary>
        /// <returns></returns>
        private DataTable CreateDataTable(string columName)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(COLUMN_VALUE, typeof(int));
            dataTable.Columns.Add(columName);

            comboBox.Properties.DisplayMember = columName;
            comboBox.Properties.ValueMember = COLUMN_VALUE;

            comboBox.Properties.Columns.Clear();
            comboBox.Properties.Columns.Add(new LookUpColumnInfo(columName));

            if (this.AllowsNull)
            {
                dataTable.Rows.Add(null, " ");
            }

            return dataTable;
        }
    }
}
