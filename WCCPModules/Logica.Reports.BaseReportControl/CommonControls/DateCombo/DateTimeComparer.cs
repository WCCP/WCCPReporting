﻿using System;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// 
    /// </summary>
    public class DateTimeComparer
    {
        private const int LEAP_YEAR = 2000;
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        private bool isYearFlag;

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeComparer"/> class.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="day">The day.</param>
        public DateTimeComparer(int year, int month, int day)
        {
            Year = year;
            Month = month;
            Day = day;
            isYearFlag = Year < 100;
        }

        /// <summary>
        /// Normalizes to min date.
        /// </summary>
        public void NormalizeToMinDate()
        {
            Year = Year <= 0 ? (int)DateCombo.YearFlag.Previous : Year;
            Month = Month <= 0 ? 1 : Month;
            Day = Day <= 0 ? 1 : Day;
        }

        /// <summary>
        /// Normalizes to max date.
        /// </summary>
        public void NormalizeToMaxDate()
        {
            Year = Year <= 0 ? (int)DateCombo.YearFlag.Current : Year;
            Month = Month <= 0 ? 12 : Month;
            Day = Day <= 0 ? DateTime.DaysInMonth(LEAP_YEAR, Month) : Day;
        }

        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <value>The date time.</value>
        public DateTime DateTime 
        {
            get
            {
                return new DateTime(isYearFlag ? LEAP_YEAR + Year : Year, Month, Month == 2 && Day > 28 ? 28 : Day);
            }
        }

        /// <summary>
        /// Compares the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public int Compare(DateTimeComparer obj)
        {
            this.NormalizeToMinDate();
            obj.NormalizeToMaxDate();
            return this.DateTime.CompareTo(obj.DateTime);
        }
    }
}
