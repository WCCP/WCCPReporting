﻿namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class DateCombo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.comboBox = new DevExpress.XtraEditors.LookUpEdit();
          ((System.ComponentModel.ISupportInitialize)(this.comboBox.Properties)).BeginInit();
          this.SuspendLayout();
          // 
          // comboBox
          // 
          this.comboBox.Dock = System.Windows.Forms.DockStyle.Fill;
          this.comboBox.Location = new System.Drawing.Point(0, 0);
          this.comboBox.Name = "comboBox";
          this.comboBox.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
          this.comboBox.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
          this.comboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
          this.comboBox.Properties.DropDownRows = 12;
          this.comboBox.Properties.NullText = "";
          this.comboBox.Properties.PopupWidth = 100;
          this.comboBox.Properties.ShowFooter = false;
          this.comboBox.Properties.ShowHeader = false;
          this.comboBox.Size = new System.Drawing.Size(137, 20);
          this.comboBox.TabIndex = 0;
          this.comboBox.EditValueChanged += new System.EventHandler(this.comboBox_EditValueChanged);
          this.comboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox_KeyDown);
          // 
          // DateCombo
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.Controls.Add(this.comboBox);
          this.Name = "DateCombo";
          this.Size = new System.Drawing.Size(137, 22);
          ((System.ComponentModel.ISupportInitialize)(this.comboBox.Properties)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit comboBox;
    }
}
