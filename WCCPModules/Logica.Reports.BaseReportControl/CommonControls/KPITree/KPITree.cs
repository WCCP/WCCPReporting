using System;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// Implements general KPI tree functionality
    /// </summary>
    public partial class KPITree : XtraUserControl, IControlReadOnly
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KPITree"/> class.
        /// </summary>
        public KPITree()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets a value indicating whether nodes could be edited.
        /// </summary>
        /// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Gets Id list with comma separator
        /// </summary>
        public string SelectedStringIdList
        {
            get
            {
                treeKPI.PostEditor();
                GetFinalCheckedNodesOperation lNodesOperation = new GetFinalCheckedNodesOperation(columnID) { IsQuotesSeparated = false };
                treeKPI.NodesIterator.DoOperation(lNodesOperation);
                return lNodesOperation.SelectedIdList;
            }
        }

        /// <summary>
        /// Gets the selected KPI id list.
        /// </summary>
        /// <value>The selected KPI id list.</value>
        public SqlXml SelectedXmlIdList
        {
            get
            {
                treeKPI.PostEditor();

                DataTable lDataTable = treeKPI.DataSource as DataTable;

                if (lDataTable != null)
                {
                    lDataTable.AcceptChanges();

                    DataView lDataView = lDataTable.Copy().DefaultView;

                    GetFinalCheckedNodesOperation lNodesOperation = new GetFinalCheckedNodesOperation(columnID) { IsQuotesSeparated = true, IsKPItree = true };
                    treeKPI.NodesIterator.DoOperation(lNodesOperation);
                    if (lNodesOperation.Count > 0)
                    {
                        lDataView.RowFilter = string.Format("{0} IN ({1})", columnID.FieldName, lNodesOperation.SelectedIdList);
                        return GetKPITreeXmlSnapshot(lDataView.ToTable());
                    }
                }

                return GetKPITreeXmlSnapshot(null);
            }
        }

        /// <summary>
        /// Gets DataSource with posted changes
        /// </summary>
        /// <value>The DataSource</value>
        public DataTable DataSourcePosted
        {
            get
            {
                treeKPI.PostEditor();
                UpdateTableNodesCheckStateOperation lNodesOperation = new UpdateTableNodesCheckStateOperation(columnChecked);
                treeKPI.NodesIterator.DoOperation(lNodesOperation);

                DataTable lDataTable = treeKPI.DataSource as DataTable;

                if (lDataTable != null)
                {
                    lDataTable.AcceptChanges();
                }

                return lDataTable;
            }
        }

        /// <summary>
        /// Loads the KPI tree.
        /// </summary>
        /// <value>The DataSource</value>
        public void LoadKPITree(DataTable dataTable)
        {
            treeKPI.BeginUpdate();

            treeKPI.DataSource = dataTable;
            treeKPI.ExpandAll();
            treeKPI.CollapseAll();
            LoadKPITreeState();

            treeKPI.EndUpdate();
            treeKPI.NodesIterator.DoOperation(new ExpandNodesOperation());
        }

        /// <summary>
        /// Allows to check only one node
        /// </summary>
        /// <param name="node">current node</param>
        /// <param name="nodeId">Id</param>
        private static void OnNodeCheck_Unchecking(TreeListNode node, int nodeId)
        {
            if (node != null)
            {
                node.Checked = node.Id == nodeId && node.Level != 0;

                foreach (TreeListNode lChildNode in node.Nodes)
                {
                    OnNodeCheck_Unchecking(lChildNode, nodeId);
                }
            }
        }

        /// <summary>
        /// Gets the KPI tree XML snapshot.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns>xml that describes the KPITree</returns>
        private SqlXml GetKPITreeXmlSnapshot(DataTable dataTable)
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement("root");
                if (dataTable != null)
                {
                    foreach (DataRow lRow in dataTable.Rows)
                    {
                        lWriter.WriteStartElement("row");
                        AddFullEndXMLElement(lWriter, columnPOCEKpiId.FieldName, lRow[columnPOCEKpiId.FieldName]);
                        AddFullEndXMLElement(lWriter, columnParentPOCEKpiId.FieldName, lRow[columnParentPOCEKpiId.FieldName]);
                        lWriter.WriteEndElement();
                    }
                }

                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                return new SqlXml(lStream);
            }
        }

        /// <summary>
        /// Loads the state of the KPI tree.
        /// </summary>
        private void LoadKPITreeState()
        {
            treeKPI.NodesIterator.DoOperation(new UpdateNodesCheckStateOperation(columnChecked));
            treeKPI.NodesIterator.DoOperation(new ExpandNodesOperation());
        }

        /// <summary>
        /// Handles the BeforeCheckNode event of the treeKPI control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.CheckNodeEventArgs"/> instance containing the event data.</param>
        private void treeKPI_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            e.CanCheck = !ReadOnly;
        }

        /// <summary>
        /// Handles the AfterCheckNode event of the treeKPI control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.NodeEventArgs"/> instance containing the event data.</param>
        private void treeKPI_AfterCheckNode(object sender, NodeEventArgs e)
        {
            if (e.Node.CheckState != CheckState.Checked)
            {
                return;
            }

            TreeList tl = sender as TreeList;

            foreach (TreeListNode node in tl.Nodes)
            {
                OnNodeCheck_Unchecking(node, e.Node.Id);
            }

            treeKPI.SetFocusedNode(e.Node);
         }

        /// <summary>
        /// Adds the full end XML node.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="nodeName">Name of the node.</param>
        /// <param name="nodeValue">The node value.</param>
        private static void AddFullEndXMLElement(XmlWriter writer, string nodeName, object nodeValue)
        {
            writer.WriteStartElement(nodeName);
            if (nodeValue != null && nodeValue != DBNull.Value)
            {
                decimal lValue = ConvertEx.ToDecimal(nodeValue);
                writer.WriteString(lValue.ToString(CultureInfo.InvariantCulture));
            }

            writer.WriteEndElement();
        }
     }
}
