namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class KPITree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeKPI = new DevExpress.XtraTreeList.TreeList();
            this.columnKPIId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnKPIName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnKPIKpiId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnKPIGroupId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnChecked = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnPOCEKpiId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnParentPOCEKpiId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.treeKPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // treeKPI
            // 
            this.treeKPI.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnKPIId,
            this.columnKPIName,
            this.columnKPIKpiId,
            this.columnKPIGroupId,
            this.columnChecked,
            this.columnPOCEKpiId,
            this.columnParentPOCEKpiId,
            this.columnID,
            this.columnParentId});
            this.treeKPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeKPI.Location = new System.Drawing.Point(0, 0);
            this.treeKPI.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeKPI.Name = "treeKPI";
            this.treeKPI.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeKPI.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.treeKPI.OptionsView.ShowCheckBoxes = true;
            this.treeKPI.OptionsView.ShowColumns = false;
            this.treeKPI.OptionsView.ShowHorzLines = false;
            this.treeKPI.OptionsView.ShowIndicator = false;
            this.treeKPI.ParentFieldName = "ID_PARENT";
            this.treeKPI.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit});
            this.treeKPI.Size = new System.Drawing.Size(458, 485);
            this.treeKPI.TabIndex = 1;
            this.treeKPI.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeKPI_BeforeCheckNode);
            this.treeKPI.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeKPI_AfterCheckNode);
            // 
            // columnKPIId
            // 
            this.columnKPIId.Caption = "columnKPIId";
            this.columnKPIId.FieldName = "ID";
            this.columnKPIId.Name = "columnKPIId";
            this.columnKPIId.OptionsColumn.AllowEdit = false;
            this.columnKPIId.OptionsColumn.ReadOnly = true;
            // 
            // columnKPIName
            // 
            this.columnKPIName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnKPIName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnKPIName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnKPIName.Caption = "KPI";
            this.columnKPIName.FieldName = "NAME";
            this.columnKPIName.Name = "columnKPIName";
            this.columnKPIName.OptionsColumn.AllowEdit = false;
            this.columnKPIName.OptionsColumn.ReadOnly = true;
            this.columnKPIName.Visible = true;
            this.columnKPIName.VisibleIndex = 0;
            this.columnKPIName.Width = 236;
            // 
            // columnKPIKpiId
            // 
            this.columnKPIKpiId.Caption = "KPI_ID";
            this.columnKPIKpiId.FieldName = "KPI_ID";
            this.columnKPIKpiId.Name = "columnKPIKpiId";
            // 
            // columnKPIGroupId
            // 
            this.columnKPIGroupId.Caption = "GROUP_ID";
            this.columnKPIGroupId.FieldName = "GROUP_ID";
            this.columnKPIGroupId.Name = "columnKPIGroupId";
            // 
            // columnChecked
            // 
            this.columnChecked.Caption = "Checked";
            this.columnChecked.FieldName = "Checked";
            this.columnChecked.Name = "columnChecked";
            // 
            // columnPOCEKpiId
            // 
            this.columnPOCEKpiId.Caption = "POCE_KPI_ID";
            this.columnPOCEKpiId.FieldName = "POCE_KPI_ID";
            this.columnPOCEKpiId.Name = "columnPOCEKpiId";
            // 
            // columnParentPOCEKpiId
            // 
            this.columnParentPOCEKpiId.Caption = "Parent_POCE_KPI_ID";
            this.columnParentPOCEKpiId.FieldName = "Parent_POCE_KPI_ID";
            this.columnParentPOCEKpiId.Name = "columnParentPOCEKpiId";
            // 
            // columnID
            // 
            this.columnID.Caption = "ID";
            this.columnID.FieldName = "ID";
            this.columnID.Name = "columnID";
            // 
            // columnParentId
            // 
            this.columnParentId.Caption = "ID_PARENT";
            this.columnParentId.FieldName = "ID_PARENT";
            this.columnParentId.Name = "columnParentId";
            // 
            // repositoryItemSpinEdit
            // 
            this.repositoryItemSpinEdit.Name = "repositoryItemSpinEdit";
            // 
            // KPITree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeKPI);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "KPITree";
            this.Size = new System.Drawing.Size(458, 485);
            ((System.ComponentModel.ISupportInitialize)(this.treeKPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeKPI;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnKPIId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnKPIName;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnKPIKpiId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnKPIGroupId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnChecked;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnPOCEKpiId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnParentPOCEKpiId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnParentId;
    }
}
