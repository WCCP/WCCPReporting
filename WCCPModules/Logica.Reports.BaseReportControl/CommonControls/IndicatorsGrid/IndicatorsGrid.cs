using System;
using System.ComponentModel;
using System.Data;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.BaseReportControl.Utility;
using System.Data.SqlTypes;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.DataAccess;
using DevExpress.XtraGrid.Views.Base;
using System.Drawing;
using System.Collections.ObjectModel;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid
{
    /// <summary>
    /// 
    /// </summary>
    public partial class IndicatorsGrid : XtraUserControl
    {
        public IndicatorEditMode EditMode { get; set; }
        private DateTime DateStart { get; set; }
        private Collection<DateTime> dateCollection = new Collection<DateTime>();
        private Collection<IndicatorType> invalidIndicators = new Collection<IndicatorType>();

        private int currColumn;
        private int currRow;

        //
        // Summary:
        //     Fires immediately after the edit value has been changed.
        [Category("Events")]
        [Description("Fires immediately after the edit value has been changed.")]
        public event EventHandler EditValueChanged;

        //
        // Summary:
        //     Fires immediately after the edit value has been changed.
        [Category("Events")]
        [Description("Fires immediately after the Plan value has been changed.")]
        public event EventHandler PlanValueChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndicatorsGrid"/> class.
        /// </summary>
        public IndicatorsGrid()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the plan total contract sum.
        /// </summary>
        /// <value>The plan total contract sum.</value>
        public decimal PlanTotalContractSum
        {
            get
            {
                return ConvertEx.ToDecimal(GetIndicatorValue(IndicatorType.ContractSumYtd, columnIndicatorPlanTotal));
            }
        }

        /// <summary>
        /// Gets the plan total season factor.
        /// </summary>
        /// <value>The plan total season factor.</value>
        public decimal PlanTotalSeasonFactor
        {
            get
            {
                return ConvertEx.ToDecimal(GetIndicatorValue(IndicatorType.SeasonFactor, columnIndicatorPlanTotal));
            }
        }

        /// <summary>
        /// Gets the plan total investment sum.
        /// </summary>
        /// <value>The plan total investment sum.</value>
        public decimal PlanTotalInvestmentSum
        {
            get
            {
                return ConvertEx.ToDecimal(GetIndicatorValue(IndicatorType.InvestmentSumYtd, columnIndicatorPlanTotal));
            }
        }

        /// <summary>
        /// Gets the plan total uplift sum.
        /// </summary>
        /// <value>The plan total uplift sum.</value>
        public decimal PlanTotalUpliftSum
        {
            get
            {
                return ConvertEx.ToDecimal(GetIndicatorValue(IndicatorType.Uplift, columnIndicatorPlanTotal));
            }
        }

        /// <summary>
        /// Gets the indicator value.
        /// </summary>
        /// <param name="indicatorType">Type of the indicator.</param>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private object GetIndicatorValue(IndicatorType indicatorType, GridColumn column)
        {
            for (int rowHandle = 0; rowHandle < gridViewIndicators.RowCount; rowHandle++)
            {
                if (ConvertEx.ToInt(gridViewIndicators.GetRowCellValue(rowHandle, columnIndicatorId)) == (int)indicatorType)
                {
                    return gridViewIndicators.GetRowCellValue(rowHandle, column);
                }
            }
            return null;
        }

        /// <summary>
        /// Gets or sets value that indicates whether allow editing for specific cells that arranged in current month
        /// </summary>
        public bool AllowEditingForCurrentMonth { get; set; }

        /// <summary>
        /// Adds the type of the invalid.
        /// </summary>
        /// <param name="type">The type.</param>
        public void AddInvalidIndicators(IndicatorType type)
        {
            invalidIndicators.Add(type);
            UpdateValidationLabel();
        }

        /// <summary>
        /// Clears the invalid indicators.
        /// </summary>
        public void ClearErrorIndicators()
        {
            invalidIndicators.Clear();
            UpdateValidationLabel();
        }

        /// <summary>
        /// Gets a value indicating whether this instance has errors.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has errors; otherwise, <c>false</c>.
        /// </value>
        public bool HasErrors {
            get {
                return invalidIndicators.Count > 0;
            }
        }

        /// <summary>
        /// Validates the indicators table.
        /// </summary>
        private void UpdateValidationLabel() {
            groupIndicators.Text = groupIndicators.Text.Replace(Constants.LABEL_INVALID, string.Empty);
            if (!HasErrors)
                groupIndicators.AppearanceCaption.ForeColor = SystemColors.ControlText;
            else {
                groupIndicators.Text = groupIndicators.Text + Constants.LABEL_INVALID;
                groupIndicators.AppearanceCaption.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Adds the date to collection.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        private static DateTime GetCorrectDate(int year, int month) {
            while (month > 12)
                month -= 12;

            if (month != 0)
                return new DateTime(year, month, 1);
            return new DateTime(year, 1, 1);
        }

        /// <summary>
        /// Loads the indicators.
        /// </summary>
        public void LoadIndicators(DataTable source, DateTime dateStart) {
            gridViewIndicators.BeginUpdate();

            int lScrollX = gridViewIndicators.LeftCoord;

            dateCollection.Clear();
            gridViewIndicators.Bands.Clear();
            GridBand lMainBand = gridViewIndicators.Bands.AddBand(string.Empty);
            lMainBand.Fixed = FixedStyle.Left;
            gridControlIndicators.DataSource = source;
            DateStart = dateStart;

            // rename all columns adequatelly
            if (null != source && source.Rows.Count > 0) {
                gridViewIndicators.PopulateColumns();
                GridBand lBand = null;
                int lCurrentMonth = -1;
                foreach (BandedGridColumn lColumn in gridViewIndicators.Columns) {
                    // prepare column
                    int lPeriodNumber = GetIndicatorsPeriodColumnNumber(lColumn);
                    int lMonth = DateStart.Month + lPeriodNumber - 1;
                    int lYear = (int)(dateStart.Year + Math.Ceiling(lMonth / 12.0)) - 1;
                    bool lIsFactColumn = IsFactPeriodColumn(lColumn);
                    lColumn.Width = 80;
                    DateTime lCorrectDate = GetCorrectDate(lYear, lMonth);

                    // dev express adds col prefix when populate column by its own              
                    if (IsPeriodColumn(lColumn)) {
                        if (IsFactPeriodColumn(lColumn))
                            dateCollection.Add(lCorrectDate);

                        lColumn.Caption = lIsFactColumn ? Constants.LABEL_FACT : Constants.LABEL_PLAN;
                        lColumn.ColumnEdit = lIsFactColumn ? repositoryIndicatorItemSpinEditFact : repositoryIndicatorItemSpinEditPlan;
                        // assign appropriate band
                        if (lCurrentMonth != lMonth) {
                            lBand = gridViewIndicators.Bands.AddBand(string.Format("{0} {1}", ConvertEx.ToMonthName(lMonth), lYear));
                            lBand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            lBand.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                            lCurrentMonth = lMonth;
                        }
                        lColumn.OwnerBand = lBand;
                    }
                    else
                        lColumn.OwnerBand = lMainBand;

                    lColumn.OwnerBand.Tag = lCorrectDate; //Setting OwnerBand.Tag property in order to correctly identify date of relevant columns in method GetAllowCellEdit.
                    lColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    lColumn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    lColumn.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
                    lColumn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    lColumn.DisplayFormat.FormatString = "{0:N3}";
                }

                // restrore columns settings
                gridViewIndicators.Columns[columnIndicatorId.FieldName].Visible = false;
                gridViewIndicators.Columns[columnIndicatorIsEditable.FieldName].Visible = false;

                RestoreFieldSettings(columnIndicatorName);
                RestoreFieldSettings(columnIndicatorPlanTotal);
                RestoreFieldSettings(columnIndicatorFactTotal);

                gridViewIndicators.Columns[columnIndicatorName.FieldName].Width = 200;
            }
            gridViewIndicators.LeftCoord = lScrollX;

            gridViewIndicators.EndUpdate();
        }

        private void RestoreFieldSettings(GridColumn col) {
            gridViewIndicators.Columns[col.FieldName].OptionsColumn.ReadOnly = true;
            gridViewIndicators.Columns[col.FieldName].OptionsColumn.AllowEdit = false;
            gridViewIndicators.Columns[col.FieldName].OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
            gridViewIndicators.Columns[col.FieldName].Caption = col.Caption;
        }

        /// <summary>
        /// Gets the indicators period column number.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private int GetIndicatorsPeriodColumnNumber(GridColumn column) {
            return GetPeriodColumnNumber(column,
                new string[] {
                    Constants.DYNAMIC_COLUMN_PREFIX_PLAN_DB,
                    Constants.DYNAMIC_COLUMN_PREFIX_FACT_DB 
                });
        }

        /// <summary>
        /// Determines whether [is period column] [the specified column].
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>
        /// 	<c>true</c> if [is period column] [the specified column]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPeriodColumn(GridColumn column) {
            return column == null ? false : column.FieldName.StartsWith(Constants.DYNAMIC_COLUMN_PREFIX_DB);
        }

        /// <summary>
        /// Determines whether [is fact period column] [the specified column].
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>
        /// 	<c>true</c> if [is fact period column] [the specified column]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsFactPeriodColumn(GridColumn column) {
            if (column == null)
                return false;

            return column.FieldName.StartsWith(Constants.DYNAMIC_COLUMN_PREFIX_FACT_DB);
        }

        /// <summary>
        /// Gets the period column number.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="possiblePrefixes">array of column prefixes</param>
        /// <returns></returns>
        public static int GetPeriodColumnNumber(GridColumn column, string[] possiblePrefixes) {
            int lNum = 0;
            if (IsPeriodColumn(column) && possiblePrefixes != null && possiblePrefixes.Length > 0) {
                try {
                    foreach (string lPrefix in possiblePrefixes) {
                        if (column.FieldName.Contains(lPrefix)) {
                            lNum = ConvertEx.ToInt(column.FieldName.TrimStart(lPrefix.ToCharArray()));
                            if (lNum > 0)
                                return lNum;
                        }
                    }
                }
                catch (InvalidCastException) {
                    lNum = 0;
                }
            }
            return lNum;
        }

        /// <summary>
        /// Gets the selected indicators XML.
        /// </summary>
        /// <value>The selected indicators XML.</value>
        public SqlXml SelectedIndicatorsXml {
            get {
                gridViewIndicators.PostEditor();
                gridViewIndicators.UpdateCurrentRow();
                return ConvertEx.ToSqlXmlAll(gridControlIndicators.DataSource as DataTable);
            }
        }

        /// <summary>
        /// Getindicators the editable mode.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        /// <returns></returns>
        private IndicatorEditableType GetIndicatorEditableMode(int rowHandle) {
            return (IndicatorEditableType)ConvertEx.ToInt(gridViewIndicators.GetRowCellValue(rowHandle, columnIndicatorIsEditable));
        }

        /// <summary>
        /// Gets the period column future.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private bool IsFuturePeriodColumn(GridColumn column) {
            int lPeriodNumber = GetIndicatorsPeriodColumnNumber(column);
            if (lPeriodNumber > 0) {
                DateTime lEditDate = dateCollection[lPeriodNumber - 1];
                DateTime lNowDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                return lEditDate > lNowDate;
            }
            return false;
        }

        /// <summary>
        /// Gets the max period value.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        /// <param name="numColumnEnd">The num column end.</param>
        /// <param name="isFactColumn">if true indicates that column is fact column</param>
        /// <returns></returns>
        private decimal GetMaxPeriodValue(int rowHandle, int numColumnEnd, bool isFactColumn) {
            decimal lMaxValue = 0;
            gridViewIndicators.UpdateCurrentRow();
            foreach (GridColumn lColumn in gridViewIndicators.Columns) {
                if (!IsPeriodColumn(lColumn) || IsFactPeriodColumn(lColumn) != isFactColumn)
                    continue;

                if (numColumnEnd > 0 && GetIndicatorsPeriodColumnNumber(lColumn) >= numColumnEnd)
                    break;

                decimal lCurrentValue = ConvertEx.ToDecimal(gridViewIndicators.GetRowCellValue(rowHandle, lColumn));
                if (lCurrentValue > lMaxValue)
                    lMaxValue = lCurrentValue;
            }
            return lMaxValue;
        }

        /// <summary>
        /// Normalizes the row.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        /// <param name="periodNumber">The period number.</param>
        /// <param name="newValue">The new value.</param>
        private void NormalizeRow(int rowHandle, int periodNumber, decimal newValue, bool factColumn) {
            if (!IsYTDIndicator(rowHandle))
                return;

            AttachEvents(false);
            // for each cycle does not work for this case o_0
            for (int i = 0; i < gridViewIndicators.Columns.Count; i++) {
                GridColumn lColumn = gridViewIndicators.Columns[i];
                if (!IsPeriodColumn(lColumn) || IsFactPeriodColumn(lColumn) != factColumn || GetIndicatorsPeriodColumnNumber(lColumn) <= periodNumber)
                    continue;

                decimal lCurrentValue = ConvertEx.ToDecimal(gridViewIndicators.GetRowCellValue(rowHandle, lColumn));
                if (lCurrentValue > newValue)
                    newValue = lCurrentValue;

                if (lCurrentValue < newValue)
                    gridViewIndicators.SetRowCellValue(rowHandle, lColumn, newValue);
            }
            AttachEvents(true);
        }

        /// <summary>
        /// Determines whether current row is YTD indicator.
        /// </summary>
        /// <param name="rowHandle">Handle of the row in grid</param>
        /// <returns>
        /// 	<c>true</c> if indicator is YearToDate (running total to it applied); otherwise, <c>false</c>.
        /// </returns>
        private bool IsYTDIndicator(int rowHandle) {
            IndicatorType lIndicatorType = (IndicatorType) ConvertEx.ToInt(gridViewIndicators.GetRowCellValue(rowHandle, columnIndicatorId));

            return lIndicatorType == IndicatorType.TargetVYtd || lIndicatorType == IndicatorType.MacoYtd
                || lIndicatorType == IndicatorType.IncrementalMacoYtd || lIndicatorType == IndicatorType.MacoDalYtd;
        }

        /// <summary>
        /// Attaches the events.
        /// </summary>
        /// <param name="attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach)
                gridViewIndicators.CellValueChanged += gridViewIndicators_CellValueChanged;
            else
                gridViewIndicators.CellValueChanged -= gridViewIndicators_CellValueChanged;
        }

        /// <summary>
        /// Handles the ShowingEditor event of the gridViewIndicators control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void gridViewIndicators_ShowingEditor(object sender, CancelEventArgs e) {
            e.Cancel = !GetAllowCellEdit(gridViewIndicators.FocusedRowHandle, gridViewIndicators.FocusedColumn);
            // update min editor value
            if (IsYTDIndicator(gridViewIndicators.FocusedRowHandle)) {
                int lCurrentColumn = GetIndicatorsPeriodColumnNumber(gridViewIndicators.FocusedColumn);
                bool lIsFactColumn = IsFactPeriodColumn(gridViewIndicators.FocusedColumn);
                if (lIsFactColumn)
                    repositoryIndicatorItemSpinEditFact.MinValue = GetMaxPeriodValue(gridViewIndicators.FocusedRowHandle, lCurrentColumn, lIsFactColumn);
                else
                    repositoryIndicatorItemSpinEditPlan.MinValue = GetMaxPeriodValue(gridViewIndicators.FocusedRowHandle, lCurrentColumn, lIsFactColumn);
            }
            else {
                repositoryIndicatorItemSpinEditPlan.MinValue = 0;
                repositoryIndicatorItemSpinEditFact.MinValue = 0;
            }

            // just remove spin buttons if required
            if (repositoryIndicatorItemSpinEditPlan.Buttons.Count > 0)
                repositoryIndicatorItemSpinEditPlan.Buttons[0].Visible = !e.Cancel;
        }

        /// <summary>
        /// Handles the CellValueChanged event of the gridViewIndicators control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridViewIndicators_CellValueChanged(object sender, CellValueChangedEventArgs e) {
            if (!IsPeriodColumn(e.Column))
                return;

            if (gridViewIndicators.FocusedRowHandle != int.MinValue)
                currRow = gridViewIndicators.FocusedRowHandle;
            if (gridViewIndicators.FocusedColumn != null)
                currColumn = gridViewIndicators.FocusedColumn.AbsoluteIndex;

            NormalizeRow(e.RowHandle, GetIndicatorsPeriodColumnNumber(e.Column), ConvertEx.ToDecimal(e.Value), IsFactPeriodColumn(e.Column));
            FireEditValueChanged(e);

            GetNextFocusPosition();
            NavigateToCell(currColumn, currRow);
        }

        /// <summary>
        /// Returns value which indicates whether specified cell is available for editing.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private bool GetAllowCellEdit(int rowHandle, GridColumn column)
        {
            bool canEdit = false;
            IndicatorEditableType editableType = GetIndicatorEditableMode(rowHandle);
            switch (EditMode)
            {
                case IndicatorEditMode.PlanReadOnly: //In case of plan read-only mode check whether specified cell isn't planned period column.
                    {
                        if (editableType == IndicatorEditableType.AllowFact || editableType == IndicatorEditableType.AllowPlanFact)
                            canEdit = !IsFuturePeriodColumn(column);
                        break;
                    }
                case IndicatorEditMode.Common:
                    {
                        if (editableType == IndicatorEditableType.AllowPlan)
                            canEdit = !IsFactPeriodColumn(column);

                        else if (editableType == IndicatorEditableType.AllowPlanFact)
                            canEdit = AllowEditingForCurrentMonth && IsPreviousOrCurrentMonthColumn(column) || !IsFactPeriodColumn(column);

                        else if (editableType == IndicatorEditableType.AllowFact)
                            canEdit = AllowEditingForCurrentMonth && IsPreviousOrCurrentMonthColumn(column) || IsFactPeriodColumn(column) && !IsFuturePeriodColumn(column);

                        else
                            canEdit = false;
                        break;
                    }
                case IndicatorEditMode.ReadOnly: //In case of read-only mode check whether cell is placed in current month column band and availability for editing current month column.
                    {
                        if (editableType == IndicatorEditableType.AllowPlanFact || editableType == IndicatorEditableType.AllowFact)
                            canEdit = AllowEditingForCurrentMonth && IsCurrentMonthColumn(column) && IsFactPeriodColumn(column);
                        break;
                    }
            }
            return canEdit;
        }

        /// <summary>
        /// Returns value that indicates whether specified column is placed in current month band.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        private bool IsCurrentMonthColumn(GridColumn column) {
            bool lResult = false;
            BandedGridColumn lBandedColumn = column as BandedGridColumn;

            if (lBandedColumn != null && lBandedColumn.OwnerBand != null && lBandedColumn.OwnerBand.Tag != null) {
                DateTime? lColumnDate = lBandedColumn.OwnerBand.Tag as DateTime?;                 //Checking whether column isn't has the same year and month that today.
                if (lColumnDate.Value.Month == DateTime.Today.Month && lColumnDate.Value.Year == DateTime.Today.Year)
                    lResult = true;
            }
            return lResult;
        }

        /// <summary>
        /// Returns value that indicates whether specified column is placed in current or earlier month band.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        private bool IsPreviousOrCurrentMonthColumn(GridColumn column) {
            bool lResult = false;
            BandedGridColumn lBandedColumn = column as BandedGridColumn;

            if ((lBandedColumn != null && lBandedColumn.OwnerBand != null) && lBandedColumn.OwnerBand.Tag != null) {
                DateTime? lColumnDate = lBandedColumn.OwnerBand.Tag as DateTime?;
                if (lColumnDate.Value <= DateTime.Today)
                    lResult = true;
            }
            return lResult;
        }

        /// <summary>
        /// Fires the edit value changed.
        /// </summary>
        private void FireEditValueChanged(CellValueChangedEventArgs e) {
            if (EditValueChanged == null || e == null || !IsPeriodColumn(e.Column))
                return;

            int lIndicatorId = ConvertEx.ToInt(gridViewIndicators.GetRowCellValue(e.RowHandle, columnIndicatorId));
            int lPeriodColumnNumber = GetIndicatorsPeriodColumnNumber(e.Column);
            bool lIsPlanColumn = !IsFactPeriodColumn(e.Column);

            EditValueChanged(this, new IndicatorEventArgs(e, lIndicatorId, lPeriodColumnNumber, lIsPlanColumn));

            if (PlanValueChanged == null || !lIsPlanColumn || lIndicatorId != (int) IndicatorType.TargetVDal)
                return;

            decimal lValue = ConvertEx.ToDecimal(gridViewIndicators.GetRowCellValue(e.RowHandle, e.Column));
            PlanValueChanged(this, new PlanVolumeChangedEventArgs(lPeriodColumnNumber, lValue));
        }

        /// <summary>
        /// Handles the CustomDrawCell event of the gridViewIndicators control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs"/> instance containing the event data.</param>
        private void gridViewIndicators_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e) {
            if (IsPeriodColumn(e.Column) && !GetAllowCellEdit(e.RowHandle, e.Column))
                e.Graphics.FillRectangle(Constants.BrushGray, e.Bounds);
            else if (e.Column.FieldName == columnIndicatorPlanTotal.FieldName) {
                int lIndicatorId = ConvertEx.ToInt(gridViewIndicators.GetRowCellValue(e.RowHandle, columnIndicatorId));
                if (invalidIndicators.Contains((IndicatorType) lIndicatorId))
                    e.Appearance.ForeColor = Color.Red;
            }

            if (e.Column != gridViewIndicators.FocusedColumn || e.RowHandle != gridViewIndicators.FocusedRowHandle)
                return;

            int lPenWidth = 1;
            if (GetAllowCellEdit(e.RowHandle, e.Column)) //Check whether cell edit available
                lPenWidth = 2;  //Setting penWidth to greater value, because edit cells cuts border a bit.
            Rectangle lRect = e.Bounds;
            lRect.Inflate(lPenWidth, lPenWidth);
            e.Graphics.DrawRectangle(new Pen(Color.RoyalBlue, lPenWidth), e.Bounds);
        }

        /// <summary>
        /// Handles the CustomColumnDisplayText event of the gridViewIndicators control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs"/> instance containing the event data.</param>
        private void gridViewIndicators_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            // calculate percent fields
            IndicatorType type = (IndicatorType)ConvertEx.ToInt(gridViewIndicators.GetListSourceRowCellValue(e.ListSourceRowIndex, columnIndicatorId));
            bool isPeriodOrTotalColumn = IsPeriodColumn(e.Column) || e.Column.FieldName == columnIndicatorFactTotal.FieldName || e.Column.FieldName == columnIndicatorPlanTotal.FieldName;
            if (isPeriodOrTotalColumn && (type == IndicatorType.Uplift || type == IndicatorType.InvestmentsWeight || type == IndicatorType.Efficiency))
            {
                decimal value = ConvertEx.ToDecimal(GetIndicatorValue(type, e.Column));
                e.DisplayText = value.ToString("P1");
            }
            if (isPeriodOrTotalColumn
                && (type == IndicatorType.ContractSumYtd
                    || type == IndicatorType.InvestmentSumYtd
                    || type == IndicatorType.MacoYtd
                    || type == IndicatorType.IncrementalMacoYtd
                    || type == IndicatorType.MacoDalYtd))
            {
                decimal value = ConvertEx.ToDecimal(GetIndicatorValue(type, e.Column));
                e.DisplayText = value.ToString("N2");
            }
        }

        /// <summary>
        /// Event handler of the GridControlIndicators ProcessGridKey event.
        /// It is used in order to navigate user to the next available cell.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridControlIndicators_ProcessGridKey(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:
                    {
                        //  Check whether value was not changed, and then just navigate user to the next cell
                        //  If values was modified, the navigating occurs in method LoadIndicators.
                        if (!IsFocusedCellModified() || gridViewIndicators.EditingValue == null)
                        {
                            AttachEvents(false);
                            //Detaching events in order to prevent bug, that was found in DevExpress:
                            #region #21827 Bug description
                            //When user presses enter while cell is empty or has 0 value, this event occurs and it navigates user to the next cell.
                            //In a bit different case, when user first typing value into focused cell, then cleared it or typed zero value,
                            //the same occurs (like in first case), but whilst "NavigateToCell" method is doing his job gridViewIndicators.CellValueChanged 
                            //event starting (when it's unncessary to rebuild grid, because value wasn't changed).
                            //Erorr "Object reference not set to an instance of an object" or "External Error" happend when NavigateToCell was called two times.
                            #endregion
                            currRow = gridViewIndicators.FocusedRowHandle;
                            currColumn = gridViewIndicators.FocusedColumn.AbsoluteIndex;
                            GetNextFocusPosition();
                            NavigateToCell(currColumn, currRow);
                            AttachEvents(true);
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Method that calculates next focus position in gridViewIndicators
        /// and sets two result number to inner row and column fields
        /// </summary>
        private void GetNextFocusPosition()
        {
            while (true)
            {
                if (currColumn < gridViewIndicators.Columns.Count - 1) // Checking whether it isn't last column
                {
                    currColumn++;
                    if (GetAllowCellEdit(currRow, gridViewIndicators.Columns[currColumn])) break; //If appropriate cell was found, just break loop
                }
                else if (currRow < gridViewIndicators.RowCount - 1)   //Checking whether it isn't last row
                {
                    currColumn = columnIndicatorFactTotal.AbsoluteIndex + 1;
                    currRow++;
                    if (GetAllowCellEdit(currRow, gridViewIndicators.Columns[currColumn])) break; //If appropriate cell was found, just break loop
                }
                else
                {
                    currColumn = columnIndicatorFactTotal.AbsoluteIndex;
                    currRow = 0;
                    // break;  //Break loop if method reached the last cell of the grid.
                }
            }
        }

        /// <summary>
        /// Method that takes two indexes of row and column and navigates user to this cell.
        /// </summary>
        /// <param name="column">Column that you want navigate</param>
        /// <param name="row">Row that you want navigate </param>
        private void NavigateToCell(Int32 column, Int32 row)
        {
            this.gridViewIndicators.FocusedColumn =
                this.gridViewIndicators.Columns[column];
            this.gridViewIndicators.FocusedRowHandle = row;
            gridViewIndicators.ShowEditor();
        }

        /// <summary>
        /// Returns values that defines whether focused cell was modified.
        /// </summary>
        /// <returns></returns>
        private bool IsFocusedCellModified()
        {
            if (gridViewIndicators.FocusedColumn.ColumnType == typeof(decimal))//Checking for decimal type
            {
                decimal focusedValue = ConvertEx.ToDecimal(gridViewIndicators.FocusedValue);
                decimal editingValue = ConvertEx.ToDecimal(gridViewIndicators.EditingValue);
                return focusedValue != editingValue;
            }
            //You may also add checking for another type checking value
            return false;
        }
    }
}
