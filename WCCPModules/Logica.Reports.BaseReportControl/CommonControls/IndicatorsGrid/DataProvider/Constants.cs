﻿using System;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using System.Drawing;

namespace Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.DataAccess
{
    internal class Constants
    {
        public const string DYNAMIC_COLUMN_PREFIX_DB = "PERIOD_";
        public const string DYNAMIC_COLUMN_PREFIX_PLAN_DB = "PERIOD_PLAN";
        public const string DYNAMIC_COLUMN_PREFIX_FACT_DB = "PERIOD_FACT";
        public const string COLUMN_PREFIX_BUDGET_DEVEXPR = "col";

        public const string LABEL_PLAN = "План";
        public const string LABEL_FACT = "Факт";

        public const string LABEL_INVALID = " (Заполнено не корректно)";

        public static Brush BrushGray = new SolidBrush(SystemColors.Control);
        
    }

    /// <summary>
    /// 
    /// </summary>
    public enum IndicatorEditMode
    {
        /// <summary>
        /// 
        /// </summary>
        Common = 0,
        /// <summary>
        /// 
        /// </summary>
        ReadOnly = 1,
        /// <summary>
        /// 
        /// </summary>
        PlanReadOnly = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum IndicatorType
    {
        Unknown = 0,
        SeasonFactor = 1,
        TargetVDal = 2,
        TargetVYtd = 3,
        Uplift = 4,
        ContractSumYtd = 5,
        InvestmentSumYtd = 6,
        MacoYtd = 7,
        IncrementalMacoYtd = 8,
        MacoDalYtd = 9,
        InvestmentsWeight = 11,
        Efficiency = 12
    }

    /// <summary>
    /// 
    /// </summary>
    internal enum IndicatorEditableType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        AllowPlan = 1,
        /// <summary>
        /// 
        /// </summary>
        AllowFact = 2,
        /// <summary>
        /// 
        /// </summary>
        AllowPlanFact = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public class IndicatorEventArgs : CellValueChangedEventArgs
    {
        public int IndicatorId { get; set; }
        public int PeriodColumnNum { get; set; }
        public bool IsPlanColumn { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndicatorEventArgs"/> class.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        /// <param name="column">The column.</param>
        /// <param name="value">The value.</param>
        /// <param name="indicatorId">The indicator id.</param>
        /// <param name="periodColumnNum">The period column num.</param>
        public IndicatorEventArgs(CellValueChangedEventArgs e, int indicatorId, int periodColumnNum, bool isPlanColumn)
            : base(e.RowHandle, e.Column, e.Value)
        {
            IndicatorId = indicatorId;
            PeriodColumnNum = periodColumnNum;
            IsPlanColumn = isPlanColumn;
        }
    }
}
