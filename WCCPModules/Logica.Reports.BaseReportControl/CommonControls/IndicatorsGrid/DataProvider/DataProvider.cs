﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using Logica.Reports.Common;

namespace Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.DataAccess
{
    internal class DataProvider
    {
        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName)
        {
            DataTable dataTable = null;
            try
            {
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(spName);
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
            return dataTable;
        }
    }
}
