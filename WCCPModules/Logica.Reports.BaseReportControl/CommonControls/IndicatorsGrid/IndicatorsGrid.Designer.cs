namespace Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid
{
    partial class IndicatorsGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupIndicators = new DevExpress.XtraEditors.GroupControl();
            this.gridControlIndicators = new DevExpress.XtraGrid.GridControl();
            this.gridViewIndicators = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridIndicatorsMainBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.columnIndicatorId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.columnIndicatorName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.columnIndicatorPlanTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.columnIndicatorFactTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.columnIndicatorIsEditable = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryIndicatorItemSpinEditPlan = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryIndicatorItemSpinEditFact = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            ((System.ComponentModel.ISupportInitialize) (this.groupIndicators)).BeginInit();
            this.groupIndicators.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.gridControlIndicators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridViewIndicators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryIndicatorItemSpinEditPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryIndicatorItemSpinEditFact)).BeginInit();
            this.SuspendLayout();
            // 
            // groupIndicators
            // 
            this.groupIndicators.AlwaysScrollActiveControlIntoView = false;
            this.groupIndicators.Controls.Add(this.gridControlIndicators);
            this.groupIndicators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupIndicators.Location = new System.Drawing.Point(0, 0);
            this.groupIndicators.Name = "groupIndicators";
            this.groupIndicators.Size = new System.Drawing.Size(595, 264);
            this.groupIndicators.TabIndex = 44;
            this.groupIndicators.Text = "������������ �����������";
            // 
            // gridControlIndicators
            // 
            this.gridControlIndicators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlIndicators.Location = new System.Drawing.Point(2, 22);
            this.gridControlIndicators.MainView = this.gridViewIndicators;
            this.gridControlIndicators.Name = "gridControlIndicators";
            this.gridControlIndicators.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryIndicatorItemSpinEditPlan,
            this.repositoryIndicatorItemSpinEditFact});
            this.gridControlIndicators.Size = new System.Drawing.Size(591, 240);
            this.gridControlIndicators.TabIndex = 3;
            this.gridControlIndicators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewIndicators});
            this.gridControlIndicators.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControlIndicators_ProcessGridKey);
            // 
            // gridViewIndicators
            // 
            this.gridViewIndicators.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridIndicatorsMainBand});
            this.gridViewIndicators.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.columnIndicatorId,
            this.columnIndicatorIsEditable,
            this.columnIndicatorName,
            this.columnIndicatorPlanTotal,
            this.columnIndicatorFactTotal});
            this.gridViewIndicators.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewIndicators.GridControl = this.gridControlIndicators;
            this.gridViewIndicators.Name = "gridViewIndicators";
            this.gridViewIndicators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewIndicators.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewIndicators.OptionsView.ColumnAutoWidth = false;
            this.gridViewIndicators.OptionsView.ShowDetailButtons = false;
            this.gridViewIndicators.OptionsView.ShowGroupPanel = false;
            this.gridViewIndicators.OptionsView.ShowIndicator = false;
            this.gridViewIndicators.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewIndicators_CustomDrawCell);
            this.gridViewIndicators.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewIndicators_ShowingEditor);
            this.gridViewIndicators.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewIndicators_CellValueChanged);
            this.gridViewIndicators.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewIndicators_CustomColumnDisplayText);
            // 
            // gridIndicatorsMainBand
            // 
            this.gridIndicatorsMainBand.Columns.Add(this.columnIndicatorId);
            this.gridIndicatorsMainBand.Columns.Add(this.columnIndicatorName);
            this.gridIndicatorsMainBand.Columns.Add(this.columnIndicatorPlanTotal);
            this.gridIndicatorsMainBand.Columns.Add(this.columnIndicatorFactTotal);
            this.gridIndicatorsMainBand.Columns.Add(this.columnIndicatorIsEditable);
            this.gridIndicatorsMainBand.Name = "gridIndicatorsMainBand";
            this.gridIndicatorsMainBand.Width = 400;
            // 
            // columnIndicatorId
            // 
            this.columnIndicatorId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnIndicatorId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnIndicatorId.FieldName = "INDICATOR_ID";
            this.columnIndicatorId.Name = "columnIndicatorId";
            this.columnIndicatorId.OptionsColumn.AllowEdit = false;
            this.columnIndicatorId.OptionsColumn.ReadOnly = true;
            this.columnIndicatorId.OptionsColumn.ShowCaption = false;
            // 
            // columnIndicatorName
            // 
            this.columnIndicatorName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnIndicatorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnIndicatorName.Caption = "����������";
            this.columnIndicatorName.FieldName = "INDICATOR_NAME";
            this.columnIndicatorName.Name = "columnIndicatorName";
            this.columnIndicatorName.OptionsColumn.AllowEdit = false;
            this.columnIndicatorName.OptionsColumn.ReadOnly = true;
            this.columnIndicatorName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnIndicatorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnIndicatorName.Visible = true;
            this.columnIndicatorName.Width = 200;
            // 
            // columnIndicatorPlanTotal
            // 
            this.columnIndicatorPlanTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.columnIndicatorPlanTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnIndicatorPlanTotal.Caption = "����� ����";
            this.columnIndicatorPlanTotal.DisplayFormat.FormatString = "{0:N3}";
            this.columnIndicatorPlanTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnIndicatorPlanTotal.FieldName = "PLAN_SUMMARY";
            this.columnIndicatorPlanTotal.Name = "columnIndicatorPlanTotal";
            this.columnIndicatorPlanTotal.OptionsColumn.AllowEdit = false;
            this.columnIndicatorPlanTotal.OptionsColumn.ReadOnly = true;
            this.columnIndicatorPlanTotal.OptionsColumn.ShowInCustomizationForm = false;
            this.columnIndicatorPlanTotal.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnIndicatorPlanTotal.Visible = true;
            this.columnIndicatorPlanTotal.Width = 100;
            // 
            // columnIndicatorFactTotal
            // 
            this.columnIndicatorFactTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.columnIndicatorFactTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnIndicatorFactTotal.Caption = "����� ����";
            this.columnIndicatorFactTotal.DisplayFormat.FormatString = "{0:N3}";
            this.columnIndicatorFactTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnIndicatorFactTotal.FieldName = "FACT_SUMMARY";
            this.columnIndicatorFactTotal.Name = "columnIndicatorFactTotal";
            this.columnIndicatorFactTotal.OptionsColumn.AllowEdit = false;
            this.columnIndicatorFactTotal.OptionsColumn.ReadOnly = true;
            this.columnIndicatorFactTotal.OptionsColumn.ShowInCustomizationForm = false;
            this.columnIndicatorFactTotal.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnIndicatorFactTotal.Visible = true;
            this.columnIndicatorFactTotal.Width = 100;
            // 
            // columnIndicatorIsEditable
            // 
            this.columnIndicatorIsEditable.FieldName = "IS_EDITABLE";
            this.columnIndicatorIsEditable.Name = "columnIndicatorIsEditable";
            this.columnIndicatorIsEditable.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // repositoryIndicatorItemSpinEditPlan
            // 
            this.repositoryIndicatorItemSpinEditPlan.AutoHeight = false;
            this.repositoryIndicatorItemSpinEditPlan.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryIndicatorItemSpinEditPlan.Mask.EditMask = "n2";
            this.repositoryIndicatorItemSpinEditPlan.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositoryIndicatorItemSpinEditPlan.Name = "repositoryIndicatorItemSpinEditPlan";
            // 
            // repositoryIndicatorItemSpinEditFact
            // 
            this.repositoryIndicatorItemSpinEditFact.AutoHeight = false;
            this.repositoryIndicatorItemSpinEditFact.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryIndicatorItemSpinEditFact.Mask.EditMask = "n2";
            this.repositoryIndicatorItemSpinEditFact.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositoryIndicatorItemSpinEditFact.Name = "repositoryIndicatorItemSpinEditFact";
            // 
            // IndicatorsGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupIndicators);
            this.Name = "IndicatorsGrid";
            this.Size = new System.Drawing.Size(595, 264);
            ((System.ComponentModel.ISupportInitialize) (this.groupIndicators)).EndInit();
            this.groupIndicators.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.gridControlIndicators)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridViewIndicators)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryIndicatorItemSpinEditPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryIndicatorItemSpinEditFact)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupIndicators;
        private DevExpress.XtraGrid.GridControl gridControlIndicators;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewIndicators;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridIndicatorsMainBand;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn columnIndicatorId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn columnIndicatorName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn columnIndicatorPlanTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn columnIndicatorFactTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn columnIndicatorIsEditable;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryIndicatorItemSpinEditPlan;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryIndicatorItemSpinEditFact;
    }
}
