﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraBars;
using Logica.Reports.BaseReportControl.CommonControls.AddressesFilters;

namespace Logica.Reports.BaseReportControl.CommonControls.Addresses
{
    public class CustomFilterShowingArgs: EventArgs
    {
        private object chanelId;

        public CustomFilterShowingArgs()
        {

        }

        public CustomFilterShowingArgs(object chanelId)
        {
            this.chanelId = chanelId;
        }

        public object ChanelId { get { return this.chanelId; } }
        public FilterBaseForm FilterForm { get; set; } 
    }

    /// <summary>
    /// 
    /// </summary>
    public class FilterChangedArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public AddressesInformation FilterInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        public string PocIds
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ComboBoxItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComboBoxItem"/> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="text">The text.</param>
        public ComboBoxItem(object id, string text)
        {
            Id = id;
            Text = text;
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public object Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        public string Text
        {
            get;
            set;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Text;
        }
    }

    public static class Extensions 
    {
        public static bool Visible(this BarItem barItem) 
        {
            return barItem.Visibility == BarItemVisibility.Always;
        }

        public static void Visible(this BarItem barItem, bool visible)
        {
            barItem.Visibility = visible ? BarItemVisibility.Always : BarItemVisibility.Never;
        }
    }
}
