﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logica.Reports.BaseReportControl.Utility
{
  /// <summary>
  /// Addresses information class to return data from Filter forms
  /// </summary>
  public class AddressesInformation
  {

    /// <summary>
    /// Initializes a new instance of the <see cref="AddressesInformation"/> class.
    /// </summary>
    public AddressesInformation()
    {
      Comment = string.Empty;
      ThRFilterId = 0;
    }

    /// <summary>
    /// Gets or sets the addresses list.
    /// </summary>
    /// <value>The addresses list.</value>
    public List<string> AddressesList { get; set; }

    /// <summary>
    /// Comment for selected items
    /// </summary>
    public string Comment { get; set; }

    /// <summary>
    /// ThR filter ID which was used in filter form. In all other forms it'll be kept as 0. 
    /// </summary>
    public Int64 ThRFilterId { get; set; }

    /// <summary>
    /// Gets the deduped addresses list.
    /// </summary>
    /// <value>The deduped addresses list.</value>
    public List<string> DedupedAddressesList { get { return (AddressesList == null) ? null : AddressesList.Distinct().ToList(); } }

  }
}
