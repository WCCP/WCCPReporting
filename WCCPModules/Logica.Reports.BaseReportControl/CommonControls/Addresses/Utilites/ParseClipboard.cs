﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Logica.Reports.BaseReportControl.Utility
{
    public static class ParseClipboard
    {
        private const string STR_REGEX_INTEGER = @"\d+";

        public static string GetAllText()
        {
            return Clipboard.GetText(TextDataFormat.Text).Trim();
        }

        public static bool IsEmpty()
        {
            return String.IsNullOrEmpty(GetAllText());
        }

        public static string[] GetCodes()
        {
            return
                new Regex(STR_REGEX_INTEGER).Matches(GetAllText())
                    .Cast<System.Text.RegularExpressions.Match>()
                    .Select(it => it.Value).Distinct().ToArray();
        }

        public static string GetNonCodes()
        {
            return new Regex(STR_REGEX_INTEGER).Replace(GetAllText(), String.Empty);
        }
    }
}
