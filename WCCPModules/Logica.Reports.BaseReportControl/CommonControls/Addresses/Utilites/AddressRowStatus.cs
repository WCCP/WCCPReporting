﻿namespace Logica.Reports.BaseReportControl.Utility
{
    /// <summary>
    /// 
    /// </summary>
    public enum AddressRowStatus
    {
        Edited = -1,
        /// <summary>
        /// 
        /// </summary>
        Common = 2,
        /// <summary>
        /// 
        /// </summary>
        New = 1,
        /// <summary>
        /// 
        /// </summary>
        Removed = 9
    }
}
