﻿namespace Logica.Reports.BaseReportControl.Utility {
    public static class RowStatusHelper {
        // Constants represent masks of row status
        public const int Unchanged = 1;
        public const int Inserted = 2;
        public const int Changed = 4;
        public const int Deleted = 8;

        public static AddressRowStatus RowStatusMaskToAddressRowStatus(int mask) {
            // order is important
            if ((mask & Deleted) == Deleted)
                return AddressRowStatus.Removed;
            if ((mask & Inserted) == Inserted)
                return AddressRowStatus.New;
            if ((mask & Changed) == Changed)
                return AddressRowStatus.Edited;
            return AddressRowStatus.Common;
        }

        public static int AddressRowStatusToRowStatusMask(AddressRowStatus addressRowStatus) {
            // order is not important
            int lResult = Unchanged;
            switch (addressRowStatus) {
                case AddressRowStatus.New:
                    lResult &= Inserted;
                    break;
                case AddressRowStatus.Edited:
                    lResult &= Changed;
                    break;
                case AddressRowStatus.Removed:
                    lResult &= Deleted;
                    break;
            }
            return lResult;
        }
    }
}