﻿using System;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;
using Logica.Reports.BaseReportControl.Utility;
using System.Collections.Generic;
using DevExpress.XtraLayout.Utils;
using System.ComponentModel;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.Common.WaitWindow;
using System.Xml.Linq;

namespace Logica.Reports.BaseReportControl.CommonControls.AddressesFilters
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FilterClipboardForm : FilterBaseForm
    {
        private string pocList;
        private const char separator = ',';
        private DataTable selectedData = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterClipboardForm"/> class.
        /// </summary>
        public FilterClipboardForm()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterClipboardForm"/> class.
        /// </summary>
        /// <param name="chanel">The chanel.</param>
        public FilterClipboardForm(object chanel)
            : base(chanel)
        {
            InitializeComponent();
        }

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether comment is required.
        /// </summary>
        /// <value><c>true</c> if [comment required]; otherwise, <c>false</c>.</value>
        [Description("Determines if comment field will be visible")]
        [DefaultValue(false)]
        public bool CommentRequired
        {
            get { return !splitContainer2.Panel2Collapsed; }
            set { splitContainer2.Panel2Collapsed = !value; }
        }

        /// <summary>
        /// Gets the poc id list.
        /// </summary>
        /// <value>The poc id list.</value>
        public override string POCList
        {
            get
            {
                return olList.GetSelectedOls();
            }
        }

        /// <summary>
        /// Gets the selected items with additional information.
        /// </summary>
        /// <value>
        /// AddressesInformation object which represents selected items with additional information.
        /// </value>
        public override AddressesInformation SelectedItems
        {
            get
            {
                return new AddressesInformation()
                {
                    AddressesList = new List<string>(POCList.Split(new char[] { separator })),
                    Comment = memoComment.Text
                };
            }
        }

        /// <summary>
        /// Gets or sets text that will be displayed in default OK button.
        /// </summary>
        public override string OKButtonText
        {
            get
            {
                return btnOk.Text;
            }
            set
            {
                btnOk.Text = value;
            }
        }


        #endregion

        /// <summary>
        /// Handles the Load event of the FilterClipboardForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FilterClipboardForm_Load(object sender, EventArgs e)
        {
            ProcessClipboard();
        }

        /// <summary>
        /// Updates the buttons.
        /// </summary>
        private void UpdateButtons()
        {
            btnOk.Enabled = selectedData != null && olList.SelectedOlCount > 0;
        }

        /// <summary>
        /// Processes the clipboard.
        /// </summary>
        private void ProcessClipboard()
        {
            if (ValidateChildren(ValidationConstraints.Selectable) && !ParseClipboard.IsEmpty())
            {
                pocList = new XElement(Constants.XML_OUTLET_ROOT_NAME, ParseClipboard.GetCodes().Select(x =>
                {
                    var e = new XElement(Constants.XML_OUTLET_ELEMENT_NAME);
                    e.SetAttributeValue(Constants.XML_OUTLET_ATTRIBUTE_FIELD_ID, x);
                    return e;
                })).ToString(SaveOptions.DisableFormatting);

                memoClipboard.Text = ParseClipboard.GetNonCodes();
                LoadPreviewList();
            }

            UpdateButtons();
        }


        /// <summary>
        /// Loads the preview list.
        /// </summary>
        private void LoadPreviewList()
        {
            WaitManager.StartWait();
            if (IsValidaData(pocList))
            {
                this.selectedData = DataProvider.GetAddressesListByOLIDs(pocList, this.Chanel, GetFilterParams());
                olList.SetData(this.selectedData);
            }
            else
            {
                this.selectedData = null;
                olList.SetData(null);
            }
            WaitManager.StopWait();
        }

        /// <summary>
        /// Determines whether [is valida data] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>
        /// 	<c>true</c> if [is valida data] [the specified text]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsValidaData(string text)
        {
            return !String.IsNullOrEmpty(text);
        }

        private void olList_POCListChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = olList.SelectedOlCount > 0;
        }
    }
}
