﻿using System;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using Logica.Reports.BaseReportControl.Utility;
using System.Data.SqlClient;
using System.Collections.Generic;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;

namespace Logica.Reports.BaseReportControl.CommonControls.AddressesFilters
{
    public class FilterBaseForm : XtraForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterBaseForm"/> class.
        /// </summary>
        public FilterBaseForm()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterBaseForm"/> class.
        /// </summary>
        /// <param name="chanel">The chanel.</param>
        public FilterBaseForm(object chanel)
        {
            if (chanel is Logica.Reports.BaseReportControl.CommonControls.Addresses.ComboBoxItem)
            {
                this.Chanel = ConvertEx.ToInt(((Logica.Reports.BaseReportControl.CommonControls.Addresses.ComboBoxItem)chanel).Id);
            }
            else
                this.Chanel = ConvertEx.ToInt(chanel);
            
        }

        /// <summary>
        /// Gets or sets the chanel.
        /// </summary>
        /// <value>The chanel.</value>
        public int? Chanel { get; set; }

        /// <summary>
        /// Gets the poc id list.
        /// </summary>
        /// <value>The poc id list.</value>
        public virtual string POCList
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the selected items with additional information.
        /// </summary>
        /// <value>AddressesInformation object which represents selected items with additional information.</value>
        public virtual AddressesInformation SelectedItems
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets or sets text that will be displayed in default OK button.
        /// </summary>
        public virtual String OKButtonText { get; set; }

        /// <summary>
        /// Occurs when [filter preview changing].
        /// </summary>
        public event FilterPreviewChangingHandler FilterPreviewChanging;

        /// <summary>
        /// Gets the filter params.
        /// </summary>
        /// <returns></returns>
        protected FilterPreviewChangingArgs GetFilterParams()
        {
            FilterPreviewChangingArgs eventArgs = new FilterPreviewChangingArgs();
            if (this.FilterPreviewChanging != null)
            {
                this.FilterPreviewChanging(eventArgs);
            }
            return eventArgs;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class FilterPreviewChangingArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the added outlets.
        /// </summary>
        /// <value>The added outlets.</value>
        public string AddedOutlets { get; set; }
        
        /// <summary>
        /// Gets or sets the promo gr outlets.
        /// </summary>
        /// <value>The promo gr outlets.</value>
        public string PromoGrOutlets { get; set; }
        
        /// <summary>
        /// Gets or sets the CTRL gr outlets.
        /// </summary>
        /// <value>The CTRL gr outlets.</value>
        public string CtrlGrOutlets { get; set; }

        /// <summary>
        /// Gets or sets the "IsCtrlGrOutlets" flag
        /// </summary>
        /// <value>true if Control Group, false otherwise.</value>
        public bool IsCtrlGroup { get; set; }

        /// <summary>
        /// Gets or sets the entity id.
        /// </summary>
        /// <value>The entity id.</value>
        public int? EntityId { get; set; }

        /// <summary>
        /// Gets or sets the module id.
        /// </summary>
        /// <value>The module id.</value>
        public int? ModuleId { get; set; }

        /// <summary>
        /// Gets as parameters list.
        /// </summary>
        /// <value>As parameters list.</value>
        internal IList<SqlParameter> AsParametersList
        {
            get
            {
                return new[] 
                {
                    new SqlParameter(Constants.PARAM_PRESENT_OUTLETS, this.AddedOutlets),
                    new SqlParameter(Constants.PARAM_PROMOTIONAL_OUTLETS, this.PromoGrOutlets),
                    new SqlParameter(Constants.PARAM_CONTROL_OUTLETS, this.CtrlGrOutlets),
                    new SqlParameter(Constants.PARAM_IS_CONTROL_GROUP, this.IsCtrlGroup),
                    new SqlParameter(Constants.PARAM_ENTITY_ID, this.EntityId),
                    new SqlParameter(Constants.PARAM_MODULE_ID, this.ModuleId)
                };
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public delegate void FilterPreviewChangingHandler(FilterPreviewChangingArgs args);
}