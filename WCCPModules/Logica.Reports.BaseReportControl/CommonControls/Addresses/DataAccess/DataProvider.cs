﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using Logica.Reports.Common;
using Logica.Reports.BaseReportControl.CommonControls.AddressesFilters;

namespace Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess
{
    internal class DataProvider
    {
        /// <summary>
        /// Gets the addresses list by OLID.
        /// </summary>
        /// <param name="strIdList">The STR id list.</param>
        /// <returns></returns>
        public static DataTable GetAddressesListByOLIDs(string strIdList, object chanel, FilterPreviewChangingArgs eventArgs)
        {
            DataTable dataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.PROC_GET_ADDRESSES_BY_IDS,
                (new SqlParameter[] { 
                            new SqlParameter(Constants.PARAM_GET_ADDRESSES_LIST_BY_OL_IDS, strIdList),
                            new SqlParameter(Constants.PARAM_CHANEL, chanel)
                        }).Concat(eventArgs.AsParametersList).ToArray());
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTable = dataSet.Tables[0];
            }
            return dataTable;
        }

        /// <summary>
        /// Gets the territory data.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTerritoryData()
        {
            try
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.PROC_GET_TERRITORY_DATA,
                    new SqlParameter[] { });
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the territory staff.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTerritoryStaff(string cities, object chanel)
        {
            try
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.PROC_GET_TERRITORY_STAFF,
                    new SqlParameter[] { 
                            new SqlParameter(Constants.PARAM_CITIES_PROC_GET_STAFF, cities),
                            new SqlParameter(Constants.PARAM_CHANEL, (chanel == null) ? DBNull.Value : chanel)
                    });
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the territory staff.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTerritoryOutlets(string routes, object chanel, FilterPreviewChangingArgs eventArgs)
        {
            try
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.PROC_GET_TERRITORY_OUTLETS,
                    (new SqlParameter[] { 
                            new SqlParameter(Constants.PARAM_ROUTES_PROC_GET_TERRITORY_OUTLETS, routes),
                            new SqlParameter(Constants.PARAM_CHANEL, ((int)chanel == 0) ? DBNull.Value : chanel)
                        }).Concat(eventArgs.AsParametersList).ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the thomas research filters.
        /// </summary>
        /// <param name="allFilters">if set to <c>true</c> selects all filters.</param>
        /// <returns></returns>
        public static DataTable GetThomasResearchFilters(Boolean allFilters, object chanel)
        {
            try
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.PROC_GET_TRFILTERS,
                    new SqlParameter[] { 
                            new SqlParameter(Constants.PARAM_ALL_PROC_GET_TRFILTERS, allFilters),
                            new SqlParameter(Constants.PARAM_CHANEL, chanel)
                        });
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the thomas research outletters.
        /// </summary>
        /// <param name="filterId">The filter id.</param>
        /// <returns></returns>
        public static DataTable GetThomasResearchOutlets(Int64 filterId, object chanel, FilterPreviewChangingArgs eventArgs)
        {
            DataTable dataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.PROC_GET_TROUTLETS,
                (new SqlParameter[] { 
                            new SqlParameter(Constants.PARAM_FILTER_ID_PROC_GET_TR_OUTLETS, filterId),
                            new SqlParameter(Constants.PARAM_CHANEL, chanel)
                        }).Concat(eventArgs.AsParametersList).ToArray());
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTable = dataSet.Tables[0];
            }
            return dataTable;
        }

        /// <summary>
        /// Gets chanels
        /// </summary>
        /// <returns>Table with chanels</returns>
        public static DataTable GetChanels()
        {
            DataTable res = null;

            try
            {
                DataAccessLayer.OpenConnection();
                DataSet ds = DataAccessLayer.ExecuteStoredProcedure(Constants.PROC_GET_CHANELS, new SqlParameter[] { });

                if (null != ds && ds.Tables.Count > 0)
                {
                    res = ds.Tables[0];
                }

                DataAccessLayer.CloseConnection();
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            return res;
        }
    }
}
