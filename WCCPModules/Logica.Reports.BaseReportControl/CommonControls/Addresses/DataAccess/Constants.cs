﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess
{
    internal class Constants
    {
        // Stored Procedure Names
        public const string PROC_GET_ADDRESSES_BY_IDS = "spDW_OUTLETROUTINES_GetOutletData";
        public const string PROC_GET_TERRITORY_DATA = "spDW_OUTLETROUTINES_GetTerritoryData";
        public const string PROC_GET_TERRITORY_STAFF = "spDW_OUTLETROUTINES_GetTerritoryStaff";
        public const string PROC_GET_TERRITORY_OUTLETS = "spDW_OUTLETROUTINES_GetTerritoryOutletList";
        public const string PROC_GET_TRFILTERS = "spDW_OUTLETROUTINES_GetTomasResearchFilterList";
        public const string PROC_GET_TROUTLETS = "spDW_OUTLETROUTINES_GetTomasResearchOutlets";
        public const string PROC_GET_CHANELS = "spDW_ChanelTypeList";

        public const string XML_OUTLET_ROOT_NAME = "root";
        public const string XML_OUTLET_ELEMENT_NAME = "row";
        public const string XML_OUTLET_ATTRIBUTE_FIELD_ID = "field_id";

        // Stored Procedure Parameter Names
        public const string PARAM_GET_ADDRESSES_LIST_BY_OL_IDS = "@OUTLET_LIST";
        public const string PARAM_CITIES_PROC_GET_STAFF = "@sCities";
        public const string PARAM_ROUTES_PROC_GET_TERRITORY_OUTLETS = "@sRoutes";
        public const string PARAM_ALL_PROC_GET_TRFILTERS = "@SelectAll";
        public const string PARAM_FILTER_ID_PROC_GET_TR_OUTLETS = "@TomasResearchFilter_ID";
        public const string PARAM_CHANEL = "@STAFFCHANNEL_ID";
        public const string PARAM_PRESENT_OUTLETS = "@PRESENT_OUTLETS";
        public const string PARAM_PROMOTIONAL_OUTLETS = "@PROMOTIONAL_OUTLETS";
        public const string PARAM_CONTROL_OUTLETS = "@CONTROL_OUTLETS";
        public const string PARAM_IS_CONTROL_GROUP = "@IsControlGroup";
        public const string PARAM_ENTITY_ID = "@MODULE_ENTITY_ID";
        public const string PARAM_MODULE_ID = "@MODULE_ID";

        public const string TERITORY_DATA_REGION_ID = "Region_id";
        public const string TERITORY_DATA_REGION_NAME = "Region_name";
        public const string TERITORY_DATA_DISTRICT_ID = "District_id";
        public const string TERITORY_DATA_DISTRICT_NAME = "District_name";
        public const string TERITORY_DATA_CITY_ID = "City_id";
        public const string TERITORY_DATA_CITY_NAME = "City_name";
        public const string TERITORY_DATA_SETTLEMENT_ID = "Settlement_id";

        public const string TERITORY_STAFF_M4_ID = "RM_Id";
        public const string TERITORY_STAFF_M4_NAME = "RM_name";
        public const string TERITORY_STAFF_M3_ID = "DSM_Id";
        public const string TERITORY_STAFF_M3_NAME = "DSM_Name";
        public const string TERITORY_STAFF_M2_ID = "Supervisor_id";
        public const string TERITORY_STAFF_M2_NAME = "Supervisor_name";
        public const string TERITORY_STAFF_M1_ID = "Merch_id";
        public const string TERITORY_STAFF_M1_NAME = "MerchName";
        public const string TERITORY_STAFF_ROUTE_ID = "Route_id";
        public const string TERITORY_STAFF_ROUTE_NAME = "RouteName";

        public const string FLD_CHANEL_ID = "ChanelType_id";
        public const string FLD_CHANEL_NAME = "ChanelType";
        public const string FLD_FILTER_PREVIEW_OUTLET_ID = "OUTLET_CODE";
        public const string FLD_FILTER_PREVIEW_OUTLET_STATUS = "OL_STATUS";

        public const int TERITORY_DATA_SETTLEMENT_VILLAGE = 1;
        public const int TERITORY_DATA_SETTLEMENT_CITY = 2;
    }

    /// <summary>
    /// 
    /// </summary>
    public enum AddressEditMode
    {
        /// <summary>
        /// 
        /// </summary>
        AllowAll = 0,
        /// <summary>
        /// 
        /// </summary>
        ReadOnly = 1,
        /// <summary>
        /// 
        /// </summary>
        AllowAddOnly = 2,
        /// <summary>
        /// 
        /// </summary>
        AllowRemoveOnly = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ChannelType
    {
        /// <summary>
        /// 
        /// </summary>
        All = 0,
        /// <summary>
        /// 
        /// </summary>
        OnTrade = 1,
        /// <summary>
        /// 
        /// </summary>
        OffTrade = 2,
        /// <summary>
        /// 
        /// </summary>
        KaTrade = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum FilterType
    {
        /// <summary>
        /// 
        /// </summary>
        Clipboard = 0,
        /// <summary>
        /// 
        /// </summary>
        ThomasResearch = 1,
        /// <summary>
        /// 
        /// </summary>
        Region = 2,
        /// <summary>
        /// 
        /// </summary>
        Custom = 3
    }
}
