﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    public partial class ColumnChooserForm : XtraForm
    {
        #region Constructors

        public ColumnChooserForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handling

        private void ColumnChooserForm_Shown(object sender, EventArgs e)
        {
            ColumnsChooserCtrl.DisEnaButtons();
        }

        private void ColumnsChooserCtrl_CancelClicked(object sender)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ColumnsChooserCtrl_SubmitClicked(object sender)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        #endregion
    }
}