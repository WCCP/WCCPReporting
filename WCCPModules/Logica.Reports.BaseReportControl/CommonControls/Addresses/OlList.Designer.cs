﻿namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class OlList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridColumnSelect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOlId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.grList = new DevExpress.XtraEditors.GroupControl();
            this.chkBoxSelectAll = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.grList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxSelectAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridColumnSelect
            // 
            this.gridColumnSelect.Caption = " ";
            this.gridColumnSelect.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnSelect.FieldName = "olSelected";
            this.gridColumnSelect.MaxWidth = 20;
            this.gridColumnSelect.Name = "gridColumnSelect";
            this.gridColumnSelect.Visible = true;
            this.gridColumnSelect.VisibleIndex = 0;
            this.gridColumnSelect.Width = 20;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(2, 22);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp,
            this.repositoryItemCheckEdit1});
            this.gridControl.Size = new System.Drawing.Size(670, 238);
            this.gridControl.TabIndex = 3;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            this.gridControl.DataSourceChanged += new System.EventHandler(this.gridControl_DataSourceChanged);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSelect,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumnOlId,
            this.gridColumn6,
            this.gridColumn5,
            this.gridColumnStatus});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            this.gridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanging);
            this.gridView.ColumnFilterChanged += new System.EventHandler(this.gridView_ColumnFilterChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "Регион";
            this.gridColumn1.FieldName = "REGION_NAME";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 81;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "М3";
            this.gridColumn2.FieldName = "M3_NAME";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 81;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Юр. имя";
            this.gridColumn3.FieldName = "LEGAL_NAME";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 81;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "Факт. имя";
            this.gridColumn4.FieldName = "TRADING_NAME";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 81;
            // 
            // gridColumnOlId
            // 
            this.gridColumnOlId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOlId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOlId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnOlId.Caption = "Код ТТ";
            this.gridColumnOlId.FieldName = "OUTLET_CODE";
            this.gridColumnOlId.Name = "gridColumnOlId";
            this.gridColumnOlId.OptionsColumn.AllowEdit = false;
            this.gridColumnOlId.OptionsColumn.AllowMove = false;
            this.gridColumnOlId.OptionsColumn.ReadOnly = true;
            this.gridColumnOlId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnOlId.Visible = true;
            this.gridColumnOlId.VisibleIndex = 5;
            this.gridColumnOlId.Width = 81;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.Caption = "Нас. пункт";
            this.gridColumn6.FieldName = "CITY";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowMove = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            this.gridColumn6.Width = 81;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.Caption = "Статус";
            this.gridColumn5.FieldName = "OL_STATUSDESCR";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 7;
            this.gridColumn5.Width = 81;
            // 
            // gridColumnStatus
            // 
            this.gridColumnStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnStatus.Caption = "OL_STATUS";
            this.gridColumnStatus.FieldName = "OL_STATUS";
            this.gridColumnStatus.Name = "gridColumnStatus";
            this.gridColumnStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "Status")});
            this.repositoryStatusLookUp.DisplayMember = "StatusName";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // grList
            // 
            this.grList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grList.Controls.Add(this.gridControl);
            this.grList.Location = new System.Drawing.Point(0, 0);
            this.grList.Name = "grList";
            this.grList.Size = new System.Drawing.Size(674, 262);
            this.grList.TabIndex = 4;
            this.grList.Text = "Статус:";
            // 
            // chkBoxSelectAll
            // 
            this.chkBoxSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkBoxSelectAll.EditValue = null;
            this.chkBoxSelectAll.Enabled = false;
            this.chkBoxSelectAll.Location = new System.Drawing.Point(21, 262);
            this.chkBoxSelectAll.Name = "chkBoxSelectAll";
            this.chkBoxSelectAll.Properties.AllowGrayed = true;
            this.chkBoxSelectAll.Properties.Caption = "Выбрать все";
            this.chkBoxSelectAll.Size = new System.Drawing.Size(100, 19);
            this.chkBoxSelectAll.TabIndex = 11;
            this.chkBoxSelectAll.CheckStateChanged += new System.EventHandler(this.chkBoxSelectAll_CheckedChanged);
            // 
            // OlList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkBoxSelectAll);
            this.Controls.Add(this.grList);
            this.Name = "OlList";
            this.Size = new System.Drawing.Size(674, 283);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.grList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxSelectAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOlId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.GroupControl grList;
        private DevExpress.XtraEditors.CheckEdit chkBoxSelectAll;
    }
}
