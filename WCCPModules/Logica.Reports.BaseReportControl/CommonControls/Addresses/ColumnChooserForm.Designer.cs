﻿namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class ColumnChooserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ColumnsChooserCtrl = new Logica.Reports.BaseReportControl.CommonControls.ColumnsChooser();
            this.SuspendLayout();
            // 
            // ColumnsChooserCtrl
            // 
            this.ColumnsChooserCtrl.AssociatedView = null;
            this.ColumnsChooserCtrl.CaptionSubmit = "Обновить адреску";
            this.ColumnsChooserCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnsChooserCtrl.FrozenColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.ColumnsChooserCtrl.FrozenColumnsDelimiter = "------";
            this.ColumnsChooserCtrl.InvisibleColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.ColumnsChooserCtrl.Location = new System.Drawing.Point(0, 0);
            this.ColumnsChooserCtrl.Name = "ColumnsChooserCtrl";
            this.ColumnsChooserCtrl.Size = new System.Drawing.Size(743, 489);
            this.ColumnsChooserCtrl.TabIndex = 0;
            this.ColumnsChooserCtrl.SubmitClicked += new Logica.Reports.BaseReportControl.CommonControls.ColumnsChooser.ButtonClickEventHandler(this.ColumnsChooserCtrl_SubmitClicked);
            this.ColumnsChooserCtrl.CancelClicked += new Logica.Reports.BaseReportControl.CommonControls.ColumnsChooser.ButtonClickEventHandler(this.ColumnsChooserCtrl_CancelClicked);
            // 
            // ColumnChooserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 489);
            this.Controls.Add(this.ColumnsChooserCtrl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ColumnChooserForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Управление колонками";
            this.Shown += new System.EventHandler(this.ColumnChooserForm_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        public ColumnsChooser ColumnsChooserCtrl;

    }
}