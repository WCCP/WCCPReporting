﻿namespace Logica.Reports.BaseReportControl.CommonControls.AddressesFilters
{
    partial class FilterRegionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.treeRegions = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeStaffList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.groupRegion = new DevExpress.XtraEditors.GroupControl();
            this.groupStaff = new DevExpress.XtraEditors.GroupControl();
            this.olList = new Logica.Reports.BaseReportControl.CommonControls.OlList();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.groupControlComment = new DevExpress.XtraEditors.GroupControl();
            this.memoComment = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemTerritory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPersonal = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItemRegion = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItemComment = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItemComment = new DevExpress.XtraLayout.SplitterItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.treeRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaffList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRegion)).BeginInit();
            this.groupRegion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupStaff)).BeginInit();
            this.groupStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlComment)).BeginInit();
            this.groupControlComment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTerritory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPersonal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemComment)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(842, 458);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            // 
            // treeRegions
            // 
            this.treeRegions.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeRegions.Location = new System.Drawing.Point(2, 22);
            this.treeRegions.Name = "treeRegions";
            this.treeRegions.OptionsView.ShowCheckBoxes = true;
            this.treeRegions.OptionsView.ShowColumns = false;
            this.treeRegions.OptionsView.ShowIndicator = false;
            this.treeRegions.Size = new System.Drawing.Size(253, 231);
            this.treeRegions.TabIndex = 1;
            this.treeRegions.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeRegions_AfterCheckNode);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 91;
            // 
            // treeStaffList
            // 
            this.treeStaffList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeStaffList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeStaffList.Location = new System.Drawing.Point(2, 22);
            this.treeStaffList.Name = "treeStaffList";
            this.treeStaffList.OptionsView.ShowCheckBoxes = true;
            this.treeStaffList.OptionsView.ShowColumns = false;
            this.treeStaffList.OptionsView.ShowIndicator = false;
            this.treeStaffList.Size = new System.Drawing.Size(301, 231);
            this.treeStaffList.TabIndex = 1;
            this.treeStaffList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeStaffList_AfterCheckNode);
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "treeListColumn2";
            this.treeListColumn2.FieldName = "treeListColumn2";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowEdit = false;
            this.treeListColumn2.OptionsColumn.ReadOnly = true;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            this.treeListColumn2.Width = 91;
            // 
            // groupRegion
            // 
            this.groupRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupRegion.Controls.Add(this.treeRegions);
            this.groupRegion.Location = new System.Drawing.Point(2, 2);
            this.groupRegion.Name = "groupRegion";
            this.groupRegion.Size = new System.Drawing.Size(257, 255);
            this.groupRegion.TabIndex = 3;
            this.groupRegion.Text = "Территория:";
            // 
            // groupStaff
            // 
            this.groupStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupStaff.Controls.Add(this.treeStaffList);
            this.groupStaff.Location = new System.Drawing.Point(269, 2);
            this.groupStaff.Name = "groupStaff";
            this.groupStaff.Size = new System.Drawing.Size(305, 255);
            this.groupStaff.TabIndex = 4;
            this.groupStaff.Text = "Персонал:";
            // 
            // olList
            // 
            this.olList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olList.Location = new System.Drawing.Point(0, 0);
            this.olList.Name = "olList";
            this.olList.Size = new System.Drawing.Size(910, 177);
            this.olList.TabIndex = 1;
            this.olList.POCListChanged += new System.EventHandler(this.olList_POCListChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(577, 458);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(259, 22);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "Вставить выбранные ТТ в адреску";
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.groupControlComment);
            this.layoutControl.Controls.Add(this.groupStaff);
            this.layoutControl.Controls.Add(this.groupRegion);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(910, 259);
            this.layoutControl.TabIndex = 6;
            this.layoutControl.Text = "layoutControl1";
            // 
            // groupControlComment
            // 
            this.groupControlComment.Controls.Add(this.memoComment);
            this.groupControlComment.Location = new System.Drawing.Point(584, 2);
            this.groupControlComment.Name = "groupControlComment";
            this.groupControlComment.Size = new System.Drawing.Size(324, 255);
            this.groupControlComment.TabIndex = 2;
            this.groupControlComment.Text = "Комментарий для добавляемых ТТ";
            // 
            // memoComment
            // 
            this.memoComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoComment.Location = new System.Drawing.Point(2, 22);
            this.memoComment.Name = "memoComment";
            this.memoComment.Size = new System.Drawing.Size(320, 231);
            this.memoComment.TabIndex = 7;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemTerritory,
            this.layoutControlItemPersonal,
            this.splitterItemRegion,
            this.layoutControlItemComment,
            this.splitterItemComment});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(910, 259);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemTerritory
            // 
            this.layoutControlItemTerritory.Control = this.groupRegion;
            this.layoutControlItemTerritory.CustomizationFormText = "layoutControlItemTerritory";
            this.layoutControlItemTerritory.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemTerritory.Name = "layoutControlItemTerritory";
            this.layoutControlItemTerritory.Size = new System.Drawing.Size(261, 259);
            this.layoutControlItemTerritory.Text = "layoutControlItemTerritory";
            this.layoutControlItemTerritory.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTerritory.TextToControlDistance = 0;
            this.layoutControlItemTerritory.TextVisible = false;
            // 
            // layoutControlItemPersonal
            // 
            this.layoutControlItemPersonal.Control = this.groupStaff;
            this.layoutControlItemPersonal.CustomizationFormText = "layoutControlItemPersonal";
            this.layoutControlItemPersonal.Location = new System.Drawing.Point(267, 0);
            this.layoutControlItemPersonal.Name = "layoutControlItemPersonal";
            this.layoutControlItemPersonal.Size = new System.Drawing.Size(309, 259);
            this.layoutControlItemPersonal.Text = "layoutControlItemPersonal";
            this.layoutControlItemPersonal.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPersonal.TextToControlDistance = 0;
            this.layoutControlItemPersonal.TextVisible = false;
            // 
            // splitterItemRegion
            // 
            this.splitterItemRegion.AllowHotTrack = true;
            this.splitterItemRegion.CustomizationFormText = "splitterItem1";
            this.splitterItemRegion.Location = new System.Drawing.Point(261, 0);
            this.splitterItemRegion.Name = "splitterItemRegion";
            this.splitterItemRegion.Size = new System.Drawing.Size(6, 259);
            // 
            // layoutControlItemComment
            // 
            this.layoutControlItemComment.Control = this.groupControlComment;
            this.layoutControlItemComment.CustomizationFormText = "layoutControlItemComment";
            this.layoutControlItemComment.Location = new System.Drawing.Point(582, 0);
            this.layoutControlItemComment.Name = "layoutControlItemComment";
            this.layoutControlItemComment.Size = new System.Drawing.Size(328, 259);
            this.layoutControlItemComment.Text = "layoutControlItemComment";
            this.layoutControlItemComment.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemComment.TextToControlDistance = 0;
            this.layoutControlItemComment.TextVisible = false;
            // 
            // splitterItemComment
            // 
            this.splitterItemComment.AllowHotTrack = true;
            this.splitterItemComment.CustomizationFormText = "splitterItemComment";
            this.splitterItemComment.Location = new System.Drawing.Point(576, 0);
            this.splitterItemComment.Name = "splitterItemComment";
            this.splitterItemComment.Size = new System.Drawing.Size(6, 259);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.layoutControl);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.olList);
            this.splitContainer1.Size = new System.Drawing.Size(910, 440);
            this.splitContainer1.SplitterDistance = 259;
            this.splitContainer1.TabIndex = 7;
            // 
            // FilterRegionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(934, 492);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.MinimizeBox = false;
            this.Name = "FilterRegionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вставка ТТ";
            ((System.ComponentModel.ISupportInitialize)(this.treeRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaffList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRegion)).EndInit();
            this.groupRegion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupStaff)).EndInit();
            this.groupStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlComment)).EndInit();
            this.groupControlComment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTerritory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPersonal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItemComment)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraTreeList.TreeList treeRegions;
        private DevExpress.XtraTreeList.TreeList treeStaffList;
        private DevExpress.XtraEditors.GroupControl groupRegion;
        private DevExpress.XtraEditors.GroupControl groupStaff;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private OlList olList;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTerritory;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPersonal;
        private DevExpress.XtraLayout.SplitterItem splitterItemRegion;
        private DevExpress.XtraLayout.SplitterItem splitterItemComment;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraEditors.GroupControl groupControlComment;
        private DevExpress.XtraEditors.MemoEdit memoComment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemComment;
    }
}