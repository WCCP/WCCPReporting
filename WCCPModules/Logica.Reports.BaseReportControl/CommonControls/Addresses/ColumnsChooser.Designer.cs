﻿namespace Logica.Reports.BaseReportControl.CommonControls
{
  partial class ColumnsChooser
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColumnsChooser));
        this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
        this.panel2 = new System.Windows.Forms.Panel();
        this.listAvailableColumns = new DevExpress.XtraEditors.ListBoxControl();
        this.panel1 = new System.Windows.Forms.Panel();
        this.listToDown = new DevExpress.XtraEditors.SimpleButton();
        this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
        this.listToUp = new DevExpress.XtraEditors.SimpleButton();
        this.listUsedColumns = new DevExpress.XtraEditors.ListBoxControl();
        this.btnRemoveColumns = new DevExpress.XtraEditors.SimpleButton();
        this.btnAddColumns = new DevExpress.XtraEditors.SimpleButton();
        this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
        this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
        this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
        this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
        this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
        this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
        this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
        this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
        this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
        this.btnOk = new DevExpress.XtraEditors.SimpleButton();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
        this.layoutControl.SuspendLayout();
        this.panel2.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.listAvailableColumns)).BeginInit();
        this.panel1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.listUsedColumns)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
        this.SuspendLayout();
        // 
        // layoutControl
        // 
        this.layoutControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                    | System.Windows.Forms.AnchorStyles.Left)
                    | System.Windows.Forms.AnchorStyles.Right)));
        this.layoutControl.Controls.Add(this.panel2);
        this.layoutControl.Controls.Add(this.panel1);
        this.layoutControl.Controls.Add(this.btnRemoveColumns);
        this.layoutControl.Controls.Add(this.btnAddColumns);
        this.layoutControl.Location = new System.Drawing.Point(0, 0);
        this.layoutControl.Name = "layoutControl";
        this.layoutControl.Root = this.layoutControlGroup1;
        this.layoutControl.Size = new System.Drawing.Size(749, 470);
        this.layoutControl.TabIndex = 0;
        this.layoutControl.Text = "layoutControl1";
        // 
        // panel2
        // 
        this.panel2.Controls.Add(this.listAvailableColumns);
        this.panel2.Location = new System.Drawing.Point(12, 12);
        this.panel2.Name = "panel2";
        this.panel2.Size = new System.Drawing.Size(336, 446);
        this.panel2.TabIndex = 13;
        // 
        // listAvailableColumns
        // 
        this.listAvailableColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                    | System.Windows.Forms.AnchorStyles.Left)
                    | System.Windows.Forms.AnchorStyles.Right)));
        this.listAvailableColumns.Location = new System.Drawing.Point(0, 3);
        this.listAvailableColumns.Name = "listAvailableColumns";
        this.listAvailableColumns.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
        this.listAvailableColumns.Size = new System.Drawing.Size(336, 443);
        this.listAvailableColumns.TabIndex = 4;
        // 
        // panel1
        // 
        this.panel1.Controls.Add(this.listToDown);
        this.panel1.Controls.Add(this.listToUp);
        this.panel1.Controls.Add(this.listUsedColumns);
        this.panel1.Location = new System.Drawing.Point(402, 12);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(335, 446);
        this.panel1.TabIndex = 8;
        // 
        // listToDown
        // 
        this.listToDown.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
        this.listToDown.ImageIndex = 2;
        this.listToDown.ImageList = this.imageCollection;
        this.listToDown.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
        this.listToDown.Location = new System.Drawing.Point(171, 398);
        this.listToDown.Name = "listToDown";
        this.listToDown.Size = new System.Drawing.Size(50, 40);
        this.listToDown.TabIndex = 12;
        this.listToDown.Click += new System.EventHandler(this.listToDown_Click);
        // 
        // imageCollection
        // 
        this.imageCollection.ImageSize = new System.Drawing.Size(32, 32);
        this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
        this.imageCollection.Images.SetKeyName(0, "arrow_left.png");
        this.imageCollection.Images.SetKeyName(1, "arrow_right.png");
        this.imageCollection.Images.SetKeyName(2, "arrow_down.png");
        this.imageCollection.Images.SetKeyName(3, "arrow_up.png");
        // 
        // listToUp
        // 
        this.listToUp.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
        this.listToUp.ImageIndex = 3;
        this.listToUp.ImageList = this.imageCollection;
        this.listToUp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
        this.listToUp.Location = new System.Drawing.Point(115, 398);
        this.listToUp.Name = "listToUp";
        this.listToUp.Size = new System.Drawing.Size(50, 40);
        this.listToUp.TabIndex = 11;
        this.listToUp.Click += new System.EventHandler(this.listToUp_Click);
        // 
        // listUsedColumns
        // 
        this.listUsedColumns.AllowDrop = true;
        this.listUsedColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                    | System.Windows.Forms.AnchorStyles.Left)
                    | System.Windows.Forms.AnchorStyles.Right)));
        this.listUsedColumns.Location = new System.Drawing.Point(0, 3);
        this.listUsedColumns.Name = "listUsedColumns";
        this.listUsedColumns.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
        this.listUsedColumns.Size = new System.Drawing.Size(335, 389);
        this.listUsedColumns.TabIndex = 7;
        this.listUsedColumns.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.listUsedColumns_DrawItem);
        this.listUsedColumns.SelectedIndexChanged += new System.EventHandler(this.listUsedColumns_SelectedIndexChanged);
        // 
        // btnRemoveColumns
        // 
        this.btnRemoveColumns.AutoSizeInLayoutControl = false;
        this.btnRemoveColumns.ImageIndex = 0;
        this.btnRemoveColumns.ImageList = this.imageCollection;
        this.btnRemoveColumns.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
        this.btnRemoveColumns.Location = new System.Drawing.Point(352, 237);
        this.btnRemoveColumns.Name = "btnRemoveColumns";
        this.btnRemoveColumns.Size = new System.Drawing.Size(46, 44);
        this.btnRemoveColumns.StyleController = this.layoutControl;
        this.btnRemoveColumns.TabIndex = 6;
        this.btnRemoveColumns.Click += new System.EventHandler(this.btnRemoveColumns_Click);
        // 
        // btnAddColumns
        // 
        this.btnAddColumns.AutoSizeInLayoutControl = false;
        this.btnAddColumns.ImageIndex = 1;
        this.btnAddColumns.ImageList = this.imageCollection;
        this.btnAddColumns.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
        this.btnAddColumns.Location = new System.Drawing.Point(352, 153);
        this.btnAddColumns.Name = "btnAddColumns";
        this.btnAddColumns.Size = new System.Drawing.Size(46, 44);
        this.btnAddColumns.StyleController = this.layoutControl;
        this.btnAddColumns.TabIndex = 5;
        this.btnAddColumns.Click += new System.EventHandler(this.btnAddColumns_Click);
        // 
        // layoutControlGroup1
        // 
        this.layoutControlGroup1.CustomizationFormText = "Root";
        this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
        this.layoutControlGroup1.GroupBordersVisible = false;
        this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem5,
            this.layoutControlItem6});
        this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
        this.layoutControlGroup1.Name = "Root";
        this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
        this.layoutControlGroup1.Size = new System.Drawing.Size(749, 470);
        this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
        this.layoutControlGroup1.Text = "Root";
        this.layoutControlGroup1.TextVisible = false;
        // 
        // layoutControlItem2
        // 
        this.layoutControlItem2.Control = this.btnAddColumns;
        this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
        this.layoutControlItem2.Location = new System.Drawing.Point(340, 141);
        this.layoutControlItem2.MaxSize = new System.Drawing.Size(110, 48);
        this.layoutControlItem2.MinSize = new System.Drawing.Size(50, 48);
        this.layoutControlItem2.Name = "layoutControlItem2";
        this.layoutControlItem2.Size = new System.Drawing.Size(50, 48);
        this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this.layoutControlItem2.Text = "layoutControlItem2";
        this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
        this.layoutControlItem2.TextToControlDistance = 0;
        this.layoutControlItem2.TextVisible = false;
        // 
        // layoutControlItem3
        // 
        this.layoutControlItem3.Control = this.btnRemoveColumns;
        this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
        this.layoutControlItem3.Location = new System.Drawing.Point(340, 225);
        this.layoutControlItem3.MaxSize = new System.Drawing.Size(110, 48);
        this.layoutControlItem3.MinSize = new System.Drawing.Size(50, 48);
        this.layoutControlItem3.Name = "layoutControlItem3";
        this.layoutControlItem3.Size = new System.Drawing.Size(50, 48);
        this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this.layoutControlItem3.Text = "layoutControlItem3";
        this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
        this.layoutControlItem3.TextToControlDistance = 0;
        this.layoutControlItem3.TextVisible = false;
        // 
        // emptySpaceItem1
        // 
        this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
        this.emptySpaceItem1.Location = new System.Drawing.Point(340, 273);
        this.emptySpaceItem1.MaxSize = new System.Drawing.Size(110, 0);
        this.emptySpaceItem1.MinSize = new System.Drawing.Size(50, 10);
        this.emptySpaceItem1.Name = "emptySpaceItem1";
        this.emptySpaceItem1.Size = new System.Drawing.Size(50, 177);
        this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this.emptySpaceItem1.Text = "emptySpaceItem1";
        this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
        // 
        // emptySpaceItem2
        // 
        this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
        this.emptySpaceItem2.Location = new System.Drawing.Point(340, 0);
        this.emptySpaceItem2.MinSize = new System.Drawing.Size(50, 30);
        this.emptySpaceItem2.Name = "emptySpaceItem2";
        this.emptySpaceItem2.Size = new System.Drawing.Size(50, 141);
        this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this.emptySpaceItem2.Text = "emptySpaceItem2";
        this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
        // 
        // emptySpaceItem3
        // 
        this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
        this.emptySpaceItem3.Location = new System.Drawing.Point(340, 189);
        this.emptySpaceItem3.MaxSize = new System.Drawing.Size(110, 36);
        this.emptySpaceItem3.MinSize = new System.Drawing.Size(50, 36);
        this.emptySpaceItem3.Name = "emptySpaceItem3";
        this.emptySpaceItem3.Size = new System.Drawing.Size(50, 36);
        this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this.emptySpaceItem3.Text = "emptySpaceItem3";
        this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
        // 
        // layoutControlItem5
        // 
        this.layoutControlItem5.Control = this.panel1;
        this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
        this.layoutControlItem5.Location = new System.Drawing.Point(390, 0);
        this.layoutControlItem5.MinSize = new System.Drawing.Size(110, 30);
        this.layoutControlItem5.Name = "layoutControlItem5";
        this.layoutControlItem5.Size = new System.Drawing.Size(339, 450);
        this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this.layoutControlItem5.Text = "layoutControlItem5";
        this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
        this.layoutControlItem5.TextToControlDistance = 0;
        this.layoutControlItem5.TextVisible = false;
        // 
        // layoutControlItem6
        // 
        this.layoutControlItem6.Control = this.panel2;
        this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
        this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
        this.layoutControlItem6.MinSize = new System.Drawing.Size(110, 30);
        this.layoutControlItem6.Name = "layoutControlItem6";
        this.layoutControlItem6.Size = new System.Drawing.Size(340, 450);
        this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this.layoutControlItem6.Text = "layoutControlItem6";
        this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
        this.layoutControlItem6.TextToControlDistance = 0;
        this.layoutControlItem6.TextVisible = false;
        // 
        // btnCancel
        // 
        this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.btnCancel.Location = new System.Drawing.Point(637, 479);
        this.btnCancel.Name = "btnCancel";
        this.btnCancel.Size = new System.Drawing.Size(109, 37);
        this.btnCancel.TabIndex = 1;
        this.btnCancel.Text = "Отмена";
        this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
        // 
        // btnOk
        // 
        this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.btnOk.Location = new System.Drawing.Point(485, 479);
        this.btnOk.Name = "btnOk";
        this.btnOk.Size = new System.Drawing.Size(146, 37);
        this.btnOk.TabIndex = 2;
        this.btnOk.Text = "Обновить";
        this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
        // 
        // ColumnsChooser
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this.btnOk);
        this.Controls.Add(this.btnCancel);
        this.Controls.Add(this.layoutControl);
        this.Name = "ColumnsChooser";
        this.Size = new System.Drawing.Size(749, 521);
        ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
        this.layoutControl.ResumeLayout(false);
        this.panel2.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.listAvailableColumns)).EndInit();
        this.panel1.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.listUsedColumns)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl layoutControl;
    private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
    private DevExpress.XtraEditors.ListBoxControl listUsedColumns;
    private DevExpress.XtraEditors.SimpleButton btnRemoveColumns;
    private DevExpress.XtraEditors.SimpleButton btnAddColumns;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    private DevExpress.XtraEditors.SimpleButton btnCancel;
    private DevExpress.XtraEditors.SimpleButton btnOk;
    private DevExpress.Utils.ImageCollection imageCollection;
    private DevExpress.XtraEditors.ListBoxControl listAvailableColumns;
    private DevExpress.XtraEditors.SimpleButton listToUp;
    private DevExpress.XtraEditors.SimpleButton listToDown;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel panel1;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
  }
}
