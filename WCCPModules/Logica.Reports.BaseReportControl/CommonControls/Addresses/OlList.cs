﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class OlList : UserControl
    {
        private DataTable source = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="OlList"/> class.
        /// </summary>
        public OlList()
        {
            InitializeComponent();
        }

        public event EventHandler POCListChanged;

        /// <summary>
        /// Gets the rows count.
        /// </summary>
        /// <value>The rows count.</value>
        public int RowsCount
        {
            get
            {
                DataTable data = this.gridControl.DataSource as DataTable;
                if (data == null) return 0;

                return data.Rows.Count;
            }
        }

        /// <summary>
        /// Gets the selected ol count.
        /// </summary>
        /// <value>The selected ol count.</value>
        public int SelectedOlCount
        {
            get
            {
                return (from DataRow row in GetDisplayedRows()
                        where ConvertEx.ToBool(row[this.gridColumnSelect.FieldName])
                        select row).Count();
            }
        }

        /// <summary>
        /// Gets the cant be added ol count.
        /// </summary>
        /// <value>The cant be added ol count.</value>
        public int CantBeAddedOlCount
        {
            get
            {
                return (from DataRow row in GetDisplayedRows()
                        where !ConvertEx.ToBool(row[Constants.FLD_FILTER_PREVIEW_OUTLET_STATUS])
                        select row).Count();
            }
        }

        /// <summary>
        /// Sets the data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void SetData(DataTable data)
        {
            this.source = data;

            if (data != null)
            {
                DataColumn column = new DataColumn(this.gridColumnSelect.FieldName, typeof(bool));
                column.DefaultValue = false;
                data.Columns.Add(column);
            }

            this.gridControl.DataSource = data;

            this.chkBoxSelectAll.CheckState = CheckState.Unchecked;
            SetSelectedToAll(true);
            this.chkBoxSelectAll.Enabled = this.chkBoxSelectAll.CheckState != CheckState.Unchecked;
        }

        /// <summary>
        /// Gets the selected ols.
        /// </summary>
        /// <returns></returns>
        public string GetSelectedOls()
        {
            string ids = string.Empty;

            DataTable data = this.gridControl.DataSource as DataTable;

            if (data == null)
            {
                return ids;
            }

            foreach (DataRow row in GetDisplayedRows())
            {
                if (!ConvertEx.ToBool(row[this.gridColumnSelect.FieldName])
                    || !ConvertEx.ToBool(row[Constants.FLD_FILTER_PREVIEW_OUTLET_STATUS]))
                {
                    continue;
                }

                string id = row[this.gridColumnOlId.FieldName].ToString();

                if (!string.IsNullOrEmpty(ids))
                {
                    ids += ",";
                }

                ids += id;
            }

            return ids;
        }

        /// <summary>
        /// Handles the CellValueChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            CalculateStatistics();
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            CalculateStatistics();
        }

        private void gridView_ColumnFilterChanged(object sender, EventArgs e)
        {
            CalculateStatistics();
        }

        /// <summary>
        /// Handles the CellValueChanging event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == this.gridColumnSelect)
            {
                if (ConvertEx.ToBool(e.Value) && !ConvertEx.ToBool(this.gridView.GetRowCellValue(e.RowHandle, Constants.FLD_FILTER_PREVIEW_OUTLET_STATUS)))
                {
                    this.gridView.HideEditor();
                }
                else
                {
                    gridView.HideEditor();
                    gridView.SetRowCellValue(e.RowHandle, e.Column, e.Value);
                }
            }
            CalculateStatistics();
        }

        private void chkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.refreshingChkBoxSelectAll)
            {
                this.SetSelectedToAll(this.chkBoxSelectAll.CheckState == CheckState.Checked);
            }
        }

        /// <summary>
        /// Sets the selected to all.
        /// </summary>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        private void SetSelectedToAll(bool selected)
        {
            DataTable dataTable = this.gridControl.DataSource as DataTable;
            if (dataTable == null) return;

            foreach (DataRow row in dataTable.Rows)
            {
                if (!selected || ConvertEx.ToBool(row[Constants.FLD_FILTER_PREVIEW_OUTLET_STATUS]))
                {
                    row[this.gridColumnSelect.FieldName] = selected;
                }
            }

            CalculateStatistics();
        }

        /// <summary>
        /// Processes the error POCs.
        /// </summary>
        private void CalculateStatistics()
        {
            grList.Text = string.Format(Resource.OlFilterStatistics,
                this.RowsCount, this.CantBeAddedOlCount, this.SelectedOlCount);
            
            RefreshChkBoxSelectAll();

            if (POCListChanged != null) 
            {
                POCListChanged(this, new EventArgs());
            }
        }

        private bool refreshingChkBoxSelectAll = false;
        private void RefreshChkBoxSelectAll()
        {
            refreshingChkBoxSelectAll = true;

            IList<DataRow> rows = GetDisplayedRows();
            if (rows.Count == 0)
            {
                this.chkBoxSelectAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                this.chkBoxSelectAll.Checked = rows[0].Field<bool>(this.gridColumnSelect.FieldName);

                foreach (DataRow row in rows)
                {
                    if (this.chkBoxSelectAll.Checked != row.Field<bool>(this.gridColumnSelect.FieldName))
                    {
                        this.chkBoxSelectAll.CheckState = CheckState.Indeterminate;
                        break;
                    }
                }
            }

            refreshingChkBoxSelectAll = false;
        }

        private IList<DataRow> GetDisplayedRows()
        {
            IList<DataRow> rows = new List<DataRow>();

            if (this.gridView == null)
            {
                return rows;
            }

            for (int handle = 0; handle < this.gridView.DataRowCount; handle++)
            {
                rows.Add((DataRow)this.gridView.GetDataRow(handle));
            }

            return rows;
        }

        private void gridControl_DataSourceChanged(object sender, EventArgs e)
        {
            CalculateStatistics();
        }
    }
}