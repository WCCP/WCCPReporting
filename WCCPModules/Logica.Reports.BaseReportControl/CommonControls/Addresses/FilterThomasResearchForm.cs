﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.Common;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;

namespace Logica.Reports.BaseReportControl.CommonControls.AddressesFilters
{
  /// <summary>
  /// 
  /// </summary>
  public partial class FilterThomasResearchForm : FilterBaseForm
  {
    private Int64 filterId = 0;

    /// <summary>
    /// Initializes a new instance of the <see cref="FilterThomasResearchForm"/> class.
    /// </summary>
    public FilterThomasResearchForm()
      : this(null)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="FilterThomasResearchForm"/> class.
    /// </summary>
    /// <param name="chanel">The chanel.</param>
    public FilterThomasResearchForm(object chanel)
      : base(chanel)
    {
      InitializeComponent();

      radioGroupOwner.SelectedIndex = 0;

      LoadData();
    }

    #region Properties
    /// <summary>
    /// Gets the poc id list.
    /// </summary>
    /// <value>The poc id list.</value>
    public override string POCList
    {
      get
      {
        return olList.GetSelectedOls();
      }
    }

    /// <summary>
    /// Gets the selected items with additional information.
    /// </summary>
    /// <value>
    /// AddressesInformation object which represents selected items with additional information.
    /// </value>
    public override AddressesInformation SelectedItems
    {
      get
      {
        return new AddressesInformation()
        {
          AddressesList = new List<string>(POCList.Split(new char[] { ',' })),
          Comment = "",
          ThRFilterId = filterId
        };
      }
    }
    /// <summary>
    /// Gets or sets text that will be displayed in default OK button.
    /// </summary>
    public override string OKButtonText
    {
        get
        {
            return btnOk.Text;
        }
        set
        {
            btnOk.Text = value;
        }
    }

    #endregion

    /// <summary>
    /// Handles the SelectedIndexChanged event of the radioGroupOwner control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void radioGroupOwner_SelectedIndexChanged(object sender, EventArgs e)
    {
      LoadData();
    }

    /// <summary>
    /// Loads the data.
    /// </summary>
    private void LoadData()
    {
      WaitManager.StartWait();
      this.gridControlFilters.DataSource = DataProvider.GetThomasResearchFilters(radioGroupOwner.SelectedIndex == 0, this.Chanel);
      this.txtBoxFilter.Text = String.Empty;
      this.olList.SetData(null);

      gridView_FocusedRowChanged(this, null);
      WaitManager.StopWait();
    }

    /// <summary>
    /// Handles the FocusedRowChanged event of the gridView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
    private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
    {
      foreach (int rowHandle in gridView.GetSelectedRows())
      {
        DataRowView row = ((DataRowView)gridView.GetRow(rowHandle));

        txtBoxFilter.Text = row == null ? string.Empty : (row[this.gridColumnFilterText.FieldName]).ToString();
      }
    }

    /// <summary>
    /// Handles the Click event of the applyFilter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void applyFilter_Click(object sender, EventArgs e)
    {
      WaitManager.StartWait();
      foreach (int rowHandle in gridView.GetSelectedRows())
      {
        DataRowView row = ((DataRowView)gridView.GetRow(rowHandle));
        filterId = (Int64)(row[this.gridColumnFilterId.FieldName]);

        DataTable data = null;

        try
        {
          data = DataProvider.GetThomasResearchOutlets(filterId, this.Chanel, GetFilterParams());
          if (data == null || data.Rows.Count == 0)
          {
            ErrorManager.ShowErrorBox(Resource.NoOlFoundByFilter);
          }
        }
        catch
        {
          ErrorManager.ShowErrorBox(Resource.BadFilter);
        }

        this.olList.SetData(data);

      }
      WaitManager.StopWait();
    }

    private void olList_POCListChanged(object sender, EventArgs e)
    {
        btnOk.Enabled = olList.SelectedOlCount > 0;
    }

  }
}
