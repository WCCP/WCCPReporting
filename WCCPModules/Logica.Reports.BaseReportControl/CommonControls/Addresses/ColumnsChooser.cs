﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    public partial class ColumnsChooser : XtraUserControl
    {
        #region Delegates

        public delegate void ButtonClickEventHandler(object sender);

        #endregion

        #region Readonly & Static Fields

        private readonly List<GridColumn> frozenColumns = new List<GridColumn>();
        private readonly List<GridColumn> invisibleColumns = new List<GridColumn>();

        #endregion

        #region Fields

        private string frozenColumnsDelimiter;
        private GridView view;

        #endregion

        #region Constructors

        public ColumnsChooser()
        {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        [Description("Grid which should be modified")]
        public GridView AssociatedView
        {
            get { return view; }
            set
            {
                view = value;
                ReLoadData();
            }
        }

        [Description("Text for cancel button")]
        [DefaultValue("Отмена")]
        public string CaptionCancel
        {
            get { return btnCancel.Text; }
            set { btnCancel.Text = value; }
        }

        [Description("Text for submit button")]
        [DefaultValue("Обновить адреск")]
        public string CaptionSubmit
        {
            get { return btnOk.Text; }
            set { btnOk.Text = value; }
        }

        [Description("Disable moving of frozen columns")]
        [DefaultValueAttribute(false)]
        public bool DisableFrozenColumnsMoving { get; set; }

        [Description("FrozenColumns")]
        public GridColumn[] FrozenColumns
        {
            get { return frozenColumns.ToArray(); }
            set
            {
                frozenColumns.Clear();
                frozenColumns.AddRange(value);
                ReLoadData();
            }
        }

        [Description("Delimiter between group of frozen columns and regular columns")]
        [DefaultValue("------------")]
        public string FrozenColumnsDelimiter
        {
            get { return frozenColumnsDelimiter; }
            set
            {
                frozenColumnsDelimiter = value;
                ReLoadData();
            }
        }

        [Description("InvisibleColumns")]
        public GridColumn[] InvisibleColumns
        {
            get { return invisibleColumns.ToArray(); }
            set
            {
                invisibleColumns.Clear();
                invisibleColumns.AddRange(value);
                ReLoadData();
            }
        }

        #endregion

        #region Instance Methods

        public void DisEnaButtons()
        {
            bool isSelected = listUsedColumns.SelectedItems.Count > 0;
            if (DisableFrozenColumnsMoving && isSelected)
            {
                List<object> selItems = listUsedColumns.SelectedItems.Cast<object>().ToList();
                foreach (object selectedItem in selItems)
                {
                    if (!(selectedItem is GridColumn) || frozenColumns.IndexOf(selectedItem as GridColumn) > -1)
                        listUsedColumns.SetSelected(listUsedColumns.Items.IndexOf(selectedItem), false);
                }
            }

            isSelected = listUsedColumns.SelectedItems.Count > 0;
            int minSelectedIndex = isSelected ? listUsedColumns.SelectedIndices.Min() : 0;
            int maxSelectedIndex = isSelected ? listUsedColumns.SelectedIndices.Max() : 0;
            int splitterIndex = listUsedColumns.Items.IndexOf(frozenColumnsDelimiter);

            btnRemoveColumns.Enabled = listUsedColumns.SelectedItems.Cast<object>().Any(item => item is GridColumn);
            listToDown.Enabled = isSelected && maxSelectedIndex < listUsedColumns.Items.Count - 1;
            listToUp.Enabled = isSelected && minSelectedIndex > (DisableFrozenColumnsMoving ? splitterIndex + 1 : 0);
        }

        private bool IsObjectCanBeRemoved(object o)
        {
            return o is GridColumn && (!DisableFrozenColumnsMoving || frozenColumns.IndexOf(o as GridColumn) == -1);
        }

        private void MoveSelectedItem(ListBoxControl listBox, bool up, IEnumerable<int> selectedIds, Func<int, int> moveRule)
        {
            IList<object> selectedItems = selectedIds.Select(x => listBox.Items[x]).ToList();

            foreach (int index in selectedIds)
            {
                if (index == 0 && up || index == listBox.Items.Count - 1 && !up) continue;

                Object item = listBox.Items[index];
                listBox.Items.Remove(item);
                listBox.Items.Insert(moveRule(index), item);
            }

            listBox.SelectedIndex = -1;

            foreach (object item in selectedItems)
            {
                listBox.SetSelected(listBox.Items.IndexOf(item), true);
            }
        }

        public void ReLoadData()
        {
            listAvailableColumns.Items.Clear();
            listUsedColumns.Items.Clear();

            if (view == null)
                return;

            listUsedColumns.Items.AddRange(frozenColumns.ToArray());
            if (!string.IsNullOrEmpty(frozenColumnsDelimiter))
            {
                listUsedColumns.Items.Add(frozenColumnsDelimiter);
            }

            List<GridColumn> alreadyUsedColumns = new List<GridColumn>();
            alreadyUsedColumns.AddRange(invisibleColumns);
            alreadyUsedColumns.AddRange(frozenColumns);

            foreach (GridColumn column in view.Columns)
            {
                if (alreadyUsedColumns.IndexOf(column) > -1)
                {
                    continue;
                }
                if (column.Visible)
                {
                    listUsedColumns.Items.Add(column);
                }
                else
                {
                    listAvailableColumns.Items.Add(column);
                }
            }
        }

        #endregion

        #region Event Handling

        private void btnAddColumns_Click(object sender, EventArgs e)
        {
            if (listAvailableColumns.SelectedItems.Count == 0)
            {
                return;
            }
            List<GridColumn> columns = new List<GridColumn>();
            foreach (GridColumn col in listAvailableColumns.SelectedItems)
            {
                columns.Add(col);
            }

            foreach (object column in columns)
            {
                listAvailableColumns.Items.Remove(column);
                listUsedColumns.Items.Add(column);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelClicked != null)
            {
                CancelClicked(this);
            }

            ReLoadData();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            foreach (GridColumn column in view.Columns)
            {
                column.VisibleIndex = -1;
            }

            int splitterIndex = listUsedColumns.Items.IndexOf(this.frozenColumnsDelimiter);
            int visibleIndex = 0;

            foreach (object o in listUsedColumns.Items)
            {
                GridColumn column = o as GridColumn;
                if (column != null)
                {
                    column.Fixed = listUsedColumns.Items.IndexOf(column) < splitterIndex ? FixedStyle.Left : FixedStyle.None;
                    column.VisibleIndex = visibleIndex;
                    visibleIndex++;
                }
            }

            if (SubmitClicked != null)
            {
                SubmitClicked(this);
            }
        }

        private void btnRemoveColumns_Click(object sender, EventArgs e)
        {
            if (listUsedColumns.SelectedItems.Count == 0)
            {
                return;
            }
            List<object> columns = new List<object>();
            foreach (object o in listUsedColumns.SelectedItems)
            {
                columns.Add(o);
            }

            foreach (object o in columns)
            {
                if (IsObjectCanBeRemoved(o))
                {
                    listUsedColumns.Items.Remove(o);
                    listAvailableColumns.Items.Add(o);
                }
            }
        }

        private void listToDown_Click(object sender, EventArgs e)
        {
            MoveSelectedItem(listUsedColumns, false, listUsedColumns.SelectedIndices.OrderByDescending(x => x), i => i + 1);
        }

        private void listToUp_Click(object sender, EventArgs e)
        {
            MoveSelectedItem(listUsedColumns, true, listUsedColumns.SelectedIndices.OrderBy(x => x), i => i - 1);
        }

        private void listUsedColumns_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            int splitterIndex = listUsedColumns.Items.IndexOf(this.frozenColumnsDelimiter);
            if (listUsedColumns.Items.IndexOf(e.Item) < splitterIndex)
            {
                e.Appearance.ForeColor = Color.Gray;
            }
        }

        private void listUsedColumns_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisEnaButtons();
        }

        #endregion

        #region Event Declarations

        [Category("Events")]
        [Description("Fires event when cancel button clicked")]
        public event ButtonClickEventHandler CancelClicked;

        [Category("Events")]
        [Description("Fires event when submit button clicked")]
        public event ButtonClickEventHandler SubmitClicked;

        #endregion
    }
}