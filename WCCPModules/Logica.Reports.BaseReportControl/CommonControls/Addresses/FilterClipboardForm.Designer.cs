﻿namespace Logica.Reports.BaseReportControl.CommonControls.AddressesFilters
{
    partial class FilterClipboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupControlComment = new DevExpress.XtraEditors.GroupControl();
            this.memoComment = new DevExpress.XtraEditors.MemoEdit();
            this.groupClipboardContent = new DevExpress.XtraEditors.GroupControl();
            this.memoClipboard = new DevExpress.XtraEditors.MemoEdit();
            this.olList = new Logica.Reports.BaseReportControl.CommonControls.OlList();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlComment)).BeginInit();
            this.groupControlComment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupClipboardContent)).BeginInit();
            this.groupClipboardContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoClipboard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(840, 458);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            // 
            // groupControlComment
            // 
            this.groupControlComment.Controls.Add(this.memoComment);
            this.groupControlComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlComment.Location = new System.Drawing.Point(0, 0);
            this.groupControlComment.Name = "groupControlComment";
            this.groupControlComment.Size = new System.Drawing.Size(908, 144);
            this.groupControlComment.TabIndex = 6;
            this.groupControlComment.Text = "Комментарий для вставляемых ТТ";
            // 
            // memoComment
            // 
            this.memoComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoComment.Location = new System.Drawing.Point(2, 22);
            this.memoComment.Name = "memoComment";
            this.memoComment.Size = new System.Drawing.Size(904, 120);
            this.memoComment.TabIndex = 0;
            // 
            // groupClipboardContent
            // 
            this.groupClipboardContent.Controls.Add(this.memoClipboard);
            this.groupClipboardContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupClipboardContent.Location = new System.Drawing.Point(0, 0);
            this.groupClipboardContent.Name = "groupClipboardContent";
            this.groupClipboardContent.Size = new System.Drawing.Size(908, 111);
            this.groupClipboardContent.TabIndex = 5;
            this.groupClipboardContent.Text = "В буфере обнаружены данные, которые не распознаются как коды ТТ";
            // 
            // memoClipboard
            // 
            this.memoClipboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoClipboard.Location = new System.Drawing.Point(2, 22);
            this.memoClipboard.Name = "memoClipboard";
            this.memoClipboard.Properties.ReadOnly = true;
            this.memoClipboard.Size = new System.Drawing.Size(904, 87);
            this.memoClipboard.TabIndex = 0;
            // 
            // olList
            // 
            this.olList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olList.Location = new System.Drawing.Point(0, 0);
            this.olList.Name = "olList";
            this.olList.Size = new System.Drawing.Size(908, 177);
            this.olList.TabIndex = 2;
            this.olList.POCListChanged += new System.EventHandler(this.olList_POCListChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(575, 458);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(259, 22);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Вставить выбранные ТТ в адреску";
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.olList);
            this.splitContainer1.Size = new System.Drawing.Size(908, 440);
            this.splitContainer1.SplitterDistance = 259;
            this.splitContainer1.TabIndex = 7;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupClipboardContent);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupControlComment);
            this.splitContainer2.Size = new System.Drawing.Size(908, 259);
            this.splitContainer2.SplitterDistance = 111;
            this.splitContainer2.TabIndex = 0;
            // 
            // FilterClipboardForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(934, 492);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.MinimizeBox = false;
            this.Name = "FilterClipboardForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вставка ТТ из буфера обмена";
            this.Load += new System.EventHandler(this.FilterClipboardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlComment)).EndInit();
            this.groupControlComment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupClipboardContent)).EndInit();
            this.groupClipboardContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoClipboard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.GroupControl groupClipboardContent;
        private DevExpress.XtraEditors.MemoEdit memoClipboard;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraEditors.GroupControl groupControlComment;
        private DevExpress.XtraEditors.MemoEdit memoComment;
        private OlList olList;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
    }
}