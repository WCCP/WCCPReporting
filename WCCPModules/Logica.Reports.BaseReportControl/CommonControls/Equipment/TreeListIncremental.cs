﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.CommonFunctionality;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// Describes treelist class enhanced with recursive nodes checking and filtering
    /// </summary>
    public partial class TreeListIncremental : TreeList
    {
        private const string STRING_DELIMITER = ",";

        private CheckEdit _checkEdit = null;

        /// <summary>
        /// Creates the new instance of <c>TreeListIncremental</c> class
        /// </summary>
        public TreeListIncremental()
        {
            InitializeComponent();

            OptionsView.ShowColumns = false;
            OptionsView.ShowCheckBoxes = true;
            OptionsView.ShowHorzLines = false;
            OptionsView.ShowVertLines = false;
            OptionsView.ShowIndicator = false;

            OptionsBehavior.AllowIndeterminateCheckState = true;

            BeforeCheckNode += tree_BeforeCheckNode;
            AfterCheckNode += tree_AfterCheckNode;
        }

        public event StateChanged AfterEditorStateChanged;

        public event NodeChecked AfterNodeChecked;

        #region General tree functionality

        public void AssignCheckEdit(CheckEdit edit)
        {
            _checkEdit = edit;

            _checkEdit.CheckStateChanged += check_CheckedChanged;

        }

        /// <summary>
        /// Gets the collection of checked nodes
        /// </summary>
        /// <returns>Checked nodes collection</returns>
        public List<TreeListNode> GetCheckedNodes()
        {
            CalcCheckedNodesOperation lOperartion = new CalcCheckedNodesOperation();

            NodesIterator.DoOperation(lOperartion);

            return lOperartion.CheckedNodes;
        }

        /// <summary>
        /// Gets the list of Ids of checked nodes
        /// </summary>
        /// <returns>The list of Ids</returns>
        public List<string> GetCheckedNodesIds()
        {
            CalcCheckedNodesIdsOperation lOperation = new CalcCheckedNodesIdsOperation();

            NodesIterator.DoOperation(lOperation);

            return lOperation.CheckedNodes;
        }

        /// <summary>
        /// Gets string representation of the list of checked nodes Ids
        /// </summary>
        /// <returns>The list of Ids</returns>
        public string GetCheckedIdsValues()
        {
            return string.Join(STRING_DELIMITER, GetCheckedNodesIds().ToArray());
        }

        /// <summary>
        /// Gets node with given KeyFieldId
        /// </summary>
        /// <param name="id">KeyFieldId Id</param>
        /// <returns>The node with given Id if exists, else - <c>null</c></returns>
        public TreeListNode GetNodeByKeyField(string id)
        {
            TreeListNode lResultNode = null;

            foreach (TreeListNode node in Nodes)
            {
                lResultNode = GetNode(node, id) ?? lResultNode;
            }

            return lResultNode;
        }

        /// <summary>
        /// Performs datasource loading
        /// </summary>
        /// <param name="source">Datasource to be loaded</param>
        public void LoadDataSource(DataTable source)
        {
            DataSource = source;

            ExpandAll();
            CollapseAll();
        }
        
        /// <summary>
        /// Gets node with given KeyFieldId via recursive search
        /// </summary>
        /// <param name="node">Start search node</param>
        /// <param name="id">KeyFieldId Id</param>
        /// <returns>The node with given Id if exists, else - <c>null</c></returns>
        private TreeListNode GetNode(TreeListNode node, string id)
        {
            TreeListNode lResultNode = null;

            if (node[KeyFieldName].ToString() == id)
            {
                lResultNode = node;
            }
            else
            {
                foreach (TreeListNode n in node.Nodes)
                {
                    lResultNode = GetNode(n, id) ?? lResultNode;
                }
            }

            return lResultNode;
        }

        #endregion

        #region Selected values lists

        /// <summary>
        /// Gets string representation of the list of checked nodes column values
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public List<string> GetCheckedColumnValues(string colName, string colLevelName, int? level = null)
        {
            CalcCheckedNodesColumnValue lOperation = new CalcCheckedNodesColumnValue(colName, level, colLevelName);

            NodesIterator.DoOperation(lOperation);

            return lOperation.CheckedNodes;
        }

        /// <summary>
        /// Gets string representation of the list of checked nodes column values regardless of children existance
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public List<string> GetCheckedColumnValuesChildren(string colName, string colLevelName, int level, CheckState state = CheckState.Checked)
        {
            CalcCheckedNodesColumnValueChildren lOperation = new CalcCheckedNodesColumnValueChildren(colName, level, colLevelName, state);

            NodesIterator.DoOperation(lOperation);

            return lOperation.CheckedNodes;
        }

        /// <summary>
        /// Gets string representation of the list of checked nodes column values regardless of children existance
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public string GetCheckedColumnValuesChildrenList(string colName, string colLevelName, int level, CheckState state = CheckState.Checked)
        {
            return string.Join(STRING_DELIMITER, GetCheckedColumnValuesChildren(colName, colLevelName, level, state).ToArray());
        }

        /// <summary>
        /// Gets string representation of the list of nodes column values without children
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public List<string> GetNodesWithoutChildren(string colName, string colLevelName, int level)
        {
            CalcNodesWithoutChildren lOperation = new CalcNodesWithoutChildren(colName, level, colLevelName);

            NodesIterator.DoOperation(lOperation);

            return lOperation.CheckedNodes;
        }

        /// <summary>
        /// Gets string representation of the list list of nodes column values without children
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public string GetNodesWithoutChildrenList(string colName, string colLevelName, int level)
        {
            return string.Join(STRING_DELIMITER, GetNodesWithoutChildren(colName, colLevelName, level).ToArray());
        }

        /// <summary>
        /// Gets the list of checked nodes column values
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <returns>The list of values</returns>
        public List<string> GetCheckedColumnValuesAll(string colName, CheckState state = CheckState.Checked)
        {
            CalcCheckedNodesColumnValueAll lOperation = new CalcCheckedNodesColumnValueAll(colName, state);

            NodesIterator.DoOperation(lOperation);

            return lOperation.CheckedNodes;
        }

        /// <summary>
        /// Gets string representation of the list of checked nodes column values
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <returns>The list of values</returns>
        public string GetCheckedColumnValuesAllList(string colName, CheckState state = CheckState.Checked)
        {
            return string.Join(STRING_DELIMITER, GetCheckedColumnValuesAll(colName, state).ToArray());
        }
        
        /// <summary>
        /// Gets string representation of the list of checked nodes column values regardless of children existance
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public List<string> GetInvisibleColumnValuesChildren(string colName, string colLevelName, int level, CheckState state = CheckState.Checked)
        {
            CalcCheckedInvisibleNodes lOperation = new CalcCheckedInvisibleNodes(colName, level, colLevelName, state);

            NodesIterator.DoOperation(lOperation);

            return lOperation.CheckedNodes;
        }

        /// <summary>
        /// Gets string representation of the list of checked nodes column values regardless of children existance
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public string GetInvisibleColumnValuesChildrenList(string colName, string colLevelName, int level, CheckState state = CheckState.Checked)
        {
            return string.Join(STRING_DELIMITER, GetInvisibleColumnValuesChildren(colName, colLevelName, level, state).ToArray());
        }

        /// <summary>
        /// Gets string representation of the list of checked and indeterminate nodes column values
        /// </summary>
        /// <param name="colName">The column to be described</param>
        /// <param name="colLevelName">The column that represents level</param>
        /// <param name="level">The level desired</param>
        /// <returns>The list of values</returns>
        public List<string> GetCheckedAndIndeterminateColumnValues(string colName, string colLevelName, int? level = null)
        {
            CalcCheckedAndIndeterminateNodesColumnValue lOperation = new CalcCheckedAndIndeterminateNodesColumnValue(colName, level, colLevelName);

            NodesIterator.DoOperation(lOperation);

            return lOperation.CheckedNodes;
        }

        #endregion

        #region Filtering & other functionality

        /// <summary>
        /// Performs filtering of the tree with given top level nodes Ids 
        /// </summary>
        /// <param name="filter">Top level nodes Ids that should be visible</param>
        public void FilterTree(List<string> filter)
        {
            if (filter.Count == 0)
            {
                foreach (TreeListNode node in Nodes)
                {
                    SetChildrensVisibility(node, true);
                }
            }
            else
            {
                foreach (TreeListNode node in Nodes)
                {
                    SetChildrensVisibility(node, false);
                }

                foreach (string nodeId in filter)
                {
                    TreeListNode lNode = GetNodeByKeyField(nodeId);

                    if (lNode != null)
                    {
                        SetChildrensVisibility(lNode, true);
                    }
                }
            }

            if(_checkEdit != null)
            {
                if (_checkEdit.CheckState != CheckState.Indeterminate)
                {
                    CheckNodes(_checkEdit.CheckState);
                }
            }
        }

        /// <summary>
        /// Hides tree level
        /// </summary>
        public void HideLevel(string colLevelName, int level)
        {
            CalcHideLevel lOperation = new CalcHideLevel(colLevelName, level);

            NodesIterator.DoOperation(lOperation);
        }

        /// <summary>
        /// Checkes/uncheckes nodes
        /// </summary>
        public void CheckNodes(CheckState state)
        {
            BeforeCheckNode -= tree_BeforeCheckNode;
            AfterCheckNode -= tree_AfterCheckNode;

            CalcCheckNode lOperation = new CalcCheckNode(state);

            NodesIterator.DoOperation(lOperation);

            BeforeCheckNode += tree_BeforeCheckNode;
            AfterCheckNode += tree_AfterCheckNode;
        }

        /// <summary>
        /// Checkes/uncheckes nodes
        /// </summary>
        public void CheckNodes(List<string> values, CheckState state)
        {
            BeforeCheckNode -= tree_BeforeCheckNode;
            AfterCheckNode -= tree_AfterCheckNode;

            CalcCheckNodeFromList lOperation = new CalcCheckNodeFromList(values, state);

            NodesIterator.DoOperation(lOperation);

            BeforeCheckNode += tree_BeforeCheckNode;
            AfterCheckNode += tree_AfterCheckNode;
        }

        /// <summary>
        /// Performs recursive change of visibility of child nodes
        /// </summary>
        /// <param name="node">The node to be updated</param>
        /// <param name="isVisible">When <c>true</c> the node is being made visible, otherwise - invisible</param>
        public void SetChildrensVisibility(TreeListNode node, bool isVisible)
        {
            BeginUpdate();

            node.Visible = isVisible;

            foreach (TreeListNode child in node.Nodes)
            {
                SetChildrensVisibility(child, isVisible);
            }

            EndUpdate();
        }
        
        /// <summary>
        /// Handles the BeforeCheckNode event of current tree
        /// </summary>
        /// <param name="sender">current tree</param>
        /// <param name="e">EventArgs</param>
        private void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            if (e.PrevState == CheckState.Unchecked)
            {
                e.State = CheckState.Checked;
            }

            if (e.PrevState == CheckState.Checked)
            {
                e.State = CheckState.Unchecked;
            }
        }

        /// <summary>
        /// Handles the AfterCheckNode event of current tree
        /// </summary>
        /// <param name="sender">current tree</param>
        /// <param name="e">EventArgs</param>
        private void tree_AfterCheckNode(object sender, NodeEventArgs e)
        {
            if (e.Node.HasChildren)
            {
                BeginUpdate();
                CheckChildren(e.Node, e.Node.CheckState);
                EndUpdate();
            }

            if (e.Node.ParentNode != null)
            {
                CheckParents(e.Node);
            }

            if (e.Node.ParentNode == null && _checkEdit != null)
            {
                _checkEdit.CheckStateChanged -= check_CheckedChanged;
                _checkEdit.CheckState = DefineEditState();
                _checkEdit.CheckStateChanged += check_CheckedChanged;
            }

            if(AfterNodeChecked != null)
            {
                AfterNodeChecked(this, new EventArgs());
            }
        }

        /// <summary>
        /// Performs recursive checking of child nodes
        /// </summary>
        /// <param name="node">The node to be checked</param>
        /// <param name="state">The check state to be assigned</param>
        private void CheckChildren(TreeListNode node, CheckState state)
        {
            node.CheckState = state;

            foreach (TreeListNode child in node.Nodes)
            {
                CheckChildren(child, state);
            }
        }

        /// <summary>
        /// Performs bottom-up checking of parent nodes
        /// </summary>
        /// <param name="node">The node to be checked</param>
        private void CheckParents(TreeListNode node)
        {
            TreeListNode lParentNode = node.ParentNode;

            if (lParentNode == null && _checkEdit == null)
                return;

            if (lParentNode == null && _checkEdit != null)
            {
                _checkEdit.CheckStateChanged -= check_CheckedChanged;
                _checkEdit.CheckState = DefineEditState();
                _checkEdit.CheckStateChanged += check_CheckedChanged;

                return;
            }

            if (lParentNode.CheckState == node.CheckState)
                return;

            lParentNode.CheckState = DefineNodeState(lParentNode);

            CheckParents(lParentNode);
        }
        
        /// <summary>
        /// Evaluate new check state of the node using states of its child nodes - non-recursive
        /// </summary>
        /// <param name="node">The node to be changes</param>
        /// <returns><c>CheckState.Checked</c> if all children are checked, <c>CheckState.Unchecked</c> - if all chidlren are unchecked, <c>CheckState.Indeterminate</c> - otherwise</returns>
        private CheckState DefineNodeState(TreeListNode node)
        {
            CheckState lCheckState = CheckState.Indeterminate;

            int checkedCount = 0;
            int uncheckedCount = 0;
            int visibleCount = 0;
            
            foreach (TreeListNode child in node.Nodes)
            {
                if(child.Visible)
                {
                    visibleCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Checked)
                {
                    checkedCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Unchecked)
                {
                    uncheckedCount++;
                }
            }

            if (checkedCount == 0 && uncheckedCount == visibleCount)
            {
                lCheckState = CheckState.Unchecked;
            }
            else
            {
                if (uncheckedCount == 0 && checkedCount == visibleCount)
                {
                    lCheckState = CheckState.Checked;
                }
            }

            return lCheckState;

        }

        /// <summary>
        /// Evaluate new check state of the node using states of its child nodes - non-recursive
        /// </summary>
        /// <returns><c>CheckState.Checked</c> if all children are checked, <c>CheckState.Unchecked</c> - if all chidlren are unchecked, <c>CheckState.Indeterminate</c> - otherwise</returns>
        private CheckState DefineEditState()
        {
            CheckState lCheckState = CheckState.Indeterminate;

            int checkedCount = 0;
            int uncheckedCount = 0;
            int visibleCount = 0;
            
            foreach (TreeListNode child in Nodes)
            {
                if (child.Visible)
                {
                    visibleCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Checked)
                {
                    checkedCount++;
                }

                if (child.Visible && child.CheckState == CheckState.Unchecked)
                {
                    uncheckedCount++;
                }
            }

            if (checkedCount == 0 && uncheckedCount == visibleCount)
            {
                lCheckState = CheckState.Unchecked;
            }
            else
            {
                if (uncheckedCount == 0 && checkedCount == visibleCount)
                {
                    lCheckState = CheckState.Checked;
                }
            }

            return lCheckState;
        }

        /// <summary>
        /// Performs tree nodes cheking according to associated check edit state
        /// </summary>
        private void check_CheckedChanged(object sender, EventArgs e)
        {
            if (_checkEdit != null)
            {
                if(_checkEdit.CheckState == CheckState.Indeterminate)
                {
                    _checkEdit.CheckStateChanged -= check_CheckedChanged;
                    _checkEdit.CheckState = CheckState.Unchecked;
                    _checkEdit.CheckStateChanged += check_CheckedChanged;
                }

                CheckNodes(_checkEdit.CheckState);

                if(AfterEditorStateChanged != null)
                {
                    AfterEditorStateChanged(this, new EventArgs());
                }
            }
        }

        #endregion

        /// <summary>
        /// Describes template for editor state changes
        /// </summary>
        /// <param name="sender"><c>TimeInterval</c></param>
        /// <param name="args">Event args</param>
        public delegate void StateChanged(object sender, EventArgs args);

        /// <summary>
        /// Describes template for after check node handling
        /// </summary>
        /// <param name="sender"><c>TimeInterval</c></param>
        /// <param name="args">Event args</param>
        public delegate void NodeChecked(object sender, EventArgs args);

        public void On_AfterCheckNode(TreeListNode node)
        {
            this.tree_AfterCheckNode(this, new NodeEventArgs(node));
        }
    }
}
