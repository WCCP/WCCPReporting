﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonFunctionality;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// Describes grid look up with ability to check items
    /// </summary>
    public partial class GridLookUpWithMultipleChecking : GridLookUpEdit
    {
        /// <summary>
        /// Selection handler
        /// </summary>
        private GridCheckMarksSelection _gridCheckMarksSA;

        /// <summary>
        /// List delimiter
        /// </summary>
        private string _delimiter = ", ";

        /// <summary>
        /// Unfiltered dataSource
        /// </summary>
        private DataTable _source;

        /// <summary>
        /// Creates the new instance of <c>GridLookUpWithMultipleChecking</c> class
        /// </summary>
        public GridLookUpWithMultipleChecking()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the display list delimiter
        /// </summary>
        public string Delimiter
        {
            get { return _delimiter; }
            set { _delimiter = value; }
        }

        /// <summary>
        /// Performs DataSource loading
        /// </summary>
        /// <param name="dataSource">SataSource to be loaded</param>
        public void LoadDataSource(DataTable dataSource)
        {
            _source = dataSource;

            Properties.DataSource = dataSource;

            Properties.View.OptionsView.ShowAutoFilterRow = true;
            Properties.View.OptionsSelection.MultiSelect = true;
            Properties.View.OptionsView.ShowGroupPanel = false;

            Properties.Tag = _gridCheckMarksSA;

            CustomDisplayText += gridLookUpEdit_CustomDisplayText;

            _gridCheckMarksSA = new GridCheckMarksSelection(Properties);
            _gridCheckMarksSA.SelectionChanged += gridCheckMarks_SelectionChanged;
            // _gridCheckMarksSA.SelectAll(dataSource.DefaultView);
        }

        /// <summary>
        /// Gets the list of checked items
        /// </summary>
        /// <returns>The list of checked items</returns>
        public List<string> SelectedIds()
        {
            List<string> lCheckedList = (from DataRowView row in _gridCheckMarksSA.Selection select row[Properties.ValueMember].ToString()).ToList();

            return lCheckedList;
        }

        /// <summary>
        /// Gets the list of checked items
        /// </summary>
        /// <returns>The list of checked items</returns>
        public string SelectedIdsString()
        {
            return string.Join(Delimiter, SelectedIds().ToArray());
        }

        /// <summary>
        /// Gets the list of checked items
        /// </summary>
        /// <returns>The list of checked items</returns>
        public List<string> SelectedValues(string fieldName)
        {
            List<string> lCheckedList = _gridCheckMarksSA.SelectedCount > 0
                                            ? (from DataRowView row in _gridCheckMarksSA.Selection
                                               select row[fieldName].ToString()).ToList()
                                            : new List<string>();

            return lCheckedList;
        }

        /// <summary>
        /// Gets the list of checked items
        /// </summary>
        /// <returns>The list of checked items</returns>
        public string SelectedValuesString(string fieldName)
        {
            return string.Join(Delimiter, SelectedValues(fieldName).ToArray());
        }

        /// <summary>
        /// Gets the list of items
        /// </summary>
        /// <returns>The list of items</returns>
        public List<string> AllIds()
        {
            List<string> lCheckedList = (from DataRow row in (Properties.DataSource as DataTable).Rows select row[Properties.ValueMember].ToString()).ToList();

            return lCheckedList;
        }

        /// <summary>
        /// Gets the list of items
        /// </summary>
        /// <returns>The list of items</returns>
        public string AllIdsString()
        {
            return string.Join(Delimiter, AllIds().ToArray());
        }

        /// <summary>
        /// Gets the list of items
        /// </summary>
        /// <returns>The list of items</returns>
        public List<string> AllValues(string fieldName)
        {
            List<string> lCheckedList = (from DataRow row in (Properties.DataSource as DataTable).Rows select row[fieldName].ToString()).ToList();

            return lCheckedList;
        }

        /// <summary>
        /// Gets the list of items
        /// </summary>
        /// <returns>The list of items</returns>
        public string AllValuesString(string fieldName)
        {
            return string.Join(Delimiter, AllValues(fieldName).ToArray());
        }

        /// <summary>
        /// Performs combo box list filtering via given filter
        /// </summary>
        /// <param name="filter">The filter</param>
        public void FilterList(string filter)
        {
            _gridCheckMarksSA.ClearSelection(Properties.View);

            DataView lDataView = _source.DefaultView;
            lDataView.RowFilter = filter;
            lDataView.Sort = Properties.DisplayMember;
            Properties.DataSource = lDataView.ToTable();
        }

        /// <summary>
        /// Checks the values by list
        /// </summary>
        public void SetComboBoxCheckedValues(List<string> values)
        {
            for(int i = 0; i<Properties.View.RowCount; i++)
            {
                if(values.Contains(Properties.View.GetRowCellValue(i, Properties.ValueMember).ToString()))
                {

                    _gridCheckMarksSA.SelectRow(Properties.View, i, true);
                }
            }
        }

        /// <summary>
        /// Select all
        /// </summary>
        public void SelectAll()
        {
            _gridCheckMarksSA.SelectAll((Properties.DataSource as DataTable).DefaultView);
        }

        public void ClearSelection()
        {
            _gridCheckMarksSA.ClearSelection(Properties.View);
        }

        /// <summary>
        /// Handles CustomDisplayText event of gridLookUpEdit
        /// </summary>
        /// <param name="sender">gridLookUpEdit</param>
        /// <param name="e">CustomDisplayTextEventArgs</param>
        private void gridLookUpEdit_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            StringBuilder lText = new StringBuilder();

            foreach (DataRowView row in _gridCheckMarksSA.Selection)
            {
                if (lText.ToString().Length > 0)
                {
                    lText.Append(Delimiter);
                }

                lText.Append(row[Properties.DisplayMember].ToString());
            }

            e.DisplayText = lText.ToString();
        }

        /// <summary>
        /// Handles SelectionChanged event
        /// </summary>
        /// <param name="sender">gridCheckMarks</param>
        /// <param name="e">EventArgs</param>
        private void gridCheckMarks_SelectionChanged(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DataRowView rv in _gridCheckMarksSA.Selection)
            {
                if (sb.ToString().Length > 0) { sb.Append(_delimiter); }
                sb.Append(rv[Properties.DisplayMember].ToString());
            }

            Text = sb.ToString();
            Refresh();
        }
    }
}
