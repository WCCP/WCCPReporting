﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// Describes extended combo bos edit
    /// </summary>
    public partial class ComboboxExtended : CheckedComboBoxEdit
    {
        /// <summary>
        /// List delimiter
        /// </summary>
        private string _delimiter = ",";

        /// <summary>
        /// Unfiltered dataSource
        /// </summary>
        private DataTable _source;

        /// <summary>
        /// Creates the new instance of <c>ComboboxExtended</c> class
        /// </summary>
        public ComboboxExtended()
        {
            InitializeComponent();

            QueryPopUp += new CancelEventHandler(checkedComboBoxEdit1_QueryPopUp);
        }

        /// <summary>
        /// Creates the new instance of <c>ComboboxExtended</c> class
        /// </summary>
        /// <param name="container">Element container</param>
        public ComboboxExtended(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

            QueryPopUp += new CancelEventHandler(checkedComboBoxEdit1_QueryPopUp);
        }

        /// <summary>
        /// Gets or sets list delimiter
        /// </summary>
        public string Delimiter
        {
            get { return _delimiter; }
            set { _delimiter = value; }
        }

        /// <summary>
        /// Gets the list of checked value members
        /// </summary>
        /// <returns>The list of checked value members</returns>
        public List<string> GetComboBoxCheckedValues()
        {
            return (from CheckedListBoxItem item in Properties.Items where item.CheckState == CheckState.Checked select item.Value.ToString()).ToList();
        }

        /// <summary>
        /// Checks the values by list
        /// </summary>
        public void SetComboBoxCheckedValues(List<string> values)
        {
            for (int i = 0; i < Properties.Items.Count; i++)
            {
                if (values.Contains(Properties.Items[i].Value.ToString()))
                {
                    Properties.Items[i].CheckState = CheckState.Checked;
                }
            }
        }

        /// <summary>
        /// Select all
        /// </summary>
        public void SelectAll()
        {
            for (int i = 0; i < Properties.Items.Count; i++)
            {
                Properties.Items[i].CheckState = CheckState.Checked;
            }
        }

        /// <summary>
        /// Gets the string representation of the list of checked value members
        /// </summary>
        /// <returns>The string representation of the list of checked value members</returns>
        public string GetComboBoxCheckedValuesList()
        {
            List<string> lList = GetComboBoxCheckedValues();

            return string.Join(Delimiter, lList.ToArray());
        }

        /// <summary>
        /// Gets the string representation of the list of checked column members
        /// </summary>
        /// <returns>The string representation of the list of checked column members</returns>
        public string GetComboBoxCheckedColumnsList()
        {
            List<string> lList = GetComboBoxCheckedValues();

            return string.Join(Delimiter, lList.ToArray());
        }

        /// <summary>
        /// Gets the string representation of the list of all value members
        /// </summary>
        /// <returns>The string representation of the list of all value members</returns>
        public string GetComboBoxValuesList()
        {
            List<string> lList = (from CheckedListBoxItem item in Properties.Items select item.Value.ToString()).ToList();

            return string.Join(Delimiter, lList.ToArray());
        }

        /// <summary>
        /// Performs data source loading
        /// </summary>
        /// <param name="source">The sourct to be loaded</param>
        public void LoadDataSource(DataTable source)
        {
            _source = source;

            BeginUpdate();
            foreach (DataRow row in source.Rows)
            {
                Properties.Items.Add(row[Properties.ValueMember], row[Properties.DisplayMember].ToString());
            }
            EndUpdate();
        }

        /// <summary>
        /// Performs combo box list filtering via given filter
        /// </summary>
        /// <param name="filter">The filter</param>
        public void FilterList(string filter)
        {
            DataView lDataView = _source.DefaultView;
            lDataView.RowFilter = filter;
            lDataView.Sort = Properties.DisplayMember;

            BeginUpdate();
            Properties.Items.Clear();

            foreach (DataRow row in lDataView.ToTable(true, Properties.ValueMember, Properties.DisplayMember).Rows)
            {
                Properties.Items.Add(row[Properties.ValueMember], row[Properties.DisplayMember].ToString());
            }
            EndUpdate();
        }

        void checkedComboBoxEdit1_QueryPopUp(object sender, CancelEventArgs e)
        {
            if (Properties.PopupControl != null)
            {
                Properties.PopupControl.Height = 300;
            }
        }
    }
}
