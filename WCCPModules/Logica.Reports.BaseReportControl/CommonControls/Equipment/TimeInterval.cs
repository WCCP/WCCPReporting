﻿using System;
using System.Windows.Forms;
using DevExpress.Utils;
using Logica.Reports.BaseReportControl.CommonFunctionality;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// Handles working with time intervals
    /// </summary>
    public partial class TimeInterval : UserControl
    {
        private DateTime MinValue = new DateTime(1990, 1, 1);
        private DateTime MaxValue = DateTime.Now;

        /// <summary>
        /// Creates the new instance od <c>TimeInterval</c> class
        /// </summary>
        public TimeInterval()
        {
            InitializeComponent();

            dTo.Properties.NullDate = DateTime.MinValue;
            dTo.Properties.NullText = String.Empty;

            dFrom.Properties.NullDate = DateTime.MinValue;
            dFrom.Properties.NullText = String.Empty;

            dTo.EditValue = null;
            dFrom.EditValue = null;

            dTo.Properties.MinValue = MinValue;
            dFrom.Properties.MinValue = MinValue;

            dTo.Properties.MaxValue = MaxValue;
            dFrom.Properties.MaxValue = MaxValue;
        }

        /// <summary>
        /// Assigns restrictions
        /// </summary>
        public void ActivateRestrictions()
        {
            dTo.EditValueChanged += dateEdit_EditValueChanged;
            dFrom.EditValueChanged += dateEdit_EditValueChanged;
        }

        /// <summary>
        /// Assigns restrictions to To date
        /// </summary>
        public void ActivateRestrictionsTo()
        {
            dTo.Properties.MinValue = !string.IsNullOrEmpty(dFrom.Text) ? dFrom.DateTime : DateTime.MinValue;
        }

        /// <summary>
        /// Assigns restrictions to From date
        /// </summary>
        public void ActivateRestrictionsFrom()
        {
            dFrom.Properties.MaxValue = !string.IsNullOrEmpty(dTo.Text) ? dTo.DateTime : DateTime.MaxValue;
        }

        /// <summary>
        /// Fires on end date change
        /// </summary>
        public event PeriodChanged EndDateChanged;

        /// <summary>
        /// Gets or sets interval description
        /// </summary>
        public string Description
        {
            get { return lDescription.Text; }
            set { lDescription.Text = value; }
        }

        /// <summary>
        /// Gets or sets whether null input is allowed
        /// </summary>
        public DefaultBoolean AllowNulls
        {
            get { return dFrom.Properties.AllowNullInput; }
            set
            {
                dFrom.Properties.AllowNullInput = value;
                dTo.Properties.AllowNullInput = value;
            }
        }

        /// <summary>
        /// Gets or sets interval start date
        /// </summary>
        public DateTime From
        {
            get { return dFrom.DateTime; }
            set { dFrom.DateTime = value; }
        }

        /// <summary>
        /// Gets or sets interval end date
        /// </summary>
        public DateTime To
        {
            get { return dTo.DateTime; }
            set { dTo.DateTime = value; }
        }

        /// <summary>
        /// Gets resulting interval
        /// </summary>
        public DateInterval Interval
        {
            get
            {
                return new DateInterval
                           {
                               From = dFrom.DateTime,
                               To = dTo.DateTime
                           };
            }
        }

        /// <summary>
        /// Gets or sets upper date bound
        /// </summary>
        public void SetMaxValue(DateTime maxValue)
        {
            MaxValue = maxValue;

            dTo.Properties.MaxValue = maxValue;
            dFrom.Properties.MaxValue = maxValue;
        }

        /// <summary>
        /// Gets or sets lower date bound
        /// </summary>
        public void SetMinValue(DateTime minValue)
        {
            MaxValue = minValue;

            dTo.Properties.MaxValue = minValue;
            dFrom.Properties.MaxValue = minValue;
        }

        /// <summary>
        /// Handles EditValueChanged event of dateEdit
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(dFrom))
            {
                dTo.Properties.MinValue = !string.IsNullOrEmpty(dFrom.Text) ? dFrom.DateTime : MinValue;
            }

            if (sender.Equals(dTo))
            {
                dFrom.Properties.MaxValue = !string.IsNullOrEmpty(dTo.Text) ? dTo.DateTime : MaxValue;

                if (EndDateChanged != null)
                {
                    EndDateChanged(dTo, new PeriodChangedArgs());
                }
            }
        }
    }

    /// <summary>
    /// Describes template for start date change handling
    /// </summary>
    /// <param name="sender"><c>TimeInterval</c></param>
    /// <param name="args">Event args</param>
    public delegate void PeriodChanged(object sender, PeriodChangedArgs args);

    /// <summary>
    /// Describes event args for start date change handling
    /// </summary>
    public class PeriodChangedArgs : EventArgs
    { }
}
