﻿namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class SizeFixator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlConent = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlConent
            // 
            this.pnlConent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlConent.Location = new System.Drawing.Point(0, 0);
            this.pnlConent.Margin = new System.Windows.Forms.Padding(0);
            this.pnlConent.Name = "pnlConent";
            this.pnlConent.Size = new System.Drawing.Size(213, 80);
            this.pnlConent.TabIndex = 0;
            this.pnlConent.Resize += new System.EventHandler(this.pnlConent_Resize);
            // 
            // SizeFixator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlConent);
            this.Name = "SizeFixator";
            this.Size = new System.Drawing.Size(213, 80);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlConent;
    }
}
