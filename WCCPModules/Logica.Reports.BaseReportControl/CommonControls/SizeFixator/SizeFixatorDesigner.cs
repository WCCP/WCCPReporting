﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    public class SizeFixatorDesigner : ParentControlDesigner
    {
        public override void Initialize(System.ComponentModel.IComponent component)
        {
            base.Initialize(component);

            if (this.Control is SizeFixator)
            {
                this.EnableDesignMode((
                   (SizeFixator)this.Control).WorkingArea, "WorkingArea");
            }
        }
    }
}
