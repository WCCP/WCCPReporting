﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    [Designer(typeof(Logica.Reports.BaseReportControl.CommonControls.SizeFixatorDesigner))]
    public partial class SizeFixator : UserControl
    {
        private Control ctrlToFix = null;

        public SizeFixator()
        {
            InitializeComponent();
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        /// <summary>
        /// This property is essential to allowing the designer to work and
        /// the DesignerSerializationVisibility Attribute (above) is essential
        /// in allowing serialization to take place at design time.
        /// </summary>
        public Panel WorkingArea
        {
            get
            {
                return this.pnlConent;
            }
        }

        private Control ControlToFix
        {
            get
            {
                if (ctrlToFix == null)
                {
                    if (pnlConent.Controls.Count > 0)
                    {
                        ctrlToFix = pnlConent.Controls[0];
                    }
                }

                return ctrlToFix;
            }
        }

        private void pnlConent_Resize(object sender, EventArgs e)
        {
            int offset = 2;
            if (this.ControlToFix == null)
            {
                return;
            }

            this.ControlToFix.Dock = DockStyle.None;

            if (this.ControlToFix.Location.X != 0 || this.ControlToFix.Location.Y != 0)
            {
                this.ControlToFix.Location = new Point(0,0);
            }
            if (this.ControlToFix.Height != pnlConent.Height - offset)
            {
                this.ControlToFix.Height = pnlConent.Height - offset;
            }
            if (this.ControlToFix.Width != pnlConent.Width - offset)
            {
                this.ControlToFix.Width = pnlConent.Width - offset;
            }
        }
    }
}
