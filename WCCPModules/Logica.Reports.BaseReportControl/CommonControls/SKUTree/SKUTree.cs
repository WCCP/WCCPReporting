using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls
{   
    /// <summary>
    /// Implements general SKU tree functionality
    /// </summary>
    public partial class SKUTree : XtraUserControl, IControlReadOnly
    {
        /// <summary>
        /// Level that is allowed to be edited
        /// </summary>
        private int allowedLevel = 0;

        /// <summary>
        /// Determines whether chkBoxAll's value is up to date
        /// </summary>
        private bool handleChkAllClick = false;

        /// <summary>
        /// The value indicating whether nodes could be edited
        /// </summary>
        private bool readOnly;

        /// <summary>
        /// Initializes a new instance of the <see cref="KPITree"/> class.
        /// </summary>
        public SKUTree()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets a value indicating whether nodes could be edited
        /// </summary>
        /// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
        public bool ReadOnly
        {
            get
            {
                return readOnly;
            }
            set
            {
                if (AllowedLevel == 0)
                {
                    this.chkBoxAll.Enabled = !value;
                }

                readOnly = value;
            }
        }

        /// <summary>
        /// Gets the selected SKU id list.
        /// </summary>
        /// <value>The selected SKU id list</value>
        public string SelectedStringIdList
        {
            get
            {
                treeSKU.PostEditor();
                GetFinalCheckedNodesOperation lNodesOperation = new GetFinalCheckedNodesOperation(columnSKUId) { IsQuotesSeparated = false };
                treeSKU.NodesIterator.DoOperation(lNodesOperation);
                return lNodesOperation.SelectedIdList;
            }
        }

        /// <summary>
        /// Gets the selected SKU id list
        /// </summary>
        /// <value>The selected SKU id list</value>
        public SqlXml SelectedXMLIdList
        {
            get
            {
                treeSKU.PostEditor();

                if (chkBoxAll.CheckState == CheckState.Checked)
                {
                    return GetXmlSnapshot();
                }

                DataTable lDataTable = treeSKU.DataSource as DataTable;

                if (lDataTable != null)
                {
                    lDataTable.AcceptChanges();

                    DataView lDataView = lDataTable.Copy().DefaultView;

                    GetFinalCheckedNodesOperation lNodesOperation = new GetFinalCheckedNodesOperation(columnSKUId) { IsQuotesSeparated = true, IsKPItree = true };
                    treeSKU.NodesIterator.DoOperation(lNodesOperation);
                    if (lNodesOperation.Count > 0)
                    {
                        lDataView.RowFilter = string.Format("{0} IN ({1})", columnSKUId.FieldName, lNodesOperation.SelectedIdList);
                        return GetXmlSnapshot(lDataView.ToTable());
                    }
                }

                return GetXmlSnapshot(null);
            }
        }

        /// <summary>
        /// Gets or sets the level that is allowed to be edited
        /// </summary>
        /// <value>zero based level number</value>
        public int AllowedLevel
        {
            get
            {
                return allowedLevel;
            }
            set
            {
                allowedLevel = value;

                if (allowedLevel > 0)
                {
                    chkBoxAll.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Gets DataSource with posted changes
        /// </summary>
        public DataTable DataSourcePosted
        {
            get
            {
                treeSKU.PostEditor();
                UpdateTableNodesCheckStateOperation lNodesOperation = new UpdateTableNodesCheckStateOperation(columnSKUChecked);
                treeSKU.NodesIterator.DoOperation(lNodesOperation);

                DataTable lDataTable = treeSKU.DataSource as DataTable;

                if (lDataTable != null)
                {
                    lDataTable.AcceptChanges();
                }

                return lDataTable;
            }
        }

        /// <summary>
        /// Gets or sets the colour of the border
        /// </summary>
        public Color BorderColor
        {
            get
            {
                return this.panelBorder.BackColor;
            }
            set
            {
                this.panelBorder.BackColor = value;
            }
        }

        /// <summary>
        /// Gets selected combi products
        /// </summary>
        public List<DataRow> SelectedCombiProducts
        {
            get
            {
                // 2 - means combi producn
                return GetSelectedProductsInfoByLevel(2);
            }
        }

        /// <summary>
        /// Loads the KPI tree.
        /// </summary>
        public void Load(DataTable dataTable)
        {
            treeSKU.BeginUpdate();

            treeSKU.DataSource = dataTable;
            treeSKU.ExpandAll();
            treeSKU.CollapseAll();
            LoadSKUTreeState();

            treeSKU.EndUpdate();
            treeSKU.NodesIterator.DoOperation(new ExpandNodesOperation());

            RefreshChkBoxAll();
        }

        /// <summary>
        /// Called when [node check].
        /// </summary>
        /// <param name="node">The node.</param>
        private static void OnNodeCheck(TreeListNode node)
        {
            OnNodeCheckRecursive(node, node.Checked);

            if (node.Checked)
            {
                // check parent nodes if all childs are checked
                TreeListNode lParent = node.ParentNode;
                while (lParent != null)
                {
                    bool allChecked = true;
                    foreach (TreeListNode child in lParent.Nodes)
                    {
                        if (!child.Checked)
                        {
                            allChecked = false;
                            break;
                        }
                    }
                    if (allChecked)
                    {
                        lParent.Checked = true;
                        lParent = lParent.ParentNode;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                // uncheck parent nodes
                TreeListNode lParent = node.ParentNode;
                while (lParent != null)
                {
                    lParent.Checked = false;
                    lParent = lParent.ParentNode;
                }
            }
        }

        /// <summary>
        /// Called when [node check recursive].
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="isChecked">if set to <c>true</c> [is checked].</param>
        private static void OnNodeCheckRecursive(TreeListNode node, bool isChecked)
        {
            if (node != null)
            {
                node.Checked = isChecked;

                foreach (TreeListNode lChildNode in node.Nodes)
                {
                    OnNodeCheckRecursive(lChildNode, isChecked);
                }
            }
        }

        /// <summary>
        /// Returns the level of the node
        /// </summary>
        /// <param name="node">node to be processed</param>
        /// <returns>number of the level</returns>
        public int GetNodeLevel(TreeListNode node)
        {
            int res = 0;
            if (null != node)
            {
                res = ConvertEx.ToInt(node.GetValue(columnLevel));
            }
            return res;
        }

        /// <summary>
        /// Gets products info by level
        /// </summary>
        /// <param name="node">node</param>
        /// <param name="level">node level</param>
        /// <returns>list of row of the source table</returns>
        public List<DataRow> GetProductsInfoByLevel(TreeListNode node, int level)
        {
            List<DataRow> resultSet = new List<DataRow>();
            if (null != node)
            {
                if (null != columnLevel && ConvertEx.ToInt(node.GetValue(columnLevel)) == level)
                {
                    DataTable data = ((DataTable)node.TreeList.DataSource);
                    DataRow row = data.Rows[node.Id];
                    if (null != row)
                        resultSet.Add(row);
                }
                else if (node.HasChildren)
                {
                    foreach (TreeListNode childNode in node.Nodes)
                        resultSet.AddRange(GetProductsInfoByLevel(childNode, level).AsEnumerable());
                }
            }
            return resultSet;
        }

        /// <summary>
        /// Gets selected products info by level
        /// </summary>
        /// <param name="level">node level</param>
        /// <returns>list of row of the source table</returns>
        private List<DataRow> GetSelectedProductsInfoByLevel(int level)
        {
            treeSKU.PostEditor();
            GetCheckedNodesRowsByLevel operation = new GetCheckedNodesRowsByLevel(columnLevel, level);
            treeSKU.NodesIterator.DoOperation(operation);
            return operation.Result;
        }

        /// <summary>
        /// Determines whether all children are checked
        /// </summary>
        /// <param name="node">currrent node</param>
        /// <returns>true if all children are checked</returns>
        private bool AreChildrenChecked(TreeListNode node)
        {
            bool result = node.Checked;

            foreach (TreeListNode cnode in node.Nodes)
            {
                result = result || AreChildrenChecked(cnode);

                if (result)
                {
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Handles the BeforeCheckNode event of the treeSKU control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.CheckNodeEventArgs"/> instance containing the event data.</param>
        private void treeSKU_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            bool readOnly = this.ReadOnly || AllowedLevel > 0 && GetNodeLevel(e.Node) != AllowedLevel || (GetSelectedProductsInfoByLevel(2).Count >=1 && e.PrevState == CheckState.Unchecked && AllowedLevel > 0) ;

            foreach (TreeListNode cnode in e.Node.Nodes)
            {
                readOnly = readOnly || AreChildrenChecked(cnode); 
            }

            TreeListNode node = e.Node.ParentNode;
            
            while (node != null && !readOnly)
            {
                readOnly = node.Checked;
                node = node.ParentNode;
            }

            e.CanCheck = !readOnly;
        }

        /// <summary>
        /// Handles the AfterCheckNode event of the treeSKU control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.NodeEventArgs"/> instance containing the event data.</param>
        private void treeSKU_AfterCheckNode(object sender, NodeEventArgs e)
        {
            //OnNodeCheck(e.Node);

            if (OnValueChanged != null)
            {
                OnValueChanged(this, e/*new EventArgs()*/);
            }

            treeSKU.SetFocusedNode(e.Node);

            RefreshChkBoxAll();
        }

        /// <summary>
        /// Gets the SKU tree XML snapshot.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns>xml Id list</returns>
        private SqlXml GetXmlSnapshot(DataTable dataTable)
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement("root");
                if (dataTable != null)
                    foreach (DataRow lRow in dataTable.Rows)
                    {
                        lWriter.WriteStartElement("row");
                        AddFullEndXMLElement(lWriter, columnItemID.FieldName.ToLower(), lRow[columnItemID.FieldName]);
                        AddFullEndXMLElement(lWriter, columnLevel.FieldName.ToLower(), lRow[columnLevel.FieldName]);
                        lWriter.WriteEndElement();
                    }
                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                return new SqlXml(lStream);
            }
        }

        /// <summary>
        /// Gets the SKU tree XML snapshot.
        /// </summary>
        /// <returns>xml Id list</returns>
        private SqlXml GetXmlSnapshot()
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement("root");
                
                lWriter.WriteStartElement("row");
                AddFullEndXMLElement(lWriter, columnItemID.FieldName.ToLower(), -1);
                AddFullEndXMLElement(lWriter, columnLevel.FieldName.ToLower(), -1);
                lWriter.WriteEndElement();
                
                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                return new SqlXml(lStream);
            }
        }

        /// <summary>
        /// Loads the state of the SKU tree.
        /// </summary>
        private void LoadSKUTreeState()
        {
            treeSKU.NodesIterator.DoOperation(new UpdateNodesCheckStateOperation(columnSKUChecked));
        }

        /// <summary>
        /// Handles the CheckStateChanged event of the chkBoxAll control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void chkBoxAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (handleChkAllClick)
            {
                if (chkBoxAll.CheckState == CheckState.Indeterminate)
                {
                    chkBoxAll.CheckState = CheckState.Unchecked;
                    return;
                }

                foreach (TreeListNode node in treeSKU.Nodes)
                {
                    node.Checked = false;
                    OnNodeCheck(node);
                }

                if (chkBoxAll.CheckState == CheckState.Checked)
                {
                    foreach (TreeListNode node in treeSKU.Nodes)
                    {
                        if (GetNodeLevel(node) == 0)
                        {
                            node.Checked = true;
                        }
                    }
                }

                if (CheckState.Indeterminate != chkBoxAll.CheckState)
                {
                    if (null != OnCheckAllChanged)
                        OnCheckAllChanged(this, new SKUTreeEventArgs(chkBoxAll.Checked));
                }
            }
        }

        /// <summary>
        /// Refreshes the CHK box all.
        /// </summary>
        private void RefreshChkBoxAll()
        {
            handleChkAllClick = false;

            bool? chkBoxChecked = GetCheckedState(treeSKU.Nodes.Cast<TreeListNode>());

            if (chkBoxChecked.HasValue)
            {
                this.chkBoxAll.Checked = chkBoxChecked.Value;
            }
            else
            {
                this.chkBoxAll.CheckState = CheckState.Indeterminate;
            }

            handleChkAllClick = true;
        }

        /// <summary>
        /// Adds the full end XML node.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="nodeName">Name of the node.</param>
        /// <param name="nodeValue">The node value.</param>
        private static void AddFullEndXMLElement(XmlWriter writer, string nodeName, object nodeValue)
        {
            writer.WriteStartElement(nodeName);
            if (nodeValue != null && nodeValue != DBNull.Value)
            {
                decimal lValue = ConvertEx.ToDecimal(nodeValue);
                writer.WriteString(lValue.ToString(CultureInfo.InvariantCulture));
            }
            writer.WriteEndElement();
        }

        /// <summary>
        /// Determines whether the node is checked
        /// </summary>
        /// <param name="node">the node to be processed</param>
        /// <returns>state of the node</returns>
        private static bool? GetCheckedState(TreeListNode node)
        {
            if (node.Checked) return true;
            if (!node.HasChildren) return false;

            return GetCheckedState(node.Nodes.Cast<TreeListNode>());
        }

        /// <summary>
        /// Determines whether the nodes are checked
        /// </summary>
        /// <param name="nodes">the nodes to be processed</param>
        /// <returns>state of the nodes</returns>
        private static bool? GetCheckedState(IEnumerable<TreeListNode> nodes)
        {
            if (nodes == null || nodes.Count() == 0) return null;

            bool? chkBoxChecked = null;

            foreach (TreeListNode node in nodes)
            {
                if (!chkBoxChecked.HasValue)
                {
                    chkBoxChecked = GetCheckedState(node);
                    if (chkBoxChecked == null)
                    {
                        break;
                    }
                }
                else if (chkBoxChecked != GetCheckedState(node))
                {
                    chkBoxChecked = null;
                    break;
                }
            }

            return chkBoxChecked;
        }

        /// <summary>
        /// Additional action on node checked 
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="args">The instance containing the event data</param>
        public delegate void ValueChanged(object sender, EventArgs args);
        public event ValueChanged OnValueChanged;

        /// <summary>
        /// On checkAll changed
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="args">The instance containing the event data</param>
        public delegate void CheckAllChanged(object sender, SKUTreeEventArgs args);
        public event CheckAllChanged OnCheckAllChanged;

        /// <summary>
        /// 
        /// </summary>
        internal static class Constants
        {
          /// <summary>
          /// 
          /// </summary>
          public const string DELIMITER_ID_LIST = ",";
        }
  }

    /// <summary>
    /// Dscribes SKUTree event args
    /// </summary>
    public partial class SKUTreeEventArgs : EventArgs
    {
        public bool IsAllItemsChecked { get; private set; }
        public SKUTreeEventArgs(bool isAllItemsChecked)
            : base()
        {
            IsAllItemsChecked = isAllItemsChecked;
        }
    }
}