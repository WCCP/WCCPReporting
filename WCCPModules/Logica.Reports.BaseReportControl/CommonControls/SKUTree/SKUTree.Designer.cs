namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class SKUTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeSKU = new DevExpress.XtraTreeList.TreeList();
            this.columnSKUId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnSKUParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnSKUName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnSKUChecked = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnItemID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkBoxAll = new DevExpress.XtraEditors.CheckEdit();
            this.panelBorder = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.treeSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxAll.Properties)).BeginInit();
            this.panelBorder.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeSKU
            // 
            this.treeSKU.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.treeSKU.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnSKUId,
            this.columnSKUParentId,
            this.columnSKUName,
            this.columnSKUChecked,
            this.columnLevel,
            this.columnItemID});
            this.treeSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeSKU.KeyFieldName = "Id";
            this.treeSKU.Location = new System.Drawing.Point(0, 0);
            this.treeSKU.Margin = new System.Windows.Forms.Padding(0);
            this.treeSKU.Name = "treeSKU";
            this.treeSKU.OptionsView.ShowCheckBoxes = true;
            this.treeSKU.OptionsView.ShowColumns = false;
            this.treeSKU.OptionsView.ShowHorzLines = false;
            this.treeSKU.OptionsView.ShowIndicator = false;
            this.treeSKU.ParentFieldName = "Parent_id";
            this.treeSKU.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit});
            this.treeSKU.Size = new System.Drawing.Size(562, 160);
            this.treeSKU.TabIndex = 2;
            this.treeSKU.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeSKU_BeforeCheckNode);
            this.treeSKU.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeSKU_AfterCheckNode);
            // 
            // columnSKUId
            // 
            this.columnSKUId.Caption = "ID";
            this.columnSKUId.FieldName = "Id";
            this.columnSKUId.Name = "columnSKUId";
            // 
            // columnSKUParentId
            // 
            this.columnSKUParentId.Caption = "Paren ID";
            this.columnSKUParentId.FieldName = "Parent_Id";
            this.columnSKUParentId.Name = "columnSKUParentId";
            // 
            // columnSKUName
            // 
            this.columnSKUName.Caption = "Name";
            this.columnSKUName.FieldName = "name";
            this.columnSKUName.Name = "columnSKUName";
            this.columnSKUName.OptionsColumn.AllowEdit = false;
            this.columnSKUName.OptionsColumn.ReadOnly = true;
            this.columnSKUName.Visible = true;
            this.columnSKUName.VisibleIndex = 0;
            // 
            // columnSKUChecked
            // 
            this.columnSKUChecked.Caption = "Checked";
            this.columnSKUChecked.FieldName = "Checked";
            this.columnSKUChecked.Name = "columnSKUChecked";
            // 
            // columnLevel
            // 
            this.columnLevel.Caption = "treeListColumn1";
            this.columnLevel.FieldName = "level";
            this.columnLevel.Name = "columnLevel";
            // 
            // columnItemID
            // 
            this.columnItemID.Caption = "treeListColumn1";
            this.columnItemID.FieldName = "Item_ID";
            this.columnItemID.Name = "columnItemID";
            // 
            // repositoryItemCheckEdit
            // 
            this.repositoryItemCheckEdit.AutoHeight = false;
            this.repositoryItemCheckEdit.Name = "repositoryItemCheckEdit";
            // 
            // chkBoxAll
            // 
            this.chkBoxAll.Location = new System.Drawing.Point(10, 10);
            this.chkBoxAll.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkBoxAll.Name = "chkBoxAll";
            this.chkBoxAll.Properties.AllowGrayed = true;
            this.chkBoxAll.Properties.Caption = "������� ��� ���";
            this.chkBoxAll.Size = new System.Drawing.Size(136, 21);
            this.chkBoxAll.TabIndex = 3;
            this.chkBoxAll.CheckStateChanged += new System.EventHandler(this.chkBoxAll_CheckStateChanged);
            // 
            // panelBorder
            // 
            this.panelBorder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBorder.Controls.Add(this.treeSKU);
            this.panelBorder.Location = new System.Drawing.Point(0, 37);
            this.panelBorder.Margin = new System.Windows.Forms.Padding(0);
            this.panelBorder.Name = "panelBorder";
            this.panelBorder.Size = new System.Drawing.Size(562, 160);
            this.panelBorder.TabIndex = 4;
            // 
            // SKUTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelBorder);
            this.Controls.Add(this.chkBoxAll);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SKUTree";
            this.Size = new System.Drawing.Size(562, 197);
            ((System.ComponentModel.ISupportInitialize)(this.treeSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxAll.Properties)).EndInit();
            this.panelBorder.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeSKU;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnSKUId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnSKUParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnSKUName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnSKUChecked;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit;
        private DevExpress.XtraEditors.CheckEdit chkBoxAll;
        private System.Windows.Forms.Panel panelBorder;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnItemID;

    }
}
