﻿using DevExpress.XtraGrid.Views.Base;

namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates
{
	/// <summary>
	/// Delegate which will draw cell. Returns true if default drawing actions not required.
	/// </summary>
	public delegate bool Drawer(object sender, RowCellCustomDrawEventArgs e, object cellValue);

	/// <summary>
	/// Abstract factory of drawers. Returns Delegate which will draw cell by string.
	/// </summary>
	public interface IDrawerFactory
	{
		/// <summary>
		/// Gets the drawer.
		/// </summary>
		/// <param name="drawerName">Name of the drawer.</param>
		/// <returns>Delegate which will draw cell.</returns>
		Drawer GetDrawer(string drawerName);
	}
}