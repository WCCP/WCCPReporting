﻿using System.IO;
using System.Xml;

namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates.DataStorage
{
	internal class XmlTextWriterFormattedNoDeclaration : XmlTextWriter
	{
		#region Constructors

		public XmlTextWriterFormattedNoDeclaration(TextWriter w)
			: base(w)
		{
			//Formatting = System.Xml.Formatting.Indented;
		}

		#endregion

		#region Instance Methods

		public override void WriteStartDocument()
		{
			// suppress
		}

		#endregion
	}
}