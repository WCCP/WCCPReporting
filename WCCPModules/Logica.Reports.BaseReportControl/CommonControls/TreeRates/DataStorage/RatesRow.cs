﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates.DataStorage
{
	internal class RatesRow
	{
		#region Enums

		public enum SummaryItemType
		{
			Sum,
			YTD,
			Min,
			Max
		} ;

		#endregion

		#region Constructors

		public RatesRow()
		{
			Cells = new List<RateCell>();
			MaxLevelForSum = -1; // Means that all levels summarize this parameter
			SummaryType = SummaryItemType.Sum;
		}

		#endregion

		#region Instance Properties

		public List<RateCell> Cells { get; set; }

		public List<ChangedCell> ChangedCells
		{
			get
			{
				return
					(
						from c in Cells
						where c.IsChanged && !c.IsSummaryCell
						select new ChangedCell
						       	{
						       		RowId = ID,
						       		PeriodId = c.PeriodId,
						       		PlanOrFact = c.PlanOrFact,
						       		Value = Convert.ToDecimal(c.Value)
						       	}
					).ToList();
			}
		}

		public int ID { get; set; }

		public int MaxLevelForSum { get; set; }

		public SummaryItemType SummaryType { get; set; }

		#endregion

		#region Instance Methods

		/// <summary>
		///   Adds the specified source row.
		/// </summary>
		/// <param name = "source">The source row.</param>
		public void Add(RatesRow source)
		{
			if (source.ID != ID || source.Cells.Count != Cells.Count)
				throw new Exception("Can't add row with different ID or different count of cells");
			for (int i = 0; i < Cells.Count; i++)
			{
				Cells[i].Add(source.Cells[i]);
			}
		}

		/// <summary>
		///   Clones this instance.
		/// </summary>
		/// <returns>Clone of this instance</returns>
		public RatesRow Clone()
		{
			RatesRow result = new RatesRow()
			                  	{
			                  		ID = ID,
			                  		MaxLevelForSum = MaxLevelForSum
			                  	};
			Cells.ForEach(childCell => result.Cells.Add(childCell.Clone()));
			return result;
		}

		/// <summary>
		///   Gets the nodes by example.
		/// </summary>
		/// <param name = "cell">The sample cell.</param>
		/// <returns>Nodes with same type</returns>
		public List<RateCell> GetNodesByExample(RateCell cell)
		{
			return
				(
					from c in Cells
					where c.PlanOrFact.Equals(cell.PlanOrFact) && !c.IsSummaryCell
					select c
				).ToList();
		}

		/// <summary>
		///   Gets the summary item.
		/// </summary>
		/// <param name = "cell">The cell.</param>
		/// <returns>Summary item</returns>
		public RateCell GetSummaryItem(RateCell cell)
		{
			return
				(
					from c in Cells
					where c.PlanOrFact.Equals(cell.PlanOrFact) && c.IsSummaryCell
					select c
				).First();
		}

		/// <summary>
		///   Updates the summary item.
		/// </summary>
		/// <param name = "changedCell">The changed cell.</param>
		public void UpdateSummaryItem(RateCell changedCell)
		{
			List<RateCell> cells = GetNodesByExample(changedCell);
			RateCell summaryItem = GetSummaryItem(changedCell);
			IEnumerable<decimal> cellsSequence = (from d in cells select ConvertEx.ToDecimal(d.Value));
			switch (SummaryType)
			{
				case SummaryItemType.Sum:
					summaryItem.Value = cellsSequence.Sum();
					break;
				case SummaryItemType.YTD:
					for (int i = 1; i < cells.Count; i++)
					{
						if (ConvertEx.ToDecimal(cells[i - 1].Value) > ConvertEx.ToDecimal(cells[i].Value))
							cells[i].Value = cells[i - 1].Value;
					}
					cellsSequence = (from d in cells select ConvertEx.ToDecimal(d.Value));
					decimal[] decimals = cellsSequence.Where(item => item > 0).ToArray();
					summaryItem.Value = decimals.Length > 0 ? decimals.Last() : 0;
					break;
				case SummaryItemType.Max:
					summaryItem.Value = cellsSequence.Max();
					break;
				case SummaryItemType.Min:
					summaryItem.Value = cellsSequence.Min();
					break;
			}
		}

		#endregion
	}
}