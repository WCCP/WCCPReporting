﻿using System;

namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates.DataStorage
{
	internal class RatesColumn
	{
		#region Constructors

		private RatesColumn(string name, string bandName)
		{
			Name = name;
			BandName = bandName;
		}

		#endregion

		#region Instance Properties

		public string BandName { get; private set; }

		public string DataTableName
		{
			get { return String.IsNullOrEmpty(BandName) ? Name : String.Format("{0}_{1}", BandName, Name); }
		}

		public string Name { get; private set; }

		#endregion

		#region Instance Methods

		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			if (obj is string)
			{
				return DataTableName.Equals((string) obj);
			}
			if (obj is RatesColumn)
			{
				return DataTableName.Equals(((RatesColumn) obj).DataTableName);
			}
			return false;
		}

		public override string ToString()
		{
			return Name;
		}

		#endregion

		#region Class Methods

		public static RatesColumn GetRatesColumn(string name, string bandName)
		{
			return new RatesColumn(name, bandName);
		}

		public static RatesColumn GetRatesColumn(string name)
		{
			return GetRatesColumn(name, String.Empty);
		}

		#endregion
	}
}