﻿using System;

namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates.DataStorage
{
	internal class RateCell
	{
		#region Fields

		private object value;

		#endregion

		#region Constructors

		public RateCell(object val)
		{
			Value = val;
			PeriodId = string.Empty;
			PlanOrFact = string.Empty;
			Drawer = string.Empty;
			IsSummaryCell = false;
			IsChanged = false;
			IsValid = true;
		}

		#endregion

		#region Instance Properties

		public string Drawer { get; set; }

		public bool Editable
		{
			get { return !string.IsNullOrEmpty(PeriodId); }
		}

		public bool IsChanged { get; protected set; }

		public bool IsSummaryCell { get; set; }

		public bool IsValid { get; set; }

		public string PeriodId { get; set; }

		public string PlanOrFact { get; set; }

		public object Value
		{
			get { return value; }
			set
			{
				if (this.value == value)
					return;
				this.value = value;
				IsChanged = true;
			}
		}

		#endregion

		#region Instance Methods

		public override string ToString()
		{
			return Value.ToString();
		}

		public void Add(RateCell source)
		{
			if (source.PeriodId != PeriodId || source.PlanOrFact != PlanOrFact)
				throw new Exception("To different rows");
			if (source.Value.GetType() == typeof (decimal))
			{
				Value = (decimal) Value + (decimal) source.Value;
			}
		}

		public RateCell Clone()
		{
			return new RateCell(Value)
			       	{
			       		PeriodId = PeriodId,
			       		PlanOrFact = PlanOrFact,
			       		IsSummaryCell = IsSummaryCell,
			       		Drawer = Drawer
			       	};
		}

		#endregion
	}
}