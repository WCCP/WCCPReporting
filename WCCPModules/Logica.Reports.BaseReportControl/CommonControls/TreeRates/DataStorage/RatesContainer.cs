﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates.DataStorage
{
    internal class RatesContainer
    {
        #region Fields

        private List<RatesColumn> columns = new List<RatesColumn>();
        private List<RatesRow> rows = new List<RatesRow>();

        #endregion

        #region Instance Properties

        public List<RatesColumn> Columns
        {
            get { return columns; }
            set { columns = value; }
        }

        public List<RatesRow> Rows
        {
            get { return rows; }
            set { rows = value; }
        }

        public DataTable Table
        {
            get
            {
                if (!IsValid) return null;
                DataTable result = new DataTable();
                columns.ForEach(column => result.Columns.Add(column.DataTableName));
                foreach (RatesRow row in rows)
                {
                    result.Rows.Add((from d in row.Cells.ToArray() select d.Value).ToArray());
                }
                return result;
            }
        }

        private bool IsValid
        {
            get { return Rows.All(row => row.Cells.Count == Columns.Count); }
        }

        #endregion

        #region Instance Methods

        public void Add(RatesContainer source, int currentNodeLevel)
        {
            if (source.Columns.Count != Columns.Count || source.Rows.Count != Rows.Count)
                throw new Exception("Adding of RatesContainer with different num of rows is imposible");
            for (int i = 0; i < Rows.Count; i++)
            {
                RatesRow currentRow = Rows[i];
                if (currentRow.MaxLevelForSum == -1 || currentNodeLevel <= currentRow.MaxLevelForSum)
                    currentRow.Add(source.Rows[i]);
            }
        }

        public RatesContainer Clone()
        {
            RatesContainer result = new RatesContainer();
            columns.ForEach(col => result.Columns.Add(col));
            rows.ForEach(row => result.Rows.Add(row.Clone()));
            return result;
        }

        public string GetChanges()
        {
            List<ChangedCell> changes = new List<ChangedCell>();
            foreach (RatesRow row in Rows)
            {
                changes.AddRange(row.ChangedCells);
            }
            if (changes.Count == 0)
            {
                return string.Empty;
            }
            else
            {
                try
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");

                    XmlSerializer xmlr = new XmlSerializer(typeof (List<ChangedCell>));
                    StringWriter sw = new StringWriter();
                    XmlTextWriter tw = new XmlTextWriterFormattedNoDeclaration(sw);

                    xmlr.Serialize(tw, changes, ns);

                    return sw.ToString();
                }
                catch (Exception e)
                {
                    return string.Empty;
                }
            }
        }

        public void PrepareBandedGridView(BandedGridView bgv)
        {
            bgv.BeginUpdate();

            try
            {
                bgv.Columns.Clear();
                bgv.Bands.Clear();

                if (!IsValid) return;

                GridBand currentBand = null;
                int leftCoord = 0;
                foreach (RatesColumn column in Columns)
                {
                    if (currentBand == null || !currentBand.Caption.Equals(column.BandName))
                    {
                        currentBand = new GridBand {Caption = column.BandName};
                        if (IsFixed(column))
                        {
                            currentBand.Fixed = FixedStyle.Left;
                        }
                        currentBand.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                        currentBand.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
                        currentBand.AppearanceHeader.Options.UseTextOptions = true;
                        bgv.Bands.Add(currentBand);
                    }

                    BandedGridColumn col = new BandedGridColumn {FieldName = column.DataTableName, Caption = column.Name, Visible = true};
                    col.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    col.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
                    col.AppearanceHeader.Options.UseTextOptions = true;


                    if (IsEditableColumn(column))
                    {
                        RepositoryItemSpinEdit repositoryItemSpinEdit = new RepositoryItemSpinEdit {AutoHeight = false};
                        repositoryItemSpinEdit.Mask.EditMask = "N3";
                        repositoryItemSpinEdit.DisplayFormat.FormatType = FormatType.Numeric;
                        repositoryItemSpinEdit.DisplayFormat.FormatString = "N3";
                        repositoryItemSpinEdit.UseParentBackground = true;
                        repositoryItemSpinEdit.MinValue = decimal.Zero;
                        repositoryItemSpinEdit.MaxValue = decimal.MaxValue;
                        repositoryItemSpinEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(repositoryItemSpinEdit_EditValueChanging);
                        bgv.GridControl.RepositoryItems.Add(repositoryItemSpinEdit);
                        col.ColumnEdit = repositoryItemSpinEdit;
                    }

                    bgv.Columns.Add(col);
                    currentBand.Columns.Add(col);
                    col.Width = GetColumnWidth(column);
                    if (IsFixed(column))
                    {
                        leftCoord += col.Width;
                    }
                }
                bgv.LeftCoord = leftCoord;
            }
            finally
            {
                bgv.EndUpdate();
            }
        }

        private int GetColumnWidth(RatesColumn column)
        {
            return Columns.IndexOf(column) == 0 ? 200 : 85;
        }

        private bool IsEditableColumn(RatesColumn column)
        {
            int colIndex = Columns.IndexOf(column);
            return Rows.Any(row => row.Cells[colIndex].Editable);
        }

        private bool IsFixed(RatesColumn column)
        {
            return Columns.IndexOf(column) < 3;
        }

        #endregion

        #region Class Methods

        public static RatesContainer LoadXML(XmlDocument doc)
        {
            RatesContainer result = new RatesContainer();

            foreach (XmlNode xmlNodeRow in doc.ChildNodes[0].ChildNodes)
            {
                RatesRow ratesRow = new RatesRow();
                XmlNode id = xmlNodeRow.Attributes.GetNamedItem("id");
                if (id != null)
                {
                    ratesRow.ID = Convert.ToInt32(id.Value);
                }
                XmlNode totalType = xmlNodeRow.Attributes.GetNamedItem("sumtype");
                if (totalType != null && !String.IsNullOrEmpty(totalType.Value.Trim()))
                {
                    object sumType = Enum.Parse(typeof (RatesRow.SummaryItemType), totalType.Value.Trim(), true);
                    if (sumType is RatesRow.SummaryItemType)
                        ratesRow.SummaryType = (RatesRow.SummaryItemType) sumType;
                }
                XmlNode maxLevelForSum = xmlNodeRow.Attributes.GetNamedItem("not_sum_level");
                if (maxLevelForSum != null)
                {
                    ratesRow.MaxLevelForSum = Convert.ToInt32(maxLevelForSum.Value);
                }

                foreach (XmlNode xmlNodeCol in xmlNodeRow.ChildNodes)
                {
                    RateCell cell;

                    decimal value = 0;
                    if (decimal.TryParse(xmlNodeCol.InnerText, System.Globalization.NumberStyles.Float, new System.Globalization.NumberFormatInfo(), out value))
                    {
                        cell = new RateCell(value);
                    }
                    else
                    {
                        cell = new RateCell(xmlNodeCol.InnerText);
                    }

                    XmlNode period = xmlNodeCol.Attributes.GetNamedItem("period");
                    if (period != null)
                    {
                        cell.PeriodId = period.Value;
                    }

                    XmlNode s = xmlNodeCol.Attributes.GetNamedItem("s");
                    if (s != null)
                    {
                        cell.PlanOrFact = s.Value;
                    }

                    XmlNode total = xmlNodeCol.Attributes.GetNamedItem("isTot");
                    if (total != null && total.Value.Equals("1"))
                    {
                        cell.IsSummaryCell = true;
                    }
                    XmlNode drawer = xmlNodeCol.Attributes.GetNamedItem("draw");
                    if (drawer != null)
                    {
                        cell.Drawer = drawer.Value;
                    }
                    ratesRow.Cells.Add(cell);
                }
                result.rows.Add(ratesRow);
            }
            result.Columns = (from d in result.rows[0].Cells select RatesColumn.GetRatesColumn(d.Value.ToString(), d.PlanOrFact)).ToList();
            result.rows.RemoveAt(0);
            return result;
        }

        private static void repositoryItemSpinEdit_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.NewValue.ToString().StartsWith("-"))
                e.Cancel = true;
        }

        #endregion
    }
}