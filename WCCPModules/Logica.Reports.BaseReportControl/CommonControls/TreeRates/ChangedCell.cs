﻿namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates
{
	public class ChangedCell
	{
		public ChangedCell()
		{
		}

		/// <summary>
		/// Gets or sets the row id.
		/// </summary>
		/// <value>The row id.</value>
		public int RowId { get; set; }

		/// <summary>
		/// Gets or sets the period id.
		/// </summary>
		/// <value>The period id.</value>
		public string PeriodId { get; set; }

		/// <summary>
		/// Gets or sets the plan or fact indicator.
		/// </summary>
		/// <value>The plan or fact indicator.</value>
		public string PlanOrFact { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public decimal Value { get; set; }
	}
}