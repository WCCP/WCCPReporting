﻿namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates
{
  partial class TreeRates
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
		this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
		this.treeList = new DevExpress.XtraTreeList.TreeList();
		this.treeColName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
		this.treeColId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
		this.treeColGridData = new DevExpress.XtraTreeList.Columns.TreeListColumn();
		this.treeColLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
		this.gridControl = new DevExpress.XtraGrid.GridControl();
		this.bandedGridView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
		((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
		this.splitContainerControl1.SuspendLayout();
		((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
		((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
		((System.ComponentModel.ISupportInitialize)(this.bandedGridView)).BeginInit();
		this.SuspendLayout();
		// 
		// splitContainerControl1
		// 
		this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
		this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
		this.splitContainerControl1.Name = "splitContainerControl1";
		this.splitContainerControl1.Panel1.Controls.Add(this.treeList);
		this.splitContainerControl1.Panel1.Text = "Panel1";
		this.splitContainerControl1.Panel2.Controls.Add(this.gridControl);
		this.splitContainerControl1.Panel2.Text = "Panel2";
		this.splitContainerControl1.Size = new System.Drawing.Size(819, 572);
		this.splitContainerControl1.SplitterPosition = 239;
		this.splitContainerControl1.TabIndex = 0;
		this.splitContainerControl1.Text = "splitContainerControl1";
		// 
		// treeList
		// 
		this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeColName,
            this.treeColId,
            this.treeColGridData,
            this.treeColLevel});
		this.treeList.Dock = System.Windows.Forms.DockStyle.Fill;
		this.treeList.Location = new System.Drawing.Point(0, 0);
		this.treeList.Name = "treeList";
		this.treeList.OptionsBehavior.Editable = false;
		this.treeList.OptionsMenu.EnableColumnMenu = false;
		this.treeList.OptionsMenu.EnableFooterMenu = false;
		this.treeList.OptionsView.ShowIndicator = false;
		this.treeList.ParentFieldName = "PARENT_ID";
		this.treeList.Size = new System.Drawing.Size(239, 572);
		this.treeList.TabIndex = 0;
		this.treeList.AfterFocusNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeList_AfterFocusNode);
		// 
		// treeColName
		// 
		this.treeColName.Caption = "Измерения целей по V, затратам и МАСО";
		this.treeColName.FieldName = "DATA";
		this.treeColName.Name = "treeColName";
		this.treeColName.Visible = true;
		this.treeColName.VisibleIndex = 0;
		// 
		// treeColId
		// 
		this.treeColId.Caption = "ID";
		this.treeColId.FieldName = "ID";
		this.treeColId.Name = "treeColId";
		// 
		// treeColGridData
		// 
		this.treeColGridData.Caption = "GRID_DATA";
		this.treeColGridData.FieldName = "GRID_DATA";
		this.treeColGridData.Name = "treeColGridData";
		// 
		// treeColLevel
		// 
		this.treeColLevel.Caption = "LEVEL";
		this.treeColLevel.FieldName = "LEVEL";
		this.treeColLevel.Name = "treeColLevel";
		// 
		// gridControl
		// 
		this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
		this.gridControl.Location = new System.Drawing.Point(0, 0);
		this.gridControl.MainView = this.bandedGridView;
		this.gridControl.Name = "gridControl";
		this.gridControl.Size = new System.Drawing.Size(574, 572);
		this.gridControl.TabIndex = 0;
		this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView});
		// 
		// bandedGridView
		// 
		this.bandedGridView.GridControl = this.gridControl;
		this.bandedGridView.Name = "bandedGridView";
		this.bandedGridView.OptionsCustomization.AllowColumnMoving = false;
		this.bandedGridView.OptionsCustomization.AllowFilter = false;
		this.bandedGridView.OptionsCustomization.AllowGroup = false;
		this.bandedGridView.OptionsCustomization.AllowQuickHideColumns = false;
		this.bandedGridView.OptionsCustomization.AllowSort = false;
		this.bandedGridView.OptionsFilter.AllowFilterEditor = false;
		this.bandedGridView.OptionsView.ColumnAutoWidth = false;
		this.bandedGridView.OptionsView.ShowGroupPanel = false;
		this.bandedGridView.OptionsView.ShowIndicator = false;
		this.bandedGridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
		this.bandedGridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView_CustomDrawCell);
		this.bandedGridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
		// 
		// TreeRates
		// 
		this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		this.Controls.Add(this.splitContainerControl1);
		this.Name = "TreeRates";
		this.Size = new System.Drawing.Size(819, 572);
		this.Resize += new System.EventHandler(this.TreeRates_Resize);
		((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
		this.splitContainerControl1.ResumeLayout(false);
		((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
		((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
		((System.ComponentModel.ISupportInitialize)(this.bandedGridView)).EndInit();
		this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
	private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraTreeList.TreeList treeList;
    private DevExpress.XtraTreeList.Columns.TreeListColumn treeColName;
    private DevExpress.XtraTreeList.Columns.TreeListColumn treeColId;
    private DevExpress.XtraTreeList.Columns.TreeListColumn treeColGridData;
	private DevExpress.XtraTreeList.Columns.TreeListColumn treeColLevel;
	private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView;

  }
}
