﻿using System.Drawing;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl.CommonControls.TreeRates.DataStorage;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors;
using System.ComponentModel;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace Logica.Reports.BaseReportControl.CommonControls.TreeRates
{
	public partial class TreeRates : UserControl
	{
		private const string FIELD_CHANGES = "CHANGES";

		#region Private fields

		private Dictionary<int, RatesContainer> data = new Dictionary<int, RatesContainer>();

		private int minGridWidth;

		#endregion

		#region Constructors

		public TreeRates()
		{
			InitializeComponent();
			DisabledCellsByType = new List<string>();
		}

		#endregion

		#region Properties

		public DataTable DataSource
		{
			get
			{
				DataTable original = (DataTable) treeList.DataSource;
				if (original == null)
					return null;

				DataTable dt = original.Copy();

				if (dt.Columns.IndexOf(FIELD_CHANGES) == -1)
					dt.Columns.Add(FIELD_CHANGES);
				foreach (DataRow row in dt.Rows)
				{
					if (row[treeColGridData.FieldName] != null && row[treeColGridData.FieldName] != DBNull.Value)
					{
						string changes = data[Convert.ToInt32(row[treeColId.FieldName])].GetChanges();
						if (!string.IsNullOrEmpty(changes))
							row[FIELD_CHANGES] = changes;
					}
				}
				return dt;
			}
			set
			{
				treeList.DataSource = value;
				data.Clear();
				if (value != null && value.Rows.Count > 0)
				{
					treeList.ExpandAll();
					foreach (DataRow row in value.Rows)
					{
						if (row[treeColGridData.FieldName] != DBNull.Value)
						{
							XmlDocument doc = new XmlDocument();
							doc.LoadXml(row[treeColGridData.FieldName].ToString());
							data.Add(Convert.ToInt32(row[treeColId.FieldName]), RatesContainer.LoadXML(doc));
						}
					}
					if (treeList.FocusedNode == null)
					{
						treeList.FocusedNode = treeList.Nodes[0];
					}
					CalculateParentNode(treeList.FocusedNode);
					SetRatesDataSource(Convert.ToInt32(treeList.FocusedNode.GetValue(treeColId)));
				}
				else
				{
					gridControl.DataSource = null;
				}
			}
		}

		public string FiledWithChanges
		{
			get { return FIELD_CHANGES; }
		}

		private bool showTree = true;

		public bool ShowTree
		{
			set
			{
				showTree = value;
				splitContainerControl1.PanelVisibility = showTree ? SplitPanelVisibility.Both : SplitPanelVisibility.Panel2;
			}
			get { return showTree; }
		}

		public List<string> DisabledCellsByType { get; set; }


		public bool IsEditable
		{
			get { return bandedGridView.OptionsBehavior.Editable; }
			set { bandedGridView.OptionsBehavior.Editable = value; }
		}

		public IDrawerFactory Drawers { get; set; }

		#endregion

		private void SetRatesDataSource(int id)
		{
			if (!data.ContainsKey(id))
				return;

			RatesContainer container = data[id];
			container.PrepareBandedGridView(bandedGridView);
			gridControl.DataSource = container.Table;

		}

		private void DiscardDataInNodeAndParents(TreeListNode node)
		{
			if (node == null)
				return;
			DiscardDataInNodeAndParents(node.ParentNode);
			data.Remove(Convert.ToInt32(node.GetValue(treeColId)));
		}

		private void DiscardDataInNodeAndChilds(TreeListNode node)
		{
			if (!node.HasChildren)
				return;

			data.Remove(Convert.ToInt32(node.GetValue(treeColId)));

			foreach (TreeListNode childNode in node.Nodes)
			{
				DiscardDataInNodeAndChilds(childNode);
			}
		}

		private void CalculateParentNode(TreeListNode node)
		{
			int id = Convert.ToInt32(node.GetValue(treeColId));
			int level = ConvertEx.ToInt(node.GetValue(treeColLevel));

			if (data.ContainsKey(id) || !node.HasChildren)
				return;

			bool isContainerInitialized = false;
			RatesContainer currentContainer = new RatesContainer();
			foreach (TreeListNode child in node.Nodes)
			{
				CalculateParentNode(child);
				int childId = Convert.ToInt32(child.GetValue(treeColId));
				if (!data.ContainsKey(childId) || data[childId] == null)
					continue; //End list without data. Just skip it.

				RatesContainer childContainer = data[childId];
				if (!isContainerInitialized)
				{
					currentContainer = childContainer.Clone();
					isContainerInitialized = true;
				}
				else
				{
					currentContainer.Add(childContainer, level);
				}
			}

			if (currentContainer.Columns.Count > 0)
				data.Add(id, currentContainer);
		}

		private void SetEditedValue(TreeListNode node, int rowId, int colId, double coefficient, double value)
		{
			int id = Convert.ToInt32(node.GetValue(treeColId));
			int level = ConvertEx.ToInt(node.GetValue(treeColLevel));

			RatesContainer changedContainer = data[id];
			RatesRow changedRow = changedContainer.Rows[rowId];
			RateCell changedCell = changedContainer.Rows[rowId].Cells[colId];
			if (coefficient < 0)
				changedCell.Value = Math.Round(Convert.ToDecimal(value), 3);
			else
				changedCell.Value = Math.Round(Convert.ToDecimal(Convert.ToDouble((decimal) changedCell.Value)*coefficient), 3);

			changedRow.UpdateSummaryItem(changedCell);

			int nodesCount = GetChildNodesCount(node);

			double newValue = (changedRow.MaxLevelForSum == -1 || level <= changedRow.MaxLevelForSum)
			                  	? value/nodesCount
			                  	: value;
			foreach (TreeListNode childNode in node.Nodes)
				SetEditedValue(childNode, rowId, colId, coefficient, newValue);
		}

		private int GetChildNodesCount(TreeListNode node)
		{
			int nodesCount = 0;
			foreach (TreeListNode childNode in node.Nodes)
				if (data.ContainsKey(Convert.ToInt32(childNode.GetValue(treeColId))))
					nodesCount++;
			return nodesCount;
		}

		private void ResizeGrid()
		{
			if (gridControl.Width > minGridWidth)
				bandedGridView.OptionsView.ColumnAutoWidth = true;
			else
			{
				bandedGridView.OptionsView.ColumnAutoWidth = false;
				foreach (GridColumn column in bandedGridView.Columns)
				{
					column.Width = column.GetBestWidth();
				}
			}
		}

		private void treeList_AfterFocusNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
		{
			int id = Convert.ToInt32(e.Node.GetValue(treeColId));
			if (!data.ContainsKey(id))
			{
				CalculateParentNode(e.Node);
			}
			if (data.ContainsKey(id) && data[id] != null)
				SetRatesDataSource(id);
			else
				gridControl.DataSource = null;
		}

		private void gridView_ShowingEditor(object sender, CancelEventArgs e)
		{
			int rowIndex = bandedGridView.GetDataSourceRowIndex(bandedGridView.FocusedRowHandle);
			string colName = bandedGridView.FocusedColumn.FieldName;
			int id = Convert.ToInt32(treeList.FocusedNode.GetValue(treeColId));
			RateCell cell = data[id].Rows[rowIndex].Cells[data[id].Columns.FindIndex(item => item.Equals(colName))];
			e.Cancel =
				!cell.Editable
				|| DisabledCellsByType.IndexOf(cell.PlanOrFact) > -1;
		}

		private void gridView_CellValueChanged(object sender, CellValueChangedEventArgs e)
		{
			double newValue = Convert.ToDouble(e.Value.ToString());

			TreeListNode currentNode = treeList.FocusedNode;
			int currentId = Convert.ToInt32(currentNode.GetValue(treeColId));
			int currentLevel = Convert.ToInt32(currentNode.GetValue(treeColLevel));
			DiscardDataInNodeAndParents(currentNode.ParentNode);

			int rowIndex = bandedGridView.GetDataSourceRowIndex(e.RowHandle);
			int colIndex = e.Column.AbsoluteIndex;

			List<int> childIds = new List<int>();
			foreach (TreeListNode childNode in currentNode.Nodes)
			{
				childIds.Add(Convert.ToInt32(childNode.GetValue(treeColId)));
			}
			decimal prevValue =
				(
					from d in data.AsQueryable()
					where childIds.IndexOf(d.Key) != -1
					select ConvertEx.ToDecimal(d.Value.Rows[rowIndex].Cells[colIndex].Value)
				).Sum();

			RatesRow currentRow = data[currentId].Rows[rowIndex];
			if (currentRow.MaxLevelForSum == -1 || currentLevel <= currentRow.MaxLevelForSum)
			{
				SetEditedValue
					(
						currentNode,
						rowIndex,
						colIndex,
						(prevValue > 0) ? newValue/Convert.ToDouble(prevValue) : -1,
						newValue
					);

				DiscardDataInNodeAndChilds(currentNode);
			}
			else
			{
				TreeListNode topNode = currentNode;
				while (currentLevel > currentRow.MaxLevelForSum)
				{
					topNode = topNode.ParentNode;
					currentLevel = Convert.ToInt32(topNode.GetValue(treeColLevel));
				}
				CalculateParentNode(topNode);
				int childsCount = GetChildNodesCount(topNode);
				SetEditedValue
					(
						topNode,
						rowIndex,
						colIndex,
						-1,
						newValue*childsCount
					);

				DiscardDataInNodeAndChilds(topNode);
			}

			CalculateParentNode(currentNode);
			gridControl.DataSource = data[currentId].Table;
		}

		private void gridView_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
		{
			int rowIndex = bandedGridView.GetDataSourceRowIndex(e.RowHandle);
			string colName = e.Column.FieldName;
			int id = Convert.ToInt32(treeList.FocusedNode.GetValue(treeColId));
			int columnIndex = data[id].Columns.FindIndex(item => item.Equals(colName));
			RateCell cell = data[id].Rows[rowIndex].Cells[columnIndex];

			if ((!IsEditable || !cell.Editable || DisabledCellsByType.IndexOf(cell.PlanOrFact) > -1) && ((BandedGridColumn)e.Column).OwnerBand.Fixed == FixedStyle.None)
			{
				e.Graphics.FillRectangle(Constants.GrayBrush, e.Bounds);
			}

			if (cell.IsSummaryCell && !cell.IsValid)
			{
				e.Appearance.ForeColor = Color.Red;
			}

			if (Drawers != null)
			{
				int colIndex = e.Column.AbsoluteIndex;
				RateCell cellToDraw = data[Convert.ToInt32(treeList.FocusedNode.GetValue(treeColId))]
					.Rows[bandedGridView.GetDataSourceRowIndex(e.RowHandle)]
					.Cells[colIndex];
				Drawer dw = Drawers.GetDrawer(
					cellToDraw.Drawer);
				if (dw != null)
					e.Handled = dw(sender, e, cellToDraw.Value);
			}
		}

		private void TreeRates_Resize(object sender, EventArgs e)
		{
			
		}
	}
}