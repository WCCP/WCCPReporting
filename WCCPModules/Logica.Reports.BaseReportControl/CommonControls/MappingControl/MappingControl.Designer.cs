﻿namespace Logica.Reports.BaseReportControl.CommonControls.MappingControl
{
    partial class MappingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridMapp = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleMapp = new DevExpress.XtraEditors.SimpleButton();
            this.simpleCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleOK = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.reportFields = new DevExpress.XtraEditors.LabelControl();
            this.listAims = new DevExpress.XtraEditors.ListBoxControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.fileFields = new DevExpress.XtraEditors.LabelControl();
            this.listFileAims = new DevExpress.XtraEditors.ListBoxControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridMapp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listFileAims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // gridMapp
            // 
            this.gridMapp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gridMapp.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridMapp.Location = new System.Drawing.Point(0, 210);
            this.gridMapp.MainView = this.gridView1;
            this.gridMapp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridMapp.Name = "gridMapp";
            this.gridMapp.Size = new System.Drawing.Size(608, 65);
            this.gridMapp.TabIndex = 8;
            this.gridMapp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ActiveFilterEnabled = false;
            this.gridView1.BestFitMaxRowCount = 1;
            this.gridView1.GridControl = this.gridMapp;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl3);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(608, 210);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.simpleMapp);
            this.panelControl3.Controls.Add(this.simpleCancel);
            this.panelControl3.Controls.Add(this.simpleOK);
            this.panelControl3.Location = new System.Drawing.Point(236, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(136, 206);
            this.panelControl3.TabIndex = 5;
            // 
            // simpleMapp
            // 
            this.simpleMapp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.simpleMapp.Location = new System.Drawing.Point(16, 41);
            this.simpleMapp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleMapp.Name = "simpleMapp";
            this.simpleMapp.Size = new System.Drawing.Size(100, 22);
            this.simpleMapp.TabIndex = 0;
            this.simpleMapp.Text = "Связать";
            this.simpleMapp.Click += new System.EventHandler(this.simpleMapp_Click);
            // 
            // simpleCancel
            // 
            this.simpleCancel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.simpleCancel.Location = new System.Drawing.Point(16, 76);
            this.simpleCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleCancel.Name = "simpleCancel";
            this.simpleCancel.Size = new System.Drawing.Size(100, 22);
            this.simpleCancel.TabIndex = 6;
            this.simpleCancel.Text = "Отменить";
            this.simpleCancel.Click += new System.EventHandler(this.simpleCancel_Click);
            // 
            // simpleOK
            // 
            this.simpleOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleOK.Location = new System.Drawing.Point(16, 156);
            this.simpleOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleOK.Name = "simpleOK";
            this.simpleOK.Size = new System.Drawing.Size(100, 22);
            this.simpleOK.TabIndex = 7;
            this.simpleOK.Text = "OK";
            this.simpleOK.Click += new System.EventHandler(this.simpleOk_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.reportFields);
            this.panelControl2.Controls.Add(this.listAims);
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Padding = new System.Windows.Forms.Padding(0, 20, 0, 10);
            this.panelControl2.Size = new System.Drawing.Size(230, 206);
            this.panelControl2.TabIndex = 0;
            // 
            // reportFields
            // 
            this.reportFields.Location = new System.Drawing.Point(3, 2);
            this.reportFields.Name = "reportFields";
            this.reportFields.Size = new System.Drawing.Size(62, 13);
            this.reportFields.TabIndex = 5;
            this.reportFields.Text = "Поля МАСО:";
            // 
            // listAims
            // 
            this.listAims.DisplayMember = "Text";
            this.listAims.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listAims.HorizontalScrollbar = true;
            this.listAims.Location = new System.Drawing.Point(0, 20);
            this.listAims.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listAims.Name = "listAims";
            this.listAims.Size = new System.Drawing.Size(230, 176);
            this.listAims.TabIndex = 2;
            this.listAims.ValueMember = "Id";
            this.listAims.SelectedIndexChanged += new System.EventHandler(this.list_SelectedIndexChanged);
            this.listAims.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.list_DrawItem);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.fileFields);
            this.panelControl1.Controls.Add(this.listFileAims);
            this.panelControl1.Location = new System.Drawing.Point(376, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(0, 20, 0, 10);
            this.panelControl1.Size = new System.Drawing.Size(230, 206);
            this.panelControl1.TabIndex = 4;
            // 
            // fileFields
            // 
            this.fileFields.Location = new System.Drawing.Point(3, 2);
            this.fileFields.Name = "fileFields";
            this.fileFields.Size = new System.Drawing.Size(78, 13);
            this.fileFields.TabIndex = 4;
            this.fileFields.Text = "Поля из файла:";
            // 
            // listFileAims
            // 
            this.listFileAims.DisplayMember = "Text";
            this.listFileAims.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listFileAims.HorizontalScrollbar = true;
            this.listFileAims.Location = new System.Drawing.Point(0, 20);
            this.listFileAims.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listFileAims.Name = "listFileAims";
            this.listFileAims.Size = new System.Drawing.Size(230, 176);
            this.listFileAims.TabIndex = 1;
            this.listFileAims.ValueMember = "Id";
            this.listFileAims.SelectedIndexChanged += new System.EventHandler(this.list_SelectedIndexChanged);
            this.listFileAims.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.list_DrawItem);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(608, 210);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(374, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(234, 210);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panelControl2;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(234, 210);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.panelControl3;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(234, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(140, 210);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // MappingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.gridMapp);
            this.Name = "MappingControl";
            this.Size = new System.Drawing.Size(608, 275);
            ((System.ComponentModel.ISupportInitialize)(this.gridMapp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listFileAims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridMapp;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleMapp;
        private DevExpress.XtraEditors.SimpleButton simpleCancel;
        private DevExpress.XtraEditors.SimpleButton simpleOK;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl reportFields;
        private DevExpress.XtraEditors.ListBoxControl listAims;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl fileFields;
        private DevExpress.XtraEditors.ListBoxControl listFileAims;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}
