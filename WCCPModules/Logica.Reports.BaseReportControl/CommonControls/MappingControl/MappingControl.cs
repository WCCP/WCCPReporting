﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;

namespace Logica.Reports.BaseReportControl.CommonControls.MappingControl
{
    /// <summary>
    /// Describes mapping control
    /// </summary>
    public partial class MappingControl : XtraUserControl
    {
        #region Fields
        /// <summary>
        /// List of items to be mapped from file
        /// </summary>
        private List<ComboBoxItem> fileList;

        /// <summary>
        /// List of items to be mapped from report
        /// </summary>
        private List<ComboBoxItem> reportList;

        /// <summary>
        /// Mapping key array
        /// </summary>
        private int[] mapping;

        #endregion Fields

        #region Events
        /// <summary>
        /// Occurs on OK button click.
        /// </summary>
        public event EventHandler OKBtnClick;

        #endregion Events

        #region Constructors
        /// <summary>
        /// Initializes the new instance of MappingControl object
        /// </summary>
        public MappingControl()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the mapping key array
        /// </summary>
        public int[] Mapping
        {
            get
            {
                return mapping;
            }
        }

        /// <summary>
        /// Gets or sets report list Caption
        /// </summary>
        public string ReportFieldsCaption
        {
            get
            {
                return reportFields.Text;
            }

            set
            {
                reportFields.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets file list Caption
        /// </summary>
        public string FileFieldsCaption
        {
            get
            {
                return fileFields.Text;
            }

            set
            {
                fileFields.Text = value;
            }
        }

        #endregion Properties

        #region Methods
        /// <summary>
        /// Loads data
        /// </summary>
        /// <param name="ReportList">List of fields from report.</param>
        /// <param name="FileList">List of fields from file.</param>
        public void LoadData(List<ComboBoxItem> ReportList, List<ComboBoxItem> FileList)
        {
            fileList = FileList;
            reportList = ReportList;

            mapping = new int[reportList.Count];

            for (int i = 0; i < mapping.Length; i++)
            {
                mapping[i] = fileList.FindIndex(a => a.Text == reportList[i].Text);
            }

            listFileAims.DataSource = fileList;
            listAims.DataSource = reportList;

            InitGrid();

            RefreshGrid();
        }

        /// <summary>
        /// Initializes grid structure 
        /// </summary>
        private void InitGrid()
        {
            DataTable dt = new DataTable();

            foreach (ComboBoxItem item in reportList)
            {
                dt.Columns.Add(item.Text);
            }

            object[] row = new object[reportList.Count];
            dt.Rows.Add(row);

            gridMapp.BeginUpdate();

            gridMapp.DataSource = dt;

            gridMapp.EndUpdate();
        }

        /// <summary>
        /// Updates grid
        /// </summary>
        private void RefreshGrid()
        {
            gridMapp.BeginUpdate();

            DataRow row = (gridMapp.DataSource as DataTable).Rows[0];
            for (int i = 0; i < mapping.Length; i++)
            {
                row[i] = mapping[i] == -1 ? null : fileList[mapping[i]].Text;

                GridColumn col = gridView1.Columns[i];
                int width = reportList[i].Text.Length * 7 + 20;
                col.Width = Math.Max(80, width);
            }

            gridMapp.EndUpdate();
        }

        /// <summary>
        /// Checks buttons access
        /// </summary>
        private void RedrawButtonAccess()
        {
            simpleMapp.Enabled = listAims.SelectedIndex > -1 && listFileAims.SelectedIndex > -1 && mapping[listAims.SelectedIndex] == -1 && Array.IndexOf(mapping, listFileAims.SelectedIndex) == -1;
            simpleCancel.Enabled = listAims.SelectedIndex > -1 && listFileAims.SelectedIndex > -1 && mapping[listAims.SelectedIndex] != -1 && Array.IndexOf(mapping, listFileAims.SelectedIndex) != -1;
            simpleOK.Enabled = Array.IndexOf(mapping, -1) == -1;
        }

        /// <summary>
        /// Mappes two fields
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void simpleMapp_Click(object sender, EventArgs e)
        {
            if (listAims.SelectedIndex >= 0 && listFileAims.SelectedIndex >= 0)
            {
                mapping[listAims.SelectedIndex] = listFileAims.SelectedIndex;

                RefreshGrid();
                RedrawButtonAccess();
                listAims.Refresh();
                listFileAims.Refresh();
            }
        }

        /// <summary>
        /// Cancels mapping
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void simpleCancel_Click(object sender, EventArgs e)
        {
            if (listAims.SelectedIndex >= 0 && listFileAims.SelectedIndex >= 0)
            {
                mapping[listAims.SelectedIndex] = -1;

                RefreshGrid();
                RedrawButtonAccess();
                listAims.Refresh();
                listFileAims.Refresh();
            }
        }

        /// <summary>
        /// Handles Click event of simpleOk
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void simpleOk_Click(object sender, EventArgs e)
        {
            if (OKBtnClick != null)
            {
                OKBtnClick(this, new EventArgs());
            }
        }

        /// <summary>
        /// Handles DrawItem event of lists
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void list_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            if (sender.Equals(listAims) && mapping[e.Index] != -1)
            {
                e.Appearance.ForeColor = Color.Gray;
            }
            else
                if (sender.Equals(listFileAims) && Array.IndexOf(mapping, e.Index) != -1)
                {
                    e.Appearance.ForeColor = Color.Gray;
                }
        }

        /// <summary>
        /// Handles SelectedIndexChanged event of lists
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event args.</param>
        private void list_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender.Equals(listAims) && mapping[listAims.SelectedIndex] != -1)
            {
                listFileAims.SetSelected(mapping[listAims.SelectedIndex], true);
            }
            else
                if (sender.Equals(listFileAims) && Array.IndexOf(mapping, listFileAims.SelectedIndex) != -1)
                {
                    listAims.SetSelected(Array.IndexOf(mapping, listFileAims.SelectedIndex), true);
                }

            RedrawButtonAccess();
        }

        #endregion Methods
    }
}
