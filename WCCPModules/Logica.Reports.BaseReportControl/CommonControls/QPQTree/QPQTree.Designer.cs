﻿using System.CodeDom;

namespace Logica.Reports.BaseReportControl.CommonControls.QPQTree
{
    partial class QPQTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeQPQ = new DevExpress.XtraTreeList.TreeList();
            this.colQPQId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colQPQParentID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colQPQName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colQPQLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colQPQQuestionType = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colQPQChecked = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.treeQPQ)).BeginInit();
            this.SuspendLayout();
            // 
            // treeQPQ
            // 
            this.treeQPQ.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.treeQPQ.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colQPQId,
            this.colQPQParentID,
            this.colQPQName,
            this.colQPQLevel,
            this.colQPQQuestionType,
            this.colQPQChecked});
            this.treeQPQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeQPQ.Location = new System.Drawing.Point(0, 0);
            this.treeQPQ.Margin = new System.Windows.Forms.Padding(0);
            this.treeQPQ.Name = "treeQPQ";
            this.treeQPQ.OptionsView.ShowCheckBoxes = true;
            this.treeQPQ.OptionsView.ShowColumns = false;
            this.treeQPQ.OptionsView.ShowHorzLines = false;
            this.treeQPQ.OptionsView.ShowIndicator = false;
            this.treeQPQ.Size = new System.Drawing.Size(482, 160);
            this.treeQPQ.TabIndex = 0;
            this.treeQPQ.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeQPQ_BeforeCheckNode);
            this.treeQPQ.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeQPQ_AfterCheckNode);
            // 
            // colQPQId
            // 
            this.colQPQId.Caption = "ID";
            this.colQPQId.FieldName = "ID";
            this.colQPQId.Name = "colQPQId";
            // 
            // colQPQParentID
            // 
            this.colQPQParentID.Caption = "Parent ID";
            this.colQPQParentID.FieldName = "ParentID";
            this.colQPQParentID.Name = "colQPQParentID";
            // 
            // colQPQName
            // 
            this.colQPQName.AppearanceHeader.Options.UseTextOptions = true;
            this.colQPQName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQPQName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colQPQName.Caption = "Name";
            this.colQPQName.FieldName = "Name";
            this.colQPQName.MinWidth = 32;
            this.colQPQName.Name = "colQPQName";
            this.colQPQName.OptionsColumn.AllowEdit = false;
            this.colQPQName.OptionsColumn.ReadOnly = true;
            this.colQPQName.Visible = true;
            this.colQPQName.VisibleIndex = 0;
            this.colQPQName.Width = 250;
            // 
            // colQPQLevel
            // 
            this.colQPQLevel.Caption = "Level";
            this.colQPQLevel.FieldName = "Level";
            this.colQPQLevel.Name = "colQPQLevel";
            // 
            // colQPQQuestionType
            // 
            this.colQPQQuestionType.Caption = "QuestionType";
            this.colQPQQuestionType.FieldName = "QuestionType";
            this.colQPQQuestionType.Name = "colQPQQuestionType";
            // 
            // colQPQChecked
            // 
            this.colQPQChecked.Caption = "Checked";
            this.colQPQChecked.FieldName = "Checked";
            this.colQPQChecked.Name = "colQPQChecked";
            // 
            // QPQTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeQPQ);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MPTree";
            this.Size = new System.Drawing.Size(482, 160);
            ((System.ComponentModel.ISupportInitialize)(this.treeQPQ)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeQPQ;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colQPQId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colQPQParentID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colQPQName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colQPQLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colQPQQuestionType;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colQPQChecked;
    }
}
