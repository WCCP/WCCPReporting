﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls.QPQTree
{
    public partial class QPQTree : XtraUserControl, IControlReadOnly
    {
        public QPQTree()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets a value indicating whether nodes could be edited.
        /// </summary>
        /// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Gets Id list with comma separator
        /// </summary>
        public string SelectedStringIdList { get; set; }

        /// <summary>
        /// Gets the selected QPQ id list.
        /// </summary>
        /// <value>The selected QPQ id list.</value>
        public SqlXml SelectedXmlIdList
        {
            get
            {
                treeQPQ.PostEditor();

                DataTable lDataTable = treeQPQ.DataSource as DataTable;

                if (lDataTable != null)
                {
                    lDataTable.AcceptChanges();

                    DataView lDataView = lDataTable.Copy().DefaultView;

                    if (!string.IsNullOrEmpty(SelectedStringIdList))
                    {
                        lDataView.RowFilter = string.Format("{0} IN (Convert('{1}','System.Guid'))", colQPQId.FieldName, SelectedStringIdList);
                        return GetQPQTreeXmlSnapshot(lDataView.ToTable());
                    }
                }

                return GetQPQTreeXmlSnapshot(null);
            }
        }

        /// <summary>
        /// Gets DataSource with posted changes
        /// </summary>
        /// <value>The DataSource</value>
        public DataTable DataSourcePosted
        {
            get
            {
                treeQPQ.PostEditor();
                UpdateTableNodesCheckStateOperation lNodesOperation = new UpdateTableNodesCheckStateOperation(colQPQChecked);
                treeQPQ.NodesIterator.DoOperation(lNodesOperation);

                DataTable lDataTable = treeQPQ.DataSource as DataTable;

                if (lDataTable != null)
                {
                    lDataTable.AcceptChanges();
                }

                return lDataTable;
            }
        }

        /// <summary>
        /// Loads the QPQ tree.
        /// </summary>
        /// <value>The DataSource</value>
        public void LoadQPQTree(DataTable dataTable)
        {
            treeQPQ.BeginUpdate();

            treeQPQ.DataSource = dataTable;
            treeQPQ.ExpandAll();
            LoadQPQTreeState();
            treeQPQ.CollapseAll();

            treeQPQ.EndUpdate();
            treeQPQ.NodesIterator.DoOperation(new ExpandNodesOperation());
        }

        /// <summary>
        /// Allows to check only one node
        /// </summary>
        /// <param name="node">current node</param>
        /// <param name="nodeId">Id</param>
        private static void OnNodeCheck_Unchecking(TreeListNode node, int nodeId)
        {
            if (node != null)
            {
                node.Checked = node.Id == nodeId && node.Level != 0;

                foreach (TreeListNode lChildNode in node.Nodes)
                {
                    OnNodeCheck_Unchecking(lChildNode, nodeId);
                }
            }
        }

        /// <summary>
        /// Gets the QPQ tree XML snapshot.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns>xml that describes the QPQTree</returns>
        private SqlXml GetQPQTreeXmlSnapshot(DataTable dataTable)
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement("root");
                if (dataTable != null)
                {
                    foreach (DataRow lRow in dataTable.Rows)
                    {
                        lWriter.WriteStartElement("row");
                        AddFullEndXMLElement(lWriter, colQPQId.FieldName, lRow[colQPQId.FieldName]);
                        lWriter.WriteEndElement();
                    }
                }

                lWriter.WriteFullEndElement();
                lWriter.Flush();
                lStream.Position = 0;

                return new SqlXml(lStream);
            }
        }

        /// <summary>
        /// Loads the state of the QPQ tree.
        /// </summary>
        private void LoadQPQTreeState()
        {
            treeQPQ.NodesIterator.DoOperation(new UpdateNodesCheckStateOperation(colQPQChecked));
            treeQPQ.NodesIterator.DoOperation(new ExpandNodesOperation());
        }

        /// <summary>
        /// Adds the full end XML node.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="nodeName">Name of the node.</param>
        /// <param name="nodeValue">The node value.</param>
        private static void AddFullEndXMLElement(XmlWriter writer, string nodeName, object nodeValue)
        {
            writer.WriteStartElement(nodeName);
            if (nodeValue != null && nodeValue != DBNull.Value)
            {
                Guid lValue = ConvertEx.ToGuid(nodeValue);
                writer.WriteString(lValue.ToString());
            }

            writer.WriteEndElement();
        }

        private void treeQPQ_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            e.CanCheck = !ReadOnly && !e.Node.HasChildren;
        }

        private void treeQPQ_AfterCheckNode(object sender, NodeEventArgs e)
        {
            if (e.Node.CheckState != CheckState.Checked)
            {
                SelectedStringIdList = string.Empty;
                return;
            }
            TreeList tl = sender as TreeList;

            foreach (TreeListNode node in tl.Nodes)
            {
                OnNodeCheck_Unchecking(node, e.Node.Id);
            }
            SelectedStringIdList = string.Empty;
            SelectedStringIdList = e.Node.GetValue("ID").ToString();
            treeQPQ.SetFocusedNode(e.Node);
        }
    }
}
