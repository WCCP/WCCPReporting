﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace Logica.Reports.BaseReportControl.Utility
{
    public static class SettingsAccessor
    {
        //private static int reportID { get; set; }
   // { return Logica.Reports.Common.WCCPAPI.GetReportId(); } }
      private static string sql_result = getData();
      private static XmlDocument xml_doc = LoadXML();
      private static StringWriter sw = new StringWriter();
      
      public static void SaveSettings(IDictionary<string, string> input_params, int reportID)
        {
            sw = new StringWriter();
            //XmlTextWriter xw = new XmlTextWriter(sw);
            XmlElement elmRoot = xml_doc.DocumentElement;
            XmlElement Report_Settings = null;
            if (DoesntHaveReportProperties(reportID))
            {
                Report_Settings = xml_doc.CreateElement(String.Format("Report_{0}", reportID.ToString()));
                elmRoot.AppendChild(Report_Settings);
            }
            else
            {
                XmlNodeList reports_lst = xml_doc.GetElementsByTagName("Reports");
                foreach (XmlNode node in reports_lst)
                {
                   if (String.Compare(node.Name, String.Format("Report_{0}", reportID.ToString()), false) == 0)
                   {
                       Report_Settings = (XmlElement)node;
                       break;
                   }
                }
             }
             foreach (KeyValuePair<string, string> kvp in input_params)
             {
                if (!Update_XML(kvp.Key, kvp.Value, reportID))
                {
                    XmlElement param = xml_doc.CreateElement(kvp.Key);
                    Report_Settings.AppendChild(param);
                    XmlText txt_param = xml_doc.CreateTextNode(kvp.Value);
                    Report_Settings.LastChild.AppendChild(txt_param);
                }
            }
            xml_doc.Save(sw);
            SaveData(sw.ToString());
        }

      private static bool Update_XML(string param_name, string param_value, int reportID)
        {
            bool update = false;
            foreach (XmlNode node_param in routine(xml_doc, reportID.ToString()))
            {
                if (String.Compare(node_param.Name, param_name, false) == 0 && String.Compare(node_param.InnerText, reportID.ToString(), false) != 0)
                {
                    node_param.InnerText = param_value;
                    update = true;
                    break;
                }
            }
            return update;
        }

      public static IDictionary<string, string> GetSettings(int reportID)
        {
            IDictionary<string, string> dict = new Dictionary<string, string>();
            if (!DoesntHaveReportProperties(reportID))
            {
                foreach (XmlNode node_param in routine(xml_doc, reportID.ToString())) //lstChildren)
                {
                    dict.Add(node_param.Name, node_param.InnerText);
                }
            } 
            return dict;
        }

        public static string DeleteReportsSettings(string report_ID)
        {
            StringWriter sw1 = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw1);
            XmlNodeList reports_lst = xml_doc.GetElementsByTagName("Reports");
            foreach (XmlNode node in reports_lst)
            {
                XmlNodeList lstChildren = node.ChildNodes;
                foreach (XmlNode node_param in lstChildren)
                {
                    if (String.Compare(node_param.Name, String.Format("Report_{0}", report_ID), false) == 0)
                        node.RemoveChild(node_param);
                }
            }
            xml_doc.Save(sw1);
            return sw1.ToString();
        }

        private static XmlNodeList routine(XmlDocument xml_doc, string report_ID)
        {
            XmlNodeList lstChildren = null;
            XmlNodeList main_lst = xml_doc.GetElementsByTagName(String.Format("Report_{0}", report_ID));
            foreach (XmlNode node in main_lst)
            {
               lstChildren = node.ChildNodes;
            }
            return lstChildren;
        }

        private static XmlDocument LoadXML()
        {
            XmlDocument xml_document = new XmlDocument();
            if (sql_result.Equals("") || sql_result == null)
            {
                xml_document.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Reports></Reports>");
            }
            else
            {
                try
                {
                    xml_document.LoadXml(sql_result);
                }
                catch (XmlException ex)
                {
                    ErrorManager.ShowErrorBox(ex.Message);
                }
            }
            return xml_document;
        }

        private static string getData()
        {
            string output = "";
            DataTable dt = new DataTable();
            try
            {
                DataAccessLayer.OpenConnection();
                dt = DataAccessLayer.ExecuteStoredProcedure("spDW_UserProfileget").Tables[0];
            }
            catch (SqlException ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
            finally
            {
                DataAccessLayer.CloseConnection();
            }
            foreach(DataRow dr in dt.Rows)
            {
                output = dr["XML"].ToString();
                break;
            }
            return output;
        }

        private static void SaveData(string input_XML)
        {
            try
            {
                DataAccessLayer.OpenConnection();
                DataAccessLayer.ExecuteStoredProcedure("spDW_UserProfilePut", new SqlParameter("@XML", input_XML));
            }
            catch (SqlException ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
            finally
            {
                DataAccessLayer.CloseConnection();
            }
        }

        private static bool DoesntHaveReportProperties(int reportID)
        {
            return ((xml_doc.GetElementsByTagName(String.Format("Report_{0}", reportID))).Count == 0);
        }
    }
}
