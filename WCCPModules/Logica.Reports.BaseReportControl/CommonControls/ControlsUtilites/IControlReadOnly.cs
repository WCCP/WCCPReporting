﻿using System;

namespace Logica.Reports.BaseReportControl.Utility
{
    public interface IControlReadOnly
    {
        bool ReadOnly { get; set; }
    }
}
