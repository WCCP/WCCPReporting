﻿using System.Collections.Generic;
using DevExpress.XtraGrid.Views.BandedGrid;
using Logica.Reports.ConfigXmlParser.GridExtension;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.XtraPrintingLinks;

namespace Logica.Reports.BaseReportControl.Utility
{
    public class GridOperationHelper
    {
        private static Dictionary<string, string> ListOfFixedParsedParams = new Dictionary<string, string>()
                    { 
                        {"&gt;", ">" }, 
                        {"gt;" , ">" },
                        {"&lt;", "<" },
                        {"lt;" , "<" },
                        {"&amp;", ""  },
                        {"amp;",  ""  }
                    };
        public static Dictionary<List<Control>, List<string>> NonPrintableTables = null; 

        // Fix the issue with bad Xml Parsing
        public static void ColumnCaptionFix(GroupBandedView DefaultView)
        {
            if(ListOfFixedParsedParams!= null)
                foreach (BandedGridColumn clmn in DefaultView.Columns)
                {
                    foreach (KeyValuePair<string, string> kvp in ListOfFixedParsedParams)
                    {
                        while (clmn.Caption.Contains(kvp.Key))
                            clmn.Caption = clmn.Caption.Replace(kvp.Key, kvp.Value);
                    }
                }
        }

        internal static DevExpress.XtraPrintingLinks.CompositeLink GetCompositeLink(DevExpress.XtraPrintingLinks.CompositeLink pl, ExportToType exportType)
        {
            if (GridOperationHelper.NonPrintableTables != null)
            {
                foreach (KeyValuePair<List<Control>, List<string>> kvp in GridOperationHelper.NonPrintableTables)
                {
                    if (kvp.Value.Contains(exportType.ToString().ToLower()))
                    {

                        for (int i = kvp.Key.Count; i > 0; --i)
                        {
                            if (kvp.Key[i - 1] is LabelControl)
                                pl.Links.Add(new RichTextBoxLink { RichTextBox = new RichTextBox { Text = (kvp.Key[i - 1] as LabelControl).Text, Font = (kvp.Key[i - 1] as LabelControl).Font } });
                            else
                                pl.Links.Add(new DevExpress.XtraPrinting.PrintableComponentLink() { Component = (kvp.Key[i - 1] as DevExpress.XtraPrinting.IPrintable) });
                        }
                    }
                }
            }
            return pl;
        }
    }
}
