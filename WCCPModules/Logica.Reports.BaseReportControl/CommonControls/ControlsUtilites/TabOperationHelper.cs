﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using DevExpress.XtraPrintingLinks;
using System.Windows.Forms;
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System.Drawing;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.Utils;
using System.Data;

namespace Logica.Reports.BaseReportControl.Utility
{
    public static class TabOperationHelper
    {
        public static CompositeLink PrepareCompositeLink(ExportToType exportType, string tabHeader, List<IPrintable> components, bool feetToOnePage)
        {
            CompositeLink pl = new CompositeLink(new PrintingSystem());

            pl.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = "" } });
            pl.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = tabHeader } });

            //pl.Links.Add(new PrintableComponentLink() { Component = components[0] }); remove this because components[0] is the last component on screen
            //pl.PrintingSystemBase.PageSettingsBase.PaperKind = PaperKind.A4Rotated;
            pl.PaperKind = PaperKind.A4Rotated;

            for (int i = 0; i < components.Count; i++)
            {
                pl.Links.Add(new PrintableComponentLink() { Component = components[i] });
                pl.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            }

            ExportFormat(ref pl);

            pl.CreateDocument();
            pl.PrintingSystemBase.Document.AutoFitToPagesWidth = 1;

            /*
            // auto select paper size for best view
            if (exportType != ExportToType.Pdf)
            {
                int pagesCount = pl.PrintingSystem.Document.Pages.Count;
                pl.PrintingSystem.PageSettingsBase.PaperKind = System.Drawing.Printing.PaperKind.A4;
                if (pagesCount < pl.PrintingSystem.Document.Pages.Count)
                    pl.PrintingSystem.PageSettingsBase.PaperKind = System.Drawing.Printing.PaperKind.A4Rotated;
            }
             */

            SetMargins(ref pl, 120);

            if ((exportType == ExportToType.Pdf) && feetToOnePage)
                FitDocumentToOnePage(ref pl);

            return pl;
        }

        /// <summary>
        /// Create settings section
        /// </summary>
        public static void CreateGridControl(XtraTabPage tab, ref GridControl headerGrid, ref GridView headerGridView)
        {
            headerGrid = new GridControl();
            headerGridView = new GridView();
            headerGrid.Dock = DockStyle.Top;
            headerGrid.MainView = headerGridView;
            headerGrid.ViewCollection.AddRange(new BaseView[] { headerGridView });
            headerGridView.Appearance.GroupRow.ForeColor = Color.Black;
            headerGridView.Appearance.GroupRow.Options.UseForeColor = true;
            headerGridView.Appearance.GroupRow.Options.UseTextOptions = true;
            headerGridView.Appearance.GroupRow.TextOptions.HAlignment = HorzAlignment.Center;
            headerGridView.AppearancePrint.GroupRow.BackColor = Color.White;
            headerGridView.AppearancePrint.GroupRow.ForeColor = Color.Black;
            headerGridView.AppearancePrint.GroupRow.Options.UseBackColor = true;
            headerGridView.AppearancePrint.GroupRow.Options.UseForeColor = true;
            headerGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            headerGridView.AppearancePrint.GroupRow.TextOptions.HAlignment = HorzAlignment.Center;
            // Header grid should be without lines in exported document!!!
            headerGridView.AppearancePrint.Lines.BackColor = Color.White;
            headerGridView.AppearancePrint.Lines.BackColor2 = Color.White;
                        
            headerGridView.FocusRectStyle = DrawFocusRectStyle.None;
            headerGridView.GridControl = headerGrid;
            headerGridView.GroupFormat = "[#image]{1} {2}";
            headerGridView.HorzScrollVisibility = ScrollVisibility.Never;
            headerGridView.OptionsBehavior.AutoExpandAllGroups = true;
            headerGridView.OptionsBehavior.Editable = false;
            headerGridView.OptionsView.ShowGroupedColumns = true;
            headerGridView.OptionsPrint.AutoWidth = true;
            headerGridView.OptionsPrint.PrintGroupFooter = false;
            headerGridView.OptionsPrint.PrintHeader = false;
            headerGridView.OptionsPrint.UsePrintStyles = true;
            headerGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            headerGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            headerGridView.OptionsView.ColumnAutoWidth = true;
            headerGridView.OptionsView.ShowColumnHeaders = false;
            headerGridView.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
            headerGridView.OptionsView.ShowGroupExpandCollapseButtons = false;
            headerGridView.OptionsView.ShowGroupPanel = false;
            headerGridView.OptionsView.ShowHorzLines = false;
            headerGridView.OptionsView.ShowIndicator = false;
            headerGridView.OptionsView.ShowVertLines = false;
            headerGridView.PaintStyleName = "UltraFlat";
            headerGridView.ScrollStyle = ScrollStyleFlags.None;
            headerGridView.VertScrollVisibility = ScrollVisibility.Never;
            headerGridView.AppearancePrint.GroupRow.Font = new Font(tab.Font.FontFamily, tab.Font.Size + 1, FontStyle.Bold);
            headerGridView.PaintAppearance.GroupRow.BackColor = headerGridView.Appearance.GroupRow.BackColor = tab.BackColor;
            headerGridView.PaintAppearance.GroupRow.TextOptions.HAlignment = headerGridView.Appearance.GroupRow.TextOptions.HAlignment = HorzAlignment.Center;
            headerGridView.PaintAppearance.GroupRow.Font = headerGridView.Appearance.GroupRow.Font = new Font(tab.Font.FontFamily, tab.Font.Size + 2, FontStyle.Bold);
            headerGridView.PaintAppearance.GroupRow.ForeColor = headerGridView.Appearance.GroupRow.ForeColor = Color.Black;
            if (null != tab.Parent && null != tab.Parent.Parent)
            {
                headerGridView.Appearance.Empty.BackColor = tab.Parent.Parent.BackColor;
                headerGridView.Appearance.Row.BackColor = tab.Parent.Parent.BackColor;
            }

            headerGridView.PaintAppearance.GroupRow.BackColor =
                headerGridView.Appearance.GroupRow.BackColor = tab.BackColor;
            headerGridView.PaintAppearance.GroupRow.TextOptions.HAlignment =
                headerGridView.Appearance.GroupRow.TextOptions.HAlignment = HorzAlignment.Center;
            headerGridView.PaintAppearance.GroupRow.Font =
                headerGridView.Appearance.GroupRow.Font = new Font(tab.Font.FontFamily, tab.Font.Size + 2, FontStyle.Bold);
            headerGridView.PaintAppearance.GroupRow.ForeColor =
                headerGridView.Appearance.GroupRow.ForeColor = Color.Black;

            tab.Controls.Add(headerGrid);
        }

        public static void CreateSettingSection(XtraTabPage tab, SheetParamCollection SheetSettings,
            ref GridControl headerGrid, ref GridView headerGridView)
        {
            //TODO: check empty section
            List<SheetParam> lSheetParams = SheetSettings.FindAll(p => !String.IsNullOrEmpty(p.DisplayParamName));

            //create grid
            if (null == headerGrid)
                CreateGridControl(tab, ref headerGrid, ref headerGridView);

            DataTable lDataSource = new DataTable();

            //create ds structure
            List<object> lList = new List<object>();
            if (lSheetParams.Count <= 0)
            {
                lDataSource.Columns.Add("");
                lList.Add("");
            }
            for (int lI = 0; lI < lSheetParams.Count * 3; lI++)
            {
                lDataSource.Columns.Add("");
                lList.Add("");
            }
            lDataSource.Rows.Add(lList.ToArray());

            //fill data
            lDataSource.Rows[0][0] = SheetSettings.MainHeader;
            for (int lI = 0; lI < lSheetParams.Count; lI++)
            {
                lDataSource.Rows[0][lI * 3 + 1] = lSheetParams[lI].DisplayParamName;
                lDataSource.Rows[0][lI * 3 + 2] = lSheetParams[lI].DisplayValue;
            }
            headerGrid.DataSource = lDataSource;
            headerGrid.Height = lDataSource.Rows.Count * 19;
            if (!String.IsNullOrEmpty(SheetSettings.MainHeader))
            {
                headerGridView.Columns[0].Visible = false;
                headerGridView.Columns[0].Group();
                headerGrid.Height += 20;
            }

            for (int lI = 1; lI < headerGridView.Columns.Count - 1; lI = lI + 3)
            {
                headerGridView.Columns[lI - 1].MinWidth = 10;
                headerGridView.Columns[lI - 1].Width = 10;
                headerGridView.Columns[lI - 1].MaxWidth = 15;

                headerGridView.Columns[lI].AppearanceCell.Font = new Font(tab.Font.FontFamily, tab.Font.Size, FontStyle.Bold);
                headerGridView.Columns[lI].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            }
            headerGridView.OptionsView.ColumnAutoWidth = false;
            headerGridView.OptionsPrint.AutoWidth = true;

            headerGridView.BestFitColumns();
        }

        
        internal static void FitDocumentToOnePage(ref CompositeLink compositeLink)
        {
            compositeLink.PrintingSystem.Document.ScaleFactor = 1;
            // Пытаемся максимально быстро приблизится к заполнению только 2-х страниц
            while (compositeLink.PrintingSystem.Document.PageCount > 2)
            {
                var proportion = 1.0 / (Math.Sqrt(compositeLink.PrintingSystem.Document.PageCount) - 0.4);
                if (proportion >= 1) break; // Перестраховка от зацикливания
                
                compositeLink.PrintingSystem.Document.ScaleFactor = (float)(compositeLink.PrintingSystem.Document.ScaleFactor*proportion);
            }

            // В случае если верхний алгоритм перескочил 2 страницы и у нас получилась одна страница 
            // (которая скорее всего заполнена неполностью), то мы должны получить минимум 2 страницы увеличивая масштаб
            while (compositeLink.PrintingSystem.Document.PageCount <= 1)
            {
                compositeLink.PrintingSystem.Document.ScaleFactor += 0.1f;
            }

            // Подчистка. Плавно уменьшаем масштаб до одной страницы.
            while (compositeLink.PrintingSystem.Document.PageCount > 1)
            {
                compositeLink.PrintingSystem.Document.ScaleFactor -= 0.01f;
            }
        }

        internal static void ExportFormat(ref CompositeLink compositeLink) {
            if (null == compositeLink || null == compositeLink.Links)
                return;

            for (int i = 0; i < compositeLink.Links.Count; i++)
            {
                PrintableComponentLink lPcl = compositeLink.Links[i] as PrintableComponentLink;
                if (null != lPcl)
                {
                    lPcl.PrintingSystem = compositeLink.PrintingSystem;
                    //lPcl.PrintingSystemBase.PageSettingsBase.PaperKind = PaperKind.A4Rotated;
                    lPcl.PaperKind = PaperKind.A4Rotated;
                    lPcl.CreateDocument();
                }
                else
                {
                    //Link lLink = compositeLink.Links[i];
                    //RichTextBoxLink lLink = compositeLink.Links[i] as RichTextBoxLink;

                    LinkBase lLink = compositeLink.Links[i];
                    if (lLink == null)
                        continue;
                    lLink.PrintingSystemBase = compositeLink.PrintingSystem;
                    //lLink.PrintingSystem = compositeLink.PrintingSystem;
                    //lLink.PrintingSystemBase.PageSettingsBase.PaperKind = PaperKind.A4Rotated;
                    lLink.PaperKind = PaperKind.A4Rotated;
                    lLink.CreateDocument();
                }
            }
        }

        internal static string RemoveSpecialCharacters(string source)
        {
            string rv = source.Replace("\\", " ");
            
            rv = rv.Replace("/", " ");
            rv = rv.Replace(":", " ");
            rv = rv.Replace("*", " ");
            rv = rv.Replace("?", " ");
            rv = rv.Replace("\"", " ");
            rv = rv.Replace(">", " ");
            rv = rv.Replace("<", " ");
            rv = rv.Replace("|", " ");
            rv = rv.Replace("&", " ");
            rv = rv.Replace("'", "");
            rv = rv.Trim(' '); // Remove leading and trailing spaces.

            return rv;
        }

        public static void SetMargins(ref CompositeLink compositeLink, int marginOffset)
        {
            foreach (Page page in compositeLink.PrintingSystemBase.Pages)
            {
                page.MarginsF.Left = page.MarginsF.Right = page.MarginsF.Top = page.MarginsF.Bottom = marginOffset;
            }
        }
    }
}
