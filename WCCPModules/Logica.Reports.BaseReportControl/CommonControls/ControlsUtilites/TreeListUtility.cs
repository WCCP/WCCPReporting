﻿using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using Logica.Reports.BaseReportControl.CommonControls;

namespace Logica.Reports.BaseReportControl.Utility
{    
    /// <summary>
    /// Gets list of checked rows by level
    /// </summary>
    public class GetCheckedNodesRowsByLevel : TreeListOperation {
        private readonly List<DataRow> _resultSet = new List<DataRow>();
        private readonly TreeListColumn _columnLevel;
        private readonly int _level;

        public GetCheckedNodesRowsByLevel(TreeListColumn columnLevel, int level) {
            _columnLevel = columnLevel;
            _level = level;
        }

        public List<DataRow> Result {
            get { return _resultSet; }
        }

        public override void Execute(TreeListNode node) {
            if (null == _columnLevel || node.CheckState != CheckState.Checked || ConvertEx.ToInt(node.GetValue(_columnLevel)) != _level)
                return;
            DataTable lDataTable = node.TreeList.DataSource as DataTable;
            if (null == lDataTable)
                return;
            DataRow lRow = lDataTable.Rows[node.Id];
            if (null != lRow)
                _resultSet.Add(lRow);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetFinalCheckedNodesOperation : TreeListOperation {
        private string _selectedIdList = string.Empty;
        private readonly TreeListColumn _columnId;
        private int _count;

        public int Count {
            get { return _count; }
        }

        public GetFinalCheckedNodesOperation(TreeListColumn columnId) {
            _columnId = columnId;
        }

        public string SelectedIdList {
            get { return _selectedIdList.TrimEnd(SKUTree.Constants.DELIMITER_ID_LIST.ToCharArray()); }
        }

        public bool IsQuotesSeparated {
            get;
            set;
        }

        public bool IsKPItree {
            get;
            set;
        }

        public override void Execute(TreeListNode node) {
            if (node.CheckState == CheckState.Unchecked)
                return;
            if (!IsKPItree && (IsKPItree || node.HasChildren))
                return;
            if (IsQuotesSeparated)
                _selectedIdList = string.Concat(_selectedIdList, '\'', node.GetValue(_columnId), '\'', SKUTree.Constants.DELIMITER_ID_LIST);
            else
                _selectedIdList = string.Concat(_selectedIdList, node.GetValue(_columnId), SKUTree.Constants.DELIMITER_ID_LIST);
            _count++;
        }
    }

    /// <summary>
    /// Sets checked state for nodes where Checked column in dataset is checked.
    /// </summary>
    public class UpdateNodesCheckStateOperation : TreeListOperation {
        private readonly TreeListColumn _columnChecked;

        public UpdateNodesCheckStateOperation(TreeListColumn columnChecked) {
            _columnChecked = columnChecked;
        }

        public override void Execute(TreeListNode node) {
            node.Checked = ConvertEx.ToBool(node.GetValue(_columnChecked));
        }
    }

    /// <summary>
    /// Post changes to the DataSource
    /// </summary>
    public class UpdateTableNodesCheckStateOperation : TreeListOperation {
        private readonly TreeListColumn _columnChecked;

        public UpdateTableNodesCheckStateOperation(TreeListColumn columnChecked) {
            _columnChecked = columnChecked;
        }

        public override void Execute(TreeListNode node) {
             node.SetValue(_columnChecked, node.Checked);
        }
    }

    /// <summary>
    /// Sets checked state for node
    /// </summary>
    public class SetNodesCheckStateOperation : TreeListOperation {
        public override void Execute(TreeListNode node) {
            node.Checked = true;
        }
    }

    /// <summary>
    /// Expands nodes.
    /// </summary>
    public class ExpandNodesOperation : TreeListOperation {
        public override void Execute(TreeListNode node) {
            if (!node.Checked)
                return;
            TreeListNode lParentNode = node.ParentNode;
            while (lParentNode != null && !lParentNode.Checked) {
                lParentNode.Expanded = true;
                lParentNode = lParentNode.ParentNode;
            }
        }
    }
}
