﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using System.Data.SqlTypes;
using System.Globalization;
using System.Collections.ObjectModel;

namespace Logica.Reports.BaseReportControl.Utility
{
    /// <summary>
    /// 
    /// </summary>
    public static class ConvertEx
    {
        /// <summary>
        /// Toes the int.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static byte ToByte(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToByte(value);
            }
            return 0;
        }

        /// <summary>
        /// Toes the int.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int ToInt(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToInt32(value);
            }
            return 0;
        }

        /// <summary>
        /// Toes the long int.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static long ToLongInt(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToInt64(value);
            }
            return 0;
        }

        public static float ToFloat(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToSingle(value);
            }
            return 0;
        }

        /// <summary>
        /// Toes the decimal.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static decimal ToDecimal(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToDecimal(value);
            }
            return 0;
        }

        /// <summary>
        /// Toes the bool.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static bool ToBool(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToBoolean(value);
            }
            return false;
        }


        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                string str = Convert.ToString(value);
                if (str.Contains("\n") && !str.Contains("\r\n"))
                {
                    str = str.Replace("\n", "\r\n");
                }
                return str;
            }
            return string.Empty;
        }

        /// <summary>
        /// Toes the bytes array.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static byte[] ToBytesArray(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return value as byte[];
            }
            return null;
        }

        /// <summary>
        /// Toes the DateTime.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static DateTime ToDateTime(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }
            return DateTime.MinValue;
        }

        /// <summary>
        /// Toes the SQL XML.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="includeColumns">The include columns.</param>
        /// <returns></returns>
        public static SqlXml ToSqlXml(DataTable source, string[] includeColumns)
        {
            if (source != null && includeColumns != null)
            {
                DataSet dataSet = new DataSet("root");
                DataTable dataTable = source.Copy();
                dataTable.TableName = "row";
                for (int colIndex = dataTable.Columns.Count - 1; colIndex >= 0; colIndex--)
                {
                    string columnName = dataTable.Columns[colIndex].ColumnName;
                    if (dataTable.Columns.Contains(columnName) && !includeColumns.Contains(columnName))
                    {
                        dataTable.Columns.Remove(columnName);
                    }
                }
                dataSet.Tables.Add(dataTable);

                MemoryStream stream = new MemoryStream();
                dataTable.WriteXml(stream, XmlWriteMode.IgnoreSchema);
                stream.Position = 0;
                stream.Flush();

                return new SqlXml(stream);
            }
            return null;
        }

        /// <summary>
        /// Toes the SQL XML all.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static SqlXml ToSqlXmlAll(DataTable source)
        {
            if (source != null)
            {
                Collection<string> columns = new Collection<string>();
                foreach (DataColumn column in source.Columns)
                {
                    columns.Add(column.ColumnName);
                }
                return ToSqlXml(source, columns.ToArray<string>());
            }
            return null;
        }

        /// <summary>
        /// Toes the name of the month.
        /// </summary>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        public static string ToMonthName(int month)
        {
            while(month > 12)
            {
                month -= 12;
            }
            DateTimeFormatInfo mfi = new CultureInfo("ru-RU", false).DateTimeFormat;
            return mfi.GetMonthName(month);
        }

        public static bool IsNull(object value)
        {
            return value == null || value == DBNull.Value;
        }

        public static double ToDouble(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToDouble(value);
            }
            return 0;
        }

        public static Guid ToGuid(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return  new Guid(value.ToString());
            }
            return Guid.Empty;
        }
    }
}
