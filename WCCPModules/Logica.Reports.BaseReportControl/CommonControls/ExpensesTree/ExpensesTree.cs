using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.DataAccess;
using Logica.Reports.BaseReportControl.Utility;
using ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility;

namespace Logica.Reports.BaseReportControl.CommonControls.ExpensesTree {
    public partial class ExpensesTree : XtraUserControl {
        private readonly CalcInformation _calcInfo = new CalcInformation();
        private decimal _capexSum;
        private decimal _otherSum;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpensesTree"/> class.
        /// </summary>
        public ExpensesTree() {
            InitializeComponent();
        }

        private int ContractId { get; set; }
        public bool ReadOnly { get; set; }

        public CalcInformation CalcInfo {
            get { return _calcInfo; }
        }

        /// <summary>
        /// Gets the current expenses id list.
        /// </summary>
        /// <value>The current expenses id list.</value>
        private string CurrentExpensesIdList {
            get {
                StringBuilder lIdListBuilder = new StringBuilder();
                DataTable lDataTable = treeExpensesItemList.DataSource as DataTable;
                if (lDataTable != null)
                    foreach (DataRow lRow in lDataTable.Rows)
                        if (lRow.RowState != DataRowState.Deleted) {
                            int lRealId = ConvertEx.ToInt(lRow[columnExpensesRealId.FieldName]);
                            if (lRealId != 0) {
                                lIdListBuilder.Append(lRealId);
                                lIdListBuilder.Append(Constants.DELIMITER_ID_LIST);
                            }
                        }
                return lIdListBuilder.ToString().TrimEnd(Constants.DELIMITER_ID_LIST.ToCharArray());
            }
        }

        /// <summary>
        /// Gets the expenses summary.
        /// </summary>
        /// <value>The expenses summary.</value>
        public SqlXml ExpensesSummaryXml {
            get {
                gridViewExpensesSummary.PostEditor();
                gridViewExpensesSummary.UpdateCurrentRow();
                return ConvertEx.ToSqlXmlAll(gridControlExpensesSummary.DataSource as DataTable);
            }
        }

        /// <summary>
        /// Gets the selected expenses item tree XML.
        /// </summary>
        /// <value>The selected expenses item tree XML.</value>
        public SqlXml ExpensesList {
            get {
                treeExpensesItemList.PostEditor();

                DataTable lDataTable = treeExpensesItemList.DataSource as DataTable;

                if (lDataTable != null) {
                    lDataTable.AcceptChanges();
                    DataView lDataView = new DataView(lDataTable);

                    lDataView.RowFilter = string.Format("{0} IS NOT NULL", columnExpensesRealId.FieldName);
                    return GetExpensesItemTreeXmlSnapshot(lDataView.ToTable());
                }
                return GetExpensesItemTreeXmlSnapshot(null);
            }
        }

        /// <summary>
        /// Gets the expenses plan for fridges and krans.
        /// </summary>
        private decimal CapexSum {
            get { return _capexSum; }
        }

        private decimal OtherSum {
            get { return _otherSum; }
        }

        private void PrepareExpensesSummary() {
            treeExpensesItemList.PostEditor();
            _capexSum = _otherSum = 0;
            DataTable lDataTable = treeExpensesItemList.DataSource as DataTable;
            if (lDataTable == null || lDataTable.Rows.Count <= 0)
                return;

            foreach (DataRow lRow in lDataTable.Rows) {
                if (lRow.RowState == DataRowState.Deleted)
                    continue;

                bool lIsIncluded = ConvertEx.ToBool(lRow[columnExpensesIncluded.FieldName]);
                int lRealId = ConvertEx.ToInt(lRow[columnExpensesRealId.FieldName]);
                bool lIsCapex = ConvertEx.ToBool(lRow[columnExpensesIsCapex.FieldName]);
                if (lIsIncluded && lRealId > 0)
                    if (lIsCapex)
                        _capexSum += ConvertEx.ToDecimal(lRow[columnExpensesPlan.FieldName]);
                    else
                        _otherSum += ConvertEx.ToDecimal(lRow[columnExpensesPlan.FieldName]);
            }
        }

        /// <summary>
        /// Sets the tax advert value.
        /// </summary>
        /// <value>The tax advert value.</value>
        public decimal TaxAdvertValue {
            get { return GetSummaryExpenses(SummaryCost.SumAdvertTax, columnExpensesSumPreValue); }
        }

        /// <summary>
        /// Gets the total without taxes.
        /// </summary>
        /// <value>The total without taxes.</value>
        public decimal TotalWithoutTaxes {
            get { return GetSummaryExpenses(SummaryCost.TotalWithoutTaxes, columnExpensesSumPlan); }
        }

        /// <summary>
        /// Gets the total investments.
        /// </summary>
        /// <value>The total investments.</value>
        public decimal TotalInvestments {
            get { return GetSummaryExpenses(SummaryCost.TotalInvestments, columnExpensesSumPlan); }
        }

        /// <summary>
        /// Gets the total VAT.
        /// </summary>
        /// <value>The total VAT.</value>
        public decimal TotalVAT {
            get { return GetSummaryExpenses(SummaryCost.VAT, columnExpensesSumPlan); }
        }

        /// <summary>
        /// Gets the sum with capex.
        /// </summary>
        /// <value>The sum with capex.</value>
        public decimal SumWithCapex {
            get { return GetSummaryExpenses(SummaryCost.SumWithCAPEX, columnExpensesSumPlan); }
        }

        /// <summary>
        /// Gets the checked common expenses count.
        /// </summary>
        /// <value>The checked common expenses count.</value>
//        private int CheckedCommonExpensesCount {
//            get {
//                treeExpensesItemList.PostEditor();
//                DataTable lDataTable = treeExpensesItemList.DataSource as DataTable;
//                int lCount = 0;
//                if (lDataTable != null && lDataTable.Rows.Count > 0)
//                    foreach (DataRow lRow in lDataTable.Rows)
//                        if (lRow.RowState != DataRowState.Deleted)
//                            if (ConvertEx.ToBool(lRow[columnExpensesIncluded.FieldName]))
//                                lCount++;
//                return lCount;
//            }
//        }

        //
        // Summary:
        //     Fires immediately after the edit value has been changed.
        [Category("Events")]
        [Description("Fires immediately after the edit value has been changed.")]
        public event EventHandler EditValueChanged;

        /// <summary>
        /// Sets the calculation input.
        /// </summary>
        /// <param name="avgMaco">The avg maco.</param>
        /// <param name="avgPocVolAfterStart">The avg poc vol after start.</param>
        /// <param name="plannedPocCount">The planned poc count.</param>
        /// <param name="dateSignPlanned"></param>
        /// <param name="monthCount"></param>
        /// <param name="taxAdvert"></param>
        public void SetCalculationInput(decimal avgMaco, decimal avgPocVolAfterStart, int plannedPocCount, DateTime dateSignPlanned, int monthCount,
                                        decimal taxAdvert) {
            CalcInfo.AvgMaco = avgMaco;
            CalcInfo.AvgPocVolAfterStart = avgPocVolAfterStart;
            CalcInfo.PlannedPocCount = plannedPocCount;
            CalcInfo.DateSignPlanned = dateSignPlanned;
            CalcInfo.MonthCount = monthCount;
            CalcInfo.TaxAdvert = taxAdvert;
            RecalculateExpensesTree();
        }

        /// <summary>
        /// Loads the expenses tree.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        public void LoadExpensesTree(int contractId) {
            ContractId = contractId;
            LoadExpensesPlanItemList();
            LoadExpensesPlanSumItemList();
            if (!ReadOnly)
                RecalculateExpensesTree();
        }

        /// <summary>
        /// Loads the expenses item list.
        /// </summary>
        private void LoadExpensesPlanItemList() {
            treeExpensesItemList.BeginUpdate();
            DataTable lDataTable = DataProvider.GetExpensesPlanItemList(ContractId);
            if (lDataTable != null)
                lDataTable.PrimaryKey = new DataColumn[] {lDataTable.Columns[columnExpensesId.FieldName]};
            treeExpensesItemList.DataSource = lDataTable;
            treeExpensesItemList.ExpandAll();
            treeExpensesItemList.CollapseAll();
            treeExpensesItemList.EndUpdate();
        }

        /// <summary>
        /// Loads the expenses plan sum item list.
        /// </summary>
        private void LoadExpensesPlanSumItemList() {
            gridControlExpensesSummary.DataSource = DataProvider.GetExpensesPlanSummaryItemList(ContractId);
        }

        /// <summary>
        /// Adds the expenses.
        /// </summary>
        public void AddExpenses(int contractId) {
            if (ReadOnly)
                return;

            FormExpensesItemList lFormExpensesItemList = new FormExpensesItemList(contractId, CurrentExpensesIdList);
            if (lFormExpensesItemList.ShowDialog() != DialogResult.OK)
                return;

            string lExpensesIdList = lFormExpensesItemList.SelectedExpensesItemIdList;
            if (string.IsNullOrEmpty(lExpensesIdList))
                return;

            DataTable lDataTableDest = treeExpensesItemList.DataSource as DataTable;
            DataTable lDataTableSource = DataProvider.GetNewExpensesPlanSummaryItemList(lExpensesIdList);
            if (null == lDataTableSource || lDataTableSource.Rows.Count <= 0 || null == lDataTableDest)
                return;

            // set count 1 for all rows
            foreach (DataRow lRow in lDataTableSource.Rows) {
                int lRealId = ConvertEx.ToInt(lRow[columnExpensesRealId.FieldName]);
                if (lRealId != 0)
                    lRow[columnExpensesItemCount.FieldName] = 1;
            }

            lDataTableSource.PrimaryKey = new DataColumn[] {lDataTableSource.Columns[columnExpensesId.FieldName]};
            lDataTableDest.Merge(lDataTableSource, false, MissingSchemaAction.Ignore);

            treeExpensesItemList.ClearSorting();
            columnExpensesRealId.SortOrder = SortOrder.Descending;

            RecalculateExpensesTree();
        }

        /// <summary>
        /// Deletes the expenses.
        /// </summary>
        private void DeleteExpenses() {
            if (treeExpensesItemList.FocusedNode == null || ReadOnly)
                return;

            int lRealId = ConvertEx.ToInt(treeExpensesItemList.FocusedNode.GetValue(columnExpensesRealId));
            string lCurrentIdList = CurrentExpensesIdList;
            bool lIsSumWithoutTaxes = lCurrentIdList.Contains(((int) RootExpenses.SumWithoutTaxes).ToString());
            bool lIsRetrobonus = lCurrentIdList.Contains(((int) RootExpenses.Retrobonus).ToString());

            if (lRealId >= 0 || (lRealId < 0 && lIsSumWithoutTaxes && lIsRetrobonus)) {
                // change sum
                TreeListNode lNodeSumWithoutTaxes = treeExpensesItemList.FindNodeByFieldValue(columnExpensesRealId.FieldName,
                    (int) RootExpenses.SumWithoutTaxes);
                bool lIsIncluded = ConvertEx.ToBool((treeExpensesItemList.FocusedNode.GetValue(columnExpensesIncluded)));
                if (lNodeSumWithoutTaxes != null && lIsIncluded) {
                    decimal lSumCurrentExpenses = GetExpensesPlan(RootExpenses.SumWithoutTaxes);
                    decimal lValueOffset = ConvertEx.ToDecimal((treeExpensesItemList.FocusedNode.GetValue(columnExpensesPlan)));
                    lNodeSumWithoutTaxes.SetValue(columnExpensesPlan, lSumCurrentExpenses - lValueOffset);
                }

                // remove approppariate nodes
                if (treeExpensesItemList.FocusedNode.ParentNode != null && treeExpensesItemList.FocusedNode.ParentNode.Nodes.Count == 1)
                    // it was the last node
                    treeExpensesItemList.DeleteNode(treeExpensesItemList.FocusedNode.ParentNode);
                else
                    treeExpensesItemList.DeleteNode(treeExpensesItemList.FocusedNode);
            }

            RecalculateExpensesTree();
        }

        /// <summary>
        /// Gets the expenses item tree XML snapshot.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns></returns>
        private SqlXml GetExpensesItemTreeXmlSnapshot(DataTable dataTable) {
            return ConvertEx.ToSqlXml(dataTable, new string[] {
                columnExpensesRealId.FieldName,
                columnExpensesPlan.FieldName,
                columnExpensesItemCount.FieldName,
                columnExpensesPrice.FieldName,
                columnExpensesIncluded.FieldName
            });
        }

        /// <summary>
        /// Gets the allow contract investment expenses column edit.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private bool GetAllowContractInvestmentExpensesColumnEdit(TreeListNode node, AllowContractInvestmentExpensesColumnEdit column) {
            bool lIsAllowed = false;
            if (node != null) {
                object lObjIsUpdatable = node.GetValue(columnExpensesIsUpdatable);
                int lIsUpdateable = ConvertEx.ToInt(lObjIsUpdatable);
                decimal lPrice = ConvertEx.ToDecimal(node.GetValue(columnExpensesPrice));
                int lRealId = ConvertEx.ToInt(node.GetValue(columnExpensesRealId));
                if (lRealId != 0 && lObjIsUpdatable != null && lObjIsUpdatable != DBNull.Value)
                    switch (column) {
                        case AllowContractInvestmentExpensesColumnEdit.Plan:
                            lIsAllowed = lIsUpdateable == 1 || (lPrice == 0 && lRealId > 0);
                            break;
                        case AllowContractInvestmentExpensesColumnEdit.Quantity:
                            lIsAllowed = lIsUpdateable == 0 || lRealId == (int) RootExpenses.Retrobonus;
                            break;
                        case AllowContractInvestmentExpensesColumnEdit.IsIncluded:
                            lIsAllowed = lRealId > 0;
                            break;
                        default:
                            break;
                    }
            }
            return lIsAllowed;
        }

        /// <summary>
        /// Fires the edit value changed.
        /// </summary>
        private void FireEditValueChanged() {
            if (EditValueChanged != null)
                EditValueChanged(this, new EventArgs());
        }

        /// <summary>
        /// Recalculates the expenses tree.
        /// </summary>
        public void RecalculateExpensesTree() {
            treeExpensesItemList.BeginUpdate();

            // recalc Retrobonus
            RecalculateRetrobonus();

            // calculate children
            treeExpensesItemList.NodesIterator.DoOperation(
                new CalculateExpensesOperation(columnExpensesPlan, columnExpensesMinVolume,
                    columnExpensesPrice, columnExpensesItemCount, columnExpensesRealId, CalcInfo, false));
            // calculate parents
            treeExpensesItemList.NodesIterator.DoOperation(
                new CalculateExpensesOperation(columnExpensesPlan, columnExpensesMinVolume,
                    columnExpensesPrice, columnExpensesItemCount, columnExpensesRealId, CalcInfo, true));

            treeExpensesItemList.EndUpdate();

            // check total without taxes sum is not less than sum of all included expenses
            //NormalizeExpensesTotalSum();

            // calculate summary
            CalculateExpensesSummary();
        }

        /// <summary>
        /// Calculates Retrobonus by formula:
        /// [����������] = [�� ��� %] / 100 * [����. V/�� ����� ������] * [���� ������� ����/���] * [���� ���������] * [����������� ���-�� ��]
        /// </summary>
        /// <returns></returns>
        private void RecalculateRetrobonus() {
            TreeListNode lNode = treeExpensesItemList.FindNodeByFieldValue(columnExpensesRealId.FieldName, (int) RootExpenses.Retrobonus);
            if (null == lNode)
                return;
            decimal lQty = ConvertEx.ToDecimal(lNode.GetValue(columnExpensesItemCount));
            //BUG #24536. ��: �������� "����������": ����������� ������������ "�����������, ����\ ���". 
            //"������� ������ � ��������" = "���� ���������" ����� ��� ���� �������� ���������� �� ������� ����� �����,
            //������ "������� ������ � ��������" = "���� ���������" + 1 �����
            decimal lMonthCount = _calcInfo.MonthCount + (_calcInfo.DateSignPlanned.Day == 1 ? 0 : 1);
            decimal lResult = lQty / 100 * _calcInfo.AvgPocVolAfterStart * _calcInfo.AvgMaco * lMonthCount * _calcInfo.PlannedPocCount;
            lResult = Math.Round(lResult, 2, MidpointRounding.AwayFromZero);
            lNode.SetValue(columnExpensesPlan, lResult);
        }

        /// <summary>
        /// Normalizes the expenses total sum.
        /// </summary>
//        private void NormalizeExpensesTotalSum() {
//            decimal lSumCurrentExpenses = GetExpensesPlan(RootExpenses.SumWithoutTaxes);
//            decimal lSumExpensesNew = CheckedCommonExpensesPlan;
//            if (lSumExpensesNew <= lSumCurrentExpenses)
//                return;
//
//            TreeListNode lNode = treeExpensesItemList.FindNodeByFieldValue(columnExpensesRealId.FieldName, (int) RootExpenses.SumWithoutTaxes);
//            if (lNode != null)
//                lNode.SetValue(columnExpensesPlan, lSumExpensesNew);
//        }

        /// <summary>
        /// Calculates the expenses summary.
        /// </summary>
        private void CalculateExpensesSummary() {
            PrepareExpensesSummary();
            // Set summary Plan
            //https://jira.softserveinc.com/browse/ABIDEV-2458
            // [����� ����� ��������� ��� ������] = [����� ��������� ��� ������] + [����������]
            decimal lTotalWithoutTaxes = GetExpensesPlan(RootExpenses.SumWithoutTaxes)
                + GetExpensesPlan(RootExpenses.Retrobonus);

            // [����� ��� CAPEX] = [����� ����� ��������� ��� ������] + <����� �� ���� ������ �������, �� ����������� ������������ � ���������� ������������>
            decimal lSumWithoutCapex = lTotalWithoutTaxes + OtherSum;
            // [����� � CAPEX] = [����� ��� CAPEX] + [����� �� ���������� � ������������ ������������]
            decimal lSumWithCapex = lSumWithoutCapex + CapexSum;
            decimal lSumAdvertTax = _calcInfo.TaxAdvert > 0 ? _calcInfo.TaxAdvert / 100 * lTotalWithoutTaxes : 0;
            // [���] = 20% �� [����� � CAPEX]
            decimal lVATCoef = GetSummaryExpenses(SummaryCost.VAT, columnExpensesSumPreValue) / 100;
            decimal lVAT = Math.Round(lSumWithCapex * lVATCoef, 2, MidpointRounding.AwayFromZero);
            // [����� ����������] = [����� � CAPEX] + [����� �� �������] + [���]
            decimal lTotalInvestments = lSumWithCapex + lSumAdvertTax + lVAT;

            SetSummaryExpenses(SummaryCost.TotalWithoutTaxes, columnExpensesSumPlan, lTotalWithoutTaxes);
            SetSummaryExpenses(SummaryCost.SumWithoutCAPEX, columnExpensesSumPlan, lSumWithoutCapex);
            SetSummaryExpenses(SummaryCost.SumWithCAPEX, columnExpensesSumPlan, lSumWithCapex);
            SetSummaryExpenses(SummaryCost.SumAdvertTax, columnExpensesSumPlan, lSumAdvertTax);
            SetSummaryExpenses(SummaryCost.VAT, columnExpensesSumPlan, lVAT);
            SetSummaryExpenses(SummaryCost.TotalInvestments, columnExpensesSumPlan, lTotalInvestments);

            // Set summary Min Vol
            for (int lRowHandle = 0; lRowHandle < gridViewExpensesSummary.RowCount; lRowHandle++) {
                decimal lPlan = ConvertEx.ToDecimal(gridViewExpensesSummary.GetRowCellValue(lRowHandle, columnExpensesSumPlan));
                decimal lMinVol = _calcInfo.AvgMaco > 0 ? lPlan / _calcInfo.AvgMaco / 10.0M : 0;
                gridViewExpensesSummary.SetRowCellValue(lRowHandle, columnExpensesSumMinVol, lMinVol);
            }

            FireEditValueChanged();
        }

        /// <summary>
        /// Sets the summary expenses.
        /// </summary>
        /// <param name="rowType">Type of the row.</param>
        /// <param name="column">The column.</param>
        /// <param name="value">The value.</param>
        private void SetSummaryExpenses(SummaryCost rowType, GridColumn column, decimal value) {
            int lRowHandle = GetSummaryExpenseRowHandle(rowType);
            if (lRowHandle >= 0 && column != null)
                gridViewExpensesSummary.SetRowCellValue(lRowHandle, column, value);
        }

        /// <summary>
        /// Gets the summary expenses.
        /// </summary>
        /// <param name="rowType">Type of the row.</param>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private decimal GetSummaryExpenses(SummaryCost rowType, GridColumn column) {
            int lRowHandle = GetSummaryExpenseRowHandle(rowType);
            if (lRowHandle >= 0 && column != null)
                return ConvertEx.ToDecimal(gridViewExpensesSummary.GetRowCellValue(lRowHandle, column));
            return 0m;
        }

        /// <summary>
        /// Gets the summary row handle.
        /// </summary>
        /// <param name="rowType">Type of the row.</param>
        /// <returns></returns>
        private int GetSummaryExpenseRowHandle(SummaryCost rowType) {
            DataTable lDataTable = gridControlExpensesSummary.DataSource as DataTable;
            if (lDataTable != null && lDataTable.Rows.Count > 0) {
                DataView lDataView = new DataView(lDataTable);
                lDataView.Sort = columnExpensesSumId.FieldName;
                int lDataSourceIndex = lDataView.Find((int) rowType);
                return gridViewExpensesSummary.GetRowHandle(lDataSourceIndex);
            }
            return -1;
        }

        /// <summary>
        /// Gets the expenses plan.
        /// </summary>
        /// <param name="expenseType">Type of the expense.</param>
        /// <returns></returns>
        private decimal GetExpensesPlan(RootExpenses expenseType) {
            if (expenseType == RootExpenses.Unknown)
                return 0;
            TreeListNode lNode = treeExpensesItemList.FindNodeByFieldValue(columnExpensesRealId.FieldName, (int) expenseType);
            if (lNode != null)
                return ConvertEx.ToDecimal(lNode.GetValue(columnExpensesPlan));
            return 0;
        }

        /// <summary>
        /// Handles the ShowingEditor event of the treeExpensesItemList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void treeExpensesItemList_ShowingEditor(object sender, CancelEventArgs e) {
            if (ReadOnly) {
                e.Cancel = true;
                if (repositoryColumnExpensesItemCount.Buttons.Count > 0) {
                    repositoryColumnExpensesItemCount.Buttons[0].Visible = !e.Cancel;
                }
                if (repositoryColumnExpensesPlan.Buttons.Count > 0) {
                    repositoryColumnExpensesPlan.Buttons[0].Visible = !e.Cancel;
                }
                return;
            }

            if (treeExpensesItemList.FocusedColumn == columnExpensesPlan) {
                e.Cancel = !GetAllowContractInvestmentExpensesColumnEdit(treeExpensesItemList.FocusedNode,
                    AllowContractInvestmentExpensesColumnEdit.Plan);
                if (repositoryColumnExpensesPlan.Buttons.Count > 0) {
                    repositoryColumnExpensesPlan.Buttons[0].Visible = !e.Cancel;
                }
//                if (!e.Cancel && treeExpensesItemList.FocusedNode != null) {
//                    repositoryColumnExpensesPlan.MinValue = 0;
//                    int lRealId = ConvertEx.ToInt(treeExpensesItemList.FocusedNode.GetValue(columnExpensesRealId));
//                    if (lRealId == (int) RootExpenses.SumWithoutTaxes && CheckedCommonExpensesCount > 0) {
//                        repositoryColumnExpensesPlan.MinValue = CheckedCommonExpensesPlan;
//                    }
//                }
            }
            else if (treeExpensesItemList.FocusedColumn == columnExpensesItemCount) {
                e.Cancel = !GetAllowContractInvestmentExpensesColumnEdit(treeExpensesItemList.FocusedNode,
                    AllowContractInvestmentExpensesColumnEdit.Quantity);
                if (repositoryColumnExpensesItemCount.Buttons.Count > 0) {
                    repositoryColumnExpensesItemCount.Buttons[0].Visible = !e.Cancel;
                }
            }
            else if (treeExpensesItemList.FocusedColumn == columnExpensesIncluded) {
                e.Cancel = !GetAllowContractInvestmentExpensesColumnEdit(treeExpensesItemList.FocusedNode,
                    AllowContractInvestmentExpensesColumnEdit.IsIncluded);
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the treeExpensesItemList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
        private void treeExpensesItemList_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Delete) {
                DeleteExpenses();
            }
        }

        /// <summary>
        /// Handles the CellValueChanged event of the treeExpensesItemList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void treeExpensesItemList_CellValueChanged(object sender, CellValueChangedEventArgs e) {
            if (e.Column == columnExpensesIncluded)
                return;

// not needed because Retrobonus calculated in RecalculateExpensesTree()
//            if (e.Column == columnExpensesItemCount && !e.Node.HasChildren
//                && ConvertEx.ToDecimal(e.Node.GetValue(columnExpensesRealId)) == (int) RootExpenses.Retrobonus)
//                RecalculateRetrobonus(e.Node);

            RecalculateExpensesTree();
        }

        /// <summary>
        /// Handles the CheckedChanged event of the repositoryItemExpensesIncluded control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void repositoryItemExpensesIncluded_CheckedChanged(object sender, EventArgs e) {
            treeExpensesItemList.PostEditor();
            RecalculateExpensesTree();
        }

        private void gridControlExpensesSummary_Resize(object sender, EventArgs e) {
            int lMinWidth = gridViewExpensesSummary.VisibleColumns.Cast<GridColumn>().Sum(column => column.Width);
            gridViewExpensesSummary.OptionsView.ColumnAutoWidth = gridControlExpensesSummary.Width >= lMinWidth;
            gridViewExpensesSummary.HorzScrollVisibility = gridControlExpensesSummary.Width < lMinWidth
                ? ScrollVisibility.Always
                : ScrollVisibility.Auto;
        }

        private void ExpensesTree_Load(object sender, EventArgs e) {
            gridControlExpensesSummary_Resize(null, null);
        }
    }
}