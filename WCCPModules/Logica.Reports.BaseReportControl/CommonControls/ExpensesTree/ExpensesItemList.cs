﻿using System;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using System.Data;
using System.Globalization;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.Utility;
using System.ComponentModel;
using DevExpress.XtraGrid.Views.Base;
using System.Text;
using Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.DataAccess;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ExpensesItemList : XtraUserControl
    {
        public event EventHandler OnCheckChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpensesItemList"/> class.
        /// </summary>
        public ExpensesItemList()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the selected expenses item id list.
        /// </summary>
        /// <value>The selected expenses item id list.</value>
        public string SelectedExpensesItemIdList
        {
            get
            {
                DataTable dataTable = gridControl.DataSource as DataTable;
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    StringBuilder selectedIdList = new StringBuilder();
                    foreach(DataRow row in dataTable.Rows)
                    {
                        bool isSelected = ConvertEx.ToBool(row[columnExpensesChecked.FieldName]);
                        int id = ConvertEx.ToInt(row[columnExpensesId.FieldName]);
                        if (isSelected && id != 0)
                        {
                            selectedIdList.Append(id);
                            selectedIdList.Append(Constants.DELIMITER_ID_LIST);
                        }
                    }
                    return selectedIdList.ToString().TrimEnd(Constants.DELIMITER_ID_LIST.ToCharArray());
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the name of the expenses id field.
        /// </summary>
        /// <value>The name of the expenses id field.</value>
        public string ExpensesIdFieldName
        {
            get
            {
                return columnExpensesId.FieldName;
            }
        }

        /// <summary>
        /// Gets or sets the expenses item list.
        /// </summary>
        /// <value>The expenses item list.</value>
        public DataTable DataSource
        {
            set
            {
                gridControl.DataSource = value;
            }
            get
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable)gridControl.DataSource;
            }
        }

        /// <summary>
        /// Checks the current row.
        /// </summary>
        private void CheckCurrentRow()
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                bool currentValue = ConvertEx.ToBool(gridView.GetFocusedRowCellValue(columnExpensesChecked));
                gridView.SetFocusedRowCellValue(columnExpensesChecked, !currentValue);
            }
        }

        /// <summary>
        /// Handles the DoubleClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DoubleClick(object sender, EventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                Point pt = gridView.GridControl.PointToClient(Control.MousePosition);
                GridHitInfo info = gridView.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    CheckCurrentRow();
                }
            }
        }

        /// <summary>
        /// Handles the CheckedChanged event of the repositoryItemExpensesCheckedEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void repositoryItemExpensesCheckedEdit_CheckedChanged(object sender, EventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                bool isChecked = !ConvertEx.ToBool(gridView.GetFocusedRowCellValue(columnExpensesChecked));
                gridView.SetFocusedRowCellValue(columnExpensesChecked, isChecked);
                if (isChecked)
                {
                    gridView.SelectRow(gridView.FocusedRowHandle);
                    
                }
                else
                {
                    gridView.UnselectRow(gridView.FocusedRowHandle);
                }

                // fire event
                if (OnCheckChanged != null)
                {
                    OnCheckChanged(sender, e);
                }
            }
        }
    }
}
