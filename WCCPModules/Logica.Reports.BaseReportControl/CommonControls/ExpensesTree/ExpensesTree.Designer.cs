namespace Logica.Reports.BaseReportControl.CommonControls.ExpensesTree
{
    partial class ExpensesTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeExpensesItemList = new DevExpress.XtraTreeList.TreeList();
            this.columnExpensesRealId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnExpensesId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnExpensesIsUpdatable = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnExpensesIsCapex = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnExpensesPrice = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnExpensesName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnExpensesItemCount = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryColumnExpensesItemCount = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.columnExpensesIncluded = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemExpensesIncluded = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.columnExpensesPlan = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryColumnExpensesPlan = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.columnExpensesMinVolume = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridControlExpensesSummary = new DevExpress.XtraGrid.GridControl();
            this.gridViewExpensesSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnExpensesSumId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesSumName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesSumPreValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesSumEmptyCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesSumPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesSumMinVol = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize) (this.treeExpensesItemList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryColumnExpensesItemCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemExpensesIncluded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryColumnExpensesPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridControlExpensesSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridViewExpensesSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // treeExpensesItemList
            // 
            this.treeExpensesItemList.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeExpensesItemList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnExpensesRealId,
            this.columnExpensesId,
            this.columnExpensesIsUpdatable,
            this.columnExpensesIsCapex,
            this.columnExpensesPrice,
            this.columnExpensesName,
            this.columnExpensesItemCount,
            this.columnExpensesIncluded,
            this.columnExpensesPlan,
            this.columnExpensesMinVolume});
            this.treeExpensesItemList.Location = new System.Drawing.Point(0, 0);
            this.treeExpensesItemList.Name = "treeExpensesItemList";
            this.treeExpensesItemList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeExpensesItemList.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.treeExpensesItemList.OptionsView.ShowIndicator = false;
            this.treeExpensesItemList.ParentFieldName = "PARENT_ID";
            this.treeExpensesItemList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryColumnExpensesItemCount,
            this.repositoryColumnExpensesPlan,
            this.repositoryItemExpensesIncluded});
            this.treeExpensesItemList.Size = new System.Drawing.Size(597, 152);
            this.treeExpensesItemList.TabIndex = 5;
            this.treeExpensesItemList.VertScrollVisibility = DevExpress.XtraTreeList.ScrollVisibility.Always;
            this.treeExpensesItemList.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.treeExpensesItemList_CellValueChanged);
            this.treeExpensesItemList.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.treeExpensesItemList_ShowingEditor);
            this.treeExpensesItemList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeExpensesItemList_KeyDown);
            // 
            // columnExpensesRealId
            // 
            this.columnExpensesRealId.Caption = "REAL_ID";
            this.columnExpensesRealId.FieldName = "REAL_ID";
            this.columnExpensesRealId.Name = "columnExpensesRealId";
            this.columnExpensesRealId.OptionsColumn.AllowEdit = false;
            this.columnExpensesRealId.OptionsColumn.AllowMove = false;
            this.columnExpensesRealId.OptionsColumn.FixedWidth = true;
            this.columnExpensesRealId.OptionsColumn.ReadOnly = true;
            this.columnExpensesRealId.SortOrder = System.Windows.Forms.SortOrder.Descending;
            this.columnExpensesRealId.Width = 200;
            // 
            // columnExpensesId
            // 
            this.columnExpensesId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesId.Caption = "ID";
            this.columnExpensesId.FieldName = "ID";
            this.columnExpensesId.Name = "columnExpensesId";
            this.columnExpensesId.OptionsColumn.AllowEdit = false;
            this.columnExpensesId.OptionsColumn.AllowMove = false;
            this.columnExpensesId.OptionsColumn.ReadOnly = true;
            // 
            // columnExpensesIsUpdatable
            // 
            this.columnExpensesIsUpdatable.Caption = "IS_UPDATABLE";
            this.columnExpensesIsUpdatable.FieldName = "IS_UPDATABLE";
            this.columnExpensesIsUpdatable.Name = "columnExpensesIsUpdatable";
            this.columnExpensesIsUpdatable.OptionsColumn.AllowEdit = false;
            this.columnExpensesIsUpdatable.OptionsColumn.AllowMove = false;
            this.columnExpensesIsUpdatable.OptionsColumn.ReadOnly = true;
            // 
            // columnExpensesIsCapex
            // 
            this.columnExpensesIsCapex.Caption = "treeListColumn1";
            this.columnExpensesIsCapex.FieldName = "IS_CAPEX";
            this.columnExpensesIsCapex.Name = "columnExpensesIsCapex";
            // 
            // columnExpensesPrice
            // 
            this.columnExpensesPrice.Caption = "����, 1 ��";
            this.columnExpensesPrice.FieldName = "PRICE";
            this.columnExpensesPrice.Name = "columnExpensesPrice";
            this.columnExpensesPrice.OptionsColumn.AllowMove = false;
            // 
            // columnExpensesName
            // 
            this.columnExpensesName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesName.Caption = "������ ������";
            this.columnExpensesName.FieldName = "LEAF_NAME";
            this.columnExpensesName.MinWidth = 100;
            this.columnExpensesName.Name = "columnExpensesName";
            this.columnExpensesName.OptionsColumn.AllowEdit = false;
            this.columnExpensesName.OptionsColumn.AllowMove = false;
            this.columnExpensesName.OptionsColumn.AllowSize = false;
            this.columnExpensesName.OptionsColumn.ReadOnly = true;
            this.columnExpensesName.Visible = true;
            this.columnExpensesName.VisibleIndex = 0;
            this.columnExpensesName.Width = 150;
            // 
            // columnExpensesItemCount
            // 
            this.columnExpensesItemCount.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesItemCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesItemCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesItemCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesItemCount.Caption = "�� ��� %";
            this.columnExpensesItemCount.ColumnEdit = this.repositoryColumnExpensesItemCount;
            this.columnExpensesItemCount.FieldName = "QUANTITY";
            this.columnExpensesItemCount.Format.FormatString = "{0:N0}";
            this.columnExpensesItemCount.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnExpensesItemCount.Name = "columnExpensesItemCount";
            this.columnExpensesItemCount.OptionsColumn.AllowMove = false;
            this.columnExpensesItemCount.OptionsColumn.FixedWidth = true;
            this.columnExpensesItemCount.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesItemCount.Visible = true;
            this.columnExpensesItemCount.VisibleIndex = 1;
            this.columnExpensesItemCount.Width = 100;
            // 
            // repositoryColumnExpensesItemCount
            // 
            this.repositoryColumnExpensesItemCount.AutoHeight = false;
            this.repositoryColumnExpensesItemCount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryColumnExpensesItemCount.Mask.EditMask = "n0";
            this.repositoryColumnExpensesItemCount.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositoryColumnExpensesItemCount.Name = "repositoryColumnExpensesItemCount";
            // 
            // columnExpensesIncluded
            // 
            this.columnExpensesIncluded.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesIncluded.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesIncluded.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesIncluded.Caption = "�������� � ����� ���������";
            this.columnExpensesIncluded.ColumnEdit = this.repositoryItemExpensesIncluded;
            this.columnExpensesIncluded.FieldName = "IS_INCLUDED";
            this.columnExpensesIncluded.Name = "columnExpensesIncluded";
            this.columnExpensesIncluded.OptionsColumn.AllowMove = false;
            this.columnExpensesIncluded.OptionsColumn.FixedWidth = true;
            this.columnExpensesIncluded.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesIncluded.Visible = true;
            this.columnExpensesIncluded.VisibleIndex = 2;
            this.columnExpensesIncluded.Width = 175;
            // 
            // repositoryItemExpensesIncluded
            // 
            this.repositoryItemExpensesIncluded.AutoHeight = false;
            this.repositoryItemExpensesIncluded.Name = "repositoryItemExpensesIncluded";
            this.repositoryItemExpensesIncluded.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemExpensesIncluded.CheckedChanged += new System.EventHandler(this.repositoryItemExpensesIncluded_CheckedChanged);
            // 
            // columnExpensesPlan
            // 
            this.columnExpensesPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesPlan.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesPlan.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesPlan.Caption = "����, ���";
            this.columnExpensesPlan.ColumnEdit = this.repositoryColumnExpensesPlan;
            this.columnExpensesPlan.FieldName = "PLAN_UAH";
            this.columnExpensesPlan.Format.FormatString = "{0:N2}";
            this.columnExpensesPlan.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnExpensesPlan.Name = "columnExpensesPlan";
            this.columnExpensesPlan.OptionsColumn.AllowMove = false;
            this.columnExpensesPlan.OptionsColumn.FixedWidth = true;
            this.columnExpensesPlan.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesPlan.Visible = true;
            this.columnExpensesPlan.VisibleIndex = 3;
            this.columnExpensesPlan.Width = 100;
            // 
            // repositoryColumnExpensesPlan
            // 
            this.repositoryColumnExpensesPlan.AutoHeight = false;
            this.repositoryColumnExpensesPlan.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryColumnExpensesPlan.Mask.EditMask = "n2";
            this.repositoryColumnExpensesPlan.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositoryColumnExpensesPlan.Name = "repositoryColumnExpensesPlan";
            // 
            // columnExpensesMinVolume
            // 
            this.columnExpensesMinVolume.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesMinVolume.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesMinVolume.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesMinVolume.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesMinVolume.Caption = "Min V ��";
            this.columnExpensesMinVolume.FieldName = "PLAN_MINGLVOL";
            this.columnExpensesMinVolume.Format.FormatString = "{0:N2}";
            this.columnExpensesMinVolume.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnExpensesMinVolume.Name = "columnExpensesMinVolume";
            this.columnExpensesMinVolume.OptionsColumn.AllowEdit = false;
            this.columnExpensesMinVolume.OptionsColumn.AllowMove = false;
            this.columnExpensesMinVolume.OptionsColumn.FixedWidth = true;
            this.columnExpensesMinVolume.OptionsColumn.ReadOnly = true;
            this.columnExpensesMinVolume.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesMinVolume.Visible = true;
            this.columnExpensesMinVolume.VisibleIndex = 4;
            this.columnExpensesMinVolume.Width = 100;
            // 
            // gridControlExpensesSummary
            // 
            this.gridControlExpensesSummary.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gridControlExpensesSummary.Location = new System.Drawing.Point(0, 158);
            this.gridControlExpensesSummary.MainView = this.gridViewExpensesSummary;
            this.gridControlExpensesSummary.Name = "gridControlExpensesSummary";
            this.gridControlExpensesSummary.Size = new System.Drawing.Size(597, 112);
            this.gridControlExpensesSummary.TabIndex = 11;
            this.gridControlExpensesSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExpensesSummary});
            this.gridControlExpensesSummary.Resize += new System.EventHandler(this.gridControlExpensesSummary_Resize);
            // 
            // gridViewExpensesSummary
            // 
            this.gridViewExpensesSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnExpensesSumId,
            this.columnExpensesSumName,
            this.columnExpensesSumPreValue,
            this.columnExpensesSumEmptyCol,
            this.columnExpensesSumPlan,
            this.columnExpensesSumMinVol});
            this.gridViewExpensesSummary.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewExpensesSummary.GridControl = this.gridControlExpensesSummary;
            this.gridViewExpensesSummary.Name = "gridViewExpensesSummary";
            this.gridViewExpensesSummary.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExpensesSummary.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewExpensesSummary.OptionsView.ShowColumnHeaders = false;
            this.gridViewExpensesSummary.OptionsView.ShowGroupPanel = false;
            this.gridViewExpensesSummary.OptionsView.ShowIndicator = false;
            this.gridViewExpensesSummary.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            // 
            // columnExpensesSumId
            // 
            this.columnExpensesSumId.Caption = "ID";
            this.columnExpensesSumId.FieldName = "ID";
            this.columnExpensesSumId.Name = "columnExpensesSumId";
            // 
            // columnExpensesSumName
            // 
            this.columnExpensesSumName.Caption = "COST_NAME";
            this.columnExpensesSumName.FieldName = "COST_NAME";
            this.columnExpensesSumName.MinWidth = 100;
            this.columnExpensesSumName.Name = "columnExpensesSumName";
            this.columnExpensesSumName.OptionsColumn.AllowEdit = false;
            this.columnExpensesSumName.OptionsColumn.AllowSize = false;
            this.columnExpensesSumName.OptionsColumn.ReadOnly = true;
            this.columnExpensesSumName.OptionsColumn.ShowCaption = false;
            this.columnExpensesSumName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesSumName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesSumName.Visible = true;
            this.columnExpensesSumName.VisibleIndex = 0;
            this.columnExpensesSumName.Width = 176;
            // 
            // columnExpensesSumPreValue
            // 
            this.columnExpensesSumPreValue.DisplayFormat.FormatString = "{0:N2}";
            this.columnExpensesSumPreValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnExpensesSumPreValue.FieldName = "COST_RATE";
            this.columnExpensesSumPreValue.Name = "columnExpensesSumPreValue";
            this.columnExpensesSumPreValue.OptionsColumn.AllowEdit = false;
            this.columnExpensesSumPreValue.OptionsColumn.AllowSize = false;
            this.columnExpensesSumPreValue.OptionsColumn.FixedWidth = true;
            this.columnExpensesSumPreValue.OptionsColumn.ReadOnly = true;
            this.columnExpensesSumPreValue.OptionsColumn.ShowCaption = false;
            this.columnExpensesSumPreValue.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesSumPreValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesSumPreValue.Visible = true;
            this.columnExpensesSumPreValue.VisibleIndex = 1;
            this.columnExpensesSumPreValue.Width = 100;
            // 
            // columnExpensesSumEmptyCol
            // 
            this.columnExpensesSumEmptyCol.FieldName = "columnExpensesEmptyCol";
            this.columnExpensesSumEmptyCol.Name = "columnExpensesSumEmptyCol";
            this.columnExpensesSumEmptyCol.OptionsColumn.AllowEdit = false;
            this.columnExpensesSumEmptyCol.OptionsColumn.AllowSize = false;
            this.columnExpensesSumEmptyCol.OptionsColumn.FixedWidth = true;
            this.columnExpensesSumEmptyCol.OptionsColumn.ReadOnly = true;
            this.columnExpensesSumEmptyCol.OptionsColumn.ShowCaption = false;
            this.columnExpensesSumEmptyCol.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesSumEmptyCol.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesSumEmptyCol.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.columnExpensesSumEmptyCol.Visible = true;
            this.columnExpensesSumEmptyCol.VisibleIndex = 2;
            this.columnExpensesSumEmptyCol.Width = 175;
            // 
            // columnExpensesSumPlan
            // 
            this.columnExpensesSumPlan.Caption = "COST_VALUE";
            this.columnExpensesSumPlan.DisplayFormat.FormatString = "{0:N2}";
            this.columnExpensesSumPlan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnExpensesSumPlan.FieldName = "COST_VALUE";
            this.columnExpensesSumPlan.Name = "columnExpensesSumPlan";
            this.columnExpensesSumPlan.OptionsColumn.AllowEdit = false;
            this.columnExpensesSumPlan.OptionsColumn.AllowSize = false;
            this.columnExpensesSumPlan.OptionsColumn.FixedWidth = true;
            this.columnExpensesSumPlan.OptionsColumn.ReadOnly = true;
            this.columnExpensesSumPlan.OptionsColumn.ShowCaption = false;
            this.columnExpensesSumPlan.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesSumPlan.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesSumPlan.Visible = true;
            this.columnExpensesSumPlan.VisibleIndex = 3;
            this.columnExpensesSumPlan.Width = 100;
            // 
            // columnExpensesSumMinVol
            // 
            this.columnExpensesSumMinVol.DisplayFormat.FormatString = "{0:N2}";
            this.columnExpensesSumMinVol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnExpensesSumMinVol.FieldName = "MIN_VOL";
            this.columnExpensesSumMinVol.Name = "columnExpensesSumMinVol";
            this.columnExpensesSumMinVol.OptionsColumn.AllowEdit = false;
            this.columnExpensesSumMinVol.OptionsColumn.AllowSize = false;
            this.columnExpensesSumMinVol.OptionsColumn.FixedWidth = true;
            this.columnExpensesSumMinVol.OptionsColumn.ReadOnly = true;
            this.columnExpensesSumMinVol.OptionsColumn.ShowCaption = false;
            this.columnExpensesSumMinVol.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesSumMinVol.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesSumMinVol.Visible = true;
            this.columnExpensesSumMinVol.VisibleIndex = 4;
            this.columnExpensesSumMinVol.Width = 100;
            // 
            // ExpensesTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlExpensesSummary);
            this.Controls.Add(this.treeExpensesItemList);
            this.Name = "ExpensesTree";
            this.Size = new System.Drawing.Size(597, 270);
            this.Load += new System.EventHandler(this.ExpensesTree_Load);
            ((System.ComponentModel.ISupportInitialize) (this.treeExpensesItemList)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryColumnExpensesItemCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemExpensesIncluded)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryColumnExpensesPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridControlExpensesSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridViewExpensesSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeExpensesItemList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesRealId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesIsUpdatable;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesPrice;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesItemCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryColumnExpensesItemCount;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesIncluded;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemExpensesIncluded;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesPlan;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryColumnExpensesPlan;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesMinVolume;
        private DevExpress.XtraGrid.GridControl gridControlExpensesSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewExpensesSummary;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesSumName;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesSumPlan;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesSumPreValue;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesSumMinVol;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesSumEmptyCol;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesSumId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnExpensesIsCapex;

    }
}
