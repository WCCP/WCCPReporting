﻿using System;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.DataAccess;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls.ExpensesTree {
    /// <summary>
    /// 
    /// </summary>
    public class CalcInformation {
        public decimal AvgMaco { get; set; }
        public decimal AvgPocVolAfterStart { get; set; }
        public int PlannedPocCount { get; set; }
        public int MonthCount { get; set; }
        public DateTime DateSignPlanned { set; get; }
        public decimal TaxAdvert { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    internal class CalculateExpensesOperation : TreeListOperation {
        private readonly CalcInformation _calcInfo;
        private readonly bool _calcParents;
        private readonly TreeListColumn _columnMinVol;
        private readonly TreeListColumn _columnPlan;
        private readonly TreeListColumn _columnPrice;
        private readonly TreeListColumn _columnQty;
        private readonly TreeListColumn _columnRealId;

        public CalculateExpensesOperation(TreeListColumn columnPlan, TreeListColumn columnMinVol,
            TreeListColumn columnPrice, TreeListColumn columnQty, TreeListColumn columnRealId, CalcInformation calcInfo, bool calcParents) {
            _columnPlan = columnPlan;
            _columnMinVol = columnMinVol;
            _columnPrice = columnPrice;
            _columnQty = columnQty;
            _columnRealId = columnRealId;
            _calcInfo = calcInfo;
            _calcParents = calcParents;
        }

        /// <summary>
        /// Must be implemented to perform an operation on the visited node.
        /// </summary>
        /// <param name="node">A <see cref="T:DevExpress.XtraTreeList.Nodes.TreeListNode"/> object representing the node against which the operation is to be performed.</param>
        public override void Execute(TreeListNode node) {
            if (_calcParents) {
                if (node.HasChildren) {
                    decimal lPlan = 0;
                    decimal lMinVol = 0;
                    foreach (TreeListNode lChildNode in node.Nodes) {
                        lPlan += ConvertEx.ToDecimal(lChildNode.GetValue(_columnPlan));
                        lMinVol += ConvertEx.ToDecimal(lChildNode.GetValue(_columnMinVol));
                    }
                    node.SetValue(_columnPlan, lPlan);
                    node.SetValue(_columnMinVol, lMinVol);
                }
            }
            else {
                if (!node.HasChildren) {
                    decimal lRealId = ConvertEx.ToDecimal(node.GetValue(_columnRealId));
                    decimal lQty = ConvertEx.ToDecimal(node.GetValue(_columnQty));
                    decimal lPrice = ConvertEx.ToDecimal(node.GetValue(_columnPrice));
                    decimal lPlan = lPrice > 0 ? lQty * lPrice : ConvertEx.ToDecimal(node.GetValue(_columnPlan));
                    if (lRealId == (int) RootExpenses.SumWithoutTaxes || lRealId == (int) RootExpenses.Retrobonus) {
                        lPlan = ConvertEx.ToDecimal(node.GetValue(_columnPlan));
                    }

                    if (_calcInfo.AvgMaco > 0) {
                        node.SetValue(_columnMinVol, lPlan / _calcInfo.AvgMaco / 10.0M);
                    }
                    else {
                        node.SetValue(_columnMinVol, DBNull.Value);
                    }
                    node.SetValue(_columnPlan, lPlan);
                }
            }
        }
    }
}