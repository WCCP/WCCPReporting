﻿namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class ExpensesItemList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.columnExpensesChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemExpensesCheckedEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnExpensesId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnExpensesPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemExpensesCheckedEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // columnExpensesChecked
            // 
            this.columnExpensesChecked.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesChecked.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesChecked.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesChecked.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesChecked.Caption = "Выбрать";
            this.columnExpensesChecked.ColumnEdit = this.repositoryItemExpensesCheckedEdit;
            this.columnExpensesChecked.FieldName = "CHECKED";
            this.columnExpensesChecked.MaxWidth = 70;
            this.columnExpensesChecked.Name = "columnExpensesChecked";
            this.columnExpensesChecked.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesChecked.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesChecked.Visible = true;
            this.columnExpensesChecked.VisibleIndex = 0;
            this.columnExpensesChecked.Width = 70;
            // 
            // repositoryItemExpensesCheckedEdit
            // 
            this.repositoryItemExpensesCheckedEdit.AutoHeight = false;
            this.repositoryItemExpensesCheckedEdit.Name = "repositoryItemExpensesCheckedEdit";
            this.repositoryItemExpensesCheckedEdit.CheckedChanged += new System.EventHandler(this.repositoryItemExpensesCheckedEdit_CheckedChanged);
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemExpensesCheckedEdit});
            this.gridControl.Size = new System.Drawing.Size(482, 310);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Highlight;
            this.gridView.Appearance.SelectedRow.BackColor2 = System.Drawing.SystemColors.Highlight;
            this.gridView.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnExpensesId,
            this.columnExpensesChecked,
            this.columnExpensesGroupName,
            this.columnExpensesName,
            this.columnExpensesPrice});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.SystemColors.Highlight;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.HighlightText;
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.columnExpensesChecked;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = true;
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.DoubleClick += new System.EventHandler(this.gridView_DoubleClick);
            // 
            // columnExpensesId
            // 
            this.columnExpensesId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesId.Caption = "ID";
            this.columnExpensesId.FieldName = "EXPENSE_ID";
            this.columnExpensesId.Name = "columnExpensesId";
            this.columnExpensesId.OptionsColumn.AllowEdit = false;
            this.columnExpensesId.OptionsColumn.ReadOnly = true;
            this.columnExpensesId.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // columnExpensesGroupName
            // 
            this.columnExpensesGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesGroupName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesGroupName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesGroupName.Caption = "Раздел";
            this.columnExpensesGroupName.FieldName = "EXPENSES_GROUP_NAME";
            this.columnExpensesGroupName.Name = "columnExpensesGroupName";
            this.columnExpensesGroupName.OptionsColumn.AllowEdit = false;
            this.columnExpensesGroupName.OptionsColumn.ReadOnly = true;
            this.columnExpensesGroupName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesGroupName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesGroupName.Visible = true;
            this.columnExpensesGroupName.VisibleIndex = 1;
            this.columnExpensesGroupName.Width = 141;
            // 
            // columnExpensesName
            // 
            this.columnExpensesName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesName.Caption = "Статья затрат";
            this.columnExpensesName.FieldName = "EXPENSE_NAME";
            this.columnExpensesName.Name = "columnExpensesName";
            this.columnExpensesName.OptionsColumn.AllowEdit = false;
            this.columnExpensesName.OptionsColumn.ReadOnly = true;
            this.columnExpensesName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesName.Visible = true;
            this.columnExpensesName.VisibleIndex = 2;
            this.columnExpensesName.Width = 141;
            // 
            // columnExpensesPrice
            // 
            this.columnExpensesPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.columnExpensesPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnExpensesPrice.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnExpensesPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnExpensesPrice.Caption = "Цена, 1 шт";
            this.columnExpensesPrice.FieldName = "EXPENSE_PRICE";
            this.columnExpensesPrice.Name = "columnExpensesPrice";
            this.columnExpensesPrice.OptionsColumn.AllowEdit = false;
            this.columnExpensesPrice.OptionsColumn.ReadOnly = true;
            this.columnExpensesPrice.OptionsColumn.ShowInCustomizationForm = false;
            this.columnExpensesPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnExpensesPrice.Visible = true;
            this.columnExpensesPrice.VisibleIndex = 3;
            this.columnExpensesPrice.Width = 143;
            // 
            // ExpensesItemList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridControl);
            this.Name = "ExpensesItemList";
            this.Size = new System.Drawing.Size(482, 310);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemExpensesCheckedEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesId;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesChecked;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesName;
        private DevExpress.XtraGrid.Columns.GridColumn columnExpensesPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemExpensesCheckedEdit;


    }
}
