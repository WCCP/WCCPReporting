﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using Logica.Reports.Common;

namespace Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.DataAccess
{
    internal class DataProvider
    {
        /// <summary>
        /// Gets the expenses item list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        public static DataTable GetExpensesAvailableItemList(int contractId)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_EXPENSES_AVAILABLE_LIST);
        }

        /// <summary>
        /// Gets the expenses plan item list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        public static DataTable GetExpensesPlanItemList(int contractId)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_EXPENSES_PLAN_LIST,
                new SqlParameter[] 
                        { 
                            new SqlParameter(Constants.SP_GET_EXPENSES_PLAN_LIST_PARAM1, contractId)
                        });
        }

        /// <summary>
        /// Gets the new expenses plan summary item list.
        /// </summary>
        /// <param name="expensesIdList">The expenses id list.</param>
        /// <returns></returns>
        public static DataTable GetNewExpensesPlanSummaryItemList(string expensesIdList)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_EXPENSES_PLAN_NEW_LIST,
                new SqlParameter[] 
                        { 
                            new SqlParameter(Constants.SP_GET_EXPENSES_PLAN_NEW_LIST_PARAM1, expensesIdList)
                        });
        }

        /// <summary>
        /// Gets the expenses plan item list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        public static DataTable GetExpensesPlanSummaryItemList(int contractId)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_EXPENSES_PLAN_SUMMARY_LIST,
                new SqlParameter[] 
                        { 
                            new SqlParameter(Constants.SP_GET_EXPENSES_PLAN_SUMMARY_LIST_PARAM1, contractId)
                        });
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <param name="paramList">The param list.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName, params SqlParameter[] paramList)
        {
            DataTable dataTable = null;
            try
            {
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(spName, paramList);
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
            return dataTable;
        }
    }
}
