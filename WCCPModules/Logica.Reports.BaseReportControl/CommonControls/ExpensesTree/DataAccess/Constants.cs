﻿namespace Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.DataAccess
{
    internal class Constants
    {
        public const string DELIMITER_ID_LIST = ",";
        public const string EXPENSE_FRIDGE_GROUP_ID = "N5";
        // ExpensesTree
        public const string SP_GET_EXPENSES_AVAILABLE_LIST = "spDW_CM_GetAvailableExpenses";
        public const string SP_GET_EXPENSES_PLAN_LIST = "spDW_CM_GetContractExpensesPlan";
        public const string SP_GET_EXPENSES_PLAN_SUMMARY_LIST = "spDW_CM_GetContractBaseInvestmentIndicatorsSummary";
        public const string SP_GET_EXPENSES_PLAN_NEW_LIST = "spDW_CM_GetNewContractExpenses";
        public const string SP_GET_EXPENSES_PLAN_FIELDS = "spDW_CM_GetNewContractExpenses";
        // ExpensesTree
        public const string SP_GET_EXPENSES_AVAILABLE_LIST_PARAM1 = "CONTRACT_ID";
        public const string SP_GET_EXPENSES_PLAN_LIST_PARAM1 = "CONTRACT_ID";
        public const string SP_GET_EXPENSES_PLAN_SUMMARY_LIST_PARAM1 = "CONTRACT_ID";
        public const string SP_GET_EXPENSES_PLAN_NEW_LIST_PARAM1 = "ExpensesList";
    }

    /// <summary>
    /// 
    /// </summary>
    internal enum AllowContractInvestmentExpensesColumnEdit
    {
        /// <summary>
        /// 
        /// </summary>
        None = 0,
        /// <summary>
        /// 
        /// </summary>
        Quantity = 1,
        /// <summary>
        /// 
        /// </summary>
        Plan = 2,
        /// <summary>
        /// 
        /// </summary>
        IsIncluded = 3
    }

    /// <summary>
    /// 
    /// </summary>
    internal enum RootExpenses
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        SumWithoutTaxes = -1,
        /// <summary>
        /// 
        /// </summary>
        Retrobonus = -2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SummaryCost
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        TotalWithoutTaxes = 1,
        /// <summary>
        /// 
        /// </summary>
        SumWithoutCAPEX = 2,
        /// <summary>
        /// 
        /// </summary>
        SumWithCAPEX = 3,
        /// <summary>
        /// 
        /// </summary>
        SumAdvertTax = 4,
        /// <summary>
        /// 
        /// </summary>
        VAT = 5,
        /// <summary>
        /// 
        /// </summary>
        TotalInvestments = 6
    }
}
