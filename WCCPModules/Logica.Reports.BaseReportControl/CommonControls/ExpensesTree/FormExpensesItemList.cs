using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.DataAccess;
using System.Data;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FormExpensesItemList : XtraForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormExpensesItemList"/> class.
        /// </summary>
        private FormExpensesItemList()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FormExpensesItemList"/> class.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        public FormExpensesItemList(int contractId, string currentExpensesIdList)
        {
            InitializeComponent();

            ContractId = contractId;
            CurrentExpensesIdList = currentExpensesIdList;
        }

        /// <summary>
        /// Gets or sets the contract id.
        /// </summary>
        /// <value>The contract id.</value>
        private int ContractId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the contract id.
        /// </summary>
        /// <value>The contract id.</value>
        private string CurrentExpensesIdList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the real id field.
        /// </summary>
        /// <value>The name of the real id field.</value>
        private string RealIdFieldName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the selected expenses item id list.
        /// </summary>
        /// <value>The selected expenses item id list.</value>
        public string SelectedExpensesItemIdList
        {
            get
            {
                return expensesItemList.SelectedExpensesItemIdList;
            }
        }

        /// <summary>
        /// Handles the Load event of the FormClientList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FormClientList_Load(object sender, EventArgs e)
        {
            DataView dataView = new DataView(DataProvider.GetExpensesAvailableItemList(ContractId));
            if(dataView != null && dataView.Count > 0)
            {
                if (!string.IsNullOrEmpty(CurrentExpensesIdList))
                {
                    dataView.RowFilter = string.Format("{0} NOT IN ({1})", expensesItemList.ExpensesIdFieldName, CurrentExpensesIdList);
                }
                expensesItemList.DataSource = dataView.ToTable();
            }
            UpdateButtonState();
        }

        /// <summary>
        /// Handles the OnCheckChanged event of the expensesItemList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void expensesItemList_OnCheckChanged(object sender, EventArgs e)
        {
            UpdateButtonState();
        }

        /// <summary>
        /// Updates the state of the button.
        /// </summary>
        private void UpdateButtonState()
        {
            btnOk.Enabled = !string.IsNullOrEmpty(SelectedExpensesItemIdList);
        }
    }
}