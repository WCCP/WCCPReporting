﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using System.Diagnostics;
using System.Collections.ObjectModel;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;

namespace Logica.Reports.BaseReportControl.CommonControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FileUploader : XtraUserControl, IControlReadOnly
    {
        private bool readOnly;
        private int maxItemsCount;
    
        private const long MAX_DOC_FILE_SIZE = 5 * 1024 * 1024; // 5 Mb
        private const string DIALOG_OPEN_FILE_FILTER = "Все файлы|*.*";
        private const string DIALOG_SAVE_FILE_FILTER = "Все файлы|*.*";
        private const string DIALOG_OPEN_FILE = "Загрузить файл";
        private const string DIALOG_SAVE_FILE = "Сохранить файл";
        private const string MESSAGE_FILE_SIZE_LIMIT = "Разрешено загружать файлы размером не больше 5 Мб.";
        private const string DIALOG_OPEN_FILE_QUESTION = "Хотите открыть сохраненный файл '{0}'?";
        private const string DIALOG_DELETE_FILE_QUESTION = "Вы хотите удалить файл '{0}'?";
        private const string DIALOG_FILE_EXISTS = "Файл с таким названием уже загружен.";

        /// <summary>
        /// 
        /// </summary>
        public delegate void FileDataEventHandler(object sender, FileDataInfo e);

        /// <summary>
        /// 
        /// </summary>
        public class FileDataInfo
        {
            public int Id {get; set;}
            public string FilePath {get; set;}
            public byte[] FileData {get; set;}
        }

        /// <summary>
        /// Occurs when [get file data].
        /// </summary>
        [Category("Events")]
        [Description("Fires event to get file data")]
        public event FileDataEventHandler GetFileData;

        //
        // Summary:
        //     Fires immediately after the edit value has been changed.
        [Category("Events")]
        [Description("Fires immediately after the edit value has been changed.")]
        public event EventHandler EditValueChanged;


        /// <summary>
        /// Initializes a new instance of the <see cref="FileUploader"/> class.
        /// </summary>
        public FileUploader()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the data.
        /// </summary>
        /// <value>The data.</value>
        public DataTable Data
        {
            set
            {
                gridControl.DataSource = value;
            }
        }

        /// <summary>
        /// Gets or sets the max items count.
        /// </summary>
        /// 
        public int MaxItemsCount
        {
            get
            {
                return maxItemsCount;
            }
            set
            {
                maxItemsCount = value;
                UpdateState();
            }
        }

        /// <summary>
        /// Gets or sets the field name ID.
        /// </summary>
        /// <value>The field name ID.</value>
        public string FieldNameID
        {
            get
            {
                return columnId.FieldName;
            }
            set
            {
                columnId.FieldName = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field name file.
        /// </summary>
        /// <value>The name of the field name file.</value>
        public string FieldNameFileName
        {
            get
            {
                return columnFileName.FieldName;
            }
            set
            {
                columnFileName.FieldName = value;
            }
        }

        /// <summary>
        /// Gets or sets the field name file data.
        /// </summary>
        /// <value>The field name file data.</value>
        public string FieldNameFileData
        {
            get
            {
                return columnFileData.FieldName;
            }
            set
            {
                columnFileData.FieldName = value;
            }
        }

        /// <summary>
        /// Gets or sets the button load text.
        /// </summary>
        /// <value>The button load text.</value>
        public string ButtonLoadText
        {
            set
            {
                btnLoad.Text = value;
            }
            get
            {
                return btnLoad.Text;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has rows.
        /// </summary>
        /// <value><c>true</c> if this instance has rows; otherwise, <c>false</c>.</value>
        public bool HasRows 
        {
            get 
            {
                return (this.gridControl.DefaultView as GridView).RowCount > 0;
            }
        }

        /// <summary>
        /// Updates the read only status.
        /// </summary>
        private void UpdateState()
        {
            columnDelete.Visible = !ReadOnly;

            btnLoad.Enabled = !ReadOnly;

            if (btnLoad.Enabled && MaxItemsCount > 0)
            {
                btnLoad.Enabled = gridView.RowCount < MaxItemsCount;
            }
        }

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="rowStates">The row states.</param>
        /// <returns></returns>
        public Collection<FileDataInfo> GetFiles(DataRowState rowStates)
        {
            Collection<FileDataInfo> collection = new Collection<FileDataInfo>();
            DataTable dataTable = gridControl.DataSource as DataTable;
            if (dataTable != null)
            {
                dataTable = dataTable.GetChanges(rowStates);
                if (dataTable != null)
                {
                    DataRowVersion rowVersion = rowStates == DataRowState.Deleted ? DataRowVersion.Original : DataRowVersion.Current;
                    foreach (DataRow row in dataTable.Rows)
                    {
                        FileDataInfo fileDataInfo = new FileDataInfo();
                        fileDataInfo.Id = ConvertEx.ToInt(row[columnId.FieldName, rowVersion]);
                        if (rowStates == DataRowState.Added)
                        {
                            fileDataInfo.FilePath = ConvertEx.ToString(row[columnFileName.FieldName, rowVersion]);
                            fileDataInfo.FileData = ConvertEx.ToBytesArray(row[columnFileData.FieldName, rowVersion]);
                        }
                        collection.Add(fileDataInfo);
                    }
                }
            }
            return collection;
        }

        /// <summary>
        /// Uploads the file.
        /// </summary>
        private void UploadFile()
        {
            string currentDirectory = Directory.GetCurrentDirectory();

            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = DIALOG_OPEN_FILE_FILTER;
            openFileDialog.Title = DIALOG_OPEN_FILE;
            openFileDialog.CheckFileExists = true;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;

                if (CheckFileExists(Path.GetFileName(filePath)))
                {
                    XtraMessageBox.Show(DIALOG_FILE_EXISTS, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                byte[] fileData = File.ReadAllBytes(filePath);

                if (fileData != null && fileData.Length > MAX_DOC_FILE_SIZE)
                {
                    XtraMessageBox.Show(MESSAGE_FILE_SIZE_LIMIT, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    InsertFile(Path.GetFileName(filePath), fileData);   
                }
            }

            Directory.SetCurrentDirectory(currentDirectory);
        }

        /// <summary>
        /// Checks the file exists.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private bool CheckFileExists(string fileName)
        {
            DataTable dataTable = gridControl.DataSource as DataTable;
            if (dataTable != null && !string.IsNullOrEmpty(fileName))
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    if (row.RowState != DataRowState.Deleted && string.Compare(ConvertEx.ToString(row[columnFileName.FieldName, DataRowVersion.Current]), fileName, true) == 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Downloads the file.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        private void DownloadFile(int rowHandle)
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string docFileName = ConvertEx.ToString(gridView.GetRowCellValue(rowHandle, columnFileName));

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = DIALOG_SAVE_FILE_FILTER;
            saveFileDialog.Title = DIALOG_SAVE_FILE;
            saveFileDialog.FileName = docFileName;
            saveFileDialog.DefaultExt = Path.GetExtension(docFileName);
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if(GetFileData != null)
                {
                    FileDataInfo args = new FileDataInfo();
                    args.Id = ConvertEx.ToInt(gridView.GetRowCellValue(rowHandle, columnId));
                    args.FilePath = saveFileDialog.FileName;
                    args.FileData = ConvertEx.ToBytesArray(gridView.GetRowCellValue(rowHandle, columnFileData));
                    if (args.FileData == null)
                    {
                        GetFileData(this, args);
                    }

                    if(args.FileData != null && args.FileData.Length > 0)
                    {
                        File.WriteAllBytes(args.FilePath, args.FileData);

                        if (XtraMessageBox.Show(string.Format(DIALOG_OPEN_FILE_QUESTION,
                            Path.GetFileName(args.FilePath)), Application.ProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            ProcessStartInfo processStartInfo = new ProcessStartInfo(args.FilePath);
                            processStartInfo.UseShellExecute = true;
                            Process.Start(processStartInfo);
                        }
                    }
                }
            }
            Directory.SetCurrentDirectory(currentDirectory);
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        private void DeleteFile(int rowHandle)
        {
            if (rowHandle >= 0)
            {
                string docFileName = ConvertEx.ToString(gridView.GetRowCellValue(rowHandle, columnFileName));

                if (XtraMessageBox.Show(string.Format(DIALOG_DELETE_FILE_QUESTION, docFileName),
                    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    gridView.DeleteRow(rowHandle);
                    FireEditValuChanged();
                }
            }
        }

        /// <summary>
        /// Inserts the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileData">The file data.</param>
        public void InsertFile(string fileName, byte[] fileData)
        {
            if (!string.IsNullOrEmpty(fileName) && fileData != null && fileData.Length > 0)
            {
                gridView.AddNewRow();
                gridView.SetFocusedRowCellValue(columnFileName, fileName);
                gridView.SetFocusedRowCellValue(columnFileData, fileData);
                gridView.UpdateCurrentRow();

                FireEditValuChanged();
            }
        }

        /// <summary>
        /// Fires the edit valu changed.
        /// </summary>
        private void FireEditValuChanged()
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(this, new EventArgs());
            }
            UpdateState();
        }

        /// <summary>
        /// Handles the Click event of the btnLoad control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnLoad_Click(object sender, EventArgs e)
        {
            UploadFile();
        }

        /// <summary>
        /// Handles the RowCellClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs"/> instance containing the event data.</param>
        private void gridView_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column.Equals(columnFileName))
            {
                DownloadFile(e.RowHandle);
            }
            else if (e.Column.Equals(columnDelete))
            {
                DeleteFile(e.RowHandle);
            }
        }

        #region IControlReadOnly Members

        public bool ReadOnly
        {
            get
            {
                return readOnly;
            }
            set
            {
                readOnly = value;
                UpdateState();
            }
        }

        #endregion
    }
}
