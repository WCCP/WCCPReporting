﻿namespace Logica.Reports.BaseReportControl.CommonControls
{
    partial class FileUploader
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileUploader));
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.columnFileData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDelete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDeleteImage = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection = new DevExpress.Utils.ImageCollection();
            ((System.ComponentModel.ISupportInitialize) (this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemHyperLinkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemDeleteImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.imageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnLoad.Location = new System.Drawing.Point(121, 93);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(131, 23);
            this.btnLoad.TabIndex = 9;
            this.btnLoad.Text = "Загрузить файл";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // gridControl
            // 
            this.gridControl.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit,
            this.repositoryItemDeleteImage});
            this.gridControl.Size = new System.Drawing.Size(373, 87);
            this.gridControl.TabIndex = 8;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnFileName,
            this.columnFileData,
            this.columnDelete});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Images = this.imageCollection;
            this.gridView.Name = "gridView";
            this.gridView.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gridView.OptionsView.ShowColumnHeaders = false;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.OptionsView.ShowVertLines = false;
            this.gridView.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView_RowCellClick);
            // 
            // columnId
            // 
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "Presentation_ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowSize = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.OptionsColumn.ShowCaption = false;
            this.columnId.OptionsColumn.ShowInCustomizationForm = false;
            this.columnId.Width = 20;
            // 
            // columnFileName
            // 
            this.columnFileName.Caption = "Документ";
            this.columnFileName.ColumnEdit = this.repositoryItemHyperLinkEdit;
            this.columnFileName.FieldName = "Presentation_FileName";
            this.columnFileName.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.columnFileName.Name = "columnFileName";
            this.columnFileName.OptionsColumn.AllowEdit = false;
            this.columnFileName.OptionsColumn.ShowCaption = false;
            this.columnFileName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnFileName.Visible = true;
            this.columnFileName.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit
            // 
            this.repositoryItemHyperLinkEdit.AutoHeight = false;
            this.repositoryItemHyperLinkEdit.Name = "repositoryItemHyperLinkEdit";
            this.repositoryItemHyperLinkEdit.SingleClick = true;
            // 
            // columnFileData
            // 
            this.columnFileData.Caption = "Данные";
            this.columnFileData.FieldName = "Presentation_FileData";
            this.columnFileData.Name = "columnFileData";
            this.columnFileData.OptionsColumn.AllowEdit = false;
            this.columnFileData.OptionsColumn.ReadOnly = true;
            this.columnFileData.OptionsColumn.ShowCaption = false;
            this.columnFileData.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // columnDelete
            // 
            this.columnDelete.Caption = "Удалить";
            this.columnDelete.ColumnEdit = this.repositoryItemDeleteImage;
            this.columnDelete.MaxWidth = 20;
            this.columnDelete.Name = "columnDelete";
            this.columnDelete.OptionsColumn.AllowEdit = false;
            this.columnDelete.OptionsColumn.AllowSize = false;
            this.columnDelete.OptionsColumn.ReadOnly = true;
            this.columnDelete.OptionsColumn.ShowCaption = false;
            this.columnDelete.OptionsColumn.ShowInCustomizationForm = false;
            this.columnDelete.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.columnDelete.Visible = true;
            this.columnDelete.VisibleIndex = 1;
            this.columnDelete.Width = 20;
            // 
            // repositoryItemDeleteImage
            // 
            this.repositoryItemDeleteImage.AutoHeight = false;
            this.repositoryItemDeleteImage.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDeleteImage.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", null, 0)});
            this.repositoryItemDeleteImage.LargeImages = this.imageCollection;
            this.repositoryItemDeleteImage.Name = "repositoryItemDeleteImage";
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer) (resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "del_24.png");
            // 
            // FileUploader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.gridControl);
            this.Name = "FileUploader";
            this.Size = new System.Drawing.Size(373, 122);
            ((System.ComponentModel.ISupportInitialize) (this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemHyperLinkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemDeleteImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.imageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnDelete;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemDeleteImage;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraGrid.Columns.GridColumn columnFileName;
        private DevExpress.XtraGrid.Columns.GridColumn columnFileData;
    }
}
