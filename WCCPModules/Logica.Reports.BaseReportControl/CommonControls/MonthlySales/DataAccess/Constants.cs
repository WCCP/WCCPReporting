﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.BaseReportControl.CommonControls.MonthlySales.DataAccess
{
    internal class Constants
    {
        public const string AskRecordDelete = "Вы действительно хотите удалить запись?";
        public const string FieldIsEmpty = "Поле не заполнено. Дозаполнить его?";        

        public const string ProcGetConcurrentBrands = "spDW_GetConcurrentBrands";
    }
}
