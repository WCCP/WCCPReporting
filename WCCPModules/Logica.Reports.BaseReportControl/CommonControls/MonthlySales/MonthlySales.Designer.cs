﻿namespace Logica.Reports.BaseReportControl.CommonControls.Editors
{
    partial class MonthlySales
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnMarkId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.markEditor = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columnMarkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnKeg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositorySpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.columnBottle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCan = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.markEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositorySpinEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.First.Enabled = false;
            this.gridControl.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Last.Enabled = false;
            this.gridControl.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Next.Enabled = false;
            this.gridControl.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.NextPage.Enabled = false;
            this.gridControl.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Prev.Enabled = false;
            this.gridControl.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.gridControl.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl.EmbeddedNavigator.TextStringFormat = "{0} из {1}";
            this.gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl_EmbeddedNavigator_ButtonClick);
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositorySpinEdit,
            this.markEditor});
            this.gridControl.Size = new System.Drawing.Size(376, 151);
            this.gridControl.TabIndex = 4;
            this.gridControl.UseEmbeddedNavigator = true;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnMarkId,
            this.columnMarkName,
            this.columnKeg,
            this.columnBottle,
            this.columnPet,
            this.columnCan});
            this.gridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.NewItemRowText = "Введите нового конкурнета здесь";
            this.gridView.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.OptionsView.ShowFooter = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            this.gridView.ShownEditor += new System.EventHandler(this.gridView_ShownEditor);
            this.gridView.HiddenEditor += new System.EventHandler(this.gridView_HiddenEditor);
            this.gridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView_ValidateRow);
            // 
            // columnMarkId
            // 
            this.columnMarkId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMarkId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMarkId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMarkId.Caption = "Марка";
            this.columnMarkId.ColumnEdit = this.markEditor;
            this.columnMarkId.FieldName = "PRODUCTTYPE_ID";
            this.columnMarkId.Name = "columnMarkId";
            this.columnMarkId.OptionsColumn.ShowInCustomizationForm = false;
            this.columnMarkId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnMarkId.SummaryItem.DisplayFormat = "Итого:";
            this.columnMarkId.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.columnMarkId.Visible = true;
            this.columnMarkId.VisibleIndex = 0;
            this.columnMarkId.Width = 125;
            // 
            // markEditor
            // 
            this.markEditor.AutoHeight = false;
            this.markEditor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.markEditor.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ConcurrentBrandName", "Конкурент")});
            this.markEditor.DisplayMember = "ConcurrentBrandName";
            this.markEditor.Name = "markEditor";
            this.markEditor.NullText = "";
            this.markEditor.ShowFooter = false;
            this.markEditor.ShowHeader = false;
            this.markEditor.ShowLines = false;
            this.markEditor.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.markEditor.ValueMember = "ID";
            this.markEditor.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.markEditor_EditValueChanging);
            // 
            // columnMarkName
            // 
            this.columnMarkName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMarkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMarkName.Caption = "Марка";
            this.columnMarkName.FieldName = "PRODTYPENAME";
            this.columnMarkName.Name = "columnMarkName";
            this.columnMarkName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnMarkName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnMarkName.SummaryItem.DisplayFormat = "Итого:";
            this.columnMarkName.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            // 
            // columnKeg
            // 
            this.columnKeg.AppearanceHeader.Options.UseTextOptions = true;
            this.columnKeg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnKeg.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnKeg.Caption = "Кега";
            this.columnKeg.ColumnEdit = this.repositorySpinEdit;
            this.columnKeg.DisplayFormat.FormatString = "{0:N2}";
            this.columnKeg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnKeg.FieldName = "KEGA";
            this.columnKeg.Name = "columnKeg";
            this.columnKeg.OptionsColumn.ShowInCustomizationForm = false;
            this.columnKeg.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnKeg.SummaryItem.DisplayFormat = "{0:N2}";
            this.columnKeg.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.columnKeg.Visible = true;
            this.columnKeg.VisibleIndex = 1;
            this.columnKeg.Width = 61;
            // 
            // repositorySpinEdit
            // 
            this.repositorySpinEdit.AutoHeight = false;
            this.repositorySpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositorySpinEdit.Mask.EditMask = "n2";
            this.repositorySpinEdit.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.repositorySpinEdit.Name = "repositorySpinEdit";
            // 
            // columnBottle
            // 
            this.columnBottle.AppearanceHeader.Options.UseTextOptions = true;
            this.columnBottle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnBottle.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnBottle.Caption = "Бутылка";
            this.columnBottle.ColumnEdit = this.repositorySpinEdit;
            this.columnBottle.DisplayFormat.FormatString = "{0:N2}";
            this.columnBottle.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnBottle.FieldName = "BOTTLE";
            this.columnBottle.Name = "columnBottle";
            this.columnBottle.OptionsColumn.ShowInCustomizationForm = false;
            this.columnBottle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnBottle.SummaryItem.DisplayFormat = "{0:N2}";
            this.columnBottle.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.columnBottle.Visible = true;
            this.columnBottle.VisibleIndex = 2;
            this.columnBottle.Width = 61;
            // 
            // columnPet
            // 
            this.columnPet.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPet.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPet.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPet.Caption = "ПЕТ";
            this.columnPet.ColumnEdit = this.repositorySpinEdit;
            this.columnPet.DisplayFormat.FormatString = "{0:N2}";
            this.columnPet.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPet.FieldName = "PET";
            this.columnPet.Name = "columnPet";
            this.columnPet.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPet.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPet.SummaryItem.DisplayFormat = "{0:N2}";
            this.columnPet.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.columnPet.Visible = true;
            this.columnPet.VisibleIndex = 3;
            this.columnPet.Width = 61;
            // 
            // columnCan
            // 
            this.columnCan.AppearanceCell.Options.UseTextOptions = true;
            this.columnCan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.columnCan.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCan.Caption = "Банка";
            this.columnCan.ColumnEdit = this.repositorySpinEdit;
            this.columnCan.DisplayFormat.FormatString = "{0:N2}";
            this.columnCan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnCan.FieldName = "CAN";
            this.columnCan.Name = "columnCan";
            this.columnCan.OptionsColumn.ShowInCustomizationForm = false;
            this.columnCan.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCan.SummaryItem.DisplayFormat = "{0:N2}";
            this.columnCan.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.columnCan.Visible = true;
            this.columnCan.VisibleIndex = 4;
            this.columnCan.Width = 64;
            // 
            // MonthlySales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Name = "MonthlySales";
            this.Size = new System.Drawing.Size(376, 151);
            this.Load += new System.EventHandler(this.MonthlySales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.markEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositorySpinEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnMarkId;
        private DevExpress.XtraGrid.Columns.GridColumn columnKeg;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositorySpinEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnBottle;
        private DevExpress.XtraGrid.Columns.GridColumn columnPet;
        private DevExpress.XtraGrid.Columns.GridColumn columnCan;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit markEditor;
        private DevExpress.XtraGrid.Columns.GridColumn columnMarkName;

    }
}
