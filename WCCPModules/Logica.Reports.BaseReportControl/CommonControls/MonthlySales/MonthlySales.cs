﻿using System;
using System.Windows.Forms;
using System.Data;
using System.ComponentModel;
using DevExpress.XtraEditors;
using System.Globalization;
using DevExpress.XtraEditors.Controls;
using System.Linq;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl.Utility;
using System.Data.SqlTypes;
using Logica.Reports.BaseReportControl.CommonControls.MonthlySales.DataAccess;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System.Collections.Generic;
using Logica.Reports.BaseReportControl.Helpers;

namespace Logica.Reports.BaseReportControl.CommonControls.Editors
{
    /// <summary>
    /// 
    /// </summary>
    public partial class MonthlySales : XtraUserControl
    {
        //
        // Summary:
        //     Fires immediately after the edit value has been changed.
        [Category("Events")]
        [Description("Fires immediately after the edit value has been changed.")]
        public event EventHandler EditValueChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonthlySales"/> class.
        /// </summary>
        public MonthlySales()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Load event of the MonthlySales control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MonthlySales_Load(object sender, EventArgs e)
        {
            if (!ReadOnly && !DesignMode)
            {
                // bind data source to repository if this is not readonly mode
                markEditor.DataSource = DataProvider.GetConcurrentBrands();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [read only].
        /// </summary>
        /// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
        [Description("ReadOnly")]
        [DefaultValue(false)]
        public Boolean ReadOnly 
        {
            get 
            {
                foreach (GridColumn column in gridView.Columns) 
                {
                    if (!column.OptionsColumn.ReadOnly) 
                    {
                        return false;
                    }
                }

                return !gridControl.UseEmbeddedNavigator;
            }
            set
            {
                bool allowEdit = !value;

                foreach (GridColumn column in gridView.Columns)
                {
                    column.OptionsColumn.ReadOnly = !allowEdit;
                    column.OptionsColumn.AllowEdit = allowEdit;
                }

                gridControl.UseEmbeddedNavigator = allowEdit;

                columnMarkId.Visible = allowEdit;
                columnMarkName.Visible = !allowEdit;

                GridColumn columnVisible = columnMarkId.Visible ? columnMarkId : columnMarkName;
                columnVisible.VisibleIndex = 0;
            }
        }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>The data source.</value>
        [Description("The data source of the control")]
        public DataTable DataSource 
        {
            get
            {
                return this.gridControl.DataSource as DataTable; 
            }
            set
            {
                this.gridControl.DataSource = value;
            }
        }

        /// <summary>
        /// Gets the data XML.
        /// </summary>
        /// <value>The data XML.</value>
        public SqlXml DataXml
        {
            get
            {
                return ConvertEx.ToSqlXml(this.DataSource, new []
                {
                    columnMarkId.FieldName,
                    columnKeg.FieldName,
                    columnBottle.FieldName,
                    columnCan.FieldName,                    
                    columnPet.FieldName
                });
            }
        }

        /// <summary>
        /// Handles the EditValueChanging event of the merkEditor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.Controls.ChangingEventArgs"/> instance containing the event data.</param>
        private void markEditor_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (this.DataSource != null && this.DataSource.Rows.Cast<DataRow>()
                .Where(x => x.RowState != DataRowState.Deleted && x.Field<Int32?>(this.columnMarkId.FieldName).HasValue && x.Field<Int32>(this.columnMarkId.FieldName) == (Int32)e.NewValue).Count() > 0)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Handles the ButtonClick event of the gridControl_EmbeddedNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.NavigatorButtonClickEventArgs"/> instance containing the event data.</param>
        private void gridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                if (XtraMessageBox.Show(Constants.AskRecordDelete, Application.ProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Handled = true;
                }
                else
                {
                    FireEditValueChanged();
                }
            }
        }

        /// <summary>
        /// Handles the ValidateRow event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
        private void gridView_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ConvertEx.ToInt(gridView.GetRowCellValue(e.RowHandle, columnMarkId)) > 0;
            if (!e.Valid)
            {
                e.ErrorText = Constants.FieldIsEmpty;
            }
        }

        /// <summary>
        /// Handles the CellValueChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            FireEditValueChanged();
        }

        /// <summary>
        /// Fires the edit value changed.
        /// </summary>
        private void FireEditValueChanged()
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(this, new EventArgs());
            }
        }

        private DataView clone = null;

        private void gridView_HiddenEditor(object sender, EventArgs e)
        {
            if (clone != null)
            {
                clone.Dispose();
                clone = null;
            }
        }

        private void gridView_ShownEditor(object sender, EventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedColumn == columnMarkId && view.ActiveEditor is LookUpEdit)
            {
                if (!(view.GridControl.DataSource is DataTable))
                    return;
                DataTable dt = (DataTable)view.GridControl.DataSource;

                IEnumerable<int> usedMarks = dt.Rows
                    .Cast<DataRow>()
                    .Select(r => r.Field<int>(columnMarkId.FieldName))
                    .Where(i => i != ConvertEx.ToInt(view.ActiveEditor.EditValue));
                if (usedMarks.Count() == 0)
                    return;
                LookUpEdit edit = (LookUpEdit)view.ActiveEditor;
                
                DataTable table = edit.Properties.DataSource as DataTable;
                clone = new DataView(table);
                DataRow row = view.GetDataRow(view.FocusedRowHandle);
                clone.RowFilter = String.Format("[{0}] NOT IN ({1}) ", markEditor.ValueMember, usedMarks.ToString(","));
                edit.Properties.DataSource = clone;
            }
        }
    }
}
