using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions
{
    partial class SKUTreeConditions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tree = new DevExpress.XtraTreeList.TreeList();
            this.columnConditionTreeSkuId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuChecked = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuProductId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuOLID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuBeginDate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuFixed = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.columnConditionTreeSkuTotalPlan = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnConditionTreeSkuTotalFact = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemSpinEditPlan = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            ((System.ComponentModel.ISupportInitialize) (this.tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemSpinEditPlan)).BeginInit();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.tree.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tree.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.tree.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.tree.ColumnPanelRowHeight = 40;
            this.tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnConditionTreeSkuId,
            this.columnConditionTreeSkuParentId,
            this.columnConditionTreeSkuName,
            this.columnConditionTreeSkuChecked,
            this.columnConditionTreeSkuProductId,
            this.columnConditionTreeSkuOLID,
            this.columnConditionTreeSkuBeginDate,
            this.columnConditionTreeSkuFixed,
            this.columnConditionTreeSkuTotalPlan,
            this.columnConditionTreeSkuTotalFact});
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Name = "tree";
            this.tree.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.tree.OptionsView.AutoWidth = false;
            this.tree.OptionsView.ShowCheckBoxes = true;
            this.tree.ParentFieldName = "PARENT_ID";
            this.tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEditPlan,
            this.repositoryItemCheckEdit1});
            this.tree.Size = new System.Drawing.Size(865, 447);
            this.tree.TabIndex = 1;
            this.tree.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.tree_BeforeCheckNode);
            this.tree.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterCheckNode);
            this.tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this.tree.CellValueChanging += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tree_CellValueChanging);
            this.tree.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tree_CellValueChanged);
            this.tree.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.tree_ShowingEditor);
            // 
            // columnConditionTreeSkuId
            // 
            this.columnConditionTreeSkuId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuId.Caption = "ID";
            this.columnConditionTreeSkuId.FieldName = "ID";
            this.columnConditionTreeSkuId.Name = "columnConditionTreeSkuId";
            // 
            // columnConditionTreeSkuParentId
            // 
            this.columnConditionTreeSkuParentId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuParentId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuParentId.Caption = "PARENT_ID";
            this.columnConditionTreeSkuParentId.FieldName = "PARENT_ID";
            this.columnConditionTreeSkuParentId.Name = "columnConditionTreeSkuParentId";
            // 
            // columnConditionTreeSkuName
            // 
            this.columnConditionTreeSkuName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuName.Caption = "������� ��������� �� V ������ �� ���";
            this.columnConditionTreeSkuName.FieldName = "NODE_NAME";
            this.columnConditionTreeSkuName.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnConditionTreeSkuName.Name = "columnConditionTreeSkuName";
            this.columnConditionTreeSkuName.OptionsColumn.AllowEdit = false;
            this.columnConditionTreeSkuName.OptionsColumn.AllowMove = false;
            this.columnConditionTreeSkuName.OptionsColumn.ReadOnly = true;
            this.columnConditionTreeSkuName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnConditionTreeSkuName.Visible = true;
            this.columnConditionTreeSkuName.VisibleIndex = 0;
            // 
            // columnConditionTreeSkuChecked
            // 
            this.columnConditionTreeSkuChecked.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuChecked.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuChecked.Caption = "CHECKED";
            this.columnConditionTreeSkuChecked.FieldName = "CHECKED";
            this.columnConditionTreeSkuChecked.Name = "columnConditionTreeSkuChecked";
            // 
            // columnConditionTreeSkuProductId
            // 
            this.columnConditionTreeSkuProductId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuProductId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuProductId.Caption = "PRODUCT_ID";
            this.columnConditionTreeSkuProductId.FieldName = "PRODUCT_ID";
            this.columnConditionTreeSkuProductId.Name = "columnConditionTreeSkuProductId";
            // 
            // columnConditionTreeSkuOLID
            // 
            this.columnConditionTreeSkuOLID.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuOLID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuOLID.Caption = "OL_ID";
            this.columnConditionTreeSkuOLID.FieldName = "OL_ID";
            this.columnConditionTreeSkuOLID.Name = "columnConditionTreeSkuOLID";
            // 
            // columnConditionTreeSkuBeginDate
            // 
            this.columnConditionTreeSkuBeginDate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuBeginDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuBeginDate.Caption = "BEGIN_DATE";
            this.columnConditionTreeSkuBeginDate.FieldName = "BEGIN_DATE";
            this.columnConditionTreeSkuBeginDate.Name = "columnConditionTreeSkuBeginDate";
            // 
            // columnConditionTreeSkuFixed
            // 
            this.columnConditionTreeSkuFixed.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuFixed.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnConditionTreeSkuFixed.Caption = "�� ������������� V";
            this.columnConditionTreeSkuFixed.ColumnEdit = this.repositoryItemCheckEdit1;
            this.columnConditionTreeSkuFixed.FieldName = "FIXED";
            this.columnConditionTreeSkuFixed.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnConditionTreeSkuFixed.Name = "columnConditionTreeSkuFixed";
            this.columnConditionTreeSkuFixed.Visible = true;
            this.columnConditionTreeSkuFixed.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // columnConditionTreeSkuTotalPlan
            // 
            this.columnConditionTreeSkuTotalPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuTotalPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuTotalPlan.Caption = "����� ����";
            this.columnConditionTreeSkuTotalPlan.FieldName = "PLAN_TOTAL";
            this.columnConditionTreeSkuTotalPlan.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnConditionTreeSkuTotalPlan.Name = "columnConditionTreeSkuTotalPlan";
            this.columnConditionTreeSkuTotalPlan.OptionsColumn.AllowEdit = false;
            this.columnConditionTreeSkuTotalPlan.OptionsColumn.ReadOnly = true;
            this.columnConditionTreeSkuTotalPlan.Visible = true;
            this.columnConditionTreeSkuTotalPlan.VisibleIndex = 2;
            // 
            // columnConditionTreeSkuTotalFact
            // 
            this.columnConditionTreeSkuTotalFact.AppearanceHeader.Options.UseTextOptions = true;
            this.columnConditionTreeSkuTotalFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnConditionTreeSkuTotalFact.Caption = "����� ����";
            this.columnConditionTreeSkuTotalFact.FieldName = "FACT_TOTAL";
            this.columnConditionTreeSkuTotalFact.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnConditionTreeSkuTotalFact.Name = "columnConditionTreeSkuTotalFact";
            this.columnConditionTreeSkuTotalFact.OptionsColumn.AllowEdit = false;
            this.columnConditionTreeSkuTotalFact.OptionsColumn.ReadOnly = true;
            this.columnConditionTreeSkuTotalFact.Visible = true;
            this.columnConditionTreeSkuTotalFact.VisibleIndex = 3;
            // 
            // repositoryItemSpinEditPlan
            // 
            this.repositoryItemSpinEditPlan.AutoHeight = false;
            this.repositoryItemSpinEditPlan.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditPlan.DisplayFormat.FormatString = "{0:N3}";
            this.repositoryItemSpinEditPlan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPlan.EditFormat.FormatString = "{0:N3}";
            this.repositoryItemSpinEditPlan.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPlan.Mask.EditMask = "\\d{0,6}\\.\\d{3}";
            this.repositoryItemSpinEditPlan.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEditPlan.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.repositoryItemSpinEditPlan.Name = "repositoryItemSpinEditPlan";
            // 
            // SKUTreeConditions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tree);
            this.Name = "SKUTreeConditions";
            this.Size = new System.Drawing.Size(865, 447);
            ((System.ComponentModel.ISupportInitialize) (this.tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemSpinEditPlan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList tree;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuProductId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuOLID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuChecked;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuBeginDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuTotalPlan;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnConditionTreeSkuTotalFact;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPlan;
        private TreeListColumn columnConditionTreeSkuFixed;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    }
}
