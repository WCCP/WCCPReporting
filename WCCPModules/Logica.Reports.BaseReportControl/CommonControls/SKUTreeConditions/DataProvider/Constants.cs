﻿using System;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using System.Drawing;

namespace Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions.DataAccess
{
    internal class Constants
    {
        public const string DYNAMIC_COLUMN_PREFIX_DB = "PERIOD_";
        public const string DYNAMIC_COLUMN_PREFIX_PLAN_DB = "PERIOD_PLAN";
        public const string DYNAMIC_COLUMN_PREFIX_FACT_DB = "PERIOD_FACT";
        public const string COLUMN_PREFIX_DEVEXPR = "col";
        public const string ColumnFixedFieldName = "FIXED";
        public const string ColumnCheckedFieldName = "CHECKED";

        public const string LABEL_PLAN = "План";
        public const string LABEL_FACT = "Факт";
        public const string DELIMITER_ID_LIST = ",";

        public const string LABEL_INVALID = " (Заполнено не корректно)";

        public static Brush BrushGray = new SolidBrush(SystemColors.Control);

        //*****************************************************************************************************************
        // Stored Procedures
        //*****************************************************************************************************************
        public const string SP_GET_CONDITIONS_BY_SKU = "spDW_CM_CT_CONTRACT_VOLUME_TERMS_TREE";
        public const string SP_GET_CONDITIONS_DYNAMIC_CALC = "spDW_CM_CT_CALCULATE_PLAN_VOLUMES";
        

        //*****************************************************************************************************************
        // Stored Procedures params
        //*****************************************************************************************************************
        public const string SP_GET_CONDITIONS_BY_SKU_PARAM1 = "CONTRACT_ID";
        public const string SP_GET_CONDITIONS_DYNAMIC_CALC_PARAM1 = "CONTRACT_ID";
        public const string SP_GET_CONDITIONS_DYNAMIC_CALC_PARAM2 = "XML_DATA";   
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SKUTreeConditionsEditMode
    {
        /// <summary>
        /// 
        /// </summary>
        Common = 0,
        /// <summary>
        /// 
        /// </summary>
        ReadOnly = 1,
        /// <summary>
        /// 
        /// </summary>
        AllowCheckSkuOnly = 2

    }
}
