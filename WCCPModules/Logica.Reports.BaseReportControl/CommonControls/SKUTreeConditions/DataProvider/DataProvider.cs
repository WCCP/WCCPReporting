﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using Logica.Reports.Common;
using System.Data.SqlClient;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Data.SqlTypes;

namespace Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions.DataAccess
{
    internal class DataProvider
    {
        /// <summary>
        /// Gets the conditions by SKU.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetConditionsBySKU(int contractId)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_CONDITIONS_BY_SKU,
                new SqlParameter[] 
                { 
                    new SqlParameter(Constants.SP_GET_CONDITIONS_BY_SKU_PARAM1, contractId)
                });
        }

        /// <summary>
        /// Gets the conditions dynamic calc.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <param name="sqlXml">The SQL XML.</param>
        /// <returns></returns>
        internal static DataTable GetConditionsDynamicCalc(int contractId, SqlXml sqlXml)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_CONDITIONS_DYNAMIC_CALC,
                new SqlParameter[] 
                { 
                    new SqlParameter(Constants.SP_GET_CONDITIONS_DYNAMIC_CALC_PARAM1, contractId),
                    new SqlParameter(Constants.SP_GET_CONDITIONS_DYNAMIC_CALC_PARAM2, sqlXml),
                });
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName)
        {
            return ExecuteStoredProcedure(spName, null);
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <param name="paramList">The param list.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName, params SqlParameter[] paramList)
        {
            WaitManager.StartWait();
            DataTable dataTable = null;
            try
            {
                DataSet dataSet = null;
                if (paramList != null)
                {
                    dataSet = DataAccessLayer.ExecuteStoredProcedure(spName, paramList);
                }
                else
                {
                    dataSet = DataAccessLayer.ExecuteStoredProcedure(spName);
                }
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            WaitManager.StopWait();
            return dataTable;
        }

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void HandleException(Exception ex)
        {
            string message = string.Empty;
            if (ex != null)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    message = ex.Message;
                }
                else if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    message = ex.InnerException.Message;
                }
            }
            if (!string.IsNullOrEmpty(message))
            {
                XtraMessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
