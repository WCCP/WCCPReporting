using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions.DataAccess;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.BaseReportControl.Helpers;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;

namespace Logica.Reports.BaseReportControl.CommonControls.SKUTreeConditions {
    /// <summary>
    /// 
    /// </summary>
    public partial class SKUTreeConditions : XtraUserControl {

        private readonly Collection<DateTime> _dateCollection = new Collection<DateTime>();
        private SKUTreeConditionsEditMode _editMode;
        private readonly Collection<TreeListNode> _initiallyCheckedNodeCollection = new Collection<TreeListNode>();
        private readonly Collection<TreeListNode> _justCheckedNodeCollection = new Collection<TreeListNode>();
        private readonly Collection<TreeListNode> _justUnCheckedNodeCollection = new Collection<TreeListNode>();
        // Inner collection that is used in order to track modified column's indexes.
        private readonly List<int> _modifiedColumns = new List<int>();

        const string FieldNameOldCoef = "old_coef";
        const string FieldNameNewCoef = "new_coef";
        const string FieldNameOldValue = "old_value";
        const string FieldNameNewValue = "new_value";
        const string FieldNameNewValueRounded = "new_value_rounded";
        const string FieldNameDelta = "delta";

        /// <summary>
        /// Initializes a new instance of the <see cref="SKUTreeConditions"/> class.
        /// </summary>
        public SKUTreeConditions() {
            InitializeComponent();
            IsRecalcImpossible = false;
            LastEditedNodeKey = null;
            TreeDataTable = null;
        }

        private DateTime DateStart { get; set; }
        private int ContractId { get; set; }
        private decimal PreviousCellValue { get; set; }
        private bool IsRecalcImpossible { get; set; }
        private string LastEditedNodeKey { get; set; }

        public bool IsChanged;
        private DataTable _treeDataTable;

        private DataTable TreeDataTable {
            get { return _treeDataTable; }
            set { _treeDataTable = value; }
        }

        public bool IsAllowAddSku { get; set; }

        [Browsable(false)]
        public string IdColumn {
            get { return tree.KeyFieldName; }
        }

        /// <summary>
        /// Gets or sets the edit mode.
        /// </summary>
        /// <value>The edit mode.</value>
        public SKUTreeConditionsEditMode EditMode {
            get { return _editMode; }
            set {
                _editMode = value;
                UpdateJustCheckedNodesDynamically();
                tree.Refresh();
            }
        }

        /// <summary>
        /// Gets the selected conditions XML.
        /// </summary>
        /// <value>The selected conditions XML.</value>
        public SqlXml SelectedConditionsXml {
            get {
                tree.PostEditor();
                DataTable lDataTable = tree.DataSource as DataTable;
                if (lDataTable != null) {
                    lDataTable.AcceptChanges();
                    DataView lDataView = new DataView(lDataTable);

                    lDataView.RowFilter = string.Format("{0} IS NOT NULL AND {1} = True", columnConditionTreeSkuProductId.FieldName,
                        columnConditionTreeSkuChecked.FieldName);
                    return ConvertEx.ToSqlXmlAll(lDataView.ToTable());
                }
                return null;
            }
        }

        public TreeList TreeList {
            get { return tree; }
        }

        //
        // Summary:
        //     Fires immediately after the edit value has been changed.
        [Category("Events")]
        [Description("Fires immediately after the edit value has been changed.")]
        public event CellValueChangedEventHandler EditValueChanged;

        [Category("Events")]
        [Description("Fires immediately SKU node checked/unchecked in tree.")]
        public event CellValueChangedEventHandler SKUStructureChanged;

        /// <summary>
        /// Loads the specified data table.
        /// </summary>
        /// <param name="contractId">The contract Id</param>
        public void Load(int contractId) {
            ContractId = contractId;

            AttachEvents(false);
            tree.BeginUpdate();
            try {
                CultureInfo lCi = Thread.CurrentThread.CurrentCulture;
                repositoryItemSpinEditPlan.EditMask = @"\d{0,6}\" + lCi.NumberFormat.NumberDecimalSeparator + @"\d{0,3}";
                LoadConditions();
                LoadTreeState();
                LoadFixedState();
                CheckPlanVolumeTotals(null);
                RecalcAllNodes();

                if (tree.Nodes.FirstNode != null)
                    tree.Nodes.FirstNode.Expanded = true;

                if (null != LastEditedNodeKey) {
                    TreeListNode lNode = tree.FindNodeByKeyID(LastEditedNodeKey);
                    if (null != lNode)
                        tree.FocusedNode = lNode;
                }
            }
            finally {
                tree.EndUpdate();
                AttachEvents(true);
            }
        }

        private void LoadFixedState() {
            TreeListNode lCashedParent = null;
            foreach (TreeListNode lNode in _initiallyCheckedNodeCollection) {
                TreeListNode lParentNode = lNode.ParentNode;
                bool lFixed = ConvertEx.ToBool(lNode.GetValue(columnConditionTreeSkuFixed));
                if (lCashedParent == lParentNode)
                    continue;

                lCashedParent = lParentNode;
                while (lParentNode != null) {
                    OnNodeFixParent(lParentNode, lFixed);
                    lParentNode = lParentNode.ParentNode;
                }
            }
            
        }

        /// <summary>
        /// Check Plan volume totals for column or all columns
        /// </summary>
        /// <param name="column">TreeListColumn to check totals</param>
        private void CheckPlanVolumeTotals(TreeListColumn column) {
            DataTable lDataTable = tree.DataSource as DataTable;
            if (lDataTable == null)
                return;

            lDataTable.AcceptChanges();
            DataView lDataView1 = new DataView(lDataTable);
            // where CHECKED = 1 AND PRODUCT_ID IS NOT NULL
            lDataView1.RowFilter = string.Format("{0} = 1 AND {1} IS NOT NULL", Constants.ColumnCheckedFieldName, columnConditionTreeSkuProductId.FieldName);
            if (lDataView1.Count <= 0)
                return;

            DataTable lFilteredTable1 = lDataView1.ToTable();
            lFilteredTable1.PrimaryKey = new DataColumn[] {lFilteredTable1.Columns[columnConditionTreeSkuId.FieldName]};

            // add additional fields to table
            PrepareTable(lFilteredTable1, column);

            if (column == null) {
                foreach (TreeListColumn lColumn in tree.Columns) {
                    if (!IsPlanPeriodColumn(lColumn))
                        continue;
                    CheckPlanTotalsByColumn(lFilteredTable1, lColumn);
                }
            }
            else
                CheckPlanTotalsByColumn(lFilteredTable1, column);
        }

        /// <summary>
        /// CHeck and updates Plan Volume for specified column
        /// </summary>
        /// <param name="filteredTable1">DataTable contains all checked in tree SKU (leaf level)</param>
        /// <param name="column">TreeListColumn for calc</param>
        private void CheckPlanTotalsByColumn(DataTable filteredTable1, TreeListColumn column) {
            decimal lNewValue;
            decimal lNewValueRounded;
            decimal lTotalSum = 0m;
            decimal lFixedSum = 0m;
            int lFixedCount = 0;
            //calc totals by leaf nodes
            foreach (DataRow lRow in filteredTable1.Rows) {
                lTotalSum += ConvertEx.ToDecimal(lRow[column.FieldName]);
                if (ConvertEx.ToBool(lRow[Constants.ColumnFixedFieldName])) {
                    lFixedCount++;
                    lFixedSum += ConvertEx.ToDecimal(lRow[column.FieldName]);
                }
            }

            decimal lRootTotalSum = ConvertEx.ToDecimal(tree.Nodes.FirstNode.RootNode.GetValue(column));
            if (lTotalSum == lRootTotalSum)
                return;

            int lNodesCount = filteredTable1.Rows.Count - lFixedCount;
            decimal lRootTotalWoFixed = lRootTotalSum - lFixedSum;
            decimal lTotalWoFixed = lTotalSum - lFixedSum;
            decimal lNewTotalSum = 0m;
            
            // check V
            if (!ValidatePlanVolume(tree.Nodes.FirstNode.RootNode, column, lNodesCount, lRootTotalWoFixed, lTotalWoFixed, 0, false))
                return;

            // work only with not Fixed nodes
            filteredTable1.AcceptChanges();
            DataView lDataView = new DataView(filteredTable1);
            lDataView.RowFilter = Constants.ColumnFixedFieldName + " = 0";
            if (lDataView.Count <= 0)
                return;

            DataTable lFilteredTable = lDataView.ToTable();
            lFilteredTable.PrimaryKey = new DataColumn[] {lFilteredTable.Columns[columnConditionTreeSkuId.FieldName]};

            foreach (DataRow lRow in lFilteredTable.Rows) {
                decimal lNewCoef;
                if (lTotalWoFixed == 0)
                    lRow[FieldNameNewValue] = lNewValue = lRootTotalWoFixed / lNodesCount;
                else {
                    lRow[FieldNameNewCoef] = lNewCoef = ConvertEx.ToDecimal(lRow[column.FieldName]) / lTotalWoFixed;
                    lRow[FieldNameNewValue] = lNewValue = lNewCoef * lRootTotalWoFixed;
                }
                lRow[FieldNameNewValueRounded] = lNewValueRounded = Math.Round(lNewValue, 3, MidpointRounding.AwayFromZero);
                lRow[FieldNameDelta] = 0;

                lNewTotalSum += lNewValueRounded;
            }

            lTotalSum = lRootTotalWoFixed;
            VolumeCorrection(lFilteredTable, lTotalSum, lNewTotalSum, lNodesCount);

            lFilteredTable.AcceptChanges();
            IsChanged = true;

            // make changes in nodes
            foreach (DataRow lRow in lFilteredTable.Rows) {
                string lId = ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]);
                lNewValue = ConvertEx.ToDecimal(lRow[FieldNameNewValueRounded]);
                TreeListNode lNode = tree.FindNodeByKeyID(lId);
                decimal lOldValue = ConvertEx.ToDecimal(lNode.GetValue(column));
                lNode.SetValue(column, lNewValue);
                //SpreadParentValueToChildren(lNode, lColumn, lOldValue, lNewValue);
            }
        }

        /// <summary>
        /// Implements algorithm for Volume correction in case when new total is not equal to previous total
        ///  after divide volumes for SKU by their weight.
        /// </summary>
        /// <param name="filteredTable">Table with only rows for processing</param>
        /// <param name="totalSum">Total that can't be changed</param>
        /// <param name="newTotalSum">New Total that calculated</param>
        /// <param name="nodesCount">Count of nodes to process except changed row</param>
        /// <param name="newValue">New entered value for column</param>
        /// <param name="keyId">ID of changed row</param>
        private void VolumeCorrection(DataTable filteredTable, decimal totalSum, decimal newTotalSum, int nodesCount, decimal newValue = 0, string keyId = "") {
            if (totalSum == newTotalSum)
                return;
            // correction for V
            decimal lBrutto = 0;
            if (nodesCount != 0)
                lBrutto = (newTotalSum - totalSum) / nodesCount;
            if (lBrutto == 0m)
                return;

            decimal lMaxDelta = -1000m; // any value < -1
            decimal lMinDelta = 1000m;  // any value > 1
            string lMinDeltaKeyId = "";
            string lMaxDeltaKeyId = "";
            lMinDeltaKeyId = lMaxDeltaKeyId = ConvertEx.ToString(filteredTable.Rows[0][columnConditionTreeSkuId.FieldName]);
            lBrutto = totalSum - newTotalSum;
            
            if (Math.Abs(lBrutto) >= 0.001m * nodesCount) {
                lBrutto = lBrutto / nodesCount;
                newTotalSum = 0m;
                foreach (DataRow lRow in filteredTable.Rows) {
                    decimal lDelta;
                    decimal lNewValue;
                    if (ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]) == keyId) {
                        newTotalSum += newValue;
                        continue;
                    }

                    lRow[FieldNameNewValue] = lNewValue = ConvertEx.ToDecimal(lRow[FieldNameNewValue]) + lBrutto;
                    decimal lNewValueRounded = Math.Round(lNewValue, 3, MidpointRounding.AwayFromZero);
                    lRow[FieldNameNewValueRounded] = lNewValueRounded;
                    lRow[FieldNameDelta] = lDelta = lNewValueRounded - lNewValue;
                    newTotalSum += lNewValueRounded;
                    if (lDelta > lMaxDelta) {
                        lMaxDelta = lDelta;
                        lMaxDeltaKeyId = ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]);
                    }
                    if (lDelta < lMinDelta) {
                        lMinDelta = lDelta;
                        lMinDeltaKeyId = ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]);
                    }
                }
            }

            // second step correction V
            if (totalSum != newTotalSum) {
                lBrutto = totalSum - newTotalSum;
                string lId = (lBrutto > 0) ? lMinDeltaKeyId : lMaxDeltaKeyId;
                DataRow lRow = filteredTable.Rows.Find(lId);
                lRow[FieldNameNewValueRounded] = ConvertEx.ToDecimal(lRow[FieldNameNewValueRounded]) + lBrutto;
            }
        }

        /// <summary>
        /// Returns true if all OK, otherwise false. Displays warning message.
        /// </summary>
        /// <param name="node">Parent node or edited node</param>
        /// <param name="column">TreeListColumn checked</param>
        /// <param name="nodesCount">Count of nodes without fixed nodes</param>
        /// <param name="totalWoFixed">Total from Parent node</param>
        /// <param name="calcTotalWoFixed">Calculated Total by child nodes</param>
        /// <param name="newValue">new entered value or spreaded from parent new value</param>
        /// <param name="unCheck"></param>
        /// <returns></returns>
        private bool ValidatePlanVolume(TreeListNode node, TreeListColumn column, int nodesCount, decimal totalWoFixed, decimal calcTotalWoFixed,
                                        decimal newValue, bool unCheck) {
            bool lResult = true;
            string lNodeName = ConvertEx.ToString(node.GetValue(columnConditionTreeSkuName));
            if (unCheck) {
                if (GetCheckedNodesCountLevelDown(node.ParentNode) == GetFixedNodesCountLevelDown(node.ParentNode) + 1) {
                    MessageBox.Show(
                        string.Format("������ ��������� ����� '{0}' ��������� ������ ������������ ����� ����� ���.\n"
                            + "�������������� ����� �� ��� ��� �������� ������� [�� ������������� V].!",
                            lNodeName, totalWoFixed),
                        DxLocalizedResources.DialogCaptionInvalidVolume,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lResult = false;
                }
            }
            else {
                // check: All nodes are Fixed))
                if (nodesCount == 0) {
                    MessageBox.Show(string.Format("���������� ������������ ����� ����� �� ������ [{0}] ����� ���.\n"
                        + "��� ��� ����� ������� [�� ������������� V]!\n"
                            + "�������������� ����� �� �������� ���������� ��� �������� ������� [�� ������������� V].",
                        column.Caption.Replace("\r\n", " ")),
                        DxLocalizedResources.DialogCaptionInvalidVolume, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lResult = false;
                }
                else
                    // check: Fixed nodes total should be <= Root node Total
                    if (totalWoFixed < 0) {
                        MessageBox.Show(string.Format("���������� ������������ ����� ����� �� ������ [{0}] ����� ���.\n"
                            + "����� �� ��� � [�� ������������� V] ������ ����� �� ���� ���!\n"
                                + "�������������� ����� �� �������� ���������� ��� ����� ��� � [�� ������������� V].",
                            column.Caption.Replace("\r\n", " ")),
                            DxLocalizedResources.DialogCaptionInvalidVolume, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lResult = false;
                    }
                    else
                        // check: All non Fixed nodes have zero volume
                        if (calcTotalWoFixed < 0) {
                            MessageBox.Show(string.Format("���������� ������������ ����� ����� �� ������ [{0}] ����� ���.\n"
                                + "��� ���, � ������� �� ������� ����� ����� ������� [�� ������������� V]!\n"
                                    +
                                    "�������������� ����� �� �������� ���������� ��� ����� ��� � [�� ������������� V] ��� �������� ������� [�� ������������� V].",
                                column.Caption.Replace("\r\n", " ")),
                                DxLocalizedResources.DialogCaptionInvalidVolume, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            lResult = false;
                        }
                        else
                            //if Total Sum < new entered value revert edit with warning
                            if (totalWoFixed < newValue) {
                                MessageBox.Show(
                                    string.Format(
                                        "��������� �������� {0} �� ����� ���� ������ {1} ([����� ������ �� ����� �� ������� ������] � [����� ���� ���������� [�� ������������� V] ���] !",
                                        newValue, totalWoFixed),
                                    DxLocalizedResources.DialogCaptionInvalidVolume,
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                lResult = false;
                            }
            }

            if (!IsRecalcImpossible)
                IsRecalcImpossible = !lResult;

            return lResult;
        }

        /// <summary>
        /// Attaches the events.
        /// </summary>
        /// <param name="attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach) {
                tree.AfterCheckNode += tree_AfterCheckNode;
                tree.BeforeCheckNode += tree_BeforeCheckNode;
                tree.CellValueChanged += tree_CellValueChanged;
            }
            else {
                tree.CellValueChanged -= tree_CellValueChanged;
                tree.BeforeCheckNode -= tree_BeforeCheckNode;
                tree.AfterCheckNode -= tree_AfterCheckNode;
            }
        }

        /// <summary>
        /// Loads the state of the tree.
        /// </summary>
        private void LoadTreeState() {
            tree.ExpandAll();
            tree.CollapseAll();

            tree.NodesIterator.DoOperation(new UpdateNodesCheckStateOperation(columnConditionTreeSkuChecked));

            UpdateInitiallyUncheckedNodes();
        }

        /// <summary>
        /// Loads the conditions.
        /// </summary>
        public void LoadConditions() {
            DataTable lDataTable = DataProvider.GetConditionsBySKU(ContractId);
            if (lDataTable == null || lDataTable.Rows.Count == 0)
                return;

            lDataTable.PrimaryKey = new[] {lDataTable.Columns[columnConditionTreeSkuId.FieldName]};
            tree.DataSource = lDataTable;
            DateStart = ConvertEx.ToDateTime(lDataTable.Rows[0][columnConditionTreeSkuBeginDate.FieldName]);
            tree.PopulateColumns();

            foreach (TreeListColumn lColumn in tree.Columns) {
                lColumn.Width = 100;
                lColumn.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                lColumn.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
                lColumn.Format.FormatType = FormatType.Numeric;
                lColumn.Format.FormatString = "{0:N3}";

                if (!IsPeriodColumn(lColumn))
                    continue;

                // prepare column
                int lPeriodNumber = GetPeriodColumnNumber(lColumn);
                int lMonth = DateStart.Month + lPeriodNumber - 1;
                int lYear = (int) (DateStart.Year + Math.Ceiling(lMonth / 12.0)) - 1;
                bool lIsFactColumn = IsFactPeriodColumn(lColumn);

                if (lIsFactColumn)
                    AddDateToCollection(lYear, lMonth);

                lColumn.Caption = string.Format("{0} {1}\r\n{2}", lYear, ConvertEx.ToMonthName(lMonth),
                    lIsFactColumn ? Constants.LABEL_FACT : Constants.LABEL_PLAN);
                if (lIsFactColumn) {
                    lColumn.OptionsColumn.ReadOnly = true;
                    lColumn.OptionsColumn.AllowEdit = false;
                }
                else {
                    lColumn.ColumnEdit = repositoryItemSpinEditPlan;
                }

                lColumn.RowFooterSummary = SummaryItemType.Sum;
            }

            // restrore columns settings
            tree.Columns[columnConditionTreeSkuName.FieldName].Width = 400;
            tree.Columns[columnConditionTreeSkuProductId.FieldName].Visible = false;
            tree.Columns[columnConditionTreeSkuOLID.FieldName].Visible = false;
            tree.Columns[columnConditionTreeSkuChecked.FieldName].Visible = false;
            tree.Columns[columnConditionTreeSkuBeginDate.FieldName].Visible = false;

            columnConditionTreeSkuTotalFact.Format.FormatType = FormatType.Numeric;
            columnConditionTreeSkuTotalFact.Format.FormatString = "{0:N3}";
            columnConditionTreeSkuTotalPlan.Format.FormatType = FormatType.Numeric;
            columnConditionTreeSkuTotalPlan.Format.FormatString = "{0:N3}";

            RestoreFieldSettings(columnConditionTreeSkuName);
            RestoreFieldSettings(columnConditionTreeSkuFixed, true);
            RestoreFieldSettings(columnConditionTreeSkuTotalPlan);
            RestoreFieldSettings(columnConditionTreeSkuTotalFact);
        }

        /// <summary>
        /// Adds the date to collection.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        private void AddDateToCollection(int year, int month) {
            while (month > 12)
                month -= 12;

            _dateCollection.Add(new DateTime(year, month, 1));
        }

        /// <summary>
        /// Determines whether [is period column] [the specified column].
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>
        /// 	<c>true</c> if [is period column] [the specified column]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPeriodColumn(TreeListColumn column) {
            return (column == null) ? false : column.FieldName.StartsWith(Constants.DYNAMIC_COLUMN_PREFIX_DB);
        }

        /// <summary>
        /// Determines whether [is fact period column] [the specified column].
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>
        /// 	<c>true</c> if [is fact period column] [the specified column]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsFactPeriodColumn(TreeListColumn column) {
            return (column != null) && column.FieldName.StartsWith(Constants.DYNAMIC_COLUMN_PREFIX_FACT_DB);
        }

        /// <summary>
        /// Determines whether column is [Plan period column]
        /// </summary>
        /// <param name="column">The tested column.</param>
        /// <returns>
        /// 	<c>true</c> if column is [Plan period column]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsPlanPeriodColumn(TreeListColumn column) {
            return (column != null) && column.FieldName.StartsWith(Constants.DYNAMIC_COLUMN_PREFIX_PLAN_DB);
        }

        /// <summary>
        /// Gets the period column number.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private static int GetPeriodColumnNumber(TreeListColumn column) {
            return GetPeriodColumnNumber(column,
                new string[] {
                    Constants.DYNAMIC_COLUMN_PREFIX_PLAN_DB,
                    Constants.DYNAMIC_COLUMN_PREFIX_FACT_DB
                });
        }

        /// <summary>
        /// Gets the period column number.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="possiblePrefixes">The possible prefixes.</param>
        /// <returns></returns>
        public static int GetPeriodColumnNumber(TreeListColumn column, string[] possiblePrefixes) {
            int lNum = 0;
            if (IsPeriodColumn(column) && possiblePrefixes != null && possiblePrefixes.Length > 0) {
                try {
                    foreach (string lPrefix in possiblePrefixes) {
                        if (!column.FieldName.Contains(lPrefix))
                            continue;

                        lNum = ConvertEx.ToInt(column.FieldName.TrimStart(lPrefix.ToCharArray()));
                        if (lNum > 0)
                            return lNum;
                    }
                }
                catch (InvalidCastException) {
                    lNum = 0;
                }
            }
            return lNum;
        }

        /// <summary>
        /// Fires the edit value changed.
        /// </summary>
        /// <param name="e">The <see cref="CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void FireEditValueChanged(CellValueChangedEventArgs e) {
            if (EditValueChanged != null && e != null)
                EditValueChanged(this, e);
        }

        private void FireSKUStructureChanged(CellValueChangedEventArgs e) {
            if (SKUStructureChanged != null && e != null)
                SKUStructureChanged(this, e);
        }

        /// <summary>
        /// Restores the field settings.
        /// </summary>
        /// <param name="col">The col.</param>
        /// <param name="editable"></param>
        private void RestoreFieldSettings(TreeListColumn col, bool editable = false) {
            tree.Columns[col.FieldName].OptionsColumn.ReadOnly = !editable;
            tree.Columns[col.FieldName].OptionsColumn.AllowEdit = editable;
            tree.Columns[col.FieldName].Caption = col.Caption;
            tree.Columns[col.FieldName].Fixed = FixedStyle.Left;
            if (col.FieldName == Constants.ColumnFixedFieldName) {
                tree.Columns[col.FieldName].AppearanceHeader.TextOptions.WordWrap = WordWrap.Wrap;
                col.ColumnEdit = repositoryItemCheckEdit1;
            }
        }

        /// <summary>
        /// Handles the CellValueChanging event of the tree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void tree_CellValueChanging(object sender, CellValueChangedEventArgs e) {
            PreviousCellValue = ConvertEx.ToDecimal(e.Node.GetValue(e.Column));
            if (e.Column.FieldName == Constants.ColumnFixedFieldName) {
                bool lFixed = ConvertEx.ToBool(e.Value);// ConvertEx.ToBool((sender as TreeList).ActiveEditor.EditValue);
                OnNodeFix(e.Node, lFixed);
                OnNodeFixParent(e.Node.ParentNode, lFixed);
                FireEditValueChanged(e);
            }
        }

        private void OnNodeFix(TreeListNode node, bool fixedV) {
            if (node.Checked)
                node.SetValue(columnConditionTreeSkuFixed, fixedV);
            for (int lIndex = 0; lIndex < node.Nodes.Count; lIndex++ ) {
                OnNodeFix(node.Nodes[lIndex], fixedV);
            }
        }

        private void OnNodeFixParent(TreeListNode parentNode, bool fixedV) {
            TreeListNode lParentNode = parentNode;
            while (lParentNode != null) {
                foreach (TreeListNode lNode in lParentNode.Nodes)
                    if (lNode.Checked)
                        fixedV &= ConvertEx.ToBool(lNode.GetValue(columnConditionTreeSkuFixed));
                        
                lParentNode.SetValue(columnConditionTreeSkuFixed, fixedV);
                lParentNode = lParentNode.ParentNode;
            }
        }

        /// <summary>
        /// Handles the CellValueChanged event of the tree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void tree_CellValueChanged(object sender, CellValueChangedEventArgs e) {
            LastEditedNodeKey = ConvertEx.ToString(tree.FocusedNode.GetValue(columnConditionTreeSkuId));
            if (e.Column.FieldName == Constants.ColumnFixedFieldName)
                FireEditValueChanged(e);

            if (!IsPeriodColumn(e.Column))
                return;

            WaitManager.StartWait();
            tree.BeginUpdate();
            SaveTreeDataTable();
            try {
                if (!_modifiedColumns.Contains(e.Column.AbsoluteIndex))  //Check whether modifiedColumns collection already has this index value
                    _modifiedColumns.Add(e.Column.AbsoluteIndex);  //And it hasn't, just add index of modified column to this collection.
                RecalcRelatedNodes(e.Node, e.Column);
                OnNodeFix(e.Node, true);
                OnNodeFixParent(e.Node, true);
                FireEditValueChanged(e);
            }
            finally {
                RestoreTreeDataTable();
                tree.EndUpdate();
                WaitManager.StopWait();    
            }
        }

        private void RestoreTreeDataTable() {
            if (!IsRecalcImpossible)
                return;

            DataTable lTable = (tree.DataSource as DataTable);
            if (lTable == null)
                return;

            lTable.Merge(TreeDataTable, false, MissingSchemaAction.Ignore);
            IsRecalcImpossible = false;
            // DEVX issue - workaround
            tree.NodesIterator.DoOperation(new UpdateNodesCheckStateOperation(columnConditionTreeSkuChecked));
        }

        private void SaveTreeDataTable() {
            IsRecalcImpossible = false;
            TreeDataTable = ((DataTable) tree.DataSource).Copy();
        }


        /// <summary>
        /// Gets the allow cell edit.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        private bool IsCellEditAllowed(TreeListColumn column, TreeListNode node) {
            if (null == column || null == node)
                return false;
            if (null == node.ParentNode)  // disable edit root node
                return false;
            if (!node.Checked)
                return false;
            //if (column.FieldName == Constants.ColumnFixedFieldName && !node.HasChildren)
            if (column.FieldName == Constants.ColumnFixedFieldName)
                return true;
            if (!IsPlanPeriodColumn(column))
                return false;
            //Debug.Assert(node.Nodes.FirstNode == null, "node.Nodes.FirstNode is null");

            bool lResult = true;
            if (node.HasChildren) {
                int lCheckedCount = GetCheckedNodesCount(node);
                int lFixedCount = GetFixedNodesCount(node);
                // if all nodes are fixed - disable edit
                if (lCheckedCount - lFixedCount == 0)
                    lResult = false;
                // if exists only one not fixed node - disable edit
                if (lCheckedCount - lFixedCount >= 1)
                    lResult = true;
                int lCheckedDown = GetCheckedNodesCountLevelDown(node.ParentNode);
                int lFixedDown = GetFixedNodesCountLevelDown(node.ParentNode);
                if (lCheckedDown - lFixedDown == 1)
                    lResult = false;
                //                if (lCheckedCount - lFixedCount == 1 && lCheckedCount == 1)
                //                    lResult = false;
            }
            else {
                int lCheckedCount = GetCheckedNodesCount(node.ParentNode);
                int lFixedCount = GetFixedNodesCount(node.ParentNode);
                // if all nodes are fixed - disable edit
                if (lCheckedCount - lFixedCount == 0)
                    lResult = false;
                if (!IsFixedNode(node) && node.ParentNode.Nodes.Count == 1)
                    lResult = false;
                else  // if exists only one not fixed node - disable edit
                if (!IsFixedNode(node) && (lCheckedCount - lFixedCount == 1))
                    lResult = false;
            }
            return (lResult && EditMode == SKUTreeConditionsEditMode.Common);
        }

        /// <summary>
        /// Gets the period column future.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns></returns>
        //private bool IsFuturePeriodColumn(TreeListColumn column)
        //{
        //    int periodNumber = GetPeriodColumnNumber(column);
        //    DateTime editDate = dateCollection[periodNumber - 1];
        //    DateTime nowDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
        //    return editDate >= nowDate;
        //}

        /// <summary>
        /// Handles the CustomDrawNodeCell event of the tree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs"/> instance containing the event data.</param>
        private void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e) {
            if (!IsPeriodColumn(e.Column) || IsCellEditAllowed(e.Column, e.Node))
                return;

            Rectangle lRect = e.Bounds;
            lRect.Inflate(-1, -1);
            e.Graphics.FillRectangle(Constants.BrushGray, lRect);
        }

        /// <summary>
        /// Handles the ShowingEditor event of the tree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void tree_ShowingEditor(object sender, CancelEventArgs e) {
            e.Cancel = !IsCellEditAllowed(tree.FocusedColumn, tree.FocusedNode);

            // just remove spin buttons if required
            if (repositoryItemSpinEditPlan.Buttons.Count > 0)
                repositoryItemSpinEditPlan.Buttons[0].Visible = !e.Cancel;
        }

        /// <summary>
        /// Handles the BeforeCheckNode event of the tree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.CheckNodeEventArgs"/> instance containing the event data.</param>
        private void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e) {
            e.CanCheck = (EditMode == SKUTreeConditionsEditMode.Common);

            if (null == e.Node.ParentNode)
                e.CanCheck = false;

            if (EditMode == SKUTreeConditionsEditMode.AllowCheckSkuOnly && IsAllowAddSku &&
                !_initiallyCheckedNodeCollection.Contains(e.Node)) {
                e.CanCheck = true;
            }
        }

        /// <summary>
        /// Handles the AfterCheckNode event of the tree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.NodeEventArgs"/> instance containing the event data.</param>
        private void tree_AfterCheckNode(object sender, NodeEventArgs e) {
            WaitManager.StartWait();
            SaveTreeDataTable();
            try {
                OnNodeCheck(e.Node);

                // Recalculate values for all modified columns.
                for (int i = 0; i < _modifiedColumns.Count; i++) {
                    int lModifiedColumn = _modifiedColumns[i];
                    //comented because not clear. may be not actual
                    //decimal lNewValue = ConvertEx.ToDecimal(e.Node.ParentNode.GetValue(tree.Columns[lModifiedColumn]));
                    //SpreadParentValueToChildren(e.Node.ParentNode, tree.Columns[lModifiedColumn], 0, lNewValue);
                    if (GetCheckedNodesCount(e.Node.ParentNode) == 0) //Check whether specified node's child have unchecked state 
                        _modifiedColumns.Remove(lModifiedColumn); //And then delete index value of this colum from modifiedColumns collection
                }

                CellValueChangedEventArgs lCellValueChangedEventArgs = new CellValueChangedEventArgs(tree.FocusedColumn, e.Node, null);
                FireSKUStructureChanged(lCellValueChangedEventArgs);
            }
            finally {
                RestoreTreeDataTable();
                WaitManager.StopWait();
            }
        }

        /// <summary>
        /// Called when [node check].
        /// </summary>
        /// <param name="node">The node.</param>
        private void OnNodeCheck(TreeListNode node) {
            tree.BeginUpdate();
            AttachEvents(false);
            try {
                //IDEA: We process just unchecked node like checked but with new value set to zero
                if (!node.Checked) {
                    node.Checked = true;
                    //first set Fixed to false for unchecked nodes
                    OnNodeFix(node, false);
                    OnNodeFixParent(node, false);
                    try {
                        foreach (TreeListColumn lColumn in tree.Columns) {
                            if (!IsPlanPeriodColumn(lColumn))
                                continue;
                            decimal lOldValue = ConvertEx.ToDecimal(node.GetValue(lColumn));
                            if (lOldValue != 0)
                                RecalcNodesByCoef(node, lColumn, lOldValue, 0, true);
                            node.SetValue(lColumn, DBNull.Value);
                        }
                    }
                    finally {
                        node.Checked = false;
                    }
                }

                // check/uncheck child nodes
                OnNodeCheckRecursive(node, node.Checked);

                // check/uncheck parent nodes
                TreeListNode lParent = node.ParentNode;
                while (lParent != null) {
                    if (node.Checked)
                        CheckNode(lParent, true);
                    else if (!HasCheckedChildren(lParent))
                        CheckNode(lParent, false);
                    lParent = lParent.ParentNode;
                }

                // get node values from DB
                UpdateJustCheckedNodesDynamically();

                //IDEA: We process just checked node as node with old value zero and new value will be zero too
//not needed because we have zero for new values
//                if (node.Checked)
//                    foreach (TreeListColumn lColumn in tree.Columns) {
//                        if (!IsPlanPeriodColumn(lColumn))
//                            continue;
//                        decimal lNewValue = ConvertEx.ToDecimal(node.GetValue(lColumn));
//                        if (lNewValue != 0)
//                            RecalcNodesByCoef(node, lColumn, 0, lNewValue);
//                    }

                TreeListNode lCashedParent = null;
                foreach (TreeListNode lNode in node.Checked ? _justCheckedNodeCollection : _justUnCheckedNodeCollection) {
                    TreeListNode lParentNode = lNode.ParentNode;
                    if (lCashedParent == lParentNode)
                        continue;

                    lCashedParent = lParentNode;
                    while (lParentNode != null) {
                        RecalcRelatedNodes(lParentNode, null);
                        lParentNode = lParentNode.ParentNode;
                    }
                }

                // DEVX issue - workaround
                tree.NodesIterator.DoOperation(new UpdateNodesCheckStateOperation(columnConditionTreeSkuChecked));
            }
            finally {
                AttachEvents(true);
                tree.EndUpdate();
                // clear the buffers
                _justCheckedNodeCollection.Clear();
                _justUnCheckedNodeCollection.Clear();
            }
        }

        /// <summary>
        /// Called when [node check recursive].
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="isChecked">if set to <c>true</c> [is checked].</param>
        private void OnNodeCheckRecursive(TreeListNode node, bool isChecked) {
            if (node == null)
                return;

            CheckNode(node, isChecked);
            for (int lIndex = 0; lIndex < node.Nodes.Count; lIndex++) // do not use foreach here - sorting issue
                OnNodeCheckRecursive(node.Nodes[lIndex], isChecked);
        }

        /// <summary>
        /// Checks the node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="check">if set to <c>true</c> [check].</param>
        private void CheckNode(TreeListNode node, bool check) {
            if (node == null)
                return;

            // update check value
            node.Checked = check;
            node.SetValue(columnConditionTreeSkuChecked, check);

            // memorize checked item
            if (!node.HasChildren)
                if (check)
                    _justCheckedNodeCollection.Add(node);
                else
                    _justUnCheckedNodeCollection.Add(node);

            if (check)
                return;

            foreach (TreeListColumn lColumn in tree.Columns)
                if (IsPeriodColumn(lColumn))
                    node.SetValue(lColumn, DBNull.Value);
            
            node.SetValue(columnConditionTreeSkuTotalPlan, DBNull.Value);
            node.SetValue(columnConditionTreeSkuTotalFact, DBNull.Value);
            node.SetValue(columnConditionTreeSkuFixed, false);
        }

        /// <summary>
        /// Determines whether [has checked children] [the specified node].
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>
        /// 	<c>true</c> if [has checked children] [the specified node]; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasCheckedChildren(TreeListNode node) {
            if (node != null && node.HasChildren)
                foreach (TreeListNode lNode in node.Nodes) {
                    if (lNode.Checked)
                        return true;
                }

            return false;
        }

        /// <summary>
        /// Updates the just checked nodes dynamically.
        /// </summary>
        private void UpdateJustCheckedNodesDynamically() {
            if (_justCheckedNodeCollection == null || _justCheckedNodeCollection.Count == 0)
                return;

            //set to zero volume
            foreach (TreeListNode lNode in _justCheckedNodeCollection) {
                foreach (TreeListColumn lColumn in tree.Columns) {
                    if (!IsPeriodColumn(lColumn))
                        continue;
                    lNode.SetValue(lColumn, 0.0);
                }
                RecalcTotalsForNode(lNode);
            }

//            // prepare list of checked IDs
//            StringBuilder lIdListBuilder = new StringBuilder(1024);
//            foreach (TreeListNode lNode in _justCheckedNodeCollection)
//                lIdListBuilder.AppendFormat("'{0}'{1}", lNode.GetValue(columnConditionTreeSkuId), Constants.DELIMITER_ID_LIST);
//            
//            string lIdList = lIdListBuilder.ToString().TrimEnd(Constants.DELIMITER_ID_LIST.ToCharArray());
//            // refresh appropriate rows
//            DataTable lDataTable = tree.DataSource as DataTable;
//            if (lDataTable == null)
//                return;
//
//            lDataTable.AcceptChanges();
//            DataView lDataView = new DataView(tree.DataSource as DataTable);
//            lDataView.RowFilter = string.Format("{0} IN ({1})", columnConditionTreeSkuId.FieldName, lIdList);
//            if (lDataView.Count <= 0)
//                return;
//
//            DataTable lDataTableToMerge = DataProvider.GetConditionsDynamicCalc(ContractId, ConvertEx.ToSqlXmlAll(lDataView.ToTable()));
//            lDataTableToMerge.PrimaryKey = new DataColumn[] {lDataTableToMerge.Columns[columnConditionTreeSkuId.FieldName]};
//            lDataTable.Merge(lDataTableToMerge, false, MissingSchemaAction.Ignore);
        }

        /// <summary>
        /// Updates the initially unchecked nodes.
        /// </summary>
        private void UpdateInitiallyUncheckedNodes() {
            _initiallyCheckedNodeCollection.Clear();

            DataTable lDataTable = tree.DataSource as DataTable;
            if (lDataTable == null)
                return;

            foreach (DataRow lRow in lDataTable.Rows) {
                bool lIsChecked = ConvertEx.ToBool(lRow[columnConditionTreeSkuChecked.FieldName]);
                if (!lIsChecked)
                    continue;

                TreeListNode lNode = tree.FindNodeByKeyID(ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]));
                if (lNode != null)
                    _initiallyCheckedNodeCollection.Add(lNode);
            }
        }

        /// <summary>
        /// Recalcs the related nodes.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="column">The TreeList column</param>
        private void RecalcRelatedNodes(TreeListNode node, TreeListColumn column) {
            if (node == null)
                return;

            decimal lNewValue = ConvertEx.ToDecimal(node.GetValue(column));
            // spread a value entered to a parent node among the children
//comented because called in RecalcNodesByCoef
//            if (node.HasChildren && column != null)
//                SpreadParentValueToChildren(node, column, PreviousCellValue, lNewValue);

            // recalc V regarding its weight in total
            if (null != column)
                RecalcNodesByCoef(node, column, PreviousCellValue, lNewValue, false);

            // recalc all parent nodes
            TreeListNode lCurrentNode = node.HasChildren ? node : node.ParentNode;
            while (lCurrentNode != null) {
                if (column != null)
                    RecalcNode(lCurrentNode, column); // calculate the only column
                else
                    foreach (TreeListColumn lColumn in tree.Columns)
                        RecalcNode(lCurrentNode, lColumn);  // calculate every column

                RecalcTotalsForNode(lCurrentNode);
                lCurrentNode = lCurrentNode.ParentNode;
            }

            // recalc all child nodes
            if (node.ParentNode == null)
                return;
            foreach (TreeListNode lChildNode in node.ParentNode.Nodes) {
                RecalcTotalsForNode(lChildNode);
            }

            //RecalcTotalsForNode(node);
        }

        /// <summary>
        /// Recalculates V for SKU by their weights in parent Total.
        /// </summary>
        /// <param name="node">The node where value changed</param>
        /// <param name="column">The column where value changed</param>
        /// <param name="oldValue">Old value for column (before edit)</param>
        /// <param name="newValue">New value for column (after edit)</param>
        /// <param name="unCheck"></param>
        private void RecalcNodesByCoef(TreeListNode node, TreeListColumn column, decimal oldValue, decimal newValue, bool unCheck) {
            if (node == null || node.ParentNode == null)
                return;
            if (oldValue == newValue)
                return;
            if (IsRecalcImpossible)
                return;

            DataTable lDataTable = tree.DataSource as DataTable;
            if (lDataTable == null)
                return;

            lDataTable.AcceptChanges();

            // prepare list of checked IDs for parent node
            DataTable lFilteredTable1 = GetFilteredTable(lDataTable, node.ParentNode);
            if (lFilteredTable1.Rows.Count <= 0)
                return;

            // calc Fixed nodes count and Sum
            decimal lFixedSum = 0m;
            int lFixedCount = 0;
            foreach (DataRow lRow in lFilteredTable1.Rows) {
                if (ConvertEx.ToBool(lRow[Constants.ColumnFixedFieldName])) {
                    lFixedCount++;
                    lFixedSum += ConvertEx.ToDecimal(lRow[column.FieldName]);
                }
            }

            // work only with not Fixed nodes
            lFilteredTable1.AcceptChanges();
            DataView lDataView = new DataView(lFilteredTable1);
            lDataView.RowFilter = Constants.ColumnFixedFieldName + " = 0";
            if (lDataView.Count <= 0)
                return;

            DataTable lFilteredTable = lDataView.ToTable();
            lFilteredTable.PrimaryKey = new DataColumn[] {lFilteredTable.Columns[columnConditionTreeSkuId.FieldName]};


            //-----------------------------------------
            //-------------- Algorithm start
            //-----------------------------------------
            string lKeyId = ConvertEx.ToString(node.GetValue(columnConditionTreeSkuId));
            int lNodesCount = lFilteredTable1.Rows.Count - lFixedCount;
            decimal lTotalWoFixed = ConvertEx.ToDecimal(node.ParentNode.GetValue(column)) - lFixedSum;
            decimal lNewTotalWoFixed = lTotalWoFixed;  //already has newValue
            if (IsFixedNode(node))
                lTotalWoFixed = lTotalWoFixed + newValue - oldValue;

            if (!ValidatePlanVolume(node, column, lNodesCount, lTotalWoFixed, lNewTotalWoFixed, newValue, unCheck)) {
                node.SetValue(column, oldValue);
                return;
            }

            decimal lNodeOldCoef = oldValue / lTotalWoFixed;
            decimal lNodeNewCoef = newValue / lTotalWoFixed;


            // add additional fields to table
            PrepareTable(lFilteredTable, column);

            // new coefs and values calc
            decimal lNewTotalSum = 0m;
            decimal lNewValue;
            foreach (DataRow lRow in lFilteredTable.Rows) {
                if (ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]) == lKeyId) {
                    lRow[FieldNameOldCoef] = lNodeOldCoef;
                    lRow[FieldNameNewCoef] = lNodeNewCoef;
                    lRow[FieldNameNewValue] = newValue;
                    lRow[FieldNameNewValueRounded] = Math.Round(newValue, 3, MidpointRounding.AwayFromZero);
                    lRow[FieldNameDelta] = 0;
                    lNewTotalSum += newValue;
                }
                else {
                    decimal lOldCoef;
                    decimal lNewCoef;
                    lRow[FieldNameOldCoef] = lOldCoef = ConvertEx.ToDecimal(lRow[column.FieldName]) / lTotalWoFixed;
                    if (IsFixedNode(node))
                        lNewCoef = ConvertEx.ToDecimal(lRow[column.FieldName]) / lTotalWoFixed;
                    else
                        lNewCoef = (lNodeOldCoef == 1) ? 0 : lOldCoef * (1 - lNodeNewCoef) / (1 - lNodeOldCoef);
                    lRow[FieldNameNewCoef] = lNewCoef;
                    lRow[FieldNameNewValue]  = lNewValue = lNewCoef * lNewTotalWoFixed;
                    lRow[FieldNameNewValueRounded] = lNewValue = Math.Round(lNewValue, 3, MidpointRounding.AwayFromZero);
                    lNewTotalSum += lNewValue;
                }
            }

            lNodesCount -= 1;
            // correction for V
            VolumeCorrection(lFilteredTable, lNewTotalWoFixed, lNewTotalSum, lNodesCount, newValue, lKeyId);

            lFilteredTable.AcceptChanges();

            //------------------------------------
            //-------------- Algorithm end
            //------------------------------------

            // make changes in nodes
            foreach (DataRow lRow in lFilteredTable.Rows) {
                string lId = ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]);
                TreeListNode lNode = tree.FindNodeByKeyID(lId);
                lNewValue = ConvertEx.ToDecimal(lRow[FieldNameNewValueRounded]);
                lNode.SetValue(column, lNewValue);
                if (lNode.HasChildren) {
                    decimal lOldValue = (lId == lKeyId) ? oldValue : ConvertEx.ToDecimal(lRow[FieldNameOldValue]);
                    SpreadParentValueToChildren(lNode, column, lOldValue, lNewValue);
                }
            }
            
        }

        /// <summary>
        /// prepare list of checked IDs for children nodes
        /// </summary>
        /// <param name="dataTable">Source table</param>
        /// <param name="node">Parent node</param>
        /// <returns></returns>
        private DataTable GetFilteredTable(DataTable dataTable, TreeListNode node) {
            StringBuilder lIdListBuilder = new StringBuilder(1024);
            foreach (TreeListNode lNode in node.Nodes)
                if (lNode.Checked)
                    lIdListBuilder.AppendFormat("'{0}'{1}", lNode.GetValue(columnConditionTreeSkuId), Constants.DELIMITER_ID_LIST);
            
            string lIdList = lIdListBuilder.ToString().TrimEnd(Constants.DELIMITER_ID_LIST.ToCharArray());
            //where ID in ( )
            DataView lDataView1 = new DataView(dataTable);
            lDataView1.RowFilter = string.Format("{0} IN ({1})", columnConditionTreeSkuId.FieldName, lIdList);

            DataTable lFilteredTable1 = lDataView1.ToTable();
            lFilteredTable1.PrimaryKey = new DataColumn[] {lFilteredTable1.Columns[columnConditionTreeSkuId.FieldName]};
            return lFilteredTable1;
        }

        private static void PrepareTable(DataTable dataTable, TreeListColumn column) {
            dataTable.Columns.Add(FieldNameOldCoef, Type.GetType("System.Decimal"));
            dataTable.Columns.Add(FieldNameNewCoef, Type.GetType("System.Decimal"));
            dataTable.Columns.Add(FieldNameOldValue, Type.GetType("System.Decimal"));
            dataTable.Columns.Add(FieldNameNewValue, Type.GetType("System.Decimal"));
            dataTable.Columns.Add(FieldNameNewValueRounded, Type.GetType("System.Decimal"));
            dataTable.Columns.Add(FieldNameDelta, Type.GetType("System.Decimal"));
            if (null != column)
                for (int lInd = 0; lInd < dataTable.Rows.Count; lInd++)
                    dataTable.Rows[lInd][FieldNameOldValue] = ConvertEx.ToDecimal(dataTable.Rows[lInd][column.FieldName]);
        }

        /// <summary>
        /// Calculates the Total for parent node by children nodes for specified column.
        /// </summary>
        /// <param name="parentNode">The parent node.</param>
        /// <param name="column">The column.</param>
        private static void RecalcNode(TreeListNode parentNode, TreeListColumn column) {
            if (!IsPeriodColumn(column) || !parentNode.HasChildren)
                return;
            // prevent recalc Root node for Plan
            if (null == parentNode.ParentNode && IsPlanPeriodColumn(column))
                return;

            decimal lPeriodSum = 0M;
            foreach (TreeListNode lChildNode in parentNode.Nodes)
                lPeriodSum += ConvertEx.ToDecimal(lChildNode.GetValue(column));
            if (parentNode.Checked)
                parentNode.SetValue(column, lPeriodSum);
        }

        /// <summary>
        /// Recalcs the total nodes.
        /// </summary>
        /// <param name="node">The node.</param>
        private void RecalcTotalsForNode(TreeListNode node) {
            if (node == null || !node.Checked)
                return;

            decimal lPlanValue = 0M;
            decimal lFactValue = 0M;

            foreach (TreeListColumn lColumn in tree.Columns) {
                if (!IsPeriodColumn(lColumn))
                    continue;

                decimal lValue = ConvertEx.ToDecimal(node.GetValue(lColumn));
                if (IsFactPeriodColumn(lColumn))
                    lFactValue += lValue;
                else
                    lPlanValue += lValue;
            }

            node.SetValue(columnConditionTreeSkuTotalPlan, lPlanValue);
            node.SetValue(columnConditionTreeSkuTotalFact, lFactValue);
        }

        /// <summary>
        /// Spreads the parent value to children.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="column">The column.</param>
        /// <param name="oldValue">Old value for column (before edit)</param>
        /// <param name="newValue">New value for column</param>
        private void SpreadParentValueToChildren(TreeListNode node, TreeListColumn column, decimal oldValue, decimal newValue) {
            if (null == node || !node.HasChildren)
                return;
            if (IsRecalcImpossible)
                return;

            int lCheckedChildCount = 0;  //GetCheckedNodesCountLevelDown(node);
            int lFixedChildCount = 0;  //GetFixedNodesCountLevelDown(node);
            decimal lFixedSum = 0m;
            
            foreach (TreeListNode lChildNode in node.Nodes) {
                if (!lChildNode.Checked)
                    continue;
                lCheckedChildCount++;
                if (!IsFixedNode(lChildNode))
                    continue;
                lFixedChildCount++;
                lFixedSum += ConvertEx.ToDecimal(lChildNode.GetValue(column));
            }

            decimal lTotalWoFixed = oldValue - lFixedSum;
            decimal lNewTotalWoFixed = newValue - lFixedSum;
            int lChildCount = lCheckedChildCount - lFixedChildCount;
            bool lAllChildHaveZeroValue = false;

            if (!ValidatePlanVolume(node, column, lChildCount, lTotalWoFixed, lNewTotalWoFixed, 0, false))
                return;

            DataTable lDataTable = tree.DataSource as DataTable;
            if (lDataTable == null)
                return;

            lDataTable.AcceptChanges();

            // prepare list of checked IDs for parent node
            DataTable lFilteredTable1 = GetFilteredTable(lDataTable, node);
            if (lFilteredTable1.Rows.Count <= 0)
                return;

            // work only with not Fixed nodes
            lFilteredTable1.AcceptChanges();
            DataView lDataView = new DataView(lFilteredTable1);
            lDataView.RowFilter = Constants.ColumnFixedFieldName + " = 0";
            if (lDataView.Count <= 0)
                return;

            DataTable lFilteredTable = lDataView.ToTable();
            lFilteredTable.PrimaryKey = new DataColumn[] {lFilteredTable.Columns[columnConditionTreeSkuId.FieldName]};
            PrepareTable(lFilteredTable, column);

            decimal lNewCalcTotal = 0m;
            foreach (DataRow lRow in lFilteredTable.Rows) {
                //string lId = ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]);
                //TreeListNode lChildNode = tree.FindNodeByKeyID(lId);

                decimal lOldChildValue = ConvertEx.ToDecimal(lRow[column.FieldName]);
                    //ConvertEx.ToDecimal(lChildNode.GetValue(column));
                decimal lNewChildValue;
                if (lAllChildHaveZeroValue || HaveChildNodesZeroValues(node, column) && lChildCount > 0) {
                    lNewChildValue = lNewTotalWoFixed / lChildCount;
                    lAllChildHaveZeroValue = true;
                }
                else if (lTotalWoFixed != 0) {
                    decimal lCoef = lOldChildValue / lTotalWoFixed;
                    lNewChildValue = lNewTotalWoFixed * lCoef;
                    lRow[FieldNameNewCoef] = lCoef;
                }
                else
                    lNewChildValue = lNewTotalWoFixed / lChildCount;

                lRow[FieldNameNewValue] = lNewChildValue;
                lNewChildValue = Math.Round(lNewChildValue, 3, MidpointRounding.AwayFromZero);
                lRow[FieldNameNewValueRounded] = lNewChildValue;
                lNewCalcTotal += lNewChildValue;
            }

            // correction for V
            VolumeCorrection(lFilteredTable, lNewTotalWoFixed, lNewCalcTotal, lChildCount);
            lFilteredTable.AcceptChanges();

            // make changes in nodes
            foreach (DataRow lRow in lFilteredTable.Rows) {
                string lId = ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]);
                TreeListNode lNode = tree.FindNodeByKeyID(lId);
                decimal lNewValue = ConvertEx.ToDecimal(lRow[FieldNameNewValueRounded]);
                lNode.SetValue(column, lNewValue);
                RecalcTotalsForNode(lNode);
                if (lNode.HasChildren) {
                    decimal lOldValue = ConvertEx.ToDecimal(lRow[FieldNameOldValue]);
                    SpreadParentValueToChildren(lNode, column, lOldValue, lNewValue);
                }
            }
        }

        /// <summary>
        /// Method that recursively checks input node's children and returns value that indicates whether any of it has zero value.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private static bool HaveChildNodesZeroValues(TreeListNode node, TreeListColumn column) {
            bool lResult = true;
            foreach (TreeListNode lChildNode in node.Nodes) {
                if (!lChildNode.Checked)
                    continue;

                decimal lPrevChildValue = ConvertEx.ToDecimal(lChildNode.GetValue(column));
                if (lPrevChildValue != 0) {
                    lResult = false;
                    break;
                }
                lResult = HaveChildNodesZeroValues(lChildNode, column);
            }
            return lResult;
        }

        /// <summary>
        /// Returns integer value that indicates how many child were checked in specified node.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static int GetCheckedNodesCount(TreeListNode node) {
            if (null == node)
                return 0;
            int lCount = 0;
            foreach (TreeListNode lChildNode in node.Nodes) {
                if (lChildNode.Checked && lChildNode.HasChildren)
                    lCount += GetCheckedNodesCount(lChildNode);
                else if (lChildNode.Checked)
                    lCount++;
            }            
            return lCount;
        }

        /// <summary>
        /// Returns count of checked nodes one level down only for specified node.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static int GetCheckedNodesCountLevelDown(TreeListNode node) {
            if (null == node)
                return 0;
            int lCount = 0;
            foreach (TreeListNode lChildNode in node.Nodes) {
                if (lChildNode.Checked)
                    lCount++;
            }            
            return lCount;
        }

        /// <summary>
        /// Returns count of child nodes where Fixed is true. Nodes should be Checked
        /// </summary>
        /// <param name="node"></param>
        /// <returns>int</returns>
        private int GetFixedNodesCount(TreeListNode node) {
            if (null == node)
                return 0;
            int lCount = 0;
            foreach (TreeListNode lChildNode in node.Nodes) {
                if (lChildNode.HasChildren && lChildNode.Checked)
                    lCount += GetFixedNodesCount(lChildNode);
                else if (lChildNode.Checked && IsFixedNode(lChildNode))
                    lCount++;
            }

            return lCount;
        }

        /// <summary>
        /// Returns count of child nodes one level down only where Fixed is true.
        /// Nodes should be Checked.
        /// </summary>
        /// <param name="node"></param>
        /// <returns>int</returns>
        private int GetFixedNodesCountLevelDown(TreeListNode node) {
            if (null == node)
                return 0;
            int lCount = 0;
            foreach (TreeListNode lChildNode in node.Nodes)
                if (lChildNode.Checked && IsFixedNode(lChildNode))
                    lCount++;

            return lCount;
        }

        /// <summary>
        /// Returns true if node has Fixed = 1
        /// </summary>
        /// <param name="node">The tree node</param>
        /// <returns></returns>
        private bool IsFixedNode(TreeListNode node) {
            return ConvertEx.ToBool(node.GetValue(columnConditionTreeSkuFixed));
        }

        /// <summary>
        /// Recalcs all nodes.
        /// </summary>
        private void RecalcAllNodes() {
            DataTable lDataTable = tree.DataSource as DataTable;
            if (lDataTable == null)
                return;

            lDataTable.AcceptChanges();
            DataView lDataView = new DataView(lDataTable);
            //where PRODUCT_ID is not null AND CHECKED = 1
            lDataView.RowFilter = string.Format("{0} IS NOT NULL AND {1} = True", columnConditionTreeSkuProductId.FieldName,
                columnConditionTreeSkuChecked.FieldName);
            DataTable lFilteredTable = lDataView.ToTable();
            //build list of checked leaf nodes
            Collection<string> lDistinctChildren = new Collection<string>();
            foreach (DataRow lRow in lFilteredTable.Rows) {
                string lStrId = ConvertEx.ToString(lRow[columnConditionTreeSkuId.FieldName]);
                if (!string.IsNullOrEmpty(lStrId) && !lDistinctChildren.Contains(lStrId))
                    lDistinctChildren.Add(lStrId);
            }

            foreach (string lKeyId in lDistinctChildren) {
                TreeListNode lNode = tree.FindNodeByKeyID(lKeyId);
                if (lNode != null)
                    RecalcRelatedNodes(lNode, null);
            }
        }

        public void UpdatePlanVolume(PlanVolumeChangedEventArgs args) {
            TreeListNode lRootNode = tree.Nodes.FirstNode.RootNode;
            TreeListColumn lColumn = GetPlanColumnByNumber(args.PeriodNumber);
            lRootNode.SetValue(lColumn, args.PlanVolume);
            CheckPlanVolumeTotals(lColumn);
        }

        private TreeListColumn GetPlanColumnByNumber(int periodNum) {
            try {
                foreach (TreeListColumn lColumn in tree.Columns) {
                    if (!IsPlanPeriodColumn(lColumn))
                        continue;
                    int lNum = ConvertEx.ToInt(lColumn.FieldName.TrimStart(Constants.DYNAMIC_COLUMN_PREFIX_PLAN_DB.ToCharArray()));
                    if (periodNum == lNum)
                        return lColumn;
                }
            }
            catch (InvalidCastException) {
                return null;
            }
            return null;
        }


        #region Nested type: UpdateNodesCheckStateOperation

        private class UpdateNodesCheckStateOperation : TreeListOperation {
            private readonly TreeListColumn _columnChecked;

            public UpdateNodesCheckStateOperation(TreeListColumn columnChecked) {
                _columnChecked = columnChecked;
            }

            public override void Execute(TreeListNode node) {
                node.Checked = ConvertEx.ToBool(node.GetValue(_columnChecked));
                if (!node.Checked)
                    return;

                TreeListNode lParentNode = node.ParentNode;
                while (lParentNode != null && !lParentNode.Checked) {
                    lParentNode.Checked = true;
                    //lParentNode.SetValue(_columnChecked, true);
                    lParentNode = lParentNode.ParentNode;
                }
            }
        }

        #endregion


    }
}