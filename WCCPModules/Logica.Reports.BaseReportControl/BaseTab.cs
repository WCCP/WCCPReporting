﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.BaseReportControl {
    /// <summary>
    /// Represent tab, which will be placed inside report control
    /// </summary>
    public class BaseTab : XtraTabPage, IPrint {
        public delegate bool GetBoolean();
        public GetBoolean IsNeedFitToPage = null;

        public Dictionary<GridView, Guid> IdGridColection
        {
            get { return idGridColection; }
        }

        #region Fields
        private Dictionary<GridView, Guid> idGridColection = new Dictionary<GridView, Guid>();
        private bool _offReports = false;
        private readonly List<BaseGridControl> _grids = new List<BaseGridControl>();
        private GridControl _headerGrid;
        private GridView _headerGridView;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tab">List of structure settings for tab</param>
        /// <param name="setting">List of params for tab </param>
        public BaseTab(Tab tab, SheetParamCollection setting) : base() {
            SheetSettings = setting;
            SheetStructure = tab;
            ParentChanged += CustomTab_ParentChanged;
        }

        #region Internal properties

        /// <summary>
        /// Define if there are all read only BaseGridControl in tab
        /// </summary>
        internal bool IsReadOnlyReport {
            get {
                foreach (BaseGridControl lGrid in _grids)
                    if (!lGrid.IsReadOnlyTable)
                        return false;
                return true;
            }
        }

        internal int PrintingMarginOffest { get; set; }

        #endregion

        #region Public properties

        /// <summary>
        /// List of params for tab (for ex., passed from user via SettingsForm )
        /// </summary>
        public SheetParamCollection SheetSettings { get; set; }

        /// <summary>
        /// List of structure settings for tab (passed from DB via XML config string)
        /// </summary>
        public Tab SheetStructure { get; set; }

        public int Length {
            get { return _grids.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether [data modified].
        /// </summary>
        /// <value><c>true</c> if [data modified]; otherwise, <c>false</c>.</value>
        public bool DataModified {
            get {
                foreach (BaseGridControl lGrid in _grids) {
                    lGrid.DefaultView.PostEditor();  //save data without cursor
                    lGrid.Enabled = false;   //Use this instead of Prev/Next Row because it doesn't 
                    lGrid.Enabled = true;    // work with 1 row table;
                    DataTable lGridDataSource = ((DataView) lGrid.DefaultView.DataSource).Table;
                    foreach (DataRow lRow in lGridDataSource.Rows)
                        if (lRow.RowState != DataRowState.Unchanged)
                            return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether gris allow multi select.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if grids allow multi select; otherwise, <c>false</c>.
        /// </value>
        public bool AllowMultiSelect {
            get {
                foreach (BaseGridControl lGrid in _grids)
                    if (!lGrid.DefaultView.OptionsSelection.MultiSelect)
                        return false;
                return true;
            }
            set {
                foreach (BaseGridControl lGrid in _grids)
                    lGrid.DefaultView.OptionsSelection.MultiSelect = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether enable appearence focused cell.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if enable appearence focused cell; otherwise, <c>false</c>.
        /// </value>
        public bool EnableAppearenceFocusedCell {
            get {
                foreach (BaseGridControl grid in _grids)
                    if (!grid.DefaultView.OptionsSelection.EnableAppearanceFocusedCell)
                        return false;
                return true;
            }
            set {
                foreach (BaseGridControl grid in _grids)
                    grid.DefaultView.OptionsSelection.EnableAppearanceFocusedCell = value;
            }
        }

        public bool IsOffReport {
            get { return _offReports; }
            set { _offReports = value; }
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when parent of tab is changed, update tab data 
        /// </summary>
        /// <param name="sender">Parent</param>
        /// <param name="e">Event arguments</param>
        private void CustomTab_ParentChanged(object sender, EventArgs e) {
            if (null != Parent) {
                UpdateData();
                this.AutoScroll = true;
            }
        }

        /// <summary>
        /// Occurs on paint, adjust column width
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            if (null != _headerGridView)
                _headerGridView.BestFitColumns();
        }

        #endregion

        #region Update engine

        /// <summary>
        /// Update data with previous saved params 
        /// </summary>
        internal void UpdateData() {
            UpdateData(SheetSettings);
        }

        /// <summary>
        /// Update data with new params 
        /// </summary>
        /// <param name="settings">New tab params</param>
        internal void UpdateData(SheetParamCollection settings) {
            if (null == SheetStructure || null == settings)
                return;

            Text = SheetStructure.Header;
            Tag = SheetStructure.Order;

            SheetSettings = settings;

            //This step is nessesary because of dinamicaly count of tables 
            Controls.Clear();
            _grids.Clear();
            //Define if tab functionality supported
            bool isTabSupported = false;

            XtraTabControl innerTabControl = null;
            for (int i = 1; i < SheetStructure.Tables.Length; i++) {
                if (SheetStructure.Tables[i - 1].TabIndex != SheetStructure.Tables[i].TabIndex) {
                    isTabSupported = true;
                    innerTabControl = new XtraTabControl() {Dock = DockStyle.Fill};
                    break;
                }
            }
            //Create dinamically tables
            List<Table> listTables = CreateDinamicalyTables(settings);

            List<Table> preCalculationTables =
                listTables.Where(t => t.Mode == ShowMode.Invisible && (settings.TableDataType == TableType.Undefined || settings.TableDataType == t.Type)).
                    ToList();
            foreach (Table preCalculationTable in preCalculationTables) {
                BaseTable.GetDataSource(preCalculationTable, settings);
            }

            listTables = listTables.Except(preCalculationTables).ToList();
            for (int i = listTables.Count - 1; i >= 0; i--) {
                Table table = listTables[i];
                if (settings.TableDataType == TableType.Undefined || settings.TableDataType == table.Type) {
                    //Create Tab-structure if table collection has different TabIndex
                    ControlCollection container;
                    if (isTabSupported) {
                        if (!this.Controls.Contains(innerTabControl))
                            this.Controls.Add(innerTabControl);
                        XtraTabPage page = null;
                        foreach (XtraTabPage pg in innerTabControl.TabPages) {
                            if (pg.Tag.Equals(table.TabIndex)) {
                                page = pg;
                                break;
                            }
                        }
                        if (null == page) {
                            page = new XtraTabPage();
                            innerTabControl.TabPages.Add(page);
                            //TODO: need to setup correctly
                            page.Text = table.TabIndex.ToString();
                            page.Tag = table.TabIndex;
                            page.AutoScroll = true;
                        }
                        container = page.Controls;
                    }
                    else {
                        container = Controls;
                    }
                    DataSet ds = BaseTable.GetDataSource(table, settings);

                    if (ds == null || ds.Tables == null || ds.Tables.Count == 0) {
                        Font fnt = new Font(Font.FontFamily, Font.Size + 4, FontStyle.Bold);
                        container.Add(new LabelControl() {Text = Resource.NoData, Font = fnt, Location = new Point(30, 100)});
                    }
                    else {
                        int lTblCount = 1;
                        // save property because several DataTables belongs to one Table
                        bool lHideHeader = table.HideHeader;
                        foreach (DataTable dt in ds.Tables) {
                            table.HideHeader = lHideHeader;
                            // hide header for all grids except last because grids displayed in reverse order
                            if (lTblCount != ds.Tables.Count)
                                table.HideHeader = true;
                            BaseGridControl lGrid = new BaseGridControl(table, dt) {isOffReport = _offReports};
                            _grids.Add(lGrid);
                            container.Add(lGrid);
                            IdGridColection.Add(lGrid.DefaultView, table.Id);
                            lTblCount += 1;
                        }
                    }
                }
            }

            if (null != _headerGrid)
                Controls.Add(_headerGrid);

            CreateSettingSection();
            //sort tabs
            if (null != innerTabControl)
                BaseTab.SortTabPages(innerTabControl);
        }

        private static DockStyle GetDockStyle(String dockStyle) {
            switch (dockStyle) {
                case "Right":
                    return DockStyle.Right;
                case "Left":
                    return DockStyle.Left;
                case "Bottom":
                    return DockStyle.Top;
                case "Top":
                    return DockStyle.Top;
                default:
                    return DockStyle.Top;
            }
        }

        /// <summary>
        /// Get preinit scripts for creating dinamically tables in DB
        /// </summary>
        /// <param name="settings">User defined settings</param>
        /// <returns>List of scripts</returns>
        private List<Table> CreateDinamicalyTables(SheetParamCollection settings) {
            List<Table> listTables = new List<Table>(SheetStructure.Tables);
            int shift = 0;
            for (int i = 0; i < SheetStructure.Tables.Length; i++) {
//investigate each table and contains its query params with param setting collection
                Table table = SheetStructure.Tables[i];
                bool isDinamical = false;
                if (null == table.Queries)
                    continue;
                foreach (Query query in table.Queries) {
                    if (query.SqlQueryType == SQLQueryType.DataSet) {
//find 'select' query in a table
                        foreach (Parameter par in query.Parameters) {
                            foreach (SheetParam param1 in settings) {
                                if (Regex.IsMatch(param1.SqlParamName, String.Format("^{0}[0-9]+$", par.Name))) {
//find params with postfix(@Mypamam -> @Myparam1, @Myparam2, @Myparam3 ...)
                                    Table dTable = (Table) table.Clone();
                                    foreach (SheetParam param2 in settings) {
//find header for curent dinamicaly param(for @Myparam1 -> @Myparam1Header)
                                        {
                                            if (Regex.IsMatch(param2.SqlParamName, String.Format("^{0}Header$", param1.SqlParamName))) {
                                                dTable.Header = param2.Value.ToString();
                                                break;
                                            }
                                        }
                                    }

                                    foreach (Query newQuery in dTable.Queries) {
//replace parameter name in new table (@Mypamam -> @Myparam1)
                                        if (newQuery.SqlQueryType == SQLQueryType.DataSet) {
                                            foreach (Parameter newPar in newQuery.Parameters) {
                                                if (newPar.Name == par.Name) {
                                                    newPar.Name = param1.SqlParamName;
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    isDinamical = true;
                                    listTables.Insert(shift + i, dTable);
                                    shift++;
                                }
                            }
                        }
                        break;
                    }
                }
                //remove table if it is the template for dinamicaly tables
                if (isDinamical)
                    listTables.Remove(table);
            }
            return listTables;
        }

        /// <summary>
        /// Sort tab pages
        /// </summary>
        /// <param name="tabControl">Tab pages container</param>
        internal static void SortTabPages(XtraTabControl tabControl) {
            try {
                // All sorting is report-dependent and should be performed in report instead of here.
                // Here we just order pages by Order which may be configured in main report module
                var tabPages = tabControl.TabPages.OrderBy(x => {
                    int position = Int32.MaxValue;
                    if (null != x.Tag) {
                        Int32.TryParse(x.Tag.ToString(), out position);
                    }
                    return position;
                }).ToArray();
                for (int i = 0; i < tabPages.Length; i++) {
                    var page = tabPages[i];
                    tabControl.TabPages.Move(i, page);
                }
            }
            catch {
                //no 'throw' because of low important functionality 
            }
        }

        /// <summary>
        /// Compare collections
        /// </summary>
        /// <param name="sheetSettings"></param>
        /// <returns>False if different</returns>
        internal bool CompareSettings(SheetParamCollection sheetSettings) {
            if (null == sheetSettings || sheetSettings.Count != SheetSettings.Count)
                return false;
            if (SheetSettings.TableDataType != sheetSettings.TableDataType)
                return false;
            foreach (SheetParam param in sheetSettings)
                if (!SheetSettings.Exists(p =>
                                          p.SqlParamName.Equals(param.SqlParamName)
                                          &&
                                          ((null != p.Value && null != param.Value && p.Value.Equals(param.Value)) || (null == p.Value && null == param.Value))
                         ))
                    return false;
            return true;
        }

        /// <summary>
        /// Check if all settings was passed fron user correctly
        /// </summary>
        /// <param name="settings"></param>
        private void CheckSettings(SheetParamCollection settings) {
            foreach (Table table in SheetStructure.Tables) {
                foreach (Query query in table.Queries) {
                    if (null != query.Parameters && query.SqlQueryType == SQLQueryType.DataSet) {
                        foreach (Parameter param in query.Parameters) {
                            if (settings == null || (!settings.Exists(s => s.SqlParamName == param.Name) && String.IsNullOrEmpty(param.DefaultValue)))
                                throw new ArgumentNullException(param.Name, "Can not find param for SQL query");
                        }
                    }
                }
            }
        }

        #endregion

        #region Settings section

        /// <summary>
        /// Create settings section
        /// </summary>
        /// <summary>
        /// Fill settings section with params
        /// </summary>
        protected internal virtual void CreateSettingSection() {
            TabOperationHelper.CreateSettingSection(this, SheetSettings, ref _headerGrid, ref _headerGridView);
            _headerGridView.GroupRowCollapsing += new RowAllowEventHandler(headerGridView_GroupRowCollapsing);
        }

        /// <summary>
        /// Disable collapsing function for settings seaction table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void headerGridView_GroupRowCollapsing(object sender, RowAllowEventArgs e) {
            e.Allow = _headerGridView.OptionsView.ShowGroupExpandCollapseButtons;
        }

        #endregion

        #region Print & export

        /// <summary>
        /// Show print dialog for selected list
        /// </summary>
        //internal void Print()
        //{
        //    PrepareCompositeLink().PrintingSystem.PreviewFormEx.Show();
        //}
        /// <summary>
        /// Export tab to document
        /// </summary>
        /// <param name="fName">Path to new document</param>
        /// <param name="type">Type of new document</param>
        internal void Export(string fName, ExportToType type) {
            switch (type) {
                case ExportToType.Html:
                    PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
                    break;
                case ExportToType.Mht:
                    PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
                    break;
                case ExportToType.Pdf:
                    PrepareCompositeLink(type).PrintingSystem.ExportToPdf(fName, new PdfExportOptions() {Compressed = true});
                    break;
                case ExportToType.Rtf:
                    PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
                    break;
                case ExportToType.Txt:
                    PrepareCompositeLink().PrintingSystem.ExportToText(fName);
                    break;
                case ExportToType.Xls:
                    PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() {SheetName = SheetStructure.Header});
                    break;
                case ExportToType.Xlsx:
                    PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
                    break;
                case ExportToType.Bmp:
                    PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
                    break;
                case ExportToType.Csv:
                    PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
                    break;
            }
        }

        public CompositeLink PrepareCompositeLink() {
            return PrepareCompositeLink(ExportToType.Xls);
        }

        /// <summary>
        /// Prepare container of elements for exporting
        /// </summary>
        /// <returns></returns>
        public CompositeLink PrepareCompositeLink(ExportToType exportType) {
            // This fake export force control initialization. In other case set of GroupRowHeight don't take affect.
            using (MemoryStream lMs = new MemoryStream()) {
                _headerGrid.ExportToPdf(lMs);
            }

            using (MemoryStream lSaveHeaderStream = new MemoryStream()) {
                _headerGridView.SaveLayoutToStream(lSaveHeaderStream);
                lSaveHeaderStream.Seek(0, SeekOrigin.Begin);
                CompositeLink lCompositeLink = new CompositeLink(new PrintingSystem());

                //lCompositeLink.PrintingSystemBase.PageSettingsBase.PaperKind = System.Drawing.Printing.PaperKind.A4Rotated;
                lCompositeLink.PaperKind = System.Drawing.Printing.PaperKind.A4Rotated;
                for (int lI = _grids.Count; lI > 0; lI--) {
                    _grids[lI - 1].DefaultView.OptionsPrint.UsePrintStyles = true;
                    _grids[lI - 1].DefaultView.AppearancePrint.BandPanel.BackColor = Color.White;
                    _grids[lI - 1].DefaultView.AppearancePrint.BandPanel.BackColor2 = Color.White;
                    _grids[lI - 1].DefaultView.AppearancePrint.HeaderPanel.BackColor = Color.White;
                    _grids[lI - 1].DefaultView.AppearancePrint.HeaderPanel.BackColor2 = Color.White;
                    _grids[lI - 1].DefaultView.AppearancePrint.FooterPanel.BackColor = Color.White;
                    _grids[lI - 1].AppendLinks(lCompositeLink.Links);
                }

                TabOperationHelper.ExportFormat(ref lCompositeLink);

                int[] lColWidths = new int[_headerGridView.Columns.Count];
                Font[] lColFonts = new Font[_headerGridView.Columns.Count];
                int lGroupRowHeight = _headerGridView.GroupRowHeight;
                lColFonts = ApplyPrintStyles(lColWidths, lColFonts);
                lCompositeLink.Links.Insert(0, new PrintableComponentLink {Component = _headerGrid});

                lCompositeLink.CreateDocument();

                // Skip this flag for *.xls and *.xlsx files because font became unreadable.
                if (!(exportType == ExportToType.Xls || exportType == ExportToType.Xlsx))
                    lCompositeLink.PrintingSystemBase.Document.AutoFitToPagesWidth = 1;

                TabOperationHelper.SetMargins(ref lCompositeLink, PrintingMarginOffest);

                if ((exportType == ExportToType.Pdf) && (null != IsNeedFitToPage && IsNeedFitToPage() || SheetStructure.PrintingFitToPage))
                    TabOperationHelper.FitDocumentToOnePage(ref lCompositeLink);

                RestoreUIStyles(lColWidths, lColFonts);
                _headerGridView.GroupRowHeight = lGroupRowHeight;
                _headerGridView.RestoreLayoutFromStream(lSaveHeaderStream);
                lSaveHeaderStream.Seek(0, SeekOrigin.Begin);
                return lCompositeLink;
            }
        }

        private void RestoreUIStyles(int[] width, Font[] fonts) {
            for (int i = 0; i < _headerGridView.Columns.Count; i++) {
                _headerGridView.Columns[i].MinWidth = width[i];
                _headerGridView.Columns[i].Width = width[i];
                _headerGridView.Columns[i].MaxWidth = width[i];
                _headerGridView.Columns[i].AppearanceCell.Font = fonts[i];
            }
            for (int i = 1; i < _headerGridView.Columns.Count - 1; i = i + 3) {
                _headerGridView.Columns[i - 1].MinWidth = 10;
                _headerGridView.Columns[i - 1].Width = 10;
                _headerGridView.Columns[i - 1].MaxWidth = 15;
            }
            _headerGridView.BestFitColumns();
        }

        private Font[] ApplyPrintStyles(int[] width, Font[] fonts) {
            GridColumn lColumn;
            int lGroupRowFontSize = 10;
            const int a4FontSize = 11;
            const int spaceLength = 10;
            int lExtraSpace = 0;
            int lAllColumnsWidth = 0;

            if (IsOffReport)
                lGroupRowFontSize = 14;

            for (int lI = 0; lI < _headerGridView.Columns.Count; lI++) {
                fonts[lI] = _headerGridView.Columns[lI].AppearanceCell.Font;
                width[lI] = _headerGridView.Columns[lI].Width;
            }

            Font lPrintHeaderFont = new Font("Verdana", lGroupRowFontSize, FontStyle.Bold);
            _headerGridView.AppearancePrint.GroupRow.Font = lPrintHeaderFont;
            Font lPrintFont = new Font("Verdana", a4FontSize, FontStyle.Regular);

            int lMaximumTableWidth = GetMaximumTableWidth();
            if (lMaximumTableWidth > 0) {
                _headerGridView.OptionsPrint.AutoWidth = false;
                _headerGrid.Width = lMaximumTableWidth;
                lMaximumTableWidth -= 15;
                int lSpacersCount = _headerGridView.Columns.Count / 3 - 1;
                lMaximumTableWidth -= spaceLength * lSpacersCount;
                for (int lI = 1; lI < _headerGridView.Columns.Count; lI++) {
                    lColumn = _headerGridView.Columns[lI];
                    //int lColWidth = (i % 3 == 0) ? lSpaceLength : lMaximumTableWidth / (headerGridView.Columns.Count - lSpacersCount - 1);
                    string lCellValue = ((DataRowView) _headerGridView.GetRow(0)).Row[lI].ToString();
                    int lValueWidth = MeasureTextWidth(lCellValue, lPrintFont);
                    lColumn.MaxWidth = 0;
                    lColumn.MinWidth = spaceLength;
                    lColumn.Width = (lValueWidth == 0) ? spaceLength : lValueWidth + spaceLength;
                    lAllColumnsWidth += lColumn.Width;
                }
                lExtraSpace = lMaximumTableWidth - lAllColumnsWidth;
            }

            while (lExtraSpace < 0) {
                if (lPrintFont.SizeInPoints < 7)
                    break;
                lAllColumnsWidth = 0;
                float lSizeInPoints = (lPrintFont.SizeInPoints > 11f) ? 11f : lPrintFont.SizeInPoints - 0.25f;
                lPrintFont = new Font(lPrintFont.Name, lSizeInPoints, lPrintFont.Style, GraphicsUnit.Point);
                for (int lI = 1; lI < _headerGridView.Columns.Count; lI++) {
                    lColumn = _headerGridView.Columns[lI];
                    string lCellValue = ((DataRowView) _headerGridView.GetRow(0)).Row[lI].ToString();
                    int lValueWidth = MeasureTextWidth(lCellValue, lPrintFont);
                    lColumn.Width = (lValueWidth == 0) ? spaceLength : lValueWidth + spaceLength;
                    lAllColumnsWidth += lColumn.Width;
                }
                lExtraSpace = lMaximumTableWidth - lAllColumnsWidth;
            }

            if (lExtraSpace > 0) {
                int lValuesCount = _headerGridView.Columns.Count / 3;
                int lAddWidth = lExtraSpace / lValuesCount;
                for (int lI = 2; lI < _headerGridView.Columns.Count; lI += 3) {
                    lColumn = _headerGridView.Columns[lI];
                    lColumn.Width += lAddWidth;
                }
            }

            foreach (GridColumn lGridColumn in _headerGridView.Columns)
                lGridColumn.AppearanceCell.Font = lPrintFont;

            _headerGridView.AppearancePrint.Row.Font = lPrintFont;
            //headerGridView.OptionsView.ColumnAutoWidth = true;
            _headerGridView.GroupRowHeight = 2 * lGroupRowFontSize;

            return fonts;
        }

        private int GetMaximumTableWidth() {
            int lMinWidth = 1000;
            for (int lI = 0; lI < Length; lI++) {
                int lTableWidth = 0;
                GridView lGridView = (GridView) GetGrid(lI).GridControl.DefaultView;
                if (!lGridView.OptionsView.ColumnAutoWidth) {
                    foreach (GridColumn lColumn in lGridView.VisibleColumns)
                        lTableWidth += lColumn.Width;
                }
                else if (lGridView.GridControl.Width == 0)
                    lTableWidth = lMinWidth; //GetGrid(i).ChartControl.Width;
                
                if (lTableWidth > lMinWidth)
                    lMinWidth = lTableWidth;
            }
            return lMinWidth;
        }

        public BaseGridControl GetGrid(int index) {
            return (index >= 0 && index < Length) ? _grids[Length - 1 - index] : null;
        }

        private int MeasureTextWidth(string text, Font font) {
            return TextRenderer.MeasureText(text, font).Width;
        }

        #endregion

        /// <summary>
        /// Save changes for modified data
        /// </summary>
        /// <returns>If changes was saved successfully</returns>
        public bool SaveChanges() {
            bool res = false;
            bool wasException = false;
            foreach (BaseGridControl grid in _grids) {
                grid.DefaultView.PostEditor(); // Save dataView state
                grid.Enabled = false; //Use this instead of Prev/Next Row because it doesn't 
                grid.Enabled = true; // work with 1 row table;

                try {
                    var saveChanges = grid.SaveChanges();
                    res |= saveChanges;
                }
                catch (Exception ex) {
                    if (ex.InnerException != null && ex.InnerException is SqlException) {
                        var innerException = ex.InnerException as SqlException;
                        if (innerException.Class == 16
                            && (innerException.Number == 8114 //Error converting data type %ls to %ls.
                                || innerException.Number == 8115 //Arithmetic overflow error converting %ls to data type %ls.
                               )) {
                            XtraMessageBox.Show(innerException.Message, Resource.Error, MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
                            wasException = true;
                            continue;
                        }
                    }
                    throw;
                }
            }
            if (res && !wasException)
                UpdateData();
            return res;
        }
    }
}