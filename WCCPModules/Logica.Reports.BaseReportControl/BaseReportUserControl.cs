﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using System.Collections.ObjectModel;
using DevExpress.XtraTab;
using DevExpress.XtraGrid;
using DevExpress.XtraBars;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.ConfigXmlParser;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;
using Logica.Reports.Common;
using System.IO;
using System.Data.SqlClient;
using DevExpress.LookAndFeel;
using DevExpress.XtraPrintingLinks;
using Logica.Reports.BaseReportControl.Utility;

namespace Logica.Reports.BaseReportControl
{
    /// <summary>
    /// Provide base functionality for reports. 
    /// To use it you need to inherit your user control from this class and override SettingsForm property
    /// </summary>
//#if DEBUG
	public partial class BaseReportUserControl : DevExpress.XtraEditors.XtraUserControl
//#else
//    public abstract partial class BaseReportUserControl : DevExpress.XtraEditors.XtraUserControl
//#endif
    {
        private bool _isOffTradeReport;

        /// <summary>
        /// Report ID
        /// </summary>
        public int ReportId { get; set; }

        private bool? hasWriteAccess;
        
        /// <summary>
        /// User write access to the plan tables
        /// </summary>
        public bool HasWriteAccess {
            get {
                if (!hasWriteAccess.HasValue) {
                    DataAccessLayer.OpenConnection();
                    try {
                        int id = ReportId;
                        hasWriteAccess = AdminReportProvider.GetUserReportWriteAccess(id);
                    }
                    catch { }
                    DataAccessLayer.CloseConnection();
                }
                return hasWriteAccess.Value;
            }
        }

        public bool off_reports {
            get { return _isOffTradeReport; }
            set { _isOffTradeReport = value; }
        }

        /// <summary>
        /// Menu click handler delegate
        /// </summary>
        public delegate void MenuClickHandler(object sender, XtraTabPage selectedPage);
        /// <summary>
        /// Menu close click handler delegate. 
        /// </summary>
        public delegate bool MenuCloseClickHandler(object sender, XtraTabPage selectedPage);
        /// <summary>
        /// Menu buttons rendering delegate.
        /// </summary>
        public delegate void MenuButtonsRenderingHandler(object sender, MenuButtonsRenderingEventArgs eventArgs);
        /// <summary>
        /// Refresh data event
        /// </summary>
        public event MenuClickHandler RefreshClick;
        /// <summary>
        /// Save data event
        /// </summary>
        public event MenuClickHandler SaveClick;
        /// <summary>
        /// Open settings form event
        /// </summary>
        public event MenuClickHandler SettingsFormClick;
        /// <summary>
        /// Close form event
        /// </summary>
        public event MenuClickHandler CloseClick;
        /// <summary>
        /// Close form event
        /// </summary>
        public event MenuCloseClickHandler MenuCloseClick;    
        /// <summary>
        /// Print report event
        /// </summary>
        public event MenuClickHandler PrintClick;
        /// <summary>
        /// Print all report event
        /// </summary>
        public event MenuClickHandler PrintAllClick;

        public event MenuClickHandler SaveForPDAClick;
        /// <summary>
        /// Occurs before menu buttons rendering.
        /// </summary>
        public event MenuButtonsRenderingHandler MenuButtonsRendering;
        public delegate void ExportMenuClickHandler(object sender, XtraTabPage selectedPage, ExportToType type, string fName);
        public delegate void CustomExportHandler(object sender, CustomExportEventArgs e);

        public delegate CompositeLink PrepareExportMenuClickHandler(object sender, XtraTabPage selectedPage, ExportToType type);
        /// <summary>
        /// Export data event
        /// </summary>
        public event ExportMenuClickHandler ExportClick;
        public event PrepareExportMenuClickHandler PrepareExport;
        public event CustomExportHandler CustomExport;
        
        public int PrintingMarginOffest
        {
            set { report.PrintingMargin = value; }
        }
        private Report report;
        /// <summary>
        /// BaseReportUserControl structure. Object model, that reflecting XML settings from DB
        /// </summary>
        public Report Report
        {
            get { return report; }
        }
        /// <summary>
        /// Represent tabs container control
        /// </summary>
        public XtraTabControl TabControl
        {
            get { return tabManager; }
        }
        /// <summary>
        /// Represent selected tab
        /// </summary>
        public BaseTab SelectedTab
        {
            get
            {
                if (null == tabManager.SelectedTabPage || !(tabManager.SelectedTabPage is BaseTab))
                    return null;
                return (tabManager.SelectedTabPage as BaseTab);
            }
        }
        /// <summary>
        /// Constructor without params
        /// </summary>
        public BaseReportUserControl() : this(-1) {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Report identifier in DB</param>
        public BaseReportUserControl(int id) {
            string xmlSettings = String.Empty;
            DataAccessLayer.OpenConnection();
            ReportId = id;
            xmlSettings = AdminReportProvider.ReportConfig(id);
            DataAccessLayer.CloseConnection();
            if (String.IsNullOrEmpty(xmlSettings))
                throw new ArgumentException("Config file absents for report id=" + id);
            InitializeComponent();
            //DEBUGING...
            //FileStream fs = new FileStream(@"C:\Logica\Logica.Reports\External\sample.xml", FileMode.Open);
            //StreamReader sr = new StreamReader(fs);
            //string xmlSettings = sr.ReadToEnd();
            //xmlSettings = xmlSettings.Replace("False", "false").Replace("True", "true");


            report = XmlParserManager.ConvertXmlToModel(xmlSettings);
            tabManager.SelectedPageChanged += new TabPageChangedEventHandler(tabManager_SelectedPageChanged);
            SetCustomStyle();
            LocalizeForm();
            CustomizeMenu();
        }

        #region Clone
        private Tab[] templateTabs = null;
        /// <summary>
        /// Create clones for tabs from template
        /// </summary>
        /// <param name="postfix">postfix for new clone tab</param>
        /// <returns>Return new tabs structure. Dictionary<Guid,Guid>: key - Template Tab id, value - new tab id</returns>
        public Dictionary<Guid, Guid> CloneTabs(string postfix)
        {
            if (null == templateTabs)
            {
                templateTabs = report.Tabs;
                report.Tabs = new Tab[] { };
            }

            Dictionary<Guid, Guid> tabs = new Dictionary<Guid, Guid>();
            List<Tab> newTabList = new List<Tab>(report.Tabs);
            int tabShift = newTabList.Count;
            foreach (Tab tabTemplate in templateTabs)
            {
                string newHeader = String.Format("{0} ({1})", tabTemplate.Header, postfix);
                Tab cloneTab = newTabList.Find(t => t.Header == newHeader);
                if (null == cloneTab)//Clone does not exists
                {
                    cloneTab = (Tab)tabTemplate.Clone();
                    cloneTab.Header = newHeader;
                    cloneTab.Order = tabShift + tabTemplate.Order;
                    cloneTab.Id = Guid.NewGuid();
                    newTabList.Add(cloneTab);
                }
                tabs.Add(tabTemplate.Id, cloneTab.Id);
            }
            report.Tabs = newTabList.ToArray();
            return tabs;
        }
        #endregion

        #region Helper
        /// <summary>
        /// Set defined in XML config style for current control
        /// </summary>
        private void SetCustomStyle()
        {
            //UserLookAndFeel.Default.UseDefaultLookAndFeel = false;
            UserLookAndFeel.Default.UseWindowsXPTheme = false;
            UserLookAndFeel.Default.Style = LookAndFeelStyle.Skin;
            //style views http://www.devexpress.com/Help/?document=winconcepts/customdocument2534.htm
            UserLookAndFeel.Default.SkinName = report.SkinName;
        }
        /// <summary>
        /// Adjust menu for  current control
        /// </summary>
        private void CustomizeMenu()
        {
            btnExportAllToExcel.Tag = btnExportToExcel.Tag = ExportToType.Xls;
            btnExportAllToHtml.Tag = btnExportToHtml.Tag = ExportToType.Html;
            btnExportAllToMht.Tag = btnExportToMht.Tag = ExportToType.Mht;
            btnExportAllToPdf.Tag = btnExportToPdf.Tag = ExportToType.Pdf;
            btnExportAllToRtf.Tag = btnExportToRtf.Tag = ExportToType.Rtf;
            btnExportAllToText.Tag = btnExportToText.Tag = ExportToType.Txt;
            btnExportAllToImage.Tag = btnExportToImage.Tag = ExportToType.Bmp;
            btnExportAllToCsv.Tag = btnExportToCsv.Tag = ExportToType.Csv;
            btnExportAllToXlsx.Tag = btnExportToXlsx.Tag = ExportToType.Xlsx;

            btnExportAllToExcel.Visibility = btnExportToExcel.Visibility = report.ExportXls ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToHtml.Visibility = btnExportToHtml.Visibility = report.ExportHtml ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToMht.Visibility = btnExportToMht.Visibility = report.ExportMht ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToPdf.Visibility = btnExportToPdf.Visibility = report.ExportPdf ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToRtf.Visibility = btnExportToRtf.Visibility = report.ExportRtf ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToText.Visibility = btnExportToText.Visibility = report.ExportTxt ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToImage.Visibility = btnExportToImage.Visibility = report.ExportImage ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToCsv.Visibility = btnExportToCsv.Visibility = report.ExportCsv ? BarItemVisibility.Always : BarItemVisibility.Never;
            btnExportAllToXlsx.Visibility = btnExportToXlsx.Visibility = report.ExportXlsx ? BarItemVisibility.Always : BarItemVisibility.Never;
        }
        protected void SetExportType(ExportToType type, bool show)
        {
            BarItemVisibility enable = show ? BarItemVisibility.Always : BarItemVisibility.Never;
            switch(type)
            {
                case ExportToType.Xlsx:
                    btnExportAllToXlsx.Visibility = btnExportToXlsx.Visibility = enable;
                    break;
                case ExportToType.Html:
                    btnExportAllToHtml.Visibility = btnExportToHtml.Visibility = enable;
                    break;
                case ExportToType.Mht:
                    btnExportAllToMht.Visibility = btnExportToMht.Visibility = enable;
                    break;
                case ExportToType.Pdf:
                    btnExportAllToPdf.Visibility = btnExportToPdf.Visibility = enable;
                    break;
                case ExportToType.Rtf:
                    btnExportAllToRtf.Visibility = btnExportToRtf.Visibility = enable;
                    break;
                case ExportToType.Txt:
                    btnExportAllToText.Visibility = btnExportToText.Visibility = enable;
                    break;
                case ExportToType.Bmp:
                    btnExportAllToImage.Visibility = btnExportToImage.Visibility = enable;
                    break;
                case ExportToType.Csv:
                    btnExportAllToCsv.Visibility = btnExportToCsv.Visibility = enable;
                    break;
            }
        }

        /// <summary>
        /// Set translated text for control from resource file
        /// </summary>
        private void LocalizeForm()
        {
            btnSettings.SuperTip = new SuperToolTip();
            btnSettings.SuperTip.Items.Add(Resource.ReportSettings);
            btnSettings.Caption = Resource.ReportSettings;

            btnSave.SuperTip = new SuperToolTip();
            btnSave.SuperTip.Items.Add(Resource.Save);
            btnSave.Caption = Resource.Save;

            btnClose.SuperTip = new SuperToolTip();
            btnClose.SuperTip.Items.Add(Resource.CloseSheet);
            btnClose.Caption = Resource.CloseSheet;

            btnPrint.SuperTip = new SuperToolTip();
            btnPrint.SuperTip.Items.Add(Resource.Print);
            btnPrint.Caption = Resource.Print;

            btnRefresh.SuperTip = new SuperToolTip();
            btnRefresh.SuperTip.Items.Add(Resource.Refresh);
            btnRefresh.Caption = Resource.Refresh;

            btnExportTo.SuperTip = new SuperToolTip();
            btnExportTo.SuperTip.Items.Add(Resource.ExportTo);
            btnExportTo.Caption = Resource.ExportTo;

            btnExportAllTo.SuperTip = new SuperToolTip();
            btnExportAllTo.SuperTip.Items.Add(Resource.ExportAllTo);
            btnExportAllTo.Caption = Resource.ExportAllTo;

            btnExportAllToCsv.Caption = btnExportToCsv.Caption = Resource.ExportToCsv;
            btnExportAllToExcel.Caption = btnExportToExcel.Caption = Resource.ExportToExcel;
            btnExportAllToHtml.Caption = btnExportToHtml.Caption = Resource.ExportToHtml;
            btnExportAllToImage.Caption = btnExportToImage.Caption = Resource.ExportToBmp;
            btnExportAllToMht.Caption = btnExportToMht.Caption = Resource.ExportToMht;
            btnExportAllToPdf.Caption = btnExportToPdf.Caption = Resource.ExportToPdf;
            btnExportAllToRtf.Caption = btnExportToRtf.Caption = Resource.ExportToRtf;
            btnExportAllToText.Caption = btnExportToText.Caption = Resource.ExportToText;
            btnExportAllToXlsx.Caption = btnExportToXlsx.Caption = Resource.ExportToXlsx;

        }
        #endregion

        #region UpdateData
        /// <summary>
        /// Update all data sheets (tabs) appropriate for sheetSettings 
        /// </summary>
        /// <param name="sheetSettings">Setting collection for each tab separately</param>
        public void UpdateSheets(List<SheetParamCollection> sheetSettings)
        {
            bool initLoaded = false;
            foreach (SheetParamCollection setting in sheetSettings)
            {
                Tab tab = null;
                foreach (Tab t in report.Tabs)
                {
                    if (t.Id == setting.TabId)
                    {
                        tab = t;
                        break;
                    }
                }
                if (null == tab)
                    continue;
                XtraTabPage page = null;
                foreach (XtraTabPage pg in tabManager.TabPages)
                {
                    if (pg is BaseTab && (pg as BaseTab).SheetSettings.TabId == setting.TabId)
                    {
                        (pg as BaseTab).SheetStructure.Order = tab.Order; // update SheetStructure.Order field because sorting may be changed
                        page = pg;
                        break;
                    }
                }

                if (null == page)
                {
                    if (!initLoaded)
                    {//load only if at least 1 BaseTab exist
                        WaitManager.StartWait();
                        ConstructTemporaryData(setting);
                        initLoaded = true;
                    }

                    BaseTab newTab = new BaseTab(tab, setting);
                    newTab.IsOffReport = this.off_reports;
                    newTab.PrintingMarginOffest = report.PrintingMargin;
                    tabManager.TabPages.Insert(tabManager.TabPages.Count, newTab);
                }
                else
                {
                    BaseTab existTab = (page as BaseTab);
                    if (!existTab.CompareSettings(setting))
                    {//load only if at least 1 BaseTab exist
                        if (!initLoaded)
                        {
                            WaitManager.StartWait();
                            ConstructTemporaryData(setting);
                            initLoaded = true;
                        }
                        existTab.UpdateData(setting);
                    }
                }
            }
            //remove unselected tabs
            for (int i = tabManager.TabPages.Count - 1; i >= 0; i--)
                if (tabManager.TabPages[i] is BaseTab && !sheetSettings.Exists(delegate(SheetParamCollection setting) { return setting.TabId == (tabManager.TabPages[i] as BaseTab).SheetStructure.Id; }))
                    tabManager.TabPages.Remove(tabManager.TabPages[i]);
            //sort tabs
            BaseTab.SortTabPages(tabManager);
            if (initLoaded)
            {
                DestructTemporaryData();
                WaitManager.StopWait();
            }

            UpdateToolButtons();

        }

        private List<string> preInitScripts;
        /// <summary>
        /// Get preinit scripts, used to prepare DB for creating temporary tables
        /// </summary>
        /// <param name="settings">Settings defined by user</param>
        /// <returns>list of scripts which should be executed</returns>
        private List<string> GetPreInitScripts(SheetParamCollection settings)
        {
            if (null == preInitScripts)
            {
                preInitScripts = new List<string>();
                if (null != report.Preconditions && null != report.Preconditions.PreInit && null != report.Preconditions.PreInit.Queries)
                {
                    foreach (Query query in report.Preconditions.PreInit.Queries)
                    {
                        List<SqlParameter> list = new List<SqlParameter>();
                        foreach (Parameter param in query.Parameters)
                        {
                            SqlParameter sqlParam = param.ConvertToSqlParameter();
                            if (settings == null || (!settings.Exists(s => s.SqlParamName == param.Name) && String.IsNullOrEmpty(param.DefaultValue)))
                                throw new ArgumentNullException(param.Name, "Can not find param for preinit SQL query");
                            SheetParam sheetParam = settings.Find(s => s.SqlParamName == param.Name);
                            if (null != sheetParam)
                                sqlParam.Value = sheetParam.Value;
                            list.Add(sqlParam);
                        }
                        object resQuery = DataAccessLayer.ExecuteScalarStoredProcedure(query.Name, list.ToArray());
                        preInitScripts.Add(resQuery.ToString());
                    }
                }
            }
            return preInitScripts;
        }

        /// <summary>
        /// Cteate in DB temporary tables
        /// </summary>
        /// <param name="settings">Settings defined by user</param>
        public void ConstructTemporaryData(SheetParamCollection settings)
        {
            DataAccessLayer.OpenConnection();
            //execute preinit queries (creation tables)
            List<string> lst = GetPreInitScripts(settings);
            foreach (string query in lst)
                DataAccessLayer.ExecuteNonQuery(query, new SqlParameter[] { });
            //execute oninit queries(filing tables)
            if (null != report.Preconditions && null != report.Preconditions.OnInit && null != report.Preconditions.OnInit.Queries)
            {
                foreach (Query query in report.Preconditions.OnInit.Queries)
                {
                    List<SqlParameter> list = null;
                    if (String.IsNullOrEmpty(query.Name))
                        continue;
                    if (null != query.Parameters)
                    {
                        list = new List<SqlParameter>();
                        foreach (Parameter param in query.Parameters)
                        {
                            SqlParameter sqlParam = param.ConvertToSqlParameter();
                            if (settings == null || (!settings.Exists(s => s.SqlParamName == param.Name) && String.IsNullOrEmpty(param.DefaultValue)))
                                throw new ArgumentNullException(param.Name, "Can not find param for preinit SQL query");
                            SheetParam sheetParam = settings.Find(s => s.SqlParamName == param.Name);
                            if (null != sheetParam)
                                sqlParam.Value = sheetParam.Value;
                            list.Add(sqlParam);
                        }
                    }
                    DataAccessLayer.ExecuteNonQueryStoredProcedure(query.Name, list.ToArray());
                }
            }
        }
        /// <summary>
        /// Close connection and drop temporary data (tables)
        /// </summary>
        public void DestructTemporaryData() {
            DataAccessLayer.CloseConnection();
        }

        #endregion

        #region Menu Event

        /// <summary>
        /// Settings button click
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != SettingsFormClick)
                SettingsFormClick(this, tabManager.SelectedTabPage);
        }

        /// <summary>
        /// Close button click
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            CloseSheet();
        }

        /// <summary>
        /// Standart close button('X') click
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void tabManager_CloseButtonClick(object sender, EventArgs e)
        {
            CloseSheet();
        }
        /// <summary>
        /// Close selected sheet
        /// </summary>
        private void CloseSheet()
        {
            if (null == tabManager.SelectedTabPage)
            {
                return;
            }

            XtraTabPage delPage = tabManager.SelectedTabPage;
            if (null != CloseClick)
            {
                CloseClick(this, tabManager.SelectedTabPage);
            }

            bool reallyClose = true;
            if (MenuCloseClick != null) 
            {
                reallyClose = MenuCloseClick(this, tabManager.SelectedTabPage);
            }

            if (reallyClose && null != delPage && delPage is BaseTab && tabManager.TabPages.Contains(delPage))
                tabManager.TabPages.Remove(delPage);
        }

        /// <summary>
        /// Refresh button click, refresh data of selected list
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != SelectedTab)
            {
                WaitManager.StartWait();
                ConstructTemporaryData(SelectedTab.SheetSettings);
                SelectedTab.UpdateData();
                DestructTemporaryData();
                WaitManager.StopWait();
            }
            if (null != RefreshClick)
                RefreshClick(this, tabManager.SelectedTabPage);
        }
        /// <summary>
        /// Save button click, save modified data
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != SelectedTab)
                if (SelectedTab.SaveChanges())
                    DevExpress.XtraEditors.XtraMessageBox.Show(Resource.DataSaved);

            if (null != SaveClick)
                SaveClick(this, tabManager.SelectedTabPage);
        }

        /// <summary>
        /// Occurs then selected page changed
        /// </summary>
        /// <param name="sender">Tab manager</param>
        /// <param name="e">Event arguments</param>
        public void tabManager_SelectedPageChanged(object sender, TabPageChangedEventArgs e) {
            if (null == e.Page)
                return;
            UpdateToolButtons();
        }

        #region Export&Print engine

        /// <summary>
        /// Print button click, show print dialog box for all sheets
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        public void btnPrintAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            CompositeLink cl = new CompositeLink(new PrintingSystem());
            foreach (object tb in tabManager.TabPages)
            {
                if (tb is IPrint)
                    cl.Links.Add(((IPrint)tb).PrepareCompositeLink());
            }
            cl.CreateDocument();
            cl.PrintingSystem.PreviewFormEx.Show();
            if (null != PrintAllClick)
                PrintAllClick(this, tabManager.SelectedTabPage);
        }

        /// <summary>
        /// Print button click, show print dialog box
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null == tabManager.SelectedTabPage)
                return;
            if (null != tabManager.SelectedTabPage && tabManager.SelectedTabPage is IPrint)
                (tabManager.SelectedTabPage as IPrint).PrepareCompositeLink().PrintingSystem.PreviewFormEx.Show();
            if (null != PrintClick)
                PrintClick(this, tabManager.SelectedTabPage);
        }

        /// <summary>
        /// Export button click, export tab content to different formats
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e) {
            if (null == tabManager.SelectedTabPage)
                return;
            ExportToType lExportToType = (ExportToType)e.Item.Tag;
            CustomExportEventArgs lArgs = new CustomExportEventArgs(false, lExportToType, tabManager.SelectedTabPage);
            if (CustomExport != null)
                CustomExport(this, lArgs);
            if (!lArgs.Handled)
                ExportSingle(lExportToType);
        }

        /// <summary>
        /// Select File Path
        /// </summary>
        /// <param name="type"></param>
        /// <returns>Path</returns>
        protected virtual String SelectFilePath(ExportToType type)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = Resource.SpecifyFileName;
            sfd.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            sfd.FileName = "Report";
            sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }
            return res;
        }

        /// <summary>
        /// Export single tab to different formats
        /// </summary>
        /// <param name="expType">Export document formt</param>
        private void ExportSingle(ExportToType expType) {
            if (null == tabManager.SelectedTabPage)
                return;

            string lFilePath = SelectFilePath(expType);
            if (string.IsNullOrEmpty(lFilePath))
                return;

            if (null != SelectedTab)
                SelectedTab.Export(lFilePath, expType);
            if (null != ExportClick)
                ExportClick(this, tabManager.SelectedTabPage, expType, lFilePath);
            if (null == PrepareExport)
                return;

            CompositeLink lTmpLink = PrepareExport(this, tabManager.SelectedTabPage, expType);
            if (null != lTmpLink)
                ExportToFile(lFilePath, lTmpLink, expType);
        }

        /// <summary>
        /// Export tab to document
        /// </summary>
        /// <param name="fileName">Path to new document</param>
        /// <param name="compositeLink">CompositeLink for export</param>
        /// <param name="type">Type of new document</param>
        internal void ExportToFile(string fileName, CompositeLink compositeLink, ExportToType type) {
            TabOperationHelper.SetMargins(ref compositeLink, report.PrintingMargin);
            switch (type) {
                case ExportToType.Html:
                    compositeLink.PrintingSystem.ExportToHtml(fileName);
                    break;
                case ExportToType.Mht:
                    compositeLink.PrintingSystem.ExportToMht(fileName);
                    break;
                case ExportToType.Pdf:
                    compositeLink.PrintingSystem.ExportToPdf(fileName, new PdfExportOptions() { Compressed = true });
                    break;
                case ExportToType.Rtf:
                    compositeLink.PrintingSystem.ExportToRtf(fileName);
                    break;
                case ExportToType.Txt:
                    compositeLink.PrintingSystem.ExportToText(fileName);
                    break;
                case ExportToType.Xls:
                    compositeLink.PrintingSystem.ExportToXls(fileName, new XlsExportOptions() { SheetName = "Report" });
                    break;
                case ExportToType.Xlsx:
                    compositeLink.PrintingSystem.ExportToXlsx(fileName);
                    break;
                case ExportToType.Bmp:
                    compositeLink.PrintingSystem.ExportToImage(fileName);
                    break;
                case ExportToType.Csv:
                    compositeLink.PrintingSystem.ExportToCsv(fileName);
                    break;
            }
        }

        /// <summary>
        /// Export All button click, export all tab content to different formats
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnExportAllTo_ItemClick(object sender, ItemClickEventArgs e) {
            if (null == tabManager.SelectedTabPage)
                return;

            ExportToType lExportType = (ExportToType)e.Item.Tag;
            string lConfirmMsg = String.Format(Resource.ConfirmExportAllSheets, tabManager.TabPages.Count);
            if (lExportType == ExportToType.Pdf)
                lConfirmMsg = String.Format(Resource.ConfirmExportAllSheetsTo1File, tabManager.TabPages.Count);

            if (tabManager.TabPages.Count < 2)
                ExportSingle(lExportType);
            else
            if (DialogResult.Yes == MessageBox.Show(lConfirmMsg, Resource.Information, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) {
                string lFilePath = SelectFilePath(lExportType);
                ExportReportToFile(lFilePath, lExportType);
                return;
            }
        }

        public void ExportReportToFile(string filePath, ExportToType exportType) {
            if (string.IsNullOrEmpty(filePath))
                return;
            if (exportType == ExportToType.Pdf) {
                CompositeLink lMainLink = new CompositeLink(new PrintingSystem());
                
                for (int lIndex = 0; lIndex < tabManager.TabPages.Count; lIndex++) {
                    CompositeLink lTmpLink = null;
                    BaseTab lTab = tabManager.TabPages[lIndex] as BaseTab;
                    if (null != lTab)
                        lTmpLink = lTab.PrepareCompositeLink(ExportToType.Pdf);
                    else {
                        if (null != PrepareExport)
                            lTmpLink = PrepareExport(this, tabManager.TabPages[lIndex], exportType);
                    }

                    if (null != lTmpLink) {
                        lMainLink.PrintingSystem.Document.Pages.AddRange(lTmpLink.PrintingSystem.Document.Pages);
                        lMainLink.PrintingSystem.ContinuousPageNumbering = true;
                    }
                    
                    if (null != ExportClick)
                        ExportClick(this, tabManager.TabPages[lIndex], exportType, filePath);
                }
                ExportToFile(filePath, lMainLink, exportType);
            }
            else {
                string lDirectory = Path.GetDirectoryName(filePath);
                string lFileName = Path.GetFileNameWithoutExtension(filePath);
                for (int lIndex = 0; lIndex < tabManager.TabPages.Count; lIndex++) {
                    BaseTab lTab = tabManager.TabPages[lIndex] as BaseTab;
                    string lFileNamePart;
                    if (null != lTab) {
                        lFileNamePart = TabOperationHelper.RemoveSpecialCharacters(lTab.Text);
                        string lFName = String.Format("{0}\\{1}_{2}.{3}", lDirectory, lFileName, lFileNamePart, exportType.ToString().ToLower());
                        lTab.Export(lFName, exportType);
                    } 
                    else {
                        lFileNamePart = TabOperationHelper.RemoveSpecialCharacters(tabManager.TabPages[lIndex].Text);
                        string lFName = String.Format("{0}\\{1}_{2}.{3}", lDirectory, lFileName, lFileNamePart, exportType.ToString().ToLower());
                        if (null != ExportClick)
                            ExportClick(this, lTab, exportType, lFName);
                        if (null != PrepareExport) {
                            CompositeLink lTmpLink = PrepareExport(this, tabManager.TabPages[lIndex], exportType);
                            if (null != lTmpLink)
                                ExportToFile(lFName, lTmpLink, exportType);
                        }
                    }
                }
            }
        }

        public void ExportReportWithMode(string filePath, ExportToType exportType, ExportModeEnum exportMode, long id) {
            if (string.IsNullOrEmpty(filePath))
                return;
            if (exportType != ExportToType.Pdf)
                return;
            if (tabManager.TabPages.Count < 1)
                return;
            if (exportMode == ExportModeEnum.M1) {
                CompositeLink lMainLink = new CompositeLink(new PrintingSystem());
                
                long lId = id;
                for (int lIndex = 0; lIndex < tabManager.TabPages.Count; lIndex++) {
                    if (!tabManager.TabPages[lIndex].Text.StartsWith("M1"))
                        continue;
                    
                    BaseTab lTab = tabManager.TabPages[lIndex] as BaseTab;
                    if (null == lTab)
                        continue;
                    
                    long lIdCur = ConvertEx.ToLongInt(lTab.SheetSettings[0].Value);
                    if (lIdCur != lId)
                        continue;
                    
                    CompositeLink lTmpLink = lTab.PrepareCompositeLink(ExportToType.Pdf);
                    if (null == lTmpLink)
                        continue;
                    
                    lMainLink.PrintingSystem.Document.Pages.AddRange(lTmpLink.PrintingSystem.Document.Pages);
                    lMainLink.PrintingSystem.ContinuousPageNumbering = true;
                    lTmpLink.Dispose();
                }
                ExportToFile(filePath, lMainLink, exportType);
                lMainLink.Dispose();
            }
            else if (exportMode == ExportModeEnum.M2) {
                CompositeLink lMainLink = new CompositeLink(new PrintingSystem());
                
                long lId = id;
                for (int lIndex = 0; lIndex < tabManager.TabPages.Count; lIndex++) {
                    if (!tabManager.TabPages[lIndex].Text.StartsWith("M2"))
                        continue;
                    
                    BaseTab lTab = tabManager.TabPages[lIndex] as BaseTab;
                    if (null == lTab)
                        continue;
                    
                    long lIdCur = ConvertEx.ToLongInt(lTab.SheetSettings[0].Value);
                    if (lIdCur != lId)
                        continue;
                    
                    CompositeLink lTmpLink = lTab.PrepareCompositeLink(ExportToType.Pdf);
                    if (null == lTmpLink)
                        continue;
                    
                    lMainLink.PrintingSystem.Document.Pages.AddRange(lTmpLink.PrintingSystem.Document.Pages);
                    lMainLink.PrintingSystem.ContinuousPageNumbering = true;
                    lTmpLink.Dispose();
                }
                ExportToFile(filePath, lMainLink, exportType);
                lMainLink.Dispose();
            }
        }

        /// <summary>
        /// Updates the tool buttons.
        /// </summary>
        private void UpdateToolButtons() {
            if (SelectedTab != null) {
                btnSave.Visibility = SelectedTab.IsReadOnlyReport ? BarItemVisibility.Never : BarItemVisibility.Always;
                btnSaveForPDA.Visibility = SelectedTab.IsReadOnlyReport ? BarItemVisibility.Never : BarItemVisibility.Always;
            }
            else if (MenuButtonsRendering != null) {
                MenuButtonsRenderingEventArgs lArgs = new MenuButtonsRenderingEventArgs { SelectedPage = tabManager.SelectedTabPage };
                // Ask exact tab whether the button state is visible
                MenuButtonsRendering(this, lArgs);

                btnSave.Visibility = lArgs.ShowSaveBtn ? BarItemVisibility.Always : BarItemVisibility.Never;
                btnSettings.Visibility = lArgs.ShowParametersBtn ? BarItemVisibility.Always : BarItemVisibility.Never;
                btnExportAllTo.Visibility = lArgs.ShowExportAllBtn ? BarItemVisibility.Always : BarItemVisibility.Never;
                btnExportTo.Visibility = lArgs.ShowExportBtn ? BarItemVisibility.Always : BarItemVisibility.Never;
                btnPrintAll.Visibility = lArgs.ShowPrintAllBtn ? BarItemVisibility.Always : BarItemVisibility.Never;
                btnPrint.Visibility = lArgs.ShowPrintBtn ? BarItemVisibility.Always : BarItemVisibility.Never;
            }
        }

        #endregion

        /// <summary>
        /// Occurs when size changed
        /// </summary>
        /// <param name="sender">Tab manager</param>
        /// <param name="e">Event arguments</param>
        private void BaseReportUserControl_SizeChanged(object sender, EventArgs e) {
            // Делаем небольшой  стресс контрола чтобы вызвать обновление вертикального скрол бара
            // См. дефект #9177. Это обходно решение - требует исправлений на DevExpress стороне
            --tabManager.Height;
            ++tabManager.Height;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public class MenuButtonsRenderingEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="MenuButtonsRenderingEventArgs"/> class.
            /// </summary>
            public MenuButtonsRenderingEventArgs()
            {
                this.ShowSaveBtn = false;
                this.ShowParametersBtn = true;
                this.ShowExportAllBtn = true;
                this.ShowExportBtn = true;
                this.ShowPrintAllBtn = true;
                this.ShowPrintBtn = true;
            }

            /// <summary>
            /// Gets or sets the selected page.
            /// </summary>
            /// <value>The selected page.</value>
            public XtraTabPage SelectedPage { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether show save button.
            /// </summary>
            /// <value><c>true</c> if show save button; otherwise, <c>false</c>.</value>
            public bool ShowSaveBtn { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether show parameters button.
            /// </summary>
            /// <value><c>true</c> if show parameters button; otherwise, <c>false</c>.</value>
            public bool ShowParametersBtn { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether [show export all BTNS].
            /// </summary>
            /// <value><c>true</c> if [show export all BTNS]; otherwise, <c>false</c>.</value>
            public bool ShowExportAllBtn { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether [show export BTN].
            /// </summary>
            /// <value><c>true</c> if [show export BTN]; otherwise, <c>false</c>.</value>
            public bool ShowExportBtn { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether [show print all BTN].
            /// </summary>
            /// <value><c>true</c> if [show print all BTN]; otherwise, <c>false</c>.</value>
            public bool ShowPrintAllBtn { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether [show print BTN].
            /// </summary>
            /// <value><c>true</c> if [show print BTN]; otherwise, <c>false</c>.</value>
            public bool ShowPrintBtn { get; set; }
        }

        private void btnSaveForPDA_ItemClick(object sender, ItemClickEventArgs e) {
            if (null != SaveForPDAClick)
                SaveForPDAClick(this, tabManager.SelectedTabPage);
        }
    }
}
