﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using Logica.Reports.ConfigXmlParser.GridExtension;
using DevExpress.XtraGrid.Views.BandedGrid;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.DataAccess;
using DevExpress.XtraGrid.Columns;
using System.Data.SqlClient;
using Logica.Reports.Common;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraGrid.Views.Printing;
using DevExpress.Utils;


namespace Logica.Reports.BaseReportControl
{
    /// <summary>
    /// Grid with header.
    /// </summary>
    public partial class BaseGridControl : BaseTable

    {
        private bool getResult = false;
        public bool isOffReport
        {
            get { return getResult; }
            set { getResult = value; }
        }

        public CustomGridControl GridControl
        {
          get { return grdReportTable; }
        }

        public ChartControl ChartControl
        {
          get { return reportChart; }
        }

        public SplitContainerControl SplitContainer
        {
          get { return splitContainer; }
        }
        
        /// <summary>
        /// Return default view of GridControl 
        /// </summary>
        public CustomGridView DefaultView
        {
            get { return ((CustomGridView)grdReportTable.DefaultView); }
        }
        /// <summary>
        /// Constructor, adjust grid view and load data
        /// </summary>
        /// <param name="tbl">Structure settings for table (passed from DB via XML config string)</param>
        /// <param name="dt">Internal data</param>
        internal BaseGridControl(Table tbl, DataTable dt) : base(tbl)
        {
            InitializeComponent();
            DefaultView.ConfigureGridView(tbl.Body.GridXml);
            GridOperationHelper.ColumnCaptionFix(DefaultView);
            DefaultView.OptionsPrint.AutoWidth = false;
            UpdateDataSource(dt);
            DefaultView.ShownEditor += DefaultView_ShownEditor;
            DefaultView.ValidatingEditor += DefaultView_ValidatingEditor;
        }

        void DefaultView_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            var view = sender as GridView;
            if (view == null) return;
            var column = view.FocusedColumn as DynamicColumn;
            if (column == null) return;

            if (column.DataValidation == DataValidationRule.GTE0 && e.Value.ToString().StartsWith("-"))
            {
                e.Valid = false;
                e.ErrorText = "Field can't contain negative values";
            }
        }

        void DefaultView_ShownEditor(object sender, EventArgs e)
        {
            var view = sender as GridView;
            if (view == null) return;
            var column = view.FocusedColumn as DynamicColumn;
            if (column == null) return;
            if (column.DataValidation == DataValidationRule.GTE0 && view.ActiveEditor != null)
            {
                view.ActiveEditor.KeyPress += ValidateGTE0Rule;
            }
        }

        void ValidateGTE0Rule(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '-')
                e.Handled = true;
        }

        /// <summary>
        /// Update data in table
        /// </summary>
        /// <param name="dt">Internal data</param>
        internal void UpdateDataSource(DataTable dt)
        {
            DataTable dataSource = dt.Copy();
            DefaultView.FillData(dataSource);
            reportChart.DataSource = dataSource; 
        }
        /// <summary>
        /// Save modified rows
        /// </summary>
        /// <returns>If changes was saved successfully</returns>
        internal bool SaveChanges()
        {
            bool res = false;
            //find appropriate query
            Query modifyQuery = null;
            foreach (Query q in Table.Queries)
            {
                if (q.SqlQueryType == SQLQueryType.NonQuery)
                {
                    modifyQuery = q;
                    break;
                }
            }
            if (null == modifyQuery)
                throw new ArgumentException("Modify query is absent");
            //grdReportTable.DefaultView.PostEditor(); // Need this for saving data
            //grdReportTable.DefaultView.Focus();      //  without moving cursor
            //create param collection and fill parameters value
            DataTable gridDataSource = ((DataView)grdReportTable.DefaultView.DataSource).Table;
            foreach (DataRow dr in gridDataSource.Rows)
            {
                if (dr.RowState != DataRowState.Unchanged)
                {
                    List<SqlParameter> list = new List<SqlParameter>();
                    AddRegularSqlParameters(list, modifyQuery.Parameters, dr);
                    List<string> agregateFields = DefaultView.GetAgregateFields();
                    ExecuteModifyQueryWithDynamicParams(list, agregateFields, dr, modifyQuery.Name);
                    if (agregateFields.Count == 0)
                        DataAccessLayer.ExecuteStoredProcedure(modifyQuery.Name, list.ToArray());
                    dr.AcceptChanges();
                    res = true;
                }
            }
            return res;
        }
        /// <summary>
        /// Prepare controls for printing or exporting
        /// </summary>
        /// <param name="links">Collection of controls for printing or exporting</param>
        internal override void AppendLinks(LinkCollection links)
        {
            base.AppendLinks(links);
            if (Table.Mode == ShowMode.Grid || Table.Mode == ShowMode.GridChart)
            {
                links.Add(new PrintableComponentLink() { Component = grdReportTable });
            }
            links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = "" } });
            if (Table.Mode == ShowMode.Chart || Table.Mode == ShowMode.GridChart)
            {
                links.Add(new PrintableComponentLink() { Component = reportChart });
            }
        }

        public override void SetSize()
        {
            if (AllowCustom())
            {
                //Can't get Band rows Height
                if (DefaultView.RowCount > 0)
                    this.Height = CustomLayout(20, 30, 55, 20);
                else
                    this.Height = CustomLayout(0, 30, 35, 70);
            }
            else
            {
                base.SetSize();
            }
        }

        

        private int CustomLayout(int RowHeight, int FooterHeight, int BandsHeight, int AdditionalSpace)
        {
            return DefaultView.RowCount * (DefaultView.RowHeight == -1 ? RowHeight : DefaultView.RowHeight) +  //Data height 
                   lbHeader.Parent.Height + DefaultView.ColumnPanelRowHeight +                                 //Grid_Name component height + Columns_Header height
                   (DefaultView.FooterPanelHeight == -1 ? FooterHeight : DefaultView.FooterPanelHeight) +      //Footer height
                   DefaultView.MinBandPanelRowCount * BandsHeight +                                            //Band_Panel height
                   (DefaultView.RowHeight == -1 ? AdditionalSpace : DefaultView.RowHeight);                    //Additional space(1 row)
        }

        private bool AllowCustom()
        {
            bool result = false;
            if (isOffReport && Table.Mode == ShowMode.Grid && Parent != null && Parent.Controls != null)
            {
                result = true;
            }
            return result;
        }

        #region Helper methods
        /// <summary>
        /// Add sql params into collection
        /// </summary>
        /// <param name="list">Output list of params</param>
        /// <param name="queryParams">Input array of parameters</param>
        /// <param name="dr">Row in table</param>
        private void AddRegularSqlParameters(List<SqlParameter> list, Parameter[] queryParams, DataRow dr)
        {
            foreach (Parameter param in queryParams)
            {
                string columnName = param.Name.TrimStart('@');
                SqlParameter sqlParam = new SqlParameter();
                sqlParam.ParameterName = param.Name;
                sqlParam.SqlDbType = param.DataType;
                if (dr.Table.Columns[columnName] == null)
                {
                    sqlParam.Value = DBNull.Value;
                }
                else sqlParam.Value = dr[columnName];
                if (sqlParam.Value == DBNull.Value)
                {
                    var gridColumn =
                        DefaultView.VisibleColumns.Cast<DynamicColumn>().FirstOrDefault(
                            c =>
                            !string.IsNullOrEmpty(c.UpdateField) &&
                            c.UpdateField.Equals(columnName, StringComparison.InvariantCultureIgnoreCase));
                    if (gridColumn != null && !dr.IsNull(gridColumn.FieldName))
                    {
                        sqlParam.Value = dr[gridColumn.FieldName];
                    }
                }
                list.Add(sqlParam);
            }
        }
        /// <summary>
        /// Execute query for saving modified data
        /// </summary>
        /// <param name="list">Required params</param>
        /// <param name="agregateFields">Defined params</param>
        /// <param name="dr">Row of table</param>
        /// <param name="procedureName">Name of stored procedure</param>
        private void ExecuteModifyQueryWithDynamicParams(List<SqlParameter> list, List<string> agregateFields, DataRow dr, string procedureName)
        {
            foreach (GroupBand band in DefaultView.GroupedBands)
            {
                SqlParameter groupParam = list.Find((par) => par.ParameterName.TrimStart('@') == band.GroupColumn); // Replace
                if (groupParam != null)
                {
                    groupParam.Value = band.GroupColumnValue; // Replace
                }
                foreach (GridColumn column in band.Columns)
                {
                    string fieldName = column.FieldName;
                    int underscoreIndex = fieldName.LastIndexOf('_');
                    if (underscoreIndex != -1)
                    {
                        string paramName = fieldName.Substring(0, underscoreIndex);

                        SqlParameter param = list.Find((par) => par.ParameterName.TrimStart('@') == paramName);
                        if (param != null)
                        {
                            param.Value = dr[fieldName];
                        }
                    }
                }

                DataAccessLayer.ExecuteStoredProcedure(procedureName, list.ToArray());
                //DataAccessLayer.ExecuteNonQueryStoredProcedure(procedureName, list.ToArray());
            }
            /*foreach (string fieldName in agregateFields)
            {
                if (dr.Table.Columns[fieldName] != null)
                {
                    int underscoreIndex = fieldName.LastIndexOf('_');
                    int valueIndex;
                    string paramName = null;
                    if (underscoreIndex > -1)
                        paramName = fieldName.Substring(0, underscoreIndex);
                    
                    bool parseSuccess = int.TryParse(fieldName.Substring(underscoreIndex + 1), out valueIndex);
                    BandedGridColumn currentColumn = DefaultView.Columns.ColumnByFieldName(fieldName);
                    if (currentColumn != null && !string.IsNullOrEmpty(paramName) && parseSuccess)
                    {
                        GroupBand band = (GroupBand)currentColumn.OwnerBand;
                        foreach (SqlParameter param in list)
                        {
                            if (param.ParameterName.TrimStart('@') == paramName)
                            {
                                param.Value = dr[fieldName];
                            }
                            if (param.ParameterName.TrimStart('@') == band.GroupColumn)
                            {
                                param.Value = DefaultView.GroupValues[band.GroupColumn][valueIndex];
                            }
                        }
                    }
                }
                DataAccessLayer.ExecuteStoredProcedure(procedureName, list.ToArray());
            }*/
        }
        #endregion // Helper methods

        /*private void gridView1_CalcRowHeight(object sender, RowHeightEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.RowHeight = 48;//(int)gridView1.GetDataRow(e.RowHandle)["RowHeight"];

        }*/
        //ColumnHeaderCustomDrawEventArgs args;
        //public ColumnHeaderCustomDrawEventArgs GetColumnHeaderCustomDrawEventArgs()
        //{
        //    return args;
        //}

        /*private void gridView1_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {

            using (StringFormat sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Center;

                sf.LineAlignment = StringAlignment.Center;
                e.Appearance.DrawVString(e.Cache, e.Column.Caption, e.Appearance.Font, e.Appearance.GetForeBrush(e.Cache), e.Info.CaptionRect, sf, 90);
            }
        }*/
    }
}
