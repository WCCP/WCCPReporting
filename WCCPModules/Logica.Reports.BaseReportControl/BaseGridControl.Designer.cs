﻿namespace Logica.Reports.BaseReportControl
{
  public partial class BaseGridControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdReportTable = new Logica.Reports.BaseReportControl.CustomGridControl();
            this.gridView1 = new Logica.Reports.BaseReportControl.CustomGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdReportTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbHeader
            // 
            this.lbHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbHeader.Appearance.Options.UseFont = true;
            this.lbHeader.Size = new System.Drawing.Size(0, 19);
            // 
            // splitContainer
            // 
            this.splitContainer.Panel1.Controls.Add(this.grdReportTable);
            this.splitContainer.Size = new System.Drawing.Size(717, 455);
            this.splitContainer.SplitterPosition = 282;
            // 
            // reportChart
            // 
            this.reportChart.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Stretch;
            this.reportChart.Size = new System.Drawing.Size(429, 455);
            // 
            // grdReportTable
            // 
            this.grdReportTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdReportTable.Location = new System.Drawing.Point(0, 0);
            this.grdReportTable.MainView = this.gridView1;
            this.grdReportTable.Name = "grdReportTable";
            this.grdReportTable.Size = new System.Drawing.Size(282, 455);
            this.grdReportTable.TabIndex = 0;
            this.grdReportTable.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.GridControl = this.grdReportTable;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.gridView1.OptionsBehavior.SummariesIgnoreNullValues = true;
            this.gridView1.OptionsCustomization.AllowChangeBandParent = true;
            this.gridView1.OptionsCustomization.AllowRowSizing = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // BaseGridControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "BaseGridControl";
            this.Size = new System.Drawing.Size(717, 482);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reportChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdReportTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CustomGridControl grdReportTable;
        private CustomGridView gridView1;


    }
}
