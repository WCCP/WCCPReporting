﻿using System;

namespace Logica.Reports.BaseReportControl.CommonFunctionality {
    public interface IReportExporter {
        /// <summary>
        /// Run export process.
        /// </summary>
        void ExportRun();

        /// <summary>
        /// Sets type of export file format
        /// </summary>
        /// <param name="exportType">export file format enum</param>
        void SetExportType(ExportToType exportType);

        /// <summary>
        /// Sets export mode.
        /// </summary>
        /// <param name="exportMode">Enum for export mode</param>
        void SetExportMode(ExportModeEnum exportMode);

        /// <summary>
        /// Gets the Report Id value
        /// </summary>
        /// <returns>int</returns>
        int GetReportId();

        /// <summary>
        /// Sets the Report Id value
        /// </summary>
        /// <param name="reportId">The report Id</param>
        void SetReportId(int reportId);

        /// <summary>
        /// Gets export path for output files.
        /// </summary>
        /// <returns></returns>
        string GetExportPath();

        /// <summary>
        /// Sets path to export folder
        /// </summary>
        /// <param name="exportPath">path to folder</param>
        void SetExportPath(string exportPath);

        /// <summary>
        /// Gets the report input date.
        /// </summary>
        /// <param name="reportDate">Report date</param>
        /// <returns></returns>
        DateTime GetReportDate(DateTime reportDate);

        /// <summary>
        /// Sets report date.
        /// </summary>
        /// <param name="date">Date.</param>
        void SetReportDate(DateTime date);

        void SetDsmId(int dsmId);
        void SetSupervisorId(int svId);
    }
}