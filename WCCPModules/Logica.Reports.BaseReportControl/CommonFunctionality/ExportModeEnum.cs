namespace Logica.Reports.BaseReportControl.CommonFunctionality {
    public enum ExportModeEnum {
        M1 = 1,
        M2 = 2,
        M3 = 3,
        M2AndM1 = 4,
        M3AndM2 = 5
    }
}