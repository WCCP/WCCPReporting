﻿namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    public enum UserLevelType
    {
        /// <summary>
        /// User of unknown level
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// User M2
        /// </summary>
        M2 = 2,
        /// <summary>
        /// User M3
        /// </summary>
        M3 = 3,
        /// <summary>
        /// User M4
        /// </summary>
        M4 = 4,
        /// <summary>
        /// User M5
        /// </summary>
        M5 = 5
    }
}