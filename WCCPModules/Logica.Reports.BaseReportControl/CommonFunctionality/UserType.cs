﻿#region

using System.Data.SqlClient;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataAccess;

#endregion

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    public class UserType
    {
        #region Constructors

        protected UserType()
        {
            Level = GetUserLevelType();
        }

        #endregion

        #region Instance Properties

        public bool IsM2
        {
            get { return Level == UserLevelType.M2; }
        }

        public bool IsM3
        {
            get { return Level == UserLevelType.M3; }
        }

        public bool IsM4
        {
            get { return Level == UserLevelType.M4; }
        }

        public bool IsM5
        {
            get { return Level == UserLevelType.M5; }
        }

        public bool IsUnknownLevel
        {
            get { return Level == UserLevelType.Unknown; }
        }

        public UserLevelType Level { get; private set; }

        #endregion

        #region Class Properties

        public static UserType Current
        {
            get { return new UserType(); }
        }

        #endregion

        #region Class Methods

        private static UserLevelType GetUserLevelType()
        {
            try
            {
                return
                    (UserLevelType) ConvertEx.ToInt(DataAccessLayer.ExecuteScalarStoredProcedure("spDW_GetUserLevel"));
            }
            catch (SqlException)
            {
                DataAccessLayer.OpenConnection();
                return
                    (UserLevelType) ConvertEx.ToInt(DataAccessLayer.ExecuteScalarStoredProcedure("spDW_GetUserLevel"));
            }
        }

        #endregion
    }
}