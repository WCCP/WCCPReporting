using DevExpress.XtraTab;

namespace Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs
{
    public class CustomExportEventArgs : System.EventArgs
    {
        public CustomExportEventArgs(bool handled, ExportToType exportType, XtraTabPage exportPage)
        {
            Handled = handled;
            ExportType = exportType;
            ExportPage = exportPage;
        }

        /// <summary>
        /// Gets or sets if export has already been performed by event handler
        /// </summary>
        public bool Handled { get; set; }
        /// <summary>
        /// Gets type of document which should be created
        /// </summary>
        public ExportToType ExportType { get; private set; }
        /// <summary>
        /// Gets report page which should be exported
        /// </summary>
        public XtraTabPage ExportPage { get; private set; }
    }
}