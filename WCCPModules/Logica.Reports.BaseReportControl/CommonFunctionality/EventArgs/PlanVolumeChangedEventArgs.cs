namespace Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs {
    public class PlanVolumeChangedEventArgs : System.EventArgs {
        public PlanVolumeChangedEventArgs(int periodNumber, decimal planVolume) {
            PeriodNumber = periodNumber;
            PlanVolume = planVolume;
        }

        public decimal PlanVolume { get; set; }
        public int PeriodNumber { get; set; }
    }
}