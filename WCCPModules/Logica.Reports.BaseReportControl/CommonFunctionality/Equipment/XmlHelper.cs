﻿using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    /// <summary>
    /// Represents Xml operation helper
    /// </summary>
    public class XmlHelper
    {
        private const string RootNodeName = "root";
        private const string NodeName = "row";
        private const string AttributeName = "field_id";

        /// <summary>
        /// Gets Xml representation of the given list
        /// </summary>
        /// <param name="values">List to be displayed</param>
        /// <returns>Xml representation</returns>
        public static SqlXml GetXmlDescription(List<string> values)
        {
            MemoryStream lStream = new MemoryStream(1024 * 10);//new FileStream("D:\\my.txt", FileMode.OpenOrCreate);// 

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (XmlWriter lWriter = XmlWriter.Create(lStream, settings))
            {
                lWriter.WriteStartElement(RootNodeName);
                foreach (string lvalue in values)
                {
                    lWriter.WriteStartElement(NodeName);
                    lWriter.WriteAttributeString(AttributeName, lvalue);
                    lWriter.WriteEndElement();
                }

                lWriter.WriteEndElement();
                lWriter.Flush();
                lStream.Position = 0;
            }

            return new SqlXml(lStream);
        }
    }
}
