﻿using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    public class CalcCheckedNodesIdsOperation : TreeListOperation
    {
        private List<string> checkedNodes = new List<string>();

        public List<string> CheckedNodes
        {
            get { return checkedNodes; }
        }
        
        public override void Execute(TreeListNode node)
        {
            if (node.Visible && !node.HasChildren && node.CheckState == CheckState.Checked)
            {
                checkedNodes.Add(node[node.TreeList.KeyFieldName].ToString());
            }
        }
    }
}

