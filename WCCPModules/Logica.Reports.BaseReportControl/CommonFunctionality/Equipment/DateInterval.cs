﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    public struct DateInterval
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public bool IsValid
        {
            get
            {
                bool areBothEmpty = From == DateTime.MinValue && To == DateTime.MinValue;
                bool areBothNonEmpty = From != DateTime.MinValue && To != DateTime.MinValue;

                return areBothEmpty || areBothNonEmpty;
            }
        }
    }
}
