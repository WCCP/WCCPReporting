﻿using System.Collections.Generic;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    class CalcHideLevel : TreeListOperation
    {
        private List<string> checkedNodes = new List<string>();

        public List<string> CheckedNodes
        {
            get { return checkedNodes; }
        }

        private string _colLevelName;
        private int? _level;
       
        public CalcHideLevel(string colLevelName, int level)
        {
            _level = level;
            _colLevelName = colLevelName;
        }

        public override void Execute(TreeListNode node)
        {
            if (node[_colLevelName].ToString() == _level.ToString())
            {
                node.Visible = false;
            }
        }
    }
}
