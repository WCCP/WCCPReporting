﻿using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    class CalcCheckedNodesColumnValueAll : TreeListOperation
    {
        private List<string> checkedNodes = new List<string>();

        public List<string> CheckedNodes
        {
            get { return checkedNodes; }
        }

        private string _colName;
        private CheckState _state;

        public CalcCheckedNodesColumnValueAll(string colName, CheckState state)
        {
            _colName = colName;
            _state = state;
        }

        public override void Execute(TreeListNode node)
        {
            if (node.Visible && node.CheckState == _state)
            {
                checkedNodes.Add(node[_colName].ToString());
            }
        }
    }
}
