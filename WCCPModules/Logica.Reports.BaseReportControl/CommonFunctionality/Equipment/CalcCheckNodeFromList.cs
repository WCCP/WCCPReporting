﻿using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    class CalcCheckNodeFromList : TreeListOperation
    {
        private CheckState _state;
        private List<string> _values; 
        
        public CalcCheckNodeFromList(List<string> values, CheckState state)
        {
            _state = state;
            _values = values;
        }

        public override void Execute(TreeListNode node)
        {
            if (node.Visible && _values.Contains(node[node.TreeList.KeyFieldName].ToString()))
            {
                node.CheckState = _state;
            }
            else
            {
                node.CheckState = CheckState.Unchecked;
            }
        }
    }
}
