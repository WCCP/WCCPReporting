﻿using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    class CalcCheckedInvisibleNodes : TreeListOperation
    {
        private List<string> checkedNodes = new List<string>();

        public List<string> CheckedNodes
        {
            get { return checkedNodes; }
        }

        private string _colName;
        private string _colLevelName;
        private int _level;
        private CheckState _state;

        public CalcCheckedInvisibleNodes(string colName, int level, string colLevelName, CheckState state)
        {
            _colName = colName;
            _level = level;
            _colLevelName = colLevelName;
            _state = state;
        }

        public override void Execute(TreeListNode node)
        {
            if (node.ParentNode != null && node.ParentNode.Visible && node.ParentNode.CheckState == _state)
            {
                if (node[_colLevelName].ToString() == _level.ToString())
                {
                    checkedNodes.Add(node[_colName].ToString());
                }
            }
        }
    }
}
