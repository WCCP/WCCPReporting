﻿using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    class CalcCheckNode : TreeListOperation
    {
        private CheckState _state;

        public CalcCheckNode(CheckState state)
        {
            _state = state;
        }

        public override void Execute(TreeListNode node)
        {
            if (node.Visible)
            {
                node.CheckState = _state;
            }
        }
    }
}
