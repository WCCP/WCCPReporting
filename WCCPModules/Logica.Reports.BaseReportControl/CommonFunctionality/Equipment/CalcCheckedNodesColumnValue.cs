﻿using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    class CalcCheckedNodesColumnValue : TreeListOperation
    {
        private List<string> checkedNodes = new List<string>();

        public List<string> CheckedNodes
        {
            get { return checkedNodes; }
        }

        private string _colName;
        private string _colLevelName;
        private int? _level;

        public CalcCheckedNodesColumnValue(string colName, int? level, string colLevelName)
        {
            _colName = colName;
            _level = level;
            _colLevelName = colLevelName;
        }

        public override void Execute(TreeListNode node)
        {
            if (node.Visible && !node.HasChildren && node.CheckState == CheckState.Checked)
            {
                if (_level.HasValue && node[_colLevelName].ToString() == _level.ToString())
                {
                    checkedNodes.Add(node[_colName].ToString());
                }
            }
        }
    }
}
