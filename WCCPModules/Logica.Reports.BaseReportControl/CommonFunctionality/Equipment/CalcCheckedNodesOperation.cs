﻿using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Logica.Reports.BaseReportControl.CommonFunctionality
{
    public class CalcCheckedNodesOperation : TreeListOperation
    {
        private List<TreeListNode> checkedNodes = new List<TreeListNode>();

        public List<TreeListNode> CheckedNodes
        {
            get { return checkedNodes; }
        }
        
        public override void Execute(TreeListNode node)
        {
            if (node.Visible && !node.HasChildren && node.CheckState == CheckState.Checked)
            {
                checkedNodes.Add(node);
            }
        }
    }
}
