﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraPrinting;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using DevExpress.XtraPrintingLinks;
using System.IO;
using Logica.Reports.Common;

namespace Logica.Reports.BaseReportControl
{
    /// <summary>
    /// Base table
    /// </summary>
    public partial class BaseTable : UserControl
    {
        const int TABLES_PER_PAGE = 2;

        #region Internal properties
        private Table table;
        /// <summary>
        /// Structure of control, passed from DB
        /// </summary>
        internal Table Table
        {
            get { return table; }
        }
        /// <summary>
        /// Define if current table is readonly
        /// </summary>
        internal bool IsReadOnlyTable
        {
            get
            {
                foreach (Query q in table.Queries)
                    if (q.SqlQueryType == SQLQueryType.NonQuery && null != q.Parameters && !String.IsNullOrEmpty(q.Name))
                        return false;
                return true;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor, initialize components
        /// </summary>
        public BaseTable()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Constructor, define view of table, load chart xml
        /// </summary>
        /// <param name="tbl">Structure of table</param>
        protected BaseTable(Table tbl) : this()
        {
            if (tbl.Display == DisplayType.ExportDocOnly)
                this.Visible = false;
            this.Dock = DockStyle.Top;
            table = tbl;
            if (tbl.HideHeader || String.IsNullOrEmpty(tbl.Header))
                tablesContainer.Visible = false;
            else
                lbHeader.Text = /*tbl.HideHeader ? String.Empty :*/ tbl.Header;
            if (!String.IsNullOrEmpty(Table.Body.ChartXml))
            {

                using (MemoryStream ms = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(ms))
                    {
                        sw.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                        sw.Write(Table.Body.ChartXml.Replace("><", "> <"));//Impossible LoadFromStream without this trick
                        sw.Flush();
                        ms.Position = 0;
                        reportChart.LoadFromStream(ms);
                    }
                }

            }
        }
        /// <summary>
        /// Raise load event, Occurs changing size method
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!DesignMode)
            {
                Parent.SizeChanged += new EventHandler(Parent_SizeChanged);
                //this.SizeChanged += new EventHandler(BaseTable_SizeChanged);
                SetSize();
            }
        }
        /// <summary>
        /// Occurs changing size method
        /// </summary>
        void Parent_SizeChanged(object sender, EventArgs e)
        {
            SetSize();
        }
        /// <summary>
        /// Set size of internal control
        /// </summary>
        public virtual void SetSize()
        {
            if (null != Parent && null != Parent.Controls)
            {
                int count = 0;
                int height = 0;
                foreach (Control c in Parent.Controls)
                {
                    if (c is BaseTable)
                        count++;
                    else
                        height += c.Height;
                }
                this.Height = (Parent.Height < 400 ? 400 : Parent.Height - height) / (count == 0 ? 1 : (count > TABLES_PER_PAGE ? TABLES_PER_PAGE : count));
            }
                
            if (Table.Mode == ShowMode.GridChart)
            {
              if (splitContainer.Horizontal) //Horisontal says about panels position. Not about splitter.
              {
                int width = splitContainer.Size.Width - 2; //TODO:define spliter size (splitContainer.SplitterWidth)
                if (width > 0)
                  splitContainer.SplitterPosition = width/2;
              }
              else
              {
                int height = splitContainer.Size.Height - 2;
                if (height > 0)
                  splitContainer.SplitterPosition = height/2;
              }
              if (splitContainer.SplitterPosition < 300)
                    splitContainer.SplitterPosition = 300;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Set type of internal data and its structure
        /// </summary>
        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            if (null == Table)
                return;
            if (Table.Mode == ShowMode.Grid)
                splitContainer.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel1;
            else if (Table.Mode == ShowMode.Chart)
                splitContainer.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel2;
            else if (Table.Mode == ShowMode.GridChart)
            {
                if (Table.Position == ChartPosition.Left || Table.Position == ChartPosition.Top)
                {
                    Control c1 = splitContainer.Panel1.Controls[0];
                    Control c2 = splitContainer.Panel2.Controls[0];
                    c1.Parent = splitContainer.Panel2;
                    c2.Parent = splitContainer.Panel1;
                }
                splitContainer.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Both;
                splitContainer.Horizontal = !(Table.Position == ChartPosition.Left || Table.Position == ChartPosition.Right);
            }
        }
        #endregion
        /// <summary>
        /// Get data from DB
        /// </summary>
        /// <param name="table">Structure of table with queries abd params collectiom</param>
        /// <param name="settings">Values of parameters</param>
        /// <returns></returns>
        internal static DataSet GetDataSource(Table table, SheetParamCollection settings)
        {
            Query query = null;
            if (table.Queries == null)
                return null;
            foreach (Query q in table.Queries)
            {
                if (q.SqlQueryType == SQLQueryType.DataSet)
                {
                    query = q;
                    break;
                }
            }
            if (null == query)
                throw new ArgumentException("Select query is absent");

            List<SqlParameter> list = new List<SqlParameter>();
            foreach (Parameter param in query.Parameters)
            {
                SqlParameter sqlParam = param.ConvertToSqlParameter();
                if (settings == null || (!settings.Exists(s => s.SqlParamName.ToUpper()  == param.Name.ToUpper() ) && String.IsNullOrEmpty(param.DefaultValue)))
                    throw new ArgumentNullException(param.Name, @"Can not find param for SQL query");
                SheetParam sheetParam = settings.Find(s => s.SqlParamName.ToUpper() == param.Name.ToUpper());
                if (null != sheetParam)
                    sqlParam.Value = sheetParam.Value;
                list.Add(sqlParam);
            }
            return DataAccessLayer.ExecuteStoredProcedure(query.Name, list.ToArray());
        }
        /// <summary>
        /// Prepare controls for printing or exporting
        /// </summary>
        /// <param name="links">Collection of controls for printing or exporting</param>
        internal virtual void AppendLinks(LinkCollection links)
        {
            links.Add(new RichTextBoxLink { RichTextBox = new RichTextBox { Text = @"     " } }); //don't delete spaces! because empty string don't separate tables in printing.
            links.Add(new RichTextBoxLink { RichTextBox = new RichTextBox { Text = lbHeader.Text, Font = lbHeader.Font } });
        }
        
    }
}
