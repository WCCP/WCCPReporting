﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;

namespace Logica.Reports.BaseReportControl
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPrint
    {
        CompositeLink PrepareCompositeLink();
    }
}
