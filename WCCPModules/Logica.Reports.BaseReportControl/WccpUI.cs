﻿using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.Common;

namespace WccpReporting
{
    /// <summary>
    /// Wccp required class 
    /// </summary>
	public class WccpUI : IStartupClass
	{
		#region IStartupClass Members
        /// <summary>
        /// Version of application
        /// </summary>
        /// <returns>Version</returns>
		public string GetVersion()
		{
			return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}
		public int ShowUI(int isSkined, string reportCaption)
		{
			return 0;
		}

		public void CloseUI()
		{
			//throw new NotImplementedException();
		}

		#endregion
	}
}
