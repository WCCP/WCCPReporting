﻿using System;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace Logica.Reports.BaseReportControl
{
    public class CustomGridHandler : DevExpress.XtraGrid.Views.BandedGrid.Handler.BandedGridHandler
    {
        public CustomGridHandler(Logica.Reports.ConfigXmlParser.GridExtension.GroupBandedView gridView) : base(gridView) { }


    }

}
