﻿using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.ConfigXmlParser.Model;

namespace Logica.Reports.BaseReportControl
{
    /// <summary>
    /// Expanded collection of SheetParam
    /// </summary>
    /// 
    

    public class SheetParamCollection : List<SheetParam>
    {
        private TableType tableDataType = TableType.Undefined;
        /// <summary>
        /// Type of table - plan or fact
        /// </summary>
        public TableType TableDataType
        {
            get { return tableDataType; }
            set { tableDataType = value; }
        }
        private Guid tabId;
        /// <summary>
        /// Unique tab identifier
        /// </summary>
        public Guid TabId
        {
            get { return tabId; }
            set { tabId = value; }
        }
        string mainHeader;
        /// <summary>
        /// Header for user defined settings section
        /// </summary>
        public string MainHeader
        {
            get { return mainHeader; }
            set { mainHeader = value; }
        }
    }
    /// <summary>
    /// Settings container
    /// </summary>
    public class SheetParam
    {
        string sqlParamName = String.Empty;
        /// <summary>
        /// Name of sql parameter
        /// </summary>
        public string SqlParamName
        {
            get { return sqlParamName; }
            set { sqlParamName = value; }
        }

        string displayParamName = String.Empty;
        /// <summary>
        /// Name of parameter for displaying
        /// </summary>
        public string DisplayParamName
        {
            get { return displayParamName; }
            set { displayParamName = value; }
        }
        object displayValue;
        /// <summary>
        /// Display value
        /// </summary>
        public object DisplayValue
        {
            get { return displayValue == null ? value : displayValue; }
            set { displayValue = value; }
        }

        object value;
        /// <summary>
        /// Sql parameter value
        /// </summary>
        public object Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }
}
