﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.BaseReportControl.Helpers
{
    public static class EnumerableHelpers
	{
		public static string ToString<TSource>(this IEnumerable<TSource> source, string delimeter)
		{
			if (source.Count() == 0) return string.Empty;

			StringBuilder sb = new StringBuilder();
			foreach (TSource src in source)
			{
				sb.AppendFormat("{0}{1}", src, delimeter);
			}
			return sb.ToString(0, sb.Length - delimeter.Length);
		}
	}
}