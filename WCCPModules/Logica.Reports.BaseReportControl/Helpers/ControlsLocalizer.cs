﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors.Controls;

namespace Logica.Reports.BaseReportControl.Helpers
{
    public class ControlsLocalizer : Localizer
    {
        public override string GetLocalizedString(StringId id)
        {
            return DxLocalizedResources.ResourceManager.GetString(id.ToString()) ?? base.GetLocalizedString(id);
        }
    }
}
