﻿using System;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Registrator;


namespace Logica.Reports.BaseReportControl
{
    public class CustomGridControl : GridControl
    {
        protected override BaseView CreateDefaultView()
        {
            return CreateView("CustomGridView");
        }
        protected override void RegisterAvailableViewsCore(InfoCollection collection)
        {
            base.RegisterAvailableViewsCore(collection);
            collection.Add(new CustomGridViewInfoRegistrator());
        }
        
    }

}
