﻿using System;
using System.Drawing;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;

namespace Logica.Reports.BaseReportControl
{
    public class CustomGridViewInfo : BandedGridViewInfo
    {
        public CustomGridViewInfo(Logica.Reports.ConfigXmlParser.GridExtension.GroupBandedView gridView) : base(gridView) { }


    }

}
