﻿namespace Logica.Reports.BaseReportControl
{
    partial class BaseTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel4 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.Series series4 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel5 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel6 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.lbHeader = new DevExpress.XtraEditors.LabelControl();
            this.tablesContainer = new DevExpress.XtraEditors.PanelControl();
            this.splitContainer = new DevExpress.XtraEditors.SplitContainerControl();
            this.reportChart = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.tablesContainer)).BeginInit();
            this.tablesContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel6)).BeginInit();
            this.SuspendLayout();
            // 
            // lbHeader
            // 
            this.lbHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbHeader.Appearance.Options.UseFont = true;
            this.lbHeader.Location = new System.Drawing.Point(3, 3);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(110, 19);
            this.lbHeader.TabIndex = 5;
            this.lbHeader.Text = string.Empty;
            // 
            // tablesContainer
            // 
            this.tablesContainer.Controls.Add(this.lbHeader);
            this.tablesContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.tablesContainer.Location = new System.Drawing.Point(0, 0);
            this.tablesContainer.Name = "tablesContainer";
            this.tablesContainer.Size = new System.Drawing.Size(513, 27);
            this.tablesContainer.TabIndex = 6;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainer.Location = new System.Drawing.Point(0, 27);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Panel2.Controls.Add(this.reportChart);
            this.splitContainer.Size = new System.Drawing.Size(513, 403);
            this.splitContainer.SplitterPosition = 160;
            this.splitContainer.TabIndex = 7;
            // 
            // reportChart
            // 
            xyDiagram2.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram2.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram2.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            this.reportChart.Diagram = xyDiagram2;
            this.reportChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportChart.Location = new System.Drawing.Point(0, 0);
            this.reportChart.Name = "reportChart";
            sideBySideBarSeriesLabel4.LineVisible = true;
            series3.Label = sideBySideBarSeriesLabel4;
            series3.Name = "Series 1";
            sideBySideBarSeriesLabel5.LineVisible = true;
            series4.Label = sideBySideBarSeriesLabel5;
            series4.Name = "Series 2";
            this.reportChart.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series3,
        series4};
            sideBySideBarSeriesLabel6.LineVisible = true;
            this.reportChart.SeriesTemplate.Label = sideBySideBarSeriesLabel6;
            this.reportChart.Size = new System.Drawing.Size(347, 403);
            this.reportChart.TabIndex = 0;
            // 
            // BaseTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.tablesContainer);
            this.Name = "BaseTable";
            this.Size = new System.Drawing.Size(513, 430);
            ((System.ComponentModel.ISupportInitialize)(this.tablesContainer)).EndInit();
            this.tablesContainer.ResumeLayout(false);
            this.tablesContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl tablesContainer;
        protected DevExpress.XtraEditors.LabelControl lbHeader;
        public DevExpress.XtraEditors.SplitContainerControl splitContainer;
        protected DevExpress.XtraCharts.ChartControl reportChart;
    }
}
