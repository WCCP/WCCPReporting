namespace Logica.Reports.BaseReportControl
{
    /// <summary>
    /// Define export document format
    /// </summary>
    public enum ExportToType
    {
        Xls = 1,
        Pdf = 2,
        Rtf = 4,
        Txt = 8,
        Html = 16,
        Mht = 32,
        Csv = 64,
        Bmp = 128,
        Xlsx = 256
    }
}