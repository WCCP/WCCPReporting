﻿namespace SoftServe.Reports.BudgetManagement.UserControls
{
    partial class PlanFactControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanFactControl));
            this.barButtonBudgetInWork = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonImportMACO = new DevExpress.XtraBars.BarButtonItem();
            this.treeList = new DevExpress.XtraTreeList.TreeList();
            this.colTeritory = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStrCaption_c = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPlanValue_c = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLEValue_c = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPlanLEDiff_c = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDev1_c = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colFactValue_c = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDev2_c = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStrCaption_v = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPlanValue_v = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLEValue_v = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPlanLEDiff_v = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDev1_v = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colFactValue_v = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDev2_v = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStrCaption_m = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPlanValue_m = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLEValue_m = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPlanLEDiff_m = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDev1_m = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colFactValue_m = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDev2_m = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
            this.SuspendLayout();
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "save.png");
            // 
            // barButtonBudgetInWork
            // 
            this.barButtonBudgetInWork.Caption = "Бюджет в работу";
            this.barButtonBudgetInWork.Id = 8;
            this.barButtonBudgetInWork.Name = "barButtonBudgetInWork";
            // 
            // barButtonImportMACO
            // 
            this.barButtonImportMACO.Caption = "Импорт МАСО";
            this.barButtonImportMACO.Id = 9;
            this.barButtonImportMACO.Name = "barButtonImportMACO";
            // 
            // treeList
            // 
            this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colTeritory,
            this.colStrCaption_c,
            this.colPlanValue_c,
            this.colLEValue_c,
            this.colPlanLEDiff_c,
            this.colDev1_c,
            this.colFactValue_c,
            this.colDev2_c,
            this.colStrCaption_v,
            this.colPlanValue_v,
            this.colLEValue_v,
            this.colPlanLEDiff_v,
            this.colDev1_v,
            this.colFactValue_v,
            this.colDev2_v,
            this.colStrCaption_m,
            this.colPlanValue_m,
            this.colLEValue_m,
            this.colPlanLEDiff_m,
            this.colDev1_m,
            this.colFactValue_m,
            this.colDev2_m});
            this.treeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList.Location = new System.Drawing.Point(0, 34);
            this.treeList.Name = "treeList";
            this.treeList.OptionsBehavior.Editable = false;
            this.treeList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeList.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.treeList.OptionsView.ShowHorzLines = false;
            this.treeList.OptionsView.ShowIndicator = false;
            this.treeList.OptionsView.ShowVertLines = false;
            this.treeList.Padding = new System.Windows.Forms.Padding(5);
            this.treeList.ParentFieldName = "PARENT_ID";
            this.treeList.PreviewLineCount = 3;
            this.treeList.RowHeight = 69;
            this.treeList.Size = new System.Drawing.Size(1311, 424);
            this.treeList.TabIndex = 4;
            this.treeList.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.treeList_CustomDrawNodeCell);
            // 
            // colTeritory
            // 
            this.colTeritory.Caption = "Территория и персонал";
            this.colTeritory.FieldName = "DATA";
            this.colTeritory.MinWidth = 160;
            this.colTeritory.Name = "colTeritory";
            this.colTeritory.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.colTeritory.Visible = true;
            this.colTeritory.VisibleIndex = 0;
            this.colTeritory.Width = 189;
            // 
            // colStrCaption_c
            // 
            this.colStrCaption_c.Caption = "Показатель";
            this.colStrCaption_c.FieldName = "StrCaption_c";
            this.colStrCaption_c.MinWidth = 90;
            this.colStrCaption_c.Name = "colStrCaption_c";
            this.colStrCaption_c.Visible = true;
            this.colStrCaption_c.VisibleIndex = 1;
            this.colStrCaption_c.Width = 115;
            // 
            // colPlanValue_c
            // 
            this.colPlanValue_c.Caption = "План";
            this.colPlanValue_c.FieldName = "PlanValue_c";
            this.colPlanValue_c.MinWidth = 80;
            this.colPlanValue_c.Name = "colPlanValue_c";
            this.colPlanValue_c.Visible = true;
            this.colPlanValue_c.VisibleIndex = 2;
            this.colPlanValue_c.Width = 118;
            // 
            // colLEValue_c
            // 
            this.colLEValue_c.Caption = "LE";
            this.colLEValue_c.FieldName = "LEValue_c";
            this.colLEValue_c.MinWidth = 80;
            this.colLEValue_c.Name = "colLEValue_c";
            this.colLEValue_c.Visible = true;
            this.colLEValue_c.VisibleIndex = 3;
            this.colLEValue_c.Width = 125;
            // 
            // colPlanLEDiff_c
            // 
            this.colPlanLEDiff_c.Caption = "LE - план";
            this.colPlanLEDiff_c.FieldName = "PlanLEDiff_c";
            this.colPlanLEDiff_c.MinWidth = 80;
            this.colPlanLEDiff_c.Name = "colPlanLEDiff_c";
            this.colPlanLEDiff_c.Visible = true;
            this.colPlanLEDiff_c.VisibleIndex = 4;
            this.colPlanLEDiff_c.Width = 112;
            // 
            // colDev1_c
            // 
            this.colDev1_c.Caption = "(LE - план)/план %";
            this.colDev1_c.FieldName = "Dev1_c";
            this.colDev1_c.MinWidth = 150;
            this.colDev1_c.Name = "colDev1_c";
            this.colDev1_c.Visible = true;
            this.colDev1_c.VisibleIndex = 5;
            this.colDev1_c.Width = 363;
            // 
            // colFactValue_c
            // 
            this.colFactValue_c.Caption = "Факт";
            this.colFactValue_c.FieldName = "FactValue_c";
            this.colFactValue_c.MinWidth = 60;
            this.colFactValue_c.Name = "colFactValue_c";
            this.colFactValue_c.Visible = true;
            this.colFactValue_c.VisibleIndex = 6;
            this.colFactValue_c.Width = 65;
            // 
            // colDev2_c
            // 
            this.colDev2_c.Caption = "Затраты грн, V гл, МАСО грн (Факт/LE)";
            this.colDev2_c.FieldName = "Dev2_c";
            this.colDev2_c.MinWidth = 215;
            this.colDev2_c.Name = "colDev2_c";
            this.colDev2_c.Visible = true;
            this.colDev2_c.VisibleIndex = 7;
            this.colDev2_c.Width = 220;
            // 
            // colStrCaption_v
            // 
            this.colStrCaption_v.Caption = "treeListColumn2";
            this.colStrCaption_v.FieldName = "StrCaption_v";
            this.colStrCaption_v.Name = "colStrCaption_v";
            // 
            // colPlanValue_v
            // 
            this.colPlanValue_v.Caption = "treeListColumn3";
            this.colPlanValue_v.FieldName = "PlanValue_v";
            this.colPlanValue_v.Name = "colPlanValue_v";
            // 
            // colLEValue_v
            // 
            this.colLEValue_v.Caption = "treeListColumn4";
            this.colLEValue_v.FieldName = "LEValue_v";
            this.colLEValue_v.Name = "colLEValue_v";
            // 
            // colPlanLEDiff_v
            // 
            this.colPlanLEDiff_v.Caption = "treeListColumn5";
            this.colPlanLEDiff_v.FieldName = "PlanLEDiff_v";
            this.colPlanLEDiff_v.Name = "colPlanLEDiff_v";
            // 
            // colDev1_v
            // 
            this.colDev1_v.Caption = "treeListColumn6";
            this.colDev1_v.FieldName = "Dev1_v";
            this.colDev1_v.Name = "colDev1_v";
            // 
            // colFactValue_v
            // 
            this.colFactValue_v.Caption = "treeListColumn7";
            this.colFactValue_v.FieldName = "FactValue_v";
            this.colFactValue_v.Name = "colFactValue_v";
            // 
            // colDev2_v
            // 
            this.colDev2_v.Caption = "treeListColumn8";
            this.colDev2_v.FieldName = "Dev2_v";
            this.colDev2_v.Name = "colDev2_v";
            // 
            // colStrCaption_m
            // 
            this.colStrCaption_m.Caption = "treeListColumn2";
            this.colStrCaption_m.FieldName = "StrCaption_m";
            this.colStrCaption_m.Name = "colStrCaption_m";
            // 
            // colPlanValue_m
            // 
            this.colPlanValue_m.Caption = "treeListColumn3";
            this.colPlanValue_m.FieldName = "PlanValue_m";
            this.colPlanValue_m.Name = "colPlanValue_m";
            // 
            // colLEValue_m
            // 
            this.colLEValue_m.Caption = "treeListColumn4";
            this.colLEValue_m.FieldName = "LEValue_m";
            this.colLEValue_m.Name = "colLEValue_m";
            // 
            // colPlanLEDiff_m
            // 
            this.colPlanLEDiff_m.Caption = "treeListColumn5";
            this.colPlanLEDiff_m.FieldName = "PlanLEDiff_m";
            this.colPlanLEDiff_m.Name = "colPlanLEDiff_m";
            // 
            // colDev1_m
            // 
            this.colDev1_m.Caption = "treeListColumn6";
            this.colDev1_m.FieldName = "Dev1_m";
            this.colDev1_m.Name = "colDev1_m";
            // 
            // colFactValue_m
            // 
            this.colFactValue_m.Caption = "treeListColumn7";
            this.colFactValue_m.FieldName = "FactValue_m";
            this.colFactValue_m.Name = "colFactValue_m";
            // 
            // colDev2_m
            // 
            this.colDev2_m.Caption = "treeListColumn8";
            this.colDev2_m.FieldName = "Dev2_m";
            this.colDev2_m.Name = "colDev2_m";
            // 
            // PlanFactControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.treeList);
            this.Name = "PlanFactControl";
            this.Size = new System.Drawing.Size(1311, 458);
            this.Controls.SetChildIndex(this.treeList, 0);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarButtonItem barButtonBudgetInWork;
        private DevExpress.XtraBars.BarButtonItem barButtonImportMACO;
        private DevExpress.XtraTreeList.TreeList treeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTeritory;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStrCaption_c;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPlanValue_c;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colLEValue_c;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPlanLEDiff_c;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDev1_c;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colFactValue_c;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDev2_c;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStrCaption_v;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPlanValue_v;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colLEValue_v;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPlanLEDiff_v;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDev1_v;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colFactValue_v;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDev2_v;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStrCaption_m;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPlanValue_m;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colLEValue_m;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPlanLEDiff_m;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDev1_m;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colFactValue_m;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDev2_m;



    }
}
