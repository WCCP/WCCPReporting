﻿namespace SoftServe.Reports.BudgetManagement.UserControls
{
  partial class BudgetCreationControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BudgetCreationControl));
        DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
        this.barButtonBudgetInWork = new DevExpress.XtraBars.BarButtonItem();
        this.gridControlI = new DevExpress.XtraGrid.GridControl();
        this.gridViewI = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.colIRegion = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIInitiative = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colICreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colITerm = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIPrevPlannedSalesV = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colICurrentPlannedSalesV = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIPrevYear = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colICurrentYear = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIStatus = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIApproveInBudget = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIInWork = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIInvestmentsPrev = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIInvestmentsCur = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIInvestmentsYTD = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colILE = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIApproveInBudgetOrig = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIID = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIChannelTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colIForLE = new DevExpress.XtraGrid.Columns.GridColumn();
        this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
        this.gridControlS = new DevExpress.XtraGrid.GridControl();
        this.gridViewS = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.colSTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSPrevYear = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSCurrentYear = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSApprovedSummary = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSInvestmentsYTD = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSLEBudget = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
        ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridControlI)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridViewI)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
        this.splitContainerControl.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.gridControlS)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridViewS)).BeginInit();
        this.SuspendLayout();
        // 
        // imageCollection
        // 
        this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
        this.imageCollection.Images.SetKeyName(0, "save.png");
        // 
        // barButtonBudgetInWork
        // 
        this.barButtonBudgetInWork.Caption = "Бюджет в работу";
        this.barButtonBudgetInWork.Id = 8;
        this.barButtonBudgetInWork.Name = "barButtonBudgetInWork";
        this.barButtonBudgetInWork.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonBudgetInWork_ItemClick);
        // 
        // gridControlI
        // 
        this.gridControlI.Dock = System.Windows.Forms.DockStyle.Fill;
        this.gridControlI.Location = new System.Drawing.Point(0, 0);
        this.gridControlI.MainView = this.gridViewI;
        this.gridControlI.MenuManager = this.barManager;
        this.gridControlI.Name = "gridControlI";
        this.gridControlI.Size = new System.Drawing.Size(1039, 190);
        this.gridControlI.TabIndex = 6;
        this.gridControlI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewI});
        this.gridControlI.SizeChanged += new System.EventHandler(this.gridControl_Resized);
        this.gridControlI.Resize += new System.EventHandler(this.gridControl_Resized);
        // 
        // gridViewI
        // 
        this.gridViewI.ColumnPanelRowHeight = 34;
        this.gridViewI.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIRegion,
            this.colIInitiative,
            this.colICreationDate,
            this.colITerm,
            this.colIPrevPlannedSalesV,
            this.colICurrentPlannedSalesV,
            this.colIPrevYear,
            this.colICurrentYear,
            this.colIStatus,
            this.colIApproveInBudget,
            this.colIInWork,
            this.colIInvestmentsPrev,
            this.colIInvestmentsCur,
            this.colIInvestmentsYTD,
            this.colILE,
            this.colIApproveInBudgetOrig,
            this.colIID,
            this.colIRegionID,
            this.colIChannelTypeId,
            this.colIForLE});
        styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.Orange;
        styleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.Orange;
        styleFormatCondition2.Appearance.Options.UseBackColor = true;
        styleFormatCondition2.ApplyToRow = true;
        styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
        styleFormatCondition2.Expression = "[APPROVE_ORIG] != [APPROVE]";
        this.gridViewI.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
        this.gridViewI.GridControl = this.gridControlI;
        this.gridViewI.Name = "gridViewI";
        this.gridViewI.OptionsBehavior.Editable = false;
        this.gridViewI.OptionsCustomization.AllowGroup = false;
        this.gridViewI.OptionsFilter.MaxCheckedListItemCount = 1000;
        this.gridViewI.OptionsMenu.EnableColumnMenu = false;
        this.gridViewI.OptionsMenu.EnableFooterMenu = false;
        this.gridViewI.OptionsSelection.EnableAppearanceFocusedCell = false;
        this.gridViewI.OptionsSelection.EnableAppearanceFocusedRow = false;
        this.gridViewI.OptionsSelection.EnableAppearanceHideSelection = false;
        this.gridViewI.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
        this.gridViewI.OptionsView.ShowGroupPanel = false;
        this.gridViewI.OptionsView.ShowIndicator = false;
        this.gridViewI.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIRegion, DevExpress.Data.ColumnSortOrder.Ascending)});
        this.gridViewI.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewI_CustomDrawCell);
        this.gridViewI.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridViewI_RowCellClick);
        // 
        // colIRegion
        // 
        this.colIRegion.AppearanceHeader.Options.UseTextOptions = true;
        this.colIRegion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIRegion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIRegion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIRegion.Caption = "Регион";
        this.colIRegion.FieldName = "REGION_NAME";
        this.colIRegion.MinWidth = 70;
        this.colIRegion.Name = "colIRegion";
        this.colIRegion.OptionsColumn.AllowEdit = false;
        this.colIRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colIRegion.Visible = true;
        this.colIRegion.VisibleIndex = 0;
        this.colIRegion.Width = 112;
        // 
        // colIInitiative
        // 
        this.colIInitiative.AppearanceHeader.Options.UseTextOptions = true;
        this.colIInitiative.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIInitiative.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIInitiative.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIInitiative.Caption = "Инициатива";
        this.colIInitiative.FieldName = "INITIATIVE_NAME";
        this.colIInitiative.MinWidth = 70;
        this.colIInitiative.Name = "colIInitiative";
        this.colIInitiative.OptionsColumn.AllowEdit = false;
        this.colIInitiative.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colIInitiative.Visible = true;
        this.colIInitiative.VisibleIndex = 1;
        this.colIInitiative.Width = 243;
        // 
        // colICreationDate
        // 
        this.colICreationDate.AppearanceHeader.Options.UseTextOptions = true;
        this.colICreationDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colICreationDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colICreationDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colICreationDate.Caption = "Дата создания";
        this.colICreationDate.FieldName = "CREATION_DATE";
        this.colICreationDate.MinWidth = 70;
        this.colICreationDate.Name = "colICreationDate";
        this.colICreationDate.OptionsColumn.AllowEdit = false;
        this.colICreationDate.Visible = true;
        this.colICreationDate.VisibleIndex = 2;
        this.colICreationDate.Width = 85;
        // 
        // colITerm
        // 
        this.colITerm.AppearanceHeader.Options.UseTextOptions = true;
        this.colITerm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colITerm.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colITerm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colITerm.Caption = "Срок, мес";
        this.colITerm.FieldName = "TERM_MONTH";
        this.colITerm.MinWidth = 70;
        this.colITerm.Name = "colITerm";
        this.colITerm.OptionsColumn.AllowEdit = false;
        this.colITerm.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colITerm.Visible = true;
        this.colITerm.VisibleIndex = 3;
        this.colITerm.Width = 70;
        // 
        // colIPrevPlannedSalesV
        // 
        this.colIPrevPlannedSalesV.AppearanceHeader.Options.UseTextOptions = true;
        this.colIPrevPlannedSalesV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIPrevPlannedSalesV.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIPrevPlannedSalesV.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIPrevPlannedSalesV.Caption = "Плановый V по инициативам 2009 года, гл";
        this.colIPrevPlannedSalesV.DisplayFormat.FormatString = "N2";
        this.colIPrevPlannedSalesV.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colIPrevPlannedSalesV.FieldName = "PLAN_VOL_PREV";
        this.colIPrevPlannedSalesV.GroupFormat.FormatString = "N2";
        this.colIPrevPlannedSalesV.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colIPrevPlannedSalesV.MinWidth = 70;
        this.colIPrevPlannedSalesV.Name = "colIPrevPlannedSalesV";
        this.colIPrevPlannedSalesV.OptionsColumn.AllowEdit = false;
        this.colIPrevPlannedSalesV.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colIPrevPlannedSalesV.Visible = true;
        this.colIPrevPlannedSalesV.VisibleIndex = 4;
        this.colIPrevPlannedSalesV.Width = 148;
        // 
        // colICurrentPlannedSalesV
        // 
        this.colICurrentPlannedSalesV.AppearanceHeader.Options.UseTextOptions = true;
        this.colICurrentPlannedSalesV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colICurrentPlannedSalesV.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colICurrentPlannedSalesV.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colICurrentPlannedSalesV.Caption = "Плановый V по инициативам 2010 года, гл";
        this.colICurrentPlannedSalesV.DisplayFormat.FormatString = "N2";
        this.colICurrentPlannedSalesV.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colICurrentPlannedSalesV.FieldName = "PLAN_VOL_CUR";
        this.colICurrentPlannedSalesV.GroupFormat.FormatString = "N2";
        this.colICurrentPlannedSalesV.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colICurrentPlannedSalesV.MinWidth = 70;
        this.colICurrentPlannedSalesV.Name = "colICurrentPlannedSalesV";
        this.colICurrentPlannedSalesV.OptionsColumn.AllowEdit = false;
        this.colICurrentPlannedSalesV.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colICurrentPlannedSalesV.Visible = true;
        this.colICurrentPlannedSalesV.VisibleIndex = 5;
        this.colICurrentPlannedSalesV.Width = 148;
        // 
        // colIPrevYear
        // 
        this.colIPrevYear.AppearanceHeader.Options.UseTextOptions = true;
        this.colIPrevYear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIPrevYear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIPrevYear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIPrevYear.Caption = "Сумма по инициативам 2009 года, грн";
        this.colIPrevYear.DisplayFormat.FormatString = "N2";
        this.colIPrevYear.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colIPrevYear.FieldName = "PLAN_SUM_PREV";
        this.colIPrevYear.GroupFormat.FormatString = "N2";
        this.colIPrevYear.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colIPrevYear.MinWidth = 70;
        this.colIPrevYear.Name = "colIPrevYear";
        this.colIPrevYear.OptionsColumn.AllowEdit = false;
        this.colIPrevYear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colIPrevYear.Visible = true;
        this.colIPrevYear.VisibleIndex = 6;
        this.colIPrevYear.Width = 125;
        // 
        // colICurrentYear
        // 
        this.colICurrentYear.AppearanceHeader.Options.UseTextOptions = true;
        this.colICurrentYear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colICurrentYear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colICurrentYear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colICurrentYear.Caption = "Сумма по инициативам 2010 года, грн";
        this.colICurrentYear.DisplayFormat.FormatString = "N2";
        this.colICurrentYear.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colICurrentYear.FieldName = "PLAN_SUM_CUR";
        this.colICurrentYear.GroupFormat.FormatString = "N2";
        this.colICurrentYear.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colICurrentYear.MinWidth = 70;
        this.colICurrentYear.Name = "colICurrentYear";
        this.colICurrentYear.OptionsColumn.AllowEdit = false;
        this.colICurrentYear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colICurrentYear.Visible = true;
        this.colICurrentYear.VisibleIndex = 7;
        this.colICurrentYear.Width = 125;
        // 
        // colIStatus
        // 
        this.colIStatus.AppearanceHeader.Options.UseTextOptions = true;
        this.colIStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIStatus.Caption = "Статус";
        this.colIStatus.FieldName = "INITIATIVE_STATE";
        this.colIStatus.MinWidth = 70;
        this.colIStatus.Name = "colIStatus";
        this.colIStatus.OptionsColumn.AllowEdit = false;
        this.colIStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colIStatus.Visible = true;
        this.colIStatus.VisibleIndex = 8;
        this.colIStatus.Width = 144;
        // 
        // colIApproveInBudget
        // 
        this.colIApproveInBudget.AppearanceHeader.Options.UseTextOptions = true;
        this.colIApproveInBudget.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIApproveInBudget.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIApproveInBudget.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIApproveInBudget.Caption = "Утвердить в бюджет";
        this.colIApproveInBudget.FieldName = "APPROVE";
        this.colIApproveInBudget.MinWidth = 70;
        this.colIApproveInBudget.Name = "colIApproveInBudget";
        this.colIApproveInBudget.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colIApproveInBudget.Visible = true;
        this.colIApproveInBudget.VisibleIndex = 9;
        this.colIApproveInBudget.Width = 70;
        // 
        // colIInWork
        // 
        this.colIInWork.AppearanceHeader.Options.UseTextOptions = true;
        this.colIInWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIInWork.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIInWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIInWork.Caption = "IN_WORK";
        this.colIInWork.FieldName = "IN_WORK";
        this.colIInWork.MinWidth = 70;
        this.colIInWork.Name = "colIInWork";
        this.colIInWork.OptionsColumn.AllowEdit = false;
        this.colIInWork.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        // 
        // colIInvestmentsPrev
        // 
        this.colIInvestmentsPrev.AppearanceHeader.Options.UseTextOptions = true;
        this.colIInvestmentsPrev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIInvestmentsPrev.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIInvestmentsPrev.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIInvestmentsPrev.Caption = "INVESTMENTS_PREV";
        this.colIInvestmentsPrev.FieldName = "INVESTMENTS_PREV";
        this.colIInvestmentsPrev.MinWidth = 70;
        this.colIInvestmentsPrev.Name = "colIInvestmentsPrev";
        this.colIInvestmentsPrev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        // 
        // colIInvestmentsCur
        // 
        this.colIInvestmentsCur.AppearanceHeader.Options.UseTextOptions = true;
        this.colIInvestmentsCur.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIInvestmentsCur.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIInvestmentsCur.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIInvestmentsCur.Caption = "INVESTMENTS_CUR";
        this.colIInvestmentsCur.FieldName = "INVESTMENTS_CUR";
        this.colIInvestmentsCur.MinWidth = 70;
        this.colIInvestmentsCur.Name = "colIInvestmentsCur";
        this.colIInvestmentsCur.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        // 
        // colIInvestmentsYTD
        // 
        this.colIInvestmentsYTD.AppearanceHeader.Options.UseTextOptions = true;
        this.colIInvestmentsYTD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIInvestmentsYTD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIInvestmentsYTD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIInvestmentsYTD.Caption = "INVESTMENTS_YTD";
        this.colIInvestmentsYTD.FieldName = "INVESTMENTS_YTD";
        this.colIInvestmentsYTD.MinWidth = 70;
        this.colIInvestmentsYTD.Name = "colIInvestmentsYTD";
        this.colIInvestmentsYTD.OptionsColumn.AllowEdit = false;
        this.colIInvestmentsYTD.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        // 
        // colILE
        // 
        this.colILE.AppearanceHeader.Options.UseTextOptions = true;
        this.colILE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colILE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colILE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colILE.Caption = "LE";
        this.colILE.FieldName = "LE";
        this.colILE.MinWidth = 70;
        this.colILE.Name = "colILE";
        this.colILE.OptionsColumn.AllowEdit = false;
        this.colILE.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        // 
        // colIApproveInBudgetOrig
        // 
        this.colIApproveInBudgetOrig.AppearanceHeader.Options.UseTextOptions = true;
        this.colIApproveInBudgetOrig.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIApproveInBudgetOrig.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIApproveInBudgetOrig.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIApproveInBudgetOrig.Caption = "APPROVE_ORIG";
        this.colIApproveInBudgetOrig.FieldName = "APPROVE_ORIG";
        this.colIApproveInBudgetOrig.MinWidth = 70;
        this.colIApproveInBudgetOrig.Name = "colIApproveInBudgetOrig";
        this.colIApproveInBudgetOrig.OptionsColumn.AllowEdit = false;
        // 
        // colIID
        // 
        this.colIID.AppearanceHeader.Options.UseTextOptions = true;
        this.colIID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIID.Caption = "INITIATIVE_ID";
        this.colIID.FieldName = "INITIATIVE_ID";
        this.colIID.MinWidth = 70;
        this.colIID.Name = "colIID";
        // 
        // colIRegionID
        // 
        this.colIRegionID.AppearanceHeader.Options.UseTextOptions = true;
        this.colIRegionID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colIRegionID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colIRegionID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colIRegionID.Caption = "RegionID";
        this.colIRegionID.FieldName = "REGION_ID";
        this.colIRegionID.MinWidth = 70;
        this.colIRegionID.Name = "colIRegionID";
        // 
        // colIChannelTypeId
        // 
        this.colIChannelTypeId.Caption = "ChanelType_ID";
        this.colIChannelTypeId.FieldName = "ChanelType_ID";
        this.colIChannelTypeId.MinWidth = 70;
        this.colIChannelTypeId.Name = "colIChannelTypeId";
        // 
        // colIForLE
        // 
        this.colIForLE.FieldName = "IS_SUM";
        this.colIForLE.Name = "colIForLE";
        // 
        // splitContainerControl
        // 
        this.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.splitContainerControl.Horizontal = false;
        this.splitContainerControl.Location = new System.Drawing.Point(0, 34);
        this.splitContainerControl.Name = "splitContainerControl";
        this.splitContainerControl.Panel1.Controls.Add(this.gridControlS);
        this.splitContainerControl.Panel1.MinSize = 200;
        this.splitContainerControl.Panel1.Text = "Panel1";
        this.splitContainerControl.Panel2.Controls.Add(this.gridControlI);
        this.splitContainerControl.Panel2.MinSize = 200;
        this.splitContainerControl.Panel2.Text = "Panel2";
        this.splitContainerControl.Size = new System.Drawing.Size(1039, 396);
        this.splitContainerControl.SplitterPosition = 223;
        this.splitContainerControl.TabIndex = 7;
        this.splitContainerControl.Text = "splitContainerControl1";
        // 
        // gridControlS
        // 
        this.gridControlS.Dock = System.Windows.Forms.DockStyle.Fill;
        this.gridControlS.Location = new System.Drawing.Point(0, 0);
        this.gridControlS.MainView = this.gridViewS;
        this.gridControlS.MenuManager = this.barManager;
        this.gridControlS.Name = "gridControlS";
        this.gridControlS.Size = new System.Drawing.Size(1039, 200);
        this.gridControlS.TabIndex = 0;
        this.gridControlS.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewS});
        this.gridControlS.SizeChanged += new System.EventHandler(this.gridControl_Resized);
        this.gridControlS.Resize += new System.EventHandler(this.gridControl_Resized);
        // 
        // gridViewS
        // 
        this.gridViewS.ColumnPanelRowHeight = 34;
        this.gridViewS.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSTerritory,
            this.colSPrevYear,
            this.colSCurrentYear,
            this.colSApprovedSummary,
            this.colSInvestmentsYTD,
            this.colSLEBudget,
            this.colSRegionID});
        this.gridViewS.GridControl = this.gridControlS;
        this.gridViewS.Name = "gridViewS";
        this.gridViewS.OptionsCustomization.AllowGroup = false;
        this.gridViewS.OptionsFilter.MaxCheckedListItemCount = 1000;
        this.gridViewS.OptionsMenu.EnableColumnMenu = false;
        this.gridViewS.OptionsMenu.EnableFooterMenu = false;
        this.gridViewS.OptionsSelection.EnableAppearanceFocusedCell = false;
        this.gridViewS.OptionsSelection.EnableAppearanceFocusedRow = false;
        this.gridViewS.OptionsSelection.EnableAppearanceHideSelection = false;
        this.gridViewS.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
        this.gridViewS.OptionsView.ShowFooter = true;
        this.gridViewS.OptionsView.ShowGroupPanel = false;
        this.gridViewS.OptionsView.ShowIndicator = false;
        this.gridViewS.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSTerritory, DevExpress.Data.ColumnSortOrder.Ascending)});
        this.gridViewS.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gridViewS_CustomDrawFooterCell);
        this.gridViewS.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewS_CustomDrawCell);
        // 
        // colSTerritory
        // 
        this.colSTerritory.AppearanceHeader.Options.UseTextOptions = true;
        this.colSTerritory.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSTerritory.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSTerritory.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSTerritory.Caption = "Территория";
        this.colSTerritory.FieldName = "Region";
        this.colSTerritory.MinWidth = 70;
        this.colSTerritory.Name = "colSTerritory";
        this.colSTerritory.OptionsColumn.AllowEdit = false;
        this.colSTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colSTerritory.SummaryItem.DisplayFormat = "Украина";
        this.colSTerritory.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
        this.colSTerritory.Visible = true;
        this.colSTerritory.VisibleIndex = 0;
        this.colSTerritory.Width = 157;
        // 
        // colSPrevYear
        // 
        this.colSPrevYear.AppearanceHeader.Options.UseTextOptions = true;
        this.colSPrevYear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSPrevYear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSPrevYear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSPrevYear.Caption = "Сумма по инициативам 2009 года, грн";
        this.colSPrevYear.DisplayFormat.FormatString = "N2";
        this.colSPrevYear.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSPrevYear.FieldName = "PrevYear";
        this.colSPrevYear.GroupFormat.FormatString = "N2";
        this.colSPrevYear.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSPrevYear.MinWidth = 70;
        this.colSPrevYear.Name = "colSPrevYear";
        this.colSPrevYear.OptionsColumn.AllowEdit = false;
        this.colSPrevYear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colSPrevYear.SummaryItem.DisplayFormat = "{0:N2}";
        this.colSPrevYear.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        this.colSPrevYear.Visible = true;
        this.colSPrevYear.VisibleIndex = 1;
        this.colSPrevYear.Width = 230;
        // 
        // colSCurrentYear
        // 
        this.colSCurrentYear.AppearanceHeader.Options.UseTextOptions = true;
        this.colSCurrentYear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSCurrentYear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSCurrentYear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSCurrentYear.Caption = "Сумма по инициативам 2010 года, грн";
        this.colSCurrentYear.DisplayFormat.FormatString = "N2";
        this.colSCurrentYear.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSCurrentYear.FieldName = "CurrentYear";
        this.colSCurrentYear.GroupFormat.FormatString = "N2";
        this.colSCurrentYear.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSCurrentYear.MinWidth = 70;
        this.colSCurrentYear.Name = "colSCurrentYear";
        this.colSCurrentYear.OptionsColumn.AllowEdit = false;
        this.colSCurrentYear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colSCurrentYear.SummaryItem.DisplayFormat = "{0:N2}";
        this.colSCurrentYear.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        this.colSCurrentYear.Visible = true;
        this.colSCurrentYear.VisibleIndex = 2;
        this.colSCurrentYear.Width = 218;
        // 
        // colSApprovedSummary
        // 
        this.colSApprovedSummary.AppearanceHeader.Options.UseTextOptions = true;
        this.colSApprovedSummary.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSApprovedSummary.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSApprovedSummary.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSApprovedSummary.Caption = "Итого бюджет, грн";
        this.colSApprovedSummary.DisplayFormat.FormatString = "N2";
        this.colSApprovedSummary.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSApprovedSummary.FieldName = "Summary";
        this.colSApprovedSummary.GroupFormat.FormatString = "N2";
        this.colSApprovedSummary.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSApprovedSummary.MinWidth = 70;
        this.colSApprovedSummary.Name = "colSApprovedSummary";
        this.colSApprovedSummary.OptionsColumn.AllowEdit = false;
        this.colSApprovedSummary.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colSApprovedSummary.SummaryItem.DisplayFormat = "{0:N2}";
        this.colSApprovedSummary.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        this.colSApprovedSummary.Visible = true;
        this.colSApprovedSummary.VisibleIndex = 3;
        this.colSApprovedSummary.Width = 141;
        // 
        // colSInvestmentsYTD
        // 
        this.colSInvestmentsYTD.AppearanceHeader.Options.UseTextOptions = true;
        this.colSInvestmentsYTD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSInvestmentsYTD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSInvestmentsYTD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSInvestmentsYTD.Caption = "Инвестиции/Затраты факт YTD, грн";
        this.colSInvestmentsYTD.DisplayFormat.FormatString = "N2";
        this.colSInvestmentsYTD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSInvestmentsYTD.FieldName = "InvestmentsYTD";
        this.colSInvestmentsYTD.GroupFormat.FormatString = "N2";
        this.colSInvestmentsYTD.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSInvestmentsYTD.MinWidth = 70;
        this.colSInvestmentsYTD.Name = "colSInvestmentsYTD";
        this.colSInvestmentsYTD.OptionsColumn.AllowEdit = false;
        this.colSInvestmentsYTD.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colSInvestmentsYTD.SummaryItem.DisplayFormat = "{0:N2}";
        this.colSInvestmentsYTD.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        this.colSInvestmentsYTD.Visible = true;
        this.colSInvestmentsYTD.VisibleIndex = 4;
        this.colSInvestmentsYTD.Width = 212;
        // 
        // colSLEBudget
        // 
        this.colSLEBudget.AppearanceHeader.Options.UseTextOptions = true;
        this.colSLEBudget.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSLEBudget.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSLEBudget.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSLEBudget.Caption = "Итого LE, грн";
        this.colSLEBudget.DisplayFormat.FormatString = "N2";
        this.colSLEBudget.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSLEBudget.FieldName = "LE";
        this.colSLEBudget.GroupFormat.FormatString = "N2";
        this.colSLEBudget.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSLEBudget.MinWidth = 70;
        this.colSLEBudget.Name = "colSLEBudget";
        this.colSLEBudget.OptionsColumn.AllowEdit = false;
        this.colSLEBudget.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colSLEBudget.SummaryItem.DisplayFormat = "{0:N2}";
        this.colSLEBudget.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        this.colSLEBudget.Visible = true;
        this.colSLEBudget.VisibleIndex = 5;
        this.colSLEBudget.Width = 141;
        // 
        // colSRegionID
        // 
        this.colSRegionID.AppearanceHeader.Options.UseTextOptions = true;
        this.colSRegionID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSRegionID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSRegionID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSRegionID.Caption = "RegionID";
        this.colSRegionID.FieldName = "RegionID";
        this.colSRegionID.MinWidth = 70;
        this.colSRegionID.Name = "colSRegionID";
        // 
        // BudgetCreationControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.AutoScrollMinSize = new System.Drawing.Size(0, 430);
        this.Controls.Add(this.splitContainerControl);
        this.Name = "BudgetCreationControl";
        this.Size = new System.Drawing.Size(1039, 429);
        this.Controls.SetChildIndex(this.splitContainerControl, 0);
        ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridControlI)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridViewI)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
        this.splitContainerControl.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.gridControlS)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridViewS)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarButtonItem barButtonBudgetInWork;
    private DevExpress.XtraGrid.GridControl gridControlI;
    private DevExpress.XtraGrid.Views.Grid.GridView gridViewI;
    private DevExpress.XtraGrid.Columns.GridColumn colIRegion;
    private DevExpress.XtraGrid.Columns.GridColumn colIInitiative;
    private DevExpress.XtraGrid.Columns.GridColumn colICreationDate;
    private DevExpress.XtraGrid.Columns.GridColumn colITerm;
    private DevExpress.XtraGrid.Columns.GridColumn colIPrevPlannedSalesV;
    private DevExpress.XtraGrid.Columns.GridColumn colICurrentPlannedSalesV;
    private DevExpress.XtraGrid.Columns.GridColumn colIPrevYear;
    private DevExpress.XtraGrid.Columns.GridColumn colICurrentYear;
    private DevExpress.XtraGrid.Columns.GridColumn colIStatus;
    private DevExpress.XtraGrid.Columns.GridColumn colIApproveInBudget;
    private DevExpress.XtraGrid.Columns.GridColumn colIInWork;
    private DevExpress.XtraGrid.Columns.GridColumn colIInvestmentsYTD;
    private DevExpress.XtraGrid.Columns.GridColumn colILE;
    private DevExpress.XtraGrid.Columns.GridColumn colIApproveInBudgetOrig;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl;
    private DevExpress.XtraGrid.GridControl gridControlS;
    private DevExpress.XtraGrid.Views.Grid.GridView gridViewS;
    private DevExpress.XtraGrid.Columns.GridColumn colSTerritory;
    private DevExpress.XtraGrid.Columns.GridColumn colSPrevYear;
    private DevExpress.XtraGrid.Columns.GridColumn colSCurrentYear;
    private DevExpress.XtraGrid.Columns.GridColumn colSApprovedSummary;
    private DevExpress.XtraGrid.Columns.GridColumn colSLEBudget;
    private DevExpress.XtraGrid.Columns.GridColumn colIID;
    private DevExpress.XtraGrid.Columns.GridColumn colIRegionID;
    private DevExpress.XtraGrid.Columns.GridColumn colSRegionID;
    private DevExpress.XtraGrid.Columns.GridColumn colIInvestmentsPrev;
    private DevExpress.XtraGrid.Columns.GridColumn colIInvestmentsCur;
    private DevExpress.XtraGrid.Columns.GridColumn colSInvestmentsYTD;
    private DevExpress.XtraGrid.Columns.GridColumn colIChannelTypeId;
    private DevExpress.XtraGrid.Columns.GridColumn colIForLE;
  }
}
