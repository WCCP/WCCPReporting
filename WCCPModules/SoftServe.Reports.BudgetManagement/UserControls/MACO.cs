﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using SoftServe.Reports.BudgetManagement.DataAccess;
using SoftServe.Reports.BudgetManagement.Tabs;
using SoftServe.Reports.BudgetManagement.Utility;
using DevExpress.XtraBars;
using SoftServe.Reports.BudgetManagement.DataAccess.DataSets;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using SoftServe.Reports.BudgetManagement.Forms;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace SoftServe.Reports.BudgetManagement.UserControls
{
    [ToolboxItem(false)]
    public partial class MACOControl : CommonBaseControl
    {
        private const int MONTHS_TO_ALLOW_EDIT_AND_IMPORT = -1;
        private const int FAKE_PRODUCT_ID = -1;

        private ImportedData.DataDataTable originalData;
        private static Dictionary<string, KeyValuePair<int, string>> regionMapping;
        private static Dictionary<string, KeyValuePair<int, string>> skuMapping;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MACOControl"/> class.
        /// </summary>
        public MACOControl()
        {
            InitializeComponent();
            AdditionalInitialization();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MACOControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        /// <param name="correlator">The correlator.</param>
        public MACOControl(CommonBaseTab parentTab, ActionsCorrelator correlator)
            : base(parentTab, correlator)
        {
            InitializeComponent();
            AdditionalInitialization();

            correlator.Save += Save;
        }

        /// <summary>
        /// Perform additional initialization.
        /// </summary>
        private void AdditionalInitialization()
        {
            barEditItemChannel.Visibility = BarItemVisibility.Never;
            barEditItemType.Visibility = BarItemVisibility.Never;
            barManager.Items.Add(barButtonImportMACO);
            barMain.LinksPersistInfo.Add(new LinkPersistInfo(
                                             barButtonImportMACO, true));


            if (DesignMode) return;

            MatchData md = new MatchData();
            DataTable dtRegion = DataAccess.DataProvider.RegionMapping;
            DataTable dtSKU = DataAccess.DataProvider.SKUMapping;

            md.Region.Load(dtRegion.CreateDataReader());
            md.SKU.Load(dtSKU.CreateDataReader());

            regionMapping = md.Region.ToDictionary(d => d.RemoteRegionName.ToUpper(), d => new KeyValuePair<int, string>(d.Region_id, d.Region_name));
            skuMapping =
                md.SKU.ToDictionary(
                    d => string.Format(Constants.PRODUCT_KEY_FORMAT, d.RemoteName.Trim().ToUpper(), d.RemotePackSize.ToString().Trim().ToUpper(), d.RemotePackName.Trim().ToUpper()),
                    d => new KeyValuePair<int, string>(d.Product_Id, d.ProductName));
        }

        #endregion

        #region Properties

        public override PrintableComponentLink[] PrintableComponentLinks
        {
            get { return new[] {new PrintableComponentLink {Component = gridControl}}; }
        }

        public override bool IsChanged
        {
            get
            {
                gridView.CloseEditor();
                ImportedData.DataDataTable data = (ImportedData.DataDataTable) gridControl.DataSource;
                var modified = from d in data where !d.IsStatusNull() && (d.Status.Equals(RowStatus.Updated.ToString()) || d.Status.Equals(RowStatus.New.ToString())) select d;
                return modified.Count() > 0;
            }
        }

        #endregion

        /// <summary>
        /// Loads data.
        /// </summary>
        public override void Load()
        {
            ImportedData data = new ImportedData();
            DataTable dtMACO = DataProvider.GetMACO(Parameters);
            data.Data.Load(dtMACO.CreateDataReader());
            data.Data.PrimaryKey = new[] {data.Data.Region_idColumn, data.Data.Product_IdColumn};
            if (data.Data.Rows.Count == 0)
            {
                foreach (DataRow row in DataProvider.AvailableRegions.Rows)
                {
                    ImportedData.DataRow dataRow = data.Data.NewDataRow();
                    dataRow.Product_Id = FAKE_PRODUCT_ID;
                    dataRow.ProductName = String.Empty;
                    dataRow.Region_id = row.Field<int>(Constants.SP_GET_TERRITORY_LIST_FLD_REGION_ID);
                    dataRow.Region_name = row.Field<string>(Constants.SP_GET_TERRITORY_LIST_FLD_REGION_NAME);
                    data.Data.Rows.Add(dataRow);
                }
            }
            originalData = (ImportedData.DataDataTable) data.Data.Copy();
            gridControl.DataSource = data.Data;

            var editable = IsAllowMacoImport;

            barButtonImportMACO.Enabled =
                editable
                && SelectedYear >= DateTime.Now.AddMonths(MONTHS_TO_ALLOW_EDIT_AND_IMPORT).Year
                && Correlator.HasWriteAccess;

            gridView.OptionsBehavior.Editable = editable && !data.Data.Any(row => row.Product_Id == FAKE_PRODUCT_ID);
            barButtonSave.Enabled = editable;
        }

        private bool IsAllowMacoImport
        {
            get { return AccessLevel.CheckAccessLevel(AccessLevelType.AllowMacoImport); }
        }

        /// <summary>
        /// Saves data.
        /// </summary>
        protected void Save(int typeId, int year)
        {
            gridView.CloseEditor();
            ImportedData.DataDataTable data = (ImportedData.DataDataTable) gridControl.DataSource;

            BudgetParameters parameters = Parameters;
            parameters.Year = year;
            parameters.ActivityType = typeId;

            var modified = from d in data where !d.IsStatusNull() && (d.Status.Equals(RowStatus.Updated.ToString()) || d.Status.Equals(RowStatus.New.ToString())) select d;
            foreach (ImportedData.DataRow row in modified)
                DataProvider.UpdateMACO(parameters, row);
        }

        /// <summary>
        /// Populates the column.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="data">The data.</param>
        private static void PopulateColumn(ImportedData.DataDataTable dt, string columnName, object data)
        {
            if (!dt.Columns.Contains(columnName))
            {
                dt.Columns.Add(columnName);
            }
            DataColumn dc = dt.Columns[dt.Columns.IndexOf(columnName)];
            foreach (DataRow row in dt.Rows)
            {
                row[dc] = data;
            }
        }

        private bool IsCellEditable(GridColumn column, object cellValue)
        {
            DateTime tmpDate = DateTime.Now.AddMonths(MONTHS_TO_ALLOW_EDIT_AND_IMPORT); //Previous month should be editable.
            return ((column.AbsoluteIndex > tmpDate.Month + 1 && SelectedYear == tmpDate.Year)
                    || (cellValue == DBNull.Value && SelectedYear >= tmpDate.Year)
                    || SelectedYear > tmpDate.Year)
                   && !column.Equals(colRegion)
                   && !column.Equals(colSKU)
                   && !column.Equals(colBudget)
                   && !column.Equals(colLe)
                   && Correlator.HasWriteAccess;
        }

        #region Event handlers

        /// <summary>
        /// Handles the ItemClick event of the barButtonImportMACO control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonImportMACO_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    WaitManager.StartWait();
                    ImportedData data = new ImportedData();
                    int year = Utility.Utility.ReadFile(openFileDialog.FileName, data);
                    WaitManager.StopWait();
                    if (year == SelectedYear
                        ||
                        XtraMessageBox.Show(String.Format(Resource.Error_IncorrectImportYear, year, SelectedYear), Application.ProductName, MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        WaitManager.StartWait();
                        Utility.Utility.MatchRecords(data, regionMapping, skuMapping);

                        string[] ur = (from d in data.Data where d.IsRegion_idNull() select d.REGNAME).Distinct(StringComparer.CurrentCultureIgnoreCase).ToArray();
                        string[] up =
                            (from d in data.Data where d.IsProduct_IdNull() select String.Format(Constants.PRODUCT_DISPLAY_FORMAT, d.PACKTYPE, d.PACKSIZE, d.SPECNAME)).Distinct(
                                StringComparer.CurrentCultureIgnoreCase).ToArray();
                        if (ur.Length > 0 || up.Length > 0)
                            new MappingErrorForm {UnmappedRegions = ur, UnmappedSKU = up}.ShowDialog();

                        for (int i = data.Data.Rows.Count - 1; i >= 0; i--)
                            if (((ImportedData.DataRow) data.Data.Rows[i]).IsRegion_idNull() || ((ImportedData.DataRow) data.Data.Rows[i]).IsProduct_IdNull())
                                data.Data.Rows[i].Delete();

                        if (data.Data.Rows.Count > 0)
                        {
                            PopulateColumn(data.Data, data.Data.StatusColumn.ColumnName, RowStatus.New);
                            ImportedData.DataDataTable dt = (ImportedData.DataDataTable) gridControl.DataSource;
                            data.Data.PrimaryKey = new[] {data.Data.Region_idColumn, data.Data.Product_IdColumn};
                            Merge(dt, data.Data);
                            for (int i = dt.Rows.Count; i > 0; i--)
                            {
                                if (((ImportedData.DataRow)dt.Rows[i - 1]).Product_Id == FAKE_PRODUCT_ID)
                                {
                                    dt.Rows.RemoveAt(i - 1);
                                }
                            }

                            gridControl.RefreshDataSource();
                            gridView.OptionsBehavior.Editable = IsAllowMacoImport;
                        }
                        WaitManager.StopWait();
                    }
                }
                catch (UserException ex)
                {
                    WaitManager.StopWait();
                    XtraMessageBox.Show(ex.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private static ImportedData.DataRow GetRowToUpdate(ImportedData.DataDataTable dstDataTable, DataColumn[] pkSrcColumns, ImportedData.DataRow srcRow)
        {
            ImportedData.DataRow rowToUpdate = null;
            DataColumn[] pkDstColumns = dstDataTable.PrimaryKey;
            foreach (ImportedData.DataRow dstRow in dstDataTable.Rows)
            {
                bool isEqual = true;
                for (int i = 0; i < pkDstColumns.Length; i++)
                {
                    isEqual = srcRow[pkSrcColumns[i]].Equals(dstRow[pkDstColumns[i]]);
                    if (!isEqual)
                        break;
                }
                if (isEqual)
                {
                    rowToUpdate = dstRow;
                    break;
                }
            }
            return rowToUpdate;
        }

        private static void UpdateRow(ImportedData.DataDataTable dstDataTable, ImportedData.DataDataTable srcDataTable, ImportedData.DataRow dstRow, ImportedData.DataRow srcRow)
        {
            for (int i = 0; i < dstDataTable.Columns.Count && i < srcDataTable.Columns.Count; i++)
            {
                if (!srcRow.IsNull(srcDataTable.Columns[i])
                    && srcDataTable.Columns[i] != srcDataTable.StatusColumn
                    && (srcRow[srcDataTable.Columns[i]].GetType() != typeof (double)
                        || (srcRow[srcDataTable.Columns[i]].GetType() == typeof (double) && srcRow.Field<double>(srcDataTable.Columns[i]) >= 0.01)
                        || dstRow.IsNull(dstDataTable.Columns[i])))
                    dstRow[dstDataTable.Columns[i]] = srcRow[srcDataTable.Columns[i]];
            }
            if (dstRow.IsStatusNull() || string.IsNullOrEmpty(dstRow.Status))
                dstRow.Status = RowStatus.Updated.ToString();
        }

        private static void AddRow(ImportedData.DataDataTable dstDataTable, DataRow srcRow)
        {
            dstDataTable.ImportRow(srcRow);
        }

        private void Merge(ImportedData.DataDataTable dstDataTable, ImportedData.DataDataTable srcDataTable)
        {
            DataColumn[] pkSrcColumns = srcDataTable.PrimaryKey;
            DataColumn[] pkDstColumns = dstDataTable.PrimaryKey;
            if (pkDstColumns.Length != pkSrcColumns.Length)
                throw new Exception("Count of PK columns should be equal.");

            foreach (ImportedData.DataRow srcRow in srcDataTable.Rows)
            {
                ImportedData.DataRow rowToUpdate = GetRowToUpdate(dstDataTable, pkSrcColumns, srcRow);
                if (rowToUpdate != null)
                {
                    UpdateRow(dstDataTable, srcDataTable, rowToUpdate, srcRow);
                }
                else
                {
                    AddRow(dstDataTable, srcRow);
                }
            }
        }


        /// <summary>
        /// Handles the CellValueChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            GridView gv = (GridView) sender;
            if (!e.Column.Equals(colStatus) && !gv.GetRowCellValue(e.RowHandle, colStatus).Equals(RowStatus.New.ToString()))
            {
                gv.SetRowCellValue(e.RowHandle, colStatus, RowStatus.Updated);
            }
        }

        /// <summary>
        /// Handles the ShowingEditor event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void gridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView) sender;
            e.Cancel = !IsCellEditable(view.FocusedColumn, view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn));
        }

        /// <summary>
        /// Handles the ValidatingEditor event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs"/> instance containing the event data.</param>
        private void gridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString()))
                e.Value = DBNull.Value;
        }

        /// <summary>
        /// Handles the KeyDown event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = (GridView) sender;
            if (e.KeyCode == Keys.Delete
                && IsCellEditable(view.FocusedColumn, view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn)) 
                && IsAllowMacoImport)
            {
                view.SetRowCellValue(view.FocusedRowHandle, view.FocusedColumn, DBNull.Value);
            }
        }

        /// <summary>
        /// Handles the CustomDrawCell event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs"/> instance containing the event data.</param>
        private void gridView_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView gv = (GridView) sender;
            if (string.IsNullOrEmpty(e.DisplayText) 
                && Logica.Reports.BaseReportControl.Utility.ConvertEx.ToInt(gv.GetRowCellValue(e.RowHandle, colProductId)) != FAKE_PRODUCT_ID)
            {
                e.Graphics.FillRectangle(Constants.SoftRed, e.Bounds);
                e.Handled = true;
            }
            else if (gv.FocusedRowHandle == e.RowHandle) return;
            else
            {
                if (gv.GetRowCellValue(e.RowHandle, colStatus).Equals(RowStatus.New.ToString()))
                    e.Graphics.FillRectangle(Constants.SoftYellow, e.Bounds);
                else if (gv.GetRowCellValue(e.RowHandle, colStatus).Equals(RowStatus.Updated.ToString())
                         && (e.CellValue.GetType() == typeof (double)
                             && !originalData.Rows[gv.GetDataSourceRowIndex(e.RowHandle)][e.Column.FieldName].Equals(e.CellValue)
                             || e.CellValue.GetType() == typeof (string)
                             && !originalData.Rows[gv.GetDataSourceRowIndex(e.RowHandle)][e.Column.FieldName].ToString().Equals(e.CellValue.ToString())))
                    e.Graphics.FillRectangle(Constants.SoftYellow, e.Bounds);
            }
        }

        private bool isResized = false;

        private void gridControl_Resized(object sender, EventArgs e)
        {
            GridControl grid = sender as GridControl;
            if (grid == null)
            {
                return;
            }

            GridView view = grid.DefaultView as GridView;
            if (view == null)
            {
                return;
            }

            GridViewInfo viewInfo = (GridViewInfo) view.GetViewInfo();
            if (viewInfo.ColumnsInfo.LastColumnInfo != null && viewInfo.ColumnsInfo.LastColumnInfo.Bounds.Right > viewInfo.ViewRects.ColumnPanel.Right)
            {
                view.OptionsView.ColumnAutoWidth = false;
                view.BestFitColumns();
            }
            else
            {
                view.OptionsView.ColumnAutoWidth = true;
            }

            if (!isResized)
            {
                col12.Width = col11.Width;
                isResized = true;
            }
        }

        #endregion
    }
}