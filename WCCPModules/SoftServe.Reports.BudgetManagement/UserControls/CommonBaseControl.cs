﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraPrinting;
using SoftServe.Reports.BudgetManagement.DataAccess;
using SoftServe.Reports.BudgetManagement.Tabs;
using SoftServe.Reports.BudgetManagement.Utility;

namespace SoftServe.Reports.BudgetManagement.UserControls
{
    [ToolboxItem(false)]
    public partial class CommonBaseControl : UserControl
    {
        #region Fields

        private ActionsCorrelator correlator;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "CommonBaseControl" /> class.
        /// </summary>
        public CommonBaseControl()
        {
            InitializeComponent();
            PopulateComboBoxes();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "CommonBaseControl" /> class.
        /// </summary>
        /// <param name = "parentTab">The parent tab.</param>
        /// <param name = "correlator">The correlator.</param>
        public CommonBaseControl(CommonBaseTab parentTab, ActionsCorrelator correlator)
        {
            InitializeComponent();

            ParentTab = parentTab;

            PopulateComboBoxes();

            Correlator = correlator;
        }

        #endregion

        #region Instance Properties

        public virtual bool IsChanged
        {
            get { return false; }
        }

        public virtual PrintableComponentLink[] PrintableComponentLinks
        {
            get { return new PrintableComponentLink[] {}; }
        }

        public ActionsCorrelator Correlator
        {
            get { return correlator; }
            set
            {
                correlator = value;

                correlator.SelectedTypeId = SelectedTypeId;
                correlator.SelectedYear = SelectedYear;

                correlator.YearChanged += (sender, e) =>
                                              {
                                                  barEditItemYear.BeginUpdate();
                                                  barEditItemYear.EditValue = ((BarEditItem) sender).EditValue;
                                                  barEditItemYear.EndUpdate();
                                              };
                correlator.TypeChanged += (sender, e) =>
                                              {
                                                  barEditItemType.BeginUpdate();
                                                  barEditItemType.EditValue = ((BarEditItem) sender).EditValue;
                                                  barEditItemType.EndUpdate();
                                              };
                correlator.ChannelChanged += (sender, e) =>
                                                 {
                                                     barEditItemChannel.BeginUpdate();
                                                     barEditItemChannel.EditValue = ((BarEditItem) sender).EditValue;
                                                     barEditItemChannel.EndUpdate();
                                                 };
                correlator.CurrentDateChanged += (sender, e) =>
                                                     {
                                                         barEditItemDate.BeginUpdate();
                                                         barEditItemDate.EditValue = ((BarEditItem) sender).EditValue;
                                                         barEditItemDate.EndUpdate();
                                                     };

                correlator.CheckForChanges += () => IsChanged;
                correlator.Load += Load;

                barButtonSave.Enabled = correlator.HasWriteAccess;
            }
        }

        public CommonBaseTab ParentTab { get; set; }

        public int SelectedChannelId
        {
            get { return barEditItemChannel.EditValue is int ? (int) barEditItemChannel.EditValue : 0; }
        }

        public DateTime SelectedCurrentDate
        {
            get { return (barEditItemDate.EditValue is DateTime ? (DateTime) barEditItemDate.EditValue : DateTime.Now).Date; }
        }

        public int SelectedTypeId
        {
            get { return (barEditItemType.EditValue != null) ? Convert.ToInt32(barEditItemType.EditValue) : 0; }
        }

        public int SelectedYear
        {
            get { return (barEditItemYear.EditValue != null) ? Convert.ToInt32(barEditItemYear.EditValue) : DateTime.Now.Year; }
        }

        public BudgetParameters Parameters
        {
            get { return new BudgetParameters {ActivityType = SelectedTypeId, Channel = SelectedChannelId, CurrentDate = SelectedCurrentDate, Year = SelectedYear}; }
        }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Loads this instance.
        /// </summary>
        public virtual void Load()
        {
        }

        /// <summary>
        ///   Populates the combo boxes.
        /// </summary>
        private void PopulateComboBoxes()
        {
            barEditItemDate.Visibility = DataProvider.IsDebug ? BarItemVisibility.Always : BarItemVisibility.Never;
            if (!DesignMode && ParentTab != null)
            {
                repositoryItemLookUpEditYear.DataSource = DataProvider.YearsList;
                repositoryItemLookUpEditType.DataSource = DataProvider.ActivityList;
                repositoryItemLookUpEditChannel.DataSource = DataProvider.ChannelsList;

                barEditItemYear.EditValue = DateTime.Now.Year;
                barEditItemType.EditValue = DataProvider.ActivityList.Rows[0][repositoryItemLookUpEditType.ValueMember];
                barEditItemChannel.EditValue = DataProvider.ChannelsList.Rows[0][repositoryItemLookUpEditChannel.ValueMember];

                barEditItemDate.EditValue = DateTime.Now;
            }
        }

        #endregion

        #region Event Handling

        private void barButtonSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (Correlator != null)
                Correlator.FireSave(this);
        }

        private void barEditItemChannel_EditValueChanged(object sender, EventArgs e)
        {
            if (Correlator != null)
                Correlator.FireChannelChange(barEditItemChannel);
        }

        private void barEditItemDate_EditValueChanged(object sender, EventArgs e)
        {
            if (Correlator != null)
                Correlator.FireCurrentDateChange(barEditItemDate);
        }

        private void barEditItemType_EditValueChanged(object sender, EventArgs e)
        {
            if (Correlator != null)
                Correlator.FireTypeChange(barEditItemType);
        }

        private void barEditItemYear_EditValueChanged(object sender, EventArgs e)
        {
            if (Correlator != null)
                Correlator.FireYearChange(barEditItemYear);
        }

        #endregion
    }
}