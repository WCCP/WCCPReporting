﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using SoftServe.Reports.BudgetManagement.DataAccess;
using SoftServe.Reports.BudgetManagement.Tabs;
using SoftServe.Reports.BudgetManagement.Utility;

namespace SoftServe.Reports.BudgetManagement.UserControls
{
    [ToolboxItem(false)]
    public partial class PlanFactControl : CommonBaseControl
    {
        #region Constants

        private const string BLACK = "BLACK";
        private const string GREEN = "#009E0F";
        private const string RED = "#D44C4A";

        #endregion

        #region Readonly & Static Fields

        private readonly Dictionary<TreeListColumn, TreeListColumn[]> columnsMapping = new Dictionary<TreeListColumn, TreeListColumn[]>();
        private readonly ReportDrawers drawers = new ReportDrawers();
        private readonly Dictionary<TreeListColumn, string[]> drawersMapping = new Dictionary<TreeListColumn, string[]>();

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "PlanFactControl" /> class.
        /// </summary>
        public PlanFactControl()
        {
            InitializeComponent();
            AdditionalInitialization();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "PlanFactControl" /> class.
        /// </summary>
        /// <param name = "parentTab">The parent tab.</param>
        /// <param name = "correlator">The correlator.</param>
        public PlanFactControl(CommonBaseTab parentTab, ActionsCorrelator correlator)
            : base(parentTab, correlator)
        {
            InitializeComponent();
            AdditionalInitialization();
            correlator.ChannelChanged += (sender, args) => Load();
        }

        #endregion

        #region Instance Properties

        public override PrintableComponentLink[] PrintableComponentLinks
        {
            get { return new[] {new PrintableComponentLink {Component = treeList}}; }
        }

        #endregion

        #region Instance Methods

        public override void Load()
        {
            treeList.DataSource = DataProvider.GetAreaAndStaffTree(Parameters);
            treeList.ExpandAll();

            barButtonSave.Enabled =
                AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveInWork) ||
                AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveActivity) ||
                AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveContract) ||
                AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveM3) ||
                AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveM4) ||
                AccessLevel.CheckAccessLevel(AccessLevelType.AllowApprovePricing) ||
                AccessLevel.CheckAccessLevel(AccessLevelType.AllowMacoImport);
        }

        private void AdditionalInitialization()
        {
            columnsMapping.Add(colStrCaption_c, new[] {colStrCaption_c, colStrCaption_v, colStrCaption_m});
            columnsMapping.Add(colPlanValue_c, new[] {colPlanValue_c, colPlanValue_v, colPlanValue_m});
            columnsMapping.Add(colLEValue_c, new[] {colLEValue_c, colLEValue_v, colLEValue_m});
            columnsMapping.Add(colPlanLEDiff_c, new[] {colPlanLEDiff_c, colPlanLEDiff_v, colPlanLEDiff_m});
            columnsMapping.Add(colDev1_c, new[] {colDev1_c, colDev1_v, colDev1_m});
            columnsMapping.Add(colFactValue_c, new[] {colFactValue_c, colFactValue_v, colFactValue_m});
            columnsMapping.Add(colDev2_c, new[] {colDev2_c, colDev2_v, colDev2_m});

            drawersMapping.Add(colStrCaption_c, new[] {"UNSIGNED_BLACK", "UNSIGNED_BLACK", "UNSIGNED_BLACK"});
            drawersMapping.Add(colPlanValue_c, new[] {"UNSIGNED_BLACK", "UNSIGNED_BLACK", "UNSIGNED_BLACK"});
            drawersMapping.Add(colLEValue_c, new[] {"UNSIGNED_{0}", "UNSIGNED_{0}", "UNSIGNED_{0}"});
            drawersMapping.Add(colPlanLEDiff_c, new[] {"SIGNED_{0}", "SIGNED_{0}", "SIGNED_{0}"});
            drawersMapping.Add(colDev1_c, new[] {"DEVIATION_#CF2A27_{0}", "DEVIATION_#6FA8DC_{0}", "DEVIATION_#009E0F_{0}"});
            drawersMapping.Add(colFactValue_c, new[] {"UNSIGNED_BLACK", "UNSIGNED_BLACK", "UNSIGNED_BLACK"});
            drawersMapping.Add(colDev2_c, new[] {"PROGRESS_#CF2A27_BLACK_C", "PROGRESS_#6FA8DC_BLACK_S", "PROGRESS_#009E0F_BLACK_M"});
        }

        #endregion

        #region Event Handling

        private void treeList_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            int y;
            if (!e.Column.Equals(colTeritory))
            {
                Rectangle[] rects = GetRectangles(e.Bounds, 3);

                for (int i = 0; i < rects.Length; i++)
                {
                    drawers.GetDrawer(String.Format(drawersMapping[e.Column][i], GetDesiredColor(e.Node, columnsMapping[colPlanLEDiff_c][i],
                                                                                                 columnsMapping[colStrCaption_c][i])))
                        .Invoke((TreeList) sender, e, e.Node.GetValue(columnsMapping[e.Column][i]), rects[i]);

                    y = rects[i].Y;
                    if (e.Node.PrevNode == null && !e.Node.Equals(e.Node.RootNode) && i == 0) // For first child node we should hide upper line
                        y--;

                    e.Graphics.DrawLines(Constants.CellBorder,
                                         new[]
                                             {
                                                 new Point(rects[i].X, y),
                                                 new Point(rects[i].Right - 1, y),
                                                 new Point(rects[i].Right - 1, rects[i].Bottom),
                                                 new Point(rects[i].X, rects[i].Bottom)
                                             });
                }
                e.Handled = true;
            }

            #region Draw borders for row

            int x = e.Bounds.X;
            y = e.Bounds.Y;
            int r = e.Bounds.Right;
            int b = e.Bounds.Bottom;

            if (e.Node.Expanded) // For expanded nodes we should show bottom
                b--;
            if (e.Node.PrevNode == null && !e.Node.Equals(e.Node.RootNode)) // For first child node we should hide upper line
                y--;
            //if (e.Node.NextNode == null)

            e.Graphics.DrawLine(Pens.Black, x, y, r, y);
            e.Graphics.DrawLine(Pens.Black, x, b, r, b);
            if (e.Column.Equals(colTeritory))
            {
                e.Graphics.DrawLine(Pens.Black, x, y, x, b);
                e.Graphics.DrawLine(Constants.CellBorder, r - 1, y, r - 1, b);
            }
            if (e.Column.Equals(colDev2_c))
                e.Graphics.DrawLine(Pens.Black, r - 1, y, r - 1, b);

            #endregion
        }

        #endregion

        #region Class Methods

        private string GetDesiredColor(TreeListNode node, TreeListColumn colPlanLeDiff, TreeListColumn colCaption)
        {
            decimal diff = (decimal) node[colPlanLeDiff];

            // diff == 0:black
            if (diff >= -0.001M && diff <= 0.001M)
            {
                return BLACK;
            }

            //В колонках "LE", "План-LE", "План-LE/План %" :
            //Если LE = плану, то цифры должны быть черными.
            //Для Затрат, если LE > плана, то цифры красные, иначе зеленые.
            if (colCaption == colStrCaption_c)
                return diff > 0 ? RED : GREEN;
            //Для МАСО и V если LE > плана, то цифра зеленая, иначе красная
            if (colCaption == colStrCaption_m || colCaption == colStrCaption_v)
                 return diff > 0 ? GREEN : RED;

            return BLACK;
        }

        private static Rectangle[] GetRectangles(Rectangle bounds, int rectanglesCount)
        {
            Rectangle boundsW = new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height);
            List<Rectangle> result = new List<Rectangle>();
            for (int i = 0; i < rectanglesCount; i++)
            {
                result.Add(new Rectangle(boundsW.X,
                                         boundsW.Y + (boundsW.Height)*i/rectanglesCount,
                                         boundsW.Width,
                                         (boundsW.Height)/rectanglesCount));
            }
            return result.ToArray();
        }

        #endregion
    }
}