﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.BudgetManagement.DataAccess;
using SoftServe.Reports.BudgetManagement.Tabs;
using SoftServe.Reports.BudgetManagement.Utility;
using System.Data.SqlClient;

//using DevExpress.CodeRush.StructuralParser;

namespace SoftServe.Reports.BudgetManagement.UserControls
{
    [ToolboxItem(false)]
    public partial class BudgetCreationControl : CommonBaseControl
    {
        #region Constructors

        public BudgetCreationControl()
        {
            InitializeComponent();
            AdditionalInitialization();
        }

        public BudgetCreationControl(CommonBaseTab parentTab, ActionsCorrelator correlator)
            : base(parentTab, correlator)
        {
            InitializeComponent();
            AdditionalInitialization();
            correlator.Save += Save;
            correlator.ChannelChanged += (sender, args) => Filter();
        }

        #endregion

        #region Instance Properties

        public override bool IsChanged
        {
            get
            {
                List<int> checkedIds = new List<int>();
                List<int> uncheckedIds = new List<int>();

                PopulateChanges(checkedIds, uncheckedIds);

                return checkedIds.Count > 0 || uncheckedIds.Count > 0;
            }
        }

        public override PrintableComponentLink[] PrintableComponentLinks
        {
            get { return new[] {new PrintableComponentLink {Component = gridControlS}, new PrintableComponentLink {Component = gridControlI}}; }
        }

        #endregion

        #region Instance Methods

        public override void Load()
        {
            DataTable dtS = new DataTable();
            dtS.Columns.Add(colSRegionID.FieldName, typeof (int));
            dtS.Columns.Add(colSTerritory.FieldName, typeof (string));
            dtS.Columns.Add(colSPrevYear.FieldName, typeof (decimal));
            dtS.Columns.Add(colSCurrentYear.FieldName, typeof (decimal));
            dtS.Columns.Add(colSApprovedSummary.FieldName, typeof (decimal));
            dtS.Columns.Add(colSInvestmentsYTD.FieldName, typeof (decimal));
            dtS.Columns.Add(colSLEBudget.FieldName, typeof (decimal));

            foreach (DataRow row in DataProvider.AvailableRegions.Rows)
            {
                dtS.Rows.Add(new object[] {row.Field<int>(Constants.SP_GET_TERRITORY_LIST_FLD_REGION_ID), row.Field<string>(Constants.SP_GET_TERRITORY_LIST_FLD_REGION_NAME)});
            }

            gridControlS.DataSource = dtS;

            DataTable dtI = DataProvider.GetInitiatives(Parameters);
            CopyColumn(dtI, colIApproveInBudget.FieldName, colIApproveInBudgetOrig.FieldName);
            gridControlI.DataSource = dtI;

            bool isBudgetInWork = dtI.AsEnumerable().Where(d => d.Field<bool>(colIInWork.FieldName)).Any();

            barButtonBudgetInWork.Enabled = AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveInWork)
                && !isBudgetInWork;

            barButtonSave.Enabled = !isBudgetInWork && (
                                                            AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveInWork) ||
                                                            AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveActivity) ||
                                                            AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveContract) ||
                                                            AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveM3) ||
                                                            AccessLevel.CheckAccessLevel(AccessLevelType.AllowApproveM4) ||
                                                            AccessLevel.CheckAccessLevel(AccessLevelType.AllowApprovePricing)
                                                        );
            //BUG #22151. БИ: Закладка "Формирование бюджета": Колонка Утвердить в бюджет має бути read-only якщо бюджет заморожений 
            colIApproveInBudget.OptionsColumn.ReadOnly = isBudgetInWork;

            #region Column captions adjustments

            colSPrevYear.Caption = string.Format(Resource.BudgetCreation_SummaryPrevYear, SelectedYear - 1);
            colSCurrentYear.Caption = string.Format(Resource.BudgetCreation_SummaryCurrentYear, SelectedYear);

            colIPrevYear.Caption = string.Format(Resource.BudgetCreation_YearCaption, SelectedYear - 1);
            colICurrentYear.Caption = string.Format(Resource.BudgetCreation_YearCaption, SelectedYear);

            colIPrevPlannedSalesV.Caption = string.Format(Resource.BudgetCreation_PlannedVCaption, SelectedYear - 1);
            colICurrentPlannedSalesV.Caption = string.Format(Resource.BudgetCreation_PlannedVCaption, SelectedYear);

            #endregion

            Filter();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        private void AdditionalInitialization()
        {
            barManager.Items.Add(barButtonBudgetInWork);
            barMain.LinksPersistInfo.Add(new DevExpress.XtraBars.LinkPersistInfo(barButtonBudgetInWork, true));
        }

        private void CopyColumn(DataTable dt, string src, string dst)
        {
            if (dt == null)
                return;
            if (dt.Columns.IndexOf(dst) == -1)
                dt.Columns.Add(dst, dt.Columns[src].DataType);
            foreach (DataRow row in dt.Rows)
                row[dst] = row[src];
        }

        private void Filter()
        {
            if (SelectedChannelId > 0)
            {
                colIChannelTypeId.FilterInfo = new ColumnFilterInfo(colIChannelTypeId, SelectedChannelId);
            }
            else
            {
                colIChannelTypeId.ClearFilter();
            }
            RecalculateTerritoryGrid();
        }

        private void PopulateChanges(List<int> checkedIds, List<int> uncheckedIds)
        {
            gridViewI.CloseEditor();
            DataTable dt = (DataTable) gridControlI.DataSource;

            foreach (DataRow row in dt.Rows)
            {
                if (row.Field<bool>(colIApproveInBudget.FieldName) != row.Field<bool>(colIApproveInBudgetOrig.FieldName))
                {
                    switch (row.Field<bool>(colIApproveInBudget.FieldName))
                    {
                        case false:
                            uncheckedIds.Add(row.Field<int>(colIID.FieldName));
                            break;
                        case true:
                            checkedIds.Add(row.Field<int>(colIID.FieldName));
                            break;
                    }
                }
            }
        }

        private void RecalculateTerritoryGrid()
        {
            DataTable dtSrc = (DataTable) gridControlI.DataSource;
            var r =
                (from d in dtSrc.AsEnumerable()
                 where (SelectedChannelId <= 0 || d.Field<int>(colIChannelTypeId.FieldName) == SelectedChannelId)
                 group d by d.Field<int>(colIRegionID.FieldName)
                 into g
                 select new object[]
                            {
                                g.Key,
                                g.Where(d => d.Field<bool>(colIForLE.FieldName) || d.Field<bool>(colIApproveInBudget.FieldName)).Sum(
                                    row => row.IsNull(colIPrevYear.FieldName) ? 0 : row.Field<decimal>(colIPrevYear.FieldName)),
                                g.Where(d => d.Field<bool>(colIApproveInBudget.FieldName)).Sum(
                                    row => row.IsNull(colICurrentYear.FieldName) ? 0 : row.Field<decimal>(colICurrentYear.FieldName)),
                                g.Where(d => d.Field<bool>(colIForLE.FieldName) || d.Field<bool>(colIApproveInBudget.FieldName)).Sum(
                                    row =>
                                    (!row.IsNull(colIPrevYear.FieldName) ? row.Field<decimal>(colIPrevYear.FieldName) : 0) +
                                    (!row.IsNull(colICurrentYear.FieldName) && row.Field<bool>(colIApproveInBudget.FieldName) ? row.Field<decimal>(colICurrentYear.FieldName) : 0)),
                                g.Where(d => d.Field<bool>(colIForLE.FieldName) || d.Field<bool>(colIApproveInBudget.FieldName)).Sum(
                                    row => row.IsNull(colIInvestmentsYTD.FieldName) ? 0 : row.Field<decimal>(colIInvestmentsYTD.FieldName)),
                                g.Where(d => d.Field<bool>(colIForLE.FieldName) || d.Field<bool>(colIApproveInBudget.FieldName)).Sum(
                                    row => row.IsNull(colILE.FieldName) ? 0 : row.Field<decimal>(colILE.FieldName))
                            }).ToDictionary(d => d[0], d => new {PrevSum = d[1], CurSum = d[2], SumApproved = d[3], InvYTD = d[4], LE = d[5]});

            DataTable dtDst = (DataTable) gridControlS.DataSource;
            foreach (DataRow row in dtDst.Rows)
            {
                if (r.ContainsKey(row.Field<int>(colSRegionID.FieldName)))
                {
                    var obj = r[row.Field<int>(colSRegionID.FieldName)];
                    row.SetField(colSPrevYear.FieldName, obj.PrevSum);
                    row.SetField(colSCurrentYear.FieldName, obj.CurSum);
                    row.SetField(colSApprovedSummary.FieldName, obj.SumApproved);
                    row.SetField(colSInvestmentsYTD.FieldName, obj.InvYTD);
                    row.SetField(colSLEBudget.FieldName, obj.LE);
                }
                else
                {
                    row.SetField(colSPrevYear.FieldName, 0);
                    row.SetField(colSCurrentYear.FieldName, 0);
                    row.SetField(colSApprovedSummary.FieldName, 0);
                    row.SetField(colSInvestmentsYTD.FieldName, 0);
                    row.SetField(colSLEBudget.FieldName, 0);
                }
            }
            gridControlS.RefreshDataSource();
        }

        private void Save(int typeId, int year)
        {
            List<int> checkedIds = new List<int>();
            List<int> uncheckedIds = new List<int>();

            PopulateChanges(checkedIds, uncheckedIds);

            BudgetParameters parameters = Parameters;
            parameters.Year = year;
            parameters.ActivityType = typeId;
            try
            {
                DataProvider.UpdateInitiatives(parameters, checkedIds.ToArray(), uncheckedIds.ToArray());
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException is SqlException)
                {
                    SqlException sex = (SqlException)ex.InnerException;
                    if (sex.Class == 16 && sex.Number == 50000)
                        XtraMessageBox.Show(sex.Message, Resource.ErrorBoxCaption, MessageBoxButtons.OK);
                }
                else
                    throw ex;
            }
        }

        #endregion

        #region Event Handling

        private void barButtonBudgetInWork_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            WaitManager.StartWait();

            Save(SelectedTypeId, SelectedYear);

            int[] initiativeIds =
                (from DataRow row in ((DataTable) gridControlI.DataSource).Rows where row.Field<bool>(colIApproveInBudget.FieldName) select row.Field<int>(colIID.FieldName)).
                    ToArray();
            try
            {
                DataProvider.MoveBudgetInWork(Parameters, initiativeIds);
            }
            catch (SqlException ex)
            {
                if (ex.InnerException != null && ex.InnerException is SqlException)
                {
                    SqlException sex = (SqlException)ex.InnerException;
                    if (sex.Class == 16 && sex.Number == 50000)
                        XtraMessageBox.Show(sex.Message, Resource.ErrorBoxCaption, MessageBoxButtons.OK);
                }
                else
                    throw ex;
            }

            Load();

            WaitManager.StopWait();
        }

        private void gridControl_Resized(object sender, EventArgs e)
        {
            GridControl grid = sender as GridControl;
            if (grid == null)
            {
                return;
            }

            GridView view = grid.DefaultView as GridView;
            if (view == null)
            {
                return;
            }
            //view.BestFitColumns();
            //int minWidth = view.VisibleColumns.Cast<GridColumn>().Sum(column => column.Width);
            //view.OptionsView.ColumnAutoWidth = grid.Width >= minWidth;
            //if (!view.OptionsView.ColumnAutoWidth)
            //{
            //    view.BestFitColumns();
            //}
            if (((GridViewInfo)view.GetViewInfo()).ColumnsInfo.ColumnsCount == 0)
            {
                grid.ForceInitialize();
            }
            GridViewInfo viewInfo = (GridViewInfo)view.GetViewInfo();
            if (viewInfo.ColumnsInfo.LastColumnInfo != null && viewInfo.ColumnsInfo.LastColumnInfo.Bounds.Right > viewInfo.ViewRects.ColumnPanel.Right)
            {
                view.OptionsView.ColumnAutoWidth = false;
                view.BestFitColumns();
            }
            else
            {
                view.OptionsView.ColumnAutoWidth = true;
            }
        }

        private void gridViewI_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (sender == null
                || e.Column != colIApproveInBudget
                || AccessLevel.CheckApproveRights(SelectedTypeId)
                || !(sender is GridView)
                || !(e.Cell is GridCellInfo)
                || !((e.Cell as GridCellInfo).ViewInfo is CheckEditViewInfo))
            {
                return;
            }
            CheckEditViewInfo checkEditViewInfo = (e.Cell as GridCellInfo).ViewInfo as CheckEditViewInfo;
            checkEditViewInfo.AllowOverridedState = true;
            checkEditViewInfo.OverridedState = ObjectState.Disabled;
            checkEditViewInfo.CalcViewInfo(e.Graphics);
        }

        private void gridViewI_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Column == colIApproveInBudget)
            {
                if (e.Column.ReadOnly || !AccessLevel.CheckApproveRights(SelectedTypeId))
                    return;

                var row = ((DataRowView) gridControlI.MainView.GetRow(e.RowHandle)).Row;
                if ((bool) row[Constants.FIELD_CAN_UPDATE])
                {
                    row[colIApproveInBudget.FieldName] = !(bool) row[colIApproveInBudget.FieldName];
                    RecalculateTerritoryGrid();
                }
                else
                {
                    XtraMessageBox.Show(row[Constants.FIELD_DESCRIPTION].ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void gridViewS_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView gv = (GridView) sender;
            if (e.Column.Equals(colSLEBudget) && ((decimal) e.CellValue) > ((decimal) gv.GetRowCellValue(e.RowHandle, colSApprovedSummary)))
                e.Graphics.FillRectangle(Constants.SoftRed, e.Bounds);
        }

        private void gridViewS_CustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            if (e.Column.Equals(colSTerritory))
            {
                e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
        }

        #endregion
    }
}