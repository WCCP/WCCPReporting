﻿namespace SoftServe.Reports.BudgetManagement.UserControls
{
  partial class MACOControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MACOControl));
        this.barButtonImportMACO = new DevExpress.XtraBars.BarButtonItem();
        this.gridControl = new DevExpress.XtraGrid.GridControl();
        this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSKU = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colBudget = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col1 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col2 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col3 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col4 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col5 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col6 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col7 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col8 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col9 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col10 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col11 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.col12 = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colLe = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colProductId = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colRegionId = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
        this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
        ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
        this.SuspendLayout();
        // 
        // imageCollection
        // 
        this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
        this.imageCollection.Images.SetKeyName(0, "save.png");
        // 
        // barButtonImportMACO
        // 
        this.barButtonImportMACO.Caption = "Импорт МАСО";
        this.barButtonImportMACO.Id = 8;
        this.barButtonImportMACO.Name = "barButtonImportMACO";
        this.barButtonImportMACO.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonImportMACO_ItemClick);
        // 
        // gridControl
        // 
        this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.gridControl.Location = new System.Drawing.Point(0, 34);
        this.gridControl.MainView = this.gridView;
        this.gridControl.MenuManager = this.barManager;
        this.gridControl.Name = "gridControl";
        this.gridControl.Size = new System.Drawing.Size(887, 498);
        this.gridControl.TabIndex = 4;
        this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
        this.gridControl.Load += new System.EventHandler(this.gridControl_Resized);
        this.gridControl.SizeChanged += new System.EventHandler(this.gridControl_Resized);
        this.gridControl.Resize += new System.EventHandler(this.gridControl_Resized);
        // 
        // gridView
        // 
        this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRegion,
            this.colSKU,
            this.colBudget,
            this.col1,
            this.col2,
            this.col3,
            this.col4,
            this.col5,
            this.col6,
            this.col7,
            this.col8,
            this.col9,
            this.col10,
            this.col11,
            this.col12,
            this.colLe,
            this.colProductId,
            this.colRegionId,
            this.colStatus});
        this.gridView.GridControl = this.gridControl;
        this.gridView.Name = "gridView";
        this.gridView.OptionsCustomization.AllowGroup = false;
        this.gridView.OptionsFilter.MaxCheckedListItemCount = 1000;
        this.gridView.OptionsMenu.EnableColumnMenu = false;
        this.gridView.OptionsMenu.EnableFooterMenu = false;
        this.gridView.OptionsMenu.EnableGroupPanelMenu = false;
        this.gridView.OptionsView.ShowGroupPanel = false;
        this.gridView.OptionsView.ShowIndicator = false;
        this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegion, DevExpress.Data.ColumnSortOrder.Ascending)});
        this.gridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView_ValidatingEditor);
        this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
        this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
        this.gridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView_CustomDrawCell);
        this.gridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
        // 
        // colRegion
        // 
        this.colRegion.AppearanceHeader.Options.UseTextOptions = true;
        this.colRegion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colRegion.Caption = "Регион";
        this.colRegion.FieldName = "Region_name";
        this.colRegion.MinWidth = 50;
        this.colRegion.Name = "colRegion";
        this.colRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colRegion.Visible = true;
        this.colRegion.VisibleIndex = 0;
        this.colRegion.Width = 70;
        // 
        // colSKU
        // 
        this.colSKU.AppearanceHeader.Options.UseTextOptions = true;
        this.colSKU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSKU.Caption = "СКЮ";
        this.colSKU.FieldName = "ProductName";
        this.colSKU.MinWidth = 50;
        this.colSKU.Name = "colSKU";
        this.colSKU.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colSKU.Visible = true;
        this.colSKU.VisibleIndex = 1;
        this.colSKU.Width = 149;
        // 
        // colBudget
        // 
        this.colBudget.AppearanceHeader.Options.UseTextOptions = true;
        this.colBudget.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colBudget.Caption = "Бюджет";
        this.colBudget.FieldName = "BUDGET";
        this.colBudget.MinWidth = 50;
        this.colBudget.Name = "colBudget";
        this.colBudget.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colBudget.Visible = true;
        this.colBudget.VisibleIndex = 2;
        this.colBudget.Width = 50;
        // 
        // col1
        // 
        this.col1.AppearanceHeader.Options.UseTextOptions = true;
        this.col1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col1.Caption = "Январь";
        this.col1.FieldName = "1";
        this.col1.MinWidth = 50;
        this.col1.Name = "col1";
        this.col1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col1.Visible = true;
        this.col1.VisibleIndex = 4;
        this.col1.Width = 50;
        // 
        // col2
        // 
        this.col2.AppearanceHeader.Options.UseTextOptions = true;
        this.col2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col2.Caption = "Февраль";
        this.col2.FieldName = "2";
        this.col2.MinWidth = 50;
        this.col2.Name = "col2";
        this.col2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col2.Visible = true;
        this.col2.VisibleIndex = 5;
        this.col2.Width = 50;
        // 
        // col3
        // 
        this.col3.AppearanceHeader.Options.UseTextOptions = true;
        this.col3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col3.Caption = "Март";
        this.col3.FieldName = "3";
        this.col3.MinWidth = 50;
        this.col3.Name = "col3";
        this.col3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col3.Visible = true;
        this.col3.VisibleIndex = 6;
        this.col3.Width = 50;
        // 
        // col4
        // 
        this.col4.AppearanceHeader.Options.UseTextOptions = true;
        this.col4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col4.Caption = "Апрель";
        this.col4.FieldName = "4";
        this.col4.MinWidth = 50;
        this.col4.Name = "col4";
        this.col4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col4.Visible = true;
        this.col4.VisibleIndex = 7;
        this.col4.Width = 50;
        // 
        // col5
        // 
        this.col5.AppearanceHeader.Options.UseTextOptions = true;
        this.col5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col5.Caption = "Май";
        this.col5.FieldName = "5";
        this.col5.MinWidth = 50;
        this.col5.Name = "col5";
        this.col5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col5.Visible = true;
        this.col5.VisibleIndex = 8;
        this.col5.Width = 50;
        // 
        // col6
        // 
        this.col6.AppearanceHeader.Options.UseTextOptions = true;
        this.col6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col6.Caption = "Июнь";
        this.col6.FieldName = "6";
        this.col6.MinWidth = 50;
        this.col6.Name = "col6";
        this.col6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col6.Visible = true;
        this.col6.VisibleIndex = 9;
        this.col6.Width = 50;
        // 
        // col7
        // 
        this.col7.AppearanceHeader.Options.UseTextOptions = true;
        this.col7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col7.Caption = "Июль";
        this.col7.FieldName = "7";
        this.col7.MinWidth = 50;
        this.col7.Name = "col7";
        this.col7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col7.Visible = true;
        this.col7.VisibleIndex = 10;
        this.col7.Width = 50;
        // 
        // col8
        // 
        this.col8.AppearanceHeader.Options.UseTextOptions = true;
        this.col8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col8.Caption = "Август";
        this.col8.FieldName = "8";
        this.col8.MinWidth = 50;
        this.col8.Name = "col8";
        this.col8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col8.Visible = true;
        this.col8.VisibleIndex = 11;
        this.col8.Width = 50;
        // 
        // col9
        // 
        this.col9.AppearanceHeader.Options.UseTextOptions = true;
        this.col9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col9.Caption = "Сентябрь";
        this.col9.FieldName = "9";
        this.col9.MinWidth = 50;
        this.col9.Name = "col9";
        this.col9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col9.Visible = true;
        this.col9.VisibleIndex = 12;
        this.col9.Width = 50;
        // 
        // col10
        // 
        this.col10.AppearanceHeader.Options.UseTextOptions = true;
        this.col10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col10.Caption = "Октябрь";
        this.col10.FieldName = "10";
        this.col10.MinWidth = 50;
        this.col10.Name = "col10";
        this.col10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col10.Visible = true;
        this.col10.VisibleIndex = 13;
        this.col10.Width = 50;
        // 
        // col11
        // 
        this.col11.AppearanceHeader.Options.UseTextOptions = true;
        this.col11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col11.Caption = "Ноябрь";
        this.col11.FieldName = "11";
        this.col11.MinWidth = 50;
        this.col11.Name = "col11";
        this.col11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col11.Visible = true;
        this.col11.VisibleIndex = 14;
        this.col11.Width = 50;
        // 
        // col12
        // 
        this.col12.AppearanceHeader.Options.UseTextOptions = true;
        this.col12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.col12.Caption = "Декабрь";
        this.col12.FieldName = "12";
        this.col12.MinWidth = 50;
        this.col12.Name = "col12";
        this.col12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.col12.Visible = true;
        this.col12.VisibleIndex = 15;
        this.col12.Width = 50;
        // 
        // colLe
        // 
        this.colLe.AppearanceHeader.Options.UseTextOptions = true;
        this.colLe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colLe.Caption = "LE";
        this.colLe.FieldName = "Total";
        this.colLe.MinWidth = 50;
        this.colLe.Name = "colLe";
        this.colLe.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        this.colLe.Visible = true;
        this.colLe.VisibleIndex = 3;
        this.colLe.Width = 50;
        // 
        // colProductId
        // 
        this.colProductId.Caption = "Product_Id";
        this.colProductId.FieldName = "Product_Id";
        this.colProductId.MinWidth = 50;
        this.colProductId.Name = "colProductId";
        // 
        // colRegionId
        // 
        this.colRegionId.Caption = "Region_id";
        this.colRegionId.FieldName = "Region_id";
        this.colRegionId.MinWidth = 50;
        this.colRegionId.Name = "colRegionId";
        // 
        // colStatus
        // 
        this.colStatus.Caption = "Status";
        this.colStatus.FieldName = "Status";
        this.colStatus.MinWidth = 50;
        this.colStatus.Name = "colStatus";
        // 
        // openFileDialog
        // 
        this.openFileDialog.Filter = "Excel files|*.xls;*.xlsx";
        this.openFileDialog.RestoreDirectory = true;
        this.openFileDialog.Title = "Выберите файл для импорта";
        // 
        // MACOControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this.gridControl);
        this.Name = "MACOControl";
        this.Size = new System.Drawing.Size(887, 532);
        this.Controls.SetChildIndex(this.gridControl, 0);
        ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarButtonItem barButtonImportMACO;
    private DevExpress.XtraGrid.GridControl gridControl;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView;
    private DevExpress.XtraGrid.Columns.GridColumn colRegion;
    private DevExpress.XtraGrid.Columns.GridColumn colSKU;
    private DevExpress.XtraGrid.Columns.GridColumn colBudget;
    private DevExpress.XtraGrid.Columns.GridColumn col1;
    private DevExpress.XtraGrid.Columns.GridColumn col2;
    private DevExpress.XtraGrid.Columns.GridColumn col3;
    private DevExpress.XtraGrid.Columns.GridColumn col4;
    private DevExpress.XtraGrid.Columns.GridColumn col5;
    private DevExpress.XtraGrid.Columns.GridColumn col6;
    private DevExpress.XtraGrid.Columns.GridColumn col7;
    private DevExpress.XtraGrid.Columns.GridColumn col8;
    private DevExpress.XtraGrid.Columns.GridColumn col9;
    private DevExpress.XtraGrid.Columns.GridColumn col10;
    private DevExpress.XtraGrid.Columns.GridColumn col11;
    private DevExpress.XtraGrid.Columns.GridColumn col12;
    private System.Windows.Forms.OpenFileDialog openFileDialog;
    private DevExpress.XtraGrid.Columns.GridColumn colProductId;
    private DevExpress.XtraGrid.Columns.GridColumn colRegionId;
    private DevExpress.XtraGrid.Columns.GridColumn colStatus;
    private DevExpress.XtraGrid.Columns.GridColumn colLe;
  }
}
