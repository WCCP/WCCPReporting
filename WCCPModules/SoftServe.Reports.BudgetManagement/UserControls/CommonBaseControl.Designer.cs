﻿namespace SoftServe.Reports.BudgetManagement.UserControls
{
  partial class CommonBaseControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommonBaseControl));
            this.imageCollection = new DevExpress.Utils.ImageCollection();
            this.barManager = new DevExpress.XtraBars.BarManager();
            this.barMain = new DevExpress.XtraBars.Bar();
            this.barButtonSave = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemYear = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemLookUpEditYear = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barEditItemType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemLookUpEditType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barEditItemChannel = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemLookUpEditChannel = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barEditItemDate = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // imageCollection
            // 
            this.imageCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "save.png");
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMain});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imageCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonSave,
            this.barEditItemYear,
            this.barEditItemType,
            this.barEditItemChannel,
            this.barEditItemDate});
            this.barManager.MainMenu = this.barMain;
            this.barManager.MaxItemId = 10;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEditYear,
            this.repositoryItemLookUpEditType,
            this.repositoryItemLookUpEditChannel,
            this.repositoryItemDateEdit});
            // 
            // barMain
            // 
            this.barMain.BarName = "Main menu";
            this.barMain.DockCol = 0;
            this.barMain.DockRow = 0;
            this.barMain.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemYear, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemType),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemChannel),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.barEditItemDate, "", true, true, true, 92)});
            this.barMain.OptionsBar.AllowQuickCustomization = false;
            this.barMain.OptionsBar.DisableClose = true;
            this.barMain.OptionsBar.DisableCustomization = true;
            this.barMain.OptionsBar.DrawDragBorder = false;
            this.barMain.OptionsBar.UseWholeRow = true;
            this.barMain.Text = "Main menu";
            // 
            // barButtonSave
            // 
            this.barButtonSave.Caption = "Сохранить бюджет";
            this.barButtonSave.Id = 0;
            this.barButtonSave.ImageIndex = 0;
            this.barButtonSave.Name = "barButtonSave";
            this.barButtonSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonSave_ItemClick);
            // 
            // barEditItemYear
            // 
            this.barEditItemYear.Caption = "Год";
            this.barEditItemYear.Edit = this.repositoryItemLookUpEditYear;
            this.barEditItemYear.Id = 6;
            this.barEditItemYear.Name = "barEditItemYear";
            this.barEditItemYear.Width = 75;
            this.barEditItemYear.EditValueChanged += new System.EventHandler(this.barEditItemYear_EditValueChanged);
            // 
            // repositoryItemLookUpEditYear
            // 
            this.repositoryItemLookUpEditYear.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemLookUpEditYear.AutoHeight = false;
            this.repositoryItemLookUpEditYear.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditYear.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("cYear", "Text")});
            this.repositoryItemLookUpEditYear.DisplayMember = "cYear";
            this.repositoryItemLookUpEditYear.Name = "repositoryItemLookUpEditYear";
            this.repositoryItemLookUpEditYear.NullText = "<Не задано>";
            this.repositoryItemLookUpEditYear.ShowFooter = false;
            this.repositoryItemLookUpEditYear.ShowHeader = false;
            this.repositoryItemLookUpEditYear.ValueMember = "cYear";
            // 
            // barEditItemType
            // 
            this.barEditItemType.Caption = "Тип";
            this.barEditItemType.Edit = this.repositoryItemLookUpEditType;
            this.barEditItemType.Id = 7;
            this.barEditItemType.Name = "barEditItemType";
            this.barEditItemType.Width = 175;
            this.barEditItemType.EditValueChanged += new System.EventHandler(this.barEditItemType_EditValueChanged);
            // 
            // repositoryItemLookUpEditType
            // 
            this.repositoryItemLookUpEditType.AutoHeight = false;
            this.repositoryItemLookUpEditType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionType_ID", "ActionType_ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionType_Name", "ActionType_Name")});
            this.repositoryItemLookUpEditType.DisplayMember = "ActionType_Name";
            this.repositoryItemLookUpEditType.Name = "repositoryItemLookUpEditType";
            this.repositoryItemLookUpEditType.NullText = "<Не задано>";
            this.repositoryItemLookUpEditType.ShowFooter = false;
            this.repositoryItemLookUpEditType.ShowHeader = false;
            this.repositoryItemLookUpEditType.ValueMember = "ActionType_ID";
            // 
            // barEditItemChannel
            // 
            this.barEditItemChannel.Caption = "Канал";
            this.barEditItemChannel.Edit = this.repositoryItemLookUpEditChannel;
            this.barEditItemChannel.Id = 8;
            this.barEditItemChannel.Name = "barEditItemChannel";
            this.barEditItemChannel.Width = 100;
            this.barEditItemChannel.EditValueChanged += new System.EventHandler(this.barEditItemChannel_EditValueChanged);
            // 
            // repositoryItemLookUpEditChannel
            // 
            this.repositoryItemLookUpEditChannel.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemLookUpEditChannel.AutoHeight = false;
            this.repositoryItemLookUpEditChannel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditChannel.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType_id", "ChanelType_id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "ChanelType")});
            this.repositoryItemLookUpEditChannel.DisplayMember = "ChanelType";
            this.repositoryItemLookUpEditChannel.Name = "repositoryItemLookUpEditChannel";
            this.repositoryItemLookUpEditChannel.NullText = "Все каналы";
            this.repositoryItemLookUpEditChannel.ShowFooter = false;
            this.repositoryItemLookUpEditChannel.ShowHeader = false;
            this.repositoryItemLookUpEditChannel.ValueMember = "ChanelType_id";
            // 
            // barEditItemDate
            // 
            this.barEditItemDate.Caption = "Дата";
            this.barEditItemDate.Edit = this.repositoryItemDateEdit;
            this.barEditItemDate.Id = 9;
            this.barEditItemDate.Name = "barEditItemDate";
            this.barEditItemDate.EditValueChanged += new System.EventHandler(this.barEditItemDate_EditValueChanged);
            // 
            // repositoryItemDateEdit
            // 
            this.repositoryItemDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEdit.AutoHeight = false;
            this.repositoryItemDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit.Name = "repositoryItemDateEdit";
            this.repositoryItemDateEdit.ValidateOnEnterKey = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(672, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 468);
            this.barDockControlBottom.Size = new System.Drawing.Size(672, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 436);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(672, 32);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 436);
            // 
            // CommonBaseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "CommonBaseControl";
            this.Size = new System.Drawing.Size(672, 468);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    protected DevExpress.Utils.ImageCollection imageCollection;
    protected DevExpress.XtraBars.BarManager barManager;
    protected DevExpress.XtraBars.Bar barMain;
    protected DevExpress.XtraBars.BarDockControl barDockControlTop;
    protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
    protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
    protected DevExpress.XtraBars.BarDockControl barDockControlRight;
    protected DevExpress.XtraBars.BarButtonItem barButtonSave;
    protected DevExpress.XtraBars.BarEditItem barEditItemYear;
    protected DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditYear;
    protected DevExpress.XtraBars.BarEditItem barEditItemType;
    protected DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditType;
    protected DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditChannel;
    protected DevExpress.XtraBars.BarEditItem barEditItemChannel;
    private DevExpress.XtraBars.BarEditItem barEditItemDate;
    private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit;
  }
}
