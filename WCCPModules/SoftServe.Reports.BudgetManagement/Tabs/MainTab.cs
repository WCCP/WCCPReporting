﻿using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using SoftServe.Reports.BudgetManagement.UserControls;
using SoftServe.Reports.BudgetManagement.Utility;

namespace SoftServe.Reports.BudgetManagement.Tabs
{
  /// <summary>
  /// 
  /// </summary>
  public class MainTab : CommonBaseTab, IPrint
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ClientListTab"/> class.
    /// </summary>
    /// <param name="parent">The parent.</param>
    public MainTab(BaseReportUserControl bruc, TabType type, ActionsCorrelator correlator)
    {
      if (!this.DesignMode && bruc != null)
      {
        switch (type)
        {
          case TabType.PlanOrFact:
            Init(bruc, new PlanFactControl(this, correlator), Resource.Tab_PlanOrFact);
            break;
          case TabType.BudgetCreation:
            Init(bruc, new BudgetCreationControl(this, correlator), Resource.Tab_BudgetCreation);
            break;
          case TabType.MACO:
            Init(bruc, new MACOControl(this, correlator), Resource.Tab_MACO);
            break;
        }  

        ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;

        Bruc.ExportClick += new BaseReportUserControl.ExportMenuClickHandler(parent_ExportClick);
      }
    }

    /// <summary>
    /// Gets the view.
    /// </summary>
    /// <value>The view.</value>
    public CommonBaseControl Content
    {
      get
      {
        return base.UserControl as CommonBaseControl;
      }
    }

    /// <summary>
    /// Prepare container of elements for exporting
    /// </summary>
    /// <returns></returns>
    public CompositeLink PrepareCompositeLink()
    {
      CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
      foreach (PrintableComponentLink link in Content.PrintableComponentLinks)
      {
        compositeLink.Links.Add(link);
      }
      compositeLink.CreateDocument();
      return compositeLink;
    }

    /// <summary>
    /// Parent_s the export click.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="selectedPage">The selected page.</param>
    /// <param name="type">The type.</param>
    /// <param name="fName">Name of the f.</param>
    private void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
    {
      if (this == selectedPage)
      {
        switch (type)
        {
          case ExportToType.Html:
            PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
            break;
          case ExportToType.Mht:
            PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
            break;
          case ExportToType.Pdf:
            PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions() { Compressed = true });
            break;
          case ExportToType.Rtf:
            PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
            break;
          case ExportToType.Txt:
            PrepareCompositeLink().PrintingSystem.ExportToText(fName);
            break;
          case ExportToType.Xls:
            PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = Text });
            break;
          case ExportToType.Xlsx:
            PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
            break;
          case ExportToType.Bmp:
            PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
            break;
          case ExportToType.Csv:
            PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
            break;
        }
      }
    }
  }
}
