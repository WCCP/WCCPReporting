﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SoftServe.Reports.BudgetManagement.Utility.Exceptions
{
  class WrongFormatException: UserException
  {
    public WrongFormatException()
    {
      
    }
    public WrongFormatException(string message)
      : base(message)
    {
      
    }
    public WrongFormatException(string message, Exception innerException)
      : base(message, innerException)
    {
      
    }
    protected WrongFormatException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
      : base(info, context)
    {
      
    }
    public override string ToString()
    {
      return String.Format(SoftServe.Reports.BudgetManagement.Resource.Exception_WrongFormat, Message);
    }
  }
}
