﻿using System;
using System.Drawing;
using Logica.Reports.Common;

namespace SoftServe.Reports.BudgetManagement.Utility
{
  /// <summary>
  /// 
  /// </summary>
  public static class Constants
  {
    public static Brush SoftRed = new SolidBrush(Color.FromArgb(255, 192, 192));
    public static Brush SoftYellow = new SolidBrush(Color.FromArgb(255, 255, 224));
    public static Pen CellBorder = SystemPens.ActiveBorder; //Pens.Black; //

    public const string LIST_DELIMITER = ",";
    public const string PRODUCT_KEY_FORMAT = "{0} {1} {2}";
    public const string PRODUCT_DISPLAY_FORMAT = "{0} {1} {2}";

    public const string SP_GET_ACCESS_LEVEL = "spDW_BM_GetAccessLevel";
    public const string SP_GET_USER_LEVEL = "spDW_GetUserLevel";
    public const string SP_GET_ACTIVITY_LIST = "spDW_BM_GetActivityList";
    public const string SP_GET_YEARS_LIST = "spDW_BM_GetYear";
    public const string SP_GET_YEARS_LIST_PARAM_1 = "UserLogin";
    public const string SP_GET_CHANNELS_LIST = "spDW_ChanelTypeList";
    public const string SP_GET_CHANNELS_LIST_FLD_ID = "ChanelType_id";
    public const string SP_GET_CHANNELS_LIST_FLD_NAME = "ChanelType";

    #region Tab plan or fact
    public const string SP_GET_AREA_AND_STAFF_TREE = "spDW_BM_GetAreaAndStaffTree";
    public const string SP_GET_AREA_AND_STAFF_TREE_PARAM_1 = "Activity_Type";
    public const string SP_GET_AREA_AND_STAFF_TREE_PARAM_2 = "Year";
    public const string SP_GET_AREA_AND_STAFF_TREE_PARAM_3 = "UserLogin";
    public const string SP_GET_AREA_AND_STAFF_TREE_PARAM_4 = "Chanel_ID";
    public const string SP_GET_AREA_AND_STAFF_TREE_PARAM_5 = "CurDate";
    #endregion

    #region Tab budget creation
    public const string SP_GET_INITIATIVES_LIST = "spDW_BM_GetInitiativesList";
    public const string SP_GET_INITIATIVES_LIST_PARAM_1 = "Activity_Type";
    public const string SP_GET_INITIATIVES_LIST_PARAM_2 = "Year";
    public const string SP_GET_INITIATIVES_LIST_PARAM_3 = "UserLogin";
    public const string SP_GET_INITIATIVES_LIST_PARAM_4 = "CurDate";
      
    public const string SP_GET_TERRITORY_LIST = "spDW_BM_GetTerritoryList";
    public const string SP_GET_TERRITORY_LIST_PARAM_1 = "UserLogin";
    public const string SP_GET_TERRITORY_LIST_FLD_REGION_ID = "REGION_ID";
    public const string SP_GET_TERRITORY_LIST_FLD_REGION_NAME = "REGION_NAME";

    public const string SP_MOVE_BUDGET_IN_WORK = "spDW_BM_SetBudgetInWork";
    public const string SP_MOVE_BUDGET_IN_WORK_PARAM_1 = "Year";
    public const string SP_MOVE_BUDGET_IN_WORK_PARAM_2 = "Activity_Type";
    public const string SP_MOVE_BUDGET_IN_WORK_PARAM_3 = "ID_LIST";

    public const string SP_UPDATE_INITIATIVES = "spDW_BM_SetIsBudget";
    public const string SP_UPDATE_INITIATIVES_PARAM_1 = "Year";
    public const string SP_UPDATE_INITIATIVES_PARAM_2 = "Activity_Type";
    public const string SP_UPDATE_INITIATIVES_PARAM_3 = "ID_ON_LIST";
    public const string SP_UPDATE_INITIATIVES_PARAM_4 = "ID_OFF_LIST";

    public const string FIELD_CAN_UPDATE = "canUpdate";
    public const string FIELD_DESCRIPTION = "Description";

    #endregion

    #region Tab MACO
    public const string SP_GET_REGION_MAPPING = "spDW_BM_GetRegionMapping";

    public const string SP_GET_SKU_MAPPING = "spDW_BM_GetSKUMapping";

    public const string SP_GET_MACO = "spDW_BM_GetMACO";
    public const string SP_GET_MACO_PARAM_1 = "Dt_Year";

    public const string SP_UPDATE_MACO = "spDW_BM_UpdateMACO";
    public const string SP_UPDATE_MACO_PARAM_1 = "LE";
    public const string SP_UPDATE_MACO_PARAM_2 = "BUDGET";
    public const string SP_UPDATE_MACO_PARAM_3 = "M1";
    public const string SP_UPDATE_MACO_PARAM_4 = "M2";
    public const string SP_UPDATE_MACO_PARAM_5 = "M3";
    public const string SP_UPDATE_MACO_PARAM_6 = "M4";
    public const string SP_UPDATE_MACO_PARAM_7 = "M5";
    public const string SP_UPDATE_MACO_PARAM_8 = "M6";
    public const string SP_UPDATE_MACO_PARAM_9 = "M7";
    public const string SP_UPDATE_MACO_PARAM_10 = "M8";
    public const string SP_UPDATE_MACO_PARAM_11 = "M9";
    public const string SP_UPDATE_MACO_PARAM_12 = "M10";
    public const string SP_UPDATE_MACO_PARAM_13 = "M11";
    public const string SP_UPDATE_MACO_PARAM_14 = "M12";
    public const string SP_UPDATE_MACO_PARAM_15 = "Region_id";
    public const string SP_UPDATE_MACO_PARAM_16 = "Product_Id";
    public const string SP_UPDATE_MACO_PARAM_17 = "Dt_Year";
    #endregion

  }

  /// <summary>
  /// 
  /// </summary>
  public enum AccessLevelType
  {
      // should be set as bit flags (logical |)
      Unknown = 0,
      ReadOnly = 1,
      AllowApproveContract = 2,
      AllowApproveActivity = 4,
      AllowApprovePricing = 8,
      AllowApproveM3 = 16,
      AllowApproveM4 = 32,
      AllowApproveInWork = 64,
      AllowMacoImport = 128
      /// <summary>
      /// 
      /// </summary>
      //XXXXX = 256, // next one 512 and so on..
  }

  /// <summary>
  /// 
  /// </summary>
  public enum UserLevelType
  {
      Unknown = 0,
      M2 = 2,
      M3 = 3,
      M4 = 4,
      M5 = 5
  }

  public enum ActivityType
  {
      Contract = 0, // Констракты
      Pricing = 1,  // Ценовые активности
      Activity = 2, // ТМ Активности
      M3 = 3,       // М3
      M4 = 4        // М4
  }
}