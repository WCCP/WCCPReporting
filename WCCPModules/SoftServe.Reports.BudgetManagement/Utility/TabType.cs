using System;

namespace SoftServe.Reports.BudgetManagement.Utility
{
  public enum TabType
  {
    PlanOrFact,
    BudgetCreation,
    MACO
  }
}
