﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.BudgetManagement.Utility
{
    public class BudgetParameters
    {
        public int Year { get; set; }
        public int ActivityType { get; set; }
        public int Channel { get; set; }
        public DateTime CurrentDate { get; set; }
    }
}
