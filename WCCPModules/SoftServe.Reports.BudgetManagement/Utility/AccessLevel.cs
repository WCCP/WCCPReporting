﻿using System;

namespace SoftServe.Reports.BudgetManagement.Utility
{
    /// <summary>
    /// 
    /// </summary>
    internal static class AccessLevel
    {
        internal static int CurrentAccessLevel { get; set; }
        internal static UserLevelType CurrentUserLevel { get; set; }

        /// <summary>
        /// Checks the access level.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        internal static bool CheckAccessLevel(AccessLevelType type)
        {
            return ((int)CurrentAccessLevel & (int)type) > 0;
        }

        internal static bool CheckApproveRights(int activityType)
        {
            switch ((ActivityType)activityType)
            {
                case ActivityType.Contract:
                    return CheckAccessLevel(AccessLevelType.AllowApproveContract);
                case ActivityType.Pricing:
                    return CheckAccessLevel(AccessLevelType.AllowApprovePricing);
                case ActivityType.Activity:
                    return CheckAccessLevel(AccessLevelType.AllowApproveActivity);
                case ActivityType.M3:
                    return CheckAccessLevel(AccessLevelType.AllowApproveM3);
                case ActivityType.M4:
                    return CheckAccessLevel(AccessLevelType.AllowApproveM4);
                default:
                    return false;
            }
        }
    }
}
