﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices;
using SoftServe.Reports.BudgetManagement.DataAccess.DataSets;
using SoftServe.Reports.BudgetManagement.Utility.Exceptions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Linq;

namespace SoftServe.Reports.BudgetManagement.Utility
{
    class Utility
    {
        static SpreadsheetDocument _excelFile;
        static WorkbookPart _workbook;

        public static int ReadFile(string fileName, ImportedData res)
        {
            int year;
            SheetData lSheetData = null;

            try
            {
                lSheetData = GetWorkSheetData(fileName);
                List<Row> rows = lSheetData.Elements<Row>().ToList();

                int firstColumnIndex = ConvertColumnNameToNumber("A");//0
                int lastColumnIndex = ConvertColumnNameToNumber("R");//23

                int budgetColumnIndex = ConvertColumnNameToNumber("E");//4

                ImportedData.DataDataTable result = res.Data;

                year = ReadYear(rows);

                CheckHeader(rows, firstColumnIndex, lastColumnIndex, budgetColumnIndex, result);

                ReadBody(rows, firstColumnIndex, lastColumnIndex, budgetColumnIndex, result);
            }
            finally
            {
                ReleaseObject(lSheetData);
                FreeResourses();
            }

            return year;
        }

        private static int ReadYear(List<Row> rows)
        {
            int year = 0;
            int yearColumnIndex = ConvertColumnNameToNumber("E");//4

            List<string> fileHeaders = GetRowData(rows[0]);

            if (string.IsNullOrEmpty(fileHeaders[yearColumnIndex]))
            {
                throw new WrongFormatException(Resource.ImportError_FirstRow);
            }
            try
            {
                year = Convert.ToInt32(fileHeaders[yearColumnIndex].Trim().Substring(0, 4), CultureInfo.InvariantCulture);
                if (year < 1900)
                    throw new Exception();
            }
            catch (Exception)
            {
                throw new WrongFormatException(Resource.ImportError_WrongYear);
            }

            return year;
        }

        /// <summary>
        /// Reads first row. It should contain only budget header.
        /// </summary>
        private static void CheckHeader(List<Row> rows, int firstColumnIndex, int lastColumnIndex, int budgetColumnIndex, ImportedData.DataDataTable result)
        {
            List<string> headers = GetRowData(rows[1]);

            for (int i = firstColumnIndex; i <= lastColumnIndex; i++)
            {
                bool isColumnNameValid = true;

                if (i != budgetColumnIndex && !string.IsNullOrEmpty(headers[i]))
                {
                    isColumnNameValid = result.Columns[i].ColumnName.ToUpper().Trim().Equals(headers[i].ToUpper());
                }
                else
                    if (i != budgetColumnIndex)
                    {
                        isColumnNameValid = false;
                    }

                if (!isColumnNameValid)
                    throw new WrongFormatException(String.Format(Resource.ImportError_SecondRow, result.Columns[i].ColumnName, headers[i] ?? Resource.EmptyCell));
            }
        }

        /// <summary>
        /// Reading rest of rows. They should contain meaningfull data.
        /// </summary>
        private static void ReadBody(List<Row> rows, int firstColumnIndex, int lastColumnIndex, int budgetColumnIndex, ImportedData.DataDataTable result)
        {
            for (int i = 2; i < rows.Count; i++)
                {
                    List<string> curRow = GetRowData(rows[i]);

                    if (IsRowEmpty(curRow) || string.IsNullOrEmpty(curRow[budgetColumnIndex]))
                        continue;

                    List<object> values = new List<object>();

                    values.Add(curRow[0]);//regname
                    values.Add(curRow[1]);//packtype
                    values.Add(Convert.ToDouble(curRow[2], CultureInfo.InvariantCulture));//packsize
                    values.Add(curRow[3]);//specname
                                    
                    for(int j = budgetColumnIndex; j <= lastColumnIndex; j++)
                    {
                        values.Add(Convert.ToDouble(curRow[j], CultureInfo.InvariantCulture));
                    }
                    result.Rows.Add(values.ToArray());
                }
        }

        private static bool MatchRow(Dictionary<string, KeyValuePair<int, string>> mapping, DataRow row, string value, string idColName, string nameColName)
        {
            if (mapping.ContainsKey(value))
            {
                KeyValuePair<int, string> d = mapping[value];
                row[idColName] = d.Key;
                row[nameColName] = d.Value;
                return true;
            }
            return false;
        }

        public static void MatchRecords(ImportedData initialData, Dictionary<string, KeyValuePair<int, string>> regionMapping, Dictionary<string, KeyValuePair<int, string>> skuMapping)
        {
            foreach (ImportedData.DataRow row in initialData.Data.Rows)
            {
                string regName = row.REGNAME.ToUpper();
                MatchRow(regionMapping,
                  row,
                  regName,
                  initialData.Data.Region_idColumn.ColumnName,
                  initialData.Data.Region_nameColumn.ColumnName);

                string prodName = string.Format(Constants.PRODUCT_KEY_FORMAT,
                  row.SPECNAME.Trim().ToUpper(),
                  row.PACKSIZE.ToString().Trim().ToUpper(),
                  row.PACKTYPE.Trim().ToUpper()
                  );

                MatchRow(skuMapping,
                  row,
                  prodName,
                  initialData.Data.Product_IdColumn.ColumnName,
                  initialData.Data.ProductNameColumn.ColumnName);
            }
        }


        private static SheetData GetWorkSheetData(string fileName)
        {
            _excelFile = SpreadsheetDocument.Open(fileName, false);
            _workbook = _excelFile.WorkbookPart;

            Sheet lSheet = _workbook.Workbook.Descendants<Sheet>().First(s => s.State == null);

            WorksheetPart lWorkSheet = (WorksheetPart)(_workbook.GetPartById(lSheet.Id));
            SheetData lSheetData = lWorkSheet.Worksheet.Elements<SheetData>().First();

            return lSheetData;
        }

        private static string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private static int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                // ASCII 'A' = 65
                int current = i == 0 ? letter - 65 : letter - 64;
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        /// <summary>
        /// Handles row reading cell by cell, cops with gaps in data
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private static IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)//empty cells
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private static List<string> GetRowData(Row row)
        {
            var dataRow = new List<string>();

            var cellEnumerator = GetExcelCellEnumerator(row);
            while (cellEnumerator.MoveNext())
            {
                var cell = cellEnumerator.Current;
                var text = ReadExcelCell(cell, _workbook).Trim();

                dataRow.Add(text);
            }

            return dataRow;
        }

        private static string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }

        private static void FreeResourses()
        {
            if (_workbook != null)
            {
                ReleaseObject(_workbook);
            }

            _excelFile.Close();
            ReleaseObject(_excelFile);
        }

        /// <summary>
        /// Released resources
        /// </summary>
        /// <param name="obj">Object to be released</param>
        private static void ReleaseObject(object obj)
        {
            if (obj != null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private static bool IsRowEmpty(List<string> row)
        {
            bool isNotEmpty = row.Any(v => !string.IsNullOrEmpty(v));

            return !isNotEmpty;
        }
    }
}
