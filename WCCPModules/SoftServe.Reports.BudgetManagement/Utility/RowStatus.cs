using System;

namespace SoftServe.Reports.BudgetManagement.Utility
{
  public enum RowStatus
  {
    New,
    Unchanged,
    Updated,
    Deleted
  }
}
