﻿using DevExpress.XtraTreeList;
using Logica.Reports.BaseReportControl.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SoftServe.Reports.BudgetManagement.Utility
{
  public delegate bool DrawFunction
  (
    TreeList sender, 
    CustomDrawNodeCellEventArgs e, 
    object cellValue, 
    Rectangle bounds
  );

  public class ReportDrawers
  {
    #region Private fields
    private string activeDrawer = "";
    private readonly Dictionary<string, DrawFunction> drawers = new Dictionary<string, DrawFunction>();
    private Brush mainBrush;
    private Brush textBrush;
    

    #endregion

    #region Constructor
    public ReportDrawers()
    {
      drawers.Add("UNSIGNED", DrawUnsigned);
      drawers.Add("SIGNED", DrawSigned);
      drawers.Add("DEVIATION", DrawDeviator);
      drawers.Add("PROGRESS", DrawProgress);
    }
    #endregion

    #region IDrawerFactory Members
    public DrawFunction GetDrawer(string drawerName)
    {

      activeDrawer = drawerName;
      mainBrush = Brushes.Black;

      string[] drawerInfo = activeDrawer.Split('_');
      if (drawerInfo.Length > 1)
      {
        mainBrush = new SolidBrush((Color)new ColorConverter().ConvertFromString(drawerInfo[1]));
        if (drawerInfo.Length > 2)
        {
          textBrush = new SolidBrush((Color)new ColorConverter().ConvertFromString(drawerInfo[2]));
        }
        string drawer = drawerInfo[0].ToUpper();
        if (drawers.ContainsKey(drawer))
          return drawers[drawer];
      }
      return null;
    }
    #endregion

    #region Drawers
    public bool DrawUnsigned(TreeList sender, CustomDrawNodeCellEventArgs e, object columnValue, Rectangle bounds)
    {
      string drawString = columnValue.ToString();
      if (columnValue.GetType() == typeof(decimal))
        drawString = ((decimal)columnValue).ToString("N2");

      e.Graphics.DrawString(drawString,
        e.Appearance.Font,
        mainBrush,
        bounds,
        new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
      return true;
    }
    public bool DrawSigned(TreeList sender, CustomDrawNodeCellEventArgs e, object columnValue, Rectangle bounds)
    {
      string text = String.Format("{0}{1}", (Convert.ToDecimal(columnValue) > 0) ? "+" : "", ((decimal)columnValue).ToString("N2"));
      e.Graphics.DrawString(text,
        e.Appearance.Font,
        mainBrush,
        bounds,
        new StringFormat { Alignment = StringAlignment.Far, LineAlignment = StringAlignment.Center });
      return true;
    }
    public bool DrawDeviator(TreeList sender, CustomDrawNodeCellEventArgs e, object columnValue, Rectangle bounds)
    {
      const int stringWidth = 100;

      //DataTable dt = (DataTable)sender.DataSource;
      //decimal max = (from d in dt.AsEnumerable() select Math.Abs(ConvertEx.ToDecimal(d.Field<int>(e.Column.FieldName)))).Max();
      //if (max < 100)
      //{
      double max = 100;
      //}
      double cellValue = Convert.ToDouble(Convert.ToDouble(columnValue));
      if (Math.Abs(cellValue) > max)
        cellValue = max*Math.Sign(cellValue);

      int poligonStart = bounds.X + Convert.ToInt32((bounds.Width - stringWidth) / 2);
      int width = Convert.ToInt32((bounds.Width - stringWidth) * cellValue / Convert.ToDouble(2 * max));
      e.Graphics.FillPolygon(mainBrush, new[]
      {
        new Point(poligonStart, bounds.Y),
        new Point(poligonStart + ((width==0)? -1: width), bounds.Y),
        new Point(poligonStart + ((width==0)? -1: width), bounds.Bottom),
        new Point(poligonStart, bounds.Bottom)
      });

      e.Graphics.DrawString(String.Format("{0}{1}%", (Convert.ToDecimal(columnValue) > 0) ? "+" : "", columnValue),
        e.Appearance.Font,
        textBrush,
        bounds,
        new StringFormat { Alignment = StringAlignment.Far, LineAlignment = StringAlignment.Center });

      e.Graphics.DrawLine(Constants.CellBorder, bounds.Right - stringWidth, bounds.Y, bounds.Right - stringWidth, bounds.Bottom);
      return true;
    }
    public bool DrawProgress(TreeList sender, CustomDrawNodeCellEventArgs e, object columnValue, Rectangle bounds)
    {
        decimal displayValue = ConvertEx.ToDecimal(columnValue);
        string displayText = String.Empty;
       string[] drawerInfo = activeDrawer.Split('_');
      if (drawerInfo.Length == 4)
      {
          string symb = drawerInfo[3].ToUpper();
          displayText = String.Format(symb.Equals("C") ? Resource.Drawer_Cost :
             symb.Equals("S") ? Resource.Drawer_Sale : Resource.Drawer_Maco, displayValue);          
      }
      DrawProgressWithText(sender, e, columnValue, displayText, bounds);
      return true;
    }
    #endregion

    #region Service methods
    private void DrawProgressWithText(TreeList gv, CustomDrawNodeCellEventArgs e, object columnValue, string displayString, Rectangle bounds)
    {
      //decimal max = (from d in ((DataTable)gv.DataSource).AsEnumerable() select ConvertEx.ToDecimal(d.Field<int>(e.Column.FieldName))).Max();
      //if (max < 100) 
      decimal max = 100;
      decimal cellValue = Convert.ToDecimal(columnValue);
      if (cellValue > max)
        cellValue = max;

      e.Graphics.FillRectangle(mainBrush, new Rectangle(bounds.X, bounds.Y, Convert.ToInt32(bounds.Width * cellValue / ((max < 1) ? 1 : max)), bounds.Height));
      e.Graphics.DrawString(displayString, e.Appearance.Font, textBrush, bounds, new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
    }
    #endregion
  }
}
