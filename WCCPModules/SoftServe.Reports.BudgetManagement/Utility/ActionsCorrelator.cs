﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftServe.Reports.BudgetManagement.Tabs;
using Logica.Reports.Common.WaitWindow;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;

namespace SoftServe.Reports.BudgetManagement.Utility
{
    public class ActionsCorrelator
    {
        public delegate void ChangesSaver(int typeId, int year);

        public delegate bool ChangesChecker();

        public MethodInvoker Load;
        public ChangesChecker CheckForChanges;
        public ChangesSaver Save;
        public EventHandler ChannelChanged;
        public EventHandler YearChanged;
        public EventHandler TypeChanged;
        public EventHandler CurrentDateChanged;


        public int SelectedYear { get; set; }
        public int SelectedTypeId { get; set; }
        public bool HasWriteAccess { get; set; }

        /// <summary>
        /// Fires the load.
        /// </summary>
        public void FireLoad()
        {
            if (Load != null)
            {
                WaitManager.StartWait();
                try
                {
                    Load();
                }
                finally
                {
                    WaitManager.StopWait();
                }
            }
        }

        /// <summary>
        /// Saves data.
        /// </summary>
        private void SaveAction()
        {
            if (Save != null)
            {
                WaitManager.StartWait();
                Save(SelectedTypeId, SelectedYear);
                WaitManager.StopWait();
                XtraMessageBox.Show(Resource.SavedSuccessfull, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Fires the save.
        /// </summary>
        /// <param name="sender">The sender.</param>
        public void FireSave(object sender)
        {
            SaveAction();
            FireLoad();
        }

        /// <summary>
        /// Gets a value indicating whether data on tabs was changed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if data on tabs was changed; otherwise, <c>false</c>.
        /// </value>
        private bool IsChanged
        {
            get
            {
                bool result = false;
                if (CheckForChanges != null)
                {
                    foreach (ChangesChecker del in CheckForChanges.GetInvocationList())
                    {
                        result = result || del();
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Saves if confirmed.
        /// </summary>
        private void SaveIfConfirmed()
        {
            if (IsChanged && HasWriteAccess &&
                XtraMessageBox.Show(SoftServe.Reports.BudgetManagement.Resource.AskForSave, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Information) ==
                DialogResult.Yes)
                SaveAction();
        }

        /// <summary>
        /// Fires the channel change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        public void FireChannelChange(object sender)
        {
            if (ChannelChanged != null)
                ChannelChanged(sender, null);
        }

        /// <summary>
        /// Fires the year change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        public void FireYearChange(object sender)
        {
            SaveIfConfirmed();

            SelectedYear = Convert.ToInt32(((BarEditItem) sender).EditValue);

            if (YearChanged != null)
                YearChanged(sender, null);

            FireLoad();
        }

        public void FireCurrentDateChange(object sender)
        {
            SaveIfConfirmed();

            if (CurrentDateChanged != null)
                CurrentDateChanged(sender, null);

            FireLoad();
        }

        /// <summary>
        /// Fires the type change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        public void FireTypeChange(object sender)
        {
            SaveIfConfirmed();

            SelectedTypeId = Convert.ToInt32(((BarEditItem) sender).EditValue);

            if (TypeChanged != null)
                TypeChanged(sender, null);

            FireLoad();
        }
    }
}