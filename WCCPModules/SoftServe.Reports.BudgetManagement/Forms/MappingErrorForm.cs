using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.BudgetManagement.Forms
{
  /// <summary>
  /// 
  /// </summary>
  public partial class MappingErrorForm : XtraForm
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="FormClientList"/> class.
    /// </summary>
    public MappingErrorForm()
    {
      InitializeComponent();
    }

    public string[] UnmappedRegions
    {
      set
      {
        if (value.Length == 0)
          layoutControlItemRegion.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        else
        {
          memoEditRegions.Text = string.Join(Environment.NewLine, value);
          memoEditRegions.Reset();
        }
      }
    }

    public string[] UnmappedSKU
    {
      set
      {
        if (value.Length == 0)
          layoutControlItemSKU.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        else
        {
          memoEditSKU.Text = string.Join(Environment.NewLine, value);
          memoEditSKU.Reset();
        }
      }
    }
  }
}