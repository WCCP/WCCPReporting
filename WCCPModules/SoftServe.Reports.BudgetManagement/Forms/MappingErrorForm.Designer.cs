namespace SoftServe.Reports.BudgetManagement.Forms
{
  partial class MappingErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
          this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
          this.memoEditSKU = new DevExpress.XtraEditors.MemoEdit();
          this.memoEditRegions = new DevExpress.XtraEditors.MemoEdit();
          this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
          this.layoutControlItemClose = new DevExpress.XtraLayout.LayoutControlItem();
          this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
          this.layoutControlItemRegion = new DevExpress.XtraLayout.LayoutControlItem();
          this.layoutControlItemSKU = new DevExpress.XtraLayout.LayoutControlItem();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
          this.layoutControl.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.memoEditSKU.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.memoEditRegions.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemClose)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRegion)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSKU)).BeginInit();
          this.SuspendLayout();
          // 
          // layoutControl
          // 
          this.layoutControl.Controls.Add(this.simpleButtonClose);
          this.layoutControl.Controls.Add(this.memoEditSKU);
          this.layoutControl.Controls.Add(this.memoEditRegions);
          this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
          this.layoutControl.Location = new System.Drawing.Point(0, 0);
          this.layoutControl.Name = "layoutControl";
          this.layoutControl.Root = this.layoutControlGroup1;
          this.layoutControl.Size = new System.Drawing.Size(282, 316);
          this.layoutControl.TabIndex = 0;
          this.layoutControl.Text = "layoutControl1";
          // 
          // simpleButtonClose
          // 
          this.simpleButtonClose.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.simpleButtonClose.Location = new System.Drawing.Point(188, 282);
          this.simpleButtonClose.Name = "simpleButtonClose";
          this.simpleButtonClose.Size = new System.Drawing.Size(82, 22);
          this.simpleButtonClose.StyleController = this.layoutControl;
          this.simpleButtonClose.TabIndex = 5;
          this.simpleButtonClose.Text = "�������";
          // 
          // memoEditSKU
          // 
          this.memoEditSKU.Location = new System.Drawing.Point(12, 158);
          this.memoEditSKU.Name = "memoEditSKU";
          this.memoEditSKU.Properties.ReadOnly = true;
          this.memoEditSKU.Size = new System.Drawing.Size(258, 120);
          this.memoEditSKU.StyleController = this.layoutControl;
          this.memoEditSKU.TabIndex = 1;
          // 
          // memoEditRegions
          // 
          this.memoEditRegions.Location = new System.Drawing.Point(12, 28);
          this.memoEditRegions.Name = "memoEditRegions";
          this.memoEditRegions.Properties.ReadOnly = true;
          this.memoEditRegions.Size = new System.Drawing.Size(258, 110);
          this.memoEditRegions.StyleController = this.layoutControl;
          this.memoEditRegions.TabIndex = 4;
          // 
          // layoutControlGroup1
          // 
          this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
          this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
          this.layoutControlGroup1.GroupBordersVisible = false;
          this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemClose,
            this.emptySpaceItem1,
            this.layoutControlItemRegion,
            this.layoutControlItemSKU});
          this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
          this.layoutControlGroup1.Name = "layoutControlGroup1";
          this.layoutControlGroup1.Size = new System.Drawing.Size(282, 316);
          this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
          this.layoutControlGroup1.Text = "layoutControlGroup1";
          this.layoutControlGroup1.TextVisible = false;
          // 
          // layoutControlItemClose
          // 
          this.layoutControlItemClose.Control = this.simpleButtonClose;
          this.layoutControlItemClose.CustomizationFormText = "layoutControlItemClose";
          this.layoutControlItemClose.Location = new System.Drawing.Point(176, 270);
          this.layoutControlItemClose.MaxSize = new System.Drawing.Size(86, 26);
          this.layoutControlItemClose.MinSize = new System.Drawing.Size(86, 26);
          this.layoutControlItemClose.Name = "layoutControlItemClose";
          this.layoutControlItemClose.Size = new System.Drawing.Size(86, 26);
          this.layoutControlItemClose.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
          this.layoutControlItemClose.Text = "layoutControlItemClose";
          this.layoutControlItemClose.TextSize = new System.Drawing.Size(0, 0);
          this.layoutControlItemClose.TextToControlDistance = 0;
          this.layoutControlItemClose.TextVisible = false;
          // 
          // emptySpaceItem1
          // 
          this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
          this.emptySpaceItem1.Location = new System.Drawing.Point(0, 270);
          this.emptySpaceItem1.Name = "emptySpaceItem1";
          this.emptySpaceItem1.Size = new System.Drawing.Size(176, 26);
          this.emptySpaceItem1.Text = "emptySpaceItem1";
          this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
          // 
          // layoutControlItemRegion
          // 
          this.layoutControlItemRegion.Control = this.memoEditRegions;
          this.layoutControlItemRegion.CustomizationFormText = "��������� ������� �� ���� ����������:";
          this.layoutControlItemRegion.Location = new System.Drawing.Point(0, 0);
          this.layoutControlItemRegion.Name = "layoutControlItemRegion";
          this.layoutControlItemRegion.Size = new System.Drawing.Size(262, 130);
          this.layoutControlItemRegion.Text = "��������� ������� �� ���� ����������:";
          this.layoutControlItemRegion.TextLocation = DevExpress.Utils.Locations.Top;
          this.layoutControlItemRegion.TextSize = new System.Drawing.Size(248, 13);
          // 
          // layoutControlItemSKU
          // 
          this.layoutControlItemSKU.Control = this.memoEditSKU;
          this.layoutControlItemSKU.CustomizationFormText = "��������� ������������ �� ���� ����������:";
          this.layoutControlItemSKU.Location = new System.Drawing.Point(0, 130);
          this.layoutControlItemSKU.Name = "layoutControlItemSKU";
          this.layoutControlItemSKU.Size = new System.Drawing.Size(262, 140);
          this.layoutControlItemSKU.Text = "��������� ������������ �� ���� ����������:";
          this.layoutControlItemSKU.TextLocation = DevExpress.Utils.Locations.Top;
          this.layoutControlItemSKU.TextSize = new System.Drawing.Size(248, 13);
          // 
          // MappingErrorForm
          // 
          this.ClientSize = new System.Drawing.Size(282, 316);
          this.Controls.Add(this.layoutControl);
          this.MinimumSize = new System.Drawing.Size(290, 350);
          this.Name = "MappingErrorForm";
          this.ShowIcon = false;
          this.ShowInTaskbar = false;
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "������ �������������";
          ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
          this.layoutControl.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.memoEditSKU.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.memoEditRegions.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemClose)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRegion)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSKU)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.MemoEdit memoEditSKU;
        private DevExpress.XtraEditors.MemoEdit memoEditRegions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRegion;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClose;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemClose;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSKU;
    }
}