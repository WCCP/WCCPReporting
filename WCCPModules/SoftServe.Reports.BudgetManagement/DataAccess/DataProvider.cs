﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using SoftServe.Reports.BudgetManagement.Utility;
using System.Data.SqlClient;
using SoftServe.Reports.BudgetManagement.DataAccess.DataSets;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.BudgetManagement.DataAccess
{
    internal static class DataProvider
    {
        private static DataTable activityList;
        private static DataTable yearsList;
        private static DataTable channelsList;
        private const int ALL_CHANNELS_ID = -1;
        private static UserLevelType? userLevel = null;
        
        internal static bool IsLDB
        {
            get { return userLevel == UserLevelType.M2; }
        }

        public static bool IsDebug
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        /// <summary>
        /// Gets the access level.
        /// </summary>
        /// <value>The access level.</value>
        public static int AccessLevel
        {
            get { return ConvertEx.ToInt(DataAccessLayer.ExecuteScalarStoredProcedure(Constants.SP_GET_ACCESS_LEVEL)); }
        }

        /// <summary>
        /// Gets the user level. (M2, 3, 4...?)
        /// </summary>
        /// <value>The user level.</value>
        internal static UserLevelType UserLevel
        {
            get 
            {
                if (userLevel == null)
                {
                    userLevel = (UserLevelType)ConvertEx.ToInt(DataAccessLayer.ExecuteScalarStoredProcedure(Constants.SP_GET_USER_LEVEL));
                }
                return userLevel ?? UserLevelType.Unknown; 
            }
        }

        public static DataTable ActivityList
        {
            get
            {
                if (activityList == null)
                {
                    DataTable dataTable = null;
                    DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_ACTIVITY_LIST);
                    if (dataSet != null && dataSet.Tables.Count > 0)
                    {
                        dataTable = dataSet.Tables[0];
                    }
                    activityList = dataTable;
                }

                return activityList;
            }
        }

        public static DataTable YearsList
        {
            get
            {
                if (yearsList == null)
                {
                    DataTable dataTable = null;
                    DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_YEARS_LIST,
                        new[] {new SqlParameter(Constants.SP_GET_YEARS_LIST_PARAM_1, DBNull.Value)});
                    if (dataSet != null && dataSet.Tables.Count > 0)
                    {
                        dataTable = dataSet.Tables[0];
                    }
                    yearsList = dataTable;
                }
                return yearsList;
            }
        }

        public static DataTable ChannelsList
        {
            get
            {
                if (channelsList == null)
                {
                    DataTable dataTable = null;
                    DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_CHANNELS_LIST);
                    if (dataSet != null && dataSet.Tables.Count > 0)
                    {
                        dataTable = dataSet.Tables[0];
                    }
                    foreach (DataRow row in dataTable.Rows)
                    {
                        if (row.IsNull(Constants.SP_GET_CHANNELS_LIST_FLD_ID))
                        {
                            row[Constants.SP_GET_CHANNELS_LIST_FLD_ID] = ALL_CHANNELS_ID;
                            row[Constants.SP_GET_CHANNELS_LIST_FLD_NAME] = Resource.AllChannels;
                        }
                    }

                    channelsList = dataTable;
                }
                return channelsList;
            }
        }

        #region Tab plan or fact

        public static DataTable GetAreaAndStaffTree(BudgetParameters parameters)
        {
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure
                (
                    Constants.SP_GET_AREA_AND_STAFF_TREE,
                    new[]
                        {
                            new SqlParameter(Constants.SP_GET_AREA_AND_STAFF_TREE_PARAM_1, parameters.ActivityType),
                            new SqlParameter(Constants.SP_GET_AREA_AND_STAFF_TREE_PARAM_2, parameters.Year),
                            new SqlParameter(Constants.SP_GET_AREA_AND_STAFF_TREE_PARAM_3, DBNull.Value),
                            new SqlParameter(Constants.SP_GET_AREA_AND_STAFF_TREE_PARAM_4, parameters.Channel > 0 ? (object) parameters.Channel : DBNull.Value),
                            new SqlParameter(Constants.SP_GET_AREA_AND_STAFF_TREE_PARAM_5, IsDebug?(object)parameters.CurrentDate:DBNull.Value)
                        }
                );
            if (dataSet != null && dataSet.Tables.Count > 0)
                return dataSet.Tables[0];
            return null;
        }

        #endregion

        #region Tab budget creation

        public static DataTable GetInitiatives(BudgetParameters parameters)
        {
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure
                (
                    Constants.SP_GET_INITIATIVES_LIST,
                    new[]
                        {
                            new SqlParameter(Constants.SP_GET_INITIATIVES_LIST_PARAM_1, parameters.ActivityType),
                            new SqlParameter(Constants.SP_GET_INITIATIVES_LIST_PARAM_2, parameters.Year),
                            new SqlParameter(Constants.SP_GET_INITIATIVES_LIST_PARAM_3, DBNull.Value),
                            new SqlParameter(Constants.SP_GET_INITIATIVES_LIST_PARAM_4, IsDebug?(object)parameters.CurrentDate:DBNull.Value)
                        }
                );
            if (dataSet != null && dataSet.Tables.Count > 0)
                return dataSet.Tables[0];
            return null;
        }

        public static DataTable AvailableRegions
        {
            get
            {
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure
                    (
                        Constants.SP_GET_TERRITORY_LIST,
                        new[]
                            {
                                new SqlParameter(Constants.SP_GET_TERRITORY_LIST_PARAM_1, DBNull.Value)
                            }
                    );

                if (dataSet != null && dataSet.Tables.Count > 0)
                    return dataSet.Tables[0];
                return null;
            }
        }

        public static void UpdateInitiatives(BudgetParameters parameters, int[] checkedInitiativeIds, int[] uncheckedInitiativeIds)
        {
            if (checkedInitiativeIds.Length == 0 && uncheckedInitiativeIds.Length == 0)
                return;

            DataAccessLayer.ExecuteNonQueryStoredProcedure
                (
                    Constants.SP_UPDATE_INITIATIVES,
                    new[]
                        {
                            new SqlParameter(Constants.SP_UPDATE_INITIATIVES_PARAM_1, parameters.Year),
                            new SqlParameter(Constants.SP_UPDATE_INITIATIVES_PARAM_2, parameters.ActivityType),
                            new SqlParameter(Constants.SP_UPDATE_INITIATIVES_PARAM_3, ArrayToList(checkedInitiativeIds)),
                            new SqlParameter(Constants.SP_UPDATE_INITIATIVES_PARAM_4, ArrayToList(uncheckedInitiativeIds))
                        }
                );
        }

        public static void MoveBudgetInWork(BudgetParameters parameters, int[] initiativeIds)
        {
            if (initiativeIds.Length == 0)
                return;

            DataAccessLayer.ExecuteNonQueryStoredProcedure
                (
                    Constants.SP_MOVE_BUDGET_IN_WORK,
                    new[]
                        {
                            new SqlParameter(Constants.SP_MOVE_BUDGET_IN_WORK_PARAM_1, parameters.Year),
                            new SqlParameter(Constants.SP_MOVE_BUDGET_IN_WORK_PARAM_2, parameters.ActivityType),
                            new SqlParameter(Constants.SP_MOVE_BUDGET_IN_WORK_PARAM_3, ArrayToList(initiativeIds))
                        }
                );
        }

        private static string ArrayToList(IEnumerable<int> ids)
        {
            StringBuilder sbIds = new StringBuilder();
            foreach (int id in ids)
                sbIds.AppendFormat("{0}{1}", id, Constants.LIST_DELIMITER);
            string checkedIds = (sbIds.Length == 0) ? string.Empty : sbIds.ToString(0, sbIds.Length - Constants.LIST_DELIMITER.Length);
            return checkedIds;
        }

        #endregion

        #region Tab MACO

        public static DataTable RegionMapping
        {
            get
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_REGION_MAPPING);
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
        }

        public static DataTable SKUMapping
        {
            get
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_SKU_MAPPING);
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
        }

        public static DataTable GetMACO(BudgetParameters parameters)
        {
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure
                (
                    Constants.SP_GET_MACO,
                    new[]
                        {
                            new SqlParameter(Constants.SP_GET_MACO_PARAM_1, parameters.Year)
                        }
                );
            if (dataSet != null && dataSet.Tables.Count > 0)
                return dataSet.Tables[0];
            return null;
        }

        public static void UpdateMACO(BudgetParameters parameters, ImportedData.DataRow row)
        {
            DataAccessLayer.ExecuteNonQueryStoredProcedure
                (
                    Constants.SP_UPDATE_MACO,
                    new[]
                        {
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_1, row.IsTotalNull() ? DBNull.Value : (object) row.Total),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_2, row.IsBUDGETNull() ? DBNull.Value : (object) row.BUDGET),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_3, row.Is_1Null() ? DBNull.Value : (object) row._1),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_4, row.Is_2Null() ? DBNull.Value : (object) row._2),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_5, row.Is_3Null() ? DBNull.Value : (object) row._3),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_6, row.Is_4Null() ? DBNull.Value : (object) row._4),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_7, row.Is_5Null() ? DBNull.Value : (object) row._5),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_8, row.Is_6Null() ? DBNull.Value : (object) row._6),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_9, row.Is_7Null() ? DBNull.Value : (object) row._7),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_10, row.Is_8Null() ? DBNull.Value : (object) row._8),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_11, row.Is_9Null() ? DBNull.Value : (object) row._9),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_12, row.Is_10Null() ? DBNull.Value : (object) row._10),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_13, row.Is_11Null() ? DBNull.Value : (object) row._11),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_14, row.Is_12Null() ? DBNull.Value : (object) row._12),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_15, row.Region_id),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_16, row.Product_Id),
                            new SqlParameter(Constants.SP_UPDATE_MACO_PARAM_17, parameters.Year)
                        }
                );
        }

        #endregion
    }
}