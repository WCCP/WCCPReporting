﻿using System.ComponentModel;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraTab;
using SoftServe.Reports.BudgetManagement.Tabs;
using SoftServe.Reports.BudgetManagement.Utility;
using SoftServe.Reports.BudgetManagement.DataAccess;

namespace WccpReporting
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : BaseReportUserControl
    {
        private ActionsCorrelator correlator;
        private MainTab tabPlanOrFact;
        private MainTab tabBudgetCreation;
        private MainTab tabMACO;

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            correlator = new ActionsCorrelator();

            if (DataProvider.IsLDB)
            {
                correlator.HasWriteAccess = false;
                AccessLevel.CurrentAccessLevel = (int)AccessLevelType.ReadOnly;
                AccessLevel.CurrentUserLevel = DataProvider.UserLevel;

            }
            else
            {
                correlator.HasWriteAccess = HasWriteAccess;
                AccessLevel.CurrentAccessLevel = DataProvider.AccessLevel;
                AccessLevel.CurrentUserLevel = DataProvider.UserLevel;
                
                if (AccessLevel.CheckAccessLevel(AccessLevelType.Unknown))
                {
                    return;
                }

                tabPlanOrFact = new MainTab(this, TabType.PlanOrFact, correlator);
                tabBudgetCreation = new MainTab(this, TabType.BudgetCreation, correlator);
            }
            tabMACO = new MainTab(this, TabType.MACO, correlator);

            TabControl.ClosePageButtonShowMode = ClosePageButtonShowMode.InAllTabPageHeaders;

            UpdateAllSheets();

            TabControl.SelectedTabPage = tabPlanOrFact;
        }

        /// <summary>
        /// Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            if (tabPlanOrFact != null)
            {
                tabPlanOrFact.UpdateSheet(new SheetParamCollection { TabId = tabPlanOrFact.TabId, TableDataType = TableType.Fact });
            }
            if (tabBudgetCreation != null)
            {
                tabBudgetCreation.UpdateSheet(new SheetParamCollection { TabId = tabBudgetCreation.TabId, TableDataType = TableType.Fact });
            }
            if (tabMACO != null)
            {
                tabMACO.UpdateSheet(new SheetParamCollection { TabId = tabMACO.TabId, TableDataType = TableType.Fact });
            }
        }
    }
}