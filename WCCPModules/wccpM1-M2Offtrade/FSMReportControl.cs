﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common;
using Logica.Reports.wccpM1M2OffTrade;
using Logica.Reports.wccpM1M2OffTrade.DataAccess;

namespace WccpReporting {
    public partial class WccpUIControl : BaseReportUserControl {
        public static WccpUIControl ReportControl;
        public static List<wccpM1M2OffTradeSettingsForm> Forms;
        private wccpM1M2OffTradeSettingsForm setForm;
        public WccpUIControl() : this(-1) {
        }

        public WccpUIControl(int reportId) : base(reportId) {
            InitializeComponent();
            if (getForm() != null) {
                setForm = getForm();
                setForm.SetParent(this);
                off_reports = true;
                btnSaveForPDA.Visibility = BarItemVisibility.Always;
                UpdateSheets(setForm.SheetParamsList);
                Attach_DetachShowingEditorEvent(true);
                tabManager.SelectedTabPageIndex = 0;
                foreach (XtraTabPage page in tabManager.TabPages) {
                    BaseTab tab = page as BaseTab;
                    if (null != tab)
                        tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
                    HideTable(page);
                }
                CustomSettings();
            }
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            MenuCloseClick += WccpUIControl_CloseClick;
            RefreshClick += WccpUIControl_AfterTabRefresh;
            SaveClick += WccpUIControl_AfterTabRefresh;
            SaveForPDAClick += WccpUIControl_SaveForPDAClick;

            ReportControl = this;
        }

        private void grid_ShowingEditor(object sender, CancelEventArgs e) {
            BandedGridView view = (BandedGridView) sender;
            if (view != null) {
                bool bsmEnabled;
                if (!bool.TryParse(view.GetRowCellValue(view.FocusedRowHandle, "POCeBSM").ToString(), out bsmEnabled))
                    return;
                if (view.FocusedColumn.FieldName == "POCeSD") {
                    if (!bsmEnabled) {
                        e.Cancel = true;
                    }
                }
                if (view.FocusedColumn.FieldName == "POCeBSM") {
                    if (bsmEnabled) {
                        view.SetRowCellValue(view.FocusedRowHandle, "POCeSD", false);
                    }
                }
            }
        }

        private void grid_RBPlan_ShowingEditor(object sender, CancelEventArgs e) {
            BandedGridView view = (BandedGridView) sender;
            if (view != null) {
                GridColumn checkCol = view.VisibleColumns[0];
                GridColumn setCol = view.VisibleColumns[1];
                if (view.FocusedColumn == setCol) {
                    if ((string) view.GetFocusedRowCellValue(checkCol) == Constants.RB_SUMMARY)
                        e.Cancel = true;
                }
            }
        }

        private void Attach_DetachShowingEditorEvent(bool isAttaching) {
            foreach (XtraTabPage page in tabManager.TabPages) {
                BaseTab tab = page as BaseTab;
                if (null == tab)
                    continue;
                AttachMethodsToTab(tab, isAttaching);
            }
        }

        private void AttachMethodsToTab(BaseTab tab, bool isAttaching) {
            if (tab.Text.Length < 4 || tab.Length == 0)
                return;

            string tabText = tab.Text.Substring(0, 4);
            if (tabText.Equals(Constants.M1A)) {
                if (isAttaching) {
                    ((BandedGridView) tab.GetGrid(tab.Length - 1).GridControl.DefaultView).ShowingEditor += grid_RBPlan_ShowingEditor;
                } else {
                    ((BandedGridView) tab.GetGrid(tab.Length - 1).GridControl.DefaultView).ShowingEditor -= grid_RBPlan_ShowingEditor;
                }
            }

            if (tabText.Equals(Constants.M1B)) {
                for (int i = 0; i < tab.Length; i++) {
                    if (isAttaching) {
                        ((BandedGridView) tab.GetGrid(i).GridControl.DefaultView).ShowingEditor += grid_ShowingEditor;
                    } else {
                        ((BandedGridView) tab.GetGrid(i).GridControl.DefaultView).ShowingEditor -= grid_ShowingEditor;
                    }
                }
            }
        }

        private void grid_CalcRowHeight(object sender, RowHeightEventArgs e) {
            e.RowHeight = 150;
        }

        private void HideTable(XtraTabPage pg) {
            if (pg.Text.Length >= 4) {
                if (pg.Text.Substring(0, 4).Equals(Constants.M1A) || pg.Text.Substring(0, 4).Equals(Constants.M21)) {
                    foreach (Control c in pg.Controls) {
                        foreach (Control target in c.Controls) {
                            if (target is PanelControl) {
                                foreach (Control child in target.Controls) {
                                    if (child is LabelControl)
                                        if ((child as LabelControl).Text.Equals(Constants.HideTable)) {
                                            c.Visible = false;
                                            ((BandedGridView) ((BaseGridControl) c).GridControl.DefaultView).CalcRowHeight += grid_CalcRowHeight;
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool IsNeedFitTabToOnePage() {
            return true;
        }

        public static void AddForm(wccpM1M2OffTradeSettingsForm form) {
            if (Forms == null)
                Forms = new List<wccpM1M2OffTradeSettingsForm>();
            Forms.Add(form);
        }

        public static WccpUIControl Instance() {
            return ReportControl ?? (ReportControl = new WccpUIControl());
        }

        public bool HasUnsavedData() {
            foreach (XtraTabPage tab in tabManager.TabPages) {
                if (tab is BaseTab) {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified) {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool SaveUnsavedData() {
            bool result = true;

            foreach (XtraTabPage tab in tabManager.TabPages) {
                if (tab is BaseTab) {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified) {
                        if (!page.SaveChanges()) {
                            result = false;
                        }
                    }
                }
            }

            return result;
        }

        protected override void OnControlRemoved(ControlEventArgs e) {
            Forms.Remove(setForm);
            base.OnControlRemoved(e);
        }

        private wccpM1M2OffTradeSettingsForm getForm() {
            return Forms != null && Forms.Count > 0 ? Forms[Forms.Count - 1] : null;
        }

        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage) {
            if (setForm.ShowDialog() == DialogResult.OK) {
                Attach_DetachShowingEditorEvent(false);
                UpdateSheets(setForm.SheetParamsList);
                FillComboBox();
                Attach_DetachShowingEditorEvent(true);
            }
        }

        private bool WccpUIControl_CloseClick(object sender, XtraTabPage selectedPage) {
            if (selectedPage is BaseTab) {
                BaseTab page = selectedPage as BaseTab;
                if (page.DataModified) {
                    DialogResult result = MessageBox.Show(Resource.SaveData, Resource.DataNotSaved, MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.OK) {
                        if (page.SaveChanges()) {
                            XtraMessageBox.Show(Resource.DataSaved);
                        }
                    } else if (result == DialogResult.Cancel) {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Print button click, show print dialog box for M1M2 report
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void btnPrintAll_ItemClick_M1M2(object sender, ItemClickEventArgs e) {
            CompositeLink cl = new CompositeLink(new PrintingSystem());
            foreach (object tb in tabManager.TabPages) {
                if (tb is IPrint) {
                    cl.Links.Add(((IPrint) tb).PrepareCompositeLink());
                    cl.BreakSpace = (int) (cl.PrintingSystem.PageSettings.UsablePageSizeInPixels.Height - 1.0);
                }
            }
            cl.CreateDocument();
            cl.PrintingSystem.PreviewFormEx.Show();
        }

        /// <summary>
        /// Set custom properties for M1M2 report
        /// </summary>
        private void CustomSettings() {
            cmbTab.Visible = true;
            lblTab.Visible = true;
            btnPrintAll.ItemClick -= btnPrintAll_ItemClick;
            btnPrintAll.ItemClick += btnPrintAll_ItemClick_M1M2;
            cmbTab.SelectedIndexChanged += SelectedIndexChanged;

            FillComboBox();
        }

        /// <summary>
        /// Fill combobox with data for M1M2 report
        /// </summary>
        private void FillComboBox() {
            cmbTab.Properties.Items.Clear();
            List<XtraTabPage> tmpLst = new List<XtraTabPage>(tabManager.TabPages);
            foreach (XtraTabPage page in tmpLst) {
                cmbTab.Properties.Items.Add(new ComboBoxItem {Id = page, Text = page.Text});
            }
            //set first Tab name as combobox Text.
            try {
                cmbTab.Text = ((ComboBoxItem) cmbTab.Properties.Items[0]).Text;
            } catch (IndexOutOfRangeException ex) {
            }
        }

        /// <summary>
        /// Occurs when index is changed
        /// </summary>
        /// <param name="sender">cmbTab</param>
        /// <param name="e">Event arguments</param>
        private void SelectedIndexChanged(object sender, EventArgs e) {
            if (!cmbTab.Text.Equals(Resource.All))
                tabManager.SelectedTabPage = ((XtraTabPage) ((ComboBoxItem) cmbTab.SelectedItem).Id);
        }

        private void WccpUIControl_AfterTabRefresh(object sender, XtraTabPage selectedPage) {
            BaseTab baseTab = selectedPage as BaseTab;
            if (baseTab != null) {
                AttachMethodsToTab(baseTab, true);
            }
        }

        private void WccpUIControl_SaveForPDAClick(object sender, XtraTabPage selectedPage) {
            //TODO: add call SP here
            foreach (XtraTabPage tab in tabManager.TabPages) {
                if (tab is BaseTab) {
                    BaseTab basetab = (BaseTab) tab;

                    basetab.SaveChanges();

                    AttachMethodsToTab(basetab, true);
                }
            }
            XtraMessageBox.Show(Resource.DataSaved);
            DateTime lDate = setForm.ReportDate;
            int lSvId = setForm.SupervisorId;
            try {
                DataAccessProvider.SaveDailyPlan(lDate, lSvId);
            } catch (Exception e) {
                ErrorManager.ShowErrorBox(Resource.ErrorDataSavedToSwDdb + e.Message);
            }
        }
    }
}