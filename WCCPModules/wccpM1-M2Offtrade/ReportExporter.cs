using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.wccpM1M2OffTrade.DataAccess;
using WccpReporting;

namespace Logica.Reports.wccpM1M2OffTrade {
    public class ReportExporter : IReportExporter {
        private bool _initialized = false;
        private int _reportId = 129;
        private string _reportName;
        private string _exportPath = null;
        private ExportToType _exportType;

        private Dictionary<int, string> _supervisors = new Dictionary<int, string>();
        private DateTime _reportDate;
        private int _supervisorId;
        private string _supervisorName;
        private TableType _reportType;
        private int _factType;
        private ExportModeEnum _exportMode;
        private int _dsmId;

        public ReportExporter() {
            _reportDate = DateTime.Today;
            _supervisorId = -1;
            _supervisorName = string.Empty;
            _reportType = TableType.Fact;
            _factType = 0;
            //_exportPath = Directory.GetCurrentDirectory();
            _exportType = ExportToType.Pdf;
            _reportName = "M1-M2 Off Trade";

            _initialized = true;
        }

        public string ExportPath {
            get { return _exportPath; }
            set { SetExportPath(value); }
        }

        #region IReportExporter implementation

        public void SetExportType(ExportToType exportType) {
            _exportType = exportType;
        }

        public void SetExportMode(ExportModeEnum exportMode) {
            _exportMode = exportMode;
        }

        public int GetReportId() {
            return _reportId;
        }

        public void SetReportId(int reportId) {
            _reportId = reportId;
        }

        public string GetExportPath() {
            return _exportPath;
        }

        public void SetExportPath(string path) {
            _exportPath = path;
        }

        public DateTime GetReportDate(DateTime reportDate) {
            return _reportDate;
        }

        public void SetReportDate(DateTime date) {
            _reportDate = date;
        }

        public void SetSupervisorId(int svId) {
            _supervisorId = svId;
        }

        public void SetDsmId(int dsmId) {
            _dsmId = dsmId;
        }

        public void ExportRun() {
            string reportCaption = "M1-M2 Off Trade";
            try {
                //set report parameters
                //_reportDate = new DateTime(2012, 9, 5);
                //_supervisorId = 163;

                //settings form
                wccpM1M2OffTradeSettingsForm setForm = new wccpM1M2OffTradeSettingsForm {ReportName = reportCaption};
                setForm.SetReportType(_reportType);
                setForm.SetFactType(_factType);
                setForm.ReportDate = _reportDate;
                WccpUIControl.AddForm(setForm);

                GetSupervisorsList();
                foreach (KeyValuePair<int, string> lSupervisor in _supervisors) {
                    _supervisorId = lSupervisor.Key;
                    _supervisorName = lSupervisor.Value;
                    setForm.SetSupervisorForExport(_supervisorId);
                    
                    LogMessage("Executing report for " + _supervisorName);
                    WccpUIControl lRpt = null;
                    try {
                        // run report
                        if (WccpUIControl.Forms.Count < 1)
                            WccpUIControl.AddForm(setForm);
                        lRpt = new WccpUIControl(_reportId);
                        //export to PDF
                        string lFileName = string.Format("{0} {1}({2}) {3}.pdf", _reportDate.ToString("yyyyMMdd"), _supervisorName, _supervisorId, _reportName);
                        string lFilePath = _exportPath + @"\" + lFileName;

                        LogMessage("Exporting report to PDF...");
                        lRpt.ExportReportToFile(lFilePath, _exportType);
                    } finally {
                        if (lRpt != null)
                            lRpt.Dispose();
                    }
                }
            }
            catch (Exception lException) {
                LogMessage(lException.Message);
            }
            
        }

        #endregion

        private static void LogMessage(string msg) {
            Console.WriteLine(string.Format("{0} " + msg, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")));
        }

        private void GetSupervisorsList() {
            DataTable lDataTable = DataAccessProvider.GetSupervisorsTable();

            if (lDataTable == null)
                return;

            _supervisors.Clear();
            bool lIsSuperviserDefined = _supervisorId > 0;

            foreach (DataRow lRow in lDataTable.Rows) {
                int lSvId = (int) lRow["Supervisor_id"];
                string lSvName = lRow["Supervisor_name"].ToString();

                if (lIsSuperviserDefined)
                {
                    if (_supervisorId == lSvId)
                        _supervisors.Add(lSvId, lSvName);
                }
                else 
                {
                    _supervisors.Add(lSvId, lSvName);
                }
            }
        }
    }
}