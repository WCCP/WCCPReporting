﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.wccpM1M2OffTrade.DataAccess;

namespace Logica.Reports.wccpM1M2OffTrade
{
    
    public partial class wccpM1M2OffTradeSettingsForm : XtraForm
    {
        private List<ComboBoxItem> _supervisors;
        DataTable dataTableDate;
        BaseReportUserControl bruc;
        DateTime dtStart, dtEnd;
        private bool m2selected;
        private Tab[] reportTabs;

        private ComboBoxItem _supervisorForExportOnly;

        public string ReportName
        {
            get { return lblReport.Text; }
            set { lblReport.Text = value; }
        }

        public DateTime ReportDate
        {
            get { return dateEdit.DateTime.Date; }
            set { dateEdit.DateTime = value; }
        }

        public int SupervisorId
        {
            get { return (int)((ComboBoxItem)cbM2.SelectedItem).Id; }
        }
        
        public void SetParent(BaseReportUserControl parent) {
            bruc = parent;
            reportTabs = bruc.Report.Tabs;
        }
        
        public wccpM1M2OffTradeSettingsForm() {
            InitializeComponent();

            _supervisors = new List<ComboBoxItem>();

            DataTable lSupervisors = DataAccessProvider.GetTable("spDW_SupervisorsGetList", "@ReportId", 100);
            FillM2(lSupervisors);

            dateEdit.DateTime = DateTime.Now;
            GetUpdData();
            rbType.SelectedIndex = 1;
        }

        public wccpM1M2OffTradeSettingsForm(BaseReportUserControl parent)
            : this()
        {
            SetParent(parent);
        }
        
        public void SetSupervisorForExport(int svId)
        {
            _supervisorForExportOnly = _supervisors.FirstOrDefault(i => Convert.ToInt32(i.Id) == svId);
            m2selected = true;
            ChangeM1List(svId);
        }

        public void SetReportType(TableType tableType) {
            rbType.SelectedIndex = (tableType == TableType.Plan) ? 0 : 1;
        }

        public void SetFactType(int factType) {
            if (factType >= rbFactType.Properties.Items.Count)
                return;
            rbFactType.SelectedIndex = factType;
        }

        /// <summary>
        /// Create list of SheetParams with all Tabs
        /// </summary>
        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                if (rbType.SelectedIndex == 0)
                {
                    dtStart = new DateTime(dateEdit.DateTime.Year, dateEdit.DateTime.Month, 1);
                    dtEnd = dtStart.AddMonths(1).AddMilliseconds(-1);
                }
                else
                    dataTableDate = DataAccessProvider.GetTable("spDW_OnWorkingDays", "@Date", dateEdit.DateTime);

                dataTableDate = DataAccessProvider.GetTable("spDW_OnWorkingDays", "@Date", dateEdit.DateTime);

                List<SheetParamCollection> list = new List<SheetParamCollection>();
                List<Tab> tabs = new List<Tab>();


                //add M1_A,M1_B tabs
                foreach (CheckedListBoxItem item in cmb1.Properties.Items.Cast<CheckedListBoxItem>().OrderBy(x => x.Description))
                {
                    if (item.CheckState != CheckState.Checked)
                        continue;

                    Dictionary<Guid, Guid> clonedTabs = bruc.CloneTabs(item.Description);
                    list.Add(FillParamCol(clonedTabs[Constants.M1ATabID], item.Description, item.Value));
                    list.Add(FillParamCol(clonedTabs[Constants.M1BTabID], item.Description, item.Value));

                    tabs.AddRange(bruc.Report.Tabs.Where(x => x.Id == clonedTabs[Constants.M1ATabID]));
                    tabs.AddRange(bruc.Report.Tabs.Where(x => x.Id == clonedTabs[Constants.M1BTabID]));
                }

                if (m2selected)
                    tabs.AddRange(reportTabs.Where(t => t.Id != Constants.M1ATabID || t.Id != Constants.M1BTabID));

                bruc.Report.Tabs = tabs.Select((t, i) =>
                {
                    t.Order = i + 1;
                    return t;
                }).ToArray();

                dataTableDate = DataAccessProvider.GetTable("spDW_OnWorkingDays", "@Date", dateEdit.DateTime);

                foreach (Tab tab in bruc.Report.Tabs)
                {
                    if (tab.Header.Substring(0, 3).Equals("M1_"))
                        continue;

                    foreach (Table tbl in tab.Tables)
                    {
                        if (rbType.SelectedIndex == 0 && tbl.Type == TableType.Plan)
                        {
                            list.Add(FillParamCol(tab.Id, "", tab.Id));
                            break;
                        }

                        if (rbType.SelectedIndex == 1)
                        {
                            if (tab.Header.Substring(0, 3).Equals("M2_"))
                                tab.Header = tab.Header.Substring(0, 4);
                            else
                                tab.Header = tab.Header.Substring(0, 8);
                            if (tab.Id != Constants.M2D)
                                list.Add(FillParamCol(tab.Id, "", tab.Id));
                            break;
                        }
                    }
                }

                foreach (Tab tab in bruc.Report.Tabs)
                    if (tab.Id == Constants.M2D && rbType.SelectedIndex == 0)
                        tab.Order = 0;

                return list;
            }
        }
        
        private SheetParamCollection FillParamCol(Guid id, string merchName, object merchId)
        {
            SheetParamCollection paramCollection = new SheetParamCollection
            {
                MainHeader = Resource.MainHeader,
                TabId = id,
                TableDataType = rbType.SelectedIndex == 0 ? TableType.Plan : TableType.Fact
            };

            if (!merchName.Equals(""))
            {
                paramCollection.Add(new SheetParam { SqlParamName = "@Merch_ID", Value = merchId, DisplayParamName = Resource.M1, DisplayValue = merchName });
                paramCollection.Add(new SheetParam { SqlParamName = "@Date", Value = DateParse(dateEdit.DateTime) });
                if (rbType.SelectedIndex == 1)
                {
                    paramCollection.Add(new SheetParam { SqlParamName = "@isEvening", Value = rbFactType.SelectedIndex, DisplayParamName = Resource.Period, DisplayValue = rbFactType.SelectedIndex == 0 ? Resource.Morning : Resource.Evening });
                    paramCollection.Add(new SheetParam { SqlParamName = "@Supervisor_Id", Value = null });
                    GetDateInfo(paramCollection);
                }
            }
            else
            {
                paramCollection.MainHeader = Resource.MainHeader_Supervisor;
                paramCollection.Add(new SheetParam
                {
                    DisplayParamName = Resource.M2,
                    SqlParamName = "@Supervisor_id",
                    Value = _supervisorForExportOnly != null ? _supervisorForExportOnly.Id : ((ComboBoxItem)cbM2.SelectedItem).Id,
                    DisplayValue = _supervisorForExportOnly != null ? _supervisorForExportOnly.Text : ((ComboBoxItem)cbM2.SelectedItem).Text
                });

                GetDateInfo(paramCollection);
                paramCollection.Add(new SheetParam { DisplayParamName = "Дата:", SqlParamName = "@Date", Value = dateEdit.DateTime.Date, DisplayValue = dateEdit.DateTime.ToShortDateString() });
                paramCollection.Add(new SheetParam { SqlParamName = "@isEvening", Value = rbFactType.SelectedIndex });
            }

            if (rbType.SelectedIndex == 0)
            {
                paramCollection.Add(new SheetParam { SqlParamName = "@StartDate", Value = dtStart, DisplayParamName = Resource.ReportMounth, DisplayValue = dtStart.ToString("MMMM yyyy") });
                paramCollection.Add(new SheetParam { SqlParamName = "@EndDate", Value = dtEnd });
            }

            return paramCollection;
        }

        private void GetUpdData() {
            lblUpd2.Text = "";
            dataTableDate = DataAccessProvider.GetTable("spDW_M_OffTrade_Get_UpdateTime", "@Date", DateTime.Now);
            foreach (DataRow dr in dataTableDate.Rows) {
                lblUpd2.Text = Convert.ToDateTime(dr["PrefValue"]).ToString();
                break;
            }
            dataTableDate.Dispose();
        }

        /// <summary>
        /// Fills Combobox with M2 names
        /// </summary>
        private void FillM2(DataTable dtSuper)
        {
            cbM2.Properties.Items.Clear();

            foreach (DataRow dr in dtSuper.Rows)
            {
                ComboBoxItem lItem = new ComboBoxItem
                    {
                        Id = dr["Supervisor_id"],
                        Text = dr["Supervisor_name"].ToString()
                    };

                _supervisors.Add(lItem);
                cbM2.Properties.Items.Add(lItem);
            }
            
            if (cbM2.Properties.Items.Count > 0)
            {
                cbM2.SelectedItem = cbM2.Properties.Items[0];
            }

            ChangeM1List();
        }
        
        private void GetDateInfo(SheetParamCollection paramCollection) {
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.ReportDay, DisplayValue = Convert.ToDateTime(dataTableDate.Rows[0]["CurrentDate"]).ToShortDateString() });
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.ReportMounth, DisplayValue = Convert.ToDateTime(dataTableDate.Rows[0]["CurrentDate"]).ToString("MM yyyy") });

            decimal daysInMonth = ConvertEx.ToDecimal(dataTableDate.Rows[0]["WorkDaysInMonth"]);
            //decimal workDay = ConvertEx.ToDecimal(dataTableDate.Rows[0]["WorkDay"]);
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.WorkDays, DisplayValue = string.Format(Math.Truncate(daysInMonth) != daysInMonth ? "{0:N1}" : "{0:N0}", daysInMonth) });
            //paramCollection.Add(new SheetParam { DisplayParamName = Resource.WorkDay, DisplayValue = string.Format(Math.Truncate(workDay) != workDay ? "{0:N1}" : "{0:N0}", workDay) });
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.WorkDay, DisplayValue = dataTableDate.Rows[0]["WorkDayText"].ToString() });
        }

        private void rbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbFactType.Enabled = (rbType.SelectedIndex == 0 ? false : true);
            lbDate.Visible = dateEdit.Visible = true;
        }

        private void cbM2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeM1List();
        }

        private void btnYes_Click(object sender, EventArgs e) {
            bool m1Selected = false;
            foreach (CheckedListBoxItem item in cmb1.Properties.Items)
                if (item.CheckState == CheckState.Checked) {
                    m1Selected = true;
                    break;
                }

            m2selected = cbM2.SelectedItem as ComboBoxItem != null;

            if (!m1Selected && !m2selected) {
                XtraMessageBox.Show(Resource.ErrorSelectM2OrM1);
                return;
            }

            if (rbType.SelectedIndex == 0 && dateEdit.DateTime < new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)) {
                XtraMessageBox.Show(Resource.Error_Date);
                return;
            }

            DialogResult = DialogResult.OK;
        }
        
        private void btnNo_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
            Close();       
        }

        private void btn_upd_Click(object sender, EventArgs e) {
            Enabled = false;
            WaitManager.StartWait();
            try {
                DataAccessProvider.RecalculateData(dateEdit.DateTime);
            } finally  {
                Enabled = true;
                WaitManager.StopWait();
            }
            GetUpdData();
        }
        
        private void ChangeM1List(int svId){
            AddM1(DataAccessProvider.GetMerchesBySupervisor(svId));

            foreach (CheckedListBoxItem lItem in cmb1.Properties.Items)
            {
                lItem.CheckState = CheckState.Checked;
            }
        }

        private void ChangeM1List() {
            if (cbM2.SelectedItem == null || !(cbM2.SelectedItem is ComboBoxItem))
                return;

            int lSvId = Convert.ToInt32(((ComboBoxItem) cbM2.SelectedItem).Id);
            ChangeM1List(lSvId);
       }

        private void AddM1(DataTable dtMerch) {
            cmb1.Properties.Items.Clear();
            foreach (DataRow dr in dtMerch.Rows)
                cmb1.Properties.Items.Add(dr[SqlConstants.FldMerchId], dr[SqlConstants.FldMerchName].ToString(), CheckState.Unchecked, true);
        }

        private static DateTime DateParse(DateTime inputDate) {
            string resultStr = inputDate.ToString("yyyy-MM-dd");
            DateTime parsed = DateTime.ParseExact(resultStr, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            return parsed;
        }
    }


    internal class ComboBoxItem {
        public object Id { get; set; }

        public string Text { get; set; }

        public override string ToString() {
            return Text;
        }

        public override int GetHashCode(){
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ComboBoxItem)
            {
                return Id.Equals((obj as ComboBoxItem).Id);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
