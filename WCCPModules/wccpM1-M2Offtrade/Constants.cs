﻿using System;

namespace Logica.Reports.wccpM1M2OffTrade
{
    class Constants
    {
        internal static Guid M1ATabID = new Guid("aa805b7d-c86d-490f-98fd-3f017f9f2d7b");
        internal static Guid M1BTabID = new Guid("ba15f54b-857b-44e7-93d6-f76eaf4c181a");
        internal static Guid M2D      = new Guid("6879a6a2-736e-47fb-b8d4-719a62fb79a0");  
        internal static string M1A = "M1_A";
        internal static string M1B = "M1_B";
        internal static string M21 = "M2_1";       
        internal static string HideTable = "Планирование действий";
        public const string RB_SUMMARY = "РБ - Итого";
    }
}
