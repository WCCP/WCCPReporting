﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.BaseReportControl;
using Logica.Reports.wccpM1M2OffTrade;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;

namespace WccpReporting
{
    public delegate void CloseUIdelegate();

    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpM1M2OffTrade.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                wccpM1M2OffTradeSettingsForm setForm = new wccpM1M2OffTradeSettingsForm {ReportName = reportCaption};

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.AddForm(setForm);

                    _reportControl = new WccpUIControl(reportId);
                    _reportCaption = reportCaption;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        public void CloseUI()
        {
            if (WccpUIControl.ReportControl.HasUnsavedData()
                && MessageBox.Show(Resource.SaveData, Resource.DataNotSaved, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (WccpUIControl.ReportControl.SaveUnsavedData())
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(Resource.DataSaved);  
                }
            }
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}
