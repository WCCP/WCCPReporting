﻿using System;
using System.Data;
using System.Data.SqlClient;
using CreateConnection;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;

namespace Logica.Reports.wccpM1M2OffTrade.DataAccess {
    internal static class DataAccessProvider {
        public static void RecalculateData(DateTime date, int cmdTimeout = 3600, int monthCount = 1) {
            try {
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpRecalculateData, cmdTimeout,
                    new[] {
                        new SqlParameter(SqlConstants.SpRecalculateDataParam1, date),
                        new SqlParameter(SqlConstants.SpRecalculateDataParam2, monthCount)
                    });
            } catch (Exception lException) {
                if (lException.InnerException != null && lException.InnerException is SqlException) {
                    SqlException lSqlException = (SqlException) lException.InnerException;
                    if (lSqlException.State == 100 && lSqlException.Class == 15)
                        throw new ApplicationException(lSqlException.Message, lSqlException);
                }
                throw;
            }
        }

        public static void SaveDailyPlan(DateTime date, int svId) {
            ConnectionToSWDB_V35 ConToSW_DDB = new ConnectionToSWDB_V35();
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(ConToSW_DDB.Connection.ConnectionString);
            sb.InitialCatalog = "LDB";
            DataAccessLayer.SetConnectionString(sb.ConnectionString);
            DataAccessLayer.OpenConnection();
            try 
            {
                DataAccessLayer.ExecuteNonQueryStoredProcedure("spDW_M1_DailyPlanRecalc", new[] {
                    new SqlParameter("ReportDate", date),
                    new SqlParameter("svID", svId),
                    new SqlParameter("ReportID", 2) });
            } 
            finally 
            {
                DataAccessLayer.CloseConnection();
                DataAccessLayer.SetConnectionStringToDefault();
            }
        }

        public static DataTable GetAllM1() {
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            DataSet lDataSet = DataAccessLayer.ExecuteQuery(SqlConstants.SqlGetAllM1);
            if (null != lDataSet && lDataSet.Tables.Count > 0)
                lDataTable = lDataSet.Tables[0];
            DataAccessLayer.CloseConnection();

            return lDataTable;
        }

        public static DataTable GetMerchesBySupervisor(object supervisorId) {
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.ProcGetMerch,
                new SqlParameter[] {
                    new SqlParameter(SqlConstants.ParamSupervisorId, supervisorId),
                    new SqlParameter(SqlConstants.ParamEntityType, 2)
                });

            if (null != lDataSet && lDataSet.Tables.Count > 0)
                lDataTable = lDataSet.Tables[0];
            DataAccessLayer.CloseConnection();

            return lDataTable;
        }

        /// <summary>
        /// Fills DataTable
        /// </summary>
        public static DataTable GetTable(string procName, string sqlParam, object value) {
            DataTable lDataTable = null;
            try {
                DataAccessLayer.OpenConnection();
                lDataTable = DataAccessLayer.ExecuteStoredProcedure(procName, new[] {new SqlParameter(sqlParam, value)}).Tables[0];
            } catch (Exception lException) {
                ErrorManager.ShowErrorBox(lException.Message);
            } finally {
                DataAccessLayer.CloseConnection();
            }

            return lDataTable;
        }

        public static DataTable GetSupervisorsTable() {
            DataTable lDataTable;
            try {
                DataAccessLayer.OpenConnection();
                lDataTable = DataAccessLayer.ExecuteQuery(SqlConstants.QueryGetSupervisors).Tables[0];
            } finally {
                DataAccessLayer.CloseConnection();
            }

            return lDataTable;
        }
    }
}