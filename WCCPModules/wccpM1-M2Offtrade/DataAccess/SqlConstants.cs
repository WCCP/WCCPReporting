﻿namespace Logica.Reports.wccpM1M2OffTrade.DataAccess
{
    static class SqlConstants {
        public const string SpRecalculateData = "dbo.spSW_ReCalculateData";
        public const string SpRecalculateDataParam1 = "RecalcDate";
        public const string SpRecalculateDataParam2 = "monthCount";

        public static string SqlGetAllM1 = "select * from tblMerchandisers";
        public static string QueryGetSupervisors = @"SELECT DISTINCT
	staff.Name    as Supervisor_name,
	s.Supervisor_id as Supervisor_ID
FROM dbo.tblStaff staff	
	inner join dbo.tblStaffUserTypes AS ut   on staff.Staffid = ut.Staffid	
	inner join dbo.DW_URM_ObjectUserTypes AS outp on ut.UserType_id = outp.UserType_id and [Object_ID] = 100	
	inner join dbo.tblSupervisors s on s.Supervisor_id = ISNULL(staff.oldstaffid, staff.staffID)
	inner join dbo.tblDSM dsm on dsm.DSM_Id = s.DSM_Id
WHERE staff.Status <> 9
    and staff.Level = 5
    and ut.Status <> 9   
    and dsm.ChanelType_id = 2 ";

        public static string ProcGetMerch = "spDW_M1M2_getMerch";

        public static string ParamSupervisorId = "@Supervisor_id";
        public static string ParamEntityType = "@entityType";

        public static string FldMerchId = "Merch_id";
        public static string FldMerchName = "MerchName";
    }
}
