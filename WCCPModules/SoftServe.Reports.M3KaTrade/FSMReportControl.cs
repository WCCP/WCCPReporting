﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using SoftServe.Reports.M3KaTrade;
using SoftServe.Reports.M3KaTrade.UserControls;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        // Plans input
        private XtraTabPage _skuPage;
        private XtraTabPage _indicatorsPage;
        private XtraTabPage _networkPage;
        private XtraTabPage _paramPage;
        private readonly SKUPlanTree _skuPlanControl = new SKUPlanTree();
        private readonly IndicatorsPlanTree _indicatorsPlanControl = new IndicatorsPlanTree();
        private readonly NetworkPlanTree _networkPlanControl = new NetworkPlanTree();
        private ParamConfiguration _paramConfig = new ParamConfiguration();

        public static WccpUIControl ReportControl = null;
        public static List<M3KaTradeSettingsForm> Forms = null;
        //        private GridControl headerGrid = null;
        private M3KaTradeSettingsForm setForm = null;

        public WccpUIControl()
            : this(-1)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();

            if (getForm() != null)
            {
                setForm = getForm();
                setForm.SetParent(this);
                off_reports = true;
                PreparePlanTabs();
                MenuButtonsRendering += WccpUIControl_MenuButtonsRendering;
                UpdateTabs();

                foreach (XtraTabPage lPage in tabManager.TabPages)
                {
                    BaseTab tab = lPage as BaseTab;
                    if (null != tab)
                        tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
                    if (lPage.Equals(_skuPage))
                        AddPlanHeader(_skuPage, Resource.MainHeader_SKUPlan);
                    if (lPage.Equals(_indicatorsPage))
                        AddPlanHeader(_indicatorsPage, Resource.MainHeader_IndicatorsPlan);
                    if (lPage.Equals(_networkPage))
                        AddPlanHeader(_networkPage, Resource.MainHeader_NetworkPlan);
                }
                CustomSettings();
            }
            // Events assigning
            //this.ExportClick += new ExportMenuClickHandler(WccpUIControl_ExportClick);
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            MenuCloseClick += WccpUIControl_CloseClick;
            SaveClick += WccpUIControl_SaveClick;
            //            PrepareExport += WccpUIControl_PrepareExport;
            ReportControl = this;
        }

        private void AddPlanHeader(XtraTabPage page, string caption)
        {
            GridControl lHeaderGrid = null;
            GridView lHeaderGridView = null;
            SheetParamCollection lSheetSettings = new SheetParamCollection();
            lSheetSettings.MainHeader = caption;
            lSheetSettings.Add(new SheetParam() { DisplayParamName = Resource.M3, DisplayValue = setForm.DsmName });
            //            lSheetSettings.Add(new SheetParam() { DisplayParamName = Resource.ReportDay, DisplayValue = setForm.CurrentDate.ToShortDateString() });
            lSheetSettings.Add(new SheetParam() { DisplayParamName = Resource.ReportMonth, DisplayValue = setForm.ReportDate.ToString("MMMM yyyy") });
            //            lSheetSettings.Add(new SheetParam() { DisplayParamName = Resource.WorkDays, DisplayValue = setForm.WorkDays });
            //            lSheetSettings.Add(new SheetParam() { DisplayParamName = Resource.WorkDay, DisplayValue = setForm.WorkDay });
            lSheetSettings.Add(new SheetParam() { DisplayParamName = Resource.ReportDate, DisplayValue = setForm.ReportDate.ToShortDateString() });

            TabOperationHelper.CreateSettingSection(page, lSheetSettings, ref lHeaderGrid, ref lHeaderGridView);
            //            return headerGrid;
        }

        private void WccpUIControl_SaveClick(object sender, XtraTabPage selectedpage)
        {
            if (_skuPlanControl.IsChanged)
                _skuPlanControl.SaveChanges();
            if (_indicatorsPlanControl.IsChanged)
                _indicatorsPlanControl.SaveChanges();
            if (_networkPlanControl.IsChanged)
                _networkPlanControl.SaveChanges();
            if (_paramConfig.IsChanged)
                _paramConfig.SaveChanges();
            XtraMessageBox.Show(Resource.DataSaved);
        }

        private void WccpUIControl_MenuButtonsRendering(object sender, MenuButtonsRenderingEventArgs eventArgs)
        {
            eventArgs.ShowSaveBtn = true;
        }

        private void PreparePlanTabs()
        {
            if (!setForm.IsPlanInput)
                return;

            _skuPage = CreatePlanTab(_skuPage, _skuPlanControl, "Продукция");
            _networkPage = CreatePlanTab(_networkPage, _networkPlanControl, "Сети");
            _indicatorsPage = CreatePlanTab(_indicatorsPage, _indicatorsPlanControl, "Персонал");
            _paramPage = CreatePlanTab(_paramPage, _paramConfig, "Настройка Параметров");
        }

        private static XtraTabPage CreatePlanTab(XtraTabPage page, UserControl userControl, string caption)
        {
            if (null == page)
            {
                page = new XtraTabPage();
                page.Controls.Add(userControl);
                page.Tag = userControl;
                page.Text = caption;
                page.Dock = DockStyle.Fill;
                userControl.Dock = DockStyle.Fill;
            }
            return page;
        }

        private CompositeLink WccpUIControl_PrepareExport(object sender, XtraTabPage selectedPage, ExportToType type)
        {
            CompositeLink cl = null;
            //            if (selectedPage == ratingsReportPage) {
            //                List<IPrintable> lst = new List<IPrintable>();
            //                lst.Add(headerGrid as IPrintable);
            //                foreach (Control ratings_gp in selectedPage.Controls)
            //                    foreach (Control panel in ratings_gp.Controls)
            //                        foreach (Control c in panel.Controls)
            //                            lst.Add(c as IPrintable);
            //                cl = TabOperationHelper.PrepareCompositeLink(type, "", lst, true); //(type, "", lst, IsNeedFitTabToOnePage);
            //            }
            return cl;
        }

        public static void AddForm(M3KaTradeSettingsForm form)
        {
            if (Forms == null)
                Forms = new List<M3KaTradeSettingsForm>();
            Forms.Add(form);
        }

        private void UpdateTabs()
        {
            //            if (setForm.IsPlanInput)
            UpdateSheets(setForm.SheetParamsList);
            foreach (XtraTabPage page in tabManager.TabPages)
            {
                BaseTab tab = page as BaseTab;
                if (null != tab)
                    tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
            }

            if (setForm.IsPlanInput)
            {
                if (!TabControl.TabPages.Contains(_skuPage))
                    TabControl.TabPages.Add(_skuPage);
                _skuPlanControl.LoadData(setForm.DsmId, setForm.ReportDate.Date);

                if (!TabControl.TabPages.Contains(_networkPage))
                    TabControl.TabPages.Add(_networkPage);
                _networkPlanControl.LoadData(setForm.DsmId, setForm.ReportDate.Date);

                if (!TabControl.TabPages.Contains(_indicatorsPage))
                    TabControl.TabPages.Add(_indicatorsPage);
                _indicatorsPlanControl.LoadData(setForm.DsmId, setForm.ReportDate.Date);

                if (!TabControl.TabPages.Contains(_paramPage))
                    TabControl.TabPages.Add(_paramPage);
                _paramConfig.LoadData(setForm.DsmId, setForm.ReportDate.Date);
            }
            else
            {
                if (TabControl.TabPages.Contains(_skuPage))
                    TabControl.TabPages.Remove(_skuPage);
                if (TabControl.TabPages.Contains(_indicatorsPage))
                    TabControl.TabPages.Remove(_indicatorsPage);
                if (TabControl.TabPages.Contains(_networkPage))
                    TabControl.TabPages.Remove(_networkPage);
                if (TabControl.TabPages.Contains(_paramPage))
                    TabControl.TabPages.Remove(_paramPage);
            }
        }

        public bool IsNeedFitTabToOnePage()
        {
            return true;
        }

        /// <summary>
        /// WCCPs the UI control_ settings form click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                PreparePlanTabs();
                UpdateTabs();
                SortPages();
            }
        }

        public bool HasUnsavedData()
        {
            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool SaveUnsavedData()
        {
            bool result = true;

            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    BaseTab page = tab as BaseTab;
                    if (page.DataModified)
                    {
                        if (!page.SaveChanges())
                        {
                            result = false;
                        }
                    }
                }
            }

            return result;
        }

        private bool WccpUIControl_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage is BaseTab)
            {
                BaseTab page = selectedPage as BaseTab;

                if (page.DataModified)
                {
                    DialogResult result = MessageBox.Show(Resource.SaveData, Resource.DataNotSaved, MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.OK)
                    {
                        if (page.SaveChanges())
                        {
                            XtraMessageBox.Show(Resource.DataSaved);
                        }
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private M3KaTradeSettingsForm getForm()
        {
            if (Forms != null && Forms.Count > 0)
                return Forms[Forms.Count - 1];
            return null;
        }

        protected override void OnControlRemoved(ControlEventArgs e)
        {
            Forms.Remove(setForm);
            base.OnControlRemoved(e);
        }

        private void CustomSettings()
        {
            cmbTab.Visible = true;
            lblTab.Visible = true;
            cmbTab.SelectedIndexChanged += SelectedIndexChanged;
            FillComboBox();
        }

        /// <summary>
        /// Fill combobox with data for M1M2 report
        /// </summary>
        private void FillComboBox()
        {
            cmbTab.Properties.Items.Clear();
            List<XtraTabPage> lTmpList = new List<XtraTabPage>(tabManager.TabPages);
            foreach (XtraTabPage lPage in lTmpList)
                cmbTab.Properties.Items.Add(new ComboBoxItem() { Id = lPage, Text = lPage.Text });

            //set first Tab name as combobox Text.
            if (cmbTab.Properties.Items.Count > 0)
                cmbTab.Text = ((ComboBoxItem)cmbTab.Properties.Items[0]).Text;
        }

        /// <summary>
        /// Occurs when index is changed
        /// </summary>
        /// <param name="sender">cmbTab</param>
        /// <param name="e">Event arguments</param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            tabManager.SelectedTabPage = ((XtraTabPage)((ComboBoxItem)cmbTab.SelectedItem).Id);
        }

        private void SortPages()
        {
            if (setForm.IsPlanInput)
                return;

            List<XtraTabPage> lTabPageList = new List<XtraTabPage>();
            foreach (XtraTabPage lTabPage in tabManager.TabPages)
                lTabPageList.Add(lTabPage);

            XtraTabPage lTmpPage;
            for (int i = 0; i < lTabPageList.Count; i++)
            {
                lTmpPage = lTabPageList[i];
                for (int j = i; j < lTabPageList.Count; j++)
                    if (lTmpPage.Text.CompareTo(lTabPageList[j].Text) >= 0)
                        lTmpPage = lTabPageList[j];

                lTabPageList.Remove(lTmpPage);
                lTabPageList.Insert(i, lTmpPage);
            }
            lTabPageList.ForEach(page => tabManager.TabPages.Move(150 + lTabPageList.IndexOf(page), page));
        }

    }
}