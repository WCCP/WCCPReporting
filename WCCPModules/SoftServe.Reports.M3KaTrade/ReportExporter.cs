using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.ConfigXmlParser.Model;
using SoftServe.Reports.M3KaTrade.DataProvider;
using WccpReporting;

namespace SoftServe.Reports.M3KaTrade {
    public class ReportExporter : IReportExporter {
//        private const int _reportId = 129; // M1-M2 Off Trade
        private int _reportId = 165; // M3 KA Trade
        private string _exportPath;
        private TableType _reportType;

        private Dictionary<int, string> _dsms = new Dictionary<int, string>();
        private Dictionary<int, string> _supervisors = new Dictionary<int, string>();
        private Dictionary<int, int> _m1OldToNew = new Dictionary<int, int>();
        private Dictionary<int, int> _m2OldToNew = new Dictionary<int, int>();
        private DateTime _reportDate;
        private int _supervisorId;
        private string _supervisorName;
        private int _dsmId;
        private string _dsmName;
        private int _factType;

        public ReportExporter() {
            _reportType = TableType.Fact;
            _reportDate = DateTime.Today;
            _supervisorId = -1;
            _supervisorName = string.Empty;
            _dsmId = -1;
            _factType = 0;
            //_exportPath = Directory.GetCurrentDirectory();
        }

        public string ExportPath {
            get { return GetExportPath(); }
            set { SetExportPath(value); }
        }

        #region IReportExporter implementation

        public int GetReportId() {
            return _reportId;
        }

        public void SetReportId(int reportId) {
            _reportId = reportId;
        }

        public void SetExportType(ExportToType exportType) {
        }

        public void SetExportMode(ExportModeEnum exportMode) {
        }

        public string GetExportPath() {
            return _exportPath;
        }

        public void SetExportPath(string path) {
            _exportPath = path;
        }

        public DateTime GetReportDate(DateTime reportDate) {
            return _reportDate;
        }

        public void SetReportDate(DateTime date) {
            _reportDate = date;
        }

        public void SetDsmId(int dsmId) {
            _dsmId = dsmId;
        }

        public void SetSupervisorId(int svId) {
            _supervisorId = svId;
        }

        public void ExportRun() {
            string reportCaption = "M3 Ka-Trade";
            try {
                //set report parameters
                //_reportDate = new DateTime(2012, 9, 5);
                //_supervisorId = 163;

                //settings form
                M3KaTradeSettingsForm setForm = new M3KaTradeSettingsForm();
                setForm.SetReportType(_reportType);
                setForm.ReportDate = _reportDate;
                WccpUIControl.AddForm(setForm);

//                _dsmId = 57;
//                _dsmName = "������ �.-��";
//                setForm.SetDsmId(_dsmId);

                //M3 and export for each M1
                GetDsmList();
                foreach (KeyValuePair<int, string> lDsm in _dsms) {
                    _dsmId = lDsm.Key;
                    _dsmName = lDsm.Value;
                    setForm.SetDsmId(_dsmId);
                    //select only passed SvId else ALL
                    if (_supervisorId < 0)
                        setForm.SelectAllM2();
                    else {
                        GetSupervisorsList(_dsmId);
                        foreach (KeyValuePair<int, string> lSupervisor in _supervisors)
                            setForm.SelectM2(lSupervisor.Key);
                    }
                    setForm.SelectAllM1();
                    LogMsgHeader("Executing report for DSM: " + _dsmName);
                    WccpUIControl lRpt = null;
                    try {
                        // run report
                        if (WccpUIControl.Forms.Count < 1)
                            WccpUIControl.AddForm(setForm);
                        lRpt = new WccpUIControl(_reportId);
                        FillOldToNewStaffIdDictionary(setForm.M1List, setForm.M2List);
                        //M1 export to PDF
                        foreach (CheckedListBoxItem lItem in setForm.M1List) {
                            int lMerchId = ConvertEx.ToInt(lItem.Value);
                            int lNewMerchId = lMerchId;
                            if (_m1OldToNew.ContainsKey(lMerchId))
                                lNewMerchId = _m1OldToNew[lMerchId];
                            string lCapsMerchId = "60" + lNewMerchId;
                            string lPathBuild = string.Format("{0}\\{1}\\{2}", lCapsMerchId, _reportDate.ToString("yyyyMM"), _reportDate.ToString("dd"));
                            string lFilePath = _exportPath + @"\" + lPathBuild;
                            if (!Directory.Exists(lFilePath))
                                Directory.CreateDirectory(lFilePath);
                            string lFileName = "\\" + reportCaption + ".pdf";
                            //string lFileName = string.Format("{0} {1}({2}) {3}.pdf", _reportDate.ToString("yyyyMMdd"), _supervisorName, _supervisorId, _reportName);
                            lFilePath += lFileName;
                            LogMsgHeader(string.Format("Exporting report to PDF for Merch: {0} ({1})", lItem.Description, lMerchId));
                            lRpt.ExportReportWithMode(lFilePath, ExportToType.Pdf, ExportModeEnum.M1, lMerchId);
                        }
                        //M2 export to PDF
                        foreach (CheckedListBoxItem lItem in setForm.M2List) {
                            int lSvId = ConvertEx.ToInt(lItem.Value);
                            int lNewSvId = lSvId;
                            if (_m2OldToNew.ContainsKey(lSvId))
                                lNewSvId = _m2OldToNew[lSvId];
                            string lCapsSvId = "50" + lNewSvId;
                            string lPathBuild = string.Format("{0}\\{1}\\{2}", lCapsSvId, _reportDate.ToString("yyyyMM"), _reportDate.ToString("dd"));
                            string lFilePath = _exportPath + @"\" + lPathBuild;
                            if (!Directory.Exists(lFilePath))
                                Directory.CreateDirectory(lFilePath);

                            string lFileName = "\\" + reportCaption + ".pdf";
                            lFilePath += lFileName;
                            LogMsgHeader(string.Format("Exporting report to PDF for Supervisor: {0} ({1})", lItem.Description, lSvId));
                            lRpt.ExportReportWithMode(lFilePath, ExportToType.Pdf, ExportModeEnum.M2, lSvId);
                        }
                    }
                    finally {
                        if (lRpt != null)
                            lRpt.Dispose();
                    }
                }
            }
            catch (Exception lException) {
                LogException(lException);
            }
        }

        private void FillOldToNewStaffIdDictionary(CheckedListBoxItemCollection m1List, CheckedListBoxItemCollection m2List) {
            // M1
            string lIdListStr = string.Empty;
            foreach (CheckedListBoxItem lItem in m1List) {
                lIdListStr += ConvertEx.ToLongInt(lItem.Value) + ",";
            }
            DataTable lDataTable = DataAccessProvider.GetNewStaffId("M1", lIdListStr);
            _m1OldToNew.Clear();
            foreach (DataRow lRow in lDataTable.Rows) {
                int lOldId = (int) lRow["OldStaffId"];
                int lNewId = (int) lRow["StaffId"];
                if (_m1OldToNew.ContainsKey(lOldId))
                    continue;
                _m1OldToNew.Add(lOldId, lNewId);
            }

            // M2
            lIdListStr = string.Empty;
            foreach (CheckedListBoxItem lItem in m2List) {
                lIdListStr += ConvertEx.ToLongInt(lItem.Value) + ",";
            }
            lDataTable = DataAccessProvider.GetNewStaffId("M2", lIdListStr);
            _m2OldToNew.Clear();
            foreach (DataRow lRow in lDataTable.Rows) {
                int lOldId = (int) lRow["OldStaffId"];
                int lNewId = (int) lRow["StaffId"];
                if (_m2OldToNew.ContainsKey(lOldId))
                    continue;
                _m2OldToNew.Add(lOldId, lNewId);
            }
        }

        #endregion

        public static void LogMessage(string msg) {
            Console.WriteLine(msg);
        }

        public static void LogMsgHeader(string msg) {
            LogMessage(string.Format("{0} " + msg, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        }

        public static void LogException(Exception exception) {
            LogMsgHeader("\t" + exception.GetType().FullName);
            LogMessage("  Data: " + exception.Data);
            LogMessage("  Message: " + exception.Message);
            LogMessage("  Source: " + exception.Source);
            LogMessage("  StackTrace: " + exception.StackTrace);
            LogMessage("\n");

            if (exception.InnerException == null)
                return;
            LogMessage("Inner exception:");
            LogException(exception.InnerException);
        }

        private void GetDsmList() {
            DataTable lDataTable = DataAccessProvider.GetPersonnel("M3", 0, _reportDate);
            if (null == lDataTable)
                return;
            _dsms.Clear();
            foreach (DataRow lRow in lDataTable.Rows) {
                int lId = (int) lRow["Staff_ID"];
                if (_dsmId < 0) {
                    _dsms.Add(lId, lRow["Staff_Name"].ToString());
                }
                else {
                    if (_dsmId == lId)
                        _dsms.Add(lId, lRow["Staff_Name"].ToString());
                }
            }
        }

        private void GetSupervisorsList(int dsmId) {
            DataTable lDataTable = DataAccessProvider.GetPersonnel("M2", dsmId, _reportDate);
            if (null == lDataTable)
                return;
            _supervisors.Clear();
            foreach (DataRow lRow in lDataTable.Rows) {
                int lId = (int) lRow["Staff_ID"];
                if (_supervisorId < 0) {
                    _supervisors.Add(lId, lRow["Staff_Name"].ToString());
                }
                else {
                    if (_supervisorId == lId)
                        _supervisors.Add(lId, lRow["Staff_Name"].ToString());
                }
            }
        }
    }
}