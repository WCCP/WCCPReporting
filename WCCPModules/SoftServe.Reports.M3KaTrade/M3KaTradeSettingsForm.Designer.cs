﻿namespace SoftServe.Reports.M3KaTrade
{
    partial class M3KaTradeSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(M3KaTradeSettingsForm));
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.rbType = new DevExpress.XtraEditors.RadioGroup();
            this.groupData1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.lbDate = new DevExpress.XtraEditors.LabelControl();
            this.lblReport = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cbM3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblM2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cbM1 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cbM2 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).BeginInit();
            this.groupData1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.CausesValidation = false;
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(223, 336);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 9;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(143, 336);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 8;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.rbType);
            this.groupControl3.Location = new System.Drawing.Point(0, 251);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(298, 63);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "Тип отчета:";
            // 
            // rbType
            // 
            this.rbType.Location = new System.Drawing.Point(5, 25);
            this.rbType.Name = "rbType";
            this.rbType.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rbType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Ввод планов"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Получение фактов")});
            this.rbType.Size = new System.Drawing.Size(276, 35);
            this.rbType.TabIndex = 7;
            // 
            // groupData1
            // 
            this.groupData1.Controls.Add(this.dateEdit);
            this.groupData1.Controls.Add(this.lbDate);
            this.groupData1.Location = new System.Drawing.Point(0, 41);
            this.groupData1.Name = "groupData1";
            this.groupData1.Size = new System.Drawing.Size(298, 50);
            this.groupData1.TabIndex = 19;
            this.groupData1.Text = "Период";
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = null;
            this.dateEdit.Location = new System.Drawing.Point(48, 25);
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit.Size = new System.Drawing.Size(83, 20);
            this.dateEdit.TabIndex = 5;
            this.dateEdit.DateTimeChanged += new System.EventHandler(this.dateEdit_DateTimeChanged);
            this.dateEdit.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // lbDate
            // 
            this.lbDate.Location = new System.Drawing.Point(13, 28);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(30, 13);
            this.lbDate.TabIndex = 4;
            this.lbDate.Text = "Дата:";
            // 
            // lblReport
            // 
            this.lblReport.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.lblReport.Appearance.Options.UseFont = true;
            this.lblReport.Location = new System.Drawing.Point(98, 12);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(91, 21);
            this.lblReport.TabIndex = 20;
            this.lblReport.Text = "M3 Ka-trade";
            this.lblReport.Click += new System.EventHandler(this.lblReport_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl2.Appearance.BackColor2 = System.Drawing.Color.White;
            this.groupControl2.Appearance.Options.UseBackColor = true;
            this.groupControl2.Controls.Add(this.cbM3);
            this.groupControl2.Controls.Add(this.lblM2);
            this.groupControl2.Location = new System.Drawing.Point(0, 96);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(298, 52);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Пользователь";
            // 
            // cbM3
            // 
            this.cbM3.Location = new System.Drawing.Point(48, 27);
            this.cbM3.Name = "cbM3";
            this.cbM3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbM3.Size = new System.Drawing.Size(150, 20);
            this.cbM3.TabIndex = 6;
            this.cbM3.SelectedIndexChanged += new System.EventHandler(this.cbM3_SelectedIndexChanged);
            this.cbM3.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cbM3_ButtonClick);
            // 
            // lblM2
            // 
            this.lblM2.Location = new System.Drawing.Point(17, 30);
            this.lblM2.Name = "lblM2";
            this.lblM2.Size = new System.Drawing.Size(18, 13);
            this.lblM2.TabIndex = 9;
            this.lblM2.Text = "М3:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cbM1);
            this.groupControl1.Controls.Add(this.cbM2);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Location = new System.Drawing.Point(5, 154);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(293, 91);
            this.groupControl1.TabIndex = 21;
            this.groupControl1.Text = "Персонал для ввода планов и отображения данных";
            this.groupControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // cbM1
            // 
            this.cbM1.Location = new System.Drawing.Point(43, 59);
            this.cbM1.Name = "cbM1";
            this.cbM1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbM1.Size = new System.Drawing.Size(150, 20);
            this.cbM1.TabIndex = 3;
            this.cbM1.EditValueChanged += new System.EventHandler(this.cbM1_EditValueChanged);
            // 
            // cbM2
            // 
            this.cbM2.Location = new System.Drawing.Point(43, 31);
            this.cbM2.Name = "cbM2";
            this.cbM2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbM2.Size = new System.Drawing.Size(150, 20);
            this.cbM2.TabIndex = 2;
            this.cbM2.EditValueChanged += new System.EventHandler(this.checkedComboBoxEdit1_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "M1:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "M2:";
            // 
            // M3KaTradeSettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(306, 373);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.lblReport);
            this.Controls.Add(this.groupData1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "M3KaTradeSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры отчёта";
            this.Load += new System.EventHandler(this.M3FSMSettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupData1)).EndInit();
            this.groupData1.ResumeLayout(false);
            this.groupData1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbM1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbM2.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.RadioGroup rbType;
        private DevExpress.XtraEditors.GroupControl groupData1;
        private DevExpress.XtraEditors.LabelControl lbDate;
        private DevExpress.XtraEditors.LabelControl lblReport;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cbM3;
        private DevExpress.XtraEditors.LabelControl lblM2;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cbM1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cbM2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}