﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.DataAccess;
using SoftServe.Reports.M3KaTrade.DataProvider;

namespace SoftServe.Reports.M3KaTrade
{
    /// <summary>
    /// 
    /// </summary>
    public partial class M3KaTradeSettingsForm : XtraForm
    {
        private BaseReportUserControl _bruc;
        private DataTable _dataTableDate;
        private IDictionary<string, string> _dict = new Dictionary<string, string>();
        private DateTime _dtEnd;
        private DateTime _dtStart;

        private DataTable _dataTableM2Users = null;
        private DataTable _dataTableM1Users = null;
        internal int ReportId { get; set; }

        //        private DataTable dtSuper;

        private Tab[] _reportTabs;

        public M3KaTradeSettingsForm(Report report)
        {
            InitializeComponent();
            Report = report;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="M3KaTradeSettingsForm"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public M3KaTradeSettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            SetParent(parent);
            dateEdit.DateTime = DateTime.Now;
            rbType.SelectedIndex = 1;
        }

        public M3KaTradeSettingsForm()
        {
            InitializeComponent();
            dateEdit.DateTime = DateTime.Now;
            rbType.SelectedIndex = 1;
        }

        #region Properties

        public DateTime ReportDate
        {
            get { return dateEdit.DateTime.Date; }
            set { dateEdit.DateTime = value; }
        }

        public DateTime CurrentDate { get; set; }

        public int DsmId
        {
            get { return (int)((ComboBoxItem)cbM3.SelectedItem).Id; }
        }

        public string DsmName
        {
            get { return ((ComboBoxItem)cbM3.SelectedItem).Text; }
        }

        public CheckedListBoxItemCollection M1List
        {
            get { return cbM1.Properties.Items; }
        }

        public CheckedListBoxItemCollection M2List
        {
            get { return cbM2.Properties.Items; }
        }

        public bool IsPlanInput
        {
            get { return (rbType.SelectedIndex == 0) ? true : false; }
        }

        public Object WorkDay { get; set; }

        public Object WorkDays { get; set; }

        private Report Report { get; set; }

        /// <summary>
        /// Gets the sheet params list.
        /// </summary>
        /// <value>The sheet params list.</value>
        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                List<SheetParamCollection> list = new List<SheetParamCollection>();

                if (rbType.SelectedIndex == 0)
                {
                    _dtStart = new DateTime(dateEdit.DateTime.Year, dateEdit.DateTime.Month, 1);
                    _dtEnd = _dtStart.AddMonths(1).AddMilliseconds(-1);
                    return list;
                }

                _dataTableDate = getTable("spDW_OnWorkingDays", "@Date", dateEdit.DateTime);

                List<Tab> tabs = new List<Tab>();

                //add M1_A,M1_B tabs
                foreach (CheckedListBoxItem item in cbM1.Properties.Items.Cast<CheckedListBoxItem>().OrderBy(x => x.Description))
                {
                    if (item.CheckState != CheckState.Checked)
                        continue;

                    Dictionary<Guid, Guid> clonedTabs = _bruc.CloneTabs(item.Description);
                    list.Add(FillParamCol(clonedTabs[SqlConstants.M1ATabID], item.Description, item.Value));
                    list.Add(FillParamCol(clonedTabs[SqlConstants.M1BTabID], item.Description, item.Value));

                    tabs.AddRange(_bruc.Report.Tabs.Where(x => x.Id == clonedTabs[SqlConstants.M1ATabID]));
                    tabs.AddRange(_bruc.Report.Tabs.Where(x => x.Id == clonedTabs[SqlConstants.M1BTabID]));
                }

                //add M2
                foreach (CheckedListBoxItem item in cbM2.Properties.Items.Cast<CheckedListBoxItem>().OrderBy(x => x.Description))
                {
                    if (item.CheckState != CheckState.Checked)
                        continue;

                    Dictionary<Guid, Guid> clonedTabs = _bruc.CloneTabs(item.Description);
                    list.Add(FillParamColM2(clonedTabs[SqlConstants.M21], item.Description, item.Value));
                    list.Add(FillParamColM2(clonedTabs[SqlConstants.M22], item.Description, item.Value));
                    list.Add(FillParamColM2(clonedTabs[SqlConstants.M23], item.Description, item.Value));

                    tabs.AddRange(_bruc.Report.Tabs.Where(x => x.Id == clonedTabs[SqlConstants.M21]));
                    tabs.AddRange(_bruc.Report.Tabs.Where(x => x.Id == clonedTabs[SqlConstants.M22]));
                    tabs.AddRange(_bruc.Report.Tabs.Where(x => x.Id == clonedTabs[SqlConstants.M23]));
                }


                tabs.AddRange(_reportTabs.Where(t => t.Id != SqlConstants.M1ATabID || t.Id != SqlConstants.M1BTabID
                    || t.Id != SqlConstants.M21 || t.Id != SqlConstants.M22 || t.Id != SqlConstants.M23));

                if (rbType.SelectedIndex == 1)
                {
                    list.Add(FillParamColM3(tabs.First(t => t.Id == SqlConstants.M31).Id, DsmName, DsmId));
                    list.Add(FillParamColM3(tabs.First(t => t.Id == SqlConstants.M32).Id, DsmName, DsmId));
                    list.Add(FillParamColM3(tabs.First(t => t.Id == SqlConstants.M33).Id, DsmName, DsmId));
                }


                _bruc.Report.Tabs = tabs.Select((t, i) =>
                {
                    t.Order = i + 1;
                    return t;
                }).ToArray();

                _dataTableDate = getTable("spDW_OnWorkingDays", "@Date", dateEdit.DateTime);

                foreach (Tab tab in _bruc.Report.Tabs)
                {
                    if (tab.Header.Substring(0, 4).Equals("M3_1")) tab.Order = 0;
                    if (tab.Header.Substring(0, 4).Equals("M3_2")) tab.Order = 0;
                    if (tab.Header.Substring(0, 4).Equals("M3_3")) tab.Order = 0;
                }

                //dataTableM2Users 

                //    SheetParamCollection shp = new SheetParamCollection();

                //    foreach (CheckedListBoxItem item in cbM2.Properties.Items)
                //        if (item.CheckState == CheckState.Checked)
                //            M2Personal = M2Personal + item.Value.ToString() + ';';
                //    dict.Add("M2_ID", M2Personal);
                //    foreach (CheckedListBoxItem item in cbM1.Properties.Items)                        
                //        if (item.CheckState == CheckState.Checked)
                //            M1Personal = M1Personal + item.Value.ToString() + ';';
                //    dict.Add("M1_ID", M1Personal);       

                return list;
            }
        }

        #endregion

        public void SetReportType(TableType tableType)
        {
            rbType.SelectedIndex = (tableType == TableType.Plan) ? 0 : 1;
        }

        public void SetDsmId(int dsmId)
        {
            for (int lIndex = 0; lIndex < cbM3.Properties.Items.Count; lIndex++)
            {
                ComboBoxItem lItem = (ComboBoxItem)cbM3.Properties.Items[lIndex];
                int lDsmId = (int)lItem.Id;
                if (lDsmId != dsmId)
                    continue;
                cbM3.SelectedItem = lItem;
            }
        }

        public void SelectAllM1()
        {
            foreach (CheckedListBoxItem lItem in cbM1.Properties.Items)
                lItem.CheckState = CheckState.Checked;
        }

        public void SelectAllM2()
        {
            foreach (CheckedListBoxItem lItem in cbM2.Properties.Items)
                lItem.CheckState = CheckState.Checked;
        }

        public void SelectM2(int id)
        {
            for (int lIndex = 0; lIndex < cbM2.Properties.Items.Count; lIndex++)
            {
                CheckedListBoxItem lItem = cbM2.Properties.Items[lIndex];
                int lId = (int)lItem.Value;
                if (lId != id)
                    continue;
                lItem.CheckState = CheckState.Checked;
                break;
            }
        }

        public void SetParent(BaseReportUserControl parent)
        {
            _bruc = parent;
            _reportTabs = _bruc.Report.Tabs;
        }

        private SheetParamCollection FillParamCol(Guid id, string merchName, object merchId)
        {
            SheetParamCollection paramCollection = new SheetParamCollection
            {
                MainHeader = Resource.MainHeader_M1,
                TabId = id,
                TableDataType =
                    rbType.SelectedIndex == 0
                        ? TableType.Plan
                        : TableType.Fact
            };

            if (!merchName.Equals(""))
            {
                paramCollection.Add(new SheetParam { SqlParamName = "@Merch_ID", Value = merchId, DisplayParamName = Resource.M1, DisplayValue = merchName });
                paramCollection.Add(new SheetParam { SqlParamName = "@Date", Value = DateParse(ReportDate) });
                if (rbType.SelectedIndex == 1)
                {
                    paramCollection.Add(new SheetParam { SqlParamName = "@Merch_ID", Value = null });
                    GetDateInfo(paramCollection);
                }
            }
            else
            {
                //paramCollection.Add(new SheetParam { DisplayParamName = Resource.M2, SqlParamName = "@Supervisor_id", Value = ((ComboBoxItem)cbM2.SelectedItem).Id, DisplayValue = ((ComboBoxItem)cbM2.SelectedItem).Text });
                GetDateInfo(paramCollection);
                paramCollection.Add(new SheetParam { DisplayParamName = "Дата:", SqlParamName = "@Date", Value = ReportDate, DisplayValue = ReportDate.ToShortDateString() });
            }

            if (rbType.SelectedIndex == 0)
            {
                paramCollection.Add(new SheetParam
                {
                    SqlParamName = "@StartDate",
                    Value = _dtStart,
                    DisplayParamName = Resource.ReportMonth,
                    DisplayValue = _dtStart.ToString("MMMM yyyy")
                });
                paramCollection.Add(new SheetParam { SqlParamName = "@EndDate", Value = _dtEnd });
            }


            return paramCollection;
        }

        private SheetParamCollection FillParamColM2(Guid id, string svName, object svId)
        {
            SheetParamCollection paramCollection = new SheetParamCollection
            {
                MainHeader = Resource.MainHeader_M2,
                TabId = id,
                TableDataType =
                    rbType.SelectedIndex == 0
                        ? TableType.Plan
                        : TableType.Fact
            };
            if (!svName.Equals(""))
            {
                paramCollection.Add(new SheetParam { SqlParamName = "@Supervisor_Id", Value = svId, DisplayParamName = Resource.M2, DisplayValue = svName });
                paramCollection.Add(new SheetParam { SqlParamName = "@date", Value = DateParse(ReportDate) });
                if (rbType.SelectedIndex == 1)
                {
                    paramCollection.Add(new SheetParam { SqlParamName = "@Supervisor_Id", Value = null });
                    GetDateInfo(paramCollection);
                }
            }
            else
            {
                paramCollection.Add(new SheetParam
                {
                    DisplayParamName = "Дата:",
                    SqlParamName = "@date",
                    Value = ReportDate,
                    DisplayValue = ReportDate.ToShortDateString()
                });
            }

            if (rbType.SelectedIndex == 0)
            {
                paramCollection.Add(new SheetParam
                {
                    SqlParamName = "@StartDate",
                    Value = _dtStart,
                    DisplayParamName = Resource.ReportMonth,
                    DisplayValue = _dtStart.ToString("MMMM yyyy")
                });
                paramCollection.Add(new SheetParam { SqlParamName = "@EndDate", Value = _dtEnd });
            }

            return paramCollection;
        }

        private SheetParamCollection FillParamColM3(Guid id, string dsmName, object dsmId)
        {
            SheetParamCollection paramCollection = new SheetParamCollection
            {
                MainHeader = Resource.MainHeader_M3,
                TabId = id,
                TableDataType =
                    rbType.SelectedIndex == 0
                        ? TableType.Plan
                        : TableType.Fact
            };

            if (!dsmName.Equals(""))
            {
                paramCollection.Add(new SheetParam { SqlParamName = "@Dsm_id", Value = dsmId, DisplayParamName = Resource.M3, DisplayValue = dsmName });
                if (rbType.SelectedIndex == 1)
                {
                    paramCollection.Add(new SheetParam { SqlParamName = "@Dsm_id", Value = null });
                    paramCollection.Add(new SheetParam { SqlParamName = "@date", Value = DateParse(dateEdit.DateTime) });
                    GetDateInfo(paramCollection);
                }
            }

            if (rbType.SelectedIndex == 0)
            {
                paramCollection.Add(new SheetParam
                {
                    SqlParamName = "@StartDate",
                    Value = _dtStart,
                    DisplayParamName = Resource.ReportMonth,
                    DisplayValue = _dtStart.ToString("MMMM yyyy")
                });
                paramCollection.Add(new SheetParam { SqlParamName = "@EndDate", Value = _dtEnd });
            }

            return paramCollection;
        }

        private void GetDateInfo(SheetParamCollection paramCollection)
        {
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.ReportDay, DisplayValue = Convert.ToDateTime(_dataTableDate.Rows[0]["CurrentDate"]).ToShortDateString() });
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.ReportMonth, DisplayValue = Convert.ToDateTime(_dataTableDate.Rows[0]["CurrentDate"]).ToString("MM yyyy") });

            decimal daysInMonth = ConvertEx.ToDecimal(_dataTableDate.Rows[0]["WorkDaysInMonth"]);
            decimal workDay = ConvertEx.ToDecimal(_dataTableDate.Rows[0]["WorkDay"]);
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.WorkDays, DisplayValue = string.Format(Math.Truncate(daysInMonth) != daysInMonth ? "{0:N1}" : "{0:N0}", daysInMonth) });
            paramCollection.Add(new SheetParam { DisplayParamName = Resource.WorkDay, DisplayValue = string.Format(Math.Truncate(workDay) != workDay ? "{0:N1}" : "{0:N0}", workDay) });
        }

        private DataTable getTable(string procName, string sqlParam, object value)
        {
            DataTable dt = null;
            try
            {
                DataAccessLayer.OpenConnection();
                dt = DataAccessLayer.ExecuteStoredProcedure(procName, new[] { new SqlParameter(sqlParam, value) }).Tables[0];
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }
            finally
            {
                DataAccessLayer.CloseConnection();
            }

            return dt;
        }


        /// <summary>
        /// Handles the Load event of the M3FSMSettingsForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void M3FSMSettingsForm_Load(object sender, EventArgs e)
        {
            UpdateControls();
        }

        /// <summary>
        /// Updates the controls.
        /// </summary>
        public void UpdateControls()
        {
            //DataTable dataTableM3Users = null;
            //DataTable dataTableM2Users = null;

            if (string.IsNullOrEmpty(dateEdit.Text))
            {
                dateEdit.DateTime = DateTime.Now;
            }

            //Очищаэмо Combobox для персоналу на ОНП
            cbM3.Properties.Items.Clear();
            cbM2.Properties.Items.Clear();
            cbM1.Properties.Items.Clear();

            //Заповнюэмо персонал рівня М3
            FillM3();
            PersonalM2M1();
            GetDefaultSettings();

        }

        private void PersonalM2M1()
        {
            // Очищаэмо списки персоналу рівня М2 та М1
            cbM2.Properties.Items.Clear();
            cbM1.Properties.Items.Clear();

            // Заповнюємо список персоналу рівня М2
            try
            {
                _dataTableM2Users = DataAccessProvider.GetPersonnel("M2", ConvertEx.ToLongInt(DsmId), ReportDate);
                foreach (DataRow dataRow in _dataTableM2Users.Rows)
                {
                    cbM2.Properties.Items.Add(dataRow["Staff_ID"], dataRow["Staff_NAME"].ToString(), CheckState.Unchecked, true);
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            // Заповнюємо список персоналу рівня М1
            try
            {
                foreach (DataRow dataRow2 in _dataTableM2Users.Rows)
                {
                    _dataTableM1Users = DataAccessProvider.GetPersonnel("M1", ConvertEx.ToInt(dataRow2["Staff_ID"]), dateEdit.DateTime.Date);
                    foreach (DataRow dataRow in _dataTableM1Users.Rows)
                    {
                        cbM1.Properties.Items.Add(dataRow["Staff_ID"], dataRow["Staff_NAME"].ToString(), CheckState.Unchecked, true);
                    }
                    _dataTableM1Users = null;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }


        private void FillM3()
        {
            DataTable dataTableM3Users = null;
            try
            {
                dataTableM3Users = DataAccessProvider.GetPersonnel("M3", 0, dateEdit.DateTime.Date);
                foreach (DataRow dataRow in dataTableM3Users.Rows)
                {
                    cbM3.Properties.Items.Add(new ComboBoxItem { Id = dataRow["Staff_ID"], Text = dataRow["Staff_NAME"].ToString() });
                }
                if (cbM3.SelectedItem == null)
                    cbM3.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }

        private static string DaysToString(decimal value)
        {
            return value.ToString(value % 1 == 0 ? "N0" : "N1");
        }

        public bool AddRatings()
        {
            if (rbType.SelectedIndex == 1 && dateEdit.DateTime.DayOfWeek != DayOfWeek.Saturday &&
                dateEdit.DateTime.DayOfWeek != DayOfWeek.Sunday)
                return true;
            return false;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {

            string M2Personal = "";
            string M1Personal = "";

            if (dateEdit.DateTime.Date == DateTime.MinValue)
                XtraMessageBox.Show(Resource.Incorrect_Date);
            else if (rbType.SelectedIndex == 0 && (dateEdit.DateTime.Month < DateTime.Now.Month
                 || dateEdit.DateTime.Year < DateTime.Now.Year))
            {
                XtraMessageBox.Show(Resource.Error_Date);
            }
            else if (cbM3.SelectedIndex == -1)
            {
                XtraMessageBox.Show(Resource.M3_error);
            }
            else
            {
                try
                {
                    _dict.Clear();
                    _dict.Add("M3_ID", ((ComboBoxItem)cbM3.SelectedItem).Id.ToString());

                    foreach (CheckedListBoxItem item in cbM2.Properties.Items)
                        if (item.CheckState == CheckState.Checked)
                            M2Personal = M2Personal + item.Value.ToString() + ';';
                    _dict.Add("M2_ID", M2Personal);
                    foreach (CheckedListBoxItem item in cbM1.Properties.Items)
                        if (item.CheckState == CheckState.Checked)
                            M1Personal = M1Personal + item.Value.ToString() + ';';
                    _dict.Add("M1_ID", M1Personal);

                    SettingsAccessor.SaveSettings(_dict, ReportId);
                }
                catch (Exception)
                {
                }
                DialogResult = DialogResult.OK;
            }
        }

        private DateTime DateParse(DateTime input_date)
        {
            string result_str = input_date.ToString("yyyy-MM-dd");
            DateTime parsed = DateTime.ParseExact(result_str, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            return parsed;
        }

        private void GetDefaultSettings()
        {
            string id = "";
            _dict = SettingsAccessor.GetSettings(ReportId);
            foreach (KeyValuePair<string, string> kvp in _dict)
            {
                if (kvp.Key.Equals("M3_ID"))
                {
                    id = kvp.Value;
                    break;
                }
            }
            if (!id.Equals(""))
            {
                foreach (ComboBoxItem item in cbM3.Properties.Items)
                {
                    if (item.Id.ToString() == id)
                    {
                        cbM3.SelectedIndex = cbM3.Properties.Items.IndexOf(item);
                        break;
                    }
                }
            }

            //Заповнюємо збережену історію персоналу М2
            char[] delimiterChars = { ';' };
            id = "";
            _dict = SettingsAccessor.GetSettings(ReportId);
            foreach (KeyValuePair<string, string> kvp in _dict)
            {
                if (kvp.Key.Equals("M2_ID"))
                {
                    id = kvp.Value;
                    string[] ListId = id.Split(delimiterChars);

                    foreach (string ID in ListId)
                    {
                        foreach (CheckedListBoxItem item in cbM2.Properties.Items)
                        {
                            if (item.Value.ToString() == ID)
                            {
                                item.CheckState = CheckState.Checked;
                                break;
                            }
                        }
                    }
                    break;
                }
            }

            //Заповнюємо збережену історію персоналу М1           
            id = "";
            _dict = SettingsAccessor.GetSettings(ReportId);
            foreach (KeyValuePair<string, string> kvp in _dict)
            {
                if (kvp.Key.Equals("M1_ID"))
                {
                    id = kvp.Value;
                    string[] ListId = id.Split(delimiterChars);

                    foreach (string ID in ListId)
                    {
                        foreach (CheckedListBoxItem item in cbM1.Properties.Items)
                        {
                            if (item.Value.ToString() == ID)
                            {
                                item.CheckState = CheckState.Checked;
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }

        private void lblReport_Click(object sender, EventArgs e)
        {
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void checkedComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            //RefreshM1();
        }

        private void cbM3_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalM2M1();
        }

        private void cbM3_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
        }

        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void cbM1_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void dateEdit_DateTimeChanged(object sender, EventArgs e)
        {
            if (dateEdit.DateTime.Date == DateTime.MinValue)
                XtraMessageBox.Show(Resource.Incorrect_Date);
            else
            {
                FillM3();
                PersonalM2M1();
            }
        }

    }


    /// <summary>
    /// ComboBoxItem
    /// </summary>
    internal class ComboBoxItem
    {
        private object _id;

        private string _text;

        public object Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public override string ToString()
        {
            return Text;
        }
    }
}