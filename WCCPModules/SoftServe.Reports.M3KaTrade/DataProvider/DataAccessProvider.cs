﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.M3KaTrade.DataProvider {
    internal class DataAccessProvider {
        /// <summary>
        /// Returns KA Trade personnel by Level and Parent personnel Id and date.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="parentId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DataTable GetPersonnel(string level, long parentId, DateTime date) {
            DataTable lResTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GetPersonalForONP,
                    new SqlParameter(SqlConstants.ParamStaff_Level, level),
                    new SqlParameter(SqlConstants.ParamParentId, parentId),
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lResTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lResTable;
        }

        public static DataTable GetNewStaffId(string level, string idListStr) {
            DataTable lResTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.GetNewStaffId,
                    new SqlParameter(SqlConstants.ParamStaff_Level, level),
                    new SqlParameter(SqlConstants.ParamIdListStr, idListStr));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lResTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lResTable;
        }

        public static DataTable GetSKUPlan(int dsmId, DateTime date) {
            DataTable lDataTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaTradeSKUPlanGet,
                    new SqlParameter(SqlConstants.ParamDsmId, dsmId),
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }

            return lDataTable;
        }

        public static DataTable GetIndicatorsPlan(int dsmId, DateTime date) {
            DataTable lDataTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaTradeIndicatorsPlanGet,
                    new SqlParameter(SqlConstants.ParamDsmId, dsmId),
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }

            return lDataTable;
        }

        public static DataTable GetNetworksPlan(int dsmId, DateTime date) {
            DataTable lDataTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaTradeNetworkPlanGet,
                    new SqlParameter(SqlConstants.ParamDsmId, dsmId),
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }

            return lDataTable;
        }

        public static void SaveSKUPlanChangedRow(DataRow row, DateTime reportDate) {
            DataAccessLayer.OpenConnection();
            try {
                DateTime lDate = new DateTime(reportDate.Year, reportDate.Month, 1);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpKaTradeSKUPlanPut,
                    new SqlParameter(SqlConstants.ParamStaffLevel, row[SqlConstants.StaffLevelFieldName]),
                    new SqlParameter(SqlConstants.ParamStaffId, row[SqlConstants.StaffIdFieldName]),
                    new SqlParameter(SqlConstants.ParamProductCnTopId, row[SqlConstants.ProductCnTopIdFieldName]),
                    new SqlParameter(SqlConstants.ParamTargetSaleMonthly, row[SqlConstants.TargetSaleMonthlyFieldName]),
                    new SqlParameter(SqlConstants.ParamTargetNumATT, row[SqlConstants.TargetNumATTFieldName]),
                    new SqlParameter(SqlConstants.ParamStartDate, lDate));
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
        }

        public static void SaveIndicatorChangedRow(DataRow row, DateTime reportDate) {
            DataAccessLayer.OpenConnection();
            try {
                DateTime lDate = new DateTime(reportDate.Year, reportDate.Month, 1);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpKaTradeIndicatorsPlanPut,
                    new SqlParameter(SqlConstants.ParamStaffLevel, row[SqlConstants.StaffLevelFieldName]),
                    new SqlParameter(SqlConstants.ParamStaffId, row[SqlConstants.StaffIdFieldName]),
                    new SqlParameter(SqlConstants.ParamStrikeRate, row[SqlConstants.StrikeRateFieldName]),
                    new SqlParameter(SqlConstants.ParamPlanStrikeRate, row[SqlConstants.PlanStrikeRateFieldName]),
                    new SqlParameter(SqlConstants.ParamMiddleTimeinField, row[SqlConstants.MiddleTimeinFieldFieldName]),
                    new SqlParameter(SqlConstants.ParamEquipmentEff, row[SqlConstants.EquipmentEffFieldName]),
                    new SqlParameter(SqlConstants.ParamEquipmentOutletEff, row[SqlConstants.EquipmentOutletEffFieldName]),
                    new SqlParameter(SqlConstants.ParamEquipmentOutletNullSales, row[SqlConstants.EquipmentOutletNullSalesFieldName]),
                    new SqlParameter(SqlConstants.ParamMiddleCountAllVisits, row[SqlConstants.MiddleCountAllVisitsFieldName]),
                    new SqlParameter(SqlConstants.ParamMiddleCountVisitsByRoute, row[SqlConstants.MiddleCountVisitsByRouteFieldName]),
                    new SqlParameter(SqlConstants.ParamEffVolume, row[SqlConstants.EffVolumeFieldName]),
                    new SqlParameter(SqlConstants.ParamEffOrder, row[SqlConstants.EffOrderFieldName]),
                    new SqlParameter(SqlConstants.ParamMixOnDate, row[SqlConstants.MixOnDateFieldName]),
                    new SqlParameter(SqlConstants.ParamMiddleSku, row[SqlConstants.MiddleSkuFieldName]),
                    new SqlParameter(SqlConstants.ParamPoceBsm, row[SqlConstants.PoceBsmFieldName]),
                    new SqlParameter(SqlConstants.ParamPoceSd, row[SqlConstants.PoceSdFieldName]),
                    new SqlParameter(SqlConstants.ParamStartDate, lDate));
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
        }

        public static void SaveNetworkChangedRow(DataRow row, DateTime reportDate) {
            DataAccessLayer.OpenConnection();
            try {
                DateTime lDate = new DateTime(reportDate.Year, reportDate.Month, 1);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpKaTradeNetworkPlanPut,
                    new SqlParameter(SqlConstants.ParamStaffLevel, row[SqlConstants.StaffLevelFieldName]),
                    new SqlParameter(SqlConstants.ParamStaffId, row[SqlConstants.StaffIdFieldName]),
                    new SqlParameter(SqlConstants.ParamNetworkId, row[SqlConstants.NetworkIdFieldName]),
                    new SqlParameter(SqlConstants.ParamTargetSaleMonthly, row[SqlConstants.TargetSaleMonthlyFieldName]),
                    new SqlParameter(SqlConstants.ParamTargetMixMonthly, row[SqlConstants.TargetMixMonthlyFieldName]),
                    new SqlParameter(SqlConstants.ParamTargetAvgSkuMonthly, row[SqlConstants.TargetAvgSkuMonthlyFieldName]),
                    new SqlParameter(SqlConstants.ParamTargetPoceBsmMonthly, row[SqlConstants.TargetPoceBsmMonthlyFieldName]),
                    new SqlParameter(SqlConstants.ParamTargetPoceSdMonthly, row[SqlConstants.TargetPoceSdMonthlyFieldName]),
                    new SqlParameter(SqlConstants.ParamStartDate, lDate));
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
        }

        public static DataTable GetSKUList(DateTime date) {
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaGetSku,
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lDataTable;
        }

        public static DataTable GetSKUTree(int prodCnTopId) {
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaGetSkuTree,
                    new SqlParameter(SqlConstants.ParamProductCnTopId, prodCnTopId));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lDataTable;
        }

        public static int DeleteProductCnTop(int prodCnTopId, bool forceDelete = false) {
            int lResult = 0;
            DataAccessLayer.OpenConnection();
            try {
                SqlParameter lParamErrCode = new SqlParameter(SqlConstants.ParamErrorCode, 0);
                lParamErrCode.Direction = ParameterDirection.Output;
                SqlParameterCollection lOutput = DataAccessLayer.ExecuteNonQueryStoredProcedureWithOutput(SqlConstants.SpKaDeleteSku,
                    new SqlParameter(SqlConstants.ParamProductCnTopId, prodCnTopId),
                    new SqlParameter(SqlConstants.ParamForceDelete, forceDelete),
                    lParamErrCode);

                if (null != lOutput && lOutput.Contains(SqlConstants.ParamErrorCode))
                    lResult = ConvertEx.ToInt(lOutput[SqlConstants.ParamErrorCode].Value);
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lResult;
        }

        public static int SaveSkuConfig(SkuConfigModel skuConfig, DateTime date) {
            int lResult = 0;
            DataAccessLayer.OpenConnection();
            try {
                SqlParameter lParamProdCnTopId = new SqlParameter(SqlConstants.ParamProductCnTopId, skuConfig.Id);
                lParamProdCnTopId.Direction = ParameterDirection.InputOutput;
                DateTime lDate;
                if (skuConfig.IsNew())
                    lDate = DateTime.Today;
                else
                    lDate = skuConfig.StartDate;
                //set first day of month
                lDate = lDate.AddDays(-lDate.Day + 1);

                SqlParameterCollection lOutput = DataAccessLayer.ExecuteNonQueryStoredProcedureWithOutput(SqlConstants.SpKaInsertSku,
                    lParamProdCnTopId,
                    new SqlParameter(SqlConstants.ParamGroupName, skuConfig.Name),
                    new SqlParameter(SqlConstants.ParamRegionId, skuConfig.RegionId),
                    new SqlParameter(SqlConstants.ParamStartDate, lDate),
                    new SqlParameter(SqlConstants.ParamGroupRowNum, skuConfig.GroupRowNum));

                if (null != lOutput && lOutput.Contains(SqlConstants.ParamProductCnTopId))
                    lResult = ConvertEx.ToInt(lOutput[SqlConstants.ParamProductCnTopId].Value);
                if (skuConfig.Id < 0) {
                    skuConfig.Id = lResult;
                    skuConfig.StartDate = lDate;
                }
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lResult;
        }

        public static void SaveSKUTree(int prodCnTopId, SqlXml sqlXml) {
            DataAccessLayer.OpenConnection();
            try {
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpKaUpdateSku,
                    new SqlParameter(SqlConstants.ParamProductCnTopId, prodCnTopId),
                    new SqlParameter(SqlConstants.ParamData, sqlXml));
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
        }

        public static DataTable GetKpiList(int dsmId, DateTime date) {
            DataTable lDataTable = null;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaKpiGet,
                    new SqlParameter(SqlConstants.ParamDsmId, dsmId),
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }

            return lDataTable;
        }

        public static void SaveKpiSettings(int dsmId, int productCnTopId1, int productCnTopId2, int type1, int type2, DateTime date) {
            DataAccessLayer.OpenConnection();
            try {
                DateTime lDate = new DateTime(date.Year, date.Month, 1);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpKaKpiPut,
                    new SqlParameter(SqlConstants.ParamDsmId, dsmId),
                    new SqlParameter(SqlConstants.ParamProductCnTopId1, productCnTopId1),
                    new SqlParameter(SqlConstants.ParamProductCnTopId2, productCnTopId2),
                    new SqlParameter(SqlConstants.ParamType1, type1),
                    new SqlParameter(SqlConstants.ParamType2, type2),
                    new SqlParameter(SqlConstants.ParamStartDate, lDate));
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
        }

        public static string GetFocusKpiText(int dsmId, DateTime date) {
            DataTable lDataTable = null;
            string lResult = string.Empty;

            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaTradeFocusKpiMonthGet,
                    new SqlParameter(SqlConstants.ParamId, dsmId),
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
                if (lDataTable != null) {
                    if (lDataTable.Rows.Count > 0)
                        lResult = lDataTable.Rows[0][SqlConstants.FocusTextFieldName].ToString();
                }
            }
            finally {
                DataAccessLayer.CloseConnection();
            }

            return lResult;
        }

        public static void SaveFocusKpi(int dsmId, DateTime date, string focusKpiText) {
            DataAccessLayer.OpenConnection();
            try {
                DateTime lDate = new DateTime(date.Year, date.Month, 1);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SqlConstants.SpKaTradeFocusKpiMonthPut,
                    new SqlParameter(SqlConstants.ParamDsmId, dsmId),
                    new SqlParameter(SqlConstants.ParamFocusText, focusKpiText),
                    new SqlParameter(SqlConstants.ParamDate, lDate));
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
        }

        public static DataTable GetSKUListDetails(DateTime date) {
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpKaGetSkuDetails,
                    new SqlParameter(SqlConstants.ParamDate, date));

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lDataTable;
        }

        public static int GetUserRegionId() {
            int lResult = -1;
            DataTable lDataTable = null;
            DataAccessLayer.OpenConnection();
            try {
                DataSet lDataSet = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.SpDwGetUserAccesibleRegions, new SqlParameter[] {});

                if (null != lDataSet && lDataSet.Tables.Count > 0)
                    lDataTable = lDataSet.Tables[0];
                if (lDataTable == null)
                    return lResult;
                if (lDataTable.Rows.Count < 1)
                    return lResult;
                lResult = ConvertEx.ToInt(lDataTable.Rows[0][0]);
            }
            finally {
                DataAccessLayer.CloseConnection();
            }
            return lResult;
        }
    }
}