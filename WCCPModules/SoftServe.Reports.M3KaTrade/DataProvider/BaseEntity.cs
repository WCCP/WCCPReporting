namespace SoftServe.Reports.M3KaTrade.DataProvider {
    public class BaseEntity {
        private bool _isNew;
        private bool _isDeleted;
        private bool _isModified;
        
        private int _id;
        private string _name;

        public BaseEntity() : this(0, string.Empty) {
        }

        public BaseEntity(int id, string name) {
            _id = id;
            _name = name;
        }

        #region Properties

        public int Id {
            get { return GetId(); }
            set { SetId(value); }
        }

        public string Name {
            get { return GetName(); }
            set { SetName(value); }
        }

        #endregion

        #region Getters and Setters

        public virtual int GetId() {
            return _id;
        }

        public virtual void SetId(int id) {
            if (Equals(_id, id))
                return;
            _id = id;
            MakeModified();
        }

        public virtual string GetName () {
            return _name;
        }

        public virtual void SetName(string name) {
            if (Equals(name, _name))
                return;
            _name = name;
            MakeModified();
        }

        #endregion


        public virtual bool IsNew() {
            return _isNew;
        }

        public virtual void MakeNew() {
            _isNew = true;
        }
        
        public virtual bool IsModified() {
            return _isNew || _isModified;
        }

        public virtual void MakeModified() {
            _isModified = true;
        }

        public virtual bool IsDeleted() {
            return _isDeleted;
        }

        public virtual void Delete() {
            _isDeleted = true;
        }

        public virtual void Undelete() {
            _isDeleted = false;
        }



        public virtual void Cancel() {
            _isModified = false;
            _isDeleted = false;
        }

        public virtual bool Apply() {
            _isNew = false;
            _isModified = false;
            System.Diagnostics.Debug.Assert(!IsModified());
            return true;
        }

        public virtual void ClearKey() {
            _isNew = true;
            _id = 0;
        }

        public virtual void Clear() {
            _isDeleted = false;
            _isModified = false;
            _isNew = false;
        }

        public virtual bool Assign(BaseEntity entity) {
            BaseEntity lEntity = entity;
            if (lEntity != null) {
                _isNew = lEntity._isNew;
                _isDeleted = lEntity._isDeleted;
                _isModified = lEntity._isModified;
                SetId(lEntity.Id);
                SetName(lEntity.Name);
                //System.Diagnostics.Debug.Assert(Equals(this, entity));
                return true;
            }
            return false;
        }

        public virtual BaseEntity Clone() {
            BaseEntity lEntity = new BaseEntity(_id, _name);
            lEntity.Assign(this);
            return lEntity;
        }

        public override bool Equals(object obj) {
            BaseEntity lEntity = obj as BaseEntity;
            if (lEntity != null) {
                return Equals(_isNew, lEntity._isNew) && Equals(_isDeleted, lEntity._isDeleted) && Equals(_isModified, lEntity._isModified)
                    && Equals(_id, lEntity._id) && Equals(_name, lEntity._name);
            }
            return true;
        }

        public bool Equals(BaseEntity baseEntity) {
            if (ReferenceEquals(null, baseEntity))
                return false;
            if (ReferenceEquals(this, baseEntity))
                return true;
            return Equals(_isNew, baseEntity._isNew) && Equals(_isDeleted, baseEntity._isDeleted) && Equals(_isModified, baseEntity._isModified)
                    && Equals(_id, baseEntity._id) && Equals(_name, baseEntity._name);
        }

        public override int GetHashCode () {
            return _id;
        }

        public override string ToString() {
            return string.IsNullOrEmpty(Name) ? string.Empty : Name;
        }
    }
}