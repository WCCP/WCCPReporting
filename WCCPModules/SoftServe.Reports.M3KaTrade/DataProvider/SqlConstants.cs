﻿using System;
using System.Drawing;

namespace SoftServe.Reports.M3KaTrade.DataProvider {
    public class SqlConstants {
        public static Brush BrushGray = new SolidBrush(SystemColors.ControlLight);
        public static string DELIMITER_ID_LIST = ",";

        // SP names
        public const string GetPersonalForONP = "spDW_M1_KaTrade_GetStaffByLevel";
        public const string GetNewStaffId = "spDW_KaTrade_GetNewStaffId";
        public const string SpKaTradeSKUPlanGet = "spDW_KaTrade_SKUPlanGet";
        public const string SpKaTradeSKUPlanPut = "spDW_KaTrade_SKUPlanPut";
        public const string SpKaTradeIndicatorsPlanGet = "spDW_KaTrade_IndicatorsPlanGet";
        public const string SpKaTradeIndicatorsPlanPut = "spDW_KaTrade_MonthlyKPIPlanPut";
        public const string SpKaTradeNetworkPlanGet = "spDW_KaTrade_NetworkPlanGet";
        public const string SpKaTradeNetworkPlanPut = "spDW_KaTrade_NetworkPlanPut";
        public const string SpKaGetSku = "spDW_Ka_GetSKU";
        public const string SpKaGetSkuDetails = "spDW_Ka_GetSKUDetails";
        public const string SpKaGetSkuTree = "spDW_Ka_GetSKUTree";
        public const string SpKaInsertSku = "spDW_Ka_InsertSKU";
        public const string SpKaDeleteSku = "spDW_Ka_DeleteSKU";
        public const string SpKaUpdateSku = "spDW_Ka_UpdateSKU";
        public const string SpKaKpiGet = "spDW_Ka_KpiGet";
        public const string SpKaKpiPut = "spDW_Ka_KpiPut";
        public const string SpKaTradeFocusKpiMonthGet = "spDW_M1_KaTrade_FocusKPIMonthGetList";
        public const string SpKaTradeFocusKpiMonthPut = "spDW_M1_KaTrade_FocusKPIMonthPut";
        public const string SpDwGetUserAccesibleRegions = "spDW_GetUserAccesibleRegions";

        // Field Names
        public const string IdFieldName = "Id";
        public const string StaffLevelFieldName = "Staff_Level";
        public const string StaffIdFieldName = "Staff_ID";
        public const string StartDateFieldName = "StartDate";
        public const string ProductCnTopIdFieldName = "ProductCnTop_ID";
        public const string TargetSaleMonthlyFieldName = "TargetSaleMonthly";
        public const string TargetNumATTFieldName = "TargetNumATT";
        public const string StrikeRateFieldName = "StrikeRate";
        public const string PlanStrikeRateFieldName = "PlanStrikeRate";
        public const string EquipmentEffFieldName = "EquipmentEff";
        public const string EquipmentOutletEffFieldName = "EquipmentOutletEff";
        public const string EquipmentOutletNullSalesFieldName = "EquipmentOutletNullSales";
        public const string PoceBsmFieldName = "POCeBSM";
        public const string PoceSdFieldName = "POCeSD";
        public const string MixOnDateFieldName = "MixOnDate";
        public const string MiddleCountAllVisitsFieldName = "MiddleCountAllVisits";
        public const string MiddleCountVisitsByRouteFieldName = "MiddleCountVisitsByRoute";
        public const string EffVolumeFieldName = "EffVolume";
        public const string EffOrderFieldName = "EffOrder";
        public const string MiddleSkuFieldName = "MiddleSku";
        public const string MiddleTimeinFieldFieldName = "MiddleTimeinField";
        public const string NetworkIdFieldName = "Network_ID";
        public const string TargetMixMonthlyFieldName = "TargetMixMonthly";
        public const string TargetAvgSkuMonthlyFieldName = "TargetAvgSkuMonthly";
        public const string TargetPoceBsmMonthlyFieldName = "TargetPoceBCMMonthly";
        public const string TargetPoceSdMonthlyFieldName = "TargetPoceCDMonthly";
        public const string MiddleCountVisitsDayFieldName = "MiddleCountVisitsDay";
        public const string GroupNameFieldName = "GroupName";
        public const string InternalStatusMaskFieldName = "InternalStatusMask";
        public const string EditableFieldName = "Editable";
        public const string RegionIdFieldName = "Region_ID";
        public const string GroupRowNumFieldName = "GroupRowNum";
        public const string Type1FieldName = "Type1";
        public const string Type2FieldName = "Type2";
        public const string TypeName1FieldName = "TypeName1";
        public const string TypeName2FieldName = "TypeName2";
        public const string ProductCnTopId1FieldName = "ProductCnTop_ID1";
        public const string ProductCnTopId2FieldName = "ProductCnTop_ID2";
        public const string GroupName1FieldName = "GroupName1";
        public const string GroupName2FieldName = "GroupName2";
        public const string Kpi1FieldName = "Kpi1";
        public const string Kpi2FieldName = "Kpi2";
        public const string FocusTextFieldName = "Focus_Text";
        public const string ItemIdFieldName = "Item_ID";
        public const string LevelFieldName = "Level";

        // SP Params
        public const string ParamId = "@ID";
        public const string ParamDsmId = "@DSM_ID";
        public const string ParamSvId = "@Supervisor_ID";
        public const string ParamReportDate = "@ReportDate";
        public const string ParamStaff_Level = "@StaffLevel";
        public const string ParamIdListStr = "@idListStr";
        public const string ParamParentId = "@Parent_ID";
        public const string ParamDate = "@date";
        public const string ParamStaffLevel = "@Staff_level";
        public const string ParamStaffId = "@Staff_ID";
        public const string ParamStartDate = "@StartDate";
        public const string ParamProductCnTopId = "@ProductCnTop_ID";
        public const string ParamTargetSaleMonthly = "@TargetSaleMonthly";
        public const string ParamTargetNumATT = "@TargetNumATT";
        public const string ParamStrikeRate = "@StrikeRate";
        public const string ParamPlanStrikeRate = "@PlanStrikeRate";
        public const string ParamMiddleTimeinField = "@MiddleTimeinField";
        public const string ParamEquipmentEff = "@EquipmentEff";
        public const string ParamEquipmentOutletEff = "@EquipmentOutletEff";
        public const string ParamEquipmentOutletNullSales = "@EquipmentOutletNullSales";
        public const string ParamMiddleCountAllVisits = "@MiddleCountAllVisits";
        public const string ParamMiddleCountVisitsByRoute = "@MiddleCountVisitsByRoute";
        public const string ParamEffVolume = "@EffVolume";
        public const string ParamEffOrder = "@EffOrder";
        public const string ParamMixOnDate = "@MixOnDate";
        public const string ParamMiddleSku = "@MiddleSku";
        public const string ParamPoceBsm = "@POCeBSM";
        public const string ParamPoceSd = "@POCeSD";
        public const string ParamNetworkId = "@Network_ID";
        public const string ParamTargetMixMonthly = "@TargetMixMonthly";
        public const string ParamTargetAvgSkuMonthly = "@TargetAvgSkuMonthly";
        public const string ParamTargetPoceBsmMonthly = "@TargetPoceBCMMonthly";
        public const string ParamTargetPoceSdMonthly = "@TargetPoceCDMonthly";
        public const string ParamErrorCode = "@ERROR_CODE";
        public const string ParamGroupName = "@GroupName";
        public const string ParamGroupRowNum = "@GroupRowNum";
        public const string ParamRegionId = "@Region_ID";
        public const string ParamData = "@Data";
        public const string ParamProductCnTopId1 = "@ProductCnTop_Id1";
        public const string ParamProductCnTopId2 = "@ProductCnTop_Id2";
        public const string ParamType1 = "@Type1";
        public const string ParamType2 = "@Type2";
        public const string ParamFocusText = "@Focus_Text";
        public const string ParamForceDelete = "@force_delete";
        //public const string Param = "";

        internal static Guid M1ATabID = new Guid("40783a26-7e2b-4366-8513-121e8fabcb37");
        internal static Guid M1BTabID = new Guid("189f8ebe-6aa2-4139-b1f8-85296b3754ad");
        internal static Guid M21 = new Guid("6879a6a2-736e-47fb-b8d4-719a62fb79a0");
        internal static Guid M22 = new Guid("abfeebb4-b8a2-4522-9fc4-839ea324f060");
        internal static Guid M23 = new Guid("8c112c83-ed23-45b2-b4db-5ebf2a02b828");
        internal static Guid M31 = new Guid("a856a93c-e13b-41e0-ad2f-ffb54faec5b2");
        internal static Guid M32 = new Guid("b3205f40-d4fd-4bde-97cc-ca41c097f5ed");
        internal static Guid M33 = new Guid("43404325-f86f-4318-9698-b33c2d94602e");

        internal static string M1A = "M1_A";
        internal static string M1B = "M1_B";
        internal static string HideTable = "Планирование действий";

        public const string RB_SUMMARY = "РБ - Итого";
    }
}