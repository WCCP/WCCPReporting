using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.M3KaTrade.DataProvider {
    public class SkuConfigModel: BaseEntity {
        // List of ID. Id is the PK in SKU Tree data table, field Id.
        private List<int> _idList = new List<int>();
        private int _regionId;
        private DateTime _startDate;
        private double _groupRowNum;
        private bool _editable;
        private string _selectedIdList = string.Empty;
        private bool _isIdListModified;

        public SkuConfigModel(int id): this(id, string.Empty) {
        }

        public SkuConfigModel(int id, string name): base(id, name) {
            _startDate = DateTime.Today;
            _regionId = -1;
            _groupRowNum = -1;
            _editable = false;
            _selectedIdList = string.Empty;

            _isIdListModified = false;
        }

        public void AddIdToList(int id) {
            if (!_idList.Contains(id)) {
                _idList.Add(id);
                _isIdListModified = true;
                MakeModified();
            }
        }

        public void ClearIdList() {
            _idList.Clear();
            _selectedIdList = string.Empty;
        }

        public bool EqualsIdList(List<int> idList) {
            if (_idList.Count != idList.Count)
                return false;
            idList.Sort();
            _idList.Sort();
            for (int lIndex = 0; lIndex < _idList.Count; lIndex++)
                if (_idList[lIndex] != idList[lIndex])
                    return false;
            return true;
        }

        #region Properties

        public int RegionId {
            get { return GetRegionId(); }
            set { SetRegionId(value); }
        }

        public DateTime StartDate {
            get { return GetStartDate(); }
            set { SetStartDate(value); }
        }

        public double GroupRowNum {
            get { return GetGroupRowNum(); }
            set { SetGroupRowNum(value); }
        }

        public bool Editable {
            get { return GetEditable(); }
            set { SetEditable(value); }
        }

        public string SelectedIdList {
            get { return GetSelectedIdList(); }
            set { SetSelectedIdList(value); }
        }

        #endregion

        #region Getters and Setters

        public int GetRegionId() {
            return _regionId;
        }

        public void SetRegionId(int regionId) {
            if (Equals(regionId, _regionId))
                return;
            _regionId = regionId;
            MakeModified();
        }

        public DateTime GetStartDate() {
            return _startDate;
        }

        public void SetStartDate(DateTime startDate) {
            if (Equals(startDate, _startDate))
                return;
            _startDate = startDate;
            MakeModified();
        }

        public double GetGroupRowNum() {
            return _groupRowNum;
        }

        public void SetGroupRowNum(double groupRowNum) {
            if (Equals(groupRowNum, _groupRowNum))
                return;
            _groupRowNum = groupRowNum;
            MakeModified();
        }

        public bool GetEditable() {
            return _editable;
        }

        public void SetEditable(bool editable) {
            if (Equals(editable, _editable))
                return;
            _editable = editable;
            MakeModified();
        }

        public string GetSelectedIdList() {
            _idList.Sort();
            _selectedIdList = string.Empty;
            for (int lIndex = 0; lIndex < _idList.Count; lIndex++)
                _selectedIdList += "," + _idList[lIndex];
            _selectedIdList = _selectedIdList.TrimStart(',');
            return _selectedIdList;
        }

        public void SetSelectedIdList(string strIdList) {
            string[] lStrIdArray = strIdList.Split(SqlConstants.DELIMITER_ID_LIST.ToCharArray());
            List<int> lIdList = new List<int>(lStrIdArray.Length);
            if (strIdList != "") {
                for (int lIndex = 0; lIndex < lStrIdArray.Length; lIndex++) {
                    lIdList.Add(ConvertEx.ToInt(lStrIdArray[lIndex]));
                }
            }
            lIdList.Sort();
            _idList.Sort();
            if (EqualsIdList(lIdList))
                return;
            _idList.Clear();
            _idList.AddRange(lIdList);
            _isIdListModified = true;
            GetSelectedIdList();
            MakeModified();
        }

        #endregion

        public override bool Apply() {
            _isIdListModified = false;
            return base.Apply();
        }

        public override BaseEntity Clone() {
            SkuConfigModel lSkuConfig = new SkuConfigModel(Id);
            lSkuConfig.Assign(this);
            return lSkuConfig;
        }

        public override bool Assign(BaseEntity entity) {
            if (!(entity is SkuConfigModel))
                return false;
            SkuConfigModel lSkuConfig = (SkuConfigModel) entity;
            SetStartDate(lSkuConfig.GetStartDate());
            SetRegionId(lSkuConfig.GetRegionId());
            SetGroupRowNum(lSkuConfig.GetGroupRowNum());
            SetEditable(lSkuConfig.GetEditable());
            _idList.Clear();
            for (int lIndex = 0; lIndex < lSkuConfig._idList.Count; lIndex++)
                AddIdToList(lSkuConfig._idList[lIndex]);
            return base.Assign(entity);
        }

        public override bool Equals(object obj) {
            if (!(obj is SkuConfigModel))
                return false;

            SkuConfigModel lSkuConfig = (SkuConfigModel) obj;
            bool lResult = base.Equals(obj) &&
                Equals(_startDate, lSkuConfig._startDate) &&
                Equals(_regionId, lSkuConfig._regionId) &&
                Equals(_groupRowNum, lSkuConfig._groupRowNum) &&
                EqualsIdList(lSkuConfig._idList);
            return lResult;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public bool IsIdListModified() {
            return _isIdListModified;
        }
    }
}