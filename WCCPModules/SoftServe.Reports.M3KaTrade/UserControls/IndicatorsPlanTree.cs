﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.ViewInfo;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.M3KaTrade.DataProvider;

namespace SoftServe.Reports.M3KaTrade.UserControls {
    public partial class IndicatorsPlanTree: UserControl {
        private readonly List<int> _changedIds = new List<int>();
        
        private int DsmId { get; set; }
        private DateTime Date { get; set; }
        private decimal PreviousCellValue { get; set; }

        public IndicatorsPlanTree() {
            InitializeComponent();
        }

        public bool IsChanged {
            get { return _changedIds.Count > 0; }
        }

        public void LoadData(int dsmId, DateTime date) {
            DsmId = dsmId;
            Date = date;

            AttachEvents(false);
            tree.BeginUpdate();
            try {
                DataTable lDataTable = DataAccessProvider.GetIndicatorsPlan(DsmId, Date);
                if (lDataTable == null)
                    return;

                lDataTable.PrimaryKey = new[] {lDataTable.Columns[columnId.FieldName]};
                tree.DataSource = lDataTable;
            
                // Restore column settings
                CustomizeColumns();
            }
            finally {
                tree.EndUpdate();
                AttachEvents(true);
            }
        }

        private void CustomizeColumns() {
            // general fields
            RestoreFieldSettings(columnStaffName);
            //tree.Columns[columnStaffName.FieldName].Width = 200;
            tree.Columns[columnStaffId.FieldName].Visible = false;
            tree.Columns[columnStaffLevel.FieldName].Visible = false;
            tree.Columns[columnStaffParentId.FieldName].Visible = false;
            tree.Columns[columnStartDate.FieldName].Visible = false;

            //specific fields
//            tree.Columns[SqlConstants.MixOnDateFieldName].Visible = false;
//            tree.Columns[SqlConstants.MiddleSkuFieldName].Visible = false;
//            tree.Columns[SqlConstants.PoceBsmFieldName].Visible = false;
//            tree.Columns[SqlConstants.PoceSdFieldName].Visible = false;
            
            RestoreFieldSettings(columnMixOnDate, true);
            RestoreFieldSettings(columnMiddleSku, true);
            RestoreFieldSettings(columnStrikeRate, true);
            RestoreFieldSettings(columnPlanStrikeRate, true);
            RestoreFieldSettings(columnMiddleTimeinField, true);
            RestoreFieldSettings(columnEquipmentEff, true);
            RestoreFieldSettings(columnEquipmentOutletEff, true);
            RestoreFieldSettings(columnEquipmentOutletNullSales, true);
            RestoreFieldSettings(columnMiddleCountAllVisits, true);
            RestoreFieldSettings(columnMiddleCountVisitsByRoute, true);
            RestoreFieldSettings(columnEffVolume, true);
            RestoreFieldSettings(columnEffOrder, true);
            RestoreFieldSettings(columnPOCeBSM, true);
            RestoreFieldSettings(columnPOCeSD, true);
        }

        /// <summary>
        /// Restores the field settings.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="editable"></param>
        private void RestoreFieldSettings(TreeListColumn column, bool editable = false) {
            tree.Columns[column.FieldName].AppearanceHeader.TextOptions.HAlignment = column.AppearanceHeader.TextOptions.HAlignment;
            tree.Columns[column.FieldName].AppearanceHeader.TextOptions.VAlignment = column.AppearanceHeader.TextOptions.VAlignment;
            tree.Columns[column.FieldName].AppearanceHeader.TextOptions.WordWrap = column.AppearanceHeader.TextOptions.WordWrap;
            tree.Columns[column.FieldName].OptionsColumn.ReadOnly = !editable;
            tree.Columns[column.FieldName].OptionsColumn.AllowEdit = editable;
            tree.Columns[column.FieldName].Width = column.Width;
            tree.Columns[column.FieldName].Caption = column.Caption;
            tree.Columns[column.FieldName].Fixed = editable ? FixedStyle.None : FixedStyle.Left;
            if (editable) {
                if (column.FieldName == columnMiddleTimeinField.FieldName) {
                    tree.Columns[column.FieldName].ColumnEdit = repositoryItemTimeEdit;
                    tree.Columns[column.FieldName].Format.FormatType = FormatType.DateTime;
                    tree.Columns[column.FieldName].Format.FormatString = "HH:mm";
                }
                else {
                    tree.Columns[column.FieldName].ColumnEdit = repositoryItemN2Edit;
                    tree.Columns[column.FieldName].Format.FormatType = FormatType.Numeric;
                    tree.Columns[column.FieldName].Format.FormatString = "n2";
                }
            }
        }

        /// <summary>
        /// Attaches the events.
        /// </summary>
        /// <param name="attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach) {
                tree.CellValueChanged += tree_CellValueChanged;
            }
            else {
                tree.CellValueChanged -= tree_CellValueChanged;
            }
        }

        private bool IsEditableColumn(TreeListColumn column) {
            return (column.FieldName == columnMixOnDate.FieldName
                || column.FieldName == columnMiddleSku.FieldName
                || column.FieldName == columnStrikeRate.FieldName
                || column.FieldName == columnPlanStrikeRate.FieldName
                || column.FieldName == columnMiddleTimeinField.FieldName
                || column.FieldName == columnEquipmentEff.FieldName
                || column.FieldName == columnEquipmentOutletEff.FieldName
                || column.FieldName == columnEquipmentOutletNullSales.FieldName
                || column.FieldName == columnMiddleCountAllVisits.FieldName
                || column.FieldName == columnMiddleCountVisitsByRoute.FieldName
                || column.FieldName == columnEffVolume.FieldName
                || column.FieldName == columnEffOrder.FieldName
                || column.FieldName == columnPOCeBSM.FieldName
                || column.FieldName == columnPOCeSD.FieldName
                );
        }

        private bool IsCellEditAllowed(TreeListColumn column, TreeListNode node) {
            if (null == column || null == node)
                return false;

            if (IsEditableColumn(column))
                return true;

            return false;
        }

//        private void tree_CellValueChanging(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e) {
//            PreviousCellValue = ConvertEx.ToDecimal(e.Node.GetValue(e.Column));
//        }

        private void tree_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e) {
            if (!IsCellEditAllowed(e.Column, e.Node))
                return;

            tree.BeginUpdate();
            try {
                //save Id of changed row
                int lId = ConvertEx.ToInt(e.Node.GetValue(columnId));
                if (!_changedIds.Contains(lId))
                    _changedIds.Add(lId);
                //FireEditValueChanged(e);
            }
            finally {
                tree.EndUpdate();
            }
        }

        private void tree_ShowingEditor(object sender, CancelEventArgs e) {
            e.Cancel = !IsCellEditAllowed(tree.FocusedColumn, tree.FocusedNode);
        }

//        private void tree_CustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e) {
//            if (IsCellEditAllowed(e.Column, e.Node))
//                return;
//            if (!IsEditableColumn(e.Column))
//                return;
//
//            Rectangle lRect = e.Bounds;
//            lRect.Inflate(-1, -1);
//            e.Graphics.FillRectangle(SqlConstants.BrushGray, lRect);
//        }

        public void SaveChanges() {
            DataTable lDataTable = tree.DataSource as DataTable;
            if (lDataTable == null)
                return;
            lDataTable.AcceptChanges();

            DataTable lFilteredTable = GetFilteredTable(lDataTable);
            foreach (DataRow lRow in lFilteredTable.Rows)
                DataAccessProvider.SaveIndicatorChangedRow(lRow, Date);
            _changedIds.Clear();
        }

        private DataTable GetFilteredTable(DataTable dataTable) {
            StringBuilder lIdListBuilder = new StringBuilder(1024);
            foreach (int lId in _changedIds)
                lIdListBuilder.AppendFormat("'{0}'{1}", lId, SqlConstants.DELIMITER_ID_LIST);

            string lIdList = lIdListBuilder.ToString().TrimEnd(SqlConstants.DELIMITER_ID_LIST.ToCharArray());
            string lFilter;
            if (IsChanged)
                //where ID in ( )
                lFilter = string.Format("{0} IN ({1})", columnId.FieldName, lIdList);
            else
                lFilter = "1 = 2";
            DataView lDataView1 = new DataView(dataTable);
            lDataView1.RowFilter = lFilter;

            DataTable lFilteredTable1 = lDataView1.ToTable();
            lFilteredTable1.PrimaryKey = new DataColumn[] {lFilteredTable1.Columns[columnId.FieldName]};
            return lFilteredTable1;
        }

        private void toolTipController_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e) {
            if (!(e.SelectedControl is TreeList))
                return;
            TreeList lTree = (TreeList)e.SelectedControl;
            TreeListHitInfo lHitInfo = lTree.CalcHitInfo(e.ControlMousePosition);

            if (lHitInfo.HitInfoType != HitInfoType.Cell)
                return;
            if (!IsEditableColumn(lHitInfo.Column))
                return;
            object lCellInfo = new TreeListCellToolTipInfo(lHitInfo.Node, lHitInfo.Column, null);
            DateTime lDate = ConvertEx.ToDateTime(lHitInfo.Node.GetValue(columnStartDate));
            string lToolTip = string.Format("Действует с {0}", lDate.ToString("Y"));
            e.Info =  new ToolTipControlInfo(lCellInfo, lToolTip);
        }

        private void menuItemExpand_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            tree.ExpandAll();
        }

        private void menuItemCollapse_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            tree.CollapseAll();
        }

        private void tree_MouseUp(object sender, MouseEventArgs e) {
            TreeList lTree = sender as TreeList;

            if (e.Button == MouseButtons.Right && ModifierKeys == Keys.None && lTree.State == TreeListState.Regular) {
                Point lPoint = tree.PointToClient(MousePosition);
                TreeListHitInfo lHitInfo = lTree.CalcHitInfo(lPoint);
                if (lHitInfo.HitInfoType == HitInfoType.Cell) {
//                    SavedFocused = lTree.FocusedNode;
//                    lTree.FocusedNode = lHitInfo.Node;
//                    NeedRestoreFocused = true;
                    popupMenu.ShowPopup(MousePosition);
                }
            }
        }

    }
}
