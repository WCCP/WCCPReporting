﻿namespace SoftServe.Reports.M3KaTrade.UserControls {
    partial class IndicatorsPlanTree {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.tree = new DevExpress.XtraTreeList.TreeList();
            this.columnStaffName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnMixOnDate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemN2Edit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnMiddleSku = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStrikeRate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnPlanStrikeRate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnMiddleTimeinField = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemTimeEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.columnEquipmentEff = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnEquipmentOutletEff = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnEquipmentOutletNullSales = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnMiddleCountAllVisits = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnMiddleCountVisitsByRoute = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnEffVolume = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnEffOrder = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnPOCeBSM = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnPOCeSD = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStartDate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.menuItemExpand = new DevExpress.XtraBars.BarButtonItem();
            this.menuItemCollapse = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize) (this.tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemN2Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemTimeEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.popupMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.ColumnPanelRowHeight = 50;
            this.tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnStaffName,
            this.columnMixOnDate,
            this.columnMiddleSku,
            this.columnStrikeRate,
            this.columnPlanStrikeRate,
            this.columnMiddleTimeinField,
            this.columnEquipmentEff,
            this.columnEquipmentOutletEff,
            this.columnEquipmentOutletNullSales,
            this.columnMiddleCountAllVisits,
            this.columnMiddleCountVisitsByRoute,
            this.columnEffVolume,
            this.columnEffOrder,
            this.columnPOCeBSM,
            this.columnPOCeSD,
            this.columnId,
            this.columnParentId,
            this.columnStaffId,
            this.columnStaffParentId,
            this.columnStaffLevel,
            this.columnStartDate});
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Name = "tree";
            this.tree.OptionsBehavior.AutoPopulateColumns = false;
            this.tree.OptionsBehavior.EnableFiltering = true;
            this.tree.OptionsView.AutoWidth = false;
            this.tree.ParentFieldName = "PARENT_ID";
            this.tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemN2Edit,
            this.repositoryItemTimeEdit});
            this.tree.Size = new System.Drawing.Size(1230, 592);
            this.tree.TabIndex = 1;
            this.tree.ToolTipController = this.toolTipController;
            this.tree.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tree_CellValueChanged);
            this.tree.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.tree_ShowingEditor);
            this.tree.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tree_MouseUp);
            // 
            // columnStaffName
            // 
            this.columnStaffName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStaffName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStaffName.Caption = "Персонал";
            this.columnStaffName.FieldName = "Staff_Name";
            this.columnStaffName.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnStaffName.Name = "columnStaffName";
            this.columnStaffName.OptionsColumn.AllowEdit = false;
            this.columnStaffName.OptionsColumn.AllowMove = false;
            this.columnStaffName.OptionsColumn.ReadOnly = true;
            this.columnStaffName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnStaffName.Visible = true;
            this.columnStaffName.VisibleIndex = 0;
            this.columnStaffName.Width = 180;
            // 
            // columnMixOnDate
            // 
            this.columnMixOnDate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMixOnDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMixOnDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMixOnDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnMixOnDate.Caption = "МИКС на дату, %";
            this.columnMixOnDate.ColumnEdit = this.repositoryItemN2Edit;
            this.columnMixOnDate.FieldName = "MixOnDate";
            this.columnMixOnDate.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMixOnDate.Name = "columnMixOnDate";
            this.columnMixOnDate.Visible = true;
            this.columnMixOnDate.VisibleIndex = 1;
            this.columnMixOnDate.Width = 60;
            // 
            // repositoryItemN2Edit
            // 
            this.repositoryItemN2Edit.AutoHeight = false;
            this.repositoryItemN2Edit.DisplayFormat.FormatString = "n2";
            this.repositoryItemN2Edit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemN2Edit.EditFormat.FormatString = "n2";
            this.repositoryItemN2Edit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemN2Edit.Mask.EditMask = "n2";
            this.repositoryItemN2Edit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemN2Edit.Name = "repositoryItemN2Edit";
            // 
            // columnMiddleSku
            // 
            this.columnMiddleSku.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMiddleSku.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMiddleSku.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMiddleSku.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnMiddleSku.Caption = "Среднее СКЮ";
            this.columnMiddleSku.ColumnEdit = this.repositoryItemN2Edit;
            this.columnMiddleSku.FieldName = "MiddleSku";
            this.columnMiddleSku.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMiddleSku.Name = "columnMiddleSku";
            this.columnMiddleSku.Visible = true;
            this.columnMiddleSku.VisibleIndex = 2;
            this.columnMiddleSku.Width = 58;
            // 
            // columnStrikeRate
            // 
            this.columnStrikeRate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStrikeRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStrikeRate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnStrikeRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnStrikeRate.Caption = "Strike Rate, %";
            this.columnStrikeRate.ColumnEdit = this.repositoryItemN2Edit;
            this.columnStrikeRate.FieldName = "StrikeRate";
            this.columnStrikeRate.Format.FormatString = "n2";
            this.columnStrikeRate.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnStrikeRate.Name = "columnStrikeRate";
            this.columnStrikeRate.Visible = true;
            this.columnStrikeRate.VisibleIndex = 3;
            this.columnStrikeRate.Width = 68;
            // 
            // columnPlanStrikeRate
            // 
            this.columnPlanStrikeRate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPlanStrikeRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPlanStrikeRate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPlanStrikeRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnPlanStrikeRate.Caption = "Плановый Strike Rate, %";
            this.columnPlanStrikeRate.ColumnEdit = this.repositoryItemN2Edit;
            this.columnPlanStrikeRate.FieldName = "PlanStrikeRate";
            this.columnPlanStrikeRate.Format.FormatString = "n2";
            this.columnPlanStrikeRate.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPlanStrikeRate.Name = "columnPlanStrikeRate";
            this.columnPlanStrikeRate.Visible = true;
            this.columnPlanStrikeRate.VisibleIndex = 4;
            this.columnPlanStrikeRate.Width = 68;
            // 
            // columnMiddleTimeinField
            // 
            this.columnMiddleTimeinField.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMiddleTimeinField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMiddleTimeinField.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMiddleTimeinField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnMiddleTimeinField.Caption = "Среднее время в полях чч:мм";
            this.columnMiddleTimeinField.ColumnEdit = this.repositoryItemTimeEdit;
            this.columnMiddleTimeinField.FieldName = "MiddleTimeinField";
            this.columnMiddleTimeinField.Format.FormatString = "HH:mm";
            this.columnMiddleTimeinField.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnMiddleTimeinField.Name = "columnMiddleTimeinField";
            this.columnMiddleTimeinField.Visible = true;
            this.columnMiddleTimeinField.VisibleIndex = 5;
            this.columnMiddleTimeinField.Width = 86;
            // 
            // repositoryItemTimeEdit
            // 
            this.repositoryItemTimeEdit.AutoHeight = false;
            this.repositoryItemTimeEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit.DisplayFormat.FormatString = "HH:mm";
            this.repositoryItemTimeEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit.EditFormat.FormatString = "HH:mm";
            this.repositoryItemTimeEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit.Mask.EditMask = "HH:mm";
            this.repositoryItemTimeEdit.Name = "repositoryItemTimeEdit";
            // 
            // columnEquipmentEff
            // 
            this.columnEquipmentEff.AppearanceHeader.Options.UseTextOptions = true;
            this.columnEquipmentEff.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnEquipmentEff.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnEquipmentEff.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnEquipmentEff.Caption = "% эфф. оборудования";
            this.columnEquipmentEff.ColumnEdit = this.repositoryItemN2Edit;
            this.columnEquipmentEff.FieldName = "EquipmentEff";
            this.columnEquipmentEff.Format.FormatString = "n2";
            this.columnEquipmentEff.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnEquipmentEff.Name = "columnEquipmentEff";
            this.columnEquipmentEff.Visible = true;
            this.columnEquipmentEff.VisibleIndex = 6;
            this.columnEquipmentEff.Width = 88;
            // 
            // columnEquipmentOutletEff
            // 
            this.columnEquipmentOutletEff.AppearanceHeader.Options.UseTextOptions = true;
            this.columnEquipmentOutletEff.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnEquipmentOutletEff.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnEquipmentOutletEff.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnEquipmentOutletEff.Caption = "% ТТ с эфф. оборудованием";
            this.columnEquipmentOutletEff.ColumnEdit = this.repositoryItemN2Edit;
            this.columnEquipmentOutletEff.FieldName = "EquipmentOutletEff";
            this.columnEquipmentOutletEff.Format.FormatString = "n2";
            this.columnEquipmentOutletEff.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnEquipmentOutletEff.Name = "columnEquipmentOutletEff";
            this.columnEquipmentOutletEff.Visible = true;
            this.columnEquipmentOutletEff.VisibleIndex = 7;
            this.columnEquipmentOutletEff.Width = 90;
            // 
            // columnEquipmentOutletNullSales
            // 
            this.columnEquipmentOutletNullSales.AppearanceHeader.Options.UseTextOptions = true;
            this.columnEquipmentOutletNullSales.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnEquipmentOutletNullSales.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnEquipmentOutletNullSales.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnEquipmentOutletNullSales.Caption = "ТТ с нулевыми продажами";
            this.columnEquipmentOutletNullSales.ColumnEdit = this.repositoryItemN2Edit;
            this.columnEquipmentOutletNullSales.FieldName = "EquipmentOutletNullSales";
            this.columnEquipmentOutletNullSales.Format.FormatString = "n2";
            this.columnEquipmentOutletNullSales.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnEquipmentOutletNullSales.Name = "columnEquipmentOutletNullSales";
            this.columnEquipmentOutletNullSales.Visible = true;
            this.columnEquipmentOutletNullSales.VisibleIndex = 8;
            this.columnEquipmentOutletNullSales.Width = 74;
            // 
            // columnMiddleCountAllVisits
            // 
            this.columnMiddleCountAllVisits.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMiddleCountAllVisits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMiddleCountAllVisits.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMiddleCountAllVisits.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnMiddleCountAllVisits.Caption = "Все визиты";
            this.columnMiddleCountAllVisits.ColumnEdit = this.repositoryItemN2Edit;
            this.columnMiddleCountAllVisits.FieldName = "MiddleCountAllVisits";
            this.columnMiddleCountAllVisits.Format.FormatString = "n2";
            this.columnMiddleCountAllVisits.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMiddleCountAllVisits.Name = "columnMiddleCountAllVisits";
            this.columnMiddleCountAllVisits.Visible = true;
            this.columnMiddleCountAllVisits.VisibleIndex = 9;
            this.columnMiddleCountAllVisits.Width = 68;
            // 
            // columnMiddleCountVisitsByRoute
            // 
            this.columnMiddleCountVisitsByRoute.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMiddleCountVisitsByRoute.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMiddleCountVisitsByRoute.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnMiddleCountVisitsByRoute.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnMiddleCountVisitsByRoute.Caption = "Визиты по маршрутам";
            this.columnMiddleCountVisitsByRoute.ColumnEdit = this.repositoryItemN2Edit;
            this.columnMiddleCountVisitsByRoute.FieldName = "MiddleCountVisitsByRoute";
            this.columnMiddleCountVisitsByRoute.Format.FormatString = "n2";
            this.columnMiddleCountVisitsByRoute.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMiddleCountVisitsByRoute.Name = "columnMiddleCountVisitsByRoute";
            this.columnMiddleCountVisitsByRoute.Visible = true;
            this.columnMiddleCountVisitsByRoute.VisibleIndex = 10;
            this.columnMiddleCountVisitsByRoute.Width = 68;
            // 
            // columnEffVolume
            // 
            this.columnEffVolume.AppearanceHeader.Options.UseTextOptions = true;
            this.columnEffVolume.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnEffVolume.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnEffVolume.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnEffVolume.Caption = "Эфф. выполнения, % Объем";
            this.columnEffVolume.ColumnEdit = this.repositoryItemN2Edit;
            this.columnEffVolume.FieldName = "EffVolume";
            this.columnEffVolume.Format.FormatString = "n2";
            this.columnEffVolume.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnEffVolume.Name = "columnEffVolume";
            this.columnEffVolume.Visible = true;
            this.columnEffVolume.VisibleIndex = 11;
            this.columnEffVolume.Width = 90;
            // 
            // columnEffOrder
            // 
            this.columnEffOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.columnEffOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnEffOrder.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnEffOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnEffOrder.Caption = "Эфф. выполнения, % Заказ";
            this.columnEffOrder.ColumnEdit = this.repositoryItemN2Edit;
            this.columnEffOrder.FieldName = "EffOrder";
            this.columnEffOrder.Format.FormatString = "n2";
            this.columnEffOrder.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnEffOrder.Name = "columnEffOrder";
            this.columnEffOrder.Visible = true;
            this.columnEffOrder.VisibleIndex = 12;
            this.columnEffOrder.Width = 90;
            // 
            // columnPOCeBSM
            // 
            this.columnPOCeBSM.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCeBSM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCeBSM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPOCeBSM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnPOCeBSM.Caption = "POCE, БСМ";
            this.columnPOCeBSM.ColumnEdit = this.repositoryItemN2Edit;
            this.columnPOCeBSM.FieldName = "POCeBSM";
            this.columnPOCeBSM.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPOCeBSM.Name = "columnPOCeBSM";
            this.columnPOCeBSM.Visible = true;
            this.columnPOCeBSM.VisibleIndex = 13;
            this.columnPOCeBSM.Width = 64;
            // 
            // columnPOCeSD
            // 
            this.columnPOCeSD.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCeSD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCeSD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPOCeSD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnPOCeSD.Caption = "POCE, СД";
            this.columnPOCeSD.ColumnEdit = this.repositoryItemN2Edit;
            this.columnPOCeSD.FieldName = "POCeSD";
            this.columnPOCeSD.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPOCeSD.Name = "columnPOCeSD";
            this.columnPOCeSD.Visible = true;
            this.columnPOCeSD.VisibleIndex = 14;
            this.columnPOCeSD.Width = 58;
            // 
            // columnId
            // 
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            // 
            // columnParentId
            // 
            this.columnParentId.Caption = "PARENT_ID";
            this.columnParentId.FieldName = "PARENT_ID";
            this.columnParentId.Name = "columnParentId";
            // 
            // columnStaffId
            // 
            this.columnStaffId.Caption = "Staff_ID";
            this.columnStaffId.FieldName = "Staff_ID";
            this.columnStaffId.Name = "columnStaffId";
            // 
            // columnStaffParentId
            // 
            this.columnStaffParentId.Caption = "Staff_Parent_ID";
            this.columnStaffParentId.FieldName = "Staff_Parent_ID";
            this.columnStaffParentId.Name = "columnStaffParentId";
            // 
            // columnStaffLevel
            // 
            this.columnStaffLevel.Caption = "Staff_Level";
            this.columnStaffLevel.FieldName = "Staff_Level";
            this.columnStaffLevel.Name = "columnStaffLevel";
            // 
            // columnStartDate
            // 
            this.columnStartDate.Caption = "StartDate";
            this.columnStartDate.FieldName = "StartDate";
            this.columnStartDate.Name = "columnStartDate";
            // 
            // toolTipController
            // 
            this.toolTipController.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController_GetActiveObjectInfo);
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.menuItemExpand,
            this.menuItemCollapse});
            this.barManager.MaxItemId = 5;
            // 
            // menuItemExpand
            // 
            this.menuItemExpand.Caption = "Развернуть Все";
            this.menuItemExpand.Id = 3;
            this.menuItemExpand.Name = "menuItemExpand";
            this.menuItemExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemExpand_ItemClick);
            // 
            // menuItemCollapse
            // 
            this.menuItemCollapse.Caption = "Свернуть Все";
            this.menuItemCollapse.Id = 4;
            this.menuItemCollapse.Name = "menuItemCollapse";
            this.menuItemCollapse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemCollapse_ItemClick);
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemCollapse)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // IndicatorsPlanTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "IndicatorsPlanTree";
            this.Size = new System.Drawing.Size(1230, 592);
            ((System.ComponentModel.ISupportInitialize) (this.tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemN2Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemTimeEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.popupMenu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList tree;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnMixOnDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemN2Edit;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnMiddleSku;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStartDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStrikeRate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnPlanStrikeRate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnMiddleTimeinField;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnEquipmentEff;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnEquipmentOutletEff;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnEquipmentOutletNullSales;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnMiddleCountAllVisits;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnEffOrder;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnPOCeBSM;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnPOCeSD;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem menuItemExpand;
        private DevExpress.XtraBars.BarButtonItem menuItemCollapse;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnMiddleCountVisitsByRoute;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnEffVolume;

    }
}
