﻿namespace SoftServe.Reports.M3KaTrade.UserControls {
    partial class NetworkPlanTree {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.tree = new DevExpress.XtraTreeList.TreeList();
            this.columnStaffName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnNetworkName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnTargetSaleMonthly = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemN2Edit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnTargetMixMonthly = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnTargetAvgSkuMonthly = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnTargetPoceBCMMonthly = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnTargetPoceCDMonthly = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnNetworkId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStartDate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnNetworkSortOrder = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.menuItemExpand = new DevExpress.XtraBars.BarButtonItem();
            this.menuItemCollapse = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize) (this.tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemN2Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.popupMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnStaffName,
            this.columnNetworkName,
            this.columnTargetSaleMonthly,
            this.columnTargetMixMonthly,
            this.columnTargetAvgSkuMonthly,
            this.columnTargetPoceBCMMonthly,
            this.columnTargetPoceCDMonthly,
            this.columnId,
            this.columnParentId,
            this.columnStaffId,
            this.columnStaffParentId,
            this.columnStaffLevel,
            this.columnNetworkId,
            this.columnStartDate,
            this.columnNetworkSortOrder});
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Name = "tree";
            this.tree.OptionsBehavior.EnableFiltering = true;
            this.tree.OptionsView.AutoWidth = false;
            this.tree.ParentFieldName = "PARENT_ID";
            this.tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemN2Edit});
            this.tree.Size = new System.Drawing.Size(957, 586);
            this.tree.TabIndex = 1;
            this.tree.ToolTipController = this.toolTipController;
            this.tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this.tree.CellValueChanging += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tree_CellValueChanging);
            this.tree.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tree_CellValueChanged);
            this.tree.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.tree_ShowingEditor);
            this.tree.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tree_MouseUp);
            // 
            // columnStaffName
            // 
            this.columnStaffName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStaffName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStaffName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnStaffName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnStaffName.Caption = "Персонал";
            this.columnStaffName.FieldName = "Staff_Name";
            this.columnStaffName.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnStaffName.Name = "columnStaffName";
            this.columnStaffName.OptionsColumn.AllowEdit = false;
            this.columnStaffName.OptionsColumn.AllowMove = false;
            this.columnStaffName.OptionsColumn.ReadOnly = true;
            this.columnStaffName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnStaffName.Visible = true;
            this.columnStaffName.VisibleIndex = 0;
            this.columnStaffName.Width = 160;
            // 
            // columnNetworkName
            // 
            this.columnNetworkName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnNetworkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnNetworkName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnNetworkName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnNetworkName.Caption = "Сети";
            this.columnNetworkName.FieldName = "Network_Name";
            this.columnNetworkName.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnNetworkName.Name = "columnNetworkName";
            this.columnNetworkName.OptionsColumn.AllowEdit = false;
            this.columnNetworkName.OptionsColumn.AllowMove = false;
            this.columnNetworkName.OptionsColumn.ReadOnly = true;
            this.columnNetworkName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnNetworkName.Visible = true;
            this.columnNetworkName.VisibleIndex = 1;
            this.columnNetworkName.Width = 160;
            // 
            // columnTargetSaleMonthly
            // 
            this.columnTargetSaleMonthly.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTargetSaleMonthly.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTargetSaleMonthly.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTargetSaleMonthly.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnTargetSaleMonthly.Caption = "Продажи";
            this.columnTargetSaleMonthly.ColumnEdit = this.repositoryItemN2Edit;
            this.columnTargetSaleMonthly.FieldName = "TargetSaleMonthly";
            this.columnTargetSaleMonthly.Name = "columnTargetSaleMonthly";
            this.columnTargetSaleMonthly.Visible = true;
            this.columnTargetSaleMonthly.VisibleIndex = 2;
            this.columnTargetSaleMonthly.Width = 83;
            // 
            // repositoryItemN2Edit
            // 
            this.repositoryItemN2Edit.AutoHeight = false;
            this.repositoryItemN2Edit.DisplayFormat.FormatString = "n2";
            this.repositoryItemN2Edit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemN2Edit.EditFormat.FormatString = "n2";
            this.repositoryItemN2Edit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemN2Edit.Mask.EditMask = "n2";
            this.repositoryItemN2Edit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemN2Edit.Name = "repositoryItemN2Edit";
            // 
            // columnTargetMixMonthly
            // 
            this.columnTargetMixMonthly.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTargetMixMonthly.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTargetMixMonthly.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTargetMixMonthly.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnTargetMixMonthly.Caption = "Микс";
            this.columnTargetMixMonthly.ColumnEdit = this.repositoryItemN2Edit;
            this.columnTargetMixMonthly.FieldName = "TargetMixMonthly";
            this.columnTargetMixMonthly.Format.FormatString = "n2";
            this.columnTargetMixMonthly.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnTargetMixMonthly.Name = "columnTargetMixMonthly";
            this.columnTargetMixMonthly.Visible = true;
            this.columnTargetMixMonthly.VisibleIndex = 3;
            this.columnTargetMixMonthly.Width = 93;
            // 
            // columnTargetAvgSkuMonthly
            // 
            this.columnTargetAvgSkuMonthly.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTargetAvgSkuMonthly.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTargetAvgSkuMonthly.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTargetAvgSkuMonthly.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnTargetAvgSkuMonthly.Caption = "Среднее СКЮ";
            this.columnTargetAvgSkuMonthly.ColumnEdit = this.repositoryItemN2Edit;
            this.columnTargetAvgSkuMonthly.FieldName = "TargetAvgSkuMonthly";
            this.columnTargetAvgSkuMonthly.Format.FormatString = "n2";
            this.columnTargetAvgSkuMonthly.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnTargetAvgSkuMonthly.Name = "columnTargetAvgSkuMonthly";
            this.columnTargetAvgSkuMonthly.Visible = true;
            this.columnTargetAvgSkuMonthly.VisibleIndex = 4;
            this.columnTargetAvgSkuMonthly.Width = 86;
            // 
            // columnTargetPoceBCMMonthly
            // 
            this.columnTargetPoceBCMMonthly.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTargetPoceBCMMonthly.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTargetPoceBCMMonthly.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTargetPoceBCMMonthly.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnTargetPoceBCMMonthly.Caption = "POCE, БСМ";
            this.columnTargetPoceBCMMonthly.ColumnEdit = this.repositoryItemN2Edit;
            this.columnTargetPoceBCMMonthly.FieldName = "TargetPoceBCMMonthly";
            this.columnTargetPoceBCMMonthly.Format.FormatString = "n2";
            this.columnTargetPoceBCMMonthly.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnTargetPoceBCMMonthly.Name = "columnTargetPoceBCMMonthly";
            this.columnTargetPoceBCMMonthly.Visible = true;
            this.columnTargetPoceBCMMonthly.VisibleIndex = 5;
            this.columnTargetPoceBCMMonthly.Width = 80;
            // 
            // columnTargetPoceCDMonthly
            // 
            this.columnTargetPoceCDMonthly.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTargetPoceCDMonthly.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTargetPoceCDMonthly.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTargetPoceCDMonthly.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnTargetPoceCDMonthly.Caption = "POCE, СД";
            this.columnTargetPoceCDMonthly.ColumnEdit = this.repositoryItemN2Edit;
            this.columnTargetPoceCDMonthly.FieldName = "TargetPoceCDMonthly";
            this.columnTargetPoceCDMonthly.Format.FormatString = "n2";
            this.columnTargetPoceCDMonthly.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnTargetPoceCDMonthly.Name = "columnTargetPoceCDMonthly";
            this.columnTargetPoceCDMonthly.Visible = true;
            this.columnTargetPoceCDMonthly.VisibleIndex = 6;
            this.columnTargetPoceCDMonthly.Width = 80;
            // 
            // columnId
            // 
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            // 
            // columnParentId
            // 
            this.columnParentId.Caption = "PARENT_ID";
            this.columnParentId.FieldName = "PARENT_ID";
            this.columnParentId.Name = "columnParentId";
            // 
            // columnStaffId
            // 
            this.columnStaffId.Caption = "Staff_ID";
            this.columnStaffId.FieldName = "Staff_ID";
            this.columnStaffId.Name = "columnStaffId";
            // 
            // columnStaffParentId
            // 
            this.columnStaffParentId.Caption = "Staff_Parent_ID";
            this.columnStaffParentId.FieldName = "Staff_Parent_ID";
            this.columnStaffParentId.Name = "columnStaffParentId";
            // 
            // columnStaffLevel
            // 
            this.columnStaffLevel.Caption = "Staff_Level";
            this.columnStaffLevel.FieldName = "Staff_Level";
            this.columnStaffLevel.Name = "columnStaffLevel";
            // 
            // columnNetworkId
            // 
            this.columnNetworkId.Caption = "Network_ID";
            this.columnNetworkId.FieldName = "Network_ID";
            this.columnNetworkId.Name = "columnNetworkId";
            // 
            // columnStartDate
            // 
            this.columnStartDate.Caption = "StartDate";
            this.columnStartDate.FieldName = "StartDate";
            this.columnStartDate.Name = "columnStartDate";
            // 
            // columnNetworkSortOrder
            // 
            this.columnNetworkSortOrder.Caption = "NetworkSortOrder";
            this.columnNetworkSortOrder.FieldName = "NetworkSortOrder";
            this.columnNetworkSortOrder.Name = "columnNetworkSortOrder";
            // 
            // toolTipController
            // 
            this.toolTipController.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController_GetActiveObjectInfo);
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.menuItemExpand,
            this.menuItemCollapse});
            this.barManager.MaxItemId = 6;
            // 
            // menuItemExpand
            // 
            this.menuItemExpand.Caption = "Развернуть Все";
            this.menuItemExpand.Id = 3;
            this.menuItemExpand.Name = "menuItemExpand";
            this.menuItemExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemExpand_ItemClick);
            // 
            // menuItemCollapse
            // 
            this.menuItemCollapse.Caption = "Свернуть Все";
            this.menuItemCollapse.Id = 4;
            this.menuItemCollapse.Name = "menuItemCollapse";
            this.menuItemCollapse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemCollapse_ItemClick);
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemCollapse)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // NetworkPlanTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "NetworkPlanTree";
            this.Size = new System.Drawing.Size(957, 586);
            ((System.ComponentModel.ISupportInitialize) (this.tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemN2Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.popupMenu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList tree;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnNetworkName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnTargetSaleMonthly;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemN2Edit;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnTargetMixMonthly;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnTargetAvgSkuMonthly;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnTargetPoceBCMMonthly;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnTargetPoceCDMonthly;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnNetworkId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStartDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnNetworkSortOrder;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem menuItemExpand;
        private DevExpress.XtraBars.BarButtonItem menuItemCollapse;
        private DevExpress.XtraBars.PopupMenu popupMenu;
    }
}
