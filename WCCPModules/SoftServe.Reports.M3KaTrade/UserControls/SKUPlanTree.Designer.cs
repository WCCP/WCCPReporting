﻿namespace SoftServe.Reports.M3KaTrade.UserControls {
    partial class SKUPlanTree {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.tree = new DevExpress.XtraTreeList.TreeList();
            this.columnStaffName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnSKUName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnTargetSaleMonthly = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemN2Edit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnTargetNumATT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStaffLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnProductCnTopId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnGroupRowNum = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnATTCount = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnOLCount = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnStartDate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.menuItemExpand = new DevExpress.XtraBars.BarButtonItem();
            this.menuItemCollapse = new DevExpress.XtraBars.BarButtonItem();
            this.menuItemExpandAll = new DevExpress.XtraBars.BarButtonItem();
            this.menuItemCollapseAll = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize) (this.tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemN2Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnStaffName,
            this.columnSKUName,
            this.columnTargetSaleMonthly,
            this.columnTargetNumATT,
            this.columnId,
            this.columnParentId,
            this.columnStaffId,
            this.columnStaffParentId,
            this.columnStaffLevel,
            this.columnProductCnTopId,
            this.columnGroupRowNum,
            this.columnATTCount,
            this.columnOLCount,
            this.columnStartDate});
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Name = "tree";
            this.tree.OptionsBehavior.EnableFiltering = true;
            this.tree.OptionsMenu.EnableColumnMenu = false;
            this.tree.OptionsMenu.EnableFooterMenu = false;
            this.tree.OptionsView.AutoWidth = false;
            this.tree.ParentFieldName = "PARENT_ID";
            this.tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemN2Edit});
            this.tree.Size = new System.Drawing.Size(605, 416);
            this.tree.TabIndex = 0;
            this.tree.ToolTipController = this.toolTipController;
            this.tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this.tree.CellValueChanging += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tree_CellValueChanging);
            this.tree.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.tree_CellValueChanged);
            this.tree.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.tree_ShowingEditor);
            this.tree.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tree_MouseUp);
            // 
            // columnStaffName
            // 
            this.columnStaffName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStaffName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStaffName.Caption = "Персонал";
            this.columnStaffName.FieldName = "Staff_Name";
            this.columnStaffName.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnStaffName.Name = "columnStaffName";
            this.columnStaffName.OptionsColumn.AllowEdit = false;
            this.columnStaffName.OptionsColumn.AllowMove = false;
            this.columnStaffName.OptionsColumn.ReadOnly = true;
            this.columnStaffName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnStaffName.Visible = true;
            this.columnStaffName.VisibleIndex = 0;
            this.columnStaffName.Width = 129;
            // 
            // columnSKUName
            // 
            this.columnSKUName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnSKUName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnSKUName.Caption = "Продукция";
            this.columnSKUName.FieldName = "ProductCnTop_Name";
            this.columnSKUName.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.columnSKUName.Name = "columnSKUName";
            this.columnSKUName.OptionsColumn.AllowEdit = false;
            this.columnSKUName.OptionsColumn.AllowMove = false;
            this.columnSKUName.OptionsColumn.ReadOnly = true;
            this.columnSKUName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnSKUName.Visible = true;
            this.columnSKUName.VisibleIndex = 1;
            this.columnSKUName.Width = 147;
            // 
            // columnTargetSaleMonthly
            // 
            this.columnTargetSaleMonthly.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTargetSaleMonthly.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTargetSaleMonthly.Caption = "Продажи";
            this.columnTargetSaleMonthly.ColumnEdit = this.repositoryItemN2Edit;
            this.columnTargetSaleMonthly.FieldName = "TargetSaleMonthly";
            this.columnTargetSaleMonthly.Name = "columnTargetSaleMonthly";
            this.columnTargetSaleMonthly.Visible = true;
            this.columnTargetSaleMonthly.VisibleIndex = 2;
            this.columnTargetSaleMonthly.Width = 83;
            // 
            // repositoryItemN2Edit
            // 
            this.repositoryItemN2Edit.AutoHeight = false;
            this.repositoryItemN2Edit.DisplayFormat.FormatString = "n2";
            this.repositoryItemN2Edit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemN2Edit.EditFormat.FormatString = "n2";
            this.repositoryItemN2Edit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemN2Edit.Mask.EditMask = "n2";
            this.repositoryItemN2Edit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemN2Edit.Name = "repositoryItemN2Edit";
            // 
            // columnTargetNumATT
            // 
            this.columnTargetNumATT.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTargetNumATT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTargetNumATT.Caption = "Покрытие";
            this.columnTargetNumATT.ColumnEdit = this.repositoryItemN2Edit;
            this.columnTargetNumATT.FieldName = "TargetNumATT";
            this.columnTargetNumATT.Name = "columnTargetNumATT";
            this.columnTargetNumATT.Visible = true;
            this.columnTargetNumATT.VisibleIndex = 3;
            this.columnTargetNumATT.Width = 93;
            // 
            // columnId
            // 
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            // 
            // columnParentId
            // 
            this.columnParentId.Caption = "PARENT_ID";
            this.columnParentId.FieldName = "PARENT_ID";
            this.columnParentId.Name = "columnParentId";
            // 
            // columnStaffId
            // 
            this.columnStaffId.Caption = "Staff_ID";
            this.columnStaffId.FieldName = "Staff_ID";
            this.columnStaffId.Name = "columnStaffId";
            // 
            // columnStaffParentId
            // 
            this.columnStaffParentId.Caption = "Staff_Parent_ID";
            this.columnStaffParentId.FieldName = "Staff_Parent_ID";
            this.columnStaffParentId.Name = "columnStaffParentId";
            // 
            // columnStaffLevel
            // 
            this.columnStaffLevel.Caption = "Staff_Level";
            this.columnStaffLevel.FieldName = "Staff_Level";
            this.columnStaffLevel.Name = "columnStaffLevel";
            // 
            // columnProductCnTopId
            // 
            this.columnProductCnTopId.Caption = "ProductCnTop_ID";
            this.columnProductCnTopId.FieldName = "productCnTop_ID";
            this.columnProductCnTopId.Name = "columnProductCnTopId";
            // 
            // columnGroupRowNum
            // 
            this.columnGroupRowNum.Caption = "GroupRowNum";
            this.columnGroupRowNum.FieldName = "GroupRowNum";
            this.columnGroupRowNum.Name = "columnGroupRowNum";
            // 
            // columnATTCount
            // 
            this.columnATTCount.Caption = "ATTCount";
            this.columnATTCount.FieldName = "ATTCount";
            this.columnATTCount.Name = "columnATTCount";
            // 
            // columnOLCount
            // 
            this.columnOLCount.Caption = "OLCount";
            this.columnOLCount.FieldName = "OLCount";
            this.columnOLCount.Name = "columnOLCount";
            // 
            // columnStartDate
            // 
            this.columnStartDate.Caption = "StartDate";
            this.columnStartDate.FieldName = "StartDate";
            this.columnStartDate.Name = "columnStartDate";
            // 
            // toolTipController
            // 
            this.toolTipController.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController_GetActiveObjectInfo);
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemCollapse),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemExpandAll, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemCollapseAll)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // menuItemExpand
            // 
            this.menuItemExpand.Caption = "Развернуть";
            this.menuItemExpand.CategoryGuid = new System.Guid("a76745fc-ae2b-4506-87ad-bf6e8d659e94");
            this.menuItemExpand.Id = 5;
            this.menuItemExpand.Name = "menuItemExpand";
            this.menuItemExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemExpand_ItemClick_1);
            // 
            // menuItemCollapse
            // 
            this.menuItemCollapse.Caption = "Свернуть";
            this.menuItemCollapse.CategoryGuid = new System.Guid("a76745fc-ae2b-4506-87ad-bf6e8d659e94");
            this.menuItemCollapse.Id = 6;
            this.menuItemCollapse.Name = "menuItemCollapse";
            this.menuItemCollapse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemCollapse_ItemClick_1);
            // 
            // menuItemExpandAll
            // 
            this.menuItemExpandAll.Caption = "Развернуть Все";
            this.menuItemExpandAll.CategoryGuid = new System.Guid("a76745fc-ae2b-4506-87ad-bf6e8d659e94");
            this.menuItemExpandAll.Id = 3;
            this.menuItemExpandAll.Name = "menuItemExpandAll";
            this.menuItemExpandAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemExpand_ItemClick);
            // 
            // menuItemCollapseAll
            // 
            this.menuItemCollapseAll.Caption = "Свернуть Все";
            this.menuItemCollapseAll.CategoryGuid = new System.Guid("a76745fc-ae2b-4506-87ad-bf6e8d659e94");
            this.menuItemCollapseAll.Id = 4;
            this.menuItemCollapseAll.Name = "menuItemCollapseAll";
            this.menuItemCollapseAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuItemCollapse_ItemClick);
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Tree", new System.Guid("a76745fc-ae2b-4506-87ad-bf6e8d659e94"))});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.menuItemExpand,
            this.menuItemCollapse,
            this.menuItemExpandAll,
            this.menuItemCollapseAll});
            this.barManager.MaxItemId = 7;
            // 
            // SKUPlanTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "SKUPlanTree";
            this.Size = new System.Drawing.Size(605, 416);
            ((System.ComponentModel.ISupportInitialize) (this.tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemN2Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList tree;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnSKUName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnTargetSaleMonthly;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnTargetNumATT;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemN2Edit;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnProductCnTopId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnATTCount;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnOLCount;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStartDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnStaffParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnGroupRowNum;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem menuItemExpandAll;
        private DevExpress.XtraBars.BarButtonItem menuItemCollapseAll;
        private DevExpress.XtraBars.BarButtonItem menuItemExpand;
        private DevExpress.XtraBars.BarButtonItem menuItemCollapse;
    }
}
