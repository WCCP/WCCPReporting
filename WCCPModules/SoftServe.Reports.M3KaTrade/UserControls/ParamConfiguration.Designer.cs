﻿namespace SoftServe.Reports.M3KaTrade.UserControls {
    partial class ParamConfiguration {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParamConfiguration));
            this.gridColumnEditable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainer = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupSKUConfig = new DevExpress.XtraEditors.GroupControl();
            this.treeSKU = new DevExpress.XtraTreeList.TreeList();
            this.columnId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnParentId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnSKUName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnSKUChecked = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnItemID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridListSKU = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnProductCnTopId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRegionId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelButtons = new DevExpress.XtraEditors.PanelControl();
            this.btnDown = new DevExpress.XtraEditors.SimpleButton();
            this.btnUp = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.CheckButton();
            this.labelSKUName = new DevExpress.XtraEditors.LabelControl();
            this.textEditSKUName = new DevExpress.XtraEditors.TextEdit();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.groupKpiConfig = new DevExpress.XtraEditors.GroupControl();
            this.groupKpi2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbType2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbKpi2Sku = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupKpi1 = new DevExpress.XtraEditors.GroupControl();
            this.labelType1 = new DevExpress.XtraEditors.LabelControl();
            this.cbType1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbKpi1Sku = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelKpiMain = new DevExpress.XtraEditors.PanelControl();
            this.memoEditFocusKpi = new DevExpress.XtraEditors.MemoEdit();
            this.labelFocusKpi = new DevExpress.XtraEditors.LabelControl();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer)).BeginInit();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.groupSKUConfig)).BeginInit();
            this.groupSKUConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.treeSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.gridListSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.panelButtons)).BeginInit();
            this.panelButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.textEditSKUName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupKpiConfig)).BeginInit();
            this.groupKpiConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.groupKpi2)).BeginInit();
            this.groupKpi2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.cbType2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.cbKpi2Sku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupKpi1)).BeginInit();
            this.groupKpi1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.cbType1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.cbKpi1Sku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.panelKpiMain)).BeginInit();
            this.panelKpiMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.memoEditFocusKpi.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridColumnEditable
            // 
            this.gridColumnEditable.Caption = "Editable";
            this.gridColumnEditable.FieldName = "Editable";
            this.gridColumnEditable.Name = "gridColumnEditable";
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Panel1.Controls.Add(this.groupSKUConfig);
            this.splitContainer.Panel1.Text = "Panel1";
            this.splitContainer.Panel2.Controls.Add(this.groupKpiConfig);
            this.splitContainer.Panel2.Text = "Panel2";
            this.splitContainer.Size = new System.Drawing.Size(750, 499);
            this.splitContainer.SplitterPosition = 469;
            this.splitContainer.TabIndex = 1;
            this.splitContainer.Text = "splitContainerControl1";
            // 
            // groupSKUConfig
            // 
            this.groupSKUConfig.Controls.Add(this.treeSKU);
            this.groupSKUConfig.Controls.Add(this.panelControl1);
            this.groupSKUConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupSKUConfig.Location = new System.Drawing.Point(0, 0);
            this.groupSKUConfig.Name = "groupSKUConfig";
            this.groupSKUConfig.Size = new System.Drawing.Size(469, 499);
            this.groupSKUConfig.TabIndex = 1;
            this.groupSKUConfig.Text = "Настройка списка СКЮ";
            // 
            // treeSKU
            // 
            this.treeSKU.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.treeSKU.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnId,
            this.columnParentId,
            this.columnSKUName,
            this.columnSKUChecked,
            this.columnLevel,
            this.columnItemID});
            this.treeSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeSKU.KeyFieldName = "Id";
            this.treeSKU.Location = new System.Drawing.Point(222, 22);
            this.treeSKU.Margin = new System.Windows.Forms.Padding(0);
            this.treeSKU.Name = "treeSKU";
            this.treeSKU.OptionsBehavior.Editable = false;
            this.treeSKU.OptionsView.ShowCheckBoxes = true;
            this.treeSKU.OptionsView.ShowColumns = false;
            this.treeSKU.OptionsView.ShowHorzLines = false;
            this.treeSKU.OptionsView.ShowIndicator = false;
            this.treeSKU.ParentFieldName = "Parent_id";
            this.treeSKU.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit});
            this.treeSKU.Size = new System.Drawing.Size(245, 475);
            this.treeSKU.TabIndex = 3;
            this.treeSKU.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeSKU_BeforeCheckNode);
            this.treeSKU.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeSKU_AfterCheckNode);
            // 
            // columnId
            // 
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "Id";
            this.columnId.Name = "columnId";
            // 
            // columnParentId
            // 
            this.columnParentId.Caption = "Parent ID";
            this.columnParentId.FieldName = "Parent_id";
            this.columnParentId.Name = "columnParentId";
            // 
            // columnSKUName
            // 
            this.columnSKUName.Caption = "Name";
            this.columnSKUName.FieldName = "name";
            this.columnSKUName.Name = "columnSKUName";
            this.columnSKUName.OptionsColumn.AllowEdit = false;
            this.columnSKUName.OptionsColumn.ReadOnly = true;
            this.columnSKUName.Visible = true;
            this.columnSKUName.VisibleIndex = 0;
            // 
            // columnSKUChecked
            // 
            this.columnSKUChecked.Caption = "Checked";
            this.columnSKUChecked.FieldName = "Checked";
            this.columnSKUChecked.Name = "columnSKUChecked";
            // 
            // columnLevel
            // 
            this.columnLevel.Caption = "Level";
            this.columnLevel.FieldName = "level";
            this.columnLevel.Name = "columnLevel";
            // 
            // columnItemID
            // 
            this.columnItemID.Caption = "Item ID";
            this.columnItemID.FieldName = "Item_ID";
            this.columnItemID.Name = "columnItemID";
            // 
            // repositoryItemCheckEdit
            // 
            this.repositoryItemCheckEdit.AutoHeight = false;
            this.repositoryItemCheckEdit.Name = "repositoryItemCheckEdit";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridListSKU);
            this.panelControl1.Controls.Add(this.panelButtons);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(2, 22);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(220, 475);
            this.panelControl1.TabIndex = 3;
            // 
            // gridListSKU
            // 
            this.gridListSKU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridListSKU.Location = new System.Drawing.Point(2, 101);
            this.gridListSKU.MainView = this.gridView;
            this.gridListSKU.Name = "gridListSKU";
            this.gridListSKU.Size = new System.Drawing.Size(216, 372);
            this.gridListSKU.TabIndex = 2;
            this.gridListSKU.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnGroupName,
            this.gridColumnProductCnTopId,
            this.gridColumnRegionId,
            this.gridColumnEditable});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.LightGray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumnEditable;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = false;
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView.GridControl = this.gridListSKU;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.AutoPopulateColumns = false;
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowColumnMoving = false;
            this.gridView.OptionsCustomization.AllowColumnResizing = false;
            this.gridView.OptionsCustomization.AllowFilter = false;
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView.OptionsDetail.AllowZoomDetail = false;
            this.gridView.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView.OptionsFilter.AllowFilterEditor = false;
            this.gridView.OptionsFilter.AllowMRUFilterList = false;
            this.gridView.OptionsMenu.EnableColumnMenu = false;
            this.gridView.OptionsMenu.EnableFooterMenu = false;
            this.gridView.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView.OptionsMenu.ShowDateTimeGroupIntervalItems = false;
            this.gridView.OptionsSelection.InvertSelection = true;
            this.gridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView.OptionsView.ShowColumnHeaders = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowHorzLines = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.OptionsView.ShowPreviewLines = false;
            this.gridView.OptionsView.ShowVertLines = false;
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            // 
            // gridColumnGroupName
            // 
            this.gridColumnGroupName.Caption = "SKU Name";
            this.gridColumnGroupName.FieldName = "Name";
            this.gridColumnGroupName.Name = "gridColumnGroupName";
            this.gridColumnGroupName.OptionsColumn.AllowEdit = false;
            this.gridColumnGroupName.OptionsColumn.AllowMove = false;
            this.gridColumnGroupName.OptionsColumn.AllowShowHide = false;
            this.gridColumnGroupName.OptionsColumn.AllowSize = false;
            this.gridColumnGroupName.OptionsColumn.ReadOnly = true;
            this.gridColumnGroupName.OptionsColumn.ShowCaption = false;
            this.gridColumnGroupName.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumnGroupName.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnGroupName.OptionsFilter.AllowFilter = false;
            this.gridColumnGroupName.Visible = true;
            this.gridColumnGroupName.VisibleIndex = 0;
            // 
            // gridColumnProductCnTopId
            // 
            this.gridColumnProductCnTopId.Caption = "ProductCnTop_ID";
            this.gridColumnProductCnTopId.FieldName = "Id";
            this.gridColumnProductCnTopId.Name = "gridColumnProductCnTopId";
            // 
            // gridColumnRegionId
            // 
            this.gridColumnRegionId.Caption = "Region_ID";
            this.gridColumnRegionId.FieldName = "RegionId";
            this.gridColumnRegionId.Name = "gridColumnRegionId";
            // 
            // panelButtons
            // 
            this.panelButtons.Controls.Add(this.btnDown);
            this.panelButtons.Controls.Add(this.btnUp);
            this.panelButtons.Controls.Add(this.btnEdit);
            this.panelButtons.Controls.Add(this.labelSKUName);
            this.panelButtons.Controls.Add(this.textEditSKUName);
            this.panelButtons.Controls.Add(this.btnDelete);
            this.panelButtons.Controls.Add(this.btnAdd);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelButtons.Location = new System.Drawing.Point(2, 2);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Padding = new System.Windows.Forms.Padding(2);
            this.panelButtons.Size = new System.Drawing.Size(216, 99);
            this.panelButtons.TabIndex = 3;
            // 
            // btnDown
            // 
            this.btnDown.Image = ((System.Drawing.Image) (resources.GetObject("btnDown.Image")));
            this.btnDown.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDown.Location = new System.Drawing.Point(108, 6);
            this.btnDown.Margin = new System.Windows.Forms.Padding(2);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(30, 32);
            this.btnDown.TabIndex = 37;
            this.btnDown.ToolTip = "Переместить СКЮ вниз";
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUp
            // 
            this.btnUp.Image = ((System.Drawing.Image) (resources.GetObject("btnUp.Image")));
            this.btnUp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnUp.Location = new System.Drawing.Point(142, 6);
            this.btnUp.Margin = new System.Windows.Forms.Padding(2);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(30, 32);
            this.btnUp.TabIndex = 36;
            this.btnUp.ToolTip = "Переместить СКЮ вверх";
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image) (resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEdit.Location = new System.Drawing.Point(40, 6);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(30, 32);
            this.btnEdit.TabIndex = 35;
            this.btnEdit.ToolTip = "Редактировать СКЮ";
            this.btnEdit.CheckedChanged += new System.EventHandler(this.btnEdit_CheckedChanged);
            // 
            // labelSKUName
            // 
            this.labelSKUName.Location = new System.Drawing.Point(6, 53);
            this.labelSKUName.Name = "labelSKUName";
            this.labelSKUName.Size = new System.Drawing.Size(106, 13);
            this.labelSKUName.TabIndex = 34;
            this.labelSKUName.Text = "Название продукции";
            // 
            // textEditSKUName
            // 
            this.textEditSKUName.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditSKUName.Location = new System.Drawing.Point(6, 72);
            this.textEditSKUName.Name = "textEditSKUName";
            this.textEditSKUName.Size = new System.Drawing.Size(203, 20);
            this.textEditSKUName.TabIndex = 5;
            this.textEditSKUName.EditValueChanged += new System.EventHandler(this.textEditSKUName_EditValueChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = ((System.Drawing.Image) (resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDelete.Location = new System.Drawing.Point(74, 6);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(30, 32);
            this.btnDelete.TabIndex = 32;
            this.btnDelete.ToolTip = "Удалить СКЮ";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image) (resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(6, 6);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(30, 32);
            this.btnAdd.TabIndex = 30;
            this.btnAdd.ToolTip = "Добавить СКЮ";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupKpiConfig
            // 
            this.groupKpiConfig.Controls.Add(this.groupKpi2);
            this.groupKpiConfig.Controls.Add(this.groupKpi1);
            this.groupKpiConfig.Controls.Add(this.panelKpiMain);
            this.groupKpiConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupKpiConfig.Location = new System.Drawing.Point(0, 0);
            this.groupKpiConfig.Name = "groupKpiConfig";
            this.groupKpiConfig.Size = new System.Drawing.Size(275, 499);
            this.groupKpiConfig.TabIndex = 0;
            this.groupKpiConfig.Text = "Настройка рейтингов";
            // 
            // groupKpi2
            // 
            this.groupKpi2.Controls.Add(this.labelControl1);
            this.groupKpi2.Controls.Add(this.cbType2);
            this.groupKpi2.Controls.Add(this.cbKpi2Sku);
            this.groupKpi2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupKpi2.Location = new System.Drawing.Point(2, 265);
            this.groupKpi2.Name = "groupKpi2";
            this.groupKpi2.Size = new System.Drawing.Size(271, 142);
            this.groupKpi2.TabIndex = 42;
            this.groupKpi2.Text = "KPI 2";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 73);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(22, 13);
            this.labelControl1.TabIndex = 46;
            this.labelControl1.Text = "Тип:";
            // 
            // cbType2
            // 
            this.cbType2.Location = new System.Drawing.Point(6, 92);
            this.cbType2.Name = "cbType2";
            this.cbType2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbType2.Properties.DropDownRows = 2;
            this.cbType2.Properties.PopupSizeable = true;
            this.cbType2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbType2.Size = new System.Drawing.Size(200, 20);
            this.cbType2.TabIndex = 45;
            this.cbType2.SelectedIndexChanged += new System.EventHandler(this.cbType2_SelectedIndexChanged);
            // 
            // cbKpi2Sku
            // 
            this.cbKpi2Sku.Location = new System.Drawing.Point(6, 34);
            this.cbKpi2Sku.Name = "cbKpi2Sku";
            this.cbKpi2Sku.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbKpi2Sku.Properties.DropDownRows = 12;
            this.cbKpi2Sku.Properties.PopupSizeable = true;
            this.cbKpi2Sku.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbKpi2Sku.Size = new System.Drawing.Size(200, 20);
            this.cbKpi2Sku.TabIndex = 40;
            this.cbKpi2Sku.SelectedIndexChanged += new System.EventHandler(this.cbKpi2Sku_SelectedIndexChanged);
            // 
            // groupKpi1
            // 
            this.groupKpi1.Controls.Add(this.labelType1);
            this.groupKpi1.Controls.Add(this.cbType1);
            this.groupKpi1.Controls.Add(this.cbKpi1Sku);
            this.groupKpi1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupKpi1.Location = new System.Drawing.Point(2, 123);
            this.groupKpi1.Name = "groupKpi1";
            this.groupKpi1.Size = new System.Drawing.Size(271, 142);
            this.groupKpi1.TabIndex = 41;
            this.groupKpi1.Text = "KPI 1";
            // 
            // labelType1
            // 
            this.labelType1.Location = new System.Drawing.Point(6, 73);
            this.labelType1.Name = "labelType1";
            this.labelType1.Size = new System.Drawing.Size(22, 13);
            this.labelType1.TabIndex = 46;
            this.labelType1.Text = "Тип:";
            // 
            // cbType1
            // 
            this.cbType1.Location = new System.Drawing.Point(6, 92);
            this.cbType1.Name = "cbType1";
            this.cbType1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbType1.Properties.DropDownRows = 2;
            this.cbType1.Properties.PopupSizeable = true;
            this.cbType1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbType1.Size = new System.Drawing.Size(200, 20);
            this.cbType1.TabIndex = 45;
            this.cbType1.SelectedIndexChanged += new System.EventHandler(this.cbType1_SelectedIndexChanged);
            // 
            // cbKpi1Sku
            // 
            this.cbKpi1Sku.Location = new System.Drawing.Point(6, 34);
            this.cbKpi1Sku.Name = "cbKpi1Sku";
            this.cbKpi1Sku.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbKpi1Sku.Properties.DropDownRows = 12;
            this.cbKpi1Sku.Properties.PopupSizeable = true;
            this.cbKpi1Sku.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbKpi1Sku.Size = new System.Drawing.Size(200, 20);
            this.cbKpi1Sku.TabIndex = 40;
            this.cbKpi1Sku.SelectedIndexChanged += new System.EventHandler(this.cbKpi1Sku_SelectedIndexChanged);
            // 
            // panelKpiMain
            // 
            this.panelKpiMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.panelKpiMain.Controls.Add(this.memoEditFocusKpi);
            this.panelKpiMain.Controls.Add(this.labelFocusKpi);
            this.panelKpiMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelKpiMain.Location = new System.Drawing.Point(2, 22);
            this.panelKpiMain.Name = "panelKpiMain";
            this.panelKpiMain.Padding = new System.Windows.Forms.Padding(2);
            this.panelKpiMain.Size = new System.Drawing.Size(271, 101);
            this.panelKpiMain.TabIndex = 39;
            // 
            // memoEditFocusKpi
            // 
            this.memoEditFocusKpi.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.memoEditFocusKpi.Location = new System.Drawing.Point(7, 26);
            this.memoEditFocusKpi.Name = "memoEditFocusKpi";
            this.memoEditFocusKpi.Size = new System.Drawing.Size(199, 68);
            this.memoEditFocusKpi.TabIndex = 35;
            this.memoEditFocusKpi.EditValueChanged += new System.EventHandler(this.memoEditFocusKpi_EditValueChanged);
            // 
            // labelFocusKpi
            // 
            this.labelFocusKpi.Location = new System.Drawing.Point(7, 7);
            this.labelFocusKpi.Name = "labelFocusKpi";
            this.labelFocusKpi.Size = new System.Drawing.Size(112, 13);
            this.labelFocusKpi.TabIndex = 34;
            this.labelFocusKpi.Text = "Фокусный KPI месяца:";
            // 
            // timer
            // 
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // ParamConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Name = "ParamConfiguration";
            this.Size = new System.Drawing.Size(750, 499);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.groupSKUConfig)).EndInit();
            this.groupSKUConfig.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.treeSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.gridListSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.panelButtons)).EndInit();
            this.panelButtons.ResumeLayout(false);
            this.panelButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.textEditSKUName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupKpiConfig)).EndInit();
            this.groupKpiConfig.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.groupKpi2)).EndInit();
            this.groupKpi2.ResumeLayout(false);
            this.groupKpi2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.cbType2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.cbKpi2Sku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.groupKpi1)).EndInit();
            this.groupKpi1.ResumeLayout(false);
            this.groupKpi1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.cbType1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.cbKpi1Sku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.panelKpiMain)).EndInit();
            this.panelKpiMain.ResumeLayout(false);
            this.panelKpiMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.memoEditFocusKpi.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainer;
        private DevExpress.XtraEditors.GroupControl groupSKUConfig;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelButtons;
        private DevExpress.XtraEditors.LabelControl labelSKUName;
        private DevExpress.XtraEditors.TextEdit textEditSKUName;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.GroupControl groupKpiConfig;
        private DevExpress.XtraTreeList.TreeList treeSKU;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnParentId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnSKUName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnSKUChecked;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnItemID;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit;
        private DevExpress.XtraGrid.GridControl gridListSKU;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProductCnTopId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRegionId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEditable;
        private DevExpress.XtraEditors.PanelControl panelKpiMain;
        private DevExpress.XtraEditors.LabelControl labelFocusKpi;
        private DevExpress.XtraEditors.GroupControl groupKpi1;
        private DevExpress.XtraEditors.LabelControl labelType1;
        private DevExpress.XtraEditors.ComboBoxEdit cbType1;
        private DevExpress.XtraEditors.ComboBoxEdit cbKpi1Sku;
        private DevExpress.XtraEditors.GroupControl groupKpi2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbType2;
        private DevExpress.XtraEditors.ComboBoxEdit cbKpi2Sku;
        private DevExpress.XtraEditors.CheckButton btnEdit;
        private DevExpress.XtraEditors.MemoEdit memoEditFocusKpi;
        private DevExpress.XtraEditors.SimpleButton btnDown;
        private DevExpress.XtraEditors.SimpleButton btnUp;
        private System.Windows.Forms.Timer timer;

    }
}
