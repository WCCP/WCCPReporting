﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Data;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.M3KaTrade.DataProvider;

namespace SoftServe.Reports.M3KaTrade.UserControls {
    public partial class ParamConfiguration: UserControl {
        #region Fields

        private const int MaxSkuLimit = 25;

        private DataTable _skuTreeTable;
        private DataTable _skuListTable;
        private DataTable _skuListDetailsTable;
        private DataTable _kpiTable;
        private readonly List<SkuConfigModel> _skuConfigList = new List<SkuConfigModel>();
        private int _currentIndex = -1;
        private int _nextProdCnTopId = -1;
        private bool _isAddingMode;
        private bool _dataLoaded;
        private bool _isKpiModified;
        private bool _isFocusKpiModified;
        private bool _isSkuConfigModified;
        private bool _isSkuTreeModified;
        private bool _internalFocusedRowChanging;
        private int _rowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle;

        #endregion

        public ParamConfiguration() {
            InitializeComponent();
            ProductCnTopId = -100;
        }

        internal int ProductCnTopId1 {
            get { return (int) ((ComboBoxItem) cbKpi1Sku.SelectedItem).Id; }
        }

        internal int ProductCnTopId2 {
            get { return (int) ((ComboBoxItem) cbKpi2Sku.SelectedItem).Id; }
        }

        internal int Type1 {
            get { return (int) ((ComboBoxItem) cbType1.SelectedItem).Id; }
        }

        internal int Type2 {
            get { return (int) ((ComboBoxItem) cbType2.SelectedItem).Id; }
        }

        internal string FocusKpiText {
            get { return memoEditFocusKpi.EditValue.ToString(); }
        }

        internal int CurrentIndex {
            get {
                return gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
            }
        }

        internal SkuConfigModel CurrentSkuConfig {
            get {
                if (_currentIndex < 0 || _currentIndex > _skuConfigList.Count - 1)
                    return null;
                return _skuConfigList[_currentIndex];
            }
        }

        public int DsmId { get; set; }
        public DateTime Date { get; set; }
        public int UserLevel { get; set; }
        public int RegionId { get; set; }
        public int ProductCnTopId { get; set; }

        public bool IsChanged {
            get { return _isKpiModified || _isFocusKpiModified || _isSkuConfigModified; }
        }

        public void LoadData(int dsmId, DateTime date) {
            _dataLoaded = false;
            DsmId = dsmId;
            Date = date;

            // SKU settings
            LoadSkuTree();
            LoadSkuList();
            _isSkuConfigModified = false;
            _isSkuTreeModified = false;

            // KPI settings
            LoadKpiSettings();
            _dataLoaded = true;
        }

        private void LoadSkuList() {
            gridListSKU.DataSource = null;
            _skuListTable = DataAccessProvider.GetSKUList(Date);
            _skuListTable.PrimaryKey = new DataColumn[] {_skuListTable.Columns[SqlConstants.ProductCnTopIdFieldName]};
            _skuListDetailsTable = DataAccessProvider.GetSKUListDetails(Date);
            _skuConfigList.Clear();
            foreach (DataRow lRow in _skuListTable.Rows) {
                SkuConfigModel lSkuConfig = new SkuConfigModel((int) lRow[SqlConstants.ProductCnTopIdFieldName]);
                lSkuConfig.SetName((string) lRow[SqlConstants.GroupNameFieldName]);
                lSkuConfig.SetEditable((bool) lRow[SqlConstants.EditableFieldName]);
                lSkuConfig.SetStartDate((DateTime) lRow[SqlConstants.StartDateFieldName]);
                lSkuConfig.SetRegionId((int) lRow[SqlConstants.RegionIdFieldName]);
                lSkuConfig.SetGroupRowNum(ConvertEx.ToDouble(lRow[SqlConstants.GroupRowNumFieldName]));
                PrepareIdList(lSkuConfig);
                lSkuConfig.Apply();
                _skuConfigList.Add(lSkuConfig);
            }
            gridListSKU.DataSource = _skuConfigList;
            AllowEdit(false);
            //lLoad RegionId regarding to UserLevel
            if (UserType.Current.IsM5)
                RegionId = -1;
            else
                RegionId = DataAccessProvider.GetUserRegionId();
        }

        private void LoadSkuTree() {
            treeSKU.BeginUpdate();
            try {
                treeSKU.DataSource = null;
                _skuTreeTable = DataAccessProvider.GetSKUTree(-1);
                _skuTreeTable.PrimaryKey = new DataColumn[] {_skuTreeTable.Columns[SqlConstants.IdFieldName]};
                treeSKU.DataSource = _skuTreeTable;
            }
            finally {
                treeSKU.EndUpdate();
            }
        }

        public void SaveChanges() {
            SaveSkuConfigListSettings();
            SaveKpiSettings();
            //ShowDataSaved();
        }

        private void PrepareIdList(SkuConfigModel skuConfig) {
            DataView lDataView = _skuListDetailsTable.Copy().DefaultView;
            lDataView.RowFilter = string.Format("{0} = {1}", SqlConstants.ProductCnTopIdFieldName, skuConfig.Id);
            DataTable lTable = lDataView.ToTable();
            string lFilter = string.Empty;
            foreach (DataRow lRow in lTable.Rows) {
                lFilter += string.Format(" OR ({0} = {1} AND {2} = {3})",
                    SqlConstants.ItemIdFieldName, lRow[SqlConstants.ItemIdFieldName],
                    SqlConstants.LevelFieldName, lRow[SqlConstants.LevelFieldName]);
            }
            lFilter = lFilter.TrimStart(" OR ".ToCharArray());
            lDataView = _skuTreeTable.Copy().DefaultView;
            lDataView.RowFilter = lFilter;
            lTable = lDataView.ToTable();
            foreach (DataRow lRow in lTable.Rows) {
                skuConfig.AddIdToList(ConvertEx.ToInt(lRow[SqlConstants.IdFieldName]));
            }
        }

        private void LoadKpiSettings() {
            //Focus KPI text
            memoEditFocusKpi.EditValue = DataAccessProvider.GetFocusKpiText(DsmId, Date);

            //KPI
            _kpiTable = DataAccessProvider.GetKpiList(DsmId, Date);
            if (_kpiTable == null || _kpiTable.Rows.Count == 0)
                return;
            DataRow lDataRow = _kpiTable.Rows[0];
            //KPI names
            groupKpi1.Text = lDataRow[SqlConstants.Kpi1FieldName].ToString();
            groupKpi2.Text = lDataRow[SqlConstants.Kpi2FieldName].ToString();
            // fill KPI Type comboboxes
            ComboBoxItem lType1 = new ComboBoxItem() {
                Id = ConvertEx.ToInt(lDataRow[SqlConstants.Type1FieldName]),
                Text = lDataRow[SqlConstants.TypeName1FieldName].ToString()
            };
            ComboBoxItem lType2 = new ComboBoxItem() {
                Id = ConvertEx.ToInt(lDataRow[SqlConstants.Type2FieldName]),
                Text = lDataRow[SqlConstants.TypeName2FieldName].ToString()
            };
            cbType1.Properties.Items.Add(lType1);
            cbType1.Properties.Items.Add(lType2);
            cbType2.Properties.Items.Add(lType1);
            cbType2.Properties.Items.Add(lType2);
            cbType1.SelectedIndex = 0;
            cbType2.SelectedIndex = 1;

            //fill SKU comboboxes
            int lProdCnTopId1 = ConvertEx.ToInt(lDataRow[SqlConstants.ProductCnTopId1FieldName]);
            int lProdCnTopId2 = ConvertEx.ToInt(lDataRow[SqlConstants.ProductCnTopId2FieldName]);
            List<ComboBoxItem> lSkuList = new List<ComboBoxItem>();
            foreach (DataRow lRow in _skuListTable.Rows) {
                ComboBoxItem lSkuItem = new ComboBoxItem() {
                    Id = ConvertEx.ToInt(lRow[SqlConstants.ProductCnTopIdFieldName]),
                    Text = lRow[SqlConstants.GroupNameFieldName].ToString()
                };
                lSkuList.Add(lSkuItem);
            }
            cbKpi1Sku.Properties.Items.AddRange(lSkuList);
            cbKpi2Sku.Properties.Items.AddRange(lSkuList);
            //select SKU
            if (lProdCnTopId1 > 0) {
                foreach (ComboBoxItem lItem in cbKpi1Sku.Properties.Items) {
                    if (lProdCnTopId1 == (int) lItem.Id) {
                        cbKpi1Sku.SelectedItem = lItem;
                        break;
                    }
                }
            }
            if (lProdCnTopId2 > 0) {
                foreach (ComboBoxItem lItem in cbKpi2Sku.Properties.Items) {
                    if (lProdCnTopId2 == (int) lItem.Id) {
                        cbKpi2Sku.SelectedItem = lItem;
                        break;
                    }
                }
            }
            _isKpiModified = false;
            _isFocusKpiModified = false;
        }

        private bool IsSkuConfigEditable() {
            if (null == CurrentSkuConfig)
                return false;
            if (CurrentSkuConfig.Editable)
                return true;
            return false;
        }

        private void AllowEdit(bool allowEdit) {
            if (allowEdit)
                if (!IsSkuConfigEditable())
                    return;
            btnEdit.Checked = allowEdit;
            if (btnEdit.Checked)
                btnEdit.ButtonStyle = BorderStyles.Simple;
            else
                btnEdit.ButtonStyle = BorderStyles.Default;

            textEditSKUName.Properties.ReadOnly = !allowEdit;
            treeSKU.OptionsBehavior.Editable = allowEdit;
        }

        private static void ClearSkuTreeTable(DataTable dataTable) {
            foreach (DataRow lRow in dataTable.Rows)
                lRow["Checked"] = false;
        }

        private void RefreshSKUTree() {
            treeSKU.BeginUpdate();
            try {
                ClearSkuTreeTable(_skuTreeTable);
                if (CurrentSkuConfig != null) {
                    if (!CurrentSkuConfig.SelectedIdList.Equals(string.Empty)) {
                        DataView lDataView = _skuTreeTable.DefaultView;
                        lDataView.RowFilter = string.Format("{0} IN ({1})", columnId.FieldName, CurrentSkuConfig.SelectedIdList);
                        for (int lIndex = 0; lIndex < lDataView.Count; lIndex++)
                            lDataView[lIndex]["Checked"] = true;
                        lDataView.RowFilter = string.Empty;
                    }
                }
                _skuTreeTable.AcceptChanges();
                treeSKU.ExpandAll();
                treeSKU.CollapseAll();
                LoadSKUTreeState();
            }
            finally {
                treeSKU.EndUpdate();
            }
            treeSKU.NodesIterator.DoOperation(new ExpandNodesOperation());
        }

        private void LoadSKUTreeState() {
            treeSKU.NodesIterator.DoOperation(new UpdateNodesCheckStateOperation(columnSKUChecked));
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            if (_skuConfigList.Count >= MaxSkuLimit) {
                MessageBox.Show(string.Format("Нельзя добавить больше {0} СКЮ.", MaxSkuLimit), "Предупреждение", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            _isAddingMode = true;
            try {
                // handle SKU Tree changes
                _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
                if (_isSkuTreeModified && CurrentSkuConfig != null) {
                    _isSkuConfigModified = true;
                    string lStr = GetSelectedIdList();
                    //handle empty SKU tree
                    if (lStr == "") {
                        XtraMessageBox.Show("Выберите хотя бы один продукт в дереве.", "M3 Ka Trade", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    //handle modified SKU tree
                    CurrentSkuConfig.SelectedIdList = lStr;
                    _isSkuTreeModified = false;
                }
                // add new SKU
                _isSkuConfigModified = true;
                _isSkuTreeModified = true;
                SkuConfigModel lNewSkuConfig = new SkuConfigModel(0, "<Новый Продукт>");
                lNewSkuConfig.Id = _nextProdCnTopId--;
                lNewSkuConfig.RegionId = RegionId;
                lNewSkuConfig.StartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                lNewSkuConfig.Editable = true;
                lNewSkuConfig.GroupRowNum = (_skuConfigList.Count > 0) ? _skuConfigList[_skuConfigList.Count - 1].GroupRowNum + 1 : 1;
                lNewSkuConfig.Apply();
                lNewSkuConfig.MakeNew();
                _skuConfigList.Add(lNewSkuConfig);
                gridView.RefreshData();
                gridView.FocusedRowHandle = gridView.GetRowHandle(_skuConfigList.Count - 1);
                _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
                textEditSKUName.EditValue = lNewSkuConfig.Name;
                AllowEdit(true);
                RefreshSKUTree();
                ProductCnTopId = CurrentSkuConfig.Id;
            }
            finally {
                _isAddingMode = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            DeleteProductCnTop();
        }

        private void btnEdit_CheckedChanged(object sender, EventArgs e) {
            AllowEdit(btnEdit.Checked);
        }

        private void DeleteProductCnTop() {
            _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
            if (CurrentSkuConfig == null)
                return;
            if (!CurrentSkuConfig.Editable) {
                MessageBox.Show("Вам запрещено удалять неактивные СКЮ.", "Предупреждение", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            int lErrorCode = 0;
            int lProdCnTopId = CurrentSkuConfig.Id;
            if (lProdCnTopId > 0)
                lErrorCode = DataAccessProvider.DeleteProductCnTop(lProdCnTopId);

            if (lErrorCode < 0) {
                DialogResult lDialogResult = MessageBox.Show(
                    "Существуют подсчитанные факты или введены планы на месяц.\n Вы действительно хотите удалить СКЮ и его факты в текущем месяце?",
                    "Предупреждение", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation);
                
                if (lDialogResult == DialogResult.Yes)
                    lErrorCode = DataAccessProvider.DeleteProductCnTop(lProdCnTopId, true);
                else
                    return;
            }

            if (lErrorCode != 0) {
                MessageBox.Show("Невозможно удалить СКЮ. Существуют подсчитанные факты или введены планы на месяц.", "Предупреждение", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            int lPrevRowHandle = gridView.FocusedRowHandle;
            gridView.DeleteRow(gridView.FocusedRowHandle);
            if (gridView.FocusedRowHandle == _rowHandle) {
                RefreshSKUTree();
                textEditSKUName.EditValue = "";
            }
            if (lPrevRowHandle == gridView.FocusedRowHandle)
                gridView_FocusedRowChanged(gridView, new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(_rowHandle, gridView.FocusedRowHandle));
        }

        private void textEditSKUName_EditValueChanged(object sender, EventArgs e) {
            if (_isAddingMode)
                return;

            if (gridView.GetFocusedRowCellValue(gridColumnGroupName) == textEditSKUName.EditValue)
                return;

            gridView.SetFocusedRowCellValue(gridColumnGroupName, textEditSKUName.EditValue);
            _isSkuConfigModified = true;
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) {
            if (_isAddingMode)
                return;

			if (_internalFocusedRowChanging)
                return;

            _currentIndex = gridView.GetDataSourceRowIndex(e.FocusedRowHandle);
            if (CurrentSkuConfig == null)
                return;
            if (ProductCnTopId == CurrentSkuConfig.Id)
                return;

            //handle modified row
            _currentIndex = gridView.GetDataSourceRowIndex(e.PrevFocusedRowHandle);
            if (_isSkuTreeModified && CurrentSkuConfig != null) {
                _isSkuConfigModified = true;
                string lStr = GetSelectedIdList();
                if (lStr == "") {
                    XtraMessageBox.Show("Выберите хотя бы один продукт в дереве.", "M3 Ka Trade", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _rowHandle = e.PrevFocusedRowHandle < 0 ? e.FocusedRowHandle : e.PrevFocusedRowHandle;
                    timer.Start();
                    return;
                }
                CurrentSkuConfig.SelectedIdList = lStr;
                _isSkuTreeModified = false;
            }

            //handle change position
            _currentIndex = gridView.GetDataSourceRowIndex(e.FocusedRowHandle);
            if (CurrentSkuConfig == null)
                return;
            int lProdCnTopId = CurrentSkuConfig.Id;
            if (ProductCnTopId == lProdCnTopId)
                return;
            textEditSKUName.EditValue = CurrentSkuConfig.Name;
            AllowEdit(false);
            ProductCnTopId = lProdCnTopId;
            RefreshSKUTree();
        }

        private void AttachEvents(bool attach) {
            if (attach) {
                treeSKU.AfterCheckNode += treeSKU_AfterCheckNode;
                treeSKU.BeforeCheckNode += treeSKU_BeforeCheckNode;
            }
            else {
                treeSKU.BeforeCheckNode -= treeSKU_BeforeCheckNode;
                treeSKU.AfterCheckNode -= treeSKU_AfterCheckNode;
            }
        }

        private void treeSKU_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e) {
            e.CanCheck = treeSKU.OptionsBehavior.Editable;
        }

        private void treeSKU_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e) {
            _isSkuTreeModified = true;

            treeSKU.BeginUpdate();
            AttachEvents(false);
            try {
                if (!e.Node.Checked)
                    OnNodeCheckRecursive(e.Node, false);
            }
            finally {
                AttachEvents(true);
                treeSKU.EndUpdate();
            }
        }

        private static void OnNodeCheckRecursive(TreeListNode node, bool isChecked) {
            if (node == null)
                return;

            node.Checked = isChecked;
            foreach (TreeListNode lChildNode in node.Nodes)
                OnNodeCheckRecursive(lChildNode, isChecked);
        }

        private void SaveKpiSettings() {
            if (_isKpiModified)
                DataAccessProvider.SaveKpiSettings(DsmId, ProductCnTopId1, ProductCnTopId2, Type1, Type2, Date);
            _isKpiModified = false;
            if (_isFocusKpiModified)
                DataAccessProvider.SaveFocusKpi(DsmId, Date, FocusKpiText);
            _isFocusKpiModified = false;
        }

        private void SaveSkuConfigListSettings() {
            _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
            if (_isSkuTreeModified && CurrentSkuConfig != null) {
                _isSkuConfigModified = true;
                string lStr = GetSelectedIdList();
                //handle empty SKU tree
                if (lStr == "") {
                    XtraMessageBox.Show("Выберите хотя бы один продукт в дереве.", "M3 Ka Trade", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                //handle modified SKU tree
                CurrentSkuConfig.SelectedIdList = lStr;
                _isSkuTreeModified = false;
            }
            //save SKU list
            foreach (SkuConfigModel lItem in _skuConfigList) {
                if (lItem.IsModified())
                    DataAccessProvider.SaveSkuConfig(lItem, Date);
                if (lItem.IsIdListModified()) {
                    SqlXml lSqlXml = GetSelectedSkuXml(lItem.GetSelectedIdList());
                    DataAccessProvider.SaveSKUTree(lItem.Id, lSqlXml);
                }
                lItem.Apply();
            }
            _isSkuConfigModified = false;
            _isSkuTreeModified = false;
        }

        private string GetSelectedIdList() {
            treeSKU.PostEditor();
            DataTable lDataTable = treeSKU.DataSource as DataTable;
            if (lDataTable == null)
                return string.Empty;
            
            lDataTable.AcceptChanges();
            GetFinalCheckedNodesOperation lNodesOperation = new GetFinalCheckedNodesOperation(columnId) {IsQuotesSeparated = false, IsKPItree = true};
            treeSKU.NodesIterator.DoOperation(lNodesOperation);
            if (lNodesOperation.Count > 0)
                return lNodesOperation.SelectedIdList;
            return string.Empty;
        }

        private SqlXml GetSelectedSkuXml(string selectedIdList) {
            if (selectedIdList.Equals(string.Empty))
                return GetXmlSnapshot(null);
            DataView lDataView = _skuTreeTable.Copy().DefaultView;
            lDataView.RowFilter = string.Format("{0} IN ({1})", columnId.FieldName, selectedIdList);
            return GetXmlSnapshot(lDataView.ToTable());
        }

        private static SqlXml GetXmlSnapshot(DataTable dataTable) {
            MemoryStream lStream = new MemoryStream(1024 * 10);

            XmlWriterSettings lSettings = new XmlWriterSettings();
            lSettings.OmitXmlDeclaration = true;

            XmlWriter lWriter = XmlWriter.Create(lStream, lSettings);
            lWriter.WriteStartElement("root");
            if (dataTable != null)
                foreach (DataRow lRow in dataTable.Rows) {
                    lWriter.WriteStartElement("row");
                    AddFullEndXmlElement(lWriter, SqlConstants.ItemIdFieldName.ToLower(), lRow[SqlConstants.ItemIdFieldName]);
                    AddFullEndXmlElement(lWriter, SqlConstants.LevelFieldName.ToLower(), lRow[SqlConstants.LevelFieldName]);
                    lWriter.WriteEndElement();
                }
            lWriter.WriteFullEndElement();
            lWriter.Flush();
            lStream.Position = 0;

            return new SqlXml(lStream);
        }

        private static void AddFullEndXmlElement(XmlWriter writer, string nodeName, object nodeValue) {
            writer.WriteStartElement(nodeName);
            if (nodeValue != null && nodeValue != DBNull.Value) {
                decimal lValue = ConvertEx.ToDecimal(nodeValue);
                writer.WriteString(lValue.ToString(CultureInfo.InvariantCulture));
            }
            writer.WriteEndElement();
        }

        private static void ShowDataSaved() {
            XtraMessageBox.Show(Resource.DataSaved, "M3 Ka Trade", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void cbKpi1Sku_SelectedIndexChanged(object sender, EventArgs e) {
            _isKpiModified = true;
        }

        private void cbKpi2Sku_SelectedIndexChanged(object sender, EventArgs e) {
            _isKpiModified = true;
        }

        private void cbType1_SelectedIndexChanged(object sender, EventArgs e) {
            _isKpiModified = true;
        }

        private void cbType2_SelectedIndexChanged(object sender, EventArgs e) {
            _isKpiModified = true;
        }

        private void memoEditFocusKpi_EditValueChanged(object sender, EventArgs e) {
            _isFocusKpiModified = true;
        }

        private void btnDown_Click(object sender, EventArgs e) {
            _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
            if (!CurrentSkuConfig.Editable) {
                MessageBox.Show("Запрещено перемещать по списку неактивные СКЮ.", "Предупреждение", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            // if last position do nothing
            if (_currentIndex == _skuConfigList.Count - 1)
                return;
            
            _isSkuConfigModified = true;
            int lNextIndex = _currentIndex + 1;
            //SkuConfigModel lSkuConfigNext = (SkuConfigModel) _skuConfigList[lNextIndex].Clone();
            SkuConfigModel lSkuConfigNext = _skuConfigList[lNextIndex];
            _skuConfigList[lNextIndex] = CurrentSkuConfig;
            _skuConfigList[_currentIndex] = lSkuConfigNext;
            gridView.FocusedRowHandle = gridView.GetRowHandle(lNextIndex);
            if (lNextIndex + 1 > _skuConfigList.Count - 1)
                CurrentSkuConfig.GroupRowNum = lSkuConfigNext.GroupRowNum + 1.0;
            else
                CurrentSkuConfig.GroupRowNum = (lSkuConfigNext.GroupRowNum + _skuConfigList[lNextIndex + 1].GroupRowNum) / 2.0;
        }

        private void btnUp_Click(object sender, EventArgs e) {
            _currentIndex = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
            if (!CurrentSkuConfig.Editable) {
                MessageBox.Show("Запрещено перемещать по списку неактивные СКЮ.", "Предупреждение", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            // if first position do nothing
            if (_currentIndex == 0)
                return;

            _isSkuConfigModified = true;
            int lPrevIndex = _currentIndex - 1;
            SkuConfigModel lSkuConfigPrev = _skuConfigList[lPrevIndex];
            _skuConfigList[lPrevIndex] = CurrentSkuConfig;
            _skuConfigList[_currentIndex] = lSkuConfigPrev;
            gridView.FocusedRowHandle = gridView.GetRowHandle(lPrevIndex);
            //_isSkuTreeModified = true;
            if (lPrevIndex - 1 < 0)
                CurrentSkuConfig.GroupRowNum = lSkuConfigPrev.GroupRowNum - 1.0;
            else
                CurrentSkuConfig.GroupRowNum = (lSkuConfigPrev.GroupRowNum + _skuConfigList[lPrevIndex - 1].GroupRowNum) / 2.0;
        }

        private void timer_Tick(object sender, EventArgs e) {
			_internalFocusedRowChanging = true;
			try {
				timer.Stop();
				gridView.FocusedRowHandle = _rowHandle;
				_rowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle;
			}
			finally {
				_internalFocusedRowChanging = false;
			}
        }

    }
}
