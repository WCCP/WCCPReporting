﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.ViewInfo;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.M3KaTrade.DataProvider;

namespace SoftServe.Reports.M3KaTrade.UserControls {
    public partial class SKUPlanTree: UserControl {
        private readonly List<int> _changedIds = new List<int>();
        private TreeListNode ClickedNode;

        private int DsmId { get; set; }
        private DateTime Date { get; set; }
        private decimal PreviousCellValue { get; set; }

        public SKUPlanTree() {
            InitializeComponent();
        }

        public bool IsChanged {
            get { return _changedIds.Count > 0; }
        }

        /// <summary>
        /// Loads the specified data table.
        /// </summary>
        /// <param name="dsmId">The DSM Id</param>
        /// <param name="date">The Report Date</param>
        public void LoadData(int dsmId, DateTime date) {
            DsmId = dsmId;
            Date = date;

            AttachEvents(false);
            tree.BeginUpdate();
            try {
                DataTable lDataTable = DataAccessProvider.GetSKUPlan(DsmId, Date);
                if (lDataTable == null)
                    return;

                lDataTable.PrimaryKey = new[] {lDataTable.Columns[columnId.FieldName]};
                tree.DataSource = lDataTable;
                tree.PopulateColumns();
            
                // Restore column settings
                CustomizeColumns();
            }
            finally {
                tree.EndUpdate();
                AttachEvents(true);
            }
        }

        private void CustomizeColumns() {
            // general fields
            tree.Columns[columnStaffName.FieldName].Width = 300;
            tree.Columns[columnStaffId.FieldName].Visible = false;
            tree.Columns[columnStaffLevel.FieldName].Visible = false;
            tree.Columns[columnStaffParentId.FieldName].Visible = false;
            tree.Columns[columnStartDate.FieldName].Visible = false;

            RestoreFieldSettings(columnStaffName);

            // specific fields
            tree.Columns[columnSKUName.FieldName].Width = 300;
            tree.Columns[columnProductCnTopId.FieldName].Visible = false;
            tree.Columns[columnGroupRowNum.FieldName].Visible = false;
            tree.Columns[columnATTCount.FieldName].Visible = false;
            tree.Columns[columnOLCount.FieldName].Visible = false;

//            columnTargetSaleMonthly.Format.FormatType = FormatType.Numeric;
//            columnTargetSaleMonthly.Format.FormatString = "{0:N2}";
//            columnTargetNumATT.Format.FormatType = FormatType.Numeric;
//            columnTargetNumATT.Format.FormatString = "{0:N2}";

            RestoreFieldSettings(columnSKUName);
            RestoreFieldSettings(columnTargetSaleMonthly, true);
            RestoreFieldSettings(columnTargetNumATT, true);
        }

        /// <summary>
        /// Restores the field settings.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="editable"></param>
        private void RestoreFieldSettings(TreeListColumn column, bool editable = false) {
            tree.Columns[column.FieldName].OptionsColumn.ReadOnly = !editable;
            tree.Columns[column.FieldName].OptionsColumn.AllowEdit = editable;
            tree.Columns[column.FieldName].Caption = column.Caption;
            tree.Columns[column.FieldName].Fixed = editable ? FixedStyle.None : FixedStyle.Left;
            if (editable) {
                tree.Columns[column.FieldName].ColumnEdit = repositoryItemN2Edit;
                tree.Columns[column.FieldName].Format.FormatType = FormatType.Numeric;
                tree.Columns[column.FieldName].Format.FormatString = "n2";
//                if (column.FieldName == columnTargetSaleMonthly.FieldName)
//                    tree.Columns[column.FieldName].ColumnEdit = repositoryItemN2Edit;
//                if (column.FieldName == columnTargetNumATT.FieldName)
//                    tree.Columns[column.FieldName].ColumnEdit = repositoryItemN2Edit;
            }
        }

        /// <summary>
        /// Attaches the events.
        /// </summary>
        /// <param name="attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach) {
            if (attach) {
                tree.CellValueChanged += tree_CellValueChanged;
            }
            else {
                tree.CellValueChanged -= tree_CellValueChanged;
            }
        }

        private void tree_CellValueChanging(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e) {
            PreviousCellValue = ConvertEx.ToDecimal(e.Node.GetValue(e.Column));
        }

        private void tree_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e) {
            if (!IsEditableColumn(e.Column))
                return;

            if (!IsCellEditAllowed(e.Column, e.Node))
                return;

            tree.BeginUpdate();
            try {
                RecalcParentNodes(e.Node, e.Column);
                //FireEditValueChanged(e);
            }
            finally {
                tree.EndUpdate();
            }
        }

        private void RecalcParentNodes(TreeListNode node, TreeListColumn column) {
            if (node == null)
                return;

            decimal lOldValue = PreviousCellValue;
            decimal lNewValue = ConvertEx.ToDecimal(node.GetValue(column));

            if (null == column)
                return;
            
            //recalc parent nodes
            if (node.ParentNode == null)
                return;
            if (lOldValue == lNewValue)
                return;
            
            //save Id of changed row
            int lId = ConvertEx.ToInt(node.GetValue(columnId));
            if (!_changedIds.Contains(lId))
                _changedIds.Add(lId);
            
            if (column.FieldName == columnTargetSaleMonthly.FieldName) {
                decimal lDelta = lNewValue - lOldValue;
                TreeListNode lParentNode = node.ParentNode;
                while (lParentNode != null) {
                    lId = ConvertEx.ToInt(lParentNode.GetValue(columnId));
                    if (!_changedIds.Contains(lId))
                        _changedIds.Add(lId);

                    decimal lParentValue = ConvertEx.ToDecimal(lParentNode.GetValue(column));
                    lParentValue = lParentValue + lDelta;
                    lParentNode.SetValue(column, lParentValue);
                    lParentNode = lParentNode.ParentNode;
                }
            }

            if (column.FieldName == columnTargetNumATT.FieldName) {
                int lOLCount = ConvertEx.ToInt(node.GetValue(columnOLCount));
                int lATTCount = (int) Math.Round(lNewValue / 100 * lOLCount, 0, MidpointRounding.AwayFromZero);
                node.SetValue(columnATTCount, lATTCount);
                TreeListNode lParentNode = node.ParentNode;
                while (lParentNode != null) {
                    lId = ConvertEx.ToInt(lParentNode.GetValue(columnId));
                    if (!_changedIds.Contains(lId))
                        _changedIds.Add(lId);

                    int lATTCountParent = 0;
                    foreach (TreeListNode lNode in lParentNode.Nodes) {
                        lATTCountParent += ConvertEx.ToInt(lNode.GetValue(columnATTCount));
                    }
                    int lOLCountParent = ConvertEx.ToInt(lParentNode.GetValue(columnOLCount));
                    decimal lParentValue;
                    if (lOLCountParent == 0)
                        lParentValue = 100;
                    else
                        lParentValue = ConvertEx.ToDecimal(Math.Round(100.0 * lATTCountParent / lOLCountParent, 2, MidpointRounding.AwayFromZero));
                    lParentNode.SetValue(column, lParentValue);
                    lParentNode = lParentNode.ParentNode;
                }
            }
        }

        private bool IsEditableColumn(TreeListColumn column) {
            return (column.FieldName == columnTargetSaleMonthly.FieldName || column.FieldName == columnTargetNumATT.FieldName);
        }

        private bool IsCellEditAllowed(TreeListColumn column, TreeListNode node) {
            if (null == column || null == node)
                return false;

            int lStaffLvevel = ConvertEx.ToInt(node.GetValue(columnStaffLevel));
            if (lStaffLvevel > 1)
                return false;

            int lProdTopId = ConvertEx.ToInt(node.GetValue(columnProductCnTopId));
            if (lProdTopId == -1)
                return false;
//            bool lResult = false;

            return true;
        }

        private void tree_ShowingEditor(object sender, CancelEventArgs e) {
            e.Cancel = !IsCellEditAllowed(tree.FocusedColumn, tree.FocusedNode);
        }

        private void tree_CustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e) {
            if (IsCellEditAllowed(e.Column, e.Node))
                return;
            if (!IsEditableColumn(e.Column))
                return;

            Rectangle lRect = e.Bounds;
            lRect.Inflate(-1, -1);
            e.Graphics.FillRectangle(SqlConstants.BrushGray, lRect);
        }

        public void SaveChanges() {
            DataTable lDataTable = tree.DataSource as DataTable;
            if (lDataTable == null)
                return;
            lDataTable.AcceptChanges();

            DataTable lFilteredTable = GetFilteredTable(lDataTable);
            foreach (DataRow lRow in lFilteredTable.Rows)
                DataAccessProvider.SaveSKUPlanChangedRow(lRow, Date);
            _changedIds.Clear();
        }

        private DataTable GetFilteredTable(DataTable dataTable) {
            StringBuilder lIdListBuilder = new StringBuilder(1024);
            foreach (int lId in _changedIds)
                lIdListBuilder.AppendFormat("'{0}'{1}", lId, SqlConstants.DELIMITER_ID_LIST);

            string lIdList = lIdListBuilder.ToString().TrimEnd(SqlConstants.DELIMITER_ID_LIST.ToCharArray());
            string lFilter;
            if (IsChanged)
                //where ID in ( )
                lFilter = string.Format("{0} IN ({1})", columnId.FieldName, lIdList);
            else
                lFilter = "1 = 2";
            DataView lDataView1 = new DataView(dataTable);
            lDataView1.RowFilter = lFilter;

            DataTable lFilteredTable1 = lDataView1.ToTable();
            lFilteredTable1.PrimaryKey = new DataColumn[] {lFilteredTable1.Columns[columnId.FieldName]};
            return lFilteredTable1;
        }

        private void toolTipController_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e) {
            if (!(e.SelectedControl is TreeList))
                return;
            TreeList lTree = (TreeList)e.SelectedControl;
            TreeListHitInfo lHitInfo = lTree.CalcHitInfo(e.ControlMousePosition);

            if (lHitInfo.HitInfoType != HitInfoType.Cell)
                return;
            if (!IsEditableColumn(lHitInfo.Column))
                return;
            object lCellInfo = new TreeListCellToolTipInfo(lHitInfo.Node, lHitInfo.Column, null);
            DateTime lDate = ConvertEx.ToDateTime(lHitInfo.Node.GetValue(columnStartDate));
            string lToolTip = string.Format("Действует с {0}", lDate.ToString("Y"));
            e.Info =  new ToolTipControlInfo(lCellInfo, lToolTip);
        }

        private void tree_MouseUp(object sender, MouseEventArgs e) {
            TreeList lTree = sender as TreeList;

            if (e.Button == MouseButtons.Right && ModifierKeys == Keys.None && lTree.State == TreeListState.Regular) {
                Point lPoint = tree.PointToClient(MousePosition);
                TreeListHitInfo lHitInfo = lTree.CalcHitInfo(lPoint);
                if (lHitInfo.HitInfoType == HitInfoType.Cell) {
                    ClickedNode = lHitInfo.Node;
//                    lTree.FocusedNode = lHitInfo.Node;
//                    NeedRestoreFocused = true;
                    popupMenu.ShowPopup(MousePosition);
                }
            }
        }

        private void menuItemExpand_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            tree.ExpandAll();
        }

        private void menuItemCollapse_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            tree.CollapseAll();
        }

        private void menuItemExpand_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            if (ClickedNode == null)
                return;
            ClickedNode.Expanded = true;
        }

        private void menuItemCollapse_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            if (ClickedNode == null)
                return;
            ClickedNode.Expanded = false;
        }
    }
}
