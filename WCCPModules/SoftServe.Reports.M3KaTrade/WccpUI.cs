﻿using System;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using SoftServe.Reports.M3KaTrade;
using System.ComponentModel.Composition;
using ModularWinApp.Core.Interfaces;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccp_m3KaTrade.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Shows the UI.
        /// </summary>
        /// <param name="isSkined">The is skined.</param>
        /// <param name="reportCaption">The report caption.</param>
        /// <returns></returns>
        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                M3KaTradeSettingsForm setForm = new M3KaTradeSettingsForm();
                setForm.ReportId = reportId;

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.AddForm(setForm);

                    _reportControl = new WccpUIControl(reportId);
                    _reportCaption = Resource.M3Off;
                    
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
            
            return 1;
        }

        /// <summary>
        /// Closes the UI.
        /// </summary>
        public void CloseUI()
        {
            DialogResult result;
            if (WccpUIControl.ReportControl.HasUnsavedData())
            {
                result = MessageBox.Show(Resource.SaveData, Resource.DataNotSaved, MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    if (WccpUIControl.ReportControl.SaveUnsavedData())
                    {
                        XtraMessageBox.Show(Resource.DataSaved);
                    }
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}