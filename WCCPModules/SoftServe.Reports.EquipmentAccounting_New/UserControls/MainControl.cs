﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.EquipmentAccounting.Tabs;
using SoftServe.Reports.EquipmentAccounting.Utility;

namespace SoftServe.Reports.EquipmentAccounting.UserControls
{
    [ToolboxItem(false)]
    public partial class MainControl : UserControl
    {
        private const string CompanyDirectory = "SoftServe";
        private const string ProgramDirectory = "WCCP Reporting";
        private const string FileName = "EquipmentAccounting.xml";

        /// <summary>
        /// Is used for storing column width before view modification before export
        /// </summary>
        private List<int> _currentColumnsConf = new List<int>();

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the new item of MainControl class
        /// </summary>
        public MainControl(DateInterval reportDates)
        {
            InitializeComponent();

            if (reportDates.From != DateTime.MinValue)
            {
                gridView.ViewCaption = string.Format("Дата    с   {0}      по   {1}",
                                                     reportDates.From.Date.ToString("dd.MM.yyyy"),
                                                     reportDates.To.Date.ToString("dd.MM.yyyy"));
            }
            else
            {
                gridView.ViewCaption = string.Format("Дата    по   {0}",
                                                     reportDates.To.Date.ToString("dd.MM.yyyy"));
            }
        }

        /// <summary>
        /// Gets or sets the grid datasource
        /// </summary>
        public DataTable DataSource
        {
            get
            {
                return gridControl.DataSource as DataTable;
            }

            set
            {
                foreach (DataRow dr in value.Rows)
                {
                    dr[colRentalPrice.FieldName] =
                        double.Parse(ConvertEx.ToDouble(dr[colRentalPrice.FieldName]).ToString("N2"));
                    dr[colInitPrice.FieldName] =
                        double.Parse(ConvertEx.ToDouble(dr[colInitPrice.FieldName]).ToString("N2"));
                }

                foreach (DataColumn col in value.Columns)
                {
                    if (col.DataType == typeof(DateTime))
                    {
                        foreach (DataRow dr in value.Rows)
                        {
                            if (dr[col] != DBNull.Value)
                            {
                                dr[col] = ConvertEx.ToDateTime(dr[col]).Date;
                            }

                        }
                    }
                }

                gridControl.BeginUpdate();
                gridControl.DataSource = value;
                gridControl.EndUpdate();
            }
        }

        /// <summary>
        /// Changes view in order to export it properly 
        /// </summary>
        internal void PrepareViewToExport()
        {
            //turn off view caption
            gridView.OptionsView.ShowViewCaption = false;

            //turn off formatting
            foreach (GridColumn col in gridView.Columns)
            {
                col.DisplayFormat.FormatString = string.Empty;
            }

            //save column conf
            _currentColumnsConf.Clear();

            foreach (GridColumn column in gridView.Columns)
            {
                _currentColumnsConf.Add(column.Width);
            }

            //tune-up columns width
            gridView.BestFitColumns();

            //for custom header display (in xls file)
            if (gridView.VisibleColumns.Count >= 2)
            {
                gridView.VisibleColumns[0].Width = Math.Max(MainTab.HEADER_WIDTH, gridView.VisibleColumns[0].Width);
                gridView.VisibleColumns[1].Width = Math.Max(MainTab.HEADER_WIDTH, gridView.VisibleColumns[1].Width);
            }
        }

        /// <summary>
        /// Restores view state after export 
        /// </summary>
        internal void RestoreViewAfterExport()
        {
            //turn on view caption
            gridView.OptionsView.ShowViewCaption = true;

            // turn on formatting
            foreach (GridColumn col in gridView.Columns)
            {
                if (col.DisplayFormat.FormatType == FormatType.Numeric)
                {
                    col.DisplayFormat.FormatString = "N0";
                }
            }

            //restore columns width
            for (int i = 0; i < _currentColumnsConf.Count; i++)
            {
                gridView.Columns[i].Width = _currentColumnsConf[i];
            }
        }

        /// <summary>
        /// Handles ShowCustomizationForm event - is needed to show form over Report, not beneath
        /// </summary>
        /// <param name="sender">gridView</param>
        /// <param name="e">EventArgs</param>
        private void gridView_ShowCustomizationForm(object sender, EventArgs e)
        {
            gridView.CustomizationForm.TopMost = true;
        }

        /// <summary>
        /// Saves gridview layout for current user on disc
        /// </summary>
        internal void SaveLayout()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);

            if (!Directory.Exists(lCompanyDirPath))
            {
                Directory.CreateDirectory(lCompanyDirPath);
            }

            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);

            if (!Directory.Exists(lProgramDirPath))
            {
                Directory.CreateDirectory(lProgramDirPath);
            }

            string lFilePath = Path.Combine(lProgramDirPath, FileName);

            gridControl.MainView.SaveLayoutToXml(lFilePath);
        }

        /// <summary>
        /// Restores gridview layout for current user from file on disc
        /// </summary>
        internal void RestoreLayout()
        {
            string lCompanyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), CompanyDirectory);
            string lProgramDirPath = Path.Combine(lCompanyDirPath, ProgramDirectory);
            string lFilePath = Path.Combine(lProgramDirPath, FileName);

            if (File.Exists(lFilePath))
            {
                gridControl.MainView.RestoreLayoutFromXml(lFilePath);
            }
        }

        private void gridView_BeforeLoadLayout(object sender, DevExpress.Utils.LayoutAllowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.PreviousVersion))
            {
                e.Allow = false;
                return;
            }

            e.Allow = e.PreviousVersion == gridView.OptionsLayout.LayoutVersion;
        }
    }
}
