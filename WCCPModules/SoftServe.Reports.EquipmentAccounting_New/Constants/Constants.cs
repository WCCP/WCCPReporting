﻿namespace SoftServe.Reports.EquipmentAccounting
{
    /// <summary>
    /// Stored procedures and their params names 
    /// </summary>
    public static class Constants
    {
        #region Settings Window

        public const string SP_TERRITORY_TREE = "spDW_GetTerritoryTree";
        public const string SP_TERRITORY_TREE_LevelFrom_PARAM = "LevelFrom";
        public const string SP_TERRITORY_TREE_LevelTo_PARAM = "LevelTo";

        public const string SP_STAFFING_TREE = "spDW_GetHistoryStaff";
        public const string SP_STAFFING_TREE_Date_PARAM = "Date";

        public const string SP_DISTR_TYPES_LIST = "Pos.spDW_GetWorkSchemas";

        public const string SP_DISTRIBUTORS_TREE = "spDW_GetDistributorsTree";
        public const string SP_DISTRIBUTORS_TREE_LevelFrom_PARAM = "LevelFrom";
        public const string SP_DISTRIBUTORS_TREE_LevelTo_PARAM = "LevelTo";

        public const string SP_IS_EXPORT_MODE = "IsExportMode";

        public const string SP_CUSTOMERS_TREE = "spDW_GetCustomersTree";

        public const string SP_WAREHOUSES_TREE = "Pos.spDW_GetWarehouseLocationTree";
        
        public const string SP_TECHNICAL_CONDITONS_LIST = "Pos.spDW_GetTechnicalConditions";

        public const string SP_MODELS_LIST = "Pos.spDW_GetGroupsWithModels";
        public const string SP_BRANDS_LIST = "Pos.spDW_GetBrands";
        public const string SP_MANUFACTURERS_LIST = "Pos.spDW_GetListOfManufactures";

        public const string SP_STATUSES_LIST = "Pos.spDW_GetListOfStatuses";

        public const string SP_PROJECT_LIST = "Pos.spDW_GetProjects";

        #endregion Settings Window

        #region Main report tab

        public const string SP_GET_REPORT_DATA = "Pos.spDW_GetWarehouseOrders_New";

        public const string SP_GET_REPORT_DATA_DateStart_PARAM = "DateStart";
        public const string SP_GET_REPORT_DATA_DateEnd_PARAM = "DateEnd";

        public const string SP_GET_REPORT_DATA_JustForDateEnd_PARAM = "JustForDateEnd";

        public const string SP_GET_REPORT_DATA_Staff_PARAM = "Staff";

        public const string SP_GET_REPORT_DATA_Customer_PARAM = "Customer";
        
        public const string SP_GET_REPORT_DATA_Warehouse_PARAM = "Warehouse";
        
        public const string SP_GET_REPORT_DATA_Model_PARAM = "Model";
        public const string SP_GET_REPORT_DATA_Brand_PARAM = "Brand";
        public const string SP_GET_REPORT_DATA_Manufacturer_PARAM = "Manufacturer";

        public const string SP_GET_REPORT_DATA_TechCondition_PARAM = "TechCondition";

        public const string SP_GET_REPORT_DATA_ContractStatus_PARAM = "ContractStatus";
        public const string SP_GET_REPORT_DATA_OrderStatus_PARAM = "OrderStatus";

        public const string SP_GET_REPORT_DATA_DateProductionFrom_PARAM = "DateProductionFrom";
        public const string SP_GET_REPORT_DATA_DateProductionTo_PARAM = "DateProductionTo";

        public const string SP_GET_REPORT_DATA_RepairDateFrom_PARAM = "RecoveryDateFrom";
        public const string SP_GET_REPORT_DATA_RepairDateTo_PARAM = "RecoveryDateTo";

        public const string SP_GET_REPORT_DATA_DateInvoiceFrom_PARAM = "DateInvoiceFrom";
        public const string SP_GET_REPORT_DATA_DateInvoiceTo_PARAM = "DateInvoiceTo";

        public const string SP_GET_REPORT_DATA_ReservationDateFrom_PARAM = "ReservationDateFrom";
        public const string SP_GET_REPORT_DATA_ReservationDateTo_PARAM = "ReservationDateTo";

        public const string SP_GET_REPORT_DATA_ContractDateFrom_PARAM = "ContractDateFrom";
        public const string SP_GET_REPORT_DATA_ContractDateTo_PARAM = "ContractDateTo";

        public const string SP_GET_REPORT_DATA_InstalationDateFrom_PARAM = "SystemDateFrom";
        public const string SP_GET_REPORT_DATA_InstalationDateTo_PARAM = "SystemDateTo";

        public const string SP_GET_REPORT_DATA_ReturnSystemDateFrom_PARAM = "ReturnSystemDateFrom";
        public const string SP_GET_REPORT_DATA_ReturnSystemDateTo_PARAM = "ReturnSystemDateTo";

        public const string SP_GET_REPORT_DATA_Project_PARAM = "Project";

        #endregion Main report tab

        #region LDB
        public const string SP_TERRITORY_TREE_LDB = "spDW_GetTerritoryTree_LDB";
        
        public const string SP_STAFFING_TREE_LDB = "spDW_GetStaffTree_LDB";
        
        public const string SP_CUSTOMERS_TREE_LDB = "spDW_GetCustomersTree_LDB";

        public const string SP_DISTRIBUTORS_TREE_LDB = "spDW_GetDistributorsTree_LDB";
        
        public const string SP_WAREHOUSES_TREE_LDB = "Pos.spDW_GetWarehouseLocationTree_LDB";

        public const string SP_GET_REPORT_DATA_LDB = "Pos.spDW_GetWarehouseOrders_LDB";
        #endregion
    }
}
