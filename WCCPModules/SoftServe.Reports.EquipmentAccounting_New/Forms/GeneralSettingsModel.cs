﻿using System;
using BLToolkit.EditableObjects;

namespace SoftServe.Reports.EquipmentAccounting.Forms
{
    public abstract class GeneralSettingsModel : EditableObject<GeneralSettingsModel>
    {
        public abstract int Country_ID { get; set; }
        public abstract int M3_ID { get; set; }
        public abstract int AmountOfTheWorstOLs { get; set; }
        public abstract int AmountOfFocusOLs { get; set; }
        public abstract DateTime TimeOfFirstVisit { get; set; }
        public abstract DateTime TimeOfLastVisit { get; set; }
        public abstract decimal InBevBeerVolumeLimit { get; set; }
        public abstract int AmountOfDistPushSKU { get; set; }
        public abstract int AmountOfFocusSKU { get; set; }
        public abstract int AmountOfMP { get; set; }
        public abstract int AmountOfContracts { get; set; }
        public abstract int AmountOfFocusTargets { get; set; }
    }
}
