﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.EquipmentAccounting.Forms
{
    /// <summary>
    /// Coordinates roles
    /// </summary>
    static class RoleManager
    {
        private const string RoleSKU_M5 = "URM_<SKU>_RW_M5";
        private const string RoleSKU_M3 = "URM_<SKU>_RW_M3";
        private const string RoleDistrPush_M5 = "URM_<DistrPush>_RW_M5";
        private const string RoleDistrPush_M3 = "URM_<DistrPush>_RW_M3";
        private const string RoleFocusSKU_M5 = "URM_<FocusSKU>_RW_M5";
        private const string RoleFocusSKU_M3 = "URM_<FocusSKU>_RW_M3";
        private const string RoleFocusGoal_M5 = "URM_<Focus Goal>_RW_M5";
        private const string RoleFocusGoal_M3 = "URM_<Focus Goal>_RW_M3";
        private const string RoleKPIRating_M5 = "URM_<KPI Rating>_RW_M5";
        private const string RoleKPIRating_M3 = "URM_<KPI Rating>_RW_M3";
        private const string RoleGeneral_M5 = "URM_<General>_RW_M5";
        private const string RoleGeneral_M3 = "URM_<General>_RW_M3";

        private static Dictionary<Roles, string> _roles;
        private static Dictionary<Roles, bool> _curUserRoles; 
        
        static RoleManager()
        {
            _roles = new Dictionary<Roles, string>();

            _roles.Add(Roles.SkuM5, RoleSKU_M5);
            _roles.Add(Roles.SkuM3, RoleSKU_M3);
            _roles.Add(Roles.DistrPushM5, RoleDistrPush_M5);
            _roles.Add(Roles.DistrPushM3, RoleDistrPush_M3);
            _roles.Add(Roles.FocusSkuM5, RoleFocusSKU_M5);
            _roles.Add(Roles.FocusSkuM3, RoleFocusSKU_M3);
            _roles.Add(Roles.FocusGoalM5, RoleFocusGoal_M5);
            _roles.Add(Roles.FocusGoalM3, RoleFocusGoal_M3);
            _roles.Add(Roles.KpiRatingM5, RoleKPIRating_M5);
            _roles.Add(Roles.KpiRatingM3, RoleKPIRating_M3);
            _roles.Add(Roles.GeneralM5, RoleGeneral_M5);
            _roles.Add(Roles.GeneralM3, RoleGeneral_M3);
        }

        /// <summary>
        /// Load roles of current user from DB
        /// </summary>
        public static void GetRoles()
        {
            _curUserRoles = new Dictionary<Roles, bool>();

            foreach (Roles role in _roles.Keys)
            {
                _curUserRoles.Add(role, DataProvider.GetUserRole(_roles[role]));
            }
        }

        /// <summary>
        /// Checkes whether the user has the role in question
        /// </summary>
        /// <param name="role">role to be checked</param>
        /// <returns><c>true</c> if the user has the role</returns>
        public static bool IsInRole(Roles role)
        {
            return _curUserRoles != null && _curUserRoles.ContainsKey(role) && _curUserRoles[role];
        }
    }
}
