﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using SoftServe.Reports.EquipmentAccounting;
using System.IO;
using Logica.Reports.DataAccess;

namespace WccpReporting
{
    public class ReportExporter : IReportExporterExtFor178
    {
        private int _reportId = 178; // Учет оборудования
        private ExportToType _expType;
        private List<int> _rmIdList = new List<int>();
        private DateTime _reportDate;
        private string _exportPath;
        private WccpUIControl lRpt = null;
        public ReportExporter()
        {
            _reportDate = DateTime.Today;
        }

        #region IReportExporterExtFor178 implementation

        public void SetRMsIds(List<int> ids)
        {
            _rmIdList.AddRange(ids);
        }

        public void ExportRun()
        {
            lRpt = new WccpUIControl(_reportId);
            SettingsForm _sForm = lRpt.GetSettingsForm();
            _sForm.TStaffing.Nodes.TreeList.UncheckAll();
            foreach (var rmId in _rmIdList)
            {
                var node = _sForm.TStaffing.Nodes.FirstOrDefault(n => Int32.Parse(n["StaffID"].ToString()) == rmId);
                node.CheckState = CheckState.Checked;
                _sForm.TStaffing.On_AfterCheckNode(node);
            }
            
            lRpt.ReportInit(_sForm);
            
            ExportData(_exportPath);
            lRpt.Dispose();
        }

        public void SetExportType(ExportToType exportType)
        {
            _expType = exportType;
        }

        public void SetExportMode(ExportModeEnum exportMode)
        {
           
        }

        public int GetReportId()
        {
            return _reportId;
        }

        public void SetReportId(int reportId)
        {
            _reportId = reportId;
        }

        public string GetExportPath()
        {
            return _exportPath;
        }

        public void SetExportPath(string exportPath)
        {
           _exportPath = exportPath;
        }

        public DateTime GetReportDate(DateTime reportDate)
        {
            return _reportDate;
        }

        public void SetReportDate(DateTime date)
        {
            _reportDate = date;
        }

        public void SetDsmId(int dsmId)
        {
            
        }

        public void SetSupervisorId(int svId)
        {
            
        }

        #endregion

        private void ExportData(string path)
        {
            string lWorkPath = Directory.GetCurrentDirectory();
            string lDestinPath = path;


            string [] files = Directory.GetFiles(lDestinPath, "Report_*", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                File.Delete(file);
            }

            files = Directory.GetFiles(lDestinPath, "Template_*", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                File.Delete(file);
            }

            if (File.Exists(Path.Combine(lWorkPath, "Template_178.xlsx")))
            {
                File.Copy(Path.Combine(lWorkPath, "Template_178.xlsx"), Path.Combine(lDestinPath, "Template_178.xlsx"));
            }

            if (File.Exists(Path.Combine(lDestinPath, "Template_178.xlsx")))
            {
                string lDate = DateTime.Today.ToShortDateString();
                lDate = lDate.Replace(".", "_");
                lDate = lDate.Replace("/", "_");
                
                string lnewDestinationPath = Path.Combine(lDestinPath,
                    string.Format("Report_{0}.xlsx", lDate));
                File.Move(Path.Combine(lDestinPath, "Template_178.xlsx"), lnewDestinationPath);
                lDestinPath = lnewDestinationPath;
            }

            DataProvider.ExportToFie(lDestinPath);
        }
    }
}