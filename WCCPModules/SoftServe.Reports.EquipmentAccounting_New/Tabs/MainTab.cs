﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.EquipmentAccounting.UserControls;

namespace SoftServe.Reports.EquipmentAccounting.Tabs
{
    public class MainTab : XtraTabPage
    {
        public const int HEADER_WIDTH = 180;
        public const int HEADER_HIGHT = 30;

        private MainControl _mainControl;

        internal MainControl MainControl
        {
            get { return _mainControl; }
        }

        private ReportInfo Settings { get; set; }

        public MainTab()
        {
            ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            Text = "Основной отчет";
        }

        public MainTab(ReportInfo settings, DataTable sourceData):this()
        {
            Settings = settings;

            _mainControl = new MainControl(settings.ReportDates);
            _mainControl.Dock = System.Windows.Forms.DockStyle.Fill;
            _mainControl.DataSource = sourceData;
            Controls.Add(_mainControl);
        }

        /// <summary>
        /// Performs export to xls
        /// </summary>
        /// <param name="filePath">Path to file</param>
        /// <param name="type">Output file format</param>
        internal void ExportToXls(string filePath, ExportToType type)
        {
            PrintingSystem lPrintingSystem = new PrintingSystem();
            PrintableComponentLink lPrintableComponentLink = new PrintableComponentLink();

            lPrintableComponentLink.CreateReportHeaderArea += printableComponentLink_CreateReportHeaderArea;
            lPrintingSystem.Links.AddRange(new object[] { lPrintableComponentLink });


            _mainControl.PrepareViewToExport();

            lPrintableComponentLink.Component = _mainControl.gridControl;
            lPrintableComponentLink.CreateDocument();

            _mainControl.RestoreViewAfterExport();


            switch (type)
            {
                case ExportToType.Xls: lPrintableComponentLink.PrintingSystem.ExportToXls(filePath, new XlsExportOptions { SheetName = "Учет оборудования" });
                    break;
                case ExportToType.Xlsx: lPrintableComponentLink.PrintingSystem.ExportToXlsx(filePath, new XlsxExportOptions { SheetName = "Учет оборудования" });
                    break;
            }
        }
        
        /// <summary>
        /// Prints custom header on export
        /// </summary>
        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            if (Settings.ReportDates.From != DateTime.MinValue)
            {
                TextBrick lDateFromHeader;
                lDateFromHeader = e.Graph.DrawString("Дата с " + Settings.ReportDates.From.ToString("d", new CultureInfo("ru-Ru")), Color.Black, new RectangleF(0, 0, _mainControl.gridView.VisibleColumns[0].Width, HEADER_HIGHT), BorderSide.None);
                lDateFromHeader.Font = new Font("Tahoma", 11);
                lDateFromHeader.StringFormat = new BrickStringFormat(StringAlignment.Center);

                TextBrick lDateToHeader;
                lDateToHeader = e.Graph.DrawString("по " + Settings.ReportDates.To.ToString("d", new CultureInfo("ru-Ru")), Color.Black, new RectangleF(_mainControl.gridView.VisibleColumns[0].Width, 0, _mainControl.gridView.VisibleColumns[1].Width, HEADER_HIGHT), BorderSide.None);
                lDateToHeader.Font = new Font("Tahoma", 11);
                lDateToHeader.StringFormat = new BrickStringFormat(StringAlignment.Center);
            }
            else
            {
                TextBrick lDateToHeader;
                lDateToHeader = e.Graph.DrawString("Дата по " + Settings.ReportDates.To.ToString("d", new CultureInfo("ru-Ru")), Color.Black, new RectangleF(0, 0, _mainControl.gridView.VisibleColumns[0].Width, HEADER_HIGHT), BorderSide.None);
                lDateToHeader.Font = new Font("Tahoma", 11);
                lDateToHeader.StringFormat = new BrickStringFormat(StringAlignment.Center);
            }
        }

        /// <summary>
        /// Performs disposing of the tab
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_mainControl != null)
            {
                _mainControl.SaveLayout();
            }

            base.Dispose(disposing);
        }
    }
}
