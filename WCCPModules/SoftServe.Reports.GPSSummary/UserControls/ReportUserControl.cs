﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using DevExpress.XtraEditors;
using DevExpress.XtraPivotGrid;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.GPSSummary.DataProvider;

namespace SoftServe.Reports.GPSSummary.UserControls
{
    public partial class ReportUserControl : XtraUserControl
    {
        private ReportOptions reportOptions = null;
        private readonly Dictionary<PivotGridField, Func<PivotDrillDownDataSource, int>> funcs;
        private readonly Dictionary<PivotGridField, Func<IEnumerable<DataRow>, int>> dsFuncs;
        private readonly byte[] initialLayout;

        public ReportUserControl()
        {
            InitializeComponent();
            using (var stream = new MemoryStream())
            {
                pivotGridControl.SaveLayoutToStream(stream);
                initialLayout = stream.ToArray();
            }
            dsFuncs = new Dictionary<PivotGridField, Func<IEnumerable<DataRow>, int>>
                          {
                              {pgfOLCount, x =>x.Where(row => row[pgfPlanedOL.FieldName] != null && row[pgfPlanedOL.FieldName] != DBNull.Value).Select(row => ConvertEx.ToLongInt(row[pgfPlanedOL.FieldName])).Distinct().Count()},
                              {pgfVisit2, x => x.Select(row => ConvertEx.ToInt(row[pgfVisit2.FieldName])).Sum()},
                              {pgfOLWithCoord, x => x.Where(row => row[pgfOLWithCoord.FieldName] != null).Select(row => ConvertEx.ToLongInt(row[pgfOLWithCoord.FieldName])).Distinct().Count()},
                          };

            funcs = new Dictionary<PivotGridField, Func<PivotDrillDownDataSource, int>>
                        {
                            {pgfOLCount,x =>x.Cast<PivotDrillDownDataRow>().Where(row=>row[pgfPlanedOL]!= null && row[pgfPlanedOL]!= DBNull.Value).Select(row => ConvertEx.ToLongInt(row[pgfPlanedOL])).Distinct().Count()},
                            {pgfVisit,x => x.Cast<PivotDrillDownDataRow>().Select(row => ConvertEx.ToInt(row[pgfVisit])).Sum()},
                            {pgfVisit2,x =>x.Cast<PivotDrillDownDataRow>().Select(row => ConvertEx.ToInt(row[pgfVisit2])).Sum()},
                            {pgfOLWithCoord,x =>x.Cast<PivotDrillDownDataRow>().Where(row => row[pgfOLWithCoord] != null).Select(row => ConvertEx.ToLongInt(row[pgfOLWithCoord])).Distinct().Count()},
                            {pgfVisitWithCoord,x =>x.Cast<PivotDrillDownDataRow>().Select(row => ConvertEx.ToInt(row[pgfVisitWithCoord])).Sum()},
                            {pgfVisitInOL,x =>x.Cast<PivotDrillDownDataRow>().Select(row => ConvertEx.ToInt(row[pgfVisitInOL])).Sum()},
                            {pgfVisit2InOL,x =>x.Cast<PivotDrillDownDataRow>().Select(row => ConvertEx.ToInt(row[pgfVisit2InOL])).Sum()},
                            {pgfTimeInOL, 
                                x =>{
                                    if (x.RowCount > 0)
                                        {
                                            var ints =
                                                x
                                                .Cast<PivotDrillDownDataRow>()
                                                .Select(row => ConvertEx.ToInt(row[pgfTimeInOLForAvg]))
                                                .Where(v => v > 0);
                                            return (int)(ints.Any() ? ints.Average() : 0);
                                        }
                                        return 0;
                                    }
                                }
                        };
        }

        public void LoadData(ReportOptions options = null)
        {
            if (options != null)
            {
                reportOptions = options;
            }
            if (reportOptions == null)
            {
                return;
            }

            pivotGridControl.DataSource = null;
            using (var stream = new MemoryStream(initialLayout))
            {
                pivotGridControl.RestoreLayoutFromStream(stream);
            }

            WaitManager.StartWait();
            try
            {
                pivotGridControl.DataSource = DataAccessProvider.GetReport(reportOptions);
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        public void Print()
        {
            pivotGridControl.ShowPrintPreview();
        }

        public void Export(string fName, ExportToType type)
        {
            switch (type)
            {
                case ExportToType.Pdf:
                    pivotGridControl.ExportToPdf(fName);
                    break;
                case ExportToType.Xls:
                    pivotGridControl.ExportToXls(fName);
                    break;
                case ExportToType.Xlsx:
                    pivotGridControl.ExportToXlsx(fName);
                    break;
            }
        }

        private void pivotGridControl_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            PivotDrillDownDataSource source = e.CreateDrillDownDataSource();
            if (e.DataField == pgfOLCount)
            {
                e.CustomValue = funcs[pgfOLCount](source);
            }
            if (e.DataField == pgfOLWithCoord)
            {
                e.CustomValue = funcs[pgfOLWithCoord](source);
            }
            if (e.DataField == pgfOLWithCoordPerc)
            {
                var olCount = funcs[pgfOLCount](source);
                e.CustomValue = olCount == 0 ? 0 : funcs[pgfOLWithCoord](source) * 1.0 / olCount;
            }
            if (e.DataField == pgfTimeInOL)
            {
                var date = DateTime.Now.Date;
                var val = date.AddSeconds(funcs[pgfTimeInOL](source));
                e.CustomValue = val.ToString((val - date).TotalHours < 1 ? "mm:ss" : "H:mm:ss");
            }
            if (e.DataField == pgfVisitWithCoordPerc)
            {
                var visit = funcs[pgfVisit](source);
                e.CustomValue = visit == 0 ? 0 : funcs[pgfVisitWithCoord](source) * 1.0 / visit;
            }
            if (e.DataField == pgfVisitInOLPerc)
            {
                var visit = funcs[pgfVisit](source);
                e.CustomValue = visit == 0 ? 0 : funcs[pgfVisitInOL](source) * 1.0 / visit;
            }
            if (e.DataField == pgfVisit2InOLPerc)
            {
                var visit2 = funcs[pgfVisit2](source);
                e.CustomValue = visit2 == 0 ? 0 : funcs[pgfVisit2InOL](source) * 1.0 / visit2;
            }
        }

        private void pivotGridControl_CustomCellValue(object sender, PivotCellValueEventArgs e)
        {
            /* 
             * Этот кусок ... был сделан для требования "ABIDEV-1614 M1 GPS Tracking ABIDEV-1610 GPS: Cube: До розрізу "Начало визита" некоректно прив'язані показники "Кво ТТ", "Визит2", "Кол ТТ с коорд", "Кол ТТ с коорд %"" 
             * Суть его в том что некоторые поля не должны зависеть от измерения "Начало визита".
             */
            var control = sender as PivotGridControl;
            if (control == null)
                return;

            var rowFields = control.GetFieldsByArea(PivotArea.RowArea);
            var colFields = control.GetFieldsByArea(PivotArea.ColumnArea);
            if ((colFields.Contains(pgfTimeDimension) || rowFields.Contains(pgfTimeDimension))
                && (e.DataField == pgfOLCount || e.DataField == pgfVisit2 || e.DataField == pgfOLWithCoord || e.DataField == pgfOLWithCoordPerc)
                && control.DataSource is DataTable)
            {
                var dateTable = (DataTable)control.DataSource;
                var dataRows = dateTable.Rows.Cast<DataRow>();
                var colrowFields = colFields.Union(rowFields);
                foreach (var field in colrowFields)
                {
                    var columnName = field.FieldName;
                    var fieldValue = e.GetFieldValue(field);
                    if (fieldValue == null || field == pgfTimeDimension)
                        continue;
                    dataRows = dataRows.Where(x => object.Equals(x[columnName], fieldValue));
                }
                for (int i = 0; i < control.Fields.Count; i++)
                {
                    var field = control.Fields[i];
                    if (!field.FilterValues.HasFilter)
                        continue;
                    var columnName = field.FieldName;
                    dataRows = dataRows.Where(x => field.FilterValues.Contains(x[columnName]));
                }
                
                dataRows = dataRows.ToArray();
                if (e.DataField == pgfOLCount)
                {
                    e.Value = dsFuncs[pgfOLCount](dataRows);
                }
                else if (e.DataField == pgfVisit2)
                {
                    e.Value = dsFuncs[pgfVisit2](dataRows);
                }
                else if (e.DataField == pgfOLWithCoord)
                {
                    e.Value = dsFuncs[pgfOLWithCoord](dataRows);
                }
                else if (e.DataField == pgfOLWithCoordPerc)
                {
                    var olCount = dsFuncs[pgfOLCount](dataRows);
                    e.Value = olCount == 0 ? 0 : dsFuncs[pgfOLWithCoord](dataRows) * 1.0 / olCount;
                }

            }

        }
    }
}
