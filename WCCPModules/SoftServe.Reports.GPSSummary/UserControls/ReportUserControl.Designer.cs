﻿namespace SoftServe.Reports.GPSSummary.UserControls
{
    partial class ReportUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pgfYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfMerchName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfSupervisorName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfChanelType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfOLId = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfOutLet = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfDevModelName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfTimeDimension = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfOLCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisit = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisit2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfOLWithCoord = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfOLWithCoordPerc = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisitWithCoord = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisitWithCoordPerc = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisitInOL = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisitInOLPerc = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisit2InOL = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfVisit2InOLPerc = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfDistance = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfTimeInOL = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfTimeInOLForAvg = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfPlanedOL = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pgfYear,
            this.pgfDate,
            this.pgfMerchName,
            this.pgfSupervisorName,
            this.pgfChanelType,
            this.pgfOLId,
            this.pgfOutLet,
            this.pgfDevModelName,
            this.pgfTimeDimension,
            this.pgfOLCount,
            this.pgfVisit,
            this.pgfVisit2,
            this.pgfOLWithCoord,
            this.pgfOLWithCoordPerc,
            this.pgfVisitWithCoord,
            this.pgfVisitWithCoordPerc,
            this.pgfVisitInOL,
            this.pgfVisitInOLPerc,
            this.pgfVisit2InOL,
            this.pgfVisit2InOLPerc,
            this.pgfDistance,
            this.pgfTimeInOL,
            this.pgfTimeInOLForAvg,
            this.pgfPlanedOL});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pivotGridControl.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridControl.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridControl.Size = new System.Drawing.Size(772, 417);
            this.pivotGridControl.TabIndex = 0;
            this.pivotGridControl.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridControl_CustomSummary);
            this.pivotGridControl.CustomCellValue += new System.EventHandler<DevExpress.XtraPivotGrid.PivotCellValueEventArgs>(this.pivotGridControl_CustomCellValue);
            // 
            // pgfYear
            // 
            this.pgfYear.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfYear.AreaIndex = 0;
            this.pgfYear.Caption = "Год";
            this.pgfYear.FieldName = "Year";
            this.pgfYear.Name = "pgfYear";
            // 
            // pgfDate
            // 
            this.pgfDate.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfDate.AreaIndex = 1;
            this.pgfDate.Caption = "Дата";
            this.pgfDate.FieldName = "Date";
            this.pgfDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Date;
            this.pgfDate.Name = "pgfDate";
            this.pgfDate.UnboundFieldName = "pgfDate";
            // 
            // pgfMerchName
            // 
            this.pgfMerchName.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfMerchName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pgfMerchName.AreaIndex = 1;
            this.pgfMerchName.Caption = "М1";
            this.pgfMerchName.FieldName = "MerchName";
            this.pgfMerchName.Name = "pgfMerchName";
            // 
            // pgfSupervisorName
            // 
            this.pgfSupervisorName.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfSupervisorName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pgfSupervisorName.AreaIndex = 0;
            this.pgfSupervisorName.Caption = "М2";
            this.pgfSupervisorName.FieldName = "Supervisor_name";
            this.pgfSupervisorName.Name = "pgfSupervisorName";
            // 
            // pgfChanelType
            // 
            this.pgfChanelType.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfChanelType.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pgfChanelType.AreaIndex = 2;
            this.pgfChanelType.Caption = "Канал персонала";
            this.pgfChanelType.FieldName = "ChanelType";
            this.pgfChanelType.Name = "pgfChanelType";
            // 
            // pgfOLId
            // 
            this.pgfOLId.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfOLId.AreaIndex = 2;
            this.pgfOLId.Caption = "Код ТТ";
            this.pgfOLId.FieldName = "OL_id";
            this.pgfOLId.Name = "pgfOLId";
            // 
            // pgfOutLet
            // 
            this.pgfOutLet.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfOutLet.AreaIndex = 3;
            this.pgfOutLet.Caption = "ТТ";
            this.pgfOutLet.FieldName = "OutLet";
            this.pgfOutLet.Name = "pgfOutLet";
            // 
            // pgfDevModelName
            // 
            this.pgfDevModelName.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfDevModelName.AreaIndex = 4;
            this.pgfDevModelName.Caption = "Модель КПК";
            this.pgfDevModelName.FieldName = "DevModelName";
            this.pgfDevModelName.Name = "pgfDevModelName";
            // 
            // pgfTimeDimension
            // 
            this.pgfTimeDimension.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pgfTimeDimension.AreaIndex = 5;
            this.pgfTimeDimension.Caption = "Начало визита";
            this.pgfTimeDimension.FieldName = "TimeDimension";
            this.pgfTimeDimension.Name = "pgfTimeDimension";
            // 
            // pgfOLCount
            // 
            this.pgfOLCount.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfOLCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfOLCount.AreaIndex = 0;
            this.pgfOLCount.Caption = "Кво ТТ";
            this.pgfOLCount.FieldName = "OL_id";
            this.pgfOLCount.Name = "pgfOLCount";
            this.pgfOLCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pgfVisit
            // 
            this.pgfVisit.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisit.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisit.AreaIndex = 1;
            this.pgfVisit.Caption = "Визиты";
            this.pgfVisit.FieldName = "Visit";
            this.pgfVisit.Name = "pgfVisit";
            // 
            // pgfVisit2
            // 
            this.pgfVisit2.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisit2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisit2.AreaIndex = 2;
            this.pgfVisit2.Caption = "Визит 2";
            this.pgfVisit2.FieldName = "Visit2";
            this.pgfVisit2.Name = "pgfVisit2";
            // 
            // pgfOLWithCoord
            // 
            this.pgfOLWithCoord.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfOLWithCoord.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfOLWithCoord.AreaIndex = 3;
            this.pgfOLWithCoord.Caption = "Кол ТТ с коорд";
            this.pgfOLWithCoord.CellFormat.FormatString = "N0";
            this.pgfOLWithCoord.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfOLWithCoord.FieldName = "OLWithCoord";
            this.pgfOLWithCoord.Name = "pgfOLWithCoord";
            this.pgfOLWithCoord.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pgfOLWithCoordPerc
            // 
            this.pgfOLWithCoordPerc.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfOLWithCoordPerc.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfOLWithCoordPerc.AreaIndex = 4;
            this.pgfOLWithCoordPerc.Caption = "Кол ТТ с коорд %";
            this.pgfOLWithCoordPerc.CellFormat.FormatString = "P0";
            this.pgfOLWithCoordPerc.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfOLWithCoordPerc.FieldName = "OLWithCoord";
            this.pgfOLWithCoordPerc.Name = "pgfOLWithCoordPerc";
            this.pgfOLWithCoordPerc.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pgfVisitWithCoord
            // 
            this.pgfVisitWithCoord.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisitWithCoord.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisitWithCoord.AreaIndex = 5;
            this.pgfVisitWithCoord.Caption = "Визиты с коорд";
            this.pgfVisitWithCoord.FieldName = "VisitWithCoord";
            this.pgfVisitWithCoord.Name = "pgfVisitWithCoord";
            // 
            // pgfVisitWithCoordPerc
            // 
            this.pgfVisitWithCoordPerc.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisitWithCoordPerc.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisitWithCoordPerc.AreaIndex = 6;
            this.pgfVisitWithCoordPerc.Caption = "Визиты с коорд %";
            this.pgfVisitWithCoordPerc.CellFormat.FormatString = "P0";
            this.pgfVisitWithCoordPerc.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfVisitWithCoordPerc.FieldName = "VisitWithCoord";
            this.pgfVisitWithCoordPerc.Name = "pgfVisitWithCoordPerc";
            this.pgfVisitWithCoordPerc.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pgfVisitInOL
            // 
            this.pgfVisitInOL.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisitInOL.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisitInOL.AreaIndex = 7;
            this.pgfVisitInOL.Caption = "Визит в ТТ";
            this.pgfVisitInOL.FieldName = "VisitInOL";
            this.pgfVisitInOL.Name = "pgfVisitInOL";
            // 
            // pgfVisitInOLPerc
            // 
            this.pgfVisitInOLPerc.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisitInOLPerc.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisitInOLPerc.AreaIndex = 8;
            this.pgfVisitInOLPerc.Caption = "Визит в ТТ %";
            this.pgfVisitInOLPerc.CellFormat.FormatString = "P0";
            this.pgfVisitInOLPerc.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfVisitInOLPerc.FieldName = "VisitInOL";
            this.pgfVisitInOLPerc.Name = "pgfVisitInOLPerc";
            this.pgfVisitInOLPerc.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pgfVisit2InOL
            // 
            this.pgfVisit2InOL.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisit2InOL.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisit2InOL.AreaIndex = 9;
            this.pgfVisit2InOL.Caption = "Визит 2 в ТТ";
            this.pgfVisit2InOL.FieldName = "Visit2InOL";
            this.pgfVisit2InOL.Name = "pgfVisit2InOL";
            // 
            // pgfVisit2InOLPerc
            // 
            this.pgfVisit2InOLPerc.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfVisit2InOLPerc.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfVisit2InOLPerc.AreaIndex = 10;
            this.pgfVisit2InOLPerc.Caption = "Визит 2 в ТТ %";
            this.pgfVisit2InOLPerc.CellFormat.FormatString = "P0";
            this.pgfVisit2InOLPerc.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfVisit2InOLPerc.FieldName = "Visit2InOL";
            this.pgfVisit2InOLPerc.Name = "pgfVisit2InOLPerc";
            this.pgfVisit2InOLPerc.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pgfDistance
            // 
            this.pgfDistance.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfDistance.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfDistance.AreaIndex = 11;
            this.pgfDistance.Caption = "Расстояние";
            this.pgfDistance.CellFormat.FormatString = "N0";
            this.pgfDistance.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfDistance.FieldName = "Distance";
            this.pgfDistance.Name = "pgfDistance";
            this.pgfDistance.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            // 
            // pgfTimeInOL
            // 
            this.pgfTimeInOL.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pgfTimeInOL.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfTimeInOL.AreaIndex = 12;
            this.pgfTimeInOL.Caption = "Время в ТТ";
            this.pgfTimeInOL.FieldName = "TimeInOL";
            this.pgfTimeInOL.Name = "pgfTimeInOL";
            this.pgfTimeInOL.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pgfTimeInOLForAvg
            // 
            this.pgfTimeInOLForAvg.AreaIndex = 17;
            this.pgfTimeInOLForAvg.FieldName = "TimeInOLForAvg";
            this.pgfTimeInOLForAvg.Name = "pgfTimeInOLForAvg";
            this.pgfTimeInOLForAvg.Visible = false;
            // 
            // pgfPlanedOL
            // 
            this.pgfPlanedOL.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pgfPlanedOL.AreaIndex = 0;
            this.pgfPlanedOL.FieldName = "PlanedOL";
            this.pgfPlanedOL.Name = "pgfPlanedOL";
            this.pgfPlanedOL.Visible = false;
            // 
            // ReportUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pivotGridControl);
            this.Name = "ReportUserControl";
            this.Size = new System.Drawing.Size(772, 417);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pgfYear;
        private DevExpress.XtraPivotGrid.PivotGridField pgfDate;
        private DevExpress.XtraPivotGrid.PivotGridField pgfMerchName;
        private DevExpress.XtraPivotGrid.PivotGridField pgfSupervisorName;
        private DevExpress.XtraPivotGrid.PivotGridField pgfChanelType;
        private DevExpress.XtraPivotGrid.PivotGridField pgfOLId;
        private DevExpress.XtraPivotGrid.PivotGridField pgfOutLet;
        private DevExpress.XtraPivotGrid.PivotGridField pgfDevModelName;
        private DevExpress.XtraPivotGrid.PivotGridField pgfTimeDimension;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisit;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisit2;
        private DevExpress.XtraPivotGrid.PivotGridField pgfOLWithCoord;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisitWithCoord;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisitInOL;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisit2InOL;
        private DevExpress.XtraPivotGrid.PivotGridField pgfDistance;
        private DevExpress.XtraPivotGrid.PivotGridField pgfTimeInOL;
        private DevExpress.XtraPivotGrid.PivotGridField pgfTimeInOLForAvg;
        private DevExpress.XtraPivotGrid.PivotGridField pgfOLCount;
        private DevExpress.XtraPivotGrid.PivotGridField pgfOLWithCoordPerc;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisitWithCoordPerc;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisitInOLPerc;
        private DevExpress.XtraPivotGrid.PivotGridField pgfVisit2InOLPerc;
        private DevExpress.XtraPivotGrid.PivotGridField pgfPlanedOL;
    }
}
