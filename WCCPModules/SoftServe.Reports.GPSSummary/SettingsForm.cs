#region

using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.GPSSummary.DataProvider;
using DevExpress.XtraEditors.Controls;

#endregion

namespace SoftServe.Reports.GPSSummary
{
    public partial class SettingsForm : XtraForm
    {
        #region Fields

        private bool disableCheckedItemsUpdate;
        private bool disableChkAllUpdate;
        private bool isLoaded;

        #endregion

        #region Constructors

        public SettingsForm()
        {
            InitializeComponent();
            btnCancel.Image = CommonResource.Close_16;
            btnSubmit.Image = CommonResource.Check_16;
            Options = null;
        }

        #endregion

        #region Instance Properties

        public ReportOptions Options { get; private set; }

        #endregion

        #region Instance Methods

        private void UpdateFormState()
        {
            btnSubmit.Enabled = clbM1.CheckedItems.Count > 0
                                && clbM2.CheckedItems.Count > 0
                                && !string.IsNullOrEmpty(deFrom.Text)
                                && !string.IsNullOrEmpty(deTo.Text)
                                && deFrom.DateTime.Date <= deTo.DateTime.Date;
        }

        #endregion

        #region Event Handling

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            if (!isLoaded)
            {
                Text = String.Format("��������� �������� ������ GPS", Constants.ReportName);
                deFrom.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                deTo.DateTime = DateTime.Now.Date;
                clbM2.DataSource = DataAccessProvider.GetM2();
                var selectedM2 = new int[] {};
                clbM1.DataSource = DataAccessProvider.GetM1(selectedM2);
            }
            UpdateFormState();
//#if DEBUG
//            if (!isLoaded)
//            {
//                deFrom.DateTime = new DateTime(2011, 07, 01);
//                deTo.DateTime = new DateTime(2011, 07, 21);
//                clbM2.SelectedIndex = 1;
//                clbM2.CheckSelectedItems();
//                chkSelectAllM1.Checked = true;
//                //btn_Click(btnSubmit, new EventArgs());
//            }
//#endif
            isLoaded = true;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            if (sender.Equals(btnSubmit))
            {
                var m2CheckedItems = clbM2.CheckedItems.Cast<DataRowView>().ToArray();
                var m1CheckedItems = clbM1.CheckedItems.Cast<DataRowView>().ToArray();
                Options = new ReportOptions
                              {
                                  PeriodStart = deFrom.DateTime.Date,
                                  PeriodEnd = deTo.DateTime.Date,
                                  M2List =
                                      m2CheckedItems.Select(row => ConvertEx.ToInt(row[clbM2.ValueMember])).ToArray(),
                                  M2NamesList =
                                      m2CheckedItems.Select(row => row[clbM2.ValueMember].ToString()).ToArray(),
                                  M1List =
                                      m1CheckedItems.Select(row => ConvertEx.ToInt(row[clbM1.ValueMember])).ToArray(),
                                  M1NamesList =
                                      m1CheckedItems.Select(row => row[clbM1.ValueMember].ToString()).ToArray()
                              };

                DialogResult = DialogResult.OK;
            }
            else if (sender.Equals(btnCancel))
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void chkSelectAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (disableChkAllUpdate)
                return;

            CheckEdit chkSelectAll = null;
            CheckedListBoxControl checkedListBoxControl = null;

            if (sender.Equals(chkSelectAllM1))
            {
                chkSelectAll = chkSelectAllM1;
                checkedListBoxControl = clbM1;
            }
            else if (sender.Equals(chkSelectAllM2))
            {
                chkSelectAll = chkSelectAllM2;
                checkedListBoxControl = clbM2;
            }

            if (chkSelectAll == null || checkedListBoxControl == null)
                return;

            if (chkSelectAll.CheckState == CheckState.Indeterminate)
            {
                chkSelectAll.CheckState = CheckState.Unchecked;
                return;
            }

            chkSelectAll.Properties.AllowGrayed = false;
            disableCheckedItemsUpdate = true;
            switch (chkSelectAll.CheckState)
            {
                case CheckState.Checked:
                    checkedListBoxControl.CheckAll();
                    break;
                case CheckState.Unchecked:
                    checkedListBoxControl.UnCheckAll();
                    break;
            }
            disableCheckedItemsUpdate = false;
            clb_ItemCheck(checkedListBoxControl, null);
        }

        private void clb_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (disableCheckedItemsUpdate)
                return;

            CheckEdit chkSelectAll = null;
            CheckedListBoxControl checkedListBoxControl = null;

            if (sender.Equals(clbM1))
            {
                chkSelectAll = chkSelectAllM1;
                checkedListBoxControl = clbM1;
            }
            else if (sender.Equals(clbM2))
            {
                WaitManager.StartWait();
                try
                {
                    var selectedM2 =
                        clbM2.CheckedItems.Cast<DataRowView>().Select(
                            rowView => ConvertEx.ToInt(rowView[clbM2.ValueMember]));
                    
                    chkSelectAllM1.CheckState = CheckState.Unchecked;
                    clbM1.DataSource = DataAccessProvider.GetM1(selectedM2);
                    chkSelectAllM1.CheckState = CheckState.Checked;
                    
                    clb_ItemCheck(clbM1, null);
                }
                finally
                {
                    WaitManager.StopWait();
                }
                chkSelectAll = chkSelectAllM2;
                checkedListBoxControl = clbM2;
            }

            if (chkSelectAll == null || checkedListBoxControl == null)
                return;

            var itemsCount = (checkedListBoxControl.DataSource == null ||
                              !(checkedListBoxControl.DataSource is DataTable)
                                  ? 0
                                  : ((DataTable) checkedListBoxControl.DataSource).Rows.Count);
            disableChkAllUpdate = true;
            if (checkedListBoxControl.CheckedItems.Count == 0)
            {
                chkSelectAll.CheckState = CheckState.Unchecked;
            }
            else if (checkedListBoxControl.CheckedItems.Count == itemsCount)
            {
                chkSelectAll.CheckState = CheckState.Checked;
            }
            else
            {
                chkSelectAll.Properties.AllowGrayed = true;
                chkSelectAll.CheckState = CheckState.Indeterminate;
            }
            disableChkAllUpdate = false;

            UpdateFormState();
        }

        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (sender.Equals(deFrom))
            {
                deTo.Properties.MinValue = !string.IsNullOrEmpty(deFrom.Text) ? deFrom.DateTime.Date : DateTime.MinValue;
            }
            if (sender.Equals(deTo))
            {
                deFrom.Properties.MaxValue = !string.IsNullOrEmpty(deTo.Text) ? deTo.DateTime.Date : DateTime.MaxValue;
            }
            UpdateFormState();
        }

        #endregion
    }
}