﻿#region

using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.GPSSummary;
using SoftServe.Reports.GPSSummary.DataProvider;
using SoftServe.Reports.GPSSummary.UserControls;

#endregion

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        #region Constructors

        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            var control = new ReportUserControl
                              {
                                  Dock = DockStyle.Fill
                              };

            var page = tabManager.TabPages.Add(Constants.ReportName);
            page.Controls.Add(control);
            tabManager.ShowTabHeader = DefaultBoolean.False;
            control.LoadData(SettingsForm.Options);
            
            /* Disable unused buttons */
            btnPrintAll.Visibility = BarItemVisibility.Never;
            btnExportAllTo.Visibility = BarItemVisibility.Never;

            var link = bar1.LinksPersistInfo.Cast<LinkPersistInfo>().FirstOrDefault(x=>x.Item == btnExportTo);
            if (link!=null)
            {
                link.BeginGroup = true;
                barMenuManager.ForceInitialize();
            }

            btnRefresh.ItemClick += (s, e) => control.LoadData();
            btnSettings.ItemClick +=
                (s, e) => { if (SettingsForm.ShowDialog() == DialogResult.OK) control.LoadData(SettingsForm.Options); };
            PrintClick += (s, t) => control.Print();
            ExportClick += (s, t, type, path)=> control.Export(path, type);
        }

        #endregion

        #region Class Properties

        public static SettingsForm SettingsForm { get; set; }

        #endregion
    }
}