namespace SoftServe.Reports.GPSSummary
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.deFrom = new DevExpress.XtraEditors.DateEdit();
            this.deTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.clbM2 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.clbM1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSubmit = new DevExpress.XtraEditors.SimpleButton();
            this.chkSelectAllM2 = new DevExpress.XtraEditors.CheckEdit();
            this.chkSelectAllM1 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.deFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAllM2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAllM1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(8, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(204, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "������ ������� ��� ������� ������*:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(8, 30);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(5, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "�";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(125, 30);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(12, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "��";
            // 
            // deFrom
            // 
            this.deFrom.EditValue = null;
            this.deFrom.Location = new System.Drawing.Point(19, 27);
            this.deFrom.Name = "deFrom";
            this.deFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFrom.Size = new System.Drawing.Size(100, 20);
            this.deFrom.TabIndex = 3;
            this.deFrom.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // deTo
            // 
            this.deTo.EditValue = null;
            this.deTo.Location = new System.Drawing.Point(143, 27);
            this.deTo.Name = "deTo";
            this.deTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deTo.Size = new System.Drawing.Size(100, 20);
            this.deTo.TabIndex = 4;
            this.deTo.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(8, 53);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(77, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "�������� �2*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(166, 53);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(77, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "�������� �1*:";
            // 
            // clbM2
            // 
            this.clbM2.CheckOnClick = true;
            this.clbM2.DisplayMember = "Supervisor_name";
            this.clbM2.Location = new System.Drawing.Point(8, 72);
            this.clbM2.Name = "clbM2";
            this.clbM2.Size = new System.Drawing.Size(152, 250);
            this.clbM2.TabIndex = 7;
            this.clbM2.ValueMember = "Supervisor_id";
            this.clbM2.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.clb_ItemCheck);
            // 
            // clbM1
            // 
            this.clbM1.CheckOnClick = true;
            this.clbM1.DisplayMember = "MerchName";
            this.clbM1.Location = new System.Drawing.Point(166, 72);
            this.clbM1.Name = "clbM1";
            this.clbM1.Size = new System.Drawing.Size(168, 250);
            this.clbM1.TabIndex = 8;
            this.clbM1.ValueMember = "Merch_id";
            this.clbM1.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.clb_ItemCheck);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(254, 353);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "��������";
            this.btnCancel.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(104, 353);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(144, 23);
            this.btnSubmit.TabIndex = 10;
            this.btnSubmit.Text = "������������� �����";
            this.btnSubmit.Click += new System.EventHandler(this.btn_Click);
            // 
            // chkSelectAllM2
            // 
            this.chkSelectAllM2.Location = new System.Drawing.Point(6, 328);
            this.chkSelectAllM2.Name = "chkSelectAllM2";
            this.chkSelectAllM2.Properties.Caption = "������� ���� �2";
            this.chkSelectAllM2.Size = new System.Drawing.Size(154, 19);
            this.chkSelectAllM2.TabIndex = 11;
            this.chkSelectAllM2.CheckStateChanged += new System.EventHandler(this.chkSelectAll_CheckStateChanged);
            // 
            // chkSelectAllM1
            // 
            this.chkSelectAllM1.Location = new System.Drawing.Point(166, 328);
            this.chkSelectAllM1.Name = "chkSelectAllM1";
            this.chkSelectAllM1.Properties.Caption = "������� ���� �1";
            this.chkSelectAllM1.Size = new System.Drawing.Size(168, 19);
            this.chkSelectAllM1.TabIndex = 12;
            this.chkSelectAllM1.CheckStateChanged += new System.EventHandler(this.chkSelectAll_CheckStateChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 384);
            this.Controls.Add(this.chkSelectAllM1);
            this.Controls.Add(this.chkSelectAllM2);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.clbM1);
            this.Controls.Add(this.clbM2);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.deTo);
            this.Controls.Add(this.deFrom);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������� ������ GPS";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAllM2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAllM1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit deFrom;
        private DevExpress.XtraEditors.DateEdit deTo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckedListBoxControl clbM2;
        private DevExpress.XtraEditors.CheckedListBoxControl clbM1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSubmit;
        private DevExpress.XtraEditors.CheckEdit chkSelectAllM2;
        private DevExpress.XtraEditors.CheckEdit chkSelectAllM1;
        
    }
}