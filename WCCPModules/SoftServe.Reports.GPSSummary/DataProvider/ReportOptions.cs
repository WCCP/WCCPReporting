﻿using System;

namespace SoftServe.Reports.GPSSummary.DataProvider
{
    public class ReportOptions
    {
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        
        public int[] M1List { get; set; }
        public string[] M1NamesList { get; set; }

        public int[] M2List { get; set; }
        public string[] M2NamesList { get; set; }
    }
}