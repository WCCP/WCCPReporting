﻿using Logica.Reports.Common;

namespace SoftServe.Reports.GPSSummary.DataProvider
{
    public static class Constants
    {
        /// <summary>
        /// Name of report which configured in WCCP reports tree
        /// </summary>
        public static string ReportName { get; set; }
    }
}