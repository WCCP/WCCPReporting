﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.BaseReportControl.Helpers;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.GPSSummary.DataProvider
{
    public static class DataAccessProvider
    {
        private const string SP_M2_LIST = "GPS.spGPS_M2Get";
        private const string SP_M1_LIST = "GPS.spGPS_M1Get";
        private const string SP_M1_LIST_PRM_1 = "M2List";
        private const string SP_REPORT = "GPS.spGPSGet";
        private const string SP_REPORT_PRM_1 = "StartDate";
        private const string SP_REPORT_PRM_2 = "EndDate";
        private const string SP_REPORT_PRM_3 = "M1List";

        public static DataTable GetM2()
        {
            var dataSet = DataAccessLayer.ExecuteStoredProcedure(SP_M2_LIST);
            if (dataSet.Tables.Count > 0)
            {
                return dataSet.Tables[0];
            }
            return null;
        }

        public static DataTable GetM1(IEnumerable<int> selectedM2)
        {
            var dataSet = DataAccessLayer.ExecuteStoredProcedure(SP_M1_LIST,
                                                                 new[]
                                                                     {
                                                                         new SqlParameter(SP_M1_LIST_PRM_1, selectedM2.ToString(","))
                                                                     });
            if (dataSet.Tables.Count > 0)
            {
                return dataSet.Tables[0];
            }
            return null;
        }

        public static DataTable GetReport(ReportOptions options)
        {
            var dataSet = DataAccessLayer.ExecuteStoredProcedure(SP_REPORT,
                                                                 new[]
                                                                     {
                                                                         new SqlParameter(SP_REPORT_PRM_1, options.PeriodStart),
                                                                         new SqlParameter(SP_REPORT_PRM_2, options.PeriodEnd),
                                                                         new SqlParameter(SP_REPORT_PRM_3, options.M1List.ToString(","))
                                                                     });        
            if (dataSet.Tables.Count > 0)
            {
                return dataSet.Tables[0];
            }
            return null;
        }
    }
}