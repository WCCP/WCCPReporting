﻿using System;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.ContractMgmtClients;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraTab;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        private ClientListTab tabClientList;

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();

            tabClientList = new ClientListTab(this);

            TabControl.ClosePageButtonShowMode = ClosePageButtonShowMode.InAllTabPageHeaders;

            UpdateAllSheets();
        }

        /// <summary>
        /// Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            SheetParamCollection parameterCollection = new SheetParamCollection { TabId = tabClientList.TabId, TableDataType = TableType.Fact };
            tabClientList.UpdateSheet(parameterCollection);
        }

        ///// <summary>
        ///// Handles the ThreadException event of the Application control.
        ///// </summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Threading.ThreadExceptionEventArgs"/> instance containing the event data.</param>
        //private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        //{
        //    //ExceptionHandler.HandleException(e.Exception);
        //}
    }
}
