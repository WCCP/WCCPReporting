﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SoftServe.Reports.ContractMgmtClients
{
    public partial class OlErrorForm : Form
    {
        private enum OlErrorType
        {
            OlEngaged = -1
        }

        public OlErrorForm()
        {
            InitializeComponent();
        }

        public OlErrorForm(DataTable table)
        {
            InitializeComponent();

            foreach (DataRow row in table.Rows)
            {
                switch ((OlErrorType)row["ERROR_CODE"])
                {
                    case OlErrorType.OlEngaged:
                        row["ERROR_COMMENT"] = String.Format(Resource.SaveAddressesErrorOlEngaged, row["ERROR_COMMENT"]);
                        break;
                    default:
                        break;
                }
            }

            gridControl.DataSource = table;
        }
    }
}
