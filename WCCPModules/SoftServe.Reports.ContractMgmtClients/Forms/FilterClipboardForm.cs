﻿using System;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FilterClipboardForm : FilterBaseForm
    {
        private string pocList;
        private const char separator = ',';

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterClipboardForm"/> class.
        /// </summary>
        public FilterClipboardForm()
            : base(FilterFormType.ThomasResearch)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Load event of the FilterClipboardForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FilterClipboardForm_Load(object sender, EventArgs e)
        {
            ProcessClipboard();
        }

        /// <summary>
        /// Updates the buttons.
        /// </summary>
        private void UpdateButtons()
        {
            btnOk.Enabled = gridView.SelectedRowsCount > 0;
        }

        /// <summary>
        /// Gets the poc id list.
        /// </summary>
        /// <value>The poc id list.</value>
        public override string POCList
        {
            set
            {
                pocList = value;
                LoadPreviewList();
            }
            get
            {
                return pocList;
            }
        }

        /// <summary>
        /// Processes the clipboard.
        /// </summary>
        private void ProcessClipboard()
        {
            string text = Clipboard.GetText(TextDataFormat.Text);
            string result = string.Empty;
           
            if (ValidateChildren(ValidationConstraints.Selectable) && !string.IsNullOrEmpty(text))
            {
                text = text.Trim();
                Regex regex = new Regex(@"\d+");

                foreach (String id in (from m in regex.Matches(text).Cast<Match>() select m.Value).Distinct()) 
                {
                    if(! String.IsNullOrEmpty(result))
                    {
                        result += ",";
                    }
                    result += id;
                }

                POCList = result;
            }

            UpdateButtons();
        }

        /// <summary>
        /// Processes the error POCs.
        /// </summary>
        private void ProcessErrorPOCs()
        {
            string[] strListPOCs = POCList.Split(new char[] { separator });
            if (strListPOCs.Length > 0)
            {
                Collection<Int64> listPOCs = new Collection<Int64>();

                foreach (string strPocId in strListPOCs)
                {
                    try
                    {
                        listPOCs.Add(Int64.Parse(strPocId));
                    }
                    catch { }
                }

                foreach (Int64 pocId in listPOCs)
                {
                    if (!POCIdExists(pocId))
                    {
                        memoClipboard.Text += string.Format("{0}\t{1}\r\n", pocId, Resource.POCIsNotParsed);
                    }
                }
            }

        }

        /// <summary>
        /// POCs the id exists.
        /// </summary>
        /// <param name="pocId">The poc id.</param>
        /// <returns></returns>
        private bool POCIdExists(Int64 pocId)
        {
            DataTable dataTable = gridControl.DataSource as DataTable;
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.RowFilter = string.Format("{0} = {1}", columnOLId.FieldName, pocId);
                return dataView.ToTable().Rows.Count > 0;
            }
            return false;
        }


        /// <summary>
        /// Loads the preview list.
        /// </summary>
        private void LoadPreviewList()
        {
            if (IsValidaData(POCList))
            {
                gridControl.DataSource = DataProvider.GetAddressesListByOLIDs(POCList);
                ProcessErrorPOCs();
            }
            else
            {
                gridControl.DataSource = null;
                memoClipboard.Text = Resource.ClipboardParseError;
            }
        }

        /// <summary>
        /// Determines whether [is valida data] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>
        /// 	<c>true</c> if [is valida data] [the specified text]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsValidaData(string text)
        {
            if(!string.IsNullOrEmpty(text))
            {
                char[] chars = text.ToCharArray();
                foreach(char ch in chars)
                {
                    if (!(char.IsDigit(ch) || ch == separator))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
}
