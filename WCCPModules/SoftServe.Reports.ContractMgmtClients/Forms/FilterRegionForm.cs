﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.Common.WaitWindow;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FilterRegionForm : FilterBaseForm
    {
        /// <summary>
        /// Represents a TerritoryUnit.
        /// </summary>
        private class TerritoryUnit
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TerritoryUnit"/> class.
            /// </summary>
            public TerritoryUnit()
            {

            }
            /// <summary>
            /// Initializes a new instance of the <see cref="TerritoryUnit"/> class.
            /// </summary>
            /// <param name="id">The id.</param>
            /// <param name="name">The name.</param>
            public TerritoryUnit(Int32 id, String name)
            {
                this.Id = id;
                this.Name = name;
            }

            /// <summary>
            /// Gets or sets the id.
            /// </summary>
            /// <value>The id.</value>
            public Int32 Id { get; set; }
            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            /// <value>The name.</value>
            public String Name { get; set; }

            /// <summary>
            /// Implements the operator ==.
            /// </summary>
            /// <param name="tu1">The tu1.</param>
            /// <param name="tu2">The tu2.</param>
            /// <returns>The result of the operator.</returns>
            public static bool operator ==(TerritoryUnit tu1, TerritoryUnit tu2)
            {
                if (Object.ReferenceEquals(tu1, null))
                {
                    return Object.ReferenceEquals(tu2, null);
                }
                else 
                {
                    return tu1.Equals(tu2);
                }
            }
            /// <summary>
            /// Implements the operator !=.
            /// </summary>
            /// <param name="tu1">The tu1.</param>
            /// <param name="tu2">The tu2.</param>
            /// <returns>The result of the operator.</returns>
            public static bool operator !=(TerritoryUnit tu1, TerritoryUnit tu2)
            {
                return !(tu1 == tu2);
            }

            /// <summary>
            /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
            /// </summary>
            /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
            /// <returns>
            /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
            /// </returns>
            /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
            public override bool Equals(object obj)
            {
                if (Object.ReferenceEquals(obj, null) || !(obj is TerritoryUnit)) 
                {
                    return false;
                }

                return this.Id.Equals((obj as TerritoryUnit).Id);
            }
            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            /// </returns>
            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }
        }

        /// <summary>
        /// The territory data.
        /// </summary>
        IEnumerable<DataRow> territoryData;

        /// <summary>
        /// The territory staff.
        /// </summary>
        IEnumerable<DataRow> territoryStaff;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterRegionForm"/> class.
        /// </summary>
        public FilterRegionForm()
            : base(FilterFormType.Region)
        {
            InitializeComponent();

            LoadData();
        }

        /// <summary>
        /// Gets the poc id list.
        /// </summary>
        /// <value>The poc id list.</value>
        public override string POCList
        {
            set
            {
            }
            get
            {
                return olList.GetSelectedTTs();
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        private void LoadData() 
        {
            BuildRegionsTree();
        }
                
        /// <summary>
        /// Handles the AfterCheckNode event of the treeRegions control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.NodeEventArgs"/> instance containing the event data.</param>
        private void treeRegions_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            WaitManager.StartWait();

            try
            {
                if (e.Node.CheckState == CheckState.Checked)
                {
                    //e.Node.Selected
                    CheckChildNodes(e.Node, true);
                }
                else if (e.Node.CheckState == CheckState.Unchecked)
                {
                    CheckChildNodes(e.Node, false);
                    UncheckParentNodes(e.Node);
                }

                BuildStaffTree();
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        private void treeStaffList_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            WaitManager.StartWait();

            try
            {
                if (e.Node.CheckState == CheckState.Checked)
                {
                    //e.Node.Selected
                    CheckChildNodes(e.Node, true);
                }
                else if (e.Node.CheckState == CheckState.Unchecked)
                {
                    CheckChildNodes(e.Node, false);
                    UncheckParentNodes(e.Node);
                }

                BindTable();
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        #region BuildTrees

        /// <summary>
        /// Builds the regions tree.
        /// </summary>
        private void BuildRegionsTree()
        {
            treeRegions.ClearNodes();
            TreeListNode root = treeRegions.AppendNode(new object[] { Resource.WholeCountry }, null);
            root.Tag = new List<object>() { false };
            AddRegions(root);
            root.Expanded = true;
        }

        /// <summary>
        /// Adds the regions.
        /// </summary>
        /// <param name="root">The root.</param>
        private void AddRegions(TreeListNode root)
        {
            territoryData = DataProvider.GetTerritoryData().Rows.Cast<DataRow>();

            if (territoryData == null) return;

            var regions = (from row in territoryData
                           select new TerritoryUnit
                               (
                                    row.Field<int>(Constants.TeritoryDataRegionId),
                                    row.Field<string>(Constants.TeritoryDataRegionName)
                               )).Distinct();

            foreach (TerritoryUnit region in regions)
            {
                TreeListNode node = AddChildNode(region, false, root);
                AddDistricts(region.Id, node);
            }
        }

        /// <summary>
        /// Adds the districts.
        /// </summary>
        /// <param name="regionId">The region id.</param>
        /// <param name="root">The root.</param>
        private void AddDistricts(int regionId, TreeListNode root)
        {
            var districts = (from row in territoryData
                             where row.Field<int>(Constants.TeritoryDataRegionId) == regionId
                             select new TerritoryUnit
                                 (
                                      row.Field<int>(Constants.TeritoryDataDistrictId),
                                      row.Field<string>(Constants.TeritoryDataDistrictName)
                                 )).Distinct();

            foreach (TerritoryUnit district in districts)
            {
                TreeListNode node = AddChildNode(district, false, root);
                AddCities(district.Id, node);
            }
        }

        /// <summary>
        /// Adds the cities.
        /// </summary>
        /// <param name="districtId">The district id.</param>
        /// <param name="root">The root.</param>
        private void AddCities(int districtId, TreeListNode root)
        {
            TreeListNode nodeCities = treeRegions.AppendNode(new object[] { Resource.Cities }, root);
            nodeCities.Tag = new List<object>() { false };

            var cities = (from row in territoryData
                          where row.Field<int>(Constants.TeritoryDataDistrictId) == districtId
                                && row.Field<int>(Constants.TeritoryDataSettlementId) == Constants.TeritoryDataSettlementCity
                          select new TerritoryUnit
                              (
                                   row.Field<int>(Constants.TeritoryDataCityId),
                                   row.Field<string>(Constants.TeritoryDataCityName)
                              )).Distinct();

            foreach (TerritoryUnit city in cities)
            {
                TreeListNode node = AddChildNode(city, true, nodeCities);
            }

            TreeListNode nodeVilages = treeRegions.AppendNode(new object[] { Resource.Villages }, root);
            nodeVilages.Tag = new List<object>() { false };

            var vilages = (from row in territoryData
                           where row.Field<int>(Constants.TeritoryDataDistrictId) == districtId
                                 && row.Field<int>(Constants.TeritoryDataSettlementId) == Constants.TeritoryDataSettlementVillage
                           select new TerritoryUnit
                               (
                                    row.Field<int>(Constants.TeritoryDataCityId),
                                    row.Field<string>(Constants.TeritoryDataCityName)
                               )).Distinct();

            foreach (TerritoryUnit vilage in vilages)
            {
                TreeListNode node = AddChildNode(vilage, true, nodeVilages);
            }
        }

        /// <summary>
        /// Adds the GEO child node.
        /// </summary>
        /// <param name="territory">The territory.</param>
        /// <param name="isBottom">if set to <c>true</c> [is bottom].</param>
        /// <param name="root">The root.</param>
        /// <returns>The TreeListNode.</returns>
        private TreeListNode AddChildNode(TerritoryUnit territory, bool isBottom, TreeListNode root)
        {
            return AddChildNode(territory.Name, territory.Id, isBottom, root);
        }

        /// <summary>
        /// Adds the GEO child node.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The id.</param>
        /// <param name="isBottom">if set to <c>true</c> [is bottom].</param>
        /// <param name="root">The root.</param>
        /// <returns>The TreeListNode.</returns>
        private TreeListNode AddChildNode(string name, object id, bool isBottom, TreeListNode root)
        {
            TreeListNode node = root.TreeList.AppendNode(new object[] { name }, root);
            node.Tag = new List<object>() { isBottom, id };
            return node;
        }

        /// <summary>
        /// Builds the regions tree.
        /// </summary>
        private void BuildStaffTree()
        {
            treeStaffList.ClearNodes();
            string cities = GetSelectedItems(this.treeRegions.Nodes);

            territoryStaff = DataProvider.GetTerritoryStaff(cities).Rows.Cast<DataRow>();
            if (territoryData == null) return;

            var m5s = (from row in territoryStaff
                       select new TerritoryUnit
                           (
                                row.Field<int>(Constants.TeritoryStaffM5Id),
                                row.Field<string>(Constants.TeritoryStaffM5Name)
                           )).Distinct();

            foreach (var m5 in m5s)
            {
                TreeListNode node = treeStaffList.AppendNode(new object[] { m5.Name }, null);
                node.Tag = new List<object> { false };

                AddM4Staff(m5.Id, node);
                node.Expanded = true;
            }
        }

        #region AddStaff

        /// <summary>
        /// Adds the m4 staff.
        /// </summary>
        /// <param name="root">The parent node.</param>
        private void AddM4Staff(int m5Id, TreeListNode parent)
        {
            var m4Staff = (from row in territoryStaff
                           where row.Field<int>(Constants.TeritoryStaffM5Id) == m5Id
                           select new TerritoryUnit
                               (
                                    row.Field<int>(Constants.TeritoryStaffM4Id),
                                    row.Field<string>(Constants.TeritoryStaffM4Name)
                               )).Distinct();

            foreach (TerritoryUnit m4 in m4Staff)
            {
                TreeListNode node = AddChildNode(m4, false, parent);
                AddM3Staff(m4.Id, node);
            }
        }

        /// <summary>
        /// Adds the m3 staff.
        /// </summary>
        /// <param name="root">The parent node.</param>
        private void AddM3Staff(int m4Id, TreeListNode parent)
        {
            var m3Staff = (from row in territoryStaff
                           where row.Field<int>(Constants.TeritoryStaffM4Id) == m4Id
                           select new TerritoryUnit
                               (
                                    row.Field<int>(Constants.TeritoryStaffM3Id),
                                    row.Field<string>(Constants.TeritoryStaffM3Name)
                               )).Distinct();

            foreach (TerritoryUnit m3 in m3Staff)
            {
                TreeListNode node = AddChildNode(m3, false, parent);
                AddM2Staff(m3.Id, node);
            }
        }

        /// <summary>
        /// Adds the m2 staff.
        /// </summary>
        /// <param name="root">The parent node.</param>
        private void AddM2Staff(int m3Id, TreeListNode parent)
        {
            var m2Staff = (from row in territoryStaff
                           where row.Field<int>(Constants.TeritoryStaffM3Id) == m3Id
                           select new TerritoryUnit
                               (
                                    row.Field<int>(Constants.TeritoryStaffM2Id),
                                    row.Field<string>(Constants.TeritoryStaffM2Name)
                               )).Distinct();

            foreach (TerritoryUnit m2 in m2Staff)
            {
                TreeListNode node = AddChildNode(m2, false, parent);
                AddM1Staff(m2.Id, node);
            }
        }

        /// <summary>
        /// Adds the m1 staff.
        /// </summary>
        /// <param name="root">The parent node.</param>
        private void AddM1Staff(int m2Id, TreeListNode parent)
        {
            var m1Staff = (from row in territoryStaff
                           where row.Field<int>(Constants.TeritoryStaffM2Id) == m2Id
                           select new TerritoryUnit
                               (
                                    row.Field<int>(Constants.TeritoryStaffM1Id),
                                    row.Field<string>(Constants.TeritoryStaffM1Name)
                               )).Distinct();

            foreach (TerritoryUnit m1 in m1Staff)
            {
                TreeListNode node = AddChildNode(m1, false, parent);
                AddRoutes(m1.Id, node);
            }
        }

        /// <summary>
        /// Adds the m1 staff.
        /// </summary>
        /// <param name="root">The parent node.</param>
        private void AddRoutes(int m1Id, TreeListNode parent)
        {
            var routes = (from row in territoryStaff
                           where row.Field<int>(Constants.TeritoryStaffM1Id) == m1Id
                           select new
                               {
                                    Id = row.Field<long>(Constants.TeritoryStaffRouteId),
                                    Name = row.Field<string>(Constants.TeritoryStaffRouteName)
                               }).Distinct();

            foreach (var route in routes)
            {
                AddChildNode(route.Name, route.Id, true, parent);
            }
        }

        #endregion

        /// <summary>
        /// Checks the child nodes.
        /// </summary>
        /// <param name="treeListNode">The tree list node.</param>
        /// <param name="checkedState">if set to <c>true</c> [checked state].</param>
        private void CheckChildNodes(TreeListNode treeListNode, bool checkedState)
        {
            if (null != treeListNode && treeListNode.HasChildren)
            {
                foreach (TreeListNode childNode in treeListNode.Nodes)
                {
                    childNode.Checked = checkedState;
                    CheckChildNodes(childNode, checkedState);
                }
            }
        }

        /// <summary>
        /// Unchecks the parent nodes.
        /// </summary>
        /// <param name="treeListNode">The tree list node.</param>
        private void UncheckParentNodes(TreeListNode treeListNode)
        {
            if (null != treeListNode && null != treeListNode.ParentNode)
            {
                treeListNode.ParentNode.Checked = false;
                UncheckParentNodes(treeListNode.ParentNode);
            }
        }

        /// <summary>
        /// Gets the selected cities.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns></returns>
        public string GetSelectedItems(TreeListNodes nodes) 
        {
            string ids = string.Empty;

            foreach (TreeListNode node in nodes) 
            {
                if (node.Checked)
                {
                    ids = ConcatWithComa(ids, GetAllItemsIdsByNode(node));
                }
                else
                {
                    if (node.Nodes != null)
                    {
                        ids = ConcatWithComa(ids, GetSelectedItems(node.Nodes));
                    }
                }
            }

            return ids;
        }

        /// <summary>
        /// Gets all cities ids by node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns></returns>
        private string GetAllItemsIdsByNode(TreeListNode node)
        {
            string ids = string.Empty;

            bool isBottom = (bool)((List<object>)node.Tag)[0];
            if (isBottom)
            {
                ids = ((List<object>)node.Tag)[1].ToString();
                return ids;
            }

            if (node.Nodes != null)
            {
                foreach (TreeListNode child in node.Nodes)
                {
                    ids = ConcatWithComa(ids, GetAllItemsIdsByNode(child));
                }
            }

            return ids;
        }

        /// <summary>
        /// Concats strings with coma.
        /// </summary>
        /// <param name="s1">The s1.</param>
        /// <param name="s2">The s2.</param>
        /// <returns></returns>
        private string ConcatWithComa(string s1, string s2) 
        {
            if (!string.IsNullOrEmpty(s2))
            {
                if (!string.IsNullOrEmpty(s1))
                {
                    s1 += ",";
                }

                s1 += s2;
            }

            return s1;
        }

        #endregion

        private void BindTable()
        {
            string items = GetSelectedItems(this.treeStaffList.Nodes);

            olList.SetData(DataProvider.GetTerritoryOutlets(items));
        }
    }
}
