﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class OlErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnOlId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnErrorComment = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(353, 136);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 7;
            this.OKButton.Text = "OK";
            // 
            // gridControl
            // 
            this.gridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(443, 128);
            this.gridControl.TabIndex = 8;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnOlId,
            this.gridColumnErrorComment});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnOlId
            // 
            this.gridColumnOlId.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOlId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOlId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnOlId.Caption = "Код ТТ";
            this.gridColumnOlId.FieldName = "OL_ID";
            this.gridColumnOlId.Name = "gridColumnOlId";
            this.gridColumnOlId.OptionsColumn.AllowEdit = false;
            this.gridColumnOlId.OptionsColumn.AllowMove = false;
            this.gridColumnOlId.OptionsColumn.ReadOnly = true;
            this.gridColumnOlId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnOlId.Visible = true;
            this.gridColumnOlId.VisibleIndex = 0;
            this.gridColumnOlId.Width = 439;
            // 
            // gridColumnErrorComment
            // 
            this.gridColumnErrorComment.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnErrorComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnErrorComment.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumnErrorComment.Caption = "Примечание";
            this.gridColumnErrorComment.FieldName = "ERROR_COMMENT";
            this.gridColumnErrorComment.Name = "gridColumnErrorComment";
            this.gridColumnErrorComment.OptionsColumn.AllowEdit = false;
            this.gridColumnErrorComment.OptionsColumn.AllowMove = false;
            this.gridColumnErrorComment.OptionsColumn.ReadOnly = true;
            this.gridColumnErrorComment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnErrorComment.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnErrorComment.Visible = true;
            this.gridColumnErrorComment.VisibleIndex = 1;
            this.gridColumnErrorComment.Width = 1291;
            // 
            // OlErrorForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 166);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.OKButton);
            this.MinimumSize = new System.Drawing.Size(450, 200);
            this.Name = "OlErrorForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Не удалось добавить следующие ТТ";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOlId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnErrorComment;


    }
}