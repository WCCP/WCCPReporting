﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class FilterClipboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.groupPOCList = new DevExpress.XtraEditors.GroupControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnLegalPOCName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridPOCCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOLId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUniqueId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupClipboardContent = new DevExpress.XtraEditors.GroupControl();
            this.memoClipboard = new DevExpress.XtraEditors.MemoEdit();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).BeginInit();
            this.groupPOCList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupClipboardContent)).BeginInit();
            this.groupClipboardContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoClipboard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(521, 449);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(440, 449);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Вставить";
            // 
            // groupPOCList
            // 
            this.groupPOCList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPOCList.Controls.Add(this.gridControl);
            this.groupPOCList.Location = new System.Drawing.Point(12, 152);
            this.groupPOCList.Name = "groupPOCList";
            this.groupPOCList.Size = new System.Drawing.Size(584, 291);
            this.groupPOCList.TabIndex = 4;
            this.groupPOCList.Text = "Список ТТ по данным из буфера обмена:";
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(3, 19);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(578, 269);
            this.gridControl.TabIndex = 5;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnRegionName,
            this.columnM3,
            this.columnLegalPOCName,
            this.columnFactName,
            this.gridPOCCode,
            this.columnCity,
            this.columnOLId,
            this.columnUniqueId});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            // 
            // columnRegionName
            // 
            this.columnRegionName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnRegionName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnRegionName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnRegionName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnRegionName.Caption = "Регион";
            this.columnRegionName.FieldName = "REGION_NAME";
            this.columnRegionName.MinWidth = 100;
            this.columnRegionName.Name = "columnRegionName";
            this.columnRegionName.OptionsColumn.AllowEdit = false;
            this.columnRegionName.OptionsColumn.AllowShowHide = false;
            this.columnRegionName.OptionsColumn.ReadOnly = true;
            this.columnRegionName.Visible = true;
            this.columnRegionName.VisibleIndex = 0;
            this.columnRegionName.Width = 100;
            // 
            // columnM3
            // 
            this.columnM3.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM3.Caption = "М3";
            this.columnM3.FieldName = "M3_NAME";
            this.columnM3.Name = "columnM3";
            this.columnM3.OptionsColumn.AllowEdit = false;
            this.columnM3.OptionsColumn.AllowShowHide = false;
            this.columnM3.OptionsColumn.ReadOnly = true;
            this.columnM3.Visible = true;
            this.columnM3.VisibleIndex = 1;
            // 
            // columnLegalPOCName
            // 
            this.columnLegalPOCName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnLegalPOCName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnLegalPOCName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnLegalPOCName.Caption = "Юр. имя ТТ";
            this.columnLegalPOCName.FieldName = "LEGAL_NAME";
            this.columnLegalPOCName.Name = "columnLegalPOCName";
            this.columnLegalPOCName.OptionsColumn.AllowEdit = false;
            this.columnLegalPOCName.OptionsColumn.AllowShowHide = false;
            this.columnLegalPOCName.OptionsColumn.ReadOnly = true;
            this.columnLegalPOCName.Visible = true;
            this.columnLegalPOCName.VisibleIndex = 2;
            // 
            // columnFactName
            // 
            this.columnFactName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFactName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFactName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFactName.Caption = "Факт. имя ТТ";
            this.columnFactName.FieldName = "TRADING_NAME";
            this.columnFactName.Name = "columnFactName";
            this.columnFactName.OptionsColumn.AllowEdit = false;
            this.columnFactName.OptionsColumn.AllowShowHide = false;
            this.columnFactName.OptionsColumn.ReadOnly = true;
            this.columnFactName.Visible = true;
            this.columnFactName.VisibleIndex = 3;
            // 
            // gridPOCCode
            // 
            this.gridPOCCode.AppearanceHeader.Options.UseTextOptions = true;
            this.gridPOCCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridPOCCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridPOCCode.Caption = "Код ТТ";
            this.gridPOCCode.FieldName = "OUTLET_CODE";
            this.gridPOCCode.Name = "gridPOCCode";
            this.gridPOCCode.OptionsColumn.AllowEdit = false;
            this.gridPOCCode.OptionsColumn.AllowShowHide = false;
            this.gridPOCCode.OptionsColumn.ReadOnly = true;
            this.gridPOCCode.Visible = true;
            this.gridPOCCode.VisibleIndex = 4;
            // 
            // columnCity
            // 
            this.columnCity.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCity.Caption = "Нас. пункт";
            this.columnCity.FieldName = "CITY";
            this.columnCity.Name = "columnCity";
            this.columnCity.OptionsColumn.AllowEdit = false;
            this.columnCity.OptionsColumn.AllowShowHide = false;
            this.columnCity.OptionsColumn.ReadOnly = true;
            this.columnCity.Visible = true;
            this.columnCity.VisibleIndex = 5;
            // 
            // columnOLId
            // 
            this.columnOLId.Caption = "OL_ID";
            this.columnOLId.FieldName = "OL_ID";
            this.columnOLId.Name = "columnOLId";
            this.columnOLId.OptionsColumn.AllowEdit = false;
            this.columnOLId.OptionsColumn.ReadOnly = true;
            // 
            // columnUniqueId
            // 
            this.columnUniqueId.Caption = "Unique ID";
            this.columnUniqueId.FieldName = "OL_UNIQUE_ID";
            this.columnUniqueId.Name = "columnUniqueId";
            this.columnUniqueId.OptionsColumn.AllowEdit = false;
            this.columnUniqueId.OptionsColumn.ReadOnly = true;
            // 
            // groupClipboardContent
            // 
            this.groupClipboardContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupClipboardContent.Controls.Add(this.memoClipboard);
            this.groupClipboardContent.Location = new System.Drawing.Point(12, 12);
            this.groupClipboardContent.Name = "groupClipboardContent";
            this.groupClipboardContent.Size = new System.Drawing.Size(584, 134);
            this.groupClipboardContent.TabIndex = 5;
            this.groupClipboardContent.Text = "Статус обработки данных из буфера обмена:";
            // 
            // memoClipboard
            // 
            this.memoClipboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoClipboard.Location = new System.Drawing.Point(3, 19);
            this.memoClipboard.Name = "memoClipboard";
            this.memoClipboard.Properties.ReadOnly = true;
            this.memoClipboard.Size = new System.Drawing.Size(578, 112);
            this.memoClipboard.TabIndex = 0;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // FilterClipboardForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(606, 482);
            this.Controls.Add(this.groupClipboardContent);
            this.Controls.Add(this.groupPOCList);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.MinimizeBox = false;
            this.Name = "FilterClipboardForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вставка ТТ из буфера обмена";
            this.Load += new System.EventHandler(this.FilterClipboardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).EndInit();
            this.groupPOCList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupClipboardContent)).EndInit();
            this.groupClipboardContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoClipboard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.GroupControl groupPOCList;
        private DevExpress.XtraEditors.GroupControl groupClipboardContent;
        private DevExpress.XtraEditors.MemoEdit memoClipboard;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn columnM3;
        private DevExpress.XtraGrid.Columns.GridColumn columnLegalPOCName;
        private DevExpress.XtraGrid.Columns.GridColumn columnFactName;
        private DevExpress.XtraGrid.Columns.GridColumn gridPOCCode;
        private DevExpress.XtraGrid.Columns.GridColumn columnCity;
        private DevExpress.XtraGrid.Columns.GridColumn columnOLId;
        private DevExpress.XtraGrid.Columns.GridColumn columnUniqueId;
    }
}