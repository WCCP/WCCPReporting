﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.Common;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FilterThomasResearchForm : FilterBaseForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterThomasResearchForm"/> class.
        /// </summary>
        public FilterThomasResearchForm() 
            : base (FilterFormType.ThomasResearch)
        {
            InitializeComponent();

            radioGroupOwner.SelectedIndex = 0;

            LoadData();
        }

        /// <summary>
        /// Gets the poc id list.
        /// </summary>
        /// <value>The poc id list.</value>
        public override string POCList
        {
            set
            {
            }
            get
            {
                return olList.GetSelectedTTs();
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the radioGroupOwner control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void radioGroupOwner_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        private void LoadData()
        {
            this.gridControlFilters.DataSource = DataProvider.GetThomasResearchFilters(radioGroupOwner.SelectedIndex == 0);
            this.txtBoxFilter.Text = String.Empty;
            this.olList.SetData(null);
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            foreach (int rowHandle in gridView.GetSelectedRows())
            {
                DataRowView row = ((DataRowView)gridView.GetRow(rowHandle));
                txtBoxFilter.Text = (row[this.gridColumnFilterText.FieldName]).ToString();
            }
        }

        private void applyFilter_Click(object sender, EventArgs e)
        {
            WaitManager.StartWait();
            foreach (int rowHandle in gridView.GetSelectedRows())
            {
                DataRowView row = ((DataRowView)gridView.GetRow(rowHandle));
                Int64 filterId = (Int64)(row[this.gridColumnFilterId.FieldName]);

                DataTable data = null;

                try
                {
                    data = DataProvider.GetThomasResearchOutlets(filterId);
                }
                catch 
                {
                    ErrorManager.ShowErrorBox(Resource.BedFilter);
                }

                this.olList.SetData(data);

            }
            WaitManager.StopWait();
        }
    }
}
