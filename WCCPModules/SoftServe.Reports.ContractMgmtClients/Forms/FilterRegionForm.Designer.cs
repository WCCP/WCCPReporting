﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class FilterRegionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.treeRegions = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeStaffList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.groupRegion = new DevExpress.XtraEditors.GroupControl();
            this.groupStaff = new DevExpress.XtraEditors.GroupControl();
            this.groupPOCList = new DevExpress.XtraEditors.GroupControl();
            this.olList = new SoftServe.Reports.ContractMgmtClients.OlList();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.treeRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaffList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRegion)).BeginInit();
            this.groupRegion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupStaff)).BeginInit();
            this.groupStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).BeginInit();
            this.groupPOCList.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(422, 433);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            // 
            // treeRegions
            // 
            this.treeRegions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeRegions.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeRegions.Location = new System.Drawing.Point(2, 22);
            this.treeRegions.Name = "treeRegions";
            this.treeRegions.OptionsView.ShowCheckBoxes = true;
            this.treeRegions.OptionsView.ShowColumns = false;
            this.treeRegions.OptionsView.ShowIndicator = false;
            this.treeRegions.Size = new System.Drawing.Size(232, 164);
            this.treeRegions.TabIndex = 1;
            this.treeRegions.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeRegions_AfterCheckNode);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 91;
            // 
            // treeStaffList
            // 
            this.treeStaffList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeStaffList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeStaffList.Location = new System.Drawing.Point(2, 22);
            this.treeStaffList.Name = "treeStaffList";
            this.treeStaffList.OptionsView.ShowCheckBoxes = true;
            this.treeStaffList.OptionsView.ShowColumns = false;
            this.treeStaffList.OptionsView.ShowIndicator = false;
            this.treeStaffList.Size = new System.Drawing.Size(229, 164);
            this.treeStaffList.TabIndex = 1;
            this.treeStaffList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeStaffList_AfterCheckNode);
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "treeListColumn2";
            this.treeListColumn2.FieldName = "treeListColumn2";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowEdit = false;
            this.treeListColumn2.OptionsColumn.ReadOnly = true;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            this.treeListColumn2.Width = 91;
            // 
            // groupRegion
            // 
            this.groupRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupRegion.Controls.Add(this.treeRegions);
            this.groupRegion.Location = new System.Drawing.Point(3, 3);
            this.groupRegion.Name = "groupRegion";
            this.groupRegion.Size = new System.Drawing.Size(236, 188);
            this.groupRegion.TabIndex = 3;
            this.groupRegion.Text = "Территория:";
            // 
            // groupStaff
            // 
            this.groupStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupStaff.Controls.Add(this.treeStaffList);
            this.groupStaff.Location = new System.Drawing.Point(3, 3);
            this.groupStaff.Name = "groupStaff";
            this.groupStaff.Size = new System.Drawing.Size(233, 188);
            this.groupStaff.TabIndex = 4;
            this.groupStaff.Text = "Персонал:";
            // 
            // groupPOCList
            // 
            this.groupPOCList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPOCList.Controls.Add(this.olList);
            this.groupPOCList.Location = new System.Drawing.Point(12, 212);
            this.groupPOCList.Name = "groupPOCList";
            this.groupPOCList.Size = new System.Drawing.Size(485, 215);
            this.groupPOCList.TabIndex = 4;
            this.groupPOCList.Text = "Список ТТ:";
            // 
            // olList
            // 
            this.olList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olList.Location = new System.Drawing.Point(2, 22);
            this.olList.Name = "olList";
            this.olList.Size = new System.Drawing.Size(481, 191);
            this.olList.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(341, 433);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "Вставить";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupRegion);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupStaff);
            this.splitContainer1.Size = new System.Drawing.Size(485, 194);
            this.splitContainer1.SplitterDistance = 242;
            this.splitContainer1.TabIndex = 5;
            // 
            // FilterRegionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(507, 467);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupPOCList);
            this.Controls.Add(this.btnCancel);
            this.MinimizeBox = false;
            this.Name = "FilterRegionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вставка ТТ";
            ((System.ComponentModel.ISupportInitialize)(this.treeRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaffList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRegion)).EndInit();
            this.groupRegion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupStaff)).EndInit();
            this.groupStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupPOCList)).EndInit();
            this.groupPOCList.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraTreeList.TreeList treeRegions;
        private DevExpress.XtraTreeList.TreeList treeStaffList;
        private DevExpress.XtraEditors.GroupControl groupRegion;
        private DevExpress.XtraEditors.GroupControl groupStaff;
        private DevExpress.XtraEditors.GroupControl groupPOCList;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private SoftServe.Reports.ContractMgmtClients.OlList olList;
    }
}