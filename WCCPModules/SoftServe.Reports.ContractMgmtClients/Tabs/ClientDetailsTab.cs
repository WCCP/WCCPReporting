﻿using System;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.ContractMgmtClients;
using DevExpress.XtraTab.ViewInfo;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientDetailsTab : CommonBaseTab //, IPrint
    {
        private ClientDetailsControl clientDetailsCtrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientDetailsTab"/> class.
        /// </summary>
        /// <param name="bruc">The bruc.</param>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        /// <param name="tabName">Name of the tab.</param>
        public ClientDetailsTab(BaseReportUserControl bruc, string tabName, int clientId, UserPreferences preferences)
        {
            if (!this.DesignMode && bruc != null)
            {
                this.Bruc = bruc;
                this.Preferences = preferences;
                ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
                clientDetailsCtrl = new ClientDetailsControl(this, clientId);

                Init(bruc, clientDetailsCtrl, tabName);

                Bruc.TabControl.CloseButtonClick += new EventHandler(TabControl_CloseButtonClick);
            }
        }

        /// <summary>
        /// Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public ClientDetailsControl Content
        {
            get
            {
                return base.UserControl as ClientDetailsControl;
            }
        }

        /// <summary>
        /// Parent_s the tool button visibility check.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void parent_ToolButtonVisibilityCheck(object sender, XtraTabPage selectedPage)
        {
            if(this == selectedPage)
            {
                DevExpress.XtraBars.BarItem barItem = sender as DevExpress.XtraBars.BarItem;
                if(barItem != null)
                {
                    barItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                }
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData()
        {
            if (Content != null)
            {
                Content.LoadData();
            }
        }

        /// <summary>
        /// Handles the CloseButtonClick event of the TabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabControl_CloseButtonClick(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            if (arg != null && arg.Page == this)
            {
                Bruc.TabControl.TabPages.Remove(this);
            }
        }

        /// <summary>
        /// Overrides base UpdateVisualState method, and triggers inner control updating.
        /// </summary>
        public override void UpdateVisualState()
        {
            base.UpdateVisualState();
            clientDetailsCtrl.UpdateControlVisualState();
        }
    }
}
