﻿using System;
using System.Collections.Generic;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using SoftServe.Reports.ContractMgmtClients;
using System.Data;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientListTab : CommonBaseTab, IPrint
    {
        private ClientListControl clientListCtrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListTab"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public ClientListTab(BaseReportUserControl bruc)
        {
            if (!this.DesignMode && bruc != null)
            {
                this.Bruc = bruc;
                clientListCtrl = new ClientListControl(this);
                Init(bruc, clientListCtrl, Resource.ClientList);

                ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            }
        }

        /// <summary>
        /// Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public ClientListControl Content
        {
            get
            {
                return base.UserControl as ClientListControl;
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData()
        {
            Content.ClientList = DataProvider.ContractClientList;
        }

        /// <summary>
        /// Prepare container of elements for exporting
        /// </summary>
        /// <returns></returns>
        public CompositeLink PrepareCompositeLink()
        {
            CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
            compositeLink.Links.Add(new PrintableComponentLink() { Component = Content.GridControl });
            compositeLink.CreateDocument();
            return compositeLink;
        }
    }
}
