﻿namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public static class Constants
    {
        public const int TAB_MAX_COUNT = 10;
        public const int ErrorClientWithThisNameExists = -123;
        public const int MODULE_ID = 1;
        public const string DELIMITER_ID_LIST = ",";

        // Stored Procedure Names
        public const string SP_GET_CONTRACT_STATUS_LIST = "spDW_CM_GetContractStatusesList";
        public const string SP_GET_CHANNEL_LIST = "spDW_ChannelList";
        public const string SP_GET_CLIENT_LIST = "spDW_CCM_ContractClientsList";
        public const string SP_GET_CLIENT_COMMON_FIELDS = "spDW_CCM_ContractClientDetails";
        public const string SP_GET_CLIENT_COMMON_AVG_SALES = "spDW_CCM_ContractClientBeerMonthlyAverageSales";
        public const string SP_GET_CLIENT_COMMON_POC_LIST = "spDW_CCM_ContractClientOutletTypesCountList";
        public const string SP_GET_CLIENT_COMMON_DISTRIBUTORS = "spDW_CCM_ContractClientDistributors";
        public const string SP_GET_CLIENT_COMMON_REGIONS = "spDW_CCM_GetRegions";
        public const string SP_GET_CLIENT_COMMON_DISTRICTS = "spDW_CCM_GetRegionDistricts";
        public const string SP_GET_CLIENT_COMMON_CITY_TYPE = "spDW_CCM_GetSettlementType";
        public const string SP_GET_CLIENT_COMMON_CITY_LIST = "spDW_CCM_GetDistrictSettlements";
        public const string SP_GET_CLIENT_COMMON_TAXES_LIST = "spDW_CCM_FiscalTypesList";
        public const string SP_GET_CLIENT_ADDRESSES_LIST = "spDW_CCM_ContractClientOutletList";
        public const string SP_GET_CONTRACT_HISTORY = "spDW_CCM_GetContractHistory";
        public const string SP_GET_ACTIVE_CONTRACTS = "spDW_CCM_GetActiveContractsList";
        public const string SP_GET_CONTRACT_EFFICIENCY = "spDW_CCM_ContractEfficiencyInvestments";
        public const string SP_GET_CONTRACT_SALES = "spDW_CCM_ContractSales";
        public const string SP_GET_CONTRACT_EXPENSES = "spDW_CCM_ContractExpenses";
        public const string SP_UPDATE_CLIENT_COMMON = "spDW_CCM_UpdateContractClient";
        public const string SP_INSERT_CLIENT_COMMON = "spDW_CCM_InsertContractClient";
        public const string SP_DELETE_CLIENT_COMMON = "spDW_CCM_DeleteContractClient";
        public const string SP_SET_OUTLET_DEPENDENT_DATA = "spDW_CCM_SetOutletDependentData";
        public const string SP_DELETE_ADDRESS = "spDW_CCM_ADDR_DeleteOutlets";
        public const string SP_INSERT_ADDRESS = "spDW_CCM_ADDR_InsertOutlets";
        public const string SP_GET_CLIENT_ADDRESSES_LIST_BY_OLID = "spDW_CCM_GetOutletData";
        public const string SP_GET_ACCESS_LEVEL = "spDW_CCM_GetAccessLevel";
        public const string SP_GET_USER_LEVEL = "spDW_GetUserLevel";
        public const string SP_GET_HAS_ACCESS_TO_ADDRESSES = "spDW_CCM_UserHasAccessToAllClientOutlets";
       
        // Stored Procedure Parameter Names
        public const string SP_GET_CLIENT_DETAILS_PARAM1 = "ID";
        public const string SP_GET_CLIENT_DETAILS_PARAM2 = "OUTLET_ID";
        public const string SP_GET_CLIENT_COMMON_AVG_SALES_PARAM1 = "ClientID";
        public const string SP_GET_CLIENT_COMMON_POC_LIST_PARAM1 = "CLIENT_ID";
        public const string SP_GET_CLIENT_COMMON_DISTRIBUTORS_PARAM1 = "CLIENT_ID";
        public const string SP_GET_CLIENT_COMMON_DISTRIBUTORS_PARAM2 = "CHANNEL_ID";
        public const string SP_GET_CLIENT_COMMON_DISTRICTS_PARAM1 = "Region_ID";
        public const string SP_GET_CLIENT_COMMON_CITY_TYPE_PARAM1 = "Region_ID";
        public const string SP_GET_CLIENT_COMMON_CITY_LIST_PARAM1 = "District_ID";
        public const string SP_GET_CLIENT_COMMON_CITY_LIST_PARAM2 = "Settlement_Type";
        public const string SP_GET_CLIENT_ADDRESSES_LIST_PARAM1 = "CLIENT_ID";
        public const string SP_GET_CONTRACT_HISTORY_PARAM_CLIENT_ID = "@ClientId";
        public const string SP_GET_ACTIVE_CONTRACTS_PARAM_CLIENT_ID = "@ClientId";
        public const string SP_GET_CONTRACT_EFFICIENCY_PARAM_CONTRACT_ID = "@ContractId";
        public const string SP_GET_CONTRACT_SALES_PARAM_CONTRACT_ID = "@ContractId";
        public const string SP_GET_CONTRACT_EXPENSES_PARAM_CONTRACT_ID = "@ContractId";
        public const string SP_GET_CLIENT_ADDRESSES_LIST_BY_OLID_PARAM1 = "OUTLET_LIST";
        public const string SP_PARAM_OUTLET_LIST = "@ClientOutletsList";
        public const string SP_GET_HAS_ACCESS_TO_ADDRESSES_PARAM1 = "CLIENT_ID";

        // Details modifications
        public const string SP_UPDATE_CLIENT_COMMON_PARAM1 = "ID";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM2 = "LEGAL_NAME";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM3 = "LEGAL_ADDRESS";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM4 = "EDRPOU";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM5 = "IIN";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM6 = "FISCALTYPE_ID";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM7 = "CITY_ID";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM8 = "ACCOUNTANT";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM9 = "ACCOUNTANT_PHONE";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM10 = "NOTES";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM11 = "ASSIGNEE";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM12 = "ASSIGNEE_APPOINTMENT";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM13 = "ASSIGNEE_PHONE";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM14 = "ASSIGNEE_MOBILE";
        public const string SP_UPDATE_CLIENT_COMMON_PARAM15 = "ASSIGNEE_EMAIL";

        // Address Modifications
        public const string SP_SAVE_ADDRESS_PARAM_CLIENT_ID = "CLIENT_ID";
        public const string SP_SAVE_ADDRESS_PARAM_OUTLETS_LIST = "OUTLETS_LIST";

        // Fields for Tab Common Information
        public const string CD_FIELD_LEGAL_NAME = "LEGAL_NAME";
        public const string CD_FIELD_EDRPOU = "EDRPOU";
        public const string CD_FIELD_FISCALTYPE_ID = "FISCALTYPE_ID";
        public const string CD_FIELD_IIN = "IIN";
        public const string CD_FIELD_LEGAL_ADDRESS = "LEGAL_ADDRESS";
        public const string CD_FIELD_NOTES = "NOTES";
        public const string CD_FIELD_DISTRICT_ID = "DISTRICT_ID";
        public const string CD_FIELD_REGION_ID = "REGION_ID";
        public const string CD_FIELD_CITY_TYPE_ID = "SETTLEMENT_ID";
        public const string CD_FIELD_CITY_ID = "CITY_ID";
        public const string CD_FIELD_ACCOUNTANT = "ACCOUNTANT";
        public const string CD_FIELD_ACCOUNTANT_PHONE = "ACCOUNTANT_PHONE";
        public const string CD_FIELD_ASSIGNEE_APPOINTMENT = "ASSIGNEE_APPOINTMENT";
        public const string CD_FIELD_ASSIGNEE = "ASSIGNEE";
        public const string CD_FIELD_ASSIGNEE_PHONE = "ASSIGNEE_PHONE";
        public const string CD_FIELD_ASSIGNEE_MOBILE = "ASSIGNEE_MOBILE";
        public const string CD_FIELD_ASSIGNEE_EMAIL = "ASSIGNEE_EMAIL";
        public const string CD_FIELD_CREATED_BY = "CREATED_BY";
        public const string CD_FIELD_CREATED_DATE = "CREATED_DATE";

        public const string CD_FIELD_CHANNEL_TYPE_ID = "ChanelType_id";
        public const string CD_FIELD_CHANNEL_TYPE_NAME = "ChanelType";
        public const string FIELD_REGION_NAME = "Region_name";

        // Active Contract
        public const string SP_GET_EXPENSES_FIELDS_PARAM1 = "CONTRACT_ID";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE_PARAM1 = "CONTRACT_ID";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE_PARAM2 = "CONTRACT_BEGIN_DATE";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE_PARAM3 = "CONTRACT_TERM_LENGTH";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE_PARAM4 = "XML_DATA";
        public const string SP_SET_EXPENSES_INDICATORS_TABLE_PARAM1 = "CONTRACT_ID";
        public const string SP_SET_EXPENSES_INDICATORS_TABLE_PARAM2 = "XML_DATA";
        public const string SP_SET_EXPENSES_PLAN_PARAM1 = "CONTRACT_ID";
        public const string SP_SET_EXPENSES_PLAN_PARAM2 = "EXPENSES_LIST_XML";
        public const string SP_SET_EXPENSES_FIELDS_PARAM1 = "CONTRACT_ID";
        public const string SP_SET_EXPENSES_FIELDS_PARAM2 = "XML_DATA";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM1 = "ContractId";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM2 = "IndicatorId";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM3 = "PeriodId";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM4 = "BaseIndicators";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD_PARAM5 = "Indicators";

        // Active Contract
        public const string SP_SET_EXPENSES_FIELDS = "spDW_CM_SetContractInvestmentsBaseIndicators";
        public const string SP_SET_EXPENSES_PLAN = "spDW_CM_SetExpensesPlan";
        public const string SP_GET_EXPENSES_FIELDS = "spDW_CM_GetContractBaseInvestmentIndicators";
        public const string SP_GET_EXPENSES_PAYMENT_SCHEMA_LIST = "spDW_CM_PaymentSchema";
        public const string SP_GET_EXPENSES_PAYMENT_TYPE_LIST = "spDW_CM_PaymentKind";
        public const string SP_GET_EXPENSES_INDICATORS_RECALC_BY_FIELD = "spDW_CM_GetDynamicIndicators";
        public const string SP_GET_EXPENSES_INDICATORS_TABLE = "spDW_CM_GetContractInvestments";
        public const string SP_SET_EXPENSES_INDICATORS_TABLE = "spDW_CM_SetContractInvestments";

        // Active Contract
        public const string FIElD_ACTIVE_CONTRACT_PAYMENT_VALUE = "PAYMENTKIND_ID";
        public const string FIElD_ACTIVE_CONTRACT_PAYMENT_DISPLAY = "PAYMENT_NAME";

        public const string FIElD_ACTIVE_CONTRACT_SCHEMA_VALUE = "ID";
        public const string FIElD_ACTIVE_CONTRACT_SCHEMA_DISPLAY = "SCHEMA_NAME";
        
        public const string FIELD_ACTIVE_CONTRACT_ID = "ID";
        public const string FIELD_ACTIVE_CONTRACT_PLANNED_SIGN_DATE = "PLANNED_SIGN_DATE";
        public const string FIELD_ACTIVE_CONTRACT_TERM = "TERM_LEN";
        public const string FIELD_COST_PAYMENT_SCHEMA_ID = "PAYMENT_SCHEMA_ID";
        public const string FIELD_COST_PAYMENT_TYPE_ID = "PAYMENT_KIND";
        public const string FIELD_COST_PREV_AVRVOLUME = "PREV_AVRVOLUME";
        public const string FIELD_COST_MARKET_TRAND = "MARKET_TRAND";
        public const string FIELD_COST_TRANDED_AVRVOLUME = "TRANDED_AVRVOLUME";
        public const string FIELD_COST_PLANNED_VOLUMEUPLIFT = "PLANNED_VOLUMEUPLIFT";
        public const string FIELD_COST_UPLIFTED_AVRVOLUME = "UPLIFTED_AVRVOLUME";
        public const string FIELD_COST_PLANNED_OUTLETSQANTITY = "PLANNED_OUTLETSQUANTITY";
        public const string FIELD_COST_EXPENSES_PEROUTLET = "EXPENSES_PEROUTLET";
        public const string FIELD_COST_PLANNED_AVRMACOPERDAL = "PLANNED_AVRMACOPERDAL";
        public const string FIELD_COST_HAS_ADVERT = "HAS_ADVERTISMENT";

        //Fields for ClientDetailsContracts
        public const string CD_FIELD_CONTRACT_ID = "ID";
        public const string CD_FIELD_CONTRACT_NUMBER = "CONTRACTNO";
        public const string CD_FIELD_CONTRACT_NAME = "LEGAL_NAME";
        public const string CD_FIELD_CONTRACT_DATE_SIGNED = "DATE_SIGNED";
        public const string CD_FIELD_CONTRACT_VALID_TILL = "VALID_TILL";
        public const string CD_FIELD_CONTRACT_STATUS = "STATUS";
        public const string CD_FIELD_CONTRACT_PLANNED_SIGN_DATE = "PLANNED_SIGN_DATE";
        public const string CD_FIELD_CONTRACT_TERM_LEN = "TERM_LEN";

        //Fields for controls tags
        public const string CD_TAG_CONTRACTS_DROP_DOWN = "cbContractsDropDown";


        //Tags for ClientDetailsHistory
        public const string CDH_TAG_VALID_TILL_EXPRESSION = "validTill";
        public const string CDH_FIELD_VALID_TILL = "VALID_TILL";
        public const string CDH_FIELD_CONTRACT_STATUS = "CONTRACT_STATUS_ID";
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ChannelType
    {
        /// <summary>
        /// 
        /// </summary>
        All = 0,
        /// <summary>
        /// 
        /// </summary>
        OnTrade = 1,
        /// <summary>
        /// 
        /// </summary>
        OffTrade = 2,
        /// <summary>
        /// 
        /// </summary>
        KaTrade = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ClientDetailsTabType
    {
        /// <summary>
        /// 
        /// </summary>
        Common = 0,
        /// <summary>
        /// 
        /// </summary>
        Addresses = 1,
        /// <summary>
        /// 
        /// </summary>
        Contracts = 2,
        /// <summary>
        /// 
        /// </summary>
        History = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum FilterFormType
    {
        /// <summary>
        /// 
        /// </summary>
        Clipboard,
        /// <summary>
        /// 
        /// </summary>
        ThomasResearch,
        /// <summary>
        /// 
        /// </summary>
        Region
    }

    /// <summary>
    /// 
    /// </summary>
    public enum AddressRowStatus
    {
        /// <summary>
        /// 
        /// </summary>
        Common = 2,
        /// <summary>
        /// 
        /// </summary>
        New = 1,
        /// <summary>
        /// 
        /// </summary>
        Removed = 9
    }

    /// <summary>
    /// 
    /// </summary>
    public enum UserLevelType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        M2 = 2,
        /// <summary>
        /// 
        /// </summary>
        M3 = 3,
        /// <summary>
        /// 
        /// </summary>
        M4 = 4,
        /// <summary>
        /// 
        /// </summary>
        M5 = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public enum AccessLevelType
    {
        // should be set as bit flags (logical |)
        Unknown = -1,
        /// <summary>
        /// 
        /// </summary>
        ReadOnly = 0,
        /// <summary>
        /// 
        /// </summary>
        ReadWrite = 2,
        //XXXXX = 4, // next one 8 and so on..
    }
}
