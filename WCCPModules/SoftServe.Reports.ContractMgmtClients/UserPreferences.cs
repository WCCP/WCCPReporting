﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// Class that defines user preferences for something like date edits, check boxes, updown editors etc. 
    /// </summary>
    public class UserPreferences
    {
        /// <summary>
        /// Gets or sets date value from specific rows will be highlighted.
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Gets or sets date value till specific rows will be highlighted.
        /// </summary>
        public DateTime DateTo { get; set; }
    }
}
