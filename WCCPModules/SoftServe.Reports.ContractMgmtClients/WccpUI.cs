﻿using System;
using System.Collections.Generic;
using System.Text;
using Logica.Reports.Common;
using Logica.Reports;
using ModularWinApp.Core.Interfaces;
using System.ComponentModel.Composition;
using System.Windows.Forms;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpcmclients.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Shows the UI.
        /// </summary>
        /// <param name="isSkined">The is skined.</param>
        /// <param name="reportCaption">The report caption.</param>
        /// <returns></returns>
        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                _reportControl = new WccpUIControl(reportId);
                _reportCaption = reportCaption;

                return 0;
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 0;
        }

        /// <summary>
        /// Closes the UI.
        /// </summary>
        public void CloseUI()
        {
        }

        public bool AllowClose()
        {
            return true;
        }
    }
}
