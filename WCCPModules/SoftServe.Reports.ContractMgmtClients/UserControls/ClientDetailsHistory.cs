﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using Logica.Reports.Common.Enums;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientDetailsHistory : CommonBaseControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientDetailsHistory"/> class.
        /// </summary>
        public ClientDetailsHistory(CommonBaseTab parentTab)
        {
            InitializeComponent();
            this.ParentTab = parentTab;
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="firstAddressId">The first address id.</param>
        public override void LoadData(int clientId, long firstAddressId)
        {
            this.gwContractHistory.DataSource = DataProvider.GetClientHistory(clientId);
            LoadStatusList();
            LoadRepositories();

            base.LoadData(clientId, firstAddressId);
            UpdateRowsHighlightState();
        }

        /// <summary>
        /// Updates highlighting state of grid view.
        /// </summary>
        private void UpdateRowsHighlightState()
        {
            StyleFormatConditionBase condition = gridView.FormatConditions[1];
            condition.Expression = Utility.HighlightingExpression.BuildDateRangeHighlightExpression(
                this.ParentTab.Preferences.DateFrom,
                this.ParentTab.Preferences.DateTo, Constants.CDH_FIELD_VALID_TILL,
                string.Format("[{0}] == {1}", Constants.CDH_FIELD_CONTRACT_STATUS, (int)ContractStatus.InWork));

        }

        private static DataTable contractStatusList;        
       /// <summary>
       /// Loads list of statuses.
       /// </summary>
        private void LoadStatusList()
        {
            contractStatusList = DataProvider.ContractStatusList;
        }

        /// <summary>
        /// Loads editor's repositories.
        /// </summary>
        private void LoadRepositories()
        {
            repositoryStatusLookUp.DataSource = contractStatusList;
        }

        /// <summary>
        /// Updates control's visual state like highlights, fonts, etc.
        /// </summary>
        public override void UpdateControlVisualState()
        {
            base.UpdateControlVisualState();
            this.UpdateRowsHighlightState();
        }
    }
}
